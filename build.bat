@echo off
set SETTINGSFILE=settings.xml
set LOCALREPO=localrepo
set PATH=%cd%;%PATH%
call mvn325.bat clean install -f jira-project/pom.xml -Dmaven.test.skip -Dmaven.test.skip -s %SETTINGSFILE% -Dmaven.repo.local=%cd%\%LOCALREPO% %* 
if %errorlevel% neq 0 exit /b %errorlevel%
call mvn325.bat clean install -f jira-project/jira-components/jira-webapp/pom.xml -Dmaven.test.skip -Pbuild-from-source-dist -Dmaven.test.skip -s %SETTINGSFILE% -Dmaven.repo.local=%cd%\%LOCALREPO% %* 
if %errorlevel% neq 0 exit /b %errorlevel%
call mvn325.bat clean package -Dmaven.test.skip -f jira-project/jira-distribution/jira-webapp-dist/pom.xml -Dmaven.test.skip -s %SETTINGSFILE% -Dmaven.repo.local=%cd%\%LOCALREPO% %* 
if %errorlevel% neq 0 exit /b %errorlevel%
