package com.atlassian.jira.webtest.capture;

import com.atlassian.buildeng.hallelujah.api.model.TestCaseName;
import com.atlassian.jira.functest.framework.WebTestDescription;
import com.atlassian.jira.hallelujah.WebTestListenerToClientListenerAdapter;
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

public class FFMpegRunListener extends RunListener {

    private final FFMpegSuiteListener ffMpegSuiteListener;

    public FFMpegRunListener() {
        this.ffMpegSuiteListener = new FFMpegSuiteListener(input -> true);
    }

    @Override
    public void testRunStarted(final Description description) throws Exception {
        ffMpegSuiteListener.suiteStarted(fakeWebTestDescription());
    }

    @Override
    public void testRunFinished(final Result result) throws Exception {
        ffMpegSuiteListener.suiteFinished(fakeWebTestDescription());
    }

    @Override
    public void testStarted(final Description description) throws Exception {
        ffMpegSuiteListener.testStarted(toWebTestDescription(description));
    }

    @Override
    public void testFailure(final Failure failure) throws Exception {
        ffMpegSuiteListener.testFailure(toWebTestDescription(failure.getDescription()), failure.getException());
    }

    @Override
    public void testFinished(final Description description) throws Exception {
        ffMpegSuiteListener.testFinished(toWebTestDescription(description));
    }

    private WebTestDescription toWebTestDescription(final Description description) {
        final TestCaseName testCaseName = new TestCaseName(description.getClassName(), description.getMethodName());
        return WebTestListenerToClientListenerAdapter.WebTestClientDescription.fromTestCaseName(testCaseName);
    }

    private WebTestDescription fakeWebTestDescription() {
        return WebTestListenerToClientListenerAdapter.WebTestClientDescription.fakeSuite();
    }
}