# JIRA Source Code
 
## Getting Started
[Setting up jira dev environment](https://extranet.atlassian.com/display/JIRADEV/Setting+up+the+JIRA+dev+environment)

[Development Handbook](https://extranet.atlassian.com/display/JIRADEV/JIRA+Development+Handbook)

[Rules of Engagement](https://extranet.atlassian.com/display/JIRADEV/The+JIRA+development+process)

## Running from Source

```
> ./jmake run
```

or

```
> ./jmake debug
```

Run Unit tests:

```
> ./jmake unit-tests
```

See also

```
> ./jmake --help
> ./jmake <command> -h
```

## Working on master

First create an issue branch from master named like **issue/JDEV-1234-my-change** (Stash UI will help you)

Watch the branch builds on Tier 1 and Tier 2 builds:


## these two doesn't seem to work
[Tier 1](https://server-gdn-bamboo.internal.atlassian.com/browse/MASTERONE)

[Tier 2](https://server-gdn-bamboo.internal.atlassian.com/browse/MASTERTWO)

You may find the [Branch view](https://server-gdn-bamboo.internal.atlassian.com/branchinator) useful. Use the repository `JIRA master stash` to find most of your branch builds.

## Code Quality

### Front-end / JavaScript

The JavaScript code quality checks can work within IDEA or on the command-line. You can either:

1. Run `npm install` from your project checkout directory.
2. Run `./node_modules/.bin/eslint .`.

Or, from IDEA,

1. Open your IDEA project preferences (`<cmd>`+`,`).
2. Find ESLint support under __Languages & Frameworks__ > __JavaScript__ > __Code Quality Tools__ > __ESLint__.
3. Enable ESLint support.
4. For the "ESLint package" setting, choose the locally installed eslint node module (i.e., "<project checkout directory>/node_modules/eslint").
5. The "Configuration file" should be set to "Search for .eslintrc".

Now every file will be automatically checked.

Contributions and improvements to the code quality rules are tracked in [the front-end code quality project](https://extranet.atlassian.com/x/v4HFl).
