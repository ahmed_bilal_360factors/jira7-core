(function(JIRA) {
    (JIRA.WorkflowDesigner
        && JIRA.WorkflowDesigner.IO
        && JIRA.WorkflowDesigner.IO.LayoutAutoSaver.disableMessage());
    window.onbeforeunload = null;
}(JIRA));
