package com.atlassian.jira.webtest.webdriver.util;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.webdriver.utils.element.ElementLocated;
import com.atlassian.webdriver.utils.element.WebDriverPoller;
import org.openqa.selenium.By;

/**
 * @since v6.4
 */
public class AUIDialogs {
    public static final String CLOSE_ALL_JS = "AJS.$('.aui-inline-dialog').hide()";
    private final JiraTestedProduct jira;

    public AUIDialogs(final JiraTestedProduct jira) {
        this.jira = jira;
    }

    public void waitForAny(int timeout) {
        WebDriverPoller poller = new WebDriverPoller(jira.getTester().getDriver());

        poller.waitUntil(new ElementLocated(By.cssSelector(".aui-inline-dialog")), timeout);
    }

    public void closeAll() {
        jira.getTester().getDriver().executeScript(CLOSE_ALL_JS);
    }
}
