package com.atlassian.jira.pageobjects.project.issuetypes;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.util.List;

abstract class AbstractIssueTypeWorkflowTab extends IssueTypeTab {
    @ElementBy(id = "workflow-designer")
    private PageElement designer;

    @ElementBy(id = "project-config-header-descriptor-subtitle")
    private PageElement workflowName;

    @ElementBy(className = "workflow-info__msg-with-actions")
    private PageElement editableControls;

    private final String projectKey;
    private final long issueTypeId;

    AbstractIssueTypeWorkflowTab() {
        projectKey = null;
        issueTypeId = -1;
    }

    AbstractIssueTypeWorkflowTab(String projectKey, long issueTypeId) {
        this.projectKey = projectKey;
        this.issueTypeId = issueTypeId;
    }

    public IssueTypeFieldsTab gotoFields() {
        return getIssueTypeHeader().gotoFields();
    }

    public boolean isSwitcherPresent() {
        return getIssueTypeHeader().isSwitcherPresent();
    }

    public String getSubtitle() {
        return StringUtils.stripToNull(workflowName.getText());
    }

    TimedCondition isAt(boolean editable) {
        List<TimedQuery<Boolean>> conditions = Lists.newArrayList();
        conditions.add(pageBinder.bind(LoadingBarrier.class).notLoading());
        conditions.add(designer.timed().isPresent());

        TimedQuery<Boolean> editableCondition = editableControls.timed().isPresent();
        if (!editable) {
            editableCondition = Conditions.not(editableCondition);
        }
        conditions.add(editableCondition);

        return Conditions.and(conditions);
    }

    @Override
    public final String getUrl() {
        if (projectKey == null || issueTypeId < 0) {
            throw new IllegalStateException("Need project and issue type to go direct to URL.");
        }

        return getUrl(projectKey, issueTypeId);
    }

    abstract String getUrl(final String projectKey, final long issueTypeId);
}
