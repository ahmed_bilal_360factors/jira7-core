package com.atlassian.jira.pageobjects.components.license;


import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

import javax.annotation.Nonnull;

/**
 * @since v7.0
 */
public class UsersExceededBanner extends LicenseContent {
    @ElementBy(cssSelector = "*[role=banner]")
    private PageElement content;

    @Nonnull
    @Override
    protected PageElement getContent() {
        return content;
    }

    @Override
    protected PageElement getMacLink() {
        return getContent().find(By.id("exceeded-banner-mac-link"));
    }

    public PageElement getManageLink() {
        return getContent().find(By.id("exceeded-banner-access-link"));
    }

    @Override
    public void dismiss() {
        throw new UnsupportedOperationException("Banners can not be dismissed.  Review test logic may be wrong");
    }
}
