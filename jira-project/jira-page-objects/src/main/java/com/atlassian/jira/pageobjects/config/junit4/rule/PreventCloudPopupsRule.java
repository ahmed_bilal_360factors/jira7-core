package com.atlassian.jira.pageobjects.config.junit4.rule;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.PopupCollector;
import com.atlassian.jira.pageobjects.config.PopupPreventer;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import javax.inject.Inject;

/**
 * Prevents popups from showing up during tests.
 *
 * @since v7.1.0
 */
public class PreventCloudPopupsRule implements TestRule {
    @Inject
    private JiraTestedProduct jira;

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                final PopupPreventer popupPreventer = new PopupPreventer(jira);
                final PopupCollector popupCollector = new PopupCollector().bundledWithCloud();
                popupPreventer.preventPopups(
                        popupCollector.collectHelpTipIds(),
                        popupCollector.collectNotificationFlagIds()
                );
                base.evaluate();
            }
        };
    }

}
