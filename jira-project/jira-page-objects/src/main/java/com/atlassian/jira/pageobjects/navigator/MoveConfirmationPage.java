package com.atlassian.jira.pageobjects.navigator;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.google.common.collect.Iterables;
import org.openqa.selenium.By;

import java.util.List;

/**
 * Changes confirmation page for Bulk Move
 *
 * @since v7.0
 */
public class MoveConfirmationPage extends AbstractJiraPage {
    @ElementBy(id = "bulkedit")
    protected PageElement form;

    @ElementBy(id = "next")
    protected PageElement confirm;

    @ElementBy(cssSelector = "div.module", within = "form")
    Iterable<PageElement> moveIssuesConfirmContainers;

    @ElementBy(id = "bulk-confirm-title")
    protected PageElement title;

    @Override
    public TimedCondition isAt() {
        return title.timed().isPresent();
    }

    @Override
    public String getUrl() {
        throw new UnsupportedOperationException("Not implemented");
    }

    public OperationProgress confirm() {
        confirm.click();
        return pageBinder.bind(OperationProgress.class);
    }

    public String getFormText() {
        return form.getText();
    }

    public PageElement getForm() {
        return form;
    }

    public Iterable<MoveIssuesConfirmContainer> getMoveIssuesConfirmContainers() {
        return Iterables.transform(moveIssuesConfirmContainers, pe -> pageBinder.bind(MoveIssuesConfirmContainer.class, pe));
    }

    public static class MoveIssuesConfirmContainer {
        private PageElement parent;

        private PageElement targetProjectEl;
        private PageElement targetIssueTypeEl;
        private PageElement targetParentIssueEl;
        private PageElement updateFieldsTablEl;

        public MoveIssuesConfirmContainer(PageElement parent) {
            this.parent = parent;

            this.targetIssueTypeEl = parent.find(By.cssSelector("[data-target-issuetype]"));
            this.targetParentIssueEl = parent.find(By.cssSelector("[data-parent-issue-key]"));
            this.targetProjectEl = parent.find(By.cssSelector("[data-target-project]"));
            this.updateFieldsTablEl = parent.find(By.cssSelector(".movedFields"));
        }

        public String getTargetProject() {
            return getDataAttribute(targetProjectEl, "data-target-project");
        }

        public String getTargetParentIssue() {
            return getDataAttribute(targetParentIssueEl, "data-parent-issue-key");
        }

        public String getTargetIssueType() {
            return getDataAttribute(targetIssueTypeEl, "data-target-issuetype");
        }

        private String getDataAttribute(PageElement element, String attribute) {
            if (element != null && element.timed().isPresent().now()) {
                return element.getAttribute(attribute);
            }
            return null;
        }

        public static class UpdateField {
            String field;
            String value;

            public UpdateField(final String field, final String value) {
                this.field = field;
                this.value = value;
            }

            public String getField() {
                return field;
            }

            public void setField(final String field) {
                this.field = field;
            }

            public String getValue() {
                return value;
            }

            public void setValue(final String value) {
                this.value = value;
            }
        }

        public TimedCondition getUpdateFieldsTabPresent() {
            return updateFieldsTablEl.timed().isPresent();
        }

        public Iterable<UpdateField> getUpdateFields() {
            Poller.waitUntilTrue("Update fields table is not present", getUpdateFieldsTabPresent());
            return Iterables.transform(updateFieldsTablEl.findAll(By.cssSelector("tbody tr")), row -> {
                List<PageElement> tds = row.findAll(By.tagName("td"));
                return new UpdateField(tds.get(0).getText(), tds.get(1).getText());
            });
        }
    }
}
