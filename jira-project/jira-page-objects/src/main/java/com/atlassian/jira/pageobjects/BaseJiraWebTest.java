package com.atlassian.jira.pageobjects;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.rules.TestRule;

/**
 * Lightweight base class mainly containing annotations common for all tests.
 * <p>
 * DO NOT put any utility methods here. Use page objects framework for that. In fact, do not put anything here without
 * permission :P
 * </p>
 *
 * @since v4.4
 */
public abstract class BaseJiraWebTest extends JiraWebDriverTest {
    @ClassRule
    public static final TestRule jiraWebTestClassRules = (base, description) -> getBaseClassRule().apply(base, description);

    @Rule
    public final TestRule webTestRule = getBaseRule();
}
