package com.atlassian.jira.pageobjects.components.license;

import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

import javax.annotation.Nonnull;
import javax.inject.Inject;

/**
 * @since v6.4
 */
public abstract class LicenseContent {
    @Inject
    private TraceContext context;

    @Nonnull
    protected abstract PageElement getContent();

    protected abstract PageElement getMacLink();

    public void clickMackLink() {
        getMacLink().click();
    }

    public abstract void dismiss();

    public boolean isPresent() {
        return isPresentAndVisible(getContent());
    }

    public String getMacUrl() {
        final PageElement pageElement = getMacLink();
        if (isPresentAndVisible(pageElement)) {
            return pageElement.getAttribute("href");
        }
        return null;
    }

    public void remindLater() {
        dismiss();
    }

    public int days() {
        if (getContent().isPresent()) {
            return Integer.parseInt(getContent().getAttribute("data-days"));
        } else {
            return Integer.MIN_VALUE;
        }
    }

    public String getMessage() {
        if (getContent().isPresent()) {
            return getContent().getText();
        } else {
            return null;
        }
    }

    public boolean isSubscription() {
        return getContent().isPresent() && Boolean.parseBoolean(getContent().getAttribute("data-subscription"));
    }

    protected void clickElementAndWaitForBanner(final PageElement element, final String traceKey) {
        final Tracer checkpoint = context.checkpoint();

        element.click();

        //Wait until the REST call and animation is done.
        Poller.waitUntilTrue(context.condition(checkpoint, traceKey));
        //Wait until the animation is done.
        Poller.waitUntilFalse(getContent().timed().isVisible());
    }

    protected static boolean isPresentAndVisible(final PageElement element) {
        return element.isPresent() && element.isVisible();
    }

    protected static boolean isPresent(final PageElement element) {
        return element.isPresent();
    }
}
