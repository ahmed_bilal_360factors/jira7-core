package com.atlassian.jira.pageobjects.project.versions;

import com.atlassian.jira.pageobjects.dialogs.JiraDialog;
import com.atlassian.jira.pageobjects.pages.admin.screen.AssociateIssueOperationToScreenForm;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.SelectElement;
import com.atlassian.pageobjects.elements.WebDriverSelectElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import org.openqa.selenium.By;

import java.util.List;
import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.by;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.is;

/**
 * Dialog for releasing a version
 *
 * @since v4.4
 */
public class ReleaseVersionDialog extends JiraDialog {
    private PageElement releaseDate;

    private PageElement submit;
    private PageElement cancel;

    private PageElement ignore;
    private PageElement ignoreLabel;

    private PageElement move;
    private PageElement moveLabel;

    private PageElement unresolvedMessage;
    private PageElement unresolvedIssuesLink;

    private PageElement releaseDateError;

    private SelectElement unfixedIssuesVersion;

    @Inject
    private Timeouts timeouts;
    @Inject
    private PageElementFinder elementFinder;

    public ReleaseVersionDialog() {
        super("version-release-dialog");
    }

    @Init
    public void initialize() {
        waitUntilTrue(isOpen());
        releaseDate = this.find(By.id("project-config-version-release-form-release-date-field"));
        submit = this.find(By.id("project-config-version-release-form-submit"));
        ignore = this.find(By.id("unresolved-ignore"));
        ignoreLabel = this.find(By.id("unresolved-ignore-label"));
        move = this.find(By.id("unresolved-move"));
        moveLabel = this.find(By.id("unresolved-move-label"));
        unresolvedMessage = this.find(By.id("unresolved-message"));
        unresolvedIssuesLink = this.find(By.id("unresolved-issues-link"));
        releaseDateError = this.find(By.id("project-config-versions-release-form-release-date-error"));
        unfixedIssuesVersion = binder.bind(WebDriverSelectElement.class, By.name("moveUnfixedIssuesTo"));
        cancel = this.find(By.className("cancel"));
    }

    public ReleaseVersionDialog setReleaseDate(final String date) {
        releaseDate.type(date);
        return this;
    }

    public boolean hasUnresolvedIssues() {
        return unresolvedIssuesLink.isPresent();
    }

    public String unresolvedIssueCountText() {
        return unresolvedIssuesLink.getText();
    }

    public String unresolvedIssueLinkUrl() {
        return unresolvedIssuesLink.getAttribute("href");
    }

    public ReleaseVersionDialog ignoreUnresolvedIssues() {
        ignore.select();
        return this;
    }

    public ReleaseVersionDialog moveUnresolvedIssues(final String version) {
        move.select();
        unfixedIssuesVersion.select(Options.text(version));
        return this;
    }

    public List<String> getVersionsAvailableForMove() {
        return AssociateIssueOperationToScreenForm.getOptionTexts(unfixedIssuesVersion);
    }

    public void submit() {
        submit.click();
        PageElement loading = this.find(By.className("loading"));
        Poller.waitUntil(loading.timed().isPresent(), is(false), by(8000));
    }

    public void cancel() {
        cancel.click();
        waitUntilTrue(isClosed());
    }

    public boolean hasMoveOption() {
        return move.isPresent();
    }

    public boolean hasIgnoreOption() {
        return ignore.isPresent();
    }

    public boolean hasUnresolvedMessage() {
        return unresolvedMessage.isPresent();
    }

    public String getUnresolvedMessage() {
        return unresolvedMessage.getText();
    }

    public String getIgnoreOptionLabelText() {
        return ignoreLabel.getText();
    }

    public boolean hasReleaseDateErrorMessage() {
        return releaseDateError.isPresent();
    }
}
