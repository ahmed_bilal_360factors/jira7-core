package com.atlassian.jira.webtest.webdriver.util;

import com.atlassian.jira.pageobjects.framework.util.TimedQueryFactory;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * The AUI Blanket component that greys out the screen (when a dialog is open, for example).
 */
public class AUIBlanket {

    @Inject
    TimedQueryFactory factory;
    @Inject
    protected PageElementFinder locator;

    /**
     * Wait until the AUI Blanket has completely faded out.
     *
     * This is useful for making sure no further page interaction takes place until the screen is clear. Otherwise,
     * click events can get lost in a fading blanket.
     *
     * @param timeoutMessage The message to log when the AUI Blanket doesn't disappear before the timeout (usually
     *                       because it never existed to begin with).
     */
    public void waitUntilClosed(String timeoutMessage) {
        waitUntilTrue(timeoutMessage, isBlanketNotExistingOrHidden());
    }

    /**
     * Same function as waitUntilClosed(String timeoutMessage), except with no custom timeout message.
     */
    public void waitUntilClosed() {
        waitUntilTrue(isBlanketNotExistingOrHidden());
    }

    private TimedCondition isBlanketNotExistingOrHidden() {
        final TimedQuery<Boolean> isBlanketNotPresentCondition = Conditions.not(getBlanket().timed().isPresent());
        final TimedQuery<Boolean> isBlanketHiddenCondition = Conditions.isEqual("hidden", factory.forSupplier(this::getComputedVisbility));
        return Conditions.or(isBlanketNotPresentCondition, isBlanketHiddenCondition);
    }

    private String getComputedVisbility() {
        return (String) getBlanket().javascript().execute("return window.getComputedStyle(arguments[0]).visibility");
    }

    private PageElement getBlanket() {
        return locator.find(By.className("aui-blanket"));
    }

}
