package com.atlassian.jira.pageobjects.pages.admin.workflow;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

/**
 * Admin page for editing a workflow transition.
 *
 * @since v7.2
 */
public final class EditWorkflowTransitionPage extends AbstractJiraPage {
    @ElementBy(id = "workflow-transition-edit-submit")
    private PageElement submitButton;

    private final String workflowMode;
    private final String workflowName;
    private final String workflowStep;
    private final String workflowTransition;

    public EditWorkflowTransitionPage(final String workflowMode, final String workflowName, final String workflowStep, final String workflowTransition) {
        this.workflowMode = workflowMode;
        this.workflowName = workflowName;
        this.workflowStep = workflowStep;
        this.workflowTransition = workflowTransition;
    }

    @Override
    public TimedCondition isAt() {
        return submitButton.timed().isPresent();
    }

    @Override
    public String getUrl() {
        return "/secure/admin/workflows/EditWorkflowTransition!default.jspa?workflowMode=" + workflowMode + "&workflowName=" + workflowName + "&workflowStep=" + workflowStep + "&workflowTransition=" + workflowTransition;
    }

    public void setField(String key, String value) {
        PageElement input = elementFinder.find(By.name(key));
        input.type(value);
    }

    public ViewWorkflowTransitionPage submit() {
        submitButton.click();
        return pageBinder.bind(ViewWorkflowTransitionPage.class, workflowMode, workflowName, workflowStep, workflowTransition);
    }
}
