package com.atlassian.jira.pageobjects.project.roles;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.jira.pageobjects.project.EditProjectLeadAndDefaultAssigneeDialog;
import com.atlassian.jira.pageobjects.project.roles.adduserdialog.AddUserAndGroupDialog;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementActions;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

public class RolesPage extends AbstractJiraPage {
    private static final String ROLE_RESULTS_RENDERED_TRACE_KEY = "roles.results.rendered";
    private static final String REMOVE_ACTOR_COMPLETED_TRACE_KEY = "roles.remove.actor.completed";
    private static final String GOTO_PAGE_COMPLETED = "roles.goto.page.completed";

    @Inject
    private PageBinder pageBinder;

    @Inject
    private PageElementActions actions;

    @Inject
    private TraceContext traceContext;

    @Inject
    private PageElementFinder elementFinder;

    @ElementBy(id = "roles-edit-defaults-button")
    private PageElement editDefaults;

    @ElementBy(id = "project-config-panel-roles-project-lead")
    private PageElement projectLead;

    @ElementBy(id = "project-config-panel-roles-project-lead-avatar")
    private PageElement projectLeadAvatar;

    @ElementBy(id = "project-config-panel-roles-default-assignee")
    private PageElement defaultAssignee;

    @ElementBy(id = "project-config-panel-roles-default-assignee-avatar")
    private PageElement defaultAssigneeAvatar;

    @ElementBy(id = "project-config-panel-role-results")
    private PageElement roleResults;

    private final String projectKey;

    private AddUserAndGroupDialog addUserAndGroupDialog;

    private RolesFilter rolesFilter;

    @Init
    public void init() {
        bindAddUserAndGroupDialog("roles-add-users-button");
        rolesFilter = pageBinder.bind(RolesFilter.class);
    }

    public RolesPage(String projectKey) {
        this.projectKey = projectKey;
    }

    @Override
    public String getUrl() {
        return "/plugins/servlet/project-config/" + projectKey + "/roles";
    }

    @Override
    public TimedCondition isAt() {
        return roleResults.timed().isPresent();
    }

    public String addActorsToRoleUsingEmptyRolesButton(long roleId, String... actors) {
        bindAddUserAndGroupDialog("roles-empty-roles-button");
        String successMessage = addActorsToRole(roleId, actors);

        bindAddUserAndGroupDialog("roles-add-users-button");

        return successMessage;
    }

    private void bindAddUserAndGroupDialog(String triggerId) {
        addUserAndGroupDialog = pageBinder.bind(AddUserAndGroupDialog.class, By.id(triggerId), "roles-add-users-dialog");
    }

    public String addActorsToRole(long roleId, String... actors) {
        final Tracer checkpoint = traceContext.checkpoint();
        String resultMessage = addActorsToRoleNoReRender(roleId, actors);
        traceContext.waitFor(checkpoint, ROLE_RESULTS_RENDERED_TRACE_KEY);

        return resultMessage;
    }

    public String addActorsToRoleNoReRender(long roleId, String... actors) {
        addUserAndGroupDialog.open();
        addUserAndGroupDialog.addActorsToRole(roleId, actors);

        return addUserAndGroupDialog.save();
    }

    public void removeAllFromRole(long roleId) {
        PageElement roleResultTable = getRoleResultTable(roleId);

        while (roleResultTable.find(By.cssSelector(".js-roles-remove")).isPresent()) {
            removeFromRole(roleId, "");
        }
    }

    public void removeGroupFromRole(long roleId, String groupName) {
        removeFromRole(roleId, "[data-group-name='" + groupName + "']");
    }

    public void removeUserFromRole(long roleId, String userName) {
        removeFromRole(roleId, "[data-user-name='" + userName + "']");
    }

    public String removeGroupFromRoleWithError(long roleId, String groupName) {
        return removeFromRoleWithError(roleId, "[data-group-name='" + groupName + "']");
    }

    public String removeUserFromRoleWithError(long roleId, String userName) {
        return removeFromRoleWithError(roleId, "[data-user-name='" + userName + "']");
    }

    public boolean setProjectLead(String projectLead) {
        return openEditDefaultsDialog().setProjectLead(projectLead).submitUpdate();
    }

    public boolean setDefaultAssignee(String defaultAssignee) {
        return openEditDefaultsDialog().setDefaultAssignee(defaultAssignee).submitUpdate();
    }

    public EditProjectLeadAndDefaultAssigneeDialog openEditDefaultsDialog() {
        editDefaults.click();
        return pageBinder.bind(EditProjectLeadAndDefaultAssigneeDialog.class);
    }

    public void filterByRole(String roleName) {
        final Tracer checkpoint = traceContext.checkpoint();
        rolesFilter.selectRole(roleName);
        traceContext.waitFor(checkpoint, ROLE_RESULTS_RENDERED_TRACE_KEY);
    }

    public void clearFilterByRole() {
        final Tracer checkpoint = traceContext.checkpoint();
        rolesFilter.selectAllRole();
        traceContext.waitFor(checkpoint, ROLE_RESULTS_RENDERED_TRACE_KEY);
    }

    public void filterByQuery(String query) {
        final Tracer checkpoint = traceContext.checkpoint();
        rolesFilter.typeQuery(query);
        traceContext.waitFor(checkpoint, ROLE_RESULTS_RENDERED_TRACE_KEY);
    }

    public void clearFilterByQuery() {
        final Tracer checkpoint = traceContext.checkpoint();
        rolesFilter.clearQuery();
        traceContext.waitFor(checkpoint, ROLE_RESULTS_RENDERED_TRACE_KEY);
    }

    public void showAllActors(long roleId) {
        final Tracer checkpoint = traceContext.checkpoint();
        roleResults.find(By.cssSelector(".js-roles-show-all[data-role-id='" + roleId + "']")).click();
        traceContext.waitFor(checkpoint, ROLE_RESULTS_RENDERED_TRACE_KEY);
    }

    public void showLess() {
        final Tracer checkpoint = traceContext.checkpoint();
        roleResults.find(By.cssSelector(".js-roles-show-less")).click();
        traceContext.waitFor(checkpoint, ROLE_RESULTS_RENDERED_TRACE_KEY);
    }

    public String getProjectLead() {
        return projectLead.getText();
    }

    public Boolean isInvalidProjectLeadWarningDialogVisible() {
        return elementFinder.find(By.cssSelector("#project-config-panel-roles > .aui-message-warning.invalid-project-lead-warning")).timed().isVisible().now();
    }

    public boolean isProjectLeadAvatarPresent() {
        return projectLeadAvatar.isPresent();
    }

    public String getProjectLeadAvatarSrc() {
        return projectLeadAvatar.find(By.tagName("img")).getAttribute("src");

    }

    public boolean isProjectLeadNonExistentIndicated() {
        return projectLead.find(By.className("errLabel")).isPresent();
    }

    public boolean isProjectLeadUserHoverEnabled() {
        final PageElement a = projectLead.find(By.tagName("a"));
        return a.isPresent() && a.hasClass("user-hover");
    }

    public String getDefaultAssignee() {
        return defaultAssignee.getText();
    }

    public boolean isDefaultAssigneeAvatarPresent() {
        return defaultAssigneeAvatar.isPresent();
    }

    public String getDefaultAssigneeAvatarSrc() {
        return defaultAssigneeAvatar.find(By.tagName("img")).getAttribute("src");
    }

    public boolean isDefaultAssigneeNotAssignableIndicated() {
        return defaultAssignee.find(By.className("project-config-invalid")).isPresent();
    }

    public boolean isRoleTablePresent(long roleId) {
        return getRoleResultTable(roleId).isPresent();
    }

    public List<String> getUserNamesForRole(long roleId) {
        return getActorNames(roleId, "js-role-user-name");
    }

    public List<String> getGroupNamesForRole(long roleId) {
        return getActorNames(roleId, "js-role-group-name");
    }

    public boolean isEmailAddressColumnPresent() {
        return roleResults.findAll(By.cssSelector(".js-role-results .roles-user-email")).size() > 0;
    }

    public boolean isAddUsersToRoleDialogVisible() {
        return elementFinder.find(By.cssSelector(".js-roles-add-user-dialog-content")).timed().isVisible().now();
    }

    public List<String> getVisibleRoleNames() {
        return roleResults.findAll(By.className("roles-result-header-role-name"))
                .stream()
                .map(element -> element.getText().trim())
                .collect(Collectors.toList());
    }

    public List<String> getActorNames(long roleId, String actorClass) {
        PageElement roleResultTable = getRoleResultTable(roleId);
        return roleResultTable.findAll(By.className(actorClass))
                .stream()
                .map(element -> element.getText().trim())
                .collect(Collectors.<String>toList());
    }

    public PageElement getPagingNavigator() {
        return roleResults.find(By.className("js-roles-pagination-navigator"));
    }

    public void goToPage(String page) {
        final Tracer checkpoint = traceContext.checkpoint();
        roleResults.findAll(By.cssSelector(".js-roles-pagination-navigator a"))
                .stream()
                .filter(pageAnchor -> page.equals(pageAnchor.getText().trim()))
                .findFirst()
                .get().click();
        traceContext.waitFor(checkpoint, GOTO_PAGE_COMPLETED);
    }

    private PageElement getRoleResultTable(long roleId) {
        return roleResults.find(By.id("role-result-table-" + roleId));
    }

    private void removeFromRole(long roleId, String additionalCssSelector) {
        PageElement roleResultTable = getRoleResultTable(roleId);

        final Tracer checkpoint = traceContext.checkpoint();
        removeFromRoleNoReRender(roleResultTable, additionalCssSelector);
        traceContext.waitFor(checkpoint, ROLE_RESULTS_RENDERED_TRACE_KEY);
    }

    private String removeFromRoleWithError(long roleId, String additionalCssSelector) {
        PageElement roleResultTable = getRoleResultTable(roleId);

        removeFromRoleNoReRender(roleResultTable, additionalCssSelector);

        PageElement removeButton = roleResultTable.find(By.cssSelector(".js-roles-remove" + additionalCssSelector));
        PageElement errorElement = removeButton.find(By.xpath("..")).find(By.className("js-roles-remove-error"));

        return errorElement.getAttribute("data-error-message");
    }

    private void removeFromRoleNoReRender(PageElement roleResultTable, String additionalCssSelector) {
        PageElement removeButton = roleResultTable.find(By.cssSelector(".js-roles-remove" + additionalCssSelector));

        final Tracer checkpoint = traceContext.checkpoint();
        actions.moveToElement(removeButton).click().perform();
        traceContext.waitFor(checkpoint, REMOVE_ACTOR_COMPLETED_TRACE_KEY);
    }
}
