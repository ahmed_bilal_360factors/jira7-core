package com.atlassian.jira.pageobjects.pages.admin.application;

import com.atlassian.jira.pageobjects.components.UserApplicationAccess;
import com.atlassian.jira.webtest.webdriver.util.AUIBlanket;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import javax.inject.Inject;

/**
 * Available via Set defaults button in Application access screen
 */
public class ApplicationAccessDefaultsDialog {
    public enum WarningType {
        NO_DEFAULT_GROUP("app-role-defaults-warning-no-default-group"),
        NO_SEATS("app-role-defaults-warning-no-seats");

        private final String className;

        WarningType(final String className) {
            this.className = className;
        }

        public String getClassName() {
            return className;
        }
    }

    @ElementBy(cssSelector = "#app-role-defaults-dialog")
    @SuppressWarnings("neverassigned")
    private PageElement dialogElement;

    @Inject
    private PageBinder pageBinder;

    public UserApplicationAccess applicationAccess() {
        return pageBinder.bind(UserApplicationAccess.class, By.id("app-role-defaults-dialog"));
    }

    public AUIBlanket auiBlanket() {
        return pageBinder.bind(AUIBlanket.class);
    }

    public ApplicationAccessDefaultsDialog toggleRole(final String roleKey, final boolean checked) {
        applicationAccess().toggleApplication(roleKey, checked);
        return this;
    }

    public void submit() {
        dialogElement.find(By.className("app-role-defaults-dialog-submit-button")).click();
        waitUntilClosed();
    }

    public boolean isRoleSelected(final String roleKey) {
        return applicationAccess().isApplicationSelected(roleKey).byDefaultTimeout();
    }

    public TimedCondition isPresent() {
        return dialogElement.timed().isPresent();
    }

    public TimedCondition isWarningVisible(final WarningType warningType) {
        return dialogElement.find(By.className("app-role-defaults-warnings")).find(By.className(warningType.getClassName())).timed().isVisible();
    }

    protected void waitUntilClosed() {
        Poller.waitUntilFalse(dialogElement.timed().isVisible());
        Poller.waitUntilFalse(dialogElement.timed().isPresent());
        auiBlanket().waitUntilClosed();
    }
}
