package com.atlassian.jira.pageobjects.components.menu;

import org.openqa.selenium.By;

/**
 * A page object representing the "Help" menu that exists in JIRA's application header.
 *
 * @since v7.2.0
 */
public class HelpMenu extends JiraAuiDropdownMenu {
    public HelpMenu() {
        super(By.id("help_menu"), By.id("system-help-menu-content"));
    }
}
