package com.atlassian.jira.pageobjects.pages.admin;

import com.atlassian.jira.pageobjects.components.fields.SingleSelect;
import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.google.common.collect.Iterables;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import javax.inject.Inject;
import java.util.List;

import static com.google.common.collect.ImmutableList.copyOf;
import static org.junit.Assert.assertTrue;

public class GlobalPermissionsPage extends AbstractJiraPage {
    public static String ADMINISTRATORS_PERM = "JIRA Administrators";
    public static String SYS_ADMINISTRATORS_PERM = "JIRA System Administrators";
    public static String BROWSE_USERS_PERM = "Browse Users";
    public static String ADMINISTRATORS_GROUP = "jira-administrators";

    @ElementBy(id = "global_perms")
    private PageElement mainPermissionTable;

    @ElementBy(cssSelector = "#group-single-selector-parent")
    private PageElement selectPermissionParent;

    private List<GlobalPermissionRow> globalPermissions;
    private SingleSelect groupSelector;
    private Select permissionSelector;

    @Inject
    PageBinder binder;


    @Override
    public TimedCondition isAt() {
        return mainPermissionTable.timed().isPresent();
    }

    @Init
    private void loadGlobalPermissions() {
        List<WebElement> rows = driver.findElements(By.cssSelector("table#global_perms > tbody > tr"));
        this.globalPermissions = copyOf(Iterables.transform(rows,
                row -> pageBinder.bind(GlobalPermissionRow.class, row)));
        permissionSelector = new Select(driver.findElement(By.id("globalPermType_select")));
    }

    @Override
    public String getUrl() {
        return "/secure/admin/jira/GlobalPermissions!default.jspa";
    }

    public List<GlobalPermissionRow> getGlobalPermissions() {
        return globalPermissions;
    }

    public GlobalPermissionRow getGlobalPermission(String permission) {
        for (GlobalPermissionRow row : getGlobalPermissions()) {
            if (permission.equals(row.getPermissionName())) {
                return row;
            }
        }
        throw new IllegalArgumentException("Global permission: '" + permission + "' does not exist.");
    }

    public static final class GlobalPermissionRow {
        private final String permissionName;
        private final String secondaryText;
        private final Iterable<String> groupsAndUsers;

        public GlobalPermissionRow(final WebElement webElement) {
            List<WebElement> cols = webElement.findElements(By.tagName("td"));

            WebElement name = cols.get(0);
            WebElement groups = cols.get(1);
            this.permissionName = name.findElement(By.tagName("strong")).getText();
            this.secondaryText = name.findElement(By.className("secondary-text")).getText();
            this.groupsAndUsers = Iterables.transform(groups.findElements(By.cssSelector("td > ul > li > span")), WebElement::getText);
        }

        public String getPermissionName() {
            return permissionName;
        }

        @SuppressWarnings("unused") // this is asserted via hamcrest's hasProperty
        public String getSecondaryText() {
            return secondaryText;
        }

        public Iterable<String> getGroupsAndUsers() {
            return groupsAndUsers;
        }
    }

    public void selectGroup(String group) {
        if (groupSelector == null) {
            groupSelector = binder.bind(SingleSelect.class, selectPermissionParent);
        }
        groupSelector.select(group);
    }

    public void selectPermission(String permission) {
        if (permissionSelector == null) {
            permissionSelector = new Select(driver.findElement(By.className("fieldValueArea")));
        }
        permissionSelector.selectByVisibleText(permission);
    }

    public TimedCondition isAddGlobalPermissionWarningPresent() {
        return elementFinder.find(By.id("default-groups-warning")).withTimeout(TimeoutType.AJAX_ACTION).timed().isVisible();
    }

    public String getWarningMessage() {
        PageElement warning = elementFinder.find(By.id("default-group-warning-message"));
        assertTrue("Warning is not displayed", warning.timed().isVisible().byDefaultTimeout());
        return warning.getText();
    }
}
