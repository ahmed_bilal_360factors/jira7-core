package com.atlassian.jira.pageobjects.pages;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

public class AddPermissionsSchemePage extends AbstractJiraPage {

    @ElementBy(id = "add-permissions-scheme")
    private PageElement addPermissionForm;

    @Override
    public TimedCondition isAt() {
        return addPermissionForm.timed().isPresent();
    }

    @Override
    public String getUrl() {
        return "/secure/admin/AddPermissionScheme!default.jspa";
    }
}
