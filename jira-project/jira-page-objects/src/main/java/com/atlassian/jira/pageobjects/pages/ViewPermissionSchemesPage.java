package com.atlassian.jira.pageobjects.pages;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.WebDriverElement;
import com.atlassian.pageobjects.elements.WebDriverLocatable;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

public class ViewPermissionSchemesPage extends AbstractJiraPage {

    @ElementBy(id = "permission_schemes_table")
    private PageElement permissionSchemesTable;

    @ElementBy(id = "add_permissions_scheme")
    private PageElement addPermissionSchemeButton;

    @Override
    public TimedCondition isAt() {
        return permissionSchemesTable.timed().isPresent();
    }

    @Override
    public String getUrl() {
        return "/secure/admin/ViewPermissionSchemes.jspa";
    }

    public void editPermissionSchemePermissions(final String permissionSchemeName) {
        permissionSchemesTable.find(
                By.xpath("//text()[contains(.,'" + permissionSchemeName + "')]/ancestor::*[self::tr][1]"),
                PermissionSchemeRow.class).clickEditPermissions();
    }

    public AddPermissionsSchemePage clickAddPermissionSchemeButton() {
        addPermissionSchemeButton.click();
        return pageBinder.bind(AddPermissionsSchemePage.class);
    }

    public boolean hasAddedPermissionSchemeButton() {
        return addPermissionSchemeButton.timed().isPresent().now();
    }

    public static final class PermissionSchemeRow extends WebDriverElement {

        public PermissionSchemeRow(By locator) {
            super(locator);
        }

        public PermissionSchemeRow(final By locator, final WebDriverLocatable parent) {
            super(locator, parent);
        }

        public void clickEditPermissions() {
            find(By.partialLinkText("Permissions")).click();
        }
    }
}
