package com.atlassian.jira.pageobjects.project;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

/**
 * Page object used for testing the Project Settings create in the reference-plugin.
 * @see {com.atlassian.jira.dev.reference.plugin.servlet.ReferenceProjectSettingsServlet}
 */
public class ReferenceProjectSettingsPage extends AbstractJiraPage {

    private final String projectKey;
    private final Long projectId;

    @ElementBy(id = "path")
    private PageElement path;

    @ElementBy(id = "query")
    private PageElement query;

    public ReferenceProjectSettingsPage(final String projectKey, final Long projectId) {
        this.projectKey = projectKey;
        this.projectId = projectId;
    }

    @Override
    public TimedCondition isAt() {
        return path.timed().isPresent();
    }

    @Override
    public String getUrl() {
        return String.format("/plugins/servlet/reference/project-settings/%s?projectId=%d", projectKey, projectId);
    }

    public String getPath() {
        return path.getText();
    }

    public String getQuery() {
        return query.getText();
    }
}
