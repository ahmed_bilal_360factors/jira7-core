package com.atlassian.jira.pageobjects.project.versions;

import com.atlassian.jira.pageobjects.components.fields.MultiSelect;
import com.atlassian.jira.pageobjects.dialogs.JiraDialog;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.SelectElement;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;

import java.util.List;
import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Dialog for merging versions.
 *
 * @since v4.4
 */
public class MergeDialog extends JiraDialog {
    @ElementBy(id = "project-config-versions-merge-form-no-versions")
    private PageElement noVersions;

    @ElementBy(id = "project-config-version-merge-form-submit")
    private PageElement submit;

    private MultiSelect idsToMerge;

    @ElementBy(name = "idMergeTo")
    private SelectElement idMergeTo;

    @Inject
    private PageElementFinder elementFinder;

    @Inject
    private PageBinder pageBinder;

    public MergeDialog() {
        super("versionsMergeDialog");
    }

    @Init
    public void initialize() {
        idsToMerge = elementFinder.find(By.id("idsToMerge-multi-select")).isPresent() ?
                pageBinder.bind(MultiSelect.class, "idsToMerge") : null;
    }

    @WaitUntil
    public void ready() {
        waitUntilTrue(isOpen());
    }

    public boolean hasWarning() {
        return this.find(getWarningSelector()).isPresent();
    }

    public String getWarningText() {
        return this.find(getWarningSelector()).getText();
    }

    public boolean hasNoVersions() {
        return noVersions.isPresent();
    }

    public String getNoVersionsText() {
        return noVersions.getText();
    }

    public MergeDialog merge(final String targetVersion, final String... sourceVersions) {
        idMergeTo.select(Options.text(targetVersion));

        for (final String sourceVersion : sourceVersions) {
            idsToMerge.add(sourceVersion);
        }

        return this;
    }

    public void submit() {
        submit.click();
    }

    public List<String> getErrorMessages() {
        final List<String> errorMessages = Lists.newArrayList();
        final List<PageElement> errors = this.findAll(By.className("error-list-item"));
        for (final PageElement error : errors) {
            errorMessages.add(error.getText());
        }
        return errorMessages;
    }

    public boolean hasErrorMessages() {
        return this.find(getErrorSelector()).isPresent();
    }

    private static By getWarningSelector() {
        return By.cssSelector(".aui-message.warning");
    }

    private static By getErrorSelector() {
        return By.cssSelector(".aui-message.error");
    }
}
