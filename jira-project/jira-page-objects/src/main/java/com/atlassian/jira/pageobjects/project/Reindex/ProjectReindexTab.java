package com.atlassian.jira.pageobjects.project.Reindex;

import com.atlassian.jira.pageobjects.project.AbstractProjectConfigPageTab;
import com.atlassian.jira.pageobjects.project.ProjectConfigPageTab;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

/**
 * Re-index project page accessed in the project administration sidebar
 *
 * @since v7.2
 */
public class ProjectReindexTab extends AbstractProjectConfigPageTab implements ProjectConfigPageTab {

    private static final String URI_TEMPLATE = "/secure/admin/IndexProject.jspa?key=%s";

    private final String uri;

    @ElementBy(id = "reindex-confirmation-page")
    private PageElement reindexPageDiv;

    @ElementBy(id = "confirm-project-reindex-button")
    private PageElement confirmButton;

    @ElementBy(id = "view_reindex_project")
    private PageElement reindexWebitem;

    public ProjectReindexTab(final String projectKey) {
        this.uri = getUrlForProject(projectKey);
    }

    public ProjectReindexTab() {
        this.uri = null;
    }

    @Override
    public TimedCondition isAt() {
        return reindexPageDiv.timed().isPresent();
    }

    @Override
    public String getUrl() {
        if (uri == null) {
            throw new IllegalStateException("Use the constructor with the project key argument.");
        }
        return uri;
    }

    public static String getUrlForProject(String projectKey) {
        return String.format(URI_TEMPLATE, projectKey);
    }

    public boolean isWebitemPresent() {
        return reindexWebitem.timed().isPresent().now();
    }

    public void confirmReindex() {
        confirmButton.click();
    }
}
