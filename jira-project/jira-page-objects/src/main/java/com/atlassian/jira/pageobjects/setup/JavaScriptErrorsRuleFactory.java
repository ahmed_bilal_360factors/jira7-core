package com.atlassian.jira.pageobjects.setup;

import com.atlassian.webdriver.testing.rule.JavaScriptErrorsRule;
import com.google.common.base.Supplier;
import com.google.common.collect.Sets;
import org.openqa.selenium.WebDriver;

import java.util.HashSet;
import java.util.function.Function;

/**
 * Configures and creates an instance of the {@link JavaScriptErrorsRule} test rule.
 *
 * @since v7.2
 */
public class JavaScriptErrorsRuleFactory {
    private static final HashSet<String> errorsToIgnore = Sets.newHashSet(
            /** a d3.js problem. See https://github.com/mbostock/d3/issues/1805 */
            "mutating the [[Prototype]] of an object will cause your code to run very slowly; instead create the object with the correct initial [[Prototype]] value using Object.create"
    );

    private static Function<JavaScriptErrorsRule, JavaScriptErrorsRule> builder = javaScriptErrorsRule -> javaScriptErrorsRule
            // by setting this to 'false', any javascript errors found
            // in the browser's console will NOT cause a webdriver test
            // to fail.
            // setting it to 'true' would mean the webdriver test would
            // fail even if all java-based assertions passed.
            .failOnJavaScriptErrors(false)
            // if errors are found in the browser's console that
            // exactly match one of these strings, they will not be
            // reported; they will be silently dropped.
            .errorsToIgnore(errorsToIgnore);

    public static JavaScriptErrorsRule create() {
        return builder.apply(new JavaScriptErrorsRule());
    }

    public static JavaScriptErrorsRule create(Supplier<? extends WebDriver> webDriverSupplier) {
        return builder.apply(new JavaScriptErrorsRule(webDriverSupplier));
    }
}
