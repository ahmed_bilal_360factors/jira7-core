package com.atlassian.jira.pageobjects.pages.webhooks;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;

public class ViewPostFunctionPage {
    @ElementBy(id = "view_post_functions")
    private PageElement viewPostFunctionsLink;

    @Inject
    private PageElementFinder finder;

    public ViewPostFunctionPage open() {
        viewPostFunctionsLink.click();
        PageElement postFunctionTable = finder.find(By.id("workflow-post-functions"));
        Poller.waitUntilTrue(postFunctionTable.timed().isVisible());
        return this;
    }

    public List<String> getPostFunctions() {
        final List<PageElement> postFunctions = finder.findAll(By.cssSelector(".criteria-item"));
        return Lists.transform(postFunctions, new Function<PageElement, String>() {
            @Override
            public String apply(final PageElement element) {
                return element.getText();
            }
        });
    }

    public void publishDraft() {
        finder.find(By.id("publish_draft_workflow")).click();
        Poller.waitUntilTrue(finder.find(By.id("publish-workflow-submit")).timed().isVisible());
        finder.find(By.id("publish-workflow-true")).select();
        finder.find(By.id("publish-workflow-submit")).click();
    }
}
