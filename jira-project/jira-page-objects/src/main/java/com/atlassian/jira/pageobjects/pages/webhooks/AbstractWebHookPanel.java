package com.atlassian.jira.pageobjects.pages.webhooks;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;

public abstract class AbstractWebHookPanel {
    private final Predicate<PageElement> isVisible = PageElement::isVisible;

    @ElementBy(id = "webhook-name")
    protected PageElement webHookName;

    @ElementBy(id = "webhook-url")
    protected PageElement webHookUrl;

    @ElementBy(id = "webhook-description")
    protected PageElement webHookDescription;

    @ElementBy(id = "webhook-exclude-details")
    protected PageElement excludeDetails;

    @ElementBy(id = "webhook-jql")
    protected PageElement jql;

    @Inject
    protected PageElementFinder finder;

    @Inject
    protected PageBinder binder;

    public String getMessage() {
        final PageElement pageElement = finder.find(By.id("webhook-global-message"));
        return pageElement.getText();
    }

    private String getWebHookName() {
        return webHookName.getValue();
    }

    private String getWebHookUrl() {
        return webHookUrl.getValue();
    }

    private String getWebHookDescription() {
        return webHookDescription.getValue();
    }

    private boolean isExcludeDetails() {
        return excludeDetails.isSelected();
    }

    private String getIssueSectionFilter() {
        return jql.getValue();
    }

    public Set<String> getEvents() {
        final List<PageElement> events = finder.findAll(By.cssSelector("[data-event-type]"));
        return ImmutableSet.copyOf(transform(filter(events, isVisible), pageElement -> pageElement.getAttribute("data-event-type")));
    }

    public DisplayedWebHookListener getDisplayedListener() {
        return DisplayedWebHookListener.builder()
                .name(getWebHookName())
                .description(getWebHookDescription())
                .url(getWebHookUrl())
                .excludeBody(isExcludeDetails())
                .events(ImmutableSet.copyOf(getEvents()))
                .issueSectionFilter(getIssueSectionFilter())
                .build();
    }

    @Override
    public String toString() {
        return getDisplayedListener().toString();
    }

    public List<String> getAvailableVariables() {
        final List<PageElement> variables = finder.findAll(By.cssSelector(".variable"));
        return Lists.newArrayList(transform(variables, input -> input.getText().replaceAll("\\$\\{([^\\}]+)\\}", "$1")));
    }
}
