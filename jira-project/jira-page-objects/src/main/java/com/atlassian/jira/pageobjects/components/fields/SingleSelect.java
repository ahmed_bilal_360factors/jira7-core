package com.atlassian.jira.pageobjects.components.fields;

import com.atlassian.fugue.Option;
import com.atlassian.jira.pageobjects.framework.elements.ExtendedElementFinder;
import com.atlassian.jira.pageobjects.framework.elements.PageElements;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Queries;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.DefaultTimeouts;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.atlassian.pageobjects.elements.query.Conditions.and;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertTrue;

/**
 * @since v4.4
 */
public class SingleSelect {
    private static final String SELECT_ALL_CHORD = Keys.chord(osModifier(), "a");

    @Inject
    private ExtendedElementFinder extendedElementFinder;
    @Inject
    private PageElementFinder elementFinder;
    @Inject
    private Timeouts timeouts;
    @Inject
    private TraceContext traceContext;
    @Inject
    private WebDriver webDriver;

    private final PageElement parent;
    private final PageElement container;
    private final PageElement field;
    private final PageElement dropMenu;

    // Don't call constructor, use PageBinder.bind(SingleSelect.class, parent) instead
    public SingleSelect(PageElement parent) {
        this.parent = parent;
        this.container = parent.find(By.className("aui-ss"), TimeoutType.AJAX_ACTION);
        this.field = parent.find(By.tagName("input"));
        this.dropMenu = parent.find(By.className("drop-menu"));
    }

    public String getId() {
        return container.getAttribute("id").replace("-single-select", "");
    }

    /**
     * This will click the 'dropdown' icon in the picker, opening or closing the suggestions depending on the current
     * state.
     *
     * @return this single select instance
     */
    public SingleSelect triggerSuggestions() {
        PageElements.scrollIntoView(this.dropMenu); //click does not work correctly if we cannot see the select
        this.dropMenu.click();
        return this;
    }

    /**
     * @return list of suggestions at any given moment
     * @deprecated use {@link #getSuggestionsTimed()} as this method is prone to return wrong results due to race
     * conditions. Using {@link #getSuggestionsTimed()} will enforce clients to execute timed assertions and improve
     * reliability of the tests
     */
    @Deprecated
    public List<String> getSuggestions() {
        final PageElement layer = getActiveLayer();
        final List<PageElement> suggestionsEls = layer.findAll(By.cssSelector("a.aui-list-item-link"));
        final List<String> suggestions = new ArrayList<>(suggestionsEls.size());
        for (PageElement suggestionsEl : suggestionsEls) {
            suggestions.add(suggestionsEl.getAttribute("title"));
        }
        return suggestions;
    }

    public TimedQuery<Iterable<String>> getSuggestionsTimed() {
        return Queries.forSupplier(timeouts, extendedElementFinder.within(getActiveLayer())
                .newQuery(By.className("aui-list-item-link"))
                .transform(PageElements.TEXT)
                .supplier());

    }

    private PageElement getActiveLayer() {
        // TODO typical fragile implementation - retrieving whatever active layer is on the page and hoping its the right one. booo :P
        return elementFinder.find(By.cssSelector(".ajs-layer.active"));
    }

    public TimedCondition isSuggestionsOpen() {
        return Conditions.and(getActiveLayer().timed().isPresent(), Conditions.forSupplier(timeouts,
                () -> getActiveLayer().find(By.id(getId() + "-suggestions")).isPresent()));
    }

    public SingleSelect select(String value) {
        assertTrue("Unable to find container of the Single-Select", field.isPresent());
        final Option<Tracer> tracer = container.hasClass("ajax-ss") ? Option.option(traceContext.checkpoint()) : Option.none();
        if (isAutocompleteDisabled()) {
            type(value);
        } else {
            if (StringUtils.isEmpty(value)) {
                clear();
            } else {
                type(value);
                waitUntilTrue("Expected query " + value, hasQuery(value));

                PageElement field = this.field;
                // Ensure the parent has an active descendant, which is the id of the selected item in the dropdown
                waitUntilTrue("Has active descendant", new AbstractTimedCondition(DefaultTimeouts.DEFAULT, DefaultTimeouts.DEFAULT_INTERVAL) {
                    @Override
                    protected Boolean currentValue() {
                        return null != field.getAttribute("aria-activedescendant");
                    }
                });

                final Actions actions = new Actions(webDriver);
                actions.sendKeys(Keys.ENTER);
                actions.perform();
            }
        }
        if (tracer.isDefined()) {
            //ensure we wait until the ajax request is finished. Otherwise this can cause
            //problems where overly eager clients abort ajax requests (resulting in an alert) when
            //navigating away from the page. (see https://jdog.jira-dev.com/browse/JDEV-30702)
            traceContext.waitFor(tracer.get(), "jira.suggestionhandler.done");
        }

        return this;
    }

    private TimedCondition hasQuery(String query) {
        return and(container.timed().isPresent(), container.timed().hasAttribute("data-query", query));
    }

    public String getValue() {
        return field.getValue();
    }

    public TimedQuery<String> getTimedValue() {
        return field.timed().getValue();
    }

    public boolean isAutocompleteDisabled() {
        return field.hasClass("aui-ss-disabled");
    }

    public boolean isDisabled() {
        return !field.isEnabled();
    }

    public SingleSelect clear() {
        // Since clear() doesn't dispatch events, we need to send an "input" event
        // to notify the SingleSelect that the input value has changed. We can do this
        // by typing "x" then backspace.
        field.clear().type("x\u0008");
        return this;
    }

    public Iterable<String> getAvailableSuggestions() {
        this.dropMenu.javascript().execute("arguments[0].scrollIntoView()"); //click does not work correctly if we cannot see the select

        // It would be nice if we could clear() then triggerSuggestions() but when we do that JIRA's select-default
        // behaviour is invoked, so we need to be a bit tricky about it and pretend we're a real human user.
        this.field.click();
        final Actions actions = new Actions(webDriver);
        actions.sendKeys(SELECT_ALL_CHORD);
        actions.sendKeys(Keys.DELETE);
        actions.sendKeys(Keys.DOWN);
        actions.perform();

        TimedQuery<Iterable<String>> timedQuery = this.getSuggestionsTimed();
        return timedQuery.by(30, TimeUnit.SECONDS);
    }

    /**
     * Type into this single select without any additional validation
     *
     * @param text text to type
     * @return this single select instance
     */
    public SingleSelect type(CharSequence text) {
        clear();
        field.type(text);
        return this;
    }

    public String getError() {
        final PageElement error = parent.find(By.className("error"));

        if (error.isPresent()) {
            return error.getText();
        } else {
            return null;
        }
    }

    /**
     * @return an appropriate osModifier for use in the browser to invoke browser commands such as "select all" -
     * typically command on mac and control oon all other browsers
     */
    private static Keys osModifier() {
        // Assume we're running on the same OS as the client. Maybe this will change in future. We can solve that then.
        final String operatingSystem = System.getProperty("os.name").toLowerCase();
        // Apple has to be special and use command instead of control, otherwise we wouldn't need this method.

        return operatingSystem.contains("mac") ? Keys.COMMAND : Keys.CONTROL;
    }
}
