package com.atlassian.jira.pageobjects.config.junit4.rule;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;
import javax.inject.Inject;

/**
 * A rule to clear some flags to help make webdriver tests more predictable on variable screen sizes by un-obscuring
 * the elements on the page which are often hidden by flags.
 *
 * @since v7.2
 */
public class ClearFlagsRule extends TestWatcher {

    @Inject
    private JiraTestedProduct jira;

    @Override
    protected void starting(@Nonnull final Description description) {
        jira.backdoor().flags().clearFlags();
    }
}
