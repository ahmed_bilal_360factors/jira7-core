package com.atlassian.jira.pageobjects.pages.webhooks;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.google.common.collect.Iterables;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

public class AddWebHookPostFunctionPage {
    @ElementBy(id = "webhookId")
    private PageElement webHookSelect;

    @ElementBy(id = "add_submit")
    private PageElement submitButton;

    public void selectWebHookByName(final String name) {
        Iterables.find(getOptions(), element -> element.getText().equals(name)).select();
    }

    public List<String> getAllWebHooks() {
        final List<PageElement> options = getOptions();
        return options.stream().map(PageElement::getText).collect(Collectors.toList());
    }

    private List<PageElement> getOptions() {
        return webHookSelect.findAll(By.tagName("option"));
    }

    public void submit() {
        submitButton.click();
    }
}
