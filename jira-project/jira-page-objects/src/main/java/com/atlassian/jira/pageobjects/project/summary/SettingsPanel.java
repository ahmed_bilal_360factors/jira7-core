package com.atlassian.jira.pageobjects.project.summary;

import com.atlassian.jira.pageobjects.pages.admin.SelectCvsModules;
import com.atlassian.jira.pageobjects.pages.admin.UalConfigurePage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.google.inject.Inject;
import org.openqa.selenium.By;

import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Page object for the Settings Panel.
 *
 * @since v4.4
 */
public class SettingsPanel extends AbstractSummaryPanel {
    @Inject
    private PageBinder binder;

    @ElementBy(id = "project-config-webpanel-summary-settings")
    private PageElement settingsElement;

    @ElementBy(id = "configure_ual")
    private PageElement ualElement;

    public boolean hasUalLink() {
        return ualElement.isPresent();
    }

    public UalConfigurePage gotoUalConfigure() {
        assertTrue("UAL link is not present.", ualElement.isPresent());
        ualElement.click();
        return binder.bind(UalConfigurePage.class, getProjectKey());
    }

    public List<PageElement> getPluginElements() {
        return settingsElement.findAll(By.cssSelector(".project-config-operation-link"));
    }
}
