package com.atlassian.jira.pageobjects.components.menu;

import org.openqa.selenium.By;

/**
 * A page object representing the "Admin" menu that exists in JIRA's application header.
 *
 * @since v7.2.0
 */
public class AdminMenu extends JiraAuiDropdownMenu {
    public AdminMenu() {
        super(By.id("admin_menu"), By.id("system-admin-menu-content"));
    }
}
