package com.atlassian.jira.pageobjects.config.junit4.rule;

import com.atlassian.jira.functest.framework.util.junit.AnnotatedDescription;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.EnableAnalytics;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;
import javax.inject.Inject;

public class EnableAnalyticsRule extends TestWatcher {

    @Inject
    private JiraTestedProduct jira;

    private boolean shouldDisableAfter = false;

    @Override
    protected void starting(@Nonnull final Description description) {
        final AnnotatedDescription annotatedDescription = new AnnotatedDescription(description);

        if (!annotatedDescription.isMethodAnnotated(EnableAnalytics.class)) {
            return;
        }

        shouldDisableAfter = !jira.backdoor().analyticsEventsControl().isEnabled();
        jira.backdoor().analyticsEventsControl().enable().clear();
    }

    @Override
    protected void finished(final Description description) {
        if (shouldDisableAfter) {
            jira.backdoor().analyticsEventsControl().disable();
        }
    }

}
