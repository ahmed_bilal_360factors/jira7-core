package com.atlassian.jira.pageobjects.navigator;

import javax.inject.Inject;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;

import org.openqa.selenium.By;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public class ColumnsConfigurationDialog {
    @ElementBy(id = "inline-dialog-column-picker-dialog")
    private PageElement dialog;

    @ElementBy(id = "user-column-sparkler-input")
    private PageElement input;

    @ElementBy(cssSelector = ".button-panel input[type=\"submit\"]")
    private PageElement submit;

    @Inject
    private PageElementFinder finder;

    @WaitUntil
    public void ready() {
        waitUntilTrue(dialog.timed().isPresent());
    }

    public void addColumn(final String columnName) {
        input.clear();
        input.type(columnName);

        final PageElement checkbox = finder.find(By.cssSelector("label[data-descriptor-title=\"" + columnName + "\"] input[type=\"checkbox\"]"));
        checkbox.click();
    }

    public void submit() {
        submit.click();
    }
}
