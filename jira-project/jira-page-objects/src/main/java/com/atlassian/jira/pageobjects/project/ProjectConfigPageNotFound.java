package com.atlassian.jira.pageobjects.project;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

/**
 * @since v6.2
 */
public class ProjectConfigPageNotFound extends AbstractJiraPage {
    private final String url;

    @ElementBy(id = "project-config-tab-not-found")
    private PageElement errorElement;

    public ProjectConfigPageNotFound(String suffix) {
        this.url = String.format("/plugins/servlet/project-config/%s", suffix);
    }

    @Override
    public TimedCondition isAt() {
        return errorElement.timed().isPresent();
    }

    @Override
    public String getUrl() {
        return url;
    }
}
