package com.atlassian.jira.pageobjects.pages.admin;

import com.atlassian.jira.pageobjects.dialogs.FormDialog;
import com.atlassian.pageobjects.PageBinder;
import javax.inject.Inject;

/**
 * The EditProject/ProjectDetails dialog associated with the "Edit" action on the ViewProjects administration page.
 *
 * @since v7.2
 */
public class EditProjectDialog extends FormDialog {

    @Inject
    private PageBinder pageBinder;

    public EditProjectDialog(String editProjectDialogId) {
        super(editProjectDialogId);
    }

    public ProjectDetailsHelpTooltip getProjectKeyHelpTooltip() {
        return pageBinder.bind(ProjectDetailsHelpTooltip.class, "project-key-help-trigger", "inline-project-key-help");
    }

    public ProjectDetailsHelpTooltip getProjectTypeHelpTooltip() {
        return pageBinder.bind(ProjectDetailsHelpTooltip.class, "project-type-help-trigger", "project-type-help");
    }

    public ProjectDetailsHelpTooltip getProjectCategoryHelpTooltip() {
        return pageBinder.bind(ProjectDetailsHelpTooltip.class, "project-category-help-trigger", "project-category-help");
    }

}
