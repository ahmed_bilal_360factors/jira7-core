package com.atlassian.jira.pageobjects.project.workflow;

import com.atlassian.jira.pageobjects.dialogs.admin.AbstractAssignIssueTypesDialog;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.pageobjects.websudo.DecoratedJiraWebSudo;
import com.atlassian.jira.pageobjects.websudo.JiraSudoFormDialog;
import com.atlassian.jira.pageobjects.websudo.JiraWebSudo;
import org.openqa.selenium.By;

import javax.inject.Inject;

/**
 * @since v5.2
 */
public class AssignIssueTypesDialog extends AbstractAssignIssueTypesDialog<AssignIssueTypesDialog> {
    @Inject
    private TraceContext traceContext;

    @Override
    public void submit() {
        final Tracer checkpoint = traceContext.checkpoint();
        super.submit();
        waitForReady(checkpoint);
    }

    @Override
    protected AssignIssueTypesDialog getThis() {
        return this;
    }

    public JiraWebSudo submitWebsudo() {
        final Tracer checkpoint = traceContext.checkpoint();
        submit(By.id("assign-issue-types-submit"));

        final JiraSudoFormDialog bind = binder.bind(JiraSudoFormDialog.class, JiraSudoFormDialog.ID_SMART_WEBSUDO);
        return new DecoratedJiraWebSudo(bind) {
            @Override
            protected void afterAuthenticate() {
                waitForReady(checkpoint);
            }
        };
    }

    public AddWorkflowDialog back() {
        clickBack();
        return binder.bind(AddWorkflowDialog.class);
    }

    private void waitForReady(Tracer checkpoint) {
        traceContext.waitFor(checkpoint, WorkflowsPageTab.TRACE_KEY);
    }
}
