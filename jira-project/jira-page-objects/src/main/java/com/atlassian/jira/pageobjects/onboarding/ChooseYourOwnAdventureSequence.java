package com.atlassian.jira.pageobjects.onboarding;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.projecttemplates.pageobject.ProjectTemplatesDialog;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;

/**
 * @since 7.1
 */
public class ChooseYourOwnAdventureSequence extends Sequence {
    @Inject
    PageBinder pageBinder;

    @ElementBy(className = "next-steps")
    private PageElement content;

    @ElementBy(id = "sampleData", within = "content")
    private PageElement createSampleDataProjectButton;

    @ElementBy(id = "emptyProject", within = "content")
    private PageElement createNewProjectButton;

    @ElementBy(id = "import", within = "content")
    private PageElement importProjectButton;

    @ElementBy(tagName = "h1", within = "content")
    private PageElement heading;

    @ElementBy(tagName = "p", within = "content")
    private PageElement subHeading;

    @Override
    public TimedCondition isAt() {
        return content.timed().isVisible();
    }

    public String getPageHeadingText() {
        return heading.getText();
    }

    public String getPageSubHeadingText() {
        return subHeading.getText();
    }

    public void createSampleDataProjectClick() {
        this.createSampleDataProjectButton.click();
    }


    public ProjectTemplatesDialog createEmptyProject() {
        createNewProjectButton.click();
        return pageBinder.bind(ProjectTemplatesDialog.class);
    }

    public JiraHeader getHeader() {
        return pageBinder.bind(JiraHeader.class);
    }

    public void importProjectClick() {
        this.importProjectButton.click();
    }

    public List<ChooseYourOwnAdventureStepBox> getStepBoxes() {
        return content.findAll(By.className("next-steps--step")).stream().map(element -> pageBinder.bind(ChooseYourOwnAdventureStepBox.class, element)).collect(CollectorsUtil.toImmutableList());
    }
}
