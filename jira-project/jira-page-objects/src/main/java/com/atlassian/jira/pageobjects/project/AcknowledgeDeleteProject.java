package com.atlassian.jira.pageobjects.project;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Acknowledge delete project page
 *
 * @since v7.2
 */
public class AcknowledgeDeleteProject extends AbstractJiraPage {
    private static final String URI = "/secure/admin/DeleteProject!default.jspa?pid=%d&returnUrl=ViewProjects.jspa";
    private Long projectId;

    @ElementBy(id = "acknowledge_submit")
    private PageElement acknowledgeSubmit;

    public AcknowledgeDeleteProject() {}

    public AcknowledgeDeleteProject(final Long projectId) {
        this.projectId = notNull(projectId);
    }

    @Override
    public TimedCondition isAt() {
        return acknowledgeSubmit.timed().isPresent();
    }

    public void acknowledgeSubmit() {
        acknowledgeSubmit.click();
    }

    @Override
    public String getUrl() {
        return String.format(URI, projectId);
    }
}
