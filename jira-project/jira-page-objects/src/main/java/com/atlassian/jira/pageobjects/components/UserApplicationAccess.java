package com.atlassian.jira.pageobjects.components;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @since v7.0
 */
public class UserApplicationAccess {
    @Inject
    private PageElementFinder pageElementFinder;

    private final By applicationAccessLocator;

    public UserApplicationAccess(final By applicationAccessLocator) {
        this.applicationAccessLocator = applicationAccessLocator;
    }

    private PageElement applicationAccess() {
        return pageElementFinder.find(applicationAccessLocator);
    }

    private PageElement applicationCheckbox(final String applicationKey) {
        return applicationAccess().find(By.cssSelector(String.format(".application[data-key=%s]:not(.application-warning)", applicationKey)));
    }

    private PageElement applicationCriticalCheckbox(final String applicationKey) {
        return applicationAccess().find(By.cssSelector(String.format(".application-warning-%s", applicationKey)));
    }

    public List<String> getApplicationIds() {
        return applicationAccess().findAll(By.cssSelector(".application")).stream()
                .map(pageElement -> pageElement.getAttribute("data-key"))
                .collect(Collectors.toList());
    }

    public List<String> getApplicationNames() {
        return applicationAccess().findAll(By.cssSelector(".application-label")).stream()
                .map(PageElement::getText)
                .collect(Collectors.toList());
    }

    public TimedCondition isApplicationEnabled(final String applicationKey) {
        return applicationCheckbox(applicationKey).timed().isEnabled();
    }

    public TimedCondition isCriticalApplicationEffective(final String applicationKey) {
        return applicationCriticalCheckbox(applicationKey).timed().hasClass("effective");
    }

    public TimedCondition isApplicationIndeterminate(final String applicationKey) {
        return applicationCheckbox(applicationKey).timed().hasAttribute("indeterminate", "true");
    }

    public TimedCondition hasApplicationEffectiveWarningVisible(final String applicationKey) {
        return pageElementFinder.find(By.id(applicationKey + "-effective-warning")).timed().isVisible();
    }

    public TimedCondition isApplicationVisible(final String applicationKey) {
        return applicationCheckbox(applicationKey).timed().isVisible();
    }

    public TimedCondition isApplicationSelected(final String applicationKey) {
        return applicationCheckbox(applicationKey).timed().isSelected();
    }

    public TimedCondition hasWarning(final String applicationKey) {
        return applicationAccess().find(By.cssSelector(String.format(".application-warning[data-key=\"%s\"]", applicationKey))).timed().isVisible();
    }

    public UserApplicationAccess toggleApplication(final String applicationKey) {
        applicationCheckbox(applicationKey).click();
        return this;
    }

    public UserApplicationAccess toggleApplication(final String applicationKey, boolean checked) {
        final TimedCondition isSelected = applicationCheckbox(applicationKey).timed().isSelected();
        if ((checked && !isSelected.byDefaultTimeout()) ||
                (!checked && isSelected.byDefaultTimeout())) {
            toggleApplication(applicationKey);
        }

        return this;
    }
}
