package com.atlassian.jira.pageobjects.pages.reports;

import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import javax.inject.Inject;

import static org.springframework.core.GenericTypeResolver.resolveTypeArgument;

/**
 * The ConfigureReport page, where parameters are set for a report. Note that the same web action is used for both this,
 * and the #ReportResultPage. This needs to be extended to a concrete page for a specific report (e.g. "Average Age
 * Report Page".
 *
 * @since v7.2
 */
abstract class AbstractConfigureReportPage<T extends AbstractJiraPage> extends AbstractJiraPage {

    private final Class<T> nextPageClass;

    @ElementBy(id = "configure-report")
    private PageElement configureReportContainer;

    @Inject
    protected Navigation navigation;

    @Override
    public TimedCondition isAt() {
        return configureReportContainer.timed().isVisible();
    }

    @Override
    public String getUrl() {
        return "/secure/ConfigureReport!default.jspa&reportKey=com.atlassian.jira.jira-core-reports-plugin:"
                + getReportKey();
    }

    @ElementBy(id = "next_submit")
    private PageElement submitButton;

    public AbstractConfigureReportPage() {
        @SuppressWarnings("unchecked")
        Class<T> clazz = (Class<T>) resolveTypeArgument(getClass(), AbstractConfigureReportPage.class);
        this.nextPageClass = clazz;
    }

    public T next() {
        submitButton.click();
        return pageBinder.bind(nextPageClass);
    }

    /**
     * @return The report key for the concrete report (Average Age Report, Pie Chart Report, etc).
     */
    protected abstract String getReportKey();

}
