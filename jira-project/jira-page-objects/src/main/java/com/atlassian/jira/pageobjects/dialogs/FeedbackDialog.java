package com.atlassian.jira.pageobjects.dialogs;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * The Feedback Dialog that displays a form to submit to the issue collector
 */
public class FeedbackDialog {
    @ElementBy(id = "atlwdg-frame")
    private PageElement issueCollector;

    @ElementBy(className = "jira-page-loading-indicator")
    private PageElement loadSpinner;

    @WaitUntil
    public void waitForLoad() {
        Poller.waitUntilFalse(loadSpinner.timed().isVisible());
        Poller.waitUntilTrue(issueCollector.timed().isVisible());
    }

    public TimedCondition isOpen() {
        return issueCollector.timed().isVisible();
    }
}