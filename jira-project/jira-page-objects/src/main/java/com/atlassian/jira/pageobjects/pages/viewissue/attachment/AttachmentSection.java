package com.atlassian.jira.pageobjects.pages.viewissue.attachment;

import com.atlassian.jira.pageobjects.framework.elements.ExtendedElementFinder;
import com.atlassian.jira.pageobjects.framework.elements.PageElements;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementActions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.Queries;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.Map;

/**
 * Attachment section of "View Issue" page
 *
 * @since v6.4
 */
public class AttachmentSection {
    @Inject
    private PageBinder pageBinder;

    @Inject
    private PageElementActions actions;

    @Inject
    private ExtendedElementFinder extendedFinder;

    @Inject
    protected Timeouts timeouts;

    @ElementBy(id = "attachmentmodule")
    private PageElement section;

    @ElementBy(cssSelector = "#attachmentmodule_heading .icon.drop-menu")
    private PageElement optionsDrop;

    public AttachmentOptions openOptions() {
        PageElements.scrollIntoView(optionsDrop);
        if (!optionsDrop.hasClass("active")) {
            optionsDrop.click();
        }
        return pageBinder.bind(AttachmentOptions.class);
    }

    private TimedQuery<Iterable<PageElement>> getSkatedElements() {
        return Queries.forSupplier(timeouts, extendedFinder.within(section)
                        .newQuery(By.className("js-file-attachment"))
                        .filter(Predicates.and(PageElements.isPresent(), PageElements.attributeExists("resolved")))
                        .supplier()
        );
    }

    public Map<String, AttachmentRow> getAttachmentRowsByTitle() {
        return getAttachmentRowsByTitle(false, -1);
    }

    public Map<String, AttachmentRow> getAttachmentRowsByTitle(boolean previewable, int expectedRows) {
        final ImmutableMap.Builder<String, AttachmentRow> rowsByTitle = ImmutableMap.builder();
        Poller.waitUntil("Not all attachment elements were initialised correctly",
                getSkatedElements(),
                Matchers.iterableWithSize(Matchers.greaterThanOrEqualTo(expectedRows))
        );
        final Iterable<PageElement> rows = getSkatedElements().now();
        for (final PageElement row : rows) {
            if (previewable && !row.find(By.cssSelector("a[file-preview-type]")).isPresent()) {
                continue;
            }
            PageElements.scrollIntoView(row);
            final AttachmentRow attachmentRow = new AttachmentRow(row, pageBinder, actions, timeouts);
            rowsByTitle.put(attachmentRow.getTitle(), attachmentRow);
        }
        return rowsByTitle.build();
    }

}
