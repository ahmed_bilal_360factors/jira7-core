package com.atlassian.jira.pageobjects.pages.webhooks;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.google.common.collect.Iterables;
import org.openqa.selenium.By;

import javax.inject.Inject;

public class WebHookAdminPage extends AbstractJiraPage {
    public static final String WEBHOOK_LIST_CSS_SELECTOR = ".webhooks .webhooks-container li";

    @Inject
    protected PageElementFinder finder;
    @ElementBy(id = "add-webhook")
    private PageElement createWebHookButton;
    @ElementBy(className = "webhook-details")
    private PageElement webHookDetails;

    @Override
    public TimedCondition isAt() {
        return Conditions.and(createWebHookButton.timed().isEnabled(),
                webHookDetails.withTimeout(TimeoutType.SLOW_PAGE_LOAD).timed().isPresent(),
                finder.find(By.cssSelector(WEBHOOK_LIST_CSS_SELECTOR)).timed().isPresent());
    }

    @Override
    public String getUrl() {
        return "/plugins/servlet/webhooks";
    }

    public Iterable<WebHookLink> getWebHookLinks() {
        return Iterables.transform(finder.findAll(By.cssSelector(WEBHOOK_LIST_CSS_SELECTOR)), element -> new WebHookLink(pageBinder, element));
    }

    public EditWebHookListenerPanel createWebHook() {
        if (createWebHookButton.isEnabled()) {
            createWebHookButton.click();
            return pageBinder.bind(EditWebHookListenerPanel.class);
        } else {
            throw new IllegalStateException("WebHook link is not enabled");
        }
    }

    public static class WebHookLink {
        private final PageBinder pageBinder;
        private final PageElement webHookLink;

        public WebHookLink(final PageBinder pageBinder, final PageElement webHookLink) {
            this.pageBinder = pageBinder;
            this.webHookLink = webHookLink;
        }

        public String getName() {
            return webHookLink.getText();
        }

        public ViewWebHookListenerPanel click() {
            webHookLink.click();
            return pageBinder.bind(ViewWebHookListenerPanel.class);
        }
    }
}
