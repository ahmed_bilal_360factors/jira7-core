package com.atlassian.jira.pageobjects.pages.viewissue.attachment;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.pageobjects.framework.elements.PageElements;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementActions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.core.Is.is;

public class FilePreview {

    public static final String FILE_PREVIEW_ID = "cp-container-1";

    public enum PreviewType {
        IMAGE, PDF, UNKNOWN;
    }

    @Inject
    Timeouts timeouts;
    @Inject
    private WebDriver driver;
    @Inject
    private PageBinder pageBinder;

    @ElementBy(id = FILE_PREVIEW_ID)
    PageElement container;

    @ElementBy(id = "cp-img")
    PageElement imageContainer;

    @ElementBy(id = "pageContainer1")
    PageElement pdfContainer;

    @ElementBy(id = "cp-control-panel-close")
    PageElement closeButton;

    @ElementBy(id = "cp-control-panel-download")
    private PageElement downloadButton;

    @ElementBy(id = "cp-nav-left")
    private PageElement leftButton;

    @ElementBy(id = "cp-nav-right")
    private PageElement rightButton;

    @ElementBy(id = "cp-file-body")
    private PageElement previewerBody;

    @ElementBy(id = "cp-image-preview")
    private PageElement imagePreviewerBody;

    @ElementBy(id = "cp-sidebar")
    private PageElement sideBar;

    @ElementBy(className = "cp-toolbar")
    private PageElement fileControls;

    @ElementBy(id = "cp-file-controls")
    private PageElement menuControls;

    @ElementBy(id = "cp-files-label")
    private PageElement showAllFiles;

    @ElementBy(id = "cp-title-container")
    protected PageElement titleContainer;

    public PreviewType getType() {
        if (container.find(By.id("cp-pdf-preview")).isPresent()) {
            return PreviewType.PDF;
        } else if (container.find(By.id("cp-image-preview")).isPresent()) {
            return PreviewType.IMAGE;
        } else {
            return PreviewType.UNKNOWN;
        }
    }

    @Init
    public void initialize() {
        PageElements.removeAuiBlanket(driver);
    }

    public void close() {
        closeButton.click();
        waitUntilFalse("Preview is gone", container.timed().isPresent());
    }

    public boolean isPresent() {
        return container.isPresent();
    }

    public TimedCondition isPresentCondition() {
        return container.timed().isPresent();
    }

    @WaitUntil
    public void isAt() {
        switch(getType()) {
            case IMAGE:
                waitUntilTrue(imageContainer.timed().isPresent());
                break;
            case PDF:
                waitUntilTrue(pdfContainer.timed().isPresent());
                break;
            case UNKNOWN:
            default:
                waitUntilTrue(container.timed().isPresent());
        }
    }


    public PageElement getPreviewerBody() {
        return previewerBody;
    }

    public PageElement getFilePreviewBody() {
        return imagePreviewerBody;
    }

    public PageElement getImage() {
        waitUntilTrue(previewerBody.find(By.id("cp-img")).timed().isVisible());
        return previewerBody.find(By.id("cp-img"));
    }

    public PageElement getPDF() {
        return previewerBody.find(By.id("viewer"));
    }

    public PageElement getFileControls() {
        return fileControls;
    }

    public PageElement getContainer() {
        return container;
    }

    public String getDownloadURL() {
        return downloadButton.getAttribute("href");
    }

    public String getTitle() {
        waitUntilTrue(titleContainer.timed().isVisible());
        return titleContainer.getText();
    }

    public FilePreview nextFile() {
        rightButton.click();
        return this;
    }

    public FilePreview previousFile() {
        leftButton.click();
        return this;
    }

    public boolean isVisible() {
        return container.isPresent() && container.isVisible();
    }

    public boolean isSidebarVisible() {
        return sideBar.isPresent() && sideBar.isVisible() && sideBar.getAttribute("class").contains("expanded");
    }

    public FilePreview waitUntilVisible() {
        waitUntil("Image previewer did not become visible", container.timed().isVisible(), is(Boolean.TRUE), Poller.by(30, TimeUnit.SECONDS));
        return this;
    }

    public FilePreview waitUntilImageVisible() {
        waitUntilTrue(getImage().timed().isVisible());
        return this;
    }

    public FilePreview waitUntilPDFVisible() {
        waitUntilTrue(getPDF().timed().isVisible());
        return this;
    }

    public FilePreview waitUntilFileControlsVisible() {
        waitUntil("File controls did not become visible", fileControls.timed().isVisible(), is(Boolean.TRUE), Poller.by(30, TimeUnit.SECONDS));
        return this;
    }

    public FilePreview waitUntilFileControlsClosed() {
        waitUntilFalse(fileControls.timed().isVisible());
        return this;
    }

    public FilePreview waitUntilClosed() {
        waitUntilFalse(container.timed().isPresent());
        return this;
    }

    public FilePreview showAllFiles() {
        showAllFiles.click();
        return this;
    }

    public int getThumbnailCount() {
        return driver.findElements(By.className("cp-thumbnail")).size();
    }

    public List<String> getThumbnailTitles() {
        return driver.findElements(By.tagName("figcaption")).stream().map(WebElement::getText)
                .collect(CollectorsUtil.toImmutableList());
    }

    public void showFileControls(boolean hoverMouseOverFileControls) {
        final PageElementActions actions = pageBinder.bind(PageElementActions.class);
        actions.moveToElement(container.find(By.id("cp-body")));
        actions.moveToElement(container.find(By.className("cp-annotatable")));
        if (hoverMouseOverFileControls) {
            actions.moveToElement(container.find(By.cssSelector(".cp-toolbar a")));
        }
        actions.perform();

        waitUntilFileControlsVisible();
    }

    public boolean isUnknownFileTypePreview() {
        PageElement pageElement = container.find(By.id("cp-unknown-file-type-view-wrapper"));
        waitUntilTrue(pageElement.timed().isPresent());
        return pageElement.isVisible();
    }

    /**
     * Check whether current view is PDF view or not.
     */
    public TimedQuery<Boolean> isPDFView() {
        PageElement pdfViewElement = container.find(By.id("cp-pdf-preview"));
        return pdfViewElement.timed().isVisible();
    }

    /**
     * Check whether current view is Image view or not.
     */
    public TimedQuery<Boolean> isImageView() {
        return imagePreviewerBody.timed().isVisible();
    }
}
