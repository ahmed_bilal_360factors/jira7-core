package com.atlassian.jira.pageobjects.config.junit4.rule;

import com.atlassian.jira.functest.framework.backdoor.ApplicationRoleControl;
import com.atlassian.jira.functest.framework.util.junit.AnnotatedDescription;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.CreateUser;
import com.google.common.collect.Sets;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Creates new user via JIRA REST API.
 *
 * @see CreateUser
 */
public class CreateUserRule extends TestWatcher {
    private boolean userCreated;

    @Inject
    private JiraTestedProduct jira;

    @Override
    protected void starting(final Description description) {
        final AnnotatedDescription annotatedDescription = new AnnotatedDescription(description);

        if (annotatedDescription.isMethodAnnotated(CreateUser.class)) {
            doCreateUser(annotatedDescription.getAnnotation(CreateUser.class));
        }
    }

    @Override
    protected void finished(final Description description) {
        final AnnotatedDescription annotatedDescription = new AnnotatedDescription(description);

        if (annotatedDescription.isMethodAnnotated(CreateUser.class)) {
            doDeleteUser(annotatedDescription.getAnnotation(CreateUser.class));
        }
    }

    private void doCreateUser(final CreateUser createUser) {
        final String username = createUser.username();
        final String password = createUser.password();
        final String[] groupnames = createUser.groupnames();

        if (!userExists(username)) {
            createUser(username, password);
            // later, allow to delete just users created by this
            userCreated = true;
        }

        final Set<String> groups = Sets.newHashSet(groupnames);
        if (createUser.admin()) {
            groups.add("jira-administrators");
        }
        // add user to all determined groups
        if (!groups.isEmpty()) {
            addUserToGroups(username, groups);
        }

        if (!createUser.user()) {
            removeUserFromGroups(username, groupsWithApplicationAccess());
        }
    }

    private void doDeleteUser(final CreateUser createUser) {
        final String username = createUser.username();

        if (userExists(username) && userCreated) {
            deleteUser(username);
        }
    }

    private boolean userExists(final String username) {
        return jira.backdoor().usersAndGroups().userExists(username);
    }

    private void createUser(final String username, final String password) {
        final String displayName = "Test User " + username;
        final String email = username + "@example.com";
        jira.backdoor().usersAndGroups().addUser(username, password, displayName, email);
    }

    private void deleteUser(final String username) {
        jira.backdoor().usersAndGroups().deleteUser(username);
    }

    private void addUserToGroups(final String username, final Iterable<String> groupNames) {
        for (final String groupName : groupNames) {
            jira.backdoor().usersAndGroups().addUserToGroup(username, groupName);
        }
    }

    private void removeUserFromGroups(final String username, final Iterable<String> groupNames) {
        for (final String groupName : groupNames) {
            if (jira.backdoor().usersAndGroups().isUserInGroup(username, groupName)) {
                jira.backdoor().usersAndGroups().removeUserFromGroup(username, groupName);
            }
        }
    }

    private Set<String> groupsWithApplicationAccess() {
        final Set<String> groups = new HashSet<>();
        final List<ApplicationRoleControl.ApplicationRoleBean> roles = jira.backdoor().applicationRoles().getRoles();
        for (final ApplicationRoleControl.ApplicationRoleBean role : roles) {
            groups.addAll(role.getGroups());
        }
        return groups;
    }

}
