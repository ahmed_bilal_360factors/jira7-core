package com.atlassian.jira.pageobjects.pages.webhooks;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

public class ViewWebHookListenerPanel extends AbstractWebHookPanel {
    @ElementBy(id = "webhook-edit")
    private PageElement webHookEditButton;

    @ElementBy(id = "webhook-delete")
    private PageElement webHookDeleteButton;

    public EditWebHookListenerPanel edit() {
        webHookEditButton.click();
        return binder.bind(EditWebHookListenerPanel.class);
    }

    public WebHookAdminPage delete() {
        webHookDeleteButton.click();
        return binder.bind(WebHookAdminPage.class);
    }

    public boolean isEnabled() {
        if (finder.find(By.id("webhook-enabled-lozenge")).isVisible()) {
            return true;
        } else if (finder.find(By.id("webhook-disabled-lozenge")).isVisible()) {
            return false;
        } else {
            throw new AssertionError("Neither webhook enabled, nor webhook disabled lozenges are visible.");
        }
    }

    public String getTransitions() {
        return finder.find(By.id("webhook-transitions")).getText();
    }
}
