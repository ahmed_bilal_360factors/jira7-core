package com.atlassian.jira.pageobjects.components;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementActions;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import org.openqa.selenium.By;

import javax.inject.Inject;

/**
 * This class is pageobject for DropdownSelect.js object
 *
 * @since v7.1
 */
public class DropdownSelect {
    @Inject
    protected PageElementActions actions;
    @Inject
    protected PageElementFinder elementFinder;

    private final By triggerLocator;
    private final By dropdownLocator;
    private final PageElement context;

    public DropdownSelect(final PageElement context, final By triggerLocator, final By dropdownLocator) {
        this.context = context;
        this.triggerLocator = triggerLocator;
        this.dropdownLocator = dropdownLocator;
    }

    protected PageElement trigger() {
        return context.find(triggerLocator);
    }

    protected PageElement dropDown() {
        return elementFinder.find(dropdownLocator);
    }

    public DropdownSelect open() {
        if (!isOpen()) {
            trigger().click();
            waitForOpen();
        }
        return this;
    }

    protected PageElement getDropDownItem(final By locator) {
        return dropDown().find(locator);
    }

    public void waitForOpen() {
        Poller.waitUntilTrue(dropDown().timed().isVisible());
    }

    public void waitForClose() {
        Poller.waitUntilFalse(dropDown().timed().isVisible());
    }

    public boolean isOpen() {
        return dropDown().isPresent() && dropDown().isVisible();
    }

    public DropdownSelect close() {
        if (isOpen()) {
            trigger().click();
        }
        return this;
    }

    /**
     * List.js used in DropdownSelect.js creates (not very reliable) IDs for every list item.
     * IDs are stored as classes "aui-list-item-li-{onListId}". For test purposes it is enough to locate element.
     *  Example:
     *      <ul class="aui-list-section aui-last" id="groups">
     *          <li class="aui-list-item aui-list-item-li-jira-administrators active">
     *              <a class="aui-list-item-link" href="#" title="jira-administrators">jira-administrators</a>
     *          </li>
     *      </ul>
     *
     * @param onListId
     */
    public void pickOption(final String onListId) {
        open();
        final By selector = By.cssSelector(String.format(".aui-list-item-li-%s", onListId));
        activateSelection(selector);
        getDropDownItem(selector).click();
        waitForClose();
    }

    private void activateSelection(final By by) {
        final PageElement selection = elementFinder.find(by);

        // movement required in List.js to work - @see Mouse.js
        actions.moveToElement(selection).moveByOffset(1, 0).perform();
    }
}
