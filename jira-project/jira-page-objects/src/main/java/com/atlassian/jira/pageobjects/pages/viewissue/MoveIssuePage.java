package com.atlassian.jira.pageobjects.pages.viewissue;

import com.atlassian.jira.pageobjects.components.fields.SingleSelect;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.SelectElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

/**
 * @since v5.0
 */
public final class MoveIssuePage extends AbstractMoveIssuePage {
    @ElementBy(id = "project-field")
    protected PageElement newProjectField;

    @ElementBy(cssSelector = "#project-single-select .drop-menu")
    protected PageElement newProjectDropMenuTrigger;

    @ElementBy(id = "next_submit")
    protected PageElement nextButton;

    @ElementBy(id = "issuetype")
    protected SelectElement issueTypeSelectDataHolder;

    @ElementBy(cssSelector = ".currentProject")
    protected PageElement currentProject;

    protected SingleSelect issueTypeSelect;

    public MoveIssuePage(String issueKey) {
        super(issueKey);
    }

    @Override
    public TimedCondition isAt() {
        return newProjectField.timed().isPresent();
    }

    @Init
    public void initIssueTypeSelect() {
        this.issueTypeSelect = pageBinder.bind(SingleSelect.class, elementFinder.find(By.id("issuetype_container")));
    }

    public MoveIssuePage setNewProject(String newProject) {
        newProjectDropMenuTrigger.click();

        final SingleSelect singleSelect = pageBinder.bind(SingleSelect.class, elementFinder.find(By.id("project_container")));
        singleSelect.select(newProject);
        return this;
    }

    public Iterable<String> getIssueTypes() {
        List<String> issueTypes = new ArrayList<String>();
        //add currently selected, because it will not show up in dropdown
        issueTypes.add(issueTypeSelect.getValue());
        issueTypeSelect.triggerSuggestions();
        for (String s : issueTypeSelect.getSuggestionsTimed().now()) {
            issueTypes.add(s);
        }
        issueTypeSelect.triggerSuggestions();
        return issueTypes;
    }

    public MoveIssueUpdateFields next() {
        nextButton.click();

        return pageBinder.bind(MoveIssueUpdateFields.class);
    }

    public MoveIssueUpdateStatus submitAndGoToSetNewIssueStatus(String issueID, String assignee) {
        nextButton.click();
        return pageBinder.bind(MoveIssueUpdateStatus.class, issueID, assignee);
    }

    public MoveIssuePage setNewIssueType(String newIssueType) {
        issueTypeSelect.select(newIssueType);
        return this;
    }

    public TimedQuery<String> getCurrentProject() {
        return currentProject.timed().getText();
    }
}
