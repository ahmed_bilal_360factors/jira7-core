package com.atlassian.jira.pageobjects.project.roles.adduserdialog;

import com.atlassian.jira.pageobjects.components.fields.MultiSelect;
import org.openqa.selenium.By;

public class UserGroupPicker extends MultiSelect {
    public UserGroupPicker(String id) {
        super(id, itemName -> {
            if (itemName == null) {
                return By.cssSelector(".recipients li");
            } else {
                return By.cssSelector(".recipients li[title=\"" + itemName + "\"]");
            }
        });
    }
}
