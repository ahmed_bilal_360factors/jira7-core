package com.atlassian.jira.pageobjects.project.versions;

import com.atlassian.jira.pageobjects.dialogs.JiraDialog;
import com.atlassian.jira.pageobjects.framework.actions.DragAndDrop;
import com.atlassian.jira.pageobjects.project.AbstractProjectConfigPageTab;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.WebDriverElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.ButtonReleaseAction;
import org.openqa.selenium.interactions.ClickAndHoldAction;
import org.openqa.selenium.interactions.CompositeAction;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.interactions.MoveToOffsetAction;
import org.openqa.selenium.internal.Locatable;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * @since v4.4
 */
public class VersionPageTab extends AbstractProjectConfigPageTab {
    public static final String TAB_LINK_ID = "administer_project_versions";

    private static final String URI_TEMPLATE = "/plugins/servlet/project-config/%s/versions";

    private final String uri;
    private EditVersionForm createVersionForm;

    @Inject
    private PageElementFinder elementFinder;

    @Inject
    private DragAndDrop dragAndDrop;

    @ElementBy(id = "project-config-panel-versions")
    private PageElement versionsPage;

    @ElementBy(id = "project-config-operations-merge")
    private PageElement mergeLink;

    @ElementBy(className = "aui-restfultable-init")
    private PageElement tableInit;

    @ElementBy(className = "aui-restfultable-loading")
    private PageElement versionLoader;

    public VersionPageTab(final String projectKey) {
        this.uri = String.format(URI_TEMPLATE, projectKey);
    }

    @Init
    public void initialise() {
        createVersionForm = pageBinder.bind(EditVersionForm.class, By.className("project-config-versions-add-fields"));
    }

    @WaitUntil
    public void isReady() {
        Poller.waitUntilFalse(tableInit.timed().isPresent());
    }

    @Override
    public TimedCondition isAt() {
        return versionsPage.timed().isPresent();
    }

    public EditVersionForm getEditVersionForm() {
        return createVersionForm;
    }

    public PageElement getMergeLink() {
        return mergeLink;
    }

    public VersionPageTab closeServerErrorDialog() {
        elementFinder.find(By.id("server-error-dialog"))
                .find(By.className("cancel"))
                .click();

        return this;
    }

    public String getServerError() {
        JiraDialog dialog = pageBinder.bind(JiraDialog.class, "server-error-dialog");
        waitUntilTrue(dialog.isOpen());
        return dialog.find(By.className("aui-message")).getText();
    }

    public Version getVersionByName(final String name) {
        final List<Version> versions = getVersions();
        for (final Version version : versions) {
            if (version.getName().equals(name)) {
                return version;
            }
        }
        return null;
    }

    public List<Version> getVersions() {
        waitForPageLoad();
        List<Version> versions = new ArrayList<Version>();

        waitUntilTrue(isVersionsTableLoaded());

        if (versionsPage.find(By.className("aui-restfultable-no-entires")).isPresent()) {
            return versions;
        }

        final List<PageElement> versionElements = versionsPage.findAll(By.className("project-config-version"));

        for (PageElement versionElement : versionElements) {
            versions.add(pageBinder.bind(DefaultVersion.class, versionElement.getAttribute("data-id")));
        }

        return versions;
    }

    public void scrollToBottom() {
        JavascriptExecutor jse = driver;
        jse.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
        waitForPageLoad();
    }

    private void waitForPageLoad() {
        Poller.waitUntilFalse(versionLoader.timed().isVisible());
    }

    private TimedCondition isVersionsTableLoaded() {
        return versionsPage.find(By.cssSelector("#project-config-versions-table .aui-restfultable-create")).timed().isPresent();
    }

    @Override
    public String getUrl() {
        return uri;
    }

    public void moveVersionAbove(final Version source, final Version target) {
        final PageElement sourceDragHandle = getDragHandle(source);
        final PageElement targetDragHandle = getDragHandle(target);

        drag(sourceDragHandle, targetDragHandle, 2);
    }

    public void moveVersionBelow(final Version source, final Version target) {
        final PageElement sourceDragHandle = getDragHandle(source);
        final PageElement targetDragHandle = getDragHandle(target);

        drag(sourceDragHandle, targetDragHandle, 30);
    }

    /**
     * Helper method for more proper Drag'n'Drop behavior for newer (>= 12.0) Firefox version.
     * The com.atlassian.jira.pageobjects.framework.actions.DragAndDrop doesn't release the
     * mouse button properly for TestVersionConfig tests (and thus they fail), plus the dragged
     * element must be actually in the viewport for the actions to happen.
     */
    private void drag(PageElement source, PageElement target, int yOffset) {
        ((Locatable) (((WebDriverElement) source).asWebElement())).getCoordinates().inViewPort(); //scrolls the page to make this element visible in the viewport
        Mouse mouse = driver.getMouse();
        CompositeAction action = new CompositeAction();
        action.addAction(new ClickAndHoldAction(mouse, (Locatable) ((WebDriverElement) source).asWebElement()));
        action.addAction(new MoveToOffsetAction(mouse, (Locatable) ((WebDriverElement) target).asWebElement(), 0, yOffset));
        action.addAction(new ButtonReleaseAction(mouse, null));
        action.perform();
    }

    private PageElement getDragHandle(Version source) {
        final PageElement sourceVersionRow = getVersionRow(source.getName());
        final PageElement sourceVersionRowWebElement = elementFinder.find(By.id(sourceVersionRow.getAttribute("id")));
        return sourceVersionRowWebElement.find(By.className("aui-restfultable-draghandle"));
    }

    private PageElement getVersionRow(final String name) {
        final List<PageElement> versionElements = versionsPage.findAll(By.className("project-config-version"));

        for (PageElement versionElement : versionElements) {
            final DefaultVersion version = pageBinder.bind(DefaultVersion.class, versionElement.getAttribute("data-id"));
            if (version.getName().equals(name)) {
                return versionElement;
            }
        }

        return null;
    }

    public MergeDialog openMergeDialog() {
        waitUntilFalse(isVersionsLoading());
        mergeLink.click();
        return pageBinder.bind(MergeDialog.class);
    }

    public TimedQuery<Boolean> isVersionsLoading() {
        return getVersionstable().timed().hasClass("loading");
    }

    private PageElement getVersionstable() {
        return elementFinder.find(By.id("project-config-versions-table"));
    }
}
