package com.atlassian.jira.pageobjects.project.versions.operations;

import com.atlassian.jira.pageobjects.dialogs.JiraDialog;
import com.atlassian.jira.pageobjects.navigator.IssueNavigatorResults;
import com.atlassian.jira.pageobjects.project.versions.VersionPageTab;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * @since v4.4
 */
public class DeleteOperation extends JiraDialog {
    public static final String SWAP_AFFECT_VERSION = "swapAffectVersion";
    public static final String REMOVE_AFFECT_VERSION = "removeAffectVersion";
    public static final String SWAP_FIX_VERSION = "swapFixVersion";
    public static final String REMOVE_FIX_VERSION = "removeFixVersion";
    public static final String CUSTOM_FIELD_SWAP_PREFIX = "customFieldSwap";
    public static final String CUSTOM_FIELD_REMOVE_PREFIX = "customFieldRemove";

    private static final Pattern CUSTOM_FIELD_ID_MATCHER = Pattern.compile("^[a-z-]+-([0-9]+)[a-z-]*$");
    private static final String MOVE_CUSTOM_FIELD_XPATH = "//*[starts-with(@id, 'move-custom-')]";
    private static final String REMOVE_CUSTOM_FIELD_XPATH = "//*[starts-with(@id, 'custom-remove-')]";

    private static String DIALOG_ID_FORMAT = "version-%s-delete-dialog";
    protected PageElement swapAffectVersion;
    protected PageElement swapAffectVersionSelect;
    protected PageElement removeAffectVersion;
    protected PageElement affectsCount;
    protected PageElement swapFixVersion;
    protected PageElement swapFixVersionSelect;
    protected PageElement removeFixVersion;
    protected PageElement fixCount;
    protected PageElement submitButton;
    protected PageElement infoMessage;
    protected PageElement customCount;

    private String projectKey;
    @Inject
    private PageElementFinder elementFinder;

    public DeleteOperation(final String versionId) {
        super(String.format(DIALOG_ID_FORMAT, versionId));
    }

    @WaitUntil
    public void dialogOpen() {
        waitUntilTrue(isOpen());
    }

    @Init
    public void getElements() {
        swapAffectVersion = this.find(By.id("affects-swap"));
        swapAffectVersionSelect = this.find(By.name("moveAffectedIssuesTo"));
        removeAffectVersion = this.find(By.id("affects-remove"));
        affectsCount = this.find(By.id("affects-count"));
        customCount = this.find(By.id("custom-count"));
        swapFixVersion = this.find(By.id("fix-swap"));
        swapFixVersionSelect = this.find(By.name("moveFixIssuesTo"));
        removeFixVersion = this.find(By.id("fix-remove"));
        fixCount = this.find(By.id("fix-count"));
        submitButton = this.find(By.id("submit"));
        infoMessage = this.find(By.cssSelector(".info-message"));
        projectKey = elementFinder.find(By.name("projectKey")).getAttribute("content");
    }

    public Map<String, Boolean> getOperations() {
        Map<String, Boolean> operationMap = new HashMap<String, Boolean>();

        operationMap.put(SWAP_AFFECT_VERSION, swapAffectVersion.isPresent());
        operationMap.put(REMOVE_AFFECT_VERSION, removeAffectVersion.isPresent());
        operationMap.put(SWAP_FIX_VERSION, swapFixVersion.isPresent());
        operationMap.put(REMOVE_FIX_VERSION, removeFixVersion.isPresent());

        final List<PageElement> customFieldModifiers = this.findAll(By.xpath(MOVE_CUSTOM_FIELD_XPATH));
        extractCustomFieldIds(customFieldModifiers)
                .forEach(customFieldId -> operationMap.put(CUSTOM_FIELD_SWAP_PREFIX + customFieldId, true));

        final List<PageElement> customFieldRemovers = this.findAll(By.xpath(REMOVE_CUSTOM_FIELD_XPATH));
        extractCustomFieldIds(customFieldRemovers)
                .forEach(customFieldId -> {
                    operationMap.put(CUSTOM_FIELD_REMOVE_PREFIX + customFieldId, true);
                });

        return operationMap;
    }

    private Stream<Integer> extractCustomFieldIds(List<PageElement> customFieldRemovers) {
        return customFieldRemovers
                .stream()
                .map(pageElement -> pageElement.getAttribute("id"))
                .map(elementId -> {
                    final Matcher matcher = CUSTOM_FIELD_ID_MATCHER.matcher(elementId);
                    return matcher.matches() ? Integer.parseInt(matcher.group(1)) : null;
                })
                .filter(Objects::nonNull);
    }

    public int getAffectsCount() {
        return Integer.parseInt(affectsCount.getText());
    }

    public int getFixCount() {
        return Integer.parseInt(fixCount.getText());
    }

    public String getInfoMessage() {
        if (infoMessage.isPresent()) {
            return infoMessage.getText();
        } else {
            return null;
        }
    }

    public DeleteOperation setCustomFieldSwapToVersion(long customFieldId, final String versionName) {
        final PageElement customFieldSwapSelector = getCustomFieldSwapVersionSelector(customFieldId);

        selectOption(customFieldSwapSelector, versionName);
        return this;
    }

    public DeleteOperation setCustomFieldRemove(long customFieldId) {
        final String swapOptionSelector = String.format("custom-remove-%d", customFieldId);
        final PageElement customFieldSwapRadio = this.find(By.id(swapOptionSelector));
        if (!customFieldSwapRadio.isPresent()) {
            throw new NoSuchElementException("There's no remove version for custom field: " + customFieldId);
        }
        customFieldSwapRadio.select();

        return this;
    }

    public int getAffectedIssuesForCustomField(long customFieldId) {
        final PageElement customFieldAffectedCount = getAffectedIssueLinkForCustomField(customFieldId);

        return Integer.parseInt(customFieldAffectedCount.getText().trim());
    }

    public DeleteOperation setFixToSwapVersion(final String versionName) {
        selectOption(swapFixVersionSelect, versionName);
        return this;
    }


    public String getAffectedIssuesLinkForCustomField(int customFieldId) {
        final PageElement affectedIssueLink = getAffectedIssueLinkForCustomField(customFieldId);

        return affectedIssueLink.getAttribute("href");
    }

    private PageElement getAffectedIssueLinkForCustomField(long customFieldId) {
        final String customFieldAffectedCountId = String.format("custom-%d-count", customFieldId);
        final PageElement customFieldAffectedCount = this.find(By.id(customFieldAffectedCountId));
        if (!customFieldAffectedCount.isPresent()) {
            throw new NoSuchElementException("There's no affected issues count for custom field: " + customFieldId);
        }
        return customFieldAffectedCount;
    }

    private PageElement getCustomFieldSwapVersionSelector(long customFieldId) {
        final String customFieldSwapSelectorId = String.format("move-custom-%d-to", customFieldId);
        final PageElement customFieldSwapSelector = this.find(By.id(customFieldSwapSelectorId));
        if (!customFieldSwapSelector.isPresent()) {
            throw new NoSuchElementException("There's no swap version selector for custom field: " + customFieldId);
        }
        return customFieldSwapSelector;
    }

    // Flakey
    public int getAffectsIssuesCountInNavigator() throws InterruptedException {

        final String mainWindowId = driver.getWindowHandle();

        affectsCount.click();

        for (String handle : driver.getWindowHandles()) {
            if (!handle.equals(mainWindowId)) {
                driver.switchTo().window(handle);
            }
        }

        final int resultCount = binder.bind(IssueNavigatorResults.class).getTotalCount();

        driver.close();

        driver.switchTo().window(mainWindowId);

        return resultCount;
    }

    // Flakey
    public int getFixIssuesCountInNavigator() throws InterruptedException {

        final String mainWindowId = driver.getWindowHandle();

        fixCount.click();


        for (String handle : driver.getWindowHandles()) {
            if (!handle.equals(mainWindowId)) {
                driver.switchTo().window(handle);
            }
        }

        final int resultCount = binder.bind(IssueNavigatorResults.class).getTotalCount();

        driver.close();

        driver.switchTo().window(mainWindowId);

        return resultCount;
    }

    public DeleteOperation setFixToRemoveVersion() {
        removeFixVersion.click();
        return this;
    }

    public DeleteOperation setAffectsToSwapVersion(final String versionName) {
        selectOption(swapAffectVersionSelect, versionName);
        return this;
    }

    public DeleteOperation setAffectsToRemoveVersion() {
        removeAffectVersion.click();
        return this;
    }

    public VersionPageTab submit() {
        submitButton.click();
        waitUntilTrue(isClosed());
        return binder.bind(VersionPageTab.class, projectKey);
    }

    private DeleteOperation selectOption(final PageElement select, final String optionText) {
        List<PageElement> options = select.findAll(By.tagName("option"));

        for (PageElement option : options) {
            if (option.getText().equals(optionText)) {
                option.select();
                break;
            }
        }

        return this;
    }
}
