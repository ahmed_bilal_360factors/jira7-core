package com.atlassian.jira.pageobjects.pages.admin;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.jira.pageobjects.pages.EditPermissionsSinglePage;
import com.atlassian.jira.pageobjects.project.ProjectSharedBy;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;

/**
 * @since v4.4
 * @deprecated since 7.1.0 - use {@link com.atlassian.jira.pageobjects.pages.EditPermissionsSinglePage} directly instead
 */
@Deprecated
public class EditPermissionScheme extends AbstractJiraPage {
    private EditPermissionsSinglePage editPermissionsSinglePage;

    private long schemeId = 0;

    public EditPermissionScheme(long schemeId) {
        this.schemeId = schemeId;
    }

    // this annotation might be a bit misleading - getUrl and isAt are called before init when this pageobject is used
    @Init
    public void init() {
        editPermissionsSinglePage = pageBinder.bind(EditPermissionsSinglePage.class, schemeId);
    }

    @Override
    public String getUrl() {
        return EditPermissionsSinglePage.getPageUrl((int) schemeId);
    }

    @Override
    public TimedCondition isAt() {
        // conditions are relaxed here because the real bind condition lives in the method annotated with @Init
        return Conditions.alwaysTrue();
    }

    public long getSchemeId() {
        return editPermissionsSinglePage.getSchemeId();
    }

    public ProjectSharedBy getSharedBy() {
        return editPermissionsSinglePage.getSharedBy();
    }
}
