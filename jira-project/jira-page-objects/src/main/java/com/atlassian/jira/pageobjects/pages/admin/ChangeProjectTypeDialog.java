package com.atlassian.jira.pageobjects.pages.admin;

import com.atlassian.jira.pageobjects.pages.AbstractJiraAdminPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import org.openqa.selenium.By;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;

/**
 * Change Project Type Dialog page on admin view projects.
 *
 * @since v7
 */
public class ChangeProjectTypeDialog extends AbstractJiraAdminPage {

    @ElementBy(className = "aui-dialog2")
    private PageElement dialog;

    public TimedQuery<String> getProjectName() {
        return dialog.find(By.className("project-header")).timed().getText();
    }

    public ViewProjectsPage change() {
        dialog.find(By.className("dialog-change-button")).click();
        waitUntilFalse(dialog.timed().isVisible());
        return pageBinder.bind(ViewProjectsPage.class);
    }

    @Override
    public TimedCondition isAt() {
        return Conditions.alwaysTrue();
    }

    @Override
    public String linkId() {
        return "change_type";
    }

    @Override
    public String getUrl() {
        return "";
    }
}
