package com.atlassian.jira.pageobjects.project.roles.adduserdialog;

import com.atlassian.jira.pageobjects.elements.AuiFlag;
import com.atlassian.jira.pageobjects.elements.GlobalFlags;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.components.aui.AuiInlineDialog;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.SelectElement;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;

public class AddUserAndGroupDialog extends AuiInlineDialog {
    private static final String ADD_USER_AND_GROUP_COMPLETED_TRACE_KEY = "add.user.and.group.completed";

    @Inject
    private TraceContext traceContext;

    @ElementBy(id = "roles-role-picker")
    private SelectElement rolePicker;

    @ElementBy(id = "roles-add-usergroup-save-button")
    private PageElement saveButton;

    private GlobalFlags flags;
    private UserGroupPicker actorPicker;

    @Init
    public void init() {
        flags = pageBinder.bind(GlobalFlags.class);
    }

    public AddUserAndGroupDialog(By triggerLocator, String identifier) {
        super(triggerLocator, identifier);
    }

    public void addActorsToRole(long roleId, String... actors) {
        actorPicker = pageBinder.bind(UserGroupPicker.class, "roles-usergroup-picker");

        for (String actor : actors) {
            actorPicker.add(actor);
        }

        rolePicker.select(Options.value(String.valueOf(roleId)));
    }

    public String save() {
        final Tracer checkpoint = traceContext.checkpoint();
        saveButton.click();
        waitUntilFalse(saveButton.timed().isVisible());
        traceContext.waitFor(checkpoint, ADD_USER_AND_GROUP_COMPLETED_TRACE_KEY);

        List<AuiFlag> auiFlags = flags.getFlags();
        AuiFlag resultFlag = auiFlags.get(auiFlags.size() - 1);

        return resultFlag.getMessage();
    }
}
