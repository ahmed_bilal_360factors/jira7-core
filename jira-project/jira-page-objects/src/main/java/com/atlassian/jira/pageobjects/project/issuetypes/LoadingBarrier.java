package com.atlassian.jira.pageobjects.project.issuetypes;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.TimedElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedQuery;

/**
 * Factory for a condition that is false while the issue type panel is loading.
 *
 * @since v6.2
 */
public class LoadingBarrier {
    @ElementBy(id = "project-issuetypes-content-container")
    private PageElement element;

    public TimedQuery<Boolean> notLoading() {
        final TimedElement timedElement = element.timed();
        return Conditions.and(timedElement.isPresent(), Conditions.not(timedElement.hasClass("loading")));
    }

    public void await() {
        Poller.waitUntilTrue("Header did not load within the timeout.", notLoading());
    }
}
