package com.atlassian.jira.pageobjects.pages.admin.user;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.pageobjects.components.UserApplicationAccess;
import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.pageobjects.elements.query.Conditions.and;

/**
 * Page for adding a new user
 *
 * @since v4.4
 */
public class AddUserPage extends AbstractJiraPage {

    private static final String URI = "/secure/admin/user/AddUser!default.jspa";
    private static final By SELECTOR_APPS_LOCATOR = By.cssSelector(".application-picker-applications");
    private static final By SELECTOR_APPS = By.cssSelector(".application-picker-applications input.application");
    private static final By SELECTOR_APPS_SELECTED = By.cssSelector(".application-picker-applications input.application:checked");
    private static final By SELECTOR_APPS_EFFECTIVE = By.cssSelector(".application-picker-applications input.application:indeterminate");
    private static final By SELECTOR_APPS_WARNING = By.cssSelector(".application-warning");

    private final List<String> applicationKeys;

    @ElementBy(id = "user-create")
    private PageElement createForm;

    @ElementBy(name = "username")
    private PageElement username;

    @ElementBy(name = "password")
    private PageElement password;

    @ElementBy(name = "fullname")
    private PageElement fullName;

    @ElementBy(name = "email")
    private PageElement email;

    @ElementBy(name = "sendEmail")
    private PageElement sendEmail;

    @ElementBy(name = "createAnother")
    private PageElement createAnother;

    @ElementBy(id = "user-create-submit")
    private PageElement submit;

    @ElementBy(id = "user-create-cancel")
    private PageElement cancelButton;

    public AddUserPage() {
        this.applicationKeys = ImmutableList.of();
    }

    public AddUserPage(String applicationKey) {
        this.applicationKeys = ImmutableList.of(applicationKey);
    }

    public AddUserPage(String applicationKey, String applicationKey2) {
        this.applicationKeys = ImmutableList.of(applicationKey, applicationKey2);
    }

    public AddUserPage(String applicationKey, String applicationKey2, String applicationKey3) {
        this.applicationKeys = ImmutableList.of(applicationKey, applicationKey2, applicationKey3);
    }

    @Override
    public String getUrl() {
        String params = "";
        if (applicationKeys.size() > 0) {
            params = "?application=" + StringUtils.join(applicationKeys, "&application=");
        }
        return URI + params;
    }

    @Override
    public TimedCondition isAt() {
        return and(username.timed().isPresent(), password.timed().isPresent(), fullName.timed().isPresent());
    }

    public AddUserPage addUser(final String username) {
        return addUser(username, username, username, username + "@example.com", false);
    }

    public AddUserPage addUser(final String username, final String password, final String fullName, final String email,
                               final boolean receiveEmail) {
        this.username.clear().type(username);
        this.password.clear().type(password);
        this.fullName.clear().type(fullName);
        this.email.clear().type(email);
        return setCheckbox(sendEmail, receiveEmail);
    }

    public AbstractJiraPage createUser(boolean createAnother) {
        setCheckbox(this.createAnother, createAnother);
        submit.click();
        if (createAnother) {
            return pageBinder.bind(AddUserPage.class);
        } else {
            return pageBinder.bind(UserBrowserPage.class);
        }
    }

    public UserBrowserPage createUser() {
        return (UserBrowserPage) createUser(false);
    }

    public AddUserPage createAnotherUser() {
        return (AddUserPage) createUser(true);
    }

    public AddUserPage createUserExpectingError() {
        submit.click();
        Poller.waitUntilTrue(elementFinder.find(By.className("error")).timed().isPresent());
        return pageBinder.bind(AddUserPage.class);
    }

    public <T extends Page> T createUser(final Class<T> nextPage, final Object... args) {
        submit.click();
        // TODO https://studio.atlassian.com/browse/JPO-12
        // this is now a dialog so clicking submit results in some JS brain-farting that eventually leads to page re-load
        // in the mean time the page would be bound to the old cached content and.... boooom!
        // as a work-around until this is re-implemented as dialog we re-navigate to the UserBrowser page to make sure
        // the page object gets properly re-bound (-binded?:)
        return pageBinder.navigateToAndBind(nextPage, args);
    }

    /**
     * @return keys as id attributes of error containers and
     * values are error messages
     */
    public Map<String, String> getPageErrors() {
        final HashMap<String, String> errors = Maps.newHashMap();
        for (final PageElement error : createForm.findAll(By.className("error"))) {
            errors.put(error.getAttribute("id"), error.getText());
        }
        return errors;
    }

    public UserBrowserPage cancelCreateUser() {
        cancelButton.click();
        return pageBinder.bind(UserBrowserPage.class);
    }

    private AddUserPage setCheckbox(final PageElement checkbox, final boolean state) {
        if (state) {
            checkbox.select();
        } else if (checkbox.isSelected()) {
            checkbox.toggle();
        }
        return this;
    }

    public Collection<String> getSelectedApplications() {
        return createForm.findAll(SELECTOR_APPS_SELECTED).stream()
                .map(element -> element.getAttribute("value"))
                .collect(CollectorsUtil.toImmutableList());
    }

    public Collection<String> getEffectiveApplications() {
        return createForm.findAll(SELECTOR_APPS_EFFECTIVE).stream()
                .map(element -> element.getAttribute("value"))
                .collect(CollectorsUtil.toImmutableList());
    }

    public List<String> getApplicationsWithWarning() {
        return createForm.findAll(SELECTOR_APPS_WARNING).stream()
                .map(element -> element.getAttribute("data-key"))
                .collect(CollectorsUtil.toImmutableList());
    }

    public AddUserPage setSelectedApplications(final String... appKeys) {
        final ImmutableSet<String> appKeySet = Arrays.stream(appKeys).collect(CollectorsUtil.toImmutableSet());

        createForm.findAll(SELECTOR_APPS)
                .forEach(element -> setCheckbox(element, appKeySet.contains(element.getValue())));

        return this;
    }

    public boolean getSendEmail() {
        return sendEmail.isSelected();
    }

    public boolean getCreateAnother() {
        return createAnother.isSelected();
    }

    public PageElement getEffectiveWarning(String appKey) {
        return elementFinder.find(By.cssSelector(String.format("*[id=\"%s-effective-warning\"]", appKey)));
    }

    public PageElement getEffectiveCriticalWarning(String appKey) {
        return elementFinder.find(By.cssSelector(String.format(".critical-application[id=\"%s-effective-warning\"]", appKey)));
    }

    public UserApplicationAccess getApplicationAccess() {
        return pageBinder.bind(UserApplicationAccess.class, SELECTOR_APPS_LOCATOR);
    }
}
