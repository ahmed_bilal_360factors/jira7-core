package com.atlassian.jira.pageobjects.navigator;

import com.atlassian.jira.pageobjects.components.fields.SingleSelect;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.TimedElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

/**
 * This class represents sections of (project,issueType,parentIssue) that are contained in "chooseContext" page.
 *
 * @since v7.0
 */
public class MoveIssuesContainer {

    @Inject
    PageBinder pageBinder;

    private PageElement pageElement;

    private PageElement projectSelectContainerEl;
    private PageElement issueTypeSelectContainerEl;
    private PageElement parentSelectContainerEl;

    private PageElement targetProjectSelectedEl;
    private PageElement targetParentIssueSelectedEl;

    private SingleSelect projectSelect;
    private SingleSelect issueTypeSelect;
    private SingleSelect parentSelect;

    private String projectFrom;
    private String issueTypeFrom;

    public MoveIssuesContainer(PageElement pageElement) {
        this.pageElement = pageElement;

        this.projectSelectContainerEl = pageElement.find(By.className("project-selector-container"));
        this.issueTypeSelectContainerEl = pageElement.find(By.className("issue-type-selector-container"));
        this.parentSelectContainerEl = pageElement.find(By.className("parent-issue-key-selector-container"));
        this.targetProjectSelectedEl = pageElement.find(By.cssSelector("[data-target-project]"));
        this.targetParentIssueSelectedEl = pageElement.find(By.cssSelector("[data-target-parent-issue]"));

        this.projectFrom = pageElement.find(By.className("from-project-name")).getText();
        this.issueTypeFrom = pageElement.find(By.className("from-issue-type")).getText();
    }

    public void selectProject(final String project) {
        bindSelectProject();
        projectSelect.select(project);
    }

    private void bindSelectProject() {
        Poller.waitUntilTrue("Project select container is not present", projectSelectContainerEl.timed().isPresent());
        projectSelect = pageBinder.bind(SingleSelect.class, projectSelectContainerEl);
    }

    public void selectIssueType(final String issueType) {
        bindIssueTypeSelect();
        issueTypeSelect.select(issueType);
    }

    private void bindIssueTypeSelect() {
        Poller.waitUntilTrue("Issue type select container is not present", issueTypeSelectContainerEl.timed().isPresent());
        issueTypeSelect = pageBinder.bind(SingleSelect.class, issueTypeSelectContainerEl);
    }

    public void selectParentIssue(final String parentIssueKey) {
        bindParentSelect();
        parentSelect.select(parentIssueKey);
    }

    public TimedCondition isParentSelectVisible() {
        return parentSelectContainerEl.timed().isVisible();
    }

    public TimedCondition isParentSelectPresent() {
        return parentSelectContainerEl.timed().isPresent();
    }

    public TimedCondition isProjectSelectVisible() {
        return projectSelectContainerEl.timed().isVisible();
    }

    public TimedCondition isProjectSelectPresent() {
        return projectSelectContainerEl.timed().isPresent();
    }

    public Iterable<String> typeParentIssueAndReturnSuggestions(final String searchKey) {
        bindParentSelect();
        parentSelect.clear();
        parentSelect.type(searchKey);
        TimedQuery<Iterable<String>> timedQuery = parentSelect.getSuggestionsTimed();
        return timedQuery.by(30, TimeUnit.SECONDS);
    }

    public String getParentIssueValue() {
        bindParentSelect();
        return parentSelect.getTimedValue().by(10, TimeUnit.SECONDS);
    }

    private void bindParentSelect() {
        Poller.waitUntilTrue("Parent select container is not present", parentSelectContainerEl.timed().isPresent());
        parentSelect = pageBinder.bind(SingleSelect.class, parentSelectContainerEl);
    }

    public Iterable<String> getAvailableIssueTypeSuggestions() {
        bindIssueTypeSelect();
        return issueTypeSelect.getAvailableSuggestions();
    }

    public String getProjectFrom() {
        return projectFrom;
    }

    public String getIssueTypeFrom() {
        return issueTypeFrom;
    }

    public TimedElement getTargetProjectSelectedEl() {
        return this.targetProjectSelectedEl.timed();
    }

    public String getTargetProjectSelected() {
        return this.targetProjectSelectedEl.timed().getAttribute("data-target-project").now();
    }

    public String getTargetParentIssueSelected() {
        return this.targetParentIssueSelectedEl.timed().getAttribute("data-target-parent-issue").now();
    }

    public String getIssueTypeSelected() {
        bindIssueTypeSelect();
        return issueTypeSelect.getValue();
    }
}