package com.atlassian.jira.pageobjects.project;

import com.atlassian.pageobjects.Page;

/**
 * Represents a project config page with tabs.
 *
 * @since v4.4
 */
public interface ProjectConfigPageTab extends Page {
    String getProjectKey();

    long getProjectId();

    ProjectConfigTabs getTabs();

    /**
     * @deprecated as no more information is stored in the header. Use {@link ProjectConfigPageTab#getProjectSettingsHeader()}
     * if you want to get the new header. Otherwise, if you want project information such as the key, use the backdoor
     * for the project.
     */
    ProjectConfigHeader getProjectHeader();

    ProjectSettingsHeader getProjectSettingsHeader();

    ProjectConfigActions getOperations();
}
