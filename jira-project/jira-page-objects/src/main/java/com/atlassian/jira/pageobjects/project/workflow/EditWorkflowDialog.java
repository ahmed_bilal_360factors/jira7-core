package com.atlassian.jira.pageobjects.project.workflow;

import com.atlassian.jira.pageobjects.pages.admin.workflow.ViewWorkflowSteps;
import com.atlassian.jira.pageobjects.pages.admin.workflow.WorkflowDesignerPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;

/**
 * @since v5.1
 */
public class EditWorkflowDialog {
    @Inject
    private PageBinder binder;

    @Inject
    private PageElementFinder finder;

    private PageElement workflowMigrateDialog;
    private PageElement buttons;
    private PageElement progressBar;
    private PageElement submitButton;

    @Init
    public void initialise() {
        workflowMigrateDialog = finder.find(By.id("wait-migrate-dialog"));
        buttons = workflowMigrateDialog.find(By.className("buttons"));
        progressBar = workflowMigrateDialog.find(By.id("progress-bar-container"));
        submitButton = workflowMigrateDialog.find(By.cssSelector("input[type=submit]"));
    }

    public <T> T clickAndWaitBeforeBind(Class<T> bindingClass, Object... args) {
        submitButton.click();
        waitUntilFalse(workflowMigrateDialog.timed().isPresent());
        return binder.bind(bindingClass, args);
    }

    public WorkflowDesignerPage gotoEditWorkflowDigram(String workflowName) {
        return clickAndWaitBeforeBind(WorkflowDesignerPage.class, workflowName, true);
    }

    public ViewWorkflowSteps gotoEditWorkflowText(String workflowName) {
        return clickAndWaitBeforeBind(ViewWorkflowSteps.class, workflowName, true);
    }

    public boolean isButtonsVisible() {
        return buttons.isVisible();
    }

    public boolean isProgressBarPresent() {
        return progressBar.isPresent();
    }
}
