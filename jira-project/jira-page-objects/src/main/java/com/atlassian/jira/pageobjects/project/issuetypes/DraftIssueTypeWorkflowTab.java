package com.atlassian.jira.pageobjects.project.issuetypes;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import static org.junit.Assert.assertTrue;

/**
 * Represents the Issue Type Workflow tab in draft mode.
 *
 * @since 7.0
 */
public class DraftIssueTypeWorkflowTab extends AbstractIssueTypeWorkflowTab {
    @ElementBy(id = "publish_workflow_edit")
    private PageElement publishButton;

    @ElementBy(id = "discard_workflow_edit")
    private PageElement discardButton;

    public void clickDiscard() {
        assertTrue("discard button should be enabled", discardButton.isEnabled());
        discardButton.click();
    }

    public void clickPublish() {
        assertTrue("publish button should be enabled", publishButton.isEnabled());
        publishButton.click();
    }

    @Override
    String getUrl(String projectKey, long issueTypeId) {
        return String.format("/plugins/servlet/project-config/%s/issuetypes/%d/workflow/edit", projectKey, issueTypeId);
    }

    @Override
    public TimedCondition isAt() {
        return isAt(true);
    }
}
