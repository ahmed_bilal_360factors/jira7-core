package com.atlassian.jira.pageobjects.project.restfultable;

import com.atlassian.jira.pageobjects.components.restfultable.AbstractEditRow;
import org.openqa.selenium.By;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public abstract class AbstractProjectConfigEditForm extends AbstractEditRow {
    protected static final String LOADING_CLASS = "loading";

    public AbstractProjectConfigEditForm(final By rowSelector) {
        super(rowSelector);
    }

    protected void submitAdd() {
        getAddButton().click();
        waitUntilFalse(row.timed().hasClass("loading"));
    }
}
