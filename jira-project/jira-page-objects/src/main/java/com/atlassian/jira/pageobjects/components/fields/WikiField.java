package com.atlassian.jira.pageobjects.components.fields;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import org.openqa.selenium.By;

/**
 * Represents wiki enabled field and provides control over preview mode
 *
 * @since v7.1
 */
public class WikiField {
    private PageElement previewLink;
    private PageElement inputContainer;

    public WikiField(PageElement parent) {
        inputContainer = parent.find(By.className("wiki-edit-content"));
        previewLink = parent.find(By.className("wiki-preview"));
    }

    public void togglePreview() {
        previewLink.click();
    }

    public TimedQuery<Boolean> isInPreviewMode() {
        return inputContainer.timed().hasClass("previewClass");
    }

}
