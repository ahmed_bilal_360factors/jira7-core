package com.atlassian.jira.pageobjects.navigator;

import com.atlassian.jira.pageobjects.dialogs.ShifterDialog;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.AtlassianWebDriver;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.stream.Collectors;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Results on issue navigator. Can be used for either simple or advanced mode.
 *
 * @since v4.4
 */
public class IssueNavigatorResults {

    public static final String ISSUE_ROW_WITH_ISSUEKEY_SELECTOR = "tr[data-issuekey]";
    public static final String ISSUEKEY_ATTRIBUTE = "data-issuekey";

    protected PageElement totalCount;
    protected PageElement issuetable;

    @Inject
    protected PageBinder binder;

    @Inject
    protected PageElementFinder finder;

    @Inject
    protected AtlassianWebDriver webDriver;

    @ElementBy (className = "column-picker-trigger")
    private PageElement columnPickerTrigger;

    @WaitUntil
    public void loaded() {
        waitUntilTrue(finder.find(By.className("navigator-content")).timed().isPresent());
        waitUntilFalse(finder.find(By.cssSelector("navigator-content > div")).timed().hasClass("pending"));
    }

    @Init
    public void getElements() {
        totalCount = finder.find(By.className("results-count-total"));
        issuetable = finder.find(By.id("issuetable"));

    }

    public int getTotalCount() {
        if (!totalCount.isPresent()) {
            return 0;
        } else {
            return Integer.parseInt(totalCount.getText());
        }

    }

    public SelectedIssue getSelectedIssue() {
        return binder.bind(SelectedIssue.class);
    }

    public ShifterDialog openActionsDialog() {
        issuetable.type(".");
        final ShifterDialog theDialog = binder.bind(ShifterDialog.class);
        waitUntilTrue("Issue Actions Dialog Dialog did not open successfully", theDialog.isOpenTimed());
        return theDialog;
    }

    public ShifterDialog openActionsDialog(long issueId) {
        selectIssue(Long.toString(issueId));
        return openActionsDialog();
    }

    public IssueNavigatorResults focus() {
        issuetable.click();
        return this;
    }

    public IssueNavigatorResults nextIssue() {
        issuetable.type("j");
        return this;
    }

    public SelectedIssue selectIssue(String issueKey) {
        // We click the resolution field rather than an arbitrary point in the row as that may mean we click a link and
        // thereby find ourselves on a new page. The resolution field is not a link and is present by default.
        issuetable.find(By.cssSelector("tr[data-issuekey='" + issueKey + "'] td.resolution")).click();
        return getSelectedIssue();
    }

    public Iterable<String> getIssueKeys() {
        return issuetable.findAll(By.cssSelector(ISSUE_ROW_WITH_ISSUEKEY_SELECTOR)).stream()
                .map(el -> el.getAttribute(ISSUEKEY_ATTRIBUTE))
                .collect(Collectors.toList());
    }

    public ColumnsConfigurationDialog getColumnsConfigurationDialog() {
        columnPickerTrigger.click();

        return binder.bind(ColumnsConfigurationDialog.class);
    }
}
