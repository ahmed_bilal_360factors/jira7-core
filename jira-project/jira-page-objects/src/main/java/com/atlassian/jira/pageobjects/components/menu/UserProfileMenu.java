package com.atlassian.jira.pageobjects.components.menu;

import org.openqa.selenium.By;

/**
 * A page object representing the "User Profile" menu that exists in JIRA's application header.
 *
 * @since v7.2.0
 */
public class UserProfileMenu extends JiraAuiDropdownMenu {
    public UserProfileMenu() {
        super(By.id("header-details-user-fullname"), By.id("user-options-content"));
    }
}

