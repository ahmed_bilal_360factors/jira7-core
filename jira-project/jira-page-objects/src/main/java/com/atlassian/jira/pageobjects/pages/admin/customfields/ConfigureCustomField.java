package com.atlassian.jira.pageobjects.pages.admin.customfields;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

/**
 * Represents custom field configuration page
 */
public class ConfigureCustomField extends AbstractJiraPage {
    private final String customFieldId;
    private final Long customFieldIdAsLong;

    @ElementBy(id = "add_new_context")
    private PageElement addContext;

    public ConfigureCustomField(final String customFieldId, final Long customFieldIdAsLong) {
        this.customFieldId = customFieldId;
        this.customFieldIdAsLong = customFieldIdAsLong;
    }

    @Override
    public String getUrl() {
        return "/secure/admin/ConfigureCustomField!default.jspa?customFieldId=" + customFieldIdAsLong;
    }

    @Override
    public TimedCondition isAt() {
        return addContext.timed().isPresent();
    }

    public EditVersionCustomFieldOrder goToEditVersionCustomFieldOrder() {
        PageElement editVersionOrderLink = body.find(By.id(customFieldId + "-edit-versionOrder"));
        Poller.waitUntilTrue(editVersionOrderLink.timed().isPresent());
        editVersionOrderLink.click();
        return pageBinder.bind(EditVersionCustomFieldOrder.class, customFieldId);
    }
}
