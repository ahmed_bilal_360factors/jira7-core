package com.atlassian.jira.pageobjects.pages.viewissue;

import com.atlassian.jira.pageobjects.components.fields.SingleSelect;
import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @since v5.0
 */
public class MoveIssueUpdateFields extends AbstractJiraPage {
    private static String URI = "/secure/MoveIssueUpdateFields.jspa!default.jspa";

    @ElementBy(id = "next_submit")
    private PageElement nextButton;

    @ElementBy(id = "assigneeFieldArea")
    private PageElement newAssigneeField;

    @Inject
    private PageElementFinder elementFinder;

    @Override
    public TimedCondition isAt() {
        return elementFinder.find(By.cssSelector("form[action='MoveIssueUpdateFields.jspa']")).timed().isPresent();
    }

    @Override
    public String getUrl() {
        return URI;
    }

    public MoveIssueUpdateFields() {
        // Do nothing
    }

    public MoveIssueUpdateFields(String issueID) {
        URI += "?id=" + issueID;
    }

    public String getFormErrors() {
        final List<PageElement> formErrors = elementFinder.findAll(By.className("formErrors"));
        return formErrors.stream().map(PageElement::getText).collect(Collectors.joining());
    }

    public MoveIssueUpdateFields setNewAssignee(String assignee) {
        pageBinder.bind(SingleSelect.class, newAssigneeField).select(assignee);
        return this;
    }

    public MoveIssueConfirmation next() {
        nextButton.click();
        return pageBinder.bind(MoveIssueConfirmation.class);
    }

    public MoveIssueUpdateFields expectValidationErrors() {
        nextButton.click();
        return pageBinder.bind(MoveIssueUpdateFields.class);
    }
}
