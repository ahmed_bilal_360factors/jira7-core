package com.atlassian.jira.pageobjects.project.workflow;

import com.atlassian.jira.pageobjects.dialogs.admin.AbstractAddWorkflowToSchemeDialog;

/**
 * @since v5.2
 */
public class AddWorkflowDialog extends AbstractAddWorkflowToSchemeDialog<AddWorkflowDialog> {
    public AssignIssueTypesDialog next() {
        clickNext();
        return binder.bind(AssignIssueTypesDialog.class);
    }

    @Override
    protected AddWorkflowDialog getThis() {
        return this;
    }
}
