package com.atlassian.jira.pageobjects.project.summary;

import com.atlassian.jira.pageobjects.components.fields.SingleSelect;
import com.atlassian.jira.pageobjects.dialogs.AvatarDialog;
import com.atlassian.jira.pageobjects.project.AbstractProjectConfigPageTab;
import com.atlassian.jira.pageobjects.project.ProjectConfigPageTab;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.google.common.collect.Maps;
import org.hamcrest.Matcher;
import org.hamcrest.core.IsEqual;
import org.openqa.selenium.By;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Edit project page accessed in the project administration sidebar under Details
 *
 * @since v6.3
 */
public class EditProjectPageTab extends AbstractProjectConfigPageTab implements ProjectConfigPageTab {

    @ElementBy(id = "project-edit")
    private PageElement editProjectForm;

    @ElementBy(id = "project-edit-submit")
    private PageElement updateButton;

    @ElementBy(id = "project-edit-key")
    private PageElement projectKey;

    @ElementBy(id = "edit-project-key-toggle")
    private PageElement editLink;

    @ElementBy(id = "edit-project-warning-message")
    private PageElement warningMessage;

    @ElementBy(id = "view_reindex_project")
    private PageElement reindexProjectWebItem;

    private SingleSelect projectTypeSingleSelect;
    private SingleSelect projectCategorySingleSelect;

    private PageElement projectName;
    private PageElement projectUrl;
    private PageElement projectDescription;

    private PageElement errorMessage;
    private PageElement avatarImage;


    private static final String URI_TEMPLATE = "/secure/project/EditProject!default.jspa?pid=%s";

    private final String uri;


    public EditProjectPageTab(final String projectId) {
        this.uri = getUrlForProject(projectId);
    }

    @Init
    public void initialize() {
        projectName = editProjectForm.find(getProjectNameLocator());
        projectUrl = editProjectForm.find(getProjectUrlLocator());
        projectDescription = editProjectForm.find(getProjectDescriptionLocator());
        updateButton = editProjectForm.find(getUpdateLocator());
        avatarImage = editProjectForm.find(getProjectAvatarLocator());
        errorMessage = editProjectForm.find(getErrorMessageLocator());
        projectTypeSingleSelect = pageBinder.bind(SingleSelect.class, editProjectForm.find(getProjectTypeSingleSelectLocator()));
        projectCategorySingleSelect = pageBinder.bind(SingleSelect.class, editProjectForm.find(getProjectCategorySingleSelectLocator()));
    }

    @Override
    public TimedCondition isAt() {
        return editProjectForm.timed().isPresent();
    }

    @Override
    public String getUrl() {
        if (uri == null) {
            throw new IllegalStateException("Use the constructor with the project key argument.");
        }
        return uri;
    }

    public EditProjectPageTab setProjectName(final String newProjectName) {
        projectName.clear().type(newProjectName);
        return this;
    }

    public TimedCondition isProjectKeyVisible() {
        return projectKey.timed().isVisible();
    }

    public EditProjectPageTab setProjectKey(final String newProjectKey) {
        projectKey.clear().type(newProjectKey);
        return this;
    }

    public boolean isProjectTypeDisabled() {
        return projectTypeSingleSelect.isDisabled();
    }

    public boolean isProjectCategoryDisabled() {
        return projectCategorySingleSelect.isDisabled();
    }

    public String getProjectType() {
        return projectTypeSingleSelect.getValue();
    }

    public Iterable<String> getProjectTypeSuggestions() {
        return projectTypeSingleSelect.getSuggestionsTimed().by(5, TimeUnit.SECONDS);
    }

    public EditProjectPageTab setProjectType(String value) {
        projectTypeSingleSelect.select(value);
        return this;
    }

    public EditProjectPageTab setProjectCategory(String value) {
        projectCategorySingleSelect.select(value);
        return this;
    }

    public TimedQuery<String> getEditProjectKey() {
        return projectKey.timed().getValue();
    }

    public TimedQuery<String> getProjectName() {
        return projectName.timed().getValue();
    }

    public TimedQuery<String> getProjectDescription() {
        return projectDescription.timed().getValue();
    }

    public EditProjectPageTab setProjectUrl(final String url) {
        projectUrl.clear().type(url);
        return this;
    }

    public TimedQuery<String> getProjectUrl() {
        return projectUrl.timed().getValue();
    }

    /**
     * Select an avatar from the avatar picker, based on its database ID.
     *
     * @return the edit project dialog
     */
    public EditProjectPageTab setAvatar(final String id) {
        avatarImage.click();
        pageBinder.bind(AvatarDialog.class, "project-avatar-picker").setAvatar(id);
        Poller.waitUntilTrue(editProjectForm.timed().isVisible());
        return this;
    }

    /**
     * Select an avatar from the avatar picker, based on its position in the list of available avatars.
     *
     * @param number from 1 to N, where N is the (expected?) number of options. There are ~16 system avatars.
     * @return the edit project dialog
     */
    public EditProjectPageTab chooseAvatarOption(final String number) {
        avatarImage.click();
        pageBinder.bind(AvatarDialog.class, "project-avatar-picker").setAvatar(number);
        Poller.waitUntilTrue(editProjectForm.timed().isVisible());
        return this;
    }

    public EditProjectPageTab setDescription(final String description) {
        projectDescription.clear().type(description);
        return this;
    }

    public void submit() {
        updateButton.click();
    }

    private static By getUpdateLocator() {
        return By.id("project-edit-submit");
    }

    private static By getProjectNameLocator() {
        return By.name("name");
    }

    private static By getProjectUrlLocator() {
        return By.name("url");
    }

    private static By getProjectDescriptionLocator() {
        return By.name("description");
    }

    private static By getProjectAvatarLocator() {
        return By.className("jira-avatar-picker-trigger");
    }

    private static By getErrorMessageLocator() {
        return By.cssSelector(".aui-message.error");
    }

    private static By getProjectTypeSingleSelectLocator() {
        return By.id("project-type-container");
    }

    private static By getProjectCategorySingleSelectLocator() {
        return By.id("project-category-container");
    }

    public String getErrorMessage() {
        return errorMessage.getText().trim();
    }

    public Map<String, String> getFormErrors() {
        Map<String, String> errors = Maps.newLinkedHashMap();
        List<PageElement> errorNodes = editProjectForm.findAll(By.cssSelector("div.error"));

        for (PageElement errorNode : errorNodes) {
            PageElement input = errorNode.find(By.xpath("preceding-sibling::input"));
            if (!input.isPresent()) {
                input = errorNode.find(By.xpath("preceding-sibling::select"));
            }


            errors.put(input.getAttribute("name"), errorNode.getText().trim());
        }

        return errors;
    }

    public TimedQuery<Boolean> isErrorMessagePresent() {
        return errorMessage.timed().isVisible();
    }

    public TimedQuery<Boolean> isProjectKeyDisabled() {
        return Conditions.not(projectKey.timed().isEnabled());
    }

    public TimedQuery<Boolean> isWarningDisplayed() {
        return warningMessage.timed().isVisible();
    }

    public TimedQuery<Boolean> isReindexLinkVisible() {
        return reindexProjectWebItem.timed().isVisible();
    }

    @SuppressWarnings("unchecked")
    private Matcher<Boolean> equalTo(final boolean enabled) {
        return (Matcher<Boolean>) IsEqual.equalTo(enabled);
    }

    public static String getUrlForProject(String projectId) {
        return String.format(URI_TEMPLATE, projectId);
    }
}
