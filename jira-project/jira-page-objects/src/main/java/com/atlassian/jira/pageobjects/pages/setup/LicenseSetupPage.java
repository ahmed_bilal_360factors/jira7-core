package com.atlassian.jira.pageobjects.pages.setup;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static java.util.concurrent.TimeUnit.MINUTES;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Step 3 in the JIRA setup process - license.
 *
 * @since v5.2
 */
public class LicenseSetupPage extends AbstractJiraPage {
    @ElementBy(cssSelector = "#jira-setupwizard h2")
    private PageElement formTitle;

    @FindBy(id = "jira-setupwizard-licenseSetupSelectorexistingLicense")
    private WebElement existingLicense;

    private PageElement submitButton;


    @Override
    public String getUrl() {
        throw new UnsupportedOperationException("You can't go to this page by entering URI");
    }

    @Override
    public TimedCondition isAt() {
        return formTitlePresent();
    }

    private TimedCondition formTitlePresent() {
        return Conditions.forMatcher(formTitle.timed().getText(), equalTo("Specify your license key"));
    }

    public LicenseSetupPage selectExistingLicense(final String licenseKey)  {
        final PageElement licenseKeyField = elementFinder.find(By.id("licenseKey"));

        // test that the my.atlassian.com button is there and that it has all the important stuff right
        final PageElement macRedirectButton = elementFinder.find(By.id("generate-mac-license"));
        final String hyperlink = macRedirectButton.getAttribute("href");
        checkMacRedirectLink(hyperlink);

        submitButton = elementFinder.find(By.cssSelector(".aui-button-primary"));
        licenseKeyField.type("");
        licenseKeyField.type(licenseKey);
        return this;
    }

    private void checkMacRedirectLink(final String hyperlink) {
        final URI uri;
        try {
            uri = new URI(hyperlink);
            final List<NameValuePair> params = URLEncodedUtils.parse(uri, "UTF-8");
            assertThat(getValueForKey(params, "callback"), endsWith("/secure/SetupLicense!default.jspa"));
            assertThat(getValueForKey(params, "licensefieldname"), equalTo("setupLicenseKey"));
        } catch (final URISyntaxException e) {
            throw new AssertionError("m.a.c redirect link is not present or malformed on setup license page.", e);
        }
    }

    private String getValueForKey(final List<NameValuePair> params, final String callback) {
        return params.stream()
                .filter(param -> param.getName().equalsIgnoreCase(callback))
                .findAny()
                .orElseThrow(() -> new AssertionError("m.a.c link does not contain parameter '" + callback + "'"))
                .getValue();
    }

    public AdminSetupPage submit() {
        submitButton.click();
        //waiting for plugin system to restart
        Poller.waitUntil(isAt(), is(false), Poller.by(5, MINUTES));
        return pageBinder.bind(AdminSetupPage.class);
    }

    public MailSetupPage submitToMailSetup() {
        submitButton.click();
        return pageBinder.bind(MailSetupPage.class);
    }
}
