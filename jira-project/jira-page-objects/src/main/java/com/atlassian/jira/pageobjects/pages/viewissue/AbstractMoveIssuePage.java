package com.atlassian.jira.pageobjects.pages.viewissue;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;

abstract class AbstractMoveIssuePage extends AbstractJiraPage {
    private static final String URI = "/secure/MoveIssue!default.jspa";

    private final String issueKey;

    public AbstractMoveIssuePage(String issueKey) {
        this.issueKey = issueKey;
    }

    @Override
    public String getUrl() {
        return URI + "?key=" + issueKey;
    }
}
