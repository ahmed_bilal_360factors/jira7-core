package com.atlassian.jira.pageobjects.config.junit4.rule;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.functest.framework.util.junit.AnnotatedDescription;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.EnableNPS;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;
import javax.inject.Inject;

/**
 * Disables the NPS plugin by default in our tests.
 *
 * Any test that wants or needs the NPS plugin will need to
 * manually re-enable it by adding an {@link EnableNPS} annotation to
 * their test class or method.
 *
 * @since v7.2
 */
public class NPSRule extends TestWatcher {

    private static final String NPS_PLUGIN_KEY = "com.atlassian.plugins.atlassian-nps-plugin";

    private boolean wasDisabled = false;

    @Inject
    private JiraTestedProduct jira;

    @Override
    protected void starting(@Nonnull final Description description) {
        if (shouldEnable(description)) {
            enableNPS();
        } else {
            disableNPS();
        }
    }

    @Override
    protected void finished(final Description description) {
        if (wasDisabled) {
            enableNPS();
        }
    }

    private boolean shouldEnable(final Description description) {
        final AnnotatedDescription annotatedDescription = new AnnotatedDescription(description);
        return annotatedDescription.isMethodAnnotated(EnableNPS.class);
    }

    private void enableNPS() {
        getBackdoor().plugins().enablePlugin(NPS_PLUGIN_KEY);
        wasDisabled = false;
    }

    private void disableNPS() {
        getBackdoor().plugins().disablePlugin(NPS_PLUGIN_KEY);
        wasDisabled = true;
    }

    private Backdoor getBackdoor() {
        return jira.backdoor();
    }
}
