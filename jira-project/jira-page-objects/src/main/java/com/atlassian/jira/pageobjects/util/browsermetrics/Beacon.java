package com.atlassian.jira.pageobjects.util.browsermetrics;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

/**
 * A browser-metrics beacon for a transition.
 *
 * Due to JIRA using Firefox 23 for Webdriver tests, the following attributes are not supported:
 *
 * - `report.resourceTiming`
 * - `report.marks`
 * - `report.measures`
 *
 * @since v7.2.0
 */
public class Beacon {
    static final Beacon FROM_MAP(Map<String, Object> beacon) {
        Report report = Report.FROM_MAP((Map)beacon.get("report"));
        return new Beacon(report);
    };

    final public Report report;

    Beacon(Report report) {
        this.report = report;
    }

    public static class Report {
        public final Double apdex;
        public final Optional<String> applicationHash;
        public final Optional<Double> connectEnd;
        public final Optional<Double> connectStart;
        public final Optional<String> correlationId;
        public final Optional<Double> dbConnsTimeInMs;
        public final Optional<Double> dbReadsTimeInMs;
        public final Optional<Double> domComplete;
        public final Optional<Double> domContentLoadedEventEnd;
        public final Optional<Double> domContentLoadedEventStart;
        public final Optional<Double> domInteractive;
        public final Optional<Double> domLoading;
        public final Optional<Double> domainLookupEnd;
        public final Optional<Double> domainLookupStart;
        public final List<String> experiments;
        public final Optional<Double>fetchStart;
        public final Optional<Double>firstPaint;
        public final Boolean isInitial;
        public final String journeyId;
        public final String key;
        public final Optional<Double> loadEventEnd;
        public final Optional<Double> loadEventStart;
        public final Optional<String> manifestoHash;
        public final Optional<NavigationType> navigationType;
        public final Optional<String> rack;
        public final Double readyForUser;
        public final Optional<Integer> redirectCount;
        public final Optional<Double> requestStart;
        public final Optional<Double> resourceLoadedEnd;
        public final Optional<Double> responseEnd;
        public final Optional<Double> responseStart;
        public final Optional<Double> serverDuration;
        public final Double threshold;
        public final Optional<Double> unloadEventEnd;
        public final Optional<Double> unloadEventStart;
        public final String userAgent;

        static final Report FROM_MAP(Map<String, Object> report) {
            return new Report(
                    maybeDouble(report, "apdex").get(),
                    maybeString(report, "applicationHash"),
                    maybeDouble(report, "connectEnd"),
                    maybeDouble(report, "connectStart"),
                    maybeString(report, "correlationId"),
                    maybeDouble(report, "dbConnsTimeInMs"),
                    maybeDouble(report, "dbReadsTimeInMs"),
                    maybeDouble(report, "domComplete"),
                    maybeDouble(report, "domContentLoadedEventEnd"),
                    maybeDouble(report, "domContentLoadedEventStart"),
                    maybeDouble(report, "domInteractive"),
                    maybeDouble(report, "domLoading"),
                    maybeDouble(report, "domainLookupEnd"),
                    maybeDouble(report, "domainLookupStart"),
                    (List<String>)report.get("experiments"),
                    maybeDouble(report, "fetchStart"),
                    maybeDouble(report, "firstPaint"),
                    (Boolean)report.get("isInitial"),
                    (String)report.get("journeyId"),
                    (String)report.get("key"),
                    maybeDouble(report, "loadEventEnd"),
                    maybeDouble(report, "loadEventStart"),
                    maybeString(report, "manifestoHash"),
                    maybeDouble(report, "navigationType")
                        .map(d -> d.intValue())
                        .flatMap(NavigationType::decode),
                    maybeString(report, "rack"),
                    maybeDouble(report, "readyForUser").get(),
                    maybeDouble(report, "redirectCount").map(d -> d.intValue()),
                    maybeDouble(report, "requestStart"),
                    maybeDouble(report, "resourceLoadedEnd"),
                    maybeDouble(report, "responseEnd"),
                    maybeDouble(report, "responseStart"),
                    maybeDouble(report, "serverDuration"),
                    maybeDouble(report, "threshold").get(),
                    maybeDouble(report, "unloadEventEnd"),
                    maybeDouble(report, "unloadEventStart"),
                    (String)report.get("userAgent"));
        };

        Report(Double apdex,
               Optional<String> applicationHash,
               Optional<Double> connectEnd,
               Optional<Double> connectStart,
               Optional<String> correlationId,
               Optional<Double> dbConnsTimeInMs,
               Optional<Double> dbReadsTimeInMs,
               Optional<Double> domComplete,
               Optional<Double> domContentLoadedEventEnd,
               Optional<Double> domContentLoadedEventStart,
               Optional<Double> domInteractive,
               Optional<Double> domLoading,
               Optional<Double> domainLookupEnd,
               Optional<Double> domainLookupStart,
               List<String> experiments,
               Optional<Double> fetchStart,
               Optional<Double> firstPaint,
               Boolean isInitial,
               String journeyId,
               String key,
               Optional<Double> loadEventEnd,
               Optional<Double> loadEventStart,
               Optional<String> manifestoHash,
               Optional<NavigationType> navigationType,
               Optional<String> rack,
               Double readyForUser,
               Optional<Integer> redirectCount,
               Optional<Double> requestStart,
               Optional<Double> resourceLoadedEnd,
               Optional<Double> responseEnd,
               Optional<Double> responseStart,
               Optional<Double> serverDuration,
               Double threshold,
               Optional<Double> unloadEventEnd,
               Optional<Double> unloadEventStart,
               String userAgent) {
            this.apdex = apdex;
            this.applicationHash = applicationHash;
            this.connectEnd = connectEnd;
            this.connectStart = connectStart;
            this.correlationId = correlationId;
            this.dbConnsTimeInMs = dbConnsTimeInMs;
            this.dbReadsTimeInMs = dbReadsTimeInMs;
            this.domComplete = domComplete;
            this.domContentLoadedEventEnd = domContentLoadedEventEnd;
            this.domContentLoadedEventStart = domContentLoadedEventStart;
            this.domInteractive = domInteractive;
            this.domLoading = domLoading;
            this.domainLookupEnd = domainLookupEnd;
            this.domainLookupStart = domainLookupStart;
            this.experiments = experiments;
            this.fetchStart = fetchStart;
            this.firstPaint = firstPaint;
            this.isInitial = isInitial;
            this.journeyId = journeyId;
            this.key = key;
            this.loadEventEnd = loadEventEnd;
            this.loadEventStart = loadEventStart;
            this.manifestoHash = manifestoHash;
            this.navigationType = navigationType;
            this.rack = rack;
            this.readyForUser = readyForUser;
            this.redirectCount = redirectCount;
            this.requestStart = requestStart;
            this.resourceLoadedEnd = resourceLoadedEnd;
            this.responseEnd = responseEnd;
            this.responseStart = responseStart;
            this.serverDuration = serverDuration;
            this.threshold = threshold;
            this.unloadEventEnd = unloadEventEnd;
            this.unloadEventStart = unloadEventStart;
            this.userAgent = userAgent;
        }
    }

    public enum NavigationType {
        NAVIGATENEXT,
        RELOAD,
        BACK_FORWARD,
        UNDEFINED;

        public static Optional<NavigationType> decode(Integer value) {
            switch (value) {
                case 0:
                    return of(NAVIGATENEXT);
                case 1:
                    return of(RELOAD);
                case 2:
                    return of(BACK_FORWARD);
                case 255:
                    return of(UNDEFINED);
            }
            return empty();
        }
    }

    private static Optional<Double> maybeDouble(Map<String, Object> report, String key) {
        Object value = report.get(key);
        return ofNullable(value).map(Beacon::asDouble);
    }

    private static Optional<String> maybeString(Map<String, Object> report, String key) {
        return ofNullable((String)report.get(key));
    }

    private static Double asDouble(Object value) {
        if (value instanceof Double) {
            return (Double)value;
        } else if (value instanceof Long) {
            return ((Long)value).doubleValue();
        }
        return 0.0;
    }
}
