package com.atlassian.jira.pageobjects.pages.viewissue.attachment;

import com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock.AttachmentBlockSetting;
import com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock.Sort;
import com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock.ViewMode;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.common.collect.ImmutableList;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.Arrays;

/**
 * Attachment options dropdown
 *
 * @since v6.4
 */
public class AttachmentOptions {
    @Inject
    PageElementFinder elementFinder;

    PageElement sortingOptions;
    PageElement sortingOrderOptions;
    PageElement viewModeOptions;

    @Init
    public void init() {
        sortingOptions = elementFinder.find(By.id("attachment-sorting-options"));
        sortingOrderOptions = elementFinder.find(By.id("attachment-sorting-order-options"));
        viewModeOptions = elementFinder.find(By.id("attachment-view-mode-options"));
    }

    public ViewMode getViewMode() {
        return getBlockSetting(ViewMode.values(), viewModeOptions);
    }

    public Sort.Key getSortKey() {
        return getBlockSetting(Sort.Key.values(), sortingOptions);
    }

    public Sort.Direction getSortOrder() {
        return getBlockSetting(Sort.Direction.values(), sortingOrderOptions);
    }

    private <T extends AttachmentBlockSetting> T getBlockSetting(final T[] sortValues, final PageElement parentElement) {
        return ImmutableList.<T>copyOf(sortValues)
                .stream()
                .filter(sortSetting -> parentElement.find(By.id(sortSetting.getLinkId())).hasClass("aui-checked"))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(String.format("%s doesn't contain any of %s", parentElement.toString(), Arrays.toString(sortValues))));
    }

    public void setViewMode(ViewMode viewMode) {
        ViewMode currentViewMode = getViewMode();

        if (currentViewMode.equals(viewMode)) {
            return;
        }

        viewModeOptions.find(By.id(viewMode.getLinkId())).click();
        Poller.waitUntilFalse(String.format("Attachment list in old view mode(%s) is gone.", currentViewMode.toString()), elementFinder.find(By.cssSelector(currentViewMode.getContainerSelector())).timed().isVisible());
        Poller.waitUntilTrue(String.format("Attachment list in new view mode(%s) is visible.", viewMode.toString()), elementFinder.find(By.cssSelector(viewMode.getContainerSelector())).timed().isVisible());
    }

    /**
     * @deprecated Use {@link #setViewMode(com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock.ViewMode)}
     */
    @Deprecated
    public void switchToThumbnails() {
        setViewMode(ViewMode.GALLERY);
    }

    /**
     * @deprecated Use {@link #setViewMode(com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock.ViewMode)}
     */
    @Deprecated
    public void switchToList() {
        setViewMode(ViewMode.LIST);
    }

    public void setSortBy(final Sort.Key sortKey) {
        if (getSortKey().equals(sortKey)) {
            return;
        }

        sortingOptions.find(By.id(sortKey.getLinkId())).click();
        Poller.waitUntilFalse("Sorting options are gone", sortingOptions.timed().isVisible());
        Poller.waitUntilTrue(String.format("Attachment list with new sort key(%s) is again visible.", sortKey.toString()), elementFinder.find(By.cssSelector(sortKey.getContainerSelector())).timed().isVisible());
    }

    public void setSortOrder(final Sort.Direction sortOrder) {
        if (getSortOrder().equals(sortOrder)) {
            return;
        }

        sortingOrderOptions.find(By.id(sortOrder.getLinkId())).click();
        Poller.waitUntilFalse("Sorting order options are gone", sortingOptions.timed().isVisible());
        Poller.waitUntilTrue(String.format("Attachment list with new sort order(%s) is again visible.", sortOrder.toString()), elementFinder.find(By.cssSelector(sortOrder.getContainerSelector())).timed().isVisible());
    }
}
