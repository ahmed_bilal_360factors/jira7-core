package com.atlassian.jira.pageobjects.elements;

import com.atlassian.pageobjects.elements.PageElement;

/**
 * Any HTML image.
 *
 * @since v6.4
 */
public class ImageElement {
    private final PageElement element;

    public ImageElement(final PageElement element) {
        this.element = element;
    }

    public String getAlternativeText() {
        return element.getAttribute("title");
    }

    public String getClassName() {
        final String className = element.getAttribute("class");
        return className.substring(className.lastIndexOf(' ') + 1);
    }
}
