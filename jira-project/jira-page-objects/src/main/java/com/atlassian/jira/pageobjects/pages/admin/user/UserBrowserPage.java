package com.atlassian.jira.pageobjects.pages.admin.user;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.pageobjects.components.DropDown;
import com.atlassian.jira.pageobjects.components.fields.SingleSelect;
import com.atlassian.jira.pageobjects.framework.elements.ExtendedElementFinder;
import com.atlassian.jira.pageobjects.framework.elements.PageElements;
import com.atlassian.jira.pageobjects.global.User;
import com.atlassian.jira.pageobjects.pages.AbstractJiraAdminPage;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.SelectElement;
import com.atlassian.pageobjects.elements.TimedElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Queries;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterables;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.atlassian.jira.pageobjects.framework.elements.PageElements.transformTimed;
import static com.atlassian.jira.pageobjects.framework.util.JiraLocators.byCellType;
import static com.atlassian.webdriver.utils.by.ByDataAttribute.byTagAndData;

/**
 * Admin user browser.
 *
 * @since v4.4
 */
public class UserBrowserPage extends AbstractJiraAdminPage {
    public static String FILTER_ANY_APPLICATION_ACCESS = "-1";
    public static String FILTER_NO_APPLICATION_ACCESS = "-2";
    private static final String MAX = "1000000";
    private static final String TEN = "10";
    private static final String URI = "/secure/admin/user/UserBrowser.jspa";

    private final String filteredApplicationKey;

    @Inject
    private TraceContext traceContext;

    @Inject
    protected ExtendedElementFinder extendedFinder;


    @FindBy(id = "user-filter-submit")
    protected WebElement filterSubmit;

    @ElementBy(id = "create_user")
    protected PageElement addUserLink;

    @ElementBy(className = "results-count-total")
    protected PageElement numUsers;

    @ElementBy(cssSelector = ".count-pagination > .pagination")
    protected PageElement pagination;

    @ElementBy(id = "user_browser_table")
    protected PageElement userTable;

    @FindBy(name = "userSearchFilter")
    protected WebElement filterUsersByUserSearch;

    @ElementBy(name = "activeFilter")
    protected SelectElement activeUsersSelect;

    @ElementBy(name = "applicationFilter")
    protected SelectElement filterApplicationsSelect;

    @ElementBy(name = "max")
    protected SelectElement filterUsersPerPage;

    @ElementBy(className = "user-row", within = "userTable")
    protected Iterable<PageElement> userRows;

    protected SingleSelect groupFilter;

    public UserBrowserPage() {
        filteredApplicationKey = null;
    }

    public UserBrowserPage(final String filteredApplicationKey) {
        this.filteredApplicationKey = filteredApplicationKey;
    }

    @Init
    public void init() {
        groupFilter = pageBinder.bind(SingleSelect.class, elementFinder.find(By.className("user-group-filter")));
    }

    public String getUrl() {
        if (StringUtils.isEmpty(filteredApplicationKey)) {
            return URI;
        } else {
            return URI + "?applicationFilter=" + filteredApplicationKey;
        }
    }

    @Override
    public TimedCondition isAt() {
        return elementFinder.find(By.id("user_browser_table")).timed().isPresent();
    }

    /**
     * When editing a users groups from this page, EditUserGroups always returns back to
     * UserBrowser unless there was an error.
     *
     * @param user user to edit
     * @return edit user groups page instance
     * @deprecated use {@link #findRow(com.atlassian.jira.pageobjects.global.User)} instead
     */
    public EditUserGroupsPage editUserGroups(User user) {
        if (hasRow(user)) {
            return findRow(user).editGroups();
        } else {
            throw new IllegalStateException("User: " + user.getUserName() + " was not found.");
        }
    }

    /**
     * @param user user
     * @return view page
     * @deprecated use {@link #findRow(com.atlassian.jira.pageobjects.global.User)} instead
     */
    @Deprecated
    public ViewUserPage gotoViewUserPage(User user) {
        if (hasRow(user)) {
            return findRow(user).view();
        } else {
            throw new IllegalStateException("The user: " + user + " was not found on the page");
        }
    }

    public int getNumberOfUsers() {
        return Integer.valueOf(numUsers.getText());
    }

    public UserBrowserPage gotoResultPage(int page) {
        pagination.find(By.linkText(String.valueOf(page))).click();

        return pageBinder.bind(UserBrowserPage.class);
    }

    public TimedQuery<String> getPaginationText() {
        return pagination.timed().getText();
    }

    /**
     * Navigates to the addUserPage by activating the add User link
     *
     * @return add user page
     */
    public AddUserPage gotoAddUserPage() {
        addUserLink.click();
        return pageBinder.bind(AddUserPage.class);
    }

    /**
     * Takes User object, fills out the addUserPage form and creates the user.
     *
     * @param user              the user to create
     * @param sendPasswordEmail sets the send email tick box to on or off
     * @return the user browser page which should have the new user added to the count.
     */
    public UserBrowserPage addUser(User user, boolean sendPasswordEmail) {
        AddUserPage addUserPage = gotoAddUserPage();
        return addUserPage.addUser(user.getUserName(), user.getPassword(), user.getFullName(), user.getEmail(), sendPasswordEmail).createUser();
    }

    public UserBrowserPage filterByUserSearch(String usersearch) {
        filterUsersByUserSearch.sendKeys(usersearch);
        return submitForm();
    }

    public UserBrowserPage clearFilterByUserSearch() {
        filterUsersByUserSearch.clear();
        return submitForm();
    }

    public TimedCondition isUserCreatedFlagVisible(String username) {
        final TimedElement flagElement = elementFinder.find(By.className("user-created-flag-single")).timed();
        return Conditions.and(flagElement.isVisible(), Conditions.isEqual(username, flagElement.getAttribute("data-user-created")));
    }

    public TimedCondition isMultipleUsersCreatedFlagVisible() {
        return elementFinder.find(By.className("user-created-flag-multiple")).timed().isVisible();
    }

    /**
     * @deprecated Not available in UI anymore, does not filter by username only, internally calls {@link #filterByUserSearch}
     */
    @Deprecated
    public UserBrowserPage filterByUserName(String username) {
        return filterByUserSearch(username);
    }

    /**
     * @deprecated Not available in UI anymore, does not filter by username only, internally calls {@link #filterByEmail}
     */
    @Deprecated
    public UserBrowserPage filterByEmail(String email) {
        return filterByUserSearch(email);
    }

    public UserBrowserPage setUserFilterToShowAllUsers() {
        filterUsersPerPage.select(Options.value(MAX));
        return submitForm();
    }

    public UserBrowserPage filterByApplication(String application) {
        filterApplicationsSelect.select(Options.value(application));
        return submitForm();
    }

    public UserBrowserPage filterByActive(String active) {
        activeUsersSelect.select(Options.value(active));
        return submitForm();
    }

    public UserBrowserPage filterByGroup(String groupName) {
        groupFilter.select(groupName);
        return submitForm();
    }

    public String filteredApplication() {
        return filterApplicationsSelect.getSelected().value();
    }

    public String getApplicationFilter() {
        return filterApplicationsSelect.getSelected().value();
    }

    public UserBrowserPage setUserFilterTo10Users() {
        filterUsersPerPage.select(Options.value(TEN));
        return submitForm();
    }

    private UserBrowserPage submitForm() {
        filterSubmit.click();

        return pageBinder.bind(UserBrowserPage.class, filteredApplicationKey);
    }

    // NEW API meant to replace most of the above - using the row pattern

    public Iterable<UserRow> getUserRows() {
        return Iterables.transform(userRows, PageElements.bind(pageBinder, UserRow.class));
    }

    public List<String> getUserNames() {
        return getUserNames(false);
    }

    public List<String> getUserNames(boolean focused) {
        return StreamSupport.stream(getUserRows().spliterator(), false)
                .filter(focused ? UserRow::isFocused : row -> true)
                .map(UserRow::getUsername)
                .collect(CollectorsUtil.toImmutableList());
    }

    public TimedQuery<Iterable<UserRow>> getUserRowsTimed() {
        return transformTimed(timeouts, pageBinder,
                extendedFinder.within(userTable).newQuery(By.className("user-row")).supplier(),
                UserRow.class);
    }

    public boolean hasRow(User user) {
        return userTable.find(byTagAndData(PageElements.TR, "user", user.getUserName())).isPresent();
    }

    /**
     * Find row for given user
     *
     * @param user user to find
     * @return user row
     * @throws java.util.NoSuchElementException if no user row found for given user (use
     *                                          {@link #hasRow(com.atlassian.jira.pageobjects.global.User)} to check)
     */
    public UserRow findRow(final User user) {
        return findRow(user.getUserName());
    }

    /**
     * Find row for given userName
     *
     * @param userName user to find
     * @return user row
     * @throws java.util.NoSuchElementException if no user row found for given user (use
     *                                          {@link #hasRow(com.atlassian.jira.pageobjects.global.User)} to check)
     */
    public UserRow findRow(final String userName) {
        return Iterables.find(getUserRows(), new Predicate<UserRow>() {
            @Override
            public boolean apply(@Nullable UserRow input) {
                return input.getUsername().equals(userName);
            }
        });
    }

    @Override
    public String linkId() {
        return "user_browser";
    }

    public TimedCondition hasApplicationFilter() {
        return filterApplicationsSelect.timed().isVisible();
    }


    /**
     * Encapsulates single row in the user browser.
     */
    public static final class UserRow {
        @Inject
        private ExtendedElementFinder extendedFinder;
        @Inject
        private PageBinder pageBinder;
        @Inject
        private Timeouts timeouts;

        private final PageElement rowElement;
        private final PageElement usernameLink;
        private final PageElement loginDetailsCell;
        private final PageElement operationsCell;
        private final PageElement userDirectory;
        private final PageElement fullName;
        private final List<PageElement> userGroupLinks;
        private final PageElement applicationRolesCells;


        public UserRow(PageElement rowElement) {
            this.rowElement = rowElement;
            usernameLink = rowElement.find(byCellType("username")).find(By.className("username"));
            fullName = rowElement.find(byCellType("fullname"));
            loginDetailsCell = rowElement.find(byCellType("login-details"));
            operationsCell = rowElement.find(byCellType("operations"));
            userDirectory = rowElement.find(byCellType("user-directory"));
            userGroupLinks = rowElement.find(byCellType("user-groups")).findAll(By.tagName("a"));
            applicationRolesCells = rowElement.find(byCellType("user-application-roles"));
        }

        public boolean isFocused() {
            return "true".equals(rowElement.getAttribute("data-focused"));
        }

        public String getUsername() {
            return usernameLink.getText();
        }

        public boolean canResetLoginCount() {
            return getResetLoginCountButton().isPresent();
        }

        public UserBrowserPage resetLoginCount() {
            // doesn't work
            getResetLoginCountButton().click();
            return pageBinder.bind(UserBrowserPage.class);
        }

        public ViewUserPage view() {
            final String username = getUsername();
            fullName.find(By.tagName("a")).click();
            return pageBinder.bind(ViewUserPage.class, username);
        }

        public EditUserGroupsPage editGroups() {
            final String username = getUsername();
            getOperationsDropdown().openAndClick(By.className("editgroups_link"));
            return pageBinder.bind(EditUserGroupsPage.class, username);
        }

        // TODO other operations


        // TODO expose cells


        public List<PageElement> getUserGroupLinks() {
            return userGroupLinks;
        }

        public List<String> getUserGroupNames() {
            return getUserGroupLinks().stream()
                    .map(pageElement -> pageElement == null ? null : pageElement.getText())
                    .collect(Collectors.toList());
        }

        public TimedCondition hasApplicationRolesColumn() {
            return applicationRolesCells.timed().isVisible();
        }

        public TimedQuery<Iterable<String>> getApplicationRoleNames() {
            final Supplier<Iterable<String>> namesSupplier = extendedFinder
                    .within(applicationRolesCells)
                    .newQuery(By.tagName("li"))
                    .transform(PageElement::getText)
                    .supplier();
            return Queries.forSupplier(timeouts, namesSupplier);
        }

        public PageElement getUserDirectory() {
            return userDirectory;
        }

        private PageElement getResetLoginCountButton() {
            return loginDetailsCell.find(byTagAndData("a", "link-type", "reset-login-type"));
        }

        private DropDown getOperationsDropdown() {
            return pageBinder.bind(
                    DropDown.class,
                    By.cssSelector(".aui-dropdown2-trigger[aria-controls=user-actions-" + getUsername() + "]"),
                    By.cssSelector(".aui-dropdown2[id=user-actions-" + getUsername() + "]")
            );
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("username", getUsername())
                    .toString();
        }
    }
}
