package com.atlassian.jira.pageobjects.dialogs;

import com.atlassian.jira.pageobjects.components.SecurityLevelSelect;
import com.atlassian.pageobjects.PageBinder;
import org.openqa.selenium.By;

import javax.inject.Inject;

public class CommentDialog extends FormDialog {
    @Inject
    protected PageBinder pageBinder;

    public CommentDialog() {
        super("comment-add-dialog");
    }

    public void setComment(String comment) {
        getDialogElement().find(By.id("comment")).type(comment);
    }

    public boolean submit() {
        return super.submit(By.id("comment-add-submit"));
    }

    public SecurityLevelSelect getSecurityLevelControl() {
        return pageBinder.bind(SecurityLevelSelect.class, this.find(By.className("security-level")));
    }

    public String getSecurityLevelError() {
        return this.find(By.className("security-level-inline-error")).getText();
    }
}
