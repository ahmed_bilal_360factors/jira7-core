package com.atlassian.jira.pageobjects.pages.admin.application;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.pageobjects.components.fields.SingleSelect;
import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.webtest.webdriver.util.AUIFlags;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.TimedElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.google.common.collect.Iterables;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;
import static java.lang.String.format;
import static org.junit.Assert.assertTrue;

public class ApplicationAccessPage extends AbstractJiraPage {
    private static final String TRACE_ROLE_PUT_FINISHED = "role.put.finished";
    private static final String TRACE_ROLE_EDITORS_UPDATED = "role.editors.updated";
    private static final String TRACE_ROLE_EDITORS_LABELS = "role.editors.labels";
    private static final String TRACE_DEFAULTS_DIALOG_ENABLED = "jira.admin.application.defaultsdialog.enabled";

    @Inject
    private PageElementFinder finder;
    @Inject
    private TraceContext context;
    @Inject
    protected Timeouts timeouts;

    @ElementBy(cssSelector = "#application-roles")
    private PageElement root;

    private AUIFlags auiFlags;

    @Init
    public void initialise() {
        auiFlags = pageBinder.bind(AUIFlags.class);
    }

    @Override
    public TimedCondition isAt() {
        //Wait until the table finishes loading.
        final TimedElement element = root.timed();
        return Conditions.and(element.isPresent(), Conditions.not(element.hasClass("loading")));
    }

    @Override
    public String getUrl() {
        return "/secure/admin/ApplicationAccess.jspa";
    }

    public List<Role> roles() {
        return copyOf(transform(root.findAll(By.cssSelector("form")), Role::new));
    }

    public Role role(String id) {
        final PageElement element = root.find(By.cssSelector(format("form[data-role='%s']", id)));
        if (!element.isPresent()) {
            throw new IllegalArgumentException(format("Can't find role with id '%s'.", id));
        }
        return new Role(element);
    }

    private void executeAndAwait(Runnable function) {
        final Tracer checkpoint = context.checkpoint();
        function.run();
        context.waitFor(checkpoint, TRACE_ROLE_PUT_FINISHED);
        context.waitFor(checkpoint, TRACE_ROLE_EDITORS_UPDATED);
        context.waitFor(checkpoint, TRACE_ROLE_EDITORS_LABELS);
    }

    public ApplicationAccessDefaultsDialog openDefaultsDialog() {
        // This link sits in the top-right corner of the page and _loves_ to be covered by AUI Flags. Also, after the
        // dialog is opened, it is immediately disabled until data is loaded from the server - we need to make sure we
        // wait until this happens before continuing (hence the trace).
        final Tracer checkpoint = context.checkpoint();
        auiFlags.closeAllFlags();
        elementFinder.find(By.className("app-role-defaults-show-button")).click();
        context.waitFor(checkpoint, TRACE_DEFAULTS_DIALOG_ENABLED);
        return pageBinder.bind(ApplicationAccessDefaultsDialog.class);
    }

    public class Role {
        private final PageElement form;

        private Role(PageElement form) {
            if (!form.isPresent()) {
                throw new IllegalArgumentException("row is not on the page.");
            }

            this.form = form;
        }

        public List<RoleGroup> getGroups() {
            final List<PageElement> all = form.findAll(By.cssSelector("tbody tr"));
            if (all.isEmpty()) {
                return Collections.emptyList();
            } else if (all.size() == 1) {
                final PageElement row = all.get(0);
                if (row.find(By.className("application-role-empty")).isPresent()) {
                    return Collections.emptyList();
                }
                //noinspection Convert2MethodRef - IDEA barfs on it
                return copyOf(transform(all, (row1) -> new RoleGroup(row1)));
            } else {
                //noinspection Convert2MethodRef - IDEA barfs on it
                return copyOf(transform(all, (row) -> new RoleGroup(row)));
            }
        }

        private PageElement getNameElement() {
            return form.find(By.tagName("h3"));
        }

        private PageElement getUserCountElement() {
            return form.find(By.className("application-role-count-total"));
        }

        private PageElement getNumberOfSeatsElement() {
            return form.find(By.className("application-role-count-licensed"));
        }

        public String getName() {
            return StringUtils.stripToNull(getNameElement().getText());
        }

        public Role addGroup(final String group) {
            executeAndAwait(() ->
            {
                SingleSelect select = pageBinder.bind(SingleSelect.class, form.find(By.className("application-role-selector-container")));
                select.select(group);
            });
            return this;
        }

        public RoleGroup getGroup(final String group) {
            return Iterables.find(getGroups(), input -> group.equals(input.getName()), null);
        }

        public TimedCondition hasSeatWarning() {
            return form.find(By.className("application-role-count-warning")).timed().isPresent();
        }

        public boolean hasDefaultGroupWarning() {
            return form.find(By.className("application-role-without-default-group")).isPresent();
        }

        public int getUserCount() {
            return Integer.parseInt(getUserCountElement().getAttribute("data-value"));
        }

        public int getRemainingSeats() {
            return getNumberOfSeats() == getUserCount() ? 0 : getNumberOfSeats() - getUserCount();
        }

        public int getNumberOfSeats() {
            return Integer.parseInt(getNumberOfSeatsElement().getText());
        }

        public TimedCondition isMarkedAsDefault() {
            return form.find(By.className("default-app-lozenge")).timed().isVisible();
        }
    }

    public class RoleGroup {
        private final String name;
        private final PageElement row;
        private final int userCount;
        private final PageElement labels;

        private RoleGroup(PageElement row) {
            this.row = row;
            this.name = StringUtils.stripToNull(row.find(By.className("application-role-name-group-name")).getText());
            this.userCount = Integer.parseInt(row.find(By.className("application-role-name-group-size"), TimeoutType.AJAX_ACTION).getAttribute("data-value"));
            this.labels = row.find(By.cssSelector("application-role-labels[resolved][updated]"));
        }

        public String getName() {
            return name;
        }

        public int getUserCount() {
            return this.userCount;
        }

        public boolean isDefault() {
            return getDefaultInput().hasAttribute("checked", Boolean.TRUE.toString());
        }

        private void toggleDefault() {
            final PageElement input = getDefaultInput();
            executeAndAwait(input::click);
        }

        public void setDefault() {
            if (isDefault()) {
                throw new IllegalStateException(format("The role '%s' is already marked as default.", getName()));
            }
            toggleDefault();
        }

        public void unsetDefault() {
            if (!isDefault()) {
                throw new IllegalStateException(format("The role '%s' is already NOT default.", getName()));
            }
            toggleDefault();
        }

        private PageElement getDefaultInput() {
            final PageElement element = this.row.find(By.cssSelector(".application-role-default input"));
            assertTrue("Default option should be present.", element.isPresent());
            return element;
        }

        public boolean canRemove() {
            //You can remove if its a link.
            return getRemove().getTagName().equals("a");
        }

        public void remove() {
            assertTrue("Remove group is disabled.", canRemove());
            final PageElement remove = getRemove();

            executeAndAwait(remove::click);
        }

        public List<String> getLabels() {
            return labels.findAll(By.className("group-label-lozenge")).stream()
                    .map(PageElement::getText)
                    .collect(CollectorsUtil.toImmutableList());
        }

        private PageElement getRemove() {
            final PageElement element = this.row.find(By.className("application-role-remove"));
            assertTrue("Remove option should be present.", element.isPresent());
            return element;
        }
    }
}
