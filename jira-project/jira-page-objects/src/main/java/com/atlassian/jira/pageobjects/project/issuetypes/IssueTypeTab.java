package com.atlassian.jira.pageobjects.project.issuetypes;

import com.atlassian.jira.pageobjects.project.AbstractProjectConfigPageTab;

/**
 * @since v6.2
 */
public abstract class IssueTypeTab extends AbstractProjectConfigPageTab {
    private static final String TAB_LINK_ID_PREFIX = "view_project_issuetype_";

    public static String getTabLinkForIssueTypeId(long issueTypeId) {
        return TAB_LINK_ID_PREFIX + issueTypeId;
    }

    private IssueTypeHeader header;

    protected IssueTypeHeader getIssueTypeHeader() {
        if (header == null) {
            header = pageBinder.bind(IssueTypeHeader.class);
        }
        return header;
    }
}
