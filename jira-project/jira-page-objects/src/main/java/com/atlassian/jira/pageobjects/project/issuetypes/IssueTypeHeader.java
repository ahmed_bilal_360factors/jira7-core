package com.atlassian.jira.pageobjects.project.issuetypes;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @since v6.2
 */
public class IssueTypeHeader {
    @Inject
    private PageBinder binder;

    @ElementBy(className = "admin-breadcrumb-trail")
    private PageElement breadcrumbsContainer;

    @ElementBy(id = "project-config-header-descriptor-title")
    private PageElement descriptorTitle;

    @ElementBy(id = "project-issuetypes-perspectives")
    private PageElement perspectiveContainer;

    @ElementBy(id = "project-issuetypes-perspective-workflow")
    private PageElement workflowPerspectiveButton;

    @ElementBy(id = "project-issuetypes-perspective-fields")
    private PageElement fieldsPerspectiveButton;

    @ElementBy(cssSelector = "#workflow-read-only .aui-lozenge")
    private PageElement readOnlyLozenge;

    @ElementBy(cssSelector = "#workflow-info .aui-message")
    private PageElement workflowInfoMessage;

    @WaitUntil
    public void waitFor() {
        binder.bind(LoadingBarrier.class).await();
    }

    public boolean isSwitcherPresent() {
        return perspectiveContainer.isVisible();
    }

    public TimedCondition isReadOnlyLozengeVisible() {
        return readOnlyLozenge.timed().isVisible();
    }

    public TimedCondition isWorkflowInfoMessageVisible() {
        return workflowInfoMessage.timed().isVisible();
    }

    public TimedQuery<String> getWorkflowInfoMessage() {
        return workflowInfoMessage.timed().getText();
    }

    public Mode<?> currentMode() {
        if (isActive(workflowPerspectiveButton)) {
            return Mode.WORKFLOW;
        } else if (isActive(fieldsPerspectiveButton)) {
            return Mode.FIELDS;
        } else {
            throw new IllegalStateException("Neither view/workflow where pressed. Impossible!");
        }
    }

    public String getDescriptorTitle() {
        return descriptorTitle.getText();
    }

    public IssueTypeFieldsTab gotoFields() {
        return gotoMode(Mode.FIELDS);
    }

    public IssueTypeWorkflowTab gotoWorkflow() {
        return gotoMode(Mode.WORKFLOW);
    }

    public <T extends IssueTypeTab> T gotoMode(final Mode<T> mode) {
        if (!mode.equals(currentMode())) {
            if (mode.equals(Mode.WORKFLOW)) {
                workflowPerspectiveButton.click();
            } else if (mode.equals(Mode.FIELDS)) {
                fieldsPerspectiveButton.click();
            } else {
                throw new IllegalStateException("Well this is embarrassing, I don't know how to handle " + mode);
            }
        }
        return mode.bind(binder);
    }

    private boolean isActive(PageElement toggle) {
        return Boolean.parseBoolean(toggle.getAttribute("aria-pressed"));
    }

    /**
     * Get the breadcrumbs displayed on the Issue Type header.
     *
     * @return list of breadcrumbs.
     */
    public List<BreadCrumb> getBreadCrumbs() {
        return breadcrumbsContainer.findAll(By.cssSelector("ol li")).stream().map(pageElement -> {
            final boolean isSelected = pageElement.hasClass("aui-nav-selected");
            final PageElement breadCrumbLink = pageElement.find(By.tagName("a"));
            if (breadCrumbLink.isPresent()) {
                final Optional<String> link = Optional.of(breadCrumbLink.getAttribute("href"));
                return new BreadCrumb(breadCrumbLink.getText(), link, isSelected, breadCrumbLink, true);
            } else {
                return new BreadCrumb(pageElement.getText(), Optional.empty(), isSelected, pageElement, false);
            }
        }).collect(Collectors.toList());
    }

    public static class BreadCrumb {
        private String displayName;
        private java.util.Optional<String> link;
        private final boolean selected;
        private final PageElement element;
        private final boolean anchorTag;

        public BreadCrumb(String displayName, java.util.Optional<String> link, boolean selected, PageElement element, boolean anchorTag) {
            this.displayName = displayName;
            this.link = link;
            this.selected = selected;
            this.element = element;
            this.anchorTag = anchorTag;
        }

        public String getDisplayName() {
            return displayName;
        }

        public java.util.Optional<String> getLink() {
            return link;
        }

        public boolean isSelected() {
            return selected;
        }

        public boolean isAnchorTag() {
            return anchorTag;
        }

        public PageElement getPageElement() {
            return element;
        }

        @Override
        public String toString() {
            return "BreadCrumb{" +
                    "displayName='" + displayName + '\'' +
                    ", link=" + link +
                    ", selected=" + selected +
                    ", element=" + element +
                    ", anchorTag=" + anchorTag +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof BreadCrumb)) return false;
            BreadCrumb that = (BreadCrumb) o;
            return selected == that.selected &&
                    anchorTag == that.anchorTag &&
                    Objects.equals(displayName, that.displayName) &&
                    Objects.equals(link, that.link) &&
                    Objects.equals(element, that.element);
        }

        @Override
        public int hashCode() {
            return Objects.hash(displayName, link, selected, element, anchorTag);
        }
    }

    public static class Mode<T extends IssueTypeTab> {
        public static final Mode<IssueTypeFieldsTab> FIELDS = new Mode<IssueTypeFieldsTab>(IssueTypeFieldsTab.class, "fields");
        public static final Mode<IssueTypeWorkflowTab> WORKFLOW = new Mode<IssueTypeWorkflowTab>(IssueTypeWorkflowTab.class, "workflow");

        private final Class<T> tab;
        private final String name;

        private Mode(Class<T> tab, String name) {
            this.tab = tab;
            this.name = name;
        }

        T bind(PageBinder binder) {
            return binder.bind(tab);
        }

        public Class<T> getTab() {
            return tab;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
