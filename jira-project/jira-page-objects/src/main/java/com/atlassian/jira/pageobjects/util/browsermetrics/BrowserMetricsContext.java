package com.atlassian.jira.pageobjects.util.browsermetrics;

import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.atlassian.webdriver.AtlassianWebDriver;
import org.openqa.selenium.WebDriverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.google.common.collect.Lists.transform;
import static java.util.Optional.empty;
import static java.util.Optional.of;

/**
 * browser-metrics is a JavaScript library which allows pages to instrument themselves and measure their load duration.
 * This means that the "definition of loaded" is encoded into the application itself, which allows us to use that
 * information to determine when a page is done rather than encoding it in a page object.
 *
 * @since v7.2.0
 */
public class BrowserMetricsContext {
    private static final Logger log = LoggerFactory.getLogger(BrowserMetricsContext.class);

    // We need a browser-metrics subscriber on the page that pools beacons, and exposes a "drain" method which
    // allows the objects to be handed over to the web driver side.
    //
    // The script is a little complex because it's _creating_ the subscriber if it doesn't yet exist. Because
    // window['browser-metrics'].subscribe() returns _all_ beacons to new subscribers (think of it as a cold
    // observable), embedding the script directly in this class simplifies integration.
    private static final String JAVASCRIPT_DRAIN_SCRIPT = "" +
        "return (window.__browser_metrics_context = window.__browser_metrics_context || (function () {" +
            "var beaconPool = [];" +
            "window['browser-metrics'].subscribe(function (beacon) {" +
                "beaconPool.push(beacon);" +
            "});" +
            "return {drain: function () { return beaconPool.splice(0, beaconPool.length); }};" +
        "}())).drain();";

    @Inject
    private AtlassianWebDriver executor;

    @Inject
    private Timeouts timeouts;

    // We want to allow multiple instances of this class to interact together on a same page, so it's necessary to share
    // beacons between them. (We would want multiple instances because page objects are composable and often there are
    // multiple interacting together).
    //
    // An alternative solution would have been to use a new browser-metrics subscriber for each context and exploit the
    // replay behavior (each subscriber is replayed with all beacons that have occurred).
    private static List<Beacon> beacons = Collections.synchronizedList(new LinkedList<>());

    /**
     * Returns a checkpoint containing the current state of beacon list.
     */
    public Checkpoint checkpoint() {
        try {
            // Attempt to retrieve all beacons before snap-shotting the checkpoint.
            retrieveBeacons();
        } catch (WebDriverException ex) {
            // Ignore - this fails when the WebDriver browser has not yet loaded, eg if we are taking a checkpoint
            // before the first page load to wait for an ajax request that is fired from document.onready
        }
        int position = beacons.size();
        log.debug("Returned checkpoint at position " + position);
        return new Checkpoint(this, position);
    }

    /**
     * Waits for the occurrence of a beacon with the given transition key exists after a checkpoint.
     *
     * @param checkpoint Starting point. Only beacons after this checkpoint will be inspected.
     * @param key        Transition key to watch for.
     */
    public Beacon waitFor(final Checkpoint checkpoint, final String key) {
        log.debug("Waiting for transition key " + key + " from position " + checkpoint.position);
        waitUntilTrue(condition(checkpoint, key));
        return firstBeacon(checkpoint, key).get();
    }

    /**
     * Check whether a beacon with given transition key exists after a checkpoint.
     *
     * @param checkpoint Starting point. Only beacons after this checkpoint will be inspected.
     * @param key        Transition key to watch for.
     * @return
     */
    public boolean exists(final Checkpoint checkpoint, final String key) {
        log.debug("Checking for transition key " + key + " from position " + checkpoint.position);
        retrieveBeacons();
        for (int i = checkpoint.position; i < beacons.size(); ++i) {
            final Beacon beacon = beacons.get(i);
            if (beacon.report.key.equals(key)) {
                log.debug("Matched transition key " + key + " at position " + i);
                return true;
            }
        }
        return false;
    }

    /**
     * Waits for the occurrence of a beacon with the given transition key after the given checkpoint.
     *
     * @param checkpoint Starting point. Only beacons after this checkpoint will be inspected.
     * @param key        Transition key to watch for.
     */
    public TimedCondition condition(final Checkpoint checkpoint, final String key) {
        final long timeout = timeouts.timeoutFor(TimeoutType.PAGE_LOAD);
        final long interval = timeouts.timeoutFor(TimeoutType.EVALUATION_INTERVAL);
        return new AbstractTimedCondition(timeout, interval) {
            @Override
            public String toString() {
                return "Beacon with transition key '" + key + "' " + super.toString();
            }

            @Override
            protected Boolean currentValue() {
                return exists(checkpoint, key);
            }
        };
    }

    private Optional<Beacon> firstBeacon(Checkpoint checkpoint, String key) {
        retrieveBeacons();
        for (int i = checkpoint.position; i < beacons.size(); ++i) {
            Beacon beacon = beacons.get(i);
            if (beacon.report.key.equals(key)) {
                return of(beacon);
            }
        }
        return empty();
    }

    private void retrieveBeacons() {
        final int sizeBefore = beacons.size();
        List<Map<String, Object>> traces = (List<Map<String, Object>>) executor.executeScript(JAVASCRIPT_DRAIN_SCRIPT);
        beacons.addAll(transform(traces, Beacon::FROM_MAP));
        if (log.isDebugEnabled()) {
            for (int i = sizeBefore; i < beacons.size(); ++i) {
                log.debug("Retrieved beacon " + beacons.get(i).report.key + " at position " + i);
            }
        }
    }
}
