package com.atlassian.jira.pageobjects.pages.admin;

import com.atlassian.jira.pageobjects.pages.AbstractJiraAdminPage;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Projects page in the admin section.
 *
 * @since v4.4
 */
public class ViewProjectsPage extends AbstractJiraAdminPage {

    private static final String NO_CHANGE_TYPE_EXCEPTION_MESSAGE = "Change project type has been removed from the view projects page." +
            "Please use EditProjectDetails dialog/page or a backdoor to change the project type";

    @ElementBy(id = "project-list")
    private PageElement projectsList;

    @FindBy(id = "add_project")
    private WebElement addProjectLink;

    @ElementBy(id = "noprojects")
    private PageElement noProjectsMessage;

    private final List<ProjectRow> projects;

    private final static String URI = "/secure/project/ViewProjects.jspa";

    public ViewProjectsPage() {
        projects = new ArrayList<ProjectRow>();
    }

    @Override
    public TimedCondition isAt() {
        return Conditions.or(projectsList.timed().isPresent(), noProjectsMessage.timed().isPresent());
    }

    public String getUrl() {
        return URI;
    }

    @Override
    public String linkId() {
        return "view_projects";
    }

    @Init
    private void loadProjects() {
        List<WebElement> rows = driver.findElements(By.cssSelector("table#project-list > tbody > tr"));

        // TODO BAD! relies on user language
        if (rows.isEmpty() || rows.get(0).getText().equals("You do not have the permissions to administer any projects, or there are none created.")) {
            return;
        }

        for (WebElement row : rows) {
            projects.add(pageBinder.bind(ProjectRow.class, row));
        }

    }

    /**
     * @deprecated since v7.2 There is no longer a change project type action in the view projects. You can do this
     * by going to the edit project page. Or clicking edit for this project on the view projects page.
     */
    public ChangeProjectTypeDialog changeProjectType(final long projectId) {
        throw new RuntimeException(NO_CHANGE_TYPE_EXCEPTION_MESSAGE);
    }

    /**
     * @deprecated since v7.2 There is no longer a change project type action in the view projects.
     */
    public boolean hasChangeProjectTypeLink(long projectId) {
        throw new RuntimeException(NO_CHANGE_TYPE_EXCEPTION_MESSAGE);
    }

    public Page addProject() {
        throw new UnsupportedOperationException("addProject for ProjectViewPage has not been implemented");
    }

    public List<ProjectRow> getProjects() {
        return Collections.unmodifiableList(projects);
    }

    public boolean isProjectPresent(final String projectName) {
        for (ProjectRow project : projects) {
            if (projectName.equals(project.getName())) {
                return true;
            }
        }
        return false;
    }

    public ProjectRow getProject(final String projectName) {
        for (ProjectRow project : projects) {
            if (projectName.equalsIgnoreCase(project.getName())) {
                return project;
            }
        }
        return null;
    }

    /**
     * @deprecated since v7.2 There is no longer a change project type action in the view projects.
     */
    private String toChangeProjectTypeLinkId(final long projectId) {
        throw new RuntimeException(NO_CHANGE_TYPE_EXCEPTION_MESSAGE);
    }
}
