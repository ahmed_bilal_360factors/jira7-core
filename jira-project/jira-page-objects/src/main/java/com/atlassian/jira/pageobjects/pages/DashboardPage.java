package com.atlassian.jira.pageobjects.pages;

import com.atlassian.jira.pageobjects.components.DropDown;
import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.gadgets.GadgetContainer;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.page.HomePage;
import org.openqa.selenium.By;

import java.util.Optional;

/**
 * Page object implementation for the Dashboard page in JIRA.
 */
public class DashboardPage extends AbstractJiraPage implements HomePage<JiraHeader> {
    private static final String URI = "/secure/Dashboard.jspa";

    @ElementBy(className = "page-type-dashboard")
    protected PageElement dashboardBody;

    @ElementBy(tagName = "dashboard")
    private PageElement dashboardElement;

    @ElementBy(cssSelector = "#dash-options .aui-dropdown2-trigger")
    private PageElement dropdown;

    @ElementBy(cssSelector = "#tools-dropdown-items")
    private PageElement dropdownItems;

    @ElementBy(cssSelector = "#wallboards")
    private PageElement wallboardView;

    protected DropDown profileDropdown;

    private Integer dashboardId;

    public DashboardPage(){
    }

    public DashboardPage(final Integer dashboardId) {
        this.dashboardId = dashboardId;
    }

    @Init
    public void initialise() {
        profileDropdown = pageBinder.bind(DropDown.class, By.cssSelector("#header-details-user .drop"), By.id("user-options-list"));
        dashboardId = findDashboardId().orElse(null);
    }

    private Optional<Integer> findDashboardId() {
        try {
            JSONObject dashboardParams = new JSONObject(dashboardElement.getAttribute("params"));
            String dashboardUrl = dashboardParams.getString("dashboardUrl");
            return Optional.of(Integer.parseInt(dashboardUrl.substring(dashboardUrl.lastIndexOf('/') + 1)));
        } catch (JSONException | NumberFormatException e) {
            return Optional.empty();
        }
    }

    @Override
    public TimedCondition isAt() {
        return dashboardBody.timed().isPresent();
    }

    @Override
    public String getUrl() {
        String selectPage = dashboardId == null ? "" : "?selectPageId=" + dashboardId;
        return URI + selectPage;
    }

    public GadgetContainer gadgets() {
        return pageBinder.bind(GadgetContainer.class, dashboardId);
    }

    public GadgetContainer switchDashboard(String dashboardTitle) {
        return getHeader().getDashboardMenu().open().getDashboard(dashboardTitle).gadgets();
    }

    public void viewAsWallboard() {
        Poller.waitUntilTrue(dropdown.timed().isVisible());
        dropdown.click();
        dropdownItems.find(By.id("wallboard_link")).click();
    }

    public TimedCondition isViewingWallboard() {
        return wallboardView.timed().isVisible();
    }

    public ConfigureWallboardPage configureWallboard() {
        Poller.waitUntilTrue(dropdown.timed().isVisible());
        dropdown.click();
        dropdownItems.find(By.id("configure_wallboard")).click();
        return pageBinder.bind(ConfigureWallboardPage.class);
    }
}
