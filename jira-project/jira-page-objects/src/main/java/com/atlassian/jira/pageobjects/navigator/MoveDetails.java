package com.atlassian.jira.pageobjects.navigator;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import java.util.Collection;

/**
 * Move details for Bulk Edit.
 *
 * @since v7.0
 */
public class MoveDetails extends AbstractJiraPage {

    @ElementBy(id = "next")
    protected PageElement next;

    @ElementBy(id = "bulkedit")
    protected PageElement bulkEditForm;

    @ElementBy(id = "sameAsBulkEditBean")
    protected PageElement sameAllCheckbox;

    public Collection<MoveIssuesContainer> getMoveIssuesContainers() {
        return bulkEditForm.findAll(By.className("be-project-type-issue")).stream().map(element -> pageBinder.bind(MoveIssuesContainer.class, element)).collect(CollectorsUtil.toImmutableList());
    }

    public void waitForJavaScriptToProcessParentIssuePicker() {
        Poller.waitUntilTrue("parent issue selector is not processed by JS", bulkEditForm.find(By.cssSelector(".be-project-type-issue")).timed().isPresent());
    }

    public MoveSetFields next() {
        return next(MoveSetFields.class);
    }

    public <T extends AbstractJiraPage> T next(Class<T> nextPage) {
        next.click();
        return pageBinder.bind(nextPage);
    }

    @Override
    public TimedCondition isAt() {
        return Conditions.and(bulkEditForm.timed().isVisible(), bulkEditForm.find(By.cssSelector(".be-project-type-issue")).timed().isPresent());
    }

    @Override
    public String getUrl() {
        throw new UnsupportedOperationException("Not implemented");
    }

    public TimedCondition isSameAllCheckboxPresent() {
        return sameAllCheckbox.timed().isPresent();
    }
}
