package com.atlassian.jira.pageobjects.dialogs.quickedit;

import com.atlassian.jira.pageobjects.components.CalendarPicker;
import com.atlassian.jira.pageobjects.components.fields.SingleSelect;
import com.atlassian.jira.pageobjects.components.fields.WikiField;
import com.atlassian.jira.pageobjects.dialogs.FormDialog;
import com.atlassian.jira.pageobjects.framework.fields.CustomField;
import com.atlassian.jira.pageobjects.pages.viewissue.CreateIssueDescriptionField;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.webdriver.AtlassianWebDriver;
import org.apache.commons.lang.StringEscapeUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Quick Create Issue Dialog
 *
 * @since v5.0
 */
public class CreateIssueDialog extends AbstractIssueDialog {

    public enum Type {
        SUBTASK,
        ISSUE
    }

    @Inject
    protected AtlassianWebDriver driver;

    @Inject
    PageBinder pageBinder;

    @ElementBy(className = "qf-field-project")
    protected PageElement projectSingleSelectEl;

    @ElementBy(id = "issuetype-field")
    protected PageElement issueTypeFieldEl;

    @ElementBy(className = "qf-field-issuetype")
    protected PageElement issueTypeSingleSelectEl;

    @ElementBy(xpath = "//div[.//div[@id = \"description-wiki-edit\"] and @class=\"field-group\"]")
    protected PageElement descriptionField;

    @ElementBy(name = "description")
    protected PageElement descriptionInput;

    protected SingleSelect issueTypeSingleSelect;
    protected SingleSelect projectSingleSelect;

    @Init
    protected void createControls() {
        // subtask dialog doesn't have one
        if (projectSingleSelectEl.isPresent()) {
            projectSingleSelect = binder.bind(SingleSelect.class, projectSingleSelectEl);
        }

        issueTypeSingleSelect = binder.bind(SingleSelect.class, issueTypeSingleSelectEl);
    }

    @WaitUntil
    private void issueTypeSelectsReady() {
        Poller.waitUntilTrue(issueTypeSingleSelectEl.timed().isPresent());
    }

    public CreateIssueDialog(final Type type) {
        super(type == Type.SUBTASK ? "create-subtask-dialog" : "create-issue-dialog");
    }

    @Override
    public CreateIssueDialog switchToCustomMode() {
        openFieldPicker().switchToCustomMode();
        return this;
    }

    @Override
    public CreateIssueDialog removeFields(String... fields) {
        openFieldPicker().removeFields(fields);
        return this;
    }

    @Override
    public CreateIssueDialog addFields(String... fields) {
        openFieldPicker().addFields(fields);
        return this;
    }

    @Override
    public CreateIssueDialog switchToFullMode() {
        openFieldPicker().switchToFullMode();
        waitWhileSubmitting(); // TODO this probably do nothing more than syncing with webdriver here
        return this;
    }

    @Override
    public CreateIssueDialog fill(String id, String value) {
        FormDialog.setElement(find(By.id(id)), value);
        return this;
    }

    public CreateIssueDialog openTab(String tabName) {
        final List<PageElement> tabs = getDialogElement().findAll(By.cssSelector(".tabs-menu .menu-item"));
        for (PageElement tab : tabs) {
            PageElement tabLink = tab.find(By.cssSelector("a"));
            if (tabName.equals(tabLink.getText().trim())) {
                tabLink.click();
                waitUntilTrue(tab.timed().hasClass("active-tab"));
                break;
            }
        }
        return this;
    }

    public List<String> getTabs() {
        final List<String> tabs = new ArrayList<String>();
        final List<PageElement> tabsEls = getDialogElement().findAll(By.cssSelector(".tabs-menu .menu-item strong"));
        for (PageElement tab : tabsEls) {
            tabs.add(tab.getText().trim());
        }
        return tabs;
    }

    public CreateIssueDialog setPriority(String priority) {
        findPrioritySingleSelectField().select(priority);
        return this;
    }

    public CreateIssueDialog selectProject(String name) {
        projectSingleSelect.select(name);
        waitUntilIssueTypeFieldIsEnabled();
        return this;
    }

    public CreateIssueDialog selectIssueType(String name) {
        issueTypeSingleSelect.select(name);
        waitUntilIssueTypeFieldIsEnabled();
        return this;
    }

    public CreateIssueDialog checkCreateMultiple() {
        final PageElement createAnother = find(By.id("qf-create-another"));

        final String checked = createAnother.getAttribute("checked");
        if (checked == null || !checked.equals("true")) {
            createAnother.click();
        }

        return this;
    }

    public CreateIssueDialog uncheckCreateMultiple() {
        final PageElement createAnother = find(By.id("qf-create-another"));
        final String checked = createAnother.getAttribute("checked");

        if (checked != null && createAnother.getAttribute("checked").equals("true")) {
            createAnother.click();
        }

        return this;
    }

    public <T extends CustomField> T getCustomField(String customFieldId, Class<T> customFieldType) {
        return binder.bind(customFieldType, form, customFieldId);
    }

    public PageElement getCustomFieldElement(String customFieldId) {
        return form.find(By.id(customFieldId));
    }

    public String getProject() {
        return projectSingleSelect.getValue();
    }

    public TimedQuery<String> getTimedProject() {
        return projectSingleSelect.getTimedValue();
    }

    public String getIssueType() {
        return issueTypeSingleSelect.getValue();
    }

    public List<String> getIssueTypes() {
        List<String> issueTypes = new ArrayList<String>();
        issueTypeSingleSelect.triggerSuggestions();
        Iterable<String> suggestions = issueTypeSingleSelect.getSuggestionsTimed().now();
        for (String issueType : suggestions) {
            issueTypes.add(StringEscapeUtils.unescapeHtml(issueType));
        }

        //add currently selected
        issueTypes.add(issueTypeSingleSelect.getValue());
        issueTypeSingleSelect.triggerSuggestions();
        return issueTypes;
    }

    public List<String> getFields() {
        final List<String> fields = new ArrayList<String>();
        final List<PageElement> fieldsEls = form.findAll(By.cssSelector(".content .field-group > label"));
        for (PageElement fieldEl : fieldsEls) {
            if (fieldEl.isVisible()) {
                fieldEl.javascript().execute("jQuery(arguments[0]).find('.icon-required').remove()"); // hack: remove required icon so we can get the real text
                fields.add(fieldEl.getText().trim());
            }
        }
        return fields;
    }

    public <P> P submit(Class<P> pageClass, Object... args) {
        boolean open = this.submit(By.id("create-issue-submit"));
        if (!open) {
            waitUntilClosed();
        }
        return binder.bind(pageClass, args);
    }

    public CalendarPicker getDueDateCalendarPicker() {
        return pageBinder.bind(CalendarPicker.class, By.id("duedate"), By.id("duedate-trigger"));
    }

    public SingleSelect findSingleSelectCustomField(String customFieldId) {
        return findSingleSelectField(customFieldId);
    }

    public void clearDueDateField() {
        final WebElement dueDateField = driver.findElement(By.id("duedate"));
        dueDateField.clear();
    }

    private SingleSelect findPrioritySingleSelectField() {
        return findSingleSelectField("priority");
    }

    private SingleSelect findSingleSelectField(String key) {
        List<PageElement> fieldGroupElements = locator.findAll(By.className("field-group"));
        PageElement singleSelectElement = null;

        for (PageElement fieldGroupElement : fieldGroupElements) {
            if (fieldGroupElement.find(By.id(key + "-single-select")).isPresent()) {
                singleSelectElement = fieldGroupElement;
            }
        }
        return pageBinder.bind(SingleSelect.class, singleSelectElement);
    }

    /**
     * If the user does not have BROWSE USER permission they should not have a select box for the reporter, instead
     * just a text box.
     * @return whether suggestions are given for the reporter
     */
    public boolean doesReporterHaveSuggestions() {
        PageElement reporter = locator.find(By.id("reporter"));
        return reporter.getTagName().equals("select");
    }

    private void waitUntilIssueTypeFieldIsEnabled() {
        waitUntilTrue(issueTypeFieldEl.timed().isEnabled());
    }

    public WikiField getDescriptionWikiField() {
        return pageBinder.bind(WikiField.class, descriptionField);
    }

    public CreateIssueDescriptionField getDescription() {
        return pageBinder.bind(CreateIssueDescriptionField.class, descriptionInput);
    }
}
