package com.atlassian.jira.pageobjects.components;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import org.openqa.selenium.By;

public final class ErrorPanel {
    private final PageElement container;
    private PageElement panel;

    public ErrorPanel(PageElement container) {
        this.container = container;
    }

    public TimedCondition isPresent() {
        return getPanel().timed().isPresent();
    }

    public TimedQuery<String> getLinkHref() {
        return getPanel().find(By.cssSelector("a")).timed().getAttribute("href");
    }

    private PageElement getPanel() {
        if (panel == null) {
            panel = container.find(By.className("form-body"));
        }
        return panel;
    }
}
