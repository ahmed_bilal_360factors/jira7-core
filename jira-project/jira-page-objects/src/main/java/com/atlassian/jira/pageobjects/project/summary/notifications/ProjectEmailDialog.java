package com.atlassian.jira.pageobjects.project.summary.notifications;

import com.atlassian.jira.pageobjects.dialogs.JiraDialog;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * @since v4.4
 */
public class ProjectEmailDialog extends JiraDialog {
    @Inject
    private PageBinder binder;

    public ProjectEmailDialog() {
        super("project-email-dialog");
    }

    @WaitUntil
    public void waitForDialogContent() {
        waitUntilTrue(isOpen());
    }

    public ProjectEmailDialog setFromAddress(final String fromAddress) {
        getFromAddressField().clear()
                .type(fromAddress);

        return this;
    }

    private PageElement getFromAddressField() {
        return this.find(By.id("fromAddress"));
    }


    private PageElement getSubmit() {
        return this.find(By.id("submit"));
    }

    public String getFromAddressValue() {
        return getFromAddressField().getValue();
    }

    public String getError() {
        final PageElement errorElement = this.find(By.className("error"));

        if (errorElement.timed().isPresent().byDefaultTimeout()) {
            return errorElement.getText();
        }

        return null;
    }

    public <T extends Page> T submit(final Class<T> nextPage, String... arguments) {
        getSubmit().click();
        waitUntilTrue("Expected dialog to be dismissed, but was not", isClosed());
        return binder.bind(nextPage, arguments);
    }

    public ProjectEmailDialog submit() {
        getSubmit().click();
        return binder.bind(ProjectEmailDialog.class);
    }

}
