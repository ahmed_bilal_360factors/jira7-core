package com.atlassian.jira.pageobjects.config;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.UserCredentials;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.ClientFilter;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.apache.log4j.Logger;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import javax.ws.rs.core.MediaType;
import java.net.URI;

import static java.net.URI.create;
import static org.apache.commons.lang3.StringUtils.appendIfMissing;

/**
 * Prevents popups from showing up. Use {@link PopupCollector} to obtain collections of popup ids.
 *
 * @since v7.1.0
 */
public class PopupPreventer {
    private static final Logger LOG = Logger.getLogger(PopupPreventer.class);

    private final JiraTestedProduct jira;

    public PopupPreventer(final JiraTestedProduct jira) {
        this.jira = jira;
    }

    public void preventPopups(final Iterable<String> helpTipIds, final Iterable<String> notificationFlagIds) {
        final WebResource restRoot = getRestRoot();
        helpTipIds.forEach((helpTipId) -> preventHelpTips(helpTipId, restRoot));
        notificationFlagIds.forEach((notificationId) -> preventNotificationFlags(notificationId, restRoot));
    }

    private WebResource getRestRoot() {
        final URI base = create(appendIfMissing(jira.getProductInstance().getBaseUrl(), "/"));
        final ClientConfig config = new DefaultClientConfig();
        config.getClasses().add(JacksonJsonProvider.class);
        final Client client = Client.create(config);
        final ClientFilter authorisation = getAuthorisation();
        client.addFilter(authorisation);
        return client
                .resource(base)
                .path("rest");
    }

    private ClientFilter getAuthorisation() {
        final UserCredentials credentials = jira.getAdminCredentials();
        return new HTTPBasicAuthFilter(
                credentials.getUsername(),
                credentials.getPassword()
        );
    }

    private void preventNotificationFlags(final String notificationId, final WebResource restRoot) {
        try {
            restRoot
                    .path("flags")
                    .path("1.0")
                    .path("flags")
                    .path(notificationId)
                    .path("dismiss")
                    .put();
        } catch (final Exception e) {
            LOG.error("Cannot prevent notification flags with id = " + notificationId, e);
        }
    }

    private void preventHelpTips(final String helpTipId, final WebResource restRoot) {
        try {
            restRoot
                    .path("helptips")
                    .path("1.0")
                    .path("tips")
                    .header("User-Agent", "HelpTipTestRule")
                    .type(MediaType.APPLICATION_JSON_TYPE)
                    .post(new HelpTip(helpTipId));
        } catch (final Exception e) {
            LOG.error("Cannot prevent help tips with id = " + helpTipId, e);
        }
    }

    @JsonAutoDetect
    private static class HelpTip {
        private final String id;

        private HelpTip(final String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }
    }
}