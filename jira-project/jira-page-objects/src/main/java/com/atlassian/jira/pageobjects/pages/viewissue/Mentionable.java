package com.atlassian.jira.pageobjects.pages.viewissue;

import com.atlassian.jira.pageobjects.components.userpicker.MentionsUserPicker;
import com.atlassian.pageobjects.elements.query.TimedQuery;

/**
 * Interface for controls which support '@mentions'
 *
 * @since v7.2
 */
interface Mentionable <T> {
    T typeInput(CharSequence ...text);
    String getInput();
    TimedQuery<String> getInputTimed();
    T selectMention(String userId);
    MentionsUserPicker getMentions();
}