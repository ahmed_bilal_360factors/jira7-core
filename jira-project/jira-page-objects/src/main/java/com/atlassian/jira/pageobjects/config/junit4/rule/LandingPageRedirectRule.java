package com.atlassian.jira.pageobjects.config.junit4.rule;

import com.atlassian.jira.functest.framework.util.junit.AnnotatedDescription;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.EnableLandingPageRedirect;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;
import javax.inject.Inject;

/**
 * Disables Onboarding in JIRA, unless the {@link EnableLandingPageRedirect} annotation is
 * present for given test.
 *
 * @since 6.4
 */
public class LandingPageRedirectRule extends TestWatcher {
    public static final String DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG = "jira.onboarding.feature.disabled";

    @Inject
    private JiraTestedProduct jira;

    @Override
    protected void starting(@Nonnull final Description description) {
        final AnnotatedDescription annotatedDescription = new AnnotatedDescription(description);
        if (annotatedDescription.hasAnnotation(EnableLandingPageRedirect.class)) {
            jira.backdoor().darkFeatures().disableForSite(DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG);

        } else {
            // default
            jira.backdoor().darkFeatures().enableForSite(DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG);
        }
    }

}
