package com.atlassian.jira.pageobjects.pages;

import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.SelectElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

/**
 * Page object implementation for the Add dashboard dialog in JIRA.
 *
 * @since 6.4
 */
public class AddDashboardPage extends AbstractJiraPage {
    @ElementBy(name = "portalPageName")
    private PageElement nameField;

    @ElementBy(name = "clonePageId")
    private PageElement startFromSelect;

    @ElementBy(id = "share_type_selector")
    private SelectElement shareSelect;

    @ElementBy(id = "share_add_global")
    private PageElement addShareButton;

    @ElementBy(id = "add-dashboard-submit")
    private PageElement addDashboardButton;

    @Init
    public void initialise() {
    }

    @Override
    public TimedCondition isAt() {
        return nameField.timed().isPresent();
    }

    @Override
    public String getUrl() {
        return "/secure/AddPortalPage!default.jspa";
    }

    public void setName(final String name) {
        nameField.clear().type(name);
    }

    public void copyFrom(final String dashboardName) {
        startFromSelect.type(dashboardName).click();
    }

    public void shareWithEveryone() {
        shareSelect.select(Options.value("global"));
        addShareButton.click();
    }

    public void submit() {
        addDashboardButton.click();
    }
}
