package com.atlassian.jira.pageobjects.project.issuetypes;

import com.atlassian.jira.pageobjects.project.workflow.EditWorkflowDialog;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import static org.junit.Assert.assertTrue;

/**
 * Represents the Issue Type Workflow tab in view mode.
 *
 * @since 6.2
 */
public class IssueTypeWorkflowTab extends AbstractIssueTypeWorkflowTab {
    @ElementBy(className = "issuetypeconfig-edit-workflow")
    private PageElement editWorkflow;

    @ElementBy(id = "read_only_workflow")
    private PageElement editWorkflowReadOnly;

    public IssueTypeWorkflowTab() {
    }

    public IssueTypeWorkflowTab(String projectKey, long issueTypeId) {
        super(projectKey, issueTypeId);
    }

    public boolean canEdit() {
        return editWorkflow.isPresent() && editWorkflow.isEnabled();
    }

    public boolean editReadonly() {
        return editWorkflowReadOnly.isPresent();
    }

    public EditWorkflowDialog gotoEditWorkflowDialog() {
        assertTrue("Not able to edit this workflow.", canEdit());

        editWorkflow.click();
        return pageBinder.bind(EditWorkflowDialog.class);
    }

    public DraftIssueTypeWorkflowTab clickEdit() {
        return clickEditWorkflowAndBind(DraftIssueTypeWorkflowTab.class);
    }

    private <P> P clickEditWorkflowAndBind(Class<P> page, Object... args) {
        assertTrue("Not able to edit this workflow.", canEdit());

        editWorkflow.click();
        return pageBinder.bind(page, args);
    }

    @Override
    String getUrl(String projectKey, long issueTypeId) {
        return String.format("/plugins/servlet/project-config/%s/issuetypes/%d/workflow", projectKey, issueTypeId);
    }

    @Override
    public TimedCondition isAt() {
        return isAt(false);
    }
}
