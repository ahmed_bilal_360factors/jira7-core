package com.atlassian.jira.pageobjects.project;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.jira.pageobjects.project.ProjectConfigActions;
import com.atlassian.jira.pageobjects.project.ProjectConfigHeader;
import com.atlassian.jira.pageobjects.project.ProjectConfigPageTab;
import com.atlassian.jira.pageobjects.project.ProjectConfigTabs;
import com.atlassian.jira.pageobjects.project.ProjectInfoLocator;
import com.atlassian.pageobjects.binder.Init;

/**
 * @since v4.4
 */
public abstract class AbstractProjectConfigPageTab extends AbstractJiraPage implements ProjectConfigPageTab {
    protected ProjectInfoLocator projectInfoLocator;

    @Init
    public void init() {
        projectInfoLocator = pageBinder.bind(ProjectInfoLocator.class);
    }

    @Override
    public String getProjectKey() {
        return projectInfoLocator.getProjectKey();
    }

    @Override
    public long getProjectId() {
        return projectInfoLocator.getProjectId();
    }

    @Override
    public ProjectConfigTabs getTabs() {
        return pageBinder.bind(ProjectConfigTabs.class);
    }

    public ProjectConfigActions getOperations() {
        return pageBinder.bind(ProjectConfigActions.class);
    }

    @Override
    public ProjectConfigHeader getProjectHeader() {
        throw new RuntimeException("Project config no longer has this header. Rewrite tests to not use this to find " +
                "the project key and instead use a backdoor.");
    }

    @Override
    public ProjectSettingsHeader getProjectSettingsHeader() {
        return pageBinder.bind(ProjectSettingsHeader.class);
    }
}
