package com.atlassian.jira.pageobjects.setup;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.functest.rule.BeforeBuildRule;
import com.atlassian.jira.functest.rule.SinceBuildRule;
import com.atlassian.jira.functest.rule.CheckCachesRule;
import com.atlassian.jira.functest.rule.LogTimeRule;
import com.atlassian.jira.functest.rule.OutgoingMailTestRule;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.junit4.rule.ClearFlagsRule;
import com.atlassian.jira.pageobjects.config.junit4.rule.CreateUserRule;
import com.atlassian.jira.pageobjects.config.junit4.rule.DirtyWarningTerminatorRule;
import com.atlassian.jira.pageobjects.config.junit4.rule.EnableAUIFlagsRule;
import com.atlassian.jira.pageobjects.config.junit4.rule.EnableAnalyticsRule;
import com.atlassian.jira.pageobjects.config.junit4.rule.DisableRTERule;
import com.atlassian.jira.pageobjects.config.junit4.rule.LandingPageRedirectRule;
import com.atlassian.jira.pageobjects.config.junit4.rule.LoginRule;
import com.atlassian.jira.pageobjects.config.junit4.rule.NPSRule;
import com.atlassian.jira.pageobjects.config.junit4.rule.RestoreDataMethodRule;
import com.atlassian.jira.pageobjects.config.junit4.rule.RuleChainBuilder;
import com.atlassian.jira.pageobjects.config.junit4.rule.WebSudoRule;
import com.atlassian.webdriver.testing.rule.LogPageSourceRule;
import com.atlassian.webdriver.testing.rule.SessionCleanupRule;
import com.atlassian.webdriver.testing.rule.WindowSizeRule;
import org.junit.rules.RuleChain;

import static com.atlassian.jira.pageobjects.config.junit4.rule.RuleChainBuilder.Conditionally.around;

/**
 * Default set of rules for JIRA product web tests.
 *
 * @since 5.1
 */
@Internal
public final class JiraWebTestRules {
    private JiraWebTestRules() {
        throw new AssertionError("Don't instantiate me");
    }

    public static RuleChain forJira(final JiraTestedProduct jira) {
        return RuleChainBuilder.forProduct(jira)
                .around(new SinceBuildRule(jira::backdoor))
                .around(new BeforeBuildRule(jira::backdoor))
                .around(LogTimeRule.class)
                        // before-test
                .around(WindowSizeRule.class)
                .around(SessionCleanupRule.class)
                .around(RestoreDataMethodRule.class)
                .when(!jira.shouldSkipSetup(), around(NPSRule.class))
                .around(new DisableRTERule(jira))
                .around(EnableAUIFlagsRule.class)
                .around(EnableAnalyticsRule.class)
                .when(!jira.shouldSkipSetup(), around(WebSudoRule.class))
                .when(!jira.shouldSkipSetup(), around(LandingPageRedirectRule.class))
                .around(CreateUserRule.class)
                .around(LoginRule.class)
                .when(!jira.shouldSkipSetup(), around(ClearFlagsRule.class))
                        // after-test
                .around(JavaScriptErrorsRuleFactory.create())
                .around(JiraWebDriverScreenshotRule.class)
                .around(DirtyWarningTerminatorRule.class)
                .around(new LogPageSourceRule(jira.getTester().getDriver(), JiraWebTestLogger.LOGGER))
                .around(new OutgoingMailTestRule(jira::backdoor))
                .around(new CheckCachesRule(jira::backdoor))
                .build();
    }

}
