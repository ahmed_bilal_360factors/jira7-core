package com.atlassian.jira.pageobjects.project.add;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static java.lang.String.format;


/**
 * This is the start of the "Create Project" accessible from the header menu. To get here, bind to the
 * com.atlassian.jira.pageobjects.components.JiraHeader and click through the menus.
 *
 * @since v4.4
 */
public class AddProjectWizardProjectTypeSelection extends AddProjectWizardPage {

    private static final String ISSUE_TRACKING_MODULE_KEY = "com.atlassian.jira-core-project-templates:jira-issuetracking-item";
    private static final String PROJECT_MANAGEMENT_MODULE_KEY = "com.atlassian.jira-core-project-templates:jira-projectmanagement-item";
    private static final String JIRA_CLASSIC_MODULE_KEY = "com.atlassian.jira-core-project-templates:jira-blank-item";
    private static final String JIRA_CORE_PROCESS_MANAGEMENT = "com.atlassian.jira-core-project-templates:jira-core-process-management";
    private static final String SCRUM_KEY = "com.pyxis.greenhopper.jira:gh-scrum-template";

    private PageElement templateList;
    private PageElement submit;

    @Inject
    private PageBinder binder;

    @Init
    public void init() {
        PageElement element = getDialogElement();
        templateList = element.find(By.className("pt-templates-list"));
        submit = element.find(By.cssSelector(".create-project-dialog-create-button.pt-submit-button"));
    }

    public AddProjectWizardProjectDetails coreProcessManagement() {
        return selectByModuleCompleteKey(JIRA_CORE_PROCESS_MANAGEMENT).acceptAndConfirm();
    }

    public AddProjectWizardProjectDetails issueTracking() {
        return selectByModuleCompleteKey(ISSUE_TRACKING_MODULE_KEY).acceptAndConfirm();
    }

    public AddProjectWizardProjectDetails projectManagement() {
        return selectByModuleCompleteKey(PROJECT_MANAGEMENT_MODULE_KEY).accept();
    }

    public AddProjectWizardProjectDetails jiraClassic() {
        return selectByModuleCompleteKey(JIRA_CLASSIC_MODULE_KEY).accept();
    }

    public AddProjectWizardProjectDetails scrum() {
        return selectByModuleCompleteKey(SCRUM_KEY).acceptAndConfirm();
    }

    public AddProjectWizardProjectTypeSelection selectByModuleCompleteKey(String key) {
        final String cssSelector = format("li.template[data-item-module-complete-key=\"%s\"]", key);
        final PageElement template = templateList.find(By.cssSelector(cssSelector));
        template.click();
        return this;
    }

    public AddProjectWizardProjectDetails accept() {
        submit.click();
        return binder.bind(AddProjectWizardProjectDetails.class);
    }

    public AddProjectWizardProjectDetails acceptAndConfirm() {
        submit.click();
        return binder.bind(AddProjectWizardConfirmSelection.class).confirm();
    }

    public AddProjectWizardProjectDetails acceptWithConfirmation() {
        submit.click();
        return binder.bind(AddProjectWizardProjectDetails.class);
    }
}
