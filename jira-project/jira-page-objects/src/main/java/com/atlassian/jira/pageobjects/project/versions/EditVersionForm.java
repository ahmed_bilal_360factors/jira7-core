package com.atlassian.jira.pageobjects.project.versions;

import com.atlassian.jira.pageobjects.components.CalendarPicker;
import com.atlassian.jira.pageobjects.project.restfultable.AbstractProjectConfigEditForm;
import com.atlassian.pageobjects.PageBinder;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * @since v4.4
 */
public class EditVersionForm extends AbstractProjectConfigEditForm {

    private static final String VERSION_NAME = ".project-config-version-name";
    private static final String VERSION_DESC = ".project-config-version-description";
    private static final String VERSION_START_DATE = ".project-config-version-start-date";
    private static final String VERSION_RELEASE_DATE = ".project-config-version-release-date";
    public static final String VERSION_START_DATE_PICKER_ID = "project-config-versions-start-date";
    public static final String VERSION_RELEASE_DATE_PICKER_ID = "project-config-versions-release-date";

    @Inject
    private PageBinder pageBinder;

    public EditVersionForm(final By rowSelector) {
        super(rowSelector);
    }

    public EditVersionForm fill(String name, String description, String releaseDate) {
        return fill(name, description, null, releaseDate);
    }

    public EditVersionForm fill(String name, String description, String startDate, String releaseDate) {
        getNameField().clear().type(name);
        getDescriptionField().clear().type(description);

        if (startDate != null) {
            getStartDateField().clear().type(startDate);
        }
        if (releaseDate != null) {
            getReleaseDateField().clear().type(releaseDate);
        }
        return this;
    }

    public EditVersionForm submit() {
        submitAdd();
        return this;
    }

    public EditVersionForm cancel() {
        getCancelLink().click();
        waitUntilFalse(row.timed().hasClass(LOADING_CLASS));
        waitUntilFalse(getCancelLink().timed().isPresent());
        return this;
    }

    public Field getNameField() {
        return pageBinder.bind(Field.class, findInRow(VERSION_NAME));
    }

    public Field getDescriptionField() {
        return pageBinder.bind(Field.class, findInRow(VERSION_DESC));
    }

    public Field getStartDateField() {
        return pageBinder.bind(Field.class, findInRow(VERSION_START_DATE));
    }

    public Field getReleaseDateField() {
        return pageBinder.bind(Field.class, findInRow(VERSION_RELEASE_DATE));
    }

    public CalendarPicker getStartDatePicker() {
        return pageBinder.bind(CalendarPicker.class, row, VERSION_START_DATE_PICKER_ID);
    }

    public CalendarPicker getReleaseDatePicker() {
        return pageBinder.bind(CalendarPicker.class, row, VERSION_RELEASE_DATE_PICKER_ID);
    }
}
