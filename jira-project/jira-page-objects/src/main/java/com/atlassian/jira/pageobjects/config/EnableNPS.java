package com.atlassian.jira.pageobjects.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * By default NPS surveys in JIRA will be disabled before each test,
 * as per {@link com.atlassian.jira.pageobjects.config.junit4.rule.NPSRule}.
 *
 * Mark a test method or class with this annotation to enable the NPS plugin,
 * thus enabling gathering product feedback for given test methods.
 *
 * @since v7.2
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface EnableNPS {
}
