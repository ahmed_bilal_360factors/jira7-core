package com.atlassian.jira.pageobjects.components.license;


import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

import javax.annotation.Nonnull;

/**
 * @since v6.3
 */
public class LicenseBanner extends LicenseContent {
    @ElementBy(id = "license-banner-content")
    private PageElement content;

    @Nonnull
    @Override
    protected PageElement getContent() {
        return content;
    }

    @Override
    protected PageElement getMacLink() {
        return getContent().find(By.id("license-banner-my-link"));
    }

    @Override
    public void dismiss() {
        throw new UnsupportedOperationException("Banners can not be dismissed.  Review test logic may be wrong");
    }
}
