package com.atlassian.jira.pageobjects.onboarding;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import java.util.List;
import java.util.Locale;

public class ChooseLanguageSequence extends Sequence {
    @ElementBy(className = "onboarding-sequence-choose-language")
    private PageElement content;

    @ElementBy(id = "next", within = "content")
    private PageElement doneButton;

    @ElementBy(tagName = "h2", within = "content")
    private PageElement heading;

    @Override
    public TimedCondition isAt() {
        return content.timed().isVisible();
    }

    public void nextStep() {
        doneButton.click();
    }

    public void selectLanguage(final Locale locale) {
        final String localeString = locale.toString();
        final List<PageElement> localeOptions = content.findAll(By.name("locale"));
        PageElement optionToSelect = null;
        for (PageElement opt : localeOptions) {
            if (opt.hasAttribute("value", localeString)) {
                optionToSelect = opt;
                break;
            }
        }
        if (null == optionToSelect) {
            throw new NoSuchElementException("The options available did not include a language with locale " + localeString);
        }
        optionToSelect.select();
    }

    public String getPageHeadingText() {
        return heading.getText();
    }
}
