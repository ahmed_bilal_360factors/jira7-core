package com.atlassian.jira.pageobjects.pages.admin.customfields;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

/**
 * Represents edit version order page
 */
public class EditVersionCustomFieldOrder extends AbstractJiraPage {
    private static final String UNRELEASED_FIRST = "Unreleased versions first";

    private final String customFieldId;
    private PageElement versionPrioritySelect;

    public EditVersionCustomFieldOrder(final String customFieldId) {
        this.customFieldId = customFieldId;
    }

    @Init
    public void init() {
        this.versionPrioritySelect = body.find(By.id(customFieldId + "_select"));
    }

    @Override
    public String getUrl() {
        return "/secure/admin/EditVersionPickerCustomFieldOptionsOrder!default.jspa";
    }

    @Override
    public TimedCondition isAt() {
        return body.find(By.id(customFieldId + "_select")).timed().isPresent();
    }

    public void setUnreleasedFirst() {
        PageElement submitButton = body.find(By.id("config_version_type_order_config"));
        Poller.waitUntilTrue(submitButton.timed().isPresent());
        versionPrioritySelect.type(UNRELEASED_FIRST);
        submitButton.click();
    }
}
