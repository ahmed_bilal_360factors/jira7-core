package com.atlassian.jira.pageobjects.project.types.warning;

import com.atlassian.jira.pageobjects.pages.admin.ChangeProjectTypeDialog;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import org.openqa.selenium.By;

import javax.inject.Inject;

/**
 * Page object representing the dialog for the warning icon displayed when a project type is inaccessible.
 */
public class InaccessibleProjectTypeWarningDialog {
    @ElementBy(className = "project-type-warning-dialog")
    private PageElement warningDialog;

    @Inject
    private PageBinder pageBinder;

    @WaitUntil
    public void isPresent() {
        Poller.waitUntilTrue("Inaccessible project type warning dialog never became visible", warningDialog.timed().isPresent());
    }

    /**
     * Returns a boolean indicating whether the messaging on the warning dialog is referring to the given project type.
     *
     * @param projectTypeName The name of the project type
     * @return Whether the messaging on the warning dialog is referring to the given project type.
     */
    public boolean isForProjectType(String projectTypeName) {
        return warningDialog.find(By.cssSelector(".header")).getText().contains(projectTypeName);
    }

    /**
     * Returns a boolean indicating whether the messaging on the warning dialog is referring to a project type that is
     * uninstalled.
     *
     * @return Whether the messaging on the warning dialog is referring to a project type that is uninstalled.
     */
    public boolean isForAnUninstalledProjectType() {
        return warningDialog.find(By.cssSelector(".type-not-accessible-message")).getText().contains("not installed");
    }

    /**
     * Returns a boolean indicating whether the messaging on the warning dialog is referring to a project type that is
     * unlicensed.
     *
     * @return Whether the messaging on the warning dialog is referring to a project type that is unlicensed.
     */
    public boolean isForAnUnlicensedProjectType() {
        return warningDialog.find(By.cssSelector(".type-not-accessible-message")).getText().contains("not licensed");
    }

    /**
     * Clicks on the "change project type" link inside the dialog.
     *
     * @return The page object for the change project type dialog.
     */
    public ChangeProjectTypeDialog clickChangeProjectType() {
        warningDialog.find(By.cssSelector(".warning-dialog-change-project-type")).click();
        return pageBinder.bind(ChangeProjectTypeDialog.class);
    }
}
