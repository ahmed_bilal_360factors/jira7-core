package com.atlassian.jira.pageobjects.pages.setup;

import com.atlassian.jira.pageobjects.components.fields.SingleSelect;
import com.atlassian.jira.pageobjects.config.ExternalDatabaseConfig;
import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import static java.util.concurrent.TimeUnit.MINUTES;
import static org.hamcrest.Matchers.is;

/**
 * Page object implementation for the database setup page in JIRA, the first step
 * in the JIRA setup.
 *
 * @since 4.4
 */
public class DatabaseSetupPage extends AbstractJiraPage {
    private static final String URI = "/secure/SetupDatabase!default.jspa";

    @ElementBy(id = "jira-setup-database-field-database-internal")
    private PageElement internalDbOption;

    @ElementBy(id = "jira-setup-database-field-database-external")
    private PageElement externalDbOption;

    @ElementBy(id = "jira-setup-database-submit")
    private PageElement submitButton;

    @ElementBy(id = "database-type-container")
    private PageElement databaseTypeParent;

    @ElementBy(id = "jira-setup-database-field-hostname")
    private PageElement databaseHostname;

    @ElementBy(id = "jira-setup-database-field-port")
    private PageElement databasePort;

    @ElementBy(id = "jira-setup-database-field-database-name")
    private PageElement databaseName;

    @ElementBy(id = "jira-setup-database-field-username")
    private PageElement username;

    @ElementBy(id = "jira-setup-database-field-password")
    private PageElement password;

    @ElementBy(id = "jira-setup-database-field-schema")
    private PageElement schema;

    private SingleSelect databaseTypeSelect;

    public String getUrl() {
        return URI;
    }

    @Override
    public TimedCondition isAt() {
        return internalDbOption.timed().isPresent();
    }

    public DatabaseSetupPage setInternalDb() {
        internalDbOption.click();
        return this;
    }

    public DatabaseSetupPage setExternalDb() {
        externalDbOption.select();
        return this;
    }

    /**
     * Submit this page selecting internal DB.
     *
     * @return next page in the setup process
     */
    public ApplicationSetupPage submitInternalDb() {
        setInternalDb();
        return submit();
    }

    /**
     * Submit this page selecting an external DB.
     *
     * @param externalDatabaseConfig    the configuration of the db
     * @return next page in the setup process
     */
    public ApplicationSetupPage submitExternalDb(ExternalDatabaseConfig externalDatabaseConfig) {
        setExternalDb();

        databaseTypeSelect = pageBinder.bind(SingleSelect.class, databaseTypeParent);
        databaseTypeSelect.select(externalDatabaseConfig.getDatabaseType());

        databaseHostname.clear().type(externalDatabaseConfig.getHostName());
        databasePort.clear().type(externalDatabaseConfig.getPort());
        databaseName.clear().type(externalDatabaseConfig.getDatabaseName());
        username.clear().type(externalDatabaseConfig.getUsername());
        password.clear().type(externalDatabaseConfig.getPassword());
        schema.clear().type(externalDatabaseConfig.getSchema());

        return submit();
    }

    public ApplicationSetupPage submit() {
        submitButton.click();
        //waiting for plugin system to restart
        Poller.waitUntil(isAt(), is(false), Poller.by(5, MINUTES));
        return pageBinder.bind(ApplicationSetupPage.class);
    }

}
