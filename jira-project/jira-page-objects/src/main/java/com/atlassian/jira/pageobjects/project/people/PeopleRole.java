package com.atlassian.jira.pageobjects.project.people;

import java.util.List;
import javax.annotation.Nonnull;

/**
 * A role on the project configuration people tab. Interface to facilitate easy testing using assertEquals
 *
 * @since v4.4
 */
public interface PeopleRole {
    @Nonnull
    List<User> getUsers();

    @Nonnull
    List<Group> getGroups();

    @Nonnull
    String getName();

    @Nonnull
    EditPeopleRoleForm edit(final String dataField);

    public interface User {
        String getName();
    }

    public interface Group {
        String getName();
    }
}
