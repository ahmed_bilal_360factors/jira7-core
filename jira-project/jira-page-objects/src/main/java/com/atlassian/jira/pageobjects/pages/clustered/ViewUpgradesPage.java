package com.atlassian.jira.pageobjects.pages.clustered;

import com.atlassian.jira.functest.framework.backdoor.ZeroDowntimeControl;
import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.AbstractTimedQuery;
import com.atlassian.pageobjects.elements.query.ExpirationHandler;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static org.hamcrest.Matchers.is;

/**
 * @since v7.3
 */
public class ViewUpgradesPage extends AbstractJiraPage {
    @Inject
    private PageBinder binder;

    @Inject
    private Timeouts timeouts;
    
    @Inject
    private TraceContext traceContext;
    
    public static class ConfirmDialog {
        @Inject
        private PageBinder binder;
        @Inject
        protected PageElementFinder elementFinder;
        @ElementBy(id = "dialog-confirm-button")
        private PageElement confirmButton;

        private final String id;

        public ConfirmDialog(final String id) {
            this.id = id;
        }

        @WaitUntil
        public void bind() {
            Poller.waitUntil(elementFinder.find(By.id(id)).timed().getAttribute("aria-hidden"), is("false"));
        }

        public ViewUpgradesPage confirm() {
            confirmButton.click();
            Poller.waitUntil(elementFinder.find(By.id(id)).timed().getAttribute("aria-hidden"), is("true"));
            return binder.bind(ViewUpgradesPage.class);
        }
    }



    @ElementBy(cssSelector = "#page-intro-container h2")
    private PageElement pageHeading;

    @ElementBy(id = "page-intro-documentation-link")
    private PageElement introDocumentationLink;

    @ElementBy(id = "cluster-health-check-link")
    private PageElement viewHealthChecksLink;

    @ElementBy(id = "plugin-compatibility-check-addons-link")
    private PageElement checkCompatibilityLink;

    @ElementBy(id = "product-versions-download-latest-link")
    private PageElement downloadLatestLink;

    @ElementBy(id = "cluster-state-lozenge")
    private PageElement clusterState;

    @ElementBy(id = "start-update-button")
    private PageElement startUpdateButton;

    @ElementBy(id = "finish-update-button")
    private PageElement finishUpdateButton;

    @ElementBy(id = "cancel-update-button")
    private PageElement cancelUpdateButton;

    public void clickIntroDocumentationLink() {
        introDocumentationLink.click();
    }

    public void clickViewHealthCheckLink() {
        viewHealthChecksLink.click();
    }

    public void clickCheckAddonsLink() {
        checkCompatibilityLink.click();
    }

    public void clickDownloadLatestLink() {
        downloadLatestLink.click();
    }

    public void startUpgrade() {
        Tracer checkpoint = traceContext.checkpoint();
        startUpdateButton.click();
        binder.bind(ConfirmDialog.class, "start-dialog")
                .confirm();
        traceContext.waitFor(checkpoint, "jira.zdu.started");
    }

    public void finishUpgrade() {
        Tracer checkpoint = traceContext.checkpoint();
        finishUpdateButton.click();
        binder.bind(ConfirmDialog.class, "finish-dialog")
                .confirm();
        traceContext.waitFor(checkpoint, "jira.zdu.finished");
    }

    public void cancelUpgrade() {
        Tracer checkpoint = traceContext.checkpoint();
        finishUpdateButton.click();
        binder.bind(ConfirmDialog.class, "cancel-dialog")
                .confirm();
        traceContext.waitFor(checkpoint, "jira.zdu.cancelled");
    }

    public TimedQuery<ZeroDowntimeControl.UpgradeState> getUpgradeState() {
        return new AbstractTimedQuery<ZeroDowntimeControl.UpgradeState>(timeouts.timeoutFor(TimeoutType.DEFAULT), Timeouts.DEFAULT_INTERVAL, ExpirationHandler.RETURN_CURRENT) {
            @Override
            protected boolean shouldReturn(final ZeroDowntimeControl.UpgradeState upgradeState) {
                return upgradeState != null;
            }

            @Override
            protected ZeroDowntimeControl.UpgradeState currentValue() {
                String text = clusterState.getText();
                if (text != null) {
                    try {
                        return ZeroDowntimeControl.UpgradeState.valueOf(text.toUpperCase().replace(' ', '_'));
                    } catch (IllegalArgumentException e) {
                        return null;
                    }
                }
                return null;
            }
        };
    }

    @Override
    public TimedCondition isAt() {
        return pageHeading.timed().hasText("JIRA upgrades");
    }

    @Override
    public String getUrl() {
        return "/secure/ViewUpgrades!default.jspa";
    }
}
