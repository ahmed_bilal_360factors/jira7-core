package com.atlassian.jira.pageobjects.pages.project;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;

import java.util.StringJoiner;

abstract class AbstractBrowseProjectsPage extends AbstractJiraPage {
    final String selectedCategory;
    final String containsFilter;
    final int page;

    public AbstractBrowseProjectsPage() {
        this(null, null, 0);
    }

    public AbstractBrowseProjectsPage(final String selectedCategory, final String containsFilter, final int page) {
        this.selectedCategory = selectedCategory;
        this.containsFilter = containsFilter;
        this.page = page;
    }

    @Override
    public String getUrl() {
        return "/secure/BrowseProjects.jspa" + getParams();
    }

    private String getParams() {
        final StringJoiner params = new StringJoiner("&", "?", "");
        params.setEmptyValue("");

        if (selectedCategory != null && !selectedCategory.isEmpty()) {
            params.add("selectedCategory=" + selectedCategory);
        }
        if (containsFilter != null && !containsFilter.isEmpty()) {
            params.add("contains=" + containsFilter);
        }
        if (page > 0) {
            params.add("page=" + page);
        }

        return params.toString();
    }
}
