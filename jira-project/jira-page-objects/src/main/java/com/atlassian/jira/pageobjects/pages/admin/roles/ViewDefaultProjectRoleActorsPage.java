package com.atlassian.jira.pageobjects.pages.admin.roles;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import org.openqa.selenium.By;

import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.transform;

/**
 * Page object representing the Edit Members for Project Role page
 *
 * @since v5.2
 */
public class ViewDefaultProjectRoleActorsPage extends AbstractJiraPage {
    private static final String URI = "/secure/project/ViewDefaultProjectRoleActors.jspa";
    private static final Function<PageElement, String> INNER_TEXT =
            pageElement -> pageElement == null ? null : pageElement.getText();

    private final String roleId;

    @ElementBy(id = "role_actors")
    protected PageElement rowsContainer;

    public ViewDefaultProjectRoleActorsPage(final String pageId) {
        this.roleId = pageId;
    }

    @Override
    public TimedCondition isAt() {
        return rowsContainer.timed().isPresent();
    }

    @Override
    public String getUrl() {
        return URI + "?id=" + roleId;
    }

    protected <T> T editActors(final Class<T> classType, final String linkId) {
        rowsContainer.find(By.id(linkId)).click();
        return pageBinder.bind(classType, roleId);
    }

    public UserRoleActorActionPage editUserActors() {
        return editActors(UserRoleActorActionPage.class, String.format("edit_%s_atlassian-user-role-actor", roleId));
    }

    public GroupRoleActorActionPage editGroupActors() {
        return editActors(GroupRoleActorActionPage.class, String.format("edit_%s_atlassian-group-role-actor", roleId));
    }

    public Iterable<String> getDefaultUserRoleActors() {
        return transform(filter(spanElementsInTable(), idSuffix("-user-role-actor")), INNER_TEXT);
    }

    public Iterable<String> getDefaultGroupRoleActors() {
        return transform(filter(spanElementsInTable(), idSuffix("-group-role-actor")), INNER_TEXT);
    }

    public PageElement getDefaultUserRoleActorsElement() {
        return find(spanElementsInTable(), idSuffix("-user-role-actor"));
    }

    public PageElement getDefaultGroupRoleActorsElement() {
        return find(spanElementsInTable(), idSuffix("-group-role-actor"));
    }

    private Iterable<PageElement> spanElementsInTable() {
        return rowsContainer.findAll(By.tagName("span"));
    }

    private Predicate<PageElement> idSuffix(final String suffix) {
        return pageElement -> pageElement != null && pageElement.getAttribute("id").endsWith(suffix);
    }
}
