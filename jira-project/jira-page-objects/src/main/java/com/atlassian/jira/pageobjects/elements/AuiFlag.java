package com.atlassian.jira.pageobjects.elements;

import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

import javax.inject.Inject;

/**
 * Represents an individual AUI notification flag.
 *
 * @since v6.4
 */
public class AuiFlag {
    private final PageElement flagElement;

    @Inject
    private TraceContext context;

    public enum Type {
        SUCCESS,
        INFO,
        WARNING,
        ERROR,
        UNKNOWN
    }

    public AuiFlag(final PageElement flagElement) {
        this.flagElement = flagElement;
    }

    public String getMessage() {
        return this.flagElement.getText();
    }

    public void dismiss() {
        if (flagElement.isVisible()) {
            flagElement.find(By.className("icon-close")).click();
        }
    }

    /**
     * Dismiss the flag and wait until the server has responded.
     *
     * @param key the flag dismissal key
     */
    public void dismissWait(String key) {
        final Tracer checkpoint = context.checkpoint();
        dismiss();
        context.waitFor(checkpoint, "flag.dismiss.finished:" + key);
    }

    /**
     * Does it have a X that when clicked dismisses message
     *
     * @return if closeable or not
     */
    public boolean isCloseable() {
        return this.flagElement.find(By.className("icon-close")).isPresent();
    }

    public Type getType() {
        final PageElement messageElement = getMessageElement();
        if (messageElement.hasClass("success")) {
            return Type.SUCCESS;
        } else if (messageElement.hasClass("info")) {
            return Type.INFO;
        } else if (messageElement.hasClass("warning")) {
            return Type.WARNING;
        } else if (messageElement.hasClass("error")) {
            return Type.ERROR;
        }
        return Type.UNKNOWN;
    }

    private PageElement getMessageElement() {
        return flagElement.find(By.className("aui-message"));
    }

    public PageElement getFlagElement() {
        return flagElement;
    }

    public AuiMessage toMessage() {
        return new AuiMessage(getMessageElement());
    }
}
