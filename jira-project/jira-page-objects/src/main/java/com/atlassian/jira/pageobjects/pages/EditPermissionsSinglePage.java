package com.atlassian.jira.pageobjects.pages;

import com.atlassian.jira.pageobjects.components.fields.SingleSelect;
import com.atlassian.jira.pageobjects.elements.AuiSelect2;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.pageobjects.elements.AuiFlag;
import com.atlassian.jira.pageobjects.elements.GlobalFlags;
import com.atlassian.jira.pageobjects.project.ProjectSharedBy;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.WebDriverElement;
import com.atlassian.pageobjects.elements.WebDriverLocatable;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Represents Page Object for page of
 * "/secure/admin/EditPermissions.jspa".
 *
 * @since v7.1
 */
public class EditPermissionsSinglePage extends EditPermissionsPage {
    @Inject
    private PageElementFinder pageElementFinder;

    @ElementBy(cssSelector = ".project-permissions-container .aui-page-header-inner")
    private PageElement pageHeader;

    // as the name suggests, this is a plugin point referenced by other plugins to locate the element
    // "fixing" tests by changing the selector string means we're breaking the API
    @ElementBy(cssSelector = ".project-permissions-container .admin-header-actions-plugin-point")
    private PageElement headerPluginPoint;

    @ElementBy(cssSelector = ".project-permissions-container .js-grant-permission-trigger")
    private PageElement grantPermissionButton;

    @ElementBy(className = "shared-by")
    private PageElement sharedBy;

    public EditPermissionsSinglePage(int schemeId) {
        super(schemeId);
    }

    /**
     * Created so it would work if we use the FuncTestConstants.DEFAULT_PERM_SCHEME_ID for example
     */
    public EditPermissionsSinglePage(long schemeId) {
        super((int) schemeId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimedCondition isAt() {
        return pageHeader.timed().isPresent();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUrl() {
        return getPageUrl(getSchemeId());
    }

    /**
     * Used to be able to get the page url for this given page type. This is used for testing
     * and redirects to check the url is correct.
     *
     * @param schemeId of a the page
     * @return url of page
     */
    public static String getPageUrl(int schemeId) {
        return "/secure/admin/EditPermissions!default.jspa?schemeId=" + schemeId;
    }

    /**
     * Determines whether an entry exists for the specified permission.
     *
     * @param permission name of permission
     * @return boolean value indicating whether the permission exists
     **/
    public boolean hasPermissionEntry(final String permission) {
        return getPermissionEntry(permission).isPresent();
    }

    /**
     * Determines whether an entry exists for the specified permission.
     *
     * @param projectPermissionKey key of the permission
     * @return boolean value indicating whether the permission exists
     */
    public boolean hasPermissionEntry(final ProjectPermissionKey projectPermissionKey) {
        return hasPermissionEntry(projectPermissionKey.permissionKey());
    }

    /**
     * Get the container for a given permission.
     *
     * @param permissionKey key of permission
     * @return {@link com.atlassian.jira.pageobjects.pages.EditPermissionsSinglePage.PermissionsEntry}
     **/
    public PermissionsEntry getPermissionEntry(final String permissionKey) {
        return getPermissionsContainer().find(
                By.xpath("//tr[@data-permission-key='" + permissionKey + "']"),
                PermissionsEntry.class
        );
    }

    /**
     * Get the container for a given permission key
     *
     * @param projectPermissionKey for the permission
     * @return {@link com.atlassian.jira.pageobjects.pages.EditPermissionsSinglePage.PermissionsEntry}
     */
    public PermissionsEntry getPermissionEntry(final ProjectPermissionKey projectPermissionKey) {
        return getPermissionEntry(projectPermissionKey.permissionKey());
    }

    public boolean hasHeaderPluginPoint() {
        return headerPluginPoint.timed().isPresent().now();
    }

    public boolean hasGrantPermissionButton() {
        return grantPermissionButton.timed().isPresent().now();
    }

    public GrantPermissionsDialog openDialogFromGrantPermissionButton() {
        grantPermissionButton.click();
        return pageElementFinder.find(By.cssSelector("#grant-project-permission-popup"), GrantPermissionsDialog.class);
    }

    public ProjectSharedBy getSharedBy() {
        return pageBinder.bind(ProjectSharedBy.class, sharedBy);
    }

    public boolean isSingleUserPermDisplayedAsInvalidUser(String permissionKey, String username) {
        return pageElementFinder.find(By.xpath(".//tr[@data-permission-key='" + permissionKey + "']/td[@class='grants']/dl/dd[contains(text(), '" + username + "')][@class='invalid-user']")).timed().isPresent().now();
    }

    public boolean hasInvalidUserWarningForSection(String sectionTitle) {
        return pageElementFinder.find(By.xpath(".//div[@class='permissions-group']/div[@class='aui-message aui-message-warning warning']/preceding-sibling::h4[contains(text(), '" + sectionTitle + "')]")).timed().isPresent().now();
    }

    public boolean invalidUserWarningMessageContains(String permissionKey, String username) {
        return pageElementFinder.find(By.xpath(".//div[@class='permissions-group']/div[@class='aui-message aui-message-warning warning']/p/strong[contains(text(), '" + permissionKey + "')]/parent::p[contains(text(), '" + username + "')]")).timed().isPresent().now();
    }

    /**
     * Dialog to grant permissions
     * NOTE: This page object assumes the locale was set to EN_MOON
     */
    public static final class GrantPermissionsDialog extends WebDriverElement {
        @ElementBy(cssSelector = "#security-type-list-more-opts-btn")
        private PageElement showMoreOrShowLessLink;

        @ElementBy(cssSelector = "#grant-permission-dialog-grant-button")
        private PageElement submitButton;

        @ElementBy(cssSelector = "#grant-permission-form .radio.secondary")
        private PageElement secondaryOption;

        @ElementBy(cssSelector = "#permission-target-select")
        private PageElement searchField;

        private SingleSelect groupSingleSelect;
        private SingleSelect userSingleSelect;
        private AuiSelect2 permissionSelect;

        public GrantPermissionsDialog(final By locator) {
            super(locator);
        }

        public void showMore() {
            if (secondaryOption.timed().isVisible().now()) {
                // nothing to do, show more is already active
                return;
            }

            showMoreOrShowLessLink.click();

            // ensure one of the "secondary" options is visible
            Poller.waitUntilTrue(secondaryOption.timed().isVisible());
        }

        public void setSingleUser(final String user) {
            showMore();

            find(By.cssSelector("#user-security-type")).click();

            userSingleSelect = pageBinder.bind(SingleSelect.class, find(By.cssSelector("#user-single-select")).find(By.xpath("..")));
            userSingleSelect.select(user);
        }

        public void setGroup(final String usersGroup) {
            find(By.cssSelector("#group-security-type")).click();

            groupSingleSelect = pageBinder.bind(SingleSelect.class, find(By.cssSelector("#group-single-select")).find(By.xpath("..")));
            groupSingleSelect.select(usersGroup);
        }

        public void setUserCF(final String userCF) {
            showMore();

            find(By.cssSelector("#userCF-security-type")).click();

            SingleSelect userCFSelect = pageBinder.bind(SingleSelect.class, find(By.cssSelector("#userCF-single-select")).find(By.xpath("..")));
            userCFSelect.select(userCF);
        }

        public void selectSecurityTypeWithNoInputField(final String securityType) {
            find(By.xpath(".//label[text() = '" + securityType + "']/preceding-sibling::input")).click();
        }

        public void setPermissionName(final String permissionName) {
            permissionSelect = pageBinder.bind(AuiSelect2.class, searchField);
            permissionSelect.add(permissionName);
        }

        public String getPermissionName() {
            permissionSelect = pageBinder.bind(AuiSelect2.class, searchField);
            return permissionSelect.getFieldText();
        }

        public void submit() {
            submitButton.click();
        }

        public void submitAssertSuccessful() {
            submit();

            // waits until the request is successful
            GlobalFlags flags = pageBinder.bind(GlobalFlags.class);
            Poller.waitUntilTrue(flags.flagContainerPresent());

            AuiFlag successFlag = flags.getFlagWithText("admin.permissions.feedback.successfulgrant");
            assertNotNull("flag notifying of successful operation should be present", successFlag);
            assertEquals("flag should be a success", AuiFlag.Type.SUCCESS, successFlag.getType());
        }
    }

    /**
     * Dialog to revoke permissions
     * NOTE: This page object assumes the locale was set to EN_MOON
     */
    public static final class RevokePermissionsDialog extends WebDriverElement {
        @ElementBy(cssSelector = "#dialog-save-button")
        private PageElement submitButton;

        public RevokePermissionsDialog(final By locator) {
            super(locator);
        }

        public void markForRemoval(final String usersGroup) {
            // ensure all other permissions are unchecked
            final List<PageElement> selectedPermissions = findAll(By.cssSelector("input:checked[type='checkbox']"));
            selectedPermissions.stream().forEach(PageElement::click);

            // now, let's find the correct permission and check it
            find(By.xpath(".//span[contains(text(), '" + usersGroup + "')]/parent::label/preceding-sibling::input")).click();
        }

        public void submit() {
            submitButton.click();

            // waits until the request is successful
            GlobalFlags flags = pageBinder.bind(GlobalFlags.class);
            Poller.waitUntilTrue(flags.flagContainerPresent());

            AuiFlag successFlag = flags.getFlagWithText("admin.permissions.feedback.successfuldelete");
            assertNotNull("flag notifying of successful operation should be present", successFlag);
            assertEquals("flag should be a success", AuiFlag.Type.SUCCESS, successFlag.getType());
        }

        public boolean isInvalidUserWarningShownFor(String username) {
            return find(By.xpath(".//span[contains(text(), '" + username + "')]/parent::label/preceding-sibling::a[contains(@class, 'invalid-user-warning-icon')]")).timed().isPresent().now();
        }

        public boolean isUsernameDisplayedAsInvalid(String username) {
            return find(By.xpath(".//span[contains(text(), '" + username + "')][@class='invalid-user']")).timed().isPresent().now();
        }
    }

    /**
     * Permissions entry (inside permissions container)
     * NOTE: This page object assumes the locale was set to EN_MOON
     */
    public static final class PermissionsEntry extends WebDriverElement {
        @Inject
        private PageElementFinder pageElementFinder;

        @ElementBy(cssSelector = ".aui-button.add-trigger")
        private PageElement addButton;

        @ElementBy(cssSelector = ".aui-button.delete-trigger")
        private PageElement deleteButton;

        public PermissionsEntry(final By locator, final WebDriverLocatable parent) {
            super(locator, parent);
        }

        public GrantPermissionsDialog openGrantPermissionDialog() {
            addButton.click();

            return pageElementFinder.find(By.cssSelector("#grant-project-permission-popup"), GrantPermissionsDialog.class);
        }

        public RevokePermissionsDialog openRevokePermissionDialog() {
            deleteButton.click();

            return pageElementFinder.find(By.cssSelector("#delete-permission-dialog"), RevokePermissionsDialog.class);
        }

        public boolean hasPermissionForGroup(final String usersGroup) {
            final PageElement groupPermissionsContainer = find(By.xpath(".//dt[text()='admin.permission.types.group']/.."));

            return Conditions.and(
                    groupPermissionsContainer.timed().isPresent(),
                    groupPermissionsContainer.find(By.xpath(".//dd[text()='" + usersGroup + "']")).timed().isPresent()
            ).now();
        }

        public boolean hasSecurityTypeWithNoInputField(final String securityType) {
            return find(By.xpath(".//dt[text()='" + securityType + "']/..")).timed().isPresent().now();
        }
    }
}
