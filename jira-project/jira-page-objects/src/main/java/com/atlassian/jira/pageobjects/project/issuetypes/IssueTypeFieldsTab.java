package com.atlassian.jira.pageobjects.project.issuetypes;

import com.atlassian.jira.pageobjects.components.ScreenEditor;
import com.atlassian.jira.pageobjects.project.ProjectConfigPageTab;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import javax.inject.Inject;

/**
 * Represents the Issue Type Config tab with the screen editor.
 *
 * @since v6.2
 */
public class IssueTypeFieldsTab extends IssueTypeTab implements ProjectConfigPageTab {
    @Inject
    private PageBinder binder;

    @ElementBy(id = "screeneditor-container")
    private PageElement screenEditorPanel;

    private ScreenEditor screenEditor;
    private final String projectKey;
    private final long issueTypeId;

    public IssueTypeFieldsTab() {
        projectKey = null;
        issueTypeId = -1;
    }

    public IssueTypeFieldsTab(String projectKey, long issueTypeId) {
        this.projectKey = projectKey;
        this.issueTypeId = issueTypeId;
    }

    @Init
    private void bindElements() {
        screenEditor = binder.bind(ScreenEditor.class);
    }

    @Override
    public TimedCondition isAt() {
        final LoadingBarrier barrier = pageBinder.bind(LoadingBarrier.class);
        return Conditions.and(barrier.notLoading(), screenEditorPanel.timed().isPresent());
    }

    public IssueTypeWorkflowTab gotoWorkflow() {
        return getIssueTypeHeader().gotoWorkflow();
    }

    @Override
    public String getUrl() {
        if (projectKey == null || issueTypeId < 0) {
            throw new IllegalStateException("Need project and issue type to go direct to URL.");
        }

        return String.format("/plugins/servlet/project-config/%s/issuetypes/%d/fields", projectKey, issueTypeId);
    }

    public ScreenEditor getScreenEditor() {
        return screenEditor;
    }
}
