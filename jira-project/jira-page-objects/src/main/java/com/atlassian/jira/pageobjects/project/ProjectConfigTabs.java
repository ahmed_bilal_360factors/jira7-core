package com.atlassian.jira.pageobjects.project;

import com.atlassian.jira.pageobjects.project.components.ComponentsPageTab;
import com.atlassian.jira.pageobjects.project.issuetypes.IssueTypeFieldsTab;
import com.atlassian.jira.pageobjects.project.issuetypes.IssueTypeHeader;
import com.atlassian.jira.pageobjects.project.issuetypes.IssueTypeTab;
import com.atlassian.jira.pageobjects.project.issuetypes.IssueTypeWorkflowTab;
import com.atlassian.jira.pageobjects.project.issuetypes.IssueTypesTab;
import com.atlassian.jira.pageobjects.project.permissions.ProjectPermissionPageTab;
import com.atlassian.jira.pageobjects.project.summary.ProjectSummaryPageTab;
import com.atlassian.jira.pageobjects.project.versions.VersionPageTab;
import com.atlassian.jira.projects.pageobjects.webdriver.page.AdministerReleasePage;
import com.atlassian.jira.projects.pageobjects.webdriver.page.ReleasePage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;

import static org.junit.Assert.assertTrue;

/**
 * Represents the tabs on the project configuration page.
 *
 * @since v4.4
 */
public class ProjectConfigTabs {
    @Inject
    private PageBinder pageBinder;

    @Inject
    private PageElementFinder elementFinder;

    private ProjectInfoLocator locator;

    @Init
    public void init() {
        locator = pageBinder.bind(ProjectInfoLocator.class);
    }

    public String getProjectKey() {
        return locator.getProjectKey();
    }

    public List<Tab> getTabs() {
        PageElement container = getTabContainer();
        if (!container.isPresent()) {
            return Collections.emptyList();
        }

        List<PageElement> elements = container.findAll(getMenuItemsSelector());
        List<Tab> tabs = new ArrayList<Tab>(elements.size());
        for (PageElement element : elements) {
            tabs.add(new Tab(element));
        }
        return tabs;
    }

    private By getMenuItemsSelector() {
        return By.cssSelector(".aui-nav a");
    }

    public Tab getSelectedTab() {
        if (!getTabContainer().isPresent()) {
            return null;
        }

        PageElement activeTab = findActiveTab();
        if (!activeTab.isPresent()) {
            return null;
        } else {
            return new Tab(activeTab);
        }
    }

    private PageElement findActiveTab() {
        return getTabContainer().find(getActiveTabSelector());
    }

    private By getActiveTabSelector() {
        return By.cssSelector(".aui-nav-selected > a");
    }

    public boolean isSummaryTabSelected() {
        return isTabSelected(ProjectSummaryPageTab.TAB_LINK_ID);
    }

    public ProjectSummaryPageTab gotoSummaryTab() {
        return gotoTab(ProjectSummaryPageTab.TAB_LINK_ID, ProjectSummaryPageTab.class, getProjectKey());
    }

    public boolean isVersionsTabSelected() {
        return isTabSelected(VersionPageTab.TAB_LINK_ID);
    }

    public AdministerReleasePage gotoVersionsTab() {
        gotoTab(VersionPageTab.TAB_LINK_ID);
        return pageBinder.bind(AdministerReleasePage.class, getProjectKey());
    }

    public boolean isComponentsTabSelected() {
        return isTabSelected(ComponentsPageTab.TAB_LINK_ID);
    }

    public ComponentsPageTab gotoComponentsTab() {
        return gotoTab(ComponentsPageTab.TAB_LINK_ID, ComponentsPageTab.class, getProjectKey());
    }

    public boolean isIssueTypesTabSelected() {
        return isTabSelected(IssueTypesTab.TAB_LINK_ID);
    }

    public IssueTypesTab gotoIssueTypesTab() {
        return gotoTab(IssueTypesTab.TAB_LINK_ID, IssueTypesTab.class, getProjectKey());
    }

    public boolean isIssueTypeTabSelected(long issueTypeId) {
        return isTabSelected(IssueTypeTab.getTabLinkForIssueTypeId(issueTypeId));
    }

    public IssueTypeWorkflowTab gotoIssueTypeWorkflowTab(long issueTypeId) {
        return gotoIssueTypeTab(issueTypeId, IssueTypeHeader.Mode.WORKFLOW);
    }

    public IssueTypeFieldsTab gotoIssueTypeFieldsTab(long issueTypeId) {
        return gotoIssueTypeTab(issueTypeId, IssueTypeHeader.Mode.FIELDS);
    }

    public <T extends IssueTypeTab> T gotoIssueTypeTab(final long issueTypeId, final IssueTypeHeader.Mode<T> mode) {
        gotoTab(IssueTypeTab.getTabLinkForIssueTypeId(issueTypeId));
        final IssueTypeHeader header = pageBinder.bind(IssueTypeHeader.class);
        return header.gotoMode(mode);
    }

    public boolean isProjectPermissionsTabSelected() {
        return isTabSelected(ProjectPermissionPageTab.TAB_LINK_ID);
    }

    public ProjectPermissionPageTab gotoProjectPermissionsTab() {
        return gotoTab(ProjectPermissionPageTab.TAB_LINK_ID, ProjectPermissionPageTab.class, getProjectKey());
    }

    public <T extends ProjectConfigPageTab> T gotoTab(String linkId, Class<T> type, Object... args) {
        gotoTab(linkId);
        return pageBinder.bind(type, args);
    }

    private void gotoTab(final String linkId) {
        assertTabContainer();
        Tab selectedTab = getSelectedTab();
        if (!selectedTab.getId().equals(linkId)) {
            PageElement element = getTabContainer().find(By.id(linkId));
            assertTrue("Tab with id '" + linkId + "' does not exist.", element.isPresent());
            element.click();
        }
    }

    public boolean isTabSelected(String linkId) {
        Tab tab = getSelectedTab();
        return tab != null && tab.getId().equals(linkId);
    }

    private void assertTabContainer() {
        assertTrue("No tabs present on page.", getTabContainer().isPresent());
    }

    private PageElement getTabContainer() {
        return elementFinder.find(getTabContainerSelector());
    }

    private By getTabContainerSelector() {
        return By.cssSelector(".page-type-admin #content .admin-menu-links");
    }

    public static class Tab {
        private final PageElement link;

        private Tab(PageElement link) {
            this.link = link;
        }

        public String getUrl() {
            return link.getAttribute("href");
        }

        public String getId() {
            return link.getAttribute("id");
        }

        public String getName() {
            return link.getText();
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                    .append("url", getUrl())
                    .append("id", getId())
                    .append("name", getName())
                    .toString();
        }
    }
}
