package com.atlassian.jira.webtest.webdriver.util;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

import static com.atlassian.pageobjects.elements.query.Conditions.or;

/**
 * @since v6.4
 */
public class AUIFlags {
    private static final String ID = "disable-aui-flag-animations";

    @Inject
    protected PageElementFinder finder;

    @Inject
    protected JiraTestedProduct jiraProduct;

    public void closeAllFlags() {
        disableFlagAnimations();
        try {
            final List<PageElement> elements = finder.findAll(By.className("aui-flag"));

            // Close all flags through JS, so flags without a 'close' button are dismissed as well.
            getExecutor().executeScript("AJS.$('.aui-flag').each(function(it) { this.close(); } );");

            if (elements.size() > 0) {
                Poller.waitUntilFalse(or(elements.stream()
                        .map(p -> p.timed().isVisible())
                        .collect(Collectors.toList())));
            }
        } finally {
            enableFlagAnimations();
        }
    }

    public void disableFlagAnimations() {
        getExecutor().executeScript("var el = document.getElementById('" + ID + "');" +
                "if (!el) {" +
                "  el = document.createElement('style');" +
                "  el.id = '"+ID+"';" +
                "  el.innerHTML = '.aui-flag {  " +
                "   transition-property: none !important;" +
                "   -o-transition-property: none !important;" +
                "   -moz-transition-property: none !important;" +
                "   -ms-transition-property: none !important;" +
                "   -webkit-transition-property: none !important;" +
                "   transform: none !important;" +
                "   -o-transform: none !important;" +
                "   -moz-transform: none !important;" +
                "   -ms-transform: none !important;" +
                "   -webkit-transform: none !important;" +
                "   animation: none !important;" +
                "   -o-animation: none !important;" +
                "   -moz-animation: none !important;" +
                "   -ms-animation: none !important;" +
                "   -webkit-animation: none !important;" +
                "  }';" +
                "  document.body.appendChild(el);" +
                "}");
    }

    public void enableFlagAnimations() {
        getExecutor().executeScript("var el = document.getElementById('" + ID + "');" +
                " if (el) { document.body.removeChild(el); }");
    }

    private JavascriptExecutor getExecutor() {
        return ((JavascriptExecutor)jiraProduct.getTester().getDriver().getDriver());
    }

}
