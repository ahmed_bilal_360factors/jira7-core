package com.atlassian.jira.pageobjects.project.roles.roleselector;

import com.atlassian.jira.pageobjects.components.DropDown;
import org.openqa.selenium.By;

public class RoleSelector extends DropDown {
    public RoleSelector(String containerClassName, String dropdownClassName) {
        super(By.cssSelector("." + containerClassName + " .select2-choice"), By.className(dropdownClassName));
    }

    public void select(String roleName) {
        open();
        dropDown().findAll(By.cssSelector(".select2-results li"))
                .stream()
                .filter(option -> option.getText().equals(roleName))
                .findFirst()
                .get()
                .click();
        waitForClose();
    }

    public void selectAll() {
        select("All");
    }
}
