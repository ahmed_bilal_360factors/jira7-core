package com.atlassian.jira.pageobjects.email;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

public class NotificationMessage implements Page {
    private final String url;

    @ElementBy(className = "page-title-pattern-header")
    private PageElement pageTitle;

    public NotificationMessage(final String url) {
        this.url = url;
    }

    @Override
    public String getUrl() {
        return url;
    }

    public String getPageTitle() {
        return pageTitle.getText();
    }
}
