package com.atlassian.jira.pageobjects.pages.admin.cluster;

import com.atlassian.jira.pageobjects.dialogs.FormDialog;
import com.atlassian.jira.pageobjects.pages.AbstractJiraAdminPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.function.Consumer;

/**
 * Replication Settings Page
 *
 * @since v6.4
 */
public class ReplicationSettingsPage extends AbstractJiraAdminPage {
    public static class ReplicationSettingsDialog extends FormDialog {
        @Inject
        private PageBinder binder;

        public ReplicationSettingsDialog(final String id) {
            super(id);
        }

        public ReplicationSettingsDialog checkAll() {
            allCheckboxes(CheckboxElement::check);
            return this;
        }

        public ReplicationSettingsDialog uncheckAll() {
            allCheckboxes(CheckboxElement::uncheck);
            return this;
        }

        private void allCheckboxes(final Consumer<CheckboxElement> action) {
            getDialogElement().findAll(By.cssSelector("input.checkbox"), CheckboxElement.class).forEach(action);
        }

        public ReplicationSettingsPage update() {
            submit("Update");
            return binder.bind(ReplicationSettingsPage.class);
        }
    }

    private static final String URI = "/secure/admin/cluster/ReplicationSettings!default.jspa";

    @Inject
    private PageBinder binder;

    @Inject
    private Timeouts timeouts;

    @ElementBy(id="edit-replication-settings")
    private PageElement editButton;

    @ElementBy(id="replicate-submit")
    private PageElement replicateButton;

    @ElementBy(cssSelector=".aui-message.info")
    private PageElement replicationRunningMessage;

    @Override
    public String linkId() {
        return "replication";
    }

    @Override
    public TimedCondition isAt() {
        return elementFinder.find(By.id("ReplicationSettings")).timed().isPresent();
    }

    @Override
    public String getUrl() {
        return URI;
    }

    public ReplicationSettingsDialog editSettings() {
        editButton.click();
        return binder.bind(ReplicationSettingsDialog.class, "edit-replication-settings-dialog");
    }

    public ReplicationSettingsPage replicate() {
        replicateButton.click();
        return binder.bind(ReplicationSettingsPage.class);
    }

    public TimedCondition replicationRunning() {
        return replicationRunningMessage.timed().isPresent();
    }

    public ReplicationSettingsPage waitForReplication() {
        Poller.waitUntilFalse(new AbstractTimedCondition(timeouts.timeoutFor(TimeoutType.PAGE_LOAD), 1000L) {
            @Override
            protected Boolean currentValue() {
                ReplicationSettingsPage replicationSettingsPage = binder.navigateToAndBind(ReplicationSettingsPage.class);
                return replicationSettingsPage.replicationRunning().now();
            }
        });
        return binder.bind(ReplicationSettingsPage.class);
    }

}
