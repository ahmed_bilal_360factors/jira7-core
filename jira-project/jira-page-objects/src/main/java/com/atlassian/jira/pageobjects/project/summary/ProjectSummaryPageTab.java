package com.atlassian.jira.pageobjects.project.summary;

import com.atlassian.jira.pageobjects.project.AbstractProjectConfigPageTab;
import com.atlassian.jira.pageobjects.project.ProjectConfigPageTab;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;

/**
 * Represents the summary page. Use {@link #openPanel(Class)} to obtain objects for individual panels.
 *
 * @since v4.4
 */
public class ProjectSummaryPageTab extends AbstractProjectConfigPageTab implements ProjectConfigPageTab {
    public static final String TAB_LINK_ID = "view_project_summary";
    public static final String PROJECT_PANEL_SELECTOR = ".project-config-webpanel";

    private static final String URI_TEMPLATE = "/plugins/servlet/project-config/%s/summary";

    private final String uri;

    @ElementBy(id = "project-config-panel-summary")
    private PageElement projectConfigDiv;

    @ElementBy(id = "project-config-header")
    private PageElement projectConfigHeader;

    @ElementBy(id = "project-config-actions")
    private PageElement projectActions;

    @Override
    public TimedCondition isAt() {
        return projectConfigDiv.timed().isPresent();
    }

    public ProjectSummaryPageTab(final String projectKey) {
        this.uri = getUrlForProject(projectKey);
    }

    public ProjectSummaryPageTab() {
        this.uri = null;
    }

    @Override
    public String getUrl() {
        if (uri == null) {
            throw new IllegalStateException("Use the constructor with the project key argument.");
        }
        return uri;
    }

    public <T extends SummaryPanel> T openPanel(final Class<T> summaryPanelClass) {
        T bind = pageBinder.bind(summaryPanelClass);
        bind.setProject(projectInfoLocator.getProjectKey(), projectInfoLocator.getProjectId());
        return bind;
    }

    public static String getUrlForProject(String projectKey) {
        return String.format(URI_TEMPLATE, projectKey);
    }

    public EditWorkflowDiscoverInfo getEditWorkflowDiscoverInfo() {
        return pageBinder.bind(EditWorkflowDiscoverInfo.class);
    }

    public static class EditWorkflowDiscoverInfo {
        @Inject
        private TraceContext traceContext;

        @ElementBy(className = "workflow-edit-discover")
        PageElement element;

        public TimedCondition isPresent() {
            return this.element.timed().isPresent();
        }

        public TimedCondition isVisible() {
            return this.element.timed().isVisible();
        }

        public TimedCondition isCloseVisible() {
            return this.getClose().timed().isVisible();
        }

        public void close() {
            Tracer tracer = traceContext.checkpoint();
            this.getClose().click();
            waitUntilFalse(this.isVisible());
            traceContext.waitFor(tracer, "workflow.edit.summary.discover.dialog.preference.done");
        }

        private PageElement getClose() {
            return element.find(By.cssSelector(".discover-close button"));
        }
    }
}
