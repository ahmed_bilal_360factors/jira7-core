package com.atlassian.jira.pageobjects.config;

import javax.inject.Inject;

/**
 * Describes the database config that was provided to JIRA by the test runner
 *
 * @since v7.2
 * @see ExternalDatabaseConfig
 */
public class DatabaseConfigProvider {
    private static final String DEFAULT_DATABASE_TYPE = "H2";

    @Inject
    public DatabaseConfigProvider() {
    }

    public boolean isExternalConfigPresent() {
        final String setupDatabaseType = getSetupDatabaseType();
        return ! DEFAULT_DATABASE_TYPE.equals(setupDatabaseType);
    }

    public ExternalDatabaseConfig getExternalConfig() {
        if (!isExternalConfigPresent()) {
            throw new IllegalStateException("Cannot load external config when none is specified");
        }

        return new ExternalDatabaseConfig()
                .databaseType(getSetupDatabaseType())
                .hostName(System.getProperty("jira.setup.database.host.name", "localhost"))
                .port(System.getProperty("jira.setup.database.host.port"))
                .databaseName(System.getProperty("jira.setup.database.name"))
                .username(System.getProperty("jira.setup.database.username"))
                .password(System.getProperty("jira.setup.database.password"))
                .schema(System.getProperty("jira.setup.database.schema"));
    }

    private String getSetupDatabaseType() {
        return System.getProperty("jira.setup.database.type", DEFAULT_DATABASE_TYPE);
    }
}
