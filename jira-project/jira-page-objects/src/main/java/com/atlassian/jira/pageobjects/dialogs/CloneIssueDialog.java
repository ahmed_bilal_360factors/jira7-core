package com.atlassian.jira.pageobjects.dialogs;

import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.google.common.base.Preconditions.checkState;

/**
 * Page object for the clone issue dialog
 *
 * @since 7.2.0
 */
public class CloneIssueDialog extends FormDialog {
    private static final String DIALOG_ID = "clone-issue-dialog";
    private static final String SUBMIT_BUTTON_ID = "clone-issue-submit";

    public CloneIssueDialog() {
        super(DIALOG_ID);
    }

    public boolean hasCloneOptionForCustomField(String customFieldId) {
        PageElement cloneOption = findCloneOptionForCustomField(customFieldId);
        return cloneOption.isPresent() && cloneOption.isVisible();
    }

    public void selectCloneOptionForCustomField(String customFieldId) {
        checkState(hasCloneOptionForCustomField(customFieldId));

        PageElement cloneOption = findCloneOptionForCustomField(customFieldId);
        if (!cloneOption.isSelected()) {
            cloneOption.select();
            waitUntilTrue(cloneOption.timed().isSelected());
        }
    }

    private PageElement findCloneOptionForCustomField(String customFieldId) {
        return find(By.id(customFieldId + "-clone-option"));
    }

    public boolean cloneIssue() {
        return submit(By.id(SUBMIT_BUTTON_ID));
    }
}
