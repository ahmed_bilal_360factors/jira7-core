package com.atlassian.jira.pageobjects.pages.viewissue;

import com.atlassian.jira.pageobjects.components.userpicker.MentionsUserPicker;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedQuery;

import javax.inject.Inject;

/**
 * Represents edit comment dialog on view issue page.
 *
 * @since v5.0
 */
public class CreateIssueDescriptionField implements Mentionable {
    @Inject
    private PageBinder pageBinder;

    @ElementBy(name = "description")
    protected PageElement description;

    private PageElement item;

    public CreateIssueDescriptionField(final PageElement item) {
        this.item = item;
    }

    @Override
    public CreateIssueDescriptionField typeInput(CharSequence... text) {
       this.description
               // by default the summary field has focus - need to steal it!
               .click()
               // now we can type the input
               .type(text);
        return this;

    }

    @Override
    public String getInput() {
        return this.description.getValue();
    }

    @Override
    public TimedQuery<String> getInputTimed() {
        return this.description.timed().getValue();
    }

    @Override
    public CreateIssueDescriptionField selectMention(String userId) {
        Poller.waitUntilTrue(getMentions().hasSuggestion(userId));
        getMentions().getSuggestion(userId).click();
        return this;
    }

    @Override
    public MentionsUserPicker getMentions() {
        return pageBinder.bind(MentionsUserPicker.class, this.description);
    }
}
