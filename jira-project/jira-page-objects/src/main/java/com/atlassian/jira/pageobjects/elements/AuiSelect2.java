package com.atlassian.jira.pageobjects.elements;

import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Represents an AuiSelect2 component
 *
 * @since v7.1
 */
public class AuiSelect2 {

    @Inject
    protected PageElementFinder elementFinder;

    private PageElement container;
    private PageElement select2Container;
    private static final String DROP_DOWN_OPTION_SELECTOR = "#select2-drop li";

    public AuiSelect2(PageElement container) {
        this.container = container;
    }

    @Init
    public void init() {
        this.select2Container = elementFinder.find(By.id("s2id_" + container.getAttribute("id")));
    }

    public void add(String item) {
        select2Container.find(By.cssSelector(".select2-choices")).click();
        List<PageElement> elements = elementFinder.findAll(By.cssSelector(DROP_DOWN_OPTION_SELECTOR));

        Optional<PageElement> result = elements.stream()
                .filter(element -> item.equals(element.getText()))
                .findFirst();

        if (result.isPresent()) {
            result.get().click();
        }
    }

    public String getFieldText() {
        return select2Container.find(By.cssSelector(".select2-choices")).getText();
    }
}
