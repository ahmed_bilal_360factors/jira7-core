package com.atlassian.jira.pageobjects.util.browsermetrics;

import com.atlassian.pageobjects.elements.query.TimedCondition;

/**
 * Browser metrics beacon checkpoint.
 */
public final class Checkpoint {
    final BrowserMetricsContext context;
    final int position;

    public Checkpoint(BrowserMetricsContext context, int position) {
        this.context = context;
        this.position = position;
    }

    /**
     * Waits for the next occurrence of a beacon with the given transition key.
     *
     * @param key Transition key to watch for.
     */
    public Beacon waitFor(String key) {
        return this.context.waitFor(this, key);
    }

    /**
     * Check whether a beacon with given transition key exists after this checkpoint.
     *
     * @param key Transition key to watch for.
     * @return
     */
    public boolean exists(String key) {
        return this.context.exists(this, key);
    }

    /**
     * A condition that waits for the next occurrence of a beacon with the given transition key.
     *
     * @param key Transition key to watch for.
     */
    public TimedCondition condition(String key) {
        return this.context.condition(this, key);
    }
}
