package com.atlassian.jira.pageobjects.project.components;

import com.atlassian.jira.pageobjects.dialogs.JiraDialog;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import java.util.List;
import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * @since v4.4
 */
public class DeleteComponentDialog extends JiraDialog {
    private static String DIALOG_ID_FORMAT = "component-%s-delete-dialog";
    private String projectKey;
    private PageElement swapComponentRadio;
    private PageElement swapComponentSelect;
    private PageElement removeComponentRadio;
    private PageElement submitButton;
    private PageElement issueCount;
    private PageElement infoMessage;

    @Inject
    private PageBinder pageBinder;

    @Inject
    private PageElementFinder elementFinder;

    public DeleteComponentDialog(final String type) {
        super(String.format(DIALOG_ID_FORMAT, type));
    }

    @WaitUntil
    public void waitForDialog() {
        waitUntilTrue(isOpen());
    }

    @Init
    public void getElements() {
        swapComponentRadio = this.find(By.id("component-swap"));
        swapComponentSelect = this.find(By.name("moveIssuesTo"));
        removeComponentRadio = this.find(By.id("component-remove"));
        submitButton = this.find(By.id("submit"));
        projectKey = elementFinder.find(By.name("projectKey")).getAttribute("content");
        issueCount = this.find(By.id("issue-count"));
        infoMessage = this.find(By.cssSelector(".aui-message.info"));
    }

    public String getInfoMessage() {
        if (infoMessage.isPresent()) {
            return infoMessage.getText();
        } else {
            return null;
        }
    }

    public Boolean hasComponentSwapOperation() {
        return swapComponentRadio.isPresent();
    }

    public Boolean hasComponentRemoveOperation() {
        return removeComponentRadio.isPresent();
    }

    public DeleteComponentDialog setSwapComponent(final String name) {
        List<PageElement> options = swapComponentSelect.findAll(By.tagName("option"));

        for (PageElement option : options) {
            if (option.getText().equals(name)) {
                option.select();
                break;
            }
        }

        return this;
    }

    public DeleteComponentDialog setRemoveComponent() {
        removeComponentRadio.click();
        return this;
    }

    public int getIssuesInComponentCount() {
        return Integer.parseInt(issueCount.getText());
    }

    public ComponentsPageTab submit() {
        submitButton.click();
        waitUntilTrue(this.isClosed());
        return pageBinder.bind(ComponentsPageTab.class, projectKey);
    }

}
