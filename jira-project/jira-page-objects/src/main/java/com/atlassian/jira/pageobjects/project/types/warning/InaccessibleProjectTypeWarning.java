package com.atlassian.jira.pageobjects.project.types.warning;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

import javax.inject.Inject;

/**
 * Page object representing the warning displayed when a project type is inaccessible.
 */
public class InaccessibleProjectTypeWarning {
    @ElementBy(className = "project-type-warning-icon")
    private PageElement warningIcon;

    @Inject
    private PageBinder pageBinder;

    @WaitUntil
    public void isPresent() {
        Poller.waitUntilTrue("Inaccessible project type warning never became visible", warningIcon.timed().isPresent());
    }

    public InaccessibleProjectTypeWarningDialog click() {
        warningIcon.click();
        return pageBinder.bind(InaccessibleProjectTypeWarningDialog.class);
    }
}
