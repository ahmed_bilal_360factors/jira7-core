package com.atlassian.jira.pageobjects.config;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.pages.setup.AdminSetupPage;
import com.atlassian.jira.pageobjects.pages.setup.ApplicationSetupPage;
import com.atlassian.jira.pageobjects.pages.setup.DatabaseSetupPage;
import com.atlassian.jira.pageobjects.pages.setup.LicenseSetupPage;
import com.atlassian.jira.pageobjects.pages.setup.MailSetupPage;
import com.atlassian.jira.pageobjects.pages.setup.SetupModePage;
import com.atlassian.jira.webtests.LicenseKeys;
import com.atlassian.pageobjects.DelayedBinder;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import static org.hamcrest.Matchers.is;

/**
 * Implementation of JIRA setup that performs the simplest set up possible via UI.
 *
 * @since v4.4
 */
public class SimpleJiraSetup {
    private static final Logger log = LoggerFactory.getLogger(SimpleJiraSetup.class);

    private final PageBinder pageBinder;
    private final JiraTestedProduct jira;
    private final DatabaseConfigProvider databaseConfigProvider;

    @Inject
    private Timeouts timeouts;

    @Inject
    public SimpleJiraSetup(PageBinder pageBinder, JiraTestedProduct jira, DatabaseConfigProvider databaseConfigProvider) {
        this.pageBinder = pageBinder;
        this.jira = jira;
        this.databaseConfigProvider = databaseConfigProvider;
    }

    public void performSetUp() {
        DelayedBinder<SetupModePage> delayedSetupMode = jira.visitDelayed(SetupModePage.class);
        if (delayedSetupMode.canBind()) {
            setupMode(delayedSetupMode.bind());
        }

        DelayedBinder<DatabaseSetupPage> delayedSetup = jira.visitDelayed(DatabaseSetupPage.class);
        if (delayedSetup.canBind()) {
            setupDatabase(delayedSetup.bind());
        }

        if (jira.isAt(ApplicationSetupPage.class)) {
            setupMail(setupAdmin(setupLicense(setupApplication(pageBinder.bind(ApplicationSetupPage.class)))));
        } else if (jira.isAt(LicenseSetupPage.class)) {
            setupMail(setupAdmin(setupLicense(pageBinder.bind(LicenseSetupPage.class))));
        } else if (jira.isAt(AdminSetupPage.class)) {
            setupMail(setupAdmin(pageBinder.bind(AdminSetupPage.class)));
        } else if (jira.isAt(MailSetupPage.class)) {
            setupMail(pageBinder.bind(MailSetupPage.class));
        } else {
            log.warn("Already set up, skipping");
        }
    }

    private DatabaseSetupPage setupMode(SetupModePage setupPage) {
        setupPage.selectMode(SetupModePage.ModeSelection.CLASSIC);
        return setupPage.submit();
    }

    private void setupDatabase(DatabaseSetupPage setupPage) {
        if (!databaseConfigProvider.isExternalConfigPresent()) {
            setupPage.submitInternalDb();
        } else {
            final ExternalDatabaseConfig externalDatabaseConfig = databaseConfigProvider.getExternalConfig();
            log.info("Read the following external database configuration from Java System Properties: " + externalDatabaseConfig);
            setupPage.submitExternalDb(externalDatabaseConfig);
        }
    }

    private LicenseSetupPage setupApplication(ApplicationSetupPage applicationSetupPage) {
        return applicationSetupPage.setTitle("Testing JIRA")
                .submit();
    }

    private AdminSetupPage setupLicense(LicenseSetupPage licenseSetupPage) {
        return licenseSetupPage.selectExistingLicense(LicenseKeys.COMMERCIAL.getLicenseString()).submit();
    }

    private MailSetupPage setupAdmin(AdminSetupPage adminSetupPage) {
        return adminSetupPage.setUsername("admin")
                .setPasswordAndConfirmation("admin")
                .setFullName("Administrator")
                .setEmail("admin@stuff.com.com")
                .submit();
    }

    private void setupMail(MailSetupPage mailSetupPage) {
        mailSetupPage.submitDisabledEmailWithoutBinding();
        // make sure we're not still on MailSetupPage
        Poller.waitUntil("Should not have remained on MailSetupPage if setup was successful",
                mailSetupPage.isAt(), is(false), Poller.by(timeouts.timeoutFor(TimeoutType.PAGE_LOAD)));
    }
}
