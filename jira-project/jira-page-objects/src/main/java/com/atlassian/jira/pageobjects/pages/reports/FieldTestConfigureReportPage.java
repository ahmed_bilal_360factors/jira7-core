package com.atlassian.jira.pageobjects.pages.reports;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.MultiSelectElement;

/**
 * @since 7.2
 */
public class FieldTestConfigureReportPage extends AbstractConfigureReportPage<FieldTestReportResultPage> {

    @ElementBy(id = "aMultiSelect_select")
    private MultiSelectElement multiSelect;

    @Override
    protected String getReportKey() {
        return "fieldtest-report";
    }

    public MultiSelectElement getMultiSelect() {
        return multiSelect;
    }
}
