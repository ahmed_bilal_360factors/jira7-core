package com.atlassian.jira.pageobjects.project;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

/**
 * Represents the project header when the sidebar is enabled.
 *
 * @since 7.1
 */
public class ProjectSettingsHeader {

    @ElementBy(id = "project-config-header")
    private PageElement header;

    @ElementBy(id = "project-config-header-name")
    private PageElement headerName;


    public boolean hasProjectNameInTitle() {
        return (getProjectName() != null);
    }

    public String getProjectName() {
        String[] sections = headerName.getText().split(" - ", 2);
        if (sections.length == 1) {
            return null;
        } else {
            return sections[1];
        }
    }
}
