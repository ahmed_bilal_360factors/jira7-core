package com.atlassian.jira.webtest.webdriver.util;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.webdriver.utils.element.ElementIsVisible;
import com.atlassian.webdriver.utils.element.WebDriverPoller;
import org.openqa.selenium.By;

/**
 * @since v7.0.0
 */
public class AUIHelpTip {
    public static final String CLOSE_ALL_JS = "AJS.$('.jira-help-tip .helptip-close').click()";
    private final JiraTestedProduct jira;

    public AUIHelpTip(final JiraTestedProduct jira) {
        this.jira = jira;
    }

    public void waitForAny(int timeout) {
        WebDriverPoller poller = new WebDriverPoller(jira.getTester().getDriver());

        poller.waitUntil(new ElementIsVisible(By.cssSelector(".jira-help-tip")), timeout);
    }

    public void closeAll() {
        jira.getTester().getDriver().executeScript(CLOSE_ALL_JS);
    }
}
