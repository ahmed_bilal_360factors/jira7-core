package com.atlassian.jira.pageobjects.onboarding;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import javax.inject.Inject;

public class ChooseYourOwnAdventureStepBox {
    @Inject
    PageBinder pageBinder;

    private PageElement pageElement;

    private PageElement image;
    private PageElement header;
    private PageElement description;
    private PageElement button;

    public ChooseYourOwnAdventureStepBox(PageElement pageElement) {
        this.pageElement = pageElement;

        this.image = pageElement.find(By.tagName("img"));
        this.header = pageElement.find(By.cssSelector(".next-steps--step-description h2"));
        this.description = pageElement.find(By.cssSelector(".next-steps--step-description p"));
        this.button = pageElement.find(By.tagName("button"));
    }

    public String getImgSrc() {
        return this.image.getAttribute("src");
    }

    public String getHeaderText() {
        return this.header.getText();
    }

    public String getDescriptionText() {
        return this.description.getText();
    }

    public String getButtonText() {
        return this.button.getText();
    }

    public String getButtonId() {
        return this.button.getAttribute("id");
    }

    public <T extends AbstractJiraPage> T clickButton(Class<T> nextPage) {
        button.click();
        return pageBinder.bind(nextPage);
    }
}
