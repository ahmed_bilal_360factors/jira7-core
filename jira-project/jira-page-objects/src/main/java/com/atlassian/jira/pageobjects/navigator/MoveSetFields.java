package com.atlassian.jira.pageobjects.navigator;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import org.openqa.selenium.By;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Represents set fields page in the process of bulk move. This class is not fully implemented, it has only basic
 * controls to advance to the next step.
 *
 * @since v7.0
 */
public class MoveSetFields extends AbstractJiraPage {

    @ElementBy(id = "bulkedit")
    protected PageElement form;

    @ElementBy(id = "next")
    protected PageElement next;

    @ElementBy(id = "editbulkmovefields")
    protected PageElement table;

    @ElementBy(cssSelector = "tbody tr", within = "table")
    Iterable<PageElement> fieldUpdateRows;

    @ElementBy(id = "bulk-updatefield-title")
    protected PageElement title;

    public <T extends AbstractJiraPage> T next(Class<T> nextPage) {
        next.click();
        return pageBinder.bind(nextPage);
    }

    @Override
    public TimedCondition isAt() {
        return title.timed().isPresent();
    }

    @Override
    public String getUrl() {
        throw new UnsupportedOperationException("Not implemented");
    }

    public Iterable<FieldUpdateRow> getFieldUpdateRows() {
        return Iterables.transform(fieldUpdateRows, new Function<PageElement, FieldUpdateRow>() {
            @Nullable
            @Override
            public FieldUpdateRow apply(final PageElement input) {
                List<PageElement> tds = input.findAll(By.tagName("td"));
                boolean retainCheckbox = false;
                boolean retainCheckboxDisabled = false;
                if (tds.size() > 2) {
                    PageElement retainSecurityCheckbox = tds.get(2).find(By.name("retain_security"));
                    retainCheckbox = retainSecurityCheckbox.isPresent();
                    if (retainCheckbox) {
                        retainCheckboxDisabled = !retainSecurityCheckbox.isEnabled();
                    }
                }
                return new FieldUpdateRow(
                        tds.size() > 0 ? tds.get(0).getText() : "",
                        tds.size() > 1 ? tds.get(1).getText() : "",
                        retainCheckbox,
                        retainCheckboxDisabled,
                        input
                );

            }
        });
    }

    public boolean isAllFieldsRetained() {
        return form.getText().contains("All field values will be retained.");
    }

    public static class FieldUpdateRow {
        final String field;
        final String content;
        final boolean retainCheckboxPresent;
        final boolean retainCheckboxDisabled;
        final PageElement element;

        public FieldUpdateRow(
                final String field,
                final String content,
                final boolean retainCheckboxPresent,
                final boolean retainCheckboxDisabled,
                final PageElement element
        ) {
            this.field = field;
            this.content = content;
            this.retainCheckboxPresent = retainCheckboxPresent;
            this.retainCheckboxDisabled = retainCheckboxDisabled;
            this.element = element;
        }

        public boolean isRetainCheckboxPresent() {
            return retainCheckboxPresent;
        }

        public String getField() {
            return field;
        }

        public String getContent() {
            return content;
        }

        public boolean isRetainCheckboxDisabled() {
            return retainCheckboxDisabled;
        }

        public void input(String text) {
            PageElement inputElement = element.find(By.tagName("input"));
            inputElement.clear().type(text);
            inputElement.javascript().execute("arguments[0].dispatchEvent(new Event('blur'));");
        }

        public String getErrorMessage() {
            return element.find(By.className("errMsg")).getText();
        }
    }
}
