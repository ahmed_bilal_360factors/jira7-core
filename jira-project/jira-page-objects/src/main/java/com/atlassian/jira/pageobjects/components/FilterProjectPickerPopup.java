package com.atlassian.jira.pageobjects.components;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.inject.Inject;
import java.util.Collection;
import org.openqa.selenium.By;

import static java.util.Optional.ofNullable;

/**
 * A PageObject for the FilterOrProject picker popup window used on some of the ConfigureReport screens. This PageObject
 * doesn't implement all behaviour of this component, only the behaviour we need to test basic reports workflow.
 *
 * @since v7.2
 */
public class FilterProjectPickerPopup {

    private static final int TAB_INDEX_FAVOURITE_FILTERS = 0;
    private static final int TAB_INDEX_PROJECTS = 3;

    @Inject
    private PageBinder pageBinder;

    @ElementBy(id = "filter_type_table")
    private PageElement filterTypeTable;

    public FavouritesTab goToFavouritesTab() {
        clickTableTab(TAB_INDEX_FAVOURITE_FILTERS);
        return pageBinder.bind(FavouritesTab.class);
    }

    public ProjectsTab goToProjectsTab() {
        clickTableTab(TAB_INDEX_PROJECTS);
        return pageBinder.bind(ProjectsTab.class);
    }

    private void clickTableTab(int tabIndex) {
        filterTypeTable.findAll(By.tagName("li")).get(tabIndex).click();
    }

    @WaitUntil
    @SuppressWarnings("unused")
    private void waitUntilPopupIsOpen() {
        Poller.waitUntilTrue(filterTypeTable.timed().isPresent());
    }

    private static void clickAnchorMatchingString(Collection<PageElement> anchors, String expectedAnchorText) {
        anchors.stream()
                .filter(e -> ofNullable(e.getText()).map(expectedAnchorText::equals).orElse(false))
                .findFirst()
                .map(PageElement::click)
                .orElseThrow(() -> new IllegalArgumentException("No anchor matching text: " + expectedAnchorText));
    }

    public static class FavouritesTab {
        @ElementBy(id = "mf_favourites")
        private PageElement favouritesTable;

        public void selectFilter(String filterName) {
            Collection<PageElement> filterLinks = favouritesTable.findAll(By.tagName("a"));
            clickAnchorMatchingString(filterLinks, filterName);
        }

        @WaitUntil
        @SuppressWarnings("unused")
        private void waitUntilTableIsLoaded() {
            Poller.waitUntilTrue(favouritesTable.timed().isPresent());
        }
    }

    public static class ProjectsTab {
        @ElementBy(id = "nocat_projects")
        private PageElement projectsTable;

        public void selectProject(String projectName) {
            Collection<PageElement> projectLinks = projectsTable.findAll(By.tagName("a"));
            clickAnchorMatchingString(projectLinks, projectName);
        }

        @WaitUntil
        @SuppressWarnings("unused")
        private void waitUntilTableIsLoaded() {
            Poller.waitUntilTrue(projectsTable.timed().isPresent());
        }
    }

}
