package com.atlassian.jira.pageobjects.pages.admin;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.utils.Check;
import java.util.List;
import javax.inject.Inject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * @since v4.2
 */
public class ProjectRow {
    @Inject
    private PageBinder pageBinder;

    @Inject
    private PageElementFinder pageElementFinder;

    private String name;
    private String key;
    private WebElement url;
    private String urlStr;
    private String projectLead;
    private String defaultAssignee;
    private String projectType;

    public ProjectRow(WebElement projectViewRow) {
        final List<WebElement> cols = projectViewRow.findElements(By.tagName("td"));

        name = getColumnTextWithCellType(cols, "name", "data-cell-type").getText();
        key = getColumnTextWithCellType(cols, "key", "data-cell-type").getText();
        projectType = getColumnTextWithCellType(cols, "project-type", "data-cell-type").getText();

        WebElement urlEl = getColumnTextWithCellType(cols, "url", "data-cell-type");
        if (Check.elementExists(By.tagName("a"), urlEl)) {
            url = urlEl.findElement(By.tagName("a"));
            urlStr = url.getText();
        } else {
            url = null;
            urlStr = "";
        }

        projectLead = getColumnTextWithCellType(cols, "lead", "data-cell-type").getText();
        defaultAssignee = getColumnTextWithCellType(cols, "default-assignee", "data-cell-type").getText();
    }

    private WebElement getColumnTextWithCellType(List<WebElement> columns, String attributeValue, String attributeName) {
        return columns.stream()
                .filter(column -> column.getAttribute(attributeName).equals(attributeValue))
                .findFirst()
                .orElse(null);
    }

    public Page viewProject() {
        throw new UnsupportedOperationException("view Project operation on ProjectSummary has not been implemented");
    }

    public EditProjectDialog editProject() {
        PageElement trigger = editProjectTrigger();
        trigger.click();
        return pageBinder.bind(EditProjectDialog.class, trigger.getId() + "-dialog");
    }

    public Page deleteProject() {
        throw new UnsupportedOperationException("delete project operation on ProjectSummary has not been implemented");
    }

    public boolean hasUrl() {
        return url != null;
    }

    private PageElement rowElement() {
        String selector = String.format("//tr[@data-project-key='%s']", getKey());
        return pageElementFinder.find(By.xpath(selector));
    }

    private PageElement editProjectTrigger() {
        return rowElement().find(By.className("edit-project"));
    }

    //
    // GENERATED CODE BELOW
    //

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    public WebElement getUrl() {
        return url;
    }

    public String getUrlStr() {
        return urlStr;
    }

    public String getProjectLead() {
        return projectLead;
    }

    public String getDefaultAssignee() {
        return defaultAssignee;
    }

    public String getProjectType() {
        return projectType;
    }
}
