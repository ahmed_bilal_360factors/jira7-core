package com.atlassian.jira.pageobjects.pages;


import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.framework.util.JiraLocators;
import com.atlassian.pageobjects.DelayedBinder;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.atlassian.webdriver.AtlassianWebDriver;
import org.hamcrest.Matchers;
import org.openqa.selenium.Alert;
import org.openqa.selenium.UnhandledAlertException;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;

/**
 * Provides a set of common functions that all JIRA pages can do, such as getting the admin menu.
 * Sets the base url for the WebDrivePage class to use which is defined in the jira-base-url system property.
 *
 * @since 4.4
 */
public abstract class AbstractJiraPage implements Page {
    @Inject
    protected Timeouts timeouts;

    @Inject
    protected PageBinder pageBinder;

    @Inject
    protected PageElementFinder elementFinder;

    @Inject
    protected AtlassianWebDriver driver;

    @ElementBy(tagName = "body", timeoutType = TimeoutType.PAGE_LOAD)
    protected PageElement body;

    @ElementBy(id = "atlassian-token", timeoutType = TimeoutType.PAGE_LOAD)
    protected PageElement metaElement;

    public JiraHeader getHeader() {
        return pageBinder.bind(JiraHeader.class);
    }

    /**
     * <p>
     * The default doWait for JIRA is defined in terms of {@link #isAt()}.
     */
    @WaitUntil
    public void doWait() {
        final long pageLoadTimeout = timeouts.timeoutFor(TimeoutType.PAGE_LOAD);
        try {
            waitUntil("Waiting for page load failed for " + getClass().getName(),
                    isAt(), Matchers.is(true), Poller.by(pageLoadTimeout));
        } catch (UnhandledAlertException e) {
            try {
                final Alert alert = driver.switchTo().alert();
                final String text = alert.getText();
                throw new UnhandledAlertException(e.getLocalizedMessage(), "Alert window present with text +" + text);
            } catch (RuntimeException re) {
                re.addSuppressed(e);
                throw re;
            }
        }
    }

    public void execKeyboardShortcut(final CharSequence... keys) {
        elementFinder.find(JiraLocators.body()).type(keys);
    }

    public String getPageTitle() {
        return driver.getTitle();
    }

    public String getXsrfToken() {
        if (!metaElement.isPresent()) {
            throw new IllegalStateException("Can't find the XSRF token on the current page.");
        } else {
            return metaElement.getAttribute("content");
        }
    }

    /**
     * Timed condition checking if we're at given page.
     *
     * @return timed condition checking, if the test is at given page
     */
    public abstract TimedCondition isAt();

    public boolean isLoggedIn() {
        DelayedBinder<JiraHeader> header = pageBinder.delayedBind(JiraHeader.class);
        return header.canBind() && header.bind().isLoggedIn();
    }

    public boolean isAdmin() {
        DelayedBinder<JiraHeader> header = pageBinder.delayedBind(JiraHeader.class);
        return header.canBind() && header.bind().isAdmin();
    }

    public <P> P back(Class<P> binder, Object... arguments) {
        driver.navigate().back();
        return pageBinder.bind(binder, arguments);
    }
}
