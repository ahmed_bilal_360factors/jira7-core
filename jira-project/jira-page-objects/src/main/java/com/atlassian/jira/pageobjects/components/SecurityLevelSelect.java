package com.atlassian.jira.pageobjects.components;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static org.junit.Assert.assertFalse;

/**
 * This class is pageobject for SecurityLevelSelect.js object
 *
 * @since v7.1
 */
public class SecurityLevelSelect {

    @Inject
    protected PageBinder pageBinder;

    @Inject
    protected PageElementFinder elementFinder;

    private DropdownSelect dropdownSelect;

    private PageElement securityLevel;

    public SecurityLevelSelect(PageElement securityLevel) {
        this.securityLevel = securityLevel;
    }

    public PageElement getCurrentLevelDiv() {
        return securityLevel.find(By.className("current-level"));
    }

    public PageElement getLinkToSetDefaultSecurity() {
        return securityLevel.find(By.className("default-comment-level-switch"));
    }

    public PageElement getDefaultCommentLevelMessageStatus() {
        return securityLevel.find(By.className("default-comment-level-status"));
    }

    public PageElement getDefaultCommentLevelLoad() {
        return securityLevel.find(By.className("default-comment-level-load"));
    }

    public String getSelectedSecurityLevelText() {
        return getCurrentLevelDiv().getText();
    }

    public String setDefaultSecurityLevel() {
        getLinkToSetDefaultSecurity().click();
        return getDefaultCommentLevelMessageStatus().getAttribute("status");
    }

    public void ensureThatLinkToSetDefaultSecurityLevelIsNotPresent() {
        waitUntilFalse(getLinkToSetDefaultSecurity().timed().isPresent());
    }

    public void waitUntilDefaultCommentLevelLoaded() {
        waitUntilFalse(getDefaultCommentLevelLoad().timed().isVisible());
    }

    public void ensureDefaultIsDisabled() {
        assertFalse(getDefaultCommentLevelLoad().isPresent());
        assertFalse(getLinkToSetDefaultSecurity().isPresent());
    }

    public DropdownSelect getSelect() {
        if (this.dropdownSelect == null) {
            this.dropdownSelect = pageBinder.bind(DropdownSelect.class, securityLevel,
                    By.cssSelector("#commentLevel-multi-select a"), By.cssSelector(".active > #commentLevel-suggestions"));
        }
        return this.dropdownSelect;
    }
}
