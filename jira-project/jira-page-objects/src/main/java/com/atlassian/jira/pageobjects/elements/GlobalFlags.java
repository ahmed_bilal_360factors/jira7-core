package com.atlassian.jira.pageobjects.elements;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.AbstractTimedQuery;
import com.atlassian.pageobjects.elements.query.ExpirationHandler;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Lists.transform;

/**
 * JIRA 6.4 introduced global notification flags. This page object allows you to access them.
 *
 * @since v6.4
 */
public class GlobalFlags {
    @ElementBy(id = "aui-flag-container")
    private PageElement flagContainer;

    @Inject
    private Timeouts timeouts;

    @Inject
    private PageBinder binder;

    public TimedCondition flagContainerPresent() {
        return flagContainer.timed().isPresent();
    }

    public boolean isPresent() {
        return flagContainer.isPresent();
    }

    public List<AuiMessage> getMessages() {
        return copyOf(transform(getFlags(), AuiFlag::toMessage));
    }

    public List<AuiFlag> getFlags() {
        final List<PageElement> flagElements = flagContainer.findAll(By.className("aui-flag"));
        final List<AuiFlag> flags = Lists.newArrayList();
        for (PageElement flagElement : flagElements) {
            if (flagElement.isVisible()) {
                flags.add(toFlag(flagElement));
            }
        }
        return flags;
    }

    public AuiFlag getFlagWithText(String text) {
        FlagTimedQuery flagExistTimedQuery = new FlagTimedQuery(text, Predicates.notNull());
        return flagExistTimedQuery.byDefaultTimeout();
    }

    public AuiFlag getFlagWithElement(By by) {
        FlagTimedQuery flagExistTimedQuery = new FlagTimedQuery(by, Predicates.notNull());
        return flagExistTimedQuery.byDefaultTimeout();
    }

    public boolean doesNotContainFlagWithText(String text) {
        FlagTimedQuery flagNotExistTimedQuery = new FlagTimedQuery(text, Predicates.isNull());
        return flagNotExistTimedQuery.byDefaultTimeout() == null;
    }

    private AuiFlag toFlag(final PageElement flagElement) {
        return binder.bind(AuiFlag.class, flagElement);
    }

    private class FlagTimedQuery extends AbstractTimedQuery<AuiFlag> {
        private final String text;
        private final By by;
        private final Predicate<AuiFlag> shouldReturn;

        private FlagTimedQuery(String text, final Predicate<AuiFlag> shouldReturn) {
            super(timeouts.timeoutFor(TimeoutType.DEFAULT), Timeouts.DEFAULT_INTERVAL, ExpirationHandler.RETURN_CURRENT);
            this.text = text;
            this.by = null;
            this.shouldReturn = shouldReturn;
        }

        private FlagTimedQuery(By by, final Predicate<AuiFlag> shouldReturn) {
            super(timeouts.timeoutFor(TimeoutType.DEFAULT), Timeouts.DEFAULT_INTERVAL, ExpirationHandler.RETURN_CURRENT);
            this.text = null;
            this.by = by;
            this.shouldReturn = shouldReturn;
        }

        @Override
        protected AuiFlag currentValue() {
            final List<PageElement> flagElements = flagContainer.findAll(By.className("aui-flag"));
            for (PageElement flagElement : flagElements) {
                if (by != null) {
                    if (flagElement.find(by).isPresent() && flagElement.find(by).isVisible()) {
                        return toFlag(flagElement);
                    }
                } else {
                    if (flagElement.isVisible() && flagElement.getText().contains(text)) {
                        return toFlag(flagElement);
                    }
                }
            }
            return null;
        }

        @Override
        protected boolean shouldReturn(final AuiFlag currentEval) {
            return shouldReturn.apply(currentEval);
        }
    }
}
