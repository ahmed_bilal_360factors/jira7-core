package com.atlassian.jira.pageobjects.project.add;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

import static com.atlassian.pageobjects.elements.query.Poller.by;
import static org.hamcrest.Matchers.is;

public class AddProjectWizardPageConfirmOdProjectCreation extends AddProjectWizardPage {


    private PageElement submit;

    @Inject
    private PageBinder binder;

    @Init
    public void init() {
        submit = find(By.className("project-create-acknowledge-button"));
    }

    public void confirm() {
        Poller.waitUntil("Projects have not been created within some time.", submit.timed().isEnabled(), is(true), by(30, TimeUnit.SECONDS));
        submit.click();
    }

}
