package com.atlassian.jira.pageobjects.pages.admin;

import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import javax.inject.Inject;
import org.openqa.selenium.By;

/**
 * The inline dialogs on the ViewProjects/ProjectDetails page that display extra information about various project
 * fields (project type, project category, etc).
 * <p/>
 * Ideally we would replace this with the com.atlassian.pageobjects.components.aui.AuiInlineDialog class, but that page
 * object doesn't do everything we need it to do right now (can't close the dialog programmatically, can't check its
 * contents). We can't just extend it either because it makes some assumptions about id naming conventions that are
 * incompatible with our existing markup for these screens (not all our inline dialogs have an id prefixed with
 * "inline-dialog-" like AuiInlineDialog assumes). This will likely be replaced with an updated AuiInlineDialog page
 * object eventually (tm).
 *
 * @since v7.2
 *
 */
public class ProjectDetailsHelpTooltip {

    @Inject
    private PageElementFinder pageElementFinder;

    private PageElement trigger;

    private PageElement inlineDialog;

    private String triggerId;

    private String inlineDialogId;

    public ProjectDetailsHelpTooltip(String triggerId, String inlineDialogId) {
        this.triggerId = triggerId;
        this.inlineDialogId = inlineDialogId;
    }

    @Init
    public void initialise() {
        trigger = pageElementFinder.find(By.id(triggerId));
        inlineDialog = pageElementFinder.find(By.id(inlineDialogId));
    }

    public void open() {
        if (!isOpen()) {
            trigger.click();
            Poller.waitUntilTrue(inlineDialog.timed().isVisible());
        }
    }

    public void close() {
        if (isOpen()) {
            trigger.click();
            Poller.waitUntilFalse(inlineDialog.timed().isVisible());
        }
    }

    public boolean isOpen() {
        return inlineDialog.isVisible();
    }

    public boolean hasVisibleContent() {
        return isOpen() && inlineDialog.getText() != null;
    }

}
