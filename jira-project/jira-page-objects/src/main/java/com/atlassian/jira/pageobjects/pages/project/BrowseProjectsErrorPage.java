package com.atlassian.jira.pageobjects.pages.project;

import com.atlassian.jira.pageobjects.components.ErrorPanel;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.query.TimedCondition;

public final class BrowseProjectsErrorPage extends AbstractBrowseProjectsPage {
    private ErrorPanel errorPanel;

    public BrowseProjectsErrorPage() {
        super();
    }

    @Init
    public void init() {
        errorPanel = pageBinder.bind(ErrorPanel.class, body);
    }

    @Override
    public TimedCondition isAt() {
        // This page is rendered server side, so we can just check that the
        // body is present
        return body.timed().isPresent();
    }

    public boolean isErrorPanelPresent() {
        return errorPanel.isPresent().now();
    }

    public String getErrorPanelLinkUrl() {
        return errorPanel.getLinkHref().now();
    }
}
