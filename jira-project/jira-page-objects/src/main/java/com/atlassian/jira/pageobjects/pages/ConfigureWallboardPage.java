package com.atlassian.jira.pageobjects.pages;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import javax.inject.Inject;

public class ConfigureWallboardPage {

    @Inject
    private PageBinder pageBinder;

    @ElementBy(cssSelector = ".configure-wallboard button.cancel")
    private PageElement cancelButton;

    @ElementBy(cssSelector = "#configure_wallboard_dialog")
    private PageElement dialog;

    @ElementBy(cssSelector = ".aui-blanket")
    private PageElement blanket;

    public DashboardPage cancel() {
        cancelButton.click();
        Poller.waitUntilFalse(blanket.timed().isVisible());
        Poller.waitUntilFalse(dialog.timed().isPresent());
        return pageBinder.bind(DashboardPage.class);
    }

    public TimedCondition isVisible() {
        return dialog.timed().isVisible();
    }
}
