package com.atlassian.jira.pageobjects.util;

import com.atlassian.webdriver.AtlassianWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;

/**
 * Java interface for "critical-css.js" and "critical-js.js" modules, can be used to find out which stylesheets or scripts are used on current page.
 *
 * @since 7.1
 */
public class CriticalResources {

    /**
     * Each resource type implements its own way of determining whether a resource is active or critical.
     */
    public enum ResourceType {
        SCRIPT("jira/func/critical-js"),
        STYLESHEET("jira/func/critical-css");

        private final String module;

        ResourceType(final String module) {
            this.module = module;
        }

        public String getModule() {
            return module;
        }
    }

    private static final Logger log = LoggerFactory.getLogger(CriticalResources.class);

    @Inject
    private AtlassianWebDriver executor;

    /**
     * Get list of all resources loaded during page load.
     */
    public List<String> getActive(ResourceType resourceType) {
        return (List<String>) executor.executeScript(String.format("return require('%s').getAll();", resourceType.getModule()));
    }

    /**
     * Get list of all resources that were used during page load.
     */
    public List<String> getCritical(ResourceType resourceType) {
        return (List<String>) executor.executeScript(String.format("return require('%s').getCritical();", resourceType.getModule()));
    }
}
