package com.atlassian.jira.pageobjects.components.license;


import com.atlassian.jira.pageobjects.elements.AuiFlag;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import org.openqa.selenium.By;

import javax.annotation.Nonnull;

public class LicenseFlag extends LicenseContent {
    private final AuiFlag flag;

    public LicenseFlag(AuiFlag auiFlag) {
        this.flag = auiFlag;
    }

    @Nonnull
    @Override
    protected PageElement getContent() {
        return flag.getFlagElement().find(By.id("license-flag-content"));
    }

    @Override
    public void dismiss() {
        if (!flag.isCloseable()) {
            throw new IllegalStateException("Unable to close flag.");
        }
        clickElementAndWaitForBanner(getCloseIcon(), "license-later-done");
        Poller.waitUntilFalse(getContent().timed().isVisible());
    }

    private PageElement getCloseIcon() {
        return flag.getFlagElement().find(By.className("icon-close"));
    }

    @Override
    protected PageElement getMacLink() {
        return getContent().find(By.id("license-flag-my-link"));
    }

    public void clickMacLink() {
        getMacLink().click();
    }
}
