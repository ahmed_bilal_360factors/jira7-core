package com.atlassian.jira.pageobjects.pages.viewissue.linkissue;

import com.atlassian.jira.pageobjects.framework.elements.PageElements;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Conditions.and;
import static com.atlassian.pageobjects.elements.query.Conditions.forSupplier;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Initial very limited implementation of SearchIssue dialog for Link Issue.
 * Contains only code needed to test JQL search.
 * Feel free to extend.
 */
public class LinkIssueSearchDialog {

    private final LinkJiraSection linkJiraSection;

    public LinkIssueSearchDialog(LinkJiraSection linkJiraSection) {
        this.linkJiraSection = linkJiraSection;
    }

    @Inject
    private PageElementFinder locator;

    @Inject
    private PageBinder pageBinder;

    @ElementBy(id = "link-search-text")
    private PageElement simpleSearchField;

    @ElementBy(id = "simple-search-panel-button")
    private PageElement simpleSearchButton;

    @ElementBy(id = "advanced-search-toggle")
    private PageElement switchToAdwancedLink;


    @ElementBy(id = "remote-jira-advanced-search-form")
    private PageElement advancedSearchForm;

    @ElementBy(id = "jql-search-text")
    private PageElement advancedSearchField;

    @ElementBy(className = "suggestions")
    private PageElement jqlSuggestions;

    @ElementBy(id = "advanced-search-panel-button")
    private PageElement advancedSearchButton;

    @ElementBy(id = "simple-search-toggle")
    private PageElement switchToSimpleLink;

    public boolean isAdvancedSearch() {
        return advancedSearchForm.isVisible();
    }

    public LinkIssueSearchDialog switchToAdvanced() {
        if (!isAdvancedSearch()) {
            switchToAdwancedLink.click();
            Poller.waitUntilTrue(advancedSearchField.timed().hasAttribute("jql-initialized", "1"));
        }
        return this;
    }

    public LinkIssueSearchDialog switchToSimple() {
        if (isAdvancedSearch()) {
            switchToSimpleLink.click();
        }
        return this;
    }

    public void typeQuery(final String query) {
        advancedSearchField.type(query);

        waitUntilTrue(
                and(forSupplier(10000L, () -> {
                    PageElements.scrollIntoView(jqlSuggestions);
                    return true;
                }), jqlSuggestions.timed().isVisible()));
    }

    public void moveCursor(final int position) {
        final int delta = advancedSearchField.getValue().length() - position;
        for (int i = 0; i < delta; i++) {
            advancedSearchField.type(Keys.LEFT);
        }
    }

    public void typeAndAutocomplete(final String query) {
        typeQuery(query);
        advancedSearchField.type(Keys.RETURN);
    }

    public PageElement getJqlSuggestions() {
        return jqlSuggestions;
    }

    public String getAdvancedJqlQuery() {
        return advancedSearchField.getValue();
    }
}
