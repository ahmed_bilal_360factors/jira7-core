package com.atlassian.jira.pageobjects.project.roles;

import com.atlassian.jira.pageobjects.project.roles.roleselector.RoleSelector;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

import javax.inject.Inject;

public class RolesFilter {
    @Inject
    private PageBinder pageBinder;

    @ElementBy(className = "js-roles-filter-query-input")
    private PageElement roleQueryInput;

    private RoleSelector roleSelector;

    @Init
    public void init() {
        roleSelector = pageBinder.bind(RoleSelector.class, "js-role-selector-container", "js-role-selector-dropdown");
    }

    public void selectRole(String roleName) {
        roleSelector.select(roleName);
    }

    public void selectAllRole() {
        roleSelector.selectAll();
    }

    public void typeQuery(String query) {
        roleQueryInput.clear().type(query);
    }

    public void clearQuery() {
        // type a blank string to make sure the input event is triggered
        typeQuery(" ");
    }
}
