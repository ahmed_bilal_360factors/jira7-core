package com.atlassian.jira.pageobjects.pages.reports;

import com.atlassian.jira.pageobjects.components.FilterProjectPickerPopup;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.google.common.collect.Iterables;
import javax.inject.Inject;

/**
 * The configuration page for the AverageAge report. This page allows users to specify a project or filter as input to
 * the report through a legacy popup picker. We need to make sure we switch Selenium's focus to the popup when it's
 * opened, then back to the main window again when it closes. If these tests are failing, I would look around those
 * interactions first.
 *
 * @since v7.2
 */
public class AverageAgeReportPage extends AbstractConfigureReportPage<ReportResultPage> {

    @Inject
    private PageBinder pageBinder;

    @ElementBy(id = "filter_projectOrFilterId_button")
    private PageElement filterOrProjectPickerTrigger;

    @Override
    protected String getReportKey() {
        return "averageage-report";
    }

    /**
     * Open the Filter or Project picker for interaction. This is a legacy popup, and will steal web driver focus from
     * the current main window. Focus will not return to the main window until the popup window is closed explicitly
     * from this class (see #changeTargetProjectTo and #changeTargetFilterTo for examples that return focus back to the
     * main window).
     */
    private FilterProjectPickerPopup openFilterOrProjectPickerPopup() {
        filterOrProjectPickerTrigger.click();
        setFocusToLatestWindow();
        return pageBinder.bind(FilterProjectPickerPopup.class);
    }

    public void changeTargetProjectTo(final String projectName) {
        final FilterProjectPickerPopup filterProjectPickerPopup = openFilterOrProjectPickerPopup();
        final FilterProjectPickerPopup.ProjectsTab projects = filterProjectPickerPopup.goToProjectsTab();
        projects.selectProject(projectName);
        resetFocusToMainWindow();
    }

    public void changeTargetFilterTo(final String filterName) {
        final FilterProjectPickerPopup filterProjectPickerPopup = openFilterOrProjectPickerPopup();
        final FilterProjectPickerPopup.FavouritesTab favouritesTab = filterProjectPickerPopup.goToFavouritesTab();
        favouritesTab.selectFilter(filterName);
        resetFocusToMainWindow();
    }

    private void setFocusToLatestWindow() {
        final String handle = Iterables.getLast(driver.getWindowHandles());
        driver.switchTo().window(handle);
    }

    private void resetFocusToMainWindow() {
        final String handle = Iterables.getFirst(driver.getWindowHandles(), null);
        driver.switchTo().window(handle);
    }

}
