package com.atlassian.jira.pageobjects.config.junit4.rule;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.functest.framework.util.junit.AnnotatedDescription;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.DisableRTE;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Disable RTE in tests or make it optional.
 *
 * @since v7.3
 */
public class DisableRTERule extends TestWatcher {

    private final static String RTE_OPTIONAL_FEATURE = "jira.richeditor.enable-rte.optional";

    private boolean wasEnabled = true;
    private boolean wasOptional = false;

    @Inject
    private final JiraTestedProduct jira;

    public DisableRTERule(JiraTestedProduct jira) {
        this.jira = jira;
    }

    @Override
    protected void starting(@Nonnull final Description description) {
        if (shouldEnable(description)) {
            configureRTE(description);
        }
    }

    @Override
    protected void finished(final Description description) {
        if (shouldEnable(description)) {
            restoreRTE();
        }
    }

    private void restoreRTE() {
        if (wasOptional) {
            getBackdoor().darkFeatures().enableForSite(RTE_OPTIONAL_FEATURE);
        } else {
            getBackdoor().darkFeatures().disableForSite(RTE_OPTIONAL_FEATURE);
        }

        getBackdoor().applicationProperties().setOption(APKeys.JIRA_OPTION_RTE_ENABLED, wasEnabled);
    }

    private boolean shouldEnable(final Description description) {
        final AnnotatedDescription annotatedDescription = new AnnotatedDescription(description);
        return annotatedDescription.isMethodAnnotated(DisableRTE.class);
    }

    private void configureRTE(@Nonnull final Description description) {
        final DisableRTE disableRTE = description.getAnnotation(DisableRTE.class);

        wasEnabled = getBackdoor().applicationProperties().getOption(APKeys.JIRA_OPTION_RTE_ENABLED);
        wasOptional = getBackdoor().darkFeatures().isGlobalEnabled(RTE_OPTIONAL_FEATURE);

        if (disableRTE.optional()) {
            getBackdoor().darkFeatures().enableForSite(RTE_OPTIONAL_FEATURE);
            getBackdoor().applicationProperties().setOption(APKeys.JIRA_OPTION_RTE_ENABLED, true);
        } else {
            getBackdoor().applicationProperties().setOption(APKeys.JIRA_OPTION_RTE_ENABLED, false);
        }
    }

    private Backdoor getBackdoor() {
        return jira.backdoor();
    }
}
