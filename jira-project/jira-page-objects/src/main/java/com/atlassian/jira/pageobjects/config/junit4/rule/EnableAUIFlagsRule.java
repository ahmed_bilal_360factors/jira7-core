package com.atlassian.jira.pageobjects.config.junit4.rule;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.functest.framework.util.junit.AnnotatedDescription;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.EnableAUIFlags;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.function.Supplier;

/**
 * @since v7.2
 */
public class EnableAUIFlagsRule extends TestWatcher {

    @Inject
    private JiraTestedProduct jira;

    @Override
    protected void starting(@Nonnull final Description description) {
        if (shouldEnable(description)) {
            getBackdoor().flags().enableFlags();
        }
    }

    @Override
    protected void finished(final Description description) {
        if (shouldEnable(description)) {
            getBackdoor().flags().disableFlags();
        }
    }

    private boolean shouldEnable(final Description description) {
        final AnnotatedDescription annotatedDescription = new AnnotatedDescription(description);
        return annotatedDescription.isAnnotatedWith(EnableAUIFlags.class);
    }

    private Backdoor getBackdoor() {
        return jira.backdoor();
    }

}
