package com.atlassian.jira.pageobjects.pages.reports;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

/**
 * @since 7.2
 */
public class FieldTestReportResultPage extends AbstractJiraPage {

    @ElementBy(cssSelector = ".reportHeading .formtitle")
    private PageElement formTitle;

    @ElementBy(xpath = "//*[@class='aui-page-header-actions']//a[contains(text(), 'Configure')]")
    private PageElement configureButton;

    @Override
    public TimedCondition isAt() {
        return formTitle.timed().hasText("Field Test Report");
    }

    @Override
    public String getUrl() {
        throw new UnsupportedOperationException("Can't navigate directly to this page.");
    }

    public FieldTestConfigureReportPage configure() {
        configureButton.click();
        return pageBinder.bind(FieldTestConfigureReportPage.class);
    }
}
