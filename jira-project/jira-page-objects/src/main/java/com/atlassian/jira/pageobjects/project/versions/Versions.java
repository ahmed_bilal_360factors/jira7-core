package com.atlassian.jira.pageobjects.project.versions;

import com.google.common.base.Function;

/**
 * @since v5.1
 */
public class Versions {
    public static Function<Version, String> getName() {
        return new Function<Version, String>() {
            @Override
            public String apply(Version input) {
                return input.getName();
            }
        };
    }
}
