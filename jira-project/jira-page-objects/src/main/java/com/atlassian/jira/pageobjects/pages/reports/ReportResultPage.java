package com.atlassian.jira.pageobjects.pages.reports;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

/**
 * PageObject for the ReportResult page (the page the shows the final rendered report). There isn't much interaction on
 * this page, but we do need to assert some things about the contents of the sidebar (if it even exists).
 *
 * @since v7.2
 */
public class ReportResultPage extends AbstractJiraPage {

    @ElementBy(className = "aui-sidebar-body")
    private PageElement sidebar;

    /**
     * If there is a sidebar on the page for this report, return the name of the project in the sidebar. Otherwise,
     * return empty denoting no sidebar (and therefore, no project) present.
     */
    public Optional<String> getProjectNameInSidebar() {
        return sidebar.isPresent() ? ofNullable(sidebar.find(By.className("project-title")).getText()) : empty();
    }

    @Override
    public TimedCondition isAt() {
        return sidebar.timed().isVisible();
    }

    @Override
    public String getUrl() {
        throw new UnsupportedOperationException("Can't directly navigate to this page.");
    }
}
