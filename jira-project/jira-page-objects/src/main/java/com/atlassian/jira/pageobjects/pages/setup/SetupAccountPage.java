package com.atlassian.jira.pageobjects.pages.setup;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import java.util.Optional;

public class SetupAccountPage extends AbstractJiraPage {

    private static final String URI = "/secure/SetupAccount!default.jspa";

    private final String withLicenseString;

    @ElementBy(id = "jira-setup-account")
    private PageElement form;

    @ElementBy(name = "jira-setup-account-field-email")
    private PageElement emailField;

    @ElementBy(id = "jira-setup-account-button-submit")
    private PageElement nextButton;

    @ElementBy(name = "jira-setup-account-field-password")
    private PageElement passwordField;

    @ElementBy(name = "jira-setup-account-field-retype-password")
    private PageElement passwordRetypeField;

    public SetupAccountPage(String withLicenseString) {
        this.withLicenseString = withLicenseString;
    }

    public SetupAccountPage() {
        this(null);
    }

    @Override
    public TimedCondition isAt() {
        return form.timed().isPresent();
    }

    @Override
    public String getUrl() {
        return Optional.ofNullable(this.withLicenseString)
                .map(license -> URI + "?productLicense=" + license)
                .orElse(URI);
    }

    public SetupAccountPage setEmail(String email) {
        emailField.type(email);
        return this;
    }

    public SetupAccountPage setPassword(String password) {
        passwordField.type(password);
        return this;
    }

    public SetupAccountPage setRetypePassword(String password) {
        passwordRetypeField.type(password);
        return this;
    }

    public SetupAccountPage setPasswordAndRetype(String password) {
        return setPassword(password).setRetypePassword(password);
    }

    public SetupFinishingPage clickNext() {
        nextButton.click();
        return pageBinder.bind(SetupFinishingPage.class);
    }
}
