package com.atlassian.jira.pageobjects.pages.webhooks;

import com.google.common.base.Objects;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static com.google.common.base.Strings.nullToEmpty;

public final class DisplayedWebHookListener {
    private final String name;
    private final String description;
    private final String url;
    private final boolean excludeBody;
    private final String issueSectionFilter;
    private final Set<String> events;

    public DisplayedWebHookListener(final String name, final String description, final String url, final boolean excludeBody, final String issueSectionFilter, final Set<String> events) {
        this.name = nullToEmpty(name);
        this.description = nullToEmpty(description);
        this.url = nullToEmpty(url);
        this.excludeBody = excludeBody;
        this.issueSectionFilter = nullToEmpty(issueSectionFilter);
        this.events = Objects.firstNonNull(events, Collections.<String>emptySet());
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public boolean isExcludeBody() {
        return excludeBody;
    }

    public String getIssueSectionFilter() {
        return issueSectionFilter;
    }

    public Set<String> getEvents() {
        return events;
    }

    public String[] getEventsAsArray() {
        return getEvents().toArray(new String[getEvents().size()]);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name, description, url, excludeBody, issueSectionFilter, events);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final DisplayedWebHookListener other = (DisplayedWebHookListener) obj;
        return Objects.equal(this.name, other.name)
                && Objects.equal(this.description, other.description)
                && Objects.equal(this.url, other.url)
                && Objects.equal(this.excludeBody, other.excludeBody)
                && Objects.equal(this.issueSectionFilter, other.issueSectionFilter)
                && Objects.equal(this.events, other.events);
    }

    @Override
    public String toString() {
        return "DisplayedWebHookListener{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", url='" + url + '\'' +
                ", excludeBody=" + excludeBody +
                ", issueSectionFilter='" + issueSectionFilter + '\'' +
                ", events=" + events +
                '}';
    }

    public static Builder builder() {
        return new Builder();
    }

    public DisplayedWebHookListener withUrl(final String url) {
        return new Builder(this).url(url).build();
    }

    public DisplayedWebHookListener withDescription(final String description) {
        return new Builder(this).description(description).build();
    }

    public DisplayedWebHookListener withEvents(final HashSet<String> events) {
        return new Builder(this).events(events).build();
    }

    public static final class Builder {

        private String name;
        private String description;
        private String url;
        private boolean excludeBody;
        private String issueSectionFilter;
        private Set<String> events;

        private Builder() {

        }

        private Builder(DisplayedWebHookListener listener) {
            name = listener.name;
            description = listener.description;
            url = listener.url;
            excludeBody = listener.excludeBody;
            issueSectionFilter = listener.issueSectionFilter;
            events = listener.events;
        }

        public Builder name(final String name) {
            this.name = name;
            return this;
        }

        public Builder description(final String description) {
            this.description = description;
            return this;
        }

        public Builder url(final String url) {
            this.url = url;
            return this;
        }

        public Builder excludeBody(final boolean excludeBody) {
            this.excludeBody = excludeBody;
            return this;
        }

        public Builder issueSectionFilter(final String issueSectionFilter) {
            this.issueSectionFilter = issueSectionFilter;
            return this;
        }

        public Builder events(final Set<String> events) {
            this.events = events;
            return this;
        }

        public DisplayedWebHookListener build() {
            return new DisplayedWebHookListener(name, description, url, excludeBody, issueSectionFilter, events);
        }
    }
}
