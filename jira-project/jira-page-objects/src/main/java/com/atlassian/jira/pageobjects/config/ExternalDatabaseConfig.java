package com.atlassian.jira.pageobjects.config;

/**
 * Represents the data required to configure an external database during JIRA Setup.
 *
 * In order to instantiate this class, use {@link DatabaseConfigProvider}.
 *
 * @since v7.2
 * @see DatabaseConfigProvider
 */
public class ExternalDatabaseConfig {
    private String databaseType;
    private String hostName;
    private String port;
    private String databaseName;
    private String username;
    private String password;
    private String schema;

    ExternalDatabaseConfig() {
    }

    public ExternalDatabaseConfig databaseType(String databaseType) {
        this.databaseType = databaseType;
        return this;
    }

    public ExternalDatabaseConfig hostName(String hostName) {
        this.hostName = hostName;
        return this;
    }

    public ExternalDatabaseConfig port(String port) {
        this.port = port;
        return this;
    }

    public ExternalDatabaseConfig databaseName(String databaseName) {
        this.databaseName = databaseName;
        return this;
    }

    public ExternalDatabaseConfig username(String username) {
        this.username = username;
        return this;
    }

    public ExternalDatabaseConfig password(String password) {
        this.password = password;
        return this;
    }

    public ExternalDatabaseConfig schema(String schema) {
        this.schema = schema;
        return this;
    }

    public String getDatabaseType() {
        return databaseType;
    }

    public String getHostName() {
        return hostName;
    }

    public String getPort() {
        return port;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getSchema() {
        return schema;
    }

    @Override
    public String toString() {
        return "ExternalDatabaseConfig{" +
                "databaseType='" + databaseType + '\'' +
                ", hostName='" + hostName + '\'' +
                ", port='" + port + '\'' +
                ", databaseName='" + databaseName + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", schema='" + schema + '\'' +
                '}';
    }
}
