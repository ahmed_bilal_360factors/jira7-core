package com.atlassian.jira.pageobjects.pages;

import com.atlassian.jira.pageobjects.dialogs.FeedbackDialog;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

/**
 * This page detects whether the feedback button is present on the header navigator.
 */
public final class DashboardPageWithFeedbackButton extends DashboardPage {
    @ElementBy(className = "jira-feedback-plugin")
    private PageElement feedbackButton;

    public FeedbackDialog clickFeedbackButton() {
        feedbackButton.click();
        return pageBinder.bind(FeedbackDialog.class);
    }

    public boolean hasFeedbackButton() {
        return feedbackButton.isPresent();
    }
}
