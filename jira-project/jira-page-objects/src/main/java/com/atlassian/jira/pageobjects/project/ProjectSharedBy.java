package com.atlassian.jira.pageobjects.project;

import com.atlassian.jira.webtest.webdriver.util.AUIBlanket;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Represents the "Shared By" project dropdown shown within JIRA.
 *
 * @since v4.4
 */
public class ProjectSharedBy {
    private PageElement container;

    @Inject
    private PageElementFinder elementFinder;

    @Inject
    protected PageBinder binder;

    @Inject
    private WebDriver webDriver;

    private final String INLINE_DIALOG_CONTAINER_SELECTOR = ".aui-inline-dialog-contents .shared-item-content";
    private final String DROPDOWN_TRIGGER = ".shared-item-trigger";

    public ProjectSharedBy(PageElement container) {
        this.container = container;
    }

    public boolean isPresent() {
        return container.isPresent();
    }

    public boolean isTiggerPresent() {
        return isPresent() && getTriggerElement().isPresent();
    }

    public String getTriggerText() {
        return getTriggerElement().getText();
    }

    public List<String> getProjects() {
        openDialog();

        String href = getTriggerElement().getAttribute("href");
        int triggerTargetStart = href.indexOf("#");

        String dialogId = "inline-dialog-" + href.substring(triggerTargetStart + 1);

        PageElement dialog = elementFinder.find(By.id(dialogId));
        assertTrue("The dialog did not appear to open.", dialog.isPresent());

        List<PageElement> elements = dialog.findAll(By.tagName("li"));
        List<String> projects = new ArrayList<String>(elements.size());
        for (PageElement element : elements) {
            projects.add(element.getText());
        }

        closeDialog();

        return projects;
    }

    private PageElement getTriggerElement() {
        return container.find(By.cssSelector(DROPDOWN_TRIGGER));
    }

    public void openDialog() {
        Poller.waitUntilTrue("The trigger could not be found.", getTriggerElement().timed().isVisible());
        getTriggerElement().click();
        Poller.waitUntilTrue("dialog should be open", elementFinder.find(By.cssSelector(INLINE_DIALOG_CONTAINER_SELECTOR)).timed().isVisible());
    }

    public void closeDialog() {
        // Type 'esc' to close the dialog so that we don't accidentally click on a link to somewhere else.
        this.container.type(Keys.ESCAPE);
        waitUntilClosed();
    }

    public boolean isDialogOpen() {
        final PageElement dialog = elementFinder.find(By.cssSelector(INLINE_DIALOG_CONTAINER_SELECTOR));
        return dialog.isPresent() && dialog.isVisible();
    }

    private void waitUntilClosed() {
        Poller.waitUntilFalse("dialog should be closed", elementFinder.find(By.cssSelector(INLINE_DIALOG_CONTAINER_SELECTOR)).timed().isVisible());
        AUIBlanket auiBlanket = binder.bind(AUIBlanket.class);
        auiBlanket.waitUntilClosed("aui blanket should be closed");
    }
}
