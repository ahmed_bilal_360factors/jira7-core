package com.atlassian.jira.pageobjects.config;

import com.google.common.collect.ImmutableList;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 * Defines common collections of popup ids and merges them.
 *
 * @since v7.1.0
 */
public class PopupCollector {
    private static final Collection<String> CLOUD_HELP_TIP_IDS = ImmutableList.of(
            "add.new.users",
            "automaticTransitionDevSummaryTooltip",
            "create-sprint",
            "devstatus.cta.createbranch.tooltip",
            "hipchat.feature.discovery.tip",
            "newsletter-signup-tip",
            "onboarding-create-sprint",
            "onboarding-start-sprint",
            "start-sprint",
            "sidebar-chaperone-collapse-tip",
            "sidebar-chaperone-disable-tip",
            "sidebar-chaperone-general-overview-tip"
    );
    private static final Collection<String> CLOUD_NOTIFICATION_FLAG_IDS = ImmutableList.of(
            "com.atlassian.jira.baseurl",
            "com.atlassian.jira.reindex.required",
            "com.atlassian.jira.tzdetect.39600000%2C36000000"
    );

    private final Collection<String> helpTipIds = new HashSet<>();
    private final Collection<String> notificationFlagIds = new HashSet<>();

    public PopupCollector bundledWithCloud() {
        return helpTips(CLOUD_HELP_TIP_IDS).notificationFlags(CLOUD_NOTIFICATION_FLAG_IDS);
    }

    public PopupCollector helpTips(final Collection<String> helpTipIds) {
        this.helpTipIds.addAll(helpTipIds);
        return this;
    }

    public PopupCollector notificationFlags(final Collection<String> notificationFlagIds) {
        this.notificationFlagIds.addAll(notificationFlagIds);
        return this;
    }

    public Collection<String> collectHelpTipIds() {
        return Collections.unmodifiableCollection(helpTipIds);
    }

    public Collection<String> collectNotificationFlagIds() {
        return Collections.unmodifiableCollection(notificationFlagIds);
    }
}
