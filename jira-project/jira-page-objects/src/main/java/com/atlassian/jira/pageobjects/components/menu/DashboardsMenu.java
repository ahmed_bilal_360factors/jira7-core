package com.atlassian.jira.pageobjects.components.menu;

import com.atlassian.jira.pageobjects.pages.DashboardPage;
import org.openqa.selenium.By;

/**
 * @since v6.4
 */
public class DashboardsMenu extends JiraAuiDropdownMenu {

    public DashboardsMenu() {
        super(By.id("home_link"), By.id("home_link-content"));
    }

    public DashboardsMenu open() {
        super.open();
        return this;
    }

    public DashboardPage getDashboard(final String dashboardName) {
        openAndClick(By.linkText(dashboardName));
        return pageBinder.bind(DashboardPage.class);
    }

}
