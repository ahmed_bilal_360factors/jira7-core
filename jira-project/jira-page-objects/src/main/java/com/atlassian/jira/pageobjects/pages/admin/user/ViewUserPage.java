package com.atlassian.jira.pageobjects.pages.admin.user;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.pageobjects.components.UserApplicationAccess;
import com.atlassian.jira.pageobjects.framework.elements.ExtendedElementFinder;
import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.jira.pageobjects.framework.elements.PageElements.transformTimed;

/**
 * @since 4.4
 */
public class ViewUserPage extends AbstractJiraPage {

    private static final String URI = "/secure/admin/user/ViewUser.jspa?name=";

    @Inject
    protected ExtendedElementFinder extendedFinder;

    @ElementBy(id = "username")
    protected PageElement username;

    @FindBy(className = "fn")
    protected WebElement fullname;

    @FindBy(className = "email")
    protected WebElement email;

    @FindBy(id = "loginCount")
    protected WebElement loginCount;

    @FindBy(id = "lastLogin")
    protected WebElement lastLogin;

    @FindBy(id = "previousLogin")
    protected WebElement previousLogin;

    @FindBy(id = "lastFailedLogin")
    protected WebElement lastFailedLogin;

    @FindBy(id = "currentFailedLoginCount")
    protected WebElement currentFailedLoginCount;

    @FindBy(id = "totalFailedLoginCount")
    protected WebElement totalFailedLoginCount;

    @ElementBy(id = "deleteuser_link")
    protected PageElement deleteUserLink;

    @FindBy(id = "editgroups_link")
    protected WebElement editUserLink;

    @ElementBy(linkText = "Edit Details")
    protected PageElement editUserDetailsLink;

    @ElementBy(linkText = "Set Password")
    protected PageElement setPasswordLink;

    @ElementBy(id = "groups")
    protected PageElement userGroups;

    @ElementBy(className = "view-user-no-groups-warning")
    protected PageElement noGroupsWarning;

    @ElementBy(className = "view-user-no-applications-warning")
    protected PageElement noApplicationsWarning;

    // TODO: groups are currently image bullets, don't handle them. Make JIRA change to <ul>.

    protected final String name;

    @Inject
    private TraceContext traceContext;

    protected PageElement getUserGroups() {
        return elementFinder.find(By.id("groups"));
    }

    public ViewUserPage(String username) {
        this.name = username;
    }

    @Override
    public String getUrl() {
        return URI + name;
    }

    @Override
    public TimedCondition isAt() {
        return deleteUserLink.timed().isPresent();
    }

    public UserApplicationAccess getApplicationAccess() {
        return pageBinder.bind(UserApplicationAccess.class, By.className("view-user-applications-and-groups-module"));
    }

    public ViewUserPage toggleApplication(String applicationKey) {
        final Tracer checkpoint = traceContext.checkpoint();

        getApplicationAccess().toggleApplication(applicationKey);

        traceContext.waitFor(checkpoint, "view-user-select");
        return this;
    }

    public DeleteUserPage gotoDeleteUserPage() {
        deleteUserLink.click();
        return pageBinder.bind(DeleteUserPage.class);
    }

    public EditUserPasswordPage setPassword() {
        String username = getUsername();
        setPasswordLink.click();
        return pageBinder.bind(EditUserPasswordPage.class, username);
    }

    public EditUserDetailsPage editDetails() {
        String username = getUsername();
        editUserDetailsLink.click();
        return pageBinder.bind(EditUserDetailsPage.class, username);
    }

    public Page viewProjectsRoles() {
        throw new UnsupportedOperationException("View project roles on ViewUserPage is not supported");
    }

    public EditUserGroupsPage editGroups() {
        final String username = getUsername();
        editUserLink.click();
        return pageBinder.bind(EditUserGroupsPage.class, username);
    }

    public List<GroupRow> getGroupRows() {
        final Supplier<Iterable<PageElement>> supplier = extendedFinder.within(userGroups).newQuery(GroupRow.SELECTOR).supplier();
        return ImmutableList.copyOf(transformTimed(timeouts, pageBinder, supplier, GroupRow.class).byDefaultTimeout());
    }

    public List<String> getGroupNames() {
        return getGroupRows().stream()
                .map(element -> element.getName())
                .collect(CollectorsUtil.toImmutableList());
    }

    public GroupRow getGroupRow(String groupName) {
        return getGroupRows().stream()
                .filter(row -> row.getName().equals(groupName))
                .findFirst().get();
    }

    public Page editProperties() {
        throw new UnsupportedOperationException("Edit properties on the ViewUserPage is not supported");
    }

    public Page viewPublicProfile() {
        throw new UnsupportedOperationException("View public profile on ViewUserPage is not supported");
    }

    public String getUsername() {
        return username.getText();
    }

    public String getFullname() {
        return fullname.getText();
    }

    public String getEmail() {
        return email.getText();
    }

    public String getLoginCount() {
        return loginCount.getText();
    }

    public String getLastLogin() {
        return lastLogin.getText();
    }

    public String getPreviousLogin() {
        return previousLogin.getText();
    }

    public String getLastFailedLogin() {
        return lastFailedLogin.getText();
    }

    public String getCurrentFailedLoginCount() {
        return currentFailedLoginCount.getText();
    }

    public String getTotalFailedLoginCount() {
        return totalFailedLoginCount.getText();
    }

    public TimedCondition isNoGroupsWarningVisible() {
        return noGroupsWarning.timed().isVisible();
    }

    public TimedCondition isNoApplicationsWarningVisible() {
        return noApplicationsWarning.timed().isVisible();
    }

    public static class GroupRow {
        private static final By SELECTOR = By.cssSelector("*[data-group-name]");
        private static final By NAME_SELECTOR = By.cssSelector(".group-name a");
        private static final By LABELS_SELECTOR = By.cssSelector(".group-labels .group-label-lozenge");

        private final PageElement pageElement;

        public GroupRow(PageElement pageElement) {
            this.pageElement = pageElement;
        }

        public String getName() {
            return getLink().getText();
        }

        public PageElement getLink() {
            return pageElement.find(NAME_SELECTOR);
        }

        public List<String> getLabels() {
            return pageElement.findAll(LABELS_SELECTOR).stream()
                    .map(element -> element.getText())
                    .collect(CollectorsUtil.toImmutableList());
        }

    }
}
