package com.atlassian.jira.pageobjects.pages.webhooks;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;

import java.util.List;
import java.util.Set;

import static com.atlassian.pageobjects.elements.query.Conditions.CombinableCondition;
import static com.atlassian.pageobjects.elements.query.Conditions.or;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;

public class EditWebHookListenerPanel extends AbstractWebHookPanel {
    @ElementBy(id = "webhook-submit")
    private PageElement submit;

    @ElementBy(id = "webhook-enabled")
    private PageElement webHookEnabledButton;

    @ElementBy(id = "webhook-disabled")
    private PageElement webHookDisabledButton;

    @ElementBy(id = "webhook-cancel")
    private PageElement cancel;

    @ElementBy(cssSelector = "input[data-event-type]")
    private Iterable<PageElement> eventsInput;

    @WaitUntil
    public void waitUntil() {
        final ImmutableList.Builder<TimedQuery<Boolean>> builder = ImmutableList.builder();
        builder.add(webHookName.timed().isVisible());
        builder.add(submit.timed().isVisible());
        builder.add(webHookDescription.timed().isVisible());
        builder.addAll(transform(eventsInput, pageElement -> pageElement.timed().isVisible()));
        builder.add(finder.find(By.cssSelector(".webhook-details form")).timed().hasClass("display-mode-edit"));
        Poller.waitUntilTrue(Conditions.and(builder.build()));
    }

    public EditWebHookListenerPanel setFields(DisplayedWebHookListener listener) {
        setName(listener.getName());
        setUrl(listener.getUrl());
        setDescription(listener.getDescription());
        setEvents(listener.getEvents());
        excludeDetails(listener.isExcludeBody());
        setFilter(listener.getIssueSectionFilter());
        return this;
    }

    public EditWebHookListenerPanel setName(final String name) {
        webHookName.clear();
        webHookName.type(name);
        return this;
    }

    public EditWebHookListenerPanel setUrl(final String url) {
        webHookUrl.clear();
        webHookUrl.type(url);
        return this;
    }

    public EditWebHookListenerPanel setDescription(final String description) {
        webHookDescription.clear();
        webHookDescription.type(description);
        return this;
    }

    public EditWebHookListenerPanel setFilter(final String filter) {
        jql.clear();
        jql.type(filter);
        return this;
    }

    public EditWebHookListenerPanel setEvents(Set<String> events) {
        for (PageElement event : eventsInput) {
            String webHookId = event.getAttribute("data-event-type");
            boolean isChecked = event.isSelected();
            boolean shouldBeSelected = events.contains(webHookId);
            if (isChecked != shouldBeSelected) {
                System.out.println("toogling event  " + webHookId);
                event.toggle();
            }
        }
        return this;
    }

    public EditWebHookListenerPanel excludeDetails(final boolean exclude) {
        if (excludeDetails.isSelected() != exclude) {
            excludeDetails.toggle();
        }
        return this;
    }

    public EditWebHookListenerPanel disable() {
        webHookDisabledButton.click();
        return this;
    }

    public EditWebHookListenerPanel enabled() {
        webHookEnabledButton.click();
        return this;
    }

    public ViewWebHookListenerPanel submit() {
        submit.click();
        Poller.waitUntilTrue(finder.find(By.cssSelector(".webhook-details form")).timed().hasClass("display-mode-display"));
        return binder.bind(ViewWebHookListenerPanel.class);
    }

    public EditWebHookListenerPanel submitAndExpectErrors() {
        submit.click();
        final CombinableCondition condition = or(transform(finder.findAll(By.cssSelector(".webhook-details div.error")), new Function<PageElement, TimedQuery<Boolean>>() {
            @Override
            public TimedQuery<Boolean> apply(final PageElement pageElement) {
                return pageElement.timed().isVisible();
            }
        }));
        Poller.waitUntilTrue(condition);
        return binder.bind(EditWebHookListenerPanel.class);
    }

    public EditWebHookListenerPanel cancel() {
        cancel.click();
        return this;
    }

    public List<String> getErrors() {
        return newArrayList(filter(transform(finder.findAll(By.cssSelector(".webhook-details div.error")), new Function<PageElement, String>() {
            @Override
            public String apply(final PageElement element) {
                return element.getText();
            }
        }), new Predicate<String>() {
            @Override
            public boolean apply(final String value) {
                return StringUtils.isNotEmpty(value);
            }
        }));
    }

}
