package com.atlassian.jira.pageobjects.project.workflow;

import com.atlassian.jira.pageobjects.dialogs.admin.DiscardDraftWorkflowSchemeDialog;
import com.atlassian.jira.pageobjects.dialogs.admin.ViewWorkflowTextDialog;
import com.atlassian.jira.pageobjects.pages.admin.workflow.SelectWorkflowScheme;
import com.atlassian.jira.pageobjects.pages.admin.workflow.StartDraftWorkflowSchemeMigrationPage;
import com.atlassian.jira.pageobjects.pages.admin.workflow.ViewWorkflowSteps;
import com.atlassian.jira.pageobjects.pages.admin.workflow.WorkflowDesignerPage;
import com.atlassian.jira.pageobjects.project.AbstractProjectConfigPageTab;
import com.atlassian.jira.pageobjects.project.ProjectSharedBy;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.pageobjects.websudo.DecoratedJiraWebSudo;
import com.atlassian.jira.pageobjects.websudo.JiraSudoFormDialog;
import com.atlassian.jira.pageobjects.websudo.JiraWebSudo;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static com.atlassian.jira.pageobjects.dialogs.admin.ViewWorkflowTextDialog.WorkflowTransition;
import static com.atlassian.pageobjects.elements.query.Conditions.and;
import static com.atlassian.pageobjects.elements.query.Conditions.not;
import static com.google.common.collect.ImmutableList.of;
import static org.junit.Assert.assertTrue;

/**
 * Page object for the workflows tab.
 *
 * @since v4.4
 */
public class WorkflowsPageTab extends AbstractProjectConfigPageTab {
    private static final String CHANGE_LINK_ID = "project-config-workflows-scheme-change";
    static final String TRACE_KEY = "project.config.complete";

    private final String projectKey;
    private final String workflowName;
    @Inject
    private PageElementFinder locator;

    @ElementBy(id = "project-config-panel-container")
    private PageElement container;

    @Inject
    private TraceContext traceContext;

    private PageElement schemeName;
    private PageElement schemeChangeLink;
    private PageElement addWorkflowDropdown;
    private PageElement addWorkflowButton;
    private PageElement importBundleButton;
    private PageElement header;
    private PageElement discardDraftLink;
    private PageElement publishDraftButton;
    private PageElement toolbar;
    private PageElement draftBar;
    private PageElement draftLozenge;

    public WorkflowsPageTab() {
        this(null);
    }

    public WorkflowsPageTab(String projectKey) {
        this(projectKey, null);
    }

    public WorkflowsPageTab(String projectKey, String workflowName) {
        this.projectKey = projectKey;
        this.workflowName = workflowName;
    }

    @Init
    public void initialise() {
        schemeName = container.find(By.className("project-config-workflows-scheme-name"));
        header = container.find(By.className("project-config-panel-header"));
        toolbar = container.find(By.className("project-config-workflows-toolbar"));
        draftBar = container.find(By.id("project-config-workflows-draft-message"));

        schemeChangeLink = toolbar.find(By.id(CHANGE_LINK_ID));
        addWorkflowDropdown = toolbar.find(By.id("project-config-workflow-add-dropdown-trigger"));
        addWorkflowButton = locator.find(By.id("project-config-workflow-add-button"));
        importBundleButton = locator.find(By.id("project-config-workflow-marketplace-button"));
        discardDraftLink = draftBar.find(By.id("project-config-workflows-discard-draft"));
        publishDraftButton = draftBar.find(By.id("project-config-workflows-publish-draft"));
        draftLozenge = header.find(By.className("status-draft"));
    }

    public ProjectSharedBy sharedBy() {
        final PageElement sharedBy = header.find(By.className("shared-by"));
        return pageBinder.bind(ProjectSharedBy.class, sharedBy);
    }

    public boolean isSchemeChangeAvailable() {
        return schemeChangeLink.isPresent() && schemeChangeLink.isVisible();
    }

    public boolean isAddWorkflowDropdownPresent() {
        return addWorkflowDropdown.isPresent() && addWorkflowDropdown.isVisible();
    }

    public boolean isAddWorkflowButtonPresent() {
        return isAddWorkflowDropdownPresent() && addWorkflowButton.isPresent();
    }

    public boolean isImportBundleButtonPresent() {
        return isAddWorkflowDropdownPresent() && importBundleButton.isPresent();
    }

    public String getImportBundleButtonLabel() {
        return importBundleButton.getAttribute("text");
    }

    public boolean canAddExistingWorkflow() {
        return isAddWorkflowButtonPresent() && !addWorkflowDropdown.hasClass("disabled") && !addWorkflowButton.hasClass("disabled");
    }

    public boolean canImportBundle() {
        return isImportBundleButtonPresent() && !addWorkflowDropdown.hasClass("disabled");
    }

    public boolean isDiscardLinkPresent() {
        return discardDraftLink.isPresent() && discardDraftLink.isVisible();
    }

    public boolean isPublishButtonPresent() {
        return publishDraftButton.isPresent() && publishDraftButton.isVisible();
    }

    public SelectWorkflowScheme gotoSelectScheme() {
        final long projectId = getProjectId();
        schemeChangeLink.click();
        return pageBinder.bind(SelectWorkflowScheme.class, projectId);
    }

    public String getSchemeName() {
        return schemeName.getText();
    }

    public List<Workflow> getWorkflowPanels() {
        final List<PageElement> rows = container.findAll(By.className("project-config-workflow-row"));
        final ArrayList<Workflow> workflows = Lists.newArrayListWithExpectedSize(rows.size());

        for (PageElement row : rows) {
            workflows.add(new Workflow(row));
        }
        return workflows;
    }

    public Workflow getPanelForName(final String name) {
        return Iterables.find(getWorkflowPanels(), new Predicate<Workflow>() {
            @Override
            public boolean apply(WorkflowsPageTab.Workflow input) {
                return input.getDisplayName().equalsIgnoreCase(name);
            }
        });
    }

    public AddWorkflowDialog addWorkflow() {
        addWorkflowDropdown.click();
        addWorkflowButton.click();
        return pageBinder.bind(AddWorkflowDialog.class);
    }

    public boolean isDraft() {
        return draftLozenge.isPresent();
    }

    public String getLastModifiedUser() {
        if (draftLozenge.isPresent()) {
            String title = draftLozenge.getAttribute("title").replaceFirst("This draft was last edited by ", "");
            return title.substring(0, title.indexOf(" at "));
        }
        return null;
    }

    public boolean isLastModifiedUserMe() {
        return "you".equals(getLastModifiedUser());
    }

    public WorkflowsPageTab discardDraft() {
        Tracer checkpoint = traceContext.checkpoint();
        discardDraftLink.click();
        pageBinder.bind(DiscardDraftWorkflowSchemeDialog.class).submit();
        waitForLoad(checkpoint);

        return pageBinder.bind(WorkflowsPageTab.class, projectKey);
    }

    public StartDraftWorkflowSchemeMigrationPage publishDraft(Long schemeId) {
        publishDraftButton.click();
        return pageBinder.bind(StartDraftWorkflowSchemeMigrationPage.class, schemeId);
    }

    public JiraWebSudo discardDraftWebSudo() {
        final Tracer checkpoint = traceContext.checkpoint();
        discardDraftLink.click();
        pageBinder.bind(DiscardDraftWorkflowSchemeDialog.class).submit();

        final JiraSudoFormDialog bind = pageBinder.bind(JiraSudoFormDialog.class, JiraSudoFormDialog.ID_SMART_WEBSUDO);
        return new DecoratedJiraWebSudo(bind) {
            @Override
            protected void afterAuthenticate() {
                waitForLoad(checkpoint);
            }
        };
    }

    public WorkflowsPageTab viewOriginal() {
        Tracer checkpoint = traceContext.checkpoint();
        getViewOriginalElement().click();
        waitForLoad(checkpoint);

        return pageBinder.bind(WorkflowsPageTab.class, projectKey);
    }

    private void waitForLoad(Tracer checkpoint) {
        traceContext.waitFor(checkpoint, TRACE_KEY);
    }

    private PageElement getViewOriginalElement() {
        return draftBar.find(By.id("project-config-workflows-view-original"));
    }

    public WorkflowsPageTab viewDraft() {
        Tracer checkpoint = traceContext.checkpoint();
        getViewDraftElement().click();
        waitForLoad(checkpoint);

        return pageBinder.bind(WorkflowsPageTab.class, projectKey);
    }

    private PageElement getViewDraftElement() {
        return draftBar.find(By.id("project-config-workflows-view-draft"));
    }

    @Override
    public String getUrl() {
        if (projectKey == null) {
            throw new IllegalStateException("The projectKey must be specified to go directly to the page.");
        }

        if (workflowName == null) {
            return String.format("/plugins/servlet/project-config/%s/workflows", projectKey);
        }

        return String.format("/plugins/servlet/project-config/%s/workflows#workflowName=%s", projectKey, workflowName);
    }

    @Override
    public TimedCondition isAt() {
        final PageElement container = elementFinder.find(By.id("project-config-panel-container"));
        return and(of(container.withTimeout(TimeoutType.PAGE_LOAD).timed().isPresent(),
                not(container.find(By.id("project-config-panel-workflows")).withTimeout(TimeoutType.PAGE_LOAD).timed().hasClass("project-config-loading"))));
    }

    public boolean isViewOriginalPresent() {
        final PageElement viewOriginalElement = getViewOriginalElement();
        return viewOriginalElement.isPresent() && viewOriginalElement.isVisible();
    }

    public boolean isViewDraftPresent() {
        final PageElement viewDraftElement = getViewDraftElement();
        return viewDraftElement.isPresent() && viewDraftElement.isVisible();
    }

    public boolean isPublishEnabled() {
        return isPublishButtonPresent() && publishDraftButton.isEnabled();
    }

    public AssignIssueTypesDialog assignIssueTypesDialogOnPageLoad() {
        return pageBinder.bind(AssignIssueTypesDialog.class);
    }

    public Collection<String> getHeaderColumnNames() {
        return elementFinder.findAll(By.cssSelector(".project-config-workflow-header th")).stream().map(PageElement::getText).collect(toImmutableList());
    }

    public class Workflow {
        private final PageElement row;
        private final PageElement edit;
        private final PageElement textView;
        private final PageElement assignIssueTypesLink;
        private final PageElement deleteButton;

        private final String displayName;
        private final boolean readOnly;
        private final List<String> issueTypes;
        private final List<String> issueTypeLinks;

        public Workflow(PageElement row) {
            this.row = row;
            final PageElement nameElement = row.find(By.className("project-config-workflow-name"));
            this.displayName = nameElement.isPresent() ? nameElement.getText() : null;
            this.edit = row.find(By.cssSelector("a.project-config-workflow-edit"));
            this.textView = row.find(By.cssSelector("a.project-config-workflow-text-link"));
            final PageElement readOnly = row.find(By.className("read-only"));
            this.readOnly = readOnly.isPresent();
            this.assignIssueTypesLink = row.find(By.className("project-config-workflow-assign-issue-types-link"));
            this.deleteButton = row.find(By.className("project-config-workflow-remove"));


            final List<PageElement> issueTypeElements = row.findAll(By.className("project-config-issuetype-name"));
            final List<String> issueTypes = Lists.newArrayListWithCapacity(issueTypeElements.size());
            final List<String> issueTypeLinks = new ArrayList<>();
            for (PageElement issueType : issueTypeElements) {
                issueTypes.add(issueType.getText());
                PageElement link = issueType.find(By.cssSelector("a"));
                if (link.isPresent()) {
                    issueTypeLinks.add(link.getAttribute("href"));
                }
            }

            this.issueTypes = Collections.unmodifiableList(issueTypes);
            this.issueTypeLinks = Collections.unmodifiableList(issueTypeLinks);
        }

        public boolean canAssignIssueTypes() {
            return assignIssueTypesLink.isPresent() && assignIssueTypesLink.isVisible();
        }

        public boolean canEdit() {
            return edit.isPresent();
        }

        public String getEditLink() {
            return canEdit() ? edit.getAttribute("href") : null;
        }

        public boolean isReadOnly() {
            return readOnly;
        }

        public List<String> getIssueTypes() {
            return issueTypes;
        }

        public List<String> getIssueTypeLinks() {
            return issueTypeLinks;
        }

        public String getDisplayName() {
            return displayName;
        }

        public EditWorkflowDialog gotoEditWorkflowDialog() {
            assertTrue("Not able to edit this workflow.", canEdit());
            return clickEditWorkflowAndBind(EditWorkflowDialog.class);
        }

        public ViewWorkflowSteps clickEditAndGotoTextMode() {
            return clickEditWorkflowAndBind(ViewWorkflowSteps.class, displayName, true);
        }

        public WorkflowDesignerPage clickEditAndGotoDiagramMode() {
            return clickEditWorkflowAndBind(WorkflowDesignerPage.class, displayName, true);
        }

        public <P> P clickEditWorkflowAndBind(Class<P> page, Object... args) {
            edit.click();
            return pageBinder.bind(page, args);
        }

        public ViewWorkflowTextDialog openTextModeDialog() {
            textView.click();
            return pageBinder.bind(ViewWorkflowTextDialog.class);
        }

        public List<WorkflowTransition> getTransitions() {
            final ViewWorkflowTextDialog dialog = openTextModeDialog();
            try {
                return dialog.getTransitions();
            } finally {
                dialog.close();
            }
        }

        public AssignIssueTypesDialog assignIssueTypes() {
            assignIssueTypesLink.click();
            return pageBinder.bind(AssignIssueTypesDialog.class);
        }

        public void removeWorkflow() {
            Tracer checkpoint = traceContext.checkpoint();
            clickRemove();
            waitForLoad(checkpoint);
        }

        private void clickRemove() {
            deleteButton.click();
        }

        public JiraWebSudo removeWorkflowWebSudo() {
            final Tracer checkpoint = traceContext.checkpoint();
            clickRemove();
            final JiraSudoFormDialog bind = pageBinder.bind(JiraSudoFormDialog.class, JiraSudoFormDialog.ID_SMART_WEBSUDO);
            return new DecoratedJiraWebSudo(bind) {
                @Override
                protected void afterAuthenticate() {
                    waitForLoad(checkpoint);
                }
            };
        }

        public boolean canRemoveWorkflow() {
            return deleteButton.isPresent() && deleteButton.isVisible() && !deleteButton.hasClass("disabled");
        }

        public int getColumnCount() {
            return this.row.findAll(By.cssSelector("td")).size();
        }
    }
}
