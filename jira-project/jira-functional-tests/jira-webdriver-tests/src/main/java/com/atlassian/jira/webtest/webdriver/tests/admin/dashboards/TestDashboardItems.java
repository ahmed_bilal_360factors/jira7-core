package com.atlassian.jira.webtest.webdriver.tests.admin.dashboards;

import com.atlassian.fugue.Iterables;
import com.atlassian.fugue.Option;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.AddDashboardPage;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.jira.testkit.client.DashboardControl;
import it.com.atlassian.gadgets.pages.Gadget;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;

@WebTest({Category.WEBDRIVER_TEST, Category.DASHBOARDS, Category.REFERENCE_PLUGIN})
public class TestDashboardItems extends BaseJiraWebTest {
    private final String openSocialGadget = "Dashboard Item Demo Gadget";
    private final String localDashboardItem = "New server-side dashboard item";

    private DashboardPage dashboardPage;

    @Before
    public void setUpTest() {
        backdoor.restoreDataFromResource("xml/blankprojects.xml");
        backdoor.flags().clearFlags();
        dashboardPage = pageBinder.navigateToAndBind(DashboardPage.class);
    }

    @Test
    public void testCreatingAndDeletingDashboardItem() throws Exception {
        testCreateDeleteItem(this.localDashboardItem);
    }

    @Test
    public void testCreatingAndDeletingOpenSocialGadget() throws Exception {
        testCreateDeleteItem(this.openSocialGadget);
    }

    private void testCreateDeleteItem(String gadget) {
        createEmptyDashboard("test");
        addDashboardItem(gadget);

        final Gadget dashboardItem = dashboardPage.gadgets().getByTitle(gadget);
        assertThat(dashboardItem, notNullValue());

        dashboardPage.gadgets().waitForAuiBlanketToVanish();
        dashboardItem.delete();
        assertThat(dashboardPage.gadgets().hasGaddget(dashboardItem.getId()), is(false));
    }

    @Test
    public void testDashboardItemCanReplaceOpenSocialGadget() throws Exception {
        createEmptyDashboard("test");
        final String openSocialGadgetToReplace = "The title of this gadget will be replaced";
        final String localDashboardItemWhichReplaceSpecUri = "New local dashboard item that replaces \"The title of this gadget will be replaced\"";
        final List<String> visibleGadgets = dashboardPage.gadgets().openAddGadgetDialog().getVisibleGadgets();

        assertThat(visibleGadgets, hasItems(openSocialGadget, localDashboardItemWhichReplaceSpecUri));
        assertThat(visibleGadgets, not(hasItem(openSocialGadgetToReplace)));
    }

    @Test
    public void testSharingDashboardItems() {
        final String sharedPageName = "shared";
        createDashboard(sharedPageName, Option.none(), true);
        addDashboardItem(localDashboardItem);
        addDashboardItem(openSocialGadget);

        final Integer dashboardId = getOwnedDashboardId(sharedPageName);
        jira.logout();
        jira.quickLogin("fred", "fred");
        goToDashboard(dashboardId);

        assertThatDashboardItemExists(openSocialGadget);
        assertThatDashboardItemExists(localDashboardItem);
    }

    private Integer getOwnedDashboardId(final String sharedPageName) {
        final List<DashboardControl.Dashboard> dashboards = jira.backdoor().dashboard().getOwnedDashboard("admin");
        return Iterables.findFirst(dashboards, input -> input.getName().equals(sharedPageName)).get().getId().intValue();
    }

    @Test
    public void testCloningDashboardWithItems() {
        final String baseDashboard = "base";
        createEmptyDashboard(baseDashboard);
        addDashboardItem(openSocialGadget);
        addDashboardItem(localDashboardItem);

        createDashboard("copied", Option.some(baseDashboard), false);

        assertThatDashboardItemExists(openSocialGadget);
        assertThatDashboardItemExists(localDashboardItem);
    }

    private void assertThatDashboardItemExists(final String gadgetTitle) {
        assertThat(dashboardPage.gadgets().getByTitle(gadgetTitle), notNullValue());
    }

    private void createEmptyDashboard(final String name) {
        createDashboard(name, Option.none(), false);
    }

    private void createDashboard(final String name, final Option<String> baseDashboard, final boolean share) {
        final AddDashboardPage addDashboardPage = pageBinder.navigateToAndBind(AddDashboardPage.class);
        addDashboardPage.setName(name);
        if (baseDashboard.isDefined()) {
            addDashboardPage.copyFrom(baseDashboard.get());
        }
        if (share) {
            addDashboardPage.shareWithEveryone();
        }
        addDashboardPage.submit();
        goToFavoriteDashboard(name);
    }

    private void goToFavoriteDashboard(String name) {
        dashboardPage.switchDashboard(name);
    }

    private void goToDashboard(Integer id) {
        dashboardPage = pageBinder.navigateToAndBind(DashboardPage.class, id);
    }

    private void addDashboardItem(final String dashboardItemTitle) {
        dashboardPage.gadgets().openAddGadgetDialog().addGadget(dashboardItemTitle).simpleClose();
    }
}
