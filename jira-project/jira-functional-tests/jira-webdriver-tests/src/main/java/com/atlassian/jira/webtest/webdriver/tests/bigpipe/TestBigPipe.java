package com.atlassian.jira.webtest.webdriver.tests.bigpipe;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.ResetData;
import com.atlassian.jira.pageobjects.navigator.BasicSearch;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.testkit.client.restclient.DarkFeature;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client.SiteDarkFeaturesClientExt;
import com.atlassian.pageobjects.DelayedBinder;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.google.common.base.Stopwatch;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertThat;

/**
 * BigPipe webdriver tests.
 *
 * @since v7.1
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ISSUES})
@ResetData
@RunWith(UnstableLoadStrategyJiraWebTestRunner.class)
public class TestBigPipe extends BaseJiraWebTest {
    private static final Logger log = LoggerFactory.getLogger(TestBigPipe.class);

    private static final String BIG_PIPE_FEATURE = "com.atlassian.jira.config.BIG_PIPE";

    private static SiteDarkFeaturesClientExt client;
    private static Boolean oldBigPipeState;

    @Inject
    private PageElementFinder elementFinder;

    @Inject
    private TraceContext traceContext;

    @Inject
    private Timeouts timeouts;

    @BeforeClass
    public static void setUpTest()
            throws Exception {
        client = new SiteDarkFeaturesClientExt(jira.environmentData());
        oldBigPipeState = enableBigPipe(true);
    }

    @AfterClass
    public static void tearDownTest() {
        if (oldBigPipeState != null) {
            enableBigPipe(oldBigPipeState);
        }
    }

    @Test
    public void testPlaceholderIsReplacedWithActivityPane() {
        final String key = backdoor.issues().createIssue("HSP", "xxx").key;
        ViewIssuePage viewIssuePage = jira.goTo(ViewIssuePage.class, key);

        assertTrue("Description should show when empty", viewIssuePage.hasDescription());
        elementFinder.find(By.id("changehistory-tabpanel")).click();

        //If we get here we have verified that the change history tab exists and the placeholder was replaced
    }

    @Test
    public void testPlaceholderIsReplacedWithinReasonableAmountOfTime() {
        final String key = backdoor.issues().createIssue("HSP", "xxx").key;

        WebDriver driver = jira.getTester().getDriver().getDriver();

        Stopwatch timer = Stopwatch.createStarted();

        backdoor.barrier().raiseBarrierAndRun("bigpipe", ()-> {
            log.debug("Going to the view issue page");
            getNoWait(ViewIssuePage.class, key);
            log.debug("I have gone to the view issue page");

            //Wait for placeholder
            WebDriverWait wait = new WebDriverWait(driver, 60, 50); //poll every 50 millis, max 60 seconds for initial page load
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@data-id='activity-panel-pipe-id']")));
            log.info("Placeholder time: " + timer);
            timer.reset();
        });

        timer.start();

        //15 seconds should be enough time after the initial page load to replace bigpipe placeholder with real content
        new FluentWait<>(driver)
                .withTimeout(15, TimeUnit.SECONDS)
                .pollingEvery(100, TimeUnit.MILLISECONDS)
                .until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("changehistory-tabpanel")));

        log.info("Fill in time: " + timer);

        //If we get here we have verified that the change history tab exists and the placeholder was replaced
        //in a reasonable amount of time
    }

    @Test
    public void testRestCallsDoNotHavePlaceholdersWhenBigpipeIsEnabled() {
        backdoor.issues().createIssue("HSP", "xxx");

        BasicSearch searchPage = jira.goTo(BasicSearch.class);
        Poller.waitUntilTrue(traceContext.conditionFromPageLoad("jira.search.finished"));
        final Tracer checkpoint = traceContext.checkpoint();
        searchPage.search();
        traceContext.waitFor(checkpoint, "jira.search.finished");

        //Ensure the BigPipe placeholder is not rendered
        Poller.waitUntilFalse(timedCondition(timeouts.timeoutFor(TimeoutType.PAGE_LOAD),
                () -> elementFinder.find(By.cssSelector("div.loading")).timed().isVisible().now()));
        Poller.waitUntilTrue(elementFinder.find(By.id("changehistory-tabpanel")).timed().isVisible());

        jira.getTester().getDriver().findElement(By.id("changehistory-tabpanel")).click();

        //If we get here, then everything's OK
    }

    /**
     * Enable or disable bigpipe in JIRA on the server.
     *
     * @param enabled true to enable, false to disable.
     * @return the old bigpipe enabled state.
     */
    private static boolean enableBigPipe(boolean enabled) {
        DarkFeature darkFeature = client.get(BIG_PIPE_FEATURE);
        boolean oldState = darkFeature.enabled;

        if (darkFeature.enabled != enabled) {
            client.put(BIG_PIPE_FEATURE, enabled);

            //Verify setting took effect
            darkFeature = client.get(BIG_PIPE_FEATURE);
            assertThat("Failed to set bigpipe enabled state.", darkFeature.enabled, is(enabled));
        }

        return oldState;
    }

    private void getNoWait(String url) {
        WebDriver driver = jira.getTester().getDriver().getDriver();
        JavascriptExecutor exe = (JavascriptExecutor) driver;
        exe.executeScript("window.location.href = '" + url + "';");
    }

    private void getNoWait(Class<? extends Page> page, String... objects) {
        DelayedBinder binder = jira.getPageBinder().delayedBind(page, objects);
        Page p = (Page) binder.get();
        String url = jira.getProductInstance().getBaseUrl() + p.getUrl();
        getNoWait(url);
    }

    private AbstractTimedCondition timedCondition(final long timeout, final Supplier<Boolean> valueSupplier) {
        return new AbstractTimedCondition(timeout, 100) {
            @Override
            protected Boolean currentValue() {
                return valueSupplier.get();
            }
        };
    }
}
