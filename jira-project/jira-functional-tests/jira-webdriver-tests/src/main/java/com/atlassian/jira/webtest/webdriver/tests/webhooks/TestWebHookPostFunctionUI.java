package com.atlassian.jira.webtest.webdriver.tests.webhooks;

import com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookRegistrationClient;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.ResetData;
import com.atlassian.jira.pageobjects.pages.admin.workflow.ViewWorkflowSteps;
import com.atlassian.jira.pageobjects.pages.admin.workflow.ViewWorkflowTransitionPage;
import com.atlassian.jira.pageobjects.pages.admin.workflow.WorkflowHeader;
import com.atlassian.jira.pageobjects.pages.webhooks.AddWebHookPostFunctionPage;
import com.atlassian.jira.pageobjects.pages.webhooks.DisplayedWebHookListener;
import com.atlassian.jira.pageobjects.pages.webhooks.ViewPostFunctionPage;
import com.atlassian.jira.pageobjects.pages.webhooks.ViewWebHookListenerPanel;
import com.atlassian.jira.pageobjects.pages.webhooks.WebHookAdminPage;
import com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks.HttpResponseTester;
import com.atlassian.pageobjects.elements.PageElement;
import com.google.common.collect.Iterables;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.atlassian.jira.webtest.webdriver.tests.webhooks.WebHooksUITestCommon.displaysListener;
import static com.atlassian.jira.webtest.webdriver.tests.webhooks.WebHooksUITestCommon.getAllJiraEvents;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItems;
import static org.junit.Assert.assertThat;

@WebTest({Category.WEBDRIVER_TEST, Category.WEBHOOKS})
@ResetData
public class TestWebHookPostFunctionUI extends BaseJiraWebTest {
    private static final String WORKFLOW_NAME = "classic default workflow";
    protected HttpResponseTester responseTester;

    @Before
    public void setUp() throws Exception {
        responseTester = HttpResponseTester.createTester(jira.environmentData());
    }

    @Test
    public void testWebHookPostFunction() {
        backdoor.flags().clearFlags();

        // add one listener with rest
        final WebHookRegistrationClient.Registration registration = new WebHookRegistrationClient.Registration();
        registration.name = "name";
        registration.excludeBody = false;
        registration.events = new String[]{"project_deleted"};
        responseTester.registerWebhook(registration);

        // add the second with UI
        final DisplayedWebHookListener secondListener = DisplayedWebHookListener.builder()
                .name("new webhook")
                .description("description")
                .events(getAllJiraEvents())
                .excludeBody(false)
                .url("http://localhost:8090/jira/plugins/servlet/abcd")
                .build();
        final ViewWebHookListenerPanel webHook = jira.goTo(WebHookAdminPage.class)
                .createWebHook()
                .setFields(secondListener)
                .submit();
        assertThat(webHook, displaysListener(secondListener));

        final ViewWorkflowSteps steps = jira.goTo(ViewWorkflowSteps.class, WORKFLOW_NAME)
                .createDraftInMode(WorkflowHeader.WorkflowMode.TEXT);

        final ViewWorkflowTransitionPage transitionPage = steps.goToEditTransition(getFirstTransition(steps), "", WORKFLOW_NAME, "", "");
        transitionPage.goToAddPostFunction().selectAndSubmitByName("Trigger a Webhook");
        final AddWebHookPostFunctionPage addWebHookPostFunctionPage = jira.getPageBinder().bind(AddWebHookPostFunctionPage.class);

        assertThat(addWebHookPostFunctionPage.getAllWebHooks(), hasItems(registration.name, secondListener.getName()));
        addWebHookPostFunctionPage.selectWebHookByName(registration.name);

        addWebHookPostFunctionPage.submit();

        final ViewPostFunctionPage viewPostFunctionPage = jira.getPageBinder().bind(ViewPostFunctionPage.class).open();
        final List<String> postFunctions = viewPostFunctionPage.getPostFunctions();

        assertThat(postFunctions, Matchers.hasItem(containsString("Send transition to webhook: " + registration.name)));

        viewPostFunctionPage.publishDraft();

        final Iterable<WebHookAdminPage.WebHookLink> webHooks = jira.goTo(WebHookAdminPage.class)
                .getWebHookLinks();

        final ViewWebHookListenerPanel webHookListenerPanel = Iterables.find(webHooks, webHookLink -> webHookLink.getName().equals(registration.name)).click();

        final String transitions = webHookListenerPanel.getTransitions();

        assertThat(transitions, containsString("Resolve Issue of classic default workflow"));
    }

    private PageElement getFirstTransition(final ViewWorkflowSteps steps) {
        return steps.getWorkflowStepItems().get(0).getTransitions().get(1).getTransition();
    }

}
