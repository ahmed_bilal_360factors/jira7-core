package com.atlassian.jira.webtest.webdriver.tests.permissions;

import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.project.ViewProjectsPage;
import com.atlassian.jira.pageobjects.project.summary.EditProjectPageTab;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

@WebTest(com.atlassian.jira.functest.framework.suite.Category.WEBDRIVER_TEST)
public class TestUserPermissions extends BaseJiraWebTest {
    private static final String TEST_PROJECT = "TSTPR";

    private static Long testProjectId;

    @BeforeClass
    public static void setUp() {
        testProjectId = jira.backdoor().project().addProject(TEST_PROJECT, TEST_PROJECT, jira.getAdminCredentials().getUsername());
    }

    @AfterClass
    public static void tearDown() {
        jira.backdoor().project().deleteProject(TEST_PROJECT);
    }

    @Test
    @LoginAs(admin = true, targetPage = ViewProjectsPage.class)
    public void testAdministratorCollaboratorCanEditProject() throws Exception {
        final ViewProjectsPage viewProjectsPage = pageBinder.bind(ViewProjectsPage.class);

        assertTrue(viewProjectsPage.findProject(TEST_PROJECT).edit(testProjectId).isProjectKeyVisible().byDefaultTimeout());

        jira.logout();
    }
}
