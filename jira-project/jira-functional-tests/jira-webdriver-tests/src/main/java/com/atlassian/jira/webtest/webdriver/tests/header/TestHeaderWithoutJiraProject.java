package com.atlassian.jira.webtest.webdriver.tests.header;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.CommonHeader;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.webtest.webdriver.tests.header.TestHeaderAsSysAdmin.expectedAdminMenuLinks;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@WebTest({Category.WEBDRIVER_TEST})
@RestoreOnce("header-blank-jira.xml")
public class TestHeaderWithoutJiraProject extends BaseJiraWebTest {
    private CommonHeader header;

    @Before
    public void setUp() {
        header = CommonHeader.visit(jira);
    }

    @Test
    @LoginAs(anonymous = true)
    public void testDashboardForAnonymous() {
        jira.visit(DashboardPage.class);
        assertTrue(header.hasMainHeaderLinks());
        assertThat(header.getMainHeaderLinkIds(), contains("home_link"));
    }

    @Test
    @LoginAs(user = "bob", password = "bob")
    public void testDashboardForAuthenticatedUser() {
        assertTrue(header.hasMainHeaderLinks());

        if ("ENABLED".equals(backdoor.plugins().getPluginState("com.atlassian.jira.dev.reference-plugin"))) {
            assertThat(header.getMainHeaderLinkIds(), contains("home_link", "reference-menu-top-level-section"));
        } else {
            assertThat(header.getMainHeaderLinkIds(), contains("home_link"));
        }
    }

    @Test
    @LoginAs(sysadmin = true)
    public void testAdminMenuLinksForSysadmin() {
        assertTrue(header.hasAdminMenu());
        assertThat(header.getAdminMenuLinkIds(), contains(expectedAdminMenuLinks()));
    }

    @Test
    @LoginAs(anonymous = true)
    public void testAnonymous() throws Exception {
        jira.visit(DashboardPage.class);
        assertTrue(header.hasAppSwitcher());
        assertTrue(header.hasMainHeaderLinks());
        assertFalse(header.hasCreateIssueButton());
        assertTrue(header.hasQuickSearch());
        assertTrue(header.hasHelpMenu());
        assertFalse(header.hasAdminMenu());
        assertTrue(header.hasLoginButton());
    }

    @Test
    @LoginAs(sysadmin = true)
    public void testSysadmin() {
        assertTrue(header.hasAppSwitcher());
        assertTrue(header.hasMainHeaderLinks());
        assertFalse(header.hasCreateIssueButton());
        assertTrue(header.hasQuickSearch());
        assertTrue(header.hasHelpMenu());
        assertTrue(header.hasAdminMenu());
        assertFalse(header.hasLoginButton());
        assertTrue(header.hasUserOptionsMenu());
    }

    @Test
    @LoginAs(user = "bob", password = "bob")
    public void testUser() {
        assertTrue(header.hasAppSwitcher());
        assertTrue(header.hasMainHeaderLinks());
        assertFalse(header.hasCreateIssueButton());
        assertTrue(header.hasQuickSearch());
        assertTrue(header.hasHelpMenu());
        assertFalse(header.hasAdminMenu());
        assertFalse(header.hasLoginButton());
        assertTrue(header.hasUserOptionsMenu());
    }
}
