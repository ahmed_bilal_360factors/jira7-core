package com.atlassian.jira.webtest.webdriver.tests.jql;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.model.DefaultIssueActions;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.pageobjects.pages.viewissue.linkissue.LinkIssueDialog;
import com.atlassian.jira.pageobjects.pages.viewissue.linkissue.LinkIssueSearchDialog;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@Restore("xml/TestLinkIssueWithPriorityFieldHiddenEnterprise.xml")
@WebTest({Category.WEBDRIVER_TEST, Category.JQL})
public class TestJQLAutocompletions extends BaseJiraWebTest {
    //Selenium didn't type unmatched parentheses. In this case we need to use this WA
    private static final String SELENIUM_LEFT_PAREN = Keys.chord(Keys.SHIFT, "9");
    private static final String SELENIUM_RIGHT_PAREN = Keys.chord(Keys.SHIFT, "0");

    @Before
    public void setUp() throws Exception {
        backdoor.darkFeatures().enableForSite("jira.jql.autoselectfirst");
    }

    @After
    public void tearDown() throws Exception {
        backdoor.darkFeatures().disableForSite("jira.jql.autoselectfirst");

    }

    //Couple ugly selenium test. Because I don't think that this code is needed in page object.
    @Test
    public void shouldSelectFirstOptionWhileTyping() throws Exception {
        final LinkIssueSearchDialog searchDialog = openJqlForm();

        searchDialog.typeQuery("pro");

        assertEquals("First option should have 'active' class", "project", searchDialog.getJqlSuggestions().find(By.className("active")).getText());
    }

    @Test
    public void shouldNotSelectFirstOptionIfWordEqualsOption() {
        final LinkIssueSearchDialog searchDialog = openJqlForm();

        searchDialog.typeQuery("project");

        assertThat(searchDialog.getJqlSuggestions().find(By.className("active")).isPresent(), is(false));
    }

    @Test
    public void shouldNotSelectFirstOptionIfWordEqualsOptionIgnoringCase() {
        final LinkIssueSearchDialog searchDialog = openJqlForm();

        searchDialog.typeQuery("PROject");

        assertThat(searchDialog.getJqlSuggestions().find(By.className("active")).isPresent(), is(false));
    }

    @Test
    public void shouldOpenSuggestionsAfterCompletion() throws Exception {
        final LinkIssueSearchDialog searchDialog = openJqlForm();

        searchDialog.typeAndAutocomplete("projec");

        assertThat(searchDialog.getJqlSuggestions().isVisible(), is(true));
    }

    @Test
    public void shouldAutoaddSpaces() {
        final LinkIssueSearchDialog searchDialog = openJqlForm();

        searchDialog.typeAndAutocomplete("projec");

        assertEquals("Applying of an autocompletion must also add a space after a word", "project ", searchDialog.getAdvancedJqlQuery());
    }

    @Test
    public void shouldAutoaddSpacesInLogicParens() {
        final LinkIssueSearchDialog searchDialog = openJqlForm();

        searchDialog.typeAndAutocomplete("project = homosapien AND " + SELENIUM_LEFT_PAREN + "assigne");

        assertEquals("Applying of an autocompletion must also add a space after a word inside logic parens", "project = homosapien AND (assignee ", searchDialog.getAdvancedJqlQuery());
    }

    @Test
    public void shouldAutoaddSpacesInListParens() {
        final LinkIssueSearchDialog searchDialog = openJqlForm();

        searchDialog.typeAndAutocomplete("project in " + SELENIUM_LEFT_PAREN + "HSP");

        assertEquals("Applying of an autocompletion must not add a space after a word inside list parens", "project in (homosapien", searchDialog.getAdvancedJqlQuery());

        searchDialog.typeAndAutocomplete(", MKY");

        assertEquals("Applying of an autocompletion must not add a space after a word inside list parens", "project in (homosapien, monkey", searchDialog.getAdvancedJqlQuery());
    }

    @Test
    public void shouldNotAutoaddSpacesInListParensWhenFirstParenIsNotTyped() {
        final LinkIssueSearchDialog searchDialog = openJqlForm();
        searchDialog.typeAndAutocomplete("project in HSP");

        assertEquals("Applying of an autocompletion must not add a space after a word inside list parens", "project in (homosapien", searchDialog.getAdvancedJqlQuery());
    }

    @Test
    public void shouldNotOpenSuggestionsAfterCompletionInsideListParens() throws Exception {
        final LinkIssueSearchDialog searchDialog = openJqlForm();

        searchDialog.typeAndAutocomplete("project in (HSP");

        assertThat(searchDialog.getJqlSuggestions().isVisible(), is(false));
    }

    @Test
    public void shouldAutoaddSpacesAfterListParens() {
        final LinkIssueSearchDialog searchDialog = openJqlForm();

        searchDialog.typeAndAutocomplete("project in (homosapien, monkey) a");     //Matched parentheses. Works ok

        assertEquals("Applying of an autocompletion must add a space after a word after logic parens", "project in (homosapien, monkey) AND ", searchDialog.getAdvancedJqlQuery());
    }

    @Test
    public void shouldAutoaddSpacesInListParensInsideLogicParens() {
        final LinkIssueSearchDialog searchDialog = openJqlForm();

        searchDialog.typeAndAutocomplete("project = homosapien AND " + SELENIUM_LEFT_PAREN + "assignee in " + SELENIUM_LEFT_PAREN + "adm");

        assertEquals("Applying of an autocompletion must not add a space after a word inside list parens", "project = homosapien AND (assignee in (admin", searchDialog.getAdvancedJqlQuery());

        searchDialog.typeAndAutocomplete(SELENIUM_RIGHT_PAREN + " a");

        assertEquals("Applying of an autocompletion must also add a space after a word inside logic parens", "project = homosapien AND (assignee in (admin) AND ", searchDialog.getAdvancedJqlQuery());
    }

    @Test
    public void shouldReplaceCurrentTokenIfQueryIsIncorrect() throws Exception {
        final LinkIssueSearchDialog searchDialog = openJqlForm();

        searchDialog.typeQuery("project");
        searchDialog.moveCursor(1);
        searchDialog.typeAndAutocomplete("");

        assertThat(searchDialog.getAdvancedJqlQuery().trim(), is("priority"));
    }

    private LinkIssueSearchDialog openJqlForm() {
        final ViewIssuePage viewIssuePage = jira.goToViewIssue("HSP-5");
        viewIssuePage.getIssueMenu().invoke(DefaultIssueActions.LINK_ISSUE);

        return pageBinder.bind(LinkIssueDialog.class, "HSP-5")
                         .gotoJiraLink()
                         .openIssueSearchDialog()
                         .switchToAdvanced();
    }

}
