package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.admin.ConfigureScreen;
import com.atlassian.jira.pageobjects.pages.admin.EditDefaultFieldConfigPage;
import com.atlassian.jira.pageobjects.pages.admin.EditFieldConfigPage;
import com.atlassian.jira.pageobjects.pages.admin.EditIssueSecurityScheme;
import com.atlassian.jira.pageobjects.pages.admin.EditNotificationsPage;
import com.atlassian.jira.pageobjects.pages.admin.EditPermissionScheme;
import com.atlassian.jira.pageobjects.pages.admin.fields.configuration.schemes.configure.ConfigureFieldConfigurationSchemePage;
import com.atlassian.jira.pageobjects.pages.admin.issuetype.EditIssueTypeSchemePage;
import com.atlassian.jira.pageobjects.pages.admin.issuetype.screen.schemes.configure.ConfigureIssueTypeScreenSchemePage;
import com.atlassian.jira.pageobjects.pages.admin.screen.ConfigureScreenScheme;
import com.atlassian.jira.pageobjects.pages.admin.workflow.EditWorkflowScheme;
import com.atlassian.jira.pageobjects.pages.admin.workflow.ViewWorkflowSteps;
import com.atlassian.jira.pageobjects.project.ProjectSharedBy;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @since v4.4
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS, Category.IGNITE})
@RestoreOnce("xml/projectconfig/TestProjectSharedBy.xml")
public class TestProjectSharedBy extends BaseJiraWebTest {
    private static final String WF_USED = "Used";
    private static final String WF_NOT_USED = "Not Used";

    private static final List<String> ALL_PROJECTS = asList("<strong>XSS</strong>", "homosapien", "monkey");
    private static final String PROJECT_COUNT = "3 PROJECTS";

    @Test
    public void testWorkflow() {
        //Check an active workflow.
        ViewWorkflowSteps steps = jira.goTo(ViewWorkflowSteps.class, WF_USED);
        ProjectSharedBy sharedBy = steps.sharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals(WF_USED, steps.getWorkflowName());
        assertEquals(PROJECT_COUNT, sharedBy.getTriggerText());
        assertEquals(ALL_PROJECTS, sharedBy.getProjects());

        //Check the draft.
        steps = jira.visit(ViewWorkflowSteps.class, WF_USED, true);
        sharedBy = steps.sharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals(PROJECT_COUNT, sharedBy.getTriggerText());
        assertEquals(ALL_PROJECTS, sharedBy.getProjects());

        //Check a workflow that is not used.
        steps = jira.visit(ViewWorkflowSteps.class, WF_NOT_USED);
        sharedBy = steps.sharedBy();
        assertFalse(sharedBy.isPresent());
        assertEquals(WF_NOT_USED, steps.getWorkflowName());
    }

    @Test
    public void testWorkflowScheme() {
        EditWorkflowScheme editWorkflowScheme = jira.goTo(EditWorkflowScheme.class, 10001L);
        ProjectSharedBy sharedBy = editWorkflowScheme.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals(PROJECT_COUNT, sharedBy.getTriggerText());
        assertEquals(ALL_PROJECTS, sharedBy.getProjects());

        editWorkflowScheme = jira.visit(EditWorkflowScheme.class, 10000L);
        sharedBy = editWorkflowScheme.getSharedBy();
        assertFalse(sharedBy.isPresent());
    }

    @Test
    public void testFieldLayout() {
        //Check an active field layout.
        final EditFieldConfigPage fieldConfigPage = jira.goTo(EditFieldConfigPage.class, 10000L);
        ProjectSharedBy sharedBy = fieldConfigPage.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals("Used", fieldConfigPage.getName());
        assertEquals("2 PROJECTS", sharedBy.getTriggerText());
        assertEquals(Lists.<String>newArrayList("<strong>XSS</strong>", "homosapien"), sharedBy.getProjects());

        //Check an active system default field layout.
        final EditDefaultFieldConfigPage defaultFieldConfigPage = jira.goTo(EditDefaultFieldConfigPage.class);
        sharedBy = defaultFieldConfigPage.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals("Default Field Configuration", fieldConfigPage.getName());
        assertEquals("1 PROJECT", sharedBy.getTriggerText());
        assertEquals(Lists.<String>newArrayList("monkey"), sharedBy.getProjects());

        //Check a field layout not used
        final EditFieldConfigPage notusedFieldConfigPage = jira.goTo(EditFieldConfigPage.class, 10001L);
        assertEquals("Not used", fieldConfigPage.getName());
        sharedBy = notusedFieldConfigPage.getSharedBy();
        assertFalse(sharedBy.isPresent());
    }

    @Test
    public void testFieldLayoutScheme() {
        //Check an active field scheme.
        final ConfigureFieldConfigurationSchemePage configureFieldConfigurationSchemePage =
                jira.goTo(ConfigureFieldConfigurationSchemePage.class, 10000L);
        ProjectSharedBy sharedBy = configureFieldConfigurationSchemePage.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals("Used", configureFieldConfigurationSchemePage.getName());
        assertEquals("1 PROJECT", sharedBy.getTriggerText());
        assertEquals(Lists.<String>newArrayList("homosapien"), sharedBy.getProjects());

        //Check a field scheme not used
        final ConfigureFieldConfigurationSchemePage notUsedFieldConfigurationConfigureFieldConfigurationPage =
                jira.goTo(ConfigureFieldConfigurationSchemePage.class, 10100L);
        assertEquals("Not used", notUsedFieldConfigurationConfigureFieldConfigurationPage.getName());
        sharedBy = notUsedFieldConfigurationConfigureFieldConfigurationPage.getSharedBy();
        assertFalse(sharedBy.isPresent());
    }

    @Test
    public void testScreenScheme() {
        //Check an active field layout.
        ConfigureScreenScheme configPage = jira.goTo(ConfigureScreenScheme.class, 1L);
        ProjectSharedBy sharedBy = configPage.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals("Default Screen Scheme", configPage.getName());
        assertEquals("2 PROJECTS", sharedBy.getTriggerText());
        assertEquals(Lists.<String>newArrayList("homosapien", "monkey"), sharedBy.getProjects());

        //Check an active system default field layout.
        configPage = jira.goTo(ConfigureScreenScheme.class, 10000L);
        sharedBy = configPage.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals("Used", configPage.getName());
        assertEquals("1 PROJECT", sharedBy.getTriggerText());
        assertEquals(Lists.<String>newArrayList("<strong>XSS</strong>"), sharedBy.getProjects());

        //Check a field layout not used
        final ConfigureScreenScheme notUsedScreenScheme = jira.goTo(ConfigureScreenScheme.class, 10001L);
        assertEquals("Not used", notUsedScreenScheme.getName());
        sharedBy = notUsedScreenScheme.getSharedBy();
        assertFalse(sharedBy.isPresent());
    }

    @Test
    public void testIssueTypeScreenScheme() {
        //Check an active field scheme.
        final ConfigureIssueTypeScreenSchemePage editIssueTypeScreenSchemePage = jira.gotoLoginPage()
                .loginAsSysAdmin(ConfigureIssueTypeScreenSchemePage.class, "10000");
        ProjectSharedBy sharedBy = editIssueTypeScreenSchemePage.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals("Used", editIssueTypeScreenSchemePage.getName());
        assertEquals("1 PROJECT", sharedBy.getTriggerText());
        assertEquals(Lists.<String>newArrayList("<strong>XSS</strong>"), sharedBy.getProjects());

        final ConfigureIssueTypeScreenSchemePage editDefaultIssueTypeScreenScheme = jira.gotoLoginPage()
                .loginAsSysAdmin(ConfigureIssueTypeScreenSchemePage.class, "1");
        sharedBy = editDefaultIssueTypeScreenScheme.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals("Default Issue Type Screen Scheme", editDefaultIssueTypeScreenScheme.getName());
        assertEquals("2 PROJECTS", sharedBy.getTriggerText());
        assertEquals(Lists.<String>newArrayList("homosapien", "monkey"), sharedBy.getProjects());

        //Check a field scheme not used
        final ConfigureIssueTypeScreenSchemePage notUsedIssueTypeScreenScheme = jira.gotoLoginPage()
                .loginAsSysAdmin(ConfigureIssueTypeScreenSchemePage.class, "10001");
        assertEquals("Not used", editDefaultIssueTypeScreenScheme.getName());
        sharedBy = notUsedIssueTypeScreenScheme.getSharedBy();
        assertFalse(sharedBy.isPresent());
    }

    @Test
    public void testIssueTypeScheme() {
        EditIssueTypeSchemePage issueTypeSchemePage = jira.goTo(EditIssueTypeSchemePage.class, 10010L);
        ProjectSharedBy sharedBy = issueTypeSchemePage.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals(PROJECT_COUNT, sharedBy.getTriggerText());
        assertEquals(ALL_PROJECTS, sharedBy.getProjects());

        issueTypeSchemePage = jira.visit(EditIssueTypeSchemePage.class, 10011L);
        sharedBy = issueTypeSchemePage.getSharedBy();
        assertFalse(sharedBy.isPresent());
    }

    @Test
    public void testIssueSecurityScheme() {
        EditIssueSecurityScheme issueSecuritySchemePage = jira.goTo(EditIssueSecurityScheme.class, 10000L);
        ProjectSharedBy sharedBy = issueSecuritySchemePage.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals(PROJECT_COUNT, sharedBy.getTriggerText());
        assertEquals(ALL_PROJECTS, sharedBy.getProjects());

        issueSecuritySchemePage = jira.visit(EditIssueSecurityScheme.class, 10001L);
        sharedBy = issueSecuritySchemePage.getSharedBy();
        assertFalse(sharedBy.isPresent());
    }

    @Test
    public void testNotificationScheme() {
        EditNotificationsPage editNotificationsPage = jira.goTo(EditNotificationsPage.class, 10000L);
        ProjectSharedBy sharedBy = editNotificationsPage.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals(PROJECT_COUNT, sharedBy.getTriggerText());
        assertEquals(ALL_PROJECTS, sharedBy.getProjects());

        editNotificationsPage = jira.visit(EditNotificationsPage.class, 10010L);
        sharedBy = editNotificationsPage.getSharedBy();
        assertFalse(sharedBy.isPresent());
    }

    @Test
    public void testPermissionScheme() {
        EditPermissionScheme permissionSchemePage = jira.goTo(EditPermissionScheme.class, 10000L);
        ProjectSharedBy sharedBy = permissionSchemePage.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals(PROJECT_COUNT, sharedBy.getTriggerText());
        assertEquals(ALL_PROJECTS, sharedBy.getProjects());

        permissionSchemePage = jira.visit(EditPermissionScheme.class, 10001L);
        sharedBy = permissionSchemePage.getSharedBy();
        assertFalse(sharedBy.isPresent());
    }

    @Test
    public void testScreensUnused() {
        final ConfigureScreen configureScreen = jira.goTo(ConfigureScreen.class, 3L);
        final ProjectSharedBy sharedBy = configureScreen.getSharedBy();
        assertFalse(sharedBy.isPresent());
    }
}
