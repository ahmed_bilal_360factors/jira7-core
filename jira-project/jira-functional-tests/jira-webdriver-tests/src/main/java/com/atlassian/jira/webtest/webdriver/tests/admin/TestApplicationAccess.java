package com.atlassian.jira.webtest.webdriver.tests.admin;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.matcher.ApplicationRoleBeanMatcher;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.ResetData;
import com.atlassian.jira.pageobjects.pages.admin.application.ApplicationAccessDefaultsDialog;
import com.atlassian.jira.pageobjects.pages.admin.application.ApplicationAccessDefaultsDialog.WarningType;
import com.atlassian.jira.pageobjects.pages.admin.application.ApplicationAccessPage;
import com.atlassian.jira.webtests.LicenseKeys;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.CORE_KEY;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.SOFTWARE_KEY;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.TEST_KEY;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION})
@ResetData
public class TestApplicationAccess extends BaseJiraWebTest {
    private static final String GROUP_USERS = "jira-users";
    private static final String GROUP_DEVELOPERS = "jira-developers";

    private static final int APP_USERS = 3;
    private static final int APP_DEVS = 3;
    private static final String GROUP_APP_USERS = "app-users";
    private static final String GROUP_APP_DEVS = "app-developers";
    private static final String GROUP_ADMINISTRATORS = "jira-administrators";

    @Before
    public void setUp() {
        backdoor.restoreDataFromResource("xml/TestApplicationAccess.xml",
                LicenseKeys.COMMERCIAL.getLicenseString());

        backdoor.usersAndGroups().addGroup(GROUP_APP_USERS);
        backdoor.usersAndGroups().addGroup(GROUP_APP_DEVS);

        // Overriding the default applications that are set by the database patch
        backdoor.applicationRoles().putRoleWithDefaultsSelectedByDefault(SOFTWARE_KEY, false, groups(GROUP_ADMINISTRATORS, GROUP_USERS), groups(GROUP_USERS));

        backdoor.usersAndGroups().addUsersWithGroup("app-user-", "User ", APP_USERS, GROUP_APP_USERS);
        backdoor.usersAndGroups().addUsersWithGroup("app-dev-", "Developer ", APP_DEVS, GROUP_APP_DEVS);
    }

    /**
     * Happy integration test. Edge cases are tested in QUnit.
     */
    @Test
    public void sanityTest() {
        //Role needs to be licensed.
        backdoor.license().set(LicenseKeys.CORE_ROLE.getLicenseString());

        final ApplicationAccessPage page = jira.goTo(ApplicationAccessPage.class);

        final ApplicationAccessPage.Role role = page.role(CORE_KEY);
        //After migration, jira-users should be default group for core and jira-administrator should have core role
        assertThat(role, RoleMatcher.forCore(groups(GROUP_USERS, GROUP_ADMINISTRATORS), groups(GROUP_USERS)));
        assertThat(backdoor.applicationRoles().getCore(),
                ApplicationRoleBeanMatcher.forCore().setGroups(GROUP_USERS, GROUP_ADMINISTRATORS)
                        .setDefaultGroups(GROUP_USERS));

        //Add developers group
        assertThat(role.addGroup(GROUP_DEVELOPERS),
                RoleMatcher.forCore(groups(GROUP_USERS, GROUP_ADMINISTRATORS, GROUP_DEVELOPERS), groups(GROUP_USERS)));
        assertThat(backdoor.applicationRoles().getCore(), ApplicationRoleBeanMatcher.forCore()
                .setGroups(GROUP_USERS, GROUP_ADMINISTRATORS, GROUP_DEVELOPERS).setDefaultGroups(GROUP_USERS));

        //Add developer group as default.
        role.getGroup(GROUP_DEVELOPERS).setDefault();
        assertThat(role, RoleMatcher.forCore(groups(GROUP_USERS, GROUP_ADMINISTRATORS, GROUP_DEVELOPERS),
                groups(GROUP_USERS, GROUP_DEVELOPERS)));
        assertThat(backdoor.applicationRoles().getCore(), ApplicationRoleBeanMatcher.forCore()
                .setGroups(GROUP_USERS, GROUP_ADMINISTRATORS, GROUP_DEVELOPERS).setDefaultGroups(GROUP_USERS,
                        GROUP_DEVELOPERS));

        //Remove users from default
        role.getGroup(GROUP_USERS).unsetDefault();
        assertThat(backdoor.applicationRoles().getCore(), ApplicationRoleBeanMatcher.forCore()
                .setGroups(GROUP_USERS, GROUP_ADMINISTRATORS, GROUP_DEVELOPERS).setDefaultGroups(GROUP_DEVELOPERS));

        //Now remove users.
        role.getGroup(GROUP_USERS).remove();
        assertThat(role, RoleMatcher.forCore(groups(GROUP_ADMINISTRATORS, GROUP_DEVELOPERS), groups(GROUP_DEVELOPERS)));
        assertThat(backdoor.applicationRoles().getCore(), ApplicationRoleBeanMatcher.forCore()
                .setGroups(GROUP_ADMINISTRATORS, GROUP_DEVELOPERS).setDefaultGroups(GROUP_DEVELOPERS));
    }

    /**
     * Happy path for defaults dialog
     */
    @Test
    public void testDefaultsDialog() {
        //Role needs to be licensed.
        backdoor.license().set(LicenseKeys.CORE_ROLE.getLicenseString());
        //remove all groups from Core app role
        backdoor.applicationRoles().putRole(CORE_KEY);

        final ApplicationAccessPage page = jira.goTo(ApplicationAccessPage.class);
        waitUntilFalse(page.role(CORE_KEY).isMarkedAsDefault());
        waitUntilFalse(page.role(SOFTWARE_KEY).isMarkedAsDefault());

        ApplicationAccessDefaultsDialog dialog = page.openDefaultsDialog();

        // enable CORE role
        dialog.toggleRole(CORE_KEY, true);
        dialog.submit();

        assertThat(backdoor.applicationRoles().getCore().isSelectedByDefault(), equalTo(true));
        waitUntilTrue(page.role(CORE_KEY).isMarkedAsDefault());

        dialog = page.openDefaultsDialog();

        //disable CORE role
        assertTrue(String.format("%s is checked", CORE_KEY), dialog.isRoleSelected(CORE_KEY));

        dialog.toggleRole(CORE_KEY, false);
        dialog.submit();
        assertThat(backdoor.applicationRoles().getCore().isSelectedByDefault(), equalTo(false));
        waitUntilFalse(page.role(CORE_KEY).isMarkedAsDefault());

        //enable both roles
        dialog = page.openDefaultsDialog();
        dialog.toggleRole(CORE_KEY, true);
        dialog.toggleRole(SOFTWARE_KEY, true);
        dialog.submit();

        assertThat(backdoor.applicationRoles().getCore().isSelectedByDefault(), equalTo(true));
        assertThat(backdoor.applicationRoles().getSoftware().isSelectedByDefault(), equalTo(true));
        waitUntilTrue(page.role(CORE_KEY).isMarkedAsDefault());
        waitUntilTrue(page.role(SOFTWARE_KEY).isMarkedAsDefault());
    }

    @Test
    public void testGroupNameSpecialCharacters() {
        backdoor.license().set(LicenseKeys.CORE_ROLE.getLicenseString());

        final String SPECIAL_CHARACTER_GROUP = "special &#%^& group";
        final String SPECIAL_CHARACTER_GROUP_SECOND = "<script>alert</script>";
        try {
            backdoor.usersAndGroups().addGroup(SPECIAL_CHARACTER_GROUP);
            backdoor.usersAndGroups().addGroup(SPECIAL_CHARACTER_GROUP_SECOND);

            backdoor.applicationRoles().putRole(CORE_KEY, SPECIAL_CHARACTER_GROUP);

            final ApplicationAccessPage page = jira.goTo(ApplicationAccessPage.class);
            final ApplicationAccessPage.Role role = page.role(CORE_KEY);

            assertThat(role, RoleMatcher.forCore(groups(SPECIAL_CHARACTER_GROUP), groups()));
            assertThat(backdoor.applicationRoles().getCore(),
                    ApplicationRoleBeanMatcher.forCore().setGroups(SPECIAL_CHARACTER_GROUP));

            role.addGroup(SPECIAL_CHARACTER_GROUP_SECOND);

            assertThat(role, RoleMatcher.forCore(groups(SPECIAL_CHARACTER_GROUP, SPECIAL_CHARACTER_GROUP_SECOND), groups()));
            assertThat(backdoor.applicationRoles().getCore(),
                    ApplicationRoleBeanMatcher.forCore().setGroups(SPECIAL_CHARACTER_GROUP, SPECIAL_CHARACTER_GROUP_SECOND));


            role.getGroup(SPECIAL_CHARACTER_GROUP_SECOND).setDefault();

            assertThat(role, RoleMatcher.forCore(groups(SPECIAL_CHARACTER_GROUP, SPECIAL_CHARACTER_GROUP_SECOND), groups(SPECIAL_CHARACTER_GROUP_SECOND)));
            assertThat(backdoor.applicationRoles().getCore(),
                    ApplicationRoleBeanMatcher.forCore().setGroups(SPECIAL_CHARACTER_GROUP, SPECIAL_CHARACTER_GROUP_SECOND).setDefaultGroups(SPECIAL_CHARACTER_GROUP_SECOND));
        } finally {
            backdoor.usersAndGroups().deleteGroup(SPECIAL_CHARACTER_GROUP);
            backdoor.usersAndGroups().deleteGroup(SPECIAL_CHARACTER_GROUP_SECOND);
        }
    }

    @Test
    public void testLicenseCountDisplayed() {
        backdoor.license().set(LicenseKeys.CORE_ROLE.getLicenseString());

        final ApplicationAccessPage page = jira.goTo(ApplicationAccessPage.class);

        final ApplicationAccessPage.Role roleCore = page.role(CORE_KEY);
        final ApplicationAccessPage.Role roleSw = page.role(SOFTWARE_KEY);

        final String[] groups = getGroups(roleCore);
        final String[] defaultGroups = getDefaultGroups(roleCore);

        assertThat(backdoor.applicationRoles().getCore(), ApplicationRoleBeanMatcher.forCore()
                .setDefaultGroups(defaultGroups)
                .setGroups(groups)
                .setNumberOfSeats(roleCore.getNumberOfSeats())
                .setRemainingSeats(roleCore.getRemainingSeats()));

        assertEquals("Software shows actual number of users", roleSw.getUserCount(), backdoor.applicationRoles().getRole(SOFTWARE_KEY).getUserCount().intValue());
    }

    @Test
    public void testLicenseCountUpdatedWhenGroupsChanged() {
        backdoor.license().set(LicenseKeys.CORE_AND_TEST_ROLES.getLicenseString());
        clearRoleConfig(TEST_KEY);

        final ApplicationAccessPage page = jira.goTo(ApplicationAccessPage.class);

        final Supplier<ApplicationAccessPage.Role> coreRole = () -> page.role(CORE_KEY);
        final Supplier<ApplicationAccessPage.Role> testRole = () -> page.role(TEST_KEY);
        final int coreSeats = coreRole.get().getNumberOfSeats();
        final int testRoleSeats = testRole.get().getNumberOfSeats();

        // 0 seats used in all Apps
        assertEquals(coreSeats, coreRole.get().getRemainingSeats());
        assertEquals(testRoleSeats, testRole.get().getRemainingSeats());

        coreRole.get().addGroup(GROUP_APP_USERS);
        // No spots left in CORE, all spots free in test
        assertEquals(0, coreRole.get().getRemainingSeats());
        assertEquals(testRoleSeats, testRole.get().getRemainingSeats());

        testRole.get().addGroup(GROUP_APP_USERS);
        // jira-core seats were released, since all users now belong to other application
        assertEquals(coreSeats, coreRole.get().getRemainingSeats());
        assertEquals(0, testRole.get().getRemainingSeats());
    }

    @Test
    public void testDefaultsDialogWithoutDefaultGroups() {
        backdoor.license().set(LicenseKeys.CORE_AND_TEST_ROLES);
        //remove all groups from Core app role
        backdoor.applicationRoles().putRole(CORE_KEY);

        final ApplicationAccessPage page = jira.goTo(ApplicationAccessPage.class);
        final ApplicationAccessPage.Role roleCore = page.role(CORE_KEY);
        final ApplicationAccessPage.Role roleTest = page.role(TEST_KEY);

        roleCore.addGroup(GROUP_DEVELOPERS);
        roleTest.addGroup(GROUP_USERS).getGroup(GROUP_USERS).setDefault();

        ApplicationAccessDefaultsDialog dialog = page.openDefaultsDialog();

        waitUntilFalse(dialog.isWarningVisible(WarningType.NO_DEFAULT_GROUP));
        // select application without default group
        dialog.toggleRole(CORE_KEY, true);
        waitUntilTrue(dialog.isWarningVisible(WarningType.NO_DEFAULT_GROUP));
        dialog.submit();

        dialog = page.openDefaultsDialog();
        waitUntilTrue(dialog.isWarningVisible(WarningType.NO_DEFAULT_GROUP));

        // unselect application without default group
        dialog.toggleRole(CORE_KEY, false);
        waitUntilFalse(dialog.isWarningVisible(WarningType.NO_DEFAULT_GROUP));

        // select application with default group
        dialog.toggleRole(TEST_KEY, true);
        waitUntilFalse(dialog.isWarningVisible(WarningType.NO_DEFAULT_GROUP));
        dialog.submit();
    }

    @Test
    public void testWarningVisibleWhenOutOfSeats() {
        backdoor.license().set(LicenseKeys.CORE_AND_TEST_ROLES.getLicenseString());
        clearRoleConfig(TEST_KEY);

        final ApplicationAccessPage page = jira.goTo(ApplicationAccessPage.class);

        final Supplier<ApplicationAccessPage.Role> coreRole = () -> page.role(CORE_KEY);
        final Supplier<ApplicationAccessPage.Role> testRole = () -> page.role(TEST_KEY);

        coreRole.get().addGroup(GROUP_APP_USERS);

        waitUntilTrue("Warning on jira core displayed", coreRole.get().hasSeatWarning());
        waitUntilFalse("Warning on jira test not displayed", testRole.get().hasSeatWarning());

        testRole.get().addGroup(GROUP_APP_USERS);

        waitUntilFalse("Warning on jira core is hidden", coreRole.get().hasSeatWarning());
        waitUntilTrue("Warning on jira test was displayed", testRole.get().hasSeatWarning());

        ApplicationAccessDefaultsDialog dialog = page.openDefaultsDialog();
        dialog.toggleRole(CORE_KEY, true);
        dialog.toggleRole(TEST_KEY, true);
        waitUntilTrue(dialog.isWarningVisible(WarningType.NO_SEATS));
    }

    /**
     * JIRA core should be sorted last, the rest in alphabetical order.
     */
    @Test
    public void testApplicationListOrder() {
        backdoor.license().set(LicenseKeys.CORE_AND_TEST_ROLES);

        final ApplicationAccessPage page = jira.goTo(ApplicationAccessPage.class);
        String coreRoleName = page.role(CORE_KEY).getName();
        List<String> roleNames = page.roles().stream().map(ApplicationAccessPage.Role::getName).collect(Collectors.toList());

        assertThat(roleNames, contains("JIRA Software", "Test Product", coreRoleName));
    }

    /**
     * JIRA core should be sorted last, the rest in alphabetical order.
     */
    @Test
    public void testApplicationAccessDefaultsDialogListOrder() {
        backdoor.license().set(LicenseKeys.CORE_AND_TEST_ROLES);

        final ApplicationAccessPage page = jira.goTo(ApplicationAccessPage.class);
        String coreRoleName = page.role(CORE_KEY).getName();
        ApplicationAccessDefaultsDialog dialog = page.openDefaultsDialog();
        List<String> roleNames = dialog.applicationAccess().getApplicationNames();

        assertThat(roleNames, contains("JIRA Software", "Test Product", coreRoleName));
    }

    @Test
    public void groupUserCountsDisplayedOnInitialLoad() {
        backdoor.license().set(LicenseKeys.CORE_ROLE.getLicenseString());

        final ApplicationAccessPage page = jira.goTo(ApplicationAccessPage.class);

        final ApplicationAccessPage.Role roleCore = page.role(CORE_KEY);
        final ApplicationAccessPage.Role roleSw = page.role(SOFTWARE_KEY);

        assertThat(roleCore.getGroup(GROUP_USERS).getUserCount(), equalTo(1));
        assertThat(roleCore.getGroup(GROUP_ADMINISTRATORS).getUserCount(), equalTo(1));

        assertThat(roleSw.getGroup(GROUP_USERS).getUserCount(), equalTo(1));
        assertThat(roleSw.getGroup(GROUP_ADMINISTRATORS).getUserCount(), equalTo(1));
    }

    @Test
    public void groupUserCountsDisplayedForAddedGroup() {
        backdoor.license().set(LicenseKeys.CORE_ROLE.getLicenseString());

        final ApplicationAccessPage page = jira.goTo(ApplicationAccessPage.class);
        ApplicationAccessPage.Role coreRole = page.role(CORE_KEY);
        coreRole = coreRole.addGroup(GROUP_APP_USERS);

        assertThat(coreRole.getGroup(GROUP_APP_USERS).getUserCount(), equalTo(3));
    }

    @Test
    public void testGroupLabelsUpdatedAfterChange() {
        final String LABEL_ADMIN = backdoor.i18n().getText("admin.viewgroup.labels.admin.text", "en_US").toUpperCase();
        final String LABEL_MULTIPLE = backdoor.i18n().getText("admin.viewgroup.labels.application.access.multiple.text", "en_US").toUpperCase();

        backdoor.license().set(LicenseKeys.CORE_AND_TEST_ROLES.getLicenseString());
        clearRoleConfig(TEST_KEY);
        clearRoleConfig(SOFTWARE_KEY);

        final ApplicationAccessPage page = jira.goTo(ApplicationAccessPage.class);

        final Supplier<ApplicationAccessPage.Role> coreRole = () -> page.role(CORE_KEY);

        final Supplier<ApplicationAccessPage.Role> testRole = () -> page.role(TEST_KEY);

        assertThat(coreRole.get().getGroup(GROUP_ADMINISTRATORS).getLabels(), contains(LABEL_ADMIN));
        assertThat(coreRole.get().getGroup(GROUP_USERS).getLabels(), is(empty()));

        testRole.get().addGroup(GROUP_USERS);

        assertThat(coreRole.get().getGroup(GROUP_ADMINISTRATORS).getLabels(), contains(LABEL_ADMIN));
        assertThat(coreRole.get().getGroup(GROUP_USERS).getLabels(), contains(LABEL_MULTIPLE));
        assertThat(testRole.get().getGroup(GROUP_USERS).getLabels(), contains(LABEL_MULTIPLE));

        coreRole.get().getGroup(GROUP_ADMINISTRATORS).setDefault();
        coreRole.get().getGroup(GROUP_USERS).remove();
        testRole.get().addGroup(GROUP_ADMINISTRATORS);

        assertThat(coreRole.get().getGroup(GROUP_ADMINISTRATORS).getLabels(), contains(LABEL_ADMIN, LABEL_MULTIPLE));
        assertThat(testRole.get().getGroup(GROUP_ADMINISTRATORS).getLabels(), contains(LABEL_ADMIN, LABEL_MULTIPLE));
        assertThat(testRole.get().getGroup(GROUP_USERS).getLabels(), is(empty()));

    }

    private void clearRoleConfig(final String key) {
        backdoor.applicationRoles().putRoleWithDefaults(key, Collections.emptyList(), Collections.emptyList());
    }

    private String[] getGroups(final ApplicationAccessPage.Role role) {
        return role.getGroups().stream()
                .map(ApplicationAccessPage.RoleGroup::getName)
                .toArray(String[]::new);
    }

    private String[] getDefaultGroups(final ApplicationAccessPage.Role role) {
        return role.getGroups().stream()
                .filter(ApplicationAccessPage.RoleGroup::isDefault)
                .map(ApplicationAccessPage.RoleGroup::getName)
                .toArray(String[]::new);
    }

    private static Iterable<String> groups(String... groups) {
        return Sets.newHashSet(groups);
    }

    private static class RoleMatcher extends TypeSafeDiagnosingMatcher<ApplicationAccessPage.Role> {
        private final String name;
        private List<Matcher<? super ApplicationAccessPage.RoleGroup>> groups;

        public static RoleMatcher forCore(Iterable<String> others, Iterable<String> defaultGroups) {
            return new RoleMatcher("JIRA Core", others, defaultGroups);
        }

        private RoleMatcher(String name, Iterable<String> groups, Iterable<String> defaultGroups) {
            this.name = name;
            List<Matcher<? super ApplicationAccessPage.RoleGroup>> matchers = Lists.newArrayList();
            matchers.addAll(GroupMatcher.forGroups(groups, defaultGroups));
            this.groups = ImmutableList.copyOf(matchers);
        }

        @Override
        protected boolean matchesSafely(final ApplicationAccessPage.Role item, final Description mismatchDescription) {
            if (!Objects.equal(name, item.getName())) {
                describeTo(item, mismatchDescription);
                return false;
            } else {
                final List<ApplicationAccessPage.RoleGroup> itemGroups = item.getGroups();
                if (groups.isEmpty()) {
                    if (!itemGroups.isEmpty()) {
                        describeTo(item, mismatchDescription);
                        return false;
                    }
                } else {
                    if (!Matchers.containsInAnyOrder(groups).matches(itemGroups)) {
                        describeTo(item, mismatchDescription);
                        return false;
                    }
                }
            }
            return true;
        }

        private static boolean describeTo(final ApplicationAccessPage.Role item, final Description mismatchDescription) {
            mismatchDescription.appendText(String.format("[name: %s, groups: [", item.getName()));
            boolean first = true;
            for (ApplicationAccessPage.RoleGroup roleGroup : item.getGroups()) {
                if (!first) {
                    mismatchDescription.appendValue(", ");
                }

                mismatchDescription.appendText(GroupMatcher.toString(roleGroup));

                first = false;
            }
            mismatchDescription.appendText("]");
            return false;
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText(String.format("[name: %s", name))
                    .appendList(", groups: [", ", ", "]", groups);
        }
    }

    private static class GroupMatcher extends TypeSafeDiagnosingMatcher<ApplicationAccessPage.RoleGroup> {
        private final String group;
        private final boolean isDefault;

        private static List<GroupMatcher> forGroups(Iterable<String> groups, Iterable<String> defaultGroups) {
            final ImmutableList.Builder<GroupMatcher> builder = ImmutableList.builder();
            for (String group : groups) {
                builder.add(new GroupMatcher(group, Iterables.contains(defaultGroups, group)));
            }
            return builder.build();
        }

        private GroupMatcher(String group, boolean isDefault) {
            this.group = group;
            this.isDefault = isDefault;
        }

        @Override
        protected boolean matchesSafely(final ApplicationAccessPage.RoleGroup item, final Description mismatchDescription) {
            if (Objects.equal(group, item.getName()) && isDefault == item.isDefault()) {
                return true;
            } else {
                mismatchDescription.appendValue(toString(item));
                return false;
            }
        }

        private static String toString(final ApplicationAccessPage.RoleGroup item) {
            final boolean isDefault = item.isDefault();
            return String.format("[name: %s, isDefault: %s]", item.getName(), isDefault);
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText(String.format("[name: %s, isDefault: %s]", group, isDefault));
        }
    }
}
