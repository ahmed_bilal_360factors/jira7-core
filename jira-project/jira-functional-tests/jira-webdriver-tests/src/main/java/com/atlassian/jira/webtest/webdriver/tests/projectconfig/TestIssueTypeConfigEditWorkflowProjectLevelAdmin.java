package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.project.issuetypes.DraftIssueTypeWorkflowTab;
import com.atlassian.jira.pageobjects.project.issuetypes.IssueTypeHeader;
import com.atlassian.jira.pageobjects.project.issuetypes.IssueTypeWorkflowTab;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.testkit.beans.ProjectSchemesBean;
import com.atlassian.jira.testkit.beans.WorkflowSchemeData;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @since v7.3
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS, Category.IGNITE})
@RestoreOnce("xml/projectconfig/TestWorkflowTab.xml")
public class TestIssueTypeConfigEditWorkflowProjectLevelAdmin extends BaseJiraWebTest {
    private static final String PROJECT_ADMIN_USER = "fred";
    private static final String PROJECT_KEY = "MKY";
    private static final String PROJECT_WITH_ONLY_BUG_KEY = "PWOB";

    private static final String PROJECT_KEY_WITH_DEFAULT_WORKFLOW_SCHEME = "HSP";

    private static final long DEFAULT_PERMISSION_SCHEME_ID = 0L;

    private static final String EDIT_WORKFLOW_PERMISSION_USER = "editworkflow";

    private static final String SHARED_PROJECT1_NAME = "Shared1";
    private static final String SHARED_PROJECT2_NAME = "Shared2";
    private static final String SHARED_PROJECT1_KEY = "SHA1";
    private static final String SHARED_PROJECT2_KEY = "SHA2";
    private static final String SHARED_WORKFLOW_NAME = "Shared workflow";
    private static final String SHARED_WORKFLOW_SCHEME_NAME = "Shared workflow scheme";
    private static final String LOCALE = "en_US";

    @BeforeClass
    public static void setUp() {
        backdoor.usersAndGroups().addUser(EDIT_WORKFLOW_PERMISSION_USER);
        backdoor.permissionSchemes().addUserPermission(DEFAULT_PERMISSION_SCHEME_ID, ProjectPermissions.ADMINISTER_PROJECTS, EDIT_WORKFLOW_PERMISSION_USER);

        /**
         * sets up workflow shared between 2 projects
         */
        backdoor.project().addProject(SHARED_PROJECT1_NAME, SHARED_PROJECT1_KEY, "admin");
        backdoor.project().addProject(SHARED_PROJECT2_NAME, SHARED_PROJECT2_KEY, "admin");

        //by default uses "Only Monkey" for unassigned, so it makes "Only Monkey" workflow shared with Project with only bug. Unshare this.
        WorkflowSchemeData data = backdoor.workflowSchemes().getWorkflowSchemeByProjectKey(PROJECT_WITH_ONLY_BUG_KEY);
        backdoor.workflowSchemes().updateScheme(data.setDefaultWorkflow("'\"abc</strong>"));

        ProjectSchemesBean schemesProject1 = backdoor.project().getSchemes(SHARED_PROJECT1_KEY);
        ProjectSchemesBean schemesProject2 = backdoor.project().getSchemes(SHARED_PROJECT2_KEY);
        backdoor.permissionSchemes().addUserPermission(schemesProject1.permissionScheme.getId(), ProjectPermissions.ADMINISTER_PROJECTS, EDIT_WORKFLOW_PERMISSION_USER);
        backdoor.permissionSchemes().addUserPermission(schemesProject2.permissionScheme.getId(), ProjectPermissions.ADMINISTER_PROJECTS, EDIT_WORKFLOW_PERMISSION_USER);
        backdoor.workflow().createWorkflow(SHARED_WORKFLOW_NAME);
        WorkflowSchemeData workflowSchemeData = backdoor.workflowSchemes().createScheme(new WorkflowSchemeData().setName(SHARED_WORKFLOW_SCHEME_NAME).setDefaultWorkflow(SHARED_WORKFLOW_NAME));
        backdoor.project().setWorkflowScheme(SHARED_PROJECT1_KEY, workflowSchemeData.getId());
        backdoor.project().setWorkflowScheme(SHARED_PROJECT2_KEY, workflowSchemeData.getId());
    }

    IssueTypeWorkflowTab gotoPageForProject(final String key) {
        return jira.goTo(IssueTypeWorkflowTab.class, key, 1);
    }

    @LoginAs(user = EDIT_WORKFLOW_PERMISSION_USER, password = EDIT_WORKFLOW_PERMISSION_USER)
    @Test
    public void editButtonShouldBeVisibleForProjectLevelAdmin() {
        IssueTypeWorkflowTab workflowTab = gotoPageForProject(PROJECT_KEY);
        assertTrue(workflowTab.canEdit());
    }

    @LoginAs(user = EDIT_WORKFLOW_PERMISSION_USER, password = EDIT_WORKFLOW_PERMISSION_USER)
    @Test
    public void messageWithActionsShouldBeVisibleForProjectLevelAdminWhenEditingWorkflow() {
        IssueTypeWorkflowTab workflowTab = gotoPageForProject(PROJECT_KEY);
        final DraftIssueTypeWorkflowTab draftTab = workflowTab.clickEdit();

        try {
            IssueTypeHeader issueTypeHeader = pageBinder.bind(IssueTypeHeader.class);
            waitUntilTrue(issueTypeHeader.isWorkflowInfoMessageVisible());
            waitUntil(issueTypeHeader.getWorkflowInfoMessage(), startsWith("There are unpublished changes"));
        } finally {
            draftTab.clickDiscard();
        }
    }

    @LoginAs(user = EDIT_WORKFLOW_PERMISSION_USER, password = EDIT_WORKFLOW_PERMISSION_USER)
    @Test
    public void editButtonShouldBeNotPresentForProjectLevelAdminWhenWorkflowIsSharedBetweenProjects() {
        IssueTypeWorkflowTab workflowTab = gotoPageForProject(SHARED_PROJECT1_KEY);
        assertFalse(workflowTab.editReadonly());
        assertFalse(workflowTab.canEdit());

        IssueTypeHeader issueTypeHeader = pageBinder.bind(IssueTypeHeader.class);

        waitUntilTrue(issueTypeHeader.isReadOnlyLozengeVisible());
        waitUntilTrue(issueTypeHeader.isWorkflowInfoMessageVisible());
        waitUntil(issueTypeHeader.getWorkflowInfoMessage(),
                equalTo(getBackdoor().i18n().getText("admin.project.config.workflow.state.READ_ONLY_DELEGATED_SHARED.info.content", "\n", LOCALE)));
    }

    @LoginAs(user = EDIT_WORKFLOW_PERMISSION_USER, password = EDIT_WORKFLOW_PERMISSION_USER)
    @Test
    public void readOnlyLozengeAndMessageShouldNotBePresentOnFieldsForProjectLevelAdminWhenWorkflowIsSharedBetweenProjects() {
        IssueTypeWorkflowTab workflowTab = gotoPageForProject(SHARED_PROJECT1_KEY);
        workflowTab.getTabs().gotoIssueTypeFieldsTab(1);
        IssueTypeHeader issueTypeHeader = pageBinder.bind(IssueTypeHeader.class);

        assertFalse(workflowTab.editReadonly());
        assertFalse(workflowTab.canEdit());

        waitUntilFalse(issueTypeHeader.isReadOnlyLozengeVisible());
        waitUntilFalse(issueTypeHeader.isWorkflowInfoMessageVisible());
    }

    @LoginAs(user = EDIT_WORKFLOW_PERMISSION_USER, password = EDIT_WORKFLOW_PERMISSION_USER)
    @Test
    public void editButtonShouldBeNotPresentForProjectLevelAdminWhenWorkflowIsSystemWorkflow() {
        IssueTypeWorkflowTab workflowTab = gotoPageForProject(PROJECT_KEY_WITH_DEFAULT_WORKFLOW_SCHEME);
        assertFalse(workflowTab.editReadonly());
        assertFalse(workflowTab.canEdit());

        IssueTypeHeader issueTypeHeader = pageBinder.bind(IssueTypeHeader.class);

        waitUntilTrue(issueTypeHeader.isReadOnlyLozengeVisible());
        waitUntilTrue(issueTypeHeader.isWorkflowInfoMessageVisible());
        waitUntil(issueTypeHeader.getWorkflowInfoMessage(),
                equalTo(getBackdoor().i18n().getText("admin.project.config.workflow.state.READ_ONLY_DELEGATED_SYSTEM.info.content", "\n", LOCALE)));
    }
}
