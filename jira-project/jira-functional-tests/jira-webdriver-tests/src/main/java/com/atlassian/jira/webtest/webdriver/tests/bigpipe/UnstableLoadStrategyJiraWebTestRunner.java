package com.atlassian.jira.webtest.webdriver.tests.bigpipe;

import com.atlassian.browsers.BrowserConfig;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.EnvironmentBasedProductInstance;
import com.atlassian.jira.pageobjects.config.junit4.JiraWebTestRunner;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.DefaultAtlassianWebDriver;
import com.atlassian.webdriver.browsers.AutoInstallConfiguration;
import com.atlassian.webdriver.pageobjects.DefaultWebDriverTester;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import org.junit.runners.model.InitializationError;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Uses the unstable webdriver load strategy when testing.  This allows testing partially finished requests with
 * the webdriver client.
 */
public class UnstableLoadStrategyJiraWebTestRunner extends JiraWebTestRunner {

    private static final List<Thread> registeredQuitters = new ArrayList<>();

    /**
     * One instance so all tests use the same driver and Firefox window.
     */
    private static final TestedProductFactory.TesterFactory<WebDriverTester> testerFactory = createTesterFactory();

    public UnstableLoadStrategyJiraWebTestRunner(Class<?> klass) throws InitializationError {
        super(klass, newLocalTestedProduct());
    }

    private static JiraTestedProduct newLocalTestedProduct() {
        return new JiraTestedProduct(testerFactory, new EnvironmentBasedProductInstance());
    }

    private static TestedProductFactory.TesterFactory<WebDriverTester> createTesterFactory() {
        Map<String, String> options = new LinkedHashMap<>();
        options.put("webdriver.load.strategy", "unstable");

        BrowserConfig config = AutoInstallConfiguration.setupBrowser();
        FirefoxDriver driver = FirefoxBrowser.getFirefoxDriver(config, options);

        DefaultAtlassianWebDriver ad = new DefaultAtlassianWebDriver(driver, com.atlassian.pageobjects.browser.Browser.FIREFOX);
        DefaultWebDriverTester tester = new DefaultWebDriverTester(ad);

        //Add shutdown hook for quit the driver at the end of the run - needed to quit the driver
        Thread quitter = new Thread(driver::quit, "WebDriver quitter");
        Runtime.getRuntime().addShutdownHook(quitter);
        registeredQuitters.add(quitter);

        return new TestedProductFactory.SingletonTesterFactory<>(tester);
    }

    /**
     * Quit any running drivers immediately.  Normally this happens on a shutdown hook anyway but this can be called
     * to do it earlier.
     */
    public static synchronized void quitAll() {
        Runtime runtime = Runtime.getRuntime();
        registeredQuitters.forEach(runtime::removeShutdownHook);
        registeredQuitters.forEach(Thread::run);
    }


}
