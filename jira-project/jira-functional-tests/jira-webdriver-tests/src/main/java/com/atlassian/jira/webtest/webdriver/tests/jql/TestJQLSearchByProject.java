package com.atlassian.jira.webtest.webdriver.tests.jql;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.navigator.AdvancedSearch;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;

/**
 * @since v7.2
 */
@WebTest({Category.WEBDRIVER_TEST, Category.JQL})
public class TestJQLSearchByProject extends BaseJiraWebTest {

    @BeforeClass
    public static void setUp() {
        backdoor.restoreBlankInstance();

        backdoor.issues().createIssue("HSP", "HSP Issue 1");
        backdoor.issues().createIssue("HSP", "HSP Issue 2");

        backdoor.issues().createIssue("MKY", "MKY Issue 1");
        backdoor.issues().createIssue("MKY", "MKY Issue 2");

        backdoor.project().addProject("New Empty Project", "EPR", "admin");
    }

    @AfterClass
    public static void tearDown() {
        backdoor.issues().deleteIssue("HSP-1", true);
        backdoor.issues().deleteIssue("HSP-2", true);

        backdoor.issues().deleteIssue("MKY-1", true);
        backdoor.issues().deleteIssue("MKY-2", true);

        backdoor.project().deleteProject("EPR");
    }

    @Test
    public void testProjectSearch() throws Exception {

        AdvancedSearch advancedSearch = jira.goTo(AdvancedSearch.class);

        advancedSearch.enterQuery("project = HSP");
        advancedSearch.submit();
        assertThat(advancedSearch.getResults().getIssueKeys(), hasItems("HSP-1", "HSP-2"));

        advancedSearch.enterQuery("project = MKY");
        advancedSearch.submit();
        assertThat(advancedSearch.getResults().getIssueKeys(), hasItems("MKY-1", "MKY-2"));

        advancedSearch.enterQuery("project = EPR");
        advancedSearch.submit();
        assertThat(advancedSearch.hasEmptyResults().by(1000), is(true));

    }

}