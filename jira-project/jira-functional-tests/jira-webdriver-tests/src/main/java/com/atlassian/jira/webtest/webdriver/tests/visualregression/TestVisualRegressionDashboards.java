package com.atlassian.jira.webtest.webdriver.tests.visualregression;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import javax.inject.Inject;
import java.util.List;

/**
 * @since v6.1
 */

@WebTest({Category.WEBDRIVER_TEST, Category.VISUAL_REGRESSION})
public class TestVisualRegressionDashboards extends JiraVisualRegressionTest {

    @Inject
    protected PageElementFinder pageElementFinder;

    @BeforeClass
    public static void restoreInstance() {
        //we cannot restore instance via @Restore annotation as it doesn't support restoring license from backup file
        backdoor.dataImport().restoreDataFromResource("xml/TestVisualRegressionCreateIssue.zip", "");
    }

    @Test
    // Covers the Favourite Filter, Assigned to Me, Guide for JIRA Administrators, Introduction, Filter Results, Heat Map and Issue Statistic Gadgets
    public void testGadgetsOne() {
        visualComparer.setWaitforJQueryTimeout(0);
        visualComparer.setRefreshAfterResize(false);
        resizeWindowToDefaultDimentions();
        jira.gotoHomePage().gadgets().switchDashboard("Dashboard-1");
        sleep(3000);
        closeInsiderPopUp();
        addElementsToIgnore(By.className("license-content"));
        addElementsToIgnore(By.className("licenseStatusMessage"));
        assertUIMatches("gadgets-part-one");
    }

    @Test
    // Covers FishEye Charts, Crucible Charts, FishEye Recent Changesets Bugzilla ID Search gadgets in edit mode and the Two Dimensional Filter Statistic Gadget.
    public void testGadgetsTwo() {
        visualComparer.setWaitforJQueryTimeout(0);
        visualComparer.setRefreshAfterResize(false);
        resizeWindowToDefaultDimentions();
        jira.gotoHomePage().gadgets().switchDashboard("Dashboard-2");
        sleep(3000);
        closeInsiderPopUp();
        assertUIMatches("gadgets-part-two");
    }

    @Test
    // Covers Watched Issues, Quick Links, Pie Chart, Issues in Progress, Activity Stream, Projects, Voted Issues and Labels Gadget
    public void testGadgetsThree() {
        visualComparer.setWaitforJQueryTimeout(0);
        visualComparer.setRefreshAfterResize(false);
        resizeWindowToDefaultDimentions();
        jira.gotoHomePage().gadgets().switchDashboard("Dashboard-3");
        WebDriver iframe = jira.getTester().getDriver().switchTo().frame(jira.getTester().getDriver().findElement(By.id("gadget-10226")));
        List<WebElement> dateHeader = iframe.findElements(By.cssSelector("#activity-stream, .date-header"));
        addElementsToIgnore(dateHeader);

        PageElement timestamp = pageElementFinder.find(By.cssSelector(".livestamp, .timestamp"));
        Poller.waitUntilTrue(timestamp.timed().isPresent());
        Poller.waitUntilEquals("08/Jul/10 4:08 PM",timestamp.timed().getText());

        jira.getTester().getDriver().switchTo().defaultContent();

        closeInsiderPopUp();
        assertUIMatches("gadgets-part-three");
    }

    @Test
    public void testGadgetsFour() {
        JavascriptExecutor js = (JavascriptExecutor) jira.getTester().getDriver();
        // removing information about cookies from local storage. It is connected with https://jdog.jira-dev.com/browse/SW-1059.
        js.executeScript(String.format(
                "window.localStorage.removeItem('%s');", "AG.congolmerate.cookie"));
        js.executeScript(String.format(
                "window.localStorage.setItem('%s','%s');", "AG.congolmerate.cookie", "gadget-10312-fh=80|gadget-10311-fh=300|gadget-10313-fh=18"));
        visualComparer.setWaitforJQueryTimeout(0);
        visualComparer.setRefreshAfterResize(false);
        resizeWindowToDefaultDimentions();
        jira.gotoHomePage().gadgets().switchDashboard("Dashboard-4");
        addElementsToIgnore(By.id("gadget-10313-title"));
        sleep(3000);
        closeInsiderPopUp();
        assertUIMatches("gadgets-part-four");
    }


    private void sleep(final int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testMyDashboards() {
        assertPageMatches("dashboards-my", "/secure/ConfigurePortalPages!default.jspa?returnUrl=/secure/Dashboard.jspa&view=my");
    }

    @Test
    public void testSearchDashboards() {
        assertPageMatches("dashboards-search", "/secure/ConfigurePortalPages!default.jspa?returnUrl=/secure/Dashboard.jspa&view=search");
    }

    @Test
    public void testAddDashboard() {
        assertPageMatches("dashboard-add", "/secure/AddPortalPage!default.jspa");
    }


}
