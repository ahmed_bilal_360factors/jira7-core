package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.project.summary.EditProjectPageTab;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @since v7.2
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PROJECTS})
@Restore("xml/projectconfig/TestProjectReindexConfirmationTab.xml")
public class TestProjectReindexConfirmationTab extends BaseJiraWebTest {

    public static final String PROJ_ADMIN_USERNAME = "project_admin";
    public static final String PROJ_ID_BLA = "10010";

    @Test
    public void shouldDisplayForAdmin() {
        EditProjectPageTab projectReindexTab = pageBinder.navigateToAndBind(EditProjectPageTab.class, String.valueOf(PROJ_ID_BLA));

        assertTrue("An admin user can reindex the project", projectReindexTab.isReindexLinkVisible().now());
    }

    @Test
    public void shouldNotDisplayForNonAdminProjectAdmin() {
        jira.logout();
        jira.quickLogin(PROJ_ADMIN_USERNAME, PROJ_ADMIN_USERNAME);
        EditProjectPageTab editProjectPageTab = pageBinder.navigateToAndBind(EditProjectPageTab.class, String.valueOf(PROJ_ID_BLA));

        assertFalse("Project admins cannot action a reindex, must be a JIRA admin", editProjectPageTab.isReindexLinkVisible().now());
    }
}
