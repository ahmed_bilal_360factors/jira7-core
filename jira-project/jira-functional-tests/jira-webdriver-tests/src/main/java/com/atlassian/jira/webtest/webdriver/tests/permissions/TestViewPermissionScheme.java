package com.atlassian.jira.webtest.webdriver.tests.permissions;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.pages.AddPermissionsSchemePage;
import com.atlassian.jira.pageobjects.pages.ViewPermissionSchemesPage;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

@WebTest(Category.WEBDRIVER_TEST)
public class TestViewPermissionScheme extends BaseJiraWebTest {
    @Test
    @LoginAs(admin = true, targetPage = ViewPermissionSchemesPage.class)
    public void addPermissionSchemeButtonExists() {
        ViewPermissionSchemesPage viewPermissionSchemesPage = pageBinder.bind(ViewPermissionSchemesPage.class);
        assertTrue("button to add permission scheme is not present", viewPermissionSchemesPage.hasAddedPermissionSchemeButton());
    }

    @Test
    @LoginAs(admin = true, targetPage = ViewPermissionSchemesPage.class)
    public void addPermissionSchemeButtonClickGoesToCorrectPage() {
        ViewPermissionSchemesPage viewPermissionSchemesPage = pageBinder.bind(ViewPermissionSchemesPage.class);
        AddPermissionsSchemePage addPermissionsSchemePage = viewPermissionSchemesPage.clickAddPermissionSchemeButton();
    }
}
