package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.admin.ConfigureScreen;
import com.atlassian.jira.pageobjects.project.ProjectSharedBy;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @since 5.1
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS, Category.IGNITE})
public class TestProjectSharedByScreens extends BaseJiraWebTest {

    @Test
    @Restore("xml/projectconfig/TestProjectSharedByScreens.xml")
    public void testScreensUsedByBothWorkflowsAndScreenSchemes() {
        // Just by workflows first
        ConfigureScreen configureScreen = jira.goTo(ConfigureScreen.class, 10000L);
        ProjectSharedBy sharedBy = configureScreen.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals("1 PROJECT", sharedBy.getTriggerText());
        assertEquals(Arrays.asList("Project A"), sharedBy.getProjects());

        configureScreen = jira.goTo(ConfigureScreen.class, 10001L);
        sharedBy = configureScreen.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals("1 PROJECT", sharedBy.getTriggerText());
        assertEquals(Arrays.asList("Project D"), sharedBy.getProjects());

        configureScreen = jira.goTo(ConfigureScreen.class, 10002L);
        sharedBy = configureScreen.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals("2 PROJECTS", sharedBy.getTriggerText());
        assertEquals(Arrays.asList("Project B", "Project C"), sharedBy.getProjects());

    }

}
