package com.atlassian.jira.webtest.webdriver.tests.visualregression;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.project.summary.ProjectSummaryPageTab;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;

import javax.inject.Inject;

/**
 * Webdriver test for visual regression for the projects section of JIRA admin.
 *
 * @since v6.4.2
 */
@WebTest({Category.WEBDRIVER_TEST, Category.VISUAL_REGRESSION})
public class TestVisualRegressionAdminProject extends JiraVisualRegressionTest {

    @Inject
    protected PageElementFinder pageElementFinder;

    @BeforeClass
    public static void restoreInstance() {
        //we cannot restore instance via @Restore annotation as it doesn't support restoring license from backup file
        backdoor.dataImport().restoreDataFromResource("xml/TestVisualRegressionSmoke.zip", "");
    }

    @Test
    public void testAdminProjectsList() {
        assertPageMatches("admin-projects-list", "/secure/project/ViewProjects.jspa");
    }

    @Test
    public void testAdminProjectSummary() {
        jira.goTo(ProjectSummaryPageTab.class, "BULK");
        assertUIMatches("project-summary");
    }

    @Test
    public void testAdminListWorkflowsPage() {
        assertPageMatches("admin-list-workflows-page", "/secure/admin/workflows/ListWorkflows.jspa");
    }

    @Test
    @Ignore("Large screen, takes too much time to compare in CI builds")
    public void testAdminPermissionScheme() {
        assertPageMatches("admin-permission-scheme", "/secure/admin/EditPermissions!default.jspa?schemeId=10001", 5000);
    }

    @Test
    public void testAdminViewWorkflowPage() {
        assertPageMatches("admin-view-workflow-page", "/secure/admin/ViewWorkflowSteps!default.jspa?workflowMode=live&workflowName=jira", 5000);
    }

    @Test
    public void testAdminProjectPermissions() {
        assertPageMatches("admin-project-permissions", "/plugins/servlet/project-config/BULK/permissions", 5000);
    }

    @Test
    public void testAdminProjectNotifications() {
        assertPageMatches("admin-project-notifications", "/plugins/servlet/project-config/BULK/notifications", 5000);
    }

    @Test
    public void testAdminProjectWorkflows() {
        assertPageMatches("admin-project-workflows", "/plugins/servlet/project-config/BULK/workflows", 5000);
    }

    @Test
    public void testAdminProjectScreens() {
        assertPageMatches("admin-project-screens", "/plugins/servlet/project-config/QA/screens");
    }

    @Test
    public void testAdminProjectAddIssueCollector() {
        goTo("/secure/AddCollector!default.jspa?projectKey=QA");

        //Fixing Flakyness caused by feedback message not showing up occasionally.
        PageElement feedbackMessageBox = pageElementFinder.find(By.className("custom-msg"));
        Poller.waitUntilTrue(feedbackMessageBox.timed().isPresent());
        PageElement feedbackMessage = feedbackMessageBox.find(By.xpath("//*[contains(text(), 'Please provide')]"));
        Poller.waitUntilTrue(feedbackMessage.timed().isVisible());

        visualComparer.setRefreshAfterResize(false);
        assertUIMatches("admin-roject-add-issue-collector");
    }

}

