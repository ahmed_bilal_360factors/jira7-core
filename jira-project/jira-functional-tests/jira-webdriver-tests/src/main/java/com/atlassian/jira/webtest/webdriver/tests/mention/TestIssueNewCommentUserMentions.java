package com.atlassian.jira.webtest.webdriver.tests.mention;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.userpicker.MentionsUserPicker;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.pages.viewissue.AddCommentSection;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.ResourceRef;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.junit.Test;
import org.openqa.selenium.Keys;

import static com.atlassian.jira.functest.framework.matchers.IterableMatchers.hasItemThat;
import static com.atlassian.jira.pageobjects.components.fields.SuggestionMatchers.hasId;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertFalse;

/**
 * Test for user mentions in JIRA
 *
 * @since v5.2
 */
@RestoreOnce("TestMentions.xml")
@WebTest({Category.WEBDRIVER_TEST, Category.USERS_AND_GROUPS, Category.ISSUES})
public class TestIssueNewCommentUserMentions extends BaseJiraWebTest {

    @Test
    public void testMentionPopupDisplaysWhenTypingAt() {
        final String username = "admin";

        // create issue
        final IssueCreateResponse response = backdoor.issues().createIssue("HSP", "Empty mention issue", username);
        final String issueKey = response.key();

        final TimedCondition isOpen = jira
                .goToViewIssue(issueKey)
                .comment()
                .typeInput("@")
                .getMentions()
                .isOpen();

        waitUntilTrue("Mention dialog should be open", isOpen);
    }

    @Test
    public void testMentionAutoCompleteWorksOnLowerCommentField() {
        final AddCommentSection commentSection = jira.goToViewIssue("HSP-1").comment();
        commentSection.typeInput("This is test comment for @adm");
        waitUntilTrue(commentSection.getMentions().isOpen());
        waitUntil(commentSection.getMentions().suggestions(), hasItemThat(hasId("admin", MentionsUserPicker.UserSuggestion.class)));
        commentSection.selectMention("admin");
        waitUntilEquals("This is test comment for [~admin]", commentSection.getInputTimed());
    }

    @Test
    @LoginAs(user = "bob")
    public void testMentionAutoCompleteDoesNotShowWithUserWithoutBrowseUsers() {
        final AddCommentSection commentSection = jira.goToViewIssue("HSP-1").comment();
        commentSection.typeInput("This is test comment for @adm");
        waitUntilFalse("Expected mentions dropdown to not show", commentSection.getMentions().isOpen());
    }

    @Test
    public void testMentionWorksQueriesContainingSpaceCharacter() {
        final AddCommentSection commentSection = jira.goToViewIssue("HSP-1").comment();
        commentSection.typeInput("This is @Bob B");
        waitUntilTrue("Mentions should be open after query with space", commentSection.getMentions().isOpen());
        commentSection.typeInput(Keys.ARROW_LEFT); // caret back one position to the left
        waitUntilTrue("Mentions should still be open after caret is right after the space", commentSection.getMentions().isOpen());
        waitUntilTrue("Bob should be selected", commentSection.getMentions().hasActiveSuggestion("bob"));
        commentSection.getMentions().selectActiveSuggestion();
        waitUntilEquals("Bob should be selected", "This is [~bob]B", commentSection.getInputTimed());
    }

    @Test
    public void testMentionRemovesSuggestionsAfterEnteringCharacterWhenNoResults() {
        final AddCommentSection commentSection = jira.goToViewIssue("HSP-1").comment();
        commentSection.typeInput("This is @y");
        waitUntilTrue("Mentions should be open after query with space", commentSection.getMentions().isOpen());
        assertFalse("Should have no mentions", commentSection.getMentions().hasSuggestions());

        commentSection.typeInput("x");
        waitUntilFalse("Expected mentions dropdown to not show", commentSection.getMentions().isOpen());
    }

    @Test
    public void testMentionListsAssignees() {
        // create user
        String username = "auser";
        backdoor.usersAndGroups()
                .addUser(username)
                .addUserToGroup(username, "jira-developers");

        // setup issue with assignee
        final IssueCreateResponse response = backdoor.issues().createIssue("HSP", "Assignee issue", username);

        // add a comment
        final AddCommentSection commentSection = jira.goToViewIssue(response.key()).comment();
        commentSection.typeInput("@assignee");
        waitUntilTrue("Assignee should be available for selection", commentSection.getMentions().hasSuggestion(username));

        commentSection.selectMention(username);
        waitUntilEquals(username + " should be selected", "[~" + username + "]", commentSection.getInputTimed());
    }

    @Test
    public void testMentionListsReporter() {
        // create user
        String username = "ruser";
        backdoor.usersAndGroups()
                .addUser(username)
                .addUserToGroup(username, "jira-developers");

        // create issue
        final IssueCreateResponse response = backdoor.issues().createIssue("HSP", "Reporter issue", "admin");

        // add reporter to issue
        IssueFields issueFields = new IssueFields();
        issueFields.reporter(ResourceRef.withName(username));
        backdoor.issues().setIssueFields(response.key(), issueFields);

        // make the comment
        final AddCommentSection commentSection = jira.goToViewIssue(response.key()).comment();
        commentSection.typeInput("@reporter");
        waitUntilTrue("Reporter should be available for selection", commentSection.getMentions().hasSuggestion(username));
        commentSection.selectMention(username);
        waitUntilEquals(username + " should be selected", "[~" + username + "]", commentSection.getInputTimed());
    }

}
