package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.dialogs.admin.AbstractAssignIssueTypesDialog;
import com.atlassian.jira.pageobjects.dialogs.admin.ViewWorkflowTextDialog;
import com.atlassian.jira.pageobjects.pages.admin.workflow.SelectWorkflowScheme;
import com.atlassian.jira.pageobjects.pages.admin.workflow.StartDraftWorkflowSchemeMigrationPage;
import com.atlassian.jira.pageobjects.project.ProjectSharedBy;
import com.atlassian.jira.pageobjects.project.workflow.AddWorkflowDialog;
import com.atlassian.jira.pageobjects.project.workflow.AssignIssueTypesDialog;
import com.atlassian.jira.pageobjects.project.workflow.WorkflowsPageTab;
import com.atlassian.jira.pageobjects.util.UserSessionHelper;
import com.atlassian.jira.pageobjects.websudo.DecoratedJiraWebSudo;
import com.atlassian.jira.pageobjects.websudo.JiraWebSudo;
import com.atlassian.jira.testkit.beans.WorkflowSchemeData;
import com.atlassian.jira.testkit.client.WorkflowSchemesControl;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.webtest.webdriver.util.admin.AbstractAddWorkflowDialogHelper;
import com.atlassian.jira.webtest.webdriver.util.admin.AssignIssueTypeDialogHelper;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.google.common.collect.TreeMultimap;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.annotation.Nullable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.jira.pageobjects.dialogs.admin.ViewWorkflowTextDialog.WorkflowTransition;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Maps.transformValues;
import static com.google.common.collect.Sets.newHashSet;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang.StringUtils.removeStart;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * These tests only seem to pass under Mac on FF36+. It seems there is some kind of bug in Webdriver that stops links
 * from being updated correctly. You can set it to use FF36 using -Dwebdriver.browser=firefox-3.6.
 *
 * @since v4.4
 */

@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS, Category.IGNITE})
@RestoreOnce("xml/projectconfig/TestWorkflowTab.xml")
public class TestTabWorkflow extends BaseJiraWebTest {
    public static final String ISSUE_TYPE_LINK_REGEX = "http[s]?://.+(:[0-9]+)?/plugins/servlet/project-config/[A-Z]+/issuetypes/[0-9]+";
    private List<Project> createdProjects = Lists.newArrayList();
    private List<AssignableWorkflowScheme> createdSchemes = Lists.newArrayList();
    @Rule
    public WebDriverScreenshotRule webDriverScreenshotRule = new WebDriverScreenshotRule(); // The screenshot is then saved in jira-project-config-plugin/target/webdriverTests


    @Before
    public void clear() {
        createdProjects.clear();
        createdSchemes.clear();
    }

    @After
    public void deleteCreated() {
        for (Project createdProject : createdProjects) {
            createdProject.delete();
        }

        for (AssignableWorkflowScheme createdScheme : createdSchemes) {
            createdScheme.delete();
        }

        createdProjects.clear();
        createdSchemes.clear();
    }

    @Test
    public void testSystemDefaultWorkflowScheme() {
        final Project newProject = createProject("TSDWS");

        EditDraftHelper editDraftHelper = new EditDraftHelper();
        editDraftHelper.setProject(newProject).setAllProjects(getAllProjects(newProject));

        editDraftHelper.gotoWorkflowTab().assertWorkflowTab();

        SelectWorkflowScheme selectWorkflowScheme = editDraftHelper.gotoSelectScheme();
        assertEquals(newProject.getId(), selectWorkflowScheme.getProjectId());
        assertEquals("Default", selectWorkflowScheme.getSelectedScheme());

        backdoor.websudo().disable();
        //Add a workflow to the scheme.
        editDraftHelper.gotoWorkflowTab();
        editDraftHelper.addWorkflowAndAssignTypes(Workflow.XSS, IssueType.XSS);

        editDraftHelper.assignIssueTypesToWorkflow(Workflow.XSS, IssueType.BUG, IssueType.IMPROVEMENT);
        editDraftHelper.removeWorkflow(Workflow.DEFAULT);

        //Adding "Only Monkey" to the scheme and assigning all issues (aka remove).
        editDraftHelper.addWorkflowAndAssignAllTypes(Workflow.MONKEY).assertWorkflowTab();

        //Assigning all the issue types to XSS workflow (aka remove all other workflows).
        editDraftHelper.addWorkflowAndAssignTypes(Workflow.XSS, IssueType.BUG);
        editDraftHelper.assignAllIssueTypesToWorkflow(Workflow.XSS).assertWorkflowTab();

        editDraftHelper.viewOriginal().assertWorkflowTab();
        editDraftHelper.viewDraft().assertWorkflowTab();

        //Assign all the workflows to make sure the screen state remains correct.
        editDraftHelper.addWorkflowAndAssignTypes(Workflow.DEFAULT, IssueType.BUG);
        editDraftHelper.addWorkflowAndAssignTypes(Workflow.MONKEY, IssueType.IMPROVEMENT);
        editDraftHelper.addWorkflowAndAssignTypes(Workflow.NOT_DEFAULT, IssueType.TASK);

        editDraftHelper.assertWorkflowTab();

        //Back to the default.
        backdoor.websudo().enable();
        checkWebSudo(editDraftHelper, input -> input.discardDraftWebSudo());
    }

    @Test
    public void testSystemDefaultWorkflowSchemeProjectAdmin() {
        //This project is here with a default scheme to make sure it does not show up in the shared list
        //for project admins (i.e. they can't see this project).
        createProject("TSDWSPA");

        final Project project = projectHsp();
        EditDraftHelper editDraftHelper = new EditDraftHelper().setProject(project)
                .setAllProjects(getAllProjects(project, project));
        editDraftHelper.setProjectAdmin().loginAndGotoWorkflowTab().assertWorkflowTab();
    }

    @Test
    public void testProjectSimpleWorkflowScheme() throws Exception {
        final Project project = projectMky();
        EditDraftHelper editDraftHelper = new EditDraftHelper()
                .setProject(project).setAllProjects(getAllProjects(project));

        editDraftHelper.gotoWorkflowTab().assertWorkflowTab();

        SelectWorkflowScheme selectWorkflowScheme = editDraftHelper.gotoSelectScheme();
        assertEquals(project.getId(), selectWorkflowScheme.getProjectId());
        assertEquals(project.getWorkflowScheme().getName(), selectWorkflowScheme.getSelectedScheme());
    }

    @Test
    public void testProjectSimpleWorkflowSchemeProjectAdmin() {
        final Project project = projectMky();
        EditDraftHelper editDraftHelper = new EditDraftHelper()
                .setProject(project).setAllProjects(getAllProjects(project));

        editDraftHelper.setProjectAdmin().loginAndGotoWorkflowTab().assertWorkflowTab();
    }

    @Test
    public void testProjectWithMultipleWorkflows() {
        backdoor.websudo().enable();

        String projectKey = "TPWMW";
        AssignableWorkflowScheme scheme = new AssignableWorkflowScheme(projectKey);
        scheme.setMapping(IssueType.XSS, Workflow.XSS);
        scheme = scheme.create();

        final Project project = createProject(projectKey);
        project.assignWorkflowScheme(scheme);

        EditDraftHelper editDraftHelper = new EditDraftHelper()
                .setProject(project).setAllProjects(getAllProjects(project));

        editDraftHelper.gotoWorkflowTab().assertWorkflowTab();

        //Make sure that remove creates a draft.
        checkWebSudo(editDraftHelper, new Function<EditDraftHelper, JiraWebSudo>() {
            @Override
            public JiraWebSudo apply(EditDraftHelper input) {
                return input.removeWorkflowWebSudo(Workflow.DEFAULT);
            }
        });

        editDraftHelper.viewOriginal().assertWorkflowTab();

        editDraftHelper.viewDraft().discardDraft().assertWorkflowTab();

        //Make sure that assign creates a draft
        checkWebSudo(editDraftHelper, new Function<EditDraftHelper, JiraWebSudo>() {
            @Override
            public JiraWebSudo apply(EditDraftHelper input) {
                return input.assignIssueTypesToWorkflowWebSudo(Workflow.DEFAULT, IssueType.XSS);
            }
        });

        SelectWorkflowScheme selectWorkflowScheme = editDraftHelper.gotoSelectScheme();
        assertEquals(project.getId(), selectWorkflowScheme.getProjectId());
        assertEquals(scheme.getName(), selectWorkflowScheme.getSelectedScheme());

        //Do we correctly read in the draft?
        editDraftHelper.gotoWorkflowTab().assertWorkflowTab();
    }

    @Test
    public void testProjectNotUsingDefaultWorkflow() {
        String projectKey = "TPNUDW";
        AssignableWorkflowScheme scheme = new AssignableWorkflowScheme(projectKey);
        scheme.setDefaultWorkflow(Workflow.MONKEY)
                .setMapping(IssueType.BUG, Workflow.XSS);
        scheme = scheme.create();

        final Project project = createProject(projectKey);
        project.assignWorkflowScheme(scheme);

        EditDraftHelper editDraftHelper = new EditDraftHelper()
                .setProject(project).setAllProjects(getAllProjects(project));
        editDraftHelper.gotoWorkflowTab().assertWorkflowTab();
    }

    @Test
    public void testProjectNotUsingDefaultFromScheme() {
        final Project project = projectBugOnly();

        EditDraftHelper editDraftHelper = new EditDraftHelper()
                .setProject(project).setAllProjects(getAllProjects(project));

        editDraftHelper.gotoWorkflowTab().assertWorkflowTab();
    }

    @Test
    public void testProjectWorkflowSchemeSharedGlobalAdmin() {
        String projectKey = "TPWSS";
        AssignableWorkflowScheme scheme = new AssignableWorkflowScheme(projectKey);
        scheme = scheme.create();

        Project projectMain = createProject(projectKey).assignWorkflowScheme(scheme).allowProjectAdmin();
        Project projectMain1 = createProject(projectKey + "ONE").assignWorkflowScheme(scheme);
        Project projectMain2 = createProject(projectKey + "TWO").assignWorkflowScheme(scheme).allowProjectAdmin();

        EditDraftHelper editDraftHelper = new EditDraftHelper()
                .setProject(projectMain).setAllProjects(getAllProjects(projectMain, projectMain1, projectMain2));
        editDraftHelper.gotoWorkflowTab().assertWorkflowTab();
    }

    @Test
    public void testProjectWorkflowSchemeSharedProjectAdmin() {
        String projectKey = "TPWSS";
        AssignableWorkflowScheme scheme = new AssignableWorkflowScheme(projectKey);
        scheme = scheme.create();

        Project projectMain = createProject(projectKey).assignWorkflowScheme(scheme).allowProjectAdmin();
        Project projectMain1 = createProject(projectKey + "ONE").assignWorkflowScheme(scheme);
        Project projectMain2 = createProject(projectKey + "TWO").assignWorkflowScheme(scheme).allowProjectAdmin();

        EditDraftHelper editDraftHelper = new EditDraftHelper()
                .setProject(projectMain).setAllProjects(getAllProjects(projectMain, projectMain2))
                .setProjectAdmin();
        editDraftHelper.loginAndGotoWorkflowTab().assertWorkflowTab();
    }

    @Test
    public void testProjectWorkflowEditByOtherUser() {
        String projectKey = "TPWEBOU";

        AssignableWorkflowScheme scheme = new AssignableWorkflowScheme(projectKey);
        scheme.setDefaultWorkflow(Workflow.XSS);
        scheme = scheme.create();

        Project projectMain = createProject(projectKey).assignWorkflowScheme(scheme).allowProjectAdmin();

        DraftWorkflowScheme orGetDraft = scheme.getDraftNotNull().create();

        orGetDraft.setDefaultWorkflow(Workflow.DEFAULT);
        orGetDraft.setMapping(IssueType.BUG, Workflow.XSS);
        orGetDraft.setMapping(IssueType.XSS, Workflow.NOT_DEFAULT);
        orGetDraft.setMapping(IssueType.IMPROVEMENT, Workflow.NOT_DEFAULT);
        orGetDraft.update("admin2", "Other Admin");

        EditDraftHelper editDraftHelper = new EditDraftHelper()
                .setProject(projectMain).setAllProjects(getAllProjects(projectMain));
        editDraftHelper.gotoWorkflowTab().assertWorkflowTab();

        //Make sure admin shows up as the last modified user now.
        editDraftHelper.assignIssueTypesToWorkflow(Workflow.DEFAULT, IssueType.BUG).assertWorkflowTab();

        editDraftHelper.viewOriginal().assertWorkflowTab();
        editDraftHelper.viewDraft().assertWorkflowTab();

        //Project admin should not see the draft but see the original.
        editDraftHelper.setProjectAdmin().loginAndGotoWorkflowTab().assertWorkflowTab();
    }

    @Test
    public void testProjectWorkflowSharedDraft() {
        String projectKey = "TPWSD";

        AssignableWorkflowScheme scheme = new AssignableWorkflowScheme(projectKey);
        scheme.setDefaultWorkflow(Workflow.XSS);
        scheme = scheme.create();

        Project projectMain = createProject(projectKey).assignWorkflowScheme(scheme);
        DraftWorkflowScheme draft = scheme.getDraftNotNull().create();
        draft.setDefaultWorkflow(Workflow.NOT_DEFAULT);
        draft.update();

        Project shared = createProject(projectKey + "SHRD").assignWorkflowScheme(scheme).allowProjectAdmin();

        EditDraftHelper editDraftHelper = new EditDraftHelper()
                .setProject(projectMain).setAllProjects(getAllProjects(shared, projectMain));

        editDraftHelper.gotoWorkflowTab().assertWorkflowTab();

        //Lets view the original to make sure we are looking at the draft
        editDraftHelper.viewOriginal().assertWorkflowTab();

        //Lets try a more complex scheme.
        scheme = new AssignableWorkflowScheme(projectKey + "Complex");
        scheme.setDefaultWorkflow(Workflow.XSS);
        scheme.setMapping(IssueType.BUG, Workflow.MONKEY);
        scheme.setMapping(IssueType.TASK, Workflow.DEFAULT);
        scheme = scheme.create();

        projectMain = projectMain.assignWorkflowScheme(scheme);
        draft = scheme.getDraftNotNull().create();
        draft.setDefaultWorkflow(Workflow.NOT_DEFAULT);
        draft.update();

        shared.assignWorkflowScheme(scheme);

        editDraftHelper = new EditDraftHelper()
                .setProject(projectMain).setAllProjects(getAllProjects(shared, projectMain));

        editDraftHelper.gotoWorkflowTab().assertWorkflowTab();

        //Lets view the original to make sure we are looking at the draft
        editDraftHelper.viewOriginal().assertWorkflowTab();
    }

    @Test
    public void testAssignIssueTypesDialog() {
        Project project = projectHsp();

        WorkflowsPageTab tab = jira.goTo(WorkflowsPageTab.class, project.getKey());

        assertAssignIssueTypesDialog(tab, project);

        project = createProject("TAITD");

        AssignableWorkflowScheme workflowScheme = new AssignableWorkflowScheme("TAITD");
        workflowScheme.setDefaultWorkflow(Workflow.DEFAULT)
                .setMapping(IssueType.BUG, Workflow.XSS)
                .setMapping(IssueType.XSS, Workflow.NOT_DEFAULT)
                .setMapping(IssueType.IMPROVEMENT, Workflow.NOT_DEFAULT);

        project.assignWorkflowScheme(workflowScheme.create());

        tab = jira.goTo(WorkflowsPageTab.class, project.getKey());
        assertAssignIssueTypesDialog(tab, project);

        EditDraftHelper helper = new EditDraftHelper(tab)
                .setProject(project).setAllProjects(getAllProjects(project));

        helper.assignIssueTypesToWorkflow(Workflow.XSS, IssueType.IMPROVEMENT);

        assertAssignIssueTypesDialog(tab, project);
    }

    @Test
    public void testAssignIssueTypesDialogFromParameter() throws UnsupportedEncodingException {
        Project project = createProject("TAITD");

        AssignableWorkflowScheme workflowScheme = new AssignableWorkflowScheme("TAITD");
        workflowScheme.setDefaultWorkflow(Workflow.DEFAULT);

        assertAssignIssueTypesDialogFromParameter(project, workflowScheme, Workflow.XSS);

        workflowScheme = new AssignableWorkflowScheme("TAITD");
        workflowScheme.setDefaultWorkflow(Workflow.DEFAULT)
                .setMapping(IssueType.BUG, Workflow.XSS);

        assertAssignIssueTypesDialogFromParameter(project, workflowScheme, Workflow.XSS);
    }

    private void assertAssignIssueTypesDialogFromParameter(Project project, AssignableWorkflowScheme workflowScheme, Workflow workflow)
            throws UnsupportedEncodingException {
        WorkflowAssignIssueTypeDialogHelper helper = createAssignIssueTypeDialogHelper(project, workflowScheme);

        project.assignWorkflowScheme(workflowScheme.create());

        // go to another page first so the browser recognizes the hash change correctly
        jira.goToAdminHomePage();

        WorkflowsPageTab tab = jira.goTo(WorkflowsPageTab.class, project.getKey(), URLEncoder.encode(workflow.getName(), "utf-8"));

        AssignIssueTypesDialog assignIssueTypesDialog = tab.assignIssueTypesDialogOnPageLoad();

        helper.dialog(assignIssueTypesDialog).withoutBack().exclude(workflow).assertDialog();
    }

    @Test
    public void testAddWorkflowDialog() {
        String key = "TAWD";
        AssignableWorkflowScheme scheme = new AssignableWorkflowScheme(key);
        scheme.setDefaultWorkflow(Workflow.MONKEY).setMapping(IssueType.BUG, Workflow.XSS);
        scheme = scheme.create();

        Project project = createProject(key).assignWorkflowScheme(scheme);

        assertAddWorkflowDialog(project, asList(Workflow.DEFAULT, Workflow.NOT_DEFAULT));
        assertAddWorkflowDialog(projectHsp(), asList(Workflow.XSS, Workflow.NOT_DEFAULT, Workflow.MONKEY));
    }

    @Test
    public void testPublishWorkflowDraft() {
        Project project = createProject("TPWD");
        AssignableWorkflowScheme scheme = new AssignableWorkflowScheme("TPWD").create();
        DraftWorkflowScheme draft = scheme.getDraftNotNull().create();

        project.assignWorkflowScheme(scheme);

        EditDraftHelper editDraftHelper = new EditDraftHelper()
                .setProject(project).setAllProjects(getAllProjects(project));

        editDraftHelper.gotoWorkflowTab();

        StartDraftWorkflowSchemeMigrationPage migrationPage = editDraftHelper.publishDraft();
        assertEquals(project.getId(), migrationPage.getProjectId());
        assertTrue(migrationPage.isDraftMigration());

        assertEquals(draft.getId(), migrationPage.getSchemeId());
        assertTrue(migrationPage.isSubmitPresent());
    }

    @Test
    public void importBundleButtonLabelIsChooseFromMpacForJiraAdmin() {
        Project project = projectHsp();

        WorkflowsPageTab tab = jira.quickLogin("jiraadmin", "jiraadmin", WorkflowsPageTab.class, project.getKey());

        assertTrue(tab.isImportBundleButtonPresent());
        assertEquals("Choose From Marketplace", tab.getImportBundleButtonLabel());
        assertTrue(tab.canImportBundle());
    }

    private WorkflowsPageTab assertAssignIssueTypesDialog(WorkflowsPageTab tab, Project project) {
        return assertAssignIssueTypesDialog(tab, project, project.getActiveScheme());
    }

    private static class WorkflowAssignIssueTypeDialogHelper extends AssignIssueTypeDialogHelper<Workflow> {
        private Project project;

        public WorkflowAssignIssueTypeDialogHelper project(Project project) {
            this.project = project;
            return this;
        }

        @Override
        protected String asDisplayed(Workflow data) {
            return data.getDisplayName();
        }

        @Override
        protected List<String> getAllIssueTypes() {
            return project.getIssuesTypeScheme().getOrderedIssueTypes();
        }
    }

    private WorkflowsPageTab assertAssignIssueTypesDialog(WorkflowsPageTab tab, Project project, AbstractWorkflowScheme scheme) {
        final List<WorkflowEntry> expectedEntries = scheme.getWorkflowEntries(project);
        final List<WorkflowsPageTab.Workflow> panels = tab.getWorkflowPanels();

        assertEquals(expectedEntries.size(), panels.size());

        final WorkflowAssignIssueTypeDialogHelper helper = createAssignIssueTypeDialogHelper(project, scheme);

        if (panels.size() > 1) {
            final Iterator<WorkflowEntry> iterator = expectedEntries.iterator();
            //Check the assign dialog for each panel.
            for (WorkflowsPageTab.Workflow actualPanel : panels) {
                final WorkflowEntry expectedEntry = iterator.next();
                final AssignIssueTypesDialog assignIssueTypesDialog = actualPanel.assignIssueTypes();
                helper.dialog(assignIssueTypesDialog).withoutBack().exclude(expectedEntry.workflow).assertDialog();
                assignIssueTypesDialog.close();
            }
        }

        final AssignIssueTypesDialog next = tab.addWorkflow().next();
        helper.exclude(null).withBack().dialog(next).assertDialog();
        next.close();

        return tab;
    }

    private WorkflowAssignIssueTypeDialogHelper createAssignIssueTypeDialogHelper(Project project, AbstractWorkflowScheme workflowScheme) {
        Set<String> types = Sets.newHashSet(project.getIssuesTypeScheme().getIssueTypes());
        WorkflowAssignIssueTypeDialogHelper helper = new WorkflowAssignIssueTypeDialogHelper().project(project);
        for (WorkflowEntry entry : workflowScheme.getWorkflowEntries(project)) {
            for (String issueType : entry.issueTypes) {
                if (types.contains(issueType)) {
                    helper.addMapping(issueType, entry.workflow);
                }
            }
        }
        return helper;
    }

    private static class WorkflowDialogHelper extends AbstractAddWorkflowDialogHelper<AddWorkflowDialog, Workflow> {
        @Override
        protected String lastModifiedUser(Workflow workflow) {
            return workflow.getLastEditedBy();
        }

        @Override
        protected String description(Workflow workflow) {
            return workflow.getDescription();
        }

        @Override
        protected String displayName(Workflow workflow) {
            return workflow.getDisplayName();
        }

        @Override
        protected AddWorkflowDialog nextAndBack(AddWorkflowDialog dialog) {
            return dialog.next().back();
        }
    }

    private void assertAddWorkflowDialog(Project project, Iterable<Workflow> expectedWorkflows) {
        final WorkflowsPageTab tab = jira.goTo(WorkflowsPageTab.class, project.getKey());
        new WorkflowDialogHelper().dialog(tab.addWorkflow()).workflows(expectedWorkflows).assertDialog();
    }

    private static <T> List<T> getFields(Class<?> container, Class<T> type,
                                         Comparator<? super T> comparator) {
        List<T> fields = Lists.newArrayList();

        final Field[] declaredFields = container.getDeclaredFields();
        final int flags = Modifier.STATIC | Modifier.FINAL;
        for (Field declaredField : declaredFields) {
            if ((declaredField.getModifiers() & flags) == flags
                    && type.isAssignableFrom(declaredField.getType())) {
                try {
                    declaredField.setAccessible(true);
                    fields.add(type.cast(declaredField.get(null)));
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        Collections.sort(fields, comparator);
        return Collections.unmodifiableList(fields);
    }

    private static <T extends Comparable<T>> List<T> getFields(Class<?> container, Class<T> type) {
        return getFields(container, type, Ordering.natural());
    }

    private Project projectMky() {
        return new Project(10001L, "MKY", "monkey").setWorkflowScheme(schemeMky());
    }

    private Project projectBugOnly() {
        return new Project(10211L, "PWOB", "Project With Only Bug")
                .setWorkflowScheme(schemeOnlyMonkey())
                .setIssueTypeScheme(IssueTypeScheme.BUG);
    }

    private Project projectHsp() {
        return new Project(10000L, "HSP", "homosapien").setWorkflowScheme(schemeDefault())
                .setProjectAdmin(true);
    }

    private AssignableWorkflowScheme schemeDefault() {
        return new AssignableWorkflowScheme("Default Workflow Scheme").setDefaultScheme(true);
    }

    private Iterable<Project> storedProjects() {
        return Arrays.asList(projectMky(), projectHsp(), projectBugOnly());
    }

    private Iterable<Project> getAllProjects(Project... otherProjects) {
        List<Project> a = Arrays.asList(otherProjects);
        final Set<String> keys = newHashSet(transform(a, new Function<Project, String>() {
            @Override
            public String apply(Project project) {
                return project.getKey();
            }
        }));

        return concat(a, Iterables.filter(storedProjects(), new Predicate<Project>() {
            @Override
            public boolean apply(Project project) {
                return !keys.contains(project.getKey());
            }
        }));
    }

    private AssignableWorkflowScheme schemeMky() {
        final AssignableWorkflowScheme assignableWorkflowScheme = new AssignableWorkflowScheme("Monkey Scheme");
        assignableWorkflowScheme
                .setDefaultWorkflow(Workflow.DEFAULT)
                .setMapping(IssueType.BUG, Workflow.MONKEY)
                .setMapping(IssueType.IMPROVEMENT, Workflow.MONKEY)
                .setMapping(IssueType.NEW_FEATURE, Workflow.MONKEY)
                .setMapping(IssueType.TASK, Workflow.MONKEY)
                .setMapping(IssueType.XSS, Workflow.MONKEY);

        return assignableWorkflowScheme;
    }

    private AssignableWorkflowScheme schemeOnlyMonkey() {
        final AssignableWorkflowScheme assignableWorkflowScheme = new AssignableWorkflowScheme("Only Bug Workflow Scheme");
        assignableWorkflowScheme
                .setDefaultWorkflow(Workflow.MONKEY)
                .setMapping(IssueType.BUG, Workflow.XSS);
        return assignableWorkflowScheme;
    }

    private Project createProject(String projectKey) {
        projectKey = projectKey.toUpperCase(Locale.ENGLISH);
        final String format = String.format("Project for Key '%s'", projectKey);
        long id = backdoor.project().addProject(format, projectKey, "admin");
        final Project project = new Project(id, projectKey, format).setWorkflowScheme(schemeDefault());
        createdProjects.add(project);
        return project;
    }

    private void checkWebSudo(final EditDraftHelper helper, Function<EditDraftHelper, JiraWebSudo> operation) {
        final UserSessionHelper bind = pageBinder.bind(UserSessionHelper.class);
        bind.clearWebSudo();

        JiraWebSudo websudo = operation.apply(helper);
        websudo.cancel();

        websudo = operation.apply(helper);
        websudo.authenticateFail("random").authenticate("admin");
        helper.assertWorkflowTab();
    }

    public class EditDraftHelper {
        private List<Project> allProjects = Lists.newArrayList();
        private Project project;

        private boolean admin = true;
        private String username;
        private WorkflowsPageTab tab;
        private boolean original;

        public EditDraftHelper(WorkflowsPageTab tab) {
            this.tab = tab;
        }

        public EditDraftHelper() {
        }

        public EditDraftHelper setProject(Project project) {
            this.project = project;
            return this;
        }

        public EditDraftHelper setAllProjects(Iterable<Project> projects) {
            this.allProjects = Lists.newArrayList(projects);
            Collections.sort(allProjects);
            return this;
        }

        public EditDraftHelper setProjectAdmin() {
            return this.setUser("fred", false);
        }

        public EditDraftHelper setUser(String username, boolean admin) {
            this.admin = admin;
            this.username = username;
            return this;
        }

        public EditDraftHelper loginAndGotoWorkflowTab() {
            if (admin) {
                tab = jira.quickLoginAsSysadmin(WorkflowsPageTab.class, project.key);
            } else {
                tab = jira.quickLogin(username, username, WorkflowsPageTab.class, project.key);
            }
            return this;
        }

        public EditDraftHelper gotoWorkflowTab() {
            tab = pageBinder.navigateToAndBind(WorkflowsPageTab.class, project.key);
            return this;
        }

        public EditDraftHelper assertWorkflowTab() {
            final boolean shared = assertShares();
            final AssignableWorkflowScheme currentAssignedScheme = project.getWorkflowScheme();
            final DraftWorkflowScheme currentDraftScheme = currentAssignedScheme.getDraft();
            final boolean draft;
            final AbstractWorkflowScheme currentScheme;
            if (currentDraftScheme == null || !admin || original) {
                draft = false;
                currentScheme = currentAssignedScheme;
            } else {
                draft = true;
                currentScheme = currentDraftScheme;
            }
            final List<WorkflowEntry> currentEntries = currentScheme.getWorkflowEntries(project);
            final boolean editable = !original && admin && (!shared || (!draft && currentAssignedScheme.isDefaultScheme()));

            assertEquals(currentAssignedScheme.getName(), tab.getSchemeName());
            assertEquals(admin, tab.isSchemeChangeAvailable());

            //Check the workflows
            final List<WorkflowsPageTab.Workflow> workflowPanels = tab.getWorkflowPanels();
            assertEquals(currentEntries.size(), workflowPanels.size());

            if (admin) {
                assertThat(tab.getHeaderColumnNames(), contains("Workflow", "Issue Types", "Actions"));
            } else {
                assertThat(tab.getHeaderColumnNames(), contains("Workflow", "Issue Types"));
            }

            int pos = 0;
            Iterator<WorkflowsPageTab.Workflow> actualIterator = workflowPanels.iterator();
            for (WorkflowEntry expected : currentEntries) {
                final Workflow expectedWorkflow = expected.getWorkflow();
                final WorkflowsPageTab.Workflow actualWorkflow = actualIterator.next();
                assertEquals(format("workflow[%d].name", pos), expectedWorkflow.getDisplayName(), actualWorkflow.getDisplayName());
                assertEquals(format("workflow[%d].issueTypes", pos), expected.getIssueTypes(), actualWorkflow.getIssueTypes());

                if (admin) {
                    if (currentAssignedScheme.isDefaultScheme() || !expectedWorkflow.isSystem) {
                        assertTrue(format("workflow[%d].canEdit", pos), actualWorkflow.canEdit());
                        assertLinkForWorkflow(format("workflow[%d].editUrl", pos), expectedWorkflow.name, actualWorkflow.getEditLink());
                    } else {
                        assertTrue(format("workflow[%d].readOnly", pos), actualWorkflow.isReadOnly());
                    }

                    assertEquals(3, actualWorkflow.getColumnCount());
                } else {
                    assertFalse(format("workflow[%d].canEdit", pos), actualWorkflow.canEdit());

                    //Project admin will never see the 'Read-Only' losenge because they can never edit a workflow.
                    assertFalse(format("workflow[%d].readOnly", pos), actualWorkflow.isReadOnly());

                    assertEquals(2, actualWorkflow.getColumnCount());
                }

                assertEquals(expected.getIssueTypes().size(), actualWorkflow.getIssueTypeLinks().size());
                actualWorkflow.getIssueTypeLinks().forEach(link -> assertTrue(link.matches(ISSUE_TYPE_LINK_REGEX)));

                final ViewWorkflowTextDialog textDialog = actualWorkflow.openTextModeDialog();

                assertEquals(format("workflow[%d].transitions", pos),
                        admin ? expectedWorkflow.getTransitions() : expectedWorkflow.getProjectAdminTransitions(),
                        Transition.toTransitions(jira.environmentData().getBaseUrl(), textDialog.getTransitions()));

                assertEquals(format("workflow[%d].name", pos),
                        expectedWorkflow.getDisplayName(), textDialog.getWorkflowName());

                textDialog.close();

                //Lets check some editing state. We can only add issue types and remove workflows when:
                // - We are admin (i.e. we have edit permission)
                // - We are not sharing the scheme or using the default scheme.
                // - There is more than one workflow in the scheme.
                if (editable && currentEntries.size() > 1) {
                    assertTrue(actualWorkflow.canRemoveWorkflow());
                    assertTrue(actualWorkflow.canAssignIssueTypes());
                } else {
                    assertFalse(actualWorkflow.canRemoveWorkflow());
                    assertFalse(actualWorkflow.canAssignIssueTypes());
                }

                pos = pos + 1;
            }

            //Check the Add a workflow button.
            if (editable) {
                final Set<Workflow> workflows = currentScheme.getWorkflows();

                assertTrue(tab.isAddWorkflowButtonPresent());
                assertTrue(tab.isImportBundleButtonPresent());
                assertEquals("Import From Bundle", tab.getImportBundleButtonLabel());
                assertTrue(tab.canImportBundle());
                if (collectionEquals(Workflow.ALL, workflows)) {
                    //Add workflow button should be disabled when all workflows are assigned.
                    assertFalse(tab.canAddExistingWorkflow());
                } else {
                    assertTrue(tab.canAddExistingWorkflow());
                }
            } else {
                assertFalse(tab.canAddExistingWorkflow());
                assertFalse(tab.canImportBundle());
            }

            //Check some draft state.
            assertEquals(draft, tab.isDraft());
            assertEquals(draft, tab.isDiscardLinkPresent());
            assertEquals(draft, tab.isPublishButtonPresent());
            assertEquals(draft, tab.isViewOriginalPresent());

            if (draft) {
                assertEquals(!shared, tab.isPublishEnabled());
                if (currentDraftScheme.getLastModifiedUser() == null) {
                    assertTrue(tab.isLastModifiedUserMe());
                } else {
                    assertEquals(currentDraftScheme.getLastModifiedUser(), tab.getLastModifiedUser());
                }
            } else {
                assertEquals(original, tab.isViewDraftPresent());
                assertNull(tab.getLastModifiedUser());
            }

            return this;
        }

        private <T> boolean collectionEquals(Collection<? extends T> o1, Collection<? extends T> o2) {
            return o2.size() == o1.size() && o1.containsAll(o2);
        }

        private AbstractWorkflowScheme getCurrentScheme() {
            final AbstractWorkflowScheme currentScheme;
            final AbstractWorkflowScheme draftWorkflowScheme = project.getDraftWorkflowScheme();
            if (draftWorkflowScheme != null && !original && admin) {
                currentScheme = draftWorkflowScheme;
            } else {
                currentScheme = project.getWorkflowScheme();
            }
            return currentScheme;
        }

        private boolean assertShares() {
            final List<Project> workflowSchemeProjects = allProjects.stream()
                    .filter(allProject -> project.getWorkflowScheme().getName().equals(allProject.getWorkflowScheme().getName()))
                    .collect(toList());

            if (workflowSchemeProjects.size() <= 1) {
                assertFalse(tab.sharedBy().isPresent());
                return false;
            } else {
                List<String> filteredProjects = workflowSchemeProjects.stream()
                        .filter(allProject -> !allProject.getId().equals(project.getId())).map(Project::getName)
                        .sorted(String.CASE_INSENSITIVE_ORDER).collect(toList());

                final ProjectSharedBy sharedBy = tab.sharedBy();
                assertTrue(sharedBy.isPresent());
                assertEquals(filteredProjects, sharedBy.getProjects());

                return true;
            }
        }

        public SelectWorkflowScheme gotoSelectScheme() {
            return tab.gotoSelectScheme();
        }

        StartDraftWorkflowSchemeMigrationPage publishDraft() {
            Long schemeId = backdoor.workflowSchemes().getWorkflowSchemeDraftByProjectKey(project.key).getId();
            return tab.publishDraft(schemeId);
        }

        private void assertLinkForWorkflow(String message, String expectedName, String actualLink) {
            StringBuilder builder = new StringBuilder(Pattern.quote(jira.getProductInstance().getBaseUrl() + "/secure/admin/workflows/EditWorkflowDispatcher.jspa"));
            builder.append("\\?atl_token=.*&wfName=([^&]*).*");

            final Matcher matcher = Pattern.compile(builder.toString()).matcher(actualLink);
            assertTrue(matcher.matches());
            final String givenWfName;
            try {
                givenWfName = URLDecoder.decode(matcher.group(1), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
            assertEquals(message, expectedName, givenWfName);
        }

        public EditDraftHelper discardDraft() {
            tab = tab.discardDraft();
            project.getWorkflowScheme().discardDraft();
            return this;
        }

        public JiraWebSudo discardDraftWebSudo() {
            final JiraWebSudo sudo = tab.discardDraftWebSudo();
            return new DecoratedJiraWebSudo(sudo) {
                @Override
                protected void afterAuthenticate() {
                    project.getWorkflowScheme().discardDraft();
                }
            };
        }

        public EditDraftHelper addWorkflowAndAssignAllTypes(Workflow workflow) {
            return addWorkflowAndAssignTypes(workflow, project.getIssuesTypeScheme().getIssueTypes());
        }

        private EditDraftHelper addWorkflowAndAssignTypes(Workflow workflow, String... issuesTypes) {
            return addWorkflowAndAssignTypes(workflow, asList(issuesTypes));
        }

        private EditDraftHelper addWorkflowAndAssignTypes(Workflow workflow, Iterable<String> issueTypes) {
            final AddWorkflowDialog addWorkflowDialog = tab.addWorkflow();

            final AbstractAssignIssueTypesDialog assign = addWorkflowDialog.selectWorkflow(workflow.getDisplayName()).next();
            assign.setIssueTypes(issueTypes).submit();

            return editWorkflowScheme(workflow, issueTypes);
        }

        private JiraWebSudo addWorkflowAndAssignTypesWebSudo(final Workflow workflow, final String... issueTypes) {
            final AddWorkflowDialog addWorkflowDialog = tab.addWorkflow();

            final AssignIssueTypesDialog assign = addWorkflowDialog.selectWorkflow(workflow.getDisplayName()).next();
            return new DecoratedJiraWebSudo(assign.setIssueTypes(issueTypes).submitWebsudo()) {
                @Override
                protected void afterAuthenticate() {
                    editWorkflowScheme(workflow, asList(issueTypes));
                }
            };
        }

        public EditDraftHelper assignAllIssueTypesToWorkflow(Workflow workflow) {
            final Set<String> issueTypes = newHashSet(project.getIssuesTypeScheme().getIssueTypes());
            final AbstractWorkflowScheme currentScheme = getCurrentScheme();

            for (Iterator<String> iterator = issueTypes.iterator(); iterator.hasNext(); ) {
                String next = iterator.next();
                if (currentScheme.getWorkflow(next).equals(workflow)) {
                    iterator.remove();
                }
            }

            return assignIssueTypesToWorkflow(workflow, issueTypes);
        }

        public EditDraftHelper assignIssueTypesToWorkflow(Workflow workflow, Iterable<String> issueTypes) {
            final WorkflowsPageTab.Workflow panelForWorkflow = getPanelForWorkflow(workflow);
            final AbstractAssignIssueTypesDialog assign = panelForWorkflow.assignIssueTypes();
            assign.setIssueTypes(issueTypes).submit();
            return editWorkflowScheme(workflow, issueTypes);
        }

        public JiraWebSudo assignIssueTypesToWorkflowWebSudo(final Workflow workflow, final Iterable<String> issueTypes) {
            final WorkflowsPageTab.Workflow panelForWorkflow = getPanelForWorkflow(workflow);
            final AssignIssueTypesDialog assign = panelForWorkflow.assignIssueTypes();
            final JiraWebSudo sudo = assign.setIssueTypes(issueTypes).submitWebsudo();
            return new DecoratedJiraWebSudo(sudo) {
                @Override
                protected void afterAuthenticate() {
                    editWorkflowScheme(workflow, issueTypes);
                }
            };
        }

        public JiraWebSudo assignIssueTypesToWorkflowWebSudo(final Workflow workflow, String... issueTypes) {
            return assignIssueTypesToWorkflowWebSudo(workflow, asList(issueTypes));
        }

        public EditDraftHelper assignIssueTypesToWorkflow(Workflow workflow, String... issuesTypes) {
            return assignIssueTypesToWorkflow(workflow, asList(issuesTypes));
        }

        public EditDraftHelper removeWorkflow(Workflow workflow) {
            getPanelForWorkflow(workflow).removeWorkflow();
            removeWorkflowFromScheme(workflow);

            return this;
        }

        public JiraWebSudo removeWorkflowWebSudo(final Workflow workflow) {
            final JiraWebSudo jiraWebSudo = getPanelForWorkflow(workflow).removeWorkflowWebSudo();
            return new DecoratedJiraWebSudo(jiraWebSudo) {
                @Override
                protected void afterAuthenticate() {
                    removeWorkflowFromScheme(workflow);
                }
            };
        }

        private void removeWorkflowFromScheme(Workflow workflow) {
            DraftWorkflowScheme draftBuilder = getDraftScheme();
            draftBuilder.removeWorkflow(workflow);
            draftBuilder.simplify(project);
            draftBuilder.setLastModifiedUser(null);
        }

        private EditDraftHelper editWorkflowScheme(Workflow workflow, Iterable<String> issueTypes) {
            DraftWorkflowScheme draftWorkflowScheme = getDraftScheme();
            for (String type : issueTypes) {
                draftWorkflowScheme.setMapping(type, workflow);
            }

            draftWorkflowScheme.simplify(project);
            draftWorkflowScheme.setLastModifiedUser(null);
            return this;
        }

        private WorkflowsPageTab.Workflow getPanelForWorkflow(Workflow workflow) {
            final List<WorkflowsPageTab.Workflow> workflowPanels = tab.getWorkflowPanels();
            for (WorkflowsPageTab.Workflow panel : workflowPanels) {
                if (panel.getDisplayName().equals(workflow.getDisplayName())) {
                    return panel;
                }
            }

            throw new IllegalArgumentException("Unable to find workflow panel for '" + workflow.getDisplayName());
        }

        private DraftWorkflowScheme getDraftScheme() {
            AssignableWorkflowScheme scheme = project.getWorkflowScheme();
            if (scheme.isDefaultScheme()) {
                String newName = String.format("%s Workflow Scheme", project.getName());
                scheme = scheme.clone(newName);
                project.setWorkflowScheme(scheme);
            }

            return scheme.getDraftNotNull();
        }

        public EditDraftHelper viewOriginal() {
            tab = tab.viewOriginal();
            this.original = true;

            return this;
        }

        public EditDraftHelper viewDraft() {
            tab = tab.viewDraft();
            this.original = false;

            return this;
        }
    }

    private static class WorkflowEntry {
        private final Workflow workflow;
        private final List<String> issueTypes;

        private WorkflowEntry(Workflow name, Iterable<String> issueTypes) {
            this.workflow = name;
            this.issueTypes = Lists.newArrayList(issueTypes);
        }

        public List<String> getIssueTypes() {
            return issueTypes;
        }

        public Workflow getWorkflow() {
            return workflow;
        }
    }

    private static class IssueType {
        private static final String XSS = "\'\"abc</strong>";
        private static final String BUG = "Bug";
        private static final String NEW_FEATURE = "New Feature";
        private static final String TASK = "Task";
        private static final String IMPROVEMENT = "Improvement";

        private static final List<String> ALL_ISSUE_TYPES = getFields(IssueType.class, String.class, String.CASE_INSENSITIVE_ORDER);
    }

    private static class IssueTypeScheme {
        private static final IssueTypeScheme DEFAULT = new IssueTypeScheme("Default Issue Type Scheme", IssueType.BUG,
                IssueType.BUG, IssueType.NEW_FEATURE, IssueType.TASK, IssueType.IMPROVEMENT, IssueType.XSS);
        private static final IssueTypeScheme BUG = new IssueTypeScheme("New issue type scheme for project Project With Only Bug", null,
                IssueType.BUG);

        private final List<String> issueTypes;
        private final String defaultIssueType;
        private final String name;

        private IssueTypeScheme(String name, String defaultIssueType, String... types) {
            List<String> issueTypes = newArrayList(asList(types));
            Collections.sort(issueTypes);

            this.name = name;
            this.defaultIssueType = defaultIssueType;
            this.issueTypes = unmodifiableList(issueTypes);
        }

        public String getName() {
            return name;
        }

        public List<String> getOrderedIssueTypes() {
            List<String> issueTypes = newArrayList(this.issueTypes);
            Collections.sort(issueTypes);

            return issueTypes;
        }

        public List<String> getDefaultOrderedIssueTypes() {
            List<String> issueTypes = newArrayList(this.issueTypes);
            Collections.sort(issueTypes);

            if (defaultIssueType != null) {
                issueTypes.remove(defaultIssueType);
                issueTypes.add(0, defaultIssueType);
            }
            return issueTypes;
        }

        public List<String> getIssueTypes() {
            return issueTypes;
        }

        public Comparator<String> orderForScheme() {
            return new ComparatorWithDefault<String>(defaultIssueType) {
                @Override
                public int nonDefaultCompare(String o1, String o2) {
                    return String.CASE_INSENSITIVE_ORDER.compare(o1, o2);
                }
            };
        }

        public Set<String> filter(Iterable<String> issueTypes) {
            final Set<String> types = newHashSet(issueTypes);
            types.retainAll(this.issueTypes);
            return types;
        }
    }

    private abstract static class ComparatorWithDefault<T> implements Comparator<T> {
        private final T defaultValue;

        private ComparatorWithDefault(T defaultValue) {
            this.defaultValue = defaultValue;
        }

        @Override
        public int compare(T o1, T o2) {
            if (null != defaultValue) {
                if (defaultValue.equals(o1)) {
                    return defaultValue.equals(o2) ? nonDefaultCompare(o1, o2) : -1;
                } else if (defaultValue.equals(o2)) {
                    return 1;
                }
            }

            return nonDefaultCompare(o1, o2);
        }

        abstract public int nonDefaultCompare(T o1, T o2);
    }

    private class Project implements Comparable<Project> {
        private IssueTypeScheme issueTypes;
        private AssignableWorkflowScheme workflowScheme;
        private String key;
        private String name;
        private Long id;
        private boolean projectAdmin;

        public Project(String key, String name) {
            this(null, key, name);
        }

        public Project(Long id, String key, String name) {
            this.id = id;
            this.key = key;
            this.name = name;
            this.issueTypes = IssueTypeScheme.DEFAULT;
        }

        public Long getId() {
            return id;
        }

        public IssueTypeScheme getIssuesTypeScheme() {
            return issueTypes;
        }

        public AssignableWorkflowScheme getWorkflowScheme() {
            return workflowScheme;
        }

        public AbstractWorkflowScheme getDraftWorkflowScheme() {
            return workflowScheme.getDraft();
        }

        public AbstractWorkflowScheme getActiveScheme() {
            return workflowScheme.hasDraft() ? workflowScheme.getDraft() : workflowScheme;
        }

        public String getKey() {
            return key;
        }

        public String getName() {
            return name;
        }

        public boolean canProjectAdminSee() {
            return projectAdmin;
        }

        @Override
        public int compareTo(Project o) {
            return name.compareToIgnoreCase(o.name);
        }

        public Project delete() {
            backdoor.project().deleteProject(key);
            return this;
        }

        public Project create() {
            id = backdoor.project().addProject(name, key, "admin");
            return this;
        }

        public Project setWorkflowScheme(AssignableWorkflowScheme workflowScheme) {
            this.workflowScheme = workflowScheme;
            return this;
        }

        public boolean isProjectAdmin() {
            return projectAdmin;
        }

        public Project setProjectAdmin(boolean projectAdmin) {
            this.projectAdmin = projectAdmin;
            return this;
        }

        public Project assignWorkflowScheme(AssignableWorkflowScheme assignableWorkflowScheme) {
            backdoor.project().setWorkflowScheme(key, assignableWorkflowScheme.getId());
            setWorkflowScheme(assignableWorkflowScheme);
            return this;
        }

        public Project allowProjectAdmin() {
            backdoor.projectRole().addActors(key, "Administrators", null, new String[]{"fred"});
            setProjectAdmin(true);
            return this;
        }

        public Project disallowProjectAdmin() {
            backdoor.projectRole().deleteUser(key, "Administrators", "fred");
            setProjectAdmin(false);
            return this;
        }


        public Project setIssueTypeScheme(IssueTypeScheme scheme) {
            this.issueTypes = scheme;
            return this;
        }
    }

    public class DraftWorkflowScheme extends AbstractWorkflowScheme {
        private AbstractWorkflowScheme parent;
        private String lastModifiedUser;

        public DraftWorkflowScheme(AbstractWorkflowScheme parent) {
            super(parent);
            this.parent = parent;
        }

        public DraftWorkflowScheme setParent(AbstractWorkflowScheme parent) {
            this.parent = parent;
            return this;
        }

        public AbstractWorkflowScheme getParent() {
            return parent;
        }

        public String getLastModifiedUser() {
            return lastModifiedUser;
        }

        public DraftWorkflowScheme setLastModifiedUser(String lastModifiedUser) {
            this.lastModifiedUser = lastModifiedUser;
            return this;
        }

        public DraftWorkflowScheme update(String user, String fullName) {
            WorkflowSchemesControl schemesControl = backdoor.workflowSchemes();
            set(schemesControl.loginAs(user)
                    .updateDraftScheme(getParent().getId(), toWorkflowSchemeData()));
            schemesControl.loginAs("admin");
            this.lastModifiedUser = fullName;
            return this;
        }

        public DraftWorkflowScheme update() {
            set(backdoor.workflowSchemes().updateDraftScheme(getParent().getId(), toWorkflowSchemeData()));
            return this;
        }

        public DraftWorkflowScheme create() {
            WorkflowSchemeData data = backdoor.workflowSchemes().createDraft(parent.toWorkflowSchemeData());
            set(data);
            return this;
        }
    }

    public class AssignableWorkflowScheme extends AbstractWorkflowScheme {
        private DraftWorkflowScheme draft;
        private String name;
        private boolean defaultScheme;

        public AssignableWorkflowScheme(String name) {
            this(name, Collections.<String, Workflow>emptyMap());
        }

        public AssignableWorkflowScheme(String name, Map<String, Workflow> mappings) {
            super(mappings);
            this.name = name;
        }

        public AssignableWorkflowScheme setDraft(DraftWorkflowScheme draft) {
            this.draft = draft;
            return this;
        }

        public DraftWorkflowScheme getDraftNotNull() {
            if (draft == null) {
                draft = new DraftWorkflowScheme(this);
            }
            return draft;
        }

        public DraftWorkflowScheme getDraft() {
            return draft;
        }

        public boolean hasDraft() {
            return draft != null;
        }

        public boolean isDefaultScheme() {
            return defaultScheme;
        }

        public AssignableWorkflowScheme setDefaultScheme(boolean defaultScheme) {
            this.defaultScheme = defaultScheme;
            return this;
        }

        public String getName() {
            return name;
        }

        public AssignableWorkflowScheme setName(String name) {
            this.name = name;
            return this;
        }

        public AssignableWorkflowScheme discardDraft() {
            draft = null;
            return this;
        }

        public AssignableWorkflowScheme clone(String newName) {
            return new AssignableWorkflowScheme(newName, getScheme());
        }

        public AssignableWorkflowScheme create() {
            WorkflowSchemeData scheme = toWorkflowSchemeData();
            scheme = backdoor.workflowSchemes().createScheme(scheme);
            set(scheme);
            createdSchemes.add(this);
            return this;
        }

        @Override
        AbstractWorkflowScheme set(WorkflowSchemeData scheme) {
            super.set(scheme);
            setName(scheme.getName());
            return this;
        }

        WorkflowSchemeData toWorkflowSchemeData() {
            return super.toWorkflowSchemeData().setName(name);
        }

        public AssignableWorkflowScheme delete() {
            backdoor.workflowSchemes().deleteScheme(getId());
            return this;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            if (!super.equals(o)) {
                return false;
            }

            AssignableWorkflowScheme that = (AssignableWorkflowScheme) o;

            if (defaultScheme != that.defaultScheme) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = super.hashCode();
            result = 31 * result + (defaultScheme ? 1 : 0);
            return result;
        }
    }

    abstract class AbstractWorkflowScheme {
        private Long id;
        private Map<String, Workflow> scheme;

        public AbstractWorkflowScheme(AbstractWorkflowScheme scheme) {
            this(scheme.getScheme());
        }

        public AbstractWorkflowScheme() {
            this(Collections.<String, Workflow>emptyMap());
        }

        public AbstractWorkflowScheme(Workflow defaultWorkflow) {
            this(MapBuilder.<String, Workflow>build(null, defaultWorkflow));
        }

        public AbstractWorkflowScheme(Workflow defaultWorkflow, String issueType, Workflow workflow) {
            this(MapBuilder.<String, Workflow>build(null, defaultWorkflow, issueType, workflow));
        }

        public AbstractWorkflowScheme(Map<String, Workflow> map) {
            this.scheme = newHashMap(map);
        }

        public Long getId() {
            return id;
        }

        public AbstractWorkflowScheme setId(Long id) {
            this.id = id;
            return this;
        }

        public Workflow getWorkflow(String issueType) {
            Workflow wf = scheme.get(issueType);
            if (wf == null) {
                wf = getDefaultWorkflow(scheme);
            }
            return wf;
        }

        public Workflow getDefaultWorkflow() {
            Workflow wf = scheme.get(null);
            if (wf == null) {
                wf = Workflow.DEFAULT;
            }
            return wf;
        }

        public Map<String, Workflow> getScheme() {
            return scheme;
        }

        public Workflow getWorkflow(Map<String, Workflow> scheme, String issueType) {
            Workflow wf = scheme.get(issueType);
            if (wf == null) {
                wf = getDefaultWorkflow(scheme);
            }
            return wf;
        }

        public Workflow getDefaultWorkflow(Map<String, Workflow> scheme) {
            Workflow wf = scheme.get(null);
            if (wf == null) {
                wf = Workflow.DEFAULT;
            }
            return wf;
        }

        public AbstractWorkflowScheme clearMappings() {
            this.scheme.clear();
            return this;
        }

        public Comparator<Workflow> orderForScheme() {
            return new ComparatorWithDefault<Workflow>(getDefaultWorkflow()) {
                @Override
                public int nonDefaultCompare(Workflow o1, Workflow o2) {
                    return o1.compareTo(o2);
                }
            };
        }

        public List<WorkflowEntry> getWorkflowEntries(Project project) {
            Multimap<Workflow, String> workflowMap = getInvertedMap(project);

            List<WorkflowEntry> entries = Lists.newArrayList();
            for (Map.Entry<Workflow, Collection<String>> entry : workflowMap.asMap().entrySet()) {
                entries.add(new WorkflowEntry(entry.getKey(), entry.getValue()));
            }

            return entries;
        }

        public Multimap<Workflow, String> getInvertedMap(Project project) {
            Multimap<Workflow, String> workflowMap = TreeMultimap.create(orderForScheme(), project.getIssuesTypeScheme().orderForScheme());
            for (String type : project.getIssuesTypeScheme().getDefaultOrderedIssueTypes()) {
                workflowMap.put(getWorkflow(type), type);
            }
            return workflowMap;
        }

        public Set<Workflow> getWorkflows() {
            Set<Workflow> workflows = newHashSet();
            workflows.addAll(scheme.values());
            workflows.add(getDefaultWorkflow());

            return Collections.unmodifiableSet(workflows);
        }

        public AbstractWorkflowScheme setMappings(Map<String, Workflow> workflowMappings) {
            scheme = Maps.newHashMap(workflowMappings);
            return this;
        }

        public AbstractWorkflowScheme setMapping(String issueType, Workflow workflowName) {
            scheme.put(issueType, workflowName);
            return this;
        }

        public AbstractWorkflowScheme setDefaultWorkflow(Workflow workflowName) {
            scheme.put(null, workflowName);
            return this;
        }

        public AbstractWorkflowScheme removeWorkflow(Workflow workflow) {
            for (Iterator<Map.Entry<String, Workflow>> iterator = scheme.entrySet().iterator(); iterator.hasNext(); ) {
                Map.Entry<String, Workflow> entry = iterator.next();
                if (entry.getValue().equals(workflow)) {
                    iterator.remove();
                }
            }

            if (!scheme.containsKey(null)) {
                Workflow defaultWorkflow = Ordering.natural().min(scheme.values());
                if (defaultWorkflow != null && !defaultWorkflow.isSystem()) {
                    scheme.put(null, defaultWorkflow);
                }
            }
            return this;
        }

        public Workflow getActualWorkflow(String issueType) {
            return getWorkflow(scheme, issueType);
        }

        public AbstractWorkflowScheme simplify(Project project) {
            //workflow -> issueType
            Multimap<Workflow, String> invertedWorkflow = HashMultimap.create();
            for (String issueTypeId : project.getIssuesTypeScheme().getIssueTypes()) {
                Workflow actualWorkflow = getActualWorkflow(issueTypeId);
                invertedWorkflow.put(actualWorkflow, issueTypeId);
            }

            Workflow defaultWorkflow = getDefaultWorkflow();
            if (!invertedWorkflow.containsKey(defaultWorkflow)) {
                defaultWorkflow = Ordering.natural().min(invertedWorkflow.keySet());
            }

            invertedWorkflow.removeAll(defaultWorkflow);
            Map<String, Workflow> workflowMap = Maps.newHashMap();
            workflowMap.put(null, defaultWorkflow);
            for (Map.Entry<Workflow, String> entry : invertedWorkflow.entries()) {
                workflowMap.put(entry.getValue(), entry.getKey());
            }
            this.scheme = workflowMap;
            return this;
        }

        AbstractWorkflowScheme set(WorkflowSchemeData scheme) {
            setId(scheme.getId());
            setMappings(transformValues(scheme.getMappings(), new Function<String, Workflow>() {
                @Override
                public Workflow apply(@Nullable String input) {
                    return Workflow.getWokflowByName(input);
                }
            }));

            if (scheme.getDefaultWorkflow() != null) {
                setDefaultWorkflow(Workflow.getWokflowByName(scheme.getDefaultWorkflow()));
            }
            return this;
        }

        WorkflowSchemeData toWorkflowSchemeData() {
            WorkflowSchemeData scheme = new WorkflowSchemeData();
            scheme.setId(getId());
            scheme.setMappingWithDefault(transformValues(getScheme(), Workflow.NAME_FUNCTION));
            return scheme;
        }
    }

    public static class Workflow implements Iterable<Transition>, Comparable<Workflow> {
        private static final Function<Workflow, String> NAME_FUNCTION = new Function<Workflow, String>() {
            @Override
            public String apply(Workflow input) {
                return input.getName();
            }
        };

        private static final Workflow DEFAULT = new Workflow("jira", "JIRA Workflow (jira)", true, Transition.createDefaultWorkflow(), null, "The default JIRA workflow.");
        private static final Workflow XSS = new Workflow("\'\"abc</strong>", false, Transition.createXSSWorkflow(), "Administrator", null);
        private static final Workflow MONKEY = new Workflow("Only Monkey", false, Transition.createOnlyMonkeyWorkflow(), "Administrator", "Only used in the monkey project");
        private static final Workflow NOT_DEFAULT = new Workflow("not default scheme Workflow", false, Transition.createDefaultWorkflow(), "Administrator", null);

        private static final List<Workflow> ALL = getFields(Workflow.class, Workflow.class);
        private static final Map<String, Workflow> WORKFLOW_MAP = Maps.uniqueIndex(ALL, NAME_FUNCTION);

        private final String name;
        private final String displayName;
        private final boolean isSystem;
        private final List<Transition> transitions;
        private final String lastEditedBy;
        private final String description;

        public Workflow(String name, boolean isSystem, Iterable<? extends Transition> transitions, String lastEditedBy, String description) {
            this(name, name, isSystem, transitions, lastEditedBy, description);
        }

        public Workflow(String name, String displayName, boolean isSystem, Iterable<? extends Transition> transitions, String lastEditedBy, String description) {
            this.name = name;
            this.displayName = displayName;
            this.isSystem = isSystem;
            this.lastEditedBy = lastEditedBy;
            this.description = description;
            this.transitions = copyOf(transitions);
        }

        public boolean isSystem() {
            return isSystem;
        }

        public String getName() {
            return name;
        }

        public String getDisplayName() {
            return displayName;
        }

        public List<Transition> getTransitions() {
            return transitions;
        }

        public List<Transition> getProjectAdminTransitions() {
            List<Transition> projectAdminTransitions = Lists.newArrayList();
            for (Transition transition : transitions) {
                projectAdminTransitions.add(transition.forProjectAdmin());
            }

            return Collections.unmodifiableList(projectAdminTransitions);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            Workflow that = (Workflow) o;

            if (name != null ? !name.equals(that.name) : that.name != null) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            return name != null ? name.hashCode() : 0;
        }

        @Override
        public Iterator<Transition> iterator() {
            return transitions.iterator();
        }

        public String getLastEditedBy() {
            return lastEditedBy;
        }

        @Override
        public int compareTo(Workflow o) {
            return name.compareToIgnoreCase(o.name);
        }

        public String getDescription() {
            return description;
        }

        public static Workflow getWokflowByName(String name) {
            if (name == null) {
                return null;
            } else {
                return WORKFLOW_MAP.get(name);
            }
        }
    }

    private static class Transition {
        private static List<Transition> createOnlyMonkeyWorkflow() {
            String testScreenUrl = createScreenLinkForId(10000);

            List<Transition> expectedRows = new ArrayList<Transition>();
            expectedRows.add(new Transition("OPEN", "Kill", null, null, "CLOSED"));
            expectedRows.add(new Transition("OPEN", "Attach", "Test Screen", testScreenUrl, "OPEN"));
            expectedRows.add(new Transition("OPEN", "Progress", "Test Screen", testScreenUrl, "IN PROGRESS"));
            expectedRows.add(new Transition("CLOSED"));
            expectedRows.add(new Transition("IN PROGRESS", "Ping", null, null, "IN PROGRESS"));
            return expectedRows;
        }

        private static List<Transition> createXSSWorkflow() {
            String xssScreenUrl = createScreenLinkForId(10001);

            List<Transition> expectedRows = new ArrayList<Transition>();
            expectedRows.add(new Transition("OPEN", "<strong>XSS Transition</strong>", "<strong>XSS Screen</strong>", xssScreenUrl, "<STRONG>XSS STATUS</STRONG>"));
            expectedRows.add(new Transition("<STRONG>XSS STATUS</STRONG>"));
            return expectedRows;
        }

        private static List<Transition> createDefaultWorkflow() {
            String resolveUrl = createScreenLinkForId(3);
            String workflowUrl = createScreenLinkForId(2);

            List<Transition> expectedRows = new ArrayList<Transition>();
            expectedRows.add(new Transition("OPEN", "Start Progress", null, null, "IN PROGRESS"));
            expectedRows.add(new Transition("OPEN", "Resolve Issue", "Resolve Issue Screen", resolveUrl, "RESOLVED"));
            expectedRows.add(new Transition("OPEN", "Close Issue", "Resolve Issue Screen", resolveUrl, "CLOSED"));

            expectedRows.add(new Transition("IN PROGRESS", "Stop Progress", null, null, "OPEN"));
            expectedRows.add(new Transition("IN PROGRESS", "Resolve Issue", "Resolve Issue Screen", resolveUrl, "RESOLVED"));
            expectedRows.add(new Transition("IN PROGRESS", "Close Issue", "Resolve Issue Screen", resolveUrl, "CLOSED"));

            expectedRows.add(new Transition("RESOLVED", "Close Issue", "Workflow Screen", workflowUrl, "CLOSED"));
            expectedRows.add(new Transition("RESOLVED", "Reopen Issue", "Workflow Screen", workflowUrl, "REOPENED"));

            expectedRows.add(new Transition("REOPENED", "Resolve Issue", "Resolve Issue Screen", resolveUrl, "RESOLVED"));
            expectedRows.add(new Transition("REOPENED", "Close Issue", "Resolve Issue Screen", resolveUrl, "CLOSED"));
            expectedRows.add(new Transition("REOPENED", "Start Progress", null, null, "IN PROGRESS"));

            expectedRows.add(new Transition("CLOSED", "Reopen Issue", "Workflow Screen", workflowUrl, "REOPENED"));
            return expectedRows;
        }

        private static String createScreenLinkForId(int id) {
            return "${BASE_URL}/secure/admin/ConfigureFieldScreen.jspa?id=" + id;
        }

        private static List<Transition> toTransitions(URL baseUrl, Iterable<WorkflowTransition> transitions) {
            final String prefix = baseUrl.toExternalForm();
            List<Transition> testTransitions = Lists.newArrayList();
            for (WorkflowTransition transition : transitions) {
                String screen = transition.getScreenLink();
                if (screen != null) {
                    screen = removeStart(screen, prefix);

                    if (!screen.startsWith("/")) {
                        screen = "/" + screen;
                    }
                    screen = "${BASE_URL}" + screen;
                }

                testTransitions.add(new Transition(transition, screen));
            }
            return testTransitions;
        }

        private final String sourceStatus;
        private final String targetStatus;
        private final String transitionName;
        private final String screenName;
        private final String screenUrl;

        private Transition(WorkflowTransition transition, String screenUrl) {
            this.sourceStatus = transition.getSourceStatusName();
            this.targetStatus = transition.getTargetStatusName();
            this.transitionName = transition.getTransitionName();
            if (transition.hasScreen()) {
                this.screenName = transition.getScreenName();
                this.screenUrl = screenUrl;
            } else {
                this.screenName = null;
                this.screenUrl = null;
            }
        }

        public Transition(String sourceStatus, String transitionName, String screenName, String screenUrl, String targetStatus) {
            this.sourceStatus = sourceStatus;
            this.targetStatus = targetStatus;
            this.screenName = screenName;
            this.transitionName = transitionName;
            this.screenUrl = screenUrl;
        }

        public Transition(String sourceStatus) {
            this(sourceStatus, null, null, null, null);
        }

        public String getScreenName() {
            return screenName;
        }

        public String getSourceStatus() {
            return sourceStatus;
        }

        public String getTargetStatus() {
            return targetStatus;
        }

        public String getTransitionName() {
            return transitionName;
        }

        public Transition forProjectAdmin() {
            return new Transition(sourceStatus, transitionName, screenName, null, targetStatus);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            Transition that = (Transition) o;

            if (screenName != null ? !screenName.equals(that.screenName) : that.screenName != null) {
                return false;
            }
            if (screenUrl != null ? !screenUrl.equals(that.screenUrl) : that.screenUrl != null) {
                return false;
            }
            if (sourceStatus != null ? !sourceStatus.equals(that.sourceStatus) : that.sourceStatus != null) {
                return false;
            }
            if (targetStatus != null ? !targetStatus.equals(that.targetStatus) : that.targetStatus != null) {
                return false;
            }
            if (transitionName != null ? !transitionName.equals(that.transitionName) : that.transitionName != null) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = sourceStatus != null ? sourceStatus.hashCode() : 0;
            result = 31 * result + (targetStatus != null ? targetStatus.hashCode() : 0);
            result = 31 * result + (transitionName != null ? transitionName.hashCode() : 0);
            result = 31 * result + (screenName != null ? screenName.hashCode() : 0);
            result = 31 * result + (screenUrl != null ? screenUrl.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }
}
