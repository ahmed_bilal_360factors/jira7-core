package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.project.summary.ProjectSummaryPageTab;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS, Category.IGNITE})
@RestoreOnce("xml/projectconfig/TestProjectConfigSummaryComponentPanel.xml")
public class TestSummary extends BaseJiraWebTest {
    private static final String PROJECT_ALL = "HSP";
    public static final String USER_1 = "user1";
    public static final String USER_2 = "user2";
    public static final String JIRA_ADMINISTRATORS = "jira-administrators";

    @BeforeClass
    public static void setup() {
        backdoor.usersAndGroups().addUser(USER_1);
        backdoor.usersAndGroups().addUserToGroup(USER_1, JIRA_ADMINISTRATORS);
        backdoor.usersAndGroups().addUser(USER_2);
        backdoor.usersAndGroups().addUserToGroup(USER_2, JIRA_ADMINISTRATORS);
    }

    @Test
    @LoginAs(user = USER_1)
    public void testWorkflowEditInfoIsPresentUntilClosed() {
        ProjectSummaryPageTab summaryPage = navigateToSummaryPageFor(PROJECT_ALL);
        waitUntilTrue(summaryPage.getEditWorkflowDiscoverInfo().isVisible());

        summaryPage = navigateToSummaryPageFor(PROJECT_ALL);
        waitUntilTrue(summaryPage.getEditWorkflowDiscoverInfo().isVisible());
        summaryPage.getEditWorkflowDiscoverInfo().close();

        summaryPage = navigateToSummaryPageFor(PROJECT_ALL);
        waitUntilFalse(summaryPage.getEditWorkflowDiscoverInfo().isPresent());
    }

    @Test
    @LoginAs(user = USER_2)
    public void testWorkflowEditInfoClose() {
        ProjectSummaryPageTab summaryPage = navigateToSummaryPageFor(PROJECT_ALL);
        waitUntilTrue(summaryPage.getEditWorkflowDiscoverInfo().isVisible());
        waitUntilTrue(summaryPage.getEditWorkflowDiscoverInfo().isCloseVisible());
        summaryPage.getEditWorkflowDiscoverInfo().close();
    }

    private ProjectSummaryPageTab navigateToSummaryPageFor(final String projectKey) {
        return pageBinder.navigateToAndBind(ProjectSummaryPageTab.class, projectKey);
    }
}
