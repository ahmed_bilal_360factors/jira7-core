package com.atlassian.jira.webtest.webdriver.weight;

import com.atlassian.core.util.FileSize;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.components.menu.IssuesMenu;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.project.add.AddProjectWizardProjectDetails;
import com.atlassian.jira.pageobjects.project.add.AddProjectWizardProjectTypeSelection;
import com.atlassian.pageweight.WebDriverResourceAnalyser;
import com.atlassian.pageweight.WebDriverResourceResult;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matchers;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.security.SecureRandom;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import static org.junit.Assert.assertThat;

/**
 * This class uses the existing Confluence WebDriver testing framework in order to download all referenced resources on a page. This allows each
 * page's resource weight to be tracked and (hopefully) reduced.
 */
@WebTest({Category.WEBDRIVER_TEST, Category.PAGE_WEIGHT})
public class PageWeightAnalyserTest extends BaseJiraWebTest {
    private static final String CSS = "css";
    private static final String PROPERTY_PREFIX = "jira.page.weight";
    private static final String JAVA_SCRIPT = "javaScript";
    private static final String RESULTS_FILE_TXT = "results.properties";
    private static final String RESULTS_FILE_JSON = "results.json";
    private static final String RESULTS_PATH = "target/pageweight-results/";
    private static final Map<String, WebDriverResourceResult.Summary> overallResults = Maps.newHashMap();
    private static final String PAGEWEIGHT_PROPERTIES = "pageweight.properties";

    private static final String FAILURE_DESCRIPTION = MessageFormat.format(
            "JIRA resource sizes should not grow because it impacts performance\n"
                    + "You have increased {0} size for page {1} by {2} . \n"
                    + "Please use WRM.require to async download required resources\n"
                    + "You may accept violations by executing.\n"
                    + "cp {0} {1}",
            new File(RESULTS_PATH, RESULTS_FILE_TXT).getAbsolutePath(),
            PageWeightAnalyserTest.class.getResource(PAGEWEIGHT_PROPERTIES).getPath()
                    .replace("target/classes", "src/main/resources"));

    @SuppressWarnings("StaticNonFinalField")
    private static Map<String, Long> referencePageSizes;
    @SuppressWarnings("StaticNonFinalField")
    private static String issueKey;

    @BeforeClass
    public static void setUpTestClass() throws Exception {
        referencePageSizes = loadReferenceSizes();
        issueKey = createProjectAndIssue();
    }

    @AfterClass
    public static void writeResults() throws IOException {
        saveAsJson(overallResults);
        saveAsTxt(overallResults);
    }

    @Test
    public void loginPageSizeShouldNotGrow() throws Exception {
        jira.gotoLoginPage();
        analysePageSize("login");
    }

    @Test
    @LoginAs(admin = true)
    public void viewIssuePageSizeShouldNotGrow() throws Exception {
        jira.goToViewIssue(issueKey);
        analysePageSize("viewIssue");
    }

    @Test
    @LoginAs(admin = true)
    public void dashboardPageSizeShouldNotGrow() throws Exception {
        jira.gotoHomePage();
        analysePageSize("dashboard");
    }

    @Test
    @LoginAs(admin = true)
    public void issueNavigatorPageSizeShouldNotGrow() throws Exception {
        jira.goToIssueNavigator();
        analysePageSize("searchIssue");
    }

    private void analysePageSize(final String pageName) throws IOException {
        final WebDriverResourceAnalyser analyser = new WebDriverResourceAnalyser(jira.getTester().getDriver(), FileSystems.getDefault().getPath(RESULTS_PATH, pageName));
        final WebDriverResourceResult result = analyser.analyseResources();

        final WebDriverResourceResult.Summary summary = result.getSummary();
        try (final PrintWriter writer = new PrintWriter(new File(RESULTS_PATH, pageName + ".json"))) {
            final Gson gson = new Gson();
            final Map<String, Object> resources = ImmutableMap.of(
                    "page", pageName,
                    "summary", summary,
                    JAVA_SCRIPT, result.getJsResources(),
                    CSS, result.getCssResources());
            gson.toJson(resources, writer);
        }

        overallResults.put(pageName, summary);

        assertPageSizeIsNotGrowing(pageName, summary);

    }

    private static String createProjectAndIssue() throws Exception {
        jira.quickLoginAsAdmin();
        jira.gotoHomePage();
        final JiraHeader jiraHeader = pageBinder.bind(JiraHeader.class);
        //if we have issue already just return it
        final String issueKey = getIssueKey(jiraHeader);
        if (StringUtils.isNoneBlank(issueKey)) {
            return issueKey;
        }
        final AddProjectWizardProjectTypeSelection project = jiraHeader.getProjectsMenu().open().createProject();
        final AddProjectWizardProjectDetails addProjectWizardProjectDetails = project.coreProcessManagement();

        final AddProjectWizardProjectDetails projectDetails = addProjectWizardProjectDetails.setName("Demo project").setKey("DEMO" + Math.abs(SecureRandom.getInstanceStrong().nextInt()));
        projectDetails.submitSuccess();
        jiraHeader.createIssue().fill("summary", "Just issue").submit(JiraHeader.class);

        return getIssueKey(jiraHeader);
    }

    private static void saveAsTxt(final Map<String, WebDriverResourceResult.Summary> overallResults) throws IOException {
        try (final PrintWriter txtOutput = new PrintWriter(new File(RESULTS_PATH, RESULTS_FILE_TXT))) {

            for (final Map.Entry<String, WebDriverResourceResult.Summary> summaryEntry : overallResults.entrySet()) {
                txtOutput
                        .append(PROPERTY_PREFIX).append('.')
                        .append(summaryEntry.getKey()).append('.')
                        .append(JAVA_SCRIPT).append('=')
                        .append(Long.toString(summaryEntry.getValue().getJavaScript().getSize()))
                        .append("\n");
                txtOutput
                        .append(PROPERTY_PREFIX).append('.')
                        .append(summaryEntry.getKey()).append('.')
                        .append(CSS).append('=')
                        .append(Long.toString(summaryEntry.getValue().getCss().getSize()))
                        .append("\n");
            }
        }
    }

    private static void saveAsJson(final Map<String, WebDriverResourceResult.Summary> overallResults) throws IOException {
        try (final PrintWriter jsonOutput = new PrintWriter(new File(RESULTS_PATH, RESULTS_FILE_JSON))) {
            final Gson gson = new Gson();
            jsonOutput.append(gson.toJson(overallResults));
        }
    }

    private static Map<String, Long> loadReferenceSizes() throws IOException {
        try (final InputStream inputStream = PageWeightAnalyserTest.class.getResourceAsStream(PAGEWEIGHT_PROPERTIES)) {
            final Properties properties = new Properties();
            properties.load(inputStream);
            return properties.entrySet().stream()
                    .collect(Collectors.toMap(entry -> entry.getKey().toString(), entry -> Long.valueOf(String.valueOf(entry.getValue()))));
        }
    }

    private static String getIssueKey(final JiraHeader jiraHeader) {
        final IssuesMenu issuesMenu = jiraHeader.getIssuesMenu();
        if (!issuesMenu.isExists()) {
            return null;
        }
        final List<String> recentIssues = issuesMenu.open().getRecentIssues();
        issuesMenu.close();
        if (recentIssues.size() > 0) {
            final String s = recentIssues.get(0);
            return s.substring(0, s.indexOf(' '));
        } else {
            return null;
        }
    }

    private void assertPageSizeIsNotGrowing(final String page, final WebDriverResourceResult.Summary summary) {
        if (System.getProperty("do.not.check.page.weight.sizes") != null) {
            return;
        }
        final Long jsMaxSize = referencePageSizes.get(PROPERTY_PREFIX + '.' + page + '.' + JAVA_SCRIPT);
        final Long cssMaxSize = referencePageSizes.get(PROPERTY_PREFIX + '.' + page + '.' + CSS);
        if (jsMaxSize != null) {
            final long jsSize = summary.getJavaScript().getSize();
            assertThat(getFailureDescription(page, JAVA_SCRIPT, jsSize - jsMaxSize), jsSize, Matchers.lessThanOrEqualTo(jsMaxSize));
        }

        if (cssMaxSize != null) {
            final long cssSize = summary.getCss().getSize();
            assertThat(getFailureDescription(page, CSS, cssSize - cssMaxSize), cssSize, Matchers.lessThanOrEqualTo(cssMaxSize));
        }
    }

    private String getFailureDescription(final String page, final String resource, final long sizeDifference) {
        return MessageFormat.format(FAILURE_DESCRIPTION, resource, page, FileSize.format(sizeDifference));
    }

}
