package com.atlassian.jira.webtest.webdriver.tests.issue;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock;
import com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock.ViewMode;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.rule.Rules;
import com.atlassian.jira.pageobjects.JiraWebDriverTest;
import com.atlassian.jira.pageobjects.dialogs.DeleteAttachmentDialog;
import com.atlassian.jira.pageobjects.navigator.AdvancedSearch;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.ArchiveEntryItem;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.AttachmentRow;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.AttachmentSection;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.FilePreview;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.FilePreview.PreviewType;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.common.collect.ImmutableList;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.pageobjects.pages.viewissue.attachment.FilePreview.FILE_PREVIEW_ID;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@WebTest({Category.WEBDRIVER_TEST, Category.ATTACHMENTS})
@RestoreOnce("TestAttachmentPreview/TestAttachmentPreview.xml")
public class TestAttachmentPreview extends JiraWebDriverTest {
    private static final String FILE_IMAGE = "image.png";
    private static final String FILE_IMAGE_LARGE = "large-image.png";
    private static final String FILE_PDF = "document.pdf";
    private static final String FILE_PDF_LARGE = "pdf-example-bookmarks.original.pdf";
    private static final String FILE_PDF_DELETEME = "pdf-example-active-pdf-links.original.pdf";
    private static final String FILE_TEXT = "textfile.txt";
    private static final String FILE_ZIP = "filepreview.zip";

    private static final String FILE_INNER_PDF = "inner-document.pdf";
    private static final String FILE_INNER_IMAGE = "inner-image.png";

    private static final int INITIAL_ATTACHMENT_COUNT = 8;
    private static final int THUMBNAIL_ATTACHMENT_COUNT = 5;

    private static final String ISSUE_KEY = "HSP-2";

    @After
    public void tearDown() throws Exception {
        backdoor.darkFeatures().disableForSite("jira.fileviewer.disable.pdf");
        backdoor.darkFeatures().disableForSite("jira.fileviewer.disabled");
    }

    @ClassRule
    public static final TestRule prepareAttachments = new TestRule() {
        @Override
        public Statement apply(final Statement base, final Description description) {
            return Rules.chain()
                    .around(getBaseClassRule())
                    .around(Rules.prepareAttachments(jira::environmentData, () -> new Backdoor(jira.environmentData()), "TestAttachmentPreview/attachments"))
                    .apply(base, description);
        }
    };

    @Rule
    public final TestRule baseRule = getBaseRule();
    private ViewIssuePage viewIsuePage;

    @Inject
    private PageElementFinder elementFinder;

    @Before
    public void visitViewIssuePage() throws Exception {
        jira.quickLoginAsAdmin();

        viewIsuePage = jira.goToViewIssue(ISSUE_KEY);
    }

    private AttachmentRow getAttachmentRow(String title) {
        return viewIsuePage.getAttachmentSection().getAttachmentRowsByTitle(false, INITIAL_ATTACHMENT_COUNT).get(title);
    }

    private void setViewMode(ViewMode viewMode) {
        viewIsuePage.getAttachmentSection().openOptions().setViewMode(viewMode);
    }

    @Test
    public void shouldPreviewImagesInThumbnailsMode() {
        setViewMode(ViewMode.GALLERY);

        assertPreviewType(FILE_IMAGE, PreviewType.IMAGE);
    }

    @Test
    public void shouldPreviewDocumentInThumbnailsMode() {
        setViewMode(ViewMode.GALLERY);

        assertPreviewType(FILE_PDF, PreviewType.PDF);
    }

    @Test
    public void shouldDownloadTextFileInThumbnailsMode() {
        setViewMode(ViewMode.GALLERY);

        getAttachmentRow(FILE_TEXT).getFileLink().click();
        assertFalse("File preview is not present", elementFinder.find(By.id(FILE_PREVIEW_ID)).isPresent());

        assertCurrentUrl(FILE_TEXT);
    }

    @Test
    public void shouldPreviewImageInListMode() {
        setViewMode(ViewMode.LIST);

        assertPreviewType("image.png", PreviewType.IMAGE);
    }

    @Test
    public void shouldPreviewDocumentInListMode() {
        setViewMode(ViewMode.LIST);

        assertPreviewType("document.pdf", PreviewType.PDF);
    }

    @Test()
    public void shouldNotPreviewDocumentInListModeWhenPDFDisabled() {
        backdoor.darkFeatures().enableForSite("jira.fileviewer.disable.pdf");
        viewIsuePage = jira.goToViewIssue(ISSUE_KEY);
        setViewMode(ViewMode.LIST);

        getAttachmentRow("document.pdf").getFileLink().click();
        assertFalse("File preview is not present", elementFinder.find(By.id(FILE_PREVIEW_ID)).isPresent());
    }

    @Test
    public void shouldPreviewImageInListModeWhenPDFDisabled() {
        backdoor.darkFeatures().enableForSite("jira.fileviewer.disable.pdf");
        viewIsuePage = jira.goToViewIssue(ISSUE_KEY);
        setViewMode(ViewMode.LIST);

        assertPreviewType("image.png", PreviewType.IMAGE);
    }

    @Test()
    public void shouldNotPreviewImageInListModeWhenDisabled() {
        backdoor.darkFeatures().enableForSite("jira.fileviewer.disabled");
        viewIsuePage = jira.goToViewIssue(ISSUE_KEY);
        setViewMode(ViewMode.LIST);

        getAttachmentRow("image.png").getFileLink().click();
        assertFalse("File preview is not present", elementFinder.find(By.id(FILE_PREVIEW_ID)).isPresent());    }

    @Test
    public void shouldDownloadTextFileInListMode() {
        setViewMode(ViewMode.LIST);

        getAttachmentRow(FILE_TEXT).getFileLink().click();
        assertFalse("File preview is not present", elementFinder.find(By.id(FILE_PREVIEW_ID)).isPresent());

        assertCurrentUrl(FILE_TEXT);
    }

    @Test
    public void shouldPreviewImageAfterModeSwitch() {
        setViewMode(ViewMode.LIST);
        assertPreviewType("document.pdf", PreviewType.PDF);

        setViewMode(ViewMode.GALLERY);
        assertPreviewType("document.pdf", PreviewType.PDF);
    }

    @Test
    public void shouldPreviewExpansionEntry() {
        setViewMode(ViewMode.LIST);
        final AttachmentRow attachmentRow = getAttachmentRow(FILE_ZIP);

        final ArchiveEntryItem entryImage = getArchiveEntryItem(attachmentRow, FILE_INNER_IMAGE);
        final ArchiveEntryItem entryDocument = getArchiveEntryItem(attachmentRow, FILE_INNER_PDF);

        assertPreviewType(entryDocument.openPreview(), PreviewType.PDF);
        assertPreviewType(entryImage.openPreview(), PreviewType.IMAGE);
    }

    @Test
    public void shouldPreviewThumbnailLink() {
        setViewMode(ViewMode.GALLERY);
        final PageElement descriptionValue = viewIsuePage.getDescriptionValue();
        final PageElement thumbnailLink = descriptionValue.find(By.cssSelector("a[file-preview-title]"));

        assertEquals(thumbnailLink.getAttribute("file-preview-title"), FILE_IMAGE_LARGE);

        thumbnailLink.click();
        final FilePreview filePreview = pageBinder.bind(FilePreview.class);

        assertPreviewType(filePreview, PreviewType.IMAGE);
    }

    @Test
    public void shouldPreserveDocumentOrderInListMode() {
        setViewMode(ViewMode.LIST);
        // collect previewable attachment titles from page
        final ImmutableList<String> attachmentTitles = elementFinder.findAll(By.cssSelector("*[file-preview-id]:not(:empty)")).stream()
                .map(el -> el.getAttribute("file-preview-title"))
                .distinct().collect(CollectorsUtil.toImmutableList());

        final List<String> thumbnailTitles = getFilePreview(FILE_IMAGE).showAllFiles().getThumbnailTitles();

        assertThat(attachmentTitles, is(thumbnailTitles));
    }

    @Test
    public void shouldPreserveDocumentOrderInGalleryMode() {
        setViewMode(ViewMode.GALLERY);
        // collect previewable attachment titles from page
        final ImmutableList<String> attachmentTitles = elementFinder.findAll(By.cssSelector("*[file-preview-id]:not(:empty)")).stream()
                .map(el -> el.getAttribute("file-preview-title"))
                .distinct().collect(CollectorsUtil.toImmutableList());

        final List<String> thumbnailTitles = getFilePreview(FILE_IMAGE).showAllFiles().getThumbnailTitles();

        assertThat(attachmentTitles, is(thumbnailTitles));
    }

    @Test
    public void testOpenLargeImage() {
        final ViewIssuePage viewIssuePage = jira.goToViewIssue(ISSUE_KEY);
        final AttachmentSection attachmentSection = viewIssuePage.getAttachmentSection();

        // Enforce list view (attachment section is broken in gallery view)
        attachmentSection.openOptions().setViewMode(AttachmentsBlock.ViewMode.LIST);

        // Open a large image
        attachmentSection.getAttachmentRowsByTitle(false, INITIAL_ATTACHMENT_COUNT).get(FILE_IMAGE_LARGE).getFileLink().click();

        final FilePreview previewer = pageBinder.bind(FilePreview.class);
        previewer.waitUntilImageVisible();

        // Open mini mode
        previewer.showAllFiles();
        assertEquals("Wrong number of thumbnails", THUMBNAIL_ATTACHMENT_COUNT, previewer.getThumbnailCount());

        previewer.close();
        assertFalse("Previewer not closed", previewer.isVisible());
    }

    @Test
    public void testOpenLargeDocument() {
        final ViewIssuePage viewIssuePage = jira.goToViewIssue(ISSUE_KEY);
        final AttachmentSection attachmentSection = viewIssuePage.getAttachmentSection();

        // Enforce list view (attachment section is broken in gallery view)
        attachmentSection.openOptions().setViewMode(AttachmentsBlock.ViewMode.LIST);

        // Open a large PDF
        attachmentSection.getAttachmentRowsByTitle(false, INITIAL_ATTACHMENT_COUNT).get(FILE_PDF_LARGE).getFileLink().click();

        final FilePreview previewer = pageBinder.bind(FilePreview.class);
        previewer.waitUntilPDFVisible();

        previewer.close();
        assertFalse("Previewer not closed", previewer.isVisible());
    }

    @Test
    public void testMiniMode() {
        try {
            final ViewIssuePage viewIssuePage = jira.goToViewIssue(ISSUE_KEY);
            final AttachmentSection attachmentSection = viewIssuePage.getAttachmentSection();

            // Enforce list view (attachment section is broken in gallery view)
            attachmentSection.openOptions().setViewMode(AttachmentsBlock.ViewMode.LIST);
            attachmentSection.openOptions().setSortBy(AttachmentsBlock.Sort.Key.DATE);
            attachmentSection.openOptions().setSortOrder(AttachmentsBlock.Sort.Direction.DESCENDING);

            Map<String, AttachmentRow> previewAttachments = attachmentSection.getAttachmentRowsByTitle(true, INITIAL_ATTACHMENT_COUNT);

            // Open an attachment
            attachmentSection.getAttachmentRowsByTitle(false, INITIAL_ATTACHMENT_COUNT).get(FILE_IMAGE).getFileLink().click();

            final FilePreview previewer = pageBinder.bind(FilePreview.class);
            Poller.waitUntilTrue("No image in view", previewer.isImageView());

            // Open mini mode
            previewer.showAllFiles();
            assertEquals("Wrong number of thumbnails", previewAttachments.size(), previewer.getThumbnailCount());

            previewer.close();
            assertFalse("Previewer not closed", previewer.isVisible());

            attachmentSection.openOptions().setSortBy(AttachmentsBlock.Sort.Key.NAME);
            attachmentSection.openOptions().setSortOrder(AttachmentsBlock.Sort.Direction.ASCENDING);

            // Delete an attachment
            final DeleteAttachmentDialog delete = attachmentSection.getAttachmentRowsByTitle(false, INITIAL_ATTACHMENT_COUNT).get(FILE_PDF_DELETEME).delete();
            delete.submit();
            Poller.waitUntilTrue("Delete dialog not closed", delete.isClosed());

            previewAttachments = attachmentSection.getAttachmentRowsByTitle(true, INITIAL_ATTACHMENT_COUNT - 1);

            // Open another attachment
            attachmentSection.getAttachmentRowsByTitle(false, INITIAL_ATTACHMENT_COUNT - 1).get(FILE_PDF).getFileLink().click();
            Poller.waitUntilTrue("No PDF in view", previewer.isPDFView());

            // Open mini mode
            previewer.showAllFiles();
            // Validate number of thumbnails has decreased by 1
            assertEquals("Wrong number of thumbnails", previewAttachments.size(), previewer.getThumbnailCount());

            previewer.close();
            assertFalse("Previewer not closed", previewer.isVisible());
        } finally {
            backdoor.restoreDataFromResource("TestAttachmentPreview/TestAttachmentPreview.xml");
        }
    }

    @Test
    public void testPreviewerWhenPanelRefreshed() {
        final ViewIssuePage viewIssuePage = jira.goToViewIssue(ISSUE_KEY);
        final AttachmentSection attachmentSection = viewIssuePage.getAttachmentSection();

        // Enforce list view (attachment section is broken in gallery view)
        attachmentSection.openOptions().setViewMode(AttachmentsBlock.ViewMode.LIST);

        // Open a document
        attachmentSection.getAttachmentRowsByTitle(false, INITIAL_ATTACHMENT_COUNT).get(FILE_PDF).getFileLink().click();
        final FilePreview previewer = pageBinder.bind(FilePreview.class);
        Poller.waitUntilTrue("No PDF in view", previewer.isPDFView());

        previewer.close();
        assertFalse("Previewer not closed", previewer.isVisible());
    }

    @Test
    public void shouldClosePreviewAfterPressingBackButtonInGlobalIssueNavigator() {
        final AdvancedSearch issueNav = jira.goToIssueNavigator();

        // This will change the URL to /browse/HSP-2?jql=issuekey%20%3D%20HSP-2 and push state to browser history,
        // but won't do a full page reload.
        issueNav.enterQuery(String.format("issuekey = %s", ISSUE_KEY)).submit();

        FilePreview preview = assertPreviewType(getFilePreview("document.pdf"), PreviewType.PDF, false);

        jira.getTester().getDriver().navigate().back();

        // Validate that pressing the browser 'back' button closes the Preview and sends the user back to the
        // previous URL /browse/HSP-2?jql=
        Poller.waitUntilFalse("File preview is still visible", preview.isPresentCondition());
    }

    private ArchiveEntryItem getArchiveEntryItem(final AttachmentRow attachmentRow, final String entryTitle) {
        final List<ArchiveEntryItem> archiveEntryItems = attachmentRow.expandEntries();
        return archiveEntryItems.stream()
                .filter(item -> item.getTitle().equals(entryTitle))
                .findFirst().get();
    }

    private FilePreview getFilePreview(final String title) {
        final AttachmentRow imageRow = getAttachmentRow(title);
        return imageRow.openPreview();
    }

    private FilePreview assertPreviewType(final String title, final PreviewType previewType) {
        return assertPreviewType(getFilePreview(title), previewType);
    }

    private FilePreview assertPreviewType(final FilePreview filePreview, final PreviewType previewType) {
        return assertPreviewType(filePreview, previewType, true);
    }

    private FilePreview assertPreviewType(final FilePreview filePreview, final PreviewType previewType, boolean close) {
        assertEquals(String.format("Preview type is %s", previewType.toString()), previewType, filePreview.getType());

        if (close) {
            filePreview.close();
        }

        return filePreview;
    }

    private void assertCurrentUrl(final String suffix) {
        assertThat(String.format("Current url ends with %s", suffix), jira.getTester().getDriver().getCurrentUrl(), endsWith(suffix));
    }
}
