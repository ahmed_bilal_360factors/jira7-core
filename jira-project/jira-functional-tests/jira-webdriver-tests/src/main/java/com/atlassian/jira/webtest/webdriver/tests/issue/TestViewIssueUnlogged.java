package com.atlassian.jira.webtest.webdriver.tests.issue;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.pages.viewissue.CommentsModule;
import com.atlassian.jira.pageobjects.pages.viewissue.HistoryModule;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.webtest.webdriver.tests.common.BaseJiraWebTest;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import org.hamcrest.collection.IsIterableWithSize;
import org.junit.Test;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * Test for viewing comments and history of issue by anonymous user.
 *
 * @since v7.0
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ISSUES})
@Restore("TestViewIssueUnlogged.zip")
public class TestViewIssueUnlogged extends BaseJiraWebTest {
    @Test
    public void testViewCommentsAndHistory() {
        jira.logout();
        ViewIssuePage viewIssuePage = jira.goToViewIssue("HSP-2");

        HistoryModule historyModule = viewIssuePage.getActivitySection().historyModule();
        TimedQuery<Iterable<HistoryModule.IssueHistoryItemContainer>> historyItemsTimed = historyModule.getHistoryItemContainers();
        Poller.waitUntil(historyItemsTimed, IsIterableWithSize.iterableWithSize(2));

        CommentsModule commentsModule = viewIssuePage.getActivitySection().commentsModule();
        assertThat(commentsModule.getComments(), hasSize(1));
    }
}

