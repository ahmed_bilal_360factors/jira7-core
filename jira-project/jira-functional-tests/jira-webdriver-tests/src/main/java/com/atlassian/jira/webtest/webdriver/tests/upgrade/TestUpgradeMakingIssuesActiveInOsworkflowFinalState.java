package com.atlassian.jira.webtest.webdriver.tests.upgrade;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.framework.util.TimedQueryFactory;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Optional;

import static com.atlassian.collectors.CollectorsUtil.toImmutableSet;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.google.common.collect.ImmutableSet.of;
import static java.lang.String.format;
import static java.util.Collections.emptySet;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;

/**
 * Test checks if after running upgrade task issues that were previously in osworkflow final state (state without outgoing transitions)
 * can be transitioned to new state (after adding 2 new final states and linking existing final state to 2 new final states)
 *
 * Previous workflow ----done
 * Altered workflow ----done----new state1
 *                          \---new state2
 *
 * TEST-12 issue created by JIM in final state (done). By default JIM adds entry in OS_CURRENTSTEP for all entries.
 * TEST-14 issue created normally and transitioned to final state (done). No initial entry in OS_CURRENTSTEP
 * TEST-17 issue created normally and bulk transitioned to final state (Done). No initial entry in OS_CURRENTSTEP
 * TEST-19 issue created normally and transitioned to done. Then finish date set to null in OS_HISTORYSTEP for that issue. Upgrade task should not process this issue.
 * TEST-20 issue created normally and transitioned to done. Then removed one top record from OS_HISTORYSTEP_PREV to mimic double entry in OS_HISTORYSTEP that should be resolved by ordering by finish date.
 *
 * SEC-2 issue created by JIM in final state (done) and then bulk moved to other project with different workflow.
 *      By default JIM adds entry in OS_CURRENTSTEP for all entries. Also changing workflow creates new entry in OS_CURRENTSTEP even in final state
 * SEC-4 issue created normally and transitioned to final state (done), then bulk moved to new project and workflow. No initial entry in OS_CURRENTSTEP
 * SEC-5 issue created normally in second project and transitioned to final state
 *
 * @since v7.3
 */
@WebTest({Category.WEBDRIVER_TEST, Category.DASHBOARDS, Category.UPGRADE_TASKS, Category.ISSUE_NAVIGATOR})
@RestoreOnce("xml/upgradetask73011.xml")
public class TestUpgradeMakingIssuesActiveInOsworkflowFinalState extends BaseJiraWebTest {

    @Inject
    PageBinder pageBinder;

    @Inject
    TimedQueryFactory timedQueryFactory;

    @Test
    public void checkIfIssuesInFinalStateInTestProjectCanBeTransitioned() {
        final ImmutableSet<String> expectedTransitions = of("To: Really done", "To: Rejected");
        checkIssueTransitions("TEST-12", expectedTransitions);
        checkIssueTransitions("TEST-14", expectedTransitions);
        checkIssueTransitions("TEST-17", expectedTransitions);
        checkIssueTransitions("TEST-20", expectedTransitions);
    }

    @Test
    public void checkIfIssuesInFinalStateInSecondProjectCanBeTransitioned() {
        final ImmutableSet<String> expectedTransitions = of("To: After done", "To: This time really done");
        checkIssueTransitions("SEC-2", expectedTransitions);
        checkIssueTransitions("SEC-4", expectedTransitions);
        checkIssueTransitions("SEC-5", expectedTransitions);
    }

    @Test
    public void checkIfIssuesInFinalStateWithIncorrectDbDataCannotBeTransitioned() {
        checkIssueTransitions("TEST-19", of());
    }

    private void checkIssueTransitions(String issueKey, Collection<String> transitions) {
        pageBinder.navigateToAndBind(ViewIssuePage.class, issueKey);

        final OpsbarTransitions opsbarTransitions = pageBinder.bind(OpsbarTransitions.class);
        waitUntil(
                format("All transitions should be available %s: %s", issueKey, transitions),
                timedQueryFactory.forSupplier(() -> opsbarTransitions.getAvailableTransitions().equals(transitions)),
                is(true)
        );

        if (!transitions.isEmpty()) {
            final String transitionTo = transitions.iterator().next();
            opsbarTransitions.transitionTo(transitionTo);

            final OpsbarTransitions opsbarTransitionsAfter = pageBinder.bind(OpsbarTransitions.class);
            waitUntil(
                    format("No transitions should be available %s", issueKey),
                    timedQueryFactory.forSupplier(() -> opsbarTransitionsAfter.getAvailableTransitions().isEmpty()),
                    is(true)
            );
        }
    }

    public static class OpsbarTransitions {
        @ElementBy(id = "opsbar-opsbar-transitions")
        PageElement transitions;

        public OpsbarTransitions() {

        }

        public Collection<String> getAvailableTransitions() {
            if (!transitions.isPresent()) {
                return emptySet();
            }
            return transitions.findAll(By.cssSelector(".issueaction-workflow-transition")).stream().map(el -> el.getText()).collect(toImmutableSet());
        }

        public void transitionTo(final String transition) {
            Optional<PageElement> element = transitions.findAll(By.cssSelector(".issueaction-workflow-transition")).stream().filter(el -> el.getText().equals(transition)).findAny();
            assertTrue("Transition should be available: " + transition, element.isPresent());
            element.map(el -> el.click());
        }
    }
}
