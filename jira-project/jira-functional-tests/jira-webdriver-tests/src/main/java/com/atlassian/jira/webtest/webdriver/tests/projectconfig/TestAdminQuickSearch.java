package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.dialogs.ShifterDialog;
import com.atlassian.jira.pageobjects.project.ProjectConfigErrorPage;
import com.atlassian.pageobjects.PageBinder;
import org.junit.Test;

import javax.inject.Inject;

import static org.junit.Assert.fail;

/**
 * @since v4.4
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.IE_INCOMPATIBLE})
@RestoreOnce("xml/projectconfig/TestAdminQuickSearch.xml")
public class TestAdminQuickSearch extends BaseJiraWebTest {
    @Inject
    private PageBinder binder;

    @Test
    public void testQuickSearchDialogNotShownForNormalUser() {
        final ProjectConfigErrorPage config = jira.quickLogin("fred", "fred", ProjectConfigErrorPage.class, "HSP");
        config.execKeyboardShortcut("g", "g");
        if (binder.delayedBind(ShifterDialog.class).inject().get().isOpen()) {
            fail("Dialog should not be opened for non-admin user");
        }
    }
}
