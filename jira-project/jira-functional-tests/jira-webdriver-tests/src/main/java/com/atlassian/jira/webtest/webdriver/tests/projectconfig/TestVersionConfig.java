package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.JiraLoginPage;
import com.atlassian.jira.pageobjects.project.ProjectConfigTabs;
import com.atlassian.jira.pageobjects.project.summary.ProjectSummaryPageTab;
import com.atlassian.jira.pageobjects.project.summary.versions.SummaryPanelVersion;
import com.atlassian.jira.pageobjects.project.summary.versions.VersionSummaryPanel;
import com.atlassian.jira.pageobjects.project.versions.EditVersionForm;
import com.atlassian.jira.pageobjects.project.versions.MergeDialog;
import com.atlassian.jira.pageobjects.project.versions.ReleaseVersionDialog;
import com.atlassian.jira.pageobjects.project.versions.Version;
import com.atlassian.jira.pageobjects.project.versions.VersionPageTab;
import com.atlassian.jira.pageobjects.project.versions.Versions;
import com.atlassian.jira.pageobjects.project.versions.operations.DeleteOperation;
import com.atlassian.jira.pageobjects.project.versions.operations.VersionOperationDropdown;
import com.atlassian.jira.projects.pageobjects.webdriver.page.AdministerReleasePage;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClientFactory;
import com.atlassian.jira.rest.client.api.domain.input.VersionInput;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import com.atlassian.jira.webtest.webdriver.util.AUIFlags;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.core.IsEqual;
import org.hamcrest.core.IsNull;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;

import javax.inject.Inject;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.atlassian.jira.pageobjects.project.versions.DefaultVersion.DATE_FORMAT;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @since v4.4
 */

@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS, Category.IGNITE})
@Restore("xml/projectconfig/versionspanel.xml")
public class TestVersionConfig extends BaseJiraWebTest {
    private static final String EXPECTED_OPERATION = "Expected [%s] to have [%s]";
    private static final String EXPECTED_NO_OPERATION = "Expected [%s] NOT to have [%s]";
    private static final String EXPECTED_FIELD_POPULATED = "Expected [%s] field to still be populated";
    private static final String EXPECTED_FIELD_CLEARED = "Expected [%s] field to be cleared";
    private static final String NEW_VERSION_1 = "New Version 1";
    private static final String NEW_VERSION_5 = "New Version 5";
    private static final String NEW_VERSION_4 = "New Version 4";
    private static final String EDIT_OPERATION = "Edit Operation";
    private static final String ARCHIVE_OPERATION = "Archive Operation";
    private static final String RELEASE_OPERATION = "Release Operation";
    private static final String DELETE_OPERATION = "Delete Operation";
    private static final String UNARCHIVE_OPERATION = "Unarchive Operation";
    private static final String UNRELEASE_OPERATION = "Unrelease Operation";
    private static final String THAT_NAME_IS_ALREADY_USED = "A version with this name already exists in this project.";
    private static final String EXPECTED_ERROR_S = "Expected error [%s]";
    private static final String INVALID_DATE = "Please enter the date in the following format: d/MMM/yy";
    private static final String NEW_VERSION_6 = "New Version 6";
    private static final String NEW_VERSION_7 = "New Version 7";
    private static final String DATE_VAL_START = "20/Mar/50";
    private static final String DATE_VAL_RELEASE = "23/Mar/50";
    private static final String A_NEW_VERSION = "A new version!";
    private static final String BLAH = "blah";
    private static final String HSP = "HSP";
    private static final String NAME = "name";
    private static final String DESCRIPTION = "description";
    private static final String START_DATE = "start date";
    private static final String RELEASE_DATE = "release date";

    private static final String NO_NAME_ERROR = "You must specify a valid version name";
    private static final String VERSION_NAME_TOO_LONG = "The entered version name is too long, it must be less than 255 chars.";
    private static final String VERSION_1 = "version 1";
    private static final String VERSION_2 = "version 2";
    private static final String VERSION_3 = "version 3";
    private static final String VERSION_4 = "version 4";
    private static final String XSS = "XSS";

    @Rule
    public WebDriverScreenshotRule webDriverScreenshotRule = new WebDriverScreenshotRule(); // The screenshot is then saved in jira-project-config-plugin/target/webdriverTests

    @Inject
    private WebDriver driver;
    private JiraRestClient restClient;

    @Inject
    private PageElementFinder finder;

    @Before
    public void before() throws URISyntaxException {
        final JiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
        restClient = factory.createWithBasicHttpAuthentication(jira.environmentData().getBaseUrl().toURI(), JiraLoginPage.USER_ADMIN, JiraLoginPage.PASSWORD_ADMIN);

        //https://jdog.jira-dev.com/browse/JDEV-33168 disable the helptips plugin since the hipchat tip (and others can cause flakiness)
        backdoor.plugins().disablePlugin("com.atlassian.plugins.helptips.jira-help-tips");
    }

    @After
    public void after() {
        backdoor.plugins().enablePlugin("com.atlassian.plugins.helptips.jira-help-tips");
    }

    @Test
    public void testTabNavigation() {
        final ProjectSummaryPageTab config = jira.goTo(ProjectSummaryPageTab.class, HSP);
        assertTrue(config.getTabs().isSummaryTabSelected());

        final AdministerReleasePage versionPageTab = config.getTabs().gotoVersionsTab();
        assertTrue(pageBinder.bind(ProjectConfigTabs.class).isVersionsTabSelected());
        assertThat(versionPageTab.getUrl(), endsWith("/" + HSP + "/administer-versions?"));
    }

    @Test
    public void testDisplayLogic() {
        final VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);

        final List<Version> versions = hspVersionPage.getVersions();

        // test order
        assertTrue(versions.get(0).getName().equals(NEW_VERSION_5));
        assertTrue(versions.get(1).getName().equals(NEW_VERSION_4));
        assertTrue(versions.get(2).getName().equals(NEW_VERSION_1));

        final Version newVersion5 = versions.get(0);
        final VersionOperationDropdown newVersion5Operations = newVersion5.openOperationsCog();

        // unreleased, unarchived

        assertTrue(String.format(EXPECTED_OPERATION, NEW_VERSION_5, ARCHIVE_OPERATION),
                newVersion5Operations.hasOperation("Archive"));

        assertTrue(String.format(EXPECTED_OPERATION, NEW_VERSION_5, RELEASE_OPERATION),
                newVersion5Operations.hasOperation("Release"));

        assertTrue(String.format(EXPECTED_OPERATION, NEW_VERSION_5, DELETE_OPERATION),
                newVersion5Operations.hasOperation("Delete"));

        assertFalse(String.format(EXPECTED_NO_OPERATION, NEW_VERSION_5, UNARCHIVE_OPERATION),
                newVersion5Operations.hasOperation("Unarchive"));

        assertFalse(String.format(EXPECTED_NO_OPERATION, NEW_VERSION_5, UNRELEASE_OPERATION),
                newVersion5Operations.hasOperation("Unrelease"));

        assertTrue("Expected " + NEW_VERSION_5 + " to be overdue", newVersion5.isOverdue());

        newVersion5Operations.close();

        // archived

        final Version newVersion4 = versions.get(1);
        final VersionOperationDropdown newVersion4Operations = newVersion4.openOperationsCog();

        assertTrue(String.format(EXPECTED_OPERATION, NEW_VERSION_4, UNARCHIVE_OPERATION),
                newVersion4Operations.hasOperation("Unarchive"));

        assertFalse(String.format(EXPECTED_NO_OPERATION, NEW_VERSION_4, EDIT_OPERATION),
                newVersion4Operations.hasOperation("Edit Details"));

        assertFalse(String.format(EXPECTED_NO_OPERATION, NEW_VERSION_4, ARCHIVE_OPERATION),
                newVersion4Operations.hasOperation("Archive"));

        assertFalse(String.format(EXPECTED_NO_OPERATION, NEW_VERSION_4, DELETE_OPERATION),
                newVersion4Operations.hasOperation("Delete"));

        assertFalse(String.format(EXPECTED_NO_OPERATION, NEW_VERSION_4, RELEASE_OPERATION),
                newVersion4Operations.hasOperation("Release"));

        assertFalse(String.format(EXPECTED_NO_OPERATION, NEW_VERSION_4, UNRELEASE_OPERATION),
                newVersion4Operations.hasOperation("Unrelease"));

        newVersion4Operations.close();

        // released, unarchived

        final Version newVersion1 = versions.get(2);
        final VersionOperationDropdown newVersion1Operations = newVersion1.openOperationsCog();

        assertTrue(String.format(EXPECTED_OPERATION, NEW_VERSION_1, ARCHIVE_OPERATION),
                newVersion1Operations.hasOperation("Archive"));

        assertTrue(String.format(EXPECTED_OPERATION, NEW_VERSION_1, UNRELEASE_OPERATION),
                newVersion1Operations.hasOperation("Unrelease"));

        assertTrue(String.format(EXPECTED_OPERATION, NEW_VERSION_1, DELETE_OPERATION),
                newVersion1Operations.hasOperation("Delete"));

        assertFalse(String.format(EXPECTED_NO_OPERATION, NEW_VERSION_1, UNARCHIVE_OPERATION),
                newVersion1Operations.hasOperation("Unarchive"));

        assertFalse(String.format(EXPECTED_NO_OPERATION, NEW_VERSION_1, RELEASE_OPERATION),
                newVersion1Operations.hasOperation("Release"));

        newVersion1Operations.close();
    }

    @Test
    public void testCreateXssVersion() {
        final VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);

        final String script = "<script>1;</script>";
        hspVersionPage.getEditVersionForm().fill(script, script, DATE_VAL_START, DATE_VAL_RELEASE).submit();

        final List<Version> versions = hspVersionPage.getVersions();

        assertEquals(new SummaryPanelVersion(script).setDescription(script).setStartDate(DATE_VAL_START).setReleaseDate(DATE_VAL_RELEASE).setOverdue(true), new SummaryPanelVersion(versions.get(0)));
    }

    @Test
    public void testCreateXssInDate() {
        final VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);

        final String script = "<script>1;</script>";
        final EditVersionForm createVersionForm = hspVersionPage.getEditVersionForm().fill(NEW_VERSION_6, A_NEW_VERSION, script, script).submit();

        final EditVersionForm.Field nameField = createVersionForm.getNameField();
        final EditVersionForm.Field descriptionField = createVersionForm.getDescriptionField();
        final EditVersionForm.Field startDateField = createVersionForm.getStartDateField();
        final EditVersionForm.Field releasedDateField = createVersionForm.getReleaseDateField();

        // fields should still be populated
        assertTrue(String.format(EXPECTED_FIELD_POPULATED, NAME),
                nameField.value().equals(NEW_VERSION_6));

        assertTrue(String.format(EXPECTED_FIELD_POPULATED, DESCRIPTION),
                descriptionField.value().equals(A_NEW_VERSION));

        assertTrue(String.format(EXPECTED_FIELD_POPULATED, START_DATE),
                startDateField.value().equals(script));

        assertTrue(String.format(EXPECTED_FIELD_POPULATED, RELEASE_DATE),
                releasedDateField.value().equals(script));

        // errors
        assertTrue(String.format(EXPECTED_ERROR_S, INVALID_DATE),
                startDateField.getError().equals(INVALID_DATE));

        assertTrue(String.format(EXPECTED_ERROR_S, INVALID_DATE),
                releasedDateField.getError().equals(INVALID_DATE));
    }

    @Test
    public void testCreateVersion() throws InterruptedException {
        final VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);

        final EditVersionForm createVersionForm = hspVersionPage.getEditVersionForm()
                .fill(NEW_VERSION_5, A_NEW_VERSION, BLAH, BLAH)
                .submit();

        final EditVersionForm.Field nameField = createVersionForm.getNameField();
        final EditVersionForm.Field descriptionField = createVersionForm.getDescriptionField();
        final EditVersionForm.Field startDateField = createVersionForm.getStartDateField();
        final EditVersionForm.Field releasedDateField = createVersionForm.getReleaseDateField();

        // fields should still be populated
        assertTrue(String.format(EXPECTED_FIELD_POPULATED, NAME),
                nameField.value().equals(NEW_VERSION_5));

        assertTrue(String.format(EXPECTED_FIELD_POPULATED, DESCRIPTION),
                descriptionField.value().equals(A_NEW_VERSION));

        assertTrue(String.format(EXPECTED_FIELD_POPULATED, START_DATE),
                startDateField.value().equals(BLAH));

        assertTrue(String.format(EXPECTED_FIELD_POPULATED, RELEASE_DATE),
                releasedDateField.value().equals(BLAH));

        // errors

        assertTrue(String.format(EXPECTED_ERROR_S, THAT_NAME_IS_ALREADY_USED),
                nameField.getError().equals(THAT_NAME_IS_ALREADY_USED));

        assertTrue(String.format(EXPECTED_ERROR_S, INVALID_DATE),
                startDateField.getError().equals(INVALID_DATE));

        assertTrue(String.format(EXPECTED_ERROR_S, INVALID_DATE),
                releasedDateField.getError().equals(INVALID_DATE));

        final String longText = StringUtils.repeat("a", 256);

        final EditVersionForm versionNameTooLongForm = hspVersionPage.getEditVersionForm()
                .fill(longText, "", null, null)
                .submit();

        assertTrue(String.format(EXPECTED_ERROR_S, VERSION_NAME_TOO_LONG),
                versionNameTooLongForm.getNameField().getError().equals(VERSION_NAME_TOO_LONG));

        final EditVersionForm noVersionNameForm = hspVersionPage.getEditVersionForm()
                .fill("", "", BLAH, BLAH)
                .submit();

        assertTrue(String.format(EXPECTED_ERROR_S, NO_NAME_ERROR),
                noVersionNameForm.getNameField().getError().equals(NO_NAME_ERROR));

        hspVersionPage.getEditVersionForm()
                .fill(NEW_VERSION_6, A_NEW_VERSION, DATE_VAL_START, DATE_VAL_RELEASE)
                .submit();

        // after successful submit fields should be cleared
        assertTrue(String.format(EXPECTED_FIELD_CLEARED, NAME), nameField.value().isEmpty());
        assertTrue(String.format(EXPECTED_FIELD_CLEARED, DESCRIPTION), descriptionField.value().isEmpty());
        assertTrue(String.format(EXPECTED_FIELD_CLEARED, START_DATE), startDateField.value().isEmpty());
        assertTrue(String.format(EXPECTED_FIELD_CLEARED, RELEASE_DATE), releasedDateField.value().isEmpty());

        final List<Version> versions = hspVersionPage.getVersions();

        assertEquals(new SummaryPanelVersion(NEW_VERSION_6).setDescription(A_NEW_VERSION).setStartDate(DATE_VAL_START).setReleaseDate(DATE_VAL_RELEASE).setOverdue(true),
                new SummaryPanelVersion(versions.get(0)));

        // create version with start and release dates selected using date picker
        final EditVersionForm withSelectedReleaseDateForm = hspVersionPage.getEditVersionForm();

        withSelectedReleaseDateForm.fill(NEW_VERSION_7, "", "", "");

        final int startDaySelected = 15;
        final int releaseDaySelected = 17;
        withSelectedReleaseDateForm.getStartDatePicker()
                .openCalendarPopup()
                .selectDay(startDaySelected);

        withSelectedReleaseDateForm.getReleaseDatePicker()
                .openCalendarPopup()
                .selectDay(releaseDaySelected);

        withSelectedReleaseDateForm.submit();

        assertThat(withSelectedReleaseDateForm.getStartDateField().getError(), IsNull.nullValue());
        assertThat(withSelectedReleaseDateForm.getReleaseDateField().getError(), IsNull.nullValue());

        final Version createdVersion = hspVersionPage.getVersionByName(NEW_VERSION_7);
        assertNotNull(createdVersion);

        final DateTime startDate = new DateTime(createdVersion.getStartDate());
        assertThat(startDate.getDayOfMonth(), IsEqual.equalTo(startDaySelected));
        final DateTime releaseDate = new DateTime(createdVersion.getReleaseDate());
        assertThat(releaseDate.getDayOfMonth(), IsEqual.equalTo(releaseDaySelected));
    }

    @Test
    public void testArchiveVersion() throws InterruptedException {

        assertTrue(navigateToSummaryPageFor(HSP).openPanel(VersionSummaryPanel.class)
                .hasVersion(NEW_VERSION_5));

        final VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);
        final List<Version> versions = hspVersionPage.getVersions();

        final Version versionToArchive = versions.get(0);

        assertTrue(versionToArchive.getName().equals(NEW_VERSION_5));
        assertFalse(versionToArchive.isArchived());

        final VersionOperationDropdown versionOperationDropdown = versionToArchive.openOperationsCog();
        assertTrue(versionOperationDropdown.hasOperation("Archive"));

        versionOperationDropdown.click("Archive");

        waitUntilTrue(versionToArchive.hasFinishedVersionOperation());
        assertTrue(versionToArchive.isArchived());

        assertFalse(navigateToSummaryPageFor(HSP).openPanel(VersionSummaryPanel.class)
                .hasVersion(NEW_VERSION_5));
    }

    @Test
    public void testArchiveVersionWithServerError() throws InterruptedException {

        assertTrue(navigateToSummaryPageFor(HSP).openPanel(VersionSummaryPanel.class)
                .hasVersion(NEW_VERSION_5));

        final VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);
        final List<Version> versions = hspVersionPage.getVersions();

        final Version versionToArchive = versions.get(0);

        assertTrue(versionToArchive.getName().equals(NEW_VERSION_5));
        assertFalse(versionToArchive.isArchived());

        driver.manage().deleteAllCookies();

        final VersionOperationDropdown versionOperationDropdown = versionToArchive.openOperationsCog();
        assertTrue(versionOperationDropdown.hasOperation("Archive"));

        versionOperationDropdown.click("Archive");

        waitUntilTrue(versionToArchive.hasFinishedVersionOperation());
        assertFalse(versionToArchive.isArchived());
        assertTrue("Expected an error, because anonymous user does not have sufficient permissions.",
                hspVersionPage.getServerError().contains("You must have browse project rights in order to view versions."));

        jira.quickLoginAsSysadmin();
        assertTrue(navigateToSummaryPageFor(HSP).openPanel(VersionSummaryPanel.class).hasVersion(NEW_VERSION_5));
    }

    @Test
    public void testUnArchiveVersion() throws InterruptedException {

        assertFalse(navigateToSummaryPageFor(HSP).openPanel(VersionSummaryPanel.class)
                .hasVersion(NEW_VERSION_4));

        final VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);
        final List<Version> versions = hspVersionPage.getVersions();

        final Version versionToUnarchive = versions.get(1);

        assertTrue(versionToUnarchive.getName().equals(NEW_VERSION_4));
        assertTrue(versionToUnarchive.isArchived());

        final VersionOperationDropdown versionOperationDropdown = versionToUnarchive.openOperationsCog();
        assertTrue(versionOperationDropdown.hasOperation("Unarchive"));

        versionOperationDropdown.click("Unarchive");

        waitUntilTrue(versionToUnarchive.hasFinishedVersionOperation());
        assertFalse(versionToUnarchive.isArchived());

        assertTrue(navigateToSummaryPageFor(HSP).openPanel(VersionSummaryPanel.class)
                .hasVersion(NEW_VERSION_4));
    }

    @Test
    public void testUnArchiveVersionWithServerError() throws InterruptedException {
        assertFalse(navigateToSummaryPageFor(HSP).openPanel(VersionSummaryPanel.class)
                .hasVersion(NEW_VERSION_4));

        final VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);
        final List<Version> versions = hspVersionPage.getVersions();

        final Version versionToUnarchive = versions.get(1);

        assertTrue(versionToUnarchive.getName().equals(NEW_VERSION_4));
        assertTrue(versionToUnarchive.isArchived());

        driver.manage().deleteAllCookies();

        final VersionOperationDropdown versionOperationDropdown = versionToUnarchive.openOperationsCog();
        assertTrue(versionOperationDropdown.hasOperation("Unarchive"));

        versionOperationDropdown.click("Unarchive");

        waitUntilTrue(versionToUnarchive.hasFinishedVersionOperation());
        assertTrue(versionToUnarchive.isArchived());
        assertTrue("Expected an error, because anonymous user does not have sufficient permissions.",
                hspVersionPage.getServerError().contains("You must have browse project rights in order to view versions."));

        jira.quickLoginAsSysadmin();
        assertFalse(navigateToSummaryPageFor(HSP).openPanel(VersionSummaryPanel.class)
                .hasVersion(NEW_VERSION_4));
    }

    @Test
    public void testCreateVersionServerErrorHandling() throws InterruptedException {
        final VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);

        driver.manage().deleteAllCookies();

        pageBinder.bind(VersionPageTab.class, HSP).getEditVersionForm()
                .fill(NEW_VERSION_5, A_NEW_VERSION, BLAH, BLAH)
                .submit();

        assertTrue("Expected an error, because anonymous user does not have sufficient permissions.", hspVersionPage.getServerError().contains("Project with key 'HSP' either does not exist or you do not have permission to create versions in it."));
    }

    @Test
    public void testEditVersion() throws InterruptedException, ParseException {
        final VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);
        final List<Version> versions = hspVersionPage.getVersions();

        // Test with all data valid
        versions.get(0)
                .edit("name")
                .fill("Scott", "Scott's Version", "16/Apr/11", "18/Apr/11")
                .submit();

        final Version version = versions.get(0);

        assertEquals("Scott", version.getName());
        assertEquals("Scott's Version", version.getDescription());

        assertEquals(DATE_FORMAT.parse("16/Apr/11"), version.getStartDate());
        assertEquals(DATE_FORMAT.parse("18/Apr/11"), version.getReleaseDate());

        // Just open and submit to test if release date is properly formatted in input field
        final EditVersionForm editForm = version
                .edit("name")
                .submit();

        assertThat(editForm.getStartDateField().getError(), IsNull.nullValue());
        assertThat(editForm.getReleaseDateField().getError(), IsNull.nullValue());

        // Test with invalid date
        final EditVersionForm invalidDateForm = versions.get(0)
                .edit("name")
                .fill("Scott", "Scott's Version", "blah", "blah");

        assertNotNull(invalidDateForm
                .submit()
                .getStartDateField()
                .getError());

        assertNotNull(invalidDateForm
                .submit()
                .getReleaseDateField()
                .getError());

        invalidDateForm.cancel();

        // Edit release date using date picker
        final EditVersionForm selectedDateFrom = versions.get(0)
                .edit("name");

        final int selectedStartDate = 13;
        final int selectedReleaseDate = 13;
        selectedDateFrom.getStartDatePicker()
                .openCalendarPopup()
                .selectDay(selectedStartDate);

        selectedDateFrom.getReleaseDatePicker()
                .openCalendarPopup()
                .selectDay(selectedReleaseDate);

        selectedDateFrom.submit();

        assertThat(selectedDateFrom.getStartDateField().getError(), IsNull.nullValue());
        assertThat(selectedDateFrom.getReleaseDateField().getError(), IsNull.nullValue());

        final DateTime startDate = new DateTime(versions.get(0).getStartDate());
        assertThat(startDate.getDayOfMonth(), IsEqual.equalTo(selectedStartDate));
        final DateTime releaseDate = new DateTime(versions.get(0).getReleaseDate());
        assertThat(releaseDate.getDayOfMonth(), IsEqual.equalTo(selectedReleaseDate));
    }

    @Test
    public void testReleaseVersionWithNoUnresolvedIssues() throws InterruptedException {
        VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);
        final List<Version> versions = hspVersionPage.getVersions();

        final Version versionToRelease = versions.get(0);
        assertFalse(versionToRelease.isReleased());
        try {
            assertEquals(DATE_FORMAT.parse("16/Feb/11"), versionToRelease.getReleaseDate());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        final VersionOperationDropdown versionOperationDropdown = versionToRelease
                .openOperationsCog();

        assertTrue(versionOperationDropdown.hasOperation("Release"));
        versionOperationDropdown.click("Release");

        final ReleaseVersionDialog releaseVersionDialog = pageBinder.bind(ReleaseVersionDialog.class);
        assertFalse(releaseVersionDialog.hasUnresolvedIssues());
        assertFalse(releaseVersionDialog.hasIgnoreOption());
        assertFalse(releaseVersionDialog.hasMoveOption());
        assertFalse(releaseVersionDialog.hasUnresolvedMessage());

        releaseVersionDialog
                .setReleaseDate("23/Apr/11")
                .submit();

        waitUntilTrue(releaseVersionDialog.isClosed());

        hspVersionPage = pageBinder.bind(VersionPageTab.class, HSP);

        final Version version = hspVersionPage.getVersions().get(0);

        assertTrue(version.isReleased());

        try {
            assertEquals(DATE_FORMAT.parse("23/Apr/11"), version.getReleaseDate());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @Restore("xml/projectconfig/TestVersionsRelease.xml")
    public void testReleaseVersionWithUnresolvedIssuesBuNoOtherUnreleasedVersions() throws InterruptedException {
        backdoor.darkFeatures().enableForSite("ka.KILL_SWITCH");
        final VersionPageTab xssVersionPage = navigateToVersionsPageFor(XSS);

        final Version versionToRelease = xssVersionPage.getVersionByName(VERSION_1);
        assertFalse(versionToRelease.isReleased());

        final VersionOperationDropdown versionOperationDropdown = versionToRelease
                .openOperationsCog();

        assertTrue(versionOperationDropdown.hasOperation("Release"));
        versionOperationDropdown.click("Release");

        final ReleaseVersionDialog releaseVersionDialog = pageBinder.bind(ReleaseVersionDialog.class);

        assertTrue(releaseVersionDialog.hasUnresolvedIssues());
        assertTrue(releaseVersionDialog.hasIgnoreOption());
        assertFalse(releaseVersionDialog.hasMoveOption());

        assertEquals(addBaseUrl("/secure/IssueNavigator.jspa?reset=true&mode=hide&pid=10010&fixfor=10010&resolution=-1"),
                releaseVersionDialog.unresolvedIssueLinkUrl());
        assertEquals("Ignore and proceed with release - there are no other unreleased fix versions available.",
                releaseVersionDialog.getIgnoreOptionLabelText());
        assertEquals("There are still 1 unresolved issue(s) for this version.",
                releaseVersionDialog.getUnresolvedMessage());

        releaseVersionDialog
                .setReleaseDate("23/Apr/11")
                .submit();

        waitUntilTrue(releaseVersionDialog.isClosed());

        final Version version = xssVersionPage.getVersionByName(VERSION_1);

        assertTrue(version.isReleased());

        try {
            assertEquals(DATE_FORMAT.parse("23/Apr/11"), version.getReleaseDate());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        // Go to issue navigator, ensure there are still open issues for this version
        assertIssueCount("fixVersion=\"" + VERSION_1 + "\" AND resolution=unresolved", 1);
    }

    @Test
    @Restore("xml/projectconfig/TestVersionsRelease.xml")
    public void testReleaseVersionWithUnresolvedIssuesAndIgnore() throws InterruptedException {
        backdoor.darkFeatures().enableForSite("ka.KILL_SWITCH");

        final VersionPageTab xssVersionPage = navigateToVersionsPageFor(XSS);

        xssVersionPage.getEditVersionForm().fill(VERSION_2, "", null, null).submit();

        final Version createdVersion = xssVersionPage.getVersionByName(VERSION_2);
        assertFalse(createdVersion.isReleased());

        Version versionToRelease = xssVersionPage.getVersionByName(VERSION_1);
        assertFalse(versionToRelease.isReleased());

        final VersionOperationDropdown versionOperationDropdown = versionToRelease
                .openOperationsCog();

        assertTrue(versionOperationDropdown.hasOperation("Release"));
        versionOperationDropdown.click("Release");

        final ReleaseVersionDialog releaseVersionDialog = pageBinder.bind(ReleaseVersionDialog.class);
        assertTrue(releaseVersionDialog.hasUnresolvedIssues());
        assertTrue(releaseVersionDialog.hasIgnoreOption());
        assertTrue(releaseVersionDialog.hasMoveOption());

        assertEquals(addBaseUrl("/secure/IssueNavigator.jspa?reset=true&mode=hide&pid=10010&fixfor=10010&resolution=-1"),
                releaseVersionDialog.unresolvedIssueLinkUrl());
        assertEquals("Ignore and proceed with release", releaseVersionDialog.getIgnoreOptionLabelText());
        assertEquals("There are still 1 unresolved issue(s) for this version.",
                releaseVersionDialog.getUnresolvedMessage());

        releaseVersionDialog
                .setReleaseDate("23/Apr/11")
                .ignoreUnresolvedIssues()
                .submit();

        waitUntilTrue(releaseVersionDialog.isClosed());

        versionToRelease = xssVersionPage.getVersionByName(VERSION_1);

        assertTrue(versionToRelease.isReleased());

        try {
            assertEquals(DATE_FORMAT.parse("23/Apr/11"), versionToRelease.getReleaseDate());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        // Go to issue navigator, ensure there are still open issues for this version
        assertIssueCount("fixVersion=\"" + VERSION_1 + "\" AND resolution=unresolved", 1);
    }

    @Test
    @Restore("xml/projectconfig/TestVersionsRelease.xml")
    public void testReleaseVersionAndServerError() throws InterruptedException {
        final VersionPageTab xssVersionPage = navigateToVersionsPageFor(XSS);

        Version versionToRelease = xssVersionPage.getVersionByName(VERSION_1);
        assertFalse(versionToRelease.isReleased());

        final VersionOperationDropdown versionOperationDropdown = versionToRelease
                .openOperationsCog();

        assertTrue(versionOperationDropdown.hasOperation("Release"));
        versionOperationDropdown.click("Release");

        final ReleaseVersionDialog releaseVersionDialog = pageBinder.bind(ReleaseVersionDialog.class);

        assertTrue(releaseVersionDialog.hasUnresolvedIssues());
        assertTrue(releaseVersionDialog.hasIgnoreOption());
        assertFalse(releaseVersionDialog.hasMoveOption());

        assertEquals(addBaseUrl("/secure/IssueNavigator.jspa?reset=true&mode=hide&pid=10010&fixfor=10010&resolution=-1"),
                releaseVersionDialog.unresolvedIssueLinkUrl());
        assertEquals("Ignore and proceed with release - there are no other unreleased fix versions available.",
                releaseVersionDialog.getIgnoreOptionLabelText());
        assertEquals("There are still 1 unresolved issue(s) for this version.",
                releaseVersionDialog.getUnresolvedMessage());

        driver.manage().deleteAllCookies();

        releaseVersionDialog
                .setReleaseDate("23/Apr/11")
                .ignoreUnresolvedIssues()
                .submit();

        waitUntilTrue(releaseVersionDialog.isClosed());
        assertEquals("You must have browse project rights in order to view versions.",
                xssVersionPage.getServerError());

        jira.quickLoginAsSysadmin();

        versionToRelease = navigateToVersionsPageFor(XSS)
                .getVersionByName(VERSION_1);
        assertFalse(versionToRelease.isReleased());
    }

    @Test
    @Restore("xml/projectconfig/TestVersionsRelease.xml")
    public void checkVersionsAreOrderedInMoveUnresolvedIssuesSelect() throws InterruptedException {
        for (final String version : ImmutableList.of("version 3", "zxcv", "vcbcvb", "release", "00000")) {
            restClient.getVersionRestClient().createVersion(new VersionInput(XSS, version, null, null, false, false)).claim();
        }

        final VersionPageTab xssVersionPage = navigateToVersionsPageFor(XSS);

        // Move a version to top of list
        xssVersionPage.moveVersionAbove(xssVersionPage.getVersionByName("zxcv"), xssVersionPage.getVersionByName("00000"));

        xssVersionPage.getEditVersionForm().fill(VERSION_2, "", null, null).submit();

        final Version createdVersion = xssVersionPage.getVersionByName(VERSION_2);
        assertFalse(createdVersion.isReleased());

        final Version versionToRelease = xssVersionPage.getVersionByName(VERSION_1);
        assertFalse(versionToRelease.isReleased());

        final VersionOperationDropdown versionOperationDropdown = versionToRelease
                .openOperationsCog();

        assertTrue(versionOperationDropdown.hasOperation("Release"));
        versionOperationDropdown.click("Release");

        final ReleaseVersionDialog releaseVersionDialog = pageBinder.bind(ReleaseVersionDialog.class);
        assertTrue(releaseVersionDialog.hasUnresolvedIssues());
        assertTrue(releaseVersionDialog.hasIgnoreOption());
        assertTrue(releaseVersionDialog.hasMoveOption());

        final List<String> moveToVersions = releaseVersionDialog.getVersionsAvailableForMove();
        assertEquals(ImmutableList.of("version 2", "zxcv", "00000", "release", "vcbcvb", "version 3"), moveToVersions);

        releaseVersionDialog.cancel();

        final List<String> versionsInTable = ImmutableList.copyOf(Iterables.filter(Iterables.transform(xssVersionPage.getVersions(), Versions.getName()),
                Predicates.not(Predicates.equalTo(VERSION_1))));

        assertEquals("Order in the select and in the table should match", moveToVersions, versionsInTable);
    }

    @Test
    @Restore("xml/projectconfig/TestVersionsRelease.xml")
    public void testReleaseVersionWithUnresolvedIssuesAndMove() throws InterruptedException {
        backdoor.darkFeatures().enableForSite("ka.KILL_SWITCH");

        final VersionPageTab xssVersionPage = navigateToVersionsPageFor(XSS);

        xssVersionPage.getEditVersionForm().fill(VERSION_2, "", null, null).submit();

        final Version createdVersion = xssVersionPage.getVersionByName(VERSION_2);
        assertFalse(createdVersion.isReleased());

        Version versionToRelease = xssVersionPage.getVersionByName(VERSION_1);
        assertFalse(versionToRelease.isReleased());

        final VersionOperationDropdown versionOperationDropdown = versionToRelease
                .openOperationsCog();

        assertTrue(versionOperationDropdown.hasOperation("Release"));
        versionOperationDropdown.click("Release");

        final ReleaseVersionDialog releaseVersionDialog = pageBinder.bind(ReleaseVersionDialog.class);
        assertTrue(releaseVersionDialog.hasUnresolvedIssues());
        assertTrue(releaseVersionDialog.hasIgnoreOption());
        assertTrue(releaseVersionDialog.hasMoveOption());

        assertEquals(addBaseUrl("/secure/IssueNavigator.jspa?reset=true&mode=hide&pid=10010&fixfor=10010&resolution=-1"),
                releaseVersionDialog.unresolvedIssueLinkUrl());
        assertEquals("Ignore and proceed with release", releaseVersionDialog.getIgnoreOptionLabelText());
        assertEquals("There are still 1 unresolved issue(s) for this version.",
                releaseVersionDialog.getUnresolvedMessage());

        releaseVersionDialog
                .setReleaseDate("23/Apr/11")
                .moveUnresolvedIssues(VERSION_2)
                .submit();

        waitUntilTrue(releaseVersionDialog.isClosed());

        versionToRelease = xssVersionPage.getVersionByName(VERSION_1);

        assertTrue(versionToRelease.isReleased());

        try {
            assertEquals(DATE_FORMAT.parse("23/Apr/11"), versionToRelease.getReleaseDate());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        // Go to issue navigator, ensure there are no open issues for this version
        // ensure there are open issues in some other version
        assertIssueCount("fixVersion=\"" + VERSION_1 + "\" AND resolution=unresolved", 0);
        assertIssueCount("fixVersion=\"" + VERSION_2 + "\" AND resolution=unresolved", 1);
    }

    @Test
    @Restore("xml/projectconfig/TestVersionsRelease.xml")
    public void testReleaseVersionWithInvalidDate() throws InterruptedException {
        backdoor.darkFeatures().enableForSite("ka.KILL_SWITCH");

        final VersionPageTab xssVersionPage = navigateToVersionsPageFor(XSS);

        xssVersionPage.getEditVersionForm().fill(VERSION_2, "", null, null).submit();

        final Version createdVersion = xssVersionPage.getVersionByName(VERSION_2);
        assertFalse(createdVersion.isReleased());

        Version versionToRelease = xssVersionPage.getVersionByName(VERSION_1);
        assertFalse(versionToRelease.isReleased());

        final VersionOperationDropdown versionOperationDropdown = versionToRelease
                .openOperationsCog();

        assertTrue(versionOperationDropdown.hasOperation("Release"));
        versionOperationDropdown.click("Release");

        final ReleaseVersionDialog releaseVersionDialog = pageBinder.bind(ReleaseVersionDialog.class);
        assertTrue(releaseVersionDialog.hasUnresolvedIssues());
        assertTrue(releaseVersionDialog.hasIgnoreOption());
        assertTrue(releaseVersionDialog.hasMoveOption());

        assertEquals(addBaseUrl("/secure/IssueNavigator.jspa?reset=true&mode=hide&pid=10010&fixfor=10010&resolution=-1"),
                releaseVersionDialog.unresolvedIssueLinkUrl());
        assertEquals("Ignore and proceed with release", releaseVersionDialog.getIgnoreOptionLabelText());
        assertEquals("There are still 1 unresolved issue(s) for this version.",
                releaseVersionDialog.getUnresolvedMessage());

        releaseVersionDialog
                .setReleaseDate(BLAH)
                .submit();

        releaseVersionDialog.waitUntilFinishedLoading();

        assertTrue(releaseVersionDialog.hasReleaseDateErrorMessage());

        releaseVersionDialog
                .setReleaseDate("23/Apr/11")
                .submit();

        waitUntilTrue(releaseVersionDialog.isClosed());

        versionToRelease = xssVersionPage.getVersionByName(VERSION_1);

        assertTrue(versionToRelease.isReleased());

        try {
            assertEquals(DATE_FORMAT.parse("23/Apr/11"), versionToRelease.getReleaseDate());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        // Go to issue navigator, ensure there are still open issues for this version
        assertIssueCount("fixVersion=\"" + VERSION_1 + "\" AND resolution=unresolved", 1);
    }

    @Test
    @Restore("xml/projectconfig/TestVersionsRelease.xml")
    public void testUnreleaseVersion() {
        final VersionPageTab versionsTab = navigateToVersionsPageFor(HSP);
        final List<Version> versions = versionsTab.getVersions();

        final Version versionToUnrelease = versions.get(2);
        assertTrue(versionToUnrelease.isReleased());

        VersionOperationDropdown versionOperationDropdown = versionToUnrelease.openOperationsCog();
        assertTrue(versionOperationDropdown.hasOperation("Unrelease"));

        versionOperationDropdown.click("Unrelease");
        waitUntilTrue(versionToUnrelease.hasFinishedVersionOperation());
        assertFalse(versionToUnrelease.isReleased());

        versionOperationDropdown = versionToUnrelease.openOperationsCog();
        assertFalse(versionOperationDropdown.hasOperation("Unrelease"));
    }

    @Test
    @Restore("xml/projectconfig/DeleteVersion.xml")
    public void testDeleteVersion() throws InterruptedException {
        backdoor.darkFeatures().enableForSite("ka.KILL_SWITCH");

        // New Version 1 (Affects Version(1))

        VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);

        DeleteOperation deleteDialog = hspVersionPage.getVersionByName("New Version 1")
                .openOperationsCog()
                .clickDelete();

        Map<String, Boolean> operations = deleteDialog.getOperations();

        assertTrue("Expected [New Version 1] to have swap affects version operation",
                operations.get(DeleteOperation.SWAP_AFFECT_VERSION));

        assertTrue("Expected [New Version 1] to have remove affects version operation",
                operations.get(DeleteOperation.SWAP_AFFECT_VERSION));

        assertEquals(1, deleteDialog.getAffectsCount());

        deleteDialog.setAffectsToRemoveVersion();

        assertFalse("Expected [New Version 1] NOT to have swap fix version operation",
                operations.get(DeleteOperation.SWAP_FIX_VERSION));

        assertFalse("Expected [New Version 1] NOT to have remove fix version operation",
                operations.get(DeleteOperation.REMOVE_FIX_VERSION));

        hspVersionPage = deleteDialog.submit();

        assertNull(hspVersionPage.getVersionByName("New Version 1"));
        Response searchResponse = backdoor.search().getSearchResponse(new SearchRequest().jql("affectedVersion=\"New Version 1\""));
        assertEquals("The value 'New Version 1' does not exist for the field 'affectedVersion'.", searchResponse.entity.errorMessages.get(0));

        hspVersionPage = navigateToVersionsPageFor(HSP);

        // New Version 5 (Affects Version(1), Fix Version(1))

        deleteDialog = hspVersionPage.getVersionByName("New Version 5")
                .openOperationsCog()
                .clickDelete();

        operations = deleteDialog.getOperations();

        assertTrue("Expected [New Version 5] to have swap affects version operation",
                operations.get(DeleteOperation.SWAP_AFFECT_VERSION));

        assertTrue("Expected [New Version 5] to have remove affects version operation",
                operations.get(DeleteOperation.SWAP_AFFECT_VERSION));

        assertEquals(1, deleteDialog.getAffectsCount());

        assertTrue("Expected [New Version 5] to have swap fix version operation",
                operations.get(DeleteOperation.SWAP_FIX_VERSION));

        assertTrue("Expected [New Version 5] to have remove fix version operation",
                operations.get(DeleteOperation.REMOVE_FIX_VERSION));

        deleteDialog.setAffectsToSwapVersion("Migrate Version");
        deleteDialog.setFixToSwapVersion("Migrate Version");

        assertEquals(1, deleteDialog.getFixCount());

        hspVersionPage = deleteDialog.submit();

        assertNull(hspVersionPage.getVersionByName("New Version 5"));

        searchResponse = backdoor.search().getSearchResponse(new SearchRequest().jql("fixVersion=\"New Version 5\""));
        assertEquals("The value 'New Version 5' does not exist for the field 'fixVersion'.", searchResponse.entity.errorMessages.get(0));

        SearchResult search = backdoor.search().getSearch(new SearchRequest().jql("fixVersion=\"Migrate Version\""));
        assertEquals(1, search.issues.size());
        search = backdoor.search().getSearch(new SearchRequest().jql("affectedVersion=\"Migrate Version\""));
        assertEquals(1, search.issues.size());

        hspVersionPage = navigateToVersionsPageFor(HSP);

        deleteDialog = hspVersionPage.getVersionByName("Another version")
                .openOperationsCog()
                .clickDelete();

        operations = deleteDialog.getOperations();

        // Another version (No Issues related to this version)

        assertFalse("Expected [Another version] NOT to have swap affects version operation",
                operations.get(DeleteOperation.SWAP_AFFECT_VERSION));

        assertFalse("Expected [Another version] NOT to have remove affects version operation",
                operations.get(DeleteOperation.SWAP_AFFECT_VERSION));

        assertFalse("Expected [Another version] NOT to have swap fix version operation",
                operations.get(DeleteOperation.SWAP_FIX_VERSION));

        assertFalse("Expected [Another version] NOT to have remove fix version operation",
                operations.get(DeleteOperation.REMOVE_FIX_VERSION));

        assertEquals("Are you sure you want to delete Another version?",
                deleteDialog.getInfoMessage());

        deleteDialog.submit();

        assertNull(hspVersionPage.getVersionByName("Another version"));
    }

    @Test
    @Restore("xml/projectconfig/DeleteVersionCustomField.xml")
    public void shouldShowOnlyCustomFieldInfoWhenOnlyCustomFieldAffected() throws InterruptedException {
        backdoor.darkFeatures().enableForSite("ka.KILL_SWITCH");

        VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);

        final String versionReferredFromCustomFields = "Custom";
        DeleteOperation deleteDialog = hspVersionPage.getVersionByName(versionReferredFromCustomFields)
                .openOperationsCog()
                .clickDelete();

        Map<String, Boolean> operations = deleteDialog.getOperations();

        assertFalse(String.format("Expected [%s] NOT to have swap affected version operation", versionReferredFromCustomFields),
                operations.get(DeleteOperation.SWAP_AFFECT_VERSION));
        assertFalse(String.format("Expected [%s] NOT to have remove affected version operation", versionReferredFromCustomFields),
                operations.get(DeleteOperation.REMOVE_AFFECT_VERSION));
        assertFalse(String.format("Expected [%s] NOT to have swap fix version operation", versionReferredFromCustomFields),
                operations.get(DeleteOperation.SWAP_FIX_VERSION));
        assertFalse(String.format("Expected [%s] NOT to have remove fix version operation", versionReferredFromCustomFields),
                operations.get(DeleteOperation.REMOVE_FIX_VERSION));

        final int affectedCustomFieldId = 10000;
        assertThat("Has swap custom field selector for custom field",
                operations, hasEntry(DeleteOperation.CUSTOM_FIELD_SWAP_PREFIX + affectedCustomFieldId, true));
        assertThat("Has remove custom field selector for custom field",
                operations, hasEntry(DeleteOperation.CUSTOM_FIELD_REMOVE_PREFIX + affectedCustomFieldId, true));
        assertThat("Affected issues count for custom version", deleteDialog.getAffectedIssuesForCustomField(affectedCustomFieldId),
                equalTo(1));

        hspVersionPage = deleteDialog.submit();

        assertNull(hspVersionPage.getVersionByName(versionReferredFromCustomFields));
        Response searchResponse = backdoor.search().getSearchResponse(new SearchRequest().jql(String.format("CustomFieldVersion = \"%s\"", versionReferredFromCustomFields)));
        assertEquals("The value 'Custom' does not exist for the field 'CustomFieldVersion'.", searchResponse.entity.errorMessages.get(0));
    }

    @Test
    @Restore("xml/projectconfig/DeleteVersionCustomFieldMany.xml")
    public void deleteDialogShouldSwapVersionOfCustomFields() throws InterruptedException {
        VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);

        final String versionReferredFromCustomFields = "Custom";
        DeleteOperation deleteDialog = hspVersionPage.getVersionByName(versionReferredFromCustomFields)
                .openOperationsCog()
                .clickDelete();

        Map<String, Boolean> operations = deleteDialog.getOperations();
        final int multiVersionCustomField = 10000;
        final int singleVersionCustomField = 10100;
        final int removeFromThisCustomField = 10101;

        assertThat("Custom fields and standard operations are listed",
                operations,
                allOf(
                        hasEntry(DeleteOperation.SWAP_AFFECT_VERSION, true),
                        hasEntry(DeleteOperation.REMOVE_AFFECT_VERSION, true),
                        hasEntry(DeleteOperation.SWAP_FIX_VERSION, true),
                        hasEntry(DeleteOperation.REMOVE_FIX_VERSION, true),
                        hasEntry(DeleteOperation.CUSTOM_FIELD_SWAP_PREFIX + multiVersionCustomField, true),
                        hasEntry(DeleteOperation.CUSTOM_FIELD_REMOVE_PREFIX + multiVersionCustomField, true),
                        hasEntry(DeleteOperation.CUSTOM_FIELD_SWAP_PREFIX + singleVersionCustomField, true),
                        hasEntry(DeleteOperation.CUSTOM_FIELD_REMOVE_PREFIX + singleVersionCustomField, true),
                        hasEntry(DeleteOperation.CUSTOM_FIELD_SWAP_PREFIX + removeFromThisCustomField, true),
                        hasEntry(DeleteOperation.CUSTOM_FIELD_REMOVE_PREFIX + removeFromThisCustomField, true)
                ));
        assertThat("Affected issues count for custom version", deleteDialog.getAffectedIssuesForCustomField(multiVersionCustomField),
                equalTo(3));
        assertThat("Affected issues count for custom version", deleteDialog.getAffectedIssuesForCustomField(singleVersionCustomField),
                equalTo(2));
        assertThat("Affected issues count for custom version", deleteDialog.getAffectedIssuesForCustomField(removeFromThisCustomField),
                equalTo(1));

        final String newVersion1 = "New Version 1";
        final String newVersion5 = "New Version 5";
        final String anotherVersion = "Another version";
        final String migrateVersion = "Migrate Version";

        deleteDialog.setAffectsToSwapVersion(newVersion1);
        deleteDialog.setFixToSwapVersion(newVersion5);
        deleteDialog.setCustomFieldSwapToVersion(multiVersionCustomField, anotherVersion);
        deleteDialog.setCustomFieldSwapToVersion(singleVersionCustomField, migrateVersion);
        deleteDialog.setCustomFieldRemove(removeFromThisCustomField);

        hspVersionPage = deleteDialog.submit();

        assertNull(hspVersionPage.getVersionByName(versionReferredFromCustomFields));

        assertVersionSwitchedTo("fixVersion", versionReferredFromCustomFields, newVersion5, "HSP-1");
        assertVersionSwitchedTo("affectedVersion", versionReferredFromCustomFields, newVersion1, "HSP-2");
        assertVersionSwitchedTo("CustomFieldVersion", versionReferredFromCustomFields, anotherVersion, "HSP-1", "HSP-2", "HSP-3");
        assertVersionSwitchedTo("CustomVersionSingle", versionReferredFromCustomFields, migrateVersion, "HSP-1", "HSP-2");
        assertVersionRemovedFromField("CustomVersionMultiple", versionReferredFromCustomFields);
    }

    @Test
    @Restore("xml/projectconfig/DeleteVersionCustomFieldsNoVersionToSwap.xml")
    public void testDeleteVersionCustomFieldsNoSwap() {
        VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);

        final String versionReferredFromCustomFields = "Custom";
        DeleteOperation deleteDialog = hspVersionPage.getVersionByName(versionReferredFromCustomFields)
                .openOperationsCog()
                .clickDelete();

        Map<String, Boolean> operations = deleteDialog.getOperations();
        final int multiVersionCustomField = 10000;
        final int singleVersionCustomField = 10100;
        final int removeFromThisCustomField = 10101;

        assertThat("No Custom fields and standard operations are listed",
                operations,
                allOf(
                        hasEntry(DeleteOperation.SWAP_AFFECT_VERSION, false),
                        hasEntry(DeleteOperation.REMOVE_AFFECT_VERSION, false),
                        hasEntry(DeleteOperation.SWAP_FIX_VERSION, false),
                        hasEntry(DeleteOperation.REMOVE_FIX_VERSION, false),
                        not(hasEntry(DeleteOperation.CUSTOM_FIELD_SWAP_PREFIX + multiVersionCustomField, true)),
                        not(hasEntry(DeleteOperation.CUSTOM_FIELD_REMOVE_PREFIX + multiVersionCustomField, true))
                ));

        assertThat("Affected issues for affects version", deleteDialog.getFixCount(),
                equalTo(1));
        assertThat("Affected issues count for fix version", deleteDialog.getAffectsCount(),
                equalTo(1));
        assertThat("Affected issues count for custom version", deleteDialog.getAffectedIssuesForCustomField(multiVersionCustomField),
                equalTo(3));
        assertThat("Affected issues count for custom version", deleteDialog.getAffectedIssuesForCustomField(singleVersionCustomField),
                equalTo(2));
        assertThat("Affected issues count for custom version", deleteDialog.getAffectedIssuesForCustomField(removeFromThisCustomField),
                equalTo(1));

        final String affectedIssuesLink = deleteDialog.getAffectedIssuesLinkForCustomField(removeFromThisCustomField);
        assertThat(affectedIssuesLink, containsString("%27CustomVersionMultiple%27%20=%2010210"));
    }

    @Test
    @Restore("xml/projectconfig/DeleteVersionCustomFieldMany.xml")
    public void shouldShowLinkToIssuesWithCustomFieldWithVersion() throws InterruptedException, UnsupportedEncodingException {
        VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);

        final String versionReferredFromCustomFields = "Custom";
        DeleteOperation deleteDialog = hspVersionPage.getVersionByName(versionReferredFromCustomFields)
                .openOperationsCog()
                .clickDelete();

        final int removeFromThisCustomField = 10101;

        final String affectedIssuesLink = deleteDialog.getAffectedIssuesLinkForCustomField(removeFromThisCustomField);
        assertThat(affectedIssuesLink, containsString("%27CustomVersionMultiple%27%20=%2010210"));
    }

    private void assertVersionSwitchedTo(String verionFieldName, String sourceVersion, String targetVersion, String... targetIssues) {
        assertVersionRemovedFromField(verionFieldName, sourceVersion);

        Response<SearchResult> found = backdoor.search().postSearchResponse(new SearchRequest().jql(String.format("%s = \"%s\"", verionFieldName, targetVersion)));
        assertThat(found.statusCode, equalTo(200));
        assertThat(
                found.body.issues.stream().map(issue -> issue.key).collect(Collectors.toList()),
                containsInAnyOrder(targetIssues));
    }

    private void assertVersionRemovedFromField(String verionFieldName, String sourceVersion) {
        Response searchResponse = backdoor.search().getSearchResponse(new SearchRequest().jql(String.format("%s = \"%s\"", verionFieldName, sourceVersion)));
        assertEquals("The value '" + sourceVersion + "' does not exist for the field '" + verionFieldName + "'.", searchResponse.entity.errorMessages.get(0));
    }

    @Test
    @Restore("xml/projectconfig/TestVersionReordering.xml")
    public void testVersionReordering() {
        driver.manage().window().setSize(new Dimension(1024, 768));
        VersionPageTab versionPage = navigateToVersionsPageFor(XSS);

        // Move a version to bottom of list
        versionPage.moveVersionBelow(versionPage.getVersionByName("version 3"), versionPage.getVersionByName("version 1"));
        List<String> expectedVersions = Lists.newArrayList(
                "version 4",
                "version 2",
                "version 1",
                "version 3"
        );
        compareVersionSortOrder(expectedVersions, versionPage.getVersions());

        // Move a version to top of list
        versionPage.moveVersionAbove(versionPage.getVersionByName("version 2"), versionPage.getVersionByName("version 4"));
        expectedVersions = Lists.newArrayList(
                "version 2",
                "version 4",
                "version 1",
                "version 3"
        );
        compareVersionSortOrder(expectedVersions, versionPage.getVersions());

        // Verify persistence across page loads
        versionPage = navigateToVersionsPageFor(XSS);
        compareVersionSortOrder(expectedVersions, versionPage.getVersions());
    }

    @Test
    @Restore("xml/projectconfig/TestVersionReordering.xml")
    public void testVersionReorderingPermissions() {
        driver.manage().window().setSize(new Dimension(1024, 768));
        final VersionPageTab versionPage = navigateToVersionsPageFor(XSS);

        driver.manage().deleteAllCookies();

        // Move a version to bottom of list
        versionPage.moveVersionBelow(versionPage.getVersionByName(VERSION_3), versionPage.getVersionByName(VERSION_1));
        assertTrue("Expected a prompt as the user is logged out", versionPage.getServerError().contains("You must have global or project administrator rights in order to modify versions."));
    }

    @Test
    @Restore("xml/projectconfig/TestVersionMerge.xml")
    public void testVersionMergeWithNoVersions() {
        final VersionPageTab versionPage = navigateToVersionsPageFor(XSS);
        final PageElement mergeLink = versionPage.getMergeLink();

        assertTrue(mergeLink.isPresent());
        assertFalse(mergeLink.isVisible());
    }

    @Test
    @Restore("xml/projectconfig/TestVersionMerge.xml")
    public void testVersionMergeWithOneOrMoreVersions() {
        backdoor.darkFeatures().enableForSite("ka.KILL_SWITCH");

        // Starts off with:
        // - 1 affectsVersion = version 2,
        // - 1 fixVersion = version 2,
        // - 1 fixVersion = version 2, affectsVersion = version 2,
        // - 1 fixVersion = version 2, version 3, affectsVersion = version 3, version 2

        final VersionPageTab versionPage = navigateToVersionsPageFor(HSP);

        final MergeDialog mergeDialog = versionPage.openMergeDialog();

        assertFalse(mergeDialog.hasErrorMessages());
        assertTrue(mergeDialog.hasWarning());
        assertFalse(mergeDialog.hasNoVersions());

        assertEquals("WARNING: You cannot un-merge these versions once they have merged.",
                mergeDialog.getWarningText());

        mergeDialog.merge(VERSION_1, VERSION_2, VERSION_3)
                .submit();

        waitUntilTrue(mergeDialog.isClosed());

        // Go to issue navigator, ensure the issues tally up
        assertIssueCount("fixVersion=\"" + VERSION_1 + "\"", 3);
        assertIssueCount("affectedVersion=\"" + VERSION_1 + "\"", 3);
        assertJqlError("fixVersion=\"" + VERSION_2 + "\"", "The value 'version 2' does not exist for the field 'fixVersion'.");
        assertJqlError("affectedVersion=\"" + VERSION_2 + "\"", "The value 'version 2' does not exist for the field 'affectedVersion'.");
        assertJqlError("fixVersion=\"" + VERSION_3 + "\"", "The value 'version 3' does not exist for the field 'fixVersion'.");
        assertJqlError("affectedVersion=\"" + VERSION_3 + "\"", "The value 'version 3' does not exist for the field 'affectedVersion'.");
    }

    private void assertIssueCount(final String jql, final long issueCount) {
        final SearchResult search = backdoor.search().getSearch(new SearchRequest().jql(jql));
        assertEquals(issueCount, search.issues.size());
    }

    private void assertJqlError(final String jql, final String error) {
        final Response searchResponse = backdoor.search().getSearchResponse(new SearchRequest().jql(jql));
        assertThat(searchResponse, IsNull.notNullValue());
        assertThat(searchResponse.entity, IsNull.notNullValue());
        assertThat(searchResponse.entity.errorMessages, IsNull.notNullValue());
        assertEquals(error, searchResponse.entity.errorMessages.get(0));
    }

    @Test
    @Restore("xml/projectconfig/TestVersionMerge.xml")
    public void testVersionMergeWithSingleVersion() {
        final VersionPageTab versionPage = navigateToVersionsPageFor(HSP);

        final MergeDialog mergeDialog = versionPage.openMergeDialog();

        mergeDialog
                .merge(VERSION_1, VERSION_1)
                .submit();

        mergeDialog.waitUntilFinishedLoading();

        final List<String> expectedErrorMessages = Lists.newArrayList(
                "You cannot move the issues to the version being deleted."
        );

        assertTrue(mergeDialog.hasErrorMessages());
        assertTrue(mergeDialog.hasWarning());

        assertEquals(expectedErrorMessages, mergeDialog.getErrorMessages());
        assertEquals("WARNING: You cannot un-merge these versions once they have merged.",
                mergeDialog.getWarningText());
    }

    @Test
    @Restore("xml/projectconfig/TestVersionMerge.xml")
    public void testVersionMergeWithServerError() {
        final VersionPageTab versionPage = navigateToVersionsPageFor(HSP);
        final MergeDialog mergeDialog = versionPage.openMergeDialog();
        mergeDialog.merge(VERSION_1, VERSION_2);

        driver.manage().deleteAllCookies();

        mergeDialog.submit();
        waitUntilTrue(mergeDialog.isClosed());

        assertTrue("Expected an error, because anonymous user does not have sufficient permissions.",
                versionPage.getServerError().contains("You must have browse project rights in order to view versions."));

        assertTrue("Should not have deleted the version because it failed", backdoor.project().getVersionsForProject(HSP)
                .stream().anyMatch(version -> version.name.equals(VERSION_2)));
    }

    @Test
    @Restore("xml/projectconfig/DeleteVersionCustomFieldMany.xml")
    public void mergeWithCustomFields() throws InterruptedException {
        final String versionReferredFromCustomFields = "Custom";
        final String newVersion1 = "New Version 1";

        VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);

        final MergeDialog mergeDialog = hspVersionPage.openMergeDialog();
        mergeDialog.merge(newVersion1, versionReferredFromCustomFields)
                .submit();
        waitUntilTrue(mergeDialog.isClosed());

        assertNull(hspVersionPage.getVersionByName(versionReferredFromCustomFields));

        assertVersionSwitchedTo("fixVersion", versionReferredFromCustomFields, newVersion1, "HSP-1");
        assertVersionSwitchedTo("affectedVersion", versionReferredFromCustomFields, newVersion1, "HSP-2");
        assertVersionSwitchedTo("CustomFieldVersion", versionReferredFromCustomFields, newVersion1, "HSP-1", "HSP-2", "HSP-3");
        assertVersionSwitchedTo("CustomVersionSingle", versionReferredFromCustomFields, newVersion1, "HSP-1", "HSP-2");
        assertVersionSwitchedTo("CustomVersionMultiple", versionReferredFromCustomFields, newVersion1, "HSP-3");
    }

    private void compareVersionSortOrder(final List<String> expectedVersionNameOrder, final List<Version> actualVersions) {
        final List<String> actualVersionNames = actualVersions.stream()
                .map(version -> (null == version) ? null : version.getName())
                .collect(Collectors.toList());

        assertThat("The items were out of the expected order", actualVersionNames, allOf(
                is(equalTo(expectedVersionNameOrder)),
                hasSize(expectedVersionNameOrder.size())
        ));
    }

    private String addBaseUrl(final String url) {
        return jira.getProductInstance().getBaseUrl() + url;
    }

    private VersionPageTab navigateToVersionsPageFor(final String projectKey) {
        VersionPageTab versionPageTab = pageBinder.navigateToAndBind(VersionPageTab.class, projectKey);

        //JDEV-33168: Ensure flags are dismissed since they could mess with element visibility.
        pageBinder.bind(AUIFlags.class).closeAllFlags();

        return versionPageTab;
    }

    private ProjectSummaryPageTab navigateToSummaryPageFor(final String projectKey) {
        return pageBinder.navigateToAndBind(ProjectSummaryPageTab.class, projectKey);
    }

    @Test
    public void testReleaseVersionAccpetsTheDefaultDateValue() throws InterruptedException {
        VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);
        final List<Version> versions = hspVersionPage.getVersions();
        final Version versionToRelease = versions.get(0);

        versionToRelease.openOperationsCog().click("Release");

        final ReleaseVersionDialog releaseVersionDialog = pageBinder.bind(ReleaseVersionDialog.class);
        releaseVersionDialog.submit();

        waitUntilTrue(releaseVersionDialog.isClosed());
    }

    @Test
    public void testReleaseVersionAccpetsTheDefaultDateValueWithACustomFormat() throws InterruptedException {
        backdoor.applicationProperties().setString("jira.lf.date.dmy", "yyy-MMM-dd");

        VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);
        final List<Version> versions = hspVersionPage.getVersions();
        final Version versionToRelease = versions.get(0);

        versionToRelease.openOperationsCog().click("Release");

        final ReleaseVersionDialog releaseVersionDialog = pageBinder.bind(ReleaseVersionDialog.class);
        releaseVersionDialog.submit();

        waitUntilTrue(releaseVersionDialog.isClosed());
    }

    @Test
    public void testInfiniteScrolling() {
        removeAllVersions(HSP);

        // create 155 versions
        for (int i = 0; i < 155; i++) {
            backdoor.versions().create(version(HSP, "Version " + i));
        }

        VersionPageTab hspVersionPage = navigateToVersionsPageFor(HSP);

        assertThat(hspVersionPage.getVersions(), hasSize(50)); // only first page is loaded at first

        hspVersionPage.scrollToBottom();

        assertThat(hspVersionPage.getVersions(), hasSize(100)); // next Page has been loaded

        hspVersionPage.scrollToBottom();
        hspVersionPage.scrollToBottom();

        assertThat(hspVersionPage.getVersions(), hasSize(155)); // everything has been loaded
    }

    @Test
    public void testMovingVersionToBottomWhenNotAllPagesHaveBeenLoaded() {
        for (int i = 155; i >= 0; i--) {
            backdoor.versions().create(version(HSP, "Version " + i));
        }

        VersionPageTab versionsPage = navigateToVersionsPageFor(HSP);

        // move first to bottom
        versionsPage.moveVersionBelow(versionsPage.getVersionByName("Version 47"), versionsPage.getVersionByName("Version 49"));
        // to really test this we should ensure that move action in the UI happens before the next page is loaded. Unfortunately
        // there is no easy way to achieve that. So if this test ever fails it doesn't mean it's flakey, it means it's broken,
        // even if it sometimes passes. It should ALWAYS pass.

        List<Version> versionsAfterMove = versionsPage.getVersions();
        assertThat(versionsAfterMove, hasSize(100)); // next Page has been loaded automatically before moving to the bottom of the page
        assertThat(versionsAfterMove.get(47).getName(), equalTo("Version 48"));
        assertThat(versionsAfterMove.get(48).getName(), equalTo("Version 49"));
        assertThat(versionsAfterMove.get(49).getName(), equalTo("Version 47"));
        assertThat(versionsAfterMove.get(50).getName(), equalTo("Version 50"));
    }

    @Test
    public void testLoadingNextPageAfterAddingNewVersion() {
        for (int i = 155; i >= 0; i--) {
            backdoor.versions().create(version(HSP, "Version " + i));
        }

        VersionPageTab versionsPage = navigateToVersionsPageFor(HSP);
        versionsPage.getEditVersionForm().fill("addedVersion", "aa", "").submit();

        versionsPage.scrollToBottom();

        List<Version> versions = versionsPage.getVersions();

        // check that new page is loaded with correct offset
        assertThat(versions.get(49).getName(), equalTo("Version 48"));
        assertThat(versions.get(50).getName(), equalTo("Version 49"));
        assertThat(versions.get(51).getName(), equalTo("Version 50"));
    }

    private void removeAllVersions(String projectKey) {
        for (com.atlassian.jira.testkit.client.restclient.Version version : backdoor.project().getVersionsForProject(projectKey)) {
            backdoor.versions().delete(version.id.toString());
        }
    }

    private com.atlassian.jira.testkit.client.restclient.Version version(String project, String versionName) {
        return new com.atlassian.jira.testkit.client.restclient.Version()
                .name(versionName)
                .project(project);
    }
}
