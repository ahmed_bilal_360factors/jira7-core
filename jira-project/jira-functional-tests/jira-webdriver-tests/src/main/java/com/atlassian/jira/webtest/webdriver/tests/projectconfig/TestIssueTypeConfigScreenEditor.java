package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.components.ScreenEditor;
import com.atlassian.jira.pageobjects.project.issuetypes.IssueTypeFieldsTab;
import com.atlassian.jira.webtest.webdriver.tests.screeneditor.AbstractTestScreenEditorComponent;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.junit.Test;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static org.junit.Assert.assertTrue;

/**
 * @since v6.2
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS, Category.IGNITE})
public class TestIssueTypeConfigScreenEditor extends AbstractTestScreenEditorComponent {
    private static final String PROJECT_ADMIN_USER = "bob";
    private static final String PROJECT_KEY = "BP";
    private static final int SYSTEM_DEFAULT_SCREEN_ISSUE_TYPE_ID = 2;
    private static final String PERSPECTIVE_SWITCHER = "project-issuetypes-perspectives";
    private static final String SCREEN_EDITOR_ID = "screenEditor";
    private static final String READ_ONLY_CLASS = "read-only";

    private static final int ISSUE_TYPE_ID = 10000;

    @Inject
    private PageElementFinder elementFinder;

    @Test
    public void testToggleVisibleIfAdmin() {
        gotoPageForProject(PROJECT_KEY, ISSUE_TYPE_ID);
        assertTrue(elementFinder.find(By.id(PERSPECTIVE_SWITCHER)).isPresent());
    }

    @Test
    public void testToggleVisibleIfProjectAdmin() {
        jira.quickLogin(PROJECT_ADMIN_USER, PROJECT_ADMIN_USER);
        gotoPageForProject(PROJECT_KEY, ISSUE_TYPE_ID);
        assertTrue(elementFinder.find(By.id(PERSPECTIVE_SWITCHER)).isPresent());
    }

    @Test
    public void testReadOnlyIfProjectAdmin() {
        jira.quickLogin(PROJECT_ADMIN_USER, PROJECT_ADMIN_USER);
        gotoPageForProject(PROJECT_KEY, ISSUE_TYPE_ID);
        assertTrue(elementFinder.find(By.id(SCREEN_EDITOR_ID)).hasClass(READ_ONLY_CLASS));
    }

    public ScreenEditor getScreenEditor(String projectKey, int issueTypeId) {
        final IssueTypeFieldsTab screen = gotoPageForProject(projectKey, issueTypeId);
        return screen.getScreenEditor();
    }

    @Override
    public ScreenEditor getScreenEditor() {
        return getScreenEditor("HSP", SYSTEM_DEFAULT_SCREEN_ISSUE_TYPE_ID);
    }

    private IssueTypeFieldsTab gotoPageForProject(final String key, int issueTypeId) {
        return jira.goTo(IssueTypeFieldsTab.class, key, issueTypeId);
    }
}
