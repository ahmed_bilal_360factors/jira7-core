package com.atlassian.jira.webtest.webdriver.tests.reports;

import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.google.inject.Inject;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.MatcherAssert.assertThat;

@WebTest({Category.WEBDRIVER_TEST, Category.REPORTS})
@RestoreBlankInstance
public class TestConfigureReports extends BaseJiraWebTest {

    @Inject
    WebDriver webDriver;

    @Test
    public void requestsWithOldReportKeysAreRedirectedWithUpdatedKeys() {
        goToConfigureReport("10000", "com.atlassian.jira.plugin.system.reports:singlelevelgroupby");

        assertThat(webDriver.getCurrentUrl(), endsWith("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin%3Asinglelevelgroupby"));
    }

    private void goToConfigureReport(String projectId, String reportKey) {
        String url = String.format(
                "%s/secure/ConfigureReport!default.jspa?selectedProjectId=%s&reportKey=%s",
                jira.getProductInstance().getBaseUrl(), projectId, reportKey
        );
        webDriver.navigate().to(url);
    }

}
