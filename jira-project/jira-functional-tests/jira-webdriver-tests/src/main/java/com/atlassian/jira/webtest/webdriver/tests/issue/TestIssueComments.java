package com.atlassian.jira.webtest.webdriver.tests.issue;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.matcher.MimeMessageMatchers;
import com.atlassian.jira.functest.rule.MailTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.SecurityLevelSelect;
import com.atlassian.jira.pageobjects.config.BlankInstance;
import com.atlassian.jira.pageobjects.config.DisableRTE;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.config.ResetData;
import com.atlassian.jira.pageobjects.dialogs.CommentDialog;
import com.atlassian.jira.pageobjects.dialogs.quickedit.EditIssueDialog;
import com.atlassian.jira.pageobjects.email.NotificationMessage;
import com.atlassian.jira.pageobjects.pages.viewissue.ActionTrigger;
import com.atlassian.jira.pageobjects.pages.viewissue.AddCommentSection;
import com.atlassian.jira.pageobjects.pages.viewissue.EditCommentDialog;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.beans.PermissionSchemeBean;
import com.atlassian.jira.testkit.client.restclient.Comment;
import com.atlassian.jira.testkit.client.restclient.ProjectRole;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.google.common.collect.Iterables;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Test adding comments on the view issue page
 *
 * @since v4.4
 */
@ResetData
@WebTest({Category.WEBDRIVER_TEST, Category.ISSUES})
public class TestIssueComments extends BaseJiraWebTest {
    private static final long PROJECT_ID = 10001L;
    public static final int CHARACTER_LIMIT_VALUE = 10;
    public static final String TEXT_TOO_LONG_ERROR_MESSAGE = "The entered text is too long. It exceeds the allowed limit of 10 characters.";
    private static final String SHORT_COMMENT = "all ok";
    private static final String TOO_LONG_COMMENT = "This is too long comment";
    public static final String TOO_LONG_MULTILINE_COMMENT = "123456789\n";
    private static final String SECURITY_LEVEL_VISIBLE_BY_ALL_LIST_ID = "all-users";
    private static final String SECURITY_LEVEL_ADMINISTRATORS_LIST_ID = "jira-administrators";
    private static final String SECURITY_LEVEL_DEVELOPERS_LIST_ID = "jira-developers";
    private static final String SECURITY_LEVEL_VISIBLE_BY_ALL_TEXT = "All Users";
    private static final String SECURITY_LEVEL_ADMINISTRATORS_TEXT = "jira-administrators";
    private static final String SECURITY_LEVEL_DEVELOPERS_TEXT = "jira-developers";

    @Inject
    private PageElementFinder pageElementFinder;

    /**
     * JRADEV-8225: adding a mention clears the rest of the line
     */
    @Test
    public void testMentionDoesNotClearLine() {
        final IssueCreateResponse issueCreateResponse = backdoor.issues().createIssue(PROJECT_ID, "Monkeys everywhere :(");

        final ViewIssuePage viewIssuePage = jira.goToViewIssue(issueCreateResponse.key());
        final AddCommentSection commentSection = viewIssuePage.comment();
        final String line1 = "This is the remainder of the line\n";
        final String line2 = "This is the next line";
        commentSection.typeComment(line1 + line2).typeComment(Keys.UP);
        //go to the beginning of the line. Keys.HOME doesn't work on all platforms :(
        for (int i = 0; i < line1.length(); i++) {
            commentSection.typeComment(Keys.LEFT);
        }
        final String comment = commentSection.typeComment("@fred").selectMention("fred").getComment();

        assertEquals(comment, "[~fred]" + line1 + line2);

        //add the comment here to make sure the next test that runs avoids the dirty form warning.
        commentSection.addAndWait();
    }

    @Test
    public void testCommentExceedsLimit() {
        backdoor.advancedSettings().setTextFieldCharacterLengthLimit(10);
        final IssueCreateResponse issueCreateResponse = backdoor.issues().createIssue(PROJECT_ID, "Monkeys everywhere :(");

        final AddCommentSection commentSection = jira.goToViewIssue(issueCreateResponse.key()).comment();
        final String errors = commentSection.typeComment(TOO_LONG_COMMENT).addWithErrors();
        assertEquals(TEXT_TOO_LONG_ERROR_MESSAGE, errors);
        commentSection.closeErrors().cancel();
    }

    @Test
    public void testMultilineCommentExceedsLimit() {
        backdoor.advancedSettings().setTextFieldCharacterLengthLimit(CHARACTER_LIMIT_VALUE);
        final IssueCreateResponse issueCreateResponse = backdoor.issues().createIssue(PROJECT_ID, "Monkeys everywhere :(");

        final AddCommentSection commentSection = jira.goToViewIssue(issueCreateResponse.key()).comment();
        final String errors = commentSection.typeComment(TOO_LONG_MULTILINE_COMMENT).addWithErrors();
        // regular error message and not a "communications breakdown"
        assertEquals(TEXT_TOO_LONG_ERROR_MESSAGE, errors);
        commentSection.closeErrors().cancel();
    }

    @Test
    public void testMultilineCommentWhileEditingIssueExceedsLimit() {
        backdoor.advancedSettings().setTextFieldCharacterLengthLimit(10);
        final IssueCreateResponse issueCreateResponse = backdoor.issues().createIssue(PROJECT_ID, "Monkeys everywhere :(");

        final EditIssueDialog editIssueDialog = jira.goToViewIssue(issueCreateResponse.key()).editIssue();
        editIssueDialog.setComment(TOO_LONG_MULTILINE_COMMENT);
        boolean open = editIssueDialog.submit();

        List<String> formErrorList = editIssueDialog.getFormErrorList();
        assertThat(formErrorList, contains(equalToIgnoringCase(TEXT_TOO_LONG_ERROR_MESSAGE)));
        editIssueDialog.escape();
    }

    @Test
    public void testCommentShorterThanLimit() {
        backdoor.advancedSettings().setTextFieldCharacterLengthLimit(10);
        final IssueCreateResponse issueCreateResponse = backdoor.issues().createIssue(PROJECT_ID, "Monkeys everywhere :(");

        final AddCommentSection commentSection = jira.goToViewIssue(issueCreateResponse.key()).comment();
        commentSection.typeComment(SHORT_COMMENT).addAndWait();

        final List<Comment> comments = backdoor.issues().getIssue(issueCreateResponse.key()).getComments();
        final Comment comment = Iterables.getOnlyElement(comments);
        assertEquals(SHORT_COMMENT, comment.body);
    }

    @Test
    @DisableRTE
    public void testCommentNotInPreviewModeAfterCanel() {
        backdoor.fieldConfiguration().setFieldRenderer("Default Field Configuration", "comment", "Wiki Style Renderer");
        final IssueCreateResponse issueCreateResponse = backdoor.issues().createIssue(PROJECT_ID, "Monkeys everywhere :(");
        final AddCommentSection commentSection = jira.goToViewIssue(issueCreateResponse.key()).comment();

        commentSection
                .typeComment(SHORT_COMMENT)
                .previewMode()
                .cancel()
                .comment();

        assertFalse("The comment is not in preview mode", commentSection.isInPreviewMode().byDefaultTimeout());
    }

    @Test
    public void testOlderCommentsAreExpandingWithoutPageReload() {
        final IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_ID, "Issue with a loooot of comments");
        generateComments(issue, 30);

        final ViewIssuePage issuePage = jira.visit(ViewIssuePage.class, issue.key);
        final String requestIdBeforeExpand = getRequestId();

        issuePage.expandHiddenComments();

        Iterable<com.atlassian.jira.pageobjects.pages.viewissue.link.activity.Comment> comments = issuePage.getComments();

        final String requestIdAfterExpand = getRequestId();

        assertThat(requestIdAfterExpand, equalTo(requestIdBeforeExpand));
        assertThat(comments, Matchers.<com.atlassian.jira.pageobjects.pages.viewissue.link.activity.Comment>iterableWithSize(30));
    }

    @Test
    public void testSecurityLevelIsSetToDefaultLevelAfterSettingItWithLinkAndRefreshingPage() {
        backdoor.getTestkit().darkFeatures().enableForSite("viewissue.comment.defaultlevel");

        final IssueCreateResponse issueCreateResponse = backdoor.getTestkit().issues().createIssue(BlankInstance.PROJECT_KEY, "Wow, such issue!");

        final AddCommentSection commentSection = jira.goToViewIssue(issueCreateResponse.key()).comment();
        final SecurityLevelSelect securityControl = commentSection.getSecurityLevelControl();

        securityControl.waitUntilDefaultCommentLevelLoaded();

        assertThat("Security level should be set to 'Visible by All'",
                securityControl.getSelectedSecurityLevelText(), containsString(SECURITY_LEVEL_VISIBLE_BY_ALL_TEXT));

        securityControl.getSelect().pickOption(SECURITY_LEVEL_ADMINISTRATORS_LIST_ID);

        final String result = securityControl.setDefaultSecurityLevel();
        assertThat("Result should be successful", result, equalTo("success"));

        securityControl.ensureThatLinkToSetDefaultSecurityLevelIsNotPresent();

        // reload page
        final AddCommentSection commentSectionAfterReload = jira.goToViewIssue(issueCreateResponse.key()).comment();
        final SecurityLevelSelect securityControlAfterReload = commentSectionAfterReload.getSecurityLevelControl();

        securityControlAfterReload.waitUntilDefaultCommentLevelLoaded();

        assertThat("Security level should be set to 'administrators'",
                securityControlAfterReload.getSelectedSecurityLevelText(), containsString(SECURITY_LEVEL_ADMINISTRATORS_TEXT));
    }

    @Test
    public void testCantPostCommentWithOutdatedDefaultSecurityLevel() {
        backdoor.getTestkit().darkFeatures().enableForSite("viewissue.comment.defaultlevel");

        final ProjectRole projectRole = backdoor.getTestkit().roleClient().create("do not remove me", "Seriously, don't remove me ;_;");
        backdoor.getTestkit().projectRole().addActors(BlankInstance.PROJECT_KEY, projectRole.getName(), null, new String[] {"admin"});

        final IssueCreateResponse issueCreateResponse = backdoor.issues().createIssue(BlankInstance.PROJECT_KEY, "Wow, such issue!");

        final AddCommentSection commentSection = jira.goToViewIssue(issueCreateResponse.key()).comment();
        final SecurityLevelSelect securityControl = commentSection.getSecurityLevelControl();

        securityControl.waitUntilDefaultCommentLevelLoaded();

        final String roleListId = projectRole.getName().replace(" ", "-");
        securityControl.getSelect().pickOption(roleListId);

        final String result = securityControl.setDefaultSecurityLevel();
        assertThat("Result should be successful", result, equalTo("success"));

        backdoor.getTestkit().roleClient().deleteProjectRole(projectRole.getId());

        // reload page
        final AddCommentSection commentSectionAfterReload = jira.goToViewIssue(issueCreateResponse.key()).comment();
        final SecurityLevelSelect securityControlAfterReload = commentSectionAfterReload.getSecurityLevelControl();

        securityControlAfterReload.waitUntilDefaultCommentLevelLoaded();

        assertThat("Security level should be 'unavailable'",
                commentSectionAfterReload.getSecurityLevelError(), containsString("unavailable"));

        assertFalse("Should not be able to post comment when incorrect security level is set", commentSectionAfterReload.isAddCommentPossible());

        commentSectionAfterReload.typeComment("Need to change level to submit!");

        assertFalse("Should not be able to post comment even after typeing some text with incorrect security level set",
                commentSectionAfterReload.isAddCommentPossible());

        securityControlAfterReload.getSelect().pickOption(SECURITY_LEVEL_VISIBLE_BY_ALL_LIST_ID);

        assertTrue("Should be able to post comment after switching security level", commentSectionAfterReload.isAddCommentPossible());
    }

    @Test
    public void testDefaultCommentSecurityLevelFeatureMakesNoChangeInEditComment() {
        backdoor.getTestkit().darkFeatures().enableForSite("viewissue.comment.defaultlevel");

        final PermissionSchemeBean projectPermissionScheme =
                backdoor.getTestkit().project().getSchemes(BlankInstance.PROJECT_KEY).permissionScheme;
        backdoor.getTestkit().permissionSchemes()
                .addUserPermission(projectPermissionScheme.getId(), ProjectPermissions.EDIT_ALL_COMMENTS, "admin");

        final IssueCreateResponse issueCreateResponse = backdoor.getTestkit().issues().createIssue(BlankInstance.PROJECT_KEY, "Wow, such issue!");

        final ViewIssuePage viewIssuePage = jira.goToViewIssue(issueCreateResponse.key());
        final AddCommentSection commentSection = viewIssuePage.comment();
        final SecurityLevelSelect securityControl = commentSection.getSecurityLevelControl();

        securityControl.waitUntilDefaultCommentLevelLoaded();

        securityControl.getSelect().pickOption(SECURITY_LEVEL_ADMINISTRATORS_LIST_ID);
        securityControl.setDefaultSecurityLevel();

        securityControl.getSelect().pickOption(SECURITY_LEVEL_DEVELOPERS_LIST_ID);

        commentSection.typeComment("I will edit this comment");
        commentSection.addAndWait();

        final long commentId = viewIssuePage.getComments().iterator().next().getId();
        final EditCommentDialog editForm = viewIssuePage.editComment(commentId);

        final SecurityLevelSelect editSecurityControl = editForm.getSecurityLevelControl();

        editSecurityControl.ensureDefaultIsDisabled();

        assertThat("Security level should be 'developers'",
                editSecurityControl.getSelectedSecurityLevelText(), containsString(SECURITY_LEVEL_DEVELOPERS_TEXT));

        editSecurityControl.getSelect().pickOption(SECURITY_LEVEL_ADMINISTRATORS_LIST_ID);

        editSecurityControl.ensureDefaultIsDisabled();

        assertThat("Security level should be 'administrators'",
                editSecurityControl.getSelectedSecurityLevelText(), containsString(SECURITY_LEVEL_ADMINISTRATORS_TEXT));
    }

    @Test
    public void testDefaultCommentSecurityLevelStaysAfterErrorWhenSubmitFromCommentDialog() {
        backdoor.getTestkit().darkFeatures().enableForSite("viewissue.comment.defaultlevel");

        final IssueCreateResponse issueCreateResponse = backdoor.getTestkit().issues().createIssue(BlankInstance.PROJECT_KEY, "Wow, such issue!");

        final CommentDialog commentDialog = jira.goToViewIssue(issueCreateResponse.key()).commentInDialog();
        SecurityLevelSelect securityControl = commentDialog.getSecurityLevelControl();

        securityControl.getSelect().pickOption(SECURITY_LEVEL_ADMINISTRATORS_LIST_ID);

        assertThat("Security level should be 'administrators'",
                securityControl.getSelectedSecurityLevelText(), containsString(SECURITY_LEVEL_ADMINISTRATORS_TEXT));

        commentDialog.submit();

        // hook to a new select control - dialog stays
        securityControl = commentDialog.getSecurityLevelControl();
        securityControl.waitUntilDefaultCommentLevelLoaded();

        assertThat("comment dialog should come back with error and not changed security level",
                securityControl.getSelectedSecurityLevelText(), containsString(SECURITY_LEVEL_ADMINISTRATORS_TEXT));
    }

    @Test
    public void testOutdatedDefaultCommentSecurityLevelStaysAfterErrorWhenSubmitFromCommentDialog() {
        backdoor.getTestkit().darkFeatures().enableForSite("viewissue.comment.defaultlevel");

        final ProjectRole projectRole = backdoor.getTestkit().roleClient().create("do not remove me", "Seriously, don't remove me ;_;");
        backdoor.getTestkit().projectRole().addActors(BlankInstance.PROJECT_KEY, projectRole.getName(), null, new String[] {"admin"});

        final IssueCreateResponse issueCreateResponse = backdoor.issues().createIssue(BlankInstance.PROJECT_KEY, "Wow, such issue!");

        final AddCommentSection commentSection = jira.goToViewIssue(issueCreateResponse.key()).comment();
        final SecurityLevelSelect securityControl = commentSection.getSecurityLevelControl();

        securityControl.waitUntilDefaultCommentLevelLoaded();

        final String roleListId = projectRole.getName().replace(" ", "-");
        securityControl.getSelect().pickOption(roleListId);

        final String result = securityControl.setDefaultSecurityLevel();
        assertThat("Result should be successful", result, equalTo("success"));

        backdoor.getTestkit().roleClient().deleteProjectRole(projectRole.getId());

        // now project has outdated default

        final CommentDialog commentDialog = jira.goToViewIssue(issueCreateResponse.key()).commentInDialog();
        SecurityLevelSelect securityControlWithInvalidLevel = commentDialog.getSecurityLevelControl();
        securityControlWithInvalidLevel.waitUntilDefaultCommentLevelLoaded();

        assertThat("Security level should be 'unavailable'",
                commentDialog.getSecurityLevelError(), containsString("unavailable"));

        commentDialog.submit();

        // hook to a new select control - dialog stays
        securityControlWithInvalidLevel = commentDialog.getSecurityLevelControl();
        securityControlWithInvalidLevel.waitUntilDefaultCommentLevelLoaded();

        assertThat("Security level should still be 'unavailable'",
                commentDialog.getSecurityLevelError(), containsString("unavailable"));
    }

    @Test
    @MailTest
    @LoginAs(user = "fred")
    public void testCommentNotifiesAssignee() throws MessagingException {
        backdoor.getTestkit().project().setNotificationScheme(PROJECT_ID, 10000l);
        final IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_ID, "Issue with a loooot of comments", "admin");

        final ViewIssuePage issuePage = jira.visit(ViewIssuePage.class, issue.key);
        issuePage.addComment("comment", ActionTrigger.KEYBOARD_SHORTCUT);

        final MimeMessage mimeMessage = mailHelper.flushMailQueueAndWait(1).stream().findFirst().get();

        assertThat(mimeMessage, MimeMessageMatchers.withSubjectEqualTo("[JIRATEST] (MKY-1) Issue with a loooot of comments"));

        NotificationMessage notificationMessage = jira.visit(NotificationMessage.class, mailHelper.getPreviewUrl(mimeMessage));
        assertEquals("Re: Issue with a loooot of comments", notificationMessage.getPageTitle());
    }

    private void generateComments(IssueCreateResponse issue, int n) {
        for (int i = 0; i < n; i++) {
            backdoor.issues().commentIssue(issue.key, "comment no " + i);
        }
    }

    private String getRequestId() {
        return pageElementFinder.find(By.id("jira_request_timing_info")).find(By.cssSelector("input[title=\"jira.request.id\"]")).getValue();
    }

}
