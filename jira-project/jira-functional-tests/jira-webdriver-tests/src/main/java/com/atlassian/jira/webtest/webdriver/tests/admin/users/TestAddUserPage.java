package com.atlassian.jira.webtest.webdriver.tests.admin.users;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.backdoor.ApplicationRoleControl;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.EnableAUIFlags;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.pages.admin.user.AddUserPage;
import com.atlassian.jira.pageobjects.pages.admin.user.UserBrowserPage;
import com.atlassian.jira.test.util.lic.ApplicationLicenseConstants;
import com.atlassian.jira.testkit.beans.ApplicationRole;
import com.atlassian.jira.testkit.client.restclient.User;
import com.atlassian.jira.testkit.client.restclient.UserClient;
import com.atlassian.jira.webtests.LicenseKeys;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@WebTest(com.atlassian.jira.functest.framework.suite.Category.WEBDRIVER_TEST)
@EnableAUIFlags
public class TestAddUserPage extends BaseJiraWebTest {

    private static final String ROLE_NAME_CORE = "JIRA Core";
    private static final String ROLE_NAME_TEST = "Test Product";
    private static final String ROLE_NAME_SOFTWARE = "JIRA Software";

    private String admin;
    private ApplicationRoleControl roleClient;
    private UserClient userClient;

    @Before
    public void setUp() throws Exception {
        admin = jira.getAdminCredentials().getUsername();
        roleClient = backdoor.applicationRoles();
        userClient = new UserClient(jira.environmentData());
    }

    @Test
    @LoginAs(admin = true, targetPage = AddUserPage.class)
    public void testErrorMessagesPresent() throws Exception {
        AddUserPage page = pageBinder.bind(AddUserPage.class);

        page.addUser(admin, "password", "", "", false);
        page = page.createUserExpectingError();

        final Map<String, String> pageErrors = page.getPageErrors();

        assertThat(txtByKey(pageErrors, "user-create-username-error"), is("A user with that username already exists."));

        assertThat(txtByKey(pageErrors, "user-create-fullname-error"), is("You must specify a full name."));
        assertThat(txtByKey(pageErrors, "user-create-email-error"), is("You must specify an email address."));
    }

    @Test
    @LoginAs(admin = true, targetPage = UserBrowserPage.class)
    public void testDefaultApplicationsSelected() throws Exception {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_AND_TEST_ROLES.getLicenseString());
        ApplicationRoleControl roleClient = backdoor.applicationRoles();

        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.CORE_KEY, true, ImmutableList.<String>of("jira-users"), ImmutableList.<String>of("jira-users"));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, false, ImmutableList.<String>of("jira-developers"), ImmutableList.<String>of("jira-developers"));

        AddUserPage page = jira.goTo(AddUserPage.class);

        assertThat(page.getSelectedApplications(), contains(ApplicationLicenseConstants.CORE_KEY));

        page.addUser("freddie", "password", "Fred", "freddie@fred", false);
        page.createUser();

        assertThat(getUserRoles("freddie"), contains(roleWithKeyAndName(ApplicationLicenseConstants.CORE_KEY, ROLE_NAME_CORE)));
    }

    @Test
    @LoginAs(admin = true, targetPage = UserBrowserPage.class)
    public void testDefaultApplicationsSelectedPersisted() throws Exception {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_AND_TEST_ROLES.getLicenseString());

        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.CORE_KEY, true, ImmutableList.<String>of("jira-users"), ImmutableList.<String>of("jira-users"));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, false, ImmutableList.<String>of("jira-developers"), ImmutableList.<String>of("jira-developers"));

        AddUserPage page = jira.goTo(AddUserPage.class);

        assertThat(page.getSelectedApplications(), contains(ApplicationLicenseConstants.CORE_KEY));
        // select only TEST app
        page.setSelectedApplications(ApplicationLicenseConstants.TEST_KEY);

        page.addUser("freddie", "password", "Fred", "freddie@fred", true);
        page = (AddUserPage) page.createUser(true);

        assertThat(getUserRoles("freddie"), contains(roleWithKeyAndName(ApplicationLicenseConstants.TEST_KEY, ROLE_NAME_TEST)));

        assertThat(page.getSelectedApplications(), contains(ApplicationLicenseConstants.TEST_KEY, ApplicationLicenseConstants.CORE_KEY));
        assertTrue("sendEmail persisted", page.getSendEmail());
        assertTrue("createAnother persisted", page.getCreateAnother());

        page.addUser("freddie2", "password2", "Fred2", "freddie2@fred", false);
        page.setSelectedApplications();
        page = (AddUserPage) page.createUser(true);

        assertThat(page.getSelectedApplications(), empty());
        assertFalse("sendEmail persisted", page.getSendEmail());
    }

    @Test
    @LoginAs(admin = true, targetPage = UserBrowserPage.class)
    public void testJiraCoreCheckedEvenThoughItIsNotDefault() throws Exception {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_AND_TEST_ROLES.getLicenseString());
        ApplicationRoleControl roleClient = backdoor.applicationRoles();

        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.CORE_KEY, false, ImmutableList.<String>of("jira-users"), ImmutableList.<String>of("jira-users"));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, true, ImmutableList.<String>of("jira-developers"), ImmutableList.<String>of("jira-developers"));

        AddUserPage page = jira.goTo(AddUserPage.class);

        assertThat(page.getSelectedApplications(), containsInAnyOrder(ApplicationLicenseConstants.CORE_KEY, ApplicationLicenseConstants.TEST_KEY));

        page.addUser("freddie", "password", "Fred", "freddie@fred", false);
        page.createUser();

        assertThat(getUserRoles("freddie"), contains(roleWithKeyAndName(ApplicationLicenseConstants.TEST_KEY, ROLE_NAME_TEST)));
    }

    @Test
    @LoginAs(admin = true, targetPage = UserBrowserPage.class)
    public void testAppSelectionPersistsWhenAccessError() throws Exception {
        backdoor.restoreBlankInstance();
        backdoor.license().set(LicenseKeys.MULTI_ROLE.getLicenseString());
        ApplicationRoleControl roleClient = backdoor.applicationRoles();

        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.CORE_KEY, false,
                ImmutableList.of("jira-users"), ImmutableList.of("jira-users"));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, true,
                ImmutableList.of("jira-developers"), ImmutableList.of());
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.SOFTWARE_KEY, true,
                ImmutableList.of("jira-administrators"), ImmutableList.of("jira-administrators"));

        AddUserPage page = jira.goTo(AddUserPage.class);

        assertThat(page.getSelectedApplications(),
                containsInAnyOrder(ApplicationLicenseConstants.CORE_KEY, ApplicationLicenseConstants.SOFTWARE_KEY));
        assertEquals(Lists.newArrayList(ApplicationLicenseConstants.TEST_KEY), page.getApplicationsWithWarning());

        page = page.addUser(admin, "password", "", "", false);
        page = page.createUserExpectingError();

        // those applications are still selected
        assertThat(page.getSelectedApplications(),
                containsInAnyOrder(ApplicationLicenseConstants.CORE_KEY, ApplicationLicenseConstants.SOFTWARE_KEY));

        // eventually add the user

        page.addUser("freddie", "password", "Fred", "freddie@fred", false);
        page.createUser();

        assertThat(getUserRoles("freddie"), contains(roleWithKeyAndName(ApplicationLicenseConstants.SOFTWARE_KEY, ROLE_NAME_SOFTWARE)));
    }


    @Test
    @LoginAs(admin = true, targetPage = UserBrowserPage.class)
    public void testApplicationSelectedBasedOnUrlParameter() throws Exception {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_AND_TEST_ROLES.getLicenseString());
        ApplicationRoleControl roleClient = backdoor.applicationRoles();

        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.CORE_KEY, false, ImmutableList.<String>of("jira-users"), ImmutableList.<String>of("jira-users"));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, false, ImmutableList.<String>of("jira-developers"), ImmutableList.<String>of("jira-developers"));

        AddUserPage page = jira.goTo(AddUserPage.class, ApplicationLicenseConstants.CORE_KEY);

        assertThat(page.getSelectedApplications(), contains(ApplicationLicenseConstants.CORE_KEY));

        page.addUser("freddie", "password", "Fred", "freddie@fred", false);
        page.createUser();

        assertThat(getUserRoles("freddie"), contains(roleWithKeyAndName(ApplicationLicenseConstants.CORE_KEY, ROLE_NAME_CORE)));
    }


    @Test
    @LoginAs(admin = true, targetPage = UserBrowserPage.class)
    public void testMultipleApplicationsSelectedBasedOnUrlParameter() throws Exception {
        backdoor.restoreBlankInstance(LicenseKeys.MULTI_ROLE.getLicenseString());
        ApplicationRoleControl roleClient = backdoor.applicationRoles();

        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.CORE_KEY, false, ImmutableList.<String>of("jira-users"), ImmutableList.<String>of("jira-users"));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, false, ImmutableList.<String>of("jira-developers"), ImmutableList.<String>of("jira-developers"));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.SOFTWARE_KEY, true, ImmutableList.<String>of("jira-administrators"), ImmutableList.<String>of("jira-administrators"));

        AddUserPage page = jira.goTo(AddUserPage.class, ApplicationLicenseConstants.SOFTWARE_KEY, ApplicationLicenseConstants.TEST_KEY);

        assertThat(page.getSelectedApplications(), containsInAnyOrder(ApplicationLicenseConstants.CORE_KEY, ApplicationLicenseConstants.SOFTWARE_KEY, ApplicationLicenseConstants.TEST_KEY));

        page.addUser("freddie", "password", "Fred", "freddie@fred", false);
        page.createUser();

        assertThat(getUserRoles("freddie"), contains(
                roleWithKeyAndName(ApplicationLicenseConstants.SOFTWARE_KEY, ROLE_NAME_SOFTWARE),
                roleWithKeyAndName(ApplicationLicenseConstants.TEST_KEY, ROLE_NAME_TEST)));
    }


    @Test
    @LoginAs(admin = true, targetPage = UserBrowserPage.class)
    public void testApplicationSelectedBasedOnUrlParameterOverrideDefaults() throws Exception {
        backdoor.restoreBlankInstance(LicenseKeys.MULTI_ROLE.getLicenseString());
        ApplicationRoleControl roleClient = backdoor.applicationRoles();

        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.CORE_KEY, false, ImmutableList.<String>of("jira-users"), ImmutableList.<String>of("jira-users"));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, true, ImmutableList.<String>of("jira-developers"), ImmutableList.<String>of("jira-developers"));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.SOFTWARE_KEY, true, ImmutableList.<String>of("jira-administrators"), ImmutableList.<String>of("jira-administrators"));

        AddUserPage page = jira.goTo(AddUserPage.class, ApplicationLicenseConstants.CORE_KEY);

        assertThat(page.getSelectedApplications(), contains(ApplicationLicenseConstants.CORE_KEY));

        page.addUser("freddie", "password", "Fred", "freddie@fred", false);
        page.createUser();

        assertThat(getUserRoles("freddie"), contains(roleWithKeyAndName(ApplicationLicenseConstants.CORE_KEY, ROLE_NAME_CORE)));
    }

    @Test
    @LoginAs(admin = true, targetPage = AddUserPage.class)
    public void shouldDisplayUserBrowserPageWithFlagAfterUserCreation() {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_ROLE);
        backdoor.flags().enableFlags();

        final AddUserPage page = jira.goTo(AddUserPage.class);
        final UserBrowserPage userBrowserPage = page.addUser("user1").createUser();

        waitUntilTrue("Flag should appear on UserBrowserPage after creating an user", userBrowserPage.isUserCreatedFlagVisible("user1"));
    }

    @Test
    @LoginAs(admin = true, targetPage = AddUserPage.class)
    public void shouldDisplayUserCreatedFlagIfUserWasCreatedAndAnotherWasCancelled() {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_ROLE);
        backdoor.flags().enableFlags();

        final AddUserPage page = jira.goTo(AddUserPage.class);
        final UserBrowserPage userBrowserPage = page.addUser("kevin").createAnotherUser().cancelCreateUser();

        waitUntilTrue(userBrowserPage.isUserCreatedFlagVisible("kevin"));
    }

    @Test
    public void shouldDisplayMultipleUsersCreatedFlagAfterUserCreation() {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_ROLE);
        backdoor.flags().enableFlags();
        jira.quickLoginAsAdmin(AddUserPage.class);


        final AddUserPage page = jira.goTo(AddUserPage.class);
        // flag should be visible also when validation fail during creation of multiple users
        final UserBrowserPage userBrowserPage = page.addUser("user2").createAnotherUser().addUser(admin, "password", "", "", false).createUserExpectingError().addUser("user3").createUser();
        waitUntilTrue("Flag should appear on UserBrowserPage after creating multiple when an error occurred users", userBrowserPage.isMultipleUsersCreatedFlagVisible());
    }

    @Test
    @LoginAs(admin = true, targetPage = AddUserPage.class)
    public void shouldPutCreatedUsersOnTopAfterUserCreation() {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_ROLE);
        final AddUserPage page = jira.goTo(AddUserPage.class);

        final UserBrowserPage userBrowserPage = page.addUser("user2").createAnotherUser().addUser(admin, "password", "", "", false).createUserExpectingError().addUser("user3").createUser();

        assertThat(userBrowserPage.getUserNames(), contains("user2", "user3", "admin", "fred"));
        assertThat(userBrowserPage.getUserNames(true), contains("user2", "user3"));
    }

    @Test
    @LoginAs(admin = true, targetPage = UserBrowserPage.class)
    public void testApplicationSelectedEffectively() throws Exception {
        backdoor.restoreBlankInstance(LicenseKeys.MULTI_ROLE.getLicenseString());
        ApplicationRoleControl roleClient = backdoor.applicationRoles();

        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.CORE_KEY, true, ImmutableList.<String>of("jira-users", "jira-developers"), ImmutableList.<String>of("jira-users", "jira-developers"));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, false, ImmutableList.<String>of("jira-developers", "jira-administrators"), ImmutableList.<String>of("jira-developers", "jira-administrators"));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.SOFTWARE_KEY, false, ImmutableList.<String>of("jira-administrators"), ImmutableList.<String>of("jira-administrators"));

        AddUserPage page = jira.goTo(AddUserPage.class, ApplicationLicenseConstants.CORE_KEY);

        assertThat("Core is selected", page.getSelectedApplications(), contains(ApplicationLicenseConstants.CORE_KEY));
        assertThat("Test Product is effectively selected", page.getEffectiveApplications(), contains(ApplicationLicenseConstants.TEST_KEY));
        waitUntilTrue("Effective warning trigger on Test Product is visible", page.getEffectiveWarning(ApplicationLicenseConstants.TEST_KEY).timed().isVisible());

        page.setSelectedApplications();

        assertThat("No application selected", page.getSelectedApplications(), is(empty()));
        assertThat("No application effectively selected", page.getEffectiveApplications(), is(empty()));
        waitUntilFalse("Effective warning trigger on Test Product is hidden", page.getEffectiveWarning(ApplicationLicenseConstants.TEST_KEY).timed().isVisible());

        page.setSelectedApplications(ApplicationLicenseConstants.TEST_KEY);

        assertThat("Test Product and Core are selected", page.getSelectedApplications(), contains(ApplicationLicenseConstants.TEST_KEY, ApplicationLicenseConstants.CORE_KEY));
        assertThat("Software is effectively selected", page.getEffectiveApplications(), contains(ApplicationLicenseConstants.SOFTWARE_KEY));
        waitUntilTrue("Effective warning trigger on Software is visible", page.getEffectiveWarning(ApplicationLicenseConstants.SOFTWARE_KEY).timed().isVisible());
    }

    @Test
    @LoginAs(admin = true)
    public void shouldDisplayEffectiveWarningOnCriticalWarningApps() {
        try {
            backdoor.restoreBlankInstance(LicenseKeys.CORE_AND_TEST_ROLES);
            backdoor.userProfile().changeUserLanguage(FunctTestConstants.ADMIN_USERNAME, FunctTestConstants.MOON_LOCALE);

            roleClient.putRole(ApplicationLicenseConstants.TEST_KEY, "jira-users");
            roleClient.putRoleWithDefaults(ApplicationLicenseConstants.CORE_KEY, ImmutableList.of("jira-users"), ImmutableList.of("jira-users"));

            final AddUserPage page = jira.goTo(AddUserPage.class, ApplicationLicenseConstants.CORE_KEY);

            waitUntilEquals("Effective warning on JIRA Test is visible", backdoor.i18n().getText("admin.warn.user.shared.default.group.other.error", FunctTestConstants.MOON_LOCALE),
                    page.getEffectiveCriticalWarning(ApplicationLicenseConstants.TEST_KEY).timed().getText());
            waitUntilTrue("JIRA Test should be effective", page.getApplicationAccess().isCriticalApplicationEffective(ApplicationLicenseConstants.TEST_KEY));
        } finally {
            backdoor.userProfile().changeUserLanguage(FunctTestConstants.ADMIN_USERNAME, "");
        }
    }

    @Test
    @LoginAs(admin = true)
    public void shouldNotSelectApplicationsEffectivelySelectedByDisabledCore() {
        final String SOFTWARE_CORE_GROUP = "software-core";
        final String SERVICE_DESK_GROUP = "service-desk";

        backdoor.restoreBlankInstance(LicenseKeys.MULTI_ROLE);

        backdoor.usersAndGroups().addGroup(SOFTWARE_CORE_GROUP);
        backdoor.usersAndGroups().addGroup(SERVICE_DESK_GROUP);

        roleClient.putRoleAndSetDefault(ApplicationLicenseConstants.CORE_KEY, SOFTWARE_CORE_GROUP);
        roleClient.putRoleAndSetDefault(ApplicationLicenseConstants.SOFTWARE_KEY, SOFTWARE_CORE_GROUP);
        roleClient.putRoleAndSetDefault(ApplicationLicenseConstants.SERVICE_DESK_KEY, SERVICE_DESK_GROUP);

        final AddUserPage addUserPage = jira.goTo(AddUserPage.class);
        addUserPage.getApplicationAccess().toggleApplication(ApplicationLicenseConstants.SERVICE_DESK_KEY);
        assertThat(addUserPage.getSelectedApplications(), contains(ApplicationLicenseConstants.SERVICE_DESK_KEY, ApplicationLicenseConstants.CORE_KEY));
        assertThat(addUserPage.getEffectiveApplications(), empty());
    }

    private String txtByKey(final Map<String, String> pageErrors, final String id) {
        return pageErrors.get(id);
    }

    private static Matcher<ApplicationRole> roleWithKeyAndName(final String key, final String name) {
        return new TypeSafeMatcher<ApplicationRole>() {
            @Override
            protected boolean matchesSafely(final ApplicationRole item) {
                return item.name.equals(name) && item.key.equals(key);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("ApplicationRole with key ").appendValue(key).appendText(" and name ").appendValue(name);
            }
        };
    }

    private List<ApplicationRole> getUserRoles(String userName) {
        final User user = userClient.get(userName, User.Expand.applicationRoles);
        return user.applicationRoles.items;
    }
}
