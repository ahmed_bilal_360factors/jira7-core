package com.atlassian.jira.webtest.webdriver.tests.header;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.CommonHeader;
import com.atlassian.jira.pageobjects.config.LoginAs;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@WebTest({Category.WEBDRIVER_TEST})
@RestoreOnce(value = "header-one-project.xml")
@LoginAs(user = "charlie", password = "charlie")
public class TestHeaderAsProjectAdmin extends BaseJiraWebTest {
    private CommonHeader header;

    @Before
    public void setUp() {
        header = CommonHeader.visit(jira);
    }

    @Test
    public void testAdminMenuLinksForProjectAdmin() {
        assertTrue(header.hasAdminMenu());
        MatcherAssert.assertThat(header.getAdminMenuLinkIds(), IsIterableContainingInOrder.contains(expectedAdminMenuLinks()));
    }

    @Test
    public void testProjectAdmin() {
        assertTrue(header.hasAppSwitcher());
        assertTrue(header.hasMainHeaderLinks());
        assertTrue(header.hasCreateIssueButton());
        assertTrue(header.hasQuickSearch());
        assertTrue(header.hasHelpMenu());
        assertTrue(header.hasAdminMenu());
        assertFalse(header.hasLoginButton());
        assertTrue(header.hasUserOptionsMenu());
    }

    private String[] expectedAdminMenuLinks() {
        return new String[]{"admin_project_menu"};
    }
}
