package com.atlassian.jira.webtest.webdriver.tests.permissions;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.pages.AddPermissionPage;
import com.atlassian.jira.pageobjects.pages.DeletePermissionPage;
import com.atlassian.jira.pageobjects.pages.EditPermissionsPage;
import com.atlassian.jira.pageobjects.pages.EditPermissionsSinglePage;
import com.atlassian.jira.pageobjects.pages.ViewPermissionSchemesPage;
import com.atlassian.jira.permission.ProjectPermissions;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

@WebTest(Category.WEBDRIVER_TEST)
public class TestPermissionSchemeRedirect extends BaseJiraWebTest {

    private static final String OLD_PERMISSIONS_DARK_FEATURE = "com.atlassian.jira.permission-schemes.single-page-ui.disabled";

    private static final Integer DEFAULT_PERM_SCHEME_ID = (int)FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
    private static final Integer ADMIN_ADMINISTER_PROJECTS_ID = 10004;

    private static final String ADMINISTER_PROJECTS = ProjectPermissions.ADMINISTER_PROJECTS.permissionKey();

    @Before
    public void setUp() {
        backdoor.darkFeatures().disableForSite(OLD_PERMISSIONS_DARK_FEATURE);
    }

    @Test
    @LoginAs(admin = true, targetPage = ViewPermissionSchemesPage.class)
    public void goingFromViewToEditWorksInNewImplementation() {
        ViewPermissionSchemesPage viewPermissionSchemesPage = pageBinder.bind(ViewPermissionSchemesPage.class);
        viewPermissionSchemesPage.editPermissionSchemePermissions("Default Permission Scheme");

        EditPermissionsSinglePage page = jira.getPageBinder().bind(EditPermissionsSinglePage.class, DEFAULT_PERM_SCHEME_ID);

        assertCurrentUrlMatchesExpectedUrl("Should have loaded new permission page on navigation from view permission schemes",
                page.getUrl());
    }

    @Test
    @LoginAs(admin = true)
    public void goesToNewProjectPermissionsByDefault() {
        EditPermissionsSinglePage page = jira.visit(EditPermissionsSinglePage.class, DEFAULT_PERM_SCHEME_ID);

        assertCurrentUrlMatchesExpectedUrl("Should have loaded new permission page on url visit",
                page.getUrl());
    }

    @Test
    @LoginAs(admin = true)
    public void goingToOldPermissionPagesRedirectsToNewIfDarkFeatureOff() {
        //noinspection deprecation
        jira.visitDelayed(EditPermissionsPage.class, DEFAULT_PERM_SCHEME_ID);

        assertCurrentUrlMatchesExpectedUrl("Did not redirect from old edit to single edit page when dark feature off",
                EditPermissionsSinglePage.getPageUrl(DEFAULT_PERM_SCHEME_ID));
    }

    @Test
    @LoginAs(admin = true)
    public void redirectsToOldProjectPermissionsOnDarkFeature() {
        backdoor.darkFeatures().enableForSite(OLD_PERMISSIONS_DARK_FEATURE);

        jira.visitDelayed(EditPermissionsSinglePage.class, DEFAULT_PERM_SCHEME_ID);

        assertCurrentUrlMatchesExpectedUrl("Should have been on the old permission page when dark feature on",
                EditPermissionsPage.getPageUrl(DEFAULT_PERM_SCHEME_ID));
    }

    @Test
    @LoginAs(admin = true)
    public void addPermissionPageWorksOnDarkFeature() {
        backdoor.darkFeatures().enableForSite(OLD_PERMISSIONS_DARK_FEATURE);

        AddPermissionPage addPermissionPage = jira.visit(AddPermissionPage.class, DEFAULT_PERM_SCHEME_ID, ADMINISTER_PROJECTS);

        assertCurrentUrlMatchesExpectedUrl("Should have been on the add permission page when dark feature on",
                addPermissionPage.getUrl());
    }

    @Test
    @LoginAs(admin = true)
    public void addPermissionRedirectsToEditWithNoDarkFeature() {
        jira.visitDelayed(AddPermissionPage.class, DEFAULT_PERM_SCHEME_ID, ADMINISTER_PROJECTS);

        assertCurrentUrlMatchesExpectedUrl("Did not redirect from add to single edit page when dark feature off",
                EditPermissionsSinglePage.getPageUrl(DEFAULT_PERM_SCHEME_ID));
    }

    @Test
    @LoginAs(admin = true)
    public void deletePermissionPageWorksOnDarkFeature() {
        backdoor.darkFeatures().enableForSite(OLD_PERMISSIONS_DARK_FEATURE);

        DeletePermissionPage deletePermissionPage = jira.visit(DeletePermissionPage.class, ADMIN_ADMINISTER_PROJECTS_ID, DEFAULT_PERM_SCHEME_ID);

        assertCurrentUrlMatchesExpectedUrl("Should have been on the delete permission page when dark feature on",
                deletePermissionPage.getUrl());
    }

    @Test
    @LoginAs(admin = true)
    public void deletePermissionPageRedirectsToEditWithNoDarkFeature() {
        jira.visitDelayed(DeletePermissionPage.class, ADMIN_ADMINISTER_PROJECTS_ID, DEFAULT_PERM_SCHEME_ID);

        assertCurrentUrlMatchesExpectedUrl("Did not redirect from delete to singe edit page when dark feature off",
                EditPermissionsSinglePage.getPageUrl(DEFAULT_PERM_SCHEME_ID));
    }


    private void assertCurrentUrlMatchesExpectedUrl(final String message, final String expectedUrl) {
        assertEquals(message, jira.environmentData().getBaseUrl() + expectedUrl, jira.getTester().getDriver().getCurrentUrl());
    }
}
