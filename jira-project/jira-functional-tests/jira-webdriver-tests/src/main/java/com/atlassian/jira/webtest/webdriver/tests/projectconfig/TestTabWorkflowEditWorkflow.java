package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.pages.admin.workflow.DiscardDraftDialog;
import com.atlassian.jira.pageobjects.pages.admin.workflow.PublishDialog;
import com.atlassian.jira.pageobjects.pages.admin.workflow.ViewWorkflowSteps;
import com.atlassian.jira.pageobjects.pages.admin.workflow.WorkflowHeader;
import com.atlassian.jira.pageobjects.websudo.JiraSudoFormDialog;
import com.atlassian.jira.pageobjects.project.workflow.EditWorkflowDialog;
import com.atlassian.jira.pageobjects.project.workflow.WorkflowsPageTab;
import com.google.common.collect.Iterables;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @since v5.1
 */

@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS, Category.IGNITE})
@RestoreOnce("xml/projectconfig/TestWorkflowTab.xml")
public class TestTabWorkflowEditWorkflow extends AbstractProjectEditWorkflow<WorkflowsPageTab> {
    @Before
    public void before() {
        //Make sure we are in text mode before this test runs.
        final ViewWorkflowSteps steps = jira.goTo(ViewWorkflowSteps.class, DEFAULT_WF_NAME, false);
        steps.setCurrentViewMode(WorkflowHeader.WorkflowMode.TEXT);
    }

    @Test
    public void testRedirectAfterEditFromText() {
        final String projectKey = "RAEFPCT";
        final String projectName = "testRedirectAfterEditFromProjectConfigText";
        backdoor.project().addProject(projectName, projectKey, "admin");

        checkRedirectFrom(projectKey, projectName, false);
    }

    @Test
    public void testRedirectAfterEditDiagram() {
        final String projectKey = "RAEFPCTD";
        final String projectName = "testRedirectAfterEditFromProjectConfigDiagram";
        backdoor.project().addProject(projectName, projectKey, "admin");

        switchIntoDiagramMode();
        checkRedirectFrom(projectKey, projectName, true);
    }

    @Override
    WorkflowsPageTab gotoPageForProject(final String key) {
        return jira.goTo(WorkflowsPageTab.class, key);
    }

    @Override
    EditWorkflowDialog openMigrationDialog(final WorkflowsPageTab page) {
        final WorkflowsPageTab.Workflow workflowPanel = Iterables.getOnlyElement(page.getWorkflowPanels());
        return workflowPanel.gotoEditWorkflowDialog();
    }

    @Override
    void authenticateSudoFormAfterMigration(final JiraSudoFormDialog dialog, final String workflowName) {
        dialog.authenticate("admin", ViewWorkflowSteps.class, workflowName, true);
    }

    @Override
    void clickEdit(final WorkflowsPageTab page, final String workflowDisplayName, final boolean diagram) {
        if (diagram) {
            clickEditAndGotoDiagramMode(page, workflowDisplayName);
        } else {
            clickEditAndGotoTextMode(page, workflowDisplayName);
        }
    }

    private void clickEditAndGotoTextMode(final WorkflowsPageTab page, final String workflowDisplayName) {
        page.getPanelForName(workflowDisplayName).clickEditAndGotoTextMode();
    }

    private void clickEditAndGotoDiagramMode(final WorkflowsPageTab page, final String workflowDisplayName) {
        page.getPanelForName(workflowDisplayName).clickEditAndGotoDiagramMode();
    }

    private WorkflowsPageTab discardDraft(final DiscardDraftDialog dialog) {
        return dialog.submitAndBind(WorkflowsPageTab.class);
    }

    private WorkflowsPageTab publishDraft(final PublishDialog dialog) {
        return dialog.submitAndBind(WorkflowsPageTab.class);
    }

    private void checkRedirectFrom(String projectKey, String projectName, boolean diagram) {
        final WorkflowsPageTab page = gotoPageForProject(projectKey);
        EditWorkflowDialog editWorkflowDialog = openMigrationDialog(page);

        // Create a new workflow scheme.
        final String newWorkflowName = String.format("%s Workflow", projectName);
        WorkflowHeader header;
        if (diagram) {
            header = editWorkflowDialog.gotoEditWorkflowDigram(newWorkflowName);
        } else {
            header = editWorkflowDialog.gotoEditWorkflowText(newWorkflowName);
        }

        // Discard the draft for the workflow and make sure we go back to project config.
        final DiscardDraftDialog discardDraftDialog = header.openDiscardDialog();
        discardDraft(discardDraftDialog);

        // Publish new workflow to make sure we end up back on project config.
        clickEdit(page, newWorkflowName, diagram);

        publishDraft(header.openPublishDialog().disableBackup());

        // Goto the workflow directly and make sure we are redirected back to view workflow.
        header = jira.goTo(ViewWorkflowSteps.class, newWorkflowName);
        WorkflowHeader.WorkflowMode<? extends WorkflowHeader> mode
                = diagram ? WorkflowHeader.WorkflowMode.DIAGRAM : WorkflowHeader.WorkflowMode.TEXT;
        header.setCurrentViewMode(mode);

        header = header.createDraft(mode);
        header = header.openDiscardDialog().submitAndGotoViewWorkflow();
        assertEquals(newWorkflowName, header.getWorkflowName());

        header = header.createDraft(mode);
        header = header.openPublishDialog().disableBackup().submitAndGotoViewWorkflow();
        assertEquals(newWorkflowName, header.getWorkflowName());
    }

    private void switchIntoDiagramMode() {
        // Make sure we are in diagram mode before this test runs.
        final ViewWorkflowSteps steps = jira.goTo(ViewWorkflowSteps.class, DEFAULT_WF_NAME, false);
        steps.setCurrentViewMode(WorkflowHeader.WorkflowMode.DIAGRAM);
    }

    @Override
    void doWorkflowMigration(EditWorkflowDialog dialog, String workflowName) {
        dialog.clickAndWaitBeforeBind(ViewWorkflowSteps.class, workflowName, true);
    }
}
