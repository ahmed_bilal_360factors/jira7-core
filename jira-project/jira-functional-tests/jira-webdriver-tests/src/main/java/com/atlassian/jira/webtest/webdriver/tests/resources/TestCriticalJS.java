package com.atlassian.jira.webtest.webdriver.tests.resources;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.config.CreateUser;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.config.ResetData;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.pageobjects.util.CriticalResources;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

/**
 * This test suite is supposed to prevent JIRA developers from adding unused resources to context.
 * <p>
 * In case this test fails for you, defer loading of offending resource, exceptions can be added to appropriate text file.
 * <p>
 * Check {@link CriticalResources} for more details on how unused resources are detected.
 */
@WebTest({Category.WEBDRIVER_TEST})
@ResetData
public class TestCriticalJS extends AbstractTestCriticalResources {
    private static final String USER_VIEW_ISSUE = "fred1";
    private static final String USER_ISSUE_SEARCH = "fred2";
    private static final String USER_DASHBOARD = "fred3";
    private static final String INSTRUMENT_PROPERTY = "instrument-js";

    private String issueKey;

    @BeforeClass
    public static void setUpClass() throws Exception {
        backdoor.systemProperties().setProperty(INSTRUMENT_PROPERTY, "true");
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        backdoor.systemProperties().unsetProperty(INSTRUMENT_PROPERTY);
    }

    @Before
    public void setUp() throws Exception {
        issueKey = backdoor.issues().createIssue("HSP", "xxx").key;
    }

    @After
    public void tearDown() throws Exception {
        backdoor.issues().deleteIssue(issueKey, true);
    }

    @Test
    @CreateUser(username = USER_VIEW_ISSUE, password = USER_VIEW_ISSUE)
    @LoginAs(user = USER_VIEW_ISSUE)
    public void testViewIssueResources() throws IOException {
        visitPage(() -> jira.goTo(ViewIssuePage.class, issueKey));

        verifyResources("exceptions-view-issue-js.txt");
        assertNoErrors();
    }

    @Test
    @CreateUser(username = USER_ISSUE_SEARCH, password = USER_ISSUE_SEARCH)
    @LoginAs(user = USER_ISSUE_SEARCH)
    public void testIssueSearchResources() throws IOException {
        visitPage(jira::goToIssueNavigator);

        verifyResources("exceptions-issue-search-js.txt");
        assertNoErrors();
    }

    @Test
    @CreateUser(username = USER_DASHBOARD, password = USER_DASHBOARD)
    @LoginAs(user = USER_DASHBOARD)
    public void testDashboardResources() throws IOException {
        visitPage(jira::gotoHomePage);

        verifyResources("exceptions-dashboard-js.txt");
        assertNoErrors();
    }

    private void verifyResources(final String exceptionsResource) throws IOException {
        super.verifyResources(exceptionsResource, CriticalResources.ResourceType.SCRIPT);
    }

}
