package com.atlassian.jira.webtest.webdriver.tests.project;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.admin.EditProjectDialog;
import com.atlassian.jira.pageobjects.pages.admin.ProjectDetailsHelpTooltip;
import com.atlassian.jira.pageobjects.pages.admin.ProjectRow;
import com.atlassian.jira.pageobjects.pages.admin.ViewProjectsPage;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyCollection.empty;

/**
 * @since v7.1
 */
@WebTest({Category.WEBDRIVER_TEST, Category.PROJECTS})
public class TestViewProjectsPage extends BaseJiraWebTest {

    private static final String PROJECT_NAME_HSP = "homosapien";

    @Test
    public void shouldNotThrowAnExceptionWhenViewingProjectsPageWhenThereAreNoProjects() {
        backdoor.restoreDataFromResource("TestViewProjectsPage.xml");

        ViewProjectsPage viewProjectsPage = goToViewProjectsPage();

        assertThat(viewProjectsPage.getProjects(), is(empty()));
    }

    @Test
    public void projectDetailsTooltipsShouldDisplayOnClick() {
        // This test is intended to prevent regression of JSEV-347.
        backdoor.restoreBlankInstance();

        ViewProjectsPage viewProjectsPage = goToViewProjectsPage();
        ProjectRow projectrow = viewProjectsPage.getProject(PROJECT_NAME_HSP);
        EditProjectDialog editProjectDialog = projectrow.editProject();

        List<ProjectDetailsHelpTooltip> tooltips = Arrays.asList(
                editProjectDialog.getProjectKeyHelpTooltip(),
                editProjectDialog.getProjectTypeHelpTooltip(),
                editProjectDialog.getProjectCategoryHelpTooltip());

        for (ProjectDetailsHelpTooltip tooltip : tooltips) {
            tooltip.open();
            assertThat(tooltip.isOpen(), is(true));
            assertThat(tooltip.hasVisibleContent(), is(true));
            tooltip.close();
        }
    }

    private ViewProjectsPage goToViewProjectsPage() {
        return jira.goTo(ViewProjectsPage.class);
    }

}
