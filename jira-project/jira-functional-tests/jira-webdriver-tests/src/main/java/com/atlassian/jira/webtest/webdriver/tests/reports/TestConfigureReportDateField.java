package com.atlassian.jira.webtest.webdriver.tests.reports;

import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.CalendarPicker;
import com.atlassian.jira.pageobjects.components.CalendarPopup;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.inject.Inject;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@WebTest({Category.WEBDRIVER_TEST, Category.REPORTS})
@RestoreBlankInstance
public class TestConfigureReportDateField extends BaseJiraWebTest {

    private static final String DATE_FIELD_ID = "date_aDate";

    @Inject
    private WebDriver webDriver;

    @Inject
    private PageBinder binder;

    @Test
    public void datePickerFillsInCorrectDate() {
        goToConfigureReport("10000", "com.atlassian.jira.dev.func-test-plugin:fieldtest-report");

        CalendarPicker calendarPicker = binder.bind(CalendarPicker.class, DATE_FIELD_ID);

        //Set the initial date so we know what year/month we are in
        calendarPicker.setDate("1/Jan/16");

        CalendarPopup calendarPopup = calendarPicker.openCalendarPopup();
        calendarPopup.getDayCell(13).click();

        Poller.waitUntilTrue(calendarPopup.isClosed());

        //Submit form
        webDriver.findElement(By.name("Next")).click();

        //Verify the date field got the right value
        assertThat(webDriver.findElement(By.xpath("//tr[th = 'aDate']/td")).getText(), is("13/Jan/16"));
    }

    private void goToConfigureReport(String projectId, String reportKey) {
        String url = String.format(
                "%s/secure/ConfigureReport!default.jspa?selectedProjectId=%s&reportKey=%s",
                jira.getProductInstance().getBaseUrl(), projectId, reportKey
        );
        webDriver.navigate().to(url);
    }

}
