package com.atlassian.jira.webtest.webdriver.tests.bigpipe;

import com.atlassian.browsers.BrowserConfig;
import com.atlassian.webdriver.browsers.profile.ProfilePreferences;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

//TODO this was copied/modified from atlassian-webdriver-core - would love to get this in there
public final class FirefoxBrowser {
    private static final Logger log = LoggerFactory.getLogger(FirefoxBrowser.class);

    private FirefoxBrowser() {
        throw new IllegalStateException("FirefoxBrowser is not constructable");
    }

    public static FirefoxDriver getFirefoxDriver() {
        FirefoxBinary firefox = new FirefoxBinary();
        setSystemProperties(firefox);
        return new FirefoxDriver(firefox, (FirefoxProfile) null);
    }

    public static FirefoxDriver getFirefoxDriver(BrowserConfig browserConfig, Map<String, String> additionalOptions) {
        FirefoxProfile profile = null;
        if (browserConfig != null) {
            FirefoxBinary firefox = new FirefoxBinary(new File(browserConfig.getBinaryPath()));
            if (browserConfig.getProfilePath() != null) {
                File profilePath = new File(browserConfig.getProfilePath());
                profile = new FirefoxProfile();
                addExtensionsToProfile(profile, profilePath);
                addPreferencesToProfile(profile, profilePath);

                for (Map.Entry<String, String> additionalOptionEntry : additionalOptions.entrySet()) {
                    profile.setPreference(additionalOptionEntry.getKey(), additionalOptionEntry.getValue());
                }
            }

            setSystemProperties(firefox);
            return new FirefoxDriver(firefox, profile);
        } else {
            return getFirefoxDriver();
        }
    }

    private static void addPreferencesToProfile(FirefoxProfile profile, File profilePath) {
        File profilePreferencesFile = new File(profilePath, "profile.preferences");
        if (profilePreferencesFile.exists()) {
            ProfilePreferences profilePreferences = new ProfilePreferences(profilePreferencesFile);
            Map preferences = profilePreferences.getPreferences();
            Iterator i$ = preferences.keySet().iterator();

            while (i$.hasNext()) {
                String key = (String) i$.next();
                Object value = preferences.get(key);
                if (value instanceof Integer) {
                    profile.setPreference(key, ((Integer) value).intValue());
                } else if (value instanceof Boolean) {
                    profile.setPreference(key, ((Boolean) value).booleanValue());
                } else {
                    profile.setPreference(key, (String) value);
                }
            }
        }

    }

    private static void addExtensionsToProfile(FirefoxProfile profile, File profilePath) {
        File[] arr$ = profilePath.listFiles(new FileFilter() {
            public boolean accept(File file) {
                return file.getName().matches(".*\\.xpi$");
            }
        });
        int len$ = arr$.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            File extension = arr$[i$];

            try {
                profile.addExtension(extension);
            } catch (IOException var7) {
                log.error("Unable to load extension: " + extension, var7);
            }
        }

    }

    public static FirefoxDriver getFirefoxDriver(String browserPath) {
        if (browserPath != null) {
            FirefoxBinary firefox = new FirefoxBinary(new File(browserPath));
            setSystemProperties(firefox);
            return new FirefoxDriver(firefox, (FirefoxProfile) null);
        } else {
            log.info("Browser path was null, falling back to default firefox driver.");
            return getFirefoxDriver();
        }
    }

    private static void setSystemProperties(FirefoxBinary firefox) {
        if (System.getProperty("DISPLAY") != null) {
            firefox.setEnvironmentProperty("DISPLAY", System.getProperty("DISPLAY"));
        }

    }
}
