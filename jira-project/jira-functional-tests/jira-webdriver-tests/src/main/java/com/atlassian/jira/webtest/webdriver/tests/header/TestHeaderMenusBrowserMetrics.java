package com.atlassian.jira.webtest.webdriver.tests.header;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.jira.pageobjects.util.browsermetrics.Beacon;
import com.atlassian.jira.pageobjects.util.browsermetrics.BrowserMetricsContext;
import com.atlassian.jira.pageobjects.util.browsermetrics.Checkpoint;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.inject.Inject;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;

/**
 * JIRA's application header has a set of menus which are instrumented using browser-metrics. This test suite ensures
 * that these menus are measured correctly.
 *
 * @since v7.2.0
 */
@WebTest({Category.WEBDRIVER_TEST})
public class TestHeaderMenusBrowserMetrics extends BaseJiraWebTest {
    @Inject
    private BrowserMetricsContext browserMetricsContext;

    private JiraHeader header;

    @BeforeClass
    public static void setUpClass() {
        backdoor.restoreDataFromResource("xml/blankprojects.xml");
    }

    @Before
    public void setUpTest() {
        header = pageBinder.navigateToAndBind(DashboardPage.class).getHeader();
    }

    @Test
    public void dashboardsMenuEmitsBeacon() {
        Checkpoint checkpoint = browserMetricsContext.checkpoint();

        header.getDashboardMenu().open();
        Beacon beacon = checkpoint.waitFor("jira.header.menu.dashboards");

        assertThat(beacon.report.readyForUser, greaterThanOrEqualTo(0.0));
        assertThat(beacon.report.isInitial, is(false));
    }

    @Test
    public void issuesMenuEmitsBeacon() {
        Checkpoint checkpoint = browserMetricsContext.checkpoint();

        header.getIssuesMenu().open();
        Beacon beacon = checkpoint.waitFor("jira.header.menu.issues");

        assertThat(beacon.report.readyForUser, greaterThanOrEqualTo(0.0));
        assertThat(beacon.report.isInitial, is(false));
    }

    @Test
    public void projectsMenuEmitsBeacon() {
        Checkpoint checkpoint = browserMetricsContext.checkpoint();

        header.getProjectsMenu().open();
        Beacon beacon = checkpoint.waitFor("jira.header.menu.projects");

        assertThat(beacon.report.readyForUser, greaterThanOrEqualTo(0.0));
        assertThat(beacon.report.isInitial, is(false));
    }

    @Test
    public void helpMenuEmitsBeacon() {
        Checkpoint checkpoint = browserMetricsContext.checkpoint();

        header.getHelpMenu().open();
        Beacon beacon = checkpoint.waitFor("jira.header.menu.help");

        assertThat(beacon.report.readyForUser, greaterThanOrEqualTo(0.0));
        assertThat(beacon.report.isInitial, is(false));
    }

    @Test
    public void adminMenuEmitsBeacon() {
        Checkpoint checkpoint = browserMetricsContext.checkpoint();

        header.getAdminMenu().open();
        Beacon beacon = checkpoint.waitFor("jira.header.menu.admin");

        assertThat(beacon.report.readyForUser, greaterThanOrEqualTo(0.0));
        assertThat(beacon.report.isInitial, is(false));
    }

    @Test
    public void userProfileMenuEmitsBeacon() {
        Checkpoint checkpoint = browserMetricsContext.checkpoint();

        header.getUserProfileMenu().open();
        Beacon beacon = checkpoint.waitFor("jira.header.menu.profile");

        assertThat(beacon.report.readyForUser, greaterThanOrEqualTo(0.0));
        assertThat(beacon.report.isInitial, is(false));
    }
}
