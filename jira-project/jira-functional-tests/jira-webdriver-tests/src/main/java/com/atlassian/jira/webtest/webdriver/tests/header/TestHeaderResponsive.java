package com.atlassian.jira.webtest.webdriver.tests.header;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.components.menu.IssuesMenu;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Dimension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * JIRA's application header has a set of menus which might be loaded asynchronously.
 * This test verifies if dropdown is operational on small screen and after resize.
 *
 * @since v7.2.0
 */
@WebTest({Category.WEBDRIVER_TEST})
public class TestHeaderResponsive extends BaseJiraWebTest {
    private static final String STUFF_FEATURE = "jira.refapp.stuff.header";

    @BeforeClass
    public static void setUpClass() {
        backdoor.restoreBlankInstance();

        jira.quickLoginAsAdmin();
        // visit issue page to populate recent issues dropdown
        jira.goToViewIssue(backdoor.issues().createIssue("HSP", "summary").key);
    }

    @Before
    public void setUp() throws Exception {
        backdoor.darkFeatures().enableForSite(STUFF_FEATURE);
    }

    @After
    public void tearDown() throws Exception {
        backdoor.darkFeatures().disableForSite(STUFF_FEATURE);
    }

    @Test
    public void issuesMenuNotEmptyInsideMoreWhenMinimized() {
        assertEquals("ENABLED", backdoor.plugins().getPluginState("com.atlassian.jira.dev.reference-plugin"));

        jira.getTester().getDriver().manage().window().setSize(new Dimension(700, 500));
        jira.gotoHomePage();
        JiraHeader header = jira.gotoHomePage().getHeader();

        final IssuesMenu issuesMenu = header.getNestedIssuesMenu().open();
        assertTrue(issuesMenu.isOpen());
        assertThat(issuesMenu.getRecentIssues(), is(not(empty())));
    }

    @Test
    public void issuesMenuNotEmptyAfterMaximize() {
        assertEquals("ENABLED", backdoor.plugins().getPluginState("com.atlassian.jira.dev.reference-plugin"));

        jira.getTester().getDriver().manage().window().setSize(new Dimension(700, 500));
        jira.gotoHomePage();
        JiraHeader header = jira.gotoHomePage().getHeader();
        assertTrue(header.getMoreMenu().isExists());

        jira.getTester().getDriver().manage().window().maximize();
        final IssuesMenu issuesMenu = header.getIssuesMenu().open();
        assertTrue(issuesMenu.isOpen());
        assertThat(issuesMenu.getRecentIssues(), is(not(empty())));
    }

    @Test
    public void issuesMenuNotEmptyInsideMoreAfterMinimize() {
        assertEquals("ENABLED", backdoor.plugins().getPluginState("com.atlassian.jira.dev.reference-plugin"));

        jira.getTester().getDriver().manage().window().maximize();
        jira.gotoHomePage();
        JiraHeader header = jira.gotoHomePage().getHeader();
        assertTrue(header.getIssuesMenu().isExists());

        jira.getTester().getDriver().manage().window().setSize(new Dimension(700, 500));
        final IssuesMenu issuesMenu = header.getNestedIssuesMenu().open();
        assertTrue(issuesMenu.isOpen());
        assertThat(issuesMenu.getRecentIssues(), is(not(empty())));
    }
}
