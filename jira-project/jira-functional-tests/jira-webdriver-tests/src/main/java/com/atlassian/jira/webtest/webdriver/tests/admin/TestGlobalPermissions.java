package com.atlassian.jira.webtest.webdriver.tests.admin;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.ResetData;
import com.atlassian.jira.pageobjects.pages.admin.GlobalPermissionsPage;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.pageobjects.pages.admin.GlobalPermissionsPage.ADMINISTRATORS_GROUP;
import static com.atlassian.jira.pageobjects.pages.admin.GlobalPermissionsPage.ADMINISTRATORS_PERM;
import static com.atlassian.jira.pageobjects.pages.admin.GlobalPermissionsPage.BROWSE_USERS_PERM;
import static com.atlassian.jira.pageobjects.pages.admin.GlobalPermissionsPage.SYS_ADMINISTRATORS_PERM;
import static org.junit.Assert.assertEquals;


@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION})
@ResetData
public class TestGlobalPermissions extends BaseJiraWebTest {

    final String SOFTWARE_GROUP = "jira-software-users";

    @Before
    public void setUp() {
        backdoor.restoreBlankInstance();

        backdoor.usersAndGroups().addGroup(SOFTWARE_GROUP);
        backdoor.applicationRoles().putRoleAndSetDefault("jira-software", SOFTWARE_GROUP);
    }

    @Test
    public void warnWhenAddingAdminPermissionsToDefaultGroup() {
        final GlobalPermissionsPage page = jira.goTo(GlobalPermissionsPage.class);
        Poller.waitUntilFalse(page.isAddGlobalPermissionWarningPresent());

        page.selectPermission(SYS_ADMINISTRATORS_PERM);
        page.selectGroup(SOFTWARE_GROUP);

        String warning = page.getWarningMessage();
        assertEquals(warning, SOFTWARE_GROUP
                + " is a default group of JIRA Software. All current and future JIRA Software users will get "
                + SYS_ADMINISTRATORS_PERM + " permission.");
        page.selectPermission(ADMINISTRATORS_PERM);
        warning = page.getWarningMessage();
        assertEquals(warning, SOFTWARE_GROUP
                + " is a default group of JIRA Software. All current and future JIRA Software users will get "
                + ADMINISTRATORS_PERM + " permission.");

        page.selectGroup(ADMINISTRATORS_GROUP);
        Poller.waitUntilFalse(page.isAddGlobalPermissionWarningPresent());

        page.selectGroup(SOFTWARE_GROUP);
        Poller.waitUntilTrue(page.isAddGlobalPermissionWarningPresent());

        page.selectPermission(BROWSE_USERS_PERM);
        Poller.waitUntilFalse(page.isAddGlobalPermissionWarningPresent());
    }
}
