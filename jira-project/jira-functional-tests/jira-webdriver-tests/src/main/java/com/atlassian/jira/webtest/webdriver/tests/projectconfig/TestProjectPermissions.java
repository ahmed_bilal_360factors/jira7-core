package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.admin.EditPermissionScheme;
import com.atlassian.jira.pageobjects.pages.admin.SelectPermissionScheme;
import com.atlassian.jira.pageobjects.project.ProjectConfigTabs;
import com.atlassian.jira.pageobjects.project.ProjectSharedBy;
import com.atlassian.jira.pageobjects.project.permissions.Permission;
import com.atlassian.jira.pageobjects.project.permissions.PermissionGroup;
import com.atlassian.jira.pageobjects.project.permissions.ProjectPermissionPageTab;
import com.atlassian.jira.pageobjects.project.summary.ProjectSummaryPageTab;
import com.atlassian.jira.util.collect.CollectionBuilder;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * @since v4.4
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS, Category.PERMISSIONS})
@Restore("xml/projectconfig/ProjectPermissionConfig.xml")
public class TestProjectPermissions extends BaseJiraWebTest {
    public static final String PROJECT_HSP = "HSP";
    public static final String DEFAULT_PERMISSION_SCHEME = "Default Permission Scheme";
    public static final String DEFAULT_PERMISSIONS_SCHEME_DESC = "This is the default Permission Scheme. Any new projects that are created will be assigned this scheme";
    public static final String EMPTY_SCHEME = "Empty Scheme";
    public static final String ANOTHER_SHARED_PROJECT = "Another Shared project";
    public static final String XSS_SCHEME = "<script>alert(\"wtf\");</script>";
    public static final String USER_CUSTOM_FIELD = "User custom field value (" + XSS_SCHEME + ")";

    @Test
    public void testTabNavigation() {
        final ProjectSummaryPageTab config = jira.goTo(ProjectSummaryPageTab.class, PROJECT_HSP);
        final ProjectConfigTabs tabs = config.getTabs();
        assertTrue(tabs.isSummaryTabSelected());

        final ProjectPermissionPageTab projectPermissionPageTab = tabs.gotoProjectPermissionsTab();
        assertTrue(projectPermissionPageTab.getTabs().isProjectPermissionsTabSelected());
        assertEquals(PROJECT_HSP, projectPermissionPageTab.getProjectKey());

        final ProjectSharedBy sharedBy = projectPermissionPageTab.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals("2 PROJECTS", sharedBy.getTriggerText());
        assertEquals(Arrays.asList("Shared project", "homosapien"), sharedBy.getProjects());
    }

    @Test
    public void testProjectAdmin() {
        final ProjectPermissionPageTab projectPermissionPage = jira.quickLogin("project_admin", "project_admin", ProjectPermissionPageTab.class, PROJECT_HSP);
        assertEquals(DEFAULT_PERMISSION_SCHEME, projectPermissionPage.getSchemeName());
        assertEquals(DEFAULT_PERMISSIONS_SCHEME_DESC, projectPermissionPage.getSchemeDescription());
        final List<PermissionGroup> permissionGroups = projectPermissionPage.getPermissionGroups();
        assertEquals(6, permissionGroups.size());

        // Assert the cog actions aren't present
        assertFalse(projectPermissionPage.isSchemeLinked());
        assertFalse(projectPermissionPage.isSchemeChangeAvailable());

        final ProjectSharedBy sharedBy = projectPermissionPage.getSharedBy();
        assertFalse(sharedBy.isPresent());
    }

    @Test
    public void testDefaultGroups() {
        ProjectPermissionPageTab projectPermissionPage = jira.goTo(ProjectPermissionPageTab.class, PROJECT_HSP);

        assertEquals(DEFAULT_PERMISSION_SCHEME, projectPermissionPage.getSchemeName());
        assertEquals(DEFAULT_PERMISSIONS_SCHEME_DESC, projectPermissionPage.getSchemeDescription());
        final List<PermissionGroup> permissionGroups = projectPermissionPage.getPermissionGroups();
        assertEquals(6, permissionGroups.size());

        assertTrue(projectPermissionPage.isSchemeLinked());

        final EditPermissionScheme editPermissionScheme = projectPermissionPage.gotoScheme();

        projectPermissionPage = editPermissionScheme.back(ProjectPermissionPageTab.class, PROJECT_HSP);
        assertEquals(DEFAULT_PERMISSION_SCHEME, projectPermissionPage.getSchemeName());
        assertEquals(DEFAULT_PERMISSIONS_SCHEME_DESC, projectPermissionPage.getSchemeDescription());

        assertTrue(projectPermissionPage.isSchemeChangeAvailable());
        final SelectPermissionScheme selectPermissionScheme = projectPermissionPage.gotoSelectScheme();
        selectPermissionScheme.setSchemeByName(EMPTY_SCHEME);
        selectPermissionScheme.submit();

        projectPermissionPage = jira.visit(ProjectPermissionPageTab.class, PROJECT_HSP);
        assertEquals(EMPTY_SCHEME, projectPermissionPage.getSchemeName());
        assertEquals("", projectPermissionPage.getSchemeDescription());
    }

    @Test
    public void testNoPermissions() {
        final ProjectPermissionPageTab projectPermissionPage = jira.goTo(ProjectPermissionPageTab.class, "MKY");

        assertEquals(EMPTY_SCHEME, projectPermissionPage.getSchemeName());
        assertEquals("", projectPermissionPage.getSchemeDescription());
        final List<PermissionGroup> permissionGroups = projectPermissionPage.getPermissionGroups();
        assertEquals(6, permissionGroups.size());
        for (final PermissionGroup permissionGroup : permissionGroups) {
            final List<Permission> permissions = permissionGroup.getPermissions();
            for (final Permission permission : permissions) {
                assertTrue(permission.getEntities().isEmpty());
            }
        }

        final ProjectSharedBy sharedBy = projectPermissionPage.getSharedBy();
        assertFalse(sharedBy.isPresent());
    }

    @Test
    public void testAllTypes() {
        final ProjectPermissionPageTab projectPermissionPage = jira.goTo(ProjectPermissionPageTab.class, "CHOC");
        assertEquals("Choc Full Scheme", projectPermissionPage.getSchemeName());
        assertEquals("Choc Full Permission Scheme", projectPermissionPage.getSchemeDescription());

        final Permission permission = projectPermissionPage.getPermissionByName("Create Issues");
        final List<String> entities = permission.getEntities();
        final List<String> expectedEntities = CollectionBuilder.newBuilder("Reporter", "Group (jira-administrators)",
                "Group (Anyone)", "Single user (Administrator)", "Project lead", "Current assignee",
                "Project Role (Administrators)", "User custom field value (User Picker)",
                "Group custom field value (Group Picker)").asList();
        assertEquals(expectedEntities, entities);

        final ProjectSharedBy sharedBy = projectPermissionPage.getSharedBy();
        assertFalse(sharedBy.isPresent());
    }

    @Test
    public void testXSS() {
        final ProjectPermissionPageTab projectPermissionPage = jira.goTo(ProjectPermissionPageTab.class, "XSS");
        assertEquals(XSS_SCHEME, projectPermissionPage.getSchemeName());
        assertEquals(XSS_SCHEME, projectPermissionPage.getSchemeDescription());
        final Permission permission = projectPermissionPage.getPermissionByName("Administer Projects");

        final List<String> entities = permission.getEntities();
        final List<String> expectedEntities = CollectionBuilder.newBuilder(USER_CUSTOM_FIELD).asList();
        assertEquals(expectedEntities, entities);

        final ProjectSharedBy sharedBy = projectPermissionPage.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals("2 PROJECTS", sharedBy.getTriggerText());
        assertEquals(Arrays.asList(XSS_SCHEME, ANOTHER_SHARED_PROJECT), sharedBy.getProjects());
    }

    @Test
    public void testProjectAdminCanViewSharedBy() {
        final ProjectPermissionPageTab projectPermissionPage = jira.quickLogin("project_admin", "project_admin", ProjectPermissionPageTab.class, "XSS");
        assertEquals(XSS_SCHEME, projectPermissionPage.getSchemeName());
        assertEquals(XSS_SCHEME, projectPermissionPage.getSchemeDescription());
        final Permission permission = projectPermissionPage.getPermissionByName("Administer Projects");

        final List<String> entities = permission.getEntities();
        final List<String> expectedEntities = CollectionBuilder.newBuilder(USER_CUSTOM_FIELD).asList();
        assertEquals(expectedEntities, entities);

        final ProjectSharedBy sharedBy = projectPermissionPage.getSharedBy();
        assertTrue(sharedBy.isPresent());
        assertEquals("2 PROJECTS", sharedBy.getTriggerText());
        assertEquals(Arrays.asList(XSS_SCHEME, ANOTHER_SHARED_PROJECT), sharedBy.getProjects());
    }

}
