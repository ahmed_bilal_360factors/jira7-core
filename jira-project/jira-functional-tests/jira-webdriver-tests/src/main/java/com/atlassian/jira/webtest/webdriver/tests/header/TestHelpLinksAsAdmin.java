package com.atlassian.jira.webtest.webdriver.tests.header;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.CommonHeader;
import com.atlassian.jira.pageobjects.config.LoginAs;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@WebTest({Category.WEBDRIVER_TEST})
@LoginAs(admin = true)
public class TestHelpLinksAsAdmin extends BaseJiraWebTest {
    @Test
    public void testCorrectHelpLinksShow() {
        backdoor.restoreBlankInstance();
        final CommonHeader header = CommonHeader.visit(jira);
        assertTrue(header.hasHelpMenu());
        assertThat(header.getHelpMenuLinkIds(), IsIterableContainingInOrder.contains("view_core_help",
                "view_admin_help", "keyshortscuthelp", "view_about", "view_credits"));
    }
}
