package com.atlassian.jira.webtest.webdriver.tests.admin.users;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.pages.admin.user.EditUserGroupsPage;
import com.atlassian.jira.pageobjects.pages.admin.user.UserBrowserPage;
import com.atlassian.jira.test.util.lic.ApplicationLicenseConstants;
import com.atlassian.jira.webtest.webdriver.util.AUIHelpTip;
import com.atlassian.jira.webtests.LicenseKeys;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Description;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.GROUP_LABEL_ADMIN;
import static com.atlassian.jira.functest.framework.FunctTestConstants.GROUP_LABEL_JIRA_CORE;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.emptyIterable;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@WebTest(Category.WEBDRIVER_TEST)
public class TestUserBrowserPage extends BaseJiraWebTest {
    public static final String TEST_APP_KEY = "jira-func-test";
    public static final String INACTIVE_USER_NAME = "inactive-user";
    public static final String JIRA_USERS_GROUP = "jira-users";

    @BeforeClass
    public static void setUpInstance() {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_AND_TEST_ROLES.getLicenseString());
        backdoor.usersAndGroups().addGroup("test-product-users");
        backdoor.applicationRoles().putRoleAndSetDefault(TEST_APP_KEY, "test-product-users");

        backdoor.usersAndGroups().addUser("no-access-user");
        backdoor.usersAndGroups().addUser(INACTIVE_USER_NAME);
        backdoor.usersAndGroups().addUserToGroup(INACTIVE_USER_NAME, JIRA_USERS_GROUP);
        backdoor.userManager().setActive(INACTIVE_USER_NAME, false);
        backdoor.usersAndGroups().removeUserFromGroup("no-access-user", "jira-users");
        backdoor.usersAndGroups().addUserToGroup("fred", "test-product-users");

        // Removing core access because JIRA Core is the default application
        backdoor.usersAndGroups().removeUserFromGroup("fred", "jira-users");
        backdoor.usersAndGroups().removeUserFromGroup("no-access-user", "jira-users");

        /**
         * EXPECTED STATE:
         * fred - Test Product access
         * admin - Core Access
         * no-access-user - No Access
         */
    }

    @Test
    public void testApplicationColumnIsVisibleOnlyWhenAppRolesAreEnabled() {
        final UserBrowserPage userBrowser = jira.goTo(UserBrowserPage.class);
        Poller.waitUntilTrue(userBrowser.hasApplicationFilter());
        Poller.waitUntilTrue(userBrowser.findRow("admin").hasApplicationRolesColumn());

        tableContainUsers(userBrowser, "admin", "fred", INACTIVE_USER_NAME, "no-access-user");
    }

    @Test
    public void shouldFilterByApplicationsWhenFilterIsSet() {
        UserBrowserPage userBrowser = jira.goTo(UserBrowserPage.class).filterByApplication(TEST_APP_KEY);
        tableContainUsers(userBrowser, "fred");

        userBrowser = userBrowser.filterByApplication(UserBrowserPage.FILTER_ANY_APPLICATION_ACCESS);
        tableContainUsers(userBrowser, "admin", "fred", INACTIVE_USER_NAME);

        userBrowser = userBrowser.filterByApplication(UserBrowserPage.FILTER_NO_APPLICATION_ACCESS);
        tableContainUsers(userBrowser, "no-access-user");
    }

    @Test
    public void shouldAllowToVisitUsersPagePrefiltered() {
        //also checks whether jira-core shows only explicit core access
        final UserBrowserPage userBrowserPage = jira.goTo(UserBrowserPage.class, "jira-core");
        tableContainUsers(userBrowserPage, "admin" , INACTIVE_USER_NAME);
    }

    @Test
    public void shouldShowGroupLabelsInEditUserDialog() {
        final UserBrowserPage userBrowserPage = jira.goTo(UserBrowserPage.class, "jira-core");
        final AUIHelpTip auiHelpTip = new AUIHelpTip(jira);
        auiHelpTip.closeAll();
        EditUserGroupsPage editUserGroups = userBrowserPage.getUserRows().iterator().next().editGroups();
        List<String> activeSuggestionLabels = editUserGroups.openSuggestions("jira-admin").getActiveSuggestionLabels();
        assertThat(activeSuggestionLabels, contains(GROUP_LABEL_ADMIN.toUpperCase(), GROUP_LABEL_JIRA_CORE.toUpperCase()));
    }

    @Test
    public void shouldFilterByUserSearchWhenFilterIsSet() {
        UserBrowserPage userBrowser = jira.goTo(UserBrowserPage.class).filterByUserSearch("fred");
        tableContainUsers(userBrowser, "fred");

        userBrowser = jira.goTo(UserBrowserPage.class).filterByUserSearch("@example.com");
        tableContainUsers(userBrowser, "admin", "fred", INACTIVE_USER_NAME, "no-access-user");

        userBrowser = jira.goTo(UserBrowserPage.class).filterByUserSearch("Administrator");
        tableContainUsers(userBrowser, "admin");
    }

    @Test
    public void shouldFilterByActiveSearchWhenFilterIsSet() {
        UserBrowserPage userBrowser = jira.goTo(UserBrowserPage.class).filterByActive("true");
        tableContainUsers(userBrowser, "admin", "fred", "no-access-user");

        userBrowser = jira.goTo(UserBrowserPage.class).filterByActive("false");
        tableContainUsers(userBrowser, INACTIVE_USER_NAME);

        userBrowser = jira.goTo(UserBrowserPage.class).filterByActive("");
        tableContainUsers(userBrowser, "admin", "fred", INACTIVE_USER_NAME, "no-access-user");
    }

    @Test
    public void coreShouldNotBeDisplayedWhenItIsImplicitOrExplicitWithAnotherAppRole() {
        final String username = "fred";
        backdoor.usersAndGroups().addUserToGroup(username, "jira-users");

        UserBrowserPage userBrowser = jira.goTo(UserBrowserPage.class);
        Poller.waitUntil(userBrowser.findRow(username).getApplicationRoleNames(), contains("Test Product"));

        backdoor.usersAndGroups().removeUserFromGroup(username, "test-product-users");
        userBrowser = jira.goTo(UserBrowserPage.class);
        Poller.waitUntil(userBrowser.findRow(username).getApplicationRoleNames(), contains("JIRA Core"));

        backdoor.usersAndGroups().removeUserFromGroup(username, "jira-users");
        userBrowser = jira.goTo(UserBrowserPage.class);
        Poller.waitUntil(userBrowser.findRow(username).getApplicationRoleNames(), emptyIterable());

        backdoor.usersAndGroups().addUserToGroup(username, "test-product-users");
        userBrowser = jira.goTo(UserBrowserPage.class);
        Poller.waitUntil(userBrowser.findRow(username).getApplicationRoleNames(), contains("Test Product"));
    }

    @Test
    public void coreShouldBeDisplayedWhenItIsExplicitWithAllOtherAppRolesExceeded() {
        final String username = "fred";
        backdoor.usersAndGroups().addUserToGroup(username, "jira-users");
        //blow up licence
        for (int i = 0; i < 3; i++) {
            final String blowUpTestLicenceUsername = "user-test-" + i;
            backdoor.usersAndGroups().addUser(blowUpTestLicenceUsername);
            backdoor.usersAndGroups().addUserToGroup(blowUpTestLicenceUsername, "test-product-users");
        }

        UserBrowserPage userBrowser = jira.goTo(UserBrowserPage.class);
        Poller.waitUntil(userBrowser.findRow(username).getApplicationRoleNames(), containsInAnyOrder("JIRA Core", "Test Product"));

        backdoor.usersAndGroups().removeUserFromGroup(username, "jira-users");
        for (int i = 0; i < 3; i++) {
            final String blowUpTestLicenceUsername = "user-test-" + i;
            backdoor.usersAndGroups().deleteUser(blowUpTestLicenceUsername);
        }
    }

    @Test
    @LoginAs(admin = true, targetPage = UserBrowserPage.class)
    public void shouldRemoveFilterFromUserBrowserAfterCreatingNewUser() {
        UserBrowserPage userBrowserPage = pageBinder.bind(UserBrowserPage.class);
        userBrowserPage.filterByApplication(ApplicationLicenseConstants.CORE_KEY);
        userBrowserPage = userBrowserPage.gotoAddUserPage().addUser("user").createUser();
        assertEquals("", userBrowserPage.filteredApplication());
    }

    private void tableContainUsers(final UserBrowserPage userBrowser, String... names) {
        final List<Matcher<? super UserBrowserPage.UserRow>> rowMatchers = ImmutableList.copyOf(names).stream()
                .map(TestUserBrowserPage::userByName)
                .collect(CollectorsUtil.toImmutableList());

        final Matcher<Iterable<? extends UserBrowserPage.UserRow>> userRowIterableMatcher = contains(rowMatchers);
        Poller.waitUntil(userBrowser.getUserRowsTimed(), userRowIterableMatcher);
    }

    private static Matcher<UserBrowserPage.UserRow> userByName(String userName) {
        return new FeatureMatcher<UserBrowserPage.UserRow, String>(Matchers.equalTo(userName), "user's name", "username") {
            @Override
            protected boolean matchesSafely(final UserBrowserPage.UserRow actual, final Description mismatch) {
                return super.matchesSafely(actual, mismatch);
            }

            @Override
            protected String featureValueOf(final UserBrowserPage.UserRow actual) {
                return actual.getUsername().split("\n")[0];
            }
        };
    }

}