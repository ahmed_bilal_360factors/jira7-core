package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.project.AcknowledgeDeleteProject;
import com.atlassian.jira.pageobjects.project.DeleteProjectPage;
import com.atlassian.jira.pageobjects.project.ProjectConfigActions;
import com.atlassian.jira.pageobjects.project.ViewProjectsPage;
import com.atlassian.jira.pageobjects.project.AcknowledgeDeleteProject;
import com.atlassian.jira.pageobjects.project.summary.ProjectSummaryPageTab;
import com.atlassian.jira.pageobjects.project.summary.EditProjectPageTab;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Web test for the project configuration summary page's pluggable view project operations block.
 *
 * @since v4.4
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS, Category.IGNITE})
@Restore("xml/projectconfig/TestWebFragment.xml")
public class TestProjectOperations extends BaseJiraWebTest {
    private static final String PROJECT_HOMOSAP_KEY = "HSP";
    private static final Long PROJECT_HOMOSAP_ID = 10000L;
    private static final String PROJECT_HOMOSAP = "homosapien";
    private static final String NEW_HOMOSAP_NAME = PROJECT_HOMOSAP + " test";
    private static final String PROJECT_ADMIN = "project_admin";
    private static String baseUrl;

    @Before
    public void setUp() {
        baseUrl = jira.getProductInstance().getBaseUrl();
    }

    @Test
    public void testLinkVisibilityOnViewProjectForProjectAdmin() {
        jira.quickLogin(PROJECT_ADMIN, PROJECT_ADMIN);
        ProjectSummaryPageTab page = navigateToSummaryPageFor(PROJECT_HOMOSAP_KEY);
        ProjectConfigActions projectConfigActions = page.getOperations();

        assertFalse(projectConfigActions.hasItem(ProjectConfigActions.ProjectOperations.DELETE));
        assertFalse("Without any actions, there's no need for the dropdown button", projectConfigActions.hasDropdownActions());

        EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(PROJECT_HOMOSAP_ID);
        editProjectPageTab
                .setProjectName(NEW_HOMOSAP_NAME)
                .submit();

        assertEquals(NEW_HOMOSAP_NAME, backdoor.project().getProjectName(PROJECT_HOMOSAP_ID));
    }

    private ProjectSummaryPageTab navigateToSummaryPageFor(final String projectKey) {
        return pageBinder.navigateToAndBind(ProjectSummaryPageTab.class, projectKey);
    }

    private EditProjectPageTab navigateToEditProjectPageFor(final Long projectId) {
        return pageBinder.navigateToAndBind(EditProjectPageTab.class, String.valueOf(projectId));
    }
}
