package com.atlassian.jira.webtest.webdriver.tests.issue;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.dialogs.JiraDialog;
import com.atlassian.jira.pageobjects.pages.ManageFiltersPage;
import com.atlassian.jira.pageobjects.pages.ManageFiltersPage.Operation;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.openqa.selenium.By.cssSelector;

@WebTest({Category.WEBDRIVER_TEST, Category.FILTERS})
public class TestDeleteFilterWarningMessage extends BaseJiraWebTest {
    @Inject
    private PageBinder pageBinder;

    private long filterId;

    @Before
    public void setUp() throws Exception {
        filterId = Long.parseLong(backdoor.filters().createFilter("", "FilterToDelete", true));
        backdoor.filterSubscriptions().addSubscription(filterId, null, "0 0 0 ? 1 MON#3", false);
    }

    @Test
    @Restore("xml/blankprojects.xml")
    public void testDeleteFilterShowsSubscriptionWarningWhenApplicable() {
        ManageFiltersPage manageFiltersPage = pageBinder.navigateToAndBind(ManageFiltersPage.class);
        JiraDialog deleteDialog = manageFiltersPage.selectFilterOperation(filterId, Operation.DELETE);

        PageElement subscriptionWarningMessage = deleteDialog.find(cssSelector("p[data-subscription-count]"));
        waitUntilTrue(subscriptionWarningMessage.timed().isVisible());
        assertThat(subscriptionWarningMessage.getAttribute("data-subscription-count"), is("1"));
    }
}
