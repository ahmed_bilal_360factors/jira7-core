package com.atlassian.jira.webtest.webdriver.tests.admin.dashboards;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.gadgets.GadgetContainer;
import com.atlassian.jira.pageobjects.pages.AddDashboardPage;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.jira.pageobjects.util.browsermetrics.Beacon;
import com.atlassian.jira.pageobjects.util.browsermetrics.BrowserMetricsContext;
import com.atlassian.jira.pageobjects.util.browsermetrics.Checkpoint;
import it.com.atlassian.gadgets.pages.Gadget;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@WebTest({Category.WEBDRIVER_TEST, Category.DASHBOARDS})
public class TestDashboard extends BaseJiraWebTest {
    private final String localDashboardItem = "Introduction";

    private DashboardPage dashboardPage;
    private GadgetContainer gadgetContainer;

    @Inject
    private BrowserMetricsContext browserMetricsContext;

    @Before
    public void setUpTest() {
        backdoor.restoreDataFromResource("xml/blankprojects.xml");
        backdoor.flags().clearFlags();
        dashboardPage = pageBinder.navigateToAndBind(DashboardPage.class);
        gadgetContainer = dashboardPage.gadgets();
    }

    @Test
    public void testDashboardEmitsBrowserMetricsBeacon() {
        Checkpoint checkpoint = browserMetricsContext.checkpoint();

        pageBinder.navigateToAndBind(DashboardPage.class);
        Beacon beacon = checkpoint.waitFor("jira.dashboard");

        assertThat(beacon.report.readyForUser, greaterThan(beacon.report.domContentLoadedEventStart.get()));
        assertThat(beacon.report.isInitial, is(true));
    }

    @Test
    public void testDashboardIsProperlyInitialized() {
        backdoor.flags().clearFlags();

        createDashboard("test");
        addDashboardItem(localDashboardItem);

        final Gadget dashboardItem = gadgetContainer.getByTitle(localDashboardItem);
        assertThat(dashboardItem, notNullValue());

        gadgetContainer.waitForAuiBlanketToVanish();

        dashboardItem.delete();
        assertThat(gadgetContainer.hasGaddget(dashboardItem.getId()), is(false));
    }

    private void createDashboard(final String name) {
        final AddDashboardPage addDashboardPage = pageBinder.navigateToAndBind(AddDashboardPage.class);
        addDashboardPage.setName(name);
        addDashboardPage.submit();
        goToFavoriteDashboard(name);
    }

    private void goToFavoriteDashboard(String name) {
        dashboardPage.switchDashboard(name);
        gadgetContainer = dashboardPage.gadgets();
    }

    private void addDashboardItem(final String dashboardItemTitle) {
        gadgetContainer.openAddGadgetDialog().addGadget(dashboardItemTitle).simpleClose();
    }
}
