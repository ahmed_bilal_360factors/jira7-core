package com.atlassian.jira.webtest.webdriver.tests.fields;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.fields.MultiSelect;
import com.atlassian.jira.pageobjects.dialogs.quickedit.EditIssueDialog;
import com.atlassian.jira.pageobjects.dialogs.quickedit.FieldPicker;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.Version;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.util.stream.Collectors;

import static org.junit.Assert.assertThat;

@WebTest({Category.WEBDRIVER_TEST, Category.ISSUES})
public class TestMultiSelectPageObject extends BaseJiraWebTest {


    public static final String HSP = "HSP";


    @Before
    public void setUp() {
        backdoor.restoreBlankInstance();
        backdoor.issues().createIssue(HSP, "test");
    }

    @Test
    public void shouldBeAbleToAddAndRemoveVersion() {
        // this test verifies if MultiSelect class properly operates on multi select internal structures
        final String VERSION_ONE_POINT_OH = "1.0";

        backdoor.versions().create(new Version().name(VERSION_ONE_POINT_OH).project(HSP));

        final String issueKey = HSP+"-1";
        final ViewIssuePage viewIssuePage = jira.goToViewIssue(issueKey);
        final EditIssueDialog editIssueDialog = viewIssuePage.editIssue();

        final MultiSelect testObj = pageBinder.bind(MultiSelect.class, FieldPicker.AFFECTS_VERSIONS);
        testObj.add(VERSION_ONE_POINT_OH);
        editIssueDialog.submitExpectingViewIssue(issueKey);

        final Issue issue = backdoor.issues().getIssue(issueKey);
        assertThat(issue.fields.versions.stream().map(version -> version.name).collect(Collectors.toList()), Matchers.hasItem(VERSION_ONE_POINT_OH));

        final ViewIssuePage viewIssuePage2 = jira.goToViewIssue(issueKey);
        final EditIssueDialog editIssueDialog2 = viewIssuePage2.editIssue();
        final MultiSelect testObj2 = pageBinder.bind(MultiSelect.class, FieldPicker.AFFECTS_VERSIONS);
        testObj2.remove(VERSION_ONE_POINT_OH);
        editIssueDialog2.submitExpectingViewIssue(issueKey);

        final Issue issue2 = backdoor.issues().getIssue(issueKey);
        assertThat(issue2.fields.versions
                , Matchers.empty());
    }
}
