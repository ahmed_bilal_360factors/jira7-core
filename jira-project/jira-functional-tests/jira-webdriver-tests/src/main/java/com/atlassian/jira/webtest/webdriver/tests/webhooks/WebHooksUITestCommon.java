package com.atlassian.jira.webtest.webdriver.tests.webhooks;

import com.atlassian.jira.pageobjects.pages.webhooks.DisplayedWebHookListener;
import com.atlassian.jira.pageobjects.pages.webhooks.ViewWebHookListenerPanel;
import com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks.AbstractWebHookTest;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;

public class WebHooksUITestCommon extends AbstractWebHookTest {
    private WebHooksUITestCommon() {
    }

    public static Matcher<ViewWebHookListenerPanel> displaysListener(final DisplayedWebHookListener listener) {
        return new TypeSafeMatcher<ViewWebHookListenerPanel>() {
            @Override
            protected boolean matchesSafely(final ViewWebHookListenerPanel viewWebHookListenerPanel) {

                return viewWebHookListenerPanel.getDisplayedListener().equals(listener);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText(listener.toString());
            }
        };
    }

    public static Set<String> getAllJiraEvents() {
        return newHashSet("jira:issue_updated",
                "jira:issue_created",
                "jira:issue_deleted",
                "jira:worklog_updated",
                "jira:version_released",
                "jira:version_unreleased",
                "jira:version_created",
                "jira:version_moved",
                "jira:version_updated",
                "jira:version_merged",
                "jira:version_deleted",
                "worklog_created",
                "worklog_updated",
                "worklog_deleted",
                "project_created",
                "project_updated",
                "project_deleted",
                "user_created",
                "option_voting_changed",
                "option_watching_changed",
                "option_unassigned_issues_changed",
                "option_subtasks_changed",
                "option_attachments_changed",
                "option_issuelinks_changed",
                "option_timetracking_changed"
        );
    }
}
