package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.project.components.Component;
import com.atlassian.jira.pageobjects.project.components.ComponentsPageTab;
import com.atlassian.jira.pageobjects.project.components.DeleteComponentDialog;
import com.atlassian.jira.pageobjects.project.components.EditComponentForm;
import com.atlassian.jira.pageobjects.project.summary.ProjectSummaryPageTab;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.testkit.client.restclient.SearchResult;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Test for the components panel.
 *
 * @since v4.4
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS})
@Restore("xml/projectconfig/ComponentsConfig.xml")
public class TestComponentsPanel extends BaseJiraWebTest {
    private static final String PROJECT_NO_COMPONENTS = "MKY";
    private static final String PROJECT_WITH_COMPONENTS = "HSP";

    @Before
    public void setUp() throws Exception {
        backdoor.darkFeatures().enableForSite("jira.project.config.old.components.screen.enabled");
    }

    @After
    public void tearDown() throws Exception {
        backdoor.darkFeatures().disableForSite("jira.project.config.old.components.screen.enabled");
    }

    @Test
    public void testTabNavigation() {
        final ProjectSummaryPageTab config = jira.quickLoginAsSysadmin(ProjectSummaryPageTab.class, PROJECT_NO_COMPONENTS);
        final ComponentsPageTab componentsPage = config.getTabs().gotoComponentsTab();
        assertTrue(config.getTabs().isComponentsTabSelected());
        assertTrue(componentsPage.getComponents().isEmpty());
        assertEquals(PROJECT_NO_COMPONENTS, componentsPage.getProjectKey());
    }

    @Test
    public void testNoComponent() {
        final ComponentsPageTab componentsPage = jira.quickLoginAsSysadmin(ComponentsPageTab.class, PROJECT_NO_COMPONENTS);
        assertTrue(componentsPage.getComponents().isEmpty());
        assertTrue(componentsPage.hasEmptyMessage());
    }

    @Test
    public void testCreateComponent() {
        final ComponentsPageTab componentsPage = jira.quickLoginAsSysadmin(ComponentsPageTab.class, PROJECT_WITH_COMPONENTS);

        EditComponentForm createComponentForm = componentsPage.getCreateComponentForm()
                .fill("New component 1", "I am new component 1", "admin", "PROJECT_LEAD")
                .submit();

        assertEquals("A component with the name New component 1 already exists in this project.",
                createComponentForm.getNameField().getError());

        assertNull("Expected no error for description field",
                createComponentForm.getDescriptionField().getError());

        assertNull("Expected no error for component lead field",
                createComponentForm.getComponentLeadField().getError());

        assertNull("Expected no error for default assignee field",
                createComponentForm.getDefaultAssigneeField().getError());

        componentsPage.getCreateComponentForm()
                .fill("Really New component 1", "I am new component 1", "admin", "PROJECT_LEAD")
                .submit();

        assertNull("Expected no error for name field",
                createComponentForm.getNameField().getError());

        final Component newComponent = componentsPage.getComponentByName("Really New component 1");

        assertNotNull(newComponent);

        assertEquals("Really New component 1", newComponent.getName());
        assertEquals("I am new component 1", newComponent.getDescription());
        assertEquals("Administrator", newComponent.getLeadLink().getUser().getFullName());
        assertEquals(Component.ComponentAssigneeType.PROJECT_LEAD, newComponent.getComponentAssigneeType());
    }

    @Test
    public void testEditComponent() {
        final ComponentsPageTab componentsPage = jira.quickLoginAsSysadmin(ComponentsPageTab.class, PROJECT_WITH_COMPONENTS);

        final EditComponentForm editComponentForm = componentsPage.getComponentByName("New Component 1")
                .edit("name")
                .fill("New Component 3", null, "admin", "COMPONENT_LEAD")
                .submit();

        assertEquals(editComponentForm.getNameField().getError(),
                "A component with the name New Component 3 already exists in this project.");

        editComponentForm.fill("Changed New Component 1", "A new description", "admin", "COMPONENT_LEAD")
                .submit();

        Component changedComponent = componentsPage.getComponentByName("Changed New Component 1");

        assertEquals("Changed New Component 1", changedComponent.getName());
        assertEquals("A new description", changedComponent.getDescription());
        assertEquals("Administrator", changedComponent.getLeadLink().getUser().getFullName());
        assertEquals(Component.ComponentAssigneeType.COMPONENT_LEAD, changedComponent.getComponentAssigneeType());


        componentsPage.getComponentByName("New Component 3")
                .edit("name")
                .fill("New Component 3", "", "admin", "Unassigned")
                .submit();

        changedComponent = componentsPage.getComponentByName("New Component 3");

        assertEquals("admin", changedComponent.getLead().getUserName());

        componentsPage.getComponentByName("New Component 3")
                .edit("name")
                .fill("New Component 3", "", "", "Unassigned")
                .submit();

        changedComponent = componentsPage.getComponentByName("New Component 3");

        assertNull("Expected component lead to be removed", changedComponent.getLead());

    }

    @Test
    @Restore("xml/projectconfig/BadAssigneeTypeAndLead.xml")
    public void testInvalidComponentLead() {
        final ComponentsPageTab componentsPage = jira.quickLoginAsSysadmin(ComponentsPageTab.class, "BAIN");

        final Component component = componentsPage.getComponentByName("Bad Lead");

        assertTrue("Expected component lead [mark] to be invalid as the LDAP directory he existed in is offline",
                component.hasInvalidLead());
    }

    @Test
    public void testDeleteComponent() {
        backdoor.darkFeatures().enableForSite("ka.KILL_SWITCH");
        // New Component 1 removal (1 related issues)

        ComponentsPageTab componentsPage = jira.quickLoginAsSysadmin(ComponentsPageTab.class, "DEL");

        DeleteComponentDialog deleteComponentDialog = componentsPage.getComponentByName("New Component 1")
                .delete();

        assertTrue("Expected delete component dialog to have swap component operation",
                deleteComponentDialog.hasComponentSwapOperation());

        assertTrue("Expected delete component dialog to have remove component operation",
                deleteComponentDialog.hasComponentRemoveOperation());

        assertEquals("Expected issues with component to be 1", 1,
                deleteComponentDialog.getIssuesInComponentCount());

        deleteComponentDialog.setSwapComponent("New Component 3");

        deleteComponentDialog.submit();

        assertNull("Expected New Component 1 to be removed",
                componentsPage.getComponentByName("New Component 1"));

        SearchResult search = backdoor.search().getSearch(new SearchRequest().jql("project = \"Delete Component\" AND component=\"New Component 1\""));
        assertEquals(0, search.issues.size());

        search = backdoor.search().getSearch(new SearchRequest().jql("project =\"Delete Component\" AND component=\"New Component 3\""));
        assertEquals(1, search.issues.size());

        componentsPage = pageBinder.navigateToAndBind(ComponentsPageTab.class, "DEL");

        assertNull("Expected New Component 1 to be removed",
                componentsPage.getComponentByName("New Component 1"));


        // New Component 2 removal (No related issues)

        deleteComponentDialog = componentsPage.getComponentByName("New Component 2").delete();

        assertEquals("There are no issues that use this component. It is safe to delete.",
                deleteComponentDialog.getInfoMessage());

        assertFalse("Expected delete component dialog NOT to have swap component operation",
                deleteComponentDialog.hasComponentSwapOperation());

        assertFalse("Expected delete component dialog NOT to have remove component operation",
                deleteComponentDialog.hasComponentRemoveOperation());

        componentsPage = deleteComponentDialog.submit();

        assertNull("Expected New Component 2 to be removed",
                componentsPage.getComponentByName("New Component 2"));


        // New Component 3 removal (1 related issues)

        deleteComponentDialog = componentsPage.getComponentByName("New Component 3").delete();

        assertFalse("Should be no remove operation as there are no other components",
                deleteComponentDialog.hasComponentRemoveOperation());

        assertFalse("Should be no swap operation as there are no other components",
                deleteComponentDialog.hasComponentSwapOperation());

        componentsPage = deleteComponentDialog.submit();

        assertNull("Expected New Component 3 to be removed",
                componentsPage.getComponentByName("New Component 3"));

        assertTrue("Expected message indicating there are no components",
                componentsPage.hasEmptyMessage());

        search = backdoor.search().getSearch(new SearchRequest().jql("project = \"Delete Component\" AND component is not EMPTY"));
        assertEquals(0, search.issues.size());
    }

    @Test
    @Restore("xml/projectconfig/BadAssigneeTypeAndLead.xml")
    public void testInvalidAssigneeType() {
        final ComponentsPageTab componentsPage = jira.quickLoginAsSysadmin(ComponentsPageTab.class, "FRH");

        assertTrue("Expected Component 1 to be invalid assignee as Component Lead is not assignable",
                componentsPage.getComponentByName("Component 1").hasInvalidAssignee());

        assertTrue("Expected Component 2 to be invalid assignee as Project Lead is not assignable",
                componentsPage.getComponentByName("Component 2").hasInvalidAssignee());

        assertTrue("Expected Component 2 to be invalid assignee as Project Default is not assignable",
                componentsPage.getComponentByName("Component 3").hasInvalidAssignee());

        assertTrue("Expected Component 4 to be invalid assignee as un-assigned issues are not allowed",
                componentsPage.getComponentByName("Component 4").hasInvalidAssignee());
    }
}
