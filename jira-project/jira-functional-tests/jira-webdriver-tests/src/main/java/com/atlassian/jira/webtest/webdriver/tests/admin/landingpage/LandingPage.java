package com.atlassian.jira.webtest.webdriver.tests.admin.landingpage;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import javax.inject.Inject;

public class LandingPage extends AbstractJiraPage {
    @ElementBy(className = "add-project-dialog-header")
    private PageElement addProjectDialog;

    @ElementBy(className = "button-panel-cancel-link")
    private PageElement cancelButton;

    @ElementBy(className = "create-project-dialog-create-button")
    private PageElement nextButton;

    @ElementBy(className = "template-info-dialog-create-button")
    private PageElement selectButton;

    @ElementBy(id = "name")
    private PageElement nameInput;

    @ElementBy(id = "key")
    private PageElement keyInput;

    @ElementBy(className = "add-project-dialog-create-button")
    private PageElement createButton;

    @Inject
    PageBinder binder;

    @Override
    public TimedCondition isAt() {
        return addProjectDialog.timed().isPresent();
    }

    @Override
    public String getUrl() {
        return "/secure/LandingPage.jspa?product=jira-core";
    }

    public void next() {
        nextButton.click();
    }

    public void select() {
        selectButton.click();
    }

    public void setName(final String name) {
        nameInput.type(name);
    }

    public void setKey(final String key) {
        keyInput.type(key);
    }

    public void create() {
        createButton.click();
    }

    public void cancel() {
        cancelButton.click();
    }
}
