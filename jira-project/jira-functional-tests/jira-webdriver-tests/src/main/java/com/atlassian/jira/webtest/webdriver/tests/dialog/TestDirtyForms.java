package com.atlassian.jira.webtest.webdriver.tests.dialog;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.config.ResetData;
import com.atlassian.jira.pageobjects.dialogs.quickedit.CreateIssueDialog;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.jira.pageobjects.pages.viewissue.AddCommentSection;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.pageobjects.DelayedBinder;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.Tester;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;

import javax.inject.Inject;

import static com.atlassian.jira.pageobjects.dialogs.quickedit.FieldPicker.SUMMARY;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 *
 * @since v7.2
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION})
@ResetData
public class TestDirtyForms extends BaseJiraWebTest {

    private static final String HSP = "HSP";
    private static final String DIRTY_TEXT = "Make the form dirty";
    private static final String LEAVE_PAGE_ALERT_TEXT = "This page is asking you to confirm that you want to leave - data you have entered may not be saved.";
    private static final String DIRTY_CREATE_DIALOG_CLOSE_ALERT_TEXT = "You have entered new data on this page. If you navigate away from this page without first saving your data, the changes will be lost.";
    private static final String DIRTY_COMMENT_CANCEL_ALERT_TEXT = "You have entered a comment on this issue. If you navigate away from this page without first saving, the comment will be lost.";

    @Inject
    Tester tester;

    @Inject
    ProductInstance product;

    @Inject
    WebDriver driver;

    @Inject
    private PageElementFinder pageElementFinder;

    @Test
    public void navigateAwayFromCreateIssueDialog() throws Exception {
        jira.gotoHomePage();
        pageBinder.bind(JiraHeader.class).createIssue();

        jira.gotoHomePage();
    }

    @Test
    public void dirtyFormOnNavigatingAwayFromCreateIssueDialog() throws Exception {
        jira.gotoHomePage();
        CreateIssueDialog createIssueDialog = pageBinder.bind(JiraHeader.class).createIssue();
        createIssueDialog.fill(SUMMARY, DIRTY_TEXT);

        jira.visitDelayed(DashboardPage.class);
        dismissAlert(LEAVE_PAGE_ALERT_TEXT);
        createIssueDialog = pageBinder.bind(CreateIssueDialog.class, CreateIssueDialog.Type.ISSUE);
        assertThat(createIssueDialog.getFieldValue(SUMMARY), equalTo(DIRTY_TEXT));

        final DelayedBinder<DashboardPage> dashboardPageDelayedBinder = jira.visitDelayed(DashboardPage.class);
        acceptAlert(LEAVE_PAGE_ALERT_TEXT);

        dashboardPageDelayedBinder.bind();
    }

    @Test
    public void closeCreateIssueDialog() throws Exception {
        jira.gotoHomePage();
        final CreateIssueDialog createIssueDialog = pageBinder.bind(JiraHeader.class).createIssue();

        createIssueDialog.close();

        pageBinder.bind(DashboardPage.class);
    }

    @Test
    public void dirtyFormOnClosingCreateIssueDialog() throws Exception {
        jira.gotoHomePage();
        CreateIssueDialog createIssueDialog = pageBinder.bind(JiraHeader.class).createIssue();
        createIssueDialog.fill(SUMMARY, DIRTY_TEXT);

        createIssueDialog.closeNoWait();
        dismissAlert(DIRTY_CREATE_DIALOG_CLOSE_ALERT_TEXT);
        createIssueDialog = pageBinder.bind(CreateIssueDialog.class, CreateIssueDialog.Type.ISSUE);
        assertThat(createIssueDialog.getFieldValue(SUMMARY), equalTo(DIRTY_TEXT));

        createIssueDialog.closeNoWait();
        acceptAlert(DIRTY_CREATE_DIALOG_CLOSE_ALERT_TEXT);
        pageBinder.bind(DashboardPage.class);
    }

    @Test
    public void navigateAwayFromEmptyComment() throws Exception {
        final String issueKey = backdoor.issues().createIssue(HSP, "Test").key();
        final ViewIssuePage viewIssuePage = jira.goToViewIssue(issueKey);
        viewIssuePage.comment();

        jira.goToViewIssue(issueKey);
    }

    @Test
    public void dirtyFormOnNavigatingAwayFromComment() throws Exception {
        final String issueKey = backdoor.issues().createIssue(HSP, "Test").key();
        final ViewIssuePage viewIssuePage = jira.goToViewIssue(issueKey);
        AddCommentSection comment = viewIssuePage.comment();
        comment.typeComment(DIRTY_TEXT);

        jira.visitDelayed(DashboardPage.class);
        dismissAlert(LEAVE_PAGE_ALERT_TEXT);
        comment = pageBinder.bind(AddCommentSection.class, viewIssuePage);
        assertThat(comment.getComment(), equalTo(DIRTY_TEXT));

        final DelayedBinder<DashboardPage> dashboardPageDelayedBinder = jira.visitDelayed(DashboardPage.class);
        acceptAlert(LEAVE_PAGE_ALERT_TEXT);

        dashboardPageDelayedBinder.bind();
    }

    @Test
    public void cancelEmptyComment() throws Exception {
        final String issueKey = backdoor.issues().createIssue(HSP, "Test").key();
        final ViewIssuePage viewIssuePage = jira.goToViewIssue(issueKey);
        viewIssuePage.comment().cancel();

        pageBinder.bind(ViewIssuePage.class, issueKey);
    }

    @Test
    public void dirtyFormOnCancelingComment() throws Exception {
        final String issueKey = backdoor.issues().createIssue(HSP, "Test").key();
        final ViewIssuePage viewIssuePage = jira.goToViewIssue(issueKey);
        AddCommentSection comment = viewIssuePage.comment();
        comment.typeComment(DIRTY_TEXT);

        comment.cancelNoWait();
        dismissAlert(DIRTY_COMMENT_CANCEL_ALERT_TEXT);
        comment = pageBinder.bind(AddCommentSection.class, viewIssuePage);
        assertThat(comment.getComment(), equalTo(DIRTY_TEXT));

        comment.cancelNoWait();
        acceptAlert(DIRTY_COMMENT_CANCEL_ALERT_TEXT);

        pageBinder.bind(ViewIssuePage.class, issueKey);
    }

    private void acceptAlert(final String text) {
        assertAlertText(text).accept();
    }

    private void dismissAlert(final String text) {
        assertAlertText(text).dismiss();
    }

    private Alert assertAlertText(final String text) {
        final Alert alert = driver.switchTo().alert();
        assertThat(alert.getText(), containsString(text));
        return alert;
    }

}
