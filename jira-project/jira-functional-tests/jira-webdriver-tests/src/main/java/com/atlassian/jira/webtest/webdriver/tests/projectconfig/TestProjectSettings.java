package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.project.ReferenceProjectSettingsPage;
import com.atlassian.jira.pageobjects.project.components.ComponentsPageTab;
import com.atlassian.jira.pageobjects.project.summary.EditProjectPageTab;
import com.atlassian.jira.pageobjects.project.summary.ProjectSummaryPageTab;
import com.atlassian.jira.projects.pageobjects.webdriver.page.sidebar.Sidebar;
import com.atlassian.pageobjects.elements.query.Poller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Web test for the project configuration summary page.
 *
 * @since v4.4
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS, Category.IGNITE})
@RestoreOnce("xml/projectconfig/TestProjectConfigSummary.xml")
public class TestProjectSettings extends BaseJiraWebTest {
    private static final String HSP_KEY = "HSP";
    private static final Long HSP_ID = 10000L;

    private static final String PROJECT_ADMIN = "project_admin";

    @Before
    public void setUp() throws Exception {
        backdoor.darkFeatures().enableForSite("jira.project.config.old.components.screen.enabled");
    }

    @After
    public void tearDown() throws Exception {
        backdoor.darkFeatures().disableForSite("jira.project.config.old.components.screen.enabled");
    }

    @Test
    public void testSummaryTabNavigation() {
        ProjectSummaryPageTab summaryTab = navigateToSummaryPageFor(HSP_KEY);
        assertTrue(summaryTab.getTabs().isSummaryTabSelected());

        ComponentsPageTab componentsPage = summaryTab.getTabs().gotoComponentsTab();
        assertTrue(componentsPage.getTabs().isComponentsTabSelected());
        assertFalse(componentsPage.getTabs().isSummaryTabSelected());
        assertEquals(HSP_KEY, componentsPage.getProjectKey());

        summaryTab = componentsPage.getTabs().gotoSummaryTab();
        assertTrue(summaryTab.getTabs().isSummaryTabSelected());
        assertFalse(summaryTab.getTabs().isComponentsTabSelected());
        assertEquals(HSP_KEY, summaryTab.getProjectKey());
    }

    @Test
    public void testSidebarProjectSettingsGoesToHighestMenuItem() {
        navigateToProjectDetailsPageFor(HSP_ID);

        Sidebar sidebar = pageBinder.bind(Sidebar.class, HSP_KEY);

        sidebar.getProjectAdminLink().click();

        ReferenceProjectSettingsPage referencePage = pageBinder.bind(ReferenceProjectSettingsPage.class, HSP_KEY, HSP_ID);

        assertThat("Should have rendered the project key correctly", referencePage.getPath(), is("/" + HSP_KEY));
        assertThat("Should have rendered the project id correctly", referencePage.getQuery(), is("projectId=" + HSP_ID));
    }

    @Test
    public void testSidebarProjectSettingsGoesToHighestAvailableMenuItem() {
        jira.quickLogin(PROJECT_ADMIN, PROJECT_ADMIN);

        navigateToProjectDetailsPageFor(HSP_ID);

        Sidebar sidebar = pageBinder.bind(Sidebar.class, HSP_KEY);

        sidebar.getProjectAdminLink().click();

        ProjectSummaryPageTab expectedSelectedTab = pageBinder.bind(ProjectSummaryPageTab.class, HSP_KEY);
        Poller.waitUntilTrue(expectedSelectedTab.isAt());

    }

    private ProjectSummaryPageTab navigateToSummaryPageFor(final String projectKey) {
        return pageBinder.navigateToAndBind(ProjectSummaryPageTab.class, projectKey);
    }

    private EditProjectPageTab navigateToProjectDetailsPageFor(final Long projectId) {
        return pageBinder.navigateToAndBind(EditProjectPageTab.class, projectId.toString());
    }
}
