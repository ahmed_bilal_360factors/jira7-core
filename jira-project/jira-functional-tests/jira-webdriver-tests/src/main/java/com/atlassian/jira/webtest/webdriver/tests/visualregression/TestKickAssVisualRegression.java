package com.atlassian.jira.webtest.webdriver.tests.visualregression;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

/**
 * Webdriver test for visual regression on the screens from the kick-ass plugin.
 * <p>
 * Deals with issue nav and view issue.
 *
 * @since v5.0
 */
@WebTest({Category.WEBDRIVER_TEST, Category.VISUAL_REGRESSION})
@Restore("xml/TestVisualRegressionSmoke.zip")
public class TestKickAssVisualRegression extends JiraVisualRegressionTest {

    @Test
    public void testIssueNavAdvanced() {
        resizeWindowToDefaultDimentions();
        goTo("/browse/BULK-53?jql=cf%5B10010%5D%20%3D%20aaa%20ORDER%20BY%20issuekey%20DESC");
        clickOnElement(".switcher-item[data-id='basic']");
        PageElement advancedTextArea = pageElementFinder.find(By.id("advanced-search"));
        Poller.waitUntilTrue(advancedTextArea.timed().isVisible());
        PageElement issueList = pageElementFinder.find(By.className("list-panel"));
        issueList.type(Keys.HOME);
        clickOnElement("#content"); // Clicking on non-clickable element to fix flackiness
        visualComparer.setRefreshAfterResize(false);
        assertUIMatches("issue-nav-advanced");
    }

    @Test
    public void testIssueNavSimple() {
        resizeWindowToDefaultDimentions();
        goTo("/browse/BULK-53?jql=cf%5B10010%5D%20%3D%20aaa%20ORDER%20BY%20issuekey%20DESC");
        PageElement issueList = pageElementFinder.find(By.className("list-panel"));
        issueList.type(Keys.HOME);
        clickOnElement("#content"); // Clicking on non-clickable element to fix flackiness
        visualComparer.setRefreshAfterResize(false);
        assertUIMatches("issue-nav-simple");
    }
}
