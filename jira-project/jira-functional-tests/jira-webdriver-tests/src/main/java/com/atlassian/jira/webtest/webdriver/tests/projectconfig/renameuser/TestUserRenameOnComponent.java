package com.atlassian.jira.webtest.webdriver.tests.projectconfig.renameuser;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.admin.user.EditUserDetailsPage;
import com.atlassian.jira.pageobjects.project.components.Component;
import com.atlassian.jira.pageobjects.project.components.ComponentsPageTab;
import com.atlassian.jira.pageobjects.project.components.EditComponentForm;
import com.atlassian.jira.pageobjects.project.summary.ProjectSummaryPageTab;
import com.atlassian.jira.pageobjects.project.summary.components.ComponentsSummaryPanel;
import com.atlassian.jira.pageobjects.project.summary.components.ProjectComponent;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * @since v6.0
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS})
@Restore("xml/projectconfig/user_rename_doggy_components.xml")
//    KEY       USERNAME    NAME
//    bb	    betty	    Betty Boop
//    ID10001	bb	        Bob Belcher
//    cc	    cat	        Crazy Cat
//    ID10101	cc	        Candy Chaos

//    COMPONENT ID  COMPONENT NAME      LEAD_KEY    LEAD_DISPLAY_NAME
//    10100         Lean & Mean	        ID10101	    Candy Chaos
//    10000         Small & Ratty	    ID10001	    Bob Belcher
//    10001         Big & Wussy	        bb	        Betty Boop
//    10102         Wiry & Skittish	    cc	        Crazy Cat

public class TestUserRenameOnComponent extends BaseJiraWebTest {
    private Map<String, String> expectedComponentsAndLeads;
    private ComponentsPageTab componentsPage;
    private static final int MAX_SUMMARY_COMPONENTS_SHOWN = 5;

    @Before
    public void setUp() {
        backdoor.darkFeatures().enableForSite("jira.project.config.old.components.screen.enabled");

        expectedComponentsAndLeads = new HashMap<String, String>();
        expectedComponentsAndLeads.put("Lean & Mean", "Candy Chaos");
        expectedComponentsAndLeads.put("Small & Ratty", "Bob Belcher");
        expectedComponentsAndLeads.put("Big & Wussy", "Betty Boop");
        expectedComponentsAndLeads.put("Wiry & Skittish", "Crazy Cat");
        componentsPage = jira.goTo(ComponentsPageTab.class, "DOG");
    }

    @After
    public void tearDown() throws Exception {
        backdoor.darkFeatures().disableForSite("jira.project.config.old.components.screen.enabled");
    }

    @Test
    public void testCreateComponentWithRenamedAndRecycledUsers() {
        assertComponentLeadsOnProjectPanel();
        componentsPage.getCreateComponentForm()
                .fill("Hardworking & Loyal", "e.g. Sheepdog, Lassie", "bb", "COMPONENT_LEAD")
                .submit()
                .fill("Smart & Mischeivous", "e.g. Beagle", "betty", "COMPONENT_LEAD")
                .submit();
        expectedComponentsAndLeads.put("Hardworking & Loyal", "Bob Belcher");
        expectedComponentsAndLeads.put("Smart & Mischeivous", "Betty Boop");
        assertComponentLeadsOnProjectPanel();
        assertComponentLeadsOnSummaryTab();
    }

    @Test
    public void testLeadUserNamePreservedAcrossUserRename() {
        assertComponentLeadsOnProjectPanel();
        final EditUserDetailsPage userEditPage = jira.goTo(EditUserDetailsPage.class, "cc");
        userEditPage.fillUserName("candy");
        userEditPage.submit();
        componentsPage = jira.goTo(ComponentsPageTab.class, "DOG");
        assertComponentLeadsOnProjectPanel();
        assertComponentLeadsOnSummaryTab();
    }

    @Test
    public void testEditComponentWithRenamedAndRecycledUsers() {
        assertComponentLeadsOnProjectPanel();
        EditComponentForm leadFieldBeingEdited = componentsPage.getComponentByName("Wiry & Skittish").edit("leadUserName-field");
        assertEquals("Crazy Cat", leadFieldBeingEdited.getComponentLeadField().getValue());
        leadFieldBeingEdited.fill(null, null, "cc", null).submit();
        leadFieldBeingEdited = componentsPage.getComponentByName("Big & Wussy").edit("leadUserName-field");
        assertEquals("Betty Boop", leadFieldBeingEdited.getComponentLeadField().getValue());
        leadFieldBeingEdited.fill(null, null, "bb", null).submit();
        expectedComponentsAndLeads.remove("Wiry & Skittish");
        expectedComponentsAndLeads.put("Wiry & Skittish", "Candy Chaos");
        expectedComponentsAndLeads.remove("Big & Wussy");
        expectedComponentsAndLeads.put("Big & Wussy", "Bob Belcher");
        assertComponentLeadsOnProjectPanel();
        assertComponentLeadsOnSummaryTab();
    }

    private void assertComponentLeadsOnSummaryTab() {
        final ProjectSummaryPageTab summaryTab = componentsPage.getTabs().gotoSummaryTab();
        final ComponentsSummaryPanel componentsPanel = summaryTab.openPanel(ComponentsSummaryPanel.class);
        final List<ProjectComponent> componentsInSummaryPanel = componentsPanel.components();
        int expectedComponentsCount = expectedComponentsAndLeads.size();
        expectedComponentsCount = expectedComponentsCount <= MAX_SUMMARY_COMPONENTS_SHOWN ? expectedComponentsCount : MAX_SUMMARY_COMPONENTS_SHOWN;
        assertEquals(expectedComponentsCount, componentsInSummaryPanel.size());

        for (final ProjectComponent component : componentsInSummaryPanel) {
            final String componentName = component.getName();
            final String actualLeadFullName = component.getLead().getFullName();
            assertEquals(expectedComponentsAndLeads.get(componentName), actualLeadFullName);
        }
    }

    private void assertComponentLeadsOnProjectPanel() {
        final List<Component> components = componentsPage.getComponents();
        assertEquals(expectedComponentsAndLeads.size(), components.size());
        for (final Component component : components) {
            final String componentName = component.getName();
            assertEquals(expectedComponentsAndLeads.get(componentName), component.getLead().getFullName());
        }
    }
}
