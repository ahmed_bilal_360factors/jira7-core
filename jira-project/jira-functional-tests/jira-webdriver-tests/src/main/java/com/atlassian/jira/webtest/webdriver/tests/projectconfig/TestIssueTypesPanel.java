package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.project.issuetypes.IssueType;
import com.atlassian.jira.pageobjects.project.issuetypes.IssueTypeHeader;
import com.atlassian.jira.pageobjects.project.issuetypes.IssueTypesTab;
import com.atlassian.jira.rest.api.util.StringList;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.jira.testkit.client.restclient.IssueCreateMeta;
import com.atlassian.jira.webtests.util.LocalTestEnvironmentData;
import org.junit.Before;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Test for the notifications panel.
 *
 * @since v4.4
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS})
@RestoreOnce("xml/projectconfig/TestProjectConfigIssueTypesTab.xml")
public class TestIssueTypesPanel extends BaseJiraWebTest {
    private static final String PROJECT_WITH_CONFIGURED_SCHEMES = "MKY";
    private static final String PROJECT_WITH_DEFAULT_ISSUE_TYPES = "HSP";
    private static final String DEFAULT_WORKFLOW_NAME = "jira";
    private static final String BASE_WORKFLOW_URL = "/secure/admin/workflows/ViewWorkflowSteps.jspa?workflowMode=live&workflowName=";
    private static final String DEFAULT_FIELD_CONFIGURATION_NAME = "System Default Field Configuration";
    private static final String BASE_FIELD_CONFIGURATION_URL = "/secure/admin/ConfigureFieldLayout!default.jspa";
    private static final String DEFAULT_SCREEN_SCHEME_NAME = "Default Screen Scheme";
    private static final String BASE_SCREEN_SCHEME_URL = "/secure/admin/ConfigureFieldScreenScheme.jspa?id=";

    IssueClient issueClient;

    @Before
    public void setUp() throws Exception {
        issueClient = new IssueClient(new LocalTestEnvironmentData());
    }

    @Test
    public void testDefault() {
        final IssueTypesTab issueTypesTab = jira.goTo(IssueTypesTab.class, PROJECT_WITH_DEFAULT_ISSUE_TYPES);

        // Assert the edit and change links in the header
        assertTrue(issueTypesTab.isSchemeLinked());
        assertTrue(issueTypesTab.isSchemeChangeAvailable());
        assertEquals("Default Issue Type Scheme", issueTypesTab.getSchemeName());

        final List<IssueType> issueTypes = issueTypesTab.getIssueTypes();

        final int issueTypesNum = getAllIssueTypes(PROJECT_WITH_DEFAULT_ISSUE_TYPES).size();

        // There should be just the 4 default issue types, linking to the default schemes
        assertEquals(issueTypesNum, issueTypes.size());
        assertIssueType(issueTypes, "Bug", false);
        assertIssueType(issueTypes, "New Feature", false);
        assertIssueType(issueTypes, "Task", false);
        assertIssueType(issueTypes, "Improvement", false);
        assertIssueType(issueTypes, "Regression", false);
        assertIssueType(issueTypes, "QA", true);
        assertIssueType(issueTypes, "Doco", true);
    }

    @Test
    public void testConfigured() {
        final IssueTypesTab issueTypesTab = jira.goTo(IssueTypesTab.class, PROJECT_WITH_CONFIGURED_SCHEMES);

        // Assert the edit and change links in the header
        assertTrue(issueTypesTab.isSchemeLinked());
        assertTrue(issueTypesTab.isSchemeChangeAvailable());
        assertEquals("Test Issue Type Scheme", issueTypesTab.getSchemeName());

        final List<IssueType> issueTypes = issueTypesTab.getIssueTypes();

        assertEquals(5, issueTypes.size());
        assertIssueType(issueTypes, "Regression", false, getWorkflowLink("TestFlow2"), null,
                getFieldConfigurationLink("TestFieldConfig1", "10000"), null,
                getScreenSchemeLink("Regressing Screen Scheme", "10000"), null);
        assertIssueType(issueTypes, "Bug", false, getWorkflowLink(DEFAULT_WORKFLOW_NAME), null,
                getFieldConfigurationLink(DEFAULT_FIELD_CONFIGURATION_NAME, null), null,
                getScreenSchemeLink(DEFAULT_SCREEN_SCHEME_NAME, "1"), null);
        assertIssueType(issueTypes, "New Feature", false, getWorkflowLink("Test Flow 1 <script>"), null,
                getFieldConfigurationLink(DEFAULT_FIELD_CONFIGURATION_NAME, null), null,
                getScreenSchemeLink("Regressing Screen Scheme", "10000"), null);
        assertIssueType(issueTypes, "QA", true);
        assertIssueType(issueTypes, "Doco", true);

    }

    @Test
    public void testThatUserCanNavigateBackToIssueTypesFromSingleIssueTypeViewByUsingBreadcrumb() {
        final IssueTypesTab issueTypesTab = jira.goTo(IssueTypesTab.class, PROJECT_WITH_CONFIGURED_SCHEMES);
        final String projectsIssueTypePageUrl = issueTypesTab.getUrl();
        final List<IssueType> issueTypes = issueTypesTab.getIssueTypes();
        assertEquals(5, issueTypes.size());

        final IssueType issueType = issueTypes.get(2);
        final String issueTypeName = issueType.getName();
        final IssueTypeHeader issueTypeHeader = issueTypesTab.selectIssueType(issueType);
        final List<IssueTypeHeader.BreadCrumb> breadCrumbs = issueTypeHeader.getBreadCrumbs();

        // Make sure we are on the correct Issue Page
        assertThat(issueTypeHeader.getDescriptorTitle(), containsString(issueTypeName));

        // Verify that Issue Types breadcrumb is present with link
        final IssueTypeHeader.BreadCrumb issueTypesBreadcrumb = breadCrumbs.get(0);
        assertThat(issueTypesBreadcrumb.getLink().get(), containsString(projectsIssueTypePageUrl));
        assertThat(issueTypesBreadcrumb.isSelected(), is(false));

        // Make sure current breadcrumb is selected and displayed
        final IssueTypeHeader.BreadCrumb currentIssueTypeBreadCrumb = breadCrumbs.get(1);
        assertThat(currentIssueTypeBreadCrumb.isSelected(), is(true));
        assertThat(currentIssueTypeBreadCrumb.getDisplayName(), is(issueTypeName));
        assertThat(currentIssueTypeBreadCrumb.isAnchorTag(), is(false));

        // Click on Issue Types Link
        assertThat(issueTypesBreadcrumb.isAnchorTag(), is(true));
        issueTypesBreadcrumb.getPageElement().click();

        final IssueTypesTab issueTypesTabAfterNavigation = pageBinder.bind(IssueTypesTab.class, PROJECT_WITH_CONFIGURED_SCHEMES);
        assertEquals(5, issueTypesTabAfterNavigation.getIssueTypes().size());
    }

    @Test
    public void testNotAdminIssueTypes() {
        final IssueTypesTab issueTypesTab = jira.quickLogin("fred", "fred", IssueTypesTab.class, PROJECT_WITH_DEFAULT_ISSUE_TYPES);

        // Assert the cog actions aren't present
        assertFalse(issueTypesTab.isSchemeLinked());
        assertFalse(issueTypesTab.isSchemeChangeAvailable());

        final List<IssueType> issueTypes = issueTypesTab.getIssueTypes();

        final int issueTypesNum = getAllIssueTypes(PROJECT_WITH_DEFAULT_ISSUE_TYPES).size();

        assertEquals(issueTypesNum, issueTypes.size());
        assertIssueTypeNoAdmin(issueTypes, "Bug", false);
        assertIssueTypeNoAdmin(issueTypes, "New Feature", false);
        assertIssueTypeNoAdmin(issueTypes, "Task", false);
        assertIssueTypeNoAdmin(issueTypes, "Improvement", false);
        assertIssueTypeNoAdmin(issueTypes, "Regression", false);
        assertIssueTypeNoAdmin(issueTypes, "QA", true);
        assertIssueTypeNoAdmin(issueTypes, "Doco", true);
    }

    private void assertIssueType(final List<IssueType> issueTypes, final String issueTypeName, final boolean subTask) {
        assertIssueType(issueTypes, issueTypeName, subTask,
                getWorkflowLink(DEFAULT_WORKFLOW_NAME), null,
                getFieldConfigurationLink(DEFAULT_FIELD_CONFIGURATION_NAME, null), null,
                getScreenSchemeLink(DEFAULT_SCREEN_SCHEME_NAME, "1"), null);
    }

    private void assertIssueTypeNoAdmin(final List<IssueType> issueTypes, final String issueTypeName, final boolean subTask) {
        assertIssueType(issueTypes, issueTypeName, subTask,
                null, DEFAULT_WORKFLOW_NAME,
                null, DEFAULT_FIELD_CONFIGURATION_NAME,
                null, DEFAULT_SCREEN_SCHEME_NAME);
    }

    private void assertIssueType(final List<IssueType> issueTypes, final String name, final boolean subTask,
                                 final IssueType.Link workflowLink, final String workflowName,
                                 final IssueType.Link fieldConfigLink, final String fieldConfigName,
                                 final IssueType.Link screenSchemeLink, final String screenSchemeName) {
        // Find the entry in the list
        for (final IssueType issueType : issueTypes) {
            if (issueType.getName().equals(name)) {
                // assert the list is the same as that provided
                assertEquals("SubTask property did not match for :" + name, subTask, issueType.isSubtask());
                assertEquals("Workflow link did not match for :" + name, workflowLink, issueType.getWorkflow());
                assertEquals("Workflow name did not match for :" + name, workflowName, issueType.getWorkflowName());
                assertEquals("Field config link did not match for :" + name, fieldConfigLink, issueType.getFieldLayout());
                assertEquals("Field config name did not match for :" + name, fieldConfigName, issueType.getFieldLayoutName());
                assertEquals("Screen scheme link did not match for :" + name, screenSchemeLink, issueType.getScreenScheme());
                assertEquals("Screen scheme name did not match for :" + name, screenSchemeName, issueType.getScreenSchemeName());
                return;
            }
        }
        fail("Expected issueType '" + name + "' not found.");
    }

    private IssueType.Link getWorkflowLink(final String name) {
        try {
            return new IssueType.Link(name, jira.getProductInstance().getBaseUrl() + BASE_WORKFLOW_URL + URLEncoder.encode(name, StandardCharsets.UTF_8.name()));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private IssueType.Link getFieldConfigurationLink(final String name, final String id) {
        String url = BASE_FIELD_CONFIGURATION_URL;
        if (id != null) {
            url = url + "?id=" + id;
        }
        return new IssueType.Link(name, jira.getProductInstance().getBaseUrl() + url);
    }

    private IssueType.Link getScreenSchemeLink(final String name, final String id) {
        return new IssueType.Link(name, jira.getProductInstance().getBaseUrl() + BASE_SCREEN_SCHEME_URL + id);
    }

    private List<String> getAllIssueTypes(final String projectKey) {
        final List<String> issueTypesString = new ArrayList<String>();

        final IssueCreateMeta meta = issueClient.getCreateMeta(null, asList(new StringList(projectKey)), null, null, IssueCreateMeta.Expand.fields);
        final List<IssueCreateMeta.IssueType> issueTypes = meta.projects.get(0).issuetypes;

        for (final IssueCreateMeta.IssueType issueType : issueTypes) {
            issueTypesString.add(issueType.name);
        }

        return issueTypesString;
    }


}
