package com.atlassian.jira.webtest.webdriver.tests.home;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.navigator.AgnosticIssueNavigator;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.jira.pageobjects.pages.MinimalContentJiraPage;
import com.atlassian.jira.pageobjects.xsrf.XsrfPage;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Moved from ODAT.
 *
 * @since v6.2
 */
@WebTest({Category.WEBDRIVER_TEST})
public class TestMyJiraHome extends BaseJiraWebTest {
    @Inject
    private PageElementFinder elementFinder;

    private static final String JIRA_HOME_URL = "/secure/MyJiraHome.jspa";
    private static final String TEST_USER = "myjirahometest";
    private static final String TEST_USER_PASS = "k1isr34ffrts";

    @Before
    public void setup() {
        jira.backdoor().restoreBlankInstance();
        jira.backdoor().darkFeatures().enableForSite(FunctTestConstants.DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG);
        jira.backdoor().usersAndGroups().addUser(TEST_USER, TEST_USER_PASS, "Test User " + TEST_USER, TEST_USER + "@testusers.com");
        jira.quickLogin(TEST_USER, TEST_USER_PASS, MinimalContentJiraPage.class);
    }

    @Test
    public void testRedirectToIssueNavigator() throws Exception {
        dropDownUserPreferences();
        setJiraHomeToIssueNav();
        pageBinder.bind(AgnosticIssueNavigator.class); // should immediately go there
        pageBinder.navigateToAndBind(JiraHomeRedirectedToIssueNav.class);
    }

    @Test
    public void testRedirectToIssueNavigatorAfterLogin() {
        dropDownUserPreferences();
        setJiraHomeToIssueNav();
        pageBinder.bind(AgnosticIssueNavigator.class); // should immediately go there

        jira.logout().gotoLoginPage().performLoginSteps(TEST_USER, TEST_USER_PASS, false);
        pageBinder.bind(AgnosticIssueNavigator.class); // should be directed to issue nav
    }

    @Test
    public void testRedirectToDashboard() throws Exception {
        dropDownUserPreferences();
        setJiraHomeToDashboard();
        pageBinder.bind(DashboardPage.class); // should immediately go there
        pageBinder.navigateToAndBind(JiraHomeRedirectedToDashboard.class);
    }

    @Test
    public void testRedirectToDashboardAfterLogin() {
        dropDownUserPreferences();
        setJiraHomeToDashboard();
        pageBinder.bind(DashboardPage.class); // should immediately go there

        jira.logout().gotoLoginPage().performLoginSteps(TEST_USER, TEST_USER_PASS, false);
        pageBinder.bind(DashboardPage.class); // should be directed dashboard
    }

    @Test
    public void testXsrfProtection() throws Exception {
        dropDownUserPreferences();
        final PageElement updateToIssueNav = getSetHomeToIssueNavLink();
        final String targetLink = updateToIssueNav.getAttribute("href");

        assertThat("My JIRA Home update link has no href attribute", targetLink, is(notNullValue()));
        assertThat("My JIRA Home update link is missing the XSRF token", targetLink, containsString("atl_token"));

        final String targetLinkWithoutXsrfToken = makeRelativeLink(targetLink.replace("atl_token", "data"));

        jira.goTo(XsrfPage.class, targetLinkWithoutXsrfToken);
    }

    private void setJiraHomeToDashboard() {
        elementFinder.find(By.id("set_my_jira_home_default")).click();
    }

    private void setJiraHomeToIssueNav() {
        final PageElement setHomeToIssueNavLink = getSetHomeToIssueNavLink();
        /*
        * This is because of how aui-dropdown2 works. If you hover it as a user, it sets the aui-dropdown-active class.
        *   As we don't have a mouse pointer in Selenium, this solution is pretty good:
        *       - User hovers over 'Issue Navigator' (.select())
        *       - User clicks 'Issue Navigator' (.click())
        */
        setHomeToIssueNavLink.select();
        setHomeToIssueNavLink.click();
    }

    private PageElement getSetHomeToIssueNavLink() {
        return elementFinder.find(By.id("set_my_jira_home_issuenav"));
    }

    private void dropDownUserPreferences() {
        elementFinder.find(By.id("header-details-user-fullname")).click();
        waitUntilTrue(elementFinder.find(By.id("user-options-content")).timed().isVisible());
    }

    public static class JiraHomeRedirectedToIssueNav extends AgnosticIssueNavigator {
        @Override
        public String getUrl() {
            return TestMyJiraHome.JIRA_HOME_URL;
        }
    }

    public static class JiraHomeRedirectedToDashboard extends DashboardPage {
        @Override
        public String getUrl() {
            return TestMyJiraHome.JIRA_HOME_URL;
        }
    }

    private String makeRelativeLink(final String fullLink) {
        final JIRAEnvironmentData jiraEnvironmentData = jira.environmentData();

        return fullLink.substring(fullLink.indexOf('/', fullLink.indexOf(jiraEnvironmentData.getBaseUrl().getHost())))
                .substring(jiraEnvironmentData.getContext().length());
    }
}


