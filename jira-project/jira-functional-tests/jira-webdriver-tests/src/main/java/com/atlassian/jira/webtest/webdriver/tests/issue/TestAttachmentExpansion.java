package com.atlassian.jira.webtest.webdriver.tests.issue;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock.Sort;
import com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock.ViewMode;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.rule.Rules;
import com.atlassian.jira.pageobjects.JiraWebDriverTest;
import com.atlassian.jira.pageobjects.elements.ImageElement;
import com.atlassian.jira.pageobjects.model.FileDownloadInfo;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.ArchiveEntryItem;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.ArchiveFooter;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.AttachmentOptions;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.AttachmentRow;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.AttachmentSection;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.FileLinkElement;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.uri.Uri;
import com.google.common.base.Optional;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import java.util.Map;

import static com.atlassian.uri.Uri.fromJavaUri;
import static javax.ws.rs.core.UriBuilder.fromUri;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

@WebTest({Category.WEBDRIVER_TEST, Category.ATTACHMENTS})
@RestoreOnce("TestAttachmentExpansion/TestAttachmentExpansion.xml")
public class TestAttachmentExpansion extends JiraWebDriverTest {
    @ClassRule
    public static final TestRule prepareAttachments = (base, description) -> Rules.chain()
            .around(getBaseClassRule())
            .around(Rules.prepareAttachments(jira::environmentData, () -> new Backdoor(jira.environmentData()), "TestAttachmentExpansion/attachments"))
            .apply(base, description);
    private static final int ISSUE_ID = 10000;

    @Rule
    public final TestRule baseRule = getBaseRule();
    private Uri baseUri;
    private Map<String, AttachmentRow> attachmentRows;

    @Inject
    TraceContext traceContext;

    @Before
    public void visitViewIssuePage() throws Exception {
        baseUri = fromJavaUri(jira.environmentData().getBaseUrl().toURI());
        final ViewIssuePage viewIssuePage = jira.goToViewIssue("HSP-1");
        final AttachmentSection attachmentSection = viewIssuePage.getAttachmentSection();

        attachmentSection.openOptions().setViewMode(ViewMode.LIST);
        attachmentRows = attachmentSection.getAttachmentRowsByTitle();
    }

    @Test
    public void shouldExpandPetabyteBombFlatly() {
        final AttachmentRow row = getAttachmentRow("42-bomb.zip");
        assertEntryCount(row, 16);
    }

    private AttachmentRow getAttachmentRow(final String rowTitle) {
        final AttachmentRow row = attachmentRows.get(rowTitle);
        assertThat("an attachment titled " + rowTitle + " should exist", row, is(notNullValue()));
        return row;
    }

    private void assertEntryCount(final AttachmentRow row, final int expectedCount) {
        assertThat("entryCount", row.expandEntries(), hasSize(expectedCount));
    }

    @Test
    public void shouldEmitWarningForCorruptArchive() {
        final AttachmentRow row = getAttachmentRow("corrupt.zip");
        assertEntryCount(row, 0);
        assertWarning(row);
    }

    private void assertWarning(final AttachmentRow row) {
        assertThat("expansion warning", row.containsExpansionWarning(), is(true));
    }

    @Test
    public void shouldExpandArchiveWithLongPaths() {
        final AttachmentRow row = getAttachmentRow("deep.zip");
        assertEntryCount(row, 1);
        assertEntryTitle(row, 0, "long/path/thanks/to/.../reach/the/leaf.txt");
    }

    private void assertEntryTitle(final AttachmentRow row, final int entryIndex, final String expectedTitle) {
        final String actualTitle = entry(row, entryIndex).getTitle();
        assertThat("entry title " + entryIndex, actualTitle, is(equalTo(expectedTitle)));
    }

    private ArchiveEntryItem entry(final AttachmentRow row, final int index) {
        return row.expandEntries().get(index);
    }

    @Test
    public void shouldEmitWarningForEmptyArchive() {
        final AttachmentRow row = getAttachmentRow("empty.zip");
        assertEntryCount(row, 0);
        assertWarning(row);
    }

    @Test
    public void shouldExpandEncryptedArchive() {
        final AttachmentRow row = getAttachmentRow("encrypted.zip");
        assertEntryTitle(row, 0, "lorem-ipsum.txt");
    }

    @Test
    public void shouldProperlyEscapeHtml() {
        final AttachmentRow row = getAttachmentRow("escape & html.zip");
        assertEntryTitle(row, 0, "delta > 0.txt");
        assertEntryTitle(row, 1, "rock & roll.txt");
        assertEntryTitle(row, 2, "the amp here &amp; is expected.txt");
    }

    @Test
    public void shouldViewEntryIcons() {
        final AttachmentRow row = getAttachmentRow("real.zip");
        assertEntryIcon(row, 0, "aui-iconfont-devtools-file", "File");
        assertEntryIcon(row, 1, "aui-iconfont-file-txt", "Text File");
        assertEntryIcon(row, 2, "aui-iconfont-image", "JPEG File");
    }

    private void assertEntryIcon(
            final AttachmentRow row,
            final int entryIndex,
            final String expectedClassName,
            final String expectedAlternativeText
    ) {
        final ImageElement icon = entry(row, entryIndex).getIcon();
        assertThat("icon URI " + entryIndex, icon.getClassName(), is(equalTo(expectedClassName)));
        assertThat("alternative text " + entryIndex, icon.getAlternativeText(), is(equalTo(expectedAlternativeText)));
    }

    @Test
    public void shouldViewEntryFileSizes() {
        final AttachmentRow row = getAttachmentRow("real.zip");
        assertEntryFileSize(row, 0, "16 kB");
        assertEntryFileSize(row, 1, "2 kB");
        assertEntryFileSize(row, 2, "4 kB");
    }

    private void assertEntryFileSize(final AttachmentRow row, final int entryIndex, final String expectedFileSize) {
        assertThat("entry file size " + entryIndex, entry(row, entryIndex).getSize(), is(equalTo(expectedFileSize)));
    }

    @Test
    public void shouldExpandRecursiveBombFlatly() {
        final AttachmentRow row = getAttachmentRow("recursive-bomb.zip");
        assertEntryTitle(row, 0, "r/r.zip");
        assertEntryFileSize(row, 0, "0.4 kB");
    }

    @Test
    public void shouldLimitPresentedEntries() {
        final AttachmentRow row = getAttachmentRow("big-tree.zip");
        assertEntryCount(row, 30);
        final ArchiveFooter footer = row.getFooter();
        final Optional<String> limitInfo = footer.getLimitInfo();
        assertThat("footer limit info presence", limitInfo.isPresent(), is(true));
        assertThat("footer limit info label", limitInfo.get(), is(equalTo("Showing 30 of 50 items")));
    }

    @Test
    public void entriesShouldHaveProperIndexes() {
        final AttachmentRow row = getAttachmentRow("big-tree.zip");
        assertThat(entry(row, 1).getIndex(), is(equalTo(3)));
    }

    @Test
    public void shouldPreventMoreSophisticatedXss() {
        final AttachmentRow row = getAttachmentRow("XSS.zip");
        assertEntryTitle(row, 0, "\"<script>alert('XSS')</script>\".txt");
        assertEntryTitle(row, 1, "\"<script>alert('XSS')<:script>\".txt");
        assertEntryTitle(row, 2, "'<script>alert(\"XSS\")</script>'.txt");
        assertEntryTitle(row, 3, "'<script>alert(\"XSS\")<:script>'.txt");
        assertEntryTitle(row, 4, "<script>alert(\"XSS\")</script>.txt");
        assertEntryTitle(row, 4, "<script>alert(\"XSS\")</script>.txt");
        assertEntryTitle(row, 5, "<script>alert(\"XSS\")<:script>.txt");
        assertEntryTitle(row, 6, "<script>alert('XSS')</script>.txt");
        assertEntryTitle(row, 7, "<script>alert('XSS')<:script>.txt");
    }

    @Test
    public void entriesShouldBeDownloadableByClicking() throws Exception {
        final AttachmentRow row = getAttachmentRow("real.zip");
        final ArchiveEntryItem entry = entry(row, 0);
        final FileLinkElement link = entry.getFileLink();
        final Uri expectedFileUri = getExpectedDownloadUri(row.getAttachmentId(), entry.getIndex());
        assertThat(link.getFileUri(), is(equalTo(expectedFileUri)));
    }

    private Uri getExpectedDownloadUri(final long attachmentId, final int entryIndex) {
        return fromJavaUri(
                fromUri(baseUri.toJavaUri())
                        .path("secure")
                        .path("attachmentzip")
                        .path("unzip")
                        .path(String.valueOf(ISSUE_ID))
                        .path(attachmentId + "[" + entryIndex + "]")
                        .path("/")
                        .build()
        );
    }

    @Test
    public void entriesShouldBeDownloadableByDragging() throws Exception {
        final AttachmentRow row = getAttachmentRow("real.zip");
        final ArchiveEntryItem entry = entry(row, 0);
        final FileLinkElement link = entry.getFileLink();
        final Uri expectedFileUri = getExpectedDownloadUri(row.getAttachmentId(), entry.getIndex());
        final FileDownloadInfo downloadInfo = link.getDownloadInfo();
        assertThat("download media type", downloadInfo.getMediaType(), is(equalTo(MediaType.valueOf("audio/mpeg"))));
        assertThat("download file name ", downloadInfo.getFileName(), is(equalTo("beep-01a.mp3")));
        assertThat("download URI", downloadInfo.getDownloadUri(), is(equalTo(expectedFileUri)));
        assertThat(link + " should be marked as draggable", link.isDraggable(), is(true));
    }

    @Test
    public void shouldWireExpansionAfterViewModeSwitch() {
        final AttachmentSection attachmentSection = pageBinder.bind(AttachmentSection.class);
        attachmentSection.openOptions().setViewMode(ViewMode.GALLERY);

        Tracer tracer = traceContext.checkpoint();
        final AttachmentOptions attachmentOptions = attachmentSection.openOptions();
        attachmentOptions.setViewMode(ViewMode.LIST);
        traceContext.waitFor(tracer, "jira.plugins.viewissue.expansion.wired");
    }

    @Test
    public void shouldWireExpansionAfterSortChange() {
        final AttachmentSection attachmentSection = pageBinder.bind(AttachmentSection.class);
        attachmentSection.openOptions().setViewMode(ViewMode.LIST);
        attachmentSection.openOptions().setSortBy(Sort.Key.NAME);
        attachmentSection.openOptions().setSortOrder(Sort.Direction.ASCENDING);

        Tracer tracer = traceContext.checkpoint();
        AttachmentOptions attachmentOptions = attachmentSection.openOptions();
        attachmentOptions.setSortBy(Sort.Key.DATE);
        traceContext.waitFor(tracer, "jira.plugins.viewissue.expansion.wired");

        tracer = traceContext.checkpoint();
        attachmentOptions = attachmentSection.openOptions();
        attachmentOptions.setSortOrder(Sort.Direction.DESCENDING);
        traceContext.waitFor(tracer, "jira.plugins.viewissue.expansion.wired");
    }
}
