package com.atlassian.jira.webtest.webdriver.tests.admin;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.admin.SchedulerAdminPage;
import com.atlassian.jira.pageobjects.pages.admin.SchedulerAdminPage.JobRunnerRow;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.google.common.collect.Iterables;
import org.hamcrest.Matchers;
import org.hamcrest.collection.IsEmptyIterable;
import org.hamcrest.collection.IsIterableWithSize;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static com.atlassian.jira.matchers.OptionalMatchers.none;
import static java.util.stream.StreamSupport.stream;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION})
public class TestSchedulerAdmin extends BaseJiraWebTest {
    @Before
    public void setUp() {
        backdoor.restoreBlankInstance();
    }

    @Test
    public void smokeTestForSchedulerAdmin() {
        SchedulerAdminPage schedulerAdmin = jira.quickLoginAsAdmin(SchedulerAdminPage.class);


        final TimedQuery<Iterable<SchedulerAdminPage.JobRunnerRow>> jobRunnersQuery = schedulerAdmin.getJobRunners();
        //Can be 2 or 3 depending on if com.atlassian.jira.upgrade.DelayedUpgradeJobRunner is scheduled, which might be scheduled
        //depending on whether there are upgrade tasks pending in background
        Poller.waitUntil(jobRunnersQuery, not(IsEmptyIterable.<SchedulerAdminPage.JobRunnerRow>emptyIterable()));

        final Iterable<SchedulerAdminPage.JobRunnerRow> jobRunners = jobRunnersQuery.now();

        final Optional<JobRunnerRow> jobRunnerRowOptional = stream(jobRunners.spliterator(), false)
                .filter(
                        jobRunnerRow1 -> jobRunnerRow1
                                .getJobRunnerKey()
                                .equals("com.atlassian.jira.cache.monitor.CacheStatisticsMonitor"))
                .findFirst();
        assertThat(jobRunnerRowOptional, Matchers.not(none()));
        final JobRunnerRow jobRunnerRow = jobRunnerRowOptional.get();
        assertThat(jobRunnerRow.getJobRunnerKey(), equalTo("com.atlassian.jira.cache.monitor.CacheStatisticsMonitor"));
        assertThat(jobRunnerRow.getSchedule(), equalTo("interval"));
        assertThat(jobRunnerRow.getNumberOfJobs(), equalTo(1));

        final TimedQuery<Iterable<SchedulerAdminPage.JobDetailsRow>> jobDetailsQuery = schedulerAdmin.getJobDetails(jobRunnerRow.getId());
        Poller.waitUntil(jobDetailsQuery, IsIterableWithSize.<SchedulerAdminPage.JobDetailsRow>iterableWithSize(1));

        jobRunnerRow.showDetails();

        final SchedulerAdminPage.JobDetailsRow jobDetails = Iterables.get(jobDetailsQuery.now(), 0);
        assertThat(jobDetails.getType(), equalTo("Runnable"));
        assertThat(jobDetails.getParameters(), equalTo("{}"));
        assertThat(jobDetails.getRunMode(), equalTo("locally"));
        assertThat(jobDetails.getSchedule(), equalTo("1 day"));
        assertThat(jobDetails.getMessage(), equalTo(""));

        jobRunnerRow.hideDetails();

        final Optional<JobRunnerRow> jobRunnerRow2Optional = stream(jobRunners.spliterator(), false)
                .filter(
                        jobRunnerRow1 -> jobRunnerRow1
                                .getJobRunnerKey()
                                .equals("com.atlassian.jira.service.DefaultServiceManager"))
                .findFirst();
        assertThat(jobRunnerRow2Optional, Matchers.not(none()));
        final JobRunnerRow jobRunnerRow2 = jobRunnerRow2Optional.get();
        assertThat(jobRunnerRow2.getJobRunnerKey(), equalTo("com.atlassian.jira.service.DefaultServiceManager"));
        assertThat(jobRunnerRow2.getSchedule(), equalTo("cron"));
        assertThat(jobRunnerRow2.getNumberOfJobs(), equalTo(2));

        jobRunnerRow2.showDetails();

        final TimedQuery<Iterable<SchedulerAdminPage.JobDetailsRow>> jobDetailsQuery2 = schedulerAdmin.getJobDetails(jobRunnerRow2.getId());
        Poller.waitUntil(jobDetailsQuery2, IsIterableWithSize.<SchedulerAdminPage.JobDetailsRow>iterableWithSize(2));

        final SchedulerAdminPage.JobDetailsRow jobDetails2 = Iterables.get(jobDetailsQuery2.now(), 0);
        assertThat(jobDetails2.getType(), equalTo("Runnable"));
        assertThat(jobDetails2.getParameters(), equalTo("{com.atlassian.jira.service.ServiceManager:serviceId=10000}"));
        assertThat(jobDetails2.getRunMode(), equalTo("locally"));
        assertThat(jobDetails2.getMessage(), equalTo(""));

        jobRunnerRow2.hideDetails();
        Poller.waitUntilFalse(schedulerAdmin.isShowingJobDetails());

    }
}
