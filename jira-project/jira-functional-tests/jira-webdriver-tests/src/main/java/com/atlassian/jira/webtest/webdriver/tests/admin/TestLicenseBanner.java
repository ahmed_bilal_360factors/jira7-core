package com.atlassian.jira.webtest.webdriver.tests.admin;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.license.LicenseBanner;
import com.atlassian.jira.pageobjects.components.license.LicenseContent;
import com.atlassian.jira.pageobjects.components.license.LicenseFlag;
import com.atlassian.jira.pageobjects.components.license.UsersExceededBanner;
import com.atlassian.jira.pageobjects.config.EnableAnalytics;
import com.atlassian.jira.pageobjects.config.ResetDataOnce;
import com.atlassian.jira.pageobjects.elements.AuiFlag;
import com.atlassian.jira.pageobjects.elements.GlobalFlags;
import com.atlassian.jira.pageobjects.framework.util.TimedQueryFactory;
import com.atlassian.jira.pageobjects.pages.admin.application.ApplicationAccessPage;
import com.atlassian.jira.pageobjects.pages.admin.user.UserBrowserPage;
import com.atlassian.jira.pageobjects.project.ViewProjectsPage;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.test.util.lic.LicenseReader;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.webtests.LicenseKeys;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.sun.jersey.api.client.WebResource;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import java.util.Map;

import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.TEST_KEY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @since v6.3
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION})
@ResetDataOnce
public class TestLicenseBanner extends BaseJiraWebTest {
    private static final String MANAGE_APPLICATIONS_URL_ENDING = "applications/versions-licenses";
    @Inject
    private TraceContext context;
    @Inject
    private TimedQueryFactory queryFactory;

    /**
     * Evaluation license that will expire in 2 days.
     */
    private static final License EVAL_2 = readLicense("eval-2.lic");

    /**
     * Evaluation license that expires tomorrow.
     */
    private static final License EVAL_1 = readLicense("eval-1.lic");

    /**
     * ELA license that will expire in 91 days.
     */
    private static final License ELA_91 = readLicense("ela-91.lic");

    /**
     * ELA license that will expire in 90 days.
     */
    private static final License ELA_90 = readLicense("ela-90.lic");

    /**
     * ELA license that will expire in 46 days.
     */
    private static final License ELA_46 = readLicense("ela-46.lic");

    /**
     * ELA license that will expire in 45 days.
     */
    private static final License ELA_45 = readLicense("ela-45.lic");

    /**
     * ELA license that will expire in 31 days.
     */
    private static final License ELA_31 = readLicense("ela-31.lic");

    /**
     * ELA license that will expire in 30 days.
     */
    private static final License ELA_30 = readLicense("ela-30.lic");

    /**
     * ELA license that will expire in 25 days.
     */
    private static final License ELA_25 = readLicense("ela-25.lic");

    /**
     * ELA license that will expire in 15 days.
     */
    private static final License ELA_15 = readLicense("ela-15.lic");

    /**
     * ELA license that will expire in 8 days.
     */
    private static final License ELA_8 = readLicense("ela-8.lic");

    /**
     * ELA license that will expire in 7 days.
     */
    private static final License ELA_7 = readLicense("ela-7.lic");

    /**
     * ELA license that will expire in 7 days.
     */
    private static final License ELA_2 = readLicense("ela-2.lic");

    /**
     * ELA license that will expire in 0 days.
     */
    private static final License ELA_0 = readLicense("ela-0.lic");

    /**
     * ELA license that will expire in -1 days.
     */
    private static final License ELA_EXPIRED = readLicense("ela-expired.lic");

    /**
     * Perpetual license with maintenance expiry in 91 days.
     */
    private static final License PERPETUAL_91 = readLicense("perpetual-91.lic");

    /**
     * Perpetual license with maintenance expiry in 90 days.
     */
    private static final License PERPETUAL_90 = readLicense("perpetual-90.lic");

    /**
     * Perpetual license with maintenance expiry in 46 days.
     */
    private static final License PERPETUAL_46 = readLicense("perpetual-46.lic");

    /**
     * Perpetual license with maintenance expiry in 45 days.
     */
    private static final License PERPETUAL_45 = readLicense("perpetual-45.lic");

    /**
     * Perpetual license with maintenance expiry in 31 days.
     */
    private static final License PERPETUAL_31 = readLicense("perpetual-31.lic");

    /**
     * Perpetual license with maintenance expiry in 30 days.
     */
    private static final License PERPETUAL_30 = readLicense("perpetual-30.lic");

    /**
     * Perpetual license with maintenance expiry in 25 days.
     */
    private static final License PERPETUAL_25 = readLicense("perpetual-25.lic");

    /**
     * Perpetual license with maintenance expiry in 15 days.
     */
    private static final License PERPETUAL_15 = readLicense("perpetual-15.lic");

    /**
     * Perpetual license with maintenance expiry in 8 days.
     */
    private static final License PERPETUAL_8 = readLicense("perpetual-8.lic");

    /**
     * Perpetual license with maintenance expiry in 6 days.
     */
    private static final License PERPETUAL_6 = readLicense("perpetual-6.lic");

    /**
     * Perpetual license with maintenance expiry in 7 days.
     */
    private static final License PERPETUAL_7 = readLicense("perpetual-7.lic");

    /**
     * Perpetual license with maintenance expiry in 1 days.
     */
    private static final License PERPETUAL_1 = readLicense("perpetual-1.lic");

    /**
     * Perpetual license with maintenance expiry in -1 days.
     */
    private static final License PERPETUAL_EXPIRED = readLicense("perpetual-expired.lic");

    private static final String OTHER_ADMIN = "admin2";
    private static final String NON_ADMIN = "fred";

    private LicenseBannerRest bannerRestClient;

    private static License readLicense(final String resource) {
        return LicenseReader.readLicense(resource, TestLicenseBanner.class);
    }

    @Before
    public void init() {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_AND_TEST_ROLES);
        backdoor.flags().enableFlags();
        backdoor.usersAndGroups().addUser(OTHER_ADMIN).addUserToGroup(OTHER_ADMIN, "jira-administrators");
        backdoor.applicationRoles().putRole(TEST_KEY);
        bannerRestClient = new LicenseBannerRest(jira.environmentData());
        bannerRestClient.clearState();
    }

    @Test
    @EnableAnalytics
    public void evaluationLicenseShowsFlag() {
        backdoor.license().set(EVAL_2);

        final LicenseFlag flag = getLicenseFlag();
        assertThat(flag.isPresent(), Matchers.equalTo(true));
        assertThat(flag.days(), Matchers.equalTo(2));
        assertThat(flag.getMessage(), Matchers.containsString("trial"));
        assertMacUrl(flag);

        assertEvent("jira.expiry-notification.subscription.flag.show", "2");

        backdoor.analyticsEventsControl().clear();
        flag.clickMacLink();
        assertEvent("jira.expiry-notification.subscription.flag.click.information", "2");

    }

    @Test
    @EnableAnalytics
    public void evaluationLicenseShowsBanner() {
        backdoor.license().set(EVAL_1);

        final LicenseBanner banner = getBanner(LicenseBanner.class);
        assertSubscriptionBanner(banner, 1);
        String message = banner.getMessage();
        assertThat(message, Matchers.containsString("trial"));
        assertThat(message, Matchers.containsString("tomorrow"));

        assertEvent("jira.expiry-notification.subscription.banner.show", "1");

        backdoor.analyticsEventsControl().clear();
        banner.clickMackLink();
        assertEvent("jira.expiry-notification.subscription.banner.click.information", "1");
    }

    @Test
    public void subscriptionLicense() {
        //This license should not display because it is outside of 90 days.
        backdoor.license().replace(ELA_91);
        assertLicenseFlagDoesNotAppear();

        //ELA in the range of [90, 45) grouped and dismissed together.
        assertElaRemindMe(ELA_90, 90, ELA_46);

        //ELA in the range of [45, 30) grouped and dismissed together.
        assertElaRemindMe(ELA_45, 45, ELA_31);

        //ELA in the range of [30, 15) grouped and dismissed together.
        assertElaRemindMe(ELA_30, 30, ELA_25);

        //ELA in the range of [15, 8) grouped and dismissed together.
        assertElaRemindMe(ELA_15, 15, ELA_8);

        //ELA in the range of [7, 1) grouped and dismissed together.
        assertElaRemindMe(ELA_7, 7, ELA_2);

        //ELA in the  range of [1, -inf) is a banner
        assertElaShowsBanner(ELA_0, 0);
        assertElaShowsBanner(ELA_EXPIRED, -1);

        //This should reset the remind-me later.
        backdoor.license().replace(ELA_91);
        assertLicenseFlagDoesNotAppear();

        assertElaRemindMe(ELA_90, 90, ELA_46);

        //Flag is dismissed for admin but not for admin2.
        jira.quickLogin(OTHER_ADMIN, OTHER_ADMIN);
        getLicenseFlag();

        //Banner should not be visible for non-admin.
        jira.quickLogin(NON_ADMIN, NON_ADMIN);
        assertLicenseFlagDoesNotAppear();
    }

    @Test
    public void perpetualLicenseRemindLater() {
        backdoor.restoreBlankInstance();
        backdoor.flags().enableFlags();
        backdoor.usersAndGroups().addUser(OTHER_ADMIN).addUserToGroup(OTHER_ADMIN, "jira-administrators");
        //For the permission schemes.
        backdoor.usersAndGroups().addUserToGroup(OTHER_ADMIN, "jira-users");

        //This license should not display because it is outside of 90 days.
        backdoor.license().set(PERPETUAL_91);
        assertLicenseFlagDoesNotAppear();

        assertPerpetualRemindMe(PERPETUAL_90, 90, PERPETUAL_46);
        assertPerpetualRemindMe(PERPETUAL_45, 45, PERPETUAL_31);
        assertPerpetualRemindMe(PERPETUAL_30, 30, PERPETUAL_25);
        assertPerpetualRemindMe(PERPETUAL_15, 15, PERPETUAL_8);
        assertPerpetualRemindMe(PERPETUAL_7, 7, PERPETUAL_6);

        backdoor.license().set(PERPETUAL_EXPIRED);
        assertPerpetualFlag(getLicenseFlag(), -1);

        //This should reset the remind-me later.
        backdoor.license().set(PERPETUAL_91);
        assertLicenseFlagDoesNotAppear();

        backdoor.license().set(PERPETUAL_46);
        LicenseFlag flag = getLicenseFlag();
        assertPerpetualFlag(flag, 46);
        flag.remindLater();

        //Banner is dismissed for admin but not for admin2.
        jira.quickLogin(OTHER_ADMIN, OTHER_ADMIN);
        flag = getLicenseFlag();
        flag.dismiss();

        //Banner should not be visible for non-admin.
        jira.quickLogin(NON_ADMIN, NON_ADMIN);
        assertLicenseFlagDoesNotAppear();
    }

    @Test
    @EnableAnalytics
    public void testMaintenanceFlagAnalytics() {
        backdoor.restoreBlankInstance();
        backdoor.flags().enableFlags();

        backdoor.analyticsEventsControl().clear();

        backdoor.license().set(PERPETUAL_46);
        final LicenseFlag flag = getLicenseFlag();
        assertEvent("jira.expiry-notification.maintenance.flag.show", "46");

        backdoor.analyticsEventsControl().clear();
        flag.dismiss();
        assertEvent("jira.expiry-notification.maintenance.flag.close", "46");
    }

    @Test
    public void testLicenseExceededBannerLinksToFilteredUserBrowser() {
        backdoor.usersAndGroups().addUsersWithGroup("overTheLimit", "overlimit", 1, "jira-users");

        UsersExceededBanner banner = getBanner(UsersExceededBanner.class);

        assertThat(banner.isPresent(), Matchers.equalTo(true));
        assertMacUrl(banner);

        banner.getManageLink().click();
        UserBrowserPage userBrowser = pageBinder.bind(UserBrowserPage.class);

        Poller.waitUntilTrue(userBrowser.hasApplicationFilter());
        assertThat(userBrowser.getApplicationFilter(), Matchers.equalTo("jira-core"));
    }

    @Test
    public void testMultipleLicenseExceededBannerLinksToApplicationLicenses() {
        backdoor.usersAndGroups().addUsersWithGroup("overTheLimit", "overlimit", 3, "jira-users");
        backdoor.usersAndGroups().addUsersWithGroup("overTheLimitDev", "overlimitdev", 3, "jira-developers");

        final ApplicationAccessPage page = jira.goTo(ApplicationAccessPage.class);
        page.role(TEST_KEY).addGroup("jira-developers");

        UsersExceededBanner banner = getBanner(UsersExceededBanner.class);

        assertThat(banner.isPresent(), Matchers.equalTo(true));
        assertMacUrl(banner);
    }

    /**
     * Make sure the user can close the license banner for the passed perpetual license.
     *
     * @param triggerLicense the license that will trigger the banner.
     * @param triggerDays    the days until the license is to expire.
     * @param noopLicense    another license that is close enough to {@code triggerLicense} such that it wont trigger a
     *                       banner.
     */
    private void assertPerpetualRemindMe(final License triggerLicense, final int triggerDays, final License noopLicense) {
        backdoor.license().set(triggerLicense);

        //The trigger license should show the flag again.
        LicenseFlag flag = getLicenseFlag();
        assertPerpetualFlag(flag, triggerDays);

        flag.remindLater();

        //Check the flag not visible on page pop.
        assertLicenseFlagDoesNotAppear();

        //This license should not show the flag again.
        backdoor.license().set(noopLicense);
        assertLicenseFlagDoesNotAppear();
    }

    private void assertPerpetualFlag(final LicenseFlag flag, final int days) {
        assertThat(flag.isPresent(), Matchers.equalTo(true));
        assertThat(flag.days(), Matchers.equalTo(days));
        assertThat(flag.isSubscription(), Matchers.equalTo(false));
        assertMacUrl(flag);
    }

    /**
     * Make ELA license shows banner that cannot be dismissed
     *
     * @param triggerLicense the license to test.
     * @param triggerDays    the days until the license is to expire.
     */
    private void assertElaShowsBanner(final License triggerLicense, final int triggerDays) {
        backdoor.license().set(triggerLicense);
        assertTrue("Banner only shows from day before expiry", triggerDays < 2);

        assertSubscriptionBanner(getBanner(LicenseBanner.class), triggerDays);
        //The user has hit the REST resource to remind later directly.
        bannerRestClient.remindLater();
        assertSubscriptionBanner(getBanner(LicenseBanner.class), triggerDays);
    }

    /**
     * Make sure the user can close the license banner for the passed license.
     *
     * @param triggerLicense the license that will trigger the banner.
     * @param triggerDays    the days until the license is to expire.
     * @param noopLicense    another license that is close enough to {@code triggerLicense} such that it wont trigger a
     *                       banner.
     */
    private void assertElaRemindMe(final License triggerLicense, final int triggerDays, final License noopLicense) {
        //This license should trigger the banner.
        backdoor.license().replace(triggerLicense);
        LicenseFlag flag = getLicenseFlag();
        assertSubscriptionFlag(flag, triggerDays);
        flag.remindLater();

        assertLicenseFlagDoesNotAppear();
        //This license should not trigger a new banner.
        backdoor.license().replace(noopLicense);
        assertLicenseFlagDoesNotAppear();
    }

    private void assertSubscriptionBanner(final LicenseBanner banner, final int days) {
        assertThat(banner.isPresent(), Matchers.equalTo(true));
        assertThat(banner.isSubscription(), Matchers.equalTo(true));
        assertThat(banner.days(), Matchers.equalTo(days));
        assertMacUrl(banner);
    }

    private void assertSubscriptionFlag(final LicenseFlag flag, final int days) {
        assertThat(flag.isPresent(), Matchers.equalTo(true));
        assertThat(flag.isSubscription(), Matchers.equalTo(true));
        assertThat(flag.days(), Matchers.equalTo(days));
        assertMacUrl(flag);
    }

    private void assertMacUrl(final LicenseContent banner) {
        assertThat("The hyperlink lands you to manage applications screen",
                banner.getMacUrl().endsWith(MANAGE_APPLICATIONS_URL_ENDING));
    }

    private <T extends LicenseContent> T getBanner(final Class<T> bannerClass) {
        jira.goTo(ViewProjectsPage.class);
        return pageBinder.bind(bannerClass);
    }

    private void assertLicenseFlagDoesNotAppear() {
        jira.goTo(ViewProjectsPage.class);
        Poller.waitUntilTrue(context.conditionFromPageLoad("license-flag-checked"));
        GlobalFlags flags = pageBinder.bind(GlobalFlags.class);
        if (flags.isPresent() && !flags.getFlags().isEmpty()) {
            for (AuiFlag flag : flags.getFlags()) {
                PageElement flagContent = flag.getFlagElement().find(By.id("license-flag-content"));
                assertFalse(flagContent.isPresent());
            }
        }
    }

    private void assertEvent(String name, String daysToExpire) {
        Map eventMap = Poller.waitUntil(queryFactory.forSupplier(() -> backdoor.analyticsEventsControl().matchEvents(name)), Matchers.iterableWithSize(1)).get(0);
        assertEquals(daysToExpire, ((Map) eventMap.get("properties")).get("days_pre_expiry"));
    }

    private LicenseFlag getLicenseFlag() {
        jira.goTo(ViewProjectsPage.class);
        GlobalFlags flags = pageBinder.bind(GlobalFlags.class);
        assertThat(flags.isPresent(), Matchers.equalTo(true));
        AuiFlag flag = flags.getFlagWithElement(By.id("license-flag-content"));
        assertThat(flag, Matchers.notNullValue());
        return pageBinder.bind(LicenseFlag.class, flag);
    }

    private static class LicenseBannerRest extends RestApiClient<LicenseBannerRest> {
        protected LicenseBannerRest(final JIRAEnvironmentData environmentData) {
            super(environmentData);
        }

        public LicenseBannerRest clearState() {
            laterResource()
                    .type(MediaType.APPLICATION_JSON_TYPE)
                    .delete();

            return this;
        }

        public LicenseBannerRest remindLater() {
            laterResource()
                    .type(MediaType.APPLICATION_JSON_TYPE)
                    .post();

            return this;
        }

        private WebResource laterResource() {
            return bannerResource().path("remindlater");
        }

        private WebResource bannerResource() {
            return createResourceInternal().path("licensebanner");
        }
    }
}
