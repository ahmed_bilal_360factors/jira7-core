package com.atlassian.jira.webtest.webdriver.tests.setup;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.rule.SkipCacheCheck;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.pages.setup.SetupAccountPage;
import com.atlassian.jira.pageobjects.pages.setup.SetupFinishingPage;
import com.atlassian.jira.pageobjects.pages.setup.SetupModePage;
import com.atlassian.jira.webtest.webdriver.util.AUIFlags;
import com.atlassian.jira.webtests.LicenseKeys;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.atlassian.webdriver.utils.element.ElementLocated;
import com.atlassian.webdriver.utils.element.WebDriverPoller;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * ************************************************************************<br>
 * Run this test with -Dtest.jira.setup.skip=true
 * ************************************************************************<br>
 */
@WebTest({Category.WEBDRIVER_TEST, Category.SETUP})
@SkipCacheCheck
public class TestInstantSetup extends BaseJiraWebTest {
    private static final String USER_EMAIL = "charlie@atlassian.com";
    private static final String USER_NAME = "charlie";
    private static final String USER_PASSWORD = "1234";

    @Inject
    private WebDriverPoller webDriverPoller;

    @Inject
    private PageElementFinder finder;

    @Before
    public void ensureJiraISNotSetup() throws Exception {
        if (isJiraSetup()) {
            throw new IllegalStateException("JIRA must be in pristine state.");
        }
    }

    @Test
    @LoginAs(anonymous = true)
    public void testInstantSetupSucceeds() throws Exception {
        final SetupModePage setupModePage = pageBinder.bind(SetupModePage.class);
        setupModePage.selectMode(SetupModePage.ModeSelection.INSTANT);
        // we can't assert on the redirect here, because before webdriver gets hold of the url, we could have
        // been redirected by MAC, and possibly to the login page which hides the forwarding url.
        setupModePage.submitToInstantPath();

        final SetupFinishingPage setupFinishingPage = pageBinder
                .navigateToAndBind(SetupAccountPage.class, LicenseKeys.AGED_EVALUATION_LICENSE.getLicenseString().replace("+", "%2B"))
                .setEmail(USER_EMAIL)
                .setPasswordAndRetype(USER_PASSWORD)
                .clickNext();

        setupFinishingPage.waitToFinish();
        backdoor.darkFeatures().loginAs(USER_NAME, USER_PASSWORD).enableForSite(FunctTestConstants.DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG);
        setupFinishingPage.submit();
        assertJiraIsOnDashboard();

        // testing also if post setup announcements are properly integrated with instant setup
        // keeping method assertContainsPostSetupPopup in PostSetup class to make visible
        // where post setup steps are tested
        pageBinder.bind(AUIFlags.class).closeAllFlags();
        assertContainsPostSetupPopup();
    }

    public void assertContainsPostSetupPopup() {
        final String POST_SETUP_DB = "inline-dialog-post-setup-announcement-database-setup";
        webDriverPoller.waitUntil(new ElementLocated(By.id(POST_SETUP_DB)), 1);
    }

    private boolean isJiraSetup() {
        final String baseUrl = jira.getProductInstance().getBaseUrl();
        final WebDriverTester tester = jira.getTester();
        tester.gotoUrl(baseUrl);
        final String currentURL = tester.getDriver().getCurrentUrl();
        return (baseUrl + "/secure/Dashboard.jspa").equals(currentURL) || (baseUrl + "/login.jsp").equals(currentURL);
    }

    private void assertJiraIsOnDashboard() {
        final String baseUrl = jira.getProductInstance().getBaseUrl();
        final String currentURL = jira.getTester().getDriver().getCurrentUrl();

        assertThat(currentURL, is(equalTo(baseUrl + "/secure/Dashboard.jspa")));
    }
}
