package com.atlassian.jira.webtest.webdriver.tests.plugin.feedback;

import com.atlassian.feedback.DefaultIssueCollectorUrlManager;
import com.atlassian.feedback.FeedbackPluginDarkFeatures;
import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.dialogs.FeedbackDialog;
import com.atlassian.jira.pageobjects.pages.DashboardPageWithFeedbackButton;
import com.atlassian.jira.projects.pageobjects.webdriver.page.sidebar.Sidebar;
import com.atlassian.jira.projects.pageobjects.webdriver.page.sidebar.SidebarBrowseProjectPage;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the feedback button with the dark feature enabled.
 * The dark features disables the sidebar feedback button and enables the header feedback button.
 */
@WebTest({Category.WEBDRIVER_TEST})
public class TestFeedbackPlugin extends BaseJiraWebTest {

    private static final String PROJECT_NAME = "Test";
    private static final String PROJECT_KEY = "TEST";
    private static final String PROJECT_LEAD = "admin";
    private static final String ISSUE_COLLECTOR_RELATIVE_PATH = "s/0b39bcc88284d551be041647233b3b46-T/en_AU-yn8txh/72000/b6b48b2829824b869586ac216d119363/2..12/_/download/resources/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs/bootstrap.js?locale=en-AU&collectorId=b4b39ee5";

    @BeforeClass
    public static void setUp() {
        backdoor.restoreBlankInstance();
        backdoor.project().addProject(PROJECT_NAME, PROJECT_KEY, PROJECT_LEAD);
        backdoor.darkFeatures().enableForSite(FeedbackPluginDarkFeatures.ENABLE_HEADER_FEEDBACK_KEY);
    }

    @AfterClass
    public static void tearDown() {
        backdoor.darkFeatures().disableForSite(FeedbackPluginDarkFeatures.ENABLE_HEADER_FEEDBACK_KEY);
    }

    @Test
    public void headerFeedbackButtonShouldBeVisibleWhenDarkFeatureIsOn() {
        final DashboardPageWithFeedbackButton page = jira.goTo(DashboardPageWithFeedbackButton.class);

        assertTrue("There should be a feedback button in the header nav", page.hasFeedbackButton());
    }

    @Test
    public void sidebarFeedbackButtonShouldNotBeVisibleWhenDarkFeatureIsOn() {
        final SidebarBrowseProjectPage page = jira.goTo(SidebarBrowseProjectPage.class, PROJECT_KEY);
        Poller.waitUntilTrue(page.isAt());
        Sidebar.SidebarLink link = page.getSidebar().getLinkByName("Give feedback");

        assertFalse("The feedback button should not be in the sidebar", link.isVisible().now());
    }

    @Test
    @Restore("TestFeedbackPluginWithIssueCollectorSetup.xml")
    public void displaysIssueCollectorAfterClickingFeedbackButton() {
        backdoor.applicationProperties().setText(DefaultIssueCollectorUrlManager.JiraType.DEFAULT.getKey(), getIssueCollectorUrl());
        final DashboardPageWithFeedbackButton page = jira.goTo(DashboardPageWithFeedbackButton.class);

        FeedbackDialog dialog = page.clickFeedbackButton();

        assertTrue("Clicking on the feedback button should open a new dialog", dialog.isOpen().now());
    }

    /**
     * Constructs the issue collector url. This helps accommodate test instances that run with a different base
     * url or different port number.
     *
     * @return a concatenated url of the base url and issue collector path
     */
    public String getIssueCollectorUrl() {
        String base = backdoor.applicationProperties().getEnvironmentData().getBaseUrl().toExternalForm();
        UrlBuilder builder = new UrlBuilder(base);
        builder.addPathUnsafe(ISSUE_COLLECTOR_RELATIVE_PATH);

        return builder.asUrlString();
    }
}
