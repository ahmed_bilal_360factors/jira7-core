package com.atlassian.jira.webtest.webdriver.tests.admin.users;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.UserApplicationAccess;
import com.atlassian.jira.pageobjects.config.CreateUser;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.pages.admin.user.ViewUserPage;
import com.atlassian.jira.test.util.lic.ApplicationLicenseConstants;
import com.atlassian.jira.testkit.beans.ApplicationRole;
import com.atlassian.jira.testkit.client.restclient.User;
import com.atlassian.jira.testkit.client.restclient.UserClient;
import com.atlassian.jira.webtests.LicenseKeys;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.inject.Inject;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.GROUP_LABEL_ADMIN;
import static com.atlassian.jira.functest.framework.FunctTestConstants.GROUP_LABEL_JIRA_CORE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_ADMIN_GROUP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_DEV_GROUP;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * @since v7.0
 */
@WebTest({Category.WEBDRIVER_TEST, Category.USERS_AND_GROUPS})
public class TestViewUserPage extends BaseJiraWebTest {
    private final static String KEVIN_USERNAME = "kevin";
    private final static String BOB_USERNAME = "bob";
    private final static String STUART_USERNAME = "stuart";
    private final static String ALANA_USERNAME = "alana";
    private final static String NEW_ADMIN_USERNAME = "new-admin";
    private final static String EFFECTIVE_USERNAME = "effective";
    private final static String INDETERMINATE_USERNAME = "indeterminate";

    @Inject
    private JIRAEnvironmentData jiraEnvironmentData;

    private UserClient userClient;

    private static final String SOFTWARE_GROUP = "software-group";
    private static final String SD_GROUP = "service-desk-group";
    private static final String SD_GROUP_ADDITIONAL = "service-desk-group-additional";
    private static final String REFERENCE_GROUP = "reference-group";
    private static final String DEFAULT_GROUP = "default-group";
    private static final String NOT_DEFAULT_GROUP = "not-default-group";
    public static final String TEST_GROUP = "test-product-users";
    public static final String JIRA_CORE_GROUP = "jira-users";

    @BeforeClass
    public static void setUpInstance() {
        backdoor.restoreBlankInstance(LicenseKeys.MULTI_ROLE);

        backdoor.applicationRoles().putRole("jira-reference");
        backdoor.applicationRoles().putRoleAndSetDefault(ApplicationLicenseConstants.CORE_KEY, JIRA_CORE_GROUP);

        backdoor.usersAndGroups().deleteGroup("jira-reference-users");
        backdoor.usersAndGroups().deleteGroup("jira-ref-group");

        backdoor.usersAndGroups().addGroup(TEST_GROUP);
        backdoor.applicationRoles().putRoleAndSetDefault(ApplicationLicenseConstants.TEST_KEY, TEST_GROUP);
        backdoor.usersAndGroups().addUser("no-access-user");
        backdoor.usersAndGroups().addUserToGroup("fred", TEST_GROUP);
        backdoor.usersAndGroups().removeUserFromGroup("fred", JIRA_CORE_GROUP);

        backdoor.usersAndGroups().addGroup(SOFTWARE_GROUP);
        // Overriding the default applications that are set by the database patch
        backdoor.applicationRoles().putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.SOFTWARE_KEY, false, groups(SOFTWARE_GROUP), groups(SOFTWARE_GROUP));

        backdoor.usersAndGroups().addUser("charlie");
        backdoor.usersAndGroups().addUserToGroup("charlie", SOFTWARE_GROUP);

        backdoor.usersAndGroups().addGroup(SD_GROUP);
        backdoor.usersAndGroups().addGroup(REFERENCE_GROUP);
        backdoor.usersAndGroups().addGroup(DEFAULT_GROUP);
        backdoor.usersAndGroups().addGroup(NOT_DEFAULT_GROUP);

        backdoor.usersAndGroups().addUserToGroup(ADMIN_USERNAME, JIRA_CORE_GROUP);

        /**
         * EXPECTED STATE:
         * fred - Test Product access
         * admin - Core Access
         * no-access-user - No Access
         * charlie - JIRA Software access
         */
    }

    @Before
    public void setUp() {
        userClient = new UserClient(jiraEnvironmentData);
    }

    @Test
    public void shouldDisplayGroupWithLabels() {
        ViewUserPage viewUserPage = jira.goTo(ViewUserPage.class, "admin");

        assertThat(viewUserPage.getGroupNames(), containsInAnyOrder(JIRA_ADMIN_GROUP,
                "jira-developers", JIRA_CORE_GROUP));
        assertThat(viewUserPage.getGroupRow(JIRA_ADMIN_GROUP).getLabels(), contains(GROUP_LABEL_ADMIN.toUpperCase()));
        assertThat(viewUserPage.getGroupRow("jira-developers").getLabels(), is(empty()));
        assertThat(viewUserPage.getGroupRow(JIRA_CORE_GROUP).getLabels(), contains(GROUP_LABEL_JIRA_CORE.toUpperCase()));
    }

    @Test
    public void shouldDisplayWarningWhenApplicationDoesNotHaveDefaultGroups() {
        backdoor.applicationRoles().putRole(ApplicationLicenseConstants.REFERENCE_KEY, REFERENCE_GROUP);

        final ViewUserPage viewUserPage = jira.goTo(ViewUserPage.class, "fred");
        waitUntilTrue(viewUserPage.getApplicationAccess().hasWarning(ApplicationLicenseConstants.REFERENCE_KEY));
        waitUntilFalse(viewUserPage.getApplicationAccess().isApplicationVisible(ApplicationLicenseConstants.REFERENCE_KEY));
    }

    @Test
    public void shouldGroupNameBeEscaped() {
        backdoor.usersAndGroups().addGroup("jira=users");
        backdoor.usersAndGroups().addUserToGroup("fred", "jira=users");

        ViewUserPage viewUserPage = jira.goTo(ViewUserPage.class, "fred");

        assertEquals(viewUserPage.getGroupRow("jira=users").getLink().getAttribute("href"),
                jira.getProductInstance().getBaseUrl() + "/secure/admin/user/ViewGroup.jspa?name=jira%3Dusers");
    }

    @Test
    @LoginAs(admin = true)
    public void shouldDisplayApplicationsAndAddUserToThem() {
        backdoor.applicationRoles().putRole(ApplicationLicenseConstants.SERVICE_DESK_KEY);
        userClient = new UserClient(jiraEnvironmentData);

        final ViewUserPage page = jira.goTo(ViewUserPage.class, "charlie");
        final UserApplicationAccess userApplicationAccess = page.getApplicationAccess();
        waitUntilTrue(userApplicationAccess.isApplicationSelected(ApplicationLicenseConstants.SOFTWARE_KEY));
        waitUntilFalse(userApplicationAccess.isApplicationSelected(ApplicationLicenseConstants.TEST_KEY));

        page.toggleApplication(ApplicationLicenseConstants.TEST_KEY);
        assertUserInApplications("charlie", roleWithKey(ApplicationLicenseConstants.TEST_KEY),
                roleWithKey(ApplicationLicenseConstants.SOFTWARE_KEY));

        page.toggleApplication(ApplicationLicenseConstants.SOFTWARE_KEY);
        waitUntilFalse(userApplicationAccess.isApplicationEnabled(ApplicationLicenseConstants.CORE_KEY));
        waitUntilTrue(userApplicationAccess.isApplicationSelected(ApplicationLicenseConstants.CORE_KEY));
        assertUserInApplications("charlie", roleWithKey(ApplicationLicenseConstants.TEST_KEY));

        page.toggleApplication(ApplicationLicenseConstants.TEST_KEY);
        waitUntilTrue(userApplicationAccess.isApplicationEnabled(ApplicationLicenseConstants.CORE_KEY));
        waitUntilFalse(userApplicationAccess.isApplicationSelected(ApplicationLicenseConstants.CORE_KEY));
        assertUserInApplications("charlie");

        page.toggleApplication(ApplicationLicenseConstants.CORE_KEY);
        waitUntilTrue(userApplicationAccess.isApplicationEnabled(ApplicationLicenseConstants.CORE_KEY));
        waitUntilTrue(userApplicationAccess.isApplicationSelected(ApplicationLicenseConstants.CORE_KEY));
        assertUserInApplications("charlie", roleWithKey(ApplicationLicenseConstants.CORE_KEY));
    }

    @Test
    @LoginAs(admin = true)
    public void shouldBeAbleToChangeApplicationAndGroupAccessWhenUsernameContainsSpecialCharacters() {
        final String SPECIAL_CHARACTERS_USERNAME = "user! \\/#";
        backdoor.usersAndGroups().addUser(SPECIAL_CHARACTERS_USERNAME);

        try {
            ViewUserPage page = jira.goTo(ViewUserPage.class, URLEncoder.encode(SPECIAL_CHARACTERS_USERNAME, "UTF-8"));

            page.toggleApplication(ApplicationLicenseConstants.SOFTWARE_KEY);
            waitUntilTrue(page.getApplicationAccess().isApplicationSelected(ApplicationLicenseConstants.SOFTWARE_KEY));

            page = page.editGroups().addTo(ImmutableList.of(JIRA_CORE_GROUP), ViewUserPage.class, SPECIAL_CHARACTERS_USERNAME);
            assertThat(page.getGroupNames(), containsInAnyOrder(JIRA_CORE_GROUP, SOFTWARE_GROUP));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        } finally {
            backdoor.usersAndGroups().deleteUser(SPECIAL_CHARACTERS_USERNAME);
        }
    }

    @Test
    @LoginAs(admin = true)
    @CreateUser(username = KEVIN_USERNAME, password = KEVIN_USERNAME)
    public void shouldUpdateGroupsAndApplicationsOnViewUserPageAfterEditingGroups() {
        ViewUserPage page = jira.goTo(ViewUserPage.class, KEVIN_USERNAME);
        waitUntilTrue(page.isNoApplicationsWarningVisible());

        page = page.editGroups().addTo(ImmutableList.of(JIRA_CORE_GROUP), ViewUserPage.class, KEVIN_USERNAME);
        final UserApplicationAccess applicationAccess = page.getApplicationAccess();

        assertThat(page.getGroupNames(), contains(JIRA_CORE_GROUP));
        waitUntilTrue(applicationAccess.isApplicationSelected(ApplicationLicenseConstants.CORE_KEY));
        waitUntilTrue(applicationAccess.isApplicationEnabled(ApplicationLicenseConstants.CORE_KEY));
        waitUntilFalse(applicationAccess.isApplicationSelected(ApplicationLicenseConstants.SOFTWARE_KEY));
        waitUntilFalse(applicationAccess.isApplicationSelected(ApplicationLicenseConstants.TEST_KEY));
    }

    @Test
    @CreateUser(username = BOB_USERNAME, password = BOB_USERNAME)
    @LoginAs(admin = true)
    public void shouldUpdateGroupsAfterChangingApplicationAccess() {
        final ViewUserPage page = jira.goTo(ViewUserPage.class, BOB_USERNAME);
        waitUntilTrue(page.isNoApplicationsWarningVisible());

        page.toggleApplication(ApplicationLicenseConstants.CORE_KEY);
        assertThat(page.getGroupNames(), contains(JIRA_CORE_GROUP));

        page.toggleApplication(ApplicationLicenseConstants.SOFTWARE_KEY);
        assertThat(page.getGroupNames(), containsInAnyOrder(JIRA_CORE_GROUP, SOFTWARE_GROUP));

        page.toggleApplication(ApplicationLicenseConstants.TEST_KEY);
        assertThat(page.getGroupNames(), containsInAnyOrder(JIRA_CORE_GROUP, SOFTWARE_GROUP, TEST_GROUP));

        backdoor.usersAndGroups().addUserToGroup(BOB_USERNAME, JIRA_ADMIN_GROUP);
        page.toggleApplication(ApplicationLicenseConstants.SOFTWARE_KEY);
        assertThat(page.getGroupNames(), containsInAnyOrder(JIRA_CORE_GROUP, JIRA_ADMIN_GROUP, TEST_GROUP));
    }

    @Test
    @CreateUser(username = EFFECTIVE_USERNAME, password = EFFECTIVE_USERNAME, groupnames = {JIRA_CORE_GROUP})
    @LoginAs(admin = true)
    public void shouldShowEffectiveApplicationWarningWhenApplicationsHaveCommonGroups() {
        backdoor.applicationRoles().putRoleWithDefaults(ApplicationLicenseConstants.SERVICE_DESK_KEY, ImmutableList.of(DEFAULT_GROUP, JIRA_CORE_GROUP), ImmutableList.of(DEFAULT_GROUP));

        final ViewUserPage page = jira.goTo(ViewUserPage.class, EFFECTIVE_USERNAME);
        waitUntilTrue(page.getApplicationAccess().hasApplicationEffectiveWarningVisible(ApplicationLicenseConstants.SERVICE_DESK_KEY));
        waitUntilTrue(page.getApplicationAccess().isApplicationIndeterminate(ApplicationLicenseConstants.SERVICE_DESK_KEY));
    }

    @Test
    @CreateUser(username = INDETERMINATE_USERNAME, password = INDETERMINATE_USERNAME, groupnames = {NOT_DEFAULT_GROUP})
    @LoginAs(admin = true)
    public void shouldNotShowEffectiveApplicationWarningWhenNoGroupsAreCommonButUserIsNotInAllDefaultGroups() {
        backdoor.applicationRoles().putRoleWithDefaults(ApplicationLicenseConstants.SERVICE_DESK_KEY, ImmutableList.of(DEFAULT_GROUP, NOT_DEFAULT_GROUP), ImmutableList.of(DEFAULT_GROUP));

        final ViewUserPage page = jira.goTo(ViewUserPage.class, INDETERMINATE_USERNAME);
        waitUntilFalse(page.getApplicationAccess().hasApplicationEffectiveWarningVisible(ApplicationLicenseConstants.SERVICE_DESK_KEY));
        waitUntilTrue(page.getApplicationAccess().isApplicationIndeterminate(ApplicationLicenseConstants.SERVICE_DESK_KEY));
        waitUntilFalse(page.getApplicationAccess().isApplicationEnabled(ApplicationLicenseConstants.CORE_KEY));
    }

    @Test
    @LoginAs(admin = true)
    @CreateUser(username = STUART_USERNAME, password = STUART_USERNAME, groupnames = {JIRA_DEV_GROUP})
    public void shouldShowNoApplicationsWarningEvenIfUserIsInGroup() {
        final ViewUserPage page = jira.goTo(ViewUserPage.class, STUART_USERNAME);
        waitUntilTrue(page.isNoApplicationsWarningVisible());
        waitUntilFalse(page.isNoGroupsWarningVisible());
        assertThat(page.getGroupNames(), contains(JIRA_DEV_GROUP));
    }

    @Test
    @LoginAs(admin = true)
    public void shouldRemoveUserFromApplicationEvenIfIsOnLimit() {
        assertThat(
                "This test only makes sense when SD has 2 users",
                backdoor.applicationRoles().getRole(ApplicationLicenseConstants.SERVICE_DESK_KEY).getNumberOfSeats(),
                is(2)
        );
        backdoor.usersAndGroups().addUsers("sd-", "sd-", 2);
        backdoor.usersAndGroups().addGroup(SD_GROUP_ADDITIONAL);
        backdoor.usersAndGroups().addUserToGroup("sd-0", SD_GROUP);
        backdoor.usersAndGroups().addUserToGroup("sd-1", SD_GROUP_ADDITIONAL);

        backdoor.applicationRoles().putRoleWithDefaults(ApplicationLicenseConstants.SERVICE_DESK_KEY, ImmutableList.of(SD_GROUP, SD_GROUP_ADDITIONAL), ImmutableList.of(SD_GROUP));

        final ViewUserPage user0 = jira.goTo(ViewUserPage.class, "sd-0");
        final UserApplicationAccess applicationAccess = user0.getApplicationAccess();
        waitUntilTrue(Conditions.and(
                applicationAccess.isApplicationEnabled(ApplicationLicenseConstants.SERVICE_DESK_KEY),
                applicationAccess.isApplicationSelected(ApplicationLicenseConstants.SERVICE_DESK_KEY),
                Conditions.not(applicationAccess.isApplicationEnabled(ApplicationLicenseConstants.CORE_KEY))
        ));

        user0.toggleApplication(ApplicationLicenseConstants.SERVICE_DESK_KEY);
        assertUserInApplications("sd-0");
        user0.toggleApplication(ApplicationLicenseConstants.SERVICE_DESK_KEY);
        assertUserInApplications("sd-0", roleWithKey(ApplicationLicenseConstants.SERVICE_DESK_KEY));

        final ViewUserPage user1 = jira.goTo(ViewUserPage.class, "sd-1");
        //SD is indeterminate
        assertUserInApplications("sd-1", roleWithKey(ApplicationLicenseConstants.SERVICE_DESK_KEY));
        user1.toggleApplication(ApplicationLicenseConstants.SERVICE_DESK_KEY);
        //SD is selected
        assertUserInApplications("sd-1", roleWithKey(ApplicationLicenseConstants.SERVICE_DESK_KEY));
        //SD is deselected
        user1.toggleApplication(ApplicationLicenseConstants.SERVICE_DESK_KEY);
        assertUserInApplications("sd-1");
    }

    @Test
    @CreateUser(username = ALANA_USERNAME, password = ALANA_USERNAME, groupnames = {REFERENCE_GROUP})
    @LoginAs(admin = true)
    public void shouldRemoveCharlieFromApplicationEvenIfAppDoesNotHaveDefaultGroups() {
        backdoor.applicationRoles().putRole(ApplicationLicenseConstants.REFERENCE_KEY, REFERENCE_GROUP);

        final ViewUserPage page = jira.goTo(ViewUserPage.class, ALANA_USERNAME);
        final UserApplicationAccess applicationAccess = page.getApplicationAccess();
        waitUntilTrue(Conditions.and(
                applicationAccess.isApplicationEnabled(ApplicationLicenseConstants.REFERENCE_KEY),
                applicationAccess.isApplicationSelected(ApplicationLicenseConstants.REFERENCE_KEY),
                Conditions.not(applicationAccess.isApplicationEnabled(ApplicationLicenseConstants.CORE_KEY))
        ));

        page.toggleApplication(ApplicationLicenseConstants.REFERENCE_KEY);
        assertUserInApplications(ALANA_USERNAME);
    }

    @Test
    @LoginAs(admin = true)
    public void shouldNotShowNoAppsWarningForAdmin() {
        backdoor.usersAndGroups().addUser(NEW_ADMIN_USERNAME);
        backdoor.usersAndGroups().addUserToGroup(NEW_ADMIN_USERNAME, JIRA_ADMIN_GROUP);

        final ViewUserPage viewUserPage = jira.goTo(ViewUserPage.class, NEW_ADMIN_USERNAME);
        waitUntilFalse(viewUserPage.isNoApplicationsWarningVisible());

        // add user to Core - warning shouldn't be visible - user is an admin and has an access to Core
        viewUserPage.toggleApplication(ApplicationLicenseConstants.CORE_KEY);
        waitUntilFalse(viewUserPage.isNoApplicationsWarningVisible());

        // remove user from Core - warning shouldn't be visible - user is in admin group
        viewUserPage.toggleApplication(ApplicationLicenseConstants.CORE_KEY);
        waitUntilFalse(viewUserPage.isNoApplicationsWarningVisible());

        backdoor.usersAndGroups().deleteUser(NEW_ADMIN_USERNAME);
    }

    @SafeVarargs
    private final void assertUserInApplications(String username, Matcher<ApplicationRole>... applicationKeysMatchers) {
        final User user = userClient.get(username, User.Expand.applicationRoles);
        assertThat(user.applicationRoles.items, Matchers.containsInAnyOrder(Arrays.asList(applicationKeysMatchers)));
    }

    private static Matcher<ApplicationRole> roleWithKey(String key) {
        return new TypeSafeMatcher<ApplicationRole>() {
            @Override
            protected boolean matchesSafely(final ApplicationRole item) {
                return item.key.equals(key);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("ApplicationRole with key ").appendValue(key);
            }
        };
    }

    private static Iterable<String> groups(String... groups) {
        return ImmutableSet.copyOf(groups);
    }
}