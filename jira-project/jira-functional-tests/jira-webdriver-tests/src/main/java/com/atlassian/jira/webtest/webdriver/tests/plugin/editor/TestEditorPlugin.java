package com.atlassian.jira.webtest.webdriver.tests.plugin.editor;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.config.CreateUser;
import com.atlassian.jira.pageobjects.config.DisableRTE;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.config.ResetData;
import com.atlassian.jira.pageobjects.util.CriticalResources;
import com.atlassian.jira.pageobjects.util.CriticalResources.ResourceType;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.After;

import javax.inject.Inject;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.IsNot.not;

@WebTest ({ com.atlassian.jira.functest.framework.suite.Category.WEBDRIVER_TEST})
@ResetData
public class TestEditorPlugin extends BaseJiraWebTest {

    private final static String RTE_OPTIONAL_FEATURE = "jira.richeditor.enable-rte.optional";

    private final static String TEST_USER = "freddie";

    private static final String PROJECT_KEY = "HSP";
    private String issueKey;

    @Inject
    private CriticalResources criticalResources;

    private static final String INSTRUMENT_PROPERTY_CSS = "instrument-css";
    private static final String INSTRUMENT_PROPERTY_JS = "instrument-js";

    @BeforeClass
    public static void setUpClass() {
        Preconditions.checkState(!backdoor.darkFeatures().isGlobalEnabled(RTE_OPTIONAL_FEATURE), "RTE dark optional feature must be disabled by default");
        Preconditions.checkState(backdoor.applicationProperties().getOption(APKeys.JIRA_OPTION_RTE_ENABLED), "RTE admin setting 'jira.rte.enabled' must be true by default");
    }

    @Before
    public void setUp() {
        backdoor.fieldConfiguration().setFieldRenderer("Default Field Configuration", "comment", "Wiki Style Renderer");
        backdoor.fieldConfiguration().setFieldRenderer("Default Field Configuration", "description", "Wiki Style Renderer");

        backdoor.systemProperties().setProperty(INSTRUMENT_PROPERTY_JS, "true");
        backdoor.systemProperties().setProperty(INSTRUMENT_PROPERTY_CSS, "true");

        issueKey = backdoor.issues().loginAs("admin").createIssue(PROJECT_KEY, "Test").key;

        clearCookie();
    }

    @After
    public void tearDown() throws Exception {
        backdoor.systemProperties().unsetProperty(INSTRUMENT_PROPERTY_JS);
        backdoor.systemProperties().unsetProperty(INSTRUMENT_PROPERTY_CSS);

        if (issueKey != null) {
            backdoor.issues().deleteIssue(issueKey, true);
        }
    }

    @Test
    @CreateUser(username = TEST_USER, password = TEST_USER)
    @LoginAs(user = TEST_USER)
    @DisableRTE
    public void shouldNotLoadEditorWhenItIsDisabled() {
        refreshResources();

        pageBinder.bind(JiraHeader.class).createIssue();

        assertThat(getResources(ResourceType.SCRIPT), is(empty()));
        assertThat(getResources(ResourceType.STYLESHEET), is(empty()));
    }

    @Test
    @CreateUser(username = TEST_USER, password = TEST_USER)
    @LoginAs(user = TEST_USER)
    public void shouldLoadEditorWhenItIsEnabled() {
        Preconditions.checkState(backdoor.applicationProperties().getOption(APKeys.JIRA_OPTION_RTE_ENABLED));

        refreshResources();

        assertThat(getResources(ResourceType.SCRIPT), is(not(empty())));
        assertThat(getResources(ResourceType.STYLESHEET), is(not(empty())));
    }

    @Test
    @CreateUser(username = TEST_USER, password = TEST_USER)
    @LoginAs(user = TEST_USER)
    @DisableRTE (optional = true)
    public void shouldLoadEditorWhenItAndOptionalFlagAreEnabled() throws Exception {
        Preconditions.checkState(backdoor.applicationProperties().getOption(APKeys.JIRA_OPTION_RTE_ENABLED));

        refreshResources();

        assertThat(getResources(ResourceType.SCRIPT), is(not(empty())));
        assertThat(getResources(ResourceType.STYLESHEET), is(not(empty())));
    }

    private void refreshResources() {
        // those page reloads are needed to avoid flakiness, instrumenting of resources does not kick in immediately
        jira.goToViewIssue(issueKey);
        jira.getTester().getDriver().navigate().refresh();
        jira.goToViewIssue(issueKey).comment();
    }

    private ImmutableList<String> getResources(ResourceType type) {
        return criticalResources.getActive(type).stream().filter(resourceKey -> resourceKey.contains("jira-editor-plugin")).collect(CollectorsUtil.toImmutableList());
    }

    private void clearCookie() {
        try {
            jira.getTester().getDriver().manage().deleteCookieNamed("jira.editor.user.mode");
        } catch(Exception e) {
            logger.warn("Problem cleaning local storage", e);
        }
    }
}