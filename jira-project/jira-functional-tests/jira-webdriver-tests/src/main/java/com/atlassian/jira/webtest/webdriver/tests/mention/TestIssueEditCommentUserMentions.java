package com.atlassian.jira.webtest.webdriver.tests.mention;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.viewissue.EditCommentDialog;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Comment;
import com.atlassian.jira.testkit.client.restclient.Response;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Test for user mentions in JIRA
 *
 * @since v5.2
 */
@WebTest({Category.WEBDRIVER_TEST, Category.USERS_AND_GROUPS, Category.ISSUES})
public class TestIssueEditCommentUserMentions extends BaseJiraWebTest {

    @Before
    public void setUp() throws Exception {
        backdoor.restoreBlankInstance();
    }

    @Test
    public void testMentionAutoCompleteWorksWhenEditingAComment() {
        final String username = "admin";

        // create issue
        final IssueCreateResponse response = backdoor.issues().createIssue("HSP", "Edit comment mention issue", username);
        final String issueKey = response.key();

        // make a comment
        final Response<Comment> commentResponse = backdoor.issues().commentIssue(issueKey, "My awesome comment");
        final Long commentId = Long.parseLong(commentResponse.body.id);

        final long defaultPermissionScheme = 0;
        backdoor.permissionSchemes().addGroupPermission(defaultPermissionScheme, ProjectPermissions.EDIT_OWN_COMMENTS, "jira-administrators");

        final EditCommentDialog editCommentDialog = jira
                .goToViewIssue(issueKey)
                .editComment(commentId);

        editCommentDialog.typeInput("@" + username);
        waitUntilTrue(username + " should be available for selection", editCommentDialog.getMentions().hasSuggestion(username));

        editCommentDialog.selectMention(username);
        waitUntilEquals(username + " should be selected", "[~" + username + "]", editCommentDialog.getInputTimed());
    }
}
