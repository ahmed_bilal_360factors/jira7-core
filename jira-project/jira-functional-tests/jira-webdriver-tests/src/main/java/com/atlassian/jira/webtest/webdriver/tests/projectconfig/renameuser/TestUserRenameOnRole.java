package com.atlassian.jira.webtest.webdriver.tests.projectconfig.renameuser;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.admin.user.EditUserDetailsPage;
import com.atlassian.jira.pageobjects.project.people.EditPeopleRoleForm;
import com.atlassian.jira.pageobjects.project.people.MockPeopleRole;
import com.atlassian.jira.pageobjects.project.people.PeopleRole;
import com.atlassian.jira.pageobjects.project.people.RolesPage;
import com.google.common.collect.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static org.junit.Assert.assertEquals;

/**
 * Web test to ensure roles are preserved when users are renamed
 *
 * @since v6.0
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION})
@Restore("xml/projectconfig/user_rename_project_roles.xml")
public class TestUserRenameOnRole extends BaseJiraWebTest {
    private static final String OLD_ROLES_SCREEN_FEATURE_KEY = "jira.project.config.old.roles.screen";

    @Before
    public void setUpTest() {
        backdoor.darkFeatures().enableForSite(OLD_ROLES_SCREEN_FEATURE_KEY);
    }

    @After
    public void tearDownTest() {
        backdoor.darkFeatures().disableForSite(OLD_ROLES_SCREEN_FEATURE_KEY);
    }

    @Test
    public void testRenamedUserRolesPreserved() {
        // Check bw is batman
        RolesPage rolesPage = jira.goTo(RolesPage.class, "COW");
        final List<PeopleRole> expectedRoles = Lists.<PeopleRole>newArrayList(
                new MockPeopleRole("Administrators")
                        .addGroup("jira-administrators", true),
                new MockPeopleRole("Batman")
                        .addUser("Batman Wayne", prependBaseUrl("secure/useravatar?size=small&avatarId=10122")),
                new MockPeopleRole("Developers")
                        .addGroup("jira-developers", true),
                new MockPeopleRole("Users")
                        .addGroup("jira-users", true)
        );
        waitUntilFalse(rolesPage.isTableLoading());
        assertEquals(expectedRoles, rolesPage.getRoles());

        // Rename bw to thedarkknight
        final EditUserDetailsPage userPage = jira.goTo(EditUserDetailsPage.class, "bw");
        userPage.fillUserName("thedarkknight").submit();

        // Check thedarkknight is batman
        rolesPage = jira.goTo(RolesPage.class, "COW");
        waitUntilFalse(rolesPage.isTableLoading());
        assertEquals(expectedRoles, rolesPage.getRoles());

        //Check thedarkknight stays batman even after an update round trip (JRADEV-18145)
        final PeopleRole batmanRoles = rolesPage.getRoleByName("Batman");
        batmanRoles.edit("project-config-people-users-select-textarea")
                .addUser("Crazy Cat")
                .submit();
        batmanRoles.edit("project-config-people-users-select-textarea")
                .removeUser("Crazy Cat")
                .submit();
        assertEquals(expectedRoles, rolesPage.getRoles());
        // Retire thedarkknight
        final PeopleRole batmen = rolesPage.getRoleByName("Batman");
        final EditPeopleRoleForm b = batmen.edit("project-config-people-users-select-textarea");
        b.removeUser("Batman Wayne");
        b.submit();

        // Check thedarkknight is not batman anymore
        rolesPage = jira.goTo(RolesPage.class, "COW");
        waitUntilFalse(rolesPage.isTableLoading());
        expectedRoles.remove(1);
        expectedRoles.add(1, new MockPeopleRole("Batman"));
        assertEquals(expectedRoles, rolesPage.getRoles());
    }

    private String prependBaseUrl(final String url) {
        return jira.getProductInstance().getBaseUrl() + url;
    }
}