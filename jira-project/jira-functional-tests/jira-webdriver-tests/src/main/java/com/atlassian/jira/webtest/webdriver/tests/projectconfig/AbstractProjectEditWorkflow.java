package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.websudo.JiraSudoFormDialog;
import com.atlassian.jira.pageobjects.project.ProjectConfigPageTab;
import com.atlassian.jira.pageobjects.project.workflow.EditWorkflowDialog;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.beans.WorkflowSchemeData;
import com.atlassian.jira.testkit.client.restclient.ChangeLog;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.google.common.collect.Ordering;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.util.Collection;
import java.util.List;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @since v6.2
 */
public abstract class AbstractProjectEditWorkflow<T extends ProjectConfigPageTab> extends BaseJiraWebTest {
    private static final String PROJECT_DEFAULT_KEY = "HSP";
    private static final String PROJECT_DEFAULT_NAME = "homosapien";
    private static final String PROJECT_PRIVATE_KEY = "MKY";
    private static final String PROJECT_XSS_NAME = "<script>alert(\"Cross Site Scripting\")</script>";
    private static final String PROJECT_XSS_KEY = "XSS";

    protected static final String DEFAULT_WF_NAME = "jira";
    private static final String MONKEY_WF_DISPLAY_NAME = "Only Monkey";

    @Test
    public void testEditWorkflowCopiesWorkflowAndSchemeOnEditWhenUsingDefaultSchemeNoIssues() {
        checkWorkflowAndSchemeCopied(PROJECT_DEFAULT_NAME, PROJECT_DEFAULT_KEY, false);
    }

    @Test
    public void testEditWorkflowCopiesWorkflowAndSchemeOnEditWhenUsingDefaultSchemeWithIssueToMigrate() {
        String name = "Project With Issues";
        String key = "PWIADWFS";

        checkWorkflowChangedAndIssuesMigrated(name, key, false);
    }

    @Test
    public void testWebsudoOnTheMigrationDialog() {
        String name = "WebSudo Project";
        String key = "WEBSUDO";

        backdoor.websudo().enable();
        try {
            checkWorkflowChangedAndIssuesMigrated(name, key, true);
        } finally {
            backdoor.websudo().disable();
        }
    }

    private void checkWorkflowChangedAndIssuesMigrated(String name, String key, boolean webSudo) {
        backdoor.project().addProject(name, key, "admin");
        final IssueCreateResponse createResponse = backdoor.issues().createIssue(key, "This is a simple issue for migration");
        checkWorkflowAndSchemeCopied(name, key, webSudo);

        //Check that the issue has been migrated correctly.
        final Issue issue = backdoor.issues().getIssue(createResponse.key, Issue.Expand.changelog);
        assertChangeLogContainsChange("Workflow", DEFAULT_WF_NAME, format("%s Workflow", name), issue.changelog);
    }

    @Test
    public void testEditWorkflowCopiesWorkflowAndSchemeOnEditWhenUsingDefaultSchemeXss() {
        String name = "\"'<strong>XSS</strong>\"'";
        String key = "SXSS";

        backdoor.project().addProject(name, key, "admin");
        checkWorkflowAndSchemeCopied(name, key, false);
    }

    @Test
    public void testEditWorkflowDoesNotCopyWorkflowWhenUsingNonDefaultWorkflowScheme() {
        checkWorkflowSchemeNotCopied(PROJECT_PRIVATE_KEY, MONKEY_WF_DISPLAY_NAME);

        assertTrue(backdoor.workflow().getWorkflowDetailed(MONKEY_WF_DISPLAY_NAME).isHasDraft());
    }

    @Test
    public void testEditWorkflowDoesNotCopyWorkflowWhenUsingNonDefaultWorkflowSchemeXss() {
        String workflowName = "Random Workflow";
        backdoor.project().addProject(PROJECT_XSS_NAME, PROJECT_XSS_KEY, "admin");

        backdoor.workflow().createWorkflow(workflowName);

        WorkflowSchemeData data = new WorkflowSchemeData().setName(PROJECT_XSS_NAME)
                .setMapping("Bug", workflowName);

        WorkflowSchemeData scheme = backdoor.workflowSchemes().createScheme(data);

        backdoor.project().setWorkflowScheme(PROJECT_XSS_KEY, scheme.getId());

        checkWorkflowSchemeNotCopied(PROJECT_XSS_KEY, workflowName);

        assertTrue(jira.backdoor().workflow().getWorkflowDetailed(workflowName).isHasDraft());
    }

    /**
     * Open up the tab on the given project.
     *
     * @param key the project's key.
     * @return the page object for the open tab.
     */
    abstract T gotoPageForProject(final String key);

    /**
     * Open the workflow migration edit dialog for the passed tab. The project will be using the default wf scheme.
     *
     * @return the workflow migration edit dialog for the passed project.
     */
    abstract EditWorkflowDialog openMigrationDialog(T page);

    /**
     * Authenticate on the websudo form after workflow migration.
     *
     * @param dialog       the workflow migration dialog.
     * @param workflowName the name of the migrated workflow.
     */
    abstract void authenticateSudoFormAfterMigration(JiraSudoFormDialog dialog, String workflowName);

    /**
     * Execute a workflow migration
     *
     * @param dialog       the workflow migration dialog.
     * @param workflowName the name of the migrated workflow.
     */
    abstract void doWorkflowMigration(EditWorkflowDialog dialog, String workflowName);

    /**
     * Open the passed workflow in the context of the passed tab.
     *
     * @param page                the current page.
     * @param workflowDisplayName the workflow's display name.
     * @param diagram             show the editor in diagram mode or not. This should only apply for the old workflow designer.
     * @return the text mode editor.
     */
    abstract void clickEdit(T page, final String workflowDisplayName, final boolean diagram);

    private void checkWorkflowAndSchemeCopied(String projectName, String key, boolean webSudo) {
        String newWorkflowName = format("%s Workflow", projectName);
        String newWorkflowSchemeName = format("%s Workflow Scheme", projectName);

        final T page = gotoPageForProject(key);
        final EditWorkflowDialog dialog = openMigrationDialog(page);
        assertFalse(dialog.isProgressBarPresent());
        assertTrue(dialog.isButtonsVisible());

        if (webSudo) {
            final JiraSudoFormDialog sudoFormDialog = dialog.clickAndWaitBeforeBind(JiraSudoFormDialog.class, JiraSudoFormDialog.ID_SMART_WEBSUDO);
            sudoFormDialog.authenticateFail("fail");
            authenticateSudoFormAfterMigration(sudoFormDialog, newWorkflowName);
        } else {
            doWorkflowMigration(dialog, newWorkflowName);
        }

        assertTrue(backdoor.workflow().getWorkflowDetailed(newWorkflowName).isHasDraft());

        assertTrue(format("Unable to find workflow '%s'.", newWorkflowName), backdoor.workflow().getWorkflows().contains(newWorkflowName));
        boolean found = false;
        for (WorkflowSchemeData scheme : backdoor.workflowSchemes().getWorkflowSchemes()) {
            if (newWorkflowSchemeName.equals(scheme.getName())) {
                found = true;
                assertEquals(newWorkflowName, scheme.getDefaultWorkflow());
                assertTrue(scheme.getMappings().isEmpty());
            }
        }

        if (!found) {
            fail(format("Workflow Scheme '%s' not found.", newWorkflowSchemeName));
        }
    }

    private void checkWorkflowSchemeNotCopied(String projectKey, String workflowName) {
        final List<String> beforeWorkflows = getWorkflows();

        clickEdit(gotoPageForProject(projectKey), workflowName, false);
        //No workflows were added or removed.
        assertEquals(beforeWorkflows, getWorkflows());
    }

    private List<String> getWorkflows() {
        return Ordering.natural().nullsLast().sortedCopy(backdoor.workflow().getWorkflows());
    }

    private void assertChangeLogContainsChange(String field, String from, String to, ChangeLog changeLog) {
        final Collection<? extends ChangeLog.HistoryItem> historyItems = changeLog.mergeHistoryItems();
        for (ChangeLog.HistoryItem historyItem : historyItems) {
            if (StringUtils.equals(historyItem.field, field)
                    && StringUtils.equals(historyItem.fromString, from)
                    && StringUtils.equals(historyItem.toString, to)) {
                return;
            }
        }
        fail(format("Unable to find item with {field: %s, fromString: %s, toString: %s} in [%s]",
                field, from, to, historyItems));
    }
}
