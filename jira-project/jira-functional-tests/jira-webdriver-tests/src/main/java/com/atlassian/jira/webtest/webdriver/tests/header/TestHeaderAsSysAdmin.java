package com.atlassian.jira.webtest.webdriver.tests.header;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.CommonHeader;
import com.atlassian.jira.pageobjects.config.LoginAs;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@WebTest({Category.WEBDRIVER_TEST})
@RestoreOnce(value = "header-one-project.xml")
@LoginAs(sysadmin = true)
public class TestHeaderAsSysAdmin extends BaseJiraWebTest {
    private CommonHeader header;

    @Before
    public void setUp() {
        header = CommonHeader.visit(jira);
    }

    @Test
    public void testAdminMenuBaseUrl() {
        final String BASE_URL = jira.getProductInstance().getBaseUrl();
        final String ADMIN_PROJECT_LIST_URL = BASE_URL + "/plugins/servlet/applications/versions-licenses";

        assertTrue(header.hasAdminMenu());
        MatcherAssert.assertThat(header.getAdminMenuLinkTarget(), is(ADMIN_PROJECT_LIST_URL));
    }

    @Test
    public void testAdminMenuLinksForSysadmin() {
        assertTrue(header.hasAdminMenu());
        MatcherAssert.assertThat(header.getAdminMenuLinkIds(), IsIterableContainingInOrder.contains(expectedAdminMenuLinks()));
    }

    @Test
    public void testSysadmin() {
        assertTrue(header.hasAppSwitcher());
        assertTrue(header.hasMainHeaderLinks());
        assertTrue(header.hasCreateIssueButton());
        assertTrue(header.hasQuickSearch());
        assertTrue(header.hasHelpMenu());
        assertTrue(header.hasAdminMenu());
        assertFalse(header.hasLoginButton());
        assertTrue(header.hasUserOptionsMenu());
    }

    public static String[] expectedAdminMenuLinks() {
        return new String[]{"admin_applications_menu", "admin_project_menu", "admin_issues_menu", "admin_plugins_menu", "admin_users_menu", "admin_system_menu"};
    }
}
