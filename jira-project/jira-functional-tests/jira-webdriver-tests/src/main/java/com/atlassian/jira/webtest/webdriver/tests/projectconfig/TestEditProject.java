package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.project.summary.EditProjectPageTab;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.atlassian.jira.webtests.LicenseKeys;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * @since v4.4
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS})
@Restore("xml/projectconfig/TestEditProjectPage.xml")
public class TestEditProject extends BaseJiraWebTest {
    private static final String NEW_URL = "http://www.realsurf.com";
    private static final String NEW_DESCRIPTION = "Yeah you would like me to set a description";
    private static final String NEW_NAME = "Scott's Project";
    private static final String INVALID_URL = "dfgsfdgs";

    private static final String BLA_PROJECT = "BLA";
    private static final String MKY_PROJECT = "MKY";
    private static final String PROJ_ID_BLA = "10010";
    private static final String PROJ_ID_MKY = "10001";

    private static final String INVALID_TYPE_PROJECT_KEY = "T1";
    private static final String UNINSTALLED_TYPE_NAME = "Uninstalled Type";
    private static final String UNINSTALLED_TYPE_KEY = "uninstalled_type";
    private static final String SOFTWARE_KEY = "software";
    private static final String SOFTWARE_PROJECT_NAME = "Software";
    private static final String BUSINESS_KEY = "business";
    private static final String BUSINESS_PROJECT_NAME = "Business";
    public static final long PROJECT_CATEGORY_ID = 10000L;
    public static final String PROJECT_CATEGORY = "test project category";
    public static final String PROJECT_ADMIN_GROUP = "project_admin_group";

    @Before
    public void setUp() {
        backdoor.license().replace(LicenseKeys.COMMERCIAL);
    }

    @Test
    public void testSuccessfulEdit() {
        Project originalProject = backdoor.project().getProject(BLA_PROJECT);

        EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(PROJ_ID_BLA);

        editProjectPageTab.setProjectName(NEW_NAME)
                .setProjectUrl(NEW_URL)
                .chooseAvatarOption("10001")
                .setDescription(NEW_DESCRIPTION)
                .submit();

        Project newProject = backdoor.project().getProject(BLA_PROJECT);

        assertEquals(NEW_NAME, newProject.name);
        assertEquals(NEW_URL, newProject.url);
        assertEquals(NEW_DESCRIPTION, newProject.description);
        assertThat(newProject.avatarUrls, not(equalTo(originalProject.avatarUrls)));
    }

    @Test
    public void testEditWithInlineErrors() {
        EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(PROJ_ID_BLA);

        String shortName = "a";

        editProjectPageTab.setProjectName(shortName)
                .setProjectUrl(INVALID_URL)
                .submit();

        Map<String, String> errors = editProjectPageTab.getFormErrors();
        assertEquals("The URL specified is not valid - it must start with http://", errors.get("url"));
        assertEquals("The project name should be at least 2 characters in length.", errors.get("name"));
    }

    @Test
    public void testEditWithTooLongName() {
        EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(PROJ_ID_BLA);

        final int stringSize = 152;
        StringBuilder longNameBuilder = new StringBuilder(stringSize);
        for (int i = 0; i < stringSize; i++) {
            longNameBuilder.append(PROJECT_CATEGORY);
        }
        String longName = longNameBuilder.toString();
        String shortenedName = longName.substring(0, 80);

        editProjectPageTab.setProjectName(longName);

        Poller.waitUntil(editProjectPageTab.getProjectName(), equalTo(shortenedName));
    }

    @Test
    public void testEditWithInvalidProjectLead() {
        EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(PROJ_ID_MKY);
        waitUntilFalse("Error message should not be visible.", editProjectPageTab.isErrorMessagePresent());
    }

    /**
     * JRADEV-8106 clicking on project avatar in edit dialog goes to 404 page
     */
    @Test
    public void testChangeAvatar() {
        backdoor.restoreBlankInstance();
        EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(PROJ_ID_MKY);

        editProjectPageTab.setAvatar("10001")
                .submit();

        Project project = backdoor.project().getProject(MKY_PROJECT);

        assertThat("Should have updated avatar", project.avatarUrls.get("16x16").contains("avatarId=10001"), is(true));
    }

    @Test
    public void testChangeProjectTypeAsGlobalAdmin() {
        jira.quickLoginAsAdmin();

        assertThat("Should originally be a software project", backdoor.project().getProject(BLA_PROJECT).projectTypeKey, equalTo(SOFTWARE_KEY));

        EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(PROJ_ID_BLA);

        editProjectPageTab.setProjectType(BUSINESS_KEY)
                .submit();

        assertThat("Project Type should now have been updated", backdoor.project().getProject(BLA_PROJECT).projectTypeKey, equalTo(BUSINESS_KEY));
    }

    @Test
    public void testEditWithInlineErrorsShouldNotClearDescription() {
        EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(PROJ_ID_BLA);

        editProjectPageTab.setProjectName("")
                .setDescription("This is a description.")
                .setProjectUrl(INVALID_URL)
                .submit();

        editProjectPageTab = pageBinder.bind(EditProjectPageTab.class, PROJ_ID_BLA);
        Poller.waitUntil(editProjectPageTab.getProjectDescription(), equalTo("This is a description."));
        Poller.waitUntil(editProjectPageTab.getProjectUrl(), equalTo(INVALID_URL));
    }

    @Test
    public void testEditWithInlineErrorsShouldNotResetProjectType() {
        EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(PROJ_ID_BLA);

        editProjectPageTab.setProjectUrl(INVALID_URL)
                .setProjectType(BUSINESS_KEY)
                .submit();

        assertThat("Project Type should not have been updated", editProjectPageTab.getProjectType(), equalTo(BUSINESS_PROJECT_NAME));

    }

    @Test
    public void testExpiredLicenceShowsSomething() {
        backdoor.license().replace(LicenseKeys.CORE_ROLE);

        EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(PROJ_ID_BLA);

        assertThat("Project Type should not have been updated", editProjectPageTab.getProjectType(), equalTo(SOFTWARE_PROJECT_NAME));
    }

    @Test
    public void testProjectAdminWithNoAdminPermissionCannotChangeProjectTypeOrProjectCategory() {
        jira.logout();
        jira.quickLogin("project_admin", "project_admin");

        EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(PROJ_ID_BLA);

        assertThat("Project Type should be not be editable", editProjectPageTab.isProjectTypeDisabled(), is(true));
        assertThat("Project category should be not be editable", editProjectPageTab.isProjectCategoryDisabled(), is(true));
    }

    @Test
    public void testChangeAdminToProjectAdminCannotChangeProjectCategory() {
        String[] projectCategories = {"None", "test project category"};
        try {
            backdoor.project().addProjectCategory(PROJECT_CATEGORY, "description here");
            backdoor.userProfile().changeUserLanguage("project_admin", FunctTestConstants.MOON_LOCALE);
            jira.logout();

            backdoor.usersAndGroups().addGroup(PROJECT_ADMIN_GROUP);
            backdoor.usersAndGroups().addUserToGroup("project_admin", PROJECT_ADMIN_GROUP);
            backdoor.permissions().addGlobalPermission(Permissions.ADMINISTER, PROJECT_ADMIN_GROUP);

            jira.quickLogin("project_admin", "project_admin");
            EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(PROJ_ID_BLA);
            editProjectPageTab.setProjectCategory(PROJECT_CATEGORY);

            backdoor.permissions().removeGlobalPermission(Permissions.ADMINISTER, PROJECT_ADMIN_GROUP);
            editProjectPageTab.submit();
            assertThat("Error message should say that only global admins can modify project category",
                    editProjectPageTab.getErrorMessage(),
                    is("admin.projects.service.error.no.admin.permission.projectcategory"));
        } finally {
            backdoor.project().deleteProjectCategory(Long.toString(PROJECT_CATEGORY_ID));
            backdoor.userProfile().changeUserLanguage("project_admin", "");
        }
    }

    @Test
    @Restore("xml/projectconfig/TestProjectTypeInaccesible.xml")
    public void testUninstalledProjectTypeShowsCorrectly() {
        EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(PROJ_ID_BLA);

        assertThat("Project Type should be un unknown one", editProjectPageTab.getProjectType(), equalTo(UNINSTALLED_TYPE_NAME));

        editProjectPageTab.setProjectType(BUSINESS_KEY).submit();

        assertThat("Should now be a business product", backdoor.project().getProject(INVALID_TYPE_PROJECT_KEY).projectTypeKey, equalTo(BUSINESS_KEY));
    }

    @Test
    @Restore("xml/projectconfig/TestProjectTypeInaccesible.xml")
    public void testCanUpdateProjectFieldsWithInvalidType() {
        EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(PROJ_ID_BLA);

        assertThat("Project Type should be un unknown one", editProjectPageTab.getProjectType(), equalTo(UNINSTALLED_TYPE_NAME));

        editProjectPageTab.setDescription(NEW_DESCRIPTION).submit();

        Project project = backdoor.project().getProject(INVALID_TYPE_PROJECT_KEY);
        assertThat("Description should have been updated", project.description, equalTo(NEW_DESCRIPTION));
        assertThat("Project type should still be invalid", project.projectTypeKey, equalTo(UNINSTALLED_TYPE_KEY));
    }

    private EditProjectPageTab navigateToEditProjectPageFor(final String projectId) {
        return pageBinder.navigateToAndBind(EditProjectPageTab.class, projectId);
    }
}
