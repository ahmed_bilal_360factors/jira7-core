package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.admin.IndexAdminPage;
import com.atlassian.jira.pageobjects.pages.admin.IndexProgressPage;
import com.atlassian.jira.pageobjects.pages.project.IndexProjectPage;
import com.atlassian.jira.pageobjects.project.Reindex.ProjectReindexTab;
import com.atlassian.jira.pageobjects.project.summary.ProjectSummaryPageTab;
import com.atlassian.pageobjects.elements.query.Poller;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.core.IsEqual;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertThat;

/**
 * @since v6.1
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS})
public class TestReindexProject extends BaseJiraWebTest {

    private static final String PROJECT_KEY_TST = "TST";

    private long projectId;

    @Before
    public void setUp() throws Exception {
        backdoor.restoreBlankInstance();
        projectId = backdoor.project().addProject("Test project", PROJECT_KEY_TST, "admin");
        backdoor.issues().createIssue(PROJECT_KEY_TST, "test 1");
    }

    @Test
    public void testCanRefreshAndAcknowledgeReindex() throws Exception {
        backdoor.barrier().raiseBarrierAndRun("backgroundReindex", () -> {
            navigateToAndBindReindexProjectPageAndConfirm(PROJECT_KEY_TST);
            IndexProjectPage indexProjectPage = pageBinder.bind(IndexProjectPage.class);
            indexProjectPage.refresh();
        });
        bindIndexProjectPageAndAcknowledge();
        ProjectSummaryPageTab summaryPageTab = pageBinder.navigateToAndBind(ProjectSummaryPageTab.class, PROJECT_KEY_TST);
        assertThat(summaryPageTab.getProjectKey(), equalTo("TST"));
    }

    @Test
    public void testCanCancelReindex() throws Exception {
        backdoor.barrier().raiseBarrierAndRun("backgroundReindex", () -> {
            navigateToAndBindReindexProjectPageAndConfirm(PROJECT_KEY_TST);
            IndexProjectPage indexProjectPage = pageBinder.bind(IndexProjectPage.class);
            indexProjectPage.cancel();
            Poller.waitUntilTrue(indexProjectPage.isCancelledOrBeingCancelled());
        });
        bindIndexProjectPageAndAcknowledge();
        ProjectSummaryPageTab summaryPageTab = pageBinder.navigateToAndBind(ProjectSummaryPageTab.class, PROJECT_KEY_TST);
        assertThat(summaryPageTab.getProjectKey(), equalTo("TST"));
    }

    @Test
    public void testBackgroundReindexPossibleDuringProjectReindex() throws Exception {
        final IndexAdminPage indexAdminPage = jira.getPageBinder().navigateToAndBind(IndexAdminPage.class);
        backdoor.barrier().raiseBarrierAndRun("backgroundReindex", () -> {
            backdoor.project().editProjectKeyNoWaitForReindex(projectId, "NEW");
            indexAdminPage.reindexBackground();
            jira.getPageBinder().bind(IndexProgressPage.class);
        });
        backdoor.indexing().getInBackgroundProgress().waitForCompletion();
    }

    @Test
    public void testLockingReindexNotPossibleDuringProjectReindex() throws Exception {
        final IndexAdminPage indexPageWithRaceCondition = jira.getPageBinder().navigateToAndBind(IndexAdminPage.class);
        backdoor.barrier().raiseBarrierAndRun("backgroundReindex", () -> {
            backdoor.project().editProjectKeyNoWaitForReindex(projectId, "NEW");
            indexPageWithRaceCondition.reindexForeground();
            IndexAdminPage indexAdminPage = jira.getPageBinder().bind(IndexAdminPage.class);
            Poller.waitUntil(indexAdminPage.hasError(), equalTo("The 'Lock JIRA and rebuild index' option is unavailable while other indexing operations are in progress."));

            indexAdminPage = jira.getPageBinder().navigateToAndBind(IndexAdminPage.class);
            Poller.waitUntil(indexAdminPage.hasInfo(), equalTo("The 'Lock JIRA and rebuild index' option is unavailable while other indexing operations are in progress."));
            assertThat(indexAdminPage.isForegroundReindexDisabled(), IsEqual.equalTo(true));
        });
        backdoor.indexing().getProjectIndexingProgress(projectId).waitForCompletion();

    }

    private void bindIndexProjectPageAndAcknowledge() {
        IndexProjectPage indexProjectPage = jira.getPageBinder().bind(IndexProjectPage.class);
        indexProjectPage.acknowledge();
    }

    private void navigateToAndBindReindexProjectPageAndConfirm(String key) {
        ProjectReindexTab projectReindexTab = jira.getPageBinder().navigateToAndBind(ProjectReindexTab.class, key);
        projectReindexTab.confirmReindex();
    }

    @SuppressWarnings("unchecked")
    public Matcher<String> equalTo(String equal) {
        return Matchers.equalTo(equal);
    }
}
