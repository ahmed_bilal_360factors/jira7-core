package com.atlassian.jira.webtest.webdriver.tests.issue;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import javax.inject.Inject;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.endsWith;

@WebTest({Category.WEBDRIVER_TEST, Category.ISSUES})
@Restore("TestRedirectToMovedIssues.xml")
public class TestRedirectToMovedIssues extends BaseJiraWebTest {

    @Inject
    private WebDriver driver;

    @Test
    public void testRedirectToMovedIssueBrowse() {
        goToIssue("MKY-1");
        assertThat(driver.getCurrentUrl(), endsWith("/browse/HSP-2"));
    }

    @Test
    public void testRedirectToTwiceMovedIssue() {
        //assert QuickLinkServlet (for /browse/ url patterns) is working
        goToIssue("HSP-1");
        assertThat(driver.getCurrentUrl(), endsWith("/browse/HSP-3"));

        goToIssue("MKY-2");
        assertThat(driver.getCurrentUrl(), endsWith("/browse/HSP-3"));

        //and IssueViewURLHandler (for plugin issue views) is working too
        goToUrl("/si/jira.issueviews:issue-xml/HSP-1/HSP-1.xml");
        assertThat(driver.getCurrentUrl(), endsWith("/si/jira.issueviews:issue-xml/HSP-3/HSP-3.xml"));

        goToUrl("/si/jira.issueviews:issue-xml/MKY-2/MKY-2.xml");
        assertThat(driver.getCurrentUrl(), endsWith("/si/jira.issueviews:issue-xml/HSP-3/HSP-3.xml"));

        //navigate away from xml to real page to make sure we can run javascript in rules
        jira.gotoHomePage();
    }

    /**
     * Navigates to an issue while not respecting {@link ViewIssuePage}'s {@link com.atlassian.pageobjects.binder.WaitUntil} method. This
     * is necessary because by default it checks for the issue key in the header, which will be different for moved issues.
     * @param key
     */
    private void goToIssue(String key) {
        driver.navigate().to(jira.getProductInstance().getBaseUrl() + new ViewIssuePage(key).getUrl());
    }

    private void goToUrl(String url) {
        driver.navigate().to(jira.getProductInstance().getBaseUrl() + url);
    }
}
