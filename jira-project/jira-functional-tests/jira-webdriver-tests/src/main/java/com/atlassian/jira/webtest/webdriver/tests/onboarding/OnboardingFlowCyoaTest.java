package com.atlassian.jira.webtest.webdriver.tests.onboarding;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.config.CreateUser;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.config.ResetDataOnce;
import com.atlassian.jira.pageobjects.onboarding.AvatarSequence;
import com.atlassian.jira.pageobjects.onboarding.ChooseLanguageSequence;
import com.atlassian.jira.pageobjects.onboarding.ChooseNextStepSequence;
import com.atlassian.jira.pageobjects.onboarding.ChooseYourOwnAdventureSequence;
import com.atlassian.jira.pageobjects.onboarding.WelcomeToJiraPage;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.jira.pageobjects.pages.project.BrowseProjectsPage;
import com.atlassian.jira.plugins.importer.po.JIMOnboardingPage;
import com.atlassian.jira.projecttemplates.pageobject.ProjectTemplatesDialog;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@WebTest({Category.WEBDRIVER_TEST})
@ResetDataOnce()
public class OnboardingFlowCyoaTest extends BaseJiraWebTest {
    private static final String DEVELOPER = "developer";

    private static final String ONBOARDING_CYOA_DARK_FEATURE = "jira.onboarding.cyoa";
    private static final String TASK_MANAGEMENT_TEMPLATE = "com.atlassian.jira-core-project-templates:jira-core-task-management";
    private static final String PROJECT_NAME = "Test My Project";
    private static final String PROJECT_KEY = "TMP";
    private static final String SECOND_ADMIN = "secondadmin";

    @BeforeClass
    public static void setUpClass() {
        // We can't use normal @Before annotation as it's executed after @LoginAs (LoginAs is based on rules).
        // So we use a combination of @BeforeClass and @ResetDataOnce to ensure we set DarkFeature before @LoginAs.
        backdoor.getTestkit().darkFeatures().enableForSite(ONBOARDING_CYOA_DARK_FEATURE);
    }

    @AfterClass
    public static void tearDownClass() {
        backdoor.getTestkit().darkFeatures().disableForSite(ONBOARDING_CYOA_DARK_FEATURE);
    }

    @Test
    @LoginAs(admin = true, targetPage = WelcomeToJiraPage.class)
    public void testOnboardingAsAdmin() {
        goThroughChooseLanguage();
        goThroughAvatarPicker();
        goToChooseYourAdventure();
    }

    @Test
    @CreateUser(username = DEVELOPER, password = DEVELOPER)
    @LoginAs(user = DEVELOPER, targetPage = WelcomeToJiraPage.class)
    public void testOnboardingAsNonAdmin() {
        goThroughChooseLanguage();
        goThroughAvatarPicker();
        goThroughChooseNextStep();
    }

    @Test
    @LoginAs(admin = true, targetPage = WelcomeToJiraPage.class)
    public void testChooseYourOwnAdventureCreateProject() {
        goThroughChooseLanguage();
        goThroughAvatarPicker();
        try {
            final ProjectTemplatesDialog dialog = goToChooseYourAdventure().createEmptyProject()
                    .clickTemplate(TASK_MANAGEMENT_TEMPLATE)
                    .clickNext()
                    .clickNext()
                    .typeProjectName(PROJECT_NAME)
                    .clickNext();

            Poller.waitUntilFalse(dialog.isVisible());

            final Project createdProject = backdoor.project().getProject(PROJECT_KEY);
            assertEquals(PROJECT_NAME, createdProject.name);
            assertEquals("business", createdProject.projectTypeKey);
        } finally {
            backdoor.project().deleteProject(PROJECT_KEY);
        }
    }

    private ChooseYourOwnAdventureSequence goToChooseYourAdventure() {
        return pageBinder.bind(ChooseYourOwnAdventureSequence.class);
    }

    @Test
    @LoginAs(admin = true, targetPage = WelcomeToJiraPage.class)
    public void testOnboardingAsAdminCyoaRedirectToImportProject() {
        goThroughChooseLanguage();
        goThroughAvatarPicker();
        ChooseYourOwnAdventureSequence cyoa = goToChooseYourAdventure();

        assertThat("There should be 3 steps to choose from", cyoa.getStepBoxes(), hasSize(3));
        assertThat("First step should contain sample data button", cyoa.getStepBoxes().get(0), hasProperty("buttonId", equalTo("sampleData")));
        assertThat("First step should contain correct image with search icon", cyoa.getStepBoxes().get(0), hasProperty("imgSrc", endsWith("cyoa-search.svg")));
        assertThat("Second step should contain create project button", cyoa.getStepBoxes().get(1), hasProperty("buttonId", equalTo("emptyProject")));
        assertThat("Second step should contain correct image with pencil", cyoa.getStepBoxes().get(1), hasProperty("imgSrc", endsWith("cyoa-create.svg")));
        assertThat("Third step should contain import project button", cyoa.getStepBoxes().get(2), hasProperty("buttonId", equalTo("import")));
        assertThat("Third step should contain correct image with vacuum cleaner", cyoa.getStepBoxes().get(2), hasProperty("imgSrc", endsWith("cyoa-import.svg")));

        cyoa.importProjectClick();
        pageBinder.bind(JIMOnboardingPage.class);
    }

    @Test
    @LoginAs(admin = true)
    public void testOnboardingShouldNotEnableHeaderWhenNoProjectExist() {
        try {
            backdoor.project().getProjects().forEach(project -> backdoor.project().deleteProject(project.key));
            assertThat("There are no projects", backdoor.project().getProjects(), is(empty()));

            jira.goTo(WelcomeToJiraPage.class);
            assertHeaderDisabled();
            goThroughChooseLanguage();
            assertHeaderDisabled();
            goThroughAvatarPicker();
            assertHeaderDisabled();
        } finally {
            backdoor.restoreBlankInstance();
            backdoor.getTestkit().darkFeatures().enableForSite(ONBOARDING_CYOA_DARK_FEATURE);
        }
    }

    @Test
    @LoginAs(admin = true)
    public void testOnboardingShouldEnableHeaderWhenProjectsExist() {
        assertThat("There are some projects", backdoor.project().getProjects(), is(not(empty())));

        jira.goTo(WelcomeToJiraPage.class);
        assertHeaderDisabled();
        goThroughChooseLanguage();
        assertHeaderDisabled();
        goThroughAvatarPicker();
        assertHeaderEnabled();
        // create issue should be available
        goToChooseYourAdventure().getHeader().createIssue().waitUntilFinishedLoading();
    }

    @Test
    public void testAdminCanGoToDashboardWhenProjectExist() {
        assertThat("There are some projects", backdoor.project().getProjects(), is(not(empty())));
        try {
            jira.backdoor().usersAndGroups().addUser(SECOND_ADMIN);
            jira.backdoor().usersAndGroups().addUserToGroup(SECOND_ADMIN, FunctTestConstants.JIRA_ADMIN_GROUP);

            jira.quickLogin(SECOND_ADMIN, SECOND_ADMIN, WelcomeToJiraPage.class);
            goThroughChooseLanguage();
            goThroughAvatarPicker();

            goToChooseYourAdventure().getHeader().getLogo().click();
            pageBinder.bind(DashboardPage.class);
        } finally {
            jira.backdoor().usersAndGroups().deleteUser(SECOND_ADMIN);
        }
    }


    private void goThroughChooseLanguage() {
        ChooseLanguageSequence chooseLanguageSequence = pageBinder.bind(ChooseLanguageSequence.class);
        chooseLanguageSequence.nextStep();
    }

    private void goThroughAvatarPicker() {
        AvatarSequence avatarSequence = pageBinder.bind(AvatarSequence.class);
        avatarSequence.done();
    }

    private void assertHeaderDisabled() {
        assertTrue("Header is disabled", pageBinder.bind(JiraHeader.class).isDisabled());
    }

    private void assertHeaderEnabled() {
        assertFalse("Header is not disabled", pageBinder.bind(JiraHeader.class).isDisabled());
    }

    private void goThroughChooseNextStep() {
        ChooseNextStepSequence chooseNextStepSequence = pageBinder.bind(ChooseNextStepSequence.class);
        chooseNextStepSequence.clickBrowseProjects();
        pageBinder.bind(BrowseProjectsPage.class);
    }
}
