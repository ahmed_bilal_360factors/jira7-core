package com.atlassian.jira.webtest.webdriver.tests.permissions;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.EnableAUIFlags;
import com.atlassian.jira.pageobjects.elements.AuiFlag;
import com.atlassian.jira.pageobjects.elements.GlobalFlags;
import com.atlassian.jira.pageobjects.pages.EditPermissionsSinglePage;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static org.junit.Assert.assertNotNull;

@WebTest(Category.WEBDRIVER_TEST)
@EnableAUIFlags
public class TestPermissionSchemeCustomFields extends BaseJiraWebTest {
    private static final String NO_SEARCHER_CF_ID = "customfield_10002";
    private static final String NO_SEARCHER_CF_NAME = "nosearchercf";
    private static final String USER_SEARCHER_FULL_KEY = "com.atlassian.jira.plugin.system.customfieldtypes:userpickergroupsearcher";


    @Before
    public void setUp() {
        backdoor.userProfile().changeUserLanguage(FunctTestConstants.ADMIN_USERNAME, FunctTestConstants.MOON_LOCALE);
    }

    @Test
    @Restore("xml/TestCustomFieldsNoSearcherPermissions.xml")
    public void testAddCustomFieldWithoutSearcherToPermission() {
        EditPermissionsSinglePage editPermissions =  jira.visit(EditPermissionsSinglePage.class, DEFAULT_PERM_SCHEME_ID);
        EditPermissionsSinglePage.PermissionsEntry entry = editPermissions.getPermissionEntry(ProjectPermissions.CREATE_ISSUES);

        EditPermissionsSinglePage.GrantPermissionsDialog dialog = entry.openGrantPermissionDialog();

        dialog.setUserCF(NO_SEARCHER_CF_NAME);
        dialog.submit();
        GlobalFlags flags = pageBinder.bind(GlobalFlags.class);
        Poller.waitUntilTrue(flags.flagContainerPresent());

        AuiFlag invalidGrantPermisionFlag = flags.getFlagWithText("admin.permissions.errors.customfieldnotindexed");
        assertNotNull("flag notifying an invalid field should be present", invalidGrantPermisionFlag);
    }



    /**
     * Test that adding a searcher to the customfield, makes it possible for that customfield to be added to a permission.
     */
    @Test
    @Restore("xml/TestCustomFieldsNoSearcherPermissions.xml")
    public void testAddingSearcherToCustomField() {
        backdoor.customFields().updateCustomField(NO_SEARCHER_CF_ID, NO_SEARCHER_CF_NAME, "", USER_SEARCHER_FULL_KEY);

        EditPermissionsSinglePage editPermissions =  jira.visit(EditPermissionsSinglePage.class, DEFAULT_PERM_SCHEME_ID);
        EditPermissionsSinglePage.PermissionsEntry entry = editPermissions.getPermissionEntry(ProjectPermissions.CREATE_ISSUES);

        EditPermissionsSinglePage.GrantPermissionsDialog dialog = entry.openGrantPermissionDialog();

        dialog.setUserCF(NO_SEARCHER_CF_NAME);
        dialog.submitAssertSuccessful();
    }
}
