package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.rule.IssueTypeUrls;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.admin.issuetype.ChangeIssueTypeSchemePage;
import com.atlassian.jira.pageobjects.project.summary.ProjectSummaryPageTab;
import com.atlassian.jira.pageobjects.project.summary.issuetypes.IssueTypesPanel;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Web test for the project configuration summary page's Issue Types panel.
 *
 * @since v4.4
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS, Category.IGNITE})
@Restore("xml/projectconfig/TestProjectConfigSummaryIssueTypesPanel.xml")
public class TestSummaryIssueTypesPanel extends BaseJiraWebTest {
    @Rule
    public IssueTypeUrls issueTypeUrls;

    private static final String HSP_KEY = "HSP";
    private static final String XSS_KEY = "XSS";
    private static final String MKY_KEY = "MKY";

    private static List<IssueTypesPanel.IssueTypeListItem> HSP_ISSUE_TYPES;
    private static List<IssueTypesPanel.IssueTypeListItem> XSS_ISSUE_TYPES;
    private static List<IssueTypesPanel.IssueTypeListItem> XSS_ISSUE_TYPES_WITH_BUG_ADDED;
    private static List<IssueTypesPanel.IssueTypeListItem> XSS_ISSUE_TYPES_WITH_BUG_AND_REORDERED;
    private static String baseUrl;

    @Before
    public void setUp() {
        baseUrl = jira.getProductInstance().getBaseUrl();
        String HSPUrl = createIssueTypesUrl("HSP");
        String XSSUrl = createIssueTypesUrl("XSS");
        issueTypeUrls = IssueTypeUrls.init(backdoor);

        HSP_ISSUE_TYPES = Lists.newArrayList(
                createListItem("<script>alert(\"wtf\");</script>",
                        "/images/icons/newfeature.gif%22%20onLoad=%22alert('wtf')",
                        HSPUrl + "/2", false),
                createListItem("Bug", issueTypeUrls.getIssueTypeUrl("bug"), HSPUrl + "/1", false),
                createListItem("Epic", issueTypeUrls.getIssueTypeUrl("epic"), HSPUrl + "/5", false),
                createListItem("Improvement", issueTypeUrls.getIssueTypeUrl("improvement"), HSPUrl + "/4",
                        false),
                createListItem("Sub-task", issueTypeUrls.getIssueTypeUrl("sub-task"), HSPUrl + "/6", true),
                createListItem("Task", issueTypeUrls.getIssueTypeUrl("task"), HSPUrl + "/3", false)
        );

        XSS_ISSUE_TYPES = Lists.newArrayList(
                createListItem("Epic", issueTypeUrls.getIssueTypeUrl("epic"), HSPUrl + "/5", false)
        );

        XSS_ISSUE_TYPES_WITH_BUG_ADDED = Lists.newArrayList(
                createListItem("Bug", issueTypeUrls.getIssueTypeUrl("bug"), XSSUrl + "/1", false),
                createListItem("Epic", issueTypeUrls.getIssueTypeUrl("epic"), XSSUrl + "/5", false)
        );

        XSS_ISSUE_TYPES_WITH_BUG_AND_REORDERED = Lists.newArrayList(
                createListItem("Bug", issueTypeUrls.getIssueTypeUrl("bug"), XSSUrl + "/1", false),
                createListItem("Epic", issueTypeUrls.getIssueTypeUrl("epic"), XSSUrl + "/5", false)
        );
    }

    @Test
    public void testListIssueTypesForProject() {
        final List<IssueTypesPanel.IssueTypeListItem> issueTypes = navigateToSummaryPageFor(HSP_KEY)
                .openPanel(IssueTypesPanel.class)
                .issueTypes();

        assertEquals(HSP_ISSUE_TYPES, issueTypes);
    }

//    @Test
//    public void testChangeIssueTypesInCurrentIssueTypeScheme()
//    {
//        final ProjectSummaryPageTab hspSummaryPage = navigateToSummaryPageFor(XSS_KEY);
//
//        final IssueTypesPanel issueTypesPanel = hspSummaryPage.openPanel(IssueTypesPanel.class);
//        final List<IssueTypeListItem> issueTypes = issueTypesPanel.issueTypes();
//        assertEquals(XSS_ISSUE_TYPES, issueTypes);
//
//        assertEquals(issueTypesPanel.getIssueTypeTabUrl(), createIssueTypesUrl(XSS_KEY));
//
//        pageBinder.navigateToAndBind(EditIssueTypeSchemePage.class, "10010", "10010")
//                .moveFromAvailableToBelowSelected("Bug", "Epic")
//                .submitSave();
//
//        final List<IssueTypeListItem> bugIssueTypeList = navigateToSummaryPageFor(XSS_KEY)
//                .openPanel(IssueTypesPanel.class)
//                .issueTypes();
//
//        assertEquals(XSS_ISSUE_TYPES_WITH_BUG_ADDED, bugIssueTypeList);
//
//    }

    @Test
    public void testChangeToDifferentIssueTypeScheme() {
        final IssueTypesPanel issueTypesPanel = navigateToSummaryPageFor(HSP_KEY)
                .openPanel(IssueTypesPanel.class);
        final List<IssueTypesPanel.IssueTypeListItem> originalIssueTypes = issueTypesPanel.issueTypes();

        assertEquals(HSP_ISSUE_TYPES, originalIssueTypes);

        pageBinder.navigateToAndBind(ChangeIssueTypeSchemePage.class, "10000")
                .chooseExistingIssueTypeScheme("Improved Issue Type Scheme");

        final IssueTypesPanel modifiedIssueTypesPanel = navigateToSummaryPageFor(HSP_KEY)
                .openPanel(IssueTypesPanel.class);
        final List<IssueTypesPanel.IssueTypeListItem> modifiedIssueTypes = modifiedIssueTypesPanel.issueTypes();

        assertEquals("Improved Issue Type Scheme", modifiedIssueTypesPanel.getIssueTypeTabLinkText());
        assertEquals(XSS_ISSUE_TYPES, modifiedIssueTypes);

    }

    @Test
    public void testCorrectIssueTypeOrder() {
        pageBinder.navigateToAndBind(ChangeIssueTypeSchemePage.class, "10010")
                .chooseExistingIssueTypeScheme("BugBelowEpic");

        final IssueTypesPanel issueTypesPanel = navigateToSummaryPageFor(XSS_KEY)
                .openPanel(IssueTypesPanel.class);
        final List<IssueTypesPanel.IssueTypeListItem> epicAndBugIssueTypeList = issueTypesPanel
                .issueTypes();

        assertEquals(XSS_ISSUE_TYPES_WITH_BUG_ADDED, epicAndBugIssueTypeList);

        pageBinder.navigateToAndBind(ChangeIssueTypeSchemePage.class, "10010")
                .chooseExistingIssueTypeScheme("EpicBelowBug");

        final List<IssueTypesPanel.IssueTypeListItem> bugAndEpicIssueTypeList = navigateToSummaryPageFor(XSS_KEY)
                .openPanel(IssueTypesPanel.class)
                .issueTypes();

        assertEquals(XSS_ISSUE_TYPES_WITH_BUG_AND_REORDERED, bugAndEpicIssueTypeList);

    }

    @Test
    public void testNoIssueTypesInCurrentIssueTypeScheme() {
        final IssueTypesPanel issueTypesPanel =
                navigateToSummaryPageFor(MKY_KEY).openPanel(IssueTypesPanel.class);

        final List<IssueTypesPanel.IssueTypeListItem> emptyIssueTypesList = issueTypesPanel
                .issueTypes();

        assertEquals(Collections.EMPTY_LIST, emptyIssueTypesList);
        assertEquals("No issue types associated", issueTypesPanel.getNoIssueTypesMessage());

    }

    private ProjectSummaryPageTab navigateToSummaryPageFor(final String projectKey) {
        return pageBinder.navigateToAndBind(ProjectSummaryPageTab.class, projectKey);
    }

    private IssueTypesPanel.IssueTypeListItem createListItem(final String issueTypeName, final String iconImage,
                                                             final String issueTypeLink, boolean isSubtask) {
        return new IssueTypesPanel.IssueTypeListItem(issueTypeName, baseUrl + iconImage, issueTypeLink, isSubtask);
    }

    private String createIssueTypesUrl(String projectKey) {
        return baseUrl + "/plugins/servlet/project-config/" + projectKey + "/issuetypes";
    }
}
