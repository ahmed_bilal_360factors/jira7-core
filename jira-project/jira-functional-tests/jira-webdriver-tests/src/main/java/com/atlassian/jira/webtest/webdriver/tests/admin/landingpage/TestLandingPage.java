package com.atlassian.jira.webtest.webdriver.tests.admin.landingpage;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.jira.projects.pageobjects.webdriver.page.sidebar.SidebarBrowseProjectPage;
import com.atlassian.jira.test.util.lic.ApplicationLicenseConstants;
import com.atlassian.jira.webtests.LicenseKeys;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;

@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION})
public class TestLandingPage extends BaseJiraWebTest {

    public static final String TEST = "TEST";
    public static final String PROPERTY_POST_MIGRATION_PAGE_DISPLAYED = "post.migration.page.displayed";

    @Before
    public void setUp() {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_AND_TEST_ROLES.getLicenseString());
        backdoor.applicationRoles().putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.CORE_KEY, false, ImmutableList.of("jira-users"), ImmutableList.of("jira-users"));
        backdoor.applicationProperties().setOption(PROPERTY_POST_MIGRATION_PAGE_DISPLAYED, true);
    }

    @Test
    public void creatingProjectWorks() {
        final LandingPage page = jira.goTo(LandingPage.class);

        createProject(page, TEST);

        final SidebarBrowseProjectPage test = pageBinder.bind(SidebarBrowseProjectPage.class, TEST);
        Poller.waitUntilTrue(test.getSidebar().isPresent());
    }

    @Test
    public void cancelProjectCreationWorks() {
        final LandingPage page = jira.goTo(LandingPage.class);

        page.cancel();

        pageBinder.bind(DashboardPage.class);
    }

    private void createProject(final LandingPage page, final String project) {
        page.next();
        page.select();
        page.setName(project);
        page.setKey(project);
        page.create();
    }
}
