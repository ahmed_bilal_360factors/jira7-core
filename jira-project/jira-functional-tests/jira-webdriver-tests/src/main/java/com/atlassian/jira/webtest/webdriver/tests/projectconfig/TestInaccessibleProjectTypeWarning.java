package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.project.summary.ProjectSummaryPageTab;
import com.atlassian.jira.pageobjects.project.types.warning.InaccessibleProjectTypeWarning;
import com.atlassian.jira.pageobjects.project.types.warning.InaccessibleProjectTypeWarningDialog;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.projects.pageobjects.webdriver.page.sidebar.Sidebar;
import com.atlassian.jira.test.util.lic.core.CoreLicenses;
import com.atlassian.jira.webtest.webdriver.util.AUIFlags;
import com.atlassian.pageobjects.binder.PageBindingWaitException;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@WebTest(com.atlassian.jira.functest.framework.suite.Category.WEBDRIVER_TEST)
public class TestInaccessibleProjectTypeWarning extends BaseJiraWebTest {
    private static final String UNINSTALLED_TYPE_PROJECT_KEY = "T1";
    private static final String UNLICENSED_TYPE_PROJECT_KEY = "T2";
    private static final String INSTALLED_AND_LICENSED_TYPE_PROJECT_KEY = "T3";

    private static final String UNINSTALLED_TYPE_NAME = "Uninstalled Type";
    private static final String UNLICENSED_TYPE_NAME = "First-type";

    @Inject
    private TraceContext traceContext;

    @Inject
    private PageElementFinder pageElementFinder;

    @Before
    public void setUp() {
        backdoor.restoreDataFromResource("xml/projectconfig/TestInaccessibleProjectTypeWarning.xml");
        backdoor.license().set(CoreLicenses.LICENSE_CORE);
    }

    @Test
    public void warningMessageIsDisplayedForGlobalAdminForAProjectTypeThatIsUninstalled() {
        goToAdminPage(UNINSTALLED_TYPE_PROJECT_KEY);

        InaccessibleProjectTypeWarningDialog dialog = pageBinder.bind(InaccessibleProjectTypeWarning.class).click();
        assertThat(dialog.isForProjectType(UNINSTALLED_TYPE_NAME), is(true));
        assertThat(dialog.isForAnUninstalledProjectType(), is(true));
        assertProjectTypeCanBeChanged(dialog);
    }

    @Test
    public void warningMessageIsDisplayedForGlobalAdminForAProjectTypeThatIsUnlicensed() {
        goToAdminPage(UNLICENSED_TYPE_PROJECT_KEY);

        InaccessibleProjectTypeWarningDialog dialog = pageBinder.bind(InaccessibleProjectTypeWarning.class).click();
        assertThat(dialog.isForProjectType(UNLICENSED_TYPE_NAME), is(true));
        assertThat(dialog.isForAnUnlicensedProjectType(), is(true));
        assertProjectTypeCanBeChanged(dialog);
    }

    @Test
    public void warningMessageIsNotDisplayedForGlobalAdminForAProjectThatIsInstalledAndLicensed() {
        goToAdminPage(INSTALLED_AND_LICENSED_TYPE_PROJECT_KEY);

        assertWarningMessageIsNotDisplayed();
    }

    @Test
    public void warningMessageIsDisplayedForProjectAdminForAProjectTypeThatIsUninstalled() {
        logInWithNonGlobalAdmin(UNINSTALLED_TYPE_PROJECT_KEY);

        goToAdminPage(UNINSTALLED_TYPE_PROJECT_KEY);

        InaccessibleProjectTypeWarningDialog dialog = pageBinder.bind(InaccessibleProjectTypeWarning.class).click();
        assertThat(dialog.isForProjectType(UNINSTALLED_TYPE_NAME), is(true));
        assertThat(dialog.isForAnUninstalledProjectType(), is(true));
        assertProjectTypeCanBeChanged(dialog);
    }

    @Test
    public void warningMessageIsDisplayedForProjectAdminForAProjectTypeThatIsUnlicensed() {
        logInWithNonGlobalAdmin(UNLICENSED_TYPE_PROJECT_KEY);

        goToAdminPage(UNLICENSED_TYPE_PROJECT_KEY);

        InaccessibleProjectTypeWarningDialog dialog = pageBinder.bind(InaccessibleProjectTypeWarning.class).click();
        assertThat(dialog.isForProjectType(UNLICENSED_TYPE_NAME), is(true));
        assertThat(dialog.isForAnUnlicensedProjectType(), is(true));
        assertProjectTypeCanBeChanged(dialog);
    }

    @Test
    public void warningMessageIsNotDisplayedForProjectAdminForAProjectThatIsInstalledAndLicensed() {
        logInWithNonGlobalAdmin(INSTALLED_AND_LICENSED_TYPE_PROJECT_KEY);

        goToAdminPage(INSTALLED_AND_LICENSED_TYPE_PROJECT_KEY);

        assertWarningMessageIsNotDisplayed();
    }

    private void assertProjectTypeCanBeChanged(InaccessibleProjectTypeWarningDialog dialog) {
        dialog.clickChangeProjectType();
    }

    private void logInWithNonGlobalAdmin(String projectKey) {
        backdoor.permissionsOf("fred").forProject(projectKey)
                .add(ProjectPermissions.BROWSE_PROJECTS)
                .add(ProjectPermissions.ADMINISTER_PROJECTS);
        jira.quickLogin("fred", "fred");
    }

    private void assertWarningMessageIsNotDisplayed() {
        try {
            pageBinder.bind(InaccessibleProjectTypeWarning.class);
            Assert.fail("The warning message should have not been present on the page");
        } catch (PageBindingWaitException e) {
            // do nothing: this is expected
        }
    }

    private ProjectSummaryPageTab goToAdminPage(String projectKey) {
        Tracer tracer = traceContext.checkpoint();
        final ProjectSummaryPageTab config = jira.goTo(ProjectSummaryPageTab.class, projectKey);
        traceContext.waitFor(tracer, "project.types.warning.messages.init");
        pageBinder.bind(AUIFlags.class).closeAllFlags();

        //Make sure that the sidebar is present and open
        Sidebar sidebar = pageBinder.bind(Sidebar.class, projectKey);
        sidebar.expandSidebar();

        return config;
    }
}
