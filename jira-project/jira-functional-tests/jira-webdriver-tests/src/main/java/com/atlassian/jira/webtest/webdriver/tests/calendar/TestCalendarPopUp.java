package com.atlassian.jira.webtest.webdriver.tests.calendar;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.CalendarPicker;
import com.atlassian.jira.pageobjects.components.CalendarPopup;
import com.atlassian.jira.pageobjects.dialogs.FormDialog;
import com.atlassian.jira.pageobjects.dialogs.LogWorkDialog;
import com.atlassian.jira.pageobjects.dialogs.quickedit.CreateIssueDialog;
import com.atlassian.jira.pageobjects.dialogs.quickedit.FieldPicker;
import com.atlassian.jira.pageobjects.dialogs.quickedit.WorkflowTransitionDialog;
import com.atlassian.jira.pageobjects.model.DefaultIssueActions;
import com.atlassian.jira.pageobjects.model.WorkflowIssueAction;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.junit.After;
import org.junit.Test;

import javax.inject.Inject;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.startsWith;

/**
 * Webdriver test for the Calendar Popup
 *
 * @since v5.2
 */
@WebTest({Category.WEBDRIVER_TEST, Category.CUSTOM_FIELDS})
public class TestCalendarPopUp extends BaseJiraWebTest {
    @Inject
    private PageElementFinder pageElementFinder;

    private static final String ISSUE = "HSP-1";
    private static final WorkflowIssueAction TEST_TRANSITION = new WorkflowIssueAction(711L, "Test");

    private FormDialog openDialog;

    @After
    public void closeDialog() {
        if (openDialog != null) {
            openDialog.close();
        }

    }

    private <T> T getOpenDialog(Class<T> dialogClass) {
        return dialogClass.cast(openDialog);
    }

    /**
     * JRADEV-2725, JRADEV-2747: Make sure the calendar works in dialogs on the view issue.
     */
    @Test
    @Restore("xml/TestCalendarInDialog.xml")
    public void testDateTimePickerInLogWorkDialogFromViewIssue() throws Exception {
        final ViewIssuePage viewIssuePage = jira.goTo(ViewIssuePage.class, ISSUE);

        openDialog = viewIssuePage.getIssueMenu().invoke(DefaultIssueActions.LOG_WORK, LogWorkDialog.class);
        final LogWorkDialog logWorkDialog = getOpenDialog(LogWorkDialog.class);
        Poller.waitUntilTrue("Log Work Dialog did not open successfully", logWorkDialog.isOpen());
        testCalendarPopupSelectsDayByClick(logWorkDialog.getDateStarted());
    }

    @Test
    @Restore("xml/TestCalendarInDialog.xml")
    public void testDateTimePickerInCloseIssueDialogFromViewIssue() throws Exception {
        final ViewIssuePage viewIssuePage = jira.goTo(ViewIssuePage.class, ISSUE);

        openDialog = viewIssuePage.getIssueMenu().invokeWorkflowAction(WorkflowIssueAction.CLOSE_ISSUE);
        final WorkflowTransitionDialog closeIssue = getOpenDialog(WorkflowTransitionDialog.class);
        testCalendarPopupSelectsDayByClick(closeIssue.getCustomField(CalendarPicker.class, 10000));
        testCalendarPopupSelectsDayByClick(closeIssue.getCustomField(CalendarPicker.class, 10001));
    }


    @Test
    @Restore("xml/TestCalendarInDialog.xml")
    public void testDateTimePickerInTestDialogFromViewIssue() throws Exception {
        final ViewIssuePage viewIssuePage = jira.goTo(ViewIssuePage.class, ISSUE);

        openDialog = viewIssuePage.getIssueMenu().invokeWorkflowAction(TEST_TRANSITION);
        final WorkflowTransitionDialog testIssue = getOpenDialog(WorkflowTransitionDialog.class);
        testCalendarPopupSelectsDayByClick(testIssue.getCustomField(CalendarPicker.class, 10000));
        testCalendarPopupSelectsDayByClick(testIssue.getCustomField(CalendarPicker.class, 10001));
    }

    /**
     * JRA-45703 - make sure the correct date is highlighted in the date picker
     */
    @Test
    @Restore("xml/TestCalendarInDialog.xml")
    public void testCreateIssueDatePickerSelectedDayInDifferentTimezones() throws Exception {
        testSelectedDayInCreateIssueDayPicker("Pacific/Midway"); // GMT-11 for both DST and non-DST
        testSelectedDayInCreateIssueDayPicker("Pacific/Auckland"); // GMT+12, GMT+13 for DST
    }

    private void testSelectedDayInCreateIssueDayPicker(String timezone) {
        int dayOfMonthBeforeTest;
        int dayOfMonthOnCalendar;
        int dayOfMonthAfterTest;
        jira.backdoor().userProfile().changeUserTimezone("admin", timezone);
        DateTimeZone dateTimeZone = DateTimeZone.forID(timezone);
        do {
            dayOfMonthBeforeTest = new DateTime(dateTimeZone).getDayOfMonth();
            jira.goTo(ViewIssuePage.class, ISSUE).execKeyboardShortcut("c");
            CreateIssueDialog createIssueDialog = pageBinder.bind(CreateIssueDialog.class, CreateIssueDialog.Type.ISSUE);
            Poller.waitUntilTrue("CreateIssueDialog was not opened.", createIssueDialog.isOpen());
            CalendarPicker calendarPicker = createIssueDialog.getDueDateCalendarPicker();
            CalendarPopup calendarPopup = calendarPicker.openCalendarPopup();
            dayOfMonthOnCalendar = calendarPopup.getSelectedDay().now();
            dayOfMonthAfterTest = new DateTime(dateTimeZone).getDayOfMonth();
        } while (dayOfMonthBeforeTest != dayOfMonthAfterTest); // if the date changed, run the test again, this will be rare
        assertThat(dayOfMonthOnCalendar, is(dayOfMonthAfterTest));
    }

    /**
     * JRA-45558 - make sure the correct date is used when clicking 'Today'
     */
    @Test
    @Restore("xml/TestCalendarInDialog.xml")
    public void testCreateIssueDatePickerTodayButtonInDifferentTimezones() throws Exception {
        testCreateIssueDatePickerTodayButton("Pacific/Midway"); // GMT-11 for both DST and non-DST
        testCreateIssueDatePickerTodayButton("Pacific/Auckland"); // GMT+12, GMT+13 for DST
    }

    private void testCreateIssueDatePickerTodayButton(String timezone) {
        String dateBeforeTest;
        String dateGeneratedByTodayButton;
        String dateAfterTest;
        jira.backdoor().userProfile().changeUserTimezone("admin", timezone);
        DateTimeZone dateTimeZone = DateTimeZone.forID(timezone);
        org.joda.time.format.DateTimeFormatter dtf = DateTimeFormat.forPattern("d/MMM/YY").withZone(dateTimeZone);
        do {
            dateBeforeTest = new DateTime(dateTimeZone).toString(dtf);
            jira.goTo(ViewIssuePage.class, ISSUE).execKeyboardShortcut("c");
            CreateIssueDialog createIssueDialog = pageBinder.bind(CreateIssueDialog.class, CreateIssueDialog.Type.ISSUE);
            Poller.waitUntilTrue("CreateIssueDialog was not opened.", createIssueDialog.isOpen());
            CalendarPicker calendarPicker = createIssueDialog.getDueDateCalendarPicker();
            CalendarPopup calendarPopup = calendarPicker.openCalendarPopup();
            calendarPopup.getTodayButton().click();
            dateGeneratedByTodayButton = calendarPicker.getDateValue().byDefaultTimeout();
            dateAfterTest = new DateTime(dateTimeZone).toString(dtf);
            createIssueDialog.fill(FieldPicker.SUMMARY, " I only need to get rid of this dialog").submit(ViewIssuePage.class, ISSUE);
        } while (!dateBeforeTest.equals(dateAfterTest)); // if the date changed, run the test again, this will be rare
        assertThat(dateGeneratedByTodayButton, is(dateAfterTest));

    }

    private void testCalendarPopupSelectsDayByClick(CalendarPicker calendarPicker) {
        final CalendarPopup calendarPopup = calendarPicker.openCalendarPopup();
        Poller.waitUntilTrue("Calender Popup did not open successfully", calendarPopup.isOpen());

        final int expectedDay = selectOtherDay(calendarPopup);
        Poller.waitUntilTrue(calendarPopup.isClosed());
        Poller.waitUntil("Calendar date was not set on the input field.", calendarPicker.getDateValue(), startsWith(Integer.toString(expectedDay)));
    }

    private int selectOtherDay(CalendarPopup popup) {
        final int selectedDay = popup.getSelectedDay().now();
        for (int day = 1; day <= 31; day++) {
            if (popup.hasDay(day) && day != selectedDay) {
                popup.selectDay(day);
                return day;
            }
        }
        throw new AssertionError("Ooops, this test is not smart enough");
    }

    /**
     * JRA-45558 - make sure the correct date-time is used when clicking 'Today'
     */
    @Test
    @Restore("xml/TestCalendarInDialog.xml")
    public void testLogWorkDatePickerTodayButtonInDifferentTimezones() throws Exception {
        testLogWorkDatePickerTodayButton("Pacific/Midway"); // GMT-11 for both DST and non-DST
        testLogWorkDatePickerTodayButton("Pacific/Auckland"); // GMT+12, GMT+13 for DST
    }

    private void testLogWorkDatePickerTodayButton(String timezone) {
        String dateTimeBeforeTest;
        String dateTimeGeneratedByTodayButton;
        String dateTimeAfterTest;
        jira.backdoor().userProfile().changeUserTimezone("admin", timezone);
        DateTimeZone dateTimeZone = DateTimeZone.forID(timezone);
        org.joda.time.format.DateTimeFormatter dtf = DateTimeFormat.forPattern("d/MMM/YY hh:mm aa").withZone(dateTimeZone);
        int numAttempts = 0;
        // repeat the test until we are sure that the minute hasn't ticked over (or up to 5 times)
        // re-running the loop should be rare - it takes 4-5 seconds to run
        do {
            dateTimeBeforeTest = new DateTime(dateTimeZone).toString(dtf);
            final ViewIssuePage viewIssuePage = jira.goTo(ViewIssuePage.class, ISSUE);
            LogWorkDialog logWorkDialog = viewIssuePage.getIssueMenu().invoke(DefaultIssueActions.LOG_WORK, LogWorkDialog.class);
            Poller.waitUntilTrue("LogWorkDialog was not opened.", logWorkDialog.isOpen());
            CalendarPicker dateStartedPicker = logWorkDialog.getDateStarted();
            CalendarPopup calendarPopup = dateStartedPicker.openCalendarPopup();
            calendarPopup.getTodayButton().click();
            dateTimeGeneratedByTodayButton = dateStartedPicker.getDateValue().byDefaultTimeout();
            dateTimeAfterTest = new DateTime(dateTimeZone).toString(dtf);
            logWorkDialog.close();
        } while (!dateTimeBeforeTest.equals(dateTimeAfterTest) && (numAttempts++ < 5));
        assertThat(dateTimeGeneratedByTodayButton, is(dateTimeAfterTest));

    }

}
