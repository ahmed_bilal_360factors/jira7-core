package com.atlassian.jira.webtest.webdriver.tests.issue;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.dialogs.quickedit.CreateIssueDialog;
import com.atlassian.jira.pageobjects.dialogs.quickedit.EditIssueDialog;
import com.atlassian.jira.pageobjects.framework.fields.CustomField;
import com.atlassian.jira.pageobjects.pages.admin.customfields.ConfigureCustomField;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import org.junit.Test;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Tests for version order
 *
 * @since v5.0
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ISSUES})
public class TestVersionCFOrderConfiguration extends BaseJiraWebTest {
    private static final String CUSTOM_FIELD_ID = "customfield_10100";
    private static final Long CUSTOM_FIELD_ID_AS_LONG = 10100L;

    @Inject
    private PageBinder pageBinder;

    @Test
    @Restore("xml/TestVersionCFOrderConfiguration.xml")
    public void testReleasedConfigFirstShowsUpFirstOnCreateIssue() {
        MultiVersionPickerCustomField customField = getMultiVersionPickerCustomFieldOnCreateIssueDialog();
        Iterable<String> firstGroupVersions = customField.getFirstGroupVersions();
        String firstGroupTitle = customField.getFirstGroupTitle();
        assertThat(firstGroupTitle, equalTo("releasedVersion"));
        assertThat(firstGroupVersions, containsInAnyOrder("Version 1.0", "Version 1.1"));
    }

    @Test
    @Restore("xml/TestVersionCFOrderConfiguration.xml")
    public void testUnreleasedConfigFirstShowsUpFirstOnCreateIssue() {
        ConfigureCustomField configureCustomField = jira.goTo(ConfigureCustomField.class, CUSTOM_FIELD_ID, CUSTOM_FIELD_ID_AS_LONG);
        configureCustomField.goToEditVersionCustomFieldOrder().setUnreleasedFirst();

        MultiVersionPickerCustomField customField = getMultiVersionPickerCustomFieldOnCreateIssueDialog();
        Iterable<String> firstGroupVersions = customField.getFirstGroupVersions();
        String firstGroupTitle = customField.getFirstGroupTitle();
        assertThat(firstGroupTitle, equalTo("unreleasedVersion"));
        assertThat(firstGroupVersions, containsInAnyOrder("Version 1.2", "Version 1.3", "Version 2.0", "Version 2.1"));
    }

    @Test
    @Restore("xml/TestVersionCFOrderConfigurationNoReleasedVersions.xml")
    public void testNoReleasedVersionsReleasedConfigFirstOnCreateIssue() {
        MultiVersionPickerCustomField customField = getMultiVersionPickerCustomFieldOnCreateIssueDialog();
        Iterable<String> firstGroupVersions = customField.getFirstGroupVersions();
        String firstGroupTitle = customField.getFirstGroupTitle();
        assertThat(firstGroupTitle, equalTo("unreleasedVersion"));
        assertThat(firstGroupVersions, containsInAnyOrder("Version 1.0", "Version 1.1", "Version 1.2"));
    }

    @Test
    @Restore("xml/TestVersionCFOrderConfigurationNoReleasedVersions.xml")
    public void testNoReleasedVersionsUnreleasedConfigFirstOnCreateIssue() {
        ConfigureCustomField configureCustomField = jira.goTo(ConfigureCustomField.class, CUSTOM_FIELD_ID, CUSTOM_FIELD_ID_AS_LONG);
        configureCustomField.goToEditVersionCustomFieldOrder().setUnreleasedFirst();

        MultiVersionPickerCustomField customField = getMultiVersionPickerCustomFieldOnCreateIssueDialog();
        Iterable<String> firstGroupVersions = customField.getFirstGroupVersions();
        String firstGroupTitle = customField.getFirstGroupTitle();
        assertThat(firstGroupTitle, equalTo("unreleasedVersion"));
        assertThat(firstGroupVersions, containsInAnyOrder("Version 1.0", "Version 1.1", "Version 1.2"));
    }

    @Test
    @Restore("xml/TestVersionCFOrderConfigurationNoUnreleasedVersions.xml")
    public void testNoUnreleasedVersionsUnreleasedConfigFirstOnCreateIssue() {
        ConfigureCustomField configureCustomField = jira.goTo(ConfigureCustomField.class, CUSTOM_FIELD_ID, CUSTOM_FIELD_ID_AS_LONG);
        configureCustomField.goToEditVersionCustomFieldOrder().setUnreleasedFirst();

        MultiVersionPickerCustomField customField = getMultiVersionPickerCustomFieldOnCreateIssueDialog();
        Iterable<String> firstGroupVersions = customField.getFirstGroupVersions();
        String firstGroupTitle = customField.getFirstGroupTitle();
        assertThat(firstGroupTitle, equalTo("releasedVersion"));
        assertThat(firstGroupVersions, containsInAnyOrder("Version 1.0", "Version 1.1", "Version 1.2"));
    }

    @Test
    @Restore("xml/TestVersionCFOrderConfigurationNoUnreleasedVersions.xml")
    public void testNoUnreleasedVersionsReleasedConfigFirstOnCreateIssue() {
        MultiVersionPickerCustomField customField = getMultiVersionPickerCustomFieldOnCreateIssueDialog();
        Iterable<String> firstGroupVersions = customField.getFirstGroupVersions();
        String firstGroupTitle = customField.getFirstGroupTitle();
        assertThat(firstGroupTitle, equalTo("releasedVersion"));
        assertThat(firstGroupVersions, containsInAnyOrder("Version 1.0", "Version 1.1", "Version 1.2"));
    }

    @Test
    @Restore("xml/TestVersionCFOrderConfiguration.xml")
    public void testReleasedConfigFirstShowsUpFirstOnEditIssue() {
        MultiVersionPickerCustomField customField = getMultiVersionPickerCustomFieldOnEditIssueDialog("HSP-1");
        Iterable<String> firstGroupVersions = customField.getFirstGroupVersions();
        String firstGroupTitle = customField.getFirstGroupTitle();
        assertThat(firstGroupTitle, equalTo("releasedVersion"));
        assertThat(firstGroupVersions, containsInAnyOrder("Version 1.0", "Version 1.1"));
    }

    @Test
    @Restore("xml/TestVersionCFOrderConfiguration.xml")
    public void testUnreleasedConfigFirstShowsUpFirstOnEditIssue() {
        ConfigureCustomField configureCustomField = jira.goTo(ConfigureCustomField.class, CUSTOM_FIELD_ID, CUSTOM_FIELD_ID_AS_LONG);
        configureCustomField.goToEditVersionCustomFieldOrder().setUnreleasedFirst();

        MultiVersionPickerCustomField customField = getMultiVersionPickerCustomFieldOnEditIssueDialog("HSP-1");
        Iterable<String> firstGroupVersions = customField.getFirstGroupVersions();
        String firstGroupTitle = customField.getFirstGroupTitle();
        assertThat(firstGroupTitle, equalTo("unreleasedVersion"));
        assertThat(firstGroupVersions, containsInAnyOrder("Version 1.2", "Version 1.3", "Version 2.0", "Version 2.1"));
    }

    private MultiVersionPickerCustomField getMultiVersionPickerCustomFieldOnCreateIssueDialog() {
        jira.gotoHomePage();
        final CreateIssueDialog createIssueDialog = pageBinder.bind(JiraHeader.class).createIssue();
        Poller.waitUntilTrue("CreateIssueDialog was not opened.", createIssueDialog.isOpen());
        createIssueDialog.selectProject("HSP");
        return createIssueDialog.getCustomField(CUSTOM_FIELD_ID, MultiVersionPickerCustomField.class);
    }

    private MultiVersionPickerCustomField getMultiVersionPickerCustomFieldOnEditIssueDialog(String issueKey) {
        final ViewIssuePage viewIssuePage = jira.goToViewIssue(issueKey);
        EditIssueDialog editIssueDialog = viewIssuePage.editIssue();
        Poller.waitUntilTrue("CreateIssueDialog was not opened.", editIssueDialog.isOpen());
        return editIssueDialog.getCustomField(MultiVersionPickerCustomField.class, CUSTOM_FIELD_ID);
    }

    public static class MultiVersionPickerCustomField implements CustomField {

        private PageElement field;

        public MultiVersionPickerCustomField(PageElement form, String id) {
            this.field = form.find(By.id(id));
        }

        public Iterable<String> getFirstGroupVersions() {
            PageElement firstGroup = field.find(By.tagName("optgroup"));
            Iterable<PageElement> options = firstGroup.findAll(By.tagName("option"));

            return Iterables.transform(options, new Function<PageElement, String>() {
                @Override
                public String apply(PageElement option) {
                    return option.getAttribute("title");
                }
            });
        }

        public String getFirstGroupTitle() {
            PageElement firstGroup = field.find(By.tagName("optgroup"));
            return firstGroup.getAttribute("data-version-group");
        }
    }
}

