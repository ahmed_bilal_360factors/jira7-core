package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.matchers.RegexMatchers;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.WindowSession;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.jira.pageobjects.pages.admin.user.AddUserPage;
import com.atlassian.jira.pageobjects.pages.admin.user.EditUserGroupsPage;
import com.atlassian.jira.pageobjects.project.EditProjectLeadAndDefaultAssigneeDialog;
import com.atlassian.jira.pageobjects.project.roles.RolesPage;
import com.atlassian.jira.testkit.beans.UserDTO;
import com.atlassian.jira.webtest.webdriver.util.AUIFlags;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import com.google.common.collect.Lists;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS, Category.IGNITE})
public class TestProjectRoles extends BaseJiraWebTest {
    @Inject
    private PageElementFinder finder;

    AUIFlags auiFlags;

    private static final String HSP_KEY = "HSP";
    private static final String MKY_KEY = "MKY";
    private static final String XSS_KEY = "XSS";
    private static final String BLUK_KEY = "BLUK";
    private static final String TST_KEY = "TST";
    private static final String LARGE_PROJECT_KEY = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private static final String EDIT_DEFAULTS_LEAD_FIELD = "lead";

    private static final String PROJECT_LEAD = "Project Lead";
    private static final String UNASSIGNED_ASSIGNEE = "Unassigned";

    private static final long ADMINISTRATORS_ROLE_ID = 10002;
    private static final long DEVELOPERS_ROLE_ID = 10001;
    private static final long USERS_ROLE_ID = 10000;

    private static final String ADMINISTRATORS_ROLE_NAME = "Administrators";
    private static final String DEVELOPERS_ROLE_NAME = "Developers";
    private static final String USERS_ROLE_NAME = "Users";

    private static final String ADMIN_GROUP = "jira-administrators";
    private static final String DEVELOPER_GROUP = "jira-developers";
    private static final String USER_GROUP = "jira-users";

    private static final String ADMIN_FULLNAME = "Administrator";
    private static final String ADMIN_USER_NAME = "admin";

    private static final String FRED_FULLNAME = "Fred Normal";
    private static final String FRED_USER_NAME = "fred";

    private static final String NON_EXISTENT_USERNAME = "adminxxx";
    private static final String DELETED_USERNAME = "mark";

    @Before
    public void setUp() {
        backdoor.restoreDataFromResource("xml/projectconfig/TestProjectConfigPeople.xml");
        auiFlags = pageBinder.bind(AUIFlags.class);
    }

    @AfterClass
    public static void tearDown() {
        backdoor.applicationProperties().enableXsrfChecking();
    }

    @Rule
    public WebDriverScreenshotRule webDriverScreenshotRule = new WebDriverScreenshotRule(); // The screenshot is then saved in jira-project-config-plugin/target/webdriverTests

    @Test
    public void testCanViewRoles() {
        final RolesPage rolesPage = jira.goTo(RolesPage.class, MKY_KEY);
        final Pattern noAvatarUrl = Pattern.compile(".*/secure/useravatar\\?size=xsmall&avatarId=[0-9]+");

        new ProjectLeadInfo()
                .name(ADMIN_FULLNAME)
                .avatar(noAvatarUrl)
                .verifyOn(rolesPage);

        new DefaultAssigneeInfo()
                .name(PROJECT_LEAD)
                .verifyOn(rolesPage);

        RoleInfo.administrators()
                .group(ADMIN_GROUP)
                .user(ADMIN_FULLNAME)
                .verifyOn(rolesPage);

        RoleInfo.developers()
                .group(DEVELOPER_GROUP)
                .user(FRED_FULLNAME)
                .verifyOn(rolesPage);

        RoleInfo.users()
                .group(USER_GROUP)
                .user(ADMIN_FULLNAME)
                .verifyOn(rolesPage);
    }

    @Test
    public void testCanEditRoles() {
        backdoor.flags().enableFlags();
        final RolesPage rolesPage = jira.goTo(RolesPage.class, MKY_KEY);
        auiFlags.closeAllFlags();

        String successMessage = rolesPage.addActorsToRole(ADMINISTRATORS_ROLE_ID, FRED_USER_NAME, DEVELOPER_GROUP);
        assertEquals("1 group and 1 user added to the Administrators role", successMessage);

        rolesPage.removeGroupFromRole(ADMINISTRATORS_ROLE_ID, ADMIN_GROUP);
        rolesPage.removeUserFromRole(ADMINISTRATORS_ROLE_ID, ADMIN_USER_NAME);

        RoleInfo.administrators()
                .group(DEVELOPER_GROUP)
                .user(FRED_FULLNAME)
                .verifyOn(rolesPage);
    }

    @Test
    public void testEmptyRoles() {
        backdoor.flags().enableFlags();
        final RolesPage rolesPage = jira.goTo(RolesPage.class, MKY_KEY);
        auiFlags.closeAllFlags();

        rolesPage.removeAllFromRole(ADMINISTRATORS_ROLE_ID);
        rolesPage.removeAllFromRole(DEVELOPERS_ROLE_ID);
        rolesPage.removeAllFromRole(USERS_ROLE_ID);

        RoleInfo.administrators().verifyOn(rolesPage);
        RoleInfo.developers().verifyOn(rolesPage);
        RoleInfo.users().verifyOn(rolesPage);

        String successMessage = rolesPage.addActorsToRoleUsingEmptyRolesButton(ADMINISTRATORS_ROLE_ID, ADMIN_GROUP, ADMIN_USER_NAME);
        assertEquals("1 group and 1 user added to the Administrators role", successMessage);

        RoleInfo.administrators()
                .group(ADMIN_GROUP)
                .user(ADMIN_FULLNAME)
                .verifyOn(rolesPage);
    }

    @Test
    public void testAddUserToRoleWithNoBrowseUsersPermission() {
        backdoor.restoreDataFromResource("xml/projectconfig/NoBrowseUsersPermissionWithAdminAsProjectAdmin.xml");
        backdoor.flags().enableFlags();
        final RolesPage rolesPage = jira.goTo(RolesPage.class, MKY_KEY);

        auiFlags.closeAllFlags();

        String successMessage = rolesPage.addActorsToRole(ADMINISTRATORS_ROLE_ID, FRED_USER_NAME);
        assertEquals("1 user added to the Administrators role", successMessage);

        RoleInfo.administrators()
                .group(ADMIN_GROUP)
                .user(ADMIN_FULLNAME)
                .user(FRED_FULLNAME)
                .verifyOn(rolesPage);

        // add Fred again will show error
        String errorMessage = rolesPage.addActorsToRoleNoReRender(ADMINISTRATORS_ROLE_ID, FRED_USER_NAME);
        assertEquals("These users are already in the Administrators role", errorMessage);
    }

    @Test
    public void testEditRolesWithServerError() {
        backdoor.applicationProperties().disableXsrfChecking();
        backdoor.flags().enableFlags();
        // create new user
        jira.goTo(AddUserPage.class)
                .addUser("project", "project", "project", "project@project.com", false).createUser();

        // make that user global admin
        pageBinder.navigateToAndBind(EditUserGroupsPage.class, "project")
                .addTo(Lists.newArrayList(ADMIN_GROUP));

        // log that user into roles page
        final RolesPage rolesPage = jira.quickLogin("project", "project", RolesPage.class, MKY_KEY);

        // open new window
        final WindowSession windowSession = jira.windowSession();
        final WindowSession.BrowserWindow newWindow = windowSession.openNewWindow("newWindow");
        newWindow.switchTo();

        // login as sysadmin to remove that user from global admin group
        jira.quickLoginAsSysadmin(EditUserGroupsPage.class, "project")
                .removeFrom(Lists.newArrayList(ADMIN_GROUP));

        // login as that user
        jira.quickLogin("project", "project", DashboardPage.class);

        // close that new window and switch back to roles page on original window
        newWindow.close();
        windowSession.switchToDefault();

        auiFlags.closeAllFlags();

        // add actor to role will show error because that user does not have admin permission anymore
        String addErrorMessage = rolesPage.addActorsToRoleNoReRender(ADMINISTRATORS_ROLE_ID, FRED_USER_NAME);
        assertEquals("You cannot edit the configuration of this project.", addErrorMessage);

        // remove actor from role will show error because that user does not have admin permission anymore
        String removeErrorMessage = rolesPage.removeUserFromRoleWithError(DEVELOPERS_ROLE_ID, FRED_USER_NAME);
        assertEquals("You cannot edit the configuration of this project.", removeErrorMessage);
    }

    @Test
    public void testNonExistentProjectLeadShownInRed() {
        final RolesPage rolesPage = jira.goTo(RolesPage.class, HSP_KEY);

        new ProjectLeadInfo()
                .name(NON_EXISTENT_USERNAME)
                .userHoverDisabled()
                .nonExistent()
                .verifyOn(rolesPage);

        new DefaultAssigneeInfo()
                .name(PROJECT_LEAD)
                .notAssignable()
                .verifyOn(rolesPage);
    }

    @Test
    public void testNonAssignableProjectLeadShownInRed() {
        final RolesPage rolesPage = jira.goTo(RolesPage.class, XSS_KEY);

        new ProjectLeadInfo()
                .name(ADMIN_FULLNAME)
                .verifyOn(rolesPage);

        new DefaultAssigneeInfo()
                .name(PROJECT_LEAD)
                .notAssignable()
                .verifyOn(rolesPage);
    }

    @Test
    public void testProjectWithIssuesThatCanBeUnassignedDisplaysCorrectDefaultAssignee() {
        final RolesPage rolesPage = jira.goTo(RolesPage.class, BLUK_KEY);
        final String anonymousAvatarUrl = jira.getProductInstance().getBaseUrl() + "/secure/useravatar?size=xsmall&avatarId=10063";

        new ProjectLeadInfo()
                .name(ADMIN_FULLNAME)
                .verifyOn(rolesPage);

        new DefaultAssigneeInfo()
                .name(UNASSIGNED_ASSIGNEE)
                .avatar(anonymousAvatarUrl)
                .verifyOn(rolesPage);
    }

    @Test
    public void testProjectWithDeletedUserThatCanStillBeAssigned() {
        backdoor.restoreDataFromResource("xml/projectconfig/TestProjectConfigSummaryPeoplePanelWithDeletedButAssignableUser.xml");
        final RolesPage rolesPage = jira.goTo(RolesPage.class, TST_KEY);
        final String anonymousAvatarUrl = jira.getProductInstance().getBaseUrl() + "/secure/useravatar?size=xsmall&avatarId=10093";

        new ProjectLeadInfo()
                .name(DELETED_USERNAME)
                .nonExistent()
                .userHoverDisabled()
                .verifyOn(rolesPage);

        new DefaultAssigneeInfo()
                .name(UNASSIGNED_ASSIGNEE)
                .avatar(anonymousAvatarUrl)
                .verifyOn(rolesPage);
    }

    @Test
    public void testChangeProjectLeadWithSuggestionsWorks() {
        RolesPage rolesPage = jira.goTo(RolesPage.class, BLUK_KEY);

        new ProjectLeadInfo()
                .name(ADMIN_FULLNAME)
                .verifyOn(rolesPage);

        rolesPage.setProjectLead(FRED_USER_NAME);

        new ProjectLeadInfo()
                .name(FRED_FULLNAME)
                .verifyOn(rolesPage);
    }

    @Test
    public void testChangeProjectLeadWithErrors() {
        final RolesPage rolesPage = jira.goTo(RolesPage.class, BLUK_KEY);

        new ProjectLeadInfo()
                .name(ADMIN_FULLNAME)
                .verifyOn(rolesPage);

        EditProjectLeadAndDefaultAssigneeDialog editDefaultsDialog = rolesPage.openEditDefaultsDialog();

        editDefaultsDialog.getLeadSelect().clear().type("doaks");
        assertFalse(editDefaultsDialog.submitUpdate());
        assertEquals("You must specify a valid project lead.", editDefaultsDialog.getFormErrors().get(EDIT_DEFAULTS_LEAD_FIELD));

        editDefaultsDialog.getLeadSelect().select("");
        assertFalse(editDefaultsDialog.submitUpdate());
        assertEquals("You must specify a valid project lead.", editDefaultsDialog.getFormErrors().get(EDIT_DEFAULTS_LEAD_FIELD));

        editDefaultsDialog.cancel();
    }

    @Test
    public void testProjectLeadPickerDisabled() {
        backdoor.restoreDataFromResource("xml/projectconfig/NoBrowseUsersPermission.xml");
        backdoor.flags().clearFlags();
        final RolesPage rolesPage = jira.goTo(RolesPage.class, "HSP");

        final EditProjectLeadAndDefaultAssigneeDialog editDefaultsDialog = rolesPage.openEditDefaultsDialog();
        assertTrue("Expected lead picker to be disabled", editDefaultsDialog.isLeadpickerDisabled());
        editDefaultsDialog.cancel();

        rolesPage.setProjectLead(FRED_USER_NAME);

        new ProjectLeadInfo()
                .name(FRED_FULLNAME)
                .verifyOn(rolesPage);
    }

    @Test
    public void testChangeDefaultAssignee() {
        RolesPage rolesPage = jira.goTo(RolesPage.class, BLUK_KEY);
        final String anonymousAvatarUrl = jira.getProductInstance().getBaseUrl() + "/secure/useravatar?size=xsmall&avatarId=10063";

        new DefaultAssigneeInfo()
                .name(UNASSIGNED_ASSIGNEE)
                .avatar(anonymousAvatarUrl)
                .verifyOn(rolesPage);

        rolesPage.setDefaultAssignee(PROJECT_LEAD);

        new DefaultAssigneeInfo()
                .name(PROJECT_LEAD)
                .verifyOn(rolesPage);
    }

    @Test
    public void testRolesFilter() {
        RolesPage rolesPage = jira.goTo(RolesPage.class, MKY_KEY);

        // filter by query "fred", only Fred user shows up under Developers role
        rolesPage.filterByQuery("fred");

        RoleInfo.administrators().verifyOn(rolesPage);
        RoleInfo.developers()
                .user(FRED_FULLNAME)
                .verifyOn(rolesPage);
        RoleInfo.users().verifyOn(rolesPage);

        // keep the query, filter by Users role, nothing shows up
        rolesPage.filterByRole(USERS_ROLE_NAME);

        RoleInfo.administrators().verifyOn(rolesPage);
        RoleInfo.developers().verifyOn(rolesPage);
        RoleInfo.users().verifyOn(rolesPage);

        // keep the query filter by Developers role, only Fred user shows up
        rolesPage.filterByRole(DEVELOPERS_ROLE_NAME);

        RoleInfo.administrators().verifyOn(rolesPage);
        RoleInfo.developers()
                .user(FRED_FULLNAME)
                .verifyOn(rolesPage);
        RoleInfo.users().verifyOn(rolesPage);

        // keep the query, clear role filter
        rolesPage.clearFilterByRole();

        // add Fred user and Developer group to Users role,
        // only Fred user shows up under Developers and Users role
        rolesPage.addActorsToRole(USERS_ROLE_ID, DEVELOPER_GROUP, FRED_USER_NAME);

        RoleInfo.administrators().verifyOn(rolesPage);
        RoleInfo.developers()
                .user(FRED_FULLNAME)
                .verifyOn(rolesPage);
        RoleInfo.users()
                .user(FRED_FULLNAME)
                .verifyOn(rolesPage);

        // filter by query "blah", nothing shows up
        rolesPage.filterByQuery("blah");

        RoleInfo.administrators().verifyOn(rolesPage);
        RoleInfo.developers().verifyOn(rolesPage);
        RoleInfo.users().verifyOn(rolesPage);

        // clear the query, everything shows up
        rolesPage.clearFilterByQuery();

        RoleInfo.administrators()
                .group(ADMIN_GROUP)
                .user(ADMIN_FULLNAME)
                .verifyOn(rolesPage);

        RoleInfo.developers()
                .group(DEVELOPER_GROUP)
                .user(FRED_FULLNAME)
                .verifyOn(rolesPage);

        RoleInfo.users()
                .group(DEVELOPER_GROUP)
                .group(USER_GROUP)
                .user(ADMIN_FULLNAME)
                .user(FRED_FULLNAME)
                .verifyOn(rolesPage);
    }

    @Test
    public void testEmailVisibilitySetting() {
        RolesPage rolesPage = jira.goTo(RolesPage.class, MKY_KEY);

        assertTrue(rolesPage.isEmailAddressColumnPresent());

        backdoor.applicationProperties().setString(APKeys.JIRA_OPTION_EMAIL_VISIBLE, "hide");
        jira.getTester().getDriver().navigate().refresh();

        assertFalse(rolesPage.isEmailAddressColumnPresent());
    }

    @Test
    public void testPaginationNavigator() {
        setupMultipleUsersToARole("user", "user", 80, MKY_KEY, ADMINISTRATORS_ROLE_NAME);
        RolesPage rolesPage = jira.goTo(RolesPage.class, MKY_KEY);

        rolesPage.showAllActors(ADMINISTRATORS_ROLE_ID);
        assertArrayEquals("There should be only role being viewed", new String[]{ADMINISTRATORS_ROLE_NAME.toUpperCase()}, rolesPage.getVisibleRoleNames().toArray());

        rolesPage.goToPage("2");
        assertEquals("Should show 20 actors per page", 20, rolesPage.getActorNames(ADMINISTRATORS_ROLE_ID, "js-role-user-name").size());

        rolesPage.goToPage("Last");
        assertEquals("The last page should be selected", "5", rolesPage.getPagingNavigator().find(By.className("aui-nav-selected")).getAttribute("data-page-number").trim());
        assertEquals("Should show the rest of actors in last page", 2, rolesPage.getActorNames(ADMINISTRATORS_ROLE_ID, "js-role-user-name").size());

        rolesPage.removeUserFromRole(ADMINISTRATORS_ROLE_ID, "user9");
        assertEquals("Removing actors should keep the current page number", "5", rolesPage.getPagingNavigator().find(By.className("aui-nav-selected")).getAttribute("data-page-number").trim());

        rolesPage.removeUserFromRole(ADMINISTRATORS_ROLE_ID, "user8");
        assertEquals("Should go back one page, after removing the last visible actor of the current page", "4", rolesPage.getPagingNavigator().find(By.className("aui-nav-selected")).getAttribute("data-page-number").trim());
        assertEquals("Should show 20 actors per page", 20, rolesPage.getActorNames(ADMINISTRATORS_ROLE_ID, "js-role-user-name").size());

        rolesPage.addActorsToRole(ADMINISTRATORS_ROLE_ID, USER_GROUP);
        assertEquals("Adding actors should keep the current page number", "4", rolesPage.getPagingNavigator().find(By.className("aui-nav-selected")).getAttribute("data-page-number").trim());

        rolesPage.showLess();
        assertArrayEquals("There should be all roles being viewed", new String[]{ADMINISTRATORS_ROLE_NAME.toUpperCase(), DEVELOPERS_ROLE_NAME.toUpperCase(), USERS_ROLE_NAME.toUpperCase()}, rolesPage.getVisibleRoleNames().toArray());
    }

    @Test
    public void testProjectWithInvalidProjectLeadDisplaysWarningMessage() {
        RolesPage rolesPage = jira.goTo(RolesPage.class, "HSP");
        assertThat("Should display warning message about Project Lead not being valid", rolesPage.isInvalidProjectLeadWarningDialogVisible(), is(true));

        rolesPage = jira.goTo(RolesPage.class, "MKY");
        assertThat("Should not display warning message when Project Lead is valid", rolesPage.isInvalidProjectLeadWarningDialogVisible(), is(false));
    }

    @Test
    public void testProjectLeadChangeWithTooLongKey() {
        final Long projectId = getBackdoor().project().getProjectId(BLUK_KEY);
        getBackdoor().project().editProjectKeyNoWaitForReindex(projectId, LARGE_PROJECT_KEY);

        final RolesPage page = jira.goTo(RolesPage.class, LARGE_PROJECT_KEY);
        assertThat(page.getProjectLead(), is(ADMIN_FULLNAME));
        page.setProjectLead(FRED_USER_NAME);

        assertThat("Changing the lead should still possible even if the project's key is too long",
                page.getProjectLead(), is(FRED_FULLNAME));
    }

    private void setupMultipleUsersToARole(String userNamePrefix, String displayNamePrefix, int numberOfUsers, String projectKey, String roleName) {
        backdoor.usersAndGroups().addUsers(userNamePrefix, displayNamePrefix, numberOfUsers);

        final String[] userNames = IntStream.range(0, numberOfUsers)
                .mapToObj(i -> userNamePrefix + i)
                .toArray(String[]::new);

        backdoor.projectRole().addActors(projectKey, roleName, null, userNames);
    }

    class ProjectLeadInfo {
        String name;
        Pattern avatar;
        boolean nonExistent;
        boolean userHoverDisabled;

        ProjectLeadInfo name(String name) {
            this.name = name;
            return this;
        }

        ProjectLeadInfo avatar(Pattern avatar) {
            this.avatar = avatar;
            return this;
        }

        ProjectLeadInfo nonExistent() {
            this.nonExistent = true;
            return this;
        }

        ProjectLeadInfo userHoverDisabled() {
            this.userHoverDisabled = true;
            return this;
        }

        void verifyOn(RolesPage rolesPage) {
            if (name != null) {
                assertEquals(name, rolesPage.getProjectLead());
            }

            if (avatar != null) {
                assertTrue(rolesPage.isProjectLeadAvatarPresent());
                assertThat(rolesPage.getProjectLeadAvatarSrc(), RegexMatchers.regexMatchesPattern(avatar));
            }

            assertEquals(nonExistent, rolesPage.isProjectLeadNonExistentIndicated());
            assertEquals(userHoverDisabled, !rolesPage.isProjectLeadUserHoverEnabled());
        }
    }

    class DefaultAssigneeInfo {
        String name;
        String avatar;
        boolean notAssignable;

        DefaultAssigneeInfo name(String name) {
            this.name = name;
            return this;
        }

        DefaultAssigneeInfo avatar(String avatar) {
            this.avatar = avatar;
            return this;
        }

        DefaultAssigneeInfo notAssignable() {
            this.notAssignable = true;
            return this;
        }

        void verifyOn(RolesPage rolesPage) {
            if (name != null) {
                assertEquals(name, rolesPage.getDefaultAssignee());
            }

            if (avatar == null) {
                assertFalse(rolesPage.isDefaultAssigneeAvatarPresent());
            } else {
                assertTrue(rolesPage.isDefaultAssigneeAvatarPresent());
                assertEquals(avatar, rolesPage.getDefaultAssigneeAvatarSrc());
            }

            assertEquals(notAssignable, rolesPage.isDefaultAssigneeNotAssignableIndicated());
        }
    }

    static class RoleInfo {
        long roleId;
        String roleName;
        List<String> groups;
        List<String> users;

        private RoleInfo(long roleId, String roleName) {
            this.roleId = roleId;
            this.roleName = roleName;
            this.groups = Lists.newArrayList();
            this.users = Lists.newArrayList();
        }

        RoleInfo group(String group) {
            this.groups.add(group);
            return this;
        }

        RoleInfo user(String user) {
            this.users.add(user);
            return this;
        }

        void verifyOn(RolesPage rolesPage) {
            if (groups.isEmpty() && users.isEmpty()) {
                assertFalse(rolesPage.isRoleTablePresent(roleId));
            } else {
                assertTrue(rolesPage.isRoleTablePresent(roleId));

                assertEquals(groups, rolesPage.getGroupNamesForRole(roleId));
                assertEquals(users, rolesPage.getUserNamesForRole(roleId));
            }
            assertFalse("dialog to add users should have been dismissed", rolesPage.isAddUsersToRoleDialogVisible());
        }

        static RoleInfo administrators() {
            return new RoleInfo(ADMINISTRATORS_ROLE_ID, ADMINISTRATORS_ROLE_NAME);
        }

        static RoleInfo developers() {
            return new RoleInfo(DEVELOPERS_ROLE_ID, DEVELOPERS_ROLE_NAME);
        }

        static RoleInfo users() {
            return new RoleInfo(USERS_ROLE_ID, USERS_ROLE_NAME);
        }
    }
}
