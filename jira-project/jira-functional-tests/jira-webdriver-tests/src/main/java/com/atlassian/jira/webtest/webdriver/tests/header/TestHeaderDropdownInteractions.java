package com.atlassian.jira.webtest.webdriver.tests.header;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

/**
 * Checking various kinds of dropdown items in the header for their ability
 * to be interacted with in a variety of ways.
 *
 * !!! IMPORTANT NOTICE!!!
 * These tests deliberately do not use page objects. There is a reason for this.
 *
 * Page objects exist to separate the concerns of the goal of the test
 * and how to achieve that goal. For example, most tests won't care about how
 * a dropdown is opened; they just want it to open so they can do stuff with its
 * contents.
 *
 * In these tests, the thing we're trying to test is precisely that means of
 * achieving the goal. We want to check what user inputs result in what outputs.
 *
 * @since v7.2
 */
@WebTest({Category.WEBDRIVER_TEST})
@RestoreOnce(value = "header-one-project.xml")
@LoginAs(user = "bob", password = "bob")
public class TestHeaderDropdownInteractions extends BaseJiraWebTest {
    @Inject
    private PageElementFinder elementFinder;

    private JiraHeader header;

    @Before
    public void setUp() {
        header = jira.gotoHomePage().getHeader();
    }

    /**
     * Check we can navigate the dropdown a few ways.
     * @see <a href="https://bulldog.internal.atlassian.com/browse/JSEV-608">JSEV-608</a>
     */
    @Test
    public void testKeyboardNavigationWithSynchronousDropdown() {
        final WebElement trigger = wd().findElement(By.id("help_menu"));
        final WebElement firstDropdownItem = wd().findElement(By.id("view_core_help"));

        // open the dropdown with enter
        setFocusTo(trigger).sendKeys(Keys.ENTER);
        assertThat("Should focus first item of dropdown",
                documentActiveElement(), equalTo(firstDropdownItem));

        // close the dropdown
        documentActiveElement().sendKeys(Keys.ESCAPE);
        assertThat("Trigger should be focussed after dropdown closes",
                documentActiveElement(), equalTo(trigger));

        // open it again, with the down arrow this time
        documentActiveElement().sendKeys(Keys.ARROW_DOWN);
        assertThat("Should focus first item of dropdown",
                documentActiveElement(), equalTo(firstDropdownItem));

        // left and right arrow keys shouldn't affect anything
        documentActiveElement().sendKeys(Keys.ARROW_RIGHT);
        assertThat("Should do nothing to focus",
                documentActiveElement(), equalTo(firstDropdownItem));
        documentActiveElement().sendKeys(Keys.ARROW_LEFT);
        assertThat("Should do nothing to focus",
                documentActiveElement(), equalTo(firstDropdownItem));

        // should have a focus trap at top of dropdown
        documentActiveElement().sendKeys(Keys.ARROW_DOWN, Keys.ARROW_DOWN);
        assertThat(documentActiveElement(), not(equalTo(firstDropdownItem)));
        documentActiveElement().sendKeys(Keys.ARROW_UP, Keys.ARROW_UP, Keys.ARROW_UP);
        assertThat(documentActiveElement(), equalTo(firstDropdownItem));
    }

    @Test
    public void testKeyboardTriggeringOfAsyncDropdown() {
        final WebElement trigger = wd().findElement(By.id("home_link"));
        final HeaderDropdown dropdown = new HeaderDropdown("home_link-content");

        // open the dropdown with enter
        setFocusTo(trigger).sendKeys(Keys.ENTER);
        waitUntilTrue(dropdown.isOpen());
        assertThat("Should focus first item of dropdown",
                documentActiveElement().getText(), equalTo("Blank dashboard"));

        // close the dropdown
        documentActiveElement().sendKeys(Keys.ESCAPE);
        waitUntilTrue(dropdown.isClosed());
        assertThat("Trigger should be focussed after dropdown closes",
                documentActiveElement(), equalTo(trigger));

        // open it again, with the down arrow this time
        documentActiveElement().sendKeys(Keys.ARROW_DOWN);
        waitUntilTrue(dropdown.isOpen());
        assertThat("Should focus first item of dropdown",
                documentActiveElement().getText(), equalTo("Blank dashboard"));
        documentActiveElement().sendKeys(Keys.ARROW_DOWN);
        assertThat("Should focus second item of dropdown",
                documentActiveElement().getText(), equalTo("Manage Dashboards"));
    }

    /**
     * Check we can activate menu items via keyboard.
     * @see <a href="https://bulldog.internal.atlassian.com/browse/JSEV-609">JSEV-609</a>
     */
    @Test
    public void testKeyboardInteractionWithDropdownItem() {
        header.getUserProfileMenu().open();
        // Select an item that should cause a navigation
        final WebElement ddItem = wd().findElement(By.id("view_profile"));
        setFocusTo(ddItem).sendKeys(Keys.ENTER);
        assertThat("dropdown item should have been interacted with",
                wd().getTitle(), containsString("User Profile: Bob"));
    }

    /**
     * Check we can activate menu items via mouse.
     * @see <a href="https://bulldog.internal.atlassian.com/browse/JSEV-609">JSEV-609</a>
     */
    @Test
    public void testMouseInteractionWithDropdownItem() {
        header.getUserProfileMenu().open();
        // Select an item that should cause a navigation
        final WebElement ddItem = wd().findElement(By.id("view_profile"));
        ddItem.click();
        assertThat("dropdown item should have been interacted with",
                wd().getTitle(), containsString("User Profile: Bob"));
    }

    private WebDriver wd() {
        return jira.getTester().getDriver().getDriver();
    }

    private WebElement setFocusTo(WebElement element) {
        new Actions(wd()).moveToElement(element).perform();
        return element;
    }

    private WebElement documentActiveElement() {
        return wd().switchTo().activeElement();
    }

    /**
     * The current implementation of JIRA's header dropdowns.
     * If this changes to use the AUI remote dropdowns one day, these classes
     * will change.
     *
     * This particular page object could be consolidated with the
     * {@link com.atlassian.pageobjects.components.aui.AuiDropDownMenu} and
     * {@link com.atlassian.jira.pageobjects.components.menu.JiraAuiDropdownMenu}
     * page objects.
     */
    private class HeaderDropdown {
        private final String id;

        HeaderDropdown(String id) {
            this.id = id;
        }

        TimedCondition isOpen() {
            return Conditions.and(
                    dd().timed().isPresent(),
                    dd().timed().hasAttribute("aria-hidden", "false"),
                    Conditions.not(dd().timed().hasClass("aui-dropdown2-loading")),
                    dd().timed().isVisible()
            );
        }

        TimedCondition isClosed() {
            return Conditions.and(
                    dd().timed().isPresent(),
                    dd().timed().hasAttribute("aria-hidden", "true"),
                    Conditions.not(dd().timed().isVisible())
            );
        }

        private PageElement dd() {
            return elementFinder.find(By.id(id));
        }
    }
}
