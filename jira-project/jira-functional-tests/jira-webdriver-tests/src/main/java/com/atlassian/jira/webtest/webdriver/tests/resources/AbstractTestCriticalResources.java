package com.atlassian.jira.webtest.webdriver.tests.resources;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.framework.util.JiraLocators;
import com.atlassian.jira.pageobjects.util.CriticalResources;
import com.atlassian.jira.pageobjects.util.CriticalResources.ResourceType;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.text.StrBuilder;
import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import static org.apache.commons.collections.ListUtils.intersection;
import static org.apache.commons.collections.ListUtils.subtract;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Abstract test class for detecting whether resources loaded during page load are in fact used. This class helps to
 * prevent adding unnecessary stylesheets and javascript files to HEAD section of document.
 *
 * On test failure developer can either fix it by deferring resource loading or adding to appropriate exception list file.
 *
 * @since 7.0
 */
public abstract class AbstractTestCriticalResources extends BaseJiraWebTest {
    private static final Logger log = LoggerFactory.getLogger(AbstractTestCriticalResources.class);

    @Inject
    private CriticalResources criticalResources;

    @Inject
    private PageElementFinder elementFinder;

    @Inject
    private WebDriver webDriver;

    protected static void setProperty(final String key) {
        backdoor.systemProperties().setProperty(key, "true");
    }

    protected static void unsetProperty(final String key) {
        backdoor.systemProperties().unsetProperty(key);
    }

    protected void visitPage(Runnable visitor) {
        visitor.run();
        // refresh page to enforce resources request without cache
        jira.getTester().getDriver().navigate().refresh();
    }

    protected void verifyResources(String exceptionsResource, ResourceType resourceType) throws IOException {
        final List<ResourceException> exceptions = loadExceptions(exceptionsResource);
        final List<String> exceptionNames = exceptions.stream()
                .map(ResourceException::getResourceName)
                .collect(CollectorsUtil.toImmutableList());
        final List<String> variableExceptionNames = exceptions.stream()
                .filter(ResourceException::isVariable)
                .map(ResourceException::getResourceName)
                .collect(CollectorsUtil.toImmutableList());

        final List<String> activeResources = criticalResources.getActive(resourceType);
        final List<String> criticalResources = this.criticalResources.getCritical(resourceType);
        final List<String> unusedResources = subtract(subtract(activeResources, criticalResources), exceptionNames);
        final List<String> redundantResources = (List<String>) intersection(subtract(exceptionNames, variableExceptionNames), criticalResources);

        log.info("Resource usage summary:");
        log.info(String.format("Active: %d, Unused: %d, New: %d, Critical: %d", activeResources.size(), redundantResources.size(), unusedResources.size(), criticalResources.size()));
        log.info(String.format("Number of JavaScript errors: %d", errorList().size()));

        assertThat("Active resources detected", activeResources, is(not(empty())));

        if (!redundantResources.isEmpty() || !unusedResources.isEmpty()) {
            StrBuilder builder = new StrBuilder();

            if (!redundantResources.isEmpty()) {
                builder.appendNewLine()
                        .appendln("Some exceptions are not necessary, those resources are in fact used during page load:").appendNewLine();
                redundantResources.forEach(builder::appendln);
                builder.appendNewLine()
                        .appendln(String.format("Review those exceptions: %s", exceptionsResource));
                builder.appendln("1. Remove if you are certain that resource will be always used during page load.");
                builder.appendln("2. Mark as variable if their usage depends on product configuration.").appendNewLine();
            }

            if (!unusedResources.isEmpty()) {
                builder.appendln("Some resources are loaded, but not used during page load.").appendNewLine();
                unusedResources.forEach(builder::appendln);
                builder.appendNewLine()
                        .appendln("Review usage of those resources:");
                builder.appendln("1. Load them asynchronously(WRM.require).");
                builder.appendln(String.format("2. Add to exceptions file: %s", exceptionsResource)).appendNewLine();
            }

            fail(builder.toString());
        }
    }

    protected void assertNoErrors() {
        Poller.waitUntil("There were JS errors on " + webDriver.getCurrentUrl(),
                errorListQuery(), Matchers.<Map<String, String>>emptyIterable());
    }

    @SuppressWarnings("unchecked")
    private TimedQuery<Iterable<Map<String, String>>> errorListQuery() {
        return (TimedQuery) elementFinder.find(JiraLocators.body()).javascript().executeTimed(List.class,
                "return window.JIRA.DevMode.Errors;");
    }

    private List<Map<String, String>> errorList() {
        return elementFinder.find(JiraLocators.body()).javascript().execute(List.class, "return window.JIRA.DevMode.Errors;");
    }

    protected static List<ResourceException> loadExceptions(String resource) throws IOException {
        final InputStream input = TestCriticalCSS.class.getResourceAsStream(resource);
        final List<String> resources = IOUtils.readLines(input);
        IOUtils.closeQuietly(input);
        return resources.stream()
                .filter(line -> !line.startsWith("#"))
                .map(ResourceException::new)
                .collect(CollectorsUtil.toImmutableList());
    }

    protected static class ResourceException {
        /**
         * Format for exception lists: "RESOURCE_NAME   VARIABLE"
         */
        private final static String EXCEPTION_PATTERN = "([\\s|\\t]+)";

        private final String resourceName;
        private final boolean variable;

        public ResourceException(String line) {
            final String[] parts = line.split(EXCEPTION_PATTERN);

            this.resourceName = parts[0];
            this.variable = parts.length > 1 && Boolean.parseBoolean(parts[1]);
        }

        /**
         * Returns resource name, which is full resource key
         *
         * @return resource name, which is full resource key
         */
        public String getResourceName() {
            return resourceName;
        }

        /**
         * Returns if this exception depends on instance configuration.
         *
         * @return true if this exception depends on instance configuration.
         */
        public boolean isVariable() {
            return variable;
        }
    }
}
