package com.atlassian.jira.webtest.webdriver.tests.visualregression;

import com.atlassian.jira.functest.framework.backdoor.OutgoingMailControl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.rule.MailTest;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.IssueUpdateRequest;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.jira.webtests.util.LocalTestEnvironmentData;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Arrays;

import static com.atlassian.jira.rest.api.issue.ResourceRef.withId;
import static com.atlassian.jira.rest.api.issue.ResourceRef.withName;

/**
 * @since v7.1
 */

@MailTest
@WebTest({Category.WEBDRIVER_TEST, Category.VISUAL_REGRESSION})
public class TestVisualRegressionEmails extends JiraVisualRegressionTest {
    private static IssueClient issueClient;
    private static OutgoingMailControl mailControl;

    @BeforeClass
    public static void restoreInstance() {
        issueClient = new IssueClient(LocalTestEnvironmentData.DEFAULT);
        mailControl = backdoor.outgoingMailControl();
        //we cannot restore instance via @Restore annotation as it doesn't support restoring license from backup file
        backdoor.dataImport().restoreDataFromResource("xml/TestVisualRegressionEmails.zip", "");
    }

    @Test
    public void testIssueCreatedEmail() throws MessagingException {
        issueClient.create(new IssueUpdateRequest().fields(prepareIssueFields("create")));

        goToEmailPreview();
        ignoreElements();
        assertUIMatches("issue-created-email");
    }

    @Test
    public void testIssueUpdatedEmail() throws MessagingException {
        final IssueFields update = prepareIssueFields("update", 1);
        update.description("h1. header\n" +
                "This _is a *test* comment_ with\n" +
                "# wiki\n" +
                "# markup :(\n" +
                "If you know\n" +
                "* what\n" +
                "* I mean (on)\n" +
                "{color:green}It is tested{color}");
        issueClient.update(prepareIssue(), new IssueUpdateRequest().fields(update));

        goToEmailPreview();
        ignoreElements();
        assertUIMatches("issue-updated-email");
    }

    @Test
    public void testIssueCommentedEmail() throws MessagingException {
        backdoor.issues().commentIssueWithVisibility(
                prepareIssue(),
                "h1. header\nThis _is_ a *test* comment with\n* wiki\n* markup :)\nIf you know\n# what\n# I mean (on)\n{color:red}Testing it{color}",
                "role",
                "Users"
        );

        goToEmailPreview();
        ignoreElements();
        assertUIMatches("issue-commented-email");
    }

    @Test
    public void testMentionEmail() throws MessagingException {
        backdoor.issues().commentIssueWithVisibility(
                prepareIssue(),
                "Hey [~bob], just mentioning you so you get the *e-mail*.",
                "role",
                "Users"
        );

        final MimeMessage mimeMessage = mailHelper.getMessageForAddress(mailHelper.flushMailQueueAndWait(2), "bob@example.com");
        goTo(mailControl.getMessagePreviewURI(mimeMessage));

        ignoreElements();
        assertUIMatches("mention-email");
    }

    @Test
    public void testIssueSubscribtionEmail() throws MessagingException {
        goTo("/secure/ViewSubscriptions.jspa?filterId=10100");
        clickOnElement(".operations-list li:last-child a"); // run subscription link

        goToEmailPreview();
        ignoreElements();
        assertUIMatches("issue-subscription-email");
    }

    @Test
    public void testCreateUserEmail() throws MessagingException {
        backdoor.usersAndGroups().addUser("test", "test", "Testing adding users", "test@te.st", true);

        goToEmailPreview();
        ignoreElements();
        assertUIMatches("create-user-email");
    }

    private String prepareIssue() {
        IssueCreateResponse createdIssue = issueClient.create(new IssueUpdateRequest().fields(prepareIssueFields("create")));
        mailHelper.flushMailQueueAndWait(1);
        mailControl.clearMessages();
        return createdIssue.key;
    }

    private IssueFields prepareIssueFields(String suffix) {
        return prepareIssueFields(suffix, 0);
    }

    private IssueFields prepareIssueFields(String suffix, int idSuffix) {
        final String TEST_PROJECT_ID = "10000";
        final String TASK_ISSUE_TYPE_ID = "10000";
        final int BASE_JIRA_ID = 10000;

        final IssueFields fields = new IssueFields();

        String defaultId = Integer.toString(BASE_JIRA_ID + idSuffix);

        fields.project(withId(TEST_PROJECT_ID));
        fields.issueType(withId(TASK_ISSUE_TYPE_ID));
        fields.summary("My issue " + suffix);
        fields.priority(withId(Integer.toString(1 + idSuffix)));
        fields.dueDate("2015-09-0" + Integer.toString(1 + idSuffix));
        fields.versions(withId(defaultId), withId(Integer.toString(10001 + idSuffix)));
        fields.assignee(withName("admin"));
        fields.reporter(withName("admin"));
        fields.environment("environment " + suffix);
        fields.labels(Arrays.asList(suffix + "1", suffix + "2", suffix + "3"));
        fields.description("h1. header\n" +
                "This _is_ a *test* comment with\n" +
                "* wiki\n" +
                "* markup :)\n" +
                "If you know\n" +
                "# what\n" +
                "# I mean (on)\n\n" +
                "{color:red}Testing it{color}" + suffix);
        fields.fixVersions(withId(defaultId));
        fields.components(withId(defaultId));

        return fields;
    }

    private void goToEmailPreview() throws MessagingException {
        final MimeMessage mimeMessage = mailHelper.flushMailQueueAndWait(1).stream().findFirst().get();
        goTo(mailControl.getMessagePreviewURI(mimeMessage));
    }

    private void ignoreElements() {
        addElementsToIgnore(By.xpath("//*[contains(text(),'Created:')]/following-sibling::td"));
        addElementsToIgnore(By.cssSelector("#header-text-container a:last-child"));
        addElementsToIgnore(By.cssSelector(".page-title-pattern-first-line a:last-child"));
        addElementsToIgnore(By.cssSelector(".text-paragraph-pattern-container a"));
    }
}
