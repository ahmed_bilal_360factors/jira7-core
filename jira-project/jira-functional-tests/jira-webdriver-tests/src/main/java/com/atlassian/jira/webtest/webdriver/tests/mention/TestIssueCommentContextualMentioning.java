package com.atlassian.jira.webtest.webdriver.tests.mention;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.ResetDataOnce;
import com.atlassian.jira.pageobjects.pages.viewissue.AddCommentSection;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Keys;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

@ResetDataOnce
@WebTest({Category.WEBDRIVER_TEST, Category.ISSUES, Category.COMMENTS})
public class TestIssueCommentContextualMentioning extends BaseJiraWebTest {

    private static final long PROJECT_ID = 10001L;

    @BeforeClass
    public static void setUp() {
        backdoor.usersAndGroups().addUserEvenIfUserExists("ntran", "123456", "Nhat Tran", "ntran@example.com");
        backdoor.usersAndGroups().addUserToGroup("ntran", "jira-developers");
    }

    @Test
    public void testSuggestionForMentioningAssignee() {
        final IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_ID, "Issue with a comment which has contextual mention", "ntran");
        final AddCommentSection commentSection = jira.goToViewIssue(issue.key()).comment();
        commentSection.typeInput("@a");
        Poller.waitUntilTrue(commentSection.getMentions().hasActiveSuggestion("ntran"));
        Poller.waitUntilTrue(commentSection.getMentions().hasSuggestion("admin"));

        commentSection.typeInput("s");
        Poller.waitUntilTrue(commentSection.getMentions().hasActiveSuggestion("ntran"));
        Poller.waitUntilFalse(commentSection.getMentions().hasSuggestion("admin"));
    }

    @Test
    public void testSuggestionForMentioningReporter() {
        final IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_ID, "Issue with a comment which has contextual mention", "ntran");
        final AddCommentSection commentSection = jira.goToViewIssue(issue.key()).comment();
        commentSection.typeInput("@reporter");

        Poller.waitUntilTrue(commentSection.getMentions().hasActiveSuggestion("admin"));
        Poller.waitUntilTrue(commentSection.getMentions().hasSuggestion("admin"));
    }

    @Test
    public void testAssigneeMentionHintShouldBeShownInSuggestionWhichHasAssigneeRole() {
        final IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_ID, "Issue with a comment which has contextual mention", "ntran");
        final AddCommentSection commentSection = jira.goToViewIssue(issue.key()).comment();
        commentSection.typeInput("@a");
        Poller.waitUntilTrue(commentSection.getMentions().hasActiveSuggestion("ntran"));
        Poller.waitUntilTrue(commentSection.getMentions().hasSuggestion("admin"));

        final Iterable<String> actualIssueRoles = commentSection.getMentions().getSuggestion("ntran").getIssueRolesAsList();
        assertThat(actualIssueRoles, contains("assignee"));
    }

    @Test
    public void testContextualMentionHintShouldBeShownInSuggestion_EvenIfUsersDoNotIntentionallyMentionAssignee() {
        final IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_ID, "Issue with a comment which has contextual mention", "ntran");
        final AddCommentSection commentSection = jira.goToViewIssue(issue.key()).comment();
        commentSection.typeInput("@n");
        Poller.waitUntilTrue(commentSection.getMentions().hasActiveSuggestion("ntran"));

        final Iterable<String> actualIssueRoles = commentSection.getMentions().getSuggestion("ntran").getIssueRolesAsList();
        assertThat(actualIssueRoles, contains("assignee"));
    }

    @Test
    public void testBothContextualMentionsHintShouldBeShownInSuggestionWhichHasBothRoles() {
        final IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_ID, "Issue with a comment which has contextual mention", "admin");
        final AddCommentSection commentSection = jira.goToViewIssue(issue.key()).comment();
        commentSection.typeInput("@ad");
        Poller.waitUntilTrue(commentSection.getMentions().hasActiveSuggestion("admin"));

        final Iterable<String> actualIssueRoles = commentSection.getMentions().getSuggestion("admin").getIssueRolesAsList();
        assertThat(actualIssueRoles, contains("assignee", "reporter", "watcher"));
    }

    @Test
    public void testCommentWithMentioningAssignee() {
        final IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_ID, "Issue with a comment which has contextual mention", "ntran");
        final AddCommentSection commentSection = jira.goToViewIssue(issue.key()).comment();

        commentSection.typeInput("@assignee");
        Poller.waitUntilTrue(commentSection.getMentions().hasActiveSuggestion("ntran"));
        commentSection.typeInput(Keys.TAB);
        Poller.waitUntilEquals("[~ntran]", commentSection.getCommentTimed());
    }

    @Test
    public void testCommentWithMentioningReporter() {
        final IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_ID, "Issue with a comment which has contextual mention", "ntran");
        final AddCommentSection commentSection = jira.goToViewIssue(issue.key()).comment();

        commentSection.typeInput("@reporter");
        Poller.waitUntilTrue(commentSection.getMentions().hasActiveSuggestion("admin"));
        commentSection.typeInput(Keys.TAB);
        Poller.waitUntilEquals("[~admin]", commentSection.getCommentTimed());
    }

    @Test
    public void testMultipleLineCommentWithMentioningAssignee() {
        final IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_ID, "Issue with a comment which has contextual mention", "ntran");
        final AddCommentSection commentSection = jira.goToViewIssue(issue.key()).comment();

        commentSection.typeInput("Hey", Keys.ENTER, "I'm mentioning you", Keys.ENTER, "@assignee");
        Poller.waitUntilTrue(commentSection.getMentions().hasActiveSuggestion("ntran"));
        commentSection.typeInput(Keys.TAB);
        Poller.waitUntilEquals("Hey\nI'm mentioning you\n[~ntran]", commentSection.getCommentTimed());
    }
}
