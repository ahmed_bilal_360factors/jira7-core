package com.atlassian.jira.webtest.webdriver.tests.project;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.project.BrowseProjectsPage;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static org.hamcrest.core.Is.is;

@WebTest({Category.WEBDRIVER_TEST, Category.PROJECTS})
public class TestProjectTypesOnBrowseProjectsPage extends BaseJiraWebTest {
    @Before
    public void setUp() {
        backdoor.restoreDataFromResource("TestProjectTypesOnBrowseProjectsPage.xml");
    }

    @Test
    public void projectTypeColumnIsDisplayedIfProjectTypesDarkFeatureIsEnabled() {
        assertProjectTypeColumnDisplays("Business", "Software");
    }

    private void assertProjectTypeColumnDisplays(String... projectTypes) {
        BrowseProjectsPage browseProjectsPage = jira.goTo(BrowseProjectsPage.class);

        for (int i = 0; i < projectTypes.length; i++) {
            waitUntil(browseProjectsPage.getProjectAt(i + 1).getProjectTypeName(), is(projectTypes[i]));
        }
    }
}
