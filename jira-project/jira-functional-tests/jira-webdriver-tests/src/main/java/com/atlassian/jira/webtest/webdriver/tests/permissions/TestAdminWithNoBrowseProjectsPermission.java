package com.atlassian.jira.webtest.webdriver.tests.permissions;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.project.BrowseProjectsErrorPage;
import com.atlassian.jira.pageobjects.pages.viewissue.MoveIssueErrorPage;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.hamcrest.Matchers.endsWith;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@WebTest(Category.WEBDRIVER_TEST)
public class TestAdminWithNoBrowseProjectsPermission extends BaseJiraWebTest {
    private static final String CALL_TO_ACTION_URL = "/secure/admin/ViewPermissionSchemes.jspa";

    @BeforeClass
    public static void setUp() {
        backdoor.restoreBlankInstance();

        final long defaultPermissionScheme = 0;
        backdoor.permissionSchemes().removeGroupPermission(defaultPermissionScheme, BROWSE_PROJECTS, "jira-users");

        backdoor.project().addProject("Test", "TEST", "admin");
        backdoor.issues().createIssue("TEST", "Test issue");
    }

    @Test
    public void errorDisplaysOnMoveIssuePage() {
        final MoveIssueErrorPage page = jira.goTo(MoveIssueErrorPage.class, "TEST-1");

        assertTrue(page.isErrorPanelPresent());
        assertThat(page.getErrorPanelLinkUrl(), endsWith(CALL_TO_ACTION_URL));
    }

    @Test
    public void errorDisplaysOnBrowseProjectPage() {
        final BrowseProjectsErrorPage page = jira.goTo(BrowseProjectsErrorPage.class);
        assertTrue(page.isErrorPanelPresent());
        assertThat(page.getErrorPanelLinkUrl(), endsWith(CALL_TO_ACTION_URL));
    }
}
