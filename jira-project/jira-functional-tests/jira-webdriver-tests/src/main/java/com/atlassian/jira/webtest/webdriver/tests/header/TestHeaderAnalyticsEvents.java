package com.atlassian.jira.webtest.webdriver.tests.header;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import javax.inject.Inject;

@WebTest({Category.WEBDRIVER_TEST})
@RestoreOnce(value = "header-one-project.xml")
@LoginAs(user = "bob", password = "bob")
public class TestHeaderAnalyticsEvents extends BaseJiraWebTest {

    @Inject
    private PageElementFinder elementFinder;

    @Inject
    TraceContext traceContext;

    private static final String JIRA_HEADER_ANALYTICS_TRACE = "jira.header.analytics.event";

    @Before
    public void setUp() {
        jira.visit(DashboardPage.class);
    }

    @Test(timeout = 3000)
    public void testDashboardsLink() {
        testHeaderMenuItem("home_link");
    }

    @Test(timeout = 3000)
    public void testProjectsLink() {
        testHeaderMenuItem("browse_link");
    }

    @Test(timeout = 3000)
    public void testIssuesLink() {
        testHeaderMenuItem("find_link");
    }

    private void testHeaderMenuItem(String elementId){
        Tracer tracer = traceContext.checkpoint();
        clickHeaderMenuDropdown(elementId);
        traceContext.waitFor(tracer,JIRA_HEADER_ANALYTICS_TRACE);
        clickHeaderMenuDropdown(elementId);
        traceContext.waitFor(tracer,JIRA_HEADER_ANALYTICS_TRACE);
    }

    private void clickHeaderMenuDropdown(String id) {
        PageElement element = elementFinder.find(By.id(id));
        Poller.waitUntilTrue(element.timed().isVisible());
        element.click();
    }

}

