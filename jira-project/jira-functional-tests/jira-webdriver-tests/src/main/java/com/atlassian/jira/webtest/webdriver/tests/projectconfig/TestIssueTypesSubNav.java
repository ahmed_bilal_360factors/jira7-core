package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.project.ProjectConfigTabs;
import com.atlassian.jira.pageobjects.project.issuetypes.IssueTypeFieldsTab;
import com.atlassian.jira.pageobjects.project.summary.ProjectSummaryPageTab;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS})
@RestoreOnce("xml/projectconfig/SummaryWorkflowPanel.xml")
public class TestIssueTypesSubNav extends BaseJiraWebTest {
    private static final String PROJECT_KEY = "MKY";
    private static final int BUG = 1;
    private static final int NEW_FEATURE = 2;

    @Test
    public void testIssueTypesTabNavigation() {
        final ProjectSummaryPageTab config = jira.quickLoginAsSysadmin(ProjectSummaryPageTab.class, PROJECT_KEY);
        final ProjectConfigTabs tabs = config.getTabs();
        tabs.gotoIssueTypesTab();
        assertThat(tabs.isIssueTypesTabSelected(), equalTo(true));
    }

    @Test
    public void testIssueTypeTabNavigation() {
        final ProjectSummaryPageTab config = jira.quickLoginAsSysadmin(ProjectSummaryPageTab.class, PROJECT_KEY);
        final ProjectConfigTabs tabs = config.getTabs();

        tabs.gotoIssueTypeWorkflowTab(BUG);
        assertThat(tabs.isIssueTypeTabSelected(BUG), equalTo(true));

        tabs.gotoIssueTypeWorkflowTab(NEW_FEATURE);
        assertThat(tabs.isIssueTypeTabSelected(NEW_FEATURE), equalTo(true));
    }

    @Test
    public void testNavigatingToAnIssueTypeThenToAnotherThenHittingTheBackButtonReturnsToTheFirst() {
        final ProjectSummaryPageTab config = jira.quickLoginAsSysadmin(ProjectSummaryPageTab.class, PROJECT_KEY);
        final ProjectConfigTabs tabs = config.getTabs();

        tabs.gotoIssueTypeFieldsTab(BUG);
        tabs.gotoIssueTypeFieldsTab(NEW_FEATURE);

        jira.getTester().getDriver().navigate().back();

        //We must bind this class to make sure that all loading of the tab has finished.
        pageBinder.bind(IssueTypeFieldsTab.class);
        assertThat(tabs.isIssueTypeTabSelected(BUG), equalTo(true));
    }
}
