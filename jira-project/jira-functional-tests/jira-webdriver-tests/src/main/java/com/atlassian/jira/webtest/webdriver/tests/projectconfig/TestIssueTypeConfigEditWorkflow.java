package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.project.issuetypes.DraftIssueTypeWorkflowTab;
import com.atlassian.jira.pageobjects.project.issuetypes.IssueTypeWorkflowTab;
import com.atlassian.jira.pageobjects.project.workflow.EditWorkflowDialog;
import com.atlassian.jira.pageobjects.websudo.JiraSudoFormDialog;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @since v6.2
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS, Category.IGNITE})
@RestoreOnce("xml/projectconfig/TestWorkflowTab.xml")
public class TestIssueTypeConfigEditWorkflow extends AbstractProjectEditWorkflow<IssueTypeWorkflowTab> {
    private static final String projectAdminUser = "fred";
    private static final String projectKey = "MKY";

    @Test
    public void testToggleVisibleIfAdmin() {
        assertTrue(gotoPageForProject(projectKey).isSwitcherPresent());
    }

    @Test
    public void testToggleVisibleIfProjectAdmin() {
        jira.quickLogin(projectAdminUser, projectAdminUser);
        assertTrue(gotoPageForProject(projectKey).isSwitcherPresent());
    }

    @Override
    IssueTypeWorkflowTab gotoPageForProject(final String key) {
        return jira.goTo(IssueTypeWorkflowTab.class, key, 1);
    }

    @Override
    EditWorkflowDialog openMigrationDialog(final IssueTypeWorkflowTab page) {
        return page.gotoEditWorkflowDialog();
    }

    @Override
    void authenticateSudoFormAfterMigration(final JiraSudoFormDialog dialog, final String workflowName) {
        dialog.authenticate("admin");
        pageBinder.bind(DraftIssueTypeWorkflowTab.class);
    }

    @Override
    void clickEdit(final IssueTypeWorkflowTab page, final String workflowDisplayName, final boolean diagram) {
        assertTrue(page.getSubtitle().contains(workflowDisplayName));
        page.clickEdit();
    }

    @Override
    void doWorkflowMigration(EditWorkflowDialog dialog, String workflowName) {
        dialog.clickAndWaitBeforeBind(DraftIssueTypeWorkflowTab.class);
    }
}
