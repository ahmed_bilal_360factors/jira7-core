package com.atlassian.jira.webtest.webdriver.tests.plugin.whisper;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.ResetData;
import com.atlassian.jira.pageobjects.framework.util.TimedQueryFactory;
import com.atlassian.jira.pageobjects.util.CriticalResources;
import com.atlassian.jira.pageobjects.util.CriticalResources.ResourceType;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.common.collect.ImmutableList;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;

import javax.inject.Inject;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.IsNot.not;

@WebTest ({ com.atlassian.jira.functest.framework.suite.Category.WEBDRIVER_TEST})
@Restore("TestWhisperPlugin.zip")
public class TestWhisperPlugin extends BaseJiraWebTest {

    private static final String PROJECT_KEY = "HSP";
    private String issueKey;

    @Inject
    private CriticalResources criticalResources;

    @Inject
    private JavascriptExecutor executor;

    @Inject
    private TimedQueryFactory queryFactory;

    private static final String INSTRUMENT_PROPERTY_CSS = "instrument-css";
    private static final String INSTRUMENT_PROPERTY_JS = "instrument-js";

    @Before
    public void setUp() {
        backdoor.systemProperties().setProperty(INSTRUMENT_PROPERTY_JS, "true");
        backdoor.systemProperties().setProperty(INSTRUMENT_PROPERTY_CSS, "true");
        backdoor.darkFeatures().disableForSite("whisper.disabled");

        issueKey = backdoor.issues().loginAs("admin").createIssue(PROJECT_KEY, "Test").key;
    }

    @After
    public void tearDown() throws Exception {
        backdoor.systemProperties().unsetProperty(INSTRUMENT_PROPERTY_JS);
        backdoor.systemProperties().unsetProperty(INSTRUMENT_PROPERTY_CSS);

        if (issueKey != null) {
            backdoor.issues().deleteIssue(issueKey, true);
        }
    }

    @Test
    public void shouldInitializeWhisper() {
        refreshResources();

        assertThat("JS files loaded", getResources(ResourceType.SCRIPT), is(not(empty())));
        assertThat("No active CSS", getResources(ResourceType.STYLESHEET), is(empty()));

        Poller.waitUntilEquals("success", queryFactory.forSupplier(() -> executor.executeScript("return window.hermes.getState().messagesLoadStatus")));
    }

    private void refreshResources() {
        // those page reloads are needed to avoid flakiness, instrumenting of resources does not kick in immediately
        jira.goToViewIssue(issueKey);
        jira.getTester().getDriver().navigate().refresh();
    }

    private ImmutableList<String> getResources(ResourceType type) {
        return criticalResources.getActive(type).stream().filter(resourceKey -> resourceKey.contains("atlassian-whisper-plugin")).collect(CollectorsUtil.toImmutableList());
    }
}