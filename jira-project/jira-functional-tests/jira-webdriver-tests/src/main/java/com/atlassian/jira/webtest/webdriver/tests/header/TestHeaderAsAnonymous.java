package com.atlassian.jira.webtest.webdriver.tests.header;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.CommonHeader;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.pageobjects.page.LoginPage;
import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@WebTest({Category.WEBDRIVER_TEST})
@RestoreOnce(value = "header-one-project.xml")
@LoginAs(anonymous = true)
public class TestHeaderAsAnonymous extends BaseJiraWebTest {
    private CommonHeader header;

    @Before
    public void setUp() {
        jira.visit(LoginPage.class);
        header = CommonHeader.visit(jira);
    }

    @Test
    public void testAppSwitcherVisible() {
        assertTrue(header.hasAppSwitcher());
    }

    @Test
    public void testAppSwitcherBaseUrl() {
        MatcherAssert.assertThat(header.getAppSwitcherMenuLinkTarget(), allOf(startsWith("http://"), endsWith("/secure/MyJiraHome.jspa")));
    }

    @Test
    public void testNoAppSwitcherMenuWhenOnlyOneApplicationVisible() {
        assertFalse(header.hasAppSwitcherMenu());
    }

    @Test
    public void testDashboardLinkVisible() {
        assertTrue(header.hasMainHeaderLinks());
        MatcherAssert.assertThat(header.getMainHeaderLinkIds(), IsIterableContainingInOrder.contains("home_link"));
    }

    @Test
    public void testCreateIssueButtonVisible() {
        assertFalse(header.hasCreateIssueButton());
    }

    @Test
    public void testQuickSearchVisible() {
        assertTrue(header.hasQuickSearch());
    }

    @Test
    public void testHelpMenuVisible() {
        assertTrue(header.hasHelpMenu());
    }

    @Test
    public void testAdminMenuNotVisible() {
        assertFalse(header.hasAdminMenu());
    }

    @Test
    public void testLoginButtonVisible() {
        assertTrue(header.hasLoginButton());
    }

}
