package com.atlassian.jira.webtest.webdriver.tests.header;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.CommonHeader;
import com.atlassian.jira.pageobjects.config.LoginAs;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@WebTest({Category.WEBDRIVER_TEST})
@RestoreOnce(value = "header-one-project.xml")
@LoginAs(user = "bob", password = "bob")
public class TestHeaderAsAuthenticatedUser extends BaseJiraWebTest {
    private CommonHeader header;

    @Before
    public void setUp() {
        header = CommonHeader.visit(jira);
    }

    @Test
    public void testMainHeaderLinks() {
        assertTrue(header.hasMainHeaderLinks());

        if ("ENABLED".equals(backdoor.plugins().getPluginState("com.atlassian.jira.dev.reference-plugin"))) {
            assertThat(header.getMainHeaderLinkIds(), contains("home_link", "browse_link", "find_link", "reference-menu-top-level-section"));
        } else {
            assertThat(header.getMainHeaderLinkIds(), contains("home_link", "browse_link", "find_link"));
        }
    }

    @Test
    public void testUser() {
        assertTrue(header.hasAppSwitcher());
        assertTrue(header.hasMainHeaderLinks());
        assertTrue(header.hasCreateIssueButton());
        assertTrue(header.hasQuickSearch());
        assertTrue(header.hasHelpMenu());
        assertFalse(header.hasAdminMenu());
        assertFalse(header.hasLoginButton());
        assertTrue(header.hasUserOptionsMenu());
    }
}
