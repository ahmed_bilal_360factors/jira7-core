package com.atlassian.jira.webtest.webdriver.tests.export;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.navigator.AgnosticIssueNavigator;
import com.atlassian.jira.pageobjects.navigator.ColumnsConfigurationDialog;
import com.atlassian.jira.pageobjects.navigator.IssueNavigatorResults;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Cookie;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

@WebTest({Category.WEBDRIVER_TEST, Category.ISSUE_NAVIGATOR, Category.ISSUES})
@Restore("xml/TestCsvExportCurrentFields.xml")
public class TestCsvExportCurrentFields extends BaseJiraWebTest {
    private final static String EXPORT_USER = "export_user";

    @Inject
    private TraceContext traceContext;

    @Before
    public void setUp() {
        backdoor.darkFeatures().enableForSite("jira.export.csv.enabled");
        backdoor.flags().clearFlags();
    }

    @Test
    @LoginAs(user = EXPORT_USER, password = EXPORT_USER, targetPage = AgnosticIssueNavigator.class)
    public void testIfCurrentFieldsExportIsTheSameAsModelFile() throws IOException {
        final AgnosticIssueNavigator page = pageBinder.navigateToAndBind(AgnosticIssueNavigator.class);
        final IssueNavigatorResults results = page.getResults();
        final Tracer checkpoint = traceContext.checkpoint();

        final ColumnsConfigurationDialog columnsDialog = results.getColumnsConfigurationDialog();
        columnsDialog.addColumn("Linked Issues");
        columnsDialog.addColumn("Single select");
        columnsDialog.submit();

        traceContext.waitFor(checkpoint, "jira.search.stable.update");
        assertThat(downloadExportData(), equalTo(getExportModelContent()));
    }

    private String downloadExportData() throws IOException {
        final String url = "/sr/jira.issueviews:searchrequest-csv-current-fields/temp/SearchRequest.csv?jqlQuery=&tempMax=1000";

        final HttpClient httpClient = new DefaultHttpClient();
        final HttpGet httpGet = new HttpGet(jira.environmentData().getBaseUrl() + url);
        httpGet.addHeader("Cookie", getCookiesString());

        final HttpResponse httpResponse = httpClient.execute(httpGet);
        final StringWriter stringWriter = new StringWriter();

        IOUtils.copy(httpResponse.getEntity().getContent(), stringWriter);

        return stringWriter.toString();
    }

    private String getCookiesString() {
        final Set<Cookie> cookies = jira.getTester().getDriver().manage().getCookies();
        final StringBuilder builder = new StringBuilder();

        for (Cookie cookie : cookies) {
            builder.append(cookie.getName());
            builder.append("=");
            builder.append(cookie.getValue());
            builder.append(";");
        }

        return builder.toString();
    }

    private String getExportModelContent() throws IOException {
        final String filePath = jira.environmentData().getXMLDataLocation().getPath()
                + System.getProperty("file.separator") + "TestCsvExportCurrentFields.csv";
        final String content = FileUtils.readFileToString(new File(filePath));

        return content.replaceAll("http://localhost:8090/jira", jira.environmentData().getBaseUrl().toString());
    }
}
