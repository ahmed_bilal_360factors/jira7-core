package com.atlassian.jira.webtest.webdriver.tests.admin.groupbrowser;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.admin.BulkEditGroupMembersPage;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION})
@Restore("xml/TestBulkEditGroupMembers.xml")
public class TestBulkEditGroupMembers extends BaseJiraWebTest {
    private static final String ERROR_CANNOT_ADD_USER_INVALID = "Cannot add user. 'invalid' does not exist";
    private static final String ERROR_ADMIN_ALREADY_MEMBER_OF_ALL = "Cannot add user 'admin', user is already a member of all the selected group(s)";
    private static final String ERROR_ADMIN_ALREADY_MEMBER_OF_JIRA_ADMIN = "Cannot add user 'admin', user is already a member of 'jira-administrators'";

    private BulkEditGroupMembersPage bulkEditGroupMembersPage;

    @Test
    public void selectGroupsAndAddInvalidUser() {
        bulkEditGroupMembersPage = jira.goTo(BulkEditGroupMembersPage.class, Arrays.asList("jira-developers", "jira-users"));
        assertTrue(bulkEditGroupMembersPage.groupHeadingText().contains("Selected 2 of 5 groups"));
        bulkEditGroupMembersPage.addNewMember("invalid").submitMembersToAdd();
        assertTrue(bulkEditGroupMembersPage.errorMessageText().contains(ERROR_CANNOT_ADD_USER_INVALID));
        assertTrue(bulkEditGroupMembersPage.newMembersText().contains("invalid"));
        bulkEditGroupMembersPage.addNewMember("");
    }

    @Test
    public void addExistingMemberToGroup() {
        bulkEditGroupMembersPage = jira.goTo(BulkEditGroupMembersPage.class, Arrays.asList("jira-administrators"));
        bulkEditGroupMembersPage.addNewMember("admin").submitMembersToAdd();
        assertTrue(bulkEditGroupMembersPage.errorMessageText().contains(ERROR_ADMIN_ALREADY_MEMBER_OF_JIRA_ADMIN));
        assertTrue(bulkEditGroupMembersPage.newMembersText().contains("admin"));
        bulkEditGroupMembersPage.addNewMember("");
    }

    @Test
    public void addExistingMemberToMultipleGroups() {
        bulkEditGroupMembersPage = jira.goTo(BulkEditGroupMembersPage.class, Arrays.asList("jira-developers", "jira-users"));
        bulkEditGroupMembersPage.addNewMember("admin").submitMembersToAdd();
        assertTrue(bulkEditGroupMembersPage.errorMessageText().contains(ERROR_ADMIN_ALREADY_MEMBER_OF_ALL));
        assertTrue(bulkEditGroupMembersPage.newMembersText().contains("admin"));
        bulkEditGroupMembersPage.addNewMember("");
    }

    @Test
    public void addExistingMemberAndNonExistingMemberToGroup() {
        bulkEditGroupMembersPage = jira.goTo(BulkEditGroupMembersPage.class, Arrays.asList("jira-administrators"));
        bulkEditGroupMembersPage.addNewMembers(Arrays.asList("admin", "dev")).submitMembersToAdd();
        assertTrue(bulkEditGroupMembersPage.errorMessageText().contains(ERROR_ADMIN_ALREADY_MEMBER_OF_JIRA_ADMIN));
        assertTrue(bulkEditGroupMembersPage.newMembersText().contains("admin"));
        assertTrue(bulkEditGroupMembersPage.newMembersText().contains("dev"));
        bulkEditGroupMembersPage.addNewMember("");
    }

    @Test
    public void addExistingMemberAndNonExistingMemberToAllGroups() {
        bulkEditGroupMembersPage = jira.goTo(BulkEditGroupMembersPage.class, Arrays.asList("jira-administrators", "jira-developers", "jira-users"));
        bulkEditGroupMembersPage.addNewMembers(Arrays.asList("admin", "dev")).submitMembersToAdd();
        assertTrue(bulkEditGroupMembersPage.errorMessageText().contains(ERROR_ADMIN_ALREADY_MEMBER_OF_ALL));
        assertTrue(bulkEditGroupMembersPage.newMembersText().contains("admin"));
        assertTrue(bulkEditGroupMembersPage.newMembersText().contains("dev"));
        bulkEditGroupMembersPage.addNewMember("");
    }

    @Test
    public void atteptToAddVariousUserMembersAndTestPruningLargeSetOfUsernames() {
        bulkEditGroupMembersPage = jira.goTo(BulkEditGroupMembersPage.class, Arrays.asList("jira-administrators"));
        bulkEditGroupMembersPage.addNewMembers(Arrays.asList("admin", "user", "admin", "duplicate", "invalid", "dev", "duplicate", "duplicate", "error", "user")).submitMembersToAdd();
        assertTrue(bulkEditGroupMembersPage.errorMessageText().contains(ERROR_ADMIN_ALREADY_MEMBER_OF_JIRA_ADMIN));
        assertTrue(bulkEditGroupMembersPage.errorMessageText().contains("Cannot add user. 'duplicate' does not exist"));
        assertTrue(bulkEditGroupMembersPage.errorMessageText().contains("Cannot add user. 'invalid' does not exist"));
        assertTrue(bulkEditGroupMembersPage.errorMessageText().contains("Cannot add user. 'duplicate' does not exist"));
        assertTrue(bulkEditGroupMembersPage.errorMessageText().contains("Cannot add user. 'duplicate' does not exist"));
        assertTrue(bulkEditGroupMembersPage.errorMessageText().contains("Cannot add user. 'error' does not exist"));
        assertTrue(bulkEditGroupMembersPage.newMembersText().contains("user"));
        assertTrue(bulkEditGroupMembersPage.newMembersText().contains("admin"));
        assertTrue(bulkEditGroupMembersPage.newMembersText().contains("admin"));
        assertTrue(bulkEditGroupMembersPage.newMembersText().contains("duplicate"));
        assertTrue(bulkEditGroupMembersPage.newMembersText().contains("invalid"));
        assertTrue(bulkEditGroupMembersPage.newMembersText().contains("error"));
        bulkEditGroupMembersPage.addNewMember("");
    }
}
