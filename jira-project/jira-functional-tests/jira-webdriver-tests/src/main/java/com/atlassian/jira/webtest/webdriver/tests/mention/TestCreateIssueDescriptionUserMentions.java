package com.atlassian.jira.webtest.webdriver.tests.mention;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.viewissue.CreateIssueDescriptionField;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Test for user mentions in JIRA
 *
 * @since v7.2
 */
@RestoreOnce("TestMentions.xml")
@WebTest({Category.WEBDRIVER_TEST, Category.USERS_AND_GROUPS, Category.ISSUES})
public class TestCreateIssueDescriptionUserMentions extends BaseJiraWebTest {

    @Test
    public void testMentionsWorkWhenCreatingAnIssueDescription() {
        final String username = "admin";

        // start create issue dialog
        final CreateIssueDescriptionField description = jira
                .gotoHomePage()
                .getHeader()
                .createIssue()
                .getDescription();

        description.typeInput("@" + username);
        waitUntilTrue(username + " should be available for selection", description.getMentions().hasSuggestion(username));

        description.selectMention(username);
        waitUntilEquals(username + " should be selected", "[~" + username + "]", description.getInputTimed());
    }

}
