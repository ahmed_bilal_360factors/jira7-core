package com.atlassian.jira.webtest.webdriver.tests.webhooks;

import com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookRegistrationClient;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.ResetData;
import com.atlassian.jira.pageobjects.pages.webhooks.AbstractWebHookPanel;
import com.atlassian.jira.pageobjects.pages.webhooks.DisplayedWebHookListener;
import com.atlassian.jira.pageobjects.pages.webhooks.EditWebHookListenerPanel;
import com.atlassian.jira.pageobjects.pages.webhooks.ViewWebHookListenerPanel;
import com.atlassian.jira.pageobjects.pages.webhooks.WebHookAdminPage;
import com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks.HttpResponseTester;
import com.google.common.collect.Iterables;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;

import static com.atlassian.jira.webtest.webdriver.tests.webhooks.WebHooksUITestCommon.displaysListener;
import static com.atlassian.jira.webtest.webdriver.tests.webhooks.WebHooksUITestCommon.getAllJiraEvents;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@WebTest({Category.WEBDRIVER_TEST, Category.WEBHOOKS})
@ResetData
public class TestWebHookAdministrationUI extends BaseJiraWebTest {
    protected HttpResponseTester responseTester;

    @Before
    public void setUp() throws Exception {
        responseTester = HttpResponseTester.createTester(jira.environmentData());

    }

    @Test
    public void testAddingSingleWebHook() {
        registerWebHook();
        final WebHookAdminPage emptyWebHookAdminPage = jira.goTo(WebHookAdminPage.class);

        final DisplayedWebHookListener listenerToAdd = DisplayedWebHookListener.builder()
                .name("new webhook")
                .description("description")
                .events(getAllJiraEvents())
                .excludeBody(false)
                .url("http://localhost:8090/jira/plugins/servlet/abcd")
                .build();

        final EditWebHookListenerPanel editWebHookListenerPanel = emptyWebHookAdminPage.createWebHook().setFields(listenerToAdd);

        final ViewWebHookListenerPanel result = editWebHookListenerPanel.submit();

        assertThat(result, displaysListener(listenerToAdd));
        assertThat(result, containsMessage("WebHook '" + listenerToAdd.getName() + "' has been successfully created."));
    }

    @Test
    public void testUpdatingWebHook() throws InterruptedException {
        final WebHookRegistrationClient.Registration registration = new WebHookRegistrationClient.Registration();

        DisplayedWebHookListener listenerToUpdate = DisplayedWebHookListener.builder()
                .name("webhook to update")
                .excludeBody(true)
                .issueSectionFilter("project = DEMO")
                .events(getAllJiraEvents())
                .build();

        registration.name = listenerToUpdate.getName();
        registration.excludeBody = listenerToUpdate.isExcludeBody();
        registration.events = listenerToUpdate.getEventsAsArray();
        registration.setFilterForIssueSection(listenerToUpdate.getIssueSectionFilter());

        responseTester.registerWebhook(registration);

        final WebHookAdminPage emptyWebHookAdminPage = jira.goTo(WebHookAdminPage.class);
        final ViewWebHookListenerPanel viewWebHookListenerPanel = Iterables.getFirst(emptyWebHookAdminPage.getWebHookLinks(), null).click();

        assertThat(viewWebHookListenerPanel, displaysListener(listenerToUpdate.withUrl(responseTester.getPath())));

        final DisplayedWebHookListener updated = listenerToUpdate
                .withUrl(responseTester.getPath())
                .withDescription("new description")
                .withEvents(newHashSet("project_deleted"));

        final EditWebHookListenerPanel editWebHookPanel = viewWebHookListenerPanel.edit();

        assertThat(editWebHookPanel, eventMatcher(getAllJiraEvents()));

        final ViewWebHookListenerPanel panel = editWebHookPanel.setFields(updated).submit();

        assertThat(panel, displaysListener(updated));

        backdoor.project().deleteProject("HSP");

        assertNotNull(responseTester.getResponseData());
    }

    @Test
    public void testDisablingWebHook() {
        registerWebHook();
        final DisplayedWebHookListener listener = DisplayedWebHookListener.builder()
                .name("webhook to update")
                .excludeBody(true)
                .issueSectionFilter("project = DEMO")
                .events(getAllJiraEvents())
                .url("http://localhost:2990/jira/plugins/servlet/abcd")
                .build();

        final ViewWebHookListenerPanel createdWebHook = jira.goTo(WebHookAdminPage.class)
                .createWebHook()
                .setFields(listener)
                .submit();

        assertThat(createdWebHook, displaysListener(listener));

        final EditWebHookListenerPanel editWebHookListenerPanel = createdWebHook.edit();
        final ViewWebHookListenerPanel viewWebHookPanel = editWebHookListenerPanel
                .disable()
                .submit();

        assertThat(viewWebHookPanel, enablementStatus(false));

        final ViewWebHookListenerPanel enabledWebHookPanel = viewWebHookPanel.edit()
                .enabled()
                .submit();

        assertThat(enabledWebHookPanel, enablementStatus(true));
    }

    @Test
    public void testWebHookEditErrorsWhenEmptyUrl() {
        registerWebHook();
        DisplayedWebHookListener.Builder listener = DisplayedWebHookListener.builder()
                .name("webhook to update")
                .excludeBody(true)
                .issueSectionFilter("project = DEMO")
                .events(getAllJiraEvents());

        final EditWebHookListenerPanel errors = jira.goTo(WebHookAdminPage.class)
                .createWebHook()
                .setFields(listener.build())
                .submitAndExpectErrors();

        assertThat(errors, containsError("Webhook url is required."));
        errors.cancel();
    }

    @Test
    public void testWebHookEditErrorsWhenWrongUrl() {
        registerWebHook();
        DisplayedWebHookListener.Builder listener = DisplayedWebHookListener.builder()
                .name("webhook to update")
                .excludeBody(true)
                .issueSectionFilter("project = DEMO")
                .events(getAllJiraEvents());

        final EditWebHookListenerPanel errors = jira.goTo(WebHookAdminPage.class)
                .createWebHook()
                .setFields(listener.url("ftp://example.com").build())
                .submitAndExpectErrors();

        assertThat(errors, containsError("Invalid url. The only allowed protocols are http and https."));
        errors.cancel();
    }

    @Test
    public void testWebHookEditErrorsWhenNoNameProvided() {
        registerWebHook();
        final DisplayedWebHookListener listener = DisplayedWebHookListener.builder()
                .excludeBody(true)
                .issueSectionFilter("project = DEMO")
                .events(getAllJiraEvents())
                .build();

        final EditWebHookListenerPanel errors = jira.goTo(WebHookAdminPage.class)
                .createWebHook()
                .setFields(listener)
                .submitAndExpectErrors();

        assertThat(errors, containsError("Name is required."));

        errors.cancel();
    }

    @Test
    public void testWebHookEditErrorsWithInvalidJql() {
        registerWebHook();
        final DisplayedWebHookListener listener = DisplayedWebHookListener.builder()
                .excludeBody(true)
                .issueSectionFilter("project ~~= DEMO")
                .description("descripton")
                .name("name")
                .url("http://example.com")
                .events(getAllJiraEvents())
                .build();

        final EditWebHookListenerPanel errors = jira.goTo(WebHookAdminPage.class)
                .createWebHook()
                .setFields(listener)
                .submitAndExpectErrors();

        assertThat(errors, containsError("Invalid JQL statement."));

        errors.cancel();
    }

    @Test
    public void testAllUrlVariablesAreDisplayed() {
        registerWebHook();
        EditWebHookListenerPanel createWebhookPage = jira.goTo(WebHookAdminPage.class).createWebHook();
        assertThat(createWebhookPage.getAvailableVariables(), Matchers.hasItems("project.id", "project.key", "issue.key", "issue.id", "modifiedUser.key", "modifiedUser.name"));
        createWebhookPage.cancel();
    }

    private void registerWebHook() {
        // At leas one web hook has to be on the page so page could bind properly.
        final WebHookRegistrationClient.Registration registration = new WebHookRegistrationClient.Registration();
        registration.name = "test";
        responseTester.registerWebhook(registration);
    }

    private Matcher<EditWebHookListenerPanel> containsError(final String expectedError) {
        return new TypeSafeMatcher<EditWebHookListenerPanel>() {
            @Override
            protected boolean matchesSafely(final EditWebHookListenerPanel webHookEditError) {
                return webHookEditError.getErrors().contains(expectedError);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("Errors on page do not contain")
                        .appendValue(expectedError);
            }
        };
    }

    private Matcher<ViewWebHookListenerPanel> enablementStatus(final boolean status) {
        return new TypeSafeMatcher<ViewWebHookListenerPanel>() {
            @Override
            protected boolean matchesSafely(final ViewWebHookListenerPanel viewWebHookListenerPanel) {
                return status == viewWebHookListenerPanel.isEnabled();
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("Expected webhook to be")
                        .appendText(status ? "enabled" : "disabled");
            }
        };
    }

    private Matcher<ViewWebHookListenerPanel> containsMessage(final String expectedMessage) {
        return new TypeSafeMatcher<ViewWebHookListenerPanel>() {
            @Override
            protected boolean matchesSafely(final ViewWebHookListenerPanel viewWebHookListenerPanel) {
                return viewWebHookListenerPanel.getMessage().equals(expectedMessage);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("Message").appendValue(expectedMessage).appendText("not found");
            }
        };
    }

    private Matcher<AbstractWebHookPanel> eventMatcher(final Iterable<String> events) {
        return new TypeSafeMatcher<AbstractWebHookPanel>() {
            @Override
            protected boolean matchesSafely(final AbstractWebHookPanel abstractWebHookPanel) {
                final Set<String> selectedEvents = abstractWebHookPanel.getEvents();
                return Iterables.all(newArrayList(events), selectedEvents::contains);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("Events not found on the page")
                        .appendValue(events);
            }
        };
    }
}
