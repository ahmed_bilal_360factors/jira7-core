package com.atlassian.jira.webtest.webdriver.tests.onboarding;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.CreateUser;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.config.ResetDataOnce;
import com.atlassian.jira.pageobjects.onboarding.AvatarSequence;
import com.atlassian.jira.pageobjects.onboarding.ChooseLanguageSequence;
import com.atlassian.jira.pageobjects.onboarding.WelcomeToJiraPage;
import org.junit.Test;

import java.util.Locale;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

@WebTest({Category.WEBDRIVER_TEST})
@ResetDataOnce()
public class OnboardingLanguageTest extends BaseJiraWebTest {
    private static final String DEVELOPER = "developer";

    @Test
    @CreateUser(username = DEVELOPER, password = DEVELOPER)
    @LoginAs(user = DEVELOPER, targetPage = WelcomeToJiraPage.class)
    public void testSwitchingLanguages() {
        String originalTitle, newTitle;
        ChooseLanguageSequence sequence = pageBinder.bind(ChooseLanguageSequence.class);
        originalTitle = sequence.getPageHeadingText();
        sequence.selectLanguage(Locale.JAPAN);
        newTitle = sequence.getPageHeadingText();
        assertThat("Title should change (shouldn't have been Japanese to start with)", newTitle, not(equalTo(originalTitle)));
        assertThat("Title should be in Japanese now", newTitle, containsString("JIRA\u3078\u3088\u3046\u3053\u305D"));

        sequence.nextStep();
        AvatarSequence avatarSequence = pageBinder.bind(AvatarSequence.class);
        assertThat("Subsequent step should be in Japanese", avatarSequence.getPageHeadingText(), containsString("JIRA\u3078\u3088\u3046\u3053\u305D"));
    }

}
