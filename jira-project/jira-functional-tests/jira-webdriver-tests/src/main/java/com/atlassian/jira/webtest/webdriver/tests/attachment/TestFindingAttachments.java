package com.atlassian.jira.webtest.webdriver.tests.attachment;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.rule.Rules;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.AttachmentRow;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.AttachmentSection;
import com.atlassian.jira.pageobjects.util.WaitForPageReload;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import javax.inject.Inject;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Tests that attachments are found correctly regardless if they are stored in the
 * current directory structure or previous legacy versions
 *
 * @since v7.0
 */
@Restore("TestFindingAttachment.xml")
@WebTest({Category.WEBDRIVER_TEST, Category.ATTACHMENTS})
public class TestFindingAttachments extends BaseJiraWebTest {
    private String attachmentPath;
    private AttachmentRow attachment;

    @Inject
    private WaitForPageReload waitForPageReload;

    @Rule
    public final TestRule cleanAttachmentsRule = Rules.cleanAttachments(backdoor::attachments);

    @Before
    public void setUpTest() {
        backdoor.attachments().enable();
        attachmentPath = backdoor.attachments().getAttachmentPath();
    }

    /**
     * This test checks that an attachment can be found when stored with the directory
     * ../ProjectKey/IssueBucket/IssueKey/AttachmentID.
     * With the buckets being determined by the issue key and the bucket size being 10,000.
     *
     * @throws Exception if something goes wrong in the test
     */
    @Test
    public void testFindingAttachmentWithBucketIssueDirectory() throws Exception {
        assertAttachedFileFound("This was stored with issue buckets", "/RAT/10000/RAT-1/", "10010");
    }

    /**
     * This test checks that an attachment can be found when stored in the old version of
     * ../ProjectKey/IssueKey/AttachmentID
     *
     * @throws Exception if something goes wrong with the test
     */
    @Test
    public void testFindingAttachmentWithProjectIssueDirectory() throws Exception {
        assertAttachedFileFound("This was stored in project/issue", "/RAT/RAT-1/", "10010");
    }

    /**
     * This test checks that an attachment can be found when stored in the old version of
     * ../ProjectKey/IssueKey/AttachmentID_AttachmentFileName
     *
     * @throws Exception if something goes wrong with the test
     */
    @Test
    public void testFindingAttachmentWithLegacyFileName() throws Exception {
        assertAttachedFileFound("This was stored with legacy filename", "/RAT/RAT-1/", "10010_info");
    }

    private void assertAttachedFileFound(String fileContents, String filePath, String filename) throws IOException {
        installAttachedFile(fileContents, filePath, filename);
        ViewIssuePage issuePage = jira.goToViewIssue("RAT-1");
        AttachmentSection attachmentSection = issuePage.getAttachmentSection();
        attachmentSection.openOptions().setViewMode(AttachmentsBlock.ViewMode.LIST);

        Map<String, AttachmentRow> attachmentRows = attachmentSection.getAttachmentRowsByTitle();
        attachment = attachmentRows.get("info.txt");
        assertThat("an item titled info.txt exists", attachment, is(notNullValue()));

        waitForPageReload.afterExecuting(() -> attachment.getFileLink().click());
        //Refresh the page for a browser cache
        jira.getTester().getDriver().navigate().refresh();
        String source = jira.getTester().getDriver().getPageSource();
        assertThat(source, containsString(fileContents));
    }

    private String installAttachedFile(final String contents, final String contentsPath, final String filename) throws IOException {
        File attachedFile = new File(attachmentPath + contentsPath, filename);
        attachedFile.getParentFile().mkdirs();
        PrintWriter out = new PrintWriter(new FileWriter(attachedFile));
        try {
            out.println(padFile(contents));
        } finally {
            out.flush();
            out.close();
        }
        return attachmentPath;
    }

    //Padding the file as the contents need to be the same length as specified in the xml for the attachment.
    private String padFile(final String fileContents) {
        String newFileContent = fileContents;
        final int FILE_LENGTH = 40;

        while (newFileContent.length() != FILE_LENGTH) {
            newFileContent += " ";
        }

        return newFileContent;
    }
}
