package com.atlassian.jira.webtest.webdriver.tests.gagets;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.gadgets.GadgetContainer;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import it.com.atlassian.gadgets.pages.Gadget;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertArrayEquals;

/**
 * Webdriver test for the FilterResults gadget.
 *
 * @since v7.1.8
 */

@WebTest({Category.WEBDRIVER_TEST, Category.GADGETS})
public class TestFilterResultsGadget extends BaseJiraWebTest {

    public static final int DASHBOARD_ID = 10100;

    public static final String GADGET_ID_ASC = "gadget-10100";
    public static final String[] EXPECTED_ORDER_ASC = new String[]{"TEST-5", "TEST-4", "TEST-3", "TEST-6", "TEST-2", "TEST-1"};

    public static final String GADGET_ID_DESC = "gadget-10200";
    public static final String[] EXPECTED_ORDER_DESC = new String[]{"TEST-5", "TEST-4", "TEST-6", "TEST-3", "TEST-2", "TEST-1"};

    /**
     * First filter on 4th place has "order by issuekey ASC", the second one has "order by issuekey DESC"
     */
    @Test
    @Restore("xml/JRA-21443.xml")
    public void testFilterOrderWithMultipleOrderByClauses() {
        final DashboardPage dashboardPage = pageBinder.navigateToAndBind(DashboardPage.class, DASHBOARD_ID);
        final GadgetContainer gadgetContainer = dashboardPage.gadgets();

        assertFilterOrder(gadgetContainer, GADGET_ID_ASC, EXPECTED_ORDER_ASC);
        assertFilterOrder(gadgetContainer, GADGET_ID_DESC, EXPECTED_ORDER_DESC);
    }

    private void assertFilterOrder(GadgetContainer gadgetContainer, String gadgetId, String[] expectedOrder) {
        final Gadget gadget = gadgetContainer.get(gadgetId);
        final PageElement issueTable = getIssueTable(gadget);

        final String[] actualOrder = getIssueKeys(issueTable);

        assertArrayEquals(expectedOrder, actualOrder);
    }

    private PageElement getIssueTable(Gadget gadget) {
        return gadget.find(By.id(gadget.getId())).find(By.className("issue-table"), TimeoutType.AJAX_ACTION);
    }

    private String[] getIssueKeys(PageElement issueTable) {
        return issueTable.findAll(By.className("issuerow")).stream().map(pe -> pe.getAttribute("data-issuekey")).toArray(String[]::new);
    }
}
