package com.atlassian.jira.webtest.webdriver.tests.admin;

import com.atlassian.jira.functest.framework.backdoor.ApplicationRoleControl;
import com.atlassian.jira.functest.framework.backdoor.ApplicationRoleControl.ApplicationRoleBean;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.elements.AuiFlag;
import com.atlassian.jira.pageobjects.elements.GlobalFlags;
import com.atlassian.jira.pageobjects.pages.admin.application.ApplicationAccessPage;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.webtests.LicenseKeys;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @since v7.0
 */
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION})
public class TestAdminFlags extends BaseJiraWebTest {
    @Inject
    private TraceContext context;

    @Test
    public void adminWillSeeFlagWithNoApplicationAccess() {
        backdoor.restoreBlankInstance(LicenseKeys.COMMERCIAL);
        backdoor.flags().enableFlags();
        //This admin must have access so no flag.
        Optional<AdminNoAccessFlag> adminFlag = reloadAndGetAdminFlag();
        assertThat(adminFlag.isPresent(), Matchers.is(false));

        //Remove access from all users including the admin.
        removeApplicationAccess();

        //This admin should now see the flag.
        adminFlag = reloadAndGetAdminFlag();
        assertThat(adminFlag.isPresent(), Matchers.is(true));

        //This admin should no longer see the flag it has been dismissed.
        adminFlag.get().dismissWait("admin.lockout");
        adminFlag = reloadAndGetAdminFlag();
        assertThat(adminFlag.isPresent(), Matchers.is(false));

        //Flag should still be dismissed even after another login.
        jira.quickLoginAsSysadmin();

        adminFlag = reloadAndGetAdminFlag();
        assertThat(adminFlag.isPresent(), Matchers.is(false));

        addApplicationAccess();

        //Flag should not be visible as admin has access. However, the flag will be reset to display on next error.
        jira.quickLoginAsSysadmin();
        adminFlag = reloadAndGetAdminFlag();
        assertThat(adminFlag.isPresent(), Matchers.is(false));

        removeApplicationAccess();

        //The flag should be displayed again.
        adminFlag = reloadAndGetAdminFlag();
        assertThat(adminFlag.isPresent(), Matchers.is(true));

        //Clicking link should hide the flag.
        final AdminNoAccessFlag noAccessFlag = adminFlag.get();
        adminFlag = doAndGetAdminFlag(noAccessFlag::manageApplications);
        assertThat(adminFlag.isPresent(), Matchers.is(false));
    }

    private void removeApplicationAccess() {
        setRoleGroups();
    }

    private void addApplicationAccess() {
        setRoleGroups("jira-administrators");
    }

    private void setRoleGroups(String... groups) {
        final ApplicationRoleControl control = backdoor.applicationRoles();
        control.getRoles().stream()
                .map(ApplicationRoleBean::getKey)
                .forEach(key -> control.putRole(key, groups));
    }

    private Optional<AdminNoAccessFlag> reloadAndGetAdminFlag() {
        return doAndGetAdminFlag(jira::goToAdminHomePage);
    }

    private Optional<AdminNoAccessFlag> doAndGetAdminFlag(Runnable runnable) {
        final Tracer checkpoint = context.checkpoint();
        runnable.run();
        context.waitFor(checkpoint, "admin.flags.done");

        GlobalFlags globalFlags = pageBinder.bind(GlobalFlags.class);
        return globalFlags.getFlags()
                .stream()
                .filter(TestAdminFlags::isAdminFlag)
                .map(AdminNoAccessFlag::new)
                .findFirst();
    }

    private static boolean isAdminFlag(AuiFlag flag) {
        return flag.getFlagElement().find(By.id("admin-noprojects")).isPresent();
    }

    private class AdminNoAccessFlag {
        private final AuiFlag flag;

        private AdminNoAccessFlag(final AuiFlag flag) {
            this.flag = flag;
        }

        private ApplicationAccessPage manageApplications() {
            flag.getFlagElement().find(By.tagName("a")).click();
            return pageBinder.bind(ApplicationAccessPage.class);
        }

        public void dismissWait(String key) {
            flag.dismissWait(key);
        }
    }
}
