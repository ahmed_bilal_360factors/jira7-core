package com.atlassian.jira.webtest.webdriver.tests.admin.users;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.admin.user.UserBrowserPage;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;

@WebTest(Category.WEBDRIVER_TEST)
public class TestUserBrowser extends BaseJiraWebTest {
    @Test
    @Restore("TestUserBrowserManyUsers.xml")
    public void testPaginatorOnFilterChange() {
        UserBrowserPage userBrowser = jira.goTo(UserBrowserPage.class).filterByUserSearch("odd.example").filterByGroup("jira-users");
        Poller.waitUntil(userBrowser.getPaginationText(), equalTo("1 2 3"));
        userBrowser = userBrowser.clearFilterByUserSearch();
        Poller.waitUntil(userBrowser.getPaginationText(), equalTo("1 2 3 4 5 6 7"));
    }
}
