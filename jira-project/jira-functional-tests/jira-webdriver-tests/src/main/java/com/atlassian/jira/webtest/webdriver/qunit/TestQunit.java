package com.atlassian.jira.webtest.webdriver.qunit;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.ResetData;
import com.atlassian.plugins.jstestrunner.runner.JSTestRunner;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.io.File;

import static com.atlassian.plugins.jstestrunner.runner.JSTestRunner.plugin;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

/**
 * Runs all Qunit tests in WebDriver.
 * <p>
 * Utilises the aui-qunit-plugin to write all test outcomes in surefire format.
 *
 * @since v5.2
 */
@WebTest({Category.WEBDRIVER_TEST, Category.QUNIT})
public class TestQunit extends BaseJiraWebTest {
    private final File outdir;

    public TestQunit() {
        String location = System.getProperty("jira.qunit.testoutput.location");
        if (StringUtils.isEmpty(location)) {
            System.err.println("Writing result XML to tmp, jira.qunit.testoutput.location not defined");
            location = System.getProperty("java.io.tmpdir");
        }

        outdir = new File(location);
    }

    @Test
    @ResetData
    public void runAllTests() throws Exception {
        new JSTestRunner(outdir, pageBinder).run(not(anyOf(
                plugin(is("com.atlassian.jira.project-templates-plugin")),
                plugin(is("com.atlassian.jira.jira-quick-edit-plugin")),
                plugin(is("com.atlassian.jira.plugins.jira-development-integration-plugin"))
        )));
    }
}
