package com.atlassian.jira.webtest.webdriver.tests.reports;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.reports.AverageAgeReportPage;
import com.atlassian.jira.pageobjects.pages.reports.FieldTestConfigureReportPage;
import com.atlassian.jira.pageobjects.pages.reports.FieldTestReportResultPage;
import com.atlassian.jira.pageobjects.pages.reports.ReportResultPage;
import com.atlassian.jira.projects.pageobjects.webdriver.page.ReportsPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.MultiSelectElement;
import com.atlassian.pageobjects.elements.Option;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

@WebTest({Category.WEBDRIVER_TEST, Category.REPORTS})
@Restore("xml/TestSidebarInReports.xml")
public class TestSidebarInReports extends BaseJiraWebTest {

    private static final String PROJECT_KEY_TP = "TP";
    private static final String PROJECT_NAME_TP = "Test Project";
    private static final String PROJECT_NAME_AP = "Another Project";
    private static final String FILTER_NAME_CROSS_PROJECT = "All Projects Filter";

    @Inject
    private PageBinder pageBinder;

    @Inject
    private WebDriver webDriver;

    @Test
    public void multiSelectInFieldTestReport() {
        List<String> selectedValues = Arrays.asList("Green", "Blue", "Orange");

        ReportsPage reportsPage = goToReportsPage(PROJECT_KEY_TP);
        FieldTestConfigureReportPage fieldTestPage = goToFieldTestReport(reportsPage);
        MultiSelectElement multiSelect = fieldTestPage.getMultiSelect();
        List<Option> allOptions = multiSelect.getAllOptions();
        allOptions.stream()
                .filter(opt -> selectedValues.contains(opt.text()))
                .forEach(multiSelect::select);
        FieldTestReportResultPage results = fieldTestPage.next();
        fieldTestPage = results.configure();

        assertThat(fieldTestPage.getMultiSelect().getSelected().stream().map(Option::text).collect(toList()), contains(selectedValues.toArray()));
    }

    @Test
    public void navigateToReportFromProjectAndKeepSidebar() {
        ReportsPage reportsPage = goToReportsPage(PROJECT_KEY_TP);
        AverageAgeReportPage averageAgeReportPage = goToAverageAgeReport(reportsPage);

        ReportResultPage reportResultPage = averageAgeReportPage.next();
        Optional<String> projectContext = reportResultPage.getProjectNameInSidebar();

        assertThat(projectContext, is(Optional.of(PROJECT_NAME_TP)));
    }

    @Test
    public void navigateToReportFromProjectAndUpdateSidebar() {
        ReportsPage reportsPage = goToReportsPage(PROJECT_KEY_TP);
        AverageAgeReportPage averageAgeReportPage = goToAverageAgeReport(reportsPage);
        averageAgeReportPage.changeTargetProjectTo(PROJECT_NAME_AP);

        ReportResultPage reportResultPage = averageAgeReportPage.next();
        Optional<String> projectContext = reportResultPage.getProjectNameInSidebar();

        assertThat(projectContext, is(Optional.of(PROJECT_NAME_AP)));
    }

    @Test
    public void navigateToFilterReportAndKeepOriginalSidebar() {
        ReportsPage reportsPage = goToReportsPage(PROJECT_KEY_TP);
        AverageAgeReportPage averageAgeReportPage = goToAverageAgeReport(reportsPage);
        averageAgeReportPage.changeTargetFilterTo(FILTER_NAME_CROSS_PROJECT);

        ReportResultPage reportResultPage = averageAgeReportPage.next();
        Optional<String> projectContext = reportResultPage.getProjectNameInSidebar();

        assertThat(projectContext, is(Optional.of(PROJECT_NAME_TP)));
    }

    @Test
    public void directLinkToReportWithSidebar() {
        webDriver.navigate().to(jira.getProductInstance().getBaseUrl() +
                "/secure/ConfigureReport.jspa" +
                "?projectOrFilterId=project-10000" +
                "&periodName=daily&daysprevious=30" +
                "&selectedProjectId=10000" +
                "&reportKey=com.atlassian.jira.jira-core-reports-plugin" +
                "%3Aaverageage-report");

        ReportResultPage reportResultPage = pageBinder.bind(ReportResultPage.class);
        Optional<String> projectContext = reportResultPage.getProjectNameInSidebar();

        assertThat(projectContext, is(Optional.of(PROJECT_NAME_TP)));
    }

    @Test
    public void fallbackToProjectInSession() {
        // TimeTrackingReport has no project picker (it has always relied on the project in the session). If we link
        // directly to it, we force a project lookup from the session.
        goToReportsPage(PROJECT_KEY_TP);

        webDriver.navigate().to(jira.getProductInstance().getBaseUrl() +
                "/secure/ConfigureReport.jspa" +
                "?versionId=-1" +
                "&sortingOrder=least" +
                "&completedFilter=all" +
                "&subtaskInclusion=onlySelected" +
                "&selectedProjectId=10000" +
                "&reportKey=com.atlassian.jira.jira-core-reports-plugin" +
                "%3Atime-tracking" +
                "&atl_token=BWP3-NZB2-6EDY-6C7K%7C94fdcdd4bd18ad9ebc8c9e20ed567d406a0cf4ac%7Clin" +
                "&Next=Next");

        ReportResultPage reportResultPage = pageBinder.bind(ReportResultPage.class);
        Optional<String> projectContext = reportResultPage.getProjectNameInSidebar();

        assertThat(projectContext, is(Optional.of(PROJECT_NAME_TP)));
    }

    private ReportsPage goToReportsPage(String projectKey) {
        return jira.goTo(ReportsPage.class, projectKey);
    }

    private AverageAgeReportPage goToAverageAgeReport(ReportsPage reportsPage) {
        return reportsPage.getReportsSection("issue.analysis")
                .getReport("Average Age Report")
                .visit(AverageAgeReportPage.class);
    }

    private FieldTestConfigureReportPage goToFieldTestReport(ReportsPage reportsPage) {
        return reportsPage.getReportsSection("issue.analysis")
                .getReport("Field Test Report")
                .visit(FieldTestConfigureReportPage.class);
    }

}
