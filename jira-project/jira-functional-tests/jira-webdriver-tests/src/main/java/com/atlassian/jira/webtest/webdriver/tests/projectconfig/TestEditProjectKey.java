package com.atlassian.jira.webtest.webdriver.tests.projectconfig;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.ResetData;
import com.atlassian.jira.pageobjects.pages.project.IndexProjectPage;
import com.atlassian.jira.pageobjects.project.summary.EditProjectPageTab;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.atlassian.pageobjects.elements.query.Poller;
import org.hamcrest.collection.IsIterableContainingInAnyOrder;
import org.hamcrest.core.IsCollectionContaining;
import org.junit.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.IsNot.not;

/**
 * @since v6.1
 */
@ResetData
@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PLUGINS, Category.PROJECTS})
public class TestEditProjectKey extends BaseJiraWebTest {

    @Test
    public void testKeyCannotBeUsedAgain() {
        final long projectId = backdoor.project().addProject("Test", "TST", "admin");
        assertThat(backdoor.project().getProjectKeys(projectId), IsCollectionContaining.hasItem("TST"));

        backdoor.project().addProjectKey(projectId, "ABC");

        final long blaProjectId = backdoor.project().addProject("Blah", "BLA", "admin");

        EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(blaProjectId);

        editProjectPageTab.setProjectKey("ABC").submit();

        Map<String, String> errors = editProjectPageTab.getFormErrors();
        assertThat(errors.get("key"), equalTo("Project 'Test' uses this project key."));
    }

    @Test
    public void testKeyCanBeReusedByTheSameProject() {
        final long blahId = backdoor.project().addProject("Blah", "BLA", "admin");

        EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(blahId);

        Poller.waitUntilTrue(editProjectPageTab.isProjectKeyVisible());

        editProjectPageTab.setProjectKey("ABC").submit();

        bindIndexProjectPageAndAcknowledge();
        Project project = backdoor.project().getProject("ABC");

        assertThat(project.name, equalTo("Blah"));
        assertThat(project.key, equalTo("ABC"));

        editProjectPageTab = navigateToEditProjectPageFor(blahId);
        editProjectPageTab.setProjectKey("BLA").submit();

        bindIndexProjectPageAndAcknowledge();
        project = backdoor.project().getProject("ABC");

        assertThat(project.name, equalTo("Blah"));
        assertThat(project.key, equalTo("BLA"));

        assertThat(backdoor.project().getProjectKeys(blahId), IsIterableContainingInAnyOrder.containsInAnyOrder("BLA", "ABC"));
    }

    @Test
    public void testProjectDeleteRemovesKeys() {
        final long projectId = backdoor.project().addProject("Test Key Rename", "TST", "admin");
        backdoor.project().addProjectKey(projectId, "ABC");
        backdoor.project().addProjectKey(projectId, "DEF");

        backdoor.project().deleteProject("TST");
        final long blahId = backdoor.project().addProject("Blah", "BLA", "admin");

        EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(blahId);

        Poller.waitUntilTrue(editProjectPageTab.isProjectKeyVisible());

        editProjectPageTab.setProjectKey("ABC").submit();

        bindIndexProjectPageAndAcknowledge();
        Project project = backdoor.project().getProject("ABC");
        assertThat(project.name, equalTo("Blah"));

        editProjectPageTab = navigateToEditProjectPageFor(blahId);
        editProjectPageTab.setProjectKey("DEF").submit();

        bindIndexProjectPageAndAcknowledge();
        project = backdoor.project().getProject("ABC");
        assertThat(project.name, equalTo("Blah"));

        assertThat(backdoor.project().getProjectKeys(blahId), IsIterableContainingInAnyOrder.containsInAnyOrder("BLA", "DEF", "ABC"));
    }

    @Test
    public void testCanChangeAvatarWhileEditingKey() {
        final String projectKey = "TST";
        final long testProjectId = backdoor.project().addProject("Test Key Rename", projectKey, "admin");
        Project originalProject = backdoor.project().getProject(projectKey);

        EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(testProjectId);
        editProjectPageTab.setProjectKey("T").submit();

        editProjectPageTab.chooseAvatarOption("10001");
        editProjectPageTab.setProjectKey("TSt").submit();

        Project updatedProject = backdoor.project().getProject(projectKey);

        assertThat(updatedProject.avatarUrls, not(equalTo(originalProject.avatarUrls)));
    }

    @Test
    public void testRenameKeyNotPossibleIfProjectIsReindexed() throws Exception {
        long testProjectId = backdoor.project().addProject("Test Key Rename", "TST", "admin");
        backdoor.issues().createIssue("TST", "Test issue for indexing and blocking on barrier");

        backdoor.barrier().raiseBarrierAndRun("backgroundReindex", () -> {
            EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(testProjectId);
            editProjectPageTab.setProjectKey("NEW").submit();

            editProjectPageTab = navigateToEditProjectPageFor(testProjectId);
            editProjectPageTab.setProjectKey("NEWER").submit();

            Map<String, String> errors = editProjectPageTab.getFormErrors();
            assertThat(errors.get("key"), equalTo("Changing key is unavailable while other indexing operations are in progress."));
            editProjectPageTab.setProjectKey("NEW").submit();
        });
        backdoor.indexing().getProjectIndexingProgress(testProjectId).waitForCompletion();
        navigateToIndexProjectPageAndAcknowledge(testProjectId);
    }


    @Test
    public void testRenameKeyWhileBackgroundReindex() throws Exception {
        final long testProjectId = backdoor.project().addProject("Test Key Rename", "TST", "admin");
        backdoor.issues().createIssue("TST", "Test issue for indexing and blocking on barrier");

        backdoor.barrier().raiseBarrierAndRun("backgroundReindex", () -> {
            backdoor.indexing().startInBackground();
            EditProjectPageTab editProjectPageTab = navigateToEditProjectPageFor(testProjectId);
            editProjectPageTab.setProjectKey("NEW").submit();

            jira.getPageBinder().bind(IndexProjectPage.class);
        });
        bindIndexProjectPageAndAcknowledge();
        backdoor.indexing().getInBackgroundProgress().waitForCompletion();
    }

    private void bindIndexProjectPageAndAcknowledge() {
        IndexProjectPage indexProjectPage = jira.getPageBinder().bind(IndexProjectPage.class);
        indexProjectPage.acknowledge();
    }

    private EditProjectPageTab navigateToEditProjectPageFor(final Long projectId) {
        return pageBinder.navigateToAndBind(EditProjectPageTab.class, String.valueOf(projectId));
    }

    private void navigateToIndexProjectPageAndAcknowledge(final Long projectId) {
        IndexProjectPage indexProjectPage = jira.getPageBinder().navigateToAndBind(IndexProjectPage.class, projectId, true);
        indexProjectPage.acknowledge();
    }

    private String getText(String key) {
        return backdoor.i18n().getText(key, "en_US");
    }
}
