package com.atlassian.jira.webtest.webdriver;

import com.atlassian.jira.functest.config.ConfigurationChecker;
import com.atlassian.jira.functest.config.xml.FuncTestsXmlResources;
import org.junit.Test;

import java.io.File;

import static junit.framework.TestCase.assertFalse;

/**
 * Test to ensure that Selenium test XMLs are clean.
 *
 * @since v4.0
 */
public class TestWebDriverXmlBackups {

    public static final String WEBDRIVER_TEST_XML_RESOURCES = "xml/webdriver_test_xml_resources";

    @Test
    public void checkXmlData() throws Exception {
        final ConfigurationChecker configurationChecker = ConfigurationChecker.createDefaultChecker(xmlsLocation());
        final ConfigurationChecker.CheckResult checkResult = configurationChecker.check();

        assertFalse("Func Test XML contains errors. Check out https://extranet.atlassian.com/x/GAW7b for details on what to do.\n", checkResult.hasErrors());
    }

    private File xmlsLocation() {
        return FuncTestsXmlResources.getXmlLocation(WEBDRIVER_TEST_XML_RESOURCES);
    }
}
