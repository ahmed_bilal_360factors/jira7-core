package com.atlassian.jira.webtests.ztests.navigator.jql.changehistory;

import com.atlassian.jira.functest.framework.assertions.ChangeHistoryAssertions;
import com.atlassian.jira.webtests.ztests.navigator.AbstractJqlFuncTest;

import java.util.Set;

import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * @since v4.4
 */
@Deprecated
//DEBT-170: this should be moven to jira-func-tests-legacy in a long run
public abstract class AbstractChangeHistoryFuncTest extends AbstractJqlFuncTest {
    @Override
    protected void setUpTest() {
        administration.restoreData("TestChangeHistorySearch.xml");
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    /**
     * Use {@link ChangeHistoryAssertions#assertWasEmptySearchReturnsEmptyValuesUsingEmptyKeyword(String, String...)} instead
     */
    @Deprecated
    protected void assertWasEmptySearchReturnsEmptyValuesUsingEmptyKeyword(String fieldName, String... issueKeys) {
        changeHistoryAssertions.assertWasEmptySearchReturnsEmptyValuesUsingEmptyKeyword(fieldName, issueKeys);
    }

    /**
     * Use {@link ChangeHistoryAssertions#assertWasNotEmptySearchReturnsNotEmptyValuesWithEmptyKeyword(String, String...)} instead
     */
    @Deprecated
    protected void assertWasNotEmptySearchReturnsNotEmptyValuesWithEmptyKeyword(String fieldName, String... issueKeys) {
        changeHistoryAssertions.assertWasNotEmptySearchReturnsNotEmptyValuesWithEmptyKeyword(fieldName, issueKeys);
    }

    protected void assertWasSearchForRenamedConstantFindsOldName(String fieldName, String oldValue, String newValue, String... issueKeys) {
        String jqlQuery = String.format("%s was %s", fieldName, oldValue);
        super.assertSearchWithResults(jqlQuery, issueKeys);
        jqlQuery = String.format("%s was %s", fieldName, newValue);
        super.assertSearchWithResults(jqlQuery, issueKeys);
    }

    protected void assertWasNotEmptySearchReturnsEmptyValuesUsingEmptyAlias(String fieldName, String emptyAlias, String... issueKeys) {
        String jqlQuery = String.format("%s was not %s", fieldName, emptyAlias);
        super.assertSearchWithResults(jqlQuery, issueKeys);
    }

    /**
     * Use {@link ChangeHistoryAssertions#assertWasSearchReturnsExpectedValues(String, String, String...)} instead
     */
    @Deprecated
    protected void assertWasSearchReturnsExpectedValues(String fieldName, String value, String... issueKeys) {
        changeHistoryAssertions.assertWasSearchReturnsExpectedValues(fieldName, value, issueKeys);
    }

    /**
     * Use {@link ChangeHistoryAssertions#assertWasInSearchReturnsExpectedValues(String, Set, String...)} instead
     */
    @Deprecated
    protected void assertWasInSearchReturnsExpectedValues(String fieldName, Set<String> listValues, String... issueKeys) {
        changeHistoryAssertions.assertWasInSearchReturnsExpectedValues(fieldName, listValues, issueKeys);
    }

    /**
     * Use {@link ChangeHistoryAssertions#assertWasNotInSearchReturnsExpectedValues(String, Set, String...)} instead
     */
    @Deprecated
    protected void assertWasNotInSearchReturnsExpectedValues(String fieldName, Set<String> listValues, String... issueKeys) {
        changeHistoryAssertions.assertWasNotInSearchReturnsExpectedValues(fieldName, listValues, issueKeys);
    }

    /**
     * Use {@link ChangeHistoryAssertions#assertWasBySearchReturnsExpectedValues(String, String, String, String...)} instead
     */
    @Deprecated
    protected void assertWasBySearchReturnsExpectedValues(String fieldName, String value, String actioner, String... issueKeys) {
        changeHistoryAssertions.assertWasBySearchReturnsExpectedValues(fieldName, value, actioner, issueKeys);
    }

    /**
     * Use {@link ChangeHistoryAssertions#assertWasBySearchUsingListOperandsReturnsExpectedValues(String, String, Set, String...)} instead
     */
    @Deprecated
    protected void assertWasBySearchUsingListOperandsReturnsExpectedValues(String fieldName, String value, Set<String> actioners, String... issueKeys) {
        changeHistoryAssertions.assertWasBySearchUsingListOperandsReturnsExpectedValues(fieldName, value, actioners, issueKeys);
    }

    /**
     * Use {@link ChangeHistoryAssertions#assertWasDuringSearchReturnsExpectedValues(String, String, String, String, String...)} instead
     */
    @Deprecated
    protected void assertWasDuringSearchReturnsExpectedValues(String fieldName, String value, String from, String to, String... issueKeys) {
        changeHistoryAssertions.assertWasDuringSearchReturnsExpectedValues(fieldName, value, from, to, issueKeys);
    }

    /**
     * Use {@link ChangeHistoryAssertions#assertWasBeforeSearchReturnsExpectedValues(String, String, String, String...)} instead
     */
    @Deprecated
    protected void assertWasBeforeSearchReturnsExpectedValues(String fieldName, String value, String date, String... issueKeys) {
        changeHistoryAssertions.assertWasBeforeSearchReturnsExpectedValues(fieldName, value, date, issueKeys);
    }

    /**
     * Use {@link ChangeHistoryAssertions#assertWasAfterSearchReturnsExpectedValues(String, String, String, String...)} instead
     */
    @Deprecated
    protected void assertWasAfterSearchReturnsExpectedValues(String fieldName, String value, String date, String... issueKeys) {
        changeHistoryAssertions.assertWasAfterSearchReturnsExpectedValues(fieldName, value, date, issueKeys);
    }

    /**
     * Use {@link ChangeHistoryAssertions#assertWasOnSearchReturnsExpectedValues(String, String, String, String...)} instead
     */
    @Deprecated
    protected void assertWasOnSearchReturnsExpectedValues(String fieldName, String value, String date, String... issueKeys) {
        changeHistoryAssertions.assertWasOnSearchReturnsExpectedValues(fieldName, value, date, issueKeys);
    }

    /**
     * Use {@link ChangeHistoryAssertions#assertWasInListFunctionReturnsExpectedValues(String, String, String...)} instead
     */
    @Deprecated
    protected void assertWasInListFunctionReturnsExpectedValues(String fieldName, String function, String... issueKeys) {
        changeHistoryAssertions.assertWasInListFunctionReturnsExpectedValues(fieldName, function, issueKeys);
    }

    /**
     * Use {@link ChangeHistoryAssertions#assertInvalidSearchProducesError(String, String, String, String)} instead
     */
    @Deprecated
    protected void assertInvalidSearchProducesError(String fieldName, String value, String whereClause, String error) {
        changeHistoryAssertions.assertInvalidSearchProducesError(fieldName, value, whereClause, error);
    }
}
