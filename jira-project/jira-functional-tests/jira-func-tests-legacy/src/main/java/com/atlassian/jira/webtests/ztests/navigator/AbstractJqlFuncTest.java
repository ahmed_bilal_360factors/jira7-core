package com.atlassian.jira.webtests.ztests.navigator;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.assertions.IssueNavigatorAssertions;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.assertions.JqlAssertions;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import org.junit.Assert;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Some common methods for the tests which are testing Jql.
 *
 * @since v4.0
 */
@Deprecated
//DEBT-170: this should be moven to jira-func-tests-legacy in a long run
public abstract class AbstractJqlFuncTest extends FuncTestCase {

    protected JqlAssertions jqlAssertions = new JqlAssertions(backdoor);

    /**
     * Use {@link JqlAssertions#assertFitsFilterForm(String, IssueNavigatorAssertions.FilterFormParam...)} instead
     */
    @Deprecated
    void assertFitsFilterForm(final String jqlQuery, final IssueNavigatorAssertions.FilterFormParam... formParams) {
        jqlAssertions.assertFitsFilterForm(jqlQuery, formParams);
    }

    /**
     * Use {@link JqlAssertions#assertTooComplex(String)} instead
     */
    @Deprecated
    void assertTooComplex(final String jqlQuery) {
        jqlAssertions.assertTooComplex(jqlQuery);
    }

    /**
     * Use {@link JqlAssertions#assertInvalidContext(String)} instead
     */
    @Deprecated
    void assertInvalidContext(final String jqlQuery) {
        jqlAssertions.assertInvalidContext(jqlQuery);
    }

    /**
     * Use {@link JqlAssertions#assertInvalidValue(String)} instead
     */
    @Deprecated
    void assertInvalidValue(final String jqlQuery) {
        jqlAssertions.assertInvalidValue(jqlQuery);
    }

    void assertFilterFormValue(IssueNavigatorAssertions.FilterFormParam formParam) {
        tester.setWorkingForm("issue-filter");
        assertSameElements(formParam.getValues(), tester.getDialog().getForm().getParameterValues(formParam.getName()));
    }

    private static void assertSameElements(String[] a, String[] b) {
        Set<String> as = (a == null || a.length == 0) ? null : new HashSet<String>(Arrays.asList(a));
        Set<String> bs = (b == null || b.length == 0) ? null : new HashSet<String>(Arrays.asList(b));
        Assert.assertEquals(as, bs);
    }

    void assertEmptyIssues() {
        assertions.getIssueNavigatorAssertions().assertSearchResultsAreEmpty();
    }

    /**
     * Use {@link IssueNavigatorAssertions#assertExactIssuesInResults(String...)} instead
     */
    @Deprecated
    void assertIssues(final String... keys) {
        assertions.getIssueNavigatorAssertions().assertExactIssuesInResults(keys);
    }

    /**
     * Use {@link IssueTableAssertions#assertSearchWithResultsForUser(String, String, String...)} instead
     */
    @Deprecated
    protected void assertSearchWithResultsForUser(String username, String jqlString, String... issueKeys) {
        issueTableAssertions.assertSearchWithResultsForUser(username, jqlString, issueKeys);
    }

    /**
     * Use {@link IssueTableAssertions#assertSearchWithResults(String, String...)} instead
     */
    @Deprecated
    protected void assertSearchWithResults(String jqlString, String... issueKeys) {
        issueTableAssertions.assertSearchWithResults(jqlString, issueKeys);
    }

    /**
     * Use {@link IssueTableAssertions#assertOrderedSearchWithResults(String, String[])} instead
     */
    @Deprecated
    protected void assertOrderedSearchWithResults(String jqlString, String... issueKeys) {
        issueTableAssertions.assertOrderedSearchWithResults(jqlString, issueKeys);
    }

    /**
     * Use {@link IssueTableAssertions#assertSearchWithErrorForUser(String, String, String)} instead
     */
    @Deprecated
    protected void assertSearchWithErrorForUser(String username, String jqlString, String error) {
        issueTableAssertions.assertSearchWithErrorForUser(username, jqlString, error);
    }

    /**
     * Use {@link IssueTableAssertions#assertSearchWithError(String, String)} instead
     */
    @Deprecated
    protected void assertSearchWithError(String jqlString, String error) {
        issueTableAssertions.assertSearchWithError(jqlString, error);
    }

    /**
     * Use {@link IssueTableAssertions#assertSearchWithWarningForUser(String, String, String)} instead
     */
    @Deprecated
    protected void assertSearchWithWarningForUser(String username, String jqlString, String warning) {
        issueTableAssertions.assertSearchWithWarningForUser(username, jqlString, warning);
    }

    /**
     * Use {@link IssueTableAssertions#assertSearchWithWarning(String, String)} instead
     */
    @Deprecated
    protected void assertSearchWithWarning(String jqlString, String warning) {
        issueTableAssertions.assertSearchWithWarning(jqlString, warning);
    }

    protected void assertJqlQueryInTextArea(final String expectedJQL) {
        final XPathLocator locator = new XPathLocator(tester, "//textarea[@id='jqltext']");
        textAssertions.assertTextPresent(locator, expectedJQL);
    }
}
