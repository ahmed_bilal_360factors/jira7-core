package com.atlassian.jira.functest.framework.helper;


import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.FuncTestWebClientListener;
import com.atlassian.jira.webtests.JIRAWebTest;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import junit.framework.TestCase;
import net.sourceforge.jwebunit.WebTester;

/**
 *
 * @since v7.1
 */
//DEBT-170: this should be moven to jira-func-tests-legacy in a long run
public class DataDumpHelper {

    public static WebTester getTester(final TestCase testCase) {
        if (testCase instanceof JIRAWebTest) {
            return ((JIRAWebTest) testCase).getTester();
        }
        if (testCase instanceof FuncTestCase) {
            return ((FuncTestCase) testCase).getTester();
        }
        throw new UnsupportedOperationException("We dont supported this class of TestCase " + testCase.getClass().getName() + " : " + testCase.getName());
    }

    public static JIRAEnvironmentData getEnviromentData(final TestCase testCase) {
        if (testCase instanceof JIRAWebTest) {
            return ((JIRAWebTest) testCase).getEnvironmentData();
        }
        if (testCase instanceof FuncTestCase) {
            return ((FuncTestCase) testCase).getEnvironmentData();
        }
        throw new UnsupportedOperationException("We dont supported this class of TestCase " + testCase.getClass().getName() + " : " + testCase.getName());
    }

    public static FuncTestWebClientListener getFuncTestWebClientListener(final TestCase testCase) {
        if (testCase instanceof JIRAWebTest) {
            return ((JIRAWebTest) testCase).getWebClientListener();
        }
        if (testCase instanceof FuncTestCase) {
            return ((FuncTestCase) testCase).getWebClientListener();
        }
        throw new UnsupportedOperationException("We dont supported this class of TestCase " + testCase.getClass().getName() + " : " + testCase.getName());
    }
}
