package com.atlassian.jira.webtests.ztests.crowd.embedded;

import com.atlassian.crowd.acceptance.tests.rest.service.SearchResourceTest;
import com.atlassian.jira.webtests.util.EnvironmentAware;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

/**
 * Acceptance tests for the Crowd search resource.
 *
 * @since v4.3
 */
public class TestCrowdSearchResource extends SearchResourceTest implements EnvironmentAware {
    public TestCrowdSearchResource(String name) {
        super(name, new CrowdEmbeddedServer().usingXmlBackup(CrowdEmbeddedServer.XML_BACKUP));
    }

    @Override
    public void setEnvironmentData(JIRAEnvironmentData environmentData) {
        setRestServer(new CrowdEmbeddedServer(environmentData).usingXmlBackup(CrowdEmbeddedServer.XML_BACKUP));
    }

    @Override
    public void testGetUserNames_Aliases() {
        // DISABLED because the functionality is not supported in JIRA
    }

    @Override
    public void testGetUserNames_AliasesIgnoreUsernameWhenAliasExists() {
        // DISABLED because the functionality is not supported in JIRA
    }

    @Override
    public void testGetUserNames_AliasesAll() {
        // DISABLED because the functionality is not supported in JIRA
    }


    // JIRA has a few extra groups in its version of the test data...
    @Override
    protected Set<String> getAllGroupnames() {
        final ImmutableSet.Builder<String> set = ImmutableSet.builder();
        set.addAll(super.getAllGroupnames());
        set.add("jira-administrators");
        set.add("jira-developers");
        set.add("jira-users");
        set.add("UpperCaseGroupName");
        return set.build();
    }
}
