package com.atlassian.jira.webtests.ztests.crowd.embedded;

import com.atlassian.jira.webtests.cargo.CargoTestHarness;
import junit.framework.Test;
import junit.framework.TestSuite;

import java.io.IOException;

public class CrowdFuncTestHarness extends TestSuite {

    public static Test suite() throws IOException {
        return CargoTestHarness.suite(CrowdTestSuite.class);
    }

    public static class CrowdTestSuite extends TestSuite {
        public CrowdTestSuite() {
            super(TestCrowdAuthenticationResource.class, TestCrowdGroupsResource.class, TestCrowdSearchResource.class, TestCrowdUsersResource.class);
        }

        public static Test suite() {
            return new CrowdTestSuite();
        }
    }
}
