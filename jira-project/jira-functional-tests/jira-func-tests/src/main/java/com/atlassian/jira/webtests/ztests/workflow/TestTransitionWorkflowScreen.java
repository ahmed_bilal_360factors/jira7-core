package com.atlassian.jira.webtests.ztests.workflow;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.CommentAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;

/**
 * Tests related to the transition workflow screen.
 */
@WebTest({Category.FUNC_TEST, Category.WORKFLOW})
@LoginAs(user = ADMIN_USERNAME)
public class TestTransitionWorkflowScreen extends BaseJiraFuncTest {

    public static final String DEVMAN_USERNAME = "devman";
    public static final String ADMINMAN_USERNAME = "adminman";
    private static final String COMMENT_VALUE = "i should survive the error";
    private static final String COMMENT_1 = "This issue is resolved now.";
    private static final String COMMENT_2 = "Viewable by developers group.";
    private static final String COMMENT_3 = "Viewable by Developers role.";
    private static final String COMMENT_4 = "Viewable by jira-administrators role.";
    private static final String COMMENT_5 = "Viewable by Administrators role.";
    private static final String HSP_1 = "HSP-1";
    private static final String HSP_2 = "HSP-2";
    private static final String HSP_3 = "HSP-3";
    private static final String HSP_4 = "HSP-4";
    private static final String HSP_5 = "HSP-5";
    @Inject
    private FuncTestLogger logger;

    @Test
    @Restore("TestTransitionWorkflowScreen.xml")
    public void testCommentValueRemainsOnError() {
        navigation.comment().enableCommentGroupVisibility(Boolean.TRUE);

        final String issueKey = HSP_1;
        navigation.issue().gotoIssue(issueKey);
        tester.clickLinkWithText("Resolve Issue");
        tester.setWorkingForm("issue-workflow-transition");
        tester.setFormElement("summary", "");
        tester.setFormElement("comment", COMMENT_VALUE);
        tester.selectOption("commentLevel", "jira-developers"); //visible to Users role
        tester.submit("Transition");

        tester.assertTextPresent("You must specify a summary of the issue");
        tester.assertTextPresent(COMMENT_VALUE); //comment value retained?

        tester.setFormElement("summary", "summary of resolving issue");
        tester.submit("Transition");

        //make sure the comment level value survived the error and so now fred can't see it
        final CommentAssertions commentAssertions = assertions.comments(COMMENT_VALUE);
        commentAssertions.areNotVisibleTo(FRED_USERNAME, issueKey);
        //admin should be able to see it
        commentAssertions.areVisibleTo(ADMIN_USERNAME, issueKey);
    }

    @Test
    @Restore("TestTransitionWorkflowScreen.xml")
    public void testTransitionIssueForTheCommentVisibility() {
        navigation.comment().enableCommentGroupVisibility(Boolean.TRUE);
        // resolve issue with comment visible to all users
        navigation.issueNavigator().displayAllIssues();
        tester.clickLinkWithText(HSP_1);
        tester.clickLink("action_id_5");
        tester.setWorkingForm("issue-workflow-transition");
        tester.setFormElement("comment", COMMENT_1);
        tester.submit("Transition");

        // resolve issue with comment visible only to jira-developers group
        navigation.issueNavigator().displayAllIssues();
        tester.clickLinkWithText(HSP_2);
        tester.clickLink("action_id_5");
        tester.setWorkingForm("issue-workflow-transition");
        tester.setFormElement("comment", COMMENT_2);
        tester.selectOption("commentLevel", "jira-developers");
        tester.submit("Transition");

        // resolve issue with comment visible only to Developers role
        navigation.issueNavigator().displayAllIssues();
        tester.clickLinkWithText(HSP_3);
        tester.clickLink("action_id_5");
        tester.setWorkingForm("issue-workflow-transition");
        tester.setFormElement("comment", COMMENT_3);
        tester.selectOption("commentLevel", "Developers");
        tester.submit("Transition");

        // resolve issue with comment visible only to Administrators role
        navigation.issueNavigator().displayAllIssues();
        tester.clickLinkWithText(HSP_4);
        tester.clickLink("action_id_5");
        tester.setWorkingForm("issue-workflow-transition");
        tester.setFormElement("comment", COMMENT_4);
        tester.selectOption("commentLevel", "Administrators");
        tester.submit("Transition");

        // resolve issue with comment visible only to jira-administrators role
        navigation.issueNavigator().displayAllIssues();
        tester.clickLinkWithText(HSP_5);
        tester.clickLink("action_id_5");
        tester.setWorkingForm("issue-workflow-transition");
        tester.setFormElement("comment", COMMENT_5);
        tester.selectOption("commentLevel", "jira-administrators");
        tester.submit("Transition");

        // verify that Fred can see general comment but not others as he is not in the visibility groups
        assertions.comments(COMMENT_1).areVisibleTo(FRED_USERNAME, HSP_1);
        assertions.comments(COMMENT_2).areNotVisibleTo(FRED_USERNAME, HSP_2);
        assertions.comments(COMMENT_3).areNotVisibleTo(FRED_USERNAME, HSP_3);

        assertions.comments(COMMENT_1).areVisibleTo(ADMIN_USERNAME, HSP_1);
        assertions.comments(COMMENT_2).areVisibleTo(ADMIN_USERNAME, HSP_2);
        assertions.comments(COMMENT_3).areVisibleTo(ADMIN_USERNAME, HSP_3);

        // verify devman can only see dev resolve comments and not admin resolve comments
        assertions.comments(COMMENT_2).areVisibleTo(DEVMAN_USERNAME, HSP_2);
        assertions.comments(COMMENT_3).areVisibleTo(DEVMAN_USERNAME, HSP_3);

        assertions.comments(COMMENT_4).areNotVisibleTo(DEVMAN_USERNAME, HSP_4);
        assertions.comments(COMMENT_5).areNotVisibleTo(DEVMAN_USERNAME, HSP_5);

        // verify adminman can only see admin resolve comment and not dev resolve comments
        assertions.comments(COMMENT_2).areNotVisibleTo(ADMINMAN_USERNAME, HSP_2);
        assertions.comments(COMMENT_3).areNotVisibleTo(ADMINMAN_USERNAME, HSP_3);

        assertions.comments(COMMENT_4).areVisibleTo(ADMINMAN_USERNAME, HSP_4);
        assertions.comments(COMMENT_5).areVisibleTo(ADMINMAN_USERNAME, HSP_5);
    }

    @Test
    @Restore("TestTransitionDescriptionEmpty.xml")
    public void testTransitionWithBlankDescription() throws Exception {
        navigation.issue().gotoIssue("HMS-1");
        tester.assertTextPresent("Start Progress - a description");
        tester.assertTextNotPresent("Resolve Issue - ");
    }

    @Test
    @Restore("TestWorkflowComments.xml")
    public void testCommentWhileTransitioningIssue() {
        logger.log("testTransitionFromStatusThatAllowsCommentsToOneThatDoes");
        testCommentWhileTransitioning("Open", "TEST-1");
        logger.log("testTransitionFromStatusThatAllowsCommentsToOneThatDoesNot");
        testCommentWhileTransitioning("Start", "TEST-2");
        logger.log("testTransitionFromStatusThatDoesNotAllowCommentsToOneThatDoes");
        testCommentWhileTransitioning("Stop Progress", "TEST-4");
        logger.log("testTransitionFromStatusThatDoesNotAllowCommentsToOneThatDoesNot");
        transitionFromStatusThatDoesNotAllowCommentsToOneThatDoesNot();
    }

    private void transitionFromStatusThatDoesNotAllowCommentsToOneThatDoesNot() {
        navigation.issue().gotoIssue("TEST-3");
        tester.clickLinkWithText("Done");
        tester.setWorkingForm("issue-workflow-transition");
        // The comment text area should not be visible for any user to add a comment.
        tester.assertFormElementNotPresent("comment");
    }

    private void testCommentWhileTransitioning(final String status, final String issueKey) {
        navigation.issue().gotoIssue(issueKey);
        tester.clickLinkWithText(status);
        tester.setWorkingForm("issue-workflow-transition");
        tester.setFormElement("comment", COMMENT_VALUE);
        tester.submit("Transition");

        // Comment should be added to the issue.
        assertions.comments(COMMENT_VALUE).areVisibleTo(ADMIN_USERNAME, issueKey);
    }

}
