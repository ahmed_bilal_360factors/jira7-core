package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.util.issue.IssueInlineEdit;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Test inline editing of fields from the view issue page.
 * <p/>
 * Note: this test should ideally be moved to the issue-nav plugin. The problem is that the build plan for the issue-nav
 * plugin master doesn't compile against jira master.
 *
 * @since v5.1
 */
@WebTest({Category.FUNC_TEST, Category.ISSUES})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestInlineEditIssueFields extends BaseJiraFuncTest {


    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Before
    public void setUpTest() {
        administration.restoreData("TestEditIssueVersion.xml");
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void testInlineEditIssueType() throws Exception {
        testInlineEditField("issuetype", "type-val", "Bug", "4", "Improvement");
    }

    @Test
    public void testInlineEditPriority() throws Exception {
        testInlineEditField("priority", "priority-val", "Major", "4", "Minor");
    }

    @Test
    public void testInlineEditDescription() throws Exception {
        testInlineEditField("description", "description-val", "oneoneoneoneoneoneoneoneone", "blablabla", "blablabla");
    }

    private void testInlineEditField(String fieldName, String fieldId, String oldValue, String newFormValue, String newTextValue)
            throws Exception {
        navigation.issue().gotoIssue("MK-1");
        assertions.assertNodeByIdHasText(fieldId, oldValue);

        IssueInlineEdit inlineEdit = new IssueInlineEdit(locator, tester, environmentData);
        inlineEdit.inlineEditField("10020", fieldName, newFormValue);

        // Refresh the page
        navigation.issue().gotoIssue("MK-1");
        // Make sure the field has the new value
        assertions.assertNodeByIdDoesNotHaveText(fieldId, oldValue);
        assertions.assertNodeByIdHasText(fieldId, newTextValue);
    }
}
