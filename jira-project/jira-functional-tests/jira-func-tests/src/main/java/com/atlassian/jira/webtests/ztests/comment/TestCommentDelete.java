package com.atlassian.jira.webtests.ztests.comment;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.TRANSIION_NAME_CLOSE;

/**
 *
 */
@WebTest({Category.FUNC_TEST, Category.COMMENTS})
@LoginAs(user = ADMIN_USERNAME)
public class TestCommentDelete extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        administration.restoreData("TestCommentDelete.xml");
    }

    @Test
    public void testDeleteOwnCommentWithOwnPermission() {
        navigation.logout();
        // Login as simple user (with "Delete Own Comment" permission)
        navigation.login("detkin", "detkin");

        // Attempt to delete own comment
        navigation.issue().gotoIssue("TST-1");
        tester.clickLink("delete_comment_10020");

        tester.submit("Delete");

        // Assert comment has been deleted
        tester.assertLinkNotPresent("delete_comment_10020");
    }

    @Test
    public void testDeleteCommentWithAllPermission() {
        navigation.issue().gotoIssue("TST-1");
        tester.clickLink("delete_comment_10020");

        tester.submit("Delete");

        // Assert comment has been deleted
        tester.assertLinkNotPresent("delete_comment_10020");
    }

    @Test
    public void testDeleteOthersCommentWithOwnPermission() {
        // Login as another user (with the "Delete Own Attachment" permission) and assert that the delete link is hidden
        navigation.logout();
        navigation.login("barney", "barney");
        navigation.issue().gotoIssue("TST-1");
        // Assert comment has been deleted
        tester.assertLinkNotPresent("delete_comment_10020");
    }

    @Test
    public void testDeleteCommentLinkNotAvailableWithNonEditableWorkflowState() {
        navigation.issue().gotoIssue("TST-1");
        // Close the issue
        tester.clickLinkWithText(TRANSIION_NAME_CLOSE);
        tester.setWorkingForm("issue-workflow-transition");
        tester.submit("Transition");

        // We should not be able to delete the comment
        tester.assertLinkNotPresent("delete_comment_10020");
    }

    @Test
    public void testDeleteCommentReindexesIssue() {
        // Check to see if returned in search
        navigation.issueNavigator().createSearch("comment ~ \"Unique Comment\"");
        tester.assertTextPresent("TST-1");

        // Delete the comment
        navigation.issue().gotoIssue("TST-1");
        tester.clickLink("delete_comment_10020");
        tester.submit("Delete");

        // Check to see that the issue is not returned in the search (i.e. it has reindexed)
        navigation.issueNavigator().createSearch("comment ~ \"Unique Comment\"");
        tester.assertTextNotPresent("TST-1");
        assertions.assertNodeByIdDoesNotExist("issuetable");
    }

    @Test
    public void testDeleteCommentWithNoPermissions() {
        // Remove the permission to delete a comment
        backdoor.permissionSchemes().removeProjectRolePermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.DELETE_ALL_COMMENTS, 10002);
        backdoor.permissionSchemes().removeProjectRolePermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.DELETE_OWN_COMMENTS, 10000);

        // Jump to the page
        tester.gotoPage("/secure/DeleteComment!default.jspa?id=10000&commentId=10020");
        tester.assertTitleEquals("Error - Your Company JIRA");
        textAssertions.assertTextPresent(new CssLocator(tester, "#content .error"), "You do not have the permission for this comment.");
    }

    @Test
    public void testDeleteCommentWithNonEditableWorkflowState() {
        // Close the issue
        navigation.issue().gotoIssue("TST-1");
        tester.clickLinkWithText(TRANSIION_NAME_CLOSE);
        tester.setWorkingForm("issue-workflow-transition");
        tester.submit("Transition");

        // Jump to the page
        tester.gotoPage("/secure/DeleteComment!default.jspa?id=10000&commentId=10020");

        tester.assertTitleEquals("Error - Your Company JIRA");
        textAssertions.assertTextPresent(new CssLocator(tester, "#content .error"), "You do not have the permission for this comment.");
    }

    @Test
    public void testOneCanNotDeleteCommentThatOneCanNotSee() {
        // admin adds comment on 'Developers' role level
        navigation.issue().gotoIssue("TST-1");
        navigation.issue().addComment("TST-1", "This comment is visible by developers only!", "Developers");
        tester.assertTextPresent("This comment is visible by developers only!");

        // add user 'barney' to the administrator group
        administration.usersAndGroups().addUserToGroup("barney", "jira-administrators");

        // login as 'barney' user
        navigation.logout();
        navigation.login("barney", "barney");

        // go to 'TST-1' issue and assert that cannot see the comment
        navigation.issue().gotoIssue("TST-1");
        tester.assertTextNotPresent("This comment is visible by developers only!");

        // now try to delete the comment
        tester.gotoPage("/secure/DeleteComment!default.jspa?id=10000&commentId=10030");
        tester.assertTitleEquals("Error - Your Company JIRA");
        textAssertions.assertTextPresent(new CssLocator(tester, "#content .error"), "You do not have the permission for this comment.");

        // now try to delete the comment
        tester.gotoPage("/secure/DeleteComment!default.jspa?id=10000&commentId=100301"); // invalid comment id
        tester.assertTitleEquals("Error - Your Company JIRA");
        textAssertions.assertTextPresent(new CssLocator(tester, "#content .error"), "You do not have the permission for this comment.");

        // login as 'admin' user
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);

        // go to 'TST-1' issue and assert that comment is still there
        navigation.issue().gotoIssue("TST-1");
        tester.assertTextPresent("This comment is visible by developers only!");
    }

    @Test
    public void testOneCanNotEditCommentThatOneCanNotSee() {
        // admin adds comment on 'Developers' role level
        navigation.issue().gotoIssue("TST-1");
        navigation.issue().addComment("TST-1", "This comment is visible by developers only!", "Developers");
        tester.assertTextPresent("This comment is visible by developers only!");

        // add user 'barney' to the administrator group
        administration.usersAndGroups().addUserToGroup("barney", "jira-administrators");

        // login as 'barney' user
        navigation.logout();
        navigation.login("barney", "barney");

        // go to 'TST-1' issue and assert that cannot see the comment
        navigation.issue().gotoIssue("TST-1");
        tester.assertTextNotPresent("This comment is visible by developers only!");

        // now try to edit the comment
        tester.gotoPage("/secure/EditComment!default.jspa?id=10000&commentId=10030");
        tester.assertTitleEquals("Error - Your Company JIRA");
        textAssertions.assertTextPresent(new CssLocator(tester, "#content .error"), "You do not have the permission for this comment.");


        // now try to edit the comment
        tester.gotoPage("/secure/EditComment!default.jspa?id=10000&commentId=100301"); // invalid comment id
        tester.assertTitleEquals("Error - Your Company JIRA");
        textAssertions.assertTextPresent(new CssLocator(tester, "#content .error"), "You do not have the permission for this comment.");


        // login as 'admin' user
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);

        // go to 'TST-1' issue and assert that comment is still there
        navigation.issue().gotoIssue("TST-1");
        tester.assertTextPresent("This comment is visible by developers only!");
    }

}
