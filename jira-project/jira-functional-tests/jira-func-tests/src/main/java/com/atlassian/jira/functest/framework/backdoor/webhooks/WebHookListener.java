package com.atlassian.jira.functest.framework.backdoor.webhooks;

import java.util.Arrays;

public class WebHookListener {
    private final String name;
    private final String url;
    private final String[] events;
    private final String filter;
    private final boolean excludeIssueDetails;
    private final Integer id;

    public WebHookListener(String name, String url, String[] events, String filter, boolean excludeIssueDetails, Integer id) {
        this.name = name;
        this.url = url;
        this.events = events;
        this.filter = filter;
        this.excludeIssueDetails = excludeIssueDetails;
        this.id = id;
    }

    @Override
    public String toString() {
        return "WebHookResponseTester{" +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", events=" + Arrays.toString(events) +
                ", filter='" + filter + '\'' +
                ", excludeIssueDetails=" + excludeIssueDetails +
                ", id=" + id +
                '}';
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String[] getEvents() {
        return events;
    }

    public String getFilter() {
        return filter;
    }

    public boolean isExcludeIssueDetails() {
        return excludeIssueDetails;
    }

    public Integer getId() {
        return id;
    }
}

