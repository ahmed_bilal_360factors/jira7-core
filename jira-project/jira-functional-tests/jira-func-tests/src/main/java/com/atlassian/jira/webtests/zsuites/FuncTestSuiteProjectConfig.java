package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.projectconfig.TestBackToProjectConfigLink;
import com.atlassian.jira.webtests.ztests.projectconfig.TestProjectDraftWorkflowSchemeResource;
import com.atlassian.jira.webtests.ztests.projectconfig.TestProjectWorkflowSchemeResource;
import com.atlassian.jira.webtests.ztests.projectconfig.TestRolesResource;
import com.atlassian.jira.webtests.ztests.projectconfig.TestUserFormat;
import com.atlassian.jira.webtests.ztests.projectconfig.TestWorkflowSchemeEditorResource;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A FuncTestSuite for Project config plugin
 *
 * @since v7.3
 */
public class FuncTestSuiteProjectConfig {

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestBackToProjectConfigLink.class)
                .add(TestProjectDraftWorkflowSchemeResource.class)
                .add(TestProjectWorkflowSchemeResource.class)
                .add(TestRolesResource.class)
                .add(TestUserFormat.class)
                .add(TestWorkflowSchemeEditorResource.class)
                .build();
    }
}
