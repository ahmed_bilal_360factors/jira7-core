/**
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

package com.atlassian.jira.webtests.ztests.navigator;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.AbstractTestIssueNavigatorView;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@WebTest({Category.FUNC_TEST, Category.ISSUE_NAVIGATOR, Category.ISSUES})
public class TestIssueNavigatorWordView extends AbstractTestIssueNavigatorView {

    @Test
    @Restore("TestSearchRequestViewNonAsciiSearchName.xml")
    @LoginAs(user = "admin")
    public void testWordFilenameWithNonAsciiCharacters() {
        final String encodedFilename = "%D0%B5%D1%81%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F+%28jWebTest+JIRA+installation%29.doc";
        final String oldUserAgent = tester.getDialog().getWebClient().getClientProperties().getUserAgent();

        try {
            logger.log("Issue Navigator: Test that the word view generates the correct filename when the search request has non-ASCII characters");

            // first test "IE"
            tester.getDialog().getWebClient().getClientProperties().setUserAgent("Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)");
            tester.gotoPage("/sr/jira.issueviews:searchrequest-word/10000/SearchRequest-10000.doc?tempMax=1000");
            String contentDisposition = tester.getDialog().getResponse().getHeaderField("content-disposition");
            assertFalse(contentDisposition.contains("filename*=UTF-8''" + encodedFilename));
            assertTrue("Expected the content disposition to contain '" + encodedFilename + "' but got '" + contentDisposition + "'!",
                    contentDisposition.contains("filename=\"" + encodedFilename + "\""));

            // next test "Mozilla"
            tester.getDialog().getWebClient().getClientProperties().setUserAgent("Mozilla/5.001 (windows; U; NT4.0; en-US; rv:1.0) Gecko/25250101");
            tester.gotoPage("/sr/jira.issueviews:searchrequest-word/10000/SearchRequest-10000.doc?tempMax=1000");
            contentDisposition = tester.getDialog().getResponse().getHeaderField("content-disposition");
            assertTrue(contentDisposition.contains("filename*=UTF-8''" + encodedFilename));
            assertFalse(contentDisposition.contains("filename=\"" + encodedFilename + "\""));
        } finally {
            // restore old user agent
            tester.getDialog().getWebClient().getClientProperties().setUserAgent(oldUserAgent);
        }
    }


}
