package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Streams related tests.
 *
 * @since v6.4
 */
public class FuncTestSuiteStreams extends FuncTestSuite {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.bundledplugins2.streams", true))
                .build();
    }
}
