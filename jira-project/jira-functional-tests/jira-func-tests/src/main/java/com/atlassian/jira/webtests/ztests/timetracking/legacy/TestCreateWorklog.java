package com.atlassian.jira.webtests.ztests.timetracking.legacy;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.Parser;
import com.atlassian.jira.functest.framework.admin.GeneralConfiguration;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.changehistory.ChangeHistoryList;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.framework.util.env.EnvironmentUtils;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FORMAT_DAYS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FORMAT_HOURS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FORMAT_PRETTY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_DEV_GROUP;
import static com.atlassian.jira.permission.ProjectPermissions.WORK_ON_ISSUES;

/**
 * Functional tests for log work
 */
@WebTest({Category.FUNC_TEST, Category.TIME_TRACKING, Category.WORKLOGS})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestLogWork.xml")
public class TestCreateWorklog extends BaseJiraFuncTest {

    private static final String HSP_1 = "HSP-1";

    private static final String WORK_LOG_COMMENT_1 = "This is a comment generated for a first work log.";
    private static final String WORK_LOG_COMMENT_2 = "This is a comment generated for a second work log.";
    private static final String ROLE_DEVELOPERS = "Developers";
    private static final String GROUP_ADMINISTRATORS = "jira-administrators";
    private String timeFormat;

    @Inject
    private Parser parse;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Inject
    private EnvironmentUtils environmentUtils;

    @Before
    public void setUp() {
        administration.generalConfiguration().setCommentVisibility(GeneralConfiguration.CommentVisibility.GROUPS_PROJECT_ROLES);
    }

    @Test
    public void testWorklogNoPermToCreate() {
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, WORK_ON_ISSUES, JIRA_DEV_GROUP);
        tester.gotoPage("/secure/CreateWorklog!default.jspa?id=10000");
        tester.assertTextPresent("It seems that you have tried to perform an operation which you are not permitted to perform");
    }

    @Test
    public void testLogWorkNoteCorrect() {
        tester.gotoPage("/secure/ShowTimeTrackingHelp.jspa?decorator=popup#LogWork");
        textAssertions.assertTextPresent("Your current conversion rates are 1w = 7d and 1d = 24h.");

        // turn it off so we can re-enable with hours as the default
        administration.timeTracking().disable();

        // check that by default we'll end up with Minutes to preserve old behaviour
        navigation.gotoAdminSection(Navigation.AdminSection.TIMETRACKING);
        tester.setFormElement("hoursPerDay", "6");
        tester.setFormElement("daysPerWeek", "5");
        tester.submit("Activate");

        tester.gotoPage("/secure/ShowTimeTrackingHelp.jspa?decorator=popup#LogWork");
        textAssertions.assertTextPresent("Your current conversion rates are 1w = 5d and 1d = 6h.");
    }

    /**
     * Errors should be reported on the "Time Spent" field, "Start Date" field and hideable "new estimate" field if
     * "Set estimated time remaining" radio button has been selected, if they are empty.
     */
    @Test
    public void testMandatoryFields() {
        navigation.issue().viewIssue(HSP_1);
        tester.clickLink("log-work");
        tester.setFormElement("timeLogged", "");
        tester.setFormElement("startDate", "");
        tester.setFormElement("newEstimate", "");
        tester.checkCheckbox("adjustEstimate", "new");
        tester.submit();
        textAssertions.assertTextPresent("You must indicate the time spent working.");
        textAssertions.assertTextPresent("You must specify a date on which the work occurred.");
        textAssertions.assertTextPresent("You must supply a valid new estimate.");
    }

    /**
     * Errors should be reported if the "Time Spent" field or the hideable "new estimate" field (if "Set estimated time
     * remaining" radio button has been selected) contains an invalid duration string.
     * <p/>
     * Valid durations strings include "4d 6h", "30m", etc.
     */
    @Test
    public void testInvalidFormattedDurationFields() {
        navigation.issue().viewIssue(HSP_1);
        getTester().clickLink("log-work");
        getTester().setFormElement("timeLogged", "16 Candles");
        getTester().setFormElement("startDate", "18/Jun/07 10:49 AM"); //should be valid
        getTester().setFormElement("newEstimate", "Six Days, Seven Nights");
        getTester().checkCheckbox("adjustEstimate", "new");
        getTester().submit();
        textAssertions.assertTextPresent("Invalid time duration entered.");
        textAssertions.assertTextPresent("Invalid new estimate entered.");
    }

    @Test
    public void testNegativeDuration() {
        navigation.issue().logWork(HSP_1, "-2h");
        textAssertions.assertTextPresent("Invalid time duration entered.");
    }

    @Test
    public void testBadFractionDuration() {
        final String[] badDurations = new String[]{
//                "2.h", // decimal in wrong place
                "5.3756h", // can't be represented as "minutes" without losing accuracy
//                "0.5m", // can't go below 1 minute
        };
        for (String badDuration : badDurations) {
            navigation.issue().logWork(HSP_1, badDuration);
            textAssertions.assertTextPresent("Invalid time duration entered.");
        }
    }

    @Test
    public void testGoodFractionDuration() throws Exception {
        navigation.issue().logWork(HSP_1, "2.5h");

        assertTextSequence(new String[]{"Time Spent", "2 hours, 30 minutes"});

        navigation.issue().logWork(HSP_1, "2.5h 30m");
        assertTextSequence(new String[]{"Time Spent", "5 hours, 30 minutes"});

        navigation.issue().logWork(HSP_1, "1.5d");
        assertTextSequence(new String[]{"Time Spent", "1 day, 17 hours, 30 minutes"});
    }


    /**
     * Errors should be reported if the "Time Spent" field if the time spent is zero.
     */
    @Test
    public void testInvalidTimeSpentZero() {
        navigation.issue().viewIssue(HSP_1);
        getTester().clickLink("log-work");
        getTester().setFormElement("timeLogged", "0");
        getTester().setFormElement("startDate", "18/Jun/07 10:49 AM"); //should be valid
        getTester().submit();
        textAssertions.assertTextPresent("Time Spent can not be zero.");
    }

    @Test
    public void testInvalidStartDateField() {
        navigation.issue().viewIssue(HSP_1);
        getTester().clickLink("log-work");
        getTester().setFormElement("timeLogged", "4h");
        getTester().setFormElement("startDate", "The Day After Tomorrow"); //should be valid
        getTester().checkCheckbox("adjustEstimate", "leave");
        getTester().submit();
        textAssertions.assertTextPresent("You must specify a date on which the work occurred.");
    }

    @Test
    public void testAutoAdjustEstimate() {
        navigation.issue().viewIssue(HSP_1);
        getTester().clickLink("edit-issue");
        getTester().setFormElement("timetracking", "4d");
        getTester().submit("Update");
        getTester().clickLink("log-work");
        getTester().setFormElement("timeLogged", "4h 30m");
        getTester().submit("Log");
        assertTextSequence(new String[]{"Original Estimate", "4 days", "Remaining Estimate", "3 days, 19 hours, 30 minutes", "Time Spent", "4 hours, 30 minutes"});
    }

    @Test
    public void testNewEstimate() {
        navigation.issue().viewIssue(HSP_1);
        getTester().clickLink("edit-issue");
        getTester().setFormElement("timetracking", "4d");
        getTester().submit("Update");
        getTester().clickLink("log-work");
        getTester().setFormElement("timeLogged", "4h 30m");
        getTester().checkCheckbox("adjustEstimate", "new");
        getTester().setFormElement("newEstimate", "1d 5h");
        getTester().submit("Log");
        assertTextSequence(new String[]{"Original Estimate", "4 days", "Remaining Estimate", "1 day, 5 hours", "Time Spent", "4 hours, 30 minutes"});
    }

    @Test
    public void testLeaveExistingEstimate() {
        navigation.issue().viewIssue(HSP_1);
        getTester().clickLink("edit-issue");
        getTester().setFormElement("timetracking", "4d");
        getTester().submit("Update");
        getTester().clickLink("log-work");
        getTester().setFormElement("timeLogged", "4h 30m");
        getTester().checkCheckbox("adjustEstimate", "leave");
        getTester().submit("Log");
        assertTextSequence(new String[]{"Original Estimate", "4 days", "Remaining Estimate", "4 days", "Time Spent", "4 hours, 30 minutes"});
    }

    /* --------- Test Group/Role Visibility --------- */

    @Test
    public void testLogWorkVisibleToAll() {
        reconfigureTimetracking(FORMAT_PRETTY);
        execLogWorkVisibleToAll();
    }

    @Test
    public void testLogWorkVisibleToAllDaysTimeFormat() {
        reconfigureTimetracking(FORMAT_DAYS);
        execLogWorkVisibleToAll();
    }

    @Test
    public void testLogWorkVisibleToAllHoursTimeFormat() {
        reconfigureTimetracking(FORMAT_HOURS);
        execLogWorkVisibleToAll();
    }

    private void execLogWorkVisibleToAll() {
        navigation.issue().viewIssue(HSP_1);
        logWorkWithComment(HSP_1, "2d", WORK_LOG_COMMENT_1);
        getTester().clickLinkWithText("Work Log");
        textAssertions.assertTextPresent(WORK_LOG_COMMENT_1);
        if (FORMAT_PRETTY.equals(timeFormat)) {
            textAssertions.assertTextPresent("2 days");
        } else if (FORMAT_DAYS.equals(timeFormat)) {
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Time Spent", "2d");
        } else if (FORMAT_HOURS.equals(timeFormat)) {
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Time Spent", "48h");
        }
    }

    private void logWorkWithComment(String issueKey, String timeLogged, String comment) {
        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("log-work");
        tester.setFormElement("timeLogged", timeLogged);
        tester.setFormElement("comment", comment);
        tester.submit("Log");
    }

    @Test
    public void testLogWorkVisibleToRole() {
        reconfigureTimetracking(FORMAT_PRETTY);
        execLogWorkVisible(ROLE_DEVELOPERS);
    }

    @Test
    public void testLogWorkVisibleToRoleDaysTimeFormat() {
        reconfigureTimetracking(FORMAT_DAYS);
        execLogWorkVisible(ROLE_DEVELOPERS);
    }

    @Test
    public void testLogWorkVisibleToRoleHoursTimeFormat() {
        reconfigureTimetracking(FORMAT_HOURS);
        execLogWorkVisible(ROLE_DEVELOPERS);
    }

    private void assertAdminCannotSeeWorklog() {
        getTester().clickLinkWithText("Work Log");
        textAssertions.assertTextPresent(WORK_LOG_COMMENT_1);
        textAssertions.assertTextPresent(WORK_LOG_COMMENT_2);
        if (FORMAT_PRETTY.equals(timeFormat)) {
            textAssertions.assertTextPresent("2 days"); // first worklog
            textAssertions.assertTextPresent("3 days"); // second worklog
            textAssertions.assertTextPresent("5 days"); // total spent
        } else if (FORMAT_DAYS.equals(timeFormat)) {
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Time Spent", "2d");
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Time Spent", "3d");
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Time Spent", "5d");
        } else if (FORMAT_HOURS.equals(timeFormat)) {
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Time Spent", "48h");
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Time Spent", "72h");
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Time Spent", "120h");
        }
    }

    private void assertFredCannotSeeWorkLog() {
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().viewIssue(HSP_1);
        textAssertions.assertTextNotPresent(WORK_LOG_COMMENT_1);
        textAssertions.assertTextNotPresent(WORK_LOG_COMMENT_2);
        if (FORMAT_PRETTY.equals(timeFormat)) {
            textAssertions.assertTextNotPresent("2 days"); // first worklog
            textAssertions.assertTextNotPresent("3 days"); // second worklog
            textAssertions.assertTextPresent("5 days"); // total worklog
        } else if (FORMAT_DAYS.equals(timeFormat)) {
            textAssertions.assertTextNotPresent(" 2d "); // first worklog
            textAssertions.assertTextNotPresent(" 3d "); // second worklog
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Time Spent", "5d");
        } else if (FORMAT_HOURS.equals(timeFormat)) {
            textAssertions.assertTextNotPresent(" 48h "); // first worklog
            textAssertions.assertTextNotPresent(" 72h "); // second worklog
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Time Spent", "120h");
        }
    }

    @Test
    public void testLogWorkVisibleToGroup() {
        reconfigureTimetracking(FORMAT_PRETTY);
        execLogWorkVisible(GROUP_ADMINISTRATORS);
    }

    @Test
    public void testLogWorkVisibleToGroupDaysTimeFormat() {
        reconfigureTimetracking(FORMAT_DAYS);
        execLogWorkVisible(GROUP_ADMINISTRATORS);
    }

    @Test
    public void testLogWorkVisibleToGroupHoursTimeFormat() {
        reconfigureTimetracking(FORMAT_HOURS);
        execLogWorkVisible(GROUP_ADMINISTRATORS);
    }

    @Test
    public void testLogWorkDateIsStartDate() {
        // Log work on a specific date and make sure it comes before other work
        navigation.issue().viewIssue(HSP_1);
        getTester().clickLink("log-work");
        getTester().setFormElement("timeLogged", "2d");
        getTester().setFormElement("startDate", "1/Jun/06 12:00 PM");
        getTester().submit();
        getTester().clickLinkWithText("Work Log");
        textAssertions.assertTextPresent("01/Jun/06 12:00 PM");

        // Add another that is older and make sure it comes first
        getTester().clickLink("log-work");
        getTester().setFormElement("timeLogged", "2d");
        getTester().setFormElement("startDate", "1/Jun/05 12:00 PM");
        getTester().submit();
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "1/Jun/05 12:00 PM", "01/Jun/06 12:00 PM");
    }

    private void execLogWorkVisible(final String level) {
        navigation.issue().viewIssue(HSP_1);

        // set the role level
        getTester().clickLink("log-work");
        getTester().setFormElement("timeLogged", "2d");
        getTester().checkCheckbox("adjustEstimate", "new");
        getTester().setFormElement("newEstimate", "2w");
        getTester().setFormElement("comment", WORK_LOG_COMMENT_1);
        getTester().selectOption("commentLevel", level);
        getTester().submit();

        getTester().clickLink("log-work");
        getTester().setFormElement("timeLogged", "3d");
        getTester().checkCheckbox("adjustEstimate", "new");
        getTester().setFormElement("newEstimate", "2w");
        getTester().setFormElement("comment", WORK_LOG_COMMENT_2);
        getTester().selectOption("commentLevel", level);
        getTester().submit();

        assertAdminCannotSeeWorklog();
        assertFredCannotSeeWorkLog();
    }

    private void reconfigureTimetracking(String format) {
        tester.gotoPage("/secure/admin/jira/TimeTrackingAdmin!default.jspa");
        tester.submit("Deactivate");
        tester.checkCheckbox("timeTrackingFormat", format);
        tester.submit("Activate");
        timeFormat = format;
    }

    /* --------- End Test Group/Role Visibility --------- */

    @Test
    public void testChangeHistory() throws Exception {
        boolean isOracle = environmentUtils.isOracle();

        navigation.issue().viewIssue(HSP_1);
        getTester().clickLink("edit-issue");
        getTester().setFormElement("timetracking", "4d");
        getTester().submit("Update");

        if (isOracle) {
            //TODO: Remove this sleep hack once http://jira.atlassian.com/browse/JRA-20274 has been resolved
            Thread.sleep(2000);
        }

        getTester().clickLink("log-work");
        getTester().setFormElement("timeLogged", "4h 30m");
        getTester().checkCheckbox("adjustEstimate", "new");
        getTester().setFormElement("newEstimate", "1d 5h");
        getTester().submit("Log");
        getTester().clickLinkWithText("History");

        ChangeHistoryList expectedList = new ChangeHistoryList();
        expectedList.addChangeSet(ADMIN_FULLNAME)
                .add("Original Estimate", "4 days [ 345600 ]")
                .add("Remaining Estimate", "4 days [ 345600 ]");
        expectedList.addChangeSet(ADMIN_FULLNAME)
                .add("Time Spent", "4 hours, 30 minutes [ 16200 ]")
                .add("Remaining Estimate", "1 day, 5 hours [ 104400 ]");

        final ChangeHistoryList list = parse.issue().parseChangeHistory();
        list.assertContainsSomeOf(expectedList);
    }


    //------------------------------------------------------------------------------------------------------------------
    //  Func Tests for manually reducing the estimate.
    //------------------------------------------------------------------------------------------------------------------

    @Test
    public void testManuallyReduceEstimate() {
        navigation.issue().viewIssue(HSP_1);
        // Set Estimated Remaining to 2d
        tester.clickLink("edit-issue");
        tester.setFormElement("timetracking", "2d");
        tester.submit("Update");

        // Click Link 'Log work' (id='log-work').
        tester.clickLink("log-work");
        tester.setFormElement("timeLogged", "12h");
        tester.checkCheckbox("adjustEstimate", "manual");
        tester.setWorkingForm("log-work");
        tester.submit();
        // We set manual adjustment, but left the texfield blank - we should get an error.
        // Click Link 'Log work' (id='log-work').
        textAssertions.assertTextPresent("You must supply a valid amount of time to adjust the estimate by.");

        // Now try an invalid value
        tester.setFormElement("adjustmentAmount", "1cow");
        tester.setWorkingForm("log-work");
        tester.submit();
        textAssertions.assertTextPresent("Invalid time entered for adjusting the estimate.");

        // Finally give a valid value
        tester.setFormElement("adjustmentAmount", "6h");
        tester.setWorkingForm("log-work");
        tester.submit();
        // Assert the table 'tt_single_table_info'
        textAssertions.assertTextSequence(new IdLocator(tester, "tt_single_table_info"), "Estimated:", "2d",
                "Remaining:", "1d 18h", "Logged:", "12h");
    }

    private void assertTextSequence(final String[] strings) {
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), strings);
    }
}
