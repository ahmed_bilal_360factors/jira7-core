package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Class of tests which run tests remotely in JIRA.
 *
 * @since v5.0
 */
public class FuncTestSuiteRemote {

    public static List<Class<?>> bundledPluginsTests() {
        return ImmutableList.<Class<?>>builder()
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.remote", true))
                .build();
    }
}
