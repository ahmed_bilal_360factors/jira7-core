package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;

/**
 * Verify the streaming users methods from user manager work.
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
@RestoreBlankInstance
public class TestProcessUsers extends BaseJiraFuncTest {
    @Test
    public void testProcessUsers() {
        List<String> results = backdoor.userManager().userNamesStreamed();
        assertThat(results, containsInAnyOrder("admin", "fred"));
    }
}
