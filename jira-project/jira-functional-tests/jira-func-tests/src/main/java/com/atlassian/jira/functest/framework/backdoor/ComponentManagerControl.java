package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Control for accessing component manager state
 *
 * @since v7.2
 */
public class ComponentManagerControl extends BackdoorControl<ComponentManagerControl> {

    public ComponentManagerControl(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    /**
     * @return the state of the component manager
     */
    public ComponentManagerStateBean state() {
        return createComponentManagerStateResource().get(ComponentManagerStateBean.class);
    }

    /**
     * @return componentManager/state resource
     */
    private WebResource createComponentManagerStateResource() {
        return createResource().path("componentManager").path("state");
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ComponentManagerStateBean {

        @JsonProperty
        private String state;

        @JsonProperty
        private String pluginSystemState;

        /**
         * @return ComponentManager state
         */
        public String getState() {
            return state;
        }

        /**
         * @return Plugin system state
         */
        public PluginSystemState getPluginSystemState() {
            return PluginSystemState.valueOf(pluginSystemState);
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("state", state)
                    .append("pluginSystemState", pluginSystemState)
                    .toString();
        }
    }

    public enum PluginSystemState {
        NOT_STARTED,
        EARLY_STARTED,
        LATE_STARTED
    }
}
