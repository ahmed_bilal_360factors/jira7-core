package com.atlassian.jira.functest.framework.rule;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.testkit.client.IssueTypeControl.IssueType;
import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

/**
 * A rule helping to test issue type urls.
 * <p>
 * Just add this to your test:
 * </p>
 * <code><pre>
 *     &#64;Rule public IssueTypeUrls urls = new IssueTypeUrls();
 * </pre></code>
 *
 * @since 7.0
 */
public class IssueTypeUrls implements MethodRule {
    private Backdoor backdoor;

    private Map<String, IssueType> nameToIssueTypeMap;
    private final Pattern pattern = Pattern.compile(".*?avatarId=(\\d+).*?");

    @Override
    public Statement apply(final Statement base, final FrameworkMethod method, final Object target) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                if (BaseJiraRestTest.class.isAssignableFrom(target.getClass())) {
                    final BaseJiraRestTest test = (BaseJiraRestTest) target;

                    final Field backdoorField = BaseJiraRestTest.class.getDeclaredField("backdoor");
                    backdoorField.setAccessible(true);

                    backdoor = (Backdoor) backdoorField.get(test);

                    base.evaluate();
                    return;
                }

                if (BaseJiraFuncTest.class.isAssignableFrom(target.getClass())) {
                    final BaseJiraFuncTest test = (BaseJiraFuncTest) target;

                    final Field backdoorField = BaseJiraFuncTest.class.getDeclaredField("backdoor");
                    backdoorField.setAccessible(true);

                    backdoor = (Backdoor) backdoorField.get(test);

                    base.evaluate();
                    return;
                }

                throw new RuntimeException("Unsupported test case.");
            }
        };
    }

    /**
     * Util factory method for legacy tests allows using this rule with JUnit3.
     *
     * @param backdoor to be used.
     */
    @Deprecated
    public static IssueTypeUrls init(final Backdoor backdoor) {
        final IssueTypeUrls instance = new IssueTypeUrls();
        instance.backdoor = backdoor;
        return instance;
    }

    public String getIssueTypeUrl(final String issueTypeName) {
        final IssueType issueType = getNameToIssueTypeMap().get(issueTypeName.toLowerCase());

        return issueType != null ? issueType.getIconUrl() : null;
    }

    public Long getAvatarId(final String issueTypeName) {
        final IssueType issueType = getNameToIssueTypeMap().get(issueTypeName.toLowerCase());
        final Matcher matcher = pattern.matcher(issueType.getIconUrl());

        if (matcher.find()) {
            try {
                return Long.parseLong(matcher.group(1));
            } catch (NumberFormatException e) {
                return null;
            }
        }

        return null;
    }

    public void invalidateIssueTypeIconsCache() {
        nameToIssueTypeMap = null;
    }

    private Map<String, IssueType> getNameToIssueTypeMap() {
        if (nameToIssueTypeMap == null) {
            final List<IssueType> issueTypes = backdoor.issueType().getIssueTypes();
            nameToIssueTypeMap = issueTypes.stream().collect(toMap(it -> it.getName().toLowerCase(), identity()));
        }

        return nameToIssueTypeMap;
    }
}
