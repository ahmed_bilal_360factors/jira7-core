package com.atlassian.jira.webtests.ztests.admin.audit;

import com.atlassian.jira.auditing.AssociatedItem;
import com.atlassian.jira.auditing.ChangedValue;
import com.atlassian.jira.rest.util.serializers.ISODateSerializer;
import com.google.common.collect.ImmutableList;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.joda.time.DateTime;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

/**
 * This class exists as a version of AuditRecordBean which we can use without
 *
 * @since 7.0.0
 */
@SuppressWarnings("unused")
@JsonAutoDetect
public class MockAuditRecordBean {
    private Long id;
    private String summary;
    private String remoteAddress;
    private String authorKey;
    private DateTime created;
    private String category;
    private String eventSource;
    private String description;
    private AssociatedItemBean objectItem;
    private List<ChangedValueBean> changedValues;
    private List<AssociatedItemBean> associatedItems;

    public MockAuditRecordBean() {
    }

    public MockAuditRecordBean(final TestingAuditRecord auditRecord, final String categoryName) {
        this.id = auditRecord.getId();
        this.summary = auditRecord.getSummary();
        this.remoteAddress = auditRecord.getRemoteAddr();
        this.authorKey = auditRecord.getAuthorKey();
        this.created = new DateTime(auditRecord.getCreated());
        this.category = categoryName;
        this.eventSource = auditRecord.getEventSource();
        this.description = auditRecord.getDescription();
        this.objectItem = (auditRecord.getObjectItem() != null) ? new AssociatedItemBean() : null;
        this.changedValues = ImmutableList.of();
        this.associatedItems = ImmutableList.of();
    }

    public Long getId() {
        return id;
    }

    public String getSummary() {
        return summary;
    }

    public String getAuthorKey() {
        return authorKey;
    }

    @JsonSerialize(using = ISODateSerializer.class)
    public DateTime getCreated() {
        return created;
    }

    public AssociatedItemBean getObjectItem() {
        return objectItem;
    }

    public String getCategory() {
        return category;
    }

    public Iterable<AssociatedItemBean> getAssociatedItems() {
        return associatedItems;
    }

    public Iterable<ChangedValueBean> getChangedValues() {
        return changedValues;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public String getEventSource() {
        return eventSource;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private class AssociatedItemBean {
        @Nonnull
        public String getName() {
            return "jira-users";
        }

        @Nullable
        public String getId() {
            return "jira-users";
        }

        @Nullable
        public String getParentName() {
            return "JIRA Internal Directory";
        }

        @Nullable
        public String getParentId() {
            return "1";
        }

        @Nonnull
        public String getTypeName() {
            return AssociatedItem.Type.GROUP.toString();
        }
    }

    private class ChangedValueBean {
    }

    public static class ChangedValueImpl implements ChangedValue {
        private final String name;
        private final String from;
        private final String to;

        public ChangedValueImpl(final String name, final String from, final String to) {
            this.name = name;
            this.from = from;
            this.to = to;
        }

        @Nonnull
        @Override
        public String getName() {
            return name;
        }

        @Nullable
        @Override
        public String getFrom() {
            return from;
        }

        @Nullable
        @Override
        public String getTo() {
            return to;
        }
    }
}
