package com.atlassian.jira.webtests.ztests.customfield;

import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.util.collect.MapBuilder;
import net.sourceforge.jwebunit.WebTester;

import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.MOVE_DOWN;
import static com.atlassian.jira.functest.framework.FunctTestConstants.MOVE_TO_FIRST;
import static com.atlassian.jira.functest.framework.FunctTestConstants.MOVE_TO_LAST;
import static com.atlassian.jira.functest.framework.FunctTestConstants.MOVE_UP;

public class ArrowControlledOrdering {

    private final WebTester tester;
    private final TextAssertions textAssertions;
    private final HtmlPage page;
    private final FuncTestLogger logger;
    private final Form form;

    public ArrowControlledOrdering(final WebTester webTester,
                                   final TextAssertions textAssertions,
                                   final HtmlPage page,
                                   final FuncTestLogger logger,
                                   final Form form) {
        this.tester = webTester;
        this.textAssertions = textAssertions;
        this.page = page;
        this.logger = logger;
        this.form = form;
    }

    private void moveOptionsToPositions(final String[] optionValue, final String[] optionId,
                                        final String itemType, final Map<String, String> moveToPosition) {

        for (final String currentPosition : moveToPosition.keySet()) {
            final String newPosition = moveToPosition.get(currentPosition);

            final int currIntPos = Integer.parseInt(currentPosition);
            final int newIntPos = Integer.parseInt(newPosition);

            //checks and asserts if the current option is before or after the position it is moving to
            if (currIntPos < newIntPos) {
                textAssertions.assertTextSequence(page.asString(), "<b>" + optionValue[currIntPos] + "</b>", "<b>" + optionValue[newIntPos] + "</b>");
            } else if (currIntPos > newIntPos) {
                textAssertions.assertTextSequence(page.asString(), "<b>" + optionValue[newIntPos] + "</b>", "<b>" + optionValue[currIntPos] + "</b>");
            }

            //sets the new position for the current option
            logger.log("      Moving item at position " + currIntPos + " to position " + newPosition);
            tester.setFormElement("new" + itemType + "Position_" + optionId[currIntPos], newPosition);
        }

        form.clickButtonWithValue("Move");

        //completes the Map by filling in missing positions
        for (int i = 1; i < optionValue.length; i++) {
            if (!moveToPosition.containsKey(String.valueOf(i))) {
                int k = 1;
                while (k <= i && moveToPosition.containsValue(String.valueOf(k))) {
                    k++;
                }
                moveToPosition.put(String.valueOf(i), String.valueOf(k));
            }
        }

        //checks if the options moved to its correct positions
        for (final String currentOption : moveToPosition.keySet()) {
            final String newCurrentPos = moveToPosition.get(currentOption);
            final String newReplacedPos = moveToPosition.get(newCurrentPos);

            final int currentOptionInt = Integer.parseInt(currentOption);
            final int newCurrentPosInt = Integer.parseInt(newCurrentPos);
            final int otherOptionInt = Integer.parseInt(newCurrentPos);
            final int newReplacedPosInt = Integer.parseInt(newReplacedPos);

            if (newCurrentPosInt < newReplacedPosInt) {
                textAssertions.assertTextSequence(page.asString(), "<b>" + optionValue[currentOptionInt] + "</b>", "<b>" + optionValue[otherOptionInt] + "</b>");
            } else if (newCurrentPosInt > newReplacedPosInt) {
                textAssertions.assertTextSequence(page.asString(), "<b>" + optionValue[otherOptionInt] + "</b>", "<b>" + optionValue[currentOptionInt] + "</b>");
            }
            //else item remained in the same position
        }
    }

    public void checkOrderingUsingArrows(final String[] optionValue, final String[] optionId) {
        //following for loops moves options using the ordering arrows such that by the end of the for loop it returns
        //all the options back into its original position - ie remain in ascending order
        logger.log("Testing reordering using the option ordering arrows");
        logger.log("  checking moveToLast arrows");
        int i;
        for (i = 1; i < optionValue.length; i++) {
            tester.clickLink(MOVE_TO_LAST + optionId[i]);
            tester.assertLinkNotPresent(MOVE_DOWN + optionId[i]);
            tester.assertLinkNotPresent(MOVE_TO_LAST + optionId[i]);
        }
        checkItemsAreInAscendingOrder(optionValue);

        logger.log("  checking moveToFirst arrows");
        for (i = optionValue.length - 1; i >= 1; i--) {
            tester.clickLink(MOVE_TO_FIRST + optionId[i]);
            tester.assertLinkNotPresent(MOVE_UP + optionId[i]);
            tester.assertLinkNotPresent(MOVE_TO_FIRST + optionId[i]);
        }
        checkItemsAreInAscendingOrder(optionValue);

        logger.log("  checking moveDown arrows");
        for (i = 1; i < optionValue.length - 1; i++) {
            tester.clickLink(MOVE_DOWN + optionId[i]);
        }
        checkItemsAreInAscendingOrder(optionValue);

        logger.log("  checking moveUp arrows");
        for (i = optionValue.length - 1; i >= 2; i--) {
            tester.clickLink(MOVE_UP + optionId[i]);
        }
        checkItemsAreInAscendingOrder(optionValue);
    }

    public String checkOrderingUsingMoveToPos(final String[] optionValue, final String[] optionId, final String itemType) {
        //the following moves options using the 'Move To Position' field.
        logger.log("  Testing reordering using 'Move To Position' field inputs");

        logger.log("    Test moving one item");
        tester.clickLinkWithText("Sort options alphabetically");
        moveOptionsToPositions(optionValue, optionId, itemType, MapBuilder.<String, String>newBuilder()
                .add("1", "2").toHashMap());

        logger.log("    Test moving two items");
        tester.clickLinkWithText("Sort options alphabetically");
        moveOptionsToPositions(optionValue, optionId, itemType, MapBuilder.<String, String>newBuilder()
                .add("1", "3").add("2", "4").toHashMap());

        logger.log("    Test moving three items");
        tester.clickLinkWithText("Sort options alphabetically");
        moveOptionsToPositions(optionValue, optionId, itemType, MapBuilder.<String, String>newBuilder()
                .add("1", "3").add("2", "5").add("3", "4").toHashMap());

        return optionValue[1]; //this is for cascading select - to check ordering works at the lowest level
    }


    private void checkItemsAreInAscendingOrder(final String[] optionValue) {
        for (int i = 1; i < optionValue.length - 1; i++) {
            textAssertions.assertTextSequence(page.asString(), "<b>" + optionValue[i] + "</b>", "<b>" + optionValue[i + 1] + "</b>");
        }
    }


}
