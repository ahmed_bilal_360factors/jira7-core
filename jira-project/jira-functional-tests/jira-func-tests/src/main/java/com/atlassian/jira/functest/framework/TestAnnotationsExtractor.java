package com.atlassian.jira.functest.framework;

import org.junit.runner.Description;

import java.lang.annotation.Annotation;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * A tool to extract annotations from test first from method level, then from class level.
 *
 * @since v7.1
 */
public class TestAnnotationsExtractor {

    private final Description description;

    public TestAnnotationsExtractor(final Description description) {
        this.description = description;
    }

    public <A extends Annotation> Optional<A> findAnnotationFor(final Class<A> annotationClass) {
        return Stream.<Supplier<Optional<A>>>of(
                () -> this.getFromMethod(annotationClass),
                () -> this.getFromClass(annotationClass)
        ).map(Supplier::get).filter(Optional::isPresent).findFirst().orElse(Optional.empty());
    }

    private <A extends Annotation> Optional<A> getFromClass(final Class<A> annotationClass) {
        final A annotation = description.getTestClass().getAnnotation(annotationClass);
        return Optional.ofNullable(annotation);
    }

    private <A extends Annotation> Optional<A> getFromMethod(final Class<A> annotationClass) {
        return Optional.ofNullable(description.getMethodName())
                .flatMap(methodName -> getFromMethod(methodName, annotationClass));
    }

    private <A extends Annotation> Optional<A> getFromMethod(final String methodName, final Class<A> annotationClass) {
        try {
            return Optional.ofNullable(description.getTestClass().getMethod(methodName).getAnnotation(annotationClass));
        } catch (final NoSuchMethodException ignored) {
            return Optional.empty();
        }
    }
}
