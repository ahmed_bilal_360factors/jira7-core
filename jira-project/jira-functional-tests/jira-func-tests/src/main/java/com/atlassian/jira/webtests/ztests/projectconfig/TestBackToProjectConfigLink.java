package com.atlassian.jira.webtests.ztests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.ProjectAdministration;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;

@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.BROWSING})
@Restore("projectconfig/TestWebFragment.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestBackToProjectConfigLink extends BaseJiraFuncTest {

    @Inject
    Administration administration;

    @Test
    public void testBackToProjectConfigLink() {
        // put the project into the session
        final ProjectAdministration homosapienAdministration = ProjectAdministration.navigateToProject(navigation, getTester(), PROJECT_HOMOSAP_KEY);
        assertNoProjectLink();

        // put the issue type tab in the session
        homosapienAdministration.fields().goTo();
        assertOnFieldsTabWithoutReturn();

        // asert the back link is present on pages with titles
        tester.clickLink("project-config-fields-scheme-change");
        assertBackAndNavigate();

        // assert link went back to correct tab
        assertOnFieldsTabWithoutReturn();

        // assert back link present on pages with titles.
        navigation.gotoAdminSection(Navigation.AdminSection.WORKFLOWS);
        assertBackAndNavigate();

        // assert link went back to correct tab
        assertOnFieldsTabWithoutReturn();
    }

    private void assertNoProjectLink() {
        assertions.assertNodeByIdDoesNotExist("proj-config-return-link");
    }

    private void assertBackAndNavigate() {
        assertions.assertNodeByIdExists("proj-config-return-link");
        assertions.assertNodeByIdHasText("proj-config-return-link", "Back to project: homosapien");

        tester.clickLink("proj-config-return-link");
    }

    private void assertOnFieldsTabWithoutReturn() {
        assertNoProjectLink();

        assertions.assertNodeByIdExists("project-config-panel-fields");
        assertions.assertNodeByIdHasText("project-config-fields-scheme-name", "System Default Field Configuration");
    }
}
