package com.atlassian.jira.webtests.ztests.project;


import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.matcher.PermissionSchemesMatcher;
import com.atlassian.jira.permission.JiraPermissionHolderType;
import com.atlassian.jira.testkit.beans.PermissionSchemeBean;
import com.atlassian.jira.testkit.client.restclient.ProjectRoleClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Responsible for testing project roles.
 */
@WebTest({Category.FUNC_TEST, Category.PROJECTS, Category.ROLES})
@LoginAs(user = ADMIN_USERNAME)
public class TestProjectRoles extends BaseJiraFuncTest {
    private static final Long ROLE_ID = 10000l;
    private static final String ROLE_DESC = "this is a test role";
    private static final String ROLE_NAME = "test role";
    private static final String ROLE_NAME_UPPER_CASE = "Test role";
    private static final String ROLE_UPDATED_NAME = "testing role";
    private static final String ROLE_UPDATED_DESC = "this is an updated description";
    private static final String EDIT_USER_ACTORS = "edit_10000_atlassian-user-role-actor";
    private static final String EDIT_GROUP_ACTORS = "edit_10000_atlassian-group-role-actor";
    private static final String DELETE_ROLE = "delete_test role";
    private static final Long OTHER_PERMISSION_SCHEME_ID = 10000L;

    @Inject
    protected HtmlPage page;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    @RestoreBlankInstance
    public void testProjectRoleOperations() {
        administration.roles().create(ROLE_NAME, ROLE_DESC);
        editProjectRole(ROLE_UPDATED_NAME);

        // JRA-13157 - checks that you can change the case of a role name, this would probably only error on a case-insensitive
        // database, but is a good test to have anyway.
        editProjectRole(ROLE_NAME_UPPER_CASE);

        ensureErrorForDuplicateRoleName();

        administration.roles().delete(ROLE_NAME);

        textAssertions.assertTextNotPresent(locator.id("project-role-" + ROLE_NAME), ROLE_NAME);
        textAssertions.assertTextNotPresent(locator.id("project-role-" + ROLE_NAME), ROLE_DESC);
    }

    public Response addGroupToProjectRole(String groupName, String projectKey, String roleName) {

        ProjectRoleClient projectRoleClient = new ProjectRoleClient(environmentData);
        return projectRoleClient.addActors(projectKey, roleName, new String[]{groupName}, null);
    }


    public Response addUserToProjectRole(String userName, String projectKey, String roleName) {
        ProjectRoleClient projectRoleClient = new ProjectRoleClient(environmentData);
        return projectRoleClient.addActors(projectKey, roleName, null, new String[]{userName});
    }


    @Test
    @Restore("TestProjectRoles.xml")
    public void testCreateIssueWithRolePermissions() {
        // Make sure we cannot create an issue
        navigation.gotoDashboard();
        tester.assertLinkNotPresent("create_link");

        // Assign the admin to the test role for Homosapien
        addUserToProjectRole(ADMIN_USERNAME, "HSP", ROLE_NAME);

        // Assert create link is now present
        navigation.gotoDashboard();
        tester.assertLinkPresent("create_link");
        tester.clickLink("create_link");

        // Assert Monkey Project option is not present since we only assigned the user to Homosapien
        assertions.getProjectFieldsAssertions().assertProjectsEquals(new String[]{"homosapien"});

        // Assign the admin to the test role for Homosapien
        addUserToProjectRole(ADMIN_USERNAME, "MKY", ROLE_NAME);

        // Assert create link is now present
        tester.assertLinkPresent("create_link");
        tester.clickLink("create_link");

        // Assert Monkey Project option is NOW present since we only assigned the user to Homosapien
        assertions.getProjectFieldsAssertions().assertProjectsEquals(new String[]{"homosapien", "monkey"});
    }

    @Test
    @Restore("TestProjectRoles.xml")
    public void testCreateIssueWithRolePermissionsForGroup() {
        // Make sure we cannot create an issue
        navigation.gotoDashboard();
        tester.assertLinkNotPresent("create_link");

        // Assign the admin to the test role for Homosapien
        addGroupToProjectRole("jira-administrators", "HSP", ROLE_NAME);


        // Assert create link is now present
        navigation.gotoDashboard();
        tester.assertLinkPresent("create_link");
        tester.clickLink("create_link");

        // Assert Monkey Project option is not present since we only assigned the user to Homosapien
        assertions.getProjectFieldsAssertions().assertProjectsEquals(new String[]{"homosapien"});

        // Assign the admin to the test role for Homosapien
        addGroupToProjectRole("jira-administrators", "MKY", ROLE_NAME);

        // Assert create link is now present
        navigation.gotoDashboard();
        tester.assertLinkPresent("create_link");
        tester.clickLink("create_link");

        // Assert Monkey Project option is NOW present since we only assigned the user to Homosapien
        assertions.getProjectFieldsAssertions().assertProjectsEquals(new String[]{"homosapien", "monkey"});
    }

    @Test
    @Restore("TestProjectRoles.xml")
    public void testAddDefaultUsersAndDefaultGroupsToRole() {
        addAdministratorToDefaultRole();

        // Assert that an error is thrown on the default screen for adding a user that does not exist
        navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_ROLE_BROWSER);
        tester.clickLink("manage_test role");
        tester.clickLink(EDIT_USER_ACTORS);
        tester.setFormElement("userNames", "detkin");
        tester.submit("add");
        textAssertions.assertTextSequence(locator.css(".jiraform .aui-message.error"), "detkin", "could not be found");

        // Check adding a member that's already a member of the group throws an error
        navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_ROLE_BROWSER);
        tester.clickLink("manage_test role");
        tester.clickLink(EDIT_USER_ACTORS);
        tester.setFormElement("userNames", ADMIN_USERNAME);
        tester.submit("add");
        textAssertions.assertTextSequence(locator.css(".jiraform .aui-message.error"), ADMIN_USERNAME, "already a member");

        // Assert that we can add default groups
        addGroupsToDefaultRole();

        // Test the deleting of a user from a default role
        deleteDefaultUsersForRole();

        // Test the deletion of a group from a default role
        deleteDefaultGroupsForRole();
    }

    @Test
    @Restore("TestProjectRoles.xml")
    public void testDeleteProjectRoleRemovesSchemeEntries() {
        PermissionSchemeBean permissionScheme = backdoor.permissionSchemes().getAssignedPermissions(DEFAULT_PERM_SCHEME_ID);
        assertTrue("Should have on test_role in permission scheme", PermissionSchemesMatcher.hasPermissionCount(JiraPermissionHolderType.PROJECT_ROLE.getKey(), String.valueOf(ROLE_ID), 1).matches(permissionScheme));

        navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_ROLE_BROWSER);
        // delete the project role test_role
        deleteProjectRole(true);

        // now make sure that the test-role entry was deleted from the schemes
        navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);
        tester.clickLinkWithText("Notifications");
        tester.assertTextNotPresent(ROLE_NAME);

        permissionScheme = backdoor.permissionSchemes().getAssignedPermissions(DEFAULT_PERM_SCHEME_ID);
        assertTrue("Should not have the test_role in the permisison scheme", PermissionSchemesMatcher.hasPermissionCount(JiraPermissionHolderType.PROJECT_ROLE.getKey(), String.valueOf(ROLE_ID), 0).matches(permissionScheme));

        administration.issueSecuritySchemes().getScheme("Default Issue Security Scheme");
        tester.assertTextNotPresent(ROLE_NAME);
    }

    @Test
    @Restore("TestProjectRoles.xml")
    public void testDeleteProjectRoleWorkflowConditions() {
        //check workflow condition is associated with project role
        administration.workflows().goTo().workflowSteps("Copy of jira workflow");
        tester.clickLinkWithText("Start Progress");
        tester.assertTextPresent("Only users in project role <b>test role</b> can execute this transition.");

        //delete test project role (being used by a workflow condition)
        navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_ROLE_BROWSER);
        tester.setFormElement("name", "");
        tester.clickLink("delete_test role");
        tester.assertTextPresent("The following <strong>1</strong> workflow actions contain conditions that rely on the project role <strong>test role</strong>. If you delete this project role, these conditions will always fail.");
        tester.submit("Delete");

        //check workflow condition displays "missing project role" message correctly
        administration.workflows().goTo().workflowSteps("Copy of jira workflow");
        tester.clickLinkWithText("Start Progress");
        tester.assertTextPresent("Project Role (id=10000) is missing, now this condition will always fail.");

        //check workflow can still be updated
        //this gotoPage is the equivalent of pressing the "edit" link - don't want tester.clickLink() to conflict with other edit links
        tester.gotoPage("/secure/admin/workflows/EditWorkflowTransitionConditionParams!default.jspa?workflowStep=1&workflowTransition=4&count=2&workflowName=Copy+of+jira+workflow&workflowMode=live");
        tester.selectOption("jira.projectrole.id", "Users");
        tester.submit("Update");
        tester.assertTextPresent("Only users in project role <b>Users</b> can execute this transition.");
    }

    /**
     * Tests to see if the view usages screen contains all the associations it should
     */
    @Test
    @Restore("TestProjectRoleViewUsages.xml")
    public void testViewUsages() {
        backdoor.darkFeatures().enableForSite("jira.project.config.old.roles.screen");
        gotoViewUsagesForUsersProjectRole();

        assertions.getLinkAssertions().assertLinkPresentWithExactTextById("relatednotificationschemes", "Default Notification Scheme");
        assertions.getLinkAssertions().assertLinkPresentWithExactTextById("relatednotificationschemes", "Other Notification Scheme");

        assertions.getLinkAssertions().assertLinkPresentWithExactTextById("relatedpermissionschemes", "Default Permission Scheme");
        assertions.getLinkAssertions().assertLinkPresentWithExactTextById("relatedpermissionschemes", "Other Permission Scheme");

        textAssertions.assertTextSequence(locator.table("relatedpermissionschemes"), new String[]{
                "Default Permission Scheme", "homosapien", "3 (View)", "monkey", "2 (View)", "test", "1 (View)",
                "Other Permission Scheme", "None", "None"
        });

        tester.assertLinkPresentWithText("test issue security scheme");

        tester.assertLinkPresentWithText("jira workflow");
        tester.assertLinkPresentWithText("Start Progress");
        tester.assertLinkPresentWithText("Stop Progress");

        // Now make sure all the links work
        tester.clickLinkWithText("Default Notification Scheme");
        tester.assertTextPresent("Edit Notifications &mdash; Default Notification Scheme");
        tester.assertLinkPresentWithText("Add notification");

        gotoViewUsagesForUsersProjectRole();
        tester.clickLinkWithText("Other Notification Scheme");
        tester.assertTextPresent("Edit Notifications &mdash; Other Notification Scheme");
        tester.assertLinkPresentWithText("Add notification");

        gotoViewUsagesForUsersProjectRole();
        assertEquals(getEditPermissionsUrl(DEFAULT_PERM_SCHEME_ID), this.page.getLinksWithExactText("Default Permission Scheme")[0].getURLString());
        assertEquals(getEditPermissionsUrl(OTHER_PERMISSION_SCHEME_ID), this.page.getLinksWithExactText("Other Permission Scheme")[0].getURLString());

        gotoViewUsagesForUsersProjectRole();
        tester.clickLinkWithText("test issue security scheme");
        tester.assertTextPresent("Edit Issue Security Levels");
        tester.assertTextPresent("test security level");

        gotoViewUsagesForUsersProjectRole();
        tester.clickLinkWithText("Start Progress");
        tester.assertTextPresent("Transition: Start Progress");

        gotoViewUsagesForUsersProjectRole();
        tester.clickLinkWithText("Stop Progress");
        tester.assertTextPresent("Transition: Stop Progress");

        // Test one of the project links
        gotoViewUsagesForUsersProjectRole();
        tester.clickLinkWithText("homosapien");
        assertions.getSidebarAssertions().assertProjectName("homosapien");

        gotoViewUsagesForUsersProjectRole();
        tester.clickLink("view_project_role_actors_10000");
        tester.assertElementPresent("project-config-panel-people");
    }

    private void gotoViewUsagesForUsersProjectRole() {
        navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_ROLE_BROWSER);

        // browse to the user role
        tester.clickLink("view_Users");
    }


    private void addGroupsToDefaultRole() {
        navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_ROLE_BROWSER);
        tester.clickLink("manage_test role");
        tester.assertTextPresent("None selected");
        tester.clickLink(EDIT_GROUP_ACTORS);
        tester.assertTextPresent("Assign Default Groups to Project Role: test role");
        tester.setFormElement("groupNames", "jira-users, jira-developers, jira-administrators");
        tester.submit("add");
        tester.clickLink("return_link");
        tester.assertTextPresent("jira-administrators");
        tester.assertTextPresent("jira-users");
        tester.assertTextPresent("jira-developers");
        tester.clickLink("return_link");
        tester.assertTextPresent("Project Role Browser");
    }

    private void addAdministratorToDefaultRole() {
        navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_ROLE_BROWSER);
        tester.clickLink("manage_test role");
        tester.assertTextPresent("None selected.");
        tester.clickLink(EDIT_USER_ACTORS);
        tester.assertTextPresent("Assign Default Users to Project Role: test role");
        tester.setFormElement("userNames", "admin, fred");
        tester.submit("add");
        tester.clickLink("return_link");
        tester.assertTextPresent(ADMIN_FULLNAME);
        tester.clickLink("return_link");
        tester.assertTextPresent("Project Role Browser");
    }

    private void deleteDefaultUsersForRole() {
        navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_ROLE_BROWSER);
        tester.clickLink("manage_test role");
        tester.clickLink(EDIT_USER_ACTORS);
        tester.assertTextPresent("Assign Default Users to Project Role: test role");
        tester.checkCheckbox("removeusers_admin", ".");
        tester.checkCheckbox("removeusers_fred", ".");
        tester.submit("remove");
        tester.assertTextPresent("There are currently no users assigned to this project role.");
    }

    private void deleteDefaultGroupsForRole() {
        navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_ROLE_BROWSER);
        tester.clickLink("manage_test role");
        deleteGroupsFromRoleForm("Assign Default Groups to Project Role: test role");
    }


    private void deleteGroupsFromRoleForm(String textPresent) {
        tester.clickLink(EDIT_GROUP_ACTORS);
        tester.assertTextPresent(textPresent);
        tester.checkCheckbox("removegroups_jira-users", ".");
        tester.checkCheckbox("removegroups_jira-developers", ".");
        tester.checkCheckbox("removegroups_jira-administrators", ".");
        tester.submit("remove");
        tester.assertTextPresent("There are currently no groups assigned to this project role");
    }

    private void deleteProjectRole(boolean checkAssociatedSchemes) {
        tester.clickLink(DELETE_ROLE);
        tester.assertTextPresent("Are you sure you would like to delete project role");
        tester.assertTextPresent("test role");
        if (checkAssociatedSchemes) {
            tester.assertLinkPresentWithText("Default Notification Scheme");
            tester.assertLinkPresentWithText("Default Permission Scheme");
            tester.assertLinkPresentWithText("Default Issue Security Scheme");

            // we need to show associated workflows as well, check they are present
            tester.assertTextPresent("Copy of jira workflow");
            // check the associated step:
            tester.assertLinkPresentWithText("Start Progress");
        }
        tester.submit("Delete");

        textAssertions.assertTextNotPresent(locator.id("project-role-" + ROLE_NAME), ROLE_NAME);
        textAssertions.assertTextNotPresent(locator.id("project-role-" + ROLE_NAME), ROLE_DESC);
    }

    private void ensureErrorForDuplicateRoleName() {
        tester.setFormElement("name", "test role");
        tester.setFormElement("description", "");
        tester.submit("Add Project Role");
        tester.assertTextPresent("A project role with name &#39;test role&#39; already exists.");
    }

    private void editProjectRole(String newRoleName) {
        administration.roles().edit("test role").setName(newRoleName);
        administration.roles().edit(newRoleName).setDescription(ROLE_UPDATED_DESC);

        tester.assertTextPresent(newRoleName);
        tester.assertTextPresent(ROLE_UPDATED_DESC);

        administration.roles().edit(newRoleName).setName(ROLE_NAME);
        administration.roles().edit(ROLE_NAME).setDescription(ROLE_DESC);
    }

    private String getEditPermissionsUrl(Long schemeId) {
        return getEnvironmentData().getContext() + "/secure/admin/EditPermissions!default.jspa?schemeId=" + Long.toString(schemeId);
    }

}
