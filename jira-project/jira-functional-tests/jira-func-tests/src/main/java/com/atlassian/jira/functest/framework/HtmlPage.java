package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.util.dom.DomKit;
import com.atlassian.jira.functest.framework.util.dom.SneakyDomExtractor;
import com.atlassian.jira.functest.framework.util.url.URLUtil;
import com.atlassian.jira.webtests.table.HtmlTable;
import com.meterware.httpunit.WebLink;
import net.sourceforge.jwebunit.WebTester;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Returns information about the current HTML page in the func test.
 *
 * @since v4.0
 */
public class HtmlPage {
    private final WebTester tester;

    @Inject
    public HtmlPage(final WebTester tester) {
        this.tester = tester;
    }

    public HtmlTable getHtmlTable(final String tableID) {
        return HtmlTable.newHtmlTable(tester, tableID);
    }

    public boolean isLinkPresentWithExactText(final String text) {
        final WebLink[] webLinks = getLinksWithExactText(text);
        return webLinks.length > 0;
    }

    public WebLink[] getLinksWithExactText(final String text) {
        try {
            return tester.getDialog().getResponse().getMatchingLinks(WebLink.MATCH_TEXT, text);
        } catch (final SAXException e) {
            throw new RuntimeException(e);
        }
    }

    public String getXsrfToken() {
        final XPathLocator locator = new XPathLocator(tester, "//meta[@id='atlassian-token']");
        final Element meta = (Element) locator.getNode();
        if (meta != null) {
            return URLUtil.encode(meta.getAttribute("content"));
        }
        return "";
    }

    public String getFreshXsrfToken() {
        // quick to load and parse and causes the XSRF token to be resynchronised
        tester.beginAt("/secure/ViewKeyboardShortcuts.jspa");
        return getXsrfToken();
    }

    /**
     * Adds a token to a given url in the FIRST position
     *
     * @param url the url in question
     * @return the url with the token in place
     */
    public String addXsrfToken(final String url) {
        return URLUtil.addXsrfToken(getXsrfToken(), url);
    }

    public Footer getFooter() {
        return new FooterImpl(tester.getDialog().getResponse());
    }

    /**
     * Returns text of the entire page
     *
     * @return text of the page
     */
    public String asString() {
        return tester.getDialog().getResponseText();
    }

    /**
     * Returns all the text nodes of the web response collapsed and then concatenated together.  Very useful for text
     * sequence detection when you don care about intermediate Elements such as anchors and spans.
     *
     * @return all the text nodes of the web respone collapsed and then concatenated together
     */
    public String getCollapsedResponseText() {
        // This doesnt work because of Xerces cloning issues so we have to be sneaky
        //Document doc = response.getDOM();

        Document doc = SneakyDomExtractor.getDOM(tester);
        Element bodyE = DomKit.getBodyElement(doc);
        final String text;
        if (bodyE != null) {
            text = DomKit.getCollapsedText(bodyE);
        } else {
            text = "";
        }
        return text;
    }

    /**
     * Returns a regex match on the page.
     *
     * @param regex the regex
     * @return first match
     */
    public Optional<String> getRegexMatch(final String regex) {
        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(asString());
        final boolean matches = matcher.find();
        if (matches) {
            return Optional.of(matcher.group(1));
        } else {
            return Optional.empty();
        }
    }
}
