package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookRegistrationClient;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import org.hamcrest.Matchers;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.testkit.client.util.JsonMatchers.hasField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.WEBHOOKS, Category.REFERENCE_PLUGIN})
public class TestWebHookWithUrlTemplate extends AbstractWebHookTest {
    @Before
    public void setUp() throws Exception {
        super.setUpTest();
    }

    @Test
    public void testIssueKeyTemplateInUrl() throws InterruptedException, JSONException {
        backdoor.restoreBlankInstance();

        final WebHookRegistrationClient.Registration registration = new WebHookRegistrationClient.Registration();
        registration.name = "Comment Issue Web Hook";
        registration.events = new String[]{"jira:issue_updated", "jira:issue_created"};
        registration.path = "/${issue.key}";
        responseTester.registerWebhook(registration);

        final IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "This is issue with utf-8 chars čšžćđ");
        final WebHookResponseData webHookResponse = getWebHookResponse();
        assertThat(webHookResponse.getUri().getPath(), Matchers.endsWith(issue.key()));

        final JSONObject event = new JSONObject(webHookResponse.getJson());
        assertThat(event, hasField("issue.fields.summary"));
        assertEquals("This is issue with utf-8 chars čšžćđ", event.getJSONObject("issue").getJSONObject("fields").getString("summary"));

        backdoor.issues().deleteIssue(issue.key(), true);
        assertThat(webHookResponse.getUri().getPath(), Matchers.endsWith(issue.key()));
    }
}
