package com.atlassian.jira.functest.rule;

import com.atlassian.jira.testkit.client.AttachmentsControl;
import org.junit.rules.ExternalResource;

import java.util.function.Supplier;

/**
 * Enables attachments feature before a test.
 *
 * @since v6.4
 */
class EnableAttachmentsRule extends ExternalResource {
    private final Supplier<AttachmentsControl> attachmentsControlSupplier;

    EnableAttachmentsRule(final Supplier<AttachmentsControl> attachmentsControlSupplier) {
        this.attachmentsControlSupplier = attachmentsControlSupplier;
    }

    @Override
    protected void before() throws Throwable {
        super.before();
        attachmentsControlSupplier.get().enable();
    }
}
