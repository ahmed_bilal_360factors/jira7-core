package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.meterware.httpunit.WebForm;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TAB_CHANGE_HISTORY;

/**
 * Test Cross-site scripting vulnerabilities when script entered in fullname
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
public class TestXSSInFullName extends BaseJiraFuncTest {
    public static final String ALANS_PASSWORD = "alans";
    private static final String FULLNAME = "Alan <script>alert('I am a script')</script> Sapinsly";
    private static final String ESCAPED_FULLNAME = "Alan &lt;script&gt;alert(&#39;I am a script&#39;)&lt;/script&gt; Sapinsly";
    private static final String TEST_PROJECT = "Test";
    private static final String NEW_PROJECT = "New Project";
    // Username and password for user with script within fullname
    private static final String ALANS_USERNAME = "alans";
    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.comment().enableCommentGroupVisibility(Boolean.TRUE);
    }

    // Restore data with user's (alans) fullname set to include script
    // The user is also the only assignable user for the project 'NP'
    // One issue TST-1 exists - created by admin user

    @Restore("TestXSSData.xml")
    @Test
    public void testFullNameWithScript() {
        _testFullUsernameInComment();
        _testEditAssigneeField();
        _testAssigneeNavigatorView();
        _testAssigneeFieldinMoveOperation();
        _testChangeHistoryTab();
        _testDeveloperWorkloadReport();
        _testWorklogTab();
        _testUserPickerCustomField();
        _testReporterInNavigatorView();
        _testWatchersTable();
        _testDashboardPortlets();
        _testReporterInBulkEdit();
        _testMultiUserCustomFieldView();
        _testTimeTrackingExcel();
    }

    /**
     * Ensure that a user's fullname is html escaped correctly in the comments and in the bodytop where the fullname of the
     * logged in user is displayed.
     */
    public void _testFullUsernameInComment() {
        // log out and log in as alans (with malicious fullname)
        navigation.logout();
        navigation.login(ALANS_USERNAME, ALANS_PASSWORD);

        navigation.issue().gotoIssue("TST-1");
        tester.clickLink("footer-comment-button");
        tester.setWorkingForm("comment-add");
        tester.setFormElement("comment", "Testing scripting error");
        tester.submit("Add");

        // Assert that the unescaped fullname is not present on the page - in the comment or the top right section where
        // the logged in user fullname is displayed.
        checkName();
        textAssertions.assertTextPresent("Testing scripting error");
    }

    public void _testEditAssigneeField() {
        // start creation of one issue
        navigation.issue().goToCreateIssueForm("New Project", null);

        // assert that the assignee name is correctly escaped
        checkName();
    }

    public void _testAssigneeNavigatorView() {
        createIssue(NEW_PROJECT);

        // Goto issue navigator and display issues (assignee column included)
        tester.clickLink("find_link");

        // assert that the assignee name is correctly escaped
        checkName();
    }

    public void _testAssigneeFieldinMoveOperation() {
        createIssue(TEST_PROJECT);

        // Move issue to "New Project"
        // Forced to select assignee during move operation as 'admin' is not assignable in "New Project"
        tester.clickLink("move-issue");
        navigation.issue().selectProject("New Project");
        tester.submit("Next >>");

        // assert that the assignee name is correctly escaped on field update screen
        checkName();
    }

    public void _testChangeHistoryTab() {
        createIssue(TEST_PROJECT);

        navigation.logout();
        navigation.login(ALANS_USERNAME, ALANS_PASSWORD);

        // Resolve an issue
        navigation.issue().gotoIssue("TST-1");
        tester.clickLink("action_id_5");
        tester.setWorkingForm("issue-workflow-transition");
        tester.submit("Transition");

        // Assert that name is correctly escaped in "Change History" tab
        tester.clickLinkWithText(ISSUE_TAB_CHANGE_HISTORY);
        checkName();
    }

    public void _testDeveloperWorkloadReport() {
        // Create issue assigned to 'alans' with an estimate set
        navigation.issue().goToCreateIssueForm("New Project", null);
        tester.setFormElement("summary", "Test");
        tester.setFormElement("timetracking", "1h");
        tester.submit("Create");

        // Create the report
        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=" + 10000 + "&reportKey=com.atlassian.jira.jira-core-reports-plugin:developer-workload");
        tester.setFormElement("developer", "alans");
        tester.submit("Next");

        // Assert that name is correctly escaped in report
        checkName();
    }

    public void _testWorklogTab() {
        // Create issue assigned to 'alans' with an estimate set
        navigation.issue().goToCreateIssueForm("New Project", null);
        tester.setFormElement("summary", "Test");
        tester.setFormElement("timetracking", "1h");
        tester.submit("Create");

        // log out and log in as alans (with malicious fullname)
        navigation.logout();
        navigation.login(ALANS_USERNAME, ALANS_PASSWORD);

        // goto issue and log work
        navigation.issue().gotoIssue("NP-1");
        tester.clickLink("log-work");
        tester.setFormElement("timeLogged", "1h");
        tester.submit();
        tester.clickLinkWithText("Work Log");

        // Assert that name is correctly escaped in report
        checkName();
    }

    public void _testUserPickerCustomField() {
        navigation.issue().goToCreateIssueForm(null, null);
        tester.setFormElement("summary", "Test");
        // Set 'alans' as the cf user
        tester.setFormElement("customfield_10000", "alans");
        tester.submit("Create");

        // Assert that name is correctly escaped in report
        checkName();
    }

    public void _testReporterInNavigatorView() {
        // log out and log in as alans (with malicious fullname)
        navigation.logout();
        navigation.login(ALANS_USERNAME, ALANS_PASSWORD);
        createIssue(TEST_PROJECT);

        // Goto issue navigator
        tester.clickLink("find_link");

        // Assert that name is correctly escaped in report
        checkName();
    }

    public void _testWatchersTable() {
        // log out and log in as alans (with malicious fullname)
        navigation.logout();
        navigation.login(ALANS_USERNAME, ALANS_PASSWORD);

        // goto issue and watch it
        navigation.issue().gotoIssue("TST-1");
        tester.clickLink("toggle-watch-issue");

        // goto watchers table
        tester.clickLink("view-watcher-list");
        // Assert that name is correctly escaped in table
        checkName();
    }

    public void _testDashboardPortlets() {
        // log out and log in as alans (with malicious fullname)
        navigation.logout();
        navigation.login(ALANS_USERNAME, ALANS_PASSWORD);

        // create an issue with User picker CF value set to alans
        navigation.issue().goToCreateIssueForm(null, null);
        tester.setFormElement("summary", "Test");
        // Set 'alans' as the cf user
        tester.setFormElement("customfield_10000", "alans");
        tester.submit("Create");

        // Navigate to the dashboard
        tester.clickLink("home_link");

        // The dashboard has a number of portlets - including the 2-D portlet that will display
        // the malicious fullname

        // Assert that name is correctly escaped on dashboard
        checkName();
    }

    public void _testReporterInBulkEdit() {
        navigation.logout();
        navigation.login(ALANS_USERNAME, ALANS_PASSWORD);

        // Bulk edit the existing issue to have 'alans' as reporter.
        tester.clickLink("find_link");
        navigation.issueNavigator().bulkEditAllIssues();

        // Check all checkboxes to select all issues
        tester.setWorkingForm("bulkedit");
        final WebForm form = tester.getDialog().getForm();
        final String[] parameterNames = form.getParameterNames();
        for (final String name : parameterNames) {
            if (name.startsWith("bulkedit_")) {
                tester.checkCheckbox(name);
            }
        }
        tester.submit("Next");

        tester.checkCheckbox("operation", "bulk.edit.operation.name");
        tester.submit("Next");
        tester.checkCheckbox("actions", "reporter");
        tester.setFormElement("reporter", "alans");
        tester.submit("Next");

        // Assert that name is correctly escaped in details table
        checkName();
    }

    public void _testMultiUserCustomFieldView() {
        // create one issue
        navigation.issue().goToCreateIssueForm(TEST_PROJECT, null);
        tester.setFormElement("summary", "Bug 1");
        tester.setFormElement("customfield_10010", "alans");
        tester.submit("Create");

        // Assert that name is correctly escaped in multi-user custom field
        checkName();
    }

    public void _testTimeTrackingExcel() {
        // log out and log in as alans (with malicious fullname)
        navigation.logout();
        navigation.login(ALANS_USERNAME, ALANS_PASSWORD);

        // Create issue assigned to 'alans' with an estimate set
        navigation.issue().goToCreateIssueForm("New Project", null);
        tester.setFormElement("summary", "Test");
        tester.setFormElement("timetracking", "1h");
        tester.submit("Create");

        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=" + 10000 + "&reportKey=com.atlassian.jira.jira-core-reports-plugin:time-tracking");
        tester.submit("Next");
        tester.clickLinkWithText("Excel View");

        // Assert that name is correctly escaped in excel view name
        checkName();
    }

    // Create one issue in project specified
    // alans is default assignee for project NP
    // admin is default assignee for project TST
    private void createIssue(final String projectKey) {
        // create one issue
        navigation.issue().goToCreateIssueForm(projectKey, null);
        tester.setFormElement("summary", "Bug 1");
        tester.submit("Create");
    }

    // Verify that the name is correctly escaped
    private void checkName() {
        textAssertions.assertTextNotPresent(FULLNAME);
        textAssertions.assertTextPresent(ESCAPED_FULLNAME);
    }
}
