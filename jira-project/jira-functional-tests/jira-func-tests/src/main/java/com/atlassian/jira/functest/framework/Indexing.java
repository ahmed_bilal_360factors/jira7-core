package com.atlassian.jira.functest.framework;

import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import net.sourceforge.jwebunit.WebTester;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;

import javax.inject.Inject;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @since v7.1
 */
public class Indexing {

    private final Navigation navigation;
    private final WebTester tester;
    private final FuncTestLogger logger;
    private final HtmlPage htmlPage;

    @Inject
    public Indexing(final Navigation navigation, final WebTester tester, final HtmlPage htmlPage, final FuncTestLogger logger) {
        this.navigation = navigation;
        this.tester = tester;
        this.htmlPage = htmlPage;
        this.logger = logger;
    }

    /**
     * Check that certain fields have been indexed correctly. This will move to the XML Index View of a given issuekey,
     * then execute some xpath to assert that the index is in order.
     *
     * @param path                xpath expression of the base node we are checking.  Eg: "//item"
     * @param expectedItemNodes   Map containing key (xpath expression starting from path) : value (the expected value).
     *                            For example: "description" : "some description". You can pass {@code null} instead of empty map.
     * @param unexpectedItemNodes List of nodes from path that are *not* expected to be present. For example, if the
     *                            //item/[environment = "some environment"] should NOT be found, then the map would be: "environment" : "some
     *                            environment". You can pass {@code null} instead of empty map.
     * @param issueKey            issue key of item we are checking.  Eg: "HSP-1"
     */
    public void assertIndexedFieldCorrect(final String path, final Map<String, String> expectedItemNodes,
                                          final Map<String, String> unexpectedItemNodes, final String issueKey) {
        try {
            navigation.gotoPage("/si/jira.issueviews:issue-xml/" + issueKey + "/" + issueKey + ".xml?jira.issue.searchlocation=index");
            final org.w3c.dom.Document doc = XMLUnit.buildControlDocument(htmlPage.asString());
            assertThat(tester.getDialog().getResponse().getContentType(), equalTo("text/xml"));

            if (expectedItemNodes != null) {
                for (final Map.Entry<String, String> e : expectedItemNodes.entrySet()) {
                    final String xpathExpression = path + "[" + e.getKey() + "= &quot;" + e.getValue() + "&quot; ] ";
                    logger.log("Searching for existence of xpath " + xpathExpression);
                    XMLAssert.assertXpathExists(xpathExpression, doc);
                }
            }

            if (unexpectedItemNodes != null) {
                for (final Map.Entry<String, String> e : unexpectedItemNodes.entrySet()) {
                    final String xpathExpression = path + "[" + e.getKey() + "= &quot;" + e.getValue() + "&quot; ] ";
                    logger.log("Searching for nonexistence of xpath " + xpathExpression);
                    XMLAssert.assertXpathNotExists(xpathExpression, doc);
                }
            }
        } catch (final Throwable e) {
            throw new RuntimeException(e);
        } finally {
            //we need to go back to the an HTML area so that subsequent tests will not fail due to being in an XML area
            //even if it fails (so we can execute teardown)
            navigation.gotoPage("/secure/Dashboard.jspa");
        }
    }

    public void assertIndexContainsItem(final String xpath, final String issueKey) {
        try {
            navigation.gotoPage("/si/jira.issueviews:issue-xml/" + issueKey + "/" + issueKey + ".xml?jira.issue.searchlocation=index");
            final org.w3c.dom.Document doc = XMLUnit.buildControlDocument(htmlPage.asString());
            assertThat(tester.getDialog().getResponse().getContentType(), equalTo("text/xml"));

            XMLAssert.assertXpathExists(xpath, doc);
        } catch (final Throwable e) {
            throw new RuntimeException(e);
        } finally {
            navigation.gotoPage("/secure/Dashboard.jspa");
        }
    }

    public void assertIndexNotContainsItem(final String xpath, final String issueKey) {
        try {
            navigation.gotoPage("/si/jira.issueviews:issue-xml/" + issueKey + "/" + issueKey + ".xml?jira.issue.searchlocation=index");
            final org.w3c.dom.Document doc = XMLUnit.buildControlDocument(htmlPage.asString());
            assertThat(tester.getDialog().getResponse().getContentType(), equalTo("text/xml"));

            XMLAssert.assertXpathNotExists(xpath, doc);
        } catch (final Throwable e) {
            throw new RuntimeException(e);
        } finally {
            navigation.gotoPage("/secure/Dashboard.jspa");
        }
    }
}
