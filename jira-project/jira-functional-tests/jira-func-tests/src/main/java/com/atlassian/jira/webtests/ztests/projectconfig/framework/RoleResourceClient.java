package com.atlassian.jira.webtests.ztests.projectconfig.framework;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.rest.beans.RoleActorsAddResponse;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.rest.beans.RolesResponse;
import com.google.common.collect.Maps;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import javax.ws.rs.core.MediaType;
import java.util.Map;

public class RoleResourceClient extends RestApiClient<RoleResourceClient> {
    private final String root;

    public RoleResourceClient(JIRAEnvironmentData environmentData) {
        super(environmentData);
        root = environmentData.getBaseUrl().toExternalForm();
    }

    @Override
    protected WebResource createResource() {
        return resourceRoot(root).path("rest").path("projectconfig").path("latest").path("roles");
    }

    protected WebResource createResource(String projectKey) {
        return createResource().path(projectKey);
    }

    public RolesResponse get(String projectKey) {
        return createResource(projectKey).type(MediaType.APPLICATION_JSON_TYPE).get(RolesResponse.class);
    }

    public RolesResponse search(String projectKey, String query) {
        return createResource(projectKey)
                .queryParam("query", query)
                .type(MediaType.APPLICATION_JSON_TYPE).get(RolesResponse.class);
    }

    public RolesResponse search(String projectKey, long roleId) {
        return createResource(projectKey)
                .queryParam("roleId", String.valueOf(roleId))
                .type(MediaType.APPLICATION_JSON_TYPE).get(RolesResponse.class);
    }

    public RolesResponse search(String projectKey, long roleId, String query) {
        return createResource(projectKey)
                .queryParam("roleId", String.valueOf(roleId))
                .queryParam("query", query)
                .type(MediaType.APPLICATION_JSON_TYPE).get(RolesResponse.class);
    }

    public RolesResponse search(String projectKey, long roleId, String query, int pageNumber, int pageSize) {
        return createResource(projectKey)
                .queryParam("roleId", String.valueOf(roleId))
                .queryParam("query", query)
                .queryParam("pageNumber", String.valueOf(pageNumber))
                .queryParam("pageSize", String.valueOf(pageSize))
                .type(MediaType.APPLICATION_JSON_TYPE).get(RolesResponse.class);
    }

    public RoleActorsAddResponse addActors(String projectKey, long roleId, String[] groups, String[] users) {
        Map<String, String[]> actors = Maps.newHashMap();
        actors.put("groups", groups);
        actors.put("users", users);

        return createResource(projectKey)
                .path(String.valueOf(roleId))
                .type(MediaType.APPLICATION_JSON_TYPE).post(RoleActorsAddResponse.class, actors);
    }

    public Response searchResponse(String projectKey, long roleId) {
        return toResponse(() -> createResource(projectKey)
                .queryParam("roleId", String.valueOf(roleId))
                .type(MediaType.APPLICATION_JSON_TYPE)
                .get(ClientResponse.class));
    }

    public Response searchResponse(String projectKey, long roleId, int pageNumber, int pageSize) {
        return toResponse(() -> createResource(projectKey)
                .queryParam("roleId", String.valueOf(roleId))
                .queryParam("pageNumber", String.valueOf(pageNumber))
                .queryParam("pageSize", String.valueOf(pageSize))
                .type(MediaType.APPLICATION_JSON_TYPE)
                .get(ClientResponse.class));
    }

    public Response addActorsResponse(String projectKey, long roleId, String[] groups, String[] users) {
        Map<String, String[]> actors = Maps.newHashMap();
        actors.put("groups", groups);
        actors.put("users", users);

        return toResponse(() -> createResource(projectKey)
                .path(String.valueOf(roleId))
                .type(MediaType.APPLICATION_JSON_TYPE)
                .post(ClientResponse.class, actors));
    }
}
