package com.atlassian.jira.webtests.ztests;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.LicenseKeys;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@WebTest({Category.FUNC_TEST, Category.IMPORT_EXPORT, Category.UPGRADE_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUpgradeExport extends BaseJiraFuncTest {
    private static final String BUILD_NUMBER_PATTERN = "Build # +: (\\d+)";
    private static final String PATCHED_NUMBER_PATTERN = "jira\\.version\\.patched +: (\\d+)";

    @Inject
    private Administration administration;

    /**
     * Tests if JIRA is able to import a data. This test also exports data for the cloud import test.
     */
    @Test
    public void testDataImport() {
        importDataFromFile("JiraSfAndSdExport_72002.zip");
        navigation.login("admin");
        final File exportFile = administration.exportDataToFile("server_backup_export.xml");

        assertTrue("Backup file does not exist", exportFile.exists());
        assertTrue("Can not read exported file", exportFile.canRead());
        verifyExportedFile(exportFile);
    }

    private void verifyExportedFile(final File exportFile) {
        ZipFile zipFile = null;
        InputStream inputStream = null;
        try {
            zipFile = new ZipFile(exportFile);
            inputStream = zipFile.getInputStream(new ZipEntry("entities.xml"));
            final String entitiesXmlContent = IOUtils.toString(inputStream);
            assertFalse("Exported file has no content.", StringUtils.isBlank(entitiesXmlContent));
            final Integer buildNumber = getBuildNumber(entitiesXmlContent, BUILD_NUMBER_PATTERN);
            final Integer patchedNumber = getBuildNumber(entitiesXmlContent, PATCHED_NUMBER_PATTERN);
            assertEquals(format("Patched build number (%d) is not equal to the application's build number (%d)", patchedNumber, buildNumber), buildNumber, patchedNumber);
        } catch (Exception e) {
            throw new IllegalStateException("Exception occurred while trying to open export file", e);
        } finally {
            IOUtils.closeQuietly(zipFile);
            IOUtils.closeQuietly(inputStream);
        }
    }

    private Integer getBuildNumber(final String xmlFile, final String stringPattern) {
        final Pattern pattern = Pattern.compile(stringPattern);
        final Matcher matcher = pattern.matcher(xmlFile);
        if (matcher.find()) {
            return Integer.valueOf(matcher.group(1));
        } else {
            throw new IllegalStateException("Can not find build number for pattern " + stringPattern);
        }
    }

    private void importDataFromFile(final String fileName) {
        final File file = new File(getEnvironmentData().getXMLDataLocation().getAbsolutePath() + "/" + fileName);
        copyFileToJiraImportDirectory(file);
        getTester().gotoPage("secure/admin/XmlRestore!default.jspa");
        getTester().setWorkingForm("restore-xml-data-backup");

        getTester().setFormElement("filename", fileName);
        getTester().setFormElement("license", LicenseKeys.COMMERCIAL.getLicenseString());
        getTester().submit();
        administration.waitForRestore();
        getTester().assertTextPresent("Your import has been successful");
        getTester().assertTextNotPresent("NullPointerException");
    }

    private void copyFileToJiraImportDirectory(final File file) {
        final File jiraImportDirectory = new File(administration.getJiraHomeDirectory(), "import");
        try {
            FileUtils.copyFileToDirectory(file, jiraImportDirectory);
        } catch (IOException e) {
            throw new RuntimeException("Could not copy file " + file.getAbsolutePath() +
                    " to the import directory in jira home " + jiraImportDirectory, e);
        }
    }
}
