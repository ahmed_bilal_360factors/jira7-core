package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.httpclient.api.HttpStatus;
import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.REST, Category.BROWSING})
@RestoreBlankInstance
@LoginAs(user = ADMIN_USERNAME)
public class TestWadlGeneration extends BaseJiraRestTest {

    @Inject
    protected Navigation navigation;

    @Inject
    protected Assertions assertions;

    @Test
    public void applicationWadlRespondsWithNoError() {
        navigation.gotoPage("rest/api/2/application.wadl");
        assertions.assertHttpStatusCode(HttpStatus.OK);
    }
}
