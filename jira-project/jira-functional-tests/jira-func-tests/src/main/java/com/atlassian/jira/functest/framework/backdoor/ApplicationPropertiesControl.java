package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;

/**
 * Use this class from func/selenium/page-object tests that need to manipulate
 * ApplicationProperties.
 * <p>
 * See ApplicationPropertiesBackdoor for the code this plugs into at the back-end.
 *
 * @since v5.0
 */
public class ApplicationPropertiesControl extends com.atlassian.jira.testkit.client.ApplicationPropertiesControl {
    public ApplicationPropertiesControl(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public ApplicationPropertiesControl disableInlineEdit() {
        setOption(APKeys.JIRA_OPTION_DISABLE_INLINE_EDIT, true);
        return this;
    }

    public ApplicationPropertiesControl enableInlineEdit() {
        setOption(APKeys.JIRA_OPTION_DISABLE_INLINE_EDIT, false);
        return this;
    }

    public ApplicationPropertiesControl disableExternalUserManagement() {
        setOption(APKeys.JIRA_OPTION_USER_EXTERNALMGT, false);
        return this;
    }

    public ApplicationPropertiesControl enableExternalUserManagement() {
        setOption(APKeys.JIRA_OPTION_USER_EXTERNALMGT, true);
        return this;
    }

}
