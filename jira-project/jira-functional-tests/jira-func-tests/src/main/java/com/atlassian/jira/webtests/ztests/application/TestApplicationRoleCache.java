package com.atlassian.jira.webtests.ztests.application;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.backdoor.ApplicationRoleControl;
import com.atlassian.jira.functest.framework.backdoor.EventClient;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.matcher.ApplicationRoleBeanMatcher;
import com.atlassian.jira.testkit.beans.UserDTO;
import com.atlassian.jira.webtests.LicenseKeys;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_ADMIN_GROUP;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @since v7.0
 */
@WebTest({Category.FUNC_TEST, Category.API, Category.LICENSING})
public class TestApplicationRoleCache extends BaseJiraFuncTest {
    private static final String JIRA_TEST = "jira-func-test";
    private static final String JIRA_SOFTWARE = "jira-software";

    @Before
    public void setUpTest() {
        backdoor.restoreBlankInstance();
    }

    @After
    public void tearDownTest() {
        backdoor.events().stopAllPollers();
    }

    @Test
    public void testCacheEventsTriggered() {
        final EventClient.EventPoller events = backdoor.events().createPoller();

        final String testGroup = "testGroup";

        backdoor.usersAndGroups().addGroup(testGroup);
        assertThat(events.events(), Matchers.hasItem("com.atlassian.crowd.event.group.GroupCreatedEvent"));

        backdoor.usersAndGroups().addUserToGroup(FRED_USERNAME, testGroup);
        assertThat(events.events(), Matchers.hasItem("com.atlassian.crowd.event.group.GroupMembershipCreatedEvent"));

        backdoor.usersAndGroups().removeUserFromGroup(FRED_USERNAME, testGroup);
        assertThat(events.events(), Matchers.hasItem("com.atlassian.crowd.event.group.GroupMembershipDeletedEvent"));

        backdoor.usersAndGroups().deleteGroup(testGroup);
        assertThat(events.events(), Matchers.hasItem("com.atlassian.crowd.event.group.GroupDeletedEvent"));

        backdoor.license().set(LicenseKeys.CORE_ROLE);
        assertThat(events.events(), Matchers.hasItem("com.atlassian.jira.license.LicenseChangedEvent"));

        //Fred is now not counted because he is not active.
        final UserDTO fred = backdoor.usersAndGroups().getUserByName(FRED_USERNAME);
        backdoor.usersAndGroups().updateUser(deactivate(fred));
        assertThat(events.events(), Matchers.hasItem("com.atlassian.crowd.event.user.UserEditedEvent"));

        //Fred is now counted because he is active again.
        backdoor.usersAndGroups().updateUser(activate(fred));
        assertThat(events.events(), Matchers.hasItem("com.atlassian.crowd.event.user.UserEditedEvent"));
    }

    @Test
    public void testCache() {
        //Make sure the admin can still login once roles are enabled.
        backdoor.applicationRoles().putRole(JIRA_SOFTWARE, JIRA_ADMIN_GROUP);

        final String testGroup = "testGroup";
        final ApplicationRoleControl roles = backdoor.applicationRoles();

        //Configure the TEST role to have testGroup as the default.
        backdoor.license().set(LicenseKeys.TEST_ROLE);
        backdoor.usersAndGroups().addGroup(testGroup);
        roles.putRoleAndSetDefault(JIRA_TEST, testGroup);

        assertThat(roles.getRole(JIRA_TEST), testRoleMatcher()
                .setGroups(testGroup)
                .setDefaultGroups(testGroup)
                .setUserCount(0));

        //Fred is now part of the application.
        backdoor.usersAndGroups().addUserToGroup(FRED_USERNAME, testGroup);
        assertThat(roles.getRole(JIRA_TEST), testRoleMatcher()
                .setGroups(testGroup)
                .setDefaultGroups(testGroup)
                .setUserCount(1));

        //The group is deleted.
        backdoor.usersAndGroups().deleteGroup(testGroup);
        assertThat(roles.getRole(JIRA_TEST), testRoleMatcher()
                .setGroups()
                .setDefaultGroups()
                .setUserCount(0));

        //The group is added back will activate latent configuration.
        backdoor.usersAndGroups().addGroup(testGroup);
        assertThat(roles.getRole(JIRA_TEST), testRoleMatcher()
                .setGroups(testGroup)
                .setDefaultGroups(testGroup)
                .setUserCount(0));

        //Fred is now part of the application (again)
        backdoor.usersAndGroups().addUserToGroup(FRED_USERNAME, testGroup);
        assertThat(roles.getRole(JIRA_TEST), testRoleMatcher()
                .setGroups(testGroup)
                .setDefaultGroups(testGroup)
                .setUserCount(1));

        //Fred is now not counted because he is not active.
        final UserDTO fred = backdoor.usersAndGroups().getUserByName(FRED_USERNAME);
        backdoor.usersAndGroups().updateUser(deactivate(fred));
        assertThat(roles.getRole(JIRA_TEST), testRoleMatcher()
                .setGroups(testGroup)
                .setDefaultGroups(testGroup)
                .setUserCount(0));

        //Fred is now counted because he is active again.
        backdoor.usersAndGroups().updateUser(activate(fred));
        assertThat(roles.getRole(JIRA_TEST), testRoleMatcher()
                .setGroups(testGroup)
                .setDefaultGroups(testGroup)
                .setUserCount(1));

        //Remove fred from the group.
        backdoor.usersAndGroups().removeUserFromGroup(FRED_USERNAME, testGroup);
        assertThat(roles.getRole(JIRA_TEST), testRoleMatcher()
                .setGroups(testGroup)
                .setDefaultGroups(testGroup)
                .setUserCount(0));
    }

    private static UserDTO deactivate(UserDTO userDTO) {
        return setActive(userDTO, false);
    }

    private static UserDTO activate(UserDTO userDTO) {
        return setActive(userDTO, true);
    }

    private static UserDTO setActive(UserDTO userDTO, boolean active) {
        return new UserDTO(active,
                userDTO.getDirectoryId(),
                userDTO.getDisplayName(),
                userDTO.getEmail(),
                userDTO.getKey(),
                userDTO.getName(),
                userDTO.getUsername(),
                null);
    }

    private static ApplicationRoleBeanMatcher testRoleMatcher() {
        return new ApplicationRoleBeanMatcher(JIRA_TEST, "Test Product");
    }
}
