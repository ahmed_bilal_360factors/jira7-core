package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.RENAME_USER;
import static com.atlassian.jira.functest.framework.suite.Category.UPGRADE_TASKS;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Verify that the values in the username column in the table containing OAuth Service Provider Tokens are converted
 * to lowercase if they are mixed case.
 *
 * @since v6.0
 */
@WebTest({FUNC_TEST, RENAME_USER, UPGRADE_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUpgradeTask6047 extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreDataWithBuildNumber("TestUpgradeTask6047.xml", 6030);
    }

    @Test
    public void testSearchForMixedCaseUsernames() {
        // Make sure the token now contains the username in lowercase
        List<Map<String, Object>> tokens = backdoor.entityEngine().findByAnd("OAuthServiceProviderToken", ImmutableMap.of("username", "mixed"));
        assertEquals(1, tokens.size());
        final Map<String, Object> token = tokens.get(0);
        assertThat(token.get("username"), is("mixed"));
    }
}
