package com.atlassian.jira.webtests.ztests.email;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraEmailTest;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.rule.MailTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import javax.mail.internet.MimeMessage;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.EMAIL})
@Restore("TestIssueNotificationsCurrentAssignee.xml")
@MailTest
public class TestIssueEmailSubject extends BaseJiraEmailTest {
    @Inject
    protected Administration administration;

    @Before
    public void setUpTest() {
        navigation.login("admin");
        navigation.userProfile().changeNotifyMyChanges(true);
        administration.generalConfiguration().setAllowUnassignedIssues(true);
    }

    private MimeMessage assertEmailSubject(Runnable setup, String expectedSubjectHeading) throws Exception {
        setup.run();

        final List<MimeMessage> messagesForAdmin = mailHelper.filterMessagesForRecipient(mailHelper.flushMailQueueAndWait(1), "admin@example.com");
        assertEquals(1, messagesForAdmin.size());

        final MimeMessage message = messagesForAdmin.get(0);
        assertEquals(message.getSubject(), expectedSubjectHeading);

        return message;
    }

    private void assertEmailSubject(Runnable setup, String expectedSubjectHeading, String expectedTitle) throws Exception {
        final MimeMessage message = assertEmailSubject(setup, expectedSubjectHeading);

        assertEmailTitleEquals(message, expectedTitle);
    }

    @Test
    public void testCreateIssue() throws Exception {
        assertEmailSubject(new Runnable() {
            public void run() {
                navigation.issue().createIssue("COW", null, "New issue");
            }
        }, "[JIRATEST] (COW-4) New issue", "New issue");
    }

    @Test
    public void testIssueUpdated() throws Exception {
        assertEmailSubject(new Runnable() {
            public void run() {
                navigation.issue().setDescription("COW-2", "Updated text");
            }
        }, "[JIRATEST] (COW-2) This cow has a calf");
    }

    @Test
    public void testAssignIssue() throws Exception {
        assertEmailSubject(new Runnable() {
            public void run() {
                navigation.issue().unassignIssue("COW-2", "this is a comment");
            }
        }, "[JIRATEST] (COW-2) This cow has a calf");
    }

    @Test
    public void testIssueResolved() throws Exception {
        assertEmailSubject(new Runnable() {
            public void run() {
                navigation.issue().resolveIssue("COW-2", "Fixed", "Yay!");
            }
        }, "[JIRATEST] (COW-2) This cow has a calf");
    }

    @Test
    public void testIssueCommented() throws Exception {
        assertEmailSubject(new Runnable() {
            public void run() {
                navigation.issue().addComment("COW-2", "jaisodf", null);
            }
        }, "[JIRATEST] (COW-2) This cow has a calf");
    }

    @Test
    public void testIssueDeleted() throws Exception {
        assertEmailSubject(new Runnable() {
            public void run() {
                navigation.issue().deleteIssue("COW-3");
            }
        }, "[JIRATEST] (COW-3) A calf is a tasty little renet factory");
    }
}
