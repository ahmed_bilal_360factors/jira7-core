package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.UPGRADE_TASKS;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Ensure max(app_user.id) is correctly stored after the upgrade task runs
 *
 * @since v6.5
 */
@WebTest({FUNC_TEST, UPGRADE_TASKS})
public class TestUpgradeTask65001 extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Test
    public void maxUserIdIsCorrectlyStored() {
        administration.restoreData("TestUpgradeTask65001-no-id-stored.xml");
        assertThat("stored id is equals to max(id) from app_user", backdoor.applicationProperties().getString(APKeys.ONBOARDING_APP_USER_ID_THRESHOLD), is("10201"));
    }

    @Test
    public void maxUserIdIsNotChangedIfAlreadyPresent() {
        administration.restoreData("TestUpgradeTask65001-id-already-stored.xml");
        assertThat("stored id should remain unchanged", backdoor.applicationProperties().getString(APKeys.ONBOARDING_APP_USER_ID_THRESHOLD), is("10100"));
    }
}
