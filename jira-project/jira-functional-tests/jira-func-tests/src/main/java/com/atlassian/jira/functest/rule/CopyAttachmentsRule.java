package com.atlassian.jira.functest.rule;

import com.atlassian.jira.testkit.client.AttachmentsControl;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.io.File;
import java.util.function.Supplier;

/**
 * Copies attachments before a test.
 *
 * @since v6.4
 */
class CopyAttachmentsRule implements TestRule {
    private final Supplier<JIRAEnvironmentData> environmentDataSupplier;
    private final Supplier<AttachmentsControl> attachmentsControlSupplier;
    private final String sourceSubPath;

    CopyAttachmentsRule(
            final Supplier<JIRAEnvironmentData> environmentDataSupplier,
            final Supplier<AttachmentsControl> attachmentsControlSupplier,
            final String sourceSubPath
    ) {
        this.environmentDataSupplier = environmentDataSupplier;
        this.attachmentsControlSupplier = attachmentsControlSupplier;
        this.sourceSubPath = sourceSubPath;
    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                final File dataDirectory = environmentDataSupplier.get().getXMLDataLocation();
                final File sourceDirectory = new File(dataDirectory, sourceSubPath);
                final String attachmentPath = attachmentsControlSupplier.get().getAttachmentPath();
                final File targetDirectory = new File(attachmentPath);
                final TestRule copyDirectoryRule = Rules.copyDirectory(sourceDirectory, targetDirectory);
                copyDirectoryRule.apply(base, description).evaluate();
            }
        };
    }
}
