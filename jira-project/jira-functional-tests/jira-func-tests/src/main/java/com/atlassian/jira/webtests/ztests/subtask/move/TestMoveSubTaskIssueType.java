package com.atlassian.jira.webtests.ztests.subtask.move;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.SUB_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestMoveSubTaskIssueType extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Before
    public void setUp() {
        administration.restoreData("TestMoveSubTaskIssueType.xml");
    }

    @After
    public void tearDown() {
        administration.restoreBlankInstance();
    }

    @Test
    public void testHappy() {
        navigation.issue().gotoIssue("HSP-2");
        tester.clickLink("move-issue");
        tester.assertRadioOptionPresent("operation", "move.subtask.type.operation.name");
        tester.assertRadioOptionPresent("operation", "move.subtask.parent.operation.name");
        tester.checkCheckbox("operation", "move.subtask.type.operation.name");
        tester.submit("Next >>");
        tester.assertTextPresent("Sub-task 2");
        tester.assertTextNotPresent("Sub-task 3");
    }

    @Test
    public void testNoOtherSubsInProject() {
        navigation.gotoAdminSection(Navigation.AdminSection.SUBTASKS);
        tester.clickLink("del_Sub-task 2");
        tester.submit("Delete");
        navigation.issue().gotoIssue("HSP-2");
        tester.clickLink("move-issue");
        tester.assertRadioOptionNotPresent("operation", "move.subtask.type.operation.name");
        tester.assertRadioOptionPresent("operation", "move.subtask.parent.operation.name");
        tester.assertTextPresent("There are no other sub-task issue types associated with this project.");

    }

    @Test
    public void testNoScheme() {
        Long projectId = backdoor.project().getProjectId("HSP");
        tester.gotoPage("/secure/admin/SelectIssueTypeSchemeForProject!default.jspa?projectId=" + projectId);

        tester.checkCheckbox("createType", "chooseScheme");
        tester.selectOption("schemeId", "Default Issue Type Scheme");
        tester.submit(" OK ");
        navigation.issue().gotoIssue("HSP-2");
        tester.clickLink("move-issue");
        tester.assertRadioOptionPresent("operation", "move.subtask.type.operation.name");
        tester.assertRadioOptionPresent("operation", "move.subtask.parent.operation.name");
        tester.checkCheckbox("operation", "move.subtask.type.operation.name");
        tester.submit("Next >>");
        tester.assertTextPresent("Sub-task 2");
        tester.assertTextPresent("Sub-task 3");
    }

    @Test
    public void testNoOtherSubsAtAll() {
        navigation.gotoAdminSection(Navigation.AdminSection.SUBTASKS);
        tester.clickLink("del_Sub-task 2");
        tester.submit("Delete");
        tester.clickLink("del_Sub-task 3");
        tester.submit("Delete");

        Long projectId = backdoor.project().getProjectId("HSP");
        tester.gotoPage("/secure/admin/SelectIssueTypeSchemeForProject!default.jspa?projectId=" + projectId);
        tester.checkCheckbox("createType", "chooseScheme");
        tester.selectOption("schemeId", "Default Issue Type Scheme");
        tester.submit(" OK ");
        navigation.issue().gotoIssue("HSP-2");
        tester.clickLink("move-issue");
        tester.assertRadioOptionNotPresent("operation", "move.subtask.type.operation.name");
        tester.assertRadioOptionPresent("operation", "move.subtask.parent.operation.name");
        tester.assertTextPresent("There are no other sub-task issue types defined in the system.");

    }
}
