package com.atlassian.jira.webtests.ztests.plugin;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Verifying that issues can be created during a transition
 *
 * @since v4.4
 */
@WebTest({Category.FUNC_TEST, Category.PLUGINS, Category.REFERENCE_PLUGIN})
@LoginAs(user = ADMIN_USERNAME)
public class TestCreateIsueinPostFunction extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Test
    public void testCreateIssues() throws Exception {
        administration.restoreData("TestCreateIssueDuringTransition.xml");
        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("action_id_711");
        navigation.issueNavigator().displayAllIssues();
        tester.assertLinkPresentWithText("HSP-1");
        tester.assertLinkPresentWithText("HSP-2");
        tester.assertLinkPresentWithText("HSP-3");

        tester.clickLinkWithText("HSP-2");
        tester.assertLinkPresentWithText("HSP-2");
        navigation.issueNavigator().displayAllIssues();
        tester.clickLinkWithText("HSP-3");
        tester.assertLinkPresentWithText("HSP-3");
        navigation.issueNavigator().displayAllIssues();
    }


}
