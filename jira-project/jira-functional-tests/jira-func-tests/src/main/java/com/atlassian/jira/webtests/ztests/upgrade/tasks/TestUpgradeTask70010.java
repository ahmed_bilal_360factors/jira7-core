package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.service.JiraService;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.google.common.collect.Maps;
import org.hamcrest.Matcher;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.UPGRADE_TASKS;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Tests upgrade tasks 70009 and 70010.
 * <p>
 * UpgradeTask_Build70009 is covered through the use of broken cron expressions in the restore file.
 * Part of the 70010 logic is also covered in the tests for 70011, as which upgrade task deals with
 * repairing broken filter subscriptions depends on the exact upgrade path (specifically, whether the
 * atlassian-schedule migration already happened or if it is happening at the same time as the Caesium
 * migration).  See {@link TestUpgradeTask70011} for more info.
 * </p>
 *
 *
 * @since v7.0.0
 */
@Restore("TestUpgradeTask70010.xml")
@WebTest({FUNC_TEST, UPGRADE_TASKS})
public class TestUpgradeTask70010 extends BaseJiraFuncTest {
    private static final JobRunnerKey NO_TIME_ZONE_JOB_RUNNER_KEY = JobRunnerKey.of("NoTimeZoneJob");
    private static final JobRunnerKey SERVICE_JOB_RUNNER_KEY =
            JobRunnerKey.of("com.atlassian.jira.service.DefaultServiceManager");
    private static final String SERVICE_PREFIX = JiraService.class.getName() + ':';

    @Test
    public void testFiltersSubscriptions() {
        assertEquals("0 0 1 ? * *", backdoor.filterSubscriptions().getCronForSubscription(10010L));
        assertEquals("0 0 3 ? * *", backdoor.filterSubscriptions().getCronForSubscription(10021L));
        assertEquals("0 0 4 ? * *", backdoor.filterSubscriptions().getCronForSubscription(10022L));
        assertEquals("0 0 5 ? * *", backdoor.filterSubscriptions().getCronForSubscription(10023L));
        assertEquals("0 2 1 ? * *", backdoor.filterSubscriptions().getCronForSubscription(10024L));
    }

    @Test
    public void testSchedulerEntries() {
        final Map<String, String> cronExpressions = new HashMap<>();
        backdoor.scheduler().getJobsByJobRunnerKey(NO_TIME_ZONE_JOB_RUNNER_KEY)
                .forEach(job -> cronExpressions.put(job.jobId, job.cronExpression));
        backdoor.scheduler().getJobsByJobRunnerKey(SERVICE_JOB_RUNNER_KEY)
                .forEach(job -> cronExpressions.put(job.jobId, job.cronExpression));

        assertThat(cronExpressions, allOf(
                hasEntry("NoTimeZone", "0 0 10 * * ?"),
                service(10000L, "0 * * * * ?"),
                service(10001L, "0 0 * * * ?"),  // repaired (invalid '/60' removed)
                service(10002L, "0 0 * * * ?"),  // repaired (extra '*' removed)
                not(service(10003L, "m000000000!!!"))));  // still broken, so did not get scheduled
    }

    @Test
    public void testServiceConfigs() {
        final Map<String, String> services = Maps.newHashMap();
        backdoor.services().getServices().forEach(service -> services.put(service.name, service.cronExpression));
        assertThat(services, allOf(
                hasEntry("Mail Queue Service", "0 * * * * ?"),
                hasEntry("Debug Service 1", "0 0 * * * ?"),  // repaired (invalid '/60' removed)
                hasEntry("Debug Service 2", "0 0 * * * ?"),  // repaired (extra '*' removed)
                hasEntry("Debug Service 3", "m000000000!!!")));  // still broken
    }

    private static Matcher<Map<? extends String, ? extends String>> service(
            long serviceId, String expectedCronExpression) {
        return hasEntry(SERVICE_PREFIX + serviceId, expectedCronExpression);
    }
}
