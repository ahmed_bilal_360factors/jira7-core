package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FuncTestRestClient;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.webtests.LicenseKeys;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.meterware.httpunit.WebResponse;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static java.util.stream.Stream.concat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Ensure the application conform to Platform's specification.
 *
 * @since 4.3.
 */
@WebTest({Category.DEV_MODE, Category.PLATFORM_COMPATIBILITY})
@LoginAs(user = ADMIN_USERNAME)
public class TestPlatformCompatibility extends BaseJiraFuncTest {
    public static final String PROJECT_NAME = "tautomerism";
    public static final String ISSUE_SUMMARY = "jira blah blah";
    public static final String ISSUE_DESCRIPTION = "ho ho ho and a bottle of rum";

    /**
     * The CTK requires the following system properties since version 2.14. See https://extranet.atlassian.com/x/IpPadw.
     */
    private static final ImmutableMap<String, String> CTK_PROPERTIES = ImmutableMap.<String, String>builder()
            .put("platform.ctk.test.admin.username", FunctTestConstants.ADMIN_USERNAME)
            .put("platform.ctk.test.admin.password", FunctTestConstants.ADMIN_PASSWORD)
            .put("platform.ctk.test.admin.fullname", FunctTestConstants.ADMIN_FULLNAME)
            .put("platform.ctk.test.validlicense", LicenseKeys.OPEN_SOURCE.getLicenseString())
            .put("platform.ctk.test.search.term", ISSUE_SUMMARY)
            .put("platform.ctk.test.search.matches", ISSUE_DESCRIPTION)
            .build();

    private static final String PLATFORM_CTK_PLUGIN_KEY = "com.atlassian.refapp.ctk";
    private static final String WEBSUDO_PROPERTY = "jira.websudo.is.disabled";

    @Inject
    private FuncTestRestClient restClient;

    private boolean shouldRun;
    private Boolean webSudoEnabled = null;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();

        shouldRun = isPlatformCtkPluginInstalled();

        if (shouldRun) {
            administration.project().addProject("tautomerism", "TTM", ADMIN_USERNAME);
            navigation.issue().createIssue(PROJECT_NAME, "Bug", ISSUE_SUMMARY, ImmutableMap.of("description", new String[]{ISSUE_DESCRIPTION}));
            administration.reIndex();
            navigation.logout();

            // save the "before" WebSudo state
            webSudoEnabled = Boolean.valueOf(backdoor.systemProperties().getProperty(WEBSUDO_PROPERTY));
            backdoor.systemProperties().setProperty(WEBSUDO_PROPERTY, "false");

            // set the required CTK system properties
            for (final String property : CTK_PROPERTIES.keySet()) {
                backdoor.systemProperties().setProperty(property, CTK_PROPERTIES.get(property));
            }
            // set the required DarkFeatures up
            backdoor.darkFeatures().enableForSite("foo");
        }
    }

    @After
    public void tearDownTest() {
        if (shouldRun) {
            // unset all the system properties
            for (final String property : CTK_PROPERTIES.keySet()) {
                backdoor.systemProperties().unsetProperty(property);
            }

            // restore the WebSudo state
            backdoor.systemProperties().setProperty(WEBSUDO_PROPERTY, webSudoEnabled.toString());
        }

    }

    @Test
    public void testCtk() throws IOException, SAXException, JSONException {
        if (shouldRun) {
            logger.log("found platform-ctk plugin. run it now!!");
            final List<String> skipClasses = getSkippedClasses();

            if (skipClasses.size() == 0) {
                logger.log("No skipped test. Full suite will be executed.");
            } else {
                logger.log("Tests to be skipped: " + StringUtils.join(skipClasses, "\n"));
            }

            final String queryParams = buildQueryParams(concat(
                    toQueryParam("outdir", getTestsOutputDirPath()),
                    toQueryParams("excludes", skipClasses)
            ));

            final WebResponse response = restClient.GET(
                    "/rest/functest/1.0/junit/runTests?" + queryParams,
                    ImmutableMap.of("Accept", "application/json"));

            // check that the output is in good format.
            assertEquals("application/json", response.getContentType());
            assertEquals("UTF-8", response.getCharacterSet());

            final JSONObject contents = new JSONObject(response.getText());

            // zero here means no test is failing.
            final int failures = Integer.parseInt(contents.getString("result"));
            if (failures > 0) {
                fail("There were " + failures + " failures. See test output below.\n\n\n" + contents.getString("output"));
            }
        } else {
            logger.log("platform-ctk plugin not found. skipped the test");
        }
    }

    private Stream<String> toQueryParams(final String paramName, final List<String> skipClasses) {
        return skipClasses.stream().map((paramValues) -> paramName + "=" + paramValues);
    }

    private Stream<String> toQueryParam(final String paramName, final String value) {
        return Stream.of(paramName + "=" + value);
    }

    private String buildQueryParams(final Stream<String> params) {
        return params.collect(Collectors.joining("&"));
    }

    private List<String> getSkippedClasses() {
        // classes to be skipped in comma-separated values.n
        final String skips = System.getProperty("platform.ctk.skips");
        if (skips == null) {
            return Collections.emptyList();
        }

        return ImmutableList.copyOf(StringUtils.split(skips, ","));
    }

    private boolean isPlatformCtkPluginInstalled() {
        navigation.gotoAdmin();
        return administration.plugins().isPluginInstalled(PLATFORM_CTK_PLUGIN_KEY);
    }

    private String getTestsOutputDirPath() {
        final File target = new File("target");
        if (target.exists()) {
            final File outputDir = new File("target/ctk-test-reports/");
            return outputDir.getAbsolutePath();
        } else {
            return "ctk-test-reports";
        }
    }
}
