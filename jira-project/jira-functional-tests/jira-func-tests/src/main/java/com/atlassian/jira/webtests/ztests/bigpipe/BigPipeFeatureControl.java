package com.atlassian.jira.webtests.ztests.bigpipe;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.functest.framework.backdoor.InstanceFeaturesControl;

import static java.util.Optional.of;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

class BigPipeFeatureControl {
    private static final String BIG_PIPE_FEATURE = "com.atlassian.jira.config.BIG_PIPE";
    private static final String BIG_PIPE_KILLSWITCH_FEATURE = "com.atlassian.jira.config.BIG_PIPE_KILLSWITCH";

    private final Backdoor backdoor;
    private final boolean oldBigPipeState;
    private final boolean oldBigPipeKillswitchState;

    BigPipeFeatureControl(Backdoor backdoor) {
        this.backdoor = backdoor;
        oldBigPipeState = backdoor.instanceFeaturesControl().isEnabled(BIG_PIPE_FEATURE);
        oldBigPipeKillswitchState = backdoor.instanceFeaturesControl().isEnabled(BIG_PIPE_KILLSWITCH_FEATURE);
    }

    public void restore() {
        setBigPipeEnabled(oldBigPipeState);
        setBigPipeKillswitchEnabled(oldBigPipeKillswitchState);
    }

    private boolean setFeatureEnabled(final String feature, final boolean enabled) {
        InstanceFeaturesControl instanceFeaturesControl = backdoor.instanceFeaturesControl();
        boolean oldState = instanceFeaturesControl.isEnabled(feature);

        if (oldState != enabled) {
            of(feature).ifPresent(enabled ? instanceFeaturesControl::enable : instanceFeaturesControl::disable);

            //Verify setting took effect
            boolean newState = instanceFeaturesControl.isEnabled(feature);
            assertThat("Failed to set " + feature + ".", newState, is(enabled));
        }

        return oldState;
    }

    public boolean setBigPipeEnabled(boolean enabled) {
        return setFeatureEnabled(BIG_PIPE_FEATURE, enabled);
    }

    public boolean setBigPipeKillswitchEnabled(boolean enabled) {
        return setFeatureEnabled(BIG_PIPE_KILLSWITCH_FEATURE, enabled);
    }

}
