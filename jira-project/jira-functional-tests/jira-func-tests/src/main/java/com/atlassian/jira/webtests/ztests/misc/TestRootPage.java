package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import java.net.HttpURLConnection;
import java.net.URL;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@WebTest({Category.FUNC_TEST, Category.BROWSING})
public class TestRootPage extends BaseJiraFuncTest {

    @Test
    public void shouldNotRedirect() throws Exception {
        // don't use httpunit, as it logs you in and establishes a session
        // we want a "pure" http request that we can reason about

        String url = getEnvironmentData().getBaseUrl().toExternalForm();
        if (!url.endsWith("/")) {
            // we want to hit default.jsp, not tomcat's redirector
            url += "/";
        }
        URL baseUrl = new URL(url);
        HttpURLConnection uc = (HttpURLConnection) baseUrl.openConnection();
        uc.connect();

        int responseCode = uc.getResponseCode();
        assertThat("expect no redirect", responseCode, equalTo(200));
    }
}
