package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.admin.TestIssueLinkCheck;
import com.atlassian.jira.webtests.ztests.admin.issuetypes.TestIssueTypeSchemeMigration;
import com.atlassian.jira.webtests.ztests.admin.issuetypes.TestIssueTypeSchemes;
import com.atlassian.jira.webtests.ztests.admin.issuetypes.TestIssueTypes;
import com.atlassian.jira.webtests.ztests.admin.issuetypes.TestMultiIssueTypes;
import com.atlassian.jira.webtests.ztests.attachment.TestIssueFileAttachments;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkChangeIssues;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkDeleteIssues;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkEditIssues;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkMoveIssues;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkMoveIssuesForEnterprise;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkMoveMappingVersionsAndComponents;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkOperationIssueNavigator;
import com.atlassian.jira.webtests.ztests.email.TestBulkDeleteIssuesNotifications;
import com.atlassian.jira.webtests.ztests.email.TestIssueNotifications;
import com.atlassian.jira.webtests.ztests.fields.TestResolutionDateField;
import com.atlassian.jira.webtests.ztests.hints.TestHints;
import com.atlassian.jira.webtests.ztests.indexing.TestIndexingResources;
import com.atlassian.jira.webtests.ztests.issue.TestCloneIssueWithSubTasks;
import com.atlassian.jira.webtests.ztests.issue.TestCloneIssueWithValidation;
import com.atlassian.jira.webtests.ztests.issue.TestCreateConcurrentIssues;
import com.atlassian.jira.webtests.ztests.issue.TestCreateIssue;
import com.atlassian.jira.webtests.ztests.issue.TestCreateIssueForEnterprise;
import com.atlassian.jira.webtests.ztests.issue.TestCreateIssueNoBrowseProjectPermission;
import com.atlassian.jira.webtests.ztests.issue.TestCreateIssueViaDirectLink;
import com.atlassian.jira.webtests.ztests.issue.TestEditIssue;
import com.atlassian.jira.webtests.ztests.issue.TestEditIssueFields;
import com.atlassian.jira.webtests.ztests.issue.TestEntityPropertyEqualToCondition;
import com.atlassian.jira.webtests.ztests.issue.TestInlineEditIssueFields;
import com.atlassian.jira.webtests.ztests.issue.TestInlineEditIssueFieldsExceedingLimit;
import com.atlassian.jira.webtests.ztests.issue.TestInlineIssueLinking;
import com.atlassian.jira.webtests.ztests.issue.TestIssueActionErrors;
import com.atlassian.jira.webtests.ztests.issue.TestIssueBrowseBadProjectRegex;
import com.atlassian.jira.webtests.ztests.issue.TestIssueConstants;
import com.atlassian.jira.webtests.ztests.issue.TestIssueHeader;
import com.atlassian.jira.webtests.ztests.issue.TestIssueOperations;
import com.atlassian.jira.webtests.ztests.issue.TestIssueOperationsCustomFieldTypeWithNoValidation;
import com.atlassian.jira.webtests.ztests.issue.TestIssueOperationsOnDeletedIssue;
import com.atlassian.jira.webtests.ztests.issue.TestIssueOperationsWithLimitedPermissions;
import com.atlassian.jira.webtests.ztests.issue.TestIssuePrintableView;
import com.atlassian.jira.webtests.ztests.issue.TestIssueSecurityActions;
import com.atlassian.jira.webtests.ztests.issue.TestIssueSecurityWithCustomFields;
import com.atlassian.jira.webtests.ztests.issue.TestIssueSecurityWithGroupsAndRoles;
import com.atlassian.jira.webtests.ztests.issue.TestIssueSecurityWithRoles;
import com.atlassian.jira.webtests.ztests.issue.TestIssueTabPanels;
import com.atlassian.jira.webtests.ztests.issue.TestIssueViews;
import com.atlassian.jira.webtests.ztests.issue.TestLabelSuggestions;
import com.atlassian.jira.webtests.ztests.issue.TestLabels;
import com.atlassian.jira.webtests.ztests.issue.TestLabelsFormats;
import com.atlassian.jira.webtests.ztests.issue.TestLinkIssue;
import com.atlassian.jira.webtests.ztests.issue.TestManageLinkClosedIssues;
import com.atlassian.jira.webtests.ztests.issue.TestMetaTagOrdering;
import com.atlassian.jira.webtests.ztests.issue.TestOpsBarStructure;
import com.atlassian.jira.webtests.ztests.issue.TestSearchXmlCustomIssueView;
import com.atlassian.jira.webtests.ztests.issue.TestTextFieldCharacterLengthValidator;
import com.atlassian.jira.webtests.ztests.issue.TestViewIssue;
import com.atlassian.jira.webtests.ztests.issue.TestViewIssueWithRest;
import com.atlassian.jira.webtests.ztests.issue.TestXmlCustomIssueView;
import com.atlassian.jira.webtests.ztests.issue.TestXmlIssueView;
import com.atlassian.jira.webtests.ztests.issue.TestXmlIssueViewBackwardCompatibility;
import com.atlassian.jira.webtests.ztests.issue.TestXmlIssueViewErrors;
import com.atlassian.jira.webtests.ztests.issue.TestXmlIssueViewXss;
import com.atlassian.jira.webtests.ztests.issue.assign.TestAssignIssue;
import com.atlassian.jira.webtests.ztests.issue.clone.TestCloneIssueLinking;
import com.atlassian.jira.webtests.ztests.issue.links.TestReindexOnLinkChange;
import com.atlassian.jira.webtests.ztests.issue.move.TestMoveIssue;
import com.atlassian.jira.webtests.ztests.issue.move.TestMoveIssueAndRemoveFields;
import com.atlassian.jira.webtests.ztests.issue.move.TestMoveIssueAssignee;
import com.atlassian.jira.webtests.ztests.issue.move.TestMoveIssueForEnterprise;
import com.atlassian.jira.webtests.ztests.issue.move.TestMoveIssueNotifications;
import com.atlassian.jira.webtests.ztests.issue.move.TestRedirectToMovedIssues;
import com.atlassian.jira.webtests.ztests.issue.security.xss.TestXssInCommentUserLink;
import com.atlassian.jira.webtests.ztests.issue.security.xss.TestXssOnCreateIssueDetailsPage;
import com.atlassian.jira.webtests.ztests.misc.TestReplacedLocalVelocityMacros;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigator;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorColumnLinks;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorCsvView;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorEncoding;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorExcelView;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorFullContentView;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorPrintableView;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorRssView;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorWordView;
import com.atlassian.jira.webtests.ztests.project.TestMultipleProjectsWithIssueSecurityWithRoles;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubTaskConversionParentPicker;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubTaskConversionSecurityLevel;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubTaskConversionStep1;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubTaskConversionStep2;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubTaskConversionSystemFields;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubTaskConversionVariousOperations;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubTaskConversionWithFields;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubtaskConversionNotifications;
import com.atlassian.jira.webtests.ztests.subtask.TestSubTaskToIssueConversionSecurityLevel;
import com.atlassian.jira.webtests.ztests.subtask.TestSubTaskToIssueConversionStep1;
import com.atlassian.jira.webtests.ztests.subtask.move.TestMoveSubTaskIssueType;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of tests around Issues.  Obviously this suite could be quite large
 *
 * @since v4.0
 */
public class FuncTestSuiteIssues {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestIssueConstants.class)
                .add(TestIssueTabPanels.class)
                .add(TestIssueLinkCheck.class)
                .add(TestAssignIssue.class)
                .add(TestCreateIssue.class)
                .add(TestIssueNavigator.class)
                .add(TestIssueHeader.class)
                .add(TestEditIssue.class)
                .add(TestEditIssueFields.class)
                .add(TestIssueOperations.class)
                .add(TestIssueOperationsOnDeletedIssue.class)
                .add(TestIssueOperationsWithLimitedPermissions.class)
                .add(TestIssueOperationsCustomFieldTypeWithNoValidation.class)
                .add(TestMoveIssue.class)
                .add(TestMoveIssueNotifications.class)
                .add(TestMoveIssueAssignee.class)
                .add(TestLinkIssue.class)
                .add(TestIssueFileAttachments.class)
                .add(TestIssueTypes.class)
                .add(TestIssueSecurityActions.class)
                .add(TestIssueSecurityWithGroupsAndRoles.class)
                .add(TestIssueSecurityWithCustomFields.class)
                .add(TestIssueSecurityWithRoles.class)
                .add(TestCreateIssueForEnterprise.class)
                .add(TestMoveIssueForEnterprise.class)
                .add(TestInlineIssueLinking.class)
                .add(TestMoveSubTaskIssueType.class)
                .add(TestIssueTypeSchemes.class)
                .add(TestIssueToSubTaskConversionSystemFields.class)
                .add(TestIssueToSubTaskConversionVariousOperations.class)
                .add(TestIssueToSubTaskConversionStep1.class)
                .add(TestIssueToSubTaskConversionStep2.class)
                .add(TestIssueToSubTaskConversionWithFields.class)
                .add(TestIssueToSubTaskConversionParentPicker.class)
                .add(TestIssueToSubtaskConversionNotifications.class)
                .add(TestIssueToSubTaskConversionSecurityLevel.class)
                .add(TestSubTaskToIssueConversionStep1.class)
                .add(TestSubTaskToIssueConversionSecurityLevel.class)
                .add(TestBulkChangeIssues.class)
                .add(TestBulkMoveIssues.class)
                .add(TestBulkMoveIssuesForEnterprise.class)
                .add(TestBulkMoveMappingVersionsAndComponents.class)
                .add(TestBulkEditIssues.class)
                .add(TestBulkDeleteIssues.class)
                .add(TestMultiIssueTypes.class)
                .add(TestIssueTypeSchemeMigration.class)
                .add(TestCreateIssueViaDirectLink.class)
                .add(TestCloneIssueWithSubTasks.class)
                .add(TestIssueNavigatorColumnLinks.class)
                .add(TestIssueNavigatorPrintableView.class)
                .add(TestIssueNavigatorExcelView.class)
                .add(TestIssueNavigatorCsvView.class)
                .add(TestIssueNavigatorRssView.class)
                .add(TestIssueNavigatorFullContentView.class)
                .add(TestIssueNavigatorWordView.class)
                .add(TestIssueViews.class)
                .add(TestXmlIssueView.class)
                .add(TestXmlIssueViewXss.class)
                .add(TestIssuePrintableView.class)
                .add(TestViewIssue.class)
                .add(TestIssueBrowseBadProjectRegex.class)
                .add(TestRedirectToMovedIssues.class)
                .add(TestManageLinkClosedIssues.class)
                .add(TestMultipleProjectsWithIssueSecurityWithRoles.class)
                .add(TestMoveIssueAndRemoveFields.class)
                .add(TestBulkOperationIssueNavigator.class)
                .add(TestIssueNotifications.class)
                .add(TestBulkDeleteIssuesNotifications.class)
                .add(TestIssueNavigatorEncoding.class)
                .add(TestResolutionDateField.class)
                .add(TestXmlIssueViewBackwardCompatibility.class)
                .add(TestSearchXmlCustomIssueView.class)
                .add(TestXmlCustomIssueView.class)
                .add(TestXmlIssueViewErrors.class)
                .add(TestCloneIssueLinking.class)
                .add(TestReindexOnLinkChange.class)
                .add(TestOpsBarStructure.class)
                .add(TestXmlIssueViewXss.class)
                .add(TestXssInCommentUserLink.class)
                .add(TestXssOnCreateIssueDetailsPage.class)
                .add(TestTextFieldCharacterLengthValidator.class)

                        //Has tests for issue view.
                .add(TestReplacedLocalVelocityMacros.class)

                .add(TestLabels.class)
                .add(TestIssueActionErrors.class)

                .add(TestLabelsFormats.class)

                .add(TestHints.class)
                .add(TestCreateConcurrentIssues.class)
                .add(TestViewIssueWithRest.class)
                .add(TestInlineEditIssueFields.class)
                .add(TestInlineEditIssueFieldsExceedingLimit.class)
                .add(TestMetaTagOrdering.class)
                .add(TestIndexingResources.class)

                .add(TestCloneIssueWithValidation.class)
                .add(TestEntityPropertyEqualToCondition.class)
                .add(TestCreateIssueNoBrowseProjectPermission.class)
                .add(TestLabelSuggestions.class)
                .build();
    }
}
