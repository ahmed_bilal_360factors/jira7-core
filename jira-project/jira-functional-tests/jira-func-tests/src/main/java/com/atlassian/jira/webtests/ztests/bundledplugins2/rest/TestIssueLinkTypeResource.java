package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.FuncTestUrlHelper;
import com.atlassian.jira.functest.framework.admin.IssueLinking;
import com.atlassian.jira.functest.framework.admin.IssueLinkingImpl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.IssueLinkType;
import com.atlassian.jira.testkit.client.restclient.IssueLinkTypeClient;
import com.atlassian.jira.testkit.client.restclient.IssueLinkTypes;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @since v4.3
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@Restore("TestIssueLinkType.xml")
public class TestIssueLinkTypeResource extends BaseJiraRestTest {

    private IssueLinkTypeClient issueLinkTypeClient;

    @Inject
    private FuncTestUrlHelper urlHelper;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    @Before
    @Test
    public void setUpTest() {
        issueLinkTypeClient = new IssueLinkTypeClient(environmentData);
    }


    @Test
    public void testGetAllIssueLinkTypes() throws Exception {
        final IssueLinkTypes issueLinkTypes = issueLinkTypeClient.getIssueLinkTypes();
        final List<IssueLinkType> list = issueLinkTypes.issueLinkTypes;
        Assert.assertEquals(2, list.size());
        IssueLinkType type = list.get(0);
        Assert.assertEquals(type.name, "Blocks");
        Assert.assertEquals("Blocks", type.outward);
        Assert.assertEquals("Blocked by", type.inward);
        Assert.assertEquals(new Long(10100).intValue(), type.id.intValue());
        Assert.assertEquals(urlHelper.getBaseUrlPlus("rest/api/2/issueLinkType/10100"), type.self.toString());
        type = list.get(1);
        Assert.assertEquals(type.name, "Duplicate");
        Assert.assertEquals("Duplicates", type.outward);
        Assert.assertEquals("Duplicated by", type.inward);
        Assert.assertEquals(new Long(10000).intValue(), type.id.intValue());
        Assert.assertEquals(urlHelper.getBaseUrlPlus("rest/api/2/issueLinkType/10000"), type.self.toString());
    }

    @Test
    public void testGetAllIssueLinkTypesReturns404WhenIssueLinkingDisabled() throws Exception {
        backdoor.issueLinking().disable();
        Assert.assertEquals(404, issueLinkTypeClient.getResponseForAllLinkTypes().statusCode);
    }

    @Test
    public void testGetAllIssueLinkTypesAnonymousUserAllowed() throws Exception {
        final IssueLinkTypes issueLinkTypes = issueLinkTypeClient.anonymous().getIssueLinkTypes();
        final List<IssueLinkType> list = issueLinkTypes.issueLinkTypes;
        Assert.assertEquals(2, list.size());
    }

    @Test
    public void testGetIssueLinkTypeReturns404WhenIssueLinkingDisabled() throws Exception {
        backdoor.issueLinking().disable();
        Assert.assertEquals(404, issueLinkTypeClient.getResponseForLinkType("10000").statusCode);
    }

    @Test
    public void testCreateIssueLinkType() throws Exception {
        IssueLinkType linkType = issueLinkTypeClient.createIssueLinkType("New Thing", "inbound", "outbound");
        assertThat(linkType.name, is("New Thing"));
        assertThat(linkType.inward, is("inbound"));
        assertThat(linkType.outward, is("outbound"));
    }

    @Test
    public void testDeleteIssueLinkType() throws Exception {
        IssueLinkType linkType = issueLinkTypeClient.createIssueLinkType("New Thing", "inbound", "outbound");
        Response response = issueLinkTypeClient.deleteIssueLinkType(linkType.id.toString());
        assertThat(response.statusCode, is(204));

        try {
            issueLinkTypeClient.getIssueLinkType(linkType.id.toString());
            Assert.fail();
        } catch (UniformInterfaceException e) {
            assertThat(e.getResponse().getClientResponseStatus(), is(ClientResponse.Status.NOT_FOUND));
        }
    }

    @Test
    public void testGetIssueLinkType() throws Exception {
        final IssueLinkType type = issueLinkTypeClient.anonymous().getIssueLinkType("10000");
        Assert.assertEquals(type.name, "Duplicate");
        Assert.assertEquals("Duplicates", type.outward);
        Assert.assertEquals("Duplicated by", type.inward);
        Assert.assertEquals(new Long(10000).intValue(), type.id.intValue());
    }

    @Test
    public void testGetIssueLinkTypeIssueLinkTypeNotFound() throws Exception {
        final Response response = issueLinkTypeClient.getResponseForLinkType("10012");
        Assert.assertEquals(404, response.statusCode);
        Assert.assertEquals("No issue link type with id '10012' found.", response.entity.errorMessages.get(0));
    }
}
