package com.atlassian.jira.webtests.ztests.navigator.jql;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PRIORITY_MAJOR;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

@WebTest({Category.FUNC_TEST, Category.JQL})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestTextClausesEscapingLuceneOperators extends BaseJiraRestTest {

    @Inject
    IssueTableAssertions issueTableAssertions;

    @Before
    public void setUpTest() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    public void testQueryingForAnIssueThatContainsALuceneOperatorOnItsSummary() {
        Map<String, String> summaryPrefixAndOperatorToTest = ImmutableMap.<String, String>builder()
                .put("one", "+")
                .put("two", "-")
                .put("three", "&")
                .put("four", "|")
                .put("five", "!")
                .put("six", "(")
                .put("seven", ")")
                .put("eight", "{")
                .put("nine", "}")
                .put("ten", "[")
                .put("eleven", "]")
                .put("twelve", "^")
                .put("thirteen", "~")
                .put("fourteen", "*")
                .put("fifteen", "?")
                .build();

        for (Map.Entry<String, String> summaryPrefixAndOperator : summaryPrefixAndOperatorToTest.entrySet()) {
            String summaryPrefix = summaryPrefixAndOperator.getKey();
            String luceneOperator = summaryPrefixAndOperator.getValue();

            // the issue will have a summary like: one+
            String issue = createIssueWithSummary(summaryPrefix + luceneOperator);

            // the query to test will be: text ~ "one\\+"
            String query = queryWithLuceneOperatorEscaped(summaryPrefix, luceneOperator);

            issueTableAssertions.assertSearchWithResults(query, issue);
        }
    }

    @Test
    public void testQueryingForAnIssueThatContainsABackslashOnItsSummary() throws Exception {
        String issue = createIssueWithSummary("issue\\");

        String queryWithAnExplicitBackslash = "text ~ \"issue\\\\\"";
        issueTableAssertions.assertSearchWithResults(queryWithAnExplicitBackslash, issue);

        String queryWithTheUnicodeCharacterForBackslash = "text ~ \"issue\\u005C\"";
        issueTableAssertions.assertSearchWithResults(queryWithTheUnicodeCharacterForBackslash, issue);
    }

    private String createIssueWithSummary(final String summary) {
        return backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, summary, null, PRIORITY_MAJOR, ISSUE_TYPE_BUG).key();
    }

    private String queryWithLuceneOperatorEscaped(final String summary, final String luceneOperator) {
        return "text ~ \"" + summary + "\\\\" + luceneOperator + "\"";
    }
}
