package com.atlassian.jira.webtests.ztests.imports.project;

import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v3.13
 */
@WebTest({Category.FUNC_TEST, Category.PROJECT_IMPORT})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestProjectImportSelectBackup extends BaseJiraProjectImportFuncTest {

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testValidationInvalidPaths() {
        this.administration.attachments().enable();
        navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);
        tester.clickLinkWithText("Edit Settings");
        tester.checkCheckbox("attachmentPathOption", "DEFAULT");
        tester.submit("Update");

        this.navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_IMPORT);

        // Test no backup file specified
        tester.setWorkingForm("project-import");
        tester.assertTextPresent("Project Import: Select Backup File");
        tester.setFormElement("backupPath", "");
        tester.submit();
        tester.assertTextPresent("You must provide a path to the JIRA backup zip file.");

        // Test the backup file specified does not exist
        tester.setWorkingForm("project-import");
        tester.assertTextPresent("Project Import: Select Backup File");
        tester.setFormElement("backupPath", "/iamafilethatwillneverexisthahahahaha/bak.xml");
        tester.submit();
        tester.assertTextPresent("The path to the JIRA backup zip file is not valid.");

        // Test the backup file is a directory not a file
        tester.setWorkingForm("project-import");
        tester.assertTextPresent("Project Import: Select Backup File");
        tester.setFormElement("backupPath", System.getProperty("java.io.tmpdir"));
        tester.submit();
        tester.assertTextPresent("The path to the JIRA backup zip file is not valid.");
    }

    @Test
    public void testValidationInvalidbackupAttachmentsNotEnabled() {
        this.administration.attachments().disable();

        this.navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_IMPORT);

        // Test no backup file specified
        tester.setWorkingForm("project-import");
        tester.assertTextPresent("Project Import: Select Backup File");
        tester.setFormElement("backupPath", "");
        tester.submit();
        tester.assertTextPresent("You must provide a path to the JIRA backup zip file.");

        // Test the backup file specified does not exist
        tester.setWorkingForm("project-import");
        tester.assertTextPresent("Project Import: Select Backup File");
        tester.setFormElement("backupPath", "/iamafilethatwillneverexisthahahahaha/bak.xml");
        tester.submit();
        tester.assertTextPresent("The path to the JIRA backup zip file is not valid.");

        // Test the backup file is a directory not a file
        tester.setWorkingForm("project-import");
        tester.assertTextPresent("Project Import: Select Backup File");
        tester.setFormElement("backupPath", System.getProperty("java.io.tmpdir"));
        tester.submit();
        tester.assertTextPresent("The path to the JIRA backup zip file is not valid.");
    }

    @Test
    public void testJumpToProgressScreen() {
        // First go to the initial page to make sure the session has been cleared.
        this.navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_IMPORT);

        tester.gotoPage("/secure/admin/ProjectImportBackupOverviewProgress.jspa");
        tester.assertTextPresent("Can not find any running task information. Perhaps your session has timed out, please restart the project import wizard.");
        tester.assertTextNotPresent("Refresh");
        tester.assertTextPresent("Cancel");
    }

    @Test
    public void testParseExceptionInJIRAData() throws Exception {
        final File file = importAndExportBackupAndSetupCurrentInstance("TestProjectImportParseExceptionScreen1.xml", "blankprojects.xml");
        try {
            this.navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_IMPORT);
            // Get to the project select page
            tester.setWorkingForm("project-import");
            tester.assertTextPresent("Project Import: Select Backup File");
            tester.setFormElement("backupPath", file.getAbsolutePath());
            tester.submit();

            advanceThroughWaitingPage();
            tester.assertTextPresent("Project Import: Select Backup File");
            //this message is actually wrong raised and issue JRADEV-23548
            textAssertions.assertTextPresentHtmlEncoded("There was a problem parsing the backup XML file at " + file.getAbsolutePath() + ": No 'key' field for Issue 10022.");
        } finally {
            //noinspection ResultOfMethodCallIgnored
            file.delete();
        }
    }
}
