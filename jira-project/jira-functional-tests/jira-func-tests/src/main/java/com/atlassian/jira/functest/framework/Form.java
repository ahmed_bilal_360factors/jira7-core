package com.atlassian.jira.functest.framework;

import com.google.inject.ImplementedBy;
import com.meterware.httpunit.WebForm;

/**
 * Used to set form values in tests.
 */
@ImplementedBy(FormImpl.class)
public interface Form {
    /**
     * Select an option with a given display value in a select element.
     *
     * @param selectName name of select element.
     * @param option     display value of option to be selected.
     */
    public void selectOption(String selectName, String option);

    /**
     * Select multiple options with given display values in a select element.
     *
     * @param selectName  name of select element.
     * @param option      display value of an option to be selected.
     * @param moreOptions display values of additional options to be selected.
     */
    public void selectOptionsByDisplayName(String selectName, String option, String... moreOptions);

    /**
     * Select multiple options with given display values in a select element.
     *
     * @param selectName name of select element.
     * @param options    display values of options to be selected.
     */
    public void selectOptionsByDisplayName(String selectName, String[] options);

    /**
     * Select multiple options with given values in a select element.
     *
     * @param selectName  name of select element.
     * @param option      value of option to be selected.
     * @param moreOptions values of additional options to be selected.
     */
    public void selectOptionsByValue(String selectName, String option, String... moreOptions);

    /**
     * Select multiple options with given values in a select element.
     *
     * @param selectName name of select element.
     * @param options    values of options to be selected.
     */
    public void selectOptionsByValue(String selectName, String[] options);

    /**
     * Returns all the forms in the current response.
     *
     * @return all the forms in the current response.
     */
    public WebForm[] getForms();

    /**
     * Switches HttpUnit current form and returns it.
     *
     * @param formId the id of the form
     * @return form with the id.
     */
    public WebForm switchTo(final String formId);

    /**
     * Click only a single button with the specific value.
     * If more than one button found with that value it will barf.
     *
     * @param value the value attribute of the of the button element to click.
     */
    public void clickButtonWithValue(final String value);

    /**
     * Click the first button you find with the given value. Ignores duplicate values.
     *
     * @param value the value attribute of the of the button element to click.
     */
    public void clickAnyButtonWithValue(final String value);

}