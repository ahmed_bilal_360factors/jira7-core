package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SYS_ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SYS_ADMIN_USERNAME;

/**
 * Func test of viewing general configuration pages as admin/sysadmin.
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestGeneralConfigurationSecurity extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Test
    public void testNonSysadminCannotAccessMimeSnifferOption() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            navigation.login(ADMIN_USERNAME);
            navigation.gotoAdminSection(Navigation.AdminSection.GENERAL_CONFIGURATION);
            assertions.assertNodeDoesNotExist(locator.css("td[data-property-id=ie-mime-sniffing]"));
        } finally {
            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }

    }

    @Test
    public void testNonSysadminCannotEditMimeSnifferOption() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            navigation.login(ADMIN_USERNAME);
            navigation.gotoAdminSection(Navigation.AdminSection.GENERAL_CONFIGURATION);
            tester.clickLink("edit-app-properties");
            assertions.assertNodeDoesNotExist(locator.css("input[name=ieMimeSniffer]"));
        } finally {
            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

}
