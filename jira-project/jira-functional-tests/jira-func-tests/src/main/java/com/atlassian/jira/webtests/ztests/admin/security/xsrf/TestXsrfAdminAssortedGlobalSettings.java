package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestXsrfAdminAssortedGlobalSettings extends BaseJiraFuncTest {
    @Inject
    private Form form;

    @Test
    public void easySettings() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck("Time Tracking Activate", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.TIMETRACKING);
                    }
                }, new XsrfCheck.FormSubmission("Activate"))
                ,
                new XsrfCheck("Time Tracking Deactivate", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.TIMETRACKING);
                    }
                }, new XsrfCheck.FormSubmission("Deactivate"))
                ,
                new XsrfCheck("Look And Feel Update", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.LOOKANDFEEL);
                    }
                }, new XsrfCheck.FormSubmission("logo-upload")),
                new XsrfCheck("Look And Feel Update", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.LOOKANDFEEL);
                    }
                }, new XsrfCheck.FormSubmission("favicon-upload")),
                new XsrfCheck("General Configuration Update", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.GENERAL_CONFIGURATION);
                        tester.clickLink("edit-app-properties");
                        tester.setFormElement("introduction", "Hello!");
                    }
                }, new XsrfCheck.FormSubmission("Update"))

        ).run(tester, navigation, form);
    }

}