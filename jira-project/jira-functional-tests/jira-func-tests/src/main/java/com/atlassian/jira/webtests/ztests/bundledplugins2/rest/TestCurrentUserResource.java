package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.v2.issue.UserBean;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.Set;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

/**
 * Verification for Create, Update and Delete operations
 *
 * @since v6.1
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestCurrentUserResource extends BaseJiraFuncTest {
    private static final String PASSWORD = "password";
    private static final String CURRENT_PASSWORD = "currentPassword";
    private static final String EMAIL_ADDRESS = "emailAddress";
    private static final String DISPLAY_NAME = "displayName";

    private static final String EMAIL_ADDRESS_CHANGED_VALUE = "charlie2@localhost";
    private static final String DISPLAY_NAME_CHANGED_VALUE = "Charlie of Atlassian II";
    private static final String REST_URL = "/rest/api/2/user?";

    private static final String USER_NAME = "notadmin";
    private static final String USER_CURRENT_PASSWORD = "withpassword";
    private static final String USER_NEW_PASSWORD = "hocuspocus";

    private UserClient userClient;
    private JIRAEnvironmentData environmentData;

    @Before
    public void setUp() {
        environmentData = getEnvironmentData();
        backdoor.usersAndGroups().addUser(USER_NAME, USER_CURRENT_PASSWORD, "DisplayNoAdminName", "noadmin@localhost");

        userClient = new UserClient(environmentData);
        userClient.loginAs(USER_NAME, USER_CURRENT_PASSWORD);
    }

    @After
    public void tearDown() {
        userClient.close();
    }

    @Test
    public void testUpdateUser() {
        //try to update myself
        updateUser();
    }

    @Test
    public void testUpdateUserWithNoPassword() {
        final ClientResponse responseUpdate = userClient.updateUser(EMAIL_ADDRESS_CHANGED_VALUE, DISPLAY_NAME_CHANGED_VALUE, null);
        assertThat(responseUpdate.getStatus(), equalTo(Response.Status.BAD_REQUEST.getStatusCode()));
    }

    @Test
    public void testUpdateUserWithInvalidPassword() {
        final ClientResponse responseUpdate = userClient.updateUser(EMAIL_ADDRESS_CHANGED_VALUE, DISPLAY_NAME_CHANGED_VALUE, CURRENT_PASSWORD + "*");
        assertThat(responseUpdate.getStatus(), equalTo(Response.Status.BAD_REQUEST.getStatusCode()));
    }

    @Test
    public void testChangePassword() {
        final ClientResponse response = userClient.changePassword(USER_NEW_PASSWORD, USER_CURRENT_PASSWORD);
        assertThat(response.getStatus(), equalTo(Response.Status.NO_CONTENT.getStatusCode()));
        response.close();
    }

    @Test
    public void testChangePasswordWithInvalidCurrentPassword() {
        final ClientResponse response = userClient.changePassword(USER_NEW_PASSWORD, USER_CURRENT_PASSWORD + "-");
        assertThat(response.getStatus(), equalTo(Response.Status.BAD_REQUEST.getStatusCode()));
        response.close();
    }

    private String getJiraExperimentalApiUserPath() {
        return environmentData.getBaseUrl() + REST_URL;
    }

    private void updateUser() {
        //try to update it
        final ClientResponse responseUpdate = userClient.updateUser(EMAIL_ADDRESS_CHANGED_VALUE, DISPLAY_NAME_CHANGED_VALUE, USER_CURRENT_PASSWORD);
        assertThat(responseUpdate.getStatus(), equalTo(Response.Status.OK.getStatusCode()));

        final UserBean userBean = getMapEntity(responseUpdate);
        responseUpdate.close();
        assertThat(userBean, notNullValue());
        assertThat(userBean.getEmailAddress(), equalTo(EMAIL_ADDRESS_CHANGED_VALUE));
        assertThat(userBean.getDisplayName(), equalTo(DISPLAY_NAME_CHANGED_VALUE));
        assertThat(userBean.getSelf().toString(), startsWith(getJiraExperimentalApiUserPath()));
        assertThat(userBean.getLocale(), equalTo("en_AU"));
    }

    private UserBean getMapEntity(final ClientResponse responseRead) {
        return responseRead.getEntity(UserBean.class);
    }


    private class UserClient extends RestApiClient<UserClient> {
        private static final String CURRENT_USER_PATH = "myself";
        private static final String PASSWORD_PATH = "password";

        private final Set<ClientResponse> responses = Sets.newHashSet();

        protected UserClient(final JIRAEnvironmentData environmentData) {
            super(environmentData);
        }

        protected WebResource createResource() {
            return resourceRoot(environmentData.getBaseUrl().toExternalForm()).path("rest").path("api").path("2");
        }

        private ClientResponse getUser(final String key) {
            checkNotNull(key);

            final WebResource webResource = createResource().path(CURRENT_USER_PATH);

            final ClientResponse clientResponse = webResource.get(ClientResponse.class);
            responses.add(clientResponse);
            return clientResponse;
        }

        private ClientResponse updateUser(final String emailAddress, final String displayName, final String password) {
            final WebResource webResource = createResource().path(CURRENT_USER_PATH);

            final ImmutableMap.Builder<Object, Object> builder = ImmutableMap.builder();
            if (emailAddress != null) {
                builder.put(EMAIL_ADDRESS, emailAddress);
            }
            if (displayName != null) {
                builder.put(DISPLAY_NAME, displayName);
            }
            if (password != null) {
                builder.put(PASSWORD, password);
            }

            final ClientResponse clientResponse = webResource.type("application/json").put(ClientResponse.class, builder.build());
            responses.add(clientResponse);
            return clientResponse;
        }

        private ClientResponse changePassword(final String password, final String currentPassword) {
            final WebResource webResource = createResource().path(CURRENT_USER_PATH + "/" + PASSWORD_PATH);

            final ImmutableMap.Builder<Object, Object> builder = ImmutableMap.builder();
            if (password != null) {
                builder.put(PASSWORD, password);
                builder.put(CURRENT_PASSWORD, currentPassword);
            }

            final ClientResponse clientResponse = webResource.type("application/json").put(ClientResponse.class, builder.build());
            responses.add(clientResponse);
            return clientResponse;
        }

        private void close() {
            for (final ClientResponse response : responses) {
                response.close();
            }
        }
    }
}
