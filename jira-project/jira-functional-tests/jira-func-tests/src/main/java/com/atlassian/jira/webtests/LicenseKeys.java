package com.atlassian.jira.webtests;

import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.test.util.lic.LicenseReader;

/**
 * This contains all the known license keys for testing.  Add new ones here not in your tests!
 * When you add new licenses remember to use the time bomb feature.
 * <p>
 * NOTE: The licenses are read from `jira-func-tests/src/main/resources/license`. The files in this directory are
 * generated using https://stash.atlassian.com/projects/JIRA/repos/jira-test-license-generator.
 *
 * @since v4.0
 */
public class LicenseKeys {
    /**
     * V1 Atlassian license. Used to ensure that JIRA no longer supports V1 licenses.
     */
    public static final License V1_ENTERPRISE = readLicense("v1-enterprise.lic");

    /**
     * Atlassian license where the evaluation period has expired.
     */
    public static final License EVAL_EXPIRED = readLicense("eval-expired.lic");

    /**
     * Time bombed unlimited commercial license.
     */
    public static final License COMMERCIAL = readLicense("commercial.lic");

    /**
     * Time bombed unlimited personal license.
     */
    public static final License PERSONAL = readLicense("personal.lic");

    /**
     * Time bombed unlimited starter license.
     */
    public static final License STARTER = readLicense("starter.lic");

    /**
     * Time bombed unlimited community license.
     */
    public static final License COMMUNITY = readLicense("community.lic");

    /**
     * Time bombed unlimited open source license.
     */
    public static final License OPEN_SOURCE = readLicense("open-source.lic");

    /**
     * Time bombed unlimited developer license.
     */
    public static final License DEVELOPER = readLicense("developer.lic");

    /**
     * Time bombed unlimited demo license.
     */
    public static final License DEMO = readLicense("demo.lic");

    /**
     * Time bombed developer license for 5 users.
     */
    public static final License DEVELOPER_LIMITED = readLicense("developer-limited.lic");

    /**
     * Time bombed developer license for 5 users.
     */
    public static final License COMMERCIAL_LIMITED = readLicense("commercial-limited.lic");

    /**
     * Time bombed JIRA Core license for 5 users.
     */
    public static final License CORE_ROLE = readLicense("core-role.lic");

    /**
     * Time bombed JIRA Software license for 100 users.
     */
    public static final License SOFTWARE_ROLE = readLicense("software-role.lic");

    /**
     * Time bombed JIRA Func Test license for 10 users.
     */
    public static final License TEST_ROLE = readLicense("test-role.lic");

    /**
     * Time bombed JIRA Func Test license for 3 users.
     */
    public static final License TEST_ROLE_3_USERS = readLicense("test-role-3-users.lic");

    /**
     * Invalid License with zero users
     */
    public static final String INVALID = readEncodedLicense("invalid.lic");

    /**
     * Invalid JIRA Application Licence Non Existent role
     */
    public static final String NON_EXISTING_ROLE = readEncodedLicense("nonexisting-role.lic");
    /**
     * Time bombed license for multiple applications:
     * <p>
     * <ul>
     * <li>JIRA Core (3 users)</li>
     * <li>JIRA Software (4 users)</li>
     * <li>JIRA Service Desk (2 users)</li>
     * <li>JIRA Func Test (4 users)</li>
     * <li>JIRA Reference Plugin (5 users)</li>
     * </ul>
     */
    public static final License MULTI_ROLE = readLicense("core-software-servicedesk-test-reference-roles.lic");

    /**
     * Time bombed license for multiple applications:
     * <p>
     * <ul>
     * <li>JIRA Core (3 users)</li>
     * <li>JIRA Service Desk (2 users)</li>
     * </ul>
     */
    public static final License CORE_SERVICEDESK = readLicense("core-servicedesk-roles.lic");

    /**
     * An evaluation license from the y2k period.
     */
    public static final License AGED_EVALUATION_LICENSE = readLicense("aged-eval.lic");

    /**
     * Time bombed license for multiple applications:
     * <p>
     * <ul>
     * <li>JIRA Core (3 users)</li>
     * <li>JIRA Func Test (4 users)</li>
     * </ul>
     */
    public static final License CORE_AND_TEST_ROLES = readLicense("core-test-roles.lic");

    private static License readLicense(String license) {
        return LicenseReader.readLicense("/resources/license/" + license, LicenseKeys.class);
    }

    private static String readEncodedLicense(String licenseResource) {
        return LicenseReader.readLicenseEncodedString("/resources/license/" + licenseResource, LicenseKeys.class);
    }
}
