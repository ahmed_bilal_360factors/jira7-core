package com.atlassian.jira.webtests.ztests.license;

import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.LicenseKeys;
import com.google.common.collect.Sets;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.CORE_KEY;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.TEST_KEY;
import static org.junit.Assert.assertTrue;

/**
 * In a "single application instance" only one JIRA Application (product) has been installed.
 * During User Creation the application is not selectable (it is not visible and selected by default).
 */
@WebTest({Category.FUNC_TEST, Category.LICENSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestCreateUserSingleApplicationInstance extends TestCreateUserHelper {
    private static final String PLATFORM_GROUP = "platform-users";
    private static final String SOFTWARE_GROUP = "software-users";
    private static final String ADMIN_GROUP = "jira-administrators";

    @Test
    public void testShouldCreateUserAndAddToGlobalUsePermissionGroupLegacy() {
        //Given
        backdoor.restoreBlankInstance(LicenseKeys.CORE_ROLE);

        //When: No application selected in single application instance
        createUser("one");

        //Then
        assertUserInGroups("one", "jira-users");
    }

    @Test
    public void testShouldCreateUserAndAddToAppDefaultGroup() {
        //Given: Default group has been configured
        backdoor.restoreBlankInstance(LicenseKeys.CORE_ROLE);
        backdoor.usersAndGroups().addGroup(PLATFORM_GROUP);
        backdoor.usersAndGroups().addGroup(SOFTWARE_GROUP);
        //Adding back admin group to ensure admin can still login
        backdoor.applicationRoles().putRoleWithDefaults(CORE_KEY, Sets.newHashSet(PLATFORM_GROUP, SOFTWARE_GROUP, ADMIN_GROUP),
                Sets.newHashSet(PLATFORM_GROUP, SOFTWARE_GROUP));

        //When: No application selected in single application instance
        createUser("one");

        //Then: User created and added to the default group of the single application
        assertUserInGroups("one", PLATFORM_GROUP, SOFTWARE_GROUP);
    }

    @Test
    public void testShouldWarnAndCreateUserWhenRoleHasNoDefaultGroups() {
        //Given: No default group configured
        backdoor.restoreBlankInstance(LicenseKeys.CORE_ROLE);
        backdoor.usersAndGroups().addGroup("platform-users");
        //Adding back admin group to ensure admin can still login
        backdoor.applicationRoles().putRoleWithDefaults(CORE_KEY,
                Sets.newHashSet("platform-users", ADMIN_GROUP),
                Sets.<String>newHashSet());

        //When: Display create user dialog
        populateCreateUserForm("user");

        //Then: Warn users
        assertTrue(hasNoDefaultGroupWarningForApplication(CORE_KEY));

        //Create User
        tester.submit("Create");

        //Then: User created but not added to any groups
        assertUserNotInAnyGroups("user");
    }

    @Test
    public void testShouldWarnUserThatLicenseLimitHasBeenReachedAndAllowAdminToCreateUser() {
        //Given: Default group configured but license limit reached
        backdoor.restoreBlankInstance(LicenseKeys.TEST_ROLE_3_USERS);
        backdoor.usersAndGroups().addGroup("test-users");
        //Adding back admin group to ensure admin can still login
        backdoor.applicationRoles().putRoleWithDefaults(TEST_KEY,
                Sets.newHashSet("test-users", ADMIN_GROUP),
                Sets.newHashSet("test-users"));
        //Use up license limit
        for (int i = 1; i < 3; i++) {
            createUser("user" + i);
            assertUserInGroups("user" + i, "test-users");
        }

        //When: Display create user dialog
        populateCreateUserForm("limitReached");

        //Then: Warn users
        assertTrue(hasUserLimitReachedWarningForApplication(TEST_KEY));

        //Create User
        tester.submit("Create");

        //Then: User created but not added to any groups
        assertUserNotInAnyGroups("limitReached");
    }
}