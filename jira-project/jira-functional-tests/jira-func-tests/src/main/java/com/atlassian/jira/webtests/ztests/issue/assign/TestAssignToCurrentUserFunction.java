package com.atlassian.jira.webtests.ztests.issue.assign;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_DEV_GROUP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_USERS_GROUP;


/**
 * Enterprise only test!
 *
 * @since v3.12
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
@LoginAs(user = ADMIN_USERNAME)
public class TestAssignToCurrentUserFunction extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        administration.restoreData("TestAssignToCurrentUserFunction.xml");
    }

    @Test
    public void testAssignToCurrentUserWithNoAssignIssuePermission() {
        //Remove the assign issues permission
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGN_ISSUES, JIRA_DEV_GROUP);

        navigation.issue().gotoIssue("MKY-1");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Assignee:", FRED_FULLNAME, "Reporter:", ADMIN_FULLNAME});

        //lets resolve the issue which as the assignToSelf post-function set.
        tester.clickLink("action_id_5");
        tester.setWorkingForm("issue-workflow-transition");
        tester.submit("Transition");

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Assignee:", ADMIN_FULLNAME, "Reporter:", ADMIN_FULLNAME});
    }

    @Test
    public void testAssignToCurrentUserWithNoAssignableUserPermission() {

        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGNABLE_USER, JIRA_DEV_GROUP);
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGNABLE_USER, JIRA_USERS_GROUP);

        navigation.issue().gotoIssue("MKY-1");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Assignee:", FRED_FULLNAME, "Reporter:", ADMIN_FULLNAME});

        //lets resolve the issue which as the assignToSelf post-function set.
        tester.clickLink("action_id_5");
        tester.setWorkingForm("issue-workflow-transition");
        tester.submit("Transition");

        //no longer have the issue summary on the resolve screen so lets go back to the issue and assert the
        //assignee hasn't changed there.
        navigation.issue().gotoIssue("MKY-1");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Assignee:", FRED_FULLNAME, "Reporter:", ADMIN_FULLNAME});
    }


    @Test
    public void testAssignToCurrentUser() {
        navigation.issue().gotoIssue("MKY-1");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Assignee:", FRED_FULLNAME, "Reporter:", ADMIN_FULLNAME});

        //lets resolve the issue which as the assignToSelf post-function set.
        tester.clickLink("action_id_5");
        tester.setWorkingForm("issue-workflow-transition");
        tester.submit("Transition");

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Assignee:", ADMIN_FULLNAME, "Reporter:", ADMIN_FULLNAME});
    }
}
