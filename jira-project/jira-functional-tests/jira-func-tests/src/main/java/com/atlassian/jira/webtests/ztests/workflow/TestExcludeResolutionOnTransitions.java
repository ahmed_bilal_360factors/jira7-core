package com.atlassian.jira.webtests.ztests.workflow;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperations;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.WorkflowSchemes;
import com.atlassian.jira.functest.framework.admin.ViewWorkflows;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.GlobalPermissionKey;
import org.hamcrest.Matchers;
import org.junit.Test;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ASSIGN_FIELD_SCREEN;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.RESOLVE_FIELD_SCREEN_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.TRANSIION_NAME_CLOSE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.TRANSIION_NAME_REOPEN;
import static com.atlassian.jira.functest.framework.FunctTestConstants.WORKFLOW_SCHEME;
import static com.atlassian.jira.webtests.Groups.USERS;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Copyright (c) 2002-2004
 * All rights reserved.
 * <p/>
 * Used to test the ability to exclude certain resolutions
 * on workflow transitions.
 */
@WebTest({Category.FUNC_TEST, Category.BROWSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestExcludeResolutionOnTransitions extends BaseJiraFuncTest {

    private static final String WORKFLOW_NAME = "Exclude Resolution Workflow";
    private static final String WORKFLOW_DESC = "A workflow where we will exclude resolutions.";
    private static final String OPEN_STEP_NAME = "Open";
    private static final String CLOSED_STEP_NAME = "Closed Step";
    private static final String BULK_TRANSITION_ELEMENT_NAME = "wftransition";

    @Inject
    private WorkflowSchemes workflowSchemes;

    @Inject
    private BulkOperations bulkOperations;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    @Restore("blankWithOldDefault.xml")
    public void testAddExcludeAttribute() {
        _testCreateWorkflowWithResolutionExcludes();

        _testExcludeResolutionsSingleIssue();

        _testExcludeResolutionBulkTransition();
    }

    private void _testExcludeResolutionsSingleIssue() {
        // CReate an issue for testing
        navigation.issue().createIssue(PROJECT_HOMOSAP, "Bug", "Test Issue");

        // We should be on the View Issue page.
        // Try closing an issue
        tester.clickLinkWithText(TRANSIION_NAME_CLOSE);

        // Should not have any extra options just the 'available resolution'.
        List<String> extraOptions = Collections.emptyList();
        assertExcludedOptions(extraOptions);

        tester.selectOption("resolution", "Fixed");

        // Close Issue
        tester.submit();
    }

    private void _testCreateWorkflowWithResolutionExcludes() {
        tester.assertTextNotPresent(WORKFLOW_NAME);
        final ViewWorkflows workflows = administration.workflows();
        workflows.goTo().addWorkflow(WORKFLOW_NAME, WORKFLOW_DESC).goTo();

        tester.assertTextPresent(WORKFLOW_NAME);
        assertTransitionNameNotPresent(CLOSED_STEP_NAME);
        workflows.goTo().workflowSteps(WORKFLOW_NAME).add(CLOSED_STEP_NAME, "Closed");
        tester.assertTextPresent(CLOSED_STEP_NAME);
        assertTransitionNameNotPresent(TRANSIION_NAME_CLOSE);
        workflows.goTo().workflowSteps(WORKFLOW_NAME).addTransition(OPEN_STEP_NAME, TRANSIION_NAME_CLOSE, "Close issue", CLOSED_STEP_NAME, RESOLVE_FIELD_SCREEN_NAME);
        tester.assertTextPresent(TRANSIION_NAME_CLOSE);
        assertTransitionNameNotPresent(TRANSIION_NAME_REOPEN);
        workflows.goTo().workflowSteps(WORKFLOW_NAME).addTransition(CLOSED_STEP_NAME, TRANSIION_NAME_REOPEN, "Reopen issue", OPEN_STEP_NAME, ASSIGN_FIELD_SCREEN);
        tester.assertTextPresent(TRANSIION_NAME_REOPEN);

        // Hide some resolutions on the Close transition
        backdoor.workflow().setTransitionProperty(WORKFLOW_NAME, false, 11, "jira.field.resolution.exclude", "2,4");

        // Create project to work with
        administration.project().addProject(PROJECT_HOMOSAP, PROJECT_HOMOSAP_KEY, ADMIN_USERNAME);

        enableWorkflow();
    }

    private void assertTransitionNameNotPresent(final String transitionName) {
        assertThat(tester.getDialog().getElement("content").getTextContent(), Matchers.not(Matchers.containsString(transitionName)));
    }

    private void enableWorkflow() {
        workflowSchemes.addWorkFlowScheme(WORKFLOW_SCHEME, "Test workflow scheme.");
        backdoor.workflowSchemes().assignScheme(10000L, "Bug", WORKFLOW_NAME);
        administration.project().associateWorkflowScheme(PROJECT_HOMOSAP, WORKFLOW_SCHEME);
        workflowSchemes.waitForSuccessfulWorkflowSchemeMigration(PROJECT_HOMOSAP, WORKFLOW_SCHEME);
    }

    private void assertExcludedOptions(Collection<String> extraOptions) {
        Collection<String> options = new ArrayList<String>(extraOptions);
        options.add("1");
        options.add("3");
        options.add("5");

        tester.assertOptionValuesEqual("resolution", options.toArray(new String[options.size()]));

        // Just n case
        tester.assertRadioOptionValueNotPresent("resolution", "Won't Fix");
        tester.assertRadioOptionValueNotPresent("resolution", "Incomplete");
    }


    private void _testExcludeResolutionBulkTransition() {
        // Try Bulk Close
        // Create a few issues
        Collection<String> issueKeys = new ArrayList<String>();
        for (int i = 0; i < 5; i++) {
            issueKeys.add(navigation.issue().createIssue(PROJECT_HOMOSAP, "Bug", "Test Issue " + i));
        }

        administration.addGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);

        this.navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        bulkOperations.bulkChangeChooseIssuesAll();
        bulkOperations.chooseOperationExecuteWorfklowTransition();
        navigation.workflows().assertStepOperationDetails();

        tester.getDialog().setWorkingForm("bulk-transition-details");

        String[] options = tester.getDialog().getForm().getOptions(BULK_TRANSITION_ELEMENT_NAME);
        String[] optionValues = tester.getDialog().getForm().getOptionValues(BULK_TRANSITION_ELEMENT_NAME);

        if (options == null || options.length == 0) {
            fail("No options for '" + BULK_TRANSITION_ELEMENT_NAME + "' element.");
        }

        String closeTransitionOptionValue = null;
        for (int i = 0; i < options.length; i++) {
            String option = options[i];
            if (option.contains(TRANSIION_NAME_CLOSE)) {
                closeTransitionOptionValue = optionValues[i];
                break;
            }
        }

        if (closeTransitionOptionValue == null) {
            fail("Could not find option for Close Issue transition");
        }

        navigation.workflows().chooseWorkflowAction(closeTransitionOptionValue);

        // On bulk workflow there is a 'please select' option for resolutions.
        Collection<String> extraOptions = new ArrayList<String>();
        extraOptions.add("-1");
        assertExcludedOptions(extraOptions);
        tester.selectOption("resolution", "Fixed");
        navigation.clickOnNext();

        tester.assertTableRowsEqual("updatedfields", 1, new String[][]{{"Resolution", "Fixed"}});
        navigation.clickOnNext();
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        // Check that the resolution has been set to fixed for all issues we have bulk transitioned
        for (final String issueKey : issueKeys) {
            this.navigation.issue().gotoIssue(issueKey);
            textAssertions.assertTextPresent(new IdLocator(tester, "resolution-val"), "Fixed");
        }
    }

}
