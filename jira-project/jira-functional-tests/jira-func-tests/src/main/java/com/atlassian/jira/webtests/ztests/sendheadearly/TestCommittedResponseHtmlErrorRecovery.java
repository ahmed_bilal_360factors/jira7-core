package com.atlassian.jira.webtests.ztests.sendheadearly;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;

/**
 * @since 6.4
 */
@WebTest({Category.FUNC_TEST, Category.HTTP})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestCommittedResponseHtmlErrorRecovery extends BaseJiraFuncTest {
    private static final String OPENING_HTML_TAG = "<html";   // Should be unique enough. Full tag probably looks something like: <html lang="en">
    private static final String CLOSING_HTML_TAG = "</html>";
    private static final String MAGIC_JAVASCRIPT_MARKER = "\'\"-->>]]>";
    private static final String POST_FLUSH_CONTENT = "POST_FLUSH_CONTENT";

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @After
    public void tearDownTest() {
        navigation.logout();
    }

    @Test
    public void testWithFlushAndContent() {
        tester.gotoPage("/CommittedResponseExceptionThrowingAction.jspa");
        String response = tester.getDialog().getResponseText();

        assertNotNull("Expected a response", response);
        assertThat("Expected to see an opening HTML tag in the response", response, containsString(OPENING_HTML_TAG));
        assertThat("Expected to NOT see a closing HTML tag in the response", response, not(containsString(CLOSING_HTML_TAG)));
        assertThat("Expected to see magic javascript injected in the response", response, containsString(MAGIC_JAVASCRIPT_MARKER));
        assertThat("Expected to NOT to see any post-flush content in the response", response, not(containsString(POST_FLUSH_CONTENT)));
    }
}
