package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.ResourceRef;
import com.atlassian.jira.testkit.client.restclient.Component;
import com.atlassian.jira.testkit.client.restclient.ProjectClient;
import com.atlassian.jira.testkit.client.restclient.Version;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * Func test for ProjectResource which check that upon deletion of a project all
 * associations with versions and components are removed.
 *
 * @since v7.1
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@RestoreBlankInstance
public class TestProjectResourceDeleteProject extends BaseJiraFuncTest {

    private ProjectClient projectClient;

    @Before
    public void setUpTest() {
        backdoor.project().addProject("ATLASSIAN", "ATL", "admin");
        backdoor.project().addProject("TEST", "TST", "admin");

        backdoor.versions().create(new Version().project("ATL").name("ATL Version 1"));
        backdoor.versions().create(new Version().project("ATL").name("ATL Version 2"));
        backdoor.versions().create(new Version().project("ATL").name("ATL Version 3"));

        backdoor.versions().create(new Version().project("TST").name("TST Version 1"));
        backdoor.versions().create(new Version().project("TST").name("TST Version 2"));

        backdoor.components().create(new Component().project("ATL").name("ATL 1"));
        backdoor.components().create(new Component().project("ATL").name("ATL 2"));

        backdoor.components().create(new Component().project("TST").name("TST 1"));
        backdoor.components().create(new Component().project("TST").name("TST 2"));

        final String firstIssueKey = backdoor.issues().createIssue("ATL", "First issue.").key;
        final String secondIssueKey = backdoor.issues().createIssue("ATL", "Second issue.").key;
        final String thirdIssueKey = backdoor.issues().createIssue("TST", "Third issue.").key;

        backdoor.issues().setIssueFields(firstIssueKey, versions("ATL Version 1", "ATL Version 2", "ATL 1"));
        backdoor.issues().setIssueFields(secondIssueKey, versions("ATL Version 1", "ATL Version 3", "ATL 2"));
        backdoor.issues().setIssueFields(thirdIssueKey, versions("TST Version 1", "TST Version 2", "TST 1"));

        projectClient = new ProjectClient(environmentData);
    }

    private IssueFields versions(final String version, final String fixVersion, final String component) {
        final ResourceRef versionRef = ResourceRef.withName(version);
        final ResourceRef fixVersionRef = ResourceRef.withName(fixVersion);
        final ResourceRef componentRef = ResourceRef.withName(component);
        return new IssueFields().versions(versionRef).fixVersions(fixVersionRef).components(componentRef);
    }

    @Test
    public void shouldDeleteAllVersionAssociations() {
        final List<Long> deletedVersions = getAllVersionIds("ATL");

        projectClient.delete("ATL");

        final ImmutableMap<String, Object> findParams = ImmutableMap.of(
                "sourceNodeEntity", "Issue",
                "sinkNodeEntity", "Version",
                "associationType", "IssueVersion"
        );

        final List<Map<String, Object>> issueVersionAssociations = getNodeAssociations(findParams);

        final ImmutableList<Long> associatedVersions = issueVersionAssociations.stream()
                .map(issueAssociation -> issueAssociation.get("sinkNodeId"))
                .map(sinkNodeId -> ((Integer) sinkNodeId).longValue())
                .collect(toImmutableList());

        assertThat(associatedVersions, not(hasItems(toArray(deletedVersions))));
        assertThat(associatedVersions, hasSize(1));
    }

    @Test
    public void shouldDeleteAllFixVersionAssociations() {
        final List<Long> deletedVersions = getAllVersionIds("ATL");

        projectClient.delete("ATL");

        final ImmutableMap<String, Object> findParams = ImmutableMap.of(
                "sourceNodeEntity", "Issue",
                "sinkNodeEntity", "Version",
                "associationType", "IssueFixVersion"
        );

        final List<Map<String, Object>> issueFixVersionAssociations = getNodeAssociations(findParams);

        final ImmutableList<Long> associatedFixVersions = issueFixVersionAssociations.stream()
                .map(issueAssociation -> issueAssociation.get("sinkNodeId"))
                .map(sinkNodeId -> ((Integer) sinkNodeId).longValue())
                .collect(toImmutableList());

        assertThat(associatedFixVersions, not(hasItems(toArray(deletedVersions))));
        assertThat(associatedFixVersions, hasSize(1));
    }

    private List<Long> getAllVersionIds(final String projectKey) {
        return backdoor.project().getVersionsForProject(projectKey).stream()
                .map(version -> version.id)
                .collect(toImmutableList());
    }

    @Test
    public void shouldDeleteAllComponentAssociations() {
        final List<Long> deletedComponents = getAllComponentIds("ATL");

        projectClient.delete("ATL");

        final ImmutableMap<String, Object> findParams = ImmutableMap.of(
                "sourceNodeEntity", "Issue",
                "sinkNodeEntity", "Component",
                "associationType", "IssueComponent"
        );

        final List<Map<String, Object>> issueComponentAssociations = getNodeAssociations(findParams);

        final ImmutableList<Long> associatedComponents = issueComponentAssociations.stream()
                .map(issueAssociation -> issueAssociation.get("sinkNodeId"))
                .map(sinkNodeId -> ((Integer) sinkNodeId).longValue())
                .collect(toImmutableList());

        assertThat(associatedComponents, not(hasItems(toArray(deletedComponents))));
        assertThat(associatedComponents, hasSize(1));
    }

    private List<Long> getAllComponentIds(final String projectKey) {
        return backdoor.project().getComponentsForProject(projectKey).stream()
                .map(version -> version.id)
                .collect(toImmutableList());
    }

    private List<Map<String, Object>> getNodeAssociations(final ImmutableMap<String, Object> findParams) {
        return backdoor.entityEngine().findByAnd("NodeAssociation", findParams);
    }

    private Long[] toArray(final List<Long> list) {
        return list.toArray(new Long[list.size()]);
    }

    @After
    public void tearDownTest() {
        projectClient.cleanUp();
    }
}
