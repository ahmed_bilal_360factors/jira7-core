package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.admin.GeneralConfiguration;
import com.atlassian.jira.functest.framework.backdoor.ColumnControl;
import com.atlassian.jira.functest.framework.backdoor.WorkflowsControlExt;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.testkit.client.restclient.User;
import com.atlassian.jira.testkit.client.restclient.UserClient;
import com.atlassian.jira.testkit.client.restclient.UserPickerResults;
import com.atlassian.jira.testkit.client.restclient.UserPickerUser;
import com.atlassian.jira.webtests.Permissions;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_ADMIN_GROUP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_DEV_GROUP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_USERS_GROUP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.USER_PICKER;
import static com.atlassian.jira.permission.ProjectPermissions.ASSIGN_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.DELETE_OWN_COMMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.EDIT_ALL_COMMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.EDIT_OWN_COMMENTS;
import static java.util.Collections.emptyMap;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Tests for the user resource.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@Restore("TestUserResource.xml")
public class TestUserResource extends BaseJiraFuncTest {
    private static final String USER_PATH = "user";
    private static final String REST_PATH = "rest/api/2";
    private static final String REST_USER_URL = REST_PATH + "/" + USER_PATH;

    private UserClient userClient;
    private ManualClient manualClient;

    @Inject
    private GeneralConfiguration generalConfiguration;

    @Before
    public void setUpTest() {
        userClient = new UserClient(environmentData);
        manualClient = new ManualClient(environmentData);
    }

    @Test
    public void testUserResourceNoUsernameNorKey() throws Exception {
        Response response = userClient.getUserResponse(null);
        assertEquals(404, response.statusCode);
        assertTrue(response.entity.errorMessages.contains("Either the 'username' or the 'key' query parameters need to be provided"));
    }

    @Test
    public void testUserResourceForUserByNameThatDoesntExist() throws Exception {
        Response response = userClient.getUserResponse("bofh");
        assertEquals(404, response.statusCode);
        assertTrue(response.entity.errorMessages.contains("The user named 'bofh' does not exist"));
    }

    @Test
    public void testUserResourceForUserByKeyThatDoesntExist() throws Exception {
        Response response = userClient.getUserResponseByKey("bofh");
        assertEquals(404, response.statusCode);
        assertTrue(response.entity.errorMessages.contains("The user with the key 'bofh' does not exist"));
    }

    @Test
    public void testUserResourceForUserByName() throws Exception {
        final User fred = userClient.get("bloblaw");
        assertNotNull(fred);
        assertEquals("bloblaw", fred.name);
        assertEquals("bob", fred.key);
    }

    @Test
    public void testUserResourceForUserByKey() throws Exception {
        final User fred = userClient.getByKey("bob");
        assertNotNull(fred);
        assertEquals("bloblaw", fred.name);
        assertEquals("bob", fred.key);
    }

    @Test
    public void testUserResourceForUserByUsernameAndKey() throws Exception {
        WebResource webResource = manualClient.createResource().path("user").queryParam("username", "blobblaw").queryParam("key", "bob");
        assertEquals(manualClient.getResponse(webResource).statusCode, 400);
    }

    @Test
    public void testUserResourceTimeZone() throws Exception {
        User user = userClient.get("admin");
        assertEquals("Australia/Sydney", user.timeZone);
    }

    @Test
    public void testUserResourceLocale() {
        User user = userClient.get("admin");
        assertEquals("en_AU", user.locale);
    }

    @Test
    public void testSearchUsers() {
        List<User> users = userClient.search("fre", "0", null);

        assertEquals(1, users.size());
        User user = users.get(0);
        assertEquals("fred", user.name);
        assertEquals("Fred Normal", user.displayName);

        //empty search should return nothing
        users = userClient.search("", "0", null);
        assertEquals(0, users.size());

        //try searching via e-mail
        users = userClient.search("love", "0", null);
        assertEquals(1, users.size());
        assertEquals("\u611b", users.get(0).name);

        //searching for a should have 3 results
        users = userClient.search("a", null, null);
        assertEquals(3, users.size());
        assertEquals("a\\b", users.get(0).name);
        assertEquals("admin", users.get(1).name);
        assertEquals("sp ace", users.get(2).name);

        //now try paging
        users = userClient.search("a", "0", "1");
        assertEquals(1, users.size());
        assertEquals("a\\b", users.get(0).name);

        users = userClient.search("a", "1", "30");
        assertEquals(2, users.size());
        assertEquals("admin", users.get(0).name);
        assertEquals("sp ace", users.get(1).name);

        // Search only includes active by default
        users = userClient.search("fred", null, null, null, null);
        assertEquals(1, users.size());
        assertEquals("fred", users.get(0).name);

        // include inactive
        users = userClient.search("fred", null, null, null, true);
        assertEquals(2, users.size());
        assertEquals("fred", users.get(0).name);
        assertEquals("fredx", users.get(1).name);

        // include inactive - explicit active
        users = userClient.search("fred", null, null, true, true);
        assertEquals(2, users.size());
        assertEquals("fred", users.get(0).name);
        assertEquals("fredx", users.get(1).name);

        // inactive only
        users = userClient.search("fred", null, null, false, true);
        assertEquals(1, users.size());
        assertEquals("fredx", users.get(0).name);

        // no-one
        users = userClient.search("fred", null, null, false, false);
        assertEquals(0, users.size());

        // with renamed user
        users = userClient.search("blo", null, null, true, false);
        assertEquals(1, users.size());
        assertEquals("bloblaw", users.get(0).name);
        assertEquals("bob", users.get(0).key);
    }

    @Test
    public void testSearchWithMultipleTokensInQuery() {
        List<User> users = userClient.search("Fred Normal", "0", null);

        assertEquals(1, users.size());
        User user = users.get(0);
        assertEquals("fred", user.name);
        assertEquals("Fred Normal", user.displayName);
    }

    @Test
    public void testSearchIsCaseInsensitive() {
        List<User> users = userClient.search("fReD", "0", null);

        assertEquals(1, users.size());
        User user = users.get(0);
        assertEquals("fred", user.name);
        assertEquals("Fred Normal", user.displayName);
    }

    @Test
    public void testHandleEdgeCaseOffset() {
        for (String term : ImmutableList.of("", "u", "user")) {
            int numOfIssues = searchWithOffset(term, 0).size();
            int offsetOutOfBounds = numOfIssues + 1;
            assertEquals(0, searchWithOffset(term, offsetOutOfBounds).size());
            assertEquals(numOfIssues, searchWithOffset(term, -1).size());
            assertEquals(0, searchWithOffset(term, Integer.MAX_VALUE).size());
        }
    }

    @Test
    public void testMultiProjectSearchEdgeCaseOffset() {
        for (String term : ImmutableList.of("", "u", "user")) {
            int numOfIssues = multiProjectSearchWithOffset(term, 0).size();
            int offsetOutOfBounds = numOfIssues + 1;
            assertEquals(0, multiProjectSearchWithOffset(term, offsetOutOfBounds).size());
            assertEquals(numOfIssues, multiProjectSearchWithOffset(term, -1).size());
            assertEquals(0, multiProjectSearchWithOffset(term, Integer.MAX_VALUE).size());
        }
    }

    private List<?> searchWithOffset(String term, int startAt) {
        return userClient.search(term, String.valueOf(startAt), "");
    }

    private List<?> multiProjectSearchWithOffset(String term, int startAt) {
        return userClient.multiProjectSearchAssignable(term, "HSP", String.valueOf(startAt), "");
    }

    @Test
    public void testSearchUsersWithMultipleTokensInUsername() {
        final List<String> separators = ImmutableList.of(" ", "@", ".", "-", "\"", ",", "'", "(");
        List<String> multiTokenUsernames = separators
                .stream()
                .map(separator -> "yyy" + separator + "zz")
                .collect(Collectors.toList());
        for (String username : multiTokenUsernames) {
            backdoor.usersAndGroups().addUser(username);
        }

        List<String> foundUserNames = userClient.search("zz", "0", null)
                .stream()
                .map(user -> user.name)
                .collect(Collectors.toList());

        assertThat(foundUserNames, containsInAnyOrder(multiTokenUsernames.toArray()));
    }

    @Test
    public void testSearchDoesNotIgnoreDiacritics() {
        List<User> users = userClient.search("fr�d", "0", null);

        assertEquals(0, users.size());
    }

    @Test
    public void testSearchBySeparatorCharacter() {
        backdoor.usersAndGroups().addUser("fr,,ed");
        List<User> users = userClient.search(",", "0", "100");

        assertEquals(10, users.size());
    }

    @Test
    public void testQueryWithInfixSeparator() {
        backdoor.usersAndGroups().addUser("a@z");
        backdoor.usersAndGroups().addUser("aa@z");
        backdoor.usersAndGroups().addUser("aaa@z");

        List<User> users = userClient.search("a@z", "0", "100");

        assertEquals(3, users.size());
    }

    @Test
    public void testPickerUsers() {
        UserPickerResults results = userClient.picker("fre", null);

        assertEquals(1, results.users.size());
        final UserPickerUser user = results.users.get(0);
        assertEquals("fred", user.name);
        assertEquals("fred", user.key);
        assertEquals("Fred Normal", user.displayName);
        assertEquals("<strong>Fre</strong>d Normal - <strong>fre</strong>d@example.com (<strong>fre</strong>d)", user.html);
        assertEquals("Showing 1 of 1 matching users", results.header);
        //try searching via e-mail
        results = userClient.picker("love", null);

        assertEquals(1, results.users.size());
        assertEquals("\u611b", results.users.get(0).name);
        assertEquals("Showing 1 of 1 matching users", results.header);

        //searching for a should have 3 results
        results = userClient.picker("a", null);
        assertEquals(3, results.users.size());
        assertEquals("<strong>a</strong>\\b - <strong>a</strong>b@example.com (<strong>a</strong>\\b)", results.users.get(0).html);
        assertEquals("<strong>A</strong>dministrator - <strong>a</strong>dmin@example.com (<strong>a</strong>dmin)", results.users.get(1).html);
        assertEquals("sp <strong>a</strong>ce - space@example.com (sp <strong>a</strong>ce)", results.users.get(2).html);
        assertEquals("Showing 3 of 3 matching users", results.header);

        results = userClient.picker("example.com", null);

        assertEquals(9, results.users.size());
        assertEquals("a\\b - ab@<strong>example.com</strong> (a\\b)", results.users.get(0).html);
        assertEquals("Administrator - admin@<strong>example.com</strong> (admin)", results.users.get(1).html);
        assertEquals("Bob Loblaw - bob@<strong>example.com</strong> (bloblaw)", results.users.get(2).html);
        assertEquals("c/d - cd@<strong>example.com</strong> (c/d)", results.users.get(3).html);
        assertEquals("Fred Normal - fred@<strong>example.com</strong> (fred)", results.users.get(4).html);
        assertEquals("per%cent - pct@<strong>example.com</strong> (per%cent)", results.users.get(5).html);
        assertEquals("pl+us - pl+us@<strong>example.com</strong> (pl+us)", results.users.get(6).html);
        assertEquals("sp ace - space@<strong>example.com</strong> (sp ace)", results.users.get(7).html);
        // have trouble with non-latin characters in test ...
        assertTrue(results.users.get(8).html.contains(" - love@<strong>example.com</strong> ("));
        assertEquals("Showing 9 of 9 matching users", results.header);
    }

    @Test
    public void testUsersByPermission() {
        String mike = "Mike";
        String theresa = "Theresa";
        String groupName = "group";

        backdoor.usersAndGroups().addGroup(groupName);
        backdoor.usersAndGroups().addUser(mike);
        backdoor.usersAndGroups().addUserToGroup(mike, groupName);
        backdoor.usersAndGroups().addUser(theresa);

        backdoor.permissionSchemes().addUserPermission(DEFAULT_PERM_SCHEME_ID, EDIT_ALL_COMMENTS, theresa);
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, EDIT_OWN_COMMENTS, groupName);
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGNABLE_USER, groupName);
        backdoor.permissionSchemes().addCurrentAssigneePermission(DEFAULT_PERM_SCHEME_ID, DELETE_OWN_COMMENTS);

        assertEquals("HSP-1", backdoor.issues().createIssue("HSP", "Permissions test").key);

        // check request without or with empty query returns all users
        Matcher<Iterable<? extends String>> allUsersMatcher = contains("a\\b", "admin", "bloblaw", "c/d", mike);

        List<String> usersWhenQueryNotDefined = getUserNames(userClient.searchByPermission(null, "ASSIGNABLE_USER", "HSP-1", null, null, null));
        assertThat(usersWhenQueryNotDefined, allUsersMatcher);

        List<String> usersWhenQueryEmpty = getUserNames(userClient.searchByPermission("", "ASSIGNABLE_USER", "HSP-1", null, null, null));
        assertThat(usersWhenQueryEmpty, allUsersMatcher);

        // check that query works
        assertEquals(ImmutableList.of(mike), getUserNames(userClient.searchByPermission("mi", "ASSIGNABLE_USER", "HSP-1", null, "0", "1")));

        // check that pagination works
        assertEquals(ImmutableList.of("admin", "bloblaw"), getUserNames(userClient.searchByPermission("", "ASSIGNABLE_USER", "HSP-1", null, "1", "2")));

        // test that checking permissions works for project and issue
        List<String> commentEditAllUsersForIssue = getUserNames(userClient.searchByPermission("", "COMMENT_EDIT_ALL", "HSP-1", null, null, null));
        assertThat(commentEditAllUsersForIssue, contains(theresa));

        List<String> commentEditAllForProject = getUserNames(userClient.searchByPermission("", "COMMENT_EDIT_ALL", null, "HSP", null, null));
        assertThat(commentEditAllForProject, contains(theresa));

        // test that checking permissions works correctly after update
        List<String> commentEditAndDeleteUsers1 = getUserNames(userClient.searchByPermission("", "COMMENT_EDIT_OWN,COMMENT_DELETE_OWN", "HSP-1", null, null, null));
        assertThat(commentEditAndDeleteUsers1, empty());

        backdoor.issues().assignIssue("HSP-1", mike);
        List<String> commentEditAndDeleteUsers2 = getUserNames(userClient.searchByPermission("", "COMMENT_EDIT_OWN,COMMENT_DELETE_OWN", "HSP-1", null, null, null));
        assertThat(commentEditAndDeleteUsers2, contains(mike));

        // check that issueKey has higher priority than projectKey
        List<String> permissionForProjectUsers = getUserNames(userClient.searchByPermission("", "COMMENT_DELETE_OWN", null, "HSP", null, null));
        assertThat(permissionForProjectUsers, empty());

        List<String> permissionForProjectAndIssueUsers = getUserNames(userClient.searchByPermission("", "COMMENT_DELETE_OWN", "HSP-1", "HSP", null, null));
        assertThat(permissionForProjectAndIssueUsers, contains(mike));

        // testing that permissions are checked
        Response userResponse = userClient.loginAs("fred").searchByPermissionResponse("", "ASSIGNABLE_USER", "HSP-1", null, null, null);
        assertEquals(403, userResponse.statusCode);
    }

    @Test
    public void testAssignableAndViewableUsers() {
        //only admins will be assignable and only devs can browse issues.
        backdoor.permissionSchemes().replaceGroupPermissions(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGNABLE_USER, JIRA_ADMIN_GROUP);
        backdoor.permissionSchemes().replaceGroupPermissions(DEFAULT_PERM_SCHEME_ID, BROWSE_PROJECTS, JIRA_DEV_GROUP);

        String issueKey = backdoor.issues().createIssue("HSP", "Sample Issue").key;

        //searching for a should have 3 results
        List<User> users = userClient.search("a", null, null);
        assertEquals(3, users.size());
        assertEquals("a\\b", users.get(0).name);
        assertEquals("admin", users.get(1).name);
        assertEquals("sp ace", users.get(2).name);

        //only admin is an admin and can there for be assigned issues
        users = userClient.searchAssignable("a", issueKey, null, null);
        assertEquals(1, users.size());
        assertEquals("admin", users.get(0).name);

        //these are the only devs which can browse issues
        users = userClient.searchViewableIssue("a", issueKey, null, null);
        assertEquals(2, users.size());
        assertEquals("a\\b", users.get(0).name);
        assertEquals("admin", users.get(1).name);
    }

    // JRA-27212
    @Test
    public void testUnprivilegedAccessToBrowseUsersYieldsNoResults() {
        String issueKey = backdoor.issues().createIssue("HSP", "Sample Issue").key;

        // only admins may browse users.
        backdoor.permissions().removeGlobalPermission(USER_PICKER, JIRA_USERS_GROUP);
        backdoor.permissions().removeGlobalPermission(USER_PICKER, JIRA_DEV_GROUP);
        backdoor.permissions().addGlobalPermission(USER_PICKER, JIRA_ADMIN_GROUP);

        List<User> users = userClient.searchViewableIssue("a", issueKey, null, null);
        assertEquals(3, users.size());

        userClient.loginAs("fred");

        users = userClient.searchViewableIssue("a", issueKey, null, null);
        assertEquals(0, users.size());
    }

    // JRA-27212
    @Test
    public void testUnprivilegedAccessToAssignableUsersIsDenied() {
        String issueKey = backdoor.issues().createIssue("HSP", "Sample Issue").key;

        // nobody is allowed to assign issues.
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGN_ISSUES, JIRA_DEV_GROUP);

        Response response = userClient.getResponse(userClient.getSearchAssignableResource("a", issueKey, null, null));
        assertEquals(401, response.statusCode);
    }

    // JRA-27212
    @Test
    public void testDoNotRequireBrowseUserPermissionToListAssignableUsers() {
        String issueKey = backdoor.issues().createIssue("HSP", "Sample Issue").key;

        // only developers are allowed to assign issues.
        backdoor.permissionSchemes().replaceGroupPermissions(DEFAULT_PERM_SCHEME_ID, ASSIGN_ISSUES, JIRA_DEV_GROUP);
        // only admins may be assigned to.
        backdoor.permissionSchemes().replaceGroupPermissions(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGNABLE_USER, JIRA_ADMIN_GROUP);

        // neither group is allowed to browse users.
        backdoor.permissions().removeGlobalPermission(Permissions.USER_PICKER, JIRA_USERS_GROUP);
        backdoor.permissions().removeGlobalPermission(Permissions.USER_PICKER, JIRA_DEV_GROUP);
        backdoor.permissions().removeGlobalPermission(Permissions.USER_PICKER, JIRA_ADMIN_GROUP);

        // log in as a developer.
        userClient.loginAs("c/d", "c/d");

        // ask for a list of assignable people.
        List<User> users = userClient.searchAssignable("a", issueKey, null, null);
        // admin should be returned, because he is the only person who is assignable.
        // "a\\b" should not be returned, as he is a developer.
        assertEquals(1, users.size());
        assertEquals("admin", users.get(0).name);
    }

    @Test
    public void testDoNotRequireBrowseUserPermissionToListAssignableUsersBulk() {
        // only developers are allowed to assign issues.
        backdoor.permissionSchemes().replaceGroupPermissions(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGN_ISSUES, JIRA_DEV_GROUP);

        // only admins may be assigned to.
        backdoor.permissionSchemes().replaceGroupPermissions(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGNABLE_USER, JIRA_ADMIN_GROUP);

        // neither group is allowed to browse users.
        backdoor.permissions().removeGlobalPermission(Permissions.USER_PICKER, JIRA_USERS_GROUP);
        backdoor.permissions().removeGlobalPermission(Permissions.USER_PICKER, JIRA_DEV_GROUP);
        backdoor.permissions().removeGlobalPermission(Permissions.USER_PICKER, JIRA_ADMIN_GROUP);

        // log in as a developer.
        userClient.loginAs("c/d", "c/d");

        // ask for a list of assignable people.
        List<User> users = userClient.multiProjectSearchAssignable("a", "HSP", null, null);
        // admin should be returned, because he is the only person who is assignable.
        // "a\\b" should not be returned, as he is a developer.
        assertEquals(1, users.size());
        assertEquals("admin", users.get(0).name);
    }

    // ACJIRA-431
    @Test
    public void testUsernameParamIsNotMandatoryForAssignableSearchResource() {
        String issueKey = backdoor.issues().createIssue("HSP", "Sample Issue").key;
        WebResource resource = manualClient.createResource().path("user").path("assignable").path("search").queryParam("issueKey", issueKey);
        Response response = manualClient.getResponse(resource);

        assertThat(response.statusCode, is(200));
    }

    // ACJIRA-431
    @Test
    public void testUsernameParamIsNotMandatoryForAssignableMultiProjectSearchResource() {
        backdoor.issues().createIssue("HSP", "Sample Issue");
        WebResource resource = manualClient.createResource().path("user").path("assignable").path("multiProjectSearch").queryParam("projectKeys", "HSP");
        Response response = manualClient.getResponse(resource);

        assertThat(response.statusCode, is(200));
    }

    @Test
    public void testProjectParamIsMandatoryForAssignableMultiProjectSearchResource() {
        backdoor.issues().createIssue("HSP", "Sample Issue");
        WebResource resource = manualClient.createResource().path("user").path("assignable").path("multiProjectSearch");
        Response response = manualClient.getResponse(resource);

        assertThat(response.statusCode, is(400));
    }

    @Test
    public void testUserResourceGroupsAndRolesNotExpanded() throws Exception {
        final String username = FRED_USERNAME;
        final String userPath = getPathFor(username);

        User user = userClient.get(username);
        assertEquals(getBaseUrlPlus(userPath), user.self);

        // verify that groups are not expanded
        assertEquals("groups,applicationRoles", user.expand);

        assertNotNull(user.groups.size);
        assertEquals(1, user.groups.size);

        assertNotNull(user.groups.items);
        assertTrue(user.groups.items.isEmpty());
    }

    @Test
    public void testUserResourceGroupsExpanded() throws Exception {
        final String username = FRED_USERNAME;
        final String userPath = getPathFor(username);

        User user = userClient.get(username, User.Expand.groups);
        assertEquals(getBaseUrlPlus(userPath), user.self);

        assertNotNull(user.groups);
        assertEquals(1, user.groups.size);

        assertNotNull(user.groups.items);
        assertEquals(1, user.groups.items.size());
        assertEquals(JIRA_USERS_GROUP, user.groups.items.get(0).name());
    }

    @Test
    public void testGetAnonymouslyUserResource() throws Exception {
        Response response = userClient.anonymous().getUserResponse("fred");
        assertEquals(401, response.statusCode);
    }

    @Test
    public void testUnicodeCharacters() throws Exception {
        // Unicode 611B = chinese symbol for love
        final String username = "\u611B";
        final String userPath = getPathFor("%E6%84%9B");

        User user = userClient.get(username);
        assertEquals(getBaseUrlPlus(userPath), user.self);

        // Name, id and display name should have UTF-8 encoded Chinese symbols
        assertEquals("\u611b", user.name);
        assertEquals("\u611b \u6237", user.displayName);
        // URLs should have the Unicode character's UTF-8 encoded then the bytes are Percent-encoded
        assertEquals(getBaseUrlPlus(REST_USER_URL + "?username=%E6%84%9B"), user.self);
    }

    @Test
    public void testAvatarUrls() throws Exception {
        final String username = "fred";
        User user = userClient.get(username);

        // Note that the avatar urls no longer contain the username, and therefore do not have any url encoding concerns
        assertThat(user.avatarUrls, equalTo(createUserAvatarUrls(10062L)));
    }

    /*
     * These encoding-related tests are all in the same method for performance reasons.
     */
    @Test
    public void testUsernamesWithInterestingCharacters() throws Exception {
        assertUserRepresentationIsOK("a\\b", "a%5Cb");          // backslash
        assertUserRepresentationIsOK("c/d", "c/d");             // slash
        assertUserRepresentationIsOK("sp ace", "sp+ace");       // space
        assertUserRepresentationIsOK("pl+us", "pl%2Bus");       // +
        assertUserRepresentationIsOK("per%cent", "per%25cent"); // %
        assertUserRepresentationIsOK("\u611B", "%E6%84%9B"); // %
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testUserResourceShouldMaskEmailAddresses() throws Exception {
        generalConfiguration.setUserEmailVisibility(GeneralConfiguration.EmailVisibility.MASKED);
        User user = userClient.get("fred");

        assertThat(user.emailAddress, equalTo("fred at example dot com"));
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testUserResourceShouldHideEmailAddresses() throws Exception {
        generalConfiguration.setUserEmailVisibility(GeneralConfiguration.EmailVisibility.HIDDEN);
        User user = userClient.get("fred");

        assertNull(user.emailAddress);
    }

    @Test
    public void testUserGetAndSetColumns() {
        //New user should have a default set of columns
        List<ColumnControl.ColumnItem> items = backdoor.columnControl().getLoggedInUserColumns();
        List<String> defaultColumns = Lists.newArrayList("issuetype", "issuekey", "summary", "assignee", "reporter",
                "priority", "status", "resolution", "created", "updated", "duedate");
        for (int i = 0; i < items.size(); ++i) {
            assertEquals(defaultColumns.get(0), items.get(0).value);
        }
        assertEquals(defaultColumns.size(), items.size());

        List<String> newColumns = Lists.newArrayList(defaultColumns);
        newColumns.add("description");
        newColumns.add("resolutiondate");
        newColumns.remove("summary");
        newColumns.remove("status");

        assertTrue("No errors when setting the column", backdoor.columnControl().setLoggedInUserColumns(newColumns));

        items = backdoor.columnControl().getLoggedInUserColumns();
        for (int i = 0; i < items.size(); ++i) {
            assertEquals(newColumns.get(0), items.get(0).value);
        }
        assertEquals(newColumns.size(), items.size());
        assertTrue("No errors when removing all columns", backdoor.columnControl().setLoggedInUserColumns(Lists.<String>newArrayList()));
        assertEquals(0, backdoor.columnControl().getLoggedInUserColumns().size());
    }

    /**
     * Test getting list of assignable users where a state override exists on a state we are transitioning to.
     */
    @Test
    public void testWorkflowAssignableOverride() {
        //Add administrators group as project administrators for HSP
        backdoor.projectRole().addActors("HSP", FunctTestConstants.JIRA_ADMIN_ROLE, new String[]{FunctTestConstants.JIRA_ADMIN_GROUP}, null);

        // log in as a developer.
        userClient.loginAs("c/d", "c/d");

        //Create an issue
        IssueCreateResponse r = backdoor.issues().createIssue("HSP", "New issue");
        String issueKey = r.key();

        //Make it so only project admins can be assigned to resolved issues
        WorkflowsControlExt workflowControl = backdoor.workflow();
        workflowControl.addMetaAttributeToWorkflowStep("classic default workflow", FunctTestConstants.STATUS_RESOLVED,
                "jira.permission.assignable.projectrole", String.valueOf(FunctTestConstants.JIRA_ADMIN_ROLE_ID));

        // ask for a list of assignable people, transitioning our issue to resolved
        List<User> users = searchAssignable("a", "HSP", issueKey, "5"); //5 = resolved

        // a/b and admin can be assignable, but for resolved state only admin can be assigned
        assertEquals(1, users.size());
        assertEquals("admin", users.get(0).name);
    }

    /**
     * Test getting list of assignable users where a state override exists on a different state to what we are transitioning to.
     */
    @Test
    public void testWorkflowAssignableOverrideDifferentState() {
        //Add administators group as project administrators for HSP
        backdoor.projectRole().addActors("HSP", FunctTestConstants.JIRA_ADMIN_ROLE, new String[]{FunctTestConstants.JIRA_ADMIN_GROUP}, null);

        // log in as a developer.
        userClient.loginAs("c/d", "c/d");

        //Create an issue
        IssueCreateResponse r = backdoor.issues().createIssue("HSP", "New issue");
        String issueKey = r.key();

        //Make it so only project admins can be assigned to resolved issues
        WorkflowsControlExt workflowControl = backdoor.workflow();
        workflowControl.addMetaAttributeToWorkflowStep("classic default workflow", FunctTestConstants.STATUS_RESOLVED,
                "jira.permission.assignable.projectrole", String.valueOf(FunctTestConstants.JIRA_ADMIN_ROLE_ID));

        // ask for a list of assignable people, transitioning our issue to closed (not resolved)
        List<User> users = searchAssignable("a", "HSP", issueKey, "2"); //2 = closed

        // a/b and admin can be assignable, but for resolved state only admin can be assigned
        assertEquals(2, users.size());
        assertEquals("a\\b", users.get(0).name);
        assertEquals("admin", users.get(1).name);
    }

    private List<User> searchAssignable(String prefix, String projectId, String issueId, String actionDescriptorId) {
        WebResource resource = userClient.getSearchAssignableResource(prefix, issueId, null, "50")
                .queryParam("projectKeys", projectId)
                .queryParam("actionDescriptorId", actionDescriptorId);
        return Arrays.asList(resource.get(User[].class));
    }

    /**
     * Creates the path for the user resource.
     *
     * @param username a String containing the user name
     * @return the path to the user
     */
    protected String getPathFor(String username) {
        return getPathFor(username, emptyMap());
    }

    /**
     * Tests that the user representation is being constructed correctly.
     *
     * @param username        the username
     * @param encodedUsername the encoded username (if encoding is necessary)
     */
    private void assertUserRepresentationIsOK(String username, String encodedUsername) {
        User user = userClient.get(username);
        assertEquals(username, user.name);
        // explicitly assert that the self URL has the encoded username
        assertEquals("The username is not encoded in the self link", getBaseUrlPlus(REST_USER_URL + "?username=" + encodedUsername), user.self);
    }

    /**
     * Creates the path for the user resource, optionally appending any additional query parameters.
     *
     * @param username    a String containing the user name
     * @param queryParams a Map containing query parameters
     * @return the path to the user
     */
    protected String getPathFor(String username, Map<?, ?> queryParams) {
        // append the query params in "&key=value" format
        return REST_USER_URL + "?username=" + username + StringUtils.join(Collections2.transform(queryParams.entrySet(), new Function<Map.Entry, Object>() {
            public Object apply(Map.Entry from) {
                return String.format("&%s=%s", from.getKey(), from.getValue());
            }
        }), "");
    }

    private Map<String, String> createUserAvatarUrls(Long avatarId) {
        return ImmutableMap.<String, String>builder()
                .put("24x24", getBaseUrlPlus("secure/useravatar?size=small&avatarId=" + avatarId))
                .put("16x16", getBaseUrlPlus("secure/useravatar?size=xsmall&avatarId=" + avatarId))
                .put("32x32", getBaseUrlPlus("secure/useravatar?size=medium&avatarId=" + avatarId))
                .put("48x48", getBaseUrlPlus("secure/useravatar?avatarId=" + avatarId))
// TODO JRADEV-20790 - Re-enable the larger avatar sizes.
//            .put("64x64", getBaseUrlPlus("secure/useravatar?size=xlarge&avatarId="+avatarId))
//            .put("96x96", getBaseUrlPlus("secure/useravatar?size=xxlarge&avatarId="+avatarId))
//            .put("128x128", getBaseUrlPlus("secure/useravatar?size=xxxlarge&avatarId="+avatarId))
//            .put("192x192", getBaseUrlPlus("secure/useravatar?size=xxlarge%402x&avatarId="+avatarId)) // %40 == "@"
//            .put("256x256", getBaseUrlPlus("secure/useravatar?size=xxxlarge%402x&avatarId="+avatarId))
                .build();
    }

    private List<String> getUserNames(final List<User> users) {
        return users.stream().map(user -> user.name).collect(Collectors.toList());
    }

    class ManualClient extends RestApiClient<ManualClient> {

        protected ManualClient(final com.atlassian.jira.testkit.client.JIRAEnvironmentData environmentData) {
            super(environmentData);
        }

        @Override
        public WebResource createResource() {
            return super.createResource();
        }

        public Response getResponse(final WebResource resource) {
            return this.toResponse(() -> resource.get(ClientResponse.class));
        }
    }

    private String getBaseUrlPlus(String... paths) {
        final Iterable<String> pathsNoLeadingSlashes = Iterables.transform(Arrays.asList(paths), path -> {
            // remove leading slashes so we don't end up with duplicates
            return path.startsWith("/") ? path.substring(1) : path;
        });

        final String path = StringUtils.join(Lists.newArrayList(pathsNoLeadingSlashes), '/');
        return String.format("%s/%s", environmentData.getBaseUrl().toExternalForm(), path);
    }
}
