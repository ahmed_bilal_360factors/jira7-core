package com.atlassian.jira.webtests.ztests.avatar;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.primitives.Bytes;
import org.apache.commons.io.IOUtils;

import javax.ws.rs.core.UriBuilder;
import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.List;

public class ImageClient {
    public Image get(String url) {
        try {
            HttpURLConnection con = (HttpURLConnection) UriBuilder.fromUri(url).build().toURL().openConnection();

            String contentType = con.getContentType();
            byte[] bytes = IOUtils.toByteArray(con.getInputStream(), con.getContentLength());

            return Image.create(contentType, bytes);
        } catch (Exception ex) {
            throw new RuntimeException(url, ex);
        }
    }

    public static final class Image {
        public enum Type {
            SVG("svg"), PNG("png"), OTHER("");

            private final String contentType;

            Type(final String contentType) {
                this.contentType = contentType;
            }

        }

        private static Image create(String contentType, byte[] bytes) {
            Type type = Arrays.asList(Type.values()).stream().filter(imgType -> contentType.contains(imgType.contentType)).findFirst().get();
            return new Image(type, bytes);
        }

        private Type type;
        private byte[] bytes;

        public Image(Type type, byte[] bytes) {
            this.type = Preconditions.checkNotNull(type);
            this.bytes = bytes;
        }

        public Type getType() {
            return type;
        }

        public void setType(Type type) {
            this.type = Preconditions.checkNotNull(type);
        }

        public byte[] getBytes() {
            return bytes;
        }

        public List<Byte> getBytesList() {
            return Bytes.asList(bytes);
        }

        public void setBytes(byte[] bytes) {
            this.bytes = bytes;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            Image that = (Image) o;

            return Objects.equal(this.type, that.type) &&
                    Objects.equal(this.bytes, that.bytes);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(type, bytes);
        }

        @Override
        public String toString() {
            return Objects.toStringHelper(this)
                    .add("type", type)
                    .add("bytes", bytes)
                    .toString();
        }
    }
}
