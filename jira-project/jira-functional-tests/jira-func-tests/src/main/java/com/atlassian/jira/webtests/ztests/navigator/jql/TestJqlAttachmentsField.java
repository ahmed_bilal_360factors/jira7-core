package com.atlassian.jira.webtests.ztests.navigator.jql;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.JqlAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * @since v6.2
 */
@WebTest({Category.FUNC_TEST, Category.JQL})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestJqlAttachments.xml")
public class TestJqlAttachmentsField extends BaseJiraFuncTest {

    @Inject
    private JqlAssertions jqlAssertions;

    @Before
    public void setUpTest() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    public void testAllIssuesWithAttachments() {
        //Make sure we find the issues in the past.
        navigation.issueNavigator().createSearch("attachments is not EMPTY");
        assertIssues("NUMBER-2", "MKY-3", "HSP-3");

        navigation.issueNavigator().createSearch("attachments is not EMPTY AND project = MKY");
        assertIssues("MKY-3");
    }

    void assertIssues(final String... keys) {
        assertions.getIssueNavigatorAssertions().assertExactIssuesInResults(keys);
    }

    @Test
    public void testIssuesWithoutAttachments() {
        //Make sure we find the issues in the past.
        navigation.issueNavigator().createSearch("attachments is EMPTY");
        assertIssues("NUMBER-1", "MKY-2", "MKY-1", "MK-2", "MK-1", "HSP-4", "HSP-1");
    }

    @Test
    public void testDisableAttachments() {
        backdoor.attachments().disable();
        navigation.issueNavigator().createSearch("attachments is not EMPTY");
        assertIssues("NUMBER-2", "MKY-3", "HSP-3");

        navigation.issueNavigator().createSearch("attachments is EMPTY");
        assertIssues("NUMBER-1", "MKY-2", "MKY-1", "MK-2", "MK-1", "HSP-4", "HSP-1");

        backdoor.attachments().enable();
    }

    @Test
    public void testRemovingAttachments() {
        // HSP-2 has an attachment
        navigation.issueNavigator().createSearch("attachments is not EMPTY");
        assertIssues("NUMBER-2", "MKY-3", "HSP-3", "HSP-2");

        navigation.issue().viewIssue("HSP-2");
        tester.clickLink("manage-attachment-link");
        // Click Link 'Delete'
        tester.clickLink("del_10002");
        tester.submit("Delete");

        navigation.issueNavigator().createSearch("attachments is EMPTY AND issuekey = HSP-2");
        assertIssues("HSP-2");
    }

    @Test
    public void testInvalidJqlQuery() {
        jqlAssertions.assertTooComplex("attachments is \"file.png\"");
        jqlAssertions.assertTooComplex("attachments is not empt");
        jqlAssertions.assertTooComplex("attachments is empt");
        jqlAssertions.assertTooComplex("attachments = empt");
        jqlAssertions.assertTooComplex("attachments != empt");
    }
}
