package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.framework.admin.SendBulkMail;

import javax.inject.Inject;

/**
 * @since v4.4
 */
public class DefaultSendBulkMail implements SendBulkMail {
    private final Navigation navigation;

    @Inject
    public DefaultSendBulkMail(final Navigation navigation) {
        this.navigation = navigation;
    }

    @Override
    public SendBulkMail goTo() {
        navigation.gotoAdminSection(Navigation.AdminSection.SEND_EMAIL);
        return this;
    }
}
