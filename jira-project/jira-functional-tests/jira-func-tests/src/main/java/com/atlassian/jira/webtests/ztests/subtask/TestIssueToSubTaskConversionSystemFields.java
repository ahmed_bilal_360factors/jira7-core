package com.atlassian.jira.webtests.ztests.subtask;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.meterware.httpunit.WebTable;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;

/**
 * This test ensures that the correct systemfields are shown/hidden in the convert issue
 * to subtask wizard.
 */
@WebTest({Category.FUNC_TEST, Category.FIELDS, Category.ISSUES, Category.SUB_TASKS})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestIssueToSubtaskConversionSystemFields.xml")
public class TestIssueToSubTaskConversionSystemFields extends BaseJiraFuncTest {
    private static final String ISSUE_MKY_1 = "MKY-1";
    private static final String PARENT_ISSUE_MKY_3 = "MKY-3";
    private static final String ISSUE_HSP_1 = "HSP-1";
    private static final String PARENT_ISSUE_HSP_2 = "HSP-2";
    private static final String ISSUE_GOD_1 = "GOD-1";
    private static final String PARENT_ISSUE_GOD_2 = "GOD-2";

    @Inject
    SubTaskAssertions subTaskAssertions;

    /**
     * This tests the following:
     * || From || Value? || To || Field Update || Confirmation || Field
     * | H | (x) | H | not shown | not shown | Affects Version
     * | H | (x) | S | not shown | not shown | Assignee
     * | H | (x) | R | prompt | shown | Components
     * | S | (/) | H | not shown | shown | Description
     * | S | (/) | S | not shown | not shown | Due Date
     * | S | (/) | Sr | prompt | shown | Env
     * <p/>
     * Where:
     * X = non-existing field
     * H = not shown, optional field
     * S = shown, optional field
     * Sr = shown, optional field with changed renderer (text <-> wiki)
     * R = shown, required field
     */
    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testConvertHiddenAndSomeShownScenarios() {
        navigation.issue().gotoIssue(ISSUE_MKY_1);
        tester.clickLink("issue-to-subtask");

        // assert 1st screen
        tester.assertTextPresent("Step 1 of 4");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_MKY_1, 1);
        tester.assertTextPresent("Convert Issue to Sub-task: " + ISSUE_MKY_1);

        // set a parent issue
        tester.setFormElement("parentIssueKey", PARENT_ISSUE_MKY_3);
        tester.submit("Next >>");

        // assert 2nd screen was skipped
        tester.assertTextNotPresent("Step 2 of 4");
        // assert we're on the 3rd screen
        tester.assertTextPresent("Step 3 of 4");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_MKY_1, 3);

        //check the correct fields are shown
        tester.assertTextPresent("Component/s:");
        tester.assertTextPresent("Environment:");
        //fields that shouldn't be there:
        tester.assertTextNotPresent("Affects:");
        tester.assertTextNotPresent("Assignee:");
        tester.assertTextNotPresent("Description:");
        tester.assertTextNotPresent("Due Date:");


        tester.submit("Next >>");

        //assert we are still on the 3rd screen and an error is shown since components is a required field.
        // assert we're on the 3rd screen
        tester.assertTextPresent("Step 3 of 4");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_MKY_1, 3);
        tester.assertTextPresent("Component/s is required");

        //now set a component and continue.
        tester.selectOption("components", "Component1");
        tester.submit("Next >>");

        // assert we're on the 4th screen
        tester.assertTextPresent("Step 4 of 4");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_MKY_1, 4);

        //assert all the correct fields are shown.
        final WebTable table = tester.getDialog().getWebTableBySummaryOrId("convert_confirm_table");
        assertEquals("number of rows", 5, table.getRowCount());

        assertions.getTableAssertions().assertTableCellHasText(table, 1, 0, "Type");
        assertions.getTableAssertions().assertTableCellHasText(table, 1, 1, "Bug");
        assertions.getTableAssertions().assertTableCellHasText(table, 1, 2, "Sub-task");
        assertions.getTableAssertions().assertTableCellHasText(table, 2, 0, "Component/s");
        assertions.getTableAssertions().assertTableCellHasNoText(table, 2, 1, "Component1");
        assertions.getTableAssertions().assertTableCellHasText(table, 2, 2, "Component1");
        assertions.getTableAssertions().assertTableCellHasText(table, 3, 0, "Environment");
        assertions.getTableAssertions().assertTableCellHasText(table, 3, 1, "A test value");
        assertions.getTableAssertions().assertTableCellHasText(table, 3, 2, "A test value");
        assertions.getTableAssertions().assertTableCellHasText(table, 4, 0, "Description");
        assertions.getTableAssertions().assertTableCellHasText(table, 4, 1, "A test value");
        assertions.getTableAssertions().assertTableCellHasNoText(table, 4, 2, "A test value");

        //finish the wizard
        tester.submit("Finish");

        textAssertions.assertTextPresent(new IdLocator(tester, "parent_issue_summary"), PARENT_ISSUE_MKY_3 + " A third monkey issue");
        textAssertions.assertTextPresent(new IdLocator(tester, "key-val"), ISSUE_MKY_1);
        textAssertions.assertTextPresent(new CssLocator(tester, "#content header h1"), "A monkey bug");
        textAssertions.assertTextPresent(new IdLocator(tester, "type-val"), "Sub-task");

    }

    /**
     * This tests the following:
     * || From || Value? || To || Field Update || Confirmation || Field
     * | S | (/) | R | not shown | not shown | Affects Vers
     * | S | (/) | Rr | prompt | shown | Description
     * | S | (x) | H | not shown | not shown | Due date
     * | S | (x) | R | prompt | shown | Component
     * | R | (/) | H | not shown | shown | Fix Version
     * | R | (/) | S | not shown | not shown | Priority
     * | R | (/) | Sr | prompt | shown | Environment
     * | R | (/) | R | not shown | not shown | Summary
     * <p/>
     * Where:
     * X = non-existing field
     * H = not shown, optional field
     * S = shown, optional field
     * Sr = shown, optional field with changed renderer (text <-> wiki)
     * R = shown, required field
     */
    @Test
    public void testShownAndSomeRequiredScenarios() {
        navigation.issue().gotoIssue(ISSUE_HSP_1);
        tester.clickLink("issue-to-subtask");

        // assert 1st screen
        tester.assertTextPresent("Step 1 of 4");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_HSP_1, 1);
        tester.assertTextPresent("Convert Issue to Sub-task: " + ISSUE_HSP_1);

        // set a parent issue
        tester.setFormElement("parentIssueKey", PARENT_ISSUE_HSP_2);
        tester.submit("Next >>");

        // assert 2nd screen was skipped
        tester.assertTextNotPresent("Step 2 of 4");
        // assert we're on the 3rd screen
        tester.assertTextPresent("Step 3 of 4");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_HSP_1, 3);

        //check the correct fields are shown
        tester.assertTextPresent("Component/s:");
        tester.assertTextPresent("Environment:");
        tester.assertTextPresent("Description:");
        //fields that shouldn't be there:
        tester.assertTextNotPresent("Affects:");
        tester.assertTextNotPresent("Due Date:");
        tester.assertTextNotPresent("Priority:");
        tester.assertTextNotPresent("Summary:");

        tester.submit("Next >>");

        //assert we are still on the 3rd screen and an error is shown since components is a required field.
        // assert we're on the 3rd screen
        tester.assertTextPresent("Step 3 of 4");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_HSP_1, 3);
        tester.assertTextPresent("Component/s is required");

        //now set a component and continue.
        tester.selectOption("components", "New Component 1");
        tester.submit("Next >>");

        // assert we're on the 4th screen
        tester.assertTextPresent("Step 4 of 4");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_HSP_1, 4);

        //assert all the correct fields are shown.
        final WebTable table = tester.getDialog().getWebTableBySummaryOrId("convert_confirm_table");
        assertEquals("number of rows", 6, table.getRowCount());

        assertions.getTableAssertions().assertTableCellHasText(table, 1, 0, "Type");
        assertions.getTableAssertions().assertTableCellHasText(table, 1, 1, "Bug");
        assertions.getTableAssertions().assertTableCellHasText(table, 1, 2, "Sub-task");
        assertions.getTableAssertions().assertTableCellHasText(table, 2, 0, "Component/s");
        assertions.getTableAssertions().assertTableCellHasNoText(table, 2, 1, "New Component 1");
        assertions.getTableAssertions().assertTableCellHasText(table, 2, 2, "New Component 1");
        assertions.getTableAssertions().assertTableCellHasText(table, 3, 0, "Description");
        assertions.getTableAssertions().assertTableCellHasText(table, 3, 1, "A test desc");
        assertions.getTableAssertions().assertTableCellHasText(table, 3, 2, "A test desc");
        assertions.getTableAssertions().assertTableCellHasText(table, 4, 0, "Environment");
        assertions.getTableAssertions().assertTableCellHasText(table, 4, 1, "A test env");
        assertions.getTableAssertions().assertTableCellHasText(table, 4, 2, "A test env");
        assertions.getTableAssertions().assertTableCellHasText(table, 5, 0, "Fix Version/s");
        assertions.getTableAssertions().assertTableCellHasText(table, 5, 1, "New Version 1");
        assertions.getTableAssertions().assertTableCellHasNoText(table, 5, 2, "New Version 1");

        //finish the wizard
        tester.submit("Finish");

        textAssertions.assertTextPresent(new IdLocator(tester, "parent_issue_summary"), PARENT_ISSUE_HSP_2 + " A second issue");
        textAssertions.assertTextPresent(new IdLocator(tester, "key-val"), ISSUE_HSP_1);
        textAssertions.assertTextPresent(new CssLocator(tester, "#content header h1"), "A new issue");
        textAssertions.assertTextPresent(new IdLocator(tester, "type-val"), "Sub-task");
    }

    /**
     * This tests the following:
     * || From || Value? || To || Field Update || Confirmation || Field
     * | S | (x) | S | not shown | not shown | Environment
     * | R | (/) | Rr | prompt | shown | Description
     * <p/>
     * Where:
     * X = non-existing field
     * H = not shown, optional field
     * S = shown, optional field
     * Sr = shown, optional field with changed renderer (text <-> wiki)
     * R = shown, required field
     */
    @Test
    public void testSomeMoreShownAndSomeRequiredScenarios() {
        navigation.issue().gotoIssue(ISSUE_GOD_1);
        tester.clickLink("issue-to-subtask");

        // assert 1st screen
        tester.assertTextPresent("Step 1 of 4");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_GOD_1, 1);
        tester.assertTextPresent("Convert Issue to Sub-task: " + ISSUE_GOD_1);

        // set a parent issue
        tester.setFormElement("parentIssueKey", PARENT_ISSUE_GOD_2);
        tester.submit("Next >>");

        // assert 2nd screen was skipped
        tester.assertTextNotPresent("Step 2 of 4");
        // assert we're on the 3rd screen
        tester.assertTextPresent("Step 3 of 4");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_GOD_1, 3);

        //check the correct fields are shown
        tester.assertTextPresent("Description:");
        //fields that shouldn't be there:
        tester.assertTextNotPresent("Component/s:");
        tester.assertTextNotPresent("Environment:");
        tester.assertTextNotPresent("Affects:");
        tester.assertTextNotPresent("Due Date:");
        tester.assertTextNotPresent("Priority:");
        tester.assertTextNotPresent("Summary:");

        tester.submit("Next >>");

        // assert we're on the 4th screen
        tester.assertTextPresent("Step 4 of 4");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_GOD_1, 4);

        //assert all the correct fields are shown.
        final WebTable table = tester.getDialog().getWebTableBySummaryOrId("convert_confirm_table");
        assertEquals("number of rows", 3, table.getRowCount());

        assertions.getTableAssertions().assertTableCellHasText(table, 1, 0, "Type");
        assertions.getTableAssertions().assertTableCellHasText(table, 1, 1, "Bug");
        assertions.getTableAssertions().assertTableCellHasText(table, 1, 2, "Sub-task");
        assertions.getTableAssertions().assertTableCellHasText(table, 2, 0, "Description");
        assertions.getTableAssertions().assertTableCellHasText(table, 2, 1, "A test desc");
        assertions.getTableAssertions().assertTableCellHasText(table, 2, 2, "A test desc");

        //finish the wizard
        tester.submit("Finish");

        textAssertions.assertTextPresent(new IdLocator(tester, "parent_issue_summary"), PARENT_ISSUE_GOD_2 + " The overlord");
        textAssertions.assertTextPresent(new IdLocator(tester, "key-val"), ISSUE_GOD_1);
        textAssertions.assertTextPresent(new CssLocator(tester, "#content header h1"), "A new issue");
        textAssertions.assertTextPresent(new IdLocator(tester, "type-val"), "Sub-task");
    }


}
