package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FuncTestUrlHelper;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Attachment;
import com.atlassian.jira.testkit.client.restclient.AttachmentRendered;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Func test case for issue resource attachments functionality.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestIssueResourceAttachments.xml")
public class TestIssueResourceAttachments extends BaseJiraFuncTest {
    private static final String ISSUE_KEY = "MKY-1";
    private IssueClient issueClient;

    @Inject
    private FuncTestUrlHelper funcTestUrlHelper;

    @Inject
    TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        issueClient = new IssueClient(environmentData);
    }

    @Test
    public void testAttachmentsExpanded() throws Exception {
        Issue issue = issueClient.get(ISSUE_KEY);
        assertEquals(ISSUE_KEY, issue.key);
        assertEquals(3, issue.fields.attachment.size());

        // check only attachment 1:
        // {
        //     self: "http://localhost:8090/jira/rest/api/2/attachment/10000",
        //     filename: "attachment.txt",
        //     author": {
        //       self: "http://localhost:8090/jira/rest/api/2/user/admin",
        //       name: "admin",
        //       fullName: "Administrator"
        //     },
        //     created: "2010-06-09T15:59:34.602+1000",
        //     size: 19,
        //     mimeType: "text/plain",
        //     content: "http://localhost:8090/jira/secure/attachment/10000/attachment.txt"
        // }
        for (Attachment attachment1 : issue.fields.attachment) {
            if (attachment1.self.endsWith("/10000")) {
                assertEquals(funcTestUrlHelper.getBaseUrl() + "/rest/api/2/attachment/10000", attachment1.self);
                assertEquals("attachment.txt", attachment1.filename);
                assertEquals(funcTestUrlHelper.getBaseUrl() + "/rest/api/2/user?username=admin", attachment1.author.self);
                assertEquals(ADMIN_USERNAME, attachment1.author.name);
                assertEquals(ADMIN_FULLNAME, attachment1.author.displayName);
                textAssertions.assertEqualDateStrings("2010-06-09T15:59:34.602+1000", attachment1.created);
                assertEquals((long) 19, attachment1.size);
                assertEquals("text/plain", attachment1.mimeType);
                assertEquals(funcTestUrlHelper.getBaseUrl() + "/secure/attachment/10000/attachment.txt", attachment1.content);
                return;
            }
        }

        fail("attachment 10000 is missing");
    }

    @Test
    public void testAttachmentsRendered() {
        Issue issue = issueClient.get(ISSUE_KEY, Issue.Expand.renderedFields);
        assertEquals(ISSUE_KEY, issue.key);
        assertEquals(3, issue.fields.attachment.size());

        int attachCount = 0;
        for (AttachmentRendered attachment1 : issue.renderedFields.attachment) {
            if (attachment1.self.endsWith("/10000")) {
                assertEquals(funcTestUrlHelper.getBaseUrl() + "/rest/api/2/attachment/10000", attachment1.self);
                assertEquals("attachment.txt", attachment1.filename);
                assertEquals(funcTestUrlHelper.getBaseUrl() + "/rest/api/2/user?username=admin", attachment1.author.self);
                assertEquals(ADMIN_USERNAME, attachment1.author.name);
                assertEquals(ADMIN_FULLNAME, attachment1.author.displayName);
                textAssertions.assertEqualDateStrings("09/Jun/10 3:59 PM", attachment1.created);
                assertEquals("0.0 kB", attachment1.size);
                assertEquals("text/plain", attachment1.mimeType);
                assertEquals(funcTestUrlHelper.getBaseUrl() + "/secure/attachment/10000/attachment.txt", attachment1.content);
                attachCount++;
            } else if (attachment1.self.endsWith("/10010")) {
                assertEquals("123 kB", attachment1.size);
                textAssertions.assertEqualDateStrings("28/Jul/11 12:12 PM", attachment1.created);
                attachCount++;
            } else if (attachment1.self.endsWith("/10001")) {
                assertEquals("0.0 kB", attachment1.size);
                textAssertions.assertEqualDateStrings("09/Jun/10 3:59 PM", attachment1.created);
                attachCount++;
            }
        }

        assertEquals("attachments collection didn't match", 3, attachCount);
    }

}
