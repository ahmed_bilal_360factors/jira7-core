package com.atlassian.jira.webtests.ztests.subtask;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.inject.Inject;

/**
 * Test the subtask security to ensure a parent cant be seen if security dictates.
 * <p/>
 * The user TED does not have permission to see HSP-1 which is a parent issue to HSP-3, which he can see. The user BIll
 * does have permission to see HSP-1 which is a parent issue to HSP-2, which he can see.
 *
 * @since v3.12
 */
@WebTest({Category.FUNC_TEST, Category.SECURITY})
@Restore("TestSubtaskSecurity.xml")
public class TestSubtaskSecurity extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Before
    public void setUp() {
        administration.attachments().enable();
    }

    @Test
    @LoginAs(user = "ted")
    public void parentIssueShouldNotBeLinkedOnIssueNavigator() {
        navigation.issueNavigator().displayAllIssues();
        tester.assertLinkPresentWithText("HSP-3");
        tester.assertLinkNotPresentWithText("HSP-1");
    }

    @Test
    @LoginAs(user = "ted")
    public void parentIssueShouldNotBeLinkedOnManageAttachmentsPage() {
        navigation.issue().attachments("HSP-3").manage();
        tester.assertTextPresent("<li>HSP-1</li>");
        tester.assertLinkNotPresent("parent_issue_summary");
        tester.assertLinkNotPresentWithText("HSP-1");
    }

    @Test
    @LoginAs(user = "ted")
    @Ignore("Unignore or remove with resolution of JSEV-36")
    public void parentIssueShouldNotBeLinkedOnViewIssuePage() {
        navigation.issue().gotoIssue("HSP-3");
        tester.assertLinkNotPresent("parent_issue_summary");
        tester.assertLinkNotPresentWithText("HSP-1");
    }

    @Test
    @LoginAs(user = "bill")
    public void parentIssueShouldBeLinkedOnIssueNavigator() {
        navigation.issueNavigator().displayAllIssues();
        tester.assertLinkPresentWithText("HSP-1");
    }

    @Test
    @LoginAs(user = "bill")
    public void parentIssueShouldBeLinkedOnManageAttachmentsPage() {
        navigation.issue().attachments("HSP-2").manage();
        tester.assertTextNotPresent("<li>HSP-1</li>");
        tester.assertLinkPresent("parent_issue_summary");
        tester.assertLinkPresentWithText("HSP-1");
    }

    @Test
    @LoginAs(user = "bill")
    public void parentIssueShouldBeLinkedOnViewIssuePage() {
        navigation.issue().gotoIssue("HSP-2");
        tester.assertLinkPresent("parent_issue_summary");
        tester.assertLinkPresentWithText("HSP-1");
    }

    @Test
    @LoginAs(user = "bill")
    public void parentIssueShouldBeLinkedOnVersions() {
        navigation.gotoPage("browse/HSP/fixforversion/10000");
        tester.assertLinkPresentWithText("HSP-1");
    }

    @Test
    @LoginAs(user = "bill")
    public void parentIssueShouldBeLinkedOnComponents() {
        navigation.gotoPage("issues/?jql=project+%3D+HSP+AND+component+%3D+%22New+Component+1%22");
        tester.assertLinkPresentWithText("HSP-1");
    }

    @Test
    @LoginAs(user = "bill")
    public void parentIssueShouldBeLinkedOnTimeTrackingReport() {
        navigation.browseProject("HSP");
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Time Tracking Report");
        tester.selectOption("versionId", "- New Version 1");
        tester.submit("Next");

        tester.assertLinkPresentWithText("HSP-1");
    }

    @Test
    @LoginAs(user = "ted")
    public void parentIssueShouldNotBeLinkedOnVersions() {
        navigation.gotoPage("browse/HSP/fixforversion/10000");
        tester.assertLinkNotPresentWithText("HSP-1");
    }

    @Test
    @LoginAs(user = "ted")
    public void parentIssueShouldNotBeLinkedOnComponents() {
        navigation.gotoPage("issues/?jql=project+%3D+HSP+AND+component+%3D+%22New+Component+1%22");
        tester.assertLinkNotPresentWithText("HSP-1");
    }

    @Test
    @LoginAs(user = "ted")
    public void parentIssueShouldNotBeLinkedOnTimeTrackingReport() {
        navigation.browseProject("HSP");
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Time Tracking Report");
        tester.selectOption("versionId", "- New Version 1");
        tester.submit("Next");

        tester.assertTextPresent("<span class=\"smallgrey\">HSP-1</span>");
        tester.assertLinkNotPresentWithText("HSP-1");
    }

}
