package com.atlassian.jira.functest.framework.navigation;

import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LocatorFactoryImpl;
import com.atlassian.jira.functest.framework.admin.Subtasks;
import com.atlassian.jira.functest.framework.admin.SubtasksImpl;
import com.atlassian.jira.functest.framework.admin.TimeTracking;
import com.atlassian.jira.functest.framework.admin.TimeTrackingImpl;
import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.assertions.AssertionsImpl;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.locator.Locator;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock;
import com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock.ViewMode;
import com.atlassian.jira.functest.framework.navigation.issue.DefaultAttachmentManagement;
import com.atlassian.jira.functest.framework.navigation.issue.DefaultAttachmentsBlock;
import com.atlassian.jira.functest.framework.navigation.issue.DefaultFileAttachmentsList;
import com.atlassian.jira.functest.framework.navigation.issue.DefaultImageAttachmentsGallery;
import com.atlassian.jira.functest.framework.page.ViewIssuePage;
import com.atlassian.jira.functest.framework.util.form.FormParameterUtil;
import com.atlassian.jira.testkit.client.IssueTypeControl;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.testkit.client.log.FuncTestLoggerImpl;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableMap;
import net.sourceforge.jwebunit.WebTester;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.w3c.dom.Node;

import javax.inject.Inject;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Navigate Issue functionality
 *
 * @since v3.13
 */
public class IssueNavigationImpl implements IssueNavigation {

    private static final String WORKFLOW_ACTION_RESOLVE_ISSUE = "action_id_5";
    private static final String WORKFLOW_ACTION_REOPEN_ISSUE = "action_id_3";
    private static final String WORKFLOW_ACTION_CLOSE_ISSUE = "action_id_2";
    private final Backdoor backdoor;
    private final ViewIssuePage viewIssuePage;
    private final FuncTestLogger logger;
    private final Assertions assertions;
    private final LocatorFactory locators;
    private final TimeTracking timeTracking;
    private final IssueNavigatorNavigation issueNavigatorNavigation;
    private final Subtasks subtasks;
    private final WebTester tester;
    private final HtmlPage htmlPage;

    private Supplier<List<IssueTypeControl.IssueType>> issueTypes = Suppliers.memoize(new Supplier<List<IssueTypeControl.IssueType>>() {
        @Override
        public List<IssueTypeControl.IssueType> get() {
            return backdoor.issueType().getIssueTypes();
        }
    });

    private Supplier<List<Project>> projects = Suppliers.memoize(new Supplier<List<Project>>() {
        @Override
        public List<Project> get() {
            return backdoor.project().getProjects();
        }
    });

    /**
     * @param tester
     * @param environmentData
     * @deprecated
     */
    @Inject
    public IssueNavigationImpl(final WebTester tester, final JIRAEnvironmentData environmentData) {
        this.tester = tester;
        this.htmlPage = new HtmlPage(tester);
        this.backdoor = new Backdoor(environmentData);
        this.locators = new LocatorFactoryImpl(tester);
        this.viewIssuePage = new ViewIssuePage(tester, locators);
        this.logger = new FuncTestLoggerImpl(2);
        this.timeTracking = new TimeTrackingImpl(tester, environmentData);
        this.subtasks = new SubtasksImpl(tester, environmentData);
        this.issueNavigatorNavigation = new IssueNavigatorNavigationImpl(tester, environmentData);

        // this particular usage can have null navigation!
        this.assertions = new AssertionsImpl(tester, environmentData, null, locators);

    }

    @Override
    public ViewIssuePage viewIssue(final String issueKey) {
        tester.gotoPage("/browse/" + issueKey);
        return viewIssuePage;
    }

    @Override
    public void viewPrintable(final String issueKey) {
        tester.gotoPage("/si/jira.issueviews:issue-html/" + issueKey + "/" + issueKey + ".html");
    }

    @Override
    public void viewXml(final String issueKey) {
        tester.gotoPage("/si/jira.issueviews:issue-xml/" + issueKey + "/" + issueKey + ".xml");
    }

    @Override
    public void gotoEditIssue(final String issueKey) {
        tester.gotoPage("/browse/" + issueKey);
        tester.clickLink("edit-issue");
    }

    @Override
    public void gotoEditIssue(final long issueId) {
        tester.gotoPage("/secure/EditIssue!default.jspa?id=" + issueId);
    }

    @Override
    public void deleteIssue(final String issueKey) {
        viewIssue(issueKey);
        tester.clickLink("delete-issue");
        tester.submit("Delete");
    }

    @Override
    public void gotoIssue(final String issueKey) {
        if (issueKey == null) {
            throw new IllegalArgumentException("IssueKey should not be null.");
        }
        tester.beginAt("/browse/" + issueKey + "?showAll=true");
    }

    @Override
    public void gotoIssueChangeHistory(final String issueKey) {
        if (issueKey == null) {
            throw new IllegalArgumentException("IssueKey should not be null.");
        }

        tester.beginAt("/browse/" + issueKey + "?page=com.atlassian.jira.plugin.system.issuetabpanels:changehistory-tabpanel");
    }

    @Override
    public void gotoIssueWorkLog(final String issueKey) {
        tester.beginAt("/browse/" + issueKey + "?page=com.atlassian.jira.plugin.system.issuetabpanels:worklog-tabpanel");
    }

    @Override
    public void gotoIssueComments(final String issueKey) {
        tester.beginAt("/browse/" + issueKey + "?page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel");
    }

    @Override
    public String createIssue(final String projectName, final String issueType, final String summary) {
        goToCreateIssueForm(projectName, issueType);
        return createIssueDetails(summary, null);
    }

    @Override
    public String createIssue(final String projectName, final String issueType, final String summary, final Map<String, String[]> params) {
        goToCreateIssueForm(projectName, issueType);
        return createIssueDetails(summary, null, params);
    }

    @Override
    public String createSubTask(final String parentIssueKey, final String subTaskType, final String subTaskSummary, final String subTaskDescription) {
        return createSubTask(parentIssueKey, subTaskType, subTaskSummary, subTaskDescription, null);
    }

    @Override
    public String createSubTask(final String parentIssueKey, final String subTaskType, final String subTaskSummary, final String subTaskDescription, final String originalEstimate) {
        createSubTaskStep1(parentIssueKey, subTaskType);
        if (originalEstimate != null) {
            tester.setFormElement("timetracking", "2h");
        }
        return createIssueDetails(subTaskSummary, subTaskDescription);
    }


    private String createIssueDetails(final String summary, final String description) {
        String issueKey;
        tester.setFormElement("summary", summary);
        if (description != null) {
            tester.setFormElement("description", description);
        }
        tester.submit("Create");
        final Locator idLocator = new IdLocator(tester, "key-val");
        issueKey = getIssueKey(idLocator);
        if (issueKey == null) {
            final Locator locator = new CssLocator(tester, "#cant-browse-warning");
            issueKey = getNoPermissionError(locator);
        }
        return issueKey;
    }

    private String getIssueKey(final Locator keyLocator) {
        String issueKey = null;
        // find the issue key on the page is in the first row and the second TD
        final Node[] tds = keyLocator.getNodes();

        if (tds.length > 0) {
            issueKey = keyLocator.getRawText();
            Assert.assertNotNull(issueKey);
        }
        return issueKey;
    }

    private String getNoPermissionError(final Locator locator) {
        // issue created but user does not have permission to see it?
        final Pattern compile = Pattern.compile("\\((.*-\\d+)\\)");
        final Node[] tds = locator.getNodes();
        for (final Node td : tds) {
            final String text = locator.getText(td);
            if (text != null) {
                final Matcher matcher = compile.matcher(text);
                if (matcher.find()) {
                    return matcher.group(1);
                }
            }
        }
        // don't know what to do?
        Assert.fail("Could not find issue key for newly created issue");
        return null;
    }

    private String createIssueDetails(final String summary, final String description, final Map<String, String[]> params) {
        if (params == null) {
            return createIssueDetails(summary, description);
        } else {
            final FormParameterUtil formParamHelper = new FormParameterUtil(tester, "issue-create", "Create");
            formParamHelper.setFormElement("summary", summary);
            if (description != null) {
                formParamHelper.setFormElement("description", description);
            }
            formParamHelper.setParameters(params);
            final Node createdIssueNode = formParamHelper.submitForm();
            // find the issue key on the page is in the first row and the second TD
            final Locator issueLocator = new XPathLocator(createdIssueNode, "//*[@id='key-val']");
            String issueKey = getIssueKey(issueLocator);
            if (issueKey == null) {
                final Locator locator = new CssLocator(createdIssueNode, "#cant-browse-warning");
                issueKey = getNoPermissionError(locator);
            }
            return issueKey;
        }
    }

    @Override
    public void goToCreateIssueForm(final String projectName, final String issueType) {
        //make sure we're no longer in the admin section (where the create issue link is no longer displayed).
        if (tester.getDialog().isLinkPresent("leave_admin")) {
            tester.clickLink("leave_admin");
        }
        tester.clickLink("create_link");
        if (tester.getDialog().getElement("issuetype") != null) {
            if (projectName != null) {
                selectProject(projectName);
            }
            if (issueType != null) {
                selectIssueType(issueType);
            }
            tester.setWorkingForm("issue-create");
            tester.submit();
        }
        tester.assertTextPresent("CreateIssueDetails.jspa");
        if (issueType != null) {
            tester.assertTextPresent(issueType);
        }
    }

    @Override
    public void selectProject(final String projectName) {
        selectProject(projectName, "pid");
    }

    @Override
    public void selectProject(final String projectName, final String projectFieldName) {
        for (final Project p : projects.get()) {
            if (projectName.equals(p.name)) {
                tester.setFormElement(projectFieldName, p.id);
            }
        }
    }

    @Override
    public void selectIssueType(final String issueType) {
        selectIssueType(issueType, "issuetype");
    }

    @Override
    public void selectIssueType(final String issueType, final String issueTypeFieldId) {
        for (final IssueTypeControl.IssueType i : issueTypes.get()) {
            if (issueType.equals(i.getName())) {
                tester.setFormElement(issueTypeFieldId, i.getId());
            }
        }
    }

    @Override
    public void createSubTaskStep1(final String issueKey, final String task_type) {
        subtasks.enable();
        viewIssue(issueKey);
        tester.clickLink("create-subtask");
        tester.assertTextPresent("Create Sub-Task");
        if (tester.getDialog().getElement("issuetype") == null) {
            logger.log("Bypassing step 1 of sub task creation");
        } else {
            tester.setWorkingForm("subtask-create-start");
            selectIssueType(task_type);
            tester.submit("Create");
        }
        tester.assertElementPresent("subtask-create-details"); // ID of the Subtask Details
    }

    @Override
    public void setPriority(final String issueKey, final String priority) {
        viewIssue(issueKey);
        tester.clickLink("edit-issue");
        tester.setWorkingForm("issue-edit");
        tester.selectOption("priority", priority);
        tester.submit("Update");
    }

    @Override
    public void setSecurity(final String issueKey, final String security) {
        viewIssue(issueKey);
        tester.clickLink("edit-issue");
        tester.setWorkingForm("issue-edit");
        tester.selectOption("security", security);
        tester.submit("Update");
    }

    @Override
    public void setEnvironment(final String issueKey, final String environment) {
        setSingleIssueField(issueKey, "environment", environment);
    }

    @Override
    public void setDescription(final String issueKey, final String description) {
        setSingleIssueField(issueKey, "description", description);
    }

    @Override
    public void setFreeTextCustomField(final String issueKey, final String customFieldId, final String text) {
        setSingleIssueField(issueKey, customFieldId, text);
    }

    private void setSingleIssueField(final String issueKey, final String formElementName, final String value) {
        viewIssue(issueKey);
        tester.clickLink("edit-issue");
        tester.setWorkingForm("issue-edit");
        tester.setFormElement(formElementName, value);
        tester.submit("Update");
    }

    @Override
    public void assignIssue(final String issueKey, final String comment, final String userFullName) {
        viewIssue(issueKey);
        tester.clickLink("assign-issue");
        tester.selectOption("assignee", userFullName);
        if (comment != null) {
            tester.setFormElement("comment", comment);
        }
        tester.submit("Assign");
        tester.assertTextPresent(userFullName);
        if (comment != null) {
            tester.assertTextPresent(comment);
        }
    }

    @Override
    public void assignIssue(final String issueKey, final String userFullName, final String comment, final String commentLevel) {
        viewIssue(issueKey);
        tester.clickLink("assign-issue");
        tester.setWorkingForm("assign-issue");

        tester.selectOption("assignee", userFullName);

        if (comment != null) {
            tester.setFormElement("comment", comment);
            if (commentLevel != null) {
                tester.selectOption("commentLevel", commentLevel);
            }
        }
        tester.submit("Assign");
    }

    @Override
    public void unassignIssue(final String issueKey, final String comment) {
        assignIssue(issueKey, comment, "Unassigned");
    }

    @Override
    public void unassignIssue(final String issueKey, final String comment, final String commentLevel) {
        assignIssue(issueKey, "Unassigned", comment, commentLevel);
    }

    @Override
    public void resolveIssue(final String issueKey, final String resolution, final String comment) {
        resolveIssue(issueKey, resolution, ImmutableMap.of("comment", comment));
    }

    @Override
    public void resolveIssue(final String issueKey, final String resolution, final Map<String, String> fieldValues) {
        viewIssue(issueKey);
        tester.clickLink(WORKFLOW_ACTION_RESOLVE_ISSUE);
        tester.setWorkingForm("issue-workflow-transition");
        tester.selectOption("resolution", resolution);
        for (final Map.Entry<String, String> entry : fieldValues.entrySet()) {
            tester.setFormElement(entry.getKey(), entry.getValue());
        }
        tester.submit("Transition");
    }

    @Override
    public void closeIssue(final String issueKey, final String resolution, final String comment) {
        viewIssue(issueKey);

        tester.clickLink(WORKFLOW_ACTION_CLOSE_ISSUE);
        tester.setWorkingForm("issue-workflow-transition");
        tester.selectOption("resolution", resolution);
        tester.setFormElement("comment", comment);
        tester.submit("Transition");
    }

    @Override
    public void linkIssueWithComment(final String currentIssueKey, final String link, final String destinationIssueKey, final String comment, final String commentLevel) {
        linkIssueWithComment(currentIssueKey, link, destinationIssueKey, comment, commentLevel, destinationIssueKey);
    }

    @Override
    public void linkIssueWithComment(final String currentIssueKey, final String link, final String destinationIssueKey, final String comment, final String commentLevel, final String expectedText) {
        logger.log("Link Issue: test linking an issue");

        final StringBuilder url = new StringBuilder().append("/secure/LinkJiraIssue.jspa?atl_token=").append(htmlPage.getXsrfToken()).
                append("&id=").append(backdoor.issues().getIssue(currentIssueKey).id).append("&linkDesc=").append(link).append("&currentIssueKey=").
                append(currentIssueKey).append("&issueKeys=").append(destinationIssueKey);
        if (comment != null) {
            url.append("&comment=").append(comment).append("&commentLevel=").append(commentLevel);

        }
        tester.gotoPage(url.toString());
        tester.assertTextPresent(expectedText);
    }

    @Override
    public AttachmentsBlock attachments(final String issueKey) {
        viewIssue(issueKey);
        final DefaultAttachmentsBlock attachmentsBlock = new DefaultAttachmentsBlock(tester, logger, new DefaultFileAttachmentsList(tester, logger),
                new DefaultImageAttachmentsGallery(tester, logger), new DefaultAttachmentManagement(tester, logger));
        return attachmentsBlock;
    }

    @Override
    public AttachmentsBlock attachments(final String issueKey, final ViewMode viewMode) {
        final AttachmentsBlock attachments = attachments(issueKey);
        attachments.setViewMode(viewMode);
        return attachments;
    }

    @Override
    public void resolveIssue(final String issueKey, final String resolution, final String comment, final String originalEstimate,
                             final String remainingEstimate) {
        final String originalEstimateFieldId = getOriginalEstimateFieldId();
        final String remainingEstimateFieldId = getRemainingEstimateFieldId();

        viewIssue(issueKey);
        tester.clickLink(WORKFLOW_ACTION_RESOLVE_ISSUE);
        tester.setWorkingForm("issue-workflow-transition");
        tester.selectOption("resolution", resolution);
        tester.setFormElement("comment", comment);

        if (originalEstimate != null) {
            tester.setFormElement(originalEstimateFieldId, originalEstimate);
        }
        if (remainingEstimate != null) {
            tester.setFormElement(remainingEstimateFieldId, remainingEstimate);
        }
        tester.submit("Transition");
    }

    @Override
    public void logWork(final String issueKey, final String timeLogged) {
        viewIssue(issueKey);
        tester.clickLink("log-work");
        tester.setFormElement("timeLogged", timeLogged);
        tester.submit("Log");
    }

    /**
     * Logs work on the issue with the given key.
     *
     * @param issueKey    the key of the issue to log work on.
     * @param timeLogged  formatted time spent e.g. 1h 30m.
     * @param newEstimate formatted new estimate e.g. 1d 2h.
     */
    @Override
    public void logWork(final String issueKey, final String timeLogged, final String newEstimate) {
        gotoIssue(issueKey);
        tester.clickLink("log-work");
        tester.setFormElement("timeLogged", timeLogged);
        tester.checkCheckbox("adjustEstimate", "new");
        tester.setFormElement("newEstimate", newEstimate);
        tester.submit("Log");
    }

    @Override
    public void logWorkWithComment(final String issueKey, final String timeLogged, final String comment) {
        gotoIssue(issueKey);
        tester.clickLink("log-work");
        tester.setFormElement("timeLogged", timeLogged);
        tester.checkCheckbox("adjustEstimate", "new");
        if (comment != null) {
            tester.setFormElement("comment", comment);
        }
        tester.submit("Log");
    }

    @Override
    public void reopenIssue(final String issueKey) {
        viewIssue(issueKey);
        tester.clickLink(WORKFLOW_ACTION_REOPEN_ISSUE);
        tester.setWorkingForm("issue-workflow-transition");
        tester.submit("Transition");
    }


    @Override
    public void performIssueActionWithoutDetailsDialog(final String issueKey, final String actionName) {
        viewIssue(issueKey);
        tester.clickLinkWithText(actionName);
    }

    @Override
    public void unwatchIssue(final String issueKey) {
        viewIssue(issueKey);
        if (tester.getDialog().getResponseText().contains("watch-state-on")) {
            tester.clickLink("toggle-watch-issue");
        }
    }

    @Override
    public void watchIssue(final String issueKey) {
        viewIssue(issueKey);
        if (tester.getDialog().getResponseText().contains("watch-state-off")) {
            tester.clickLink("toggle-watch-issue");
        }
    }

    @Override
    public IssueNavigation addWatchers(final String issueKey, final String... usernames) {
        viewIssue(issueKey);
        tester.clickLink("view-watcher-list");
        tester.setWorkingForm("startform");
        tester.setFormElement("userNames", StringUtils.join(usernames, ","));
        tester.submit("add");
        for (final String username : usernames) {
            assertions.assertNodeExists(locators.id("watcher_link_" + username));
        }
        return this;
    }

    @Override
    public void unvoteIssue(final String issueKey) {
        viewIssue(issueKey);
        if (tester.getDialog().getResponseText().contains("vote-state-on")) {
            tester.clickLink("toggle-vote-issue");
        } else {
            throw new RuntimeException("Could not unvote on issue");
        }
    }

    @Override
    public void voteIssue(final String issueKey) {
        viewIssue(issueKey);
        if (tester.getDialog().getResponseText().contains("vote-state-off")) {
            tester.clickLink("toggle-vote-issue");
        } else {
            throw new RuntimeException("Could no vote on issue");
        }
    }

    @Override
    public void addComment(final String issueKey, final String comment) {
        addComment(issueKey, comment, null);
    }

    @Override
    public void addComment(final String issueKey, final String comment, final String roleLevel) {
        viewIssue(issueKey);
        tester.clickLink("footer-comment-button");
        tester.setFormElement("comment", comment);
        if (roleLevel != null) {
            tester.selectOption("commentLevel", roleLevel);
        }
        tester.submit();
    }

    @Override
    public void setFixVersions(final String issueKey, final String... fixVersions) {
        setIssueMultiSelectField(issueKey, "fixVersions", fixVersions);
    }

    @Override
    public void setAffectsVersions(final String issueKey, final String... affectsVersions) {
        setIssueMultiSelectField(issueKey, "versions", affectsVersions);
    }

    @Override
    public void setComponents(final String issueKey, final String... components) {
        setIssueMultiSelectField(issueKey, "components", components);
    }

    @Override
    public void setDueDate(final String issueKey, final String dateString) {
        setSingleIssueField(issueKey, "duedate", dateString);
    }

    public void setIssueMultiSelectField(final String issueKey, final String selectName, final String... options) {
        viewIssue(issueKey);
        tester.clickLink("edit-issue");
        tester.setWorkingForm("issue-edit");
        for (final String option : options) {
            final String value = tester.getDialog().getValueForOption(selectName, option);
            tester.checkCheckbox(selectName, value);
        }
        tester.submit("Update");
    }

    public void setEstimates(final String issueKey, final String originalEstimate, final String remainingEstimate) {
        final String originalEstimateFieldId = getOriginalEstimateFieldId();
        final String remainingEstimateFieldId = getRemainingEstimateFieldId();

        viewIssue(issueKey);
        tester.clickLink("edit-issue");

        tester.assertFormElementPresent(originalEstimateFieldId);
        tester.setFormElement(originalEstimateFieldId, originalEstimate);

        tester.assertFormElementPresent(remainingEstimateFieldId);
        tester.setFormElement(remainingEstimateFieldId, remainingEstimate);

        tester.submit();
    }

    public void setOriginalEstimate(final String issueKey, final String newValue) {
        final String estimateFieldId = getOriginalEstimateFieldId();

        viewIssue(issueKey);
        tester.clickLink("edit-issue");

        tester.assertFormElementPresent(estimateFieldId);
        tester.setFormElement(estimateFieldId, newValue);

        tester.submit();
    }

    public void setRemainingEstimate(final String issueKey, final String newValue) {
        final String remainingEstimateFieldId = getRemainingEstimateFieldId();

        viewIssue(issueKey);
        tester.clickLink("edit-issue");

        tester.assertFormElementPresent(remainingEstimateFieldId);
        tester.setFormElement(remainingEstimateFieldId, newValue);

        tester.submit();
    }

    public IssueNavigation editLabels(final int issueId) {
        tester.clickLink("edit-labels-" + issueId + "-labels");
        return this;
    }

    public IssueNavigation editCustomLabels(final int issueId, final int customFieldId) {
        tester.clickLink("edit-labels-" + issueId + "-customfield_" + customFieldId);
        return this;
    }

    public IssueNavigatorNavigation returnToSearch() {
        tester.gotoPage("/issues/?jql=");
        return issueNavigatorNavigation;
    }

    public String getId(final String issueKey) {
        gotoIssue(issueKey);

        final String text;
        String issueId = "";

        try {
            text = tester.getDialog().getResponse().getText();
            final String paramName = "ViewVoters!default.jspa?id=";
            final int issueIdLocation = text.indexOf(paramName) + paramName.length();
            issueId = text.substring(issueIdLocation, issueIdLocation + 5);
        } catch (final IOException e) {
            logger.log("Unable to retrieve issue id" + e.getMessage());
            Assert.fail(String.format("No issue id could be found using issue key:'%s'. IOException caught: '%s'",
                    issueKey, e.getMessage()));
        }

        return issueId;
    }

    private String getOriginalEstimateFieldId() {
        if (timeTracking.isIn(TimeTracking.Mode.LEGACY)) {
            return "timetracking";
        } else {
            return "timetracking_originalestimate";
        }
    }

    private String getRemainingEstimateFieldId() {
        if (timeTracking.isIn(TimeTracking.Mode.LEGACY)) {
            return "timetracking";
        } else {
            return "timetracking_remainingestimate";
        }
    }
}
