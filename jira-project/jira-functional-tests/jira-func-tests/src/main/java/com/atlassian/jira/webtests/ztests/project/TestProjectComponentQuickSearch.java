package com.atlassian.jira.webtests.ztests.project;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.parser.SystemInfoParser;
import com.atlassian.jira.functest.framework.rule.IssueTypeUrls;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.table.AndCell;
import com.atlassian.jira.webtests.table.ImageCell;
import com.atlassian.jira.webtests.table.LinkCell;
import com.atlassian.jira.webtests.table.TextCell;
import com.meterware.httpunit.WebTable;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PRIORITY_IMAGE_MAJOR;
import static com.atlassian.jira.functest.framework.FunctTestConstants.STATUS_OPEN;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static org.junit.Assert.assertEquals;

@WebTest({Category.FUNC_TEST, Category.COMPONENTS_AND_VERSIONS, Category.PROJECTS})
@LoginAs(user = ADMIN_USERNAME)
public class TestProjectComponentQuickSearch extends BaseJiraFuncTest {

    @Rule
    public IssueTypeUrls issueTypeUrls = new IssueTypeUrls();
    String appServer;
    private String bugIcon;
    private String taskIcon;
    private String improvementIcon;
    private String newFeatureIcon;
    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private SystemInfoParser systemInfoParser;

    @Before
    public void setUp() {
        administration.restoreData("TestProjectComponentQuickSearch.xml");
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
        // what app server are we running on
        appServer = systemInfoParser.getSystemInfo().getAppServer();

        bugIcon = issueTypeUrls.getIssueTypeUrl("bug");
        taskIcon = issueTypeUrls.getIssueTypeUrl("task");
        improvementIcon = issueTypeUrls.getIssueTypeUrl("improvement");
        newFeatureIcon = issueTypeUrls.getIssueTypeUrl("new feature");
    }

    @Test
    public void testProjectComponentQuickSearchMultipleProjects() throws Exception {
        //see all issues
        WebTable issueTable = assertComponentQuickSearch("", 5);
        assertIssueTableHasHSP_12345(issueTable);
        assertIssueTableHasHSP_2345(issueTable);
        assertIssueTableHasMKY_35(issueTable);
        assertIssueTableHasMKY_45(issueTable);
        assertIssueTableHasHSP_45(issueTable);

        //project component quicksearch across multiple projects
        issueTable = assertComponentQuickSearchOnAdvanced("c:one", 1);
        assertIssueTableHasHSP_12345(issueTable);

        issueTable = assertComponentQuickSearchOnAdvanced(" c:two ", 2);
        assertIssueTableHasHSP_12345(issueTable);
        assertIssueTableHasHSP_2345(issueTable);

        issueTable = assertComponentQuickSearchOnAdvanced("three c:three", 3);
        assertIssueTableHasHSP_12345(issueTable);
        assertIssueTableHasHSP_2345(issueTable);
        assertIssueTableHasMKY_35(issueTable);

        issueTable = assertComponentQuickSearchOnAdvanced("c:four four", 4);
        assertIssueTableHasHSP_12345(issueTable);
        assertIssueTableHasHSP_2345(issueTable);
        assertIssueTableHasMKY_45(issueTable);
        assertIssueTableHasHSP_45(issueTable);

        issueTable = assertComponentQuickSearchOnAdvanced("five c:five five", 5);
        assertIssueTableHasHSP_12345(issueTable);
        assertIssueTableHasHSP_2345(issueTable);
        assertIssueTableHasMKY_35(issueTable);
        assertIssueTableHasMKY_45(issueTable);
        assertIssueTableHasHSP_45(issueTable);
    }

    @Test
    public void testProjectComponentQuickSearchHomosapienProject() throws Exception {
        //project component quick search for HSP
        WebTable issueTable = assertComponentQuickSearch("hsp c:one", 1);
        assertIssueTableHasHSP_12345(issueTable);

        issueTable = assertComponentQuickSearch(" c:two homosapien ", 2);
        assertIssueTableHasHSP_12345(issueTable);
        assertIssueTableHasHSP_2345(issueTable);

        issueTable = assertComponentQuickSearch("three HSP c:three", 2);
        assertIssueTableHasHSP_12345(issueTable);
        assertIssueTableHasHSP_2345(issueTable);

        issueTable = assertComponentQuickSearch("c:four four HOMOSAPIEN", 3);
        assertIssueTableHasHSP_12345(issueTable);
        assertIssueTableHasHSP_2345(issueTable);
        assertIssueTableHasHSP_45(issueTable);

        issueTable = assertComponentQuickSearch("hsp five c:five five", 3);
        assertIssueTableHasHSP_12345(issueTable);
        assertIssueTableHasHSP_2345(issueTable);
        assertIssueTableHasHSP_45(issueTable);
    }

    @Test
    public void testProjectComponentQuickSearchMonkey() throws Exception {
        //project component quick search for MKY
        WebTable issueTable = assertComponentQuickSearch("c:one MONKEY", 2);
        assertIssueTableHasMKY_35(issueTable);
        assertIssueTableHasMKY_45(issueTable);

        issueTable = assertComponentQuickSearch(" MKY c:two ", 2);
        assertIssueTableHasMKY_35(issueTable);
        assertIssueTableHasMKY_45(issueTable);

        issueTable = assertComponentQuickSearch("monkey three c:three", 1);
        assertIssueTableHasMKY_35(issueTable);

        issueTable = assertComponentQuickSearch("c:four mky four", 1);
        assertIssueTableHasMKY_45(issueTable);

        issueTable = assertComponentQuickSearch("five c:five five monkey", 2);
        assertIssueTableHasMKY_35(issueTable);
        assertIssueTableHasMKY_45(issueTable);
    }

    private WebTable assertComponentQuickSearch(final String searchInput, final int numOfResults) throws SAXException {
        runQuickSearch(searchInput);
        assertions.getIssueNavigatorAssertions().assertIssueNavigatorDisplaying(locator.page(), "1", String.valueOf(numOfResults), String.valueOf(numOfResults));
        final WebTable issueTable = tester.getDialog().getResponse().getTableWithID("issuetable");
        assertEquals(numOfResults + 1, issueTable.getRowCount());
        assertions.getTableAssertions().assertTableRowEquals(issueTable, 0, new Object[]{"T", "Key", "Summary", "Assignee", "Reporter", "P", "Status", "Resolution", "Created", "Updated", "Due"});
        return issueTable;
    }

    private WebTable assertComponentQuickSearchOnAdvanced(final String searchInput, final int numOfResults) throws SAXException {
        runQuickSearch(searchInput);
        assertAdvacnedIssueNavigatorDisplaying("1", String.valueOf(numOfResults), String.valueOf(numOfResults));
        final WebTable issueTable = tester.getDialog().getResponse().getTableWithID("issuetable");
        assertEquals(numOfResults + 1, issueTable.getRowCount());
        assertions.getTableAssertions().assertTableRowEquals(issueTable, 0, new Object[]{"T", "Key", "Summary", "Assignee", "Reporter", "P", "Status", "Resolution", "Created", "Updated", "Due"});
        return issueTable;
    }

    private void assertAdvacnedIssueNavigatorDisplaying(final String from, final String to, final String of) {
        tester.assertTextPresent("<span class=\"results-count-start\">" + from + "</span>&ndash;<span class=\"results-count-end\">" + to + "</span> of <span class=\"results-count-total results-count-link\">" + of + "</span>");
    }

    private void assertIssueTableHasHSP_45(final WebTable issueTable) {
        assertions.getTableAssertions().assertTableContainsRowOnce(issueTable, new Object[]{new AndCell(new LinkCell("/browse/HSP-1", ""), new ImageCell(bugIcon)), new LinkCell("/browse/HSP-1", "HSP-1"), "homo four five", ADMIN_FULLNAME, ADMIN_FULLNAME, new ImageCell(PRIORITY_IMAGE_MAJOR), new TextCell(STATUS_OPEN), "Unresolved", "23/Nov/07", "23/Nov/07", ""});
    }

    private void assertIssueTableHasHSP_12345(final WebTable issueTable) {
        assertions.getTableAssertions().assertTableContainsRowOnce(issueTable, new Object[]{new AndCell(new LinkCell("/browse/HSP-2", ""), new ImageCell(taskIcon)), new LinkCell("/browse/HSP-2", "HSP-2"), "one two homo three four five", ADMIN_FULLNAME, ADMIN_FULLNAME, new ImageCell(PRIORITY_IMAGE_MAJOR), new TextCell(STATUS_OPEN), "Unresolved", "23/Nov/07", "23/Nov/07", ""});
    }

    private void assertIssueTableHasHSP_2345(final WebTable issueTable) {
        assertions.getTableAssertions().assertTableContainsRowOnce(issueTable, new Object[]{new AndCell(new LinkCell("/browse/HSP-3", ""), new ImageCell(improvementIcon)), new LinkCell("/browse/HSP-3", "HSP-3"), "two three homo four five", ADMIN_FULLNAME, ADMIN_FULLNAME, new ImageCell(PRIORITY_IMAGE_MAJOR), new TextCell(STATUS_OPEN), "Unresolved", "23/Nov/07", "23/Nov/07", ""});
    }

    private void assertIssueTableHasMKY_35(final WebTable issueTable) {
        assertions.getTableAssertions().assertTableContainsRowOnce(issueTable, new Object[]{new AndCell(new LinkCell("/browse/MKY-2", ""), new ImageCell(newFeatureIcon)), new LinkCell("/browse/MKY-2", "MKY-2"), "three monk five", ADMIN_FULLNAME, ADMIN_FULLNAME, new ImageCell(PRIORITY_IMAGE_MAJOR), new TextCell(STATUS_OPEN), "Unresolved", "23/Nov/07", "23/Nov/07", ""});
    }

    private void assertIssueTableHasMKY_45(final WebTable issueTable) {
        assertions.getTableAssertions().assertTableContainsRowOnce(issueTable, new Object[]{new AndCell(new LinkCell("/browse/MKY-1", ""), new ImageCell(bugIcon)), new LinkCell("/browse/MKY-1", "MKY-1"), "monk five four", ADMIN_FULLNAME, ADMIN_FULLNAME, new ImageCell(PRIORITY_IMAGE_MAJOR), new TextCell(STATUS_OPEN), "Unresolved", "23/Nov/07", "23/Nov/07", ""});
    }

    /**
     * Executes quicksearch with no search string to return all issues
     */
    private void runQuickSearch(final String searchInput) {
        navigation.gotoDashboard();
        tester.setWorkingForm("quicksearch");
        tester.setFormElement("searchString", searchInput);
        tester.submit();
    }
}
