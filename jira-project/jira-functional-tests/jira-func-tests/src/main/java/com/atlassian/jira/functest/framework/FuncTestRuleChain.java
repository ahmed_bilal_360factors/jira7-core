package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.rule.BeforeBuildRule;
import com.atlassian.jira.functest.rule.SinceBuildRule;
import com.atlassian.jira.functest.rule.CheckCachesRule;
import com.atlassian.jira.functest.rule.DisableOnboardingRule;
import com.atlassian.jira.functest.rule.EnsureJiraSetupRule;
import com.atlassian.jira.functest.rule.HttpUnitConfigurationRule;
import com.atlassian.jira.functest.rule.InjectFieldsRule;
import com.atlassian.jira.functest.rule.LogTimeRule;
import com.atlassian.jira.functest.rule.LoginAsRule;
import com.atlassian.jira.functest.rule.OutgoingMailTestRule;
import com.atlassian.jira.functest.rule.RestoreDataRule;
import com.atlassian.jira.functest.rule.WebTesterRule;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.testkit.client.log.FuncTestLoggerImpl;
import com.atlassian.jira.testkit.client.xmlbackup.XmlBackupCopier;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.rules.RuleChain;

import java.util.function.Supplier;

public class FuncTestRuleChain {
    public static RuleChain forTest(final Object test) {
        final FuncTestWebClientListener webClientListener = new FuncTestWebClientListener();
        final BackdoorModule backdoorModule = new BackdoorModule();
        final Supplier<FuncTestWebClientListener> clientListenerSupplier = () -> webClientListener;
        final Supplier<JIRAEnvironmentData> environmentDataSupplier = backdoorModule::getEnvironmentData;
        final WebTesterRule webTesterRule = new WebTesterRule(environmentDataSupplier, clientListenerSupplier);

        final Injector injector = Guice.createInjector(
                webTesterRule,
                backdoorModule,
                binder -> binder.bind(FuncTestLogger.class).toProvider(() -> new FuncTestLoggerImpl(3)),
                binder -> binder.bind(XmlBackupCopier.class).toProvider(() -> new XmlBackupCopier(backdoorModule.getEnvironmentData().getBaseUrl()))
        );
        final Supplier<Backdoor> testkitSupplier = injector.getProvider(Backdoor.class)::get;
        final Supplier<Navigation> navigationSupplier = injector.getProvider(Navigation.class)::get;

        return RuleChain
                .emptyRuleChain()
                .around(new SinceBuildRule(backdoorModule))
                .around(new BeforeBuildRule(backdoorModule))
                .around(new LogTimeRule(environmentDataSupplier, clientListenerSupplier, testkitSupplier))
                .around(new EnsureJiraSetupRule(backdoorModule, environmentDataSupplier, () -> {
                    //we need to initialize jwebunit just to click trough setup process
                    HttpUnitConfigurationRule.restoreDefaults();
                    return new WebTesterRule(environmentDataSupplier, clientListenerSupplier).get();
                }))
                .around(new RestoreDataRule(backdoorModule))
                .around(new DisableOnboardingRule(backdoorModule))
                .around(new HttpUnitConfigurationRule())
                .around(webTesterRule)
                .around(new OutgoingMailTestRule(backdoorModule))
                .around(new LoginAsRule(navigationSupplier))
                .around(new CheckCachesRule(backdoorModule))
                .around(new InjectFieldsRule(injector, test));
    }
}
