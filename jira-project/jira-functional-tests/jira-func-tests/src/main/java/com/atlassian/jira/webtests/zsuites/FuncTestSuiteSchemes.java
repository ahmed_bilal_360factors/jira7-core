package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.admin.TestOldPermissionSchemes;
import com.atlassian.jira.webtests.ztests.admin.issuetypes.TestIssueTypeSchemeMigration;
import com.atlassian.jira.webtests.ztests.admin.issuetypes.TestIssueTypeSchemes;
import com.atlassian.jira.webtests.ztests.fields.TestFieldConfigurationSchemes;
import com.atlassian.jira.webtests.ztests.workflow.TestWorkFlowSchemes;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of tests around Schemes
 *
 * @since v4.0
 */
public class FuncTestSuiteSchemes {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestOldPermissionSchemes.class)
                .add(TestFieldConfigurationSchemes.class)
                .add(TestWorkFlowSchemes.class)
                .add(TestIssueTypeSchemes.class)
                .add(TestIssueTypeSchemeMigration.class)
                .build();
    }
}