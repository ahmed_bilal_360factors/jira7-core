package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.testkit.client.restclient.PageBean;
import com.atlassian.jira.testkit.client.restclient.UserJson;

import java.util.List;

/**
 * @since v7.0
 */
public class UsersPageBean extends PageBean<UserJson> {
    public UsersPageBean() {
    }

    public UsersPageBean(List<UserJson> values, long total, int maxResults, long startAt) {
        super(values, total, maxResults, startAt);
    }
}
