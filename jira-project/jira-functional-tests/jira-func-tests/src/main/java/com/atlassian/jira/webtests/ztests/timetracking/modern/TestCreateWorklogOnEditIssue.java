package com.atlassian.jira.webtests.ztests.timetracking.modern;

import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Uses the tests defined in {@link AbstractTestCreateWorklogAsField} to test the "log work" system field on the Edit
 * Issue screen.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.TIME_TRACKING, Category.WORKLOGS})
@LoginAs(user = ADMIN_USERNAME)
public class TestCreateWorklogOnEditIssue extends AbstractTestCreateWorklogAsField {
    private LogWorkRunner logWorkRunner;

    protected LogWorkRunner getLogWorkRunner() {
        if (logWorkRunner == null) {
            logWorkRunner = new EditIssueLogWorkRunner();
        }
        return logWorkRunner;
    }

    class EditIssueLogWorkRunner implements LogWorkRunner {
        public void gotoLogWorkScreen1() {
            navigation.issue().viewIssue(HSP_1);
            tester.clickLink("edit-issue");
        }

        public boolean isCommentFieldShown() {
            return true;
        }

        public void gotoLogWorkScreen2() {
            gotoLogWorkScreen1();
        }

        public void gotoLogWorkScreenWithOriginalEstimate(final String originalEstimate) {
            // preset the original estimate
            navigation.issue().viewIssue(HSP_1);
            tester.clickLink("edit-issue");
            tester.setFormElement("timetracking", originalEstimate);
            tester.submit("Update");

            // now go to log work screen
            gotoLogWorkScreen1();
        }

        public void gotoLogWorkResult() {
            navigation.issue().viewIssue(HSP_1);
        }


        public boolean isLoggingWorkTwiceSupported() {
            return true;
        }
    }

    @Test
    public void testWorklogNoPermToCreate() {
        super.testWorklogNoPermToCreate();
    }

    @Test
    public void testWorklogTimeTrackingDisabled() {
        super.testWorklogTimeTrackingDisabled();
    }

    @Test
    public void testLogWorkLeaveEstimateNoteCorrect() {
        super.testLogWorkLeaveEstimateNoteCorrect();
    }

    @Test
    public void testMandatoryFields() {
        super.testMandatoryFields();
    }

    @Test
    public void testInvalidFormattedDurationFields() {
        super.testInvalidFormattedDurationFields();
    }

    @Test
    public void testBadFractionDuration() {
        super.testBadFractionDuration();
    }

    @Test
    public void testGoodFractionDuration() throws Exception {
        super.testGoodFractionDuration();
    }

    @Test
    public void testInvalidTimeSpentZero() {
        super.testInvalidTimeSpentZero();
    }

    @Test
    public void testInvalidStartDateField() {
        super.testInvalidStartDateField();
    }

    @Test
    public void testAutoAdjustEstimate() {
        super.testAutoAdjustEstimate();
    }

    @Test
    public void testNewEstimate() {
        super.testNewEstimate();
    }

    @Test
    public void testLeaveExistingEstimate() {
        super.testLeaveExistingEstimate();
    }

    @Test
    public void testManuallyAdjustEstimate() {
        super.testManuallyAdjustEstimate();
    }

    @Test
    public void testLogWorkVisibleToAll() {
        super.testLogWorkVisibleToAll();
    }

    @Test
    public void testLogWorkVisibleToAllDaysTimeFormat() {
        super.testLogWorkVisibleToAllDaysTimeFormat();
    }

    @Test
    public void testLogWorkVisibleToAllHoursTimeFormat() {
        super.testLogWorkVisibleToAllHoursTimeFormat();
    }

    @Test
    public void testLogWorkVisibleToRole() {
        super.testLogWorkVisibleToRole();
    }

    @Test
    public void testLogWorkVisibleToRoleDaysTimeFormat() {
        super.testLogWorkVisibleToRoleDaysTimeFormat();
    }

    @Test
    public void testLogWorkVisibleToRoleHoursTimeFormat() {
        super.testLogWorkVisibleToRoleHoursTimeFormat();
    }

    @Test
    public void testLogWorkVisibleToGroup() {
        super.testLogWorkVisibleToGroup();
    }

    @Test
    public void testLogWorkVisibleToGroupDaysTimeFormat() {
        super.testLogWorkVisibleToGroupDaysTimeFormat();
    }

    @Test
    public void testLogWorkVisibleToGroupHoursTimeFormat() {
        super.testLogWorkVisibleToGroupHoursTimeFormat();
    }

    @Test
    public void testLogWorkCommentsNotCopiedWhenCopyingDisabled() {
        super.testLogWorkCommentsNotCopiedWhenCopyingDisabled();
    }

    @Test
    public void testLogWorkCommentsCopiedWhenCopyingEnabled() {
        super.testLogWorkCommentsCopiedWhenCopyingEnabled();
    }

    @Test
    public void testLogWorkDateIsStartDate() {
        super.testLogWorkDateIsStartDate();
    }

    @Test
    public void testChangeHistory() throws Exception {
        super.testChangeHistory();
    }
}
