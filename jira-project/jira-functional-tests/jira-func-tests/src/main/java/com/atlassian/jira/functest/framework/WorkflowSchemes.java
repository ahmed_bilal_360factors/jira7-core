package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.testkit.beans.WorkflowSchemeData;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Mimics a user dealing with workflow schemes.
 */
public class WorkflowSchemes {

    private final WebTester tester;
    private final Assertions assertions;
    /**
     * This should not be needed, because users don't use backdoor.
     */
    private final Backdoor backdoor;
    private final ProgressBar progressBar;
    private final Navigation navigation;

    @Inject
    public WorkflowSchemes(
            final WebTester tester,
            final Assertions assertions,
            final Backdoor backdoor,
            final ProgressBar progressBar,
            final Navigation navigation
    ) {
        this.tester = tester;
        this.assertions = assertions;
        this.backdoor = backdoor;
        this.progressBar = progressBar;
        this.navigation = navigation;
    }

    public void addWorkFlowScheme(final String name, final String description) {
        navigation.gotoAdminSection(Navigation.AdminSection.WORKFLOW_SCHEMES);
        tester.clickLink("add_workflowscheme");
        tester.setFormElement("name", name);
        tester.setFormElement("description", description);
        tester.submit("Add");
    }

    public void deleteWorkFlowScheme(final String workflowscheme_name) {
        navigation.gotoAdminSection(Navigation.AdminSection.WORKFLOW_SCHEMES);
        final String linkId = "del_" + workflowscheme_name;
        if (tester.getDialog().isLinkPresent(linkId)) {
            tester.clickLink(linkId);
            tester.submit("Delete");
        }
    }

    /**
     * Waits until a workflow scheme migration completes now that it runs asynchronously.
     *
     * @param projectName        the name of the project to associate the worflow scheme to
     * @param targetWorkflowName the name of the workflow scheme to associate the project to
     */
    public void waitForSuccessfulWorkflowSchemeMigration(String projectName, String targetWorkflowName) {
        final int MAX_ITERATIONS = 100;
        int its = 0;
        while (true) {
            its++;
            if (its > MAX_ITERATIONS) {
                fail("The Workflow Migration took longer than " + MAX_ITERATIONS + " attempts!  Why?");
            }
            // are we on the "still working" page or the "done" page
            // if its neither then fail
            if (tester.getDialog().getResponseText().contains("type=\"submit\" name=\"Refresh\"")) {
                // we are on the "still working page"
                // click on the Refresh Button
                tester.submit("Refresh");
            } else if (tester.getDialog().getResponseText().contains("type=\"submit\" name=\"Done\"")) { // its on the done page
                // we are on the "done" page" but we dont own the task
                progressBar.validateProgressBarUI("Done");
                tester.submit("Done");
                // now check we are on the project page
                assertions.getSidebarAssertions().assertProjectName(projectName);
                final WorkflowSchemeData scheme = backdoor.workflowSchemes().getWorkflowSchemeByProjectName(projectName);
                assertEquals(scheme.getName(), targetWorkflowName);
                return;
            } else if (tester.getDialog().getResponseText().contains("input type=\"submit\" name=\"Acknowledge\"")) {
                progressBar.validateProgressBarUI("Acknowledge");
                // we are on the "Acknowledge" page"
                tester.submit("Acknowledge");

                // now check we are on the project page
                assertions.getSidebarAssertions().assertProjectName(projectName);
                final WorkflowSchemeData scheme = backdoor.workflowSchemes().getWorkflowSchemeByProjectName(projectName);
                assertEquals(scheme.getName(), targetWorkflowName);

                return;
            } else if (tester.getDialog().getElement("project-config-header-name") != null) {
                // its on the project page for this project (straight thru association)
                return;
            } else {
                // we are on a page we dont expect
                fail("Page encountered during migration that was not expected : PROJECT:" + projectName + " - WORKFLOW SCHEME NAME" + targetWorkflowName);
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                fail("Test interupted");
            }
        }
    }
}
