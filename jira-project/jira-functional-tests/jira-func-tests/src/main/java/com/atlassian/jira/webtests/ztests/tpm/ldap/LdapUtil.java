package com.atlassian.jira.webtests.ztests.tpm.ldap;


import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.locator.TableLocator;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.meterware.httpunit.TableCell;
import com.meterware.httpunit.WebLink;
import com.meterware.httpunit.WebTable;
import net.sourceforge.jwebunit.WebTester;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * @since v7.1
 */
public class LdapUtil {
    private final WebTester tester;
    private final JIRAEnvironmentData environmentData;
    private final Navigation navigation;
    private final FuncTestLogger logger;
    private final Assertions assertions;
    private final TextAssertions text;

    @Inject
    public LdapUtil(final WebTester tester,
                    final JIRAEnvironmentData environmentData,
                    final Navigation navigation,
                    final FuncTestLogger logger,
                    final Assertions assertions,
                    final TextAssertions text) {
        this.tester = tester;
        this.environmentData = environmentData;
        this.navigation = navigation;
        this.logger = logger;
        this.assertions = assertions;
        this.text = text;
    }

    /*
     * You can easily run it from IDE with -Dldap.type=ActiveDirectory -Dldap.server=crowd-ad1.sydney.atlassian.com -Dldap.nossl=true
     * You can also run it against arbitrary LDAP e.g. the one in Gdansk office:
     * -Dldap.type=OpenLdap -Dldap.server=rabbit -Dldap.userdn="cn=admin,dc=atlassian,dc=pl" -Dldap.password=admin123 -Dldap.basedn="dc=atlassian,dc=pl"
     *
     * Notice: Active Directory will not allow for modification via unencrypted connection. You can read more about
     * configuring SSL with Active Directory here: https://extranet.atlassian.com/x/6QDDHQ
     */
    public void createLdapDirectory() throws InterruptedException {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_DIRECTORIES);
        tester.assertTextPresent("User Directories");

        if (!tester.getDialog().isTextInResponse("LDAP Directory")) {
            // We run these tests against Active Directory and OpenLDAP, but we need different connection settings for each.
            if (isActiveDirectory()) {
                logger.log("Attempting to add an Active Directory LDAP User Directory...");
                addActiveDirectory();
            } else {
                logger.log("Attempting to add an Open LDAP User Directory...");
                addOpenLdap();
            }
            // Currently the Internal Directory is first in the list:
            WebTable table = assertions.getTableAssertions().getWebTable("directory-list");

            assertions.getTableAssertions().assertTableCellHasText(table, 1, 1, "JIRA Internal Directory");
            assertions.getTableAssertions().assertTableCellHasText(table, 2, 1, "LDAP Directory");
            // Move LDAP to top
            final WebTable tblDirectoryList = new TableLocator(tester, "directory-list").getTable();
            final WebLink link = tblDirectoryList.getTableCell(2, 3).getLinkWith("up");

            navigation.clickLink(link);
            table = assertions.getTableAssertions().getWebTable("directory-list");
            assertions.getTableAssertions().assertTableCellHasText(table, 1, 1, "LDAP Directory");
            assertions.getTableAssertions().assertTableCellHasText(table, 2, 1, "JIRA Internal Directory");

            synchroniseDirectory(1);
        }
    }

    private void addActiveDirectory() {
        navigation.gotoPage("/plugins/servlet/embedded-crowd/configure/ldap/");
        text.assertTextPresent(new IdLocator(tester, "embcwd"), "Server Settings");

        tester.setWorkingForm("configure-ldap-form");
        // Set the required Simple fields
        tester.setFormElement("name", "LDAP Directory");
        tester.selectOption("type", "Microsoft Active Directory");
        tester.setFormElement("hostname", getLdapServer());
        // AD will not allow mutating operations unless you use SSL
        if (!Boolean.valueOf(StringUtils.defaultString(getConfigurationOption("ldap.nossl"), "false"))) {
            tester.setFormElement("port", "636");
            tester.checkCheckbox("useSSL", "true");
        }
        tester.setFormElement("ldapUserdn", getUserDn());
        tester.setFormElement("ldapPassword", getPassword());
        tester.setFormElement("ldapBasedn", getBaseDn());

        // Set the advanced fields manually - Func tests don't have javascript to do this for us
        tester.setFormElement("ldapUserObjectclass", "user");
        tester.setFormElement("ldapUserFilter", "(&(objectCategory=Person)(sAMAccountName=*))");
        tester.setFormElement("ldapUserUsername", "sAMAccountName");
        tester.setFormElement("ldapUserUsernameRdn", "cn");
        tester.setFormElement("ldapUserFirstname", "givenName");
        tester.setFormElement("ldapUserLastname", "sn");
        tester.setFormElement("ldapUserDisplayname", "displayName");
        tester.setFormElement("ldapUserEmail", "mail");
        tester.setFormElement("ldapUserGroup", "memberOf");
        tester.setFormElement("ldapUserPassword", "unicodePwd");
        tester.setFormElement("ldapGroupObjectclass", "group");
        tester.setFormElement("ldapGroupFilter", "(objectCategory=Group)");
        tester.setFormElement("ldapGroupName", "cn");
        tester.setFormElement("ldapGroupDescription", "description");
        tester.setFormElement("ldapGroupUsernames", "member");
        tester.setFormElement("ldapPermissionOption", "READ_WRITE");
        tester.setFormElement("ldapExternalId", "objectGUID");

        // Add the new Directory
        tester.submit("test");
        text.assertTextPresent("Connection test successful");

        tester.submit("save");
        // Now we are forced to the "Extended test" page
        assertExtendedTestPageAndReturnToDirectoryList();

        text.assertTextPresent("The table below shows the user directories currently configured for JIRA.");
        tester.assertTextPresent("JIRA Internal Directory");
        tester.assertTextPresent("LDAP Directory");
    }

    private void addOpenLdap() {
        navigation.gotoPage("/plugins/servlet/embedded-crowd/configure/ldap/");
        text.assertTextPresent(new IdLocator(tester, "embcwd"), "Server Settings");

        tester.setWorkingForm("configure-ldap-form");
        // Set the required Simple fields
        tester.setFormElement("name", "LDAP Directory");
        tester.selectOption("type", "OpenLDAP");
        // Allow flexibility of setting up localtest.properties to run test locally against crowd-op23
        tester.setFormElement("hostname", getLdapServer());
        tester.setFormElement("port", "389");
        tester.setFormElement("ldapUserdn", getUserDn());
        tester.setFormElement("ldapBasedn", getBaseDn());
        tester.setFormElement("ldapPassword", getPassword());
        // Set the advanced fields manually - Func tests don't have javascript to do this for us
        tester.setFormElement("ldapUserObjectclass", "inetorgperson");
        tester.setFormElement("ldapUserFilter", "(objectclass=inetorgperson)");
        tester.setFormElement("ldapUserUsername", "cn");
        tester.setFormElement("ldapUserUsernameRdn", "cn");
        tester.setFormElement("ldapUserFirstname", "givenName");
        tester.setFormElement("ldapUserLastname", "sn");
        tester.setFormElement("ldapUserDisplayname", "displayName");
        tester.setFormElement("ldapUserEmail", "mail");
        tester.setFormElement("ldapUserGroup", "memberOf");
        tester.setFormElement("ldapUserPassword", "userPassword");
        tester.setFormElement("ldapGroupObjectclass", "groupOfUniqueNames");
        tester.setFormElement("ldapGroupFilter", "(objectclass=groupOfUniqueNames)");
        tester.setFormElement("ldapGroupName", "cn");
        tester.setFormElement("ldapGroupDescription", "description");
        tester.setFormElement("ldapGroupUsernames", "uniqueMember");
        tester.setFormElement("ldapPermissionOption", "READ_WRITE");
        tester.setFormElement("ldapExternalId", "entryUUID");

        // Add the new Directory
        tester.submit("test");
        text.assertTextPresent("Connection test successful");

        tester.submit("save");
        // Now we are forced to the "Extended test" page
        assertExtendedTestPageAndReturnToDirectoryList();

        text.assertTextPresent("The table below shows the user directories currently configured for JIRA.");
        tester.assertTextPresent("JIRA Internal Directory");
        tester.assertTextPresent("LDAP Directory");
    }

    protected boolean isActiveDirectory() {
        final String ldapType = getConfigurationOption("ldap.type");
        if (ldapType == null) {
            throw new IllegalStateException("Missing configuration for 'ldap.type'.");
        }
        if (ldapType.equals("ActiveDirectory")) {
            return true;
        }
        if (ldapType.equals("OpenLdap")) {
            return false;
        }
        throw new IllegalStateException("Unknown LDAP type '" + ldapType + "'");
    }

    /**
     * Will return "OpenLDAP" or "Microsoft Active Directory" depending on the LDAP server under test.
     *
     * @return "OpenLDAP" or "Microsoft Active Directory" depending on the LDAP server under test.
     */
    protected String getTypeDisplayName() {
        final String ldapType = getConfigurationOption("ldap.type");
        if (ldapType == null) {
            throw new IllegalStateException("Missing configuration for 'ldap.type'.");
        }
        if (ldapType.equals("ActiveDirectory")) {
            return "Microsoft Active Directory";
        }
        if (ldapType.equals("OpenLdap")) {
            return "OpenLDAP";
        }
        throw new IllegalStateException("Unknown LDAP type '" + ldapType + "'");
    }

    protected String getUserDn() {
        final String userDn = getConfigurationOption("ldap.userdn");
        if (userDn == null) {
            if (isActiveDirectory()) {
                if (getLdapServer().equals("crowd-ad1.sydney.atlassian.com")) {
                    // Running the test locally against crowd-ad1:
                    return "cn=Administrator,cn=Users,dc=sydney,dc=atlassian,dc=com";
                } else {
                    // Running a proper Lab manager test:
                    return "cn=Administrator,cn=Users,dc=tpm,dc=atlassian,dc=com";
                }
            } else {
                // Open LDAP
                if (getLdapServer().equals("crowd-op23") || getLdapServer().equals("172.22.200.133")) {
                    // Running the test locally against crowd-op23:
                    return "o=sgi,c=us";
                } else {
                    return "cn=admin,dc=example,dc=com";
                }
            }
        } else {
            return userDn;
        }
    }

    protected String getPassword() {
        final String password = getConfigurationOption("ldap.password");
        if (password == null) {
            if (isActiveDirectory()) {
                if (getLdapServer().equals("crowd-ad1.sydney.atlassian.com")) {
                    // Running the test locally against crowd-ad1:
                    return "atlassian";
                } else {
                    // Running a proper Lab manager test:
                    return "5P3rtaaah";
                }
            } else {
                // Open LDAP
                return "secret";
            }
        } else {
            return password;
        }
    }

    protected String getBaseDn() {
        final String baseDn = getConfigurationOption("ldap.basedn");
        if (baseDn == null) {
            if (isActiveDirectory()) {
                if (getLdapServer().equals("crowd-ad1.sydney.atlassian.com")) {
                    // Running the test locally against crowd-ad1:
                    return "OU=People,dc=sydney,dc=atlassian,dc=com";
                } else {
                    // Running a proper Lab manager test:
                    return "dc=tpm,dc=atlassian,dc=com";
                }
            } else {
                // Open LDAP
                if (getLdapServer().equals("crowd-op23") || getLdapServer().equals("172.22.200.133")) {
                    // Running the test locally against crowd-op23:
                    return "ou=JIRA-TPM,o=sgi,c=us";
                } else {
                    return "dc=example,dc=com";
                }
            }
        } else {
            return baseDn;
        }
    }

    protected String getConfigurationOption(final String key) {
        final String value = System.getProperty(key);
        if (value != null) {
            return value;
        }
        return environmentData.getProperty(key);
    }

    protected String getLdapServer() {
        // this allows us to connect to a remote ldap server for running inside IDE (set [ldap.server = crowd-op23] in localtest.properties)
        final String server = getConfigurationOption("ldap.server");
        if (server == null) {
            if (isActiveDirectory()) {
                // Use the actual hostname because of checks against the SSL certificate
                return "ec2-windows.tpm.atlassian.com";
            } else {
                return "localhost";
            }
        } else {
            return server;
        }
    }

    protected void assertExtendedTestPageAndReturnToDirectoryList() {
        // Assert we are on the "Extended test" page
        tester.assertTextPresent("Test Remote Directory Connection");
        tester.assertTextPresent("For extended testing enter the credentials of a user in the remote directory");
        // click the link to go back to directory list
        tester.clickLinkWithText("Back to directory list");
    }

    protected void synchroniseDirectory(final int row) throws InterruptedException {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_DIRECTORIES);

        final WebTable tblDirectoryList = new TableLocator(tester, "directory-list").getTable();
        logger.log("Attempting to synchronise Directory " + row);
        TableCell operations = tblDirectoryList.getTableCell(row, 4);
        if (operations.asText().contains("Synchronising")) {
            // Looks like an automatic synch was kicked off in the background - just wait for it to complete.
            logger.log("Directory " + row + " is already synchronising ... we will wait until it is complete.");
        } else {
            navigation.clickLink(operations.getLinkWith("Synchronise"));
        }
        // Synchronise is asynchronous, so we need to check until it is complete
        int attempts = 0;
        while (true) {
            Thread.sleep(100);
            // refresh the page to get latest synchronised status
            navigation.gotoAdminSection(Navigation.AdminSection.USER_DIRECTORIES);
            operations = new UserDirectoryTable(tester, logger, assertions).getTableCell(row, 4);
            if (operations.asText().contains("Synchronising")) {
                // synch in progress
                attempts++;
                if (attempts > 100) {
                    // Just in case...
                    fail("Directory did not finish synchronising. Giving up after " + attempts + " retries.");
                }
                logger.log("Still synchronising ...");
            } else {
                // assert that the Synchronise link is now present
                assertNotNull(operations.getLinkWith("Synchronise"));
                logger.log("Synchronise is finished.");
                break;
            }
        }
    }
}
