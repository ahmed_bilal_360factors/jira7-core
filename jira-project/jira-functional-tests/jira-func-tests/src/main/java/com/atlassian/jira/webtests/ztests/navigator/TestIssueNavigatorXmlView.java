package com.atlassian.jira.webtests.ztests.navigator;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.framework.util.text.TextKit;
import com.atlassian.jira.webtests.AbstractTestIssueNavigatorXmlView;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.dom4j.DocumentException;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Format.DAYS;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Format.HOURS;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Format.PRETTY;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

@WebTest({Category.FUNC_TEST, Category.ISSUE_NAVIGATOR})
@Restore("TestSearchRequestViewsAndIssueViews.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueNavigatorXmlView extends AbstractTestIssueNavigatorXmlView {

    @Inject
    private TextAssertions textAssertions;

    @Override
    public void setUp() {
        super.setUp();

        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
        backdoor.columnControl().addLoggedInUserColumns(ImmutableList.of("creator"));
    }

    @Test
    @Restore("TestIssueXmlViewAbsoluteIcon.xml")
    public void testIssueNavigatorXmlViewAbsoluteIcon() {
        tester.gotoPage("/sr/jira.issueviews:searchrequest-xml/10000/SearchRequest-10000.xml?tempMax=1000");
        textAssertions.assertRegexNoMatch(tester.getDialog().getResponseText(), "iconUrl=\"http://.*http://.*\"");

        // JRA-19099: url must also be escaped!
        textAssertions.assertRegexNoMatch(tester.getDialog().getResponseText(), "iconUrl=\".*\\?x=y&z=a");
        textAssertions.assertRegexMatch(tester.getDialog().getResponseText(), "iconUrl=\".*\\?x=y&amp;z=a");
    }

    /*
     * Checks that the issue navigators XML view has a link back to the original filter search and displays
     * the expected issues. (Checks 'reset=true' is not part of the url for a filter search as it is ignored JRA-12036)
     */
    @Test
    public void testIssueNavigatorXmlViewLinksToFilter() throws IOException, ParserConfigurationException, SAXException, TransformerException, DocumentException {
        //make a filter search
        final String filterId = backdoor.filters().createFilter("issuetype='New Feature'", "xmlview");

        //goto the xml view
        tester.gotoPage("/sr/jira.issueviews:searchrequest-xml/" + filterId + "/SearchRequest-" + filterId + ".xml?tempMax=1000");
        assertAndGetLinkToFilterWithId(filterId);
    }

    @Test
    public void testIssueXmlView() throws Exception {
        reconfigureTimetracking(PRETTY);
        execIssueXmlView();
    }

    @Test
    public void testIssueXmlViewDaysTimeFormat() throws Exception {
        reconfigureTimetracking(DAYS);
        execIssueXmlView();
    }

    @Test
    public void testIssueXmlViewHoursTimeFormat() throws Exception {
        reconfigureTimetracking(HOURS);
        execIssueXmlView();
    }

    private void execIssueXmlView() throws Exception {
        logger.log("Issue Navigator: Test that the IssueView XML page correctly shows the custom field information.");

        for (final Item item : items) {
            final String key = (String) item.getAttributeMap().get("key");
            logger.log("testing item key = " + key + "...");


            backdoor.issueTableClient().getIssueTable(""); // create session search
            tester.gotoPage("/browse/" + key);
            tester.clickLinkWithText("XML");

            final String responseText = tester.getDialog().getResponse().getText();
            final Document doc = XMLUnit.buildControlDocument(responseText);
            final String xpath = "//channel[title='" + XML_TITLE + "'][contains(link,'" + getEnvironmentData().getBaseUrl() + "')][description='"
                    + XML_DESCRIPTION_SINGLE + "'][language='" + XML_LANGUAGE + "']";

            logger.log("Searching for existence of xpath " + xpath);
            XMLAssert.assertXpathExists(xpath, doc);

            execTestOnAttributes(item, doc);
            execTestOnComments(item, doc);
            execTestOnLinks(item, doc);
            execTestOnComponents(item, doc);
            execTestOnCustomFields(item, doc);
            execTestOnAttachements(item, doc);
        }

    }

    @Test
    public void testXMLViewAllItems() throws Exception {
        reconfigureTimetracking(PRETTY);
        execXMLViewAllItems();
    }

    @Test
    public void testXMLViewAllItemsDaysTimeFormat() throws Exception {
        reconfigureTimetracking(DAYS);
        execXMLViewAllItems();
    }

    @Test
    public void testXMLViewAllItemsHoursTimeFormat() throws Exception {
        reconfigureTimetracking(HOURS);
        execXMLViewAllItems();
    }

    public void execXMLViewAllItems() throws Exception {
        logger.log("Issue Navigator: Test that the XML page correctly shows the custom field information.");
        tester.gotoPage("/sr/jira.issueviews:searchrequest-xml/temp/SearchRequest.xml?jqlQuery=project+%3D+HSP&tempMax=1000");
        final String responseText = tester.getDialog().getResponse().getText();
        final Document doc = XMLUnit.buildControlDocument(responseText);
        final String xpath = "//channel[title='" + XML_TITLE + "'][contains(link,'" + getEnvironmentData().getBaseUrl() + XML_LINK_MULTIPLE + "')][description='" +
                XML_DESCRIPTION_MULTIPLE + "'][language='" + XML_LANGUAGE + "']";
        logger.log("Searching for existence of xpath " + xpath);
        XMLAssert.assertXpathExists(xpath, doc);
        for (final Item item : items) {
            logger.log("testing item...");
            execTestOnAttributes(item, doc);
            execTestOnComments(item, doc);
            execTestOnLinks(item, doc);
            execTestOnComponents(item, doc);
            execTestOnCustomFields(item, doc);
            execTestOnAttachements(item, doc);
        }
    }

    /* Executes test on issue tag
     * JRA-15127
     */
    @Test
    public void testIssueXMLViewForIssueCountTag() throws Exception {
        logger.log("Issue Navigator: Test that the XML page shows correct values for the <issue> tag");

        int tempMax, start = 0;
        // iterate and check some combinations of start and tempMax
        for (tempMax = 0; tempMax < items.size() + 3; tempMax++) {
            // when tempMax is more than the item size, test different start issue
            if (tempMax > items.size()) {
                start = items.size() / 2;
            }

            checkIssueCountTag(start, tempMax);
        }
    }

    /**
     * Executes the assertions on the attributes of an item of the XML
     *
     * @param item item
     * @param doc  document
     * @throws javax.xml.transform.TransformerException if exceptional condition happens during the transformation process
     */
    private void execTestOnLinks(final TestIssueNavigatorXmlView.Item item, final Document doc) throws TransformerException {
        final TestIssueNavigatorXmlView.IssueLinks links = item.getLinks();
        if (links != null) {
            final String key = item.getAttribute("key");
            String xpath = "//item[key='" + key + "']/issuelinks/issuelinktype[@id='" + links.getId() + "'][name='"
                    + links.getName() + "']";
            logger.log("Searching for existence of xpath " + xpath);
            XMLAssert.assertXpathExists(xpath, doc);

            if (links.getOutLinks() != null && !links.getOutLinks().isEmpty()) {
                xpath = "//item[key='" + key + "']/issuelinks/issuelinktype[@id='" + links.getId()
                        + "']/outwardlinks[@description='" + links.getOutDesc() + "']";
                for (final Object o : links.getOutLinks()) {
                    final IssueLink link = (IssueLink) o;
                    final String xxpath = xpath + "/issuelink/issuekey[@id='" + link.getId() + "'][.='" + link.getLink() + "']";
                    logger.log("Searching for existence of xpath " + xxpath);
                    XMLAssert.assertXpathExists(xxpath, doc);
                }
            } else {
                xpath = "//item[key='" + key + "']/issuelinks/issuelinktype[@id='" + links.getId() + "']/outwardlinks";
                logger.log("Searching for existence of xpath " + xpath);
                XMLAssert.assertXpathNotExists(xpath, doc);
            }
            if (links.getInLinks() != null && !links.getInLinks().isEmpty()) {
                xpath = "//item[key='" + key + "']/issuelinks/issuelinktype[@id='" + links.getId()
                        + "']/inwardlinks[@description='" + links.getInDesc() + "']";
                for (final Object o : links.getInLinks()) {
                    final IssueLink link = (IssueLink) o;
                    final String xxpath = xpath + "/issuelink/issuekey[@id='" + link.getId() + "'][.='" + link.getLink() + "']";
                    logger.log("Searching for existence of xpath " + xxpath);
                    XMLAssert.assertXpathExists(xxpath, doc);
                }
            } else {
                xpath = "//item[key='" + key + "']/issuelinks/issuelinktype[@id='" + links.getId() + "']/inwardlinks";
                logger.log("Searching for existence of xpath " + xpath);
                XMLAssert.assertXpathNotExists(xpath, doc);
            }
        }
    }

    /**
     * Executes the assertions on the attributes of an item of the XML
     *
     * @param item item
     * @param doc  document
     * @throws javax.xml.transform.TransformerException if exceptional condition happens during the transformation process
     */
    @SuppressWarnings("StatementWithEmptyBody")
    private void execTestOnAttributes(final TestIssueNavigatorXmlView.Item item, final Document doc) throws TransformerException {
        final Map attributeMap = item.getAttributeMap();
        if (!attributeMap.isEmpty()) {
            StringBuffer xpath = new StringBuffer("//item");
            for (final Object o2 : attributeMap.entrySet()) {
                final Map.Entry entry = (Map.Entry) o2;
                final String key = (String) entry.getKey();
                if (TestIssueNavigatorXmlView.ATT_DATE_CREATED.equals(key) || TestIssueNavigatorXmlView.ATT_DATE_UPDATED.equals(key)
                        || TestIssueNavigatorXmlView.ATT_DATE_DUE.equals(key) || TestIssueNavigatorXmlView.ATT_DATE_RESOLVED.equals(key)) {
                    xpath.append("[");
                    xpath.append(key);
                    xpath.append("=*");
                    xpath.append("]");
                } else if (TestIssueNavigatorXmlView.ATT_TIMEORIGINALESTIMATE_DAYS.equals(key)
                        || TestIssueNavigatorXmlView.ATT_TIMEORIGINALESTIMATE_HOURS.equals(key)
                        || TestIssueNavigatorXmlView.ATT_TIMEORIGINALESTIMATE.equals(key)) {
                    if (TestIssueNavigatorXmlView.ATT_TIMEORIGINALESTIMATE_DAYS.equals(key) && (DAYS == timeFormat)
                            || TestIssueNavigatorXmlView.ATT_TIMEORIGINALESTIMATE_HOURS.equals(key) && (HOURS == timeFormat)
                            || TestIssueNavigatorXmlView.ATT_TIMEORIGINALESTIMATE.equals(key) && (PRETTY == timeFormat)) {
                        xpath.append("[");
                        xpath.append(TestIssueNavigatorXmlView.ATT_TIMEORIGINALESTIMATE);
                        xpath.append("='");
                        xpath.append(entry.getValue());
                        xpath.append("'");
                        xpath.append("]");
                    }
                } else if (TestIssueNavigatorXmlView.ATT_TIMESPENT_DAYS.equals(key)
                        || TestIssueNavigatorXmlView.ATT_TIMESPENT_HOURS.equals(key)
                        || TestIssueNavigatorXmlView.ATT_TIMESPENT.equals(key)) {
                    if (TestIssueNavigatorXmlView.ATT_TIMESPENT_DAYS.equals(key) && (DAYS == timeFormat)
                            || TestIssueNavigatorXmlView.ATT_TIMESPENT_HOURS.equals(key) && (HOURS == timeFormat)
                            || TestIssueNavigatorXmlView.ATT_TIMESPENT.equals(key) && (PRETTY == timeFormat)) {
                        xpath.append("[");
                        xpath.append(TestIssueNavigatorXmlView.ATT_TIMESPENT);
                        xpath.append("='");
                        xpath.append(entry.getValue());
                        xpath.append("'");
                        xpath.append("]");
                    }
                } else if (TestIssueNavigatorXmlView.ATT_REMAINING_ESTIMATE_DAYS.equals(key)
                        || TestIssueNavigatorXmlView.ATT_REMAINING_ESTIMATE_HOURS.equals(key)
                        || TestIssueNavigatorXmlView.ATT_REMAINING_ESTIMATE.equals(key)) {
                    if (TestIssueNavigatorXmlView.ATT_REMAINING_ESTIMATE_DAYS.equals(key) && (DAYS == timeFormat)
                            || TestIssueNavigatorXmlView.ATT_REMAINING_ESTIMATE_HOURS.equals(key) && (HOURS == timeFormat)
                            || TestIssueNavigatorXmlView.ATT_REMAINING_ESTIMATE.equals(key) && (PRETTY == timeFormat)) {
                        xpath.append("[");
                        xpath.append(TestIssueNavigatorXmlView.ATT_REMAINING_ESTIMATE);
                        xpath.append("='");
                        xpath.append(entry.getValue());
                        xpath.append("'");
                        xpath.append("]");
                    }
                } else if (TestIssueNavigatorXmlView.ATT_CREATOR.equals(key)) {
                    //ignore the creator field - it's navigable and not sortable, looking at IssueViewRequestParamsHelperImpl
                    //we only ever include Orderable Fields in the returned XML.
                } else {
                    xpath.append("[");
                    xpath.append(key);
                    xpath.append("='");
                    xpath.append(entry.getValue());
                    xpath.append("'");
                    xpath.append("]");
                }
            }
            logger.log("Searching for existence of xpath " + xpath);
            XMLAssert.assertXpathExists(xpath.toString(), doc);

            final Map allAttributesMap = item.getAllAttributeAttributesMap();
            for (final Object o1 : allAttributesMap.entrySet()) {
                xpath = new StringBuffer("//item/");
                final Map.Entry attributeEntry = (Map.Entry) o1;
                xpath.append(attributeEntry.getKey()); // attribute (tag) name
                final Map attributeAttributes = (Map) attributeEntry.getValue();
                for (final Object o : attributeAttributes.entrySet()) {
                    final Map.Entry attEntry = (Map.Entry) o;
                    xpath.append("[@");
                    xpath.append(attEntry.getKey()); // attribute name
                    xpath.append("='");
                    xpath.append(TextKit.htmlEncode(attEntry.getValue().toString())); // attribute value
                    xpath.append("']");
                }

                logger.log("Searching for existence of xpath " + xpath);
                XMLAssert.assertXpathExists(xpath.toString(), doc);
            }
        }
    }

    /**
     * Executes the assertions on the comment part of an item of the XML
     *
     * @param item item
     * @param doc  document
     * @throws javax.xml.transform.TransformerException if exceptional condition happens during the transformation process
     */
    private void execTestOnComments(final TestIssueNavigatorXmlView.Item item, final Document doc) throws TransformerException {
        final List commentList = item.getComments();
        if (commentList != null && !commentList.isEmpty()) {
            for (final Object aCommentList : commentList) {
                final Comment comment = (Comment) aCommentList;
                final StringBuilder xpath = new StringBuilder();
                xpath.append("//item[key='");
                xpath.append(item.getAttribute("key"));
                xpath.append("']/comments/comment");
                xpath.append("[@author='");
                xpath.append(comment.getAuthor());
                xpath.append("'][@created]");
                if (StringUtils.isNotBlank(comment.getLevel())) {
                    xpath.append("[@grouplevel='");
                    xpath.append(comment.getLevel());
                    xpath.append("']");
                }
                xpath.append("[. = '");
                xpath.append(comment.getValue());
                xpath.append("']");
                logger.log("Searching for existence of xpath " + xpath);
                XMLAssert.assertXpathExists(xpath.toString(), doc);
            }
        } else {
            final String xpath = "//item[key='" + item.getAttribute("key") + "']/comments";
            logger.log("Searching for existence of xpath " + xpath);
            XMLAssert.assertXpathNotExists(xpath, doc);
        }
    }

    /**
     * Executes the assertions on the attachment part of an item of the XML
     *
     * @param item item
     * @param doc  document
     * @throws javax.xml.transform.TransformerException if exceptional condition happens during the transformation process
     */
    private void execTestOnAttachements(final TestIssueNavigatorXmlView.Item item, final Document doc) throws TransformerException {
        final List<String> attachmentList = item.getAttachments();
        if (attachmentList != null && !attachmentList.isEmpty()) {
            for (final String attachment : attachmentList) {
                final StringBuilder xpath = new StringBuilder();
                xpath.append("//item[key='");
                xpath.append(item.getAttribute("key"));
                xpath.append("']/attachments/attachment");
                xpath.append("[@name='");
                xpath.append(attachment);
                xpath.append("']");
                logger.log("Searching for existence of xpath " + xpath);
                XMLAssert.assertXpathExists(xpath.toString(), doc);
            }
        } else {
            final String xpath = "//item[key='" + item.getAttribute("key") + "']/attachments/attachment";
            logger.log("Searching for existence of xpath " + xpath);
            XMLAssert.assertXpathNotExists(xpath, doc);
        }
    }


    /**
     * Executes the assertions on the components part of an item of the XML
     *
     * @param item item
     * @param doc  document
     * @throws javax.xml.transform.TransformerException if exceptional condition happens during the transformation process
     */
    private void execTestOnComponents(final TestIssueNavigatorXmlView.Item item, final Document doc) throws TransformerException {
        final List<String> componentList = item.getComponents();
        if (componentList != null && !componentList.isEmpty()) {
            for (final String component : componentList) {
                final StringBuilder xpath = new StringBuilder();
                xpath.append("//item[key='");
                xpath.append(item.getAttribute("key"));
                xpath.append("'][component='");
                xpath.append(component);
                xpath.append("']");
                logger.log("Searching for existence of xpath " + xpath);
                XMLAssert.assertXpathExists(xpath.toString(), doc);
            }
        } else {
            final String xpath = "//item[key='" + item.getAttribute("key") + "']/component";
            logger.log("Searching for existence of xpath " + xpath);
            XMLAssert.assertXpathNotExists(xpath, doc);
        }
    }

    /**
     * Executes the assertions on the custom field part on an item of the XML
     *
     * @param item item
     * @param doc  document
     * @throws javax.xml.transform.TransformerException if exceptional condition happens during the transformation process
     */
    private void execTestOnCustomFields(final TestIssueNavigatorXmlView.Item item, final Document doc) throws TransformerException {
        final List<CustomField> customFieldList = item.getCustomFields();
        for (final CustomField customField : customFieldList) {
            final StringBuilder xpath = new StringBuilder();
            xpath.append("//item/customfields/customfield[@id='");
            xpath.append(customField.getId());
            xpath.append("'][customfieldname='");
            xpath.append(customField.getName());
            xpath.append("'][customfieldvalues");
            final String customFieldKey = customField.getKey();
            if (TestIssueNavigatorXmlView.TYPE_DATETIME.equals(customFieldKey) || TestIssueNavigatorXmlView.TYPE_DATEPICKER.equals(customFieldKey)) {
                for (final Iterator j = customField.getValues().iterator(); j.hasNext(); j.next()) {
                    xpath.append("[customfieldvalue=*]");
                }
            } else if (TestIssueNavigatorXmlView.TYPE_CASCADINGSELECT.equals(customFieldKey)) {
                for (final CustomField.Value value : customField.getValues()) {
                    xpath.append("[customfieldvalue='");
                    xpath.append(value.getDisplayValue());
                    xpath.append("']");
                }
            } else {
                for (final CustomField.Value value : customField.getValues()) {
                    xpath.append("[customfieldvalue='");
                    xpath.append(value.getValue());
                    xpath.append("']");
                }
            }
            xpath.append("]");
            logger.log("Searching for existence of xpath " + xpath);
            XMLAssert.assertXpathExists(xpath.toString(), doc);
        }
    }

    // JRA-15127
    private void checkIssueCountTag(final int start, final int tempMax) throws Exception {
        tester.gotoPage("/sr/jira.issueviews:searchrequest-xml/temp/SearchRequest.xml?sorter/field=issuekey&sorter/order=DESC&pager/start=" + start + "&tempMax=" + tempMax);
        final String responseText = tester.getDialog().getResponse().getText();

        final int end = Math.min(start + tempMax, items.size());
        final Document doc = XMLUnit.buildControlDocument(responseText);
        final String xpath = "//issue[@start='" + start + "']" +
                "[@end='" + end + "']" +
                "[@total='" + items.size() + "']";
        logger.log("Searching for existence of xpath " + xpath);
        XMLAssert.assertXpathExists(xpath, doc);
    }

}
