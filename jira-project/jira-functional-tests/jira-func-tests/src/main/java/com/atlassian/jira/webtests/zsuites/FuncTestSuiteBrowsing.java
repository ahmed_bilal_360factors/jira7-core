package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.admin.TestAdminMenuWebFragment;
import com.atlassian.jira.webtests.ztests.admin.TestEventTypes;
import com.atlassian.jira.webtests.ztests.admin.TestGeneralConfiguration;
import com.atlassian.jira.webtests.ztests.admin.TestGlobalPermissions;
import com.atlassian.jira.webtests.ztests.admin.TestIntegrityChecker;
import com.atlassian.jira.webtests.ztests.admin.TestModz;
import com.atlassian.jira.webtests.ztests.admin.TestServices;
import com.atlassian.jira.webtests.ztests.admin.TestSystemInfoPage;
import com.atlassian.jira.webtests.ztests.admin.TestTimeTrackingAdmin;
import com.atlassian.jira.webtests.ztests.admin.index.TestIndexAdmin;
import com.atlassian.jira.webtests.ztests.admin.issuetypes.TestIssueTypeDeleteFieldConfig;
import com.atlassian.jira.webtests.ztests.admin.statuses.TestStatuses;
import com.atlassian.jira.webtests.ztests.admin.trustedapps.TestTrustedApplicationClientVersion0;
import com.atlassian.jira.webtests.ztests.admin.trustedapps.TestTrustedApplicationClientVersion1;
import com.atlassian.jira.webtests.ztests.admin.trustedapps.TestTrustedApplications;
import com.atlassian.jira.webtests.ztests.attachment.TestAttachFile;
import com.atlassian.jira.webtests.ztests.attachment.TestAttachmentsListSorting;
import com.atlassian.jira.webtests.ztests.attachment.TestEditAttachmentSettings;
import com.atlassian.jira.webtests.ztests.attachment.TestImageAttachmentsGallerySorting;
import com.atlassian.jira.webtests.ztests.customfield.TestCustomFieldMultiUserPicker;
import com.atlassian.jira.webtests.ztests.customfield.TestMultiGroupSelector;
import com.atlassian.jira.webtests.ztests.email.TestEmailSubscription;
import com.atlassian.jira.webtests.ztests.email.TestMailServer;
import com.atlassian.jira.webtests.ztests.email.TestSharedEmailSubscription;
import com.atlassian.jira.webtests.ztests.favourite.TestAdjustFavourite;
import com.atlassian.jira.webtests.ztests.filter.TestFilterPageNavigation;
import com.atlassian.jira.webtests.ztests.issue.TestVoters;
import com.atlassian.jira.webtests.ztests.issue.TestVotersWhenVotePermissionSetToUserPickerCustomField;
import com.atlassian.jira.webtests.ztests.issue.TestWatchers;
import com.atlassian.jira.webtests.ztests.issue.assign.TestAssignToMe;
import com.atlassian.jira.webtests.ztests.issue.history.TestRecentIssueHistory;
import com.atlassian.jira.webtests.ztests.issue.security.TestIssueSecurityLevel;
import com.atlassian.jira.webtests.ztests.issue.security.TestIssueSecurityLevelOnlyCheckedForBrowseProjectPermission;
import com.atlassian.jira.webtests.ztests.misc.Test500Page;
import com.atlassian.jira.webtests.ztests.misc.TestAnnouncementBanner;
import com.atlassian.jira.webtests.ztests.misc.TestBasic;
import com.atlassian.jira.webtests.ztests.misc.TestCustomCreateButtonName;
import com.atlassian.jira.webtests.ztests.misc.TestDateInputValidationOnCreateIssue;
import com.atlassian.jira.webtests.ztests.misc.TestDefaultJiraDataFromInstall;
import com.atlassian.jira.webtests.ztests.misc.TestJohnsonFiltersWhileNotSetup;
import com.atlassian.jira.webtests.ztests.misc.TestLoginActions;
import com.atlassian.jira.webtests.ztests.misc.TestMyJiraHome;
import com.atlassian.jira.webtests.ztests.misc.TestReleaseNotes;
import com.atlassian.jira.webtests.ztests.misc.TestRememberMeCookie;
import com.atlassian.jira.webtests.ztests.misc.TestRootPage;
import com.atlassian.jira.webtests.ztests.misc.TestSessionIdInUrl;
import com.atlassian.jira.webtests.ztests.misc.TestSetup;
import com.atlassian.jira.webtests.ztests.misc.TestShowConstantsHelp;
import com.atlassian.jira.webtests.ztests.misc.TestSignup;
import com.atlassian.jira.webtests.ztests.misc.TestUnsupportedBrowser;
import com.atlassian.jira.webtests.ztests.misc.TestWadlGeneration;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorEncoding;
import com.atlassian.jira.webtests.ztests.project.TestNotificationOptions;
import com.atlassian.jira.webtests.ztests.timetracking.legacy.TestTimeTrackingAggregates;
import com.atlassian.jira.webtests.ztests.user.TestGroupSelector;
import com.atlassian.jira.webtests.ztests.user.TestMultiUserPicker;
import com.atlassian.jira.webtests.ztests.user.TestUserBrowser;
import com.atlassian.jira.webtests.ztests.user.TestViewProfile;
import com.atlassian.jira.webtests.ztests.workflow.TestExcludeResolutionOnTransitions;
import com.google.common.collect.ImmutableList;

import java.util.List;

public class FuncTestSuiteBrowsing {

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestDefaultJiraDataFromInstall.class)

                .add(Test500Page.class)
                .add(TestLoginActions.class)
                .add(TestMyJiraHome.class)
                .add(TestRootPage.class)
                .add(TestUserBrowser.class)
                .add(TestGroupSelector.class)
                .add(TestMultiUserPicker.class)
                .add(TestMultiGroupSelector.class)
                .add(TestCustomFieldMultiUserPicker.class)
                .add(TestIssueNavigatorEncoding.class)
                .add(TestViewProfile.class)
                .add(TestRememberMeCookie.class)
                .add(TestSessionIdInUrl.class)
                .add(TestSystemInfoPage.class)
                .add(TestTimeTrackingAdmin.class)
                .add(TestVoters.class)
                .add(TestVotersWhenVotePermissionSetToUserPickerCustomField.class)
                .add(TestWatchers.class)
                .add(TestBasic.class)


                .add(TestAdjustFavourite.class)
                .add(TestAdminMenuWebFragment.class)
                .add(TestAnnouncementBanner.class)
                .add(TestAssignToMe.class)
                .add(TestAttachFile.class)
                .add(TestAttachmentsListSorting.class)
                .add(TestImageAttachmentsGallerySorting.class)
                .add(TestCustomCreateButtonName.class)


                .add(TestEditAttachmentSettings.class)
                .add(TestEmailSubscription.class)
                .add(TestEventTypes.class)
                .add(TestExcludeResolutionOnTransitions.class)
                .add(TestGeneralConfiguration.class)
                .add(TestGlobalPermissions.class)
                .add(TestIndexAdmin.class)
                .add(TestIntegrityChecker.class)

                        // this test must run before JIRA is setup because it tests the
                        // johnson event filter for this situation
                .add(TestJohnsonFiltersWhileNotSetup.class)

                .add(TestMailServer.class)

                .add(TestNotificationOptions.class)
                .add(TestRecentIssueHistory.class)
                .add(TestStatuses.class)
                .add(TestReleaseNotes.class)
                .add(TestServices.class)
                .add(TestSetup.class)
                .add(TestSharedEmailSubscription.class)
                .add(TestShowConstantsHelp.class)
                .add(TestSignup.class)
                .add(TestTimeTrackingAggregates.class)
                .add(TestTrustedApplicationClientVersion0.class)
                .add(TestTrustedApplicationClientVersion1.class)
                .add(TestTrustedApplications.class)

                .add(TestModz.class)
                .add(TestIssueTypeDeleteFieldConfig.class)
                .add(TestFilterPageNavigation.class)
                .add(TestDateInputValidationOnCreateIssue.class)
                .add(TestIssueSecurityLevel.class)
                .add(TestIssueSecurityLevelOnlyCheckedForBrowseProjectPermission.class)
                .add(TestUnsupportedBrowser.class)
                .add(TestWadlGeneration.class)
                .build();
    }
}