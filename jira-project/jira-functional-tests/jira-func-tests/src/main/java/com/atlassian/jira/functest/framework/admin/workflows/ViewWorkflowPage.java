package com.atlassian.jira.functest.framework.admin.workflows;

import com.atlassian.jira.functest.framework.admin.WorkflowSteps;
import com.atlassian.jira.functest.framework.admin.WorkflowStepsImpl;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;

/**
 * @since v5.1
 */
public class ViewWorkflowPage {
    private final TextView textView;
    private final WebTester tester;

    public ViewWorkflowPage(WebTester tester, JIRAEnvironmentData environmentData, int logIndentLevel) {
        this(tester, environmentData);
    }

    @Inject
    public ViewWorkflowPage(WebTester tester, JIRAEnvironmentData environmentData) {
        this.tester = tester;
        textView = new TextView(tester, environmentData, 2);
    }

    public TextView textView() {
        return textView;
    }

    public String downloadAsXml() {
        tester.clickLinkWithText("XML");
        return tester.getDialog().getResponseText();
    }

    public static class DiagramView {
        public DiagramView goTo() {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    public static class TextView {
        private final WebTester tester;
        private final WorkflowSteps workflowSteps;

        public TextView(WebTester tester, JIRAEnvironmentData jiraEnvironmentData, int logIndentLevel) {
            this.tester = tester;
            workflowSteps = new WorkflowStepsImpl(tester, jiraEnvironmentData, logIndentLevel);
        }

        public TextView goTo() {
            tester.clickLink("workflow-text");
            return this;
        }

        public WorkflowSteps steps() {
            return workflowSteps;
        }
    }
}
