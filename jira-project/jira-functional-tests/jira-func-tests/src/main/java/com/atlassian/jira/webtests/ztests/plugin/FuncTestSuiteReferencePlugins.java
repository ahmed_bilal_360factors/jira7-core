package com.atlassian.jira.webtests.ztests.plugin;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Test suite for all functional tests against plugin modules reloadability.
 *
 * @since v4.3
 */
public class FuncTestSuiteReferencePlugins {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestWebWork1PrepareActionInterface.class)
                .add(TestCreateIsueinPostFunction.class)
                .add(TestPluginWikiRenderer.class)
                .build();
    }
}
