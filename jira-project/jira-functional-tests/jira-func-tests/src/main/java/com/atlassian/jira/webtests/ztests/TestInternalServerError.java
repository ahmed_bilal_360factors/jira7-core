package com.atlassian.jira.webtests.ztests;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.Backdoor;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import java.util.UUID;

import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@WebTest({FUNC_TEST})
public class TestInternalServerError extends BaseJiraFuncTest {

    @Before
    public void setUp() {
        backdoor.server().recordLogs(true);
    }

    @After
    public void tearDown() {
        backdoor.server().recordLogs(false);
    }

    @Test
    public void testReferralIdVisibleInLogs() {
        tester.gotoPage("/plugins/servlet/functest-cause-error");
        String errorId = tester.getDialog().getElement("log-referral-id").getTextContent();
        assertTrue(errorId.matches("^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"));
        String logs = backdoor.server().getLogs();
        assertThat(logs, containsString(errorId));
    }

    @Test
    public void testLogSensitiveData() {
        String password = UUID.randomUUID().toString();
        tester.gotoPage("/plugins/servlet/functest-cause-error?os_username=admin&os_password=" + password);
        String logs = backdoor.server().getLogs();
        assertThat(logs, not(containsString(password)));
    }
}
