package com.atlassian.jira.webtests.ztests.issue.move;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperationProgress;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Session;
import com.atlassian.jira.functest.framework.SessionFactory;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Tests that attachments are moved successfully with an Issue move (single and Bulk).
 * <p/>
 * There are separate tests depending on whether we need to do a workflow migration or not, because there was a regression
 * that only occured when we did the migration (JRA-16223).
 */
@WebTest({Category.FUNC_TEST, Category.ATTACHMENTS, Category.MOVE_ISSUE})
@Restore("TestMoveIssueAttachment.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestMoveIssueAttachment extends BaseJiraFuncTest {
    private String attachmentPath;

    @Inject
    private BulkOperationProgress bulkOperationProgress;

    @Inject
    private Administration administration;

    @Inject
    private SessionFactory sessionFactory;

    @Before
    public void setUpTest() {
        // Enable attachments as we want to use the default attachments directory.
        administration.attachments().enable();
        attachmentPath = administration.getCurrentAttachmentPath();
    }

    @Test
    public void testMoveSingleIssueSameWorkflow() throws Exception {
        // we need to put a file in the attachments directory where JIRA expects to find it from the DB attachment data.
        installAttachedFile("AAAAAA");
        navigation.issue().viewIssue("RAT-1");
        tester.clickLinkWithText("info.txt");
        tester.assertTextPresent("AAAAAA");

        navigation.issue().viewIssue("RAT-1");
        // Click Link 'Move' (id='move_issue').
        tester.clickLink("move-issue");
        // Select 'Bovine' from select box 'pid'.
        navigation.issue().selectProject("Bovine");
        tester.submit("Next >>");
        tester.submit("Next >>");
        tester.submit("Move");
        tester.clickLinkWithText("info.txt");
        // Now we should see the data in the info.txt file if it was moved correctly.
        tester.assertTextPresent("AAAAAA");

        // Check that the file is where we expect it to be:
        File movedFile = new File(attachmentPath + "/COW/10000/COW-15", "10010");
        Assert.assertTrue(movedFile.exists());
        // Delete the file, or it could cause the next test to accidentally pass.
        deleteAttachment("COW-15");
        // Check that the file is actually deleted from the file sytem:
        Assert.assertFalse(movedFile.exists());
    }

    @Test
    public void testMoveSingleIssueDifferentWorkflow() throws Exception {
        // we need to put a file in the attachments directory where JIRA expects to find it from the DB attachment data.
        installAttachedFile("testMo");

        navigation.issue().viewIssue("RAT-1");
        // Click Link 'Move' (id='move_issue').
        tester.clickLink("move-issue");
        tester.assertTextPresent("Step 1 of 4");
        // Select 'Canine' from select box 'pid'.
        navigation.issue().selectProject("Canine");
        tester.submit("Next >>");
        tester.assertTextPresent("Step 3 of 4");
        tester.submit("Next >>");
        tester.assertTextPresent("Move Issue: Confirm");
        tester.submit("Move");
        tester.assertTextPresent("DOG-1");
        navigation.issue().viewIssue("DOG-1");
        tester.clickLinkWithText("info.txt");
        // Now we should see the data in the info.txt file if it was moved correctly.
        tester.assertTextPresent("testMo");

        // Check that the file is where we expect it to be:
        File movedFile = new File(attachmentPath + "/DOG/10000/DOG-1", "10010");
        Assert.assertTrue(movedFile.exists());
        // Delete the file, or it could cause the next test to accidentally pass.
        deleteAttachment("DOG-1");
        // Check that the file is actually deleted from the file sytem:
        Assert.assertFalse(movedFile.exists());
    }

    @Test
    public void testBulkMoveIssueSameWorkflow() throws Exception {
        // we need to put a file in the attachments directory where JIRA expects to find it from the DB attachment data.
        installAttachedFile("testBu");

        // Go to navigator
        navigation.issueNavigator().bulkEditAllIssues();
        tester.checkCheckbox("bulkedit_10000", "on");
        tester.submit("Next");
        tester.checkCheckbox("operation", "bulk.move.operation.name");
        tester.submit("Next");
        // Select 'Bovine' from select box '10000_1_pid'.
        navigation.issue().selectProject("Bovine", "10000_1_pid");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        bulkOperationProgress.waitAndReloadBulkOperationProgressPage(tester);

        // Go tot the moved issue - COW-15
        navigation.issue().viewIssue("COW-15");
        tester.clickLinkWithText("info.txt");
        // Now we should see the data in the info.txt file if it was moved correctly.
        tester.assertTextPresent("testBu");

        // Check that the file is where we expect it to be:
        File movedFile = new File(attachmentPath + "/COW/10000/COW-15", "10010");
        Assert.assertTrue(movedFile.exists());
        // Delete the file, or it could cause the next test to accidentally pass.
        deleteAttachment("COW-15");
        // Check that the file is actually deleted from the file sytem:
        Assert.assertFalse(movedFile.exists());
    }

    @Test
    public void testBulkMoveIssueDifferentWorkflow() throws Exception {
        // we need to put a file in the attachments directory where JIRA expects to find it from the DB attachment data.
        installAttachedFile("testBu");

        // Go to navigator
        navigation.issueNavigator().bulkEditAllIssues();
        tester.checkCheckbox("bulkedit_10000", "on");
        tester.submit("Next");
        tester.checkCheckbox("operation", "bulk.move.operation.name");
        tester.submit("Next");
        // Select 'Bovine' from select box '10000_1_pid'.
        navigation.issue().selectProject("Canine", "10000_1_pid");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        bulkOperationProgress.waitAndReloadBulkOperationProgressPage(tester);

        // Go to the moved issue - Dog-1
        navigation.issue().viewIssue("DOG-1");
        tester.clickLinkWithText("info.txt");
        // Now we should see the data in the info.txt file if it was moved correctly.
        tester.assertTextPresent("testBu");

        // Check that the file is where we expect it to be:
        File movedFile = new File(attachmentPath + "/DOG/10000/DOG-1", "10010");
        Assert.assertTrue(movedFile.exists());
        // Delete the file, or it could cause the next test to accidentally pass.
        deleteAttachment("DOG-1");
        // Check that the file is actually deleted from the file sytem:
        Assert.assertFalse(movedFile.exists());
    }

    @Test
    public void testMoveSingleIssueTwoUsers() throws Exception {
        // we need to put a file in the attachments directory where JIRA expects to find it from the DB attachment data.
        installAttachedFile("WWWWww");

        final Session otherUser = sessionFactory.begin();

        // Start a move with User 1
        navigation.issue().viewIssue("RAT-1");
        // Click Link 'Move' (id='move_issue').
        tester.clickLink("move-issue");

        // Start a move with User 2
        otherUser.withSession(()->{
            navigation.login(ADMIN_USERNAME);
            navigation.issue().viewIssue("RAT-1");
            tester.clickLink("move-issue");
        });

        // Let user 1 move the issue to project Bovine
        // Select 'Bovine' from select box 'pid'.
        navigation.issue().selectProject("Bovine");
        tester.submit("Next >>");
        tester.assertTextPresent("Step 3 of 4");
        tester.submit("Next >>");

        // Let User 2 try to move the isse to project "Canine"
        otherUser.withSession(()-> {
            navigation.issue().selectProject("Canine");
            tester.submit("Next >>");
            tester.assertTextPresent("Step 3 of 4");
            tester.submit("Next >>");
            tester.assertTextPresent("Move Issue: Confirm");
            tester.submit("Move");
            tester.assertTextPresent("DOG-1");
            tester.clickLinkWithText("info.txt");
            tester.assertTextPresent("WWWWww");
        });

        // User 1
        tester.assertTextPresent("Move Issue: Confirm");
        tester.submit("Move");
        tester.assertTextPresent("Move Issue: Confirm");
        tester.assertTextPresent("Cannot move Issue RAT-1 because it has already been moved to DOG-1.");

        // Check that the file is where we expect it to be:
        File movedFile = new File(attachmentPath + "/DOG/10000/DOG-1", "10010");
        Assert.assertTrue(movedFile.exists());
        // Delete the file, or it could cause the next test to accidentally pass.
        deleteAttachment("DOG-1");
        // Check that the file is actually deleted from the file sytem:
        Assert.assertFalse(movedFile.exists());
    }

    /**
     * This tests what happens to attachments when two users do a bulk move at the same time to different projects.
     * <p/>
     * See http://jira.atlassian.com/browse/JRA-15475
     *
     * @throws Exception If the test is stuffed.
     */
    @Test
    public void testBulkMoveIssueTwoUsers() throws Exception {
        // we need to put a file in the attachments directory where JIRA expects to find it from the DB attachment data.
        installAttachedFile("JJJJJJ");

        final Session otherUser = sessionFactory.begin();

        //Start the bulk move for user 1
        navigation.issueNavigator().bulkEditAllIssues();
        tester.assertTextPresent("Step 1 of 4: Choose Issues");
        tester.checkCheckbox("bulkedit_10000", "on");
        tester.submit("Next");
        tester.assertTextPresent("Step 2 of 4: Choose Operation");
        tester.checkCheckbox("operation", "bulk.move.operation.name");
        tester.submit("Next");
        tester.assertTextPresent("Select Projects and Issue Types");
        // Select 'Canine' from select box '10000_1_pid'.
        navigation.issue().selectProject("Canine", "10000_1_pid");
        tester.submit("Next");
        tester.assertTextPresent("Update Fields for Target Project 'Canine' - Issue Type 'Bug'");
        tester.submit("Next");
        tester.assertTextPresent("Confirmation");

        //Start the bulk move for user 2
        otherUser.withSession(()-> {
            navigation.login(ADMIN_USERNAME);
            navigation.issueNavigator().bulkEditAllIssues();
            tester.assertTextPresent("Step 1 of 4: Choose Issues");
            tester.checkCheckbox("bulkedit_10000", "on");
            tester.submit("Next");
            tester.assertTextPresent("Step 2 of 4: Choose Operation");
            tester.checkCheckbox("operation", "bulk.move.operation.name");
            tester.submit("Next");
            tester.assertTextPresent("Select Projects and Issue Types");
            // Select 'Bovine' from select box '10000_1_pid'.
            navigation.issue().selectProject("Bovine", "10000_1_pid");
            tester.submit("Next");
            tester.assertTextPresent("Update Fields for Target Project 'Bovine' - Issue Type 'Bug'");
            tester.submit("Next");
            tester.assertTextPresent("Confirmation");
            tester.submit("Next");
            bulkOperationProgress.waitAndReloadBulkOperationProgressPage();
            tester.assertTextPresent("Issue Navigator");

            // User 2 has completed the move - check the attachment
            // Go to the moved issue
            navigation.issue().viewIssue("COW-15");
            tester.clickLinkWithText("info.txt");
            // Now we should see the data in the info.txt file if it was moved correctly.
            tester.assertTextPresent("JJJJJJ");
        });
        // Check that the file is where we expect it to be:
        File movedFile = new File(attachmentPath + "/COW/10000/COW-15", "10010");
        Assert.assertTrue(movedFile.exists());

        // Now let User 1 finish trying to move:

        tester.submit("Next");
        tester.assertTextPresent("Confirmation");
        tester.assertTextPresent("At least one of the issues you are trying to move has been recently moved by another user (RAT-1). Please cancel and start again.");

        // Check that the file is still in COW:
        movedFile = new File(attachmentPath + "/COW/10000/COW-15", "10010");
        Assert.assertTrue(movedFile.exists());
        // Delete the file, or it could cause the next test to accidentally pass.
        deleteAttachment("COW-15");
        // Check that the file is actually deleted from the file sytem:
        Assert.assertFalse(movedFile.exists());
    }

    private String installAttachedFile(final String contents) throws IOException {
        File attachedFile = new File(attachmentPath + "/RAT/RAT-1", "10010");
        attachedFile.getParentFile().mkdirs();
        PrintWriter out = new PrintWriter(new FileWriter(attachedFile));
        try {
            out.println(contents);
        } finally {
            out.close();
        }
        return attachmentPath;
    }

    private void deleteAttachment(String issueKey) {
        navigation.issue().viewIssue(issueKey);
        tester.clickLink("manage-attachment-link");
        // Click Link 'Delete'
        tester.clickLink("del_10010");
        tester.submit("Delete");
    }
}
