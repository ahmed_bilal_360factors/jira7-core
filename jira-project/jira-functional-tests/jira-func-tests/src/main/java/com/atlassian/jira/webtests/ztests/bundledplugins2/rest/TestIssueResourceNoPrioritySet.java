package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DUE_DATE_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PRIORITY_BLOCKER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PRIORITY_FIELD_ID;

@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueResourceNoPrioritySet extends BaseJiraFuncTest {
    private IssueClient issueClient;

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        issueClient = new IssueClient(environmentData);
    }

    @Test
    @Restore("TestIssueResourceNoPrioritySet.xml")
    public void testUnassignedIssueHasNoValue() throws Exception {
        final Issue issue = issueClient.get("TST-2");
        Assert.assertNull(issue.fields.priority);
        // testing it here too - to save time of func tests (every time restoring JIRA already takes too long)
        Assert.assertNull(issue.fields.reporter);

        final Issue issue2 = issueClient.get("TST-1");
        Assert.assertEquals(PRIORITY_BLOCKER, issue2.fields.priority.name());
        Assert.assertEquals(ADMIN_USERNAME, issue2.fields.reporter.name);

        administration.fieldConfigurations().defaultFieldConfiguration().hideFields(PRIORITY_FIELD_ID);
        Assert.assertNull(issueClient.get("TST-2").fields.priority);
        Assert.assertNull(issueClient.get("TST-1").fields.priority);

        // now testing something similar - Due Date built-in field (again to save time on restoring JIRA)

        Assert.assertNull(issueClient.get("TST-1").fields.duedate);

        administration.fieldConfigurations().defaultFieldConfiguration().hideFields(DUE_DATE_FIELD_ID);
        Assert.assertNull(issueClient.get("TST-1").fields.duedate);

        administration.fieldConfigurations().defaultFieldConfiguration().showFields(DUE_DATE_FIELD_ID);
        navigation.issue().setDueDate("TST-1", "10/May/11");
        Assert.assertTrue(issueClient.get("TST-1").fields.duedate.startsWith("2011-05-"));

    }
}
