package com.atlassian.jira.functest.rule;

import com.atlassian.jira.functest.framework.SkipSetup;
import com.atlassian.jira.functest.framework.TestAnnotationsExtractor;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.functest.framework.setup.JiraSetupInstanceHelper;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.util.function.Supplier;

/**
 * The rule ensures that JIRA is setup.
 *
 * @since v6.5
 */
public class EnsureJiraSetupRule implements TestRule {
    private final Supplier<Backdoor> backdoorSupplier;
    private final Supplier<JIRAEnvironmentData> environmentDataSupplier;
    private final Supplier<WebTester> webTesterSupplier;

    public EnsureJiraSetupRule(final Supplier<Backdoor> backdoorSupplier, final Supplier<JIRAEnvironmentData> environmentDataSupplier, final Supplier<WebTester> webTesterSupplier) {
        this.backdoorSupplier = backdoorSupplier;
        this.environmentDataSupplier = environmentDataSupplier;
        this.webTesterSupplier = webTesterSupplier;
    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return isSkipSetupAnnotationPresent(description) ? base : new StatementDecorator(base, this::ensureJiraIsSetup);
    }

    private boolean isSkipSetupAnnotationPresent(final Description description) {
        return new TestAnnotationsExtractor(description).findAnnotationFor(SkipSetup.class).isPresent();
    }

    public void ensureJiraIsSetup() {
        final Backdoor backdoor = backdoorSupplier.get();
        final boolean jiraIsSetup = backdoor.dataImport().isSetUp();
        if (!jiraIsSetup) {
            new JiraSetupInstanceHelper(webTesterSupplier.get(), environmentDataSupplier.get()).ensureJIRAIsReadyToGo();
        }
    }
}
