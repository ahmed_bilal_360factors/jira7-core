package com.atlassian.jira.webtests.ztests.bundledplugins2.gadget;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.GadgetSpecClient;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Simple smoke test to test that the road map gadget spec xml can be retrieved.
 */
@WebTest({Category.FUNC_TEST, Category.GADGETS})
public class TestRoadMapGadget extends BaseJiraFuncTest {
    private static final String PLUGIN_KEY = "com.atlassian.jira.gadgets";
    private static final String MODULE_KEY = "road-map-gadget";
    private static final String LOCATION = "/gadgets/roadmap-gadget.xml";

    private static final String DEFAULT_LOC = PLUGIN_KEY + ":" + MODULE_KEY + LOCATION;

    private GadgetSpecClient gadgetSpecClient;

    @Before
    public void setUpClient() {
        gadgetSpecClient = new GadgetSpecClient(getEnvironmentData());
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testRequestToRoadMapGadgetSpecReturnsSuccess() {
        assertThat(gadgetSpecClient.getGadgetSpecXML(DEFAULT_LOC).statusCode, equalTo(200));
    }

    @Test
    public void testRequestToRoadMapGadgetSpecReturnsSuccessAnonymously() {
        assertThat(gadgetSpecClient.getGadgetSpecXML(DEFAULT_LOC).statusCode, equalTo(200));
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testRoadMapGadgetIsAddable() {
        Collection<String> addableGadgets = gadgetSpecClient.getAddableGadgets();
        assertThat(addableGadgets, hasItem(DEFAULT_LOC));
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testRoadMapGadgetIsAvailableInFeed() {
        String feedResourceContent = gadgetSpecClient.getFeedResourceContent();
        assertThat(feedResourceContent, containsString(DEFAULT_LOC));
    }
}
