package com.atlassian.jira.webtests.ztests.upgrade;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * tests that the system property upgrade.reindex.allowed=false is set that no reindex takes place and that
 * a warning message is displayed and cleared  Unfortunately we can't test the in place upgrade - you will have
 * to do this testing manually for now
 *
 * @since v6.1
 */
@WebTest({Category.FUNC_TEST, Category.INFRASTRUCTURE, Category.SLOW_IMPORT})
@LoginAs(user = ADMIN_USERNAME)
public class TestUpgradeTasksReindexSuppression extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        this.backdoor.systemProperties().setProperty("upgrade.reindex.allowed", "false");
    }


    @Test
    public void testReindexClearsMessage() {
        administration.restoreData("TestUpgradeTasks.xml");
        navigation.gotoAdmin();
        assertNoUpgradeReindexMessage();

    }

    private void assertNoUpgradeReindexMessage() {
        assertions.getTextAssertions().assertTextNotPresent(new CssLocator(tester, ".aui-message.info"),
                "While upgrading JIRA");
    }
}
