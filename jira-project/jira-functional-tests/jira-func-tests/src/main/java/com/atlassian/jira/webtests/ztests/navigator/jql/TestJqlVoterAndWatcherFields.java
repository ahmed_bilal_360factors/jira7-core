package com.atlassian.jira.webtests.ztests.navigator.jql;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.JQL})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestJqlVoterAndWatcherFields.xml")
public class TestJqlVoterAndWatcherFields extends BaseJiraFuncTest {

    @Inject
    private IssueTableAssertions issueTableAssertions;

    @Before
    public void setUp() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, BOB_USERNAME);
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, FRED_USERNAME);
    }

    @Test
    public void testVoterField() {
        issueTableAssertions.assertSearchWithResultsForUser(BOB_USERNAME, "voter = currentUser()", "HSP-4", "HSP-3");
        issueTableAssertions.assertSearchWithResultsForUser(BOB_USERNAME, "voter = bob", "HSP-4", "HSP-3");
        // check that it ignores case
        issueTableAssertions.assertSearchWithResultsForUser(BOB_USERNAME, "voter = BoB", "HSP-4", "HSP-3");

        issueTableAssertions.assertSearchWithResultsForUser(BOB_USERNAME, "voter = fred");
        issueTableAssertions.assertSearchWithResultsForUser(BOB_USERNAME, "voter = FreD");

        // sees own issue despite not being able to see other votes
        issueTableAssertions.assertSearchWithResultsForUser(FRED_USERNAME, "voter = currentUser()", "HSP-4", "HSP-3");
        issueTableAssertions.assertSearchWithResultsForUser(FRED_USERNAME, "voter = fred", "HSP-4", "HSP-3");
        issueTableAssertions.assertSearchWithResultsForUser(FRED_USERNAME, "voter = FreD", "HSP-4", "HSP-3");
        issueTableAssertions.assertSearchWithResultsForUser(FRED_USERNAME, "voter = bob", "HSP-4");
        // check that it ignores case
        issueTableAssertions.assertSearchWithResultsForUser(FRED_USERNAME, "voter = BoB", "HSP-4");

        // reassign HSP-2 to fred
        issueTableAssertions.assertSearchWithResultsForUser(ADMIN_USERNAME, "voter = fred", "HSP-4", "HSP-3");
        issueTableAssertions.assertSearchWithResultsForUser(ADMIN_USERNAME, "voter = FreD", "HSP-4", "HSP-3");
        issueTableAssertions.assertSearchWithResultsForUser(ADMIN_USERNAME, "voter = bob", "HSP-4", "HSP-3");
        issueTableAssertions.assertSearchWithResultsForUser(ADMIN_USERNAME, "voter = BoB", "HSP-4", "HSP-3");

        issueTableAssertions.assertSearchWithWarningForUser(ADMIN_USERNAME, "voter = dingbat", "The value 'dingbat' does not exist for the field 'voter'.");
    }

    @Test
    public void testWatcherField() {
        issueTableAssertions.assertSearchWithResultsForUser(BOB_USERNAME, "watcher = currentUser()", "HSP-3");
        issueTableAssertions.assertSearchWithResultsForUser(BOB_USERNAME, "watcher = bob", "HSP-3");
        // check that it ignores case
        issueTableAssertions.assertSearchWithResultsForUser(BOB_USERNAME, "watcher = BoB", "HSP-3");

        issueTableAssertions.assertSearchWithResultsForUser(BOB_USERNAME, "watcher = fred");
        issueTableAssertions.assertSearchWithResultsForUser(BOB_USERNAME, "watcher = FreD");

        // sees own issue despite not being able to see other votes
        issueTableAssertions.assertSearchWithResultsForUser(FRED_USERNAME, "watcher = currentUser()", "HSP-4", "HSP-3");
        issueTableAssertions.assertSearchWithResultsForUser(FRED_USERNAME, "watcher = fred", "HSP-4", "HSP-3");
        issueTableAssertions.assertSearchWithResultsForUser(FRED_USERNAME, "watcher = FreD", "HSP-4", "HSP-3");
        issueTableAssertions.assertSearchWithResultsForUser(FRED_USERNAME, "watcher = bob");
        // check that it ignores case
        issueTableAssertions.assertSearchWithResultsForUser(FRED_USERNAME, "watcher = BoB");

        // reassign HSP-2 to fred
        issueTableAssertions.assertSearchWithResultsForUser(ADMIN_USERNAME, "watcher = fred", "HSP-4", "HSP-3");
        issueTableAssertions.assertSearchWithResultsForUser(ADMIN_USERNAME, "watcher = FreD", "HSP-4", "HSP-3");
        issueTableAssertions.assertSearchWithResultsForUser(ADMIN_USERNAME, "watcher = bob", "HSP-3");
        issueTableAssertions.assertSearchWithResultsForUser(ADMIN_USERNAME, "watcher = BoB", "HSP-3");

        issueTableAssertions.assertSearchWithWarningForUser(ADMIN_USERNAME, "watcher = dingbat", "The value 'dingbat' does not exist for the field 'watcher'.");
    }
}
