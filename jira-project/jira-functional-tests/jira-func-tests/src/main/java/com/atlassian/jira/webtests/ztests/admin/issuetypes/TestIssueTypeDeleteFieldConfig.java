package com.atlassian.jira.webtests.ztests.admin.issuetypes;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.IssueTypeControl;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * tests fix for JRA-14009
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.BROWSING, Category.ISSUE_TYPES})
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueTypeDeleteFieldConfig extends BaseJiraFuncTest {
    /**
     * we want to delete an IssueType that has some obsolete field configs (dud data) associated with it and make sure nothing bad happens
     */
    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testIssueTypeDeleteWithDanglingFieldConfigs() {
        administration.restoreData("TestDeleteIssueTypeWithDanglingFieldConfig.xml");
        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_TYPES);

        tester.assertTextPresent("Issue Type To Delete");
        tester.gotoPage("/secure/admin/DeleteIssueType!default.jspa?id=5");
        tester.submit("Delete");
        textAssertions.assertTextPresent(locator.css("h2"), "Issue types");
        tester.assertTextNotPresent("Issue Type To Delete");
    }

    @Test
    public void testIssueTypeDeleteWithExistingFieldConfigs() {
        administration.restoreBlankInstance();

        final IssueTypeControl.IssueType type = backdoor.issueType().createIssueType("Defunkt Issue Type");

        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);

        tester.clickLink("add_custom_fields");
        tester.checkCheckbox("fieldType", "com.atlassian.jira.plugin.system.customfieldtypes:textfield");
        tester.submit("nextBtn");
        tester.setFormElement("fieldName", "Silly Field");
        // Select 'Defunkt Issue Type' from select box 'issuetypes'.
        tester.selectOption("issuetypes", "Defunkt Issue Type");
        tester.submit("nextBtn");
        tester.submit("Update");
        // Click Link 'Issue Types' (id='issue_types').
        tester.gotoPage("/secure/admin/DeleteIssueType!default.jspa?id=" + type.getId());
        tester.submit("Delete");
        textAssertions.assertTextPresent(locator.css("h2"), "Issue types");
        tester.assertTextNotPresent("Defunkt Issue Type");
    }

    @Test
    public void testIssueTypeDeleteWithDeletedFieldConfigs() {
        administration.restoreBlankInstance();

        final IssueTypeControl.IssueType type = backdoor.issueType().createIssueType("Defunkt Issue Type");

        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);

        tester.clickLink("add_custom_fields");
        tester.checkCheckbox("fieldType", "com.atlassian.jira.plugin.system.customfieldtypes:textfield");
        tester.submit("nextBtn");
        tester.setFormElement("fieldName", "Silly Field");
        // Select 'Defunkt Issue Type' from select box 'issuetypes'.
        tester.selectOption("issuetypes", "Defunkt Issue Type");
        tester.submit("nextBtn");
        tester.submit("Update");

        tester.clickLink("del_customfield_10000");
        tester.submit("Delete");

        // Click Link 'Issue Types' (id='issue_types').
        tester.gotoPage("/secure/admin/DeleteIssueType!default.jspa?id=" + type.getId());
        tester.submit("Delete");
        textAssertions.assertTextPresent(locator.css("h2"), "Issue types");
        tester.assertTextNotPresent("Defunkt Issue Type");
    }
}
