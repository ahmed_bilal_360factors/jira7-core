package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.atlassian.jira.webtests.ztests.issue.TestLabels;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A test suite for all JQL related func tests.
 *
 * @since v4.0
 */
public class FuncTestSuiteJql {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.navigator.jql", true))
                .add(TestLabels.class)
                .build();
    }
}
