package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.GroupAndUserPickerClient;
import com.atlassian.jira.testkit.client.restclient.GroupAndUserSuggestions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

@WebTest({Category.FUNC_TEST, Category.REST})
@Restore("TestGroupPickerResource.xml")
public class TestGroupAndUserPickerResource extends BaseJiraRestTest {
    private GroupAndUserPickerClient groupAndUserPickerClient;

    @Before
    public void setUpTest() {
        groupAndUserPickerClient = new GroupAndUserPickerClient(environmentData);
    }

    @Test
    public void testMatches() {
        GroupAndUserSuggestions suggestions = groupAndUserPickerClient.get("z");
        Assert.assertEquals(20, suggestions.groups.groups.size());
        Assert.assertEquals("Showing 20 of 21 matching groups", suggestions.groups.header);
        Assert.assertEquals(0, suggestions.users.users.size());
        Assert.assertEquals("Showing 0 of 0 matching users", suggestions.users.header);
        suggestions = groupAndUserPickerClient.get("a");
        Assert.assertEquals(3, suggestions.groups.groups.size());
        Assert.assertEquals("Showing 3 of 3 matching groups", suggestions.groups.header);
        Assert.assertEquals(1, suggestions.users.users.size());
        Assert.assertEquals("Showing 1 of 1 matching users", suggestions.users.header);
        Assert.assertEquals("admin", suggestions.users.users.get(0).name);
        Assert.assertEquals("admin", suggestions.users.users.get(0).key);
    }
}
