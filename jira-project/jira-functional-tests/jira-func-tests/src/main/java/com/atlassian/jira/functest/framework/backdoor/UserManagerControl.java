package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.sun.jersey.api.client.GenericType;

import java.util.List;

/**
 * @since v7.0
 */
public class UserManagerControl extends BackdoorControl<UserManagerControl> {
    public UserManagerControl(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public List<String> userKeysByFullName(String fullName) {
        return createResource().path("userManager/keysByFullName")
                .queryParam("fullName", fullName)
                .get(new GenericType<List<String>>() {
                });
    }

    public List<String> userKeysByEmail(String email) {
        return createResource().path("userManager/keysByEmail")
                .queryParam("email", email)
                .get(new GenericType<List<String>>() {
                });
    }

    public List<String> userNamesStreamed() {
        return createResource().path("userManager/userNamesStreamed")
                .get(new GenericType<List<String>>() {
                });
    }

    public void setActive(final String username, final boolean active) {
        createResource().path("userManager/active")
                .queryParam("username", username)
                .queryParam("active", Boolean.toString(active))
                .put();
    }
}
