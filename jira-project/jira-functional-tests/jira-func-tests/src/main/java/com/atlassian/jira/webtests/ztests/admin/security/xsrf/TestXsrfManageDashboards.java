package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Dashboard;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.dashboard.DashboardPageInfo;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.sharing.TestSharingPermissionUtils.createPublicPermissions;

/**
 * Responsible for holding tests which verify that the Dashboards actions are not susceptible to
 * XSRF attacks.
 *
 * @since v6.1.1
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestXsrfManageDashboards extends BaseJiraFuncTest {
    @Inject
    private Form form;

    private Dashboard addSharedPublicDashboard() {
        return navigation.dashboard().addPage(Data.SHARED_PUBLIC_DASHBOARD, null);
    }

    @Test
    public void testDasboardManagementOperations() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck(
                        "Adding Dashboard",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                navigation.dashboard().navigateToMy();
                                tester.clickLink("create_page");
                                tester.setWorkingForm("add-dashboard");
                            }
                        },
                        new XsrfCheck.FormSubmissionWithId("add-dashboard-submit")
                ),
                new XsrfCheck(
                        "Restore Default Dashboard",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                navigation.dashboard().navigateToMy();
                                tester.clickLink("restore_defaults");
                                tester.setWorkingForm("jiraform");
                            }
                        },
                        new XsrfCheck.FormSubmissionWithId("restore_submit")
                )
        ).run(getTester(), navigation, form);
    }

    @Test
    public void testDasboardOperations() throws Exception {
        addSharedPublicDashboard();
        new XsrfTestSuite(
                new XsrfCheck(
                        "Editing Dashboard",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                navigation.dashboard().navigateToMy();
                                tester.clickLink("edit_0");
                                tester.setWorkingForm("edit-dashboard");
                            }
                        },
                        new XsrfCheck.FormSubmission("update_submit")
                ),
                new XsrfCheck(
                        "Copy Dashboard",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                navigation.dashboard().navigateToMy();
                                tester.clickLink("clone_0");
                                tester.setWorkingForm("add-dashboard");
                                tester.setFormElement("portalPageName", "New Name");
                            }
                        },
                        new XsrfCheck.FormSubmissionWithId("add-dashboard-submit")
                ),
                new XsrfCheck(
                        "Delete Dashboard",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                navigation.dashboard().navigateToMy();
                                tester.clickLink("delete_0");
                                tester.setWorkingForm("delete-portal-page");
                            }
                        },
                        new XsrfCheck.FormSubmissionWithId("delete-portal-page-submit")
                )
        ).run(getTester(), navigation, form);
    }

    private static class Data {
        private static final DashboardPageInfo SHARED_PUBLIC_DASHBOARD =
                new DashboardPageInfo
                        (
                                10014, "Public - Owner: Admin", null, true, createPublicPermissions(), ADMIN_USERNAME,
                                1, DashboardPageInfo.Operation.ALL
                        );
    }
}
