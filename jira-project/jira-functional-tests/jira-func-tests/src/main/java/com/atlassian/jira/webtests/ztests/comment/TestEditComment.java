package com.atlassian.jira.webtests.ztests.comment;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.util.Collections;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_ADMIN_GROUP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_USERS_GROUP;
import static com.atlassian.jira.permission.ProjectPermissions.ADD_COMMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.EDIT_ALL_COMMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.EDIT_OWN_COMMENTS;

@WebTest({Category.FUNC_TEST, Category.COMMENTS})
@LoginAs(user = ADMIN_USERNAME)
public class TestEditComment extends BaseJiraFuncTest {
    private static final String ISSUE_KEY = "HSP-1";

    private static final String COMMENT_ID_ADMIN = "10031";
    private static final String COMMENT_ID_FRED = "10040";
    private static final String COMMENT_ID_ANON = "10041";
    private static final String OLD_COMMENT_ANON = "Just a random comment by noname.";
    private static final String OLD_COMMENT_ADMIN = "I&#39;m a hero!";
    private static final String OLD_COMMENT_FRED = "Australia is an island continent.";
    private static final String NEW_COMMENT_ANON = "Firefox rocks!";
    private static final String NEW_COMMENT_ADMIN = "Thunderbird is cool!";
    private static final String NEW_COMMENT_FRED = "Linux just works!";

    private static final String SAVE_BUTTON_NAME = "Save";

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        administration.restoreData("TestEditComment.xml");
    }

    @Test
    public void testSimpleCommentEdit() {
        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("edit_comment_10031");
        tester.setWorkingForm("comment-edit");
        tester.assertTextPresent(OLD_COMMENT_ADMIN);
        tester.setFormElement("comment", "new func test value");
        tester.submit(SAVE_BUTTON_NAME);

        // Make sure we got back to the view issue page
        tester.assertTextPresent("Edit comment issue summary for our test");

        // Make sure the updated comment is there
        tester.assertTextPresent("new func test value");
    }

    @Test
    public void testSimpleErrorNoBodyProvided() {
        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("edit_comment_10031");
        tester.setWorkingForm("comment-edit");
        tester.assertTextPresent(OLD_COMMENT_ADMIN);
        tester.setFormElement("comment", "");
        tester.submit(SAVE_BUTTON_NAME);

        // Make sure we do not go back to the view issue page
        tester.assertFormElementPresent("comment");

        // Make sure the updated comment is there
        tester.assertTextPresent("Comment body can not be empty!");

        // Make sure that user can re-submit the form - Save button is present
        tester.assertFormElementPresent(SAVE_BUTTON_NAME);
    }

    @Test
    public void testCommentEditPermissionGroup() throws SAXException {
        final String COMMENT1 = "This comment is updated";

        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("edit_comment_10031");
        tester.setWorkingForm("comment-edit");
        tester.assertTextPresent(OLD_COMMENT_ADMIN);
        tester.setFormElement("comment", COMMENT1);
        tester.selectOption("commentLevel", "jira-developers");
        tester.submit(SAVE_BUTTON_NAME);

        // Make sure we got back to the view issue page
        tester.assertTextPresent("Edit comment issue summary for our test");

        // Make sure the updated comment is there
        tester.assertTextPresent(COMMENT1);
        tester.assertTextPresent("Restricted to <span class=redText>jira-developers</span>");

        // Verify that the correct option is selected by default
        tester.clickLink("edit_comment_10031");
        tester.assertOptionEquals("commentLevel", "jira-developers");
    }

    @Test
    public void testCommentEditPermissionRole() throws SAXException {
        final String COMMENT1 = "This comment is updated";

        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("edit_comment_10031");
        tester.setWorkingForm("comment-edit");
        tester.assertTextPresent(OLD_COMMENT_ADMIN);
        tester.setFormElement("comment", COMMENT1);
        tester.selectOption("commentLevel", "Developers");
        tester.submit(SAVE_BUTTON_NAME);

        // Make sure we got back to the view issue page
        tester.assertTextPresent("Edit comment issue summary for our test");

        // Make sure the updated comment is there
        tester.assertTextPresent(COMMENT1);
        tester.assertTextPresent("Restricted to <span class=redText>Developers</span>");

        // Verify that the correct option is selected by default
        tester.clickLink("edit_comment_10031");
        tester.assertOptionEquals("commentLevel", "Developers");
    }

    @Test
    public void testCommentEditRemovePermission() throws SAXException {
        final String COMMENT1 = "This comment is updated";

        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("edit_comment_10031");
        tester.setWorkingForm("comment-edit");
        tester.assertTextPresent(OLD_COMMENT_ADMIN);
        tester.setFormElement("comment", COMMENT1);
        tester.selectOption("commentLevel", "All Users");
        tester.submit(SAVE_BUTTON_NAME);

        // Make sure we got back to the view issue page
        tester.assertTextPresent("Edit comment issue summary for our test");

        // Make sure the updated comment is there
        tester.assertTextPresent(COMMENT1);
        tester.assertTextNotPresent("Restricted to <span class=redText>Administrators</span>");
    }

    @Test
    public void testCommentEditCommentNotEditedBefore() throws SAXException {
        navigation.issue().gotoIssue("HSP-1");

        tester.clickLink("edit_comment_10031");
        tester.assertTextPresent(OLD_COMMENT_ADMIN);

        // Make sure that the edit author and date are not shown since this comment has not been updated yet.

        tester.assertTextNotPresent("Edited by");
        tester.assertTextNotPresent("Edited on");
    }

    @Test
    public void testCommentEditCommentIsSearchable() throws SAXException {
        navigation.issue().gotoIssue("HSP-1");

        tester.clickLink("edit_comment_10031");
        tester.setWorkingForm("comment-edit");
        tester.assertTextPresent(OLD_COMMENT_ADMIN);

        final String STRANGE_COMMENT = "STRANGE COMMENT";
        tester.setFormElement("comment", STRANGE_COMMENT);
        tester.selectOption("commentLevel", "All Users");
        tester.submit(SAVE_BUTTON_NAME);

        navigation.issueNavigator().createSearch("comment ~ \"STRANGE COMMENT\"");
        tester.assertTextPresent("Edit comment issue summary for our test");
    }

    /**
     * Test that you can create a comment when not logged in
     */
    @Test
    public void testAnonymousCanCreateComment() {
        addCreateCommentPermissionToAnonymous();
        navigation.logout();
        tester.gotoPage("/secure/Dashboard.jspa");
        navigation.issue().gotoIssue(ISSUE_KEY);
        final String newComment = "Can I add a comment?";
        tester.setFormElement("comment", newComment);
        tester.submit();
        tester.assertTextPresent(newComment);
    }

    @Test
    public void testAnonymousCanSeeAllComments() {
        navigation.logout();
        tester.gotoPage("/secure/Dashboard.jspa");
        navigation.issue().gotoIssue(ISSUE_KEY);
        tester.assertTextPresent(OLD_COMMENT_ANON);
        tester.assertTextPresent(OLD_COMMENT_ADMIN);
        tester.assertTextPresent(OLD_COMMENT_FRED);
    }

    @Test
    public void testNoEditPermissions() {
        // By default, in the imported data, we have the EDIT_ALL permission granted to ANYONE and jira-administrators
        removeEditAllCommentsPermission(JIRA_ADMIN_GROUP);
        removeAnonymousEditAllCommentsPermission();

        // By default, in the imported data, we have the EDIT_OWN permission granted to ANYONE and jira-users
        removeEditOwnCommentsPermission(JIRA_USERS_GROUP);
        removeAnonymousEditOwnCommentsPermission();

        assertAdminCanEditAnonymousComment(false);
        assertAdminCanEditAdminsComment(false);
        assertAdminCanEditFredsComment(false);
        navigation.logout();
        assertAnonymousCanEditAnonymousComment(false);
        assertAnonymousCanEditAdminsComment(false);
        assertAnonymousCanEditFredsComment(false);
    }

    @Test
    public void testEditOwnPermissions() {
        // By default, in the imported data, we have the EDIT_ALL permission granted to jira-administrators
        removeEditAllCommentsPermission(JIRA_ADMIN_GROUP);
        removeAnonymousEditAllCommentsPermission();

        // By default, in the imported data, we have the EDIT_OWN permission granted to ANYONE and jira-users
        removeEditOwnCommentsPermission(JIRA_USERS_GROUP);

        assertAdminCanEditAnonymousComment(false);
        assertAdminCanEditAdminsComment(true);
        assertAdminCanEditFredsComment(false);
        navigation.logout();
        // JRA-16138 Now Anonymous Cannot Edit own Comment, as we don't really know who "owns" it.
        assertAnonymousCanEditAnonymousComment(false);
        assertAnonymousCanEditAdminsComment(false);
        assertAnonymousCanEditFredsComment(false);
    }

    @Test
    public void testEditAllPermissions() {
        // By default, in the imported data, we have the EDIT_ALL permission granted to jira-administrators
        removeEditAllCommentsPermission(JIRA_ADMIN_GROUP);
        // By default, in the imported data, we have the EDIT_OWN permission granted to jira-users
        removeEditOwnCommentsPermission(JIRA_USERS_GROUP);
        removeAnonymousEditOwnCommentsPermission();

        assertAdminCanEditAnonymousComment(true);
        assertAdminCanEditAdminsComment(true);
        assertAdminCanEditFredsComment(true);
        navigation.logout();
        assertAnonymousCanEditAnonymousComment(true);
        assertAnonymousCanEditAdminsComment(true);
        assertAnonymousCanEditFredsComment(true);
    }

    @Test
    public void testErrorCases() {
        // issue does not exist
        tester.gotoPage("/secure/EditComment!default.jspa?id=123&commentId=10041");
        tester.assertTextPresent("The issue no longer exists.");
        tester.assertFormElementNotPresent(SAVE_BUTTON_NAME);

        // invalid comment id, issue exists
        final String INVALID_COMMENT_ID = "123";
        tester.gotoPage("/secure/EditComment!default.jspa?id=10000&commentId=" + INVALID_COMMENT_ID);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Error", "You do not have the permission for this comment."});

        // test no permission
        removeEditAllCommentsPermission(JIRA_ADMIN_GROUP);
        removeAnonymousEditAllCommentsPermission();
        removeEditOwnCommentsPermission(JIRA_USERS_GROUP);
        removeAnonymousEditOwnCommentsPermission();
        tester.gotoPage("/secure/EditComment!default.jspa?id=10000&commentId=10041");

        tester.assertTitleEquals("Error - jWebTest JIRA installation");
        textAssertions.assertTextPresent(new CssLocator(tester, "#content .error"), "You do not have the permission for this comment.");
    }

    @Test
    public void testEditCommentWithTooLongBody() {
        backdoor.advancedSettings().setTextFieldCharacterLengthLimit(10);
        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("edit_comment_10031");
        tester.setWorkingForm("comment-edit");
        tester.assertTextPresent(OLD_COMMENT_ADMIN);
        tester.setFormElement("comment", "too long comment");
        tester.submit(SAVE_BUTTON_NAME);

        tester.assertTextPresent("The entered text is too long. It exceeds the allowed limit of 10 characters.");
    }

    @Test
    public void testEditCommentWithTextLengthLimitOn() {
        backdoor.advancedSettings().setTextFieldCharacterLengthLimit(10);
        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("edit_comment_10031");
        tester.setWorkingForm("comment-edit");
        tester.assertTextPresent(OLD_COMMENT_ADMIN);
        final String correctComment = "AllGood";
        tester.setFormElement("comment", correctComment);
        tester.submit(SAVE_BUTTON_NAME);

        tester.assertTextPresent("Edit comment issue summary for our test");
        tester.assertTextPresent(correctComment);
        assertions.comments(Collections.singletonList(correctComment)).areVisibleTo("admin", "HSP-1");
    }


// --------------------------------------------------------------------------------------------------------------------

    private void assertAdminCanEditAnonymousComment(boolean expected) {
        verifyUserCanEditComment(expected, COMMENT_ID_ANON, NEW_COMMENT_ANON, true);
    }

    private void assertAdminCanEditAdminsComment(boolean expected) {
        verifyUserCanEditComment(expected, COMMENT_ID_ADMIN, NEW_COMMENT_ADMIN, true);
    }

    private void assertAdminCanEditFredsComment(boolean expected) {
        verifyUserCanEditComment(expected, COMMENT_ID_FRED, NEW_COMMENT_FRED, true);
    }

    private void assertAnonymousCanEditAnonymousComment(boolean expected) {
        verifyUserCanEditComment(expected, COMMENT_ID_ANON, NEW_COMMENT_ANON, false);
    }

    private void assertAnonymousCanEditAdminsComment(boolean expected) {
        verifyUserCanEditComment(expected, COMMENT_ID_ADMIN, NEW_COMMENT_ADMIN, false);
    }

    private void assertAnonymousCanEditFredsComment(boolean expected) {
        verifyUserCanEditComment(expected, COMMENT_ID_FRED, NEW_COMMENT_FRED, false);
    }

    private void verifyUserCanEditComment(boolean expected, String commentId, String commentBody, boolean userExists) {
        tester.gotoPage("/secure/Dashboard.jspa");
        navigation.issue().gotoIssue(ISSUE_KEY);
        if (expected) {
            verifyEditLinkExistsAndEdit(commentId, commentBody, userExists);
        } else {
            verifyEditLinkDoesNotExist(commentId);
            verifyEditCommentPagePrintsError(commentId);
            verifyCommentUpdatePrintsError(commentId);
        }
    }

    private void verifyEditLinkExistsAndEdit(String commentId, String newComment, boolean userExists) {
        final String commentLink = "edit_comment_" + commentId;

        // Verify that the link is present since the user can make the comment
        tester.assertLinkPresent(commentLink);

        // lets make sure that we can edit the comment
        tester.clickLink(commentLink);
        tester.setWorkingForm("comment-edit");
        tester.setFormElement("comment", newComment);
        if (userExists) {
            // anonymous user cannot set comment visibility level
            tester.selectOption("commentLevel", "All Users");
        }
        tester.submit(SAVE_BUTTON_NAME);

        tester.assertTextPresent("Edit comment issue summary for our test");

        tester.assertTextPresent(newComment);
    }

    private void verifyEditLinkDoesNotExist(String commentId) {
        navigation.issue().gotoIssue(ISSUE_KEY);
        final String commentLink = "edit_comment_" + commentId;
        tester.assertLinkNotPresent(commentLink);
    }

    private void verifyEditCommentPagePrintsError(String commentId) {
        // Now fake the go to edit comment page with all the params and make sure we get an error
        tester.gotoPage(page.addXsrfToken("/secure/EditComment!default.jspa?id=10000&commentId=" + commentId));
        tester.assertTitleEquals("Error - jWebTest JIRA installation");
        textAssertions.assertTextPresent(new CssLocator(tester, "#content .error"), "You do not have the permission for this comment.");
    }

    private void verifyCommentUpdatePrintsError(String commentId) {
        // Now fake the submit with all the params and make sure we get an error
        tester.gotoPage(page.addXsrfToken("/secure/EditComment.jspa?id=10000&commentId=" + commentId + "&comment=WooHoo"));
        tester.assertTitleEquals("Error - jWebTest JIRA installation");
        textAssertions.assertTextPresent(new CssLocator(tester, "#content .error"), "You do not have the permission for this comment.");
    }

    private void removeEditOwnCommentsPermission(String group) {
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, EDIT_OWN_COMMENTS, group);
    }

    private void removeAnonymousEditOwnCommentsPermission() {
        backdoor.permissionSchemes().removeEveryonePermission(DEFAULT_PERM_SCHEME_ID, EDIT_OWN_COMMENTS);

    }

    private void removeEditAllCommentsPermission(String group) {
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, EDIT_ALL_COMMENTS, group);
    }

    private void removeAnonymousEditAllCommentsPermission() {
        backdoor.permissionSchemes().removeEveryonePermission(DEFAULT_PERM_SCHEME_ID, EDIT_ALL_COMMENTS);
    }

    private void addCreateCommentPermissionToAnonymous() {
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_PERM_SCHEME_ID, ADD_COMMENTS);
    }
}
