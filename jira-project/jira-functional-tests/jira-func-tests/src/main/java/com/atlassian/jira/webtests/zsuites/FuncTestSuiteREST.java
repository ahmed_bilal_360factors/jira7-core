package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * @since v4.2
 */
public class FuncTestSuiteREST {
    public static List<Class<?>> bundledPluginsTests() {
        return ImmutableList.<Class<?>>builder()
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.bundledplugins2.rest", true))
                .build();
    }
}
