package com.atlassian.jira.functest.framework.admin;

import com.google.inject.ImplementedBy;

import java.io.File;

/**
 * Framework for executing project imports.
 *
 * @since v4.1
 */
@ImplementedBy(ProjectImportImpl.class)
public interface ProjectImport {
    File doImportToSummary(String backupFileName, String currentSystemXML, final String attachmentPath);
}
