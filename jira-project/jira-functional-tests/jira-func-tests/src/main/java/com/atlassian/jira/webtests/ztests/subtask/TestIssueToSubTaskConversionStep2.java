package com.atlassian.jira.webtests.ztests.subtask;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.locator.Locator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CHANGE_HISTORY;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.SUB_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueToSubTaskConversionStep2 extends BaseJiraFuncTest {
    private static final String ISSUE_TO_CONVERT_ID = "10050";
    private static final String ISSUE_TO_CONVERT_KEY = "MKY-4";
    private static final String ISSUE_TO_CONVERT_INVALID_STATUS_ID = "10051";
    private static final String ISSUE_TO_CONVERT_INVALID_STATUS_KEY = "MKY-5";
    private static final String PARENT_ISSUE = "MKY-3";
    private static final String SUBTASK_TYPE = "Sub-task";
    private static final String SUBTASK_TYPE_ID = "5";
    private static final String SUBTASK_TYPE_3 = "Sub-task 3";
    private static final String SUBTASK_TYPE_3_ID = "7";
    private static final String VALID_STATUS = "1";
    private static final String INVALID_STATUS = "3";
    private static final String NON_EXISTANT_STATUS = "932";

    @Inject
    SubTaskAssertions subTaskAssertions;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        administration.restoreData("TestIssueToSubTaskConversion.xml");
    }

    @Test
    public void testSameWorkflow() {
        gotoConvertIssueStep2(ISSUE_TO_CONVERT_ID, PARENT_ISSUE, SUBTASK_TYPE_ID);
        tester.assertTextPresent("Step 2 is not required.");

        // check pane
        assertThirdStepPaneWithout2nd(ISSUE_TO_CONVERT_KEY, PARENT_ISSUE, SUBTASK_TYPE, "Open");


        // Make sure Status is not displayed for update or on confirmation

        //assert other status not shown on screen
        final String convertIssueElementContent = tester.getDialog().getElement("stepped-process").getTextContent();
        assertThat(convertIssueElementContent, Matchers.not(Matchers.containsString("Closed")));
        assertThat(convertIssueElementContent, Matchers.not(Matchers.containsString("Reopened")));
        assertThat(convertIssueElementContent, Matchers.not(Matchers.containsString("Resolved")));

        tester.submit("Next >>");


        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_TO_CONVERT_KEY, 4);
        tester.assertTextNotInTable("convert_confirm_table", "Status");

        tester.submit("Finish");

        //check issue type
        Locator locator = new IdLocator(tester, "type-val");
        textAssertions.assertTextPresent(locator, SUBTASK_TYPE);
        // check that issue has correct status
        locator = new IdLocator(tester, "status-val");
        textAssertions.assertTextPresent(locator, "Open");

        // Check change history
        tester.clickLinkWithText(CHANGE_HISTORY);
        textAssertions.assertTextPresentNumOccurences(tester.getDialog().getElement("jira").getTextContent(), "Status", 1);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{CHANGE_HISTORY, "Issue Type", "Bug", SUBTASK_TYPE});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{CHANGE_HISTORY, "Parent", "MKY-3"});
    }

    @Test
    public void testDiffWorkflowValidStatus() {
        gotoConvertIssueStep2(ISSUE_TO_CONVERT_ID, PARENT_ISSUE, SUBTASK_TYPE_3_ID);
        tester.assertTextPresent("Step 2 is not required.");

        // check pane
        assertThirdStepPaneWithout2nd(ISSUE_TO_CONVERT_KEY, PARENT_ISSUE, SUBTASK_TYPE_3, "Open");

        // Make sure Status is not displayed for update but is shown on confirm

        //assert other status not shown on screen
        final String convertIssueElementContent = tester.getDialog().getElement("stepped-process").getTextContent();
        assertThat(convertIssueElementContent, Matchers.not(Matchers.containsString("Closed")));
        assertThat(convertIssueElementContent, Matchers.not(Matchers.containsString("Reopened")));
        assertThat(convertIssueElementContent, Matchers.not(Matchers.containsString("Resolved")));

        tester.submit("Next >>");

        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_TO_CONVERT_KEY, 4);
        tester.assertTextInTable("convert_confirm_table", "Status");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Status", "(Workflow)", "Open", "status-inactive", "(jira)", "Open", "status-active", "(Subtask Workflow)"});

        tester.submit("Finish");

        //check issue type
        Locator locator = new IdLocator(tester, "type-val");
        textAssertions.assertTextPresent(locator, SUBTASK_TYPE_3);
        // check that issue has correct status
        locator = new IdLocator(tester, "status-val");
        textAssertions.assertTextPresent(locator, "Open");


        // Check change history
        tester.clickLinkWithText(CHANGE_HISTORY);
        textAssertions.assertTextPresentNumOccurences(tester.getDialog().getElement("jira").getTextContent(), "Status", 1);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{CHANGE_HISTORY, "Workflow", "jira", "Subtask Workflow"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{CHANGE_HISTORY, "Issue Type", "Bug", SUBTASK_TYPE_3});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{CHANGE_HISTORY, "Parent", "MKY-3"});
    }

    @Test
    public void testDiffWorkflowInvalidStatus() {
        gotoConvertIssueStep2(ISSUE_TO_CONVERT_INVALID_STATUS_ID, PARENT_ISSUE, SUBTASK_TYPE_3_ID);
        tester.assertTextPresent("Step 2 of 4");

        // test that all shown statuses are valid
        tester.assertOptionsEqual("targetStatusId", new String[]{"Open", "Resolved", "Reopened", "Closed"});
        tester.assertRadioOptionValueNotPresent("targetStatusId", "In Progress");

        //Check from and target workflow
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Select New Status", "In Progress", "Workflow", "jira", "Workflow", "Subtask Workflow"});

        // check pane
        assertSecondStepPane(ISSUE_TO_CONVERT_INVALID_STATUS_KEY, PARENT_ISSUE, SUBTASK_TYPE_3);

        tester.setFormElement("targetStatusId", "1");

        tester.submit("Next >>");

        //assert other status not shown on screen
        final String convertIssueElementContent = tester.getDialog().getElement("stepped-process").getTextContent();
        assertThat(convertIssueElementContent, Matchers.not(Matchers.containsString("Closed")));
        assertThat(convertIssueElementContent, Matchers.not(Matchers.containsString("Reopened")));
        assertThat(convertIssueElementContent, Matchers.not(Matchers.containsString("Resolved")));

        tester.submit("Next >>");

        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_TO_CONVERT_INVALID_STATUS_KEY, 4);
        tester.assertTextInTable("convert_confirm_table", "Status");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Status", "(Workflow)", "In Progress", "status-inactive", "(jira)", "Open", "status-active", "(Subtask Workflow)"});

        tester.submit("Finish");

        //check issue type
        Locator locator = new IdLocator(tester, "type-val");
        textAssertions.assertTextPresent(locator, SUBTASK_TYPE_3);
        // check that issue has correct status
        locator = new IdLocator(tester, "status-val");
        textAssertions.assertTextPresent(locator, "Open");

        // Check change history
        tester.clickLinkWithText(CHANGE_HISTORY);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{CHANGE_HISTORY, "Workflow", "jira", "Subtask Workflow"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{CHANGE_HISTORY, "Status", "In Progress", "Open"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{CHANGE_HISTORY, "Issue Type", "Bug", SUBTASK_TYPE_3});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{CHANGE_HISTORY, "Parent", "MKY-3"});

    }

    @Test
    public void testBrowseBackOnPane() {
        //Test back to Step 1
        gotoConvertIssueStep3(ISSUE_TO_CONVERT_INVALID_STATUS_ID, PARENT_ISSUE, SUBTASK_TYPE_3_ID, "1");
        tester.submit("Next >>");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_TO_CONVERT_INVALID_STATUS_KEY, 4);

        tester.clickLinkWithText("Select Parent and Sub-task Type");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_TO_CONVERT_INVALID_STATUS_KEY, 1);
        tester.assertTextPresent("Step 1 of 4");
        tester.assertTextPresent(PARENT_ISSUE);
        tester.getDialog().dumpResponse();
        tester.assertTextPresent("option value=\"" + SUBTASK_TYPE_3_ID + "\" SELECTED");
        //assertOptionSelectedById("issuetype", SUBTASK_TYPE_3_ID);

        //Test back to Step 2
        gotoConvertIssueStep3(ISSUE_TO_CONVERT_INVALID_STATUS_ID, PARENT_ISSUE, SUBTASK_TYPE_3_ID, "1");
        tester.submit("Next >>");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_TO_CONVERT_INVALID_STATUS_KEY, 4);

        tester.clickLinkWithText("Select New Status");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_TO_CONVERT_INVALID_STATUS_KEY, 2);
        tester.assertTextPresent("Step 2 of 4");
        tester.assertTextPresent("option value=\"" + "1" + "\" SELECTED");
        //assertOptionSelected("targetStatusId", "Open");

        //Test back to Step 3
        gotoConvertIssueStep3(ISSUE_TO_CONVERT_INVALID_STATUS_ID, PARENT_ISSUE, SUBTASK_TYPE_3_ID, "1");
        tester.submit("Next >>");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_TO_CONVERT_INVALID_STATUS_KEY, 4);

        tester.clickLinkWithText("Update Fields");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_TO_CONVERT_INVALID_STATUS_KEY, 3);
        tester.assertTextPresent("Step 3 of 4");

    }

    @Test
    public void testResultsValidStatus() {
        gotoConvertIssueStep3(ISSUE_TO_CONVERT_INVALID_STATUS_ID, PARENT_ISSUE, SUBTASK_TYPE_3_ID, VALID_STATUS);
        tester.assertTextPresent("Step 3 of 4");

        // check pane
        assertThirdStepPaneWith2nd(ISSUE_TO_CONVERT_INVALID_STATUS_KEY, PARENT_ISSUE, SUBTASK_TYPE_3, "Open");

    }

    @Test
    public void testResultsInvalidStatus() {
        gotoConvertIssueStep3(ISSUE_TO_CONVERT_INVALID_STATUS_ID, PARENT_ISSUE, SUBTASK_TYPE_3_ID, INVALID_STATUS);
        tester.assertTextPresent("Step 2 of 4");
        tester.assertTextPresent("Selected status (In Progress) is not valid for target workflow");

        // check pane
        assertSecondStepPane(ISSUE_TO_CONVERT_INVALID_STATUS_KEY, PARENT_ISSUE, SUBTASK_TYPE_3);
    }

    @Test
    public void testResultsNoStatus() {
        gotoConvertIssueStep3(ISSUE_TO_CONVERT_INVALID_STATUS_ID, PARENT_ISSUE, SUBTASK_TYPE_3_ID, "");
        tester.assertTextPresent("Step 2 of 4");
        tester.assertTextPresent("No status specified");

        // check pane
        assertSecondStepPane(ISSUE_TO_CONVERT_INVALID_STATUS_KEY, PARENT_ISSUE, SUBTASK_TYPE_3);
    }

    @Test
    public void testResultsNonExistantStatus() {
        gotoConvertIssueStep3(ISSUE_TO_CONVERT_INVALID_STATUS_ID, PARENT_ISSUE, SUBTASK_TYPE_3_ID, NON_EXISTANT_STATUS);
        tester.assertTextPresent("Step 2 of 4");
        tester.assertTextPresent("Status with id " + NON_EXISTANT_STATUS + " does not exist");

        // check pane
        assertSecondStepPane(ISSUE_TO_CONVERT_INVALID_STATUS_KEY, PARENT_ISSUE, SUBTASK_TYPE_3);
    }

    @Test
    public void testResultsValidStatusForIssueThatDoesntNeedChanging() {
        gotoConvertIssueStep3(ISSUE_TO_CONVERT_ID, PARENT_ISSUE, SUBTASK_TYPE_ID, INVALID_STATUS);

        tester.assertTextPresent("Status should not be changed for this conversion");
    }

    private void assertSecondStepPane(String key, String parent, String type) {
        subTaskAssertions.assertSubTaskConversionPanelSteps(key, 2);
        tester.assertLinkPresentWithText("Select Parent and Sub-task Type");
        tester.assertLinkNotPresentWithText("Select New Status");
        tester.assertLinkNotPresentWithText("Update Fields");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{
                "Parent Issue:", "<strong>" + parent + "</strong>",
                "Sub-task Type:", "<strong>" + type + "</strong>",
                "Select New Status"
        });
    }

    private void assertThirdStepPaneWith2nd(String key, String parent, String type, String status) {
        subTaskAssertions.assertSubTaskConversionPanelSteps(key, 3);
        tester.assertLinkPresentWithText("Select Parent and Sub-task Type");
        tester.assertLinkPresentWithText("Select New Status");
        tester.assertLinkNotPresentWithText("Update Fields");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{
                "Parent Issue:", "<strong>" + parent + "</strong>",
                "Sub-task Type:", "<strong>" + type + "</strong>",
                "Status:", "<strong>" + status + "</strong>"
        });

    }

    private void assertThirdStepPaneWithout2nd(String key, String parent, String type, String status) {
        subTaskAssertions.assertSubTaskConversionPanelSteps(key, 3);
        tester.assertLinkPresentWithText("Select Parent and Sub-task Type");
        tester.assertLinkNotPresentWithText("Select New Status");
        tester.assertLinkNotPresentWithText("Update Fields");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{
                "Parent Issue:", "<strong>" + parent + "</strong>",
                "Sub-task Type:", "<strong>" + type + "</strong>",
                "Status:", "<strong>" + status + "</strong>"
        });

    }

    private void gotoConvertIssueStep2(String issueId, String parent, String issueType) {
        tester.gotoPage("/secure/ConvertIssueSetIssueType.jspa?id=" + issueId +
                "&parentIssueKey=" + parent + "&issuetype=" + issueType);
    }

    private void gotoConvertIssueStep3(String issueId, String parentKey, String issueType, String status) {
        tester.gotoPage("/secure/ConvertIssueSetStatus.jspa?id=" + issueId +
                "&parentIssueKey=" + parentKey + "&issuetype=" + issueType + "&targetStatusId=" + status);
    }

}
