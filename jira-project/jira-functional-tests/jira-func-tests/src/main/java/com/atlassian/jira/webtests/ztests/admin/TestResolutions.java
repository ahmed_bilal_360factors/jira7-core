package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.beans.Resolution;
import org.hamcrest.Matchers;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertThat;

/**
 * Tests for the administration of resolutions.
 *
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION})
@LoginAs(user = ADMIN_USERNAME)
public class TestResolutions extends BaseJiraFuncTest {
    //JRA-18985: It is possible to make a resolution a duplicate.
    @Inject
    private Administration administration;

    @Test
    public void testSameName() throws Exception {
        administration.restoreBlankInstance();
        navigation.gotoAdminSection(Navigation.AdminSection.RESOLUTIONS);

        //Check to see that we can't add a resolution of the same name.
        addResolution("Fixed");
        assertDuplicateResolutionError();

        addResolution("fixED");
        assertDuplicateResolutionError();

        //Check to see that we can't change a resolution name to an already existing name.
        tester.gotoPage("secure/admin/EditResolution!default.jspa?id=2");

        //Check to see that we can't edit a resolution such that becomes a duplicate.
        tester.setFormElement("name", "Fixed");
        tester.submit();
        assertDuplicateResolutionError();

        tester.setFormElement("name", "FIXED");
        tester.submit();
        assertDuplicateResolutionError();
    }

    private void assertDuplicateResolutionError() {
        assertions.getJiraFormAssertions().assertFieldErrMsg("A resolution with that name already exists.");
    }

    private void addResolution(String name) {
        tester.setFormElement("name", name);
        tester.submit();
    }

    @Test
    public void testNoDefaultResolutionsOnNewInstance() {
        administration.restoreData("TestDefaultResolutions.xml");

        List<Resolution> resolutions = backdoor.getTestkit().resolutions().getResolutions();
        assertThat(resolutions, Matchers.<Resolution>empty());
    }
}
