package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.locator.Locator;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.util.ProgressPageControl;
import com.meterware.httpunit.TableCell;
import com.meterware.httpunit.WebForm;
import com.meterware.httpunit.WebTable;
import net.sourceforge.jwebunit.WebTester;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.BUTTON_CONFIRM;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BUTTON_NEXT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_ASSIGNEE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_COMPONENTS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_FIX_VERSIONS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_OPERATION;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_PRIORITY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_VERSIONS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUETABLE_HEADER_ROW;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUETABLE_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.LABEL_ISSUE_NAVIGATOR;
import static com.atlassian.jira.functest.framework.FunctTestConstants.RADIO_OPERATION_EDIT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.RADIO_OPERATION_MOVE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.RADIO_OPERATION_WORKFLOW;
import static com.atlassian.jira.functest.framework.FunctTestConstants.STEP_CHOOSE_ISSUES;
import static com.atlassian.jira.functest.framework.FunctTestConstants.STEP_CHOOSE_OPERATION;
import static com.atlassian.jira.functest.framework.FunctTestConstants.STEP_CONFIRMATION;
import static com.atlassian.jira.functest.framework.FunctTestConstants.STEP_OPERATION_DETAILS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.STEP_PREFIX;
import static com.atlassian.jira.webtests.Groups.USERS;
import static org.junit.Assert.fail;

/**
 * Class to handle bulk operations in func tests.
 *
 * @since v7.1
 */
public class BulkOperations {

    private final WebTester tester;
    private final FuncTestLogger logger;
    private final Navigation navigation;
    private final Administration administration;
    private final Assertions assertions;

    public static final String SAME_FOR_ALL = "sameAsBulkEditBean";

    @Inject
    public BulkOperations(final WebTester tester,
                          final FuncTestLogger logger,
                          final Navigation navigation,
                          final Administration administration,
                          final Assertions assertions) {
        this.tester = tester;
        this.logger = logger;
        this.navigation = navigation;
        this.administration = administration;
        this.assertions = assertions;
    }

    /**
     * Simulates the clicking on bulk change all issues
     */
    public void bulkChangeIncludeAllPages() {
        tester.gotoPage("/views/bulkedit/BulkEdit1!default.jspa?reset=true&tempMax=10000");
    }

    /**
     * Simulates the clicking on bulk change current page of issues
     */
    public void bulkChangeIncludeCurrentPage() {
        tester.gotoPage("/views/bulkedit/BulkEdit1!default.jspa?reset=true");
    }

    /**
     * Checks if the current step in bulk change is: Choose Issues
     */
    public void isStepChooseIssues() {
        tester.assertTextPresent(STEP_PREFIX + STEP_CHOOSE_ISSUES);
        logger.log("Step 1 of 4");
    }

    /**
     * Checks if the current step in bulk change is: Choose Operation
     */
    public void isStepChooseOperation() {
        tester.assertTextPresent(STEP_PREFIX + STEP_CHOOSE_OPERATION);
        logger.log("Step 2 of 4");
    }

    /**
     * Checks if the current step in bulk change is: Operation Details
     */
    public void isStepOperationDetails() {
        tester.assertTextPresent(STEP_PREFIX + STEP_OPERATION_DETAILS);
        logger.log("Step 3 of 4");
    }

    /**
     * Checks if the current step in bulk change is: Confirmation.
     */
    public void isStepConfirmation() {
        tester.assertTextPresent(STEP_CONFIRMATION);
        logger.log("Step 4 of 4");
    }

    public void bulkChangeSelectIssue(final String key) {
        isStepChooseIssues();
        bulkOperationCheckIssues(Collections.singletonList(key));
        tester.submit(BUTTON_NEXT);
    }

    public void bulkChangeSelectIssues(final Collection keys) {
        isStepChooseIssues();
        bulkOperationCheckIssues(keys);
        tester.submit(BUTTON_NEXT);
    }

    /**
     * used by {@link BulkOperations#bulkChangeSelectIssue(String key)}
     * used by {@link BulkOperations#bulkChangeSelectIssues(Collection keys)}
     */
    private void bulkOperationCheckIssues(final Collection keys) {
        try {
            final int checkBoxColumn = 0;
            final WebTable table = tester.getDialog().getResponse().getTableWithID(ISSUETABLE_ID);
            int keyColumn = -1;
            for (int i = 0; i < table.getColumnCount(); i++) {
                final String headerCell = table.getCellAsText(ISSUETABLE_HEADER_ROW, i);
                if (headerCell.trim().equals("Key")) {
                    keyColumn = i;
                }
            }

            if (keyColumn < 0) {
                fail("Could not find column for Key");
            }

            int checkBoxesChecked = 0;
            for (int i = 0; i < table.getRowCount(); i++) {
                final String key = table.getCellAsText(i, keyColumn);
                if (keys.contains(key.trim())) {
                    final TableCell checkBoxCell = table.getTableCell(i, checkBoxColumn);
                    final String[] elementNames = checkBoxCell.getElementNames();
                    boolean foundCheckbox = false;
                    for (final String elementName : elementNames) {
                        if (elementName.startsWith("bulkedit_")) {
                            tester.checkCheckbox(elementName);
                            if (++checkBoxesChecked >= keys.size()) {
                                // If we have selected all provided issue keys then there us no need to continue
                                // looking through the table. Return out of the method.
                                return;
                            } else {
                                // Otherwise no need to loop through the nodes. Continue with the next table row.
                                foundCheckbox = true;
                                break;
                            }
                        }
                    }

                    if (!foundCheckbox) {
                        fail("Could not find the check box for issue with key '" + key + "'.");
                    }
                }
            }

        } catch (final SAXException e) {
            e.printStackTrace();
            fail("Error occurred selecting issues.");
        }
    }

    public void waitAndReloadBulkOperationProgressPage() {
        tester.assertTextPresent("Bulk Operation Progress");
        ProgressPageControl.waitAndReload(tester, "bulkoperationprogressform", "Refresh", "Acknowledge");
        logger.log("waitAndReloadBulkOperationProgressPage");
    }

    /**
     * Chooses the Edit Operation radio button in the Step Choose Operation
     */
    public void bulkChangeChooseOperationEdit() {
        isStepChooseOperation();
        tester.setFormElement(FIELD_OPERATION, RADIO_OPERATION_EDIT);
        tester.assertRadioOptionSelected(FIELD_OPERATION, RADIO_OPERATION_EDIT);
        logger.log("Operation Selected: Edit Issues");
        tester.submit(BUTTON_NEXT);
    }

    /**
     * Selects the 'listValue' in the 'field' list and checks that the corresponding 'field' checkbox is selected
     */
    public void setBulkEditFieldTo(final String field, final String listValue) {
        logger.log("Set " + field + " To: \"" + listValue + "\"");
        tester.checkCheckbox("actions", field);
        tester.selectOption(field, listValue);
        tester.assertOptionEquals(field, listValue);
    }

    /**
     * Chooses the bulk action(s) you wish to perform on the selected issue.<br> if a field is not to be selected place
     * "" in place of it.<br> Used in Operation Details
     *
     * @param fields A map woth field ids as keys and field values (have to be simple Strings) as values.
     */
    public void bulkEditOperationDetailsSetAs(final Map<String, String> fields) {
        isStepOperationDetails();
        tester.assertFormElementEquals("actions", null);

        for (final Map.Entry<String, String> entry : fields.entrySet()) {
            final String fieldName = entry.getKey();
            final String value = entry.getValue();
            if (FIELD_FIX_VERSIONS.equals(fieldName)) {
                setBulkEditFieldTo(FIELD_FIX_VERSIONS, value);
            }
            if (FIELD_VERSIONS.equals(fieldName)) {
                setBulkEditFieldTo(FIELD_VERSIONS, value);
            }
            if (FIELD_COMPONENTS.equals(fieldName)) {
                setBulkEditFieldTo(FIELD_COMPONENTS, value);
            }
            if (FIELD_ASSIGNEE.equals(fieldName)) {
                setBulkEditFieldTo(FIELD_ASSIGNEE, value);
            }
            if (FIELD_PRIORITY.equals(fieldName)) {
                setBulkEditFieldTo(FIELD_PRIORITY, value);
            }
        }
        tester.submit(BUTTON_NEXT);
    }

    /**
     * Checks in step Confirmation of edit operation before confirming, whether or not the selected fields have been
     * made<br> DOES NOT goto the issues change log and check that they are changed after confirmation... something to
     * consider testing...
     *
     * @param fields a map with field ids as keys and simple Strings as values.
     */
    public void bulkEditConfirmEdit(final Map<String, String> fields) {
        isStepConfirmation();

        for (final Map.Entry<String, String> entry : fields.entrySet()) {
            final String fieldName = entry.getKey();
            final String value = entry.getValue();
            if (FIELD_FIX_VERSIONS.equals(fieldName)) {
                tester.assertTextPresent("Fix Version/s");
                tester.assertTextPresent(value);
            }
            if (FIELD_VERSIONS.equals(fieldName)) {
                tester.assertTextPresent("Affects Version/s");
                tester.assertTextPresent(value);
            }
            if (FIELD_COMPONENTS.equals(fieldName)) {
                tester.assertTextPresent("Component/s");
                tester.assertTextPresent(value);
            }
            if (FIELD_ASSIGNEE.equals(fieldName)) {
                tester.assertTextPresent("Assignee");
                tester.assertTextPresent(value);
            }
            if (FIELD_PRIORITY.equals(fieldName)) {
                tester.assertTextPresent("Priority");
                tester.assertTextPresent(value);
            }
        }
    }

    /**
     * Clicks on the 'Confirm' button on the confirmation steps
     */
    public void bulkChangeConfirm() {
        isStepConfirmation();
        tester.submit(BUTTON_CONFIRM);
        logger.log("Confirmed");
    }

    /**
     * Clicks on the 'Cancel' button on any of the bulk change steps
     */
    public void bulkChangeCancel() {
        assertions.assertNodeHasText(new CssLocator(tester, "#cancel"), "Cancel");
        tester.clickLink("cancel");
        tester.assertTextPresent(LABEL_ISSUE_NAVIGATOR);
        logger.log("Canceled");
    }

    /**
     * selects the checkbox with id all<br> Used in the Step Choose Issues
     */
    public void bulkChangeChooseIssuesAll() {
        isStepChooseIssues();

        tester.getDialog().setWorkingForm("bulkedit");
        final String[] paramNames = tester.getDialog().getForm().getParameterNames();
        for (final String paramName : paramNames) {
            if (paramName.startsWith("bulkedit_")) {
                tester.checkCheckbox(paramName);
            }
        }
        tester.submit(BUTTON_NEXT);
    }

    /**
     * Chooses the Execute Worfklow Action radio button in the Step Choose Operation
     */
    public void chooseOperationExecuteWorfklowTransition() {
        isStepChooseOperation();
        tester.setFormElement(FIELD_OPERATION, RADIO_OPERATION_WORKFLOW);
        tester.assertRadioOptionSelected(FIELD_OPERATION, RADIO_OPERATION_WORKFLOW);
        logger.log("Operation Selected: Transition Issues");
        tester.submit(BUTTON_NEXT);
    }

    /**
     * Chooses the Move Operation radio button in the Step Choose Operation
     */
    public void chooseOperationBulkMove() {
        isStepChooseOperation();
        tester.setFormElement(FIELD_OPERATION, RADIO_OPERATION_MOVE);
        tester.assertRadioOptionSelected(FIELD_OPERATION, RADIO_OPERATION_MOVE);
        logger.log("Operation Selected: Move Issues");
        tester.submit(BUTTON_NEXT);
    }

    public void deleteAllIssuesInAllPages() {
        final boolean mailServerExists = isMailServerExists();

        administration.addGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
        navigation.issueNavigator().displayAllIssues();
        logger.log("Deleting all issues");
        tester.assertElementPresent("issuetable");
        tester.gotoPage("/secure/views/bulkedit/BulkEdit1!default.jspa?reset=true&tempMax=10000");

        tester.assertTextPresent("Bulk Operation: " + "Choose Issues");

        tester.setWorkingForm("bulkedit");
        final WebForm form = tester.getDialog().getForm();
        final String[] parameterNames = form.getParameterNames();
        for (final String name : parameterNames) {
            if (name.startsWith("bulkedit_")) {
                tester.checkCheckbox(name);
            }
        }

        tester.submit("Next");

        tester.assertTextPresent("Bulk Operation: " + "Choose Operation");
        tester.setFormElement("operation", "bulk.delete.operation.name");
        tester.assertRadioOptionSelected("operation", "bulk.delete.operation.name");
        tester.submit("Next");

        // Do nothing - send mail notification option not needed
        if (mailServerExists) {
            tester.submit("Next");
        }

        tester.assertTextPresent("Bulk Operation: " + "Confirmation");
        tester.submit("Confirm");
        waitAndReloadBulkOperationProgressPage();
        administration.removeGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
    }

    private boolean isMailServerExists() {
        navigation.gotoAdminSection(Navigation.AdminSection.OUTGOING_MAIL);

        final String testExisitingMailServerId = "sendTestEmail";
        final Locator xPathLocator = new XPathLocator(tester, "//*[@id='" + testExisitingMailServerId + "']");
        return xPathLocator.getNodes().length > 0;
    }
}
