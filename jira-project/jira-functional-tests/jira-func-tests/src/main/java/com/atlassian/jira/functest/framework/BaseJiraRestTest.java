package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.functest.rule.RestoreDataBackdoor;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.rules.TestRule;

import javax.inject.Inject;

/**
 * Base class for REST tests.
 *
 * @since v6.5
 */
public class BaseJiraRestTest {
    @ClassRule
    public static final RestoreDataBackdoor initClass = new RestoreDataBackdoor();
    @Rule
    public TestRule initData = FuncTestRuleChain.forTest(this);

    @Inject
    protected JIRAEnvironmentData environmentData;
    @Inject
    protected Backdoor backdoor;

    public JIRAEnvironmentData getEnvironmentData() {
        return environmentData;
    }
}
