package com.atlassian.jira.webtests.ztests.user.security.xsrf;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Responsible for holding tests which verify that the User Profile preferences are not susceptible to XSRF attacks.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.SECURITY, Category.USERS_AND_GROUPS})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestXsrfUserProfile extends BaseJiraFuncTest {

    @Inject
    private Form form;

    @Test
    public void testUpdateProfile() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck(
                        "Update Profile",
                        () -> {
                            gotoUserProfile();
                            tester.clickLink("edit_profile_lnk");

                        },
                        new XsrfCheck.FormSubmission("Edit")
                ),
                new XsrfCheck(
                        "Update Profile Preferences",
                        () -> {
                            gotoUserProfile();
                            tester.clickLink("edit_prefs_lnk");

                        },
                        new XsrfCheck.FormSubmission("Update")
                )

        ).run(tester, navigation, form);
    }

    private void gotoUserProfile() {
        tester.gotoPage("secure/ViewProfile.jspa");
    }
}
