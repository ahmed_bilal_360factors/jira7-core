package com.atlassian.jira.functest.rule;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.testkit.client.AttachmentsControl;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;

import java.io.File;
import java.util.function.Supplier;

/**
 * Creates {@link org.junit.rules.TestRule} instances.
 *
 * @since v6.4
 */
public class Rules {
    /**
     * Prepares attachment files, but not database entries.
     *
     * @param environmentDataSupplier Supplier for environment, which specifies attachment source and target directory
     * @param backdoorSupplier        Supplier for backdoor
     * @param sourceSubPath           Path relative to {@code environmentData.getXMLDataLocation()} (typically
     *                                ".../jira-webdriver-tests/src/main/xml/")
     */
    public static TestRule prepareAttachments(final Supplier<JIRAEnvironmentData> environmentDataSupplier, final Supplier<Backdoor> backdoorSupplier, final String sourceSubPath) {
        final Supplier<AttachmentsControl> attachmentsControlSupplier = () -> backdoorSupplier.get().attachments();
        return chain()
                .around(enableAttachments(attachmentsControlSupplier))
                .around(cleanAttachments(attachmentsControlSupplier))
                .around(copyAttachments(environmentDataSupplier, attachmentsControlSupplier, sourceSubPath));
    }

    public static RuleChain chain() {
        return RuleChain.emptyRuleChain();
    }

    public static TestRule enableAttachments(final Supplier<AttachmentsControl> attachmentsControlSupplier) {
        return new EnableAttachmentsRule(attachmentsControlSupplier);
    }

    public static TestRule cleanAttachments(final Supplier<AttachmentsControl> attachmentsControlSupplier) {
        return new CleanAttachmentsRule(attachmentsControlSupplier);
    }

    public static TestRule copyAttachments(
            final Supplier<JIRAEnvironmentData> environmentDataSupplier,
            final Supplier<AttachmentsControl> attachmentsControlSupplier,
            final String sourceSubPath
    ) {
        return new CopyAttachmentsRule(environmentDataSupplier, attachmentsControlSupplier, sourceSubPath);
    }

    public static TestRule cleanDirectory(final File directory) {
        return new CleanDirectoryRule(directory);
    }

    public static TestRule cleanDirectory(final Supplier<File> directory) {
        return new CleanDirectoryRule(directory);
    }

    public static TestRule copyDirectory(final File sourceDirectory, final File targetDirectory) {
        return new CopyDirectoryRule(sourceDirectory, targetDirectory);
    }

}
