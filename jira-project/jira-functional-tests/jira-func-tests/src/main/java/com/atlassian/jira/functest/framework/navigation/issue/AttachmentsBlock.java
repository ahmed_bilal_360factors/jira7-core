package com.atlassian.jira.functest.framework.navigation.issue;

/**
 * Represents the attachments block on the view issue page.
 *
 * @since v4.2
 */
public interface AttachmentsBlock {
    /**
     * Sorts the file attachments list on the view issue page for an specific issue.
     *
     * @param key       The {@link com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock.Sort.Key} to use to
     *                  sort the list.
     * @param direction The {@link com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock.Sort.Direction}
     *                  to use to sort the list.
     */
    public void sort(AttachmentsBlock.Sort.Key key, AttachmentsBlock.Sort.Direction direction);

    /**
     * Navigates to the Manage Attachments page.
     *
     * @return An {@link com.atlassian.jira.functest.framework.navigation.issue.AttachmentManagement} object to interact
     * with the Manage Attachments page.
     */
    AttachmentManagement manage();

    ImageAttachmentsGallery gallery();

    FileAttachmentsList list();

    ViewMode getViewMode();

    void setViewMode(final ViewMode viewMode);

    interface AttachmentBlockSetting {
        String getLinkId();

        String getContainerSelector();
    }

    interface Sort extends AttachmentBlockSetting {
        /**
         * Represents a key used to sort the attachments list on the view issue page.
         */
        enum Key implements Sort {
            NAME("attachment-sort-key-name", ".item-attachments[data-sort-key=fileName]"), DATE("attachment-sort-key-date", ".item-attachments[data-sort-key=dateTime]");

            private final String linkId;
            private final String containerSelector;

            Key(final String linkId, final String containerSelector) {
                this.linkId = linkId;
                this.containerSelector = containerSelector;
            }

            @Override
            public String getLinkId() {
                return linkId;
            }

            @Override
            public String getContainerSelector() {
                return containerSelector;
            }
        }

        /**
         * Represents a sort direction used when sorting the attachments list on the view issue page.
         */
        enum Direction implements Sort {
            ASCENDING("attachment-sort-direction-asc", ".item-attachments[data-sort-order=asc]"), DESCENDING("attachment-sort-direction-desc", ".item-attachments[data-sort-order=desc]");

            private final String linkId;
            private final String containerSelector;

            Direction(final String linkId, final String containerSelector) {
                this.linkId = linkId;
                this.containerSelector = containerSelector;
            }

            @Override
            public String getLinkId() {
                return linkId;
            }

            @Override
            public String getContainerSelector() {
                return containerSelector;
            }
        }
    }

    enum ViewMode implements AttachmentBlockSetting {
        GALLERY("attachment-view-mode-gallery", "#attachment_thumbnails"), LIST("attachment-view-mode-list", "#file_attachments");

        private final String linkId;
        private final String containerId;

        ViewMode(final String linkId, final String containerId) {

            this.linkId = linkId;
            this.containerId = containerId;
        }

        @Override
        public String getLinkId() {
            return linkId;
        }

        @Override
        public String getContainerSelector() {
            return containerId;
        }
    }
}
