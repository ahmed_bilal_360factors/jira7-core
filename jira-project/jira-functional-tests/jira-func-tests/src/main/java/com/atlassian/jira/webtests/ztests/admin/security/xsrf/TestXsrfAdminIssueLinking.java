package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestXsrfAdminIssueLinking extends BaseJiraFuncTest {
    @Inject
    private Form form;

    @Test
    public void testEasySettings() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck("Issue Linking Activate", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.LINKING);
                    }
                }, new XsrfCheck.FormSubmission("Activate"))
                ,
                new XsrfCheck("Issue Linking Add", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.LINKING);
                        tester.setFormElement("name", "inout");
                        tester.setFormElement("outward", "out");
                        tester.setFormElement("inward", "in");
                    }
                }, new XsrfCheck.FormSubmission("Add"))
                ,
                new XsrfCheck("Issue Linking Edit", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.LINKING);
                        tester.clickLink("edit_inout");
                    }
                }, new XsrfCheck.FormSubmission("Update"))
                ,
                new XsrfCheck("Issue Linking Delete", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.LINKING);
                        tester.clickLinkWithText("Delete");
                    }
                }, new XsrfCheck.FormSubmission("Delete"))
                ,
                new XsrfCheck("Issue Linking DeActivate", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.LINKING);
                    }
                }, new XsrfCheck.FormSubmission("Deactivate"))

        ).run(tester, navigation, form);
    }

}