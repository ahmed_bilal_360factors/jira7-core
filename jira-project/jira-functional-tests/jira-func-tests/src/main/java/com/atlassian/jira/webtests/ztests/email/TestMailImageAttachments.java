package com.atlassian.jira.webtests.ztests.email;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.EmailBaseFuncTestCase;
import com.google.common.collect.Lists;
import com.icegreen.greenmail.util.GreenMailUtil;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageInputStream;
import javax.inject.Inject;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsArrayWithSize.arrayWithSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

@WebTest({Category.FUNC_TEST, Category.EMAIL})
@LoginAs(user = ADMIN_USERNAME)
public class TestMailImageAttachments extends EmailBaseFuncTestCase {

    private static final Pattern IMG_SRC_WITH_CID = Pattern.compile("<img.*?src=\"cid:(\\S+)\"");
    private static final String FORMAT_NAME = "javax_imageio_png_1.0";

    @Inject
    private Administration administration;

    private static IIOImage readFirstImage(final InputStream inputStream) throws IOException {
        final ImageInputStream imageInputStream = ImageIO.createImageInputStream(inputStream);
        final ImageReader reader = ImageIO.getImageReaders(imageInputStream).next();
        reader.setInput(imageInputStream, true, false);
        return reader.readAll(0, reader.getDefaultReadParam());
    }

    @Test
    @Restore("TestIssueNotifications.xml")
    public void testEachImageAttachmentIsVisible() throws Exception {
        configureAndStartSmtpServerWithNotify();

        final String issueId = "COW-1";
        navigation.issue().viewIssue(issueId);
        tester.clickLink("delete-issue");
        tester.submit("Delete");

        //there should 2 notifications, because of the issue's security level
        flushMailQueueAndWait(2);
        final MimeMessage[] mimeMessages = mailService.getReceivedMessages();
        assertEquals(2, mimeMessages.length);

        for (MimeMessage mimeMessage : mimeMessages) {
            assertEachCidAttachmentIsUsedInMailMessage(mimeMessage);
        }
    }

    @Ignore("AID-1049")
    @Restore("\"TestIssueNotifications.xml\"")
    public void testMentionsEmails() throws Exception {
        administration.restoreData("TestIssueNotifications.xml");
        configureAndStartSmtpServerWithNotify();

        backdoor.usersAndGroups().addUser("user10");

        final String issueId = "COW-1";
        navigation.issue().addComment(issueId, "Some Comment [~user10]");

        flushMailQueueAndWait(3);
        final MimeMessage[] mimeMessages = mailService.getReceivedMessages();
        assertThat(mimeMessages.length, equalTo(3));

        for (MimeMessage message : mimeMessages) {
            assertEachImageWithCidHasCorrespondingAttachment(message);
        }
    }

    private void assertEachCidAttachmentIsUsedInMailMessage(final MimeMessage mimeMessage) throws IOException, MessagingException {
        final Object content = mimeMessage.getContent();
        assertThat(content, is(instanceOf(Multipart.class)));

        final Multipart multipart = (Multipart) content;
        final Collection<String> contentIds = getContentIds(multipart);
        for (String contentId : contentIds) {
            assertEmailBodyContains(mimeMessage, contentId);
        }
    }

    private Collection<String> getContentIds(final Multipart multipart) throws MessagingException, IOException {
        final Collection<String> result = Lists.newArrayList();
        for (int i = 0; i < multipart.getCount(); i++) {
            final BodyPart bodyPart = multipart.getBodyPart(i);
            final String[] headers = bodyPart.getHeader("Content-ID");
            if (headers == null) {
                continue;
            }
            assertThat("There should be only one 'Content-ID' header in BodyPart", headers, arrayWithSize(1));
            final String cidHeader = headers[0];

            final String[] contentTypeHeaders = bodyPart.getHeader("Content-Type");
            assertThat(contentTypeHeaders, arrayWithSize(1));

            final String contentTypeHeader = contentTypeHeaders[0];
            assertThat(contentTypeHeader, equalTo("image/png"));

            assertContainsJiraMetaData(bodyPart);

            //Content Id is surrounded with '<' and '>'
            result.add(cidHeader.substring(1, cidHeader.length() - 1));
        }
        return result;
    }

    private void assertEachImageWithCidHasCorrespondingAttachment(final MimeMessage message) throws MessagingException, IOException {
        final Object content = message.getContent();
        assertThat(content, is(instanceOf(Multipart.class)));

        final Multipart multipart = (Multipart) content;
        final String[] cidsFromMessage = getCidsFromMessage(GreenMailUtil.getBody(message));
        final Collection<String> contentIds = getContentIds(multipart);
        assertThat(contentIds, containsInAnyOrder(cidsFromMessage));
    }

    private String[] getCidsFromMessage(final String messageBody) {
        final Matcher matcher = IMG_SRC_WITH_CID.matcher(messageBody);
        final List<String> result = Lists.newArrayList();
        while (matcher.find()) {
            result.add(matcher.group(1));
        }
        return result.toArray(new String[result.size()]);
    }

    private void assertContainsJiraMetaData(final BodyPart bodyPart) throws MessagingException, IOException {
        final IIOImage image = readFirstImage(bodyPart.getInputStream());
        final IIOMetadata metadata = image.getMetadata();

        final Node root = metadata.getAsTree(FORMAT_NAME);

        final Node textNode = findChildNode("tEXt", root);
        final Node textEntry = findChildNode("tEXtEntry", textNode);

        final NamedNodeMap attributes = textEntry.getAttributes();

        final Node keyword = attributes.getNamedItem("keyword");
        final Node value = attributes.getNamedItem("value");

        assertThat(keyword.getNodeValue(), equalTo("jira-system-image-type"));
        assertThat(value.getNodeValue(), anyOf(equalTo("avatar"), equalTo("icon"), equalTo("logo")));
    }

    private Node findChildNode(final String nodeName, final Node parent) {
        final NodeList childNodes = parent.getChildNodes();

        for (int i = 0; i < childNodes.getLength(); i++) {
            final Node node = childNodes.item(i);

            if (nodeName.equals(node.getNodeName())) {
                return node;
            }

        }
        fail(String.format("No node with name %s found in parent %s.", nodeName, parent.getNodeName()));
        return null;
    }
}
