package com.atlassian.jira.webtests.ztests.bulk;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperations;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.navigation.IssueNavigatorNavigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.BULK_OPERATIONS})
@Restore("TestBulkOperationsIndexing.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestBulkOperationsIndexing extends BaseJiraFuncTest {

    @Inject
    private BulkOperations bulkOperations;

    @Test
    public void testBulkEditIndexing() {
        findAndVerifyIssuesArePresent(false);

        //lets bulk edit and add a comment
        navigation.issueNavigator().bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES);
        tester.checkCheckbox("bulkedit_10003", "on");
        tester.checkCheckbox("bulkedit_10002", "on");
        tester.checkCheckbox("bulkedit_10001", "on");
        tester.checkCheckbox("bulkedit_10000", "on");
        tester.submit("Next");
        tester.checkCheckbox("operation", "bulk.edit.operation.name");
        tester.submit("Next");
        tester.checkCheckbox("actions", "comment");
        tester.setFormElement("comment", "whatsminesay");
        tester.submit("Next");
        tester.submit("Confirm");
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        //search for all issues with the comment we've just added.
        navigation.issueNavigator().createSearch("comment~whatsminesay");

        tester.assertTextPresent("HSP-1");
        tester.assertTextPresent("HSP-2");
        tester.assertTextPresent("Test issue 1");
        tester.assertTextPresent("Test Issue 2");
        tester.assertTextPresent("HSP-3");
        tester.assertTextPresent("HSP-4");
        tester.assertTextPresent("Subtask 1");
        tester.assertTextPresent("Subtask 2");
    }

    @Test
    public void testBulkMoveIndexing() {
        findAndVerifyIssuesArePresent(true);

        //lets bulk move, this set includes an issue that is already in the project we are moving to
        navigation.issueNavigator().bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES);
        tester.checkCheckbox("bulkedit_10003", "on");
        tester.checkCheckbox("bulkedit_10002", "on");
        tester.checkCheckbox("bulkedit_10001", "on");
        tester.checkCheckbox("bulkedit_10000", "on");
        tester.checkCheckbox("bulkedit_10010", "on");
        tester.submit("Next");
        tester.checkCheckbox("operation", "bulk.move.operation.name");
        tester.submit("Next");
        navigation.issue().selectProject("monkey", "10010_1_pid");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        //search for all issues with the comment we've just added.
        navigation.issueNavigator().createSearch("project=monkey");

        tester.assertTextPresent("MKY-1");
        tester.assertTextPresent("MKY-2");
        tester.assertTextPresent("MKY-3");
        tester.assertTextPresent("Test issue 1");
        tester.assertTextPresent("Test Issue 2");
        tester.assertTextPresent("MKY-4");
        tester.assertTextPresent("MKY-5");
        tester.assertTextPresent("Subtask 1");
        tester.assertTextPresent("Subtask 2");
        tester.assertTextPresent("Test Monkey Issue 1");

        // Verify there are no issues left in the old project
        navigation.issueNavigator().createSearch("project=homosapian");
        assertions.assertNodeByIdDoesNotExist("issuetable");
    }

    @Test
    public void testBulkWorkflowTransitionIndexing() {
        findAndVerifyIssuesArePresent(true);

        //lets bulk move, this set includes an issue that is already in the project we are moving to
        navigation.issueNavigator().bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES);
        tester.checkCheckbox("bulkedit_10003", "on");
        tester.checkCheckbox("bulkedit_10002", "on");
        tester.checkCheckbox("bulkedit_10001", "on");
        tester.checkCheckbox("bulkedit_10000", "on");
        tester.checkCheckbox("bulkedit_10010", "on");
        tester.submit("Next");
        tester.checkCheckbox("operation", "bulk.workflowtransition.operation.name");
        tester.submit("Next");
        tester.checkCheckbox("wftransition", "classic default workflow_5_5");
        tester.submit("Next");
        tester.selectOption("resolution", "Fixed");
        tester.submit("Next");
        tester.submit("Next");
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        // Search to make sure they have all been put into the right status and resolution
        navigation.issueNavigator().createSearch("resolution=Fixed AND status = Resolved");

        tester.assertTextPresent("HSP-1");
        tester.assertTextPresent("HSP-2");
        tester.assertTextPresent("Test issue 1");
        tester.assertTextPresent("Test Issue 2");
        tester.assertTextPresent("HSP-3");
        tester.assertTextPresent("HSP-4");
        tester.assertTextPresent("Subtask 1");
        tester.assertTextPresent("Subtask 2");
        tester.assertTextPresent("MKY-1");
        tester.assertTextPresent("Test Monkey Issue 1");
    }

    private void findAndVerifyIssuesArePresent(final boolean all) {
        //Show all issues.
        if (!all) {
            navigation.issueNavigator().createSearch("project=homosapien");
        } else {
            navigation.issueNavigator().displayAllIssues();
        }

        tester.assertTextPresent("HSP-1");
        tester.assertTextPresent("HSP-2");
        tester.assertTextPresent("Test issue 1");
        tester.assertTextPresent("Test Issue 2");
        tester.assertTextPresent("HSP-3");
        tester.assertTextPresent("HSP-4");
        tester.assertTextPresent("Subtask 1");
        tester.assertTextPresent("Subtask 2");
        if (all) {
            tester.assertTextPresent("MKY-1");
            tester.assertTextPresent("Test Monkey Issue 1");
        }
    }
}
