package com.atlassian.jira.functest.framework.admin;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.JiraPermissionHolderType;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.common.collect.ImmutableMap;
import net.sourceforge.jwebunit.WebTester;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * This class was added to replace PermissionSchemesImpl to stop plugins/tests outside of JIRA who were using it from
 * failing. E.g. JIRA-REST-JAVA-Client. If you actually want to test the Project Permissions page, you should write
 * tests using a webdriver test with {@link com.atlassian.jira.pageobjects.pages.EditPermissionsSinglePage} otherwise, if
 * you just want to change permissions in a scheme you should use the backdoor.
 *
 * @deprecated use {@link com.atlassian.jira.functest.framework.backdoor.Backdoor#permissionSchemes()}
 */
@Deprecated
public class PermissionSchemesBackdoorImpl implements PermissionSchemes,
        PermissionSchemes.PermissionScheme {

    private final Backdoor backdoor;
    private int schemeId;

    public PermissionSchemesBackdoorImpl(Backdoor backdoor, WebTester tester, JIRAEnvironmentData environmentData) {
        this.backdoor = backdoor;
    }


    @Override
    public PermissionScheme defaultScheme() {
        schemeId = (int) FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
        return this;
    }

    @Override
    public PermissionScheme scheme(String schemeName) {
        List<Map<String, Object>> schemes = backdoor.entityEngine().findByAnd("PermissionScheme", ImmutableMap.of("name", schemeName));
        if (schemes.size() != 1) {
            final String error = String.format("Could not find a unique scheme with name: '%s'. Number matching schemes: %d",
                    schemeName, schemes.size());
            throw new RuntimeException(error);
        }

        schemeId = (int) schemes.get(0).get("id");
        return this;
    }

    private String anyoneGroupValidation(String groupName) {
        if (StringUtils.isBlank(groupName)) {
            return null;
        }
        return groupName;
    }

    @Override
    public void grantPermissionToGroup(int permission, String groupName) {
        groupName = anyoneGroupValidation(groupName);
        addPermission(permission, JiraPermissionHolderType.GROUP, groupName);
    }

    @Override
    public void grantPermissionToGroup(String permission, String groupName) {
        groupName = anyoneGroupValidation(groupName);
        addPermission(permission, JiraPermissionHolderType.GROUP, groupName);
    }

    @Override
    public void grantPermissionToGroup(ProjectPermissionKey permission, String groupName) {
        groupName = anyoneGroupValidation(groupName);
        addPermission(permission, JiraPermissionHolderType.GROUP, groupName);
    }

    @Override
    public void grantPermissionToSingleUser(int permission, String username) {
        addPermission(permission, JiraPermissionHolderType.USER, username);
    }

    @Override
    public void grantPermissionToSingleUser(String permission, String username) {
        addPermission(permission, JiraPermissionHolderType.USER, username);
    }

    @Override
    public void grantPermissionToSingleUser(ProjectPermissionKey permission, String username) {
        addPermission(permission, JiraPermissionHolderType.USER, username);
    }

    @Override
    public void grantPermissionToReporter(int permission) {
        addPermission(permission, JiraPermissionHolderType.REPORTER, null);

    }

    @Override
    public void grantPermissionToReporter(String permission) {
        addPermission(permission, JiraPermissionHolderType.REPORTER, null);
    }

    @Override
    public void grantPermissionToReporter(ProjectPermissionKey permission) {
        addPermission(permission, JiraPermissionHolderType.REPORTER, null);
    }

    @Override
    public void grantPermissionToProjectLead(int permission) {
        addPermission(permission, JiraPermissionHolderType.PROJECT_LEAD, null);
    }

    @Override
    public void grantPermissionToProjectLead(String permission) {
        addPermission(permission, JiraPermissionHolderType.PROJECT_LEAD, null);
    }

    @Override
    public void grantPermissionToProjectLead(ProjectPermissionKey permission) {
        addPermission(permission, JiraPermissionHolderType.PROJECT_LEAD, null);
    }

    @Override
    public void grantPermissionToCurrentAssignee(int permission) {
        addPermission(permission, JiraPermissionHolderType.ASSIGNEE, null);
    }

    @Override
    public void grantPermissionToCurrentAssignee(String permission) {
        addPermission(permission, JiraPermissionHolderType.ASSIGNEE, null);
    }

    @Override
    public void grantPermissionToCurrentAssignee(ProjectPermissionKey permission) {
        addPermission(permission, JiraPermissionHolderType.ASSIGNEE, null);
    }

    @Override
    public void grantPermissionToUserCustomFieldValue(int permission, String customFieldId) {
        addPermission(permission, JiraPermissionHolderType.USER_CUSTOM_FIELD, customFieldId);
    }

    @Override
    public void grantPermissionToUserCustomFieldValue(String permission, String customFieldId) {
        addPermission(permission, JiraPermissionHolderType.USER_CUSTOM_FIELD, customFieldId);
    }

    @Override
    public void grantPermissionToUserCustomFieldValue(ProjectPermissionKey permission, String customFieldId) {
        addPermission(permission, JiraPermissionHolderType.USER_CUSTOM_FIELD, customFieldId);
    }

    @Override
    public void grantPermissionToGroupCustomFieldValue(int permission, String customFieldId) {
        addPermission(permission, JiraPermissionHolderType.GROUP_CUSTOM_FIELD, customFieldId);
    }

    @Override
    public void grantPermissionToGroupCustomFieldValue(String permission, String customFieldId) {
        addPermission(permission, JiraPermissionHolderType.GROUP_CUSTOM_FIELD, customFieldId);
    }

    @Override
    public void grantPermissionToGroupCustomFieldValue(ProjectPermissionKey permission, String customFieldId) {
        addPermission(permission, JiraPermissionHolderType.GROUP_CUSTOM_FIELD, customFieldId);
    }

    @Override
    public void grantPermissionToProjectRole(int permission, String projectRoleId) {
        addPermission(permission, JiraPermissionHolderType.PROJECT_ROLE, projectRoleId);
    }

    @Override
    public void grantPermissionToProjectRole(String permission, String projectRoleId) {
        addPermission(permission, JiraPermissionHolderType.PROJECT_ROLE, projectRoleId);
    }

    @Override
    public void grantPermissionToProjectRole(ProjectPermissionKey permission, String projectRoleId) {
        addPermission(permission, JiraPermissionHolderType.PROJECT_ROLE, projectRoleId);
    }

    @Override
    public void grantPermissionToApplicationRole(ProjectPermissionKey permission, String applicationRoleId) {
        applicationRoleId = anyoneGroupValidation(applicationRoleId);
        addPermission(permission, JiraPermissionHolderType.APPLICATION_ROLE, applicationRoleId);
    }

    @Override
    public void removePermission(int permissionType, String permissionParam) {
        removePermission(new ProjectPermissionKey(permissionType), permissionParam);
    }

    @Override
    public void removePermission(String permissionType, String permissionParam) {
        removePermission(new ProjectPermissionKey(permissionType), permissionParam);
    }

    @Override
    public void removePermission(ProjectPermissionKey permissionType, String permissionParam) {
        backdoor.permissionSchemes().removePermission(schemeId, permissionType, permissionParam);
    }

    @Override
    public void removePermission(GlobalPermissionKey permissionType, String permissionParam) {
        removePermission(new ProjectPermissionKey(permissionType.getKey()), permissionParam);
    }

    public void addPermission(int permission, JiraPermissionHolderType type, String parameter) {
        addPermission(new ProjectPermissionKey(permission), type, parameter);
    }

    public void addPermission(String permission, JiraPermissionHolderType type, String parameter) {
        addPermission(new ProjectPermissionKey(permission), type, parameter);
    }

    public void addPermission(ProjectPermissionKey permission, JiraPermissionHolderType type, String parameter) {
        backdoor.permissionSchemes().addPermission(schemeId, permission, type).parameter(parameter).getRequest();
    }
}
