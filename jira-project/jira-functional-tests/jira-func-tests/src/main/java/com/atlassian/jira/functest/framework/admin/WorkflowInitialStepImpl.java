package com.atlassian.jira.functest.framework.admin;

import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.inject.Inject;
import net.sourceforge.jwebunit.WebTester;

/**
 * Concrete implementation of {@link WorkflowInitialStep}
 */
public class WorkflowInitialStepImpl implements WorkflowInitialStep {
    private final WebTester tester;
    private final WorkflowTransition transition;
    private final String workflowName;

    public WorkflowInitialStepImpl(final WebTester tester, final JIRAEnvironmentData environmentData, final int logIndentLevel, String workflowName) {
        this(tester, environmentData, workflowName);
    }

    @Inject
    public WorkflowInitialStepImpl(final WebTester tester, final JIRAEnvironmentData environmentData, String workflowName) {
        this.tester = tester;
        this.transition = new WorkflowTransitionImpl(tester, environmentData, 2);
        this.workflowName = workflowName;

        initEditWorkflowDesignerToAccessInitialStep();
    }

    private void initEditWorkflowDesignerToAccessInitialStep() {
        String xsrfToken = new HtmlPage(this.tester).getXsrfToken();
        this.tester.gotoPage("/secure/admin/workflows/EditWorkflowDispatcher.jspa?atl_token=" + xsrfToken + "&wfName=" + workflowName);
    }

    @Override
    public WorkflowTransition createTransition() {
        tester.gotoPage("/secure/admin/workflows/ViewWorkflowTransition.jspa?workflowMode=draft&workflowName=" + workflowName + "&workflowTransition=1&descriptorTab=validators&");
        return transition;
    }

}
