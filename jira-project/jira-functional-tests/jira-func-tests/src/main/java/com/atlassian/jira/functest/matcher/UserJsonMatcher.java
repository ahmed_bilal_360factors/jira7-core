package com.atlassian.jira.functest.matcher;

import com.atlassian.jira.testkit.client.restclient.UserJson;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class UserJsonMatcher {
    public static Matcher<UserJson> matcher(final Matcher<String> usernameMatcher,
                                            final Matcher<Boolean> activeMatcher) {
        return new TypeSafeMatcher<UserJson>() {
            @Override
            protected boolean matchesSafely(final UserJson item) {
                return usernameMatcher.matches(item.name)
                        && activeMatcher.matches(item.active);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendValue(usernameMatcher);
            }
        };
    }
}
