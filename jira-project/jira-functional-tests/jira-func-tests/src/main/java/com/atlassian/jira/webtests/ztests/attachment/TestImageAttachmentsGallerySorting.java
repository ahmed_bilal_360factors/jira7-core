package com.atlassian.jira.webtests.ztests.attachment;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock;
import com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock.ViewMode;
import com.atlassian.jira.functest.framework.navigation.issue.ImageAttachmentsGallery;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.util.collect.CollectionBuilder;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Responsible for holding tests that verify that the image attachments shown in the image gallery on the view issue
 * page can be sorted using a key (i.e. name, date ...) in ascending or descending order.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.BROWSING})
@LoginAs(user = FunctTestConstants.ADMIN_USERNAME)
public class TestImageAttachmentsGallerySorting extends AbstractTestAttachmentsBlockSortingOnViewIssue {

    @Test
    public void testAttachmentsDefaultToSortingByNameInDescendingOrder() throws Exception {
        final List<ImageAttachmentsGallery.ImageAttachmentItem> expectedImageAttachments =
                CollectionBuilder.newBuilder(
                        new ImageAttachmentsGallery.ImageAttachmentItem("_fil\u00E5e", "0.0 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("[#JRA-18780] Test Issue 123 - Atlassian.pdf", "193 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("[#JRA-18780] Test Issue 123 - Printable.pdf", "98 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("200px-FCB.svg.png", "16 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("235px-Floppy_disk_2009_G1.jpg", "8 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("235px-Floppy_disk_2009_G1.jpg", "8 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("a", "0.0 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("a1k4BJwT.jpg.part", "22 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("\u00E1 file", "0.0 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("build.xml", "1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("catalina.sh", "12 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("license.txt", "1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("license.txt", "1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("pom.xml", "5 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("pom.xml", "2 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("Tickspot", "0.1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("tropical-desktop-wallpaper-1280x1024.jpg", "115 kB")
                ).asList();

        final List<ImageAttachmentsGallery.ImageAttachmentItem> actualImageAttachments =
                navigation.issue().attachments("HSP-1", ViewMode.GALLERY).gallery().get();

        assertEquals(expectedImageAttachments, actualImageAttachments);
    }

    @Test
    public void testCanSortAttachmentsByFileNameInAscendingOrder() throws Exception {
        final List<ImageAttachmentsGallery.ImageAttachmentItem> expectedImageAttachments =
                CollectionBuilder.newBuilder(
                        new ImageAttachmentsGallery.ImageAttachmentItem("_fil\u00E5e", "0.0 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("[#JRA-18780] Test Issue 123 - Atlassian.pdf", "193 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("[#JRA-18780] Test Issue 123 - Printable.pdf", "98 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("200px-FCB.svg.png", "16 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("235px-Floppy_disk_2009_G1.jpg", "8 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("235px-Floppy_disk_2009_G1.jpg", "8 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("a", "0.0 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("a1k4BJwT.jpg.part", "22 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("\u00E1 file", "0.0 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("build.xml", "1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("catalina.sh", "12 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("license.txt", "1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("license.txt", "1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("pom.xml", "5 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("pom.xml", "2 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("Tickspot", "0.1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("tropical-desktop-wallpaper-1280x1024.jpg", "115 kB")
                ).asList();

        final AttachmentsBlock attachmentsBlock = navigation.issue().attachments("HSP-1", ViewMode.GALLERY);
        attachmentsBlock.sort(AttachmentsBlock.Sort.Key.NAME, AttachmentsBlock.Sort.Direction.ASCENDING);

        final List<ImageAttachmentsGallery.ImageAttachmentItem> actualImageAttachments =
                attachmentsBlock.gallery().get();

        assertEquals(expectedImageAttachments, actualImageAttachments);
        verifySortingSettingIsStickyDuringTheSession(expectedImageAttachments);
    }

    @Test
    public void testCanSortAttachmentsByFileNameInDescendingOrder() throws Exception {
        final List<ImageAttachmentsGallery.ImageAttachmentItem> expectedImageAttachments =
                CollectionBuilder.newBuilder(
                        new ImageAttachmentsGallery.ImageAttachmentItem("tropical-desktop-wallpaper-1280x1024.jpg", "115 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("Tickspot", "0.1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("pom.xml", "2 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("pom.xml", "5 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("license.txt", "1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("license.txt", "1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("catalina.sh", "12 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("build.xml", "1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("\u00E1 file", "0.0 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("a1k4BJwT.jpg.part", "22 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("a", "0.0 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("235px-Floppy_disk_2009_G1.jpg", "8 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("235px-Floppy_disk_2009_G1.jpg", "8 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("200px-FCB.svg.png", "16 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("[#JRA-18780] Test Issue 123 - Printable.pdf", "98 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("[#JRA-18780] Test Issue 123 - Atlassian.pdf", "193 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("_fil\u00E5e", "0.0 kB")
                ).asList();

        final AttachmentsBlock attachmentsBlock = navigation.issue().attachments("HSP-1", ViewMode.GALLERY);
        attachmentsBlock.sort(AttachmentsBlock.Sort.Key.NAME, AttachmentsBlock.Sort.Direction.DESCENDING);

        final List<ImageAttachmentsGallery.ImageAttachmentItem> actualImageAttachments =
                attachmentsBlock.gallery().get();

        assertEquals(expectedImageAttachments, actualImageAttachments);
        verifySortingSettingIsStickyDuringTheSession(expectedImageAttachments);
    }

    @Test
    public void testCanSortAttachmentsByDateInAscendingOrder() throws Exception {
        final List<ImageAttachmentsGallery.ImageAttachmentItem> expectedImageAttachments =
                CollectionBuilder.newBuilder(
                        new ImageAttachmentsGallery.ImageAttachmentItem("license.txt", "1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("pom.xml", "2 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("235px-Floppy_disk_2009_G1.jpg", "8 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("license.txt", "1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("200px-FCB.svg.png", "16 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("tropical-desktop-wallpaper-1280x1024.jpg", "115 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("[#JRA-18780] Test Issue 123 - Atlassian.pdf", "193 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("pom.xml", "5 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("235px-Floppy_disk_2009_G1.jpg", "8 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("build.xml", "1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("a1k4BJwT.jpg.part", "22 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("[#JRA-18780] Test Issue 123 - Printable.pdf", "98 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("Tickspot", "0.1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("a", "0.0 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("_fil\u00E5e", "0.0 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("\u00E1 file", "0.0 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("catalina.sh", "12 kB")
                ).asList();

        final AttachmentsBlock attachmentsBlock = navigation.issue().attachments("HSP-1", ViewMode.GALLERY);
        attachmentsBlock.sort(AttachmentsBlock.Sort.Key.DATE, AttachmentsBlock.Sort.Direction.ASCENDING);

        final List<ImageAttachmentsGallery.ImageAttachmentItem> actualImageAttachments =
                attachmentsBlock.gallery().get();

        assertEquals(expectedImageAttachments, actualImageAttachments);
        verifySortingSettingIsStickyDuringTheSession(expectedImageAttachments);
    }

    @Test
    public void testCanSortAttachmentsByDateInDescendingOrder() throws Exception {
        final List<ImageAttachmentsGallery.ImageAttachmentItem> expectedImageAttachments =
                CollectionBuilder.newBuilder(
                        new ImageAttachmentsGallery.ImageAttachmentItem("catalina.sh", "12 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("\u00E1 file", "0.0 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("_fil\u00E5e", "0.0 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("a", "0.0 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("Tickspot", "0.1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("[#JRA-18780] Test Issue 123 - Printable.pdf", "98 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("a1k4BJwT.jpg.part", "22 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("build.xml", "1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("235px-Floppy_disk_2009_G1.jpg", "8 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("pom.xml", "5 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("[#JRA-18780] Test Issue 123 - Atlassian.pdf", "193 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("tropical-desktop-wallpaper-1280x1024.jpg", "115 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("200px-FCB.svg.png", "16 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("license.txt", "1 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("235px-Floppy_disk_2009_G1.jpg", "8 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("pom.xml", "2 kB"),
                        new ImageAttachmentsGallery.ImageAttachmentItem("license.txt", "1 kB")
                ).asList();

        final AttachmentsBlock attachmentsBlock = navigation.issue().attachments("HSP-1", ViewMode.GALLERY);
        attachmentsBlock.sort(AttachmentsBlock.Sort.Key.DATE, AttachmentsBlock.Sort.Direction.DESCENDING);

        final List<ImageAttachmentsGallery.ImageAttachmentItem> actualImageAttachments =
                attachmentsBlock.gallery().get();

        assertEquals(expectedImageAttachments, actualImageAttachments);
        verifySortingSettingIsStickyDuringTheSession(expectedImageAttachments);
    }

    private void verifySortingSettingIsStickyDuringTheSession
            (final List<ImageAttachmentsGallery.ImageAttachmentItem> expectedFileAttachmentsList) {
        navigation.gotoDashboard();

        final AttachmentsBlock attachmentsBlock = navigation.issue().attachments("HSP-1", ViewMode.GALLERY);
        final List<ImageAttachmentsGallery.ImageAttachmentItem> actualImageAttachments =
                attachmentsBlock.gallery().get();

        assertEquals(expectedFileAttachmentsList, actualImageAttachments);
    }
}
