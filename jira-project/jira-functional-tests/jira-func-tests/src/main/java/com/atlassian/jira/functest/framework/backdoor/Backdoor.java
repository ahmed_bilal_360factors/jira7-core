package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.functest.framework.backdoor.application.ApplicationControl;
import com.atlassian.jira.functest.framework.backdoor.upgrade.UpgradeControl;
import com.atlassian.jira.functest.framework.util.IssueTableClient;
import com.atlassian.jira.functest.framework.util.SearchersClient;
import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.testkit.client.AdvancedSettingsControl;
import com.atlassian.jira.testkit.client.AttachmentsControl;
import com.atlassian.jira.testkit.client.CustomFieldsControl;
import com.atlassian.jira.testkit.client.DarkFeaturesControl;
import com.atlassian.jira.testkit.client.DashboardControl;
import com.atlassian.jira.testkit.client.DataImportControl;
import com.atlassian.jira.testkit.client.GeneralConfigurationControl;
import com.atlassian.jira.testkit.client.I18nControl;
import com.atlassian.jira.testkit.client.IssueLinkingControl;
import com.atlassian.jira.testkit.client.IssueSecuritySchemesControl;
import com.atlassian.jira.testkit.client.IssueTypeControl;
import com.atlassian.jira.testkit.client.MailServersControl;
import com.atlassian.jira.testkit.client.PermissionSchemesControl;
import com.atlassian.jira.testkit.client.PermissionsControl;
import com.atlassian.jira.testkit.client.ScreensControl;
import com.atlassian.jira.testkit.client.SearchRequestControl;
import com.atlassian.jira.testkit.client.ServicesControl;
import com.atlassian.jira.testkit.client.SubtaskControl;
import com.atlassian.jira.testkit.client.SystemPropertiesControl;
import com.atlassian.jira.testkit.client.UsersAndGroupsControl;
import com.atlassian.jira.testkit.client.WebSudoControl;
import com.atlassian.jira.testkit.client.WorkflowSchemesControl;
import com.atlassian.jira.testkit.client.restclient.ComponentClient;
import com.atlassian.jira.testkit.client.restclient.ProjectRoleClient;
import com.atlassian.jira.testkit.client.restclient.SearchClient;
import com.atlassian.jira.testkit.client.restclient.VersionClient;
import com.atlassian.jira.webtests.LicenseKeys;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;

/**
 * @since v5.2
 */
public class Backdoor {
    private final com.atlassian.jira.testkit.client.Backdoor testkit;
    private final ApplicationPropertiesControl applicationPropertiesControl;
    private final UserProfileControl userProfileControl;
    private final UserManagerControl userManagerControl;
    private final GroupManagerControl groupManagerControl;
    private final TestRunnerControl testRunnerControl;
    private final EntityEngineControl entityEngineControl;
    private final FilterSubscriptionControl filterSubscriptionControl;
    private final IssueTypeScreenSchemesControl issueTypeScreenSchemes;
    private final BarrierControl barrierControl;
    private final PermissionsControl permissionsControl;
    private final FieldConfigurationControlExt fieldConfigurationControl;
    private final ScreensControlExt screensControl;
    private final SchedulerControl schedulerControl;
    private final ManagedConfigurationControl managedConfigurationControl;
    private final IndexingControl indexingControl;
    private final ProjectControlExt projectControl;
    private final IssuesControl issueNavControl;
    private final FiltersClient filtersControl;
    private final IssueTableClient issueTableClient;
    private final SearchersClient searchersClient;
    private final WorkflowsControlExt workflowsControlExt;
    private final PluginsControlExt pluginsControl;
    private final ColumnControl columnControl;
    private final VersionClient versionClient;
    private final ComponentClient componentClient;
    private final PluginIndexConfigurationControl pluginIndexConfigurationControl;
    private final PermissionSchemesControl permissionSchemesControl;
    private final ApplicationRoleControl applicationRoleControl;
    private final LicenseControl licenseControl;
    private final ResourceBundleCacheControl resourceBundleCacheControl;
    private final DeprecatedWebResourceControl deprecatedWebResourceControl;
    private final ApplicationControl applicationControl;
    private final ServerInfoControl serverInfoControl;
    private final ComponentManagerControl componentManagerControl;
    private final EventClient eventControl;
    private final HelpUrlsControl helpUrlsControl;
    private final OutgoingMailControl outgoingMailControl;
    private final AnalyticsEventsControl analyticsEventsControl;
    private final VCacheControl vCacheControl;
    private final InstanceFeaturesControl instanceFeaturesControl;
    private final StatisticsControl statisticsControl;
    private final IssueSecuritySchemesControl issueSecuritySchemesControl;
    private final CacheCheckControl cacheCheckControl;
    private final FlagsControl flagsControl;
    private final ZeroDowntimeControl zeroDowntimeControl;
    private final UpgradeControl upgradeControl;
    private final ServerControl serverControl;

    public Backdoor(JIRAEnvironmentData environmentData) {
        testkit = new com.atlassian.jira.testkit.client.Backdoor(environmentData);
        applicationPropertiesControl = new ApplicationPropertiesControl(environmentData);
        entityEngineControl = new EntityEngineControl(environmentData);
        userProfileControl = new UserProfileControl(environmentData);
        userManagerControl = new UserManagerControl(environmentData);
        groupManagerControl = new GroupManagerControl(environmentData);
        testRunnerControl = new TestRunnerControl(environmentData);
        filterSubscriptionControl = new FilterSubscriptionControl(environmentData);
        issueTypeScreenSchemes = new IssueTypeScreenSchemesControl(environmentData);
        barrierControl = new BarrierControl(environmentData);
        permissionsControl = new PermissionsControl(environmentData);
        fieldConfigurationControl = new FieldConfigurationControlExt(environmentData);
        screensControl = new ScreensControlExt(environmentData);
        schedulerControl = new SchedulerControl(environmentData);
        this.managedConfigurationControl = new ManagedConfigurationControl(environmentData);
        indexingControl = new IndexingControl(environmentData);
        projectControl = new ProjectControlExt(environmentData);
        issueNavControl = new IssuesControl(environmentData);
        filtersControl = new FiltersClient(environmentData);
        issueTableClient = new IssueTableClient(environmentData);
        searchersClient = new SearchersClient(environmentData);
        workflowsControlExt = new WorkflowsControlExt(environmentData);
        pluginsControl = new PluginsControlExt(environmentData);
        columnControl = new ColumnControl(environmentData);
        componentClient = new ComponentClient(environmentData);
        versionClient = new VersionClient(environmentData);
        pluginIndexConfigurationControl = new PluginIndexConfigurationControl(environmentData);
        permissionSchemesControl = new PermissionSchemesControl(environmentData);
        applicationRoleControl = new ApplicationRoleControl(environmentData);
        licenseControl = new LicenseControl(environmentData);
        deprecatedWebResourceControl = new DeprecatedWebResourceControl(environmentData);
        this.resourceBundleCacheControl = new ResourceBundleCacheControl(environmentData);
        applicationControl = new ApplicationControl(environmentData);
        serverInfoControl = new ServerInfoControl(environmentData);
        componentManagerControl = new ComponentManagerControl(environmentData);
        eventControl = new EventClient(environmentData);
        helpUrlsControl = new HelpUrlsControl(environmentData);
        outgoingMailControl = new OutgoingMailControl(environmentData);
        analyticsEventsControl = new AnalyticsEventsControl(environmentData);
        vCacheControl = new VCacheControl(environmentData);
        instanceFeaturesControl = new InstanceFeaturesControl(environmentData);
        statisticsControl = new StatisticsControl(environmentData);
        issueSecuritySchemesControl = new IssueSecuritySchemesControl(environmentData);
        cacheCheckControl = new CacheCheckControl(environmentData);
        flagsControl = new FlagsControl(environmentData, this);
        zeroDowntimeControl = new ZeroDowntimeControl(environmentData);
        upgradeControl = new UpgradeControl(environmentData);
        serverControl = new ServerControl(environmentData);
    }

    public com.atlassian.jira.testkit.client.Backdoor getTestkit() {
        return testkit;
    }

    /**
     * @param fileName xml file name
     * @deprecated use {@link #restoreDataFromResource(String, String)} instead
     */
    @Deprecated
    public void restoreData(String fileName) {
        testkit.restoreData(fileName, LicenseKeys.COMMERCIAL.getLicenseString());
    }

    public void restoreBlankInstance() {
        testkit.restoreBlankInstance(LicenseKeys.COMMERCIAL.getLicenseString());
    }

    /**
     *
     * @param license the license to use during restoration
     * @deprecated use {@link Backdoor#restoreBlankInstance(License)} instead, since 7.0.1
     */
    @Deprecated
    public void restoreBlankInstance(String license) {
        testkit.restoreBlankInstance(license);
    }

    public void restoreBlankInstance(License license) {
        testkit.restoreBlankInstance(license.getLicenseString());
    }

    /**
     * Restores blank instance with {@link com.atlassian.jira.config.CoreFeatures#LICENSE_ROLES_ENABLED} feature enabled,
     * so that renaissance migration task will be run.
     * <p>
     * Temporary method that will be removed once we get rid of Dark Ages mode.
     *
     * @param license to set after restore is completed.
     * @deprecated since 7.0.1 as roles are always enabled, use {@link Backdoor#restoreBlankInstance(License)} instead
     */
    @Deprecated
    public void restoreBlankInstanceWithRolesEnabled(License license) {
        restoreDataFromResource("blankprojects-roles-enabled-6004.xml", license.getLicenseString());
    }

    public void restoreDataFromResource(String resource) {
        testkit.restoreDataFromResource(resource, LicenseKeys.COMMERCIAL.getLicenseString());
    }

    @Deprecated
    public void restoreData(String xmlFileName, String license) {
        testkit.restoreData(xmlFileName, license);
    }

    public void restoreDataFromResource(String resourcePath, String license) {
        testkit.restoreDataFromResource(resourcePath, license);
    }

    public IssuesControl issueNavControl() {
        return issueNavControl;
    }

    public AttachmentsControl attachments() {
        return testkit.attachments();
    }

    public ScreensControlExt screens() {
        return screensControl;
    }

    public SchedulerControl scheduler() {
        return schedulerControl;
    }

    public UsersAndGroupsControl usersAndGroups() {
        return testkit.usersAndGroups();
    }

    public com.atlassian.jira.testkit.client.IssuesControl issues() {
        return testkit.issues();
    }

    public I18nControl i18n() {
        return testkit.i18n();
    }

    public DarkFeaturesControl darkFeatures() {
        return testkit.darkFeatures();
    }

    public PluginsControlExt plugins() {
        return pluginsControl;
    }

    public PermissionsControl permissions() {
        return permissionsControl;
    }

    public FiltersClient filters() {
        return filtersControl;
    }

    public ApplicationPropertiesControl applicationProperties() {
        return applicationPropertiesControl;
    }

    public EntityEngineControl entityEngine() {
        return entityEngineControl;
    }

    public SystemPropertiesControl systemProperties() {
        return testkit.systemProperties();
    }

    public ProjectControlExt project() {
        return projectControl;
    }

    public PermissionSchemesControl permissionSchemes() {
        return permissionSchemesControl;
    }

    public IssueTypeScreenSchemesControl issueTypeScreenSchemes() {
        return issueTypeScreenSchemes;
    }

    public ScreensControl screensControl() {
        return testkit.screensControl();
    }

    public MailServersControl mailServers() {
        return testkit.mailServers();
    }

    public SearchRequestControl searchRequests() {
        return testkit.searchRequests();
    }

    public UserProfileControl userProfile() {
        return userProfileControl;
    }

    public UserManagerControl userManager() {
        return userManagerControl;
    }

    public GroupManagerControl groupManager() {
        return groupManagerControl;
    }

    public ServicesControl services() {
        return testkit.services();
    }

    public DataImportControl dataImport() {
        return testkit.dataImport();
    }

    public TestRunnerControl testRunner() {
        return testRunnerControl;
    }

    public FieldConfigurationControlExt fieldConfiguration() {
        return fieldConfigurationControl;
    }

    public IssueTypeControl issueType() {
        return testkit.issueType();
    }

    public SubtaskControl subtask() {
        return testkit.subtask();
    }

    public WebSudoControl websudo() {
        return testkit.websudo();
    }

    public DashboardControl dashboard() {
        return testkit.dashboard();
    }

    public BarrierControl barrier() {
        return barrierControl;
    }

    public IndexingControl indexing() {
        return indexingControl;
    }

    public GeneralConfigurationControl generalConfiguration() {
        return testkit.generalConfiguration();
    }

    public AdvancedSettingsControl advancedSettings() {
        return testkit.advancedSettings();
    }

    public CustomFieldsControl customFields() {
        return testkit.customFields();
    }

    public IssueLinkingControl issueLinking() {
        return testkit.issueLinking();
    }

    public WorkflowsControlExt workflow() {
        return workflowsControlExt;
    }

    public SearchClient search() {
        return testkit.search();
    }

    public WorkflowSchemesControl workflowSchemes() {
        return testkit.workflowSchemes();
    }

    public ProjectRoleClient projectRole() {
        return testkit.projectRole();
    }

    public FilterSubscriptionControl filterSubscriptions() {
        return filterSubscriptionControl;
    }

    public ManagedConfigurationControl managedConfiguration() {
        return managedConfigurationControl;
    }

    public IssueTableClient issueTableClient() {
        return issueTableClient;
    }

    public SearchersClient searchersClient() {
        return searchersClient;
    }

    public ColumnControl columnControl() {
        return columnControl;
    }

    public VersionClient versions() {
        return versionClient;
    }

    public ComponentClient components() {
        return componentClient;
    }

    public ResourceBundleCacheControl resourceBundleCacheControl() {
        return resourceBundleCacheControl;
    }

    public PluginIndexConfigurationControl getPluginIndexConfigurationControl() {
        return pluginIndexConfigurationControl;
    }

    public ApplicationRoleControl applicationRoles() {
        return applicationRoleControl;
    }

    public LicenseControl license() {
        return licenseControl;
    }

    public DeprecatedWebResourceControl getDeprecatedWebResourceControl() {
        return deprecatedWebResourceControl;
    }

    public ManagedPermissions permissionsOf(String userName) {
        return new ManagedPermissions(usersAndGroups(), permissionSchemes(), project(), userName);
    }

    public ApplicationControl getApplicationControl() {
        return applicationControl;
    }

    public ServerInfoControl serverInfo() {
        return serverInfoControl;
    }

    public ComponentManagerControl componentManager() {
        return componentManagerControl;
    }

    public EventClient events() {
        return eventControl;
    }

    public HelpUrlsControl helpUrls() {
        return helpUrlsControl;
    }

    public OutgoingMailControl outgoingMailControl() {
        return outgoingMailControl;
    }

    public AnalyticsEventsControl analyticsEventsControl() {
        return analyticsEventsControl;
    }

    public VCacheControl vCacheControl() {
        return vCacheControl;
    }

    public InstanceFeaturesControl instanceFeaturesControl() {
        return instanceFeaturesControl;
    }

    public StatisticsControl statisticsControl() {
        return statisticsControl;
    }

    public IssueSecuritySchemesControl issueSecuritySchemesControl() {
        return issueSecuritySchemesControl;
    }

    public FlagsControl flags() {
        return flagsControl;
    }

    public CacheCheckControl cacheCheckControl() {
        return cacheCheckControl;
    }

    public ZeroDowntimeControl zdu() {
        return zeroDowntimeControl;
    }
    public UpgradeControl upgradeControl() {
        return upgradeControl;
    }

    public ServerControl server() {
        return serverControl;
    }
}
