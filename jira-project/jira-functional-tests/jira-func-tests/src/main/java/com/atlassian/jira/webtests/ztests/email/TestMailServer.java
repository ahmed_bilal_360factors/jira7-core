package com.atlassian.jira.webtests.ztests.email;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.BROWSING, Category.EMAIL})
@LoginAs(user = ADMIN_USERNAME)
// passing as well, however this test is flaky as it sometimes fails depending on test execution order and it has ugly
// assertions on the raw html output. This should be rewritten.
public class TestMailServer extends BaseJiraFuncTest {
    protected static final String FIELD_NAME = "name";
    protected static final String FIELD_FROM = "from";
    protected static final String FIELD_PREFIX = "prefix";
    protected static final String FIELD_SERVER_NAME = "serverName";
    protected static final String FIELD_USERNAME = "username";
    protected static final String FIELD_PASSWORD = "password";
    protected static final String VALUE_NAME_SMTP = "name SMTP";
    protected static final String VALUE_NAME_POP = "name POP";
    protected static final String VALUE_FROM = "from@atlassian.com";
    protected static final String VALUE_PREFIX = "prefix";
    protected static final String VALUE_SERVER_NAME = "server name";
    protected static final String VALUE_USERNAME = "username";
    protected static final String VALUE_PASSWORD = "password";
    protected static final String TITLE_SMTP_MAIL_SERVERS = "<h3 class=\"formtitle\">SMTP Mail Server</h3>";
    protected static final String TITLE_ADD_SMTP_MAIL_SERVER = "<h3 class=\"formtitle\">Add SMTP Mail Server</h3>";
    protected static final String TITLE_POP_MAIL_SERVERS = "<h3>POP / IMAP Mail Servers</h3>";
    protected static final String TITLE_ADD_POP_MAIL_SERVER = "<h3 class=\"formtitle\">Add POP / IMAP Mail Server</h3>";
    protected static final String TITLE_DELETE_MAIL_SERVER = "<h3 class=\"formtitle\">Delete Mail Server</h3>";
    protected static final String TITLE_SUPPORT_REQUEST = "<h3 class=\"formtitle\">Support Request</h3>";
    protected static final String TITLE_SEND_EMAIL = "<h3 class=\"formtitle\">Send email</h3>";
    protected static final String TITLE_MAIL_QUEUE = "<h3 class=\"formtitle\">Mail queue</h3>";
    protected static final String LINK_TEXT_CONFIG_NEW_SMTP = "Configure new SMTP mail server";
    protected static final String LINK_TEXT_CONFIG_NEW_POP = "Add POP / IMAP mail server";
    protected static final String LINK_DELETE_SMTP = "deleteSMTP";
    protected static final String LABEL_NO_SMTP_SUPPORT_REQUEST_PART_ONE = "<span class=\"note\">Note</span>: To send a support request you need to";
    protected static final String LABEL_NO_SMTP_SUPPORT_REQUEST_PART_TWO = "configure</a> a mail server.";
    protected static final String LABEL_NO_SMTP_SEND_EMAIL_PART_ONE = "To send email you need to";
    protected static final String LABEL_NO_SMTP_SEND_EMAIL_PART_TWO = "configure</a> a mail server.";
    protected static final String LABEL_NO_SMTP_MAIL_QUEUE_PART_ONE = "There is no default ";
    protected static final String LABEL_NO_SMTP_MAIL_QUEUE_PART_TWO = "mail server</a>, so mails will not be sent.";
    protected static final String LABEL_NO_POP_MAIL_SERVER = "You do not currently have any POP / IMAP servers configured.";
    protected static final String BUTTON_CANCEL = "cancelButton";
    protected static final String BUTTON_ADD = "Add";
    protected static final String BUTTON_DELETE = "Delete";
    protected static final String ERROR_SPECIFY_SERVER = "You must specify the name of this Mail Server.";
    protected static final String ERROR_SPECIFY_ADDRESS = "You must specify a valid from address";
    protected static final String ERROR_SPECIFY_PREFIX = "You must specify an email prefix";
    protected static final String ERROR_SPECIFY_SERVER_DETAILS = "You must specify a host name or a JNDI location.";
    protected static final String ERROR_SPECIFY_SERVER_LOCATION = "You must specify the location of the server.";
    protected static final String ERROR_SPECIFY_USERNAME = "You must specify a username";
    protected static final String ERROR_SPECIFY_PASSWORD = "You must specify a password";
    private static final String FIELD_PORT = "port";
    private static final String BUTTON_UPDATE = "Update";
    private static final String ERROR_ILLEGAL_PORT = "SMTP port must be a number between 0 and 65535";
    private static final String TEST_EXISTING_MAIL_SERVER_ID = "sendTestEmail";

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testPOPServerConfiguration() {
        backdoor.restoreBlankInstance();
        configureNewPOPServer();
        checkPOPServerExists();
        deletePOPServer();
    }

    @Test
    public void testSMTPServerConfiguration() {
        backdoor.restoreBlankInstance();
        validateSMTPForm();
        configureNewSMTPServer();
        checkSMTPServerExists();
        deleteSMTPServer();
        checkNoSMTPServer();
    }

    /**
     * Check 'Add SMTP Mail Server' form validation<br>
     * Check cancel button
     */
    private void validateSMTPForm() {
        logger.log("Mail Server - Validating empty form for new SMTP mail server setup");
        navigation.gotoAdminSection(Navigation.AdminSection.OUTGOING_MAIL);
        tester.assertTextPresent(TITLE_SMTP_MAIL_SERVERS);

        // try to meet the assumptions of this test - no existing mail servers
        if (tester.getDialog().isLinkPresent(LINK_DELETE_SMTP)) {
            tester.clickLink(LINK_DELETE_SMTP);
            tester.assertTextPresent(TITLE_DELETE_MAIL_SERVER);
            tester.assertTextPresent("Are you sure you want to delete");
            tester.submit(BUTTON_DELETE);
            assertions.assertNodeByIdDoesNotExist(TEST_EXISTING_MAIL_SERVER_ID);
        }
        tester.clickLinkWithText(LINK_TEXT_CONFIG_NEW_SMTP);
        tester.assertTextPresent(TITLE_ADD_SMTP_MAIL_SERVER);
        tester.submit(BUTTON_ADD);
        tester.assertTextPresent(ERROR_SPECIFY_SERVER);
        tester.assertTextPresent(ERROR_SPECIFY_ADDRESS);
        tester.assertTextPresent(ERROR_SPECIFY_PREFIX);

        tester.setFormElement(FIELD_NAME, VALUE_NAME_SMTP);
        tester.setFormElement(FIELD_FROM, VALUE_FROM);
        tester.setFormElement(FIELD_PREFIX, VALUE_PREFIX);
        tester.submit(BUTTON_ADD);
        tester.assertTextPresent(ERROR_SPECIFY_SERVER_DETAILS);

        tester.setFormElement(FIELD_SERVER_NAME, VALUE_SERVER_NAME);
        tester.setFormElement(FIELD_USERNAME, VALUE_USERNAME);
        tester.setFormElement(FIELD_PASSWORD, VALUE_PASSWORD);
        tester.setFormElement(FIELD_PORT, String.valueOf(0xFFFF + 1));
        tester.submit(BUTTON_ADD);
        tester.assertTextPresent(ERROR_ILLEGAL_PORT);

        tester.submit(BUTTON_ADD);
        tester.assertTextPresent(ERROR_ILLEGAL_PORT);

        tester.setFormElement(FIELD_PORT, String.valueOf(-1));
        tester.submit(BUTTON_ADD);
        tester.assertTextPresent(ERROR_ILLEGAL_PORT);
    }

    /**
     * Add a new SMTP mail server<br>
     * Check that it has been added and correct details displayed<br>
     * Check no more SMTP servers can be added
     */
    private void configureNewSMTPServer() {
        logger.log("Mail Server - Configuring a new SMTP mail server");
        navigation.gotoAdminSection(Navigation.AdminSection.OUTGOING_MAIL);
        tester.assertTextPresent(TITLE_SMTP_MAIL_SERVERS);

        tester.clickLinkWithText(LINK_TEXT_CONFIG_NEW_SMTP);
        tester.assertTextPresent(TITLE_ADD_SMTP_MAIL_SERVER);

        tester.setFormElement(FIELD_NAME, VALUE_NAME_SMTP);
        tester.setFormElement(FIELD_FROM, VALUE_FROM);
        tester.setFormElement(FIELD_PREFIX, VALUE_PREFIX);
        tester.setFormElement(FIELD_SERVER_NAME, VALUE_SERVER_NAME);
        tester.setFormElement(FIELD_USERNAME, VALUE_USERNAME);
        tester.setFormElement(FIELD_PASSWORD, VALUE_PASSWORD);
        tester.setFormElement(FIELD_PORT, String.valueOf(0xFFFF));
        tester.submit(BUTTON_ADD);
        tester.assertTextPresent(TITLE_SMTP_MAIL_SERVERS);
        tester.assertTextPresent(VALUE_NAME_SMTP);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]
                {"From:", VALUE_FROM,
                        "Prefix:", VALUE_PREFIX,
                        "Host:", VALUE_SERVER_NAME,
                        "SMTP Port:", String.valueOf(0xFFFF),
                        "Username:", VALUE_USERNAME});
        tester.assertLinkNotPresentWithText(LINK_TEXT_CONFIG_NEW_SMTP);

        // sometimes the id is different than expected edit_10000, harvest it first
        final String deleteLink = tester.getDialog().getElement(LINK_DELETE_SMTP).getAttribute("href");
        final String editId = "edit_" + StringUtils.substringAfter(deleteLink, "id=");
        tester.clickLink(editId);
        tester.setFormElement(FIELD_PORT, String.valueOf(0));
        tester.setFormElement(FIELD_PASSWORD, VALUE_PASSWORD);
        tester.submit(BUTTON_UPDATE);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"SMTP Port:", "0"});
    }

    /**
     * Assumptions: SMTP Mail Server set<br>
     * Checks that SMTP server exists<br>
     * Checks that the following pages have NO message about no SMTP mail server set<br>
     * <li>Support Request
     * <li>Send Email
     * <li>Mail Queue
     */
    private void checkSMTPServerExists() {
        logger.log("Mail Server - Checking labels when a SMTP server is setup");
        navigation.gotoAdminSection(Navigation.AdminSection.OUTGOING_MAIL);
        tester.assertLinkNotPresentWithText(LINK_TEXT_CONFIG_NEW_SMTP);

        navigation.gotoAdminSection(Navigation.AdminSection.SEND_EMAIL);
        tester.assertTextPresent(TITLE_SEND_EMAIL);
        tester.assertTextNotPresent(LABEL_NO_SMTP_SEND_EMAIL_PART_ONE);
        tester.assertTextNotPresent(LABEL_NO_SMTP_SEND_EMAIL_PART_TWO);

        navigation.gotoAdminSection(Navigation.AdminSection.MAIL_QUEUE);
        tester.assertTextPresent(TITLE_MAIL_QUEUE);
        tester.assertTextNotPresent(LABEL_NO_SMTP_MAIL_QUEUE_PART_ONE);
        tester.assertTextNotPresent(LABEL_NO_SMTP_MAIL_QUEUE_PART_TWO);
    }

    /**
     * Deletes the SMTP Server<br>
     * Checks if a new SMTP server can be configured
     */
    private void deleteSMTPServer() {
        logger.log("Mail Server - Deleting the SMTP server");
        navigation.gotoAdminSection(Navigation.AdminSection.OUTGOING_MAIL);
        tester.assertTextPresent(TITLE_SMTP_MAIL_SERVERS);
        assertions.assertNodeByIdExists(TEST_EXISTING_MAIL_SERVER_ID);

        tester.clickLink(LINK_DELETE_SMTP);
        tester.assertTextPresent(TITLE_DELETE_MAIL_SERVER);
        tester.assertTextPresent("Are you sure you want to delete <b>" + VALUE_NAME_SMTP + "</b>?");
        tester.submit(BUTTON_DELETE);

        assertions.assertNodeByIdDoesNotExist(TEST_EXISTING_MAIL_SERVER_ID);
        tester.assertLinkPresentWithText(LINK_TEXT_CONFIG_NEW_SMTP);
    }

    /**
     * Assumptions: NO SMTP Mail Server set<br>
     * Checks that SMTP server NOT exists<br>
     * Checks that the following pages have a message about no SMTP mail server set<br>
     * <li>Support Request
     * <li>Send Email
     * <li>Mail Queue
     */
    private void checkNoSMTPServer() {
        logger.log("Mail Server - Checking labels when a SMTP server is NOT setup");
        navigation.gotoAdminSection(Navigation.AdminSection.OUTGOING_MAIL);
        assertions.assertNodeByIdDoesNotExist(TEST_EXISTING_MAIL_SERVER_ID);
        tester.assertLinkPresentWithText(LINK_TEXT_CONFIG_NEW_SMTP);

        navigation.gotoAdminSection(Navigation.AdminSection.SEND_EMAIL);
        tester.assertTextPresent(TITLE_SEND_EMAIL);
        tester.assertTextPresent(LABEL_NO_SMTP_SEND_EMAIL_PART_ONE);
        tester.assertTextPresent(LABEL_NO_SMTP_SEND_EMAIL_PART_TWO);

        navigation.gotoAdminSection(Navigation.AdminSection.MAIL_QUEUE);
        tester.assertTextPresent(TITLE_MAIL_QUEUE);
        tester.assertTextPresent(LABEL_NO_SMTP_MAIL_QUEUE_PART_ONE);
        tester.assertTextPresent(LABEL_NO_SMTP_MAIL_QUEUE_PART_TWO);
    }

    /**
     * Check 'Add POP / IMAP Mail Server' form validation<br>
     * Check cancel button
     */
    @Test
    public void testValidatePOPForm() {
        logger.log("Mail Server - Validayting empty form for new POP mail server setup");
        backdoor.restoreBlankInstance();
        navigation.gotoAdminSection(Navigation.AdminSection.INCOMING_MAIL);
        tester.assertTextPresent(TITLE_POP_MAIL_SERVERS);

        tester.clickLinkWithText(LINK_TEXT_CONFIG_NEW_POP);
        tester.assertTextPresent(TITLE_ADD_POP_MAIL_SERVER);
        tester.submit(BUTTON_ADD);
        tester.assertTextPresent(ERROR_SPECIFY_SERVER);
        tester.assertTextPresent(ERROR_SPECIFY_SERVER_LOCATION);
        tester.assertTextPresent(ERROR_SPECIFY_USERNAME);
        tester.assertTextPresent(ERROR_SPECIFY_PASSWORD);
        tester.setFormElement(FIELD_NAME, VALUE_NAME_POP);
        tester.setFormElement(FIELD_SERVER_NAME, VALUE_SERVER_NAME);
        tester.setFormElement(FIELD_USERNAME, VALUE_USERNAME);
        tester.setFormElement(FIELD_PASSWORD, VALUE_PASSWORD);
        tester.clickLink(BUTTON_CANCEL);
        //same propblem as testVakudateSMTPForm
//        assertTextPresent(TITLE_POP_MAIL_SERVERS);
    }

    /**
     * Add a new POP mail server<br>
     * Check that it has been added and correct details displayed<br>
     * Check more POP servers can be added
     */
    private void configureNewPOPServer() {
        administration.restoreBlankInstance();
        logger.log("Mail Server - Configuring a new POP mail server");
        navigation.gotoAdminSection(Navigation.AdminSection.INCOMING_MAIL);
        tester.assertTextPresent(TITLE_POP_MAIL_SERVERS);

        configurePopServer(VALUE_NAME_POP, VALUE_SERVER_NAME, "110", VALUE_USERNAME, VALUE_PASSWORD);
        tester.assertTextPresent(TITLE_POP_MAIL_SERVERS);
        tester.assertTextPresent(VALUE_NAME_POP);
        textAssertions.assertTextPresent(locator.page(), "Host: " + VALUE_SERVER_NAME);
        textAssertions.assertTextPresent(locator.page(), "Username: " + VALUE_USERNAME);
        tester.clickLinkWithText(LINK_TEXT_CONFIG_NEW_POP);
    }

    /**
     * Assumptions: POP Mail Server set<br>
     * Checks that POP server does not exist<br>
     */
    private void checkPOPServerExists() {
        logger.log("Mail Server - Checking labels when a POP server is setup");
        navigation.gotoAdminSection(Navigation.AdminSection.INCOMING_MAIL);
        tester.assertTextNotPresent(LABEL_NO_POP_MAIL_SERVER);
    }

    /**
     * Deletes the POP Server with the VALUE_NAME_POP<br>
     * Checks if a new POP server can be configured
     */
    private void deletePOPServer() {
        logger.log("Mail Server - Deleting the POP server");
        navigation.gotoAdminSection(Navigation.AdminSection.INCOMING_MAIL);
        tester.assertTextPresent(TITLE_POP_MAIL_SERVERS);

        tester.clickLink("delete-pop-10000");
        tester.assertTextPresent(TITLE_DELETE_MAIL_SERVER);
        tester.assertTextPresent("Are you sure you want to delete <b>" + VALUE_NAME_POP + "</b>?");
        tester.submit(BUTTON_DELETE);
        tester.assertTextPresent(LABEL_NO_POP_MAIL_SERVER);
        tester.assertLinkPresentWithText(LINK_TEXT_CONFIG_NEW_POP);
    }

    /**
     * Assumptions: NO POP Mail Server set<br>
     * Checks that POP server NOT exists<br>
     */
    @Test
    public void testNoPOPServer() {
        backdoor.restoreBlankInstance();
        logger.log("Mail Server - Checking labels when a POP server is NOT setup");
        navigation.gotoAdminSection(Navigation.AdminSection.INCOMING_MAIL);
        tester.assertTextPresent(LABEL_NO_POP_MAIL_SERVER);
        tester.assertLinkPresentWithText(LINK_TEXT_CONFIG_NEW_POP);
    }

    @Test
    public void testPortSetOnUpdatePopServer() {
        backdoor.restoreBlankInstance();
        navigation.gotoAdminSection(Navigation.AdminSection.INCOMING_MAIL);
        configurePopServer(VALUE_NAME_POP, VALUE_SERVER_NAME, "222", VALUE_USERNAME, VALUE_PASSWORD);
        tester.clickLink("edit-pop-10000");
        tester.assertFormElementEquals("port", "222");
    }

    private void configurePopServer(String name, String hostName, String port, String username, String password) {
        tester.clickLinkWithText(LINK_TEXT_CONFIG_NEW_POP);
        tester.assertTextPresent(TITLE_ADD_POP_MAIL_SERVER);
        tester.setFormElement(FIELD_NAME, name);
        tester.setFormElement(FIELD_SERVER_NAME, hostName);
        tester.setFormElement(FIELD_PORT, port);
        tester.setFormElement(FIELD_USERNAME, username);
        tester.setFormElement(FIELD_PASSWORD, password);
        tester.submit(BUTTON_ADD);
    }
}
