package com.atlassian.jira.functest.framework;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @since v7.1
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface HttpUnitConfiguration {

    String characterSet() default "";
    boolean throwOnErrorStatus() default false;
    boolean enableScripting() default false;
    boolean throwOnScriptError() default false;

}