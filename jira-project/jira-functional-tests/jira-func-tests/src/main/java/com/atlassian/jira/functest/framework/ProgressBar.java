package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;
import java.util.Optional;

import static com.atlassian.jira.matchers.OptionalMatchers.none;
import static com.atlassian.jira.matchers.OptionalMatchers.some;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @since v7.1
 */
public class ProgressBar {

    private final WebTester tester;
    private final Assertions assertions;
    private final TextAssertions textAssertions;
    private final HtmlPage htmlPage;

    @Inject
    public ProgressBar(final WebTester tester,
                       final Assertions assertions,
                       final TextAssertions textAssertions,
                       final HtmlPage htmlPage) {
        this.tester = tester;
        this.assertions = assertions;
        this.textAssertions = textAssertions;
        this.htmlPage = htmlPage;
    }

    /**
     * This method assums that you have just submitted a long running task and that you need the taskId back.  It does
     * not acknowledge the task in in way
     *
     * @return the id of the submitted task.
     */
    public long getSubmittedTaskId() {
        waitForStableTaskPage();
        return getTaskIdFromProgressBarUI();
    }


    /**
     * This method assumes that you have just submitted a long running task, and you know the task id of it.
     *
     * @param taskId the task to wait for until it  is in acknowledge state
     */
    public void waitForTaskAcknowledgement(final long taskId) {
        waitForTaskState(taskId, "Acknowledge");
    }

    /**
     * This method assums that you have just submitted a long running task, and you know the task id of it.
     *
     * @param taskId the task to wait for until it
     */
    private void waitForTaskState(final long taskId, final String desiredTaskState) {
        String taskState;
        do {
            taskState = waitForStableTaskPage();
            final long taskIdTest = getSubmittedTaskId();
            if (taskId != taskIdTest) {
                fail("Expecting taskid <" + taskId + "> but got <" + taskIdTest + ">");
            }
            if (taskState.equals(desiredTaskState)) {
                return;
            }
            if (taskState.equals("Refresh")) {
                // we need to hit refresh other wise we never change state as auto refresh dones not
                // work inside this web unit framework
                tester.submit("Refresh");
            }
        }
        while (!desiredTaskState.equalsIgnoreCase(taskState));
    }

    private String waitForStableTaskPage() {
        final int MAX_ITERATIONS = 100;
        int its = 0;

        while (true) {
            its++;
            if (its > MAX_ITERATIONS) {
                fail("The task took longer than " + MAX_ITERATIONS + " attempts!  Why?");
            }
            // are we on the "still working" page or the "done" page
            // if its neither then fail
            if (getResponseText().contains("type=\"submit\" name=\"Refresh\"")) {
                return "Refresh";

            } else if (getResponseText().contains("type=\"submit\" name=\"Done\"")) { // its on the done page
                // we are on the "done" page" but we dont own the task
                validateProgressBarUI("Done");
                return "Done";
            } else if (getResponseText().contains("input type=\"submit\" name=\"Acknowledge\"")) {
                validateProgressBarUI("Acknowledge");
                return "Acknowledge";
            }

            try {
                Thread.sleep(1000);
            } catch (final InterruptedException e) {
                fail("Test interupted");
            }
        }
    }

    private String getResponseText() {
        return tester.getDialog().getResponseText();
    }

    private long getTaskIdFromProgressBarUI() {
        final String taskLocatorStr = "<div class=\"pb_border\" id=\"pb_taskid_";
        // find out the task id, its in the progress bar UI
        int startIndex = getResponseText().indexOf(taskLocatorStr);
        if (startIndex == -1) {
            fail("Failed to find task progress bar as expected");
        }
        startIndex += taskLocatorStr.length();
        final int endIndex = getResponseText().indexOf("\">", startIndex);

        final String taskId = getResponseText().substring(startIndex, endIndex);
        return Long.parseLong(taskId);
    }

    /**
     * The button name controls what to check for in terms of the progress bar UI.
     * <p/>
     * - Acknowledge - means you started the task and its finished - Done means that some one else started the task and
     * its finished - Refresh - means its submitted, maybe running and not finished
     *
     * @param desiredTaskState one of the above
     */
    public void validateProgressBarUI(final String desiredTaskState) {

        final Optional<Integer> leftPercentage = htmlPage.getRegexMatch("pb_barlefttd.+style\\s*=\\s*\"[^\"]*width\\s*\\:\\s+(\\d+)%.*\"")
                .map(Integer::new);
        final Optional<Integer> rightPercentage = htmlPage.getRegexMatch("pb_barrighttd.+style\\s*=\\s*\"[^\"]*width\\s*\\:\\s+(\\d+)%.*\"")
                .map(Integer::new);

        if ("Acknowledge".equalsIgnoreCase(desiredTaskState) || "Done".equalsIgnoreCase(desiredTaskState)) {
            // ok we are finished and need acknowledge ment.  So it should have a certain look in the progress bar
            tester.assertTextPresent("Task completed");
            tester.assertTextPresent("Started");
            tester.assertTextPresent("Finished");

            assertThat(rightPercentage, none());
            // we must be 100% complete to be here!
            assertThat(leftPercentage, some(equalTo(100)));

            // we can get a bit more specific here.  The user who started the task should be on the page
            if ("Done".equalsIgnoreCase(desiredTaskState)) {

                // test that the link to another user is there
                tester.assertTextPresent("<span>Started");

                textAssertions.assertRegexMatch(tester.getDialog().getResponseText(), "by <a href=\"[^\"]*/secure/admin/user/ViewUser.jspa\\?name=");

                // if you break this test is because your page is not following the Task UI convention of
                // putting a warning at the top of the page.  So I am being strict here as a starting point
                // but we might want to relax this later!
                assertions.assertNodeHasText(new CssLocator(tester, ".aui-message.info"), "This task has finished running.");
                assertions.assertNodeHasText(new CssLocator(tester, ".aui-message.info"), "who started this task should acknowledge it.");
            }
        } else {
            tester.assertTextNotPresent("Task completed");
            tester.assertTextNotPresent("Finished");

            //lets make sure the bar is working correctly.
            if (!leftPercentage.isPresent()) {
                //in this case no progress has been made.
                assertThat(rightPercentage, some(equalTo(100)));
            } else if (!rightPercentage.isPresent()) {
                //in the case we have 100% completion but the task is not finished.
                assertThat(leftPercentage, some(equalTo(100)));
            } else {
                assertThat("Task should not be complete", leftPercentage, some(not(equalTo(100))));
                assertThat("Task should not be complete", rightPercentage, some(not(equalTo(100))));
                //in this case we have a regular completion.
                assertThat(leftPercentage.get(), equalTo(100 - rightPercentage.get()));
            }
        }
    }

}
