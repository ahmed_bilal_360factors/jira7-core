package com.atlassian.jira.webtests.ztests.workflow;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * This func test verifies that the i18n of custom workflow screens is working.
 * The i18n keys for some labels are retrieved from properties of the transition.
 *
 * @since v3.13
 */
@WebTest({Category.FUNC_TEST, Category.WORKFLOW})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestCustomWorkflowScreenLocalization extends BaseJiraFuncTest {

    public static final String WORKFLOW_NAME = "Workflow2";

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreData("TestCustomWorkflowScreenLocalization.xml");
    }


    @Test
    public void testSubmitButtonLabelIsTransitionName() {
        navigation.issue().viewIssue("HMS-1");
        tester.clickLink("action_id_11");
        tester.setWorkingForm("issue-workflow-transition");
        assertions.assertSubmitButtonPresentWithText("issue-workflow-transition-submit", "Resolve");
    }

    @Test
    public void testSubmitButtonLabelIsLocalized() {
        administration.workflows().goTo().createDraft(WORKFLOW_NAME);

        backdoor.workflow().setTransitionProperty(WORKFLOW_NAME, true, 11, "jira.i18n.submit", "resolveissue.title");

        administration.workflows().goTo().publishDraft(WORKFLOW_NAME).publish();

        navigation.issue().viewIssue("HMS-1");
        tester.clickLink("action_id_11");
        tester.setWorkingForm("issue-workflow-transition");
        assertions.assertSubmitButtonPresentWithText("issue-workflow-transition-submit", "Resolve Issue");
    }


    @Test
    public void testFallBackToTransitionName() {
        administration.workflows().goTo().createDraft(WORKFLOW_NAME);
        administration.workflows().goTo().edit(WORKFLOW_NAME).textView().goTo();

        backdoor.workflow().setTransitionProperty(WORKFLOW_NAME, true, 11, "jira.i18n.submit", "blah.doesnt.exist");

        administration.workflows().goTo().publishDraft(WORKFLOW_NAME).publish();

        navigation.issue().viewIssue("HMS-1");
        tester.clickLink("action_id_11");
        tester.setWorkingForm("issue-workflow-transition");
        assertions.assertSubmitButtonPresentWithText("issue-workflow-transition-submit", "Resolve");
    }

    @Test
    public void testTransitionNameTitle() {
        navigation.issue().viewIssue("HMS-1");
        tester.clickLink("action_id_11");
        tester.setWorkingForm("issue-workflow-transition");
        assertions.assertSubmitButtonPresentWithText("issue-workflow-transition-submit", "Resolve");
        tester.assertTitleEquals("Resolve [HMS-1] - Your Company JIRA");
    }

    @Test
    public void testLocalizedTitle() {
        administration.workflows().goTo().createDraft(WORKFLOW_NAME);
        administration.workflows().goTo().edit(WORKFLOW_NAME).textView().goTo();

        backdoor.workflow().setTransitionProperty(WORKFLOW_NAME, true, 11, "jira.i18n.title", "resolveissue.title");

        administration.workflows().goTo().publishDraft(WORKFLOW_NAME).publish();

        navigation.issue().viewIssue("HMS-1");
        tester.clickLink("action_id_11");
        tester.setWorkingForm("issue-workflow-transition");
        assertions.assertSubmitButtonPresentWithText("issue-workflow-transition-submit", "Resolve");

        tester.assertTitleEquals("Resolve Issue [HMS-1] - Your Company JIRA");

    }


    @Test
    public void testFixedDescription() {
        administration.workflows().goTo().createDraft(WORKFLOW_NAME);
        administration.workflows().goTo().edit(WORKFLOW_NAME).textView().goTo();

        backdoor.workflow().setTransitionProperty(WORKFLOW_NAME, true, 11, "description", "My Special description");

        administration.workflows().goTo().publishDraft(WORKFLOW_NAME).publish();

        navigation.issue().viewIssue("HMS-1");
        tester.clickLink("action_id_11");
        tester.setWorkingForm("issue-workflow-transition");
        assertions.assertSubmitButtonPresentWithText("issue-workflow-transition-submit", "Resolve");

        tester.assertTextPresent("My Special description");
    }

}
