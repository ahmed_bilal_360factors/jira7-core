package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;

public class SimpleHttpHandler implements HttpHandler {
    private final BlockingQueue<WebHookResponseData> responseQueue;

    public SimpleHttpHandler(BlockingQueue<WebHookResponseData> responseQueue) {
        this.responseQueue = responseQueue;
    }

    public void handle(HttpExchange exchange) throws IOException {
        final String eventData = IOUtils.toString(exchange.getRequestBody());
        responseQueue.offer(new WebHookResponseData(eventData, exchange.getRequestURI()));
        String response = "This is the response";
        exchange.sendResponseHeaders(200, response.length());
        IOUtils.write(response, exchange.getResponseBody());
        exchange.getResponseBody().close();
    }
}
