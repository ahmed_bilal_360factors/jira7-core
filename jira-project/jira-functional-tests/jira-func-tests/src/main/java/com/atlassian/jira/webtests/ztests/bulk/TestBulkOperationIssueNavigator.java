package com.atlassian.jira.webtests.ztests.bulk;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.navigation.IssueNavigatorNavigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.collect.Lists;
import com.meterware.httpunit.WebTable;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

@WebTest({Category.FUNC_TEST, Category.BULK_OPERATIONS, Category.ISSUE_NAVIGATOR, Category.ISSUES})
@RestoreOnce("TestBulkOperationIssueNavigator.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestBulkOperationIssueNavigator extends BaseJiraFuncTest {

    @Before
    public void setUp() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    public void testUnsavedFilter() {
        navigation.issueNavigator().displayAllIssues();
        assertColumnsInBulkWizard("T", "Key", "Summary", "Assignee", "Reporter", "Status", "Resolution", "Created", "Updated");
    }

    @Test
    public void testCustomFilterCustomColumns() {
        navigation.issueNavigator().loadFilterAndAssert(10011);

        assertColumnsInBulkWizard("T", "Key", "Summary");
    }

    @Test
    public void testCustomFilterStandardColumns() {
        navigation.issueNavigator().loadFilterAndAssert(10010);

        assertColumnsInBulkWizard("T", "Key", "Summary", "Assignee", "Reporter", "Status", "Resolution", "Created", "Updated");
    }

    private void assertColumnsInBulkWizard(final String... headerRow) {
        // assert columns
        assertions.getTableAssertions().assertTableContainsRow(getWebTableWithID("issuetable"), headerRow);
        navigation.issueNavigator().bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES);

        // assert same columns, prepended with extra column for checkboxes
        final List<String> list = Lists.newArrayList();
        list.add(null);
        list.addAll(Arrays.asList(headerRow));
        assertions.getTableAssertions().assertTableContainsRow(getWebTableWithID("issuetable"), list.toArray(new String[list.size()]));

        tester.checkCheckbox("bulkedit_10030", "on");
        tester.submit("Next");

        tester.checkCheckbox("operation", "bulk.edit.operation.name");
        tester.submit("Next");

        tester.checkCheckbox("actions", "comment");
        tester.setFormElement("comment", "Test");
        tester.submit("Next");

        // assert same columns as original
        assertions.getTableAssertions().assertTableContainsRow(getWebTableWithID("issuetable"), headerRow);
    }

    private WebTable getWebTableWithID(final String id) {
        return tester.getDialog().getWebTableBySummaryOrId(id);
    }

}
