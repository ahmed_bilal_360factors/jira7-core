package com.atlassian.jira.webtests.ztests.navigator.jql;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.JQL})
@Restore("TestCurrentUser.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestCurrentUser extends BaseJiraFuncTest {

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testXmlViewWithCurrentUser() throws Exception {
        navigation.gotoPage("/sr/jira.issueviews:searchrequest-xml/temp/SearchRequest.xml?reporterSelect=issue_current_user&tempMax=1000");
        textAssertions.assertTextPresent(tester.getDialog().getResponseText(), "jql=reporter+%3D+currentUser%28%29");
        textAssertions.assertTextPresent(tester.getDialog().getResponseText(), "HSP-3");
        textAssertions.assertTextPresent(tester.getDialog().getResponseText(), "HSP-2");
        textAssertions.assertTextPresent(tester.getDialog().getResponseText(), "HSP-1");

        navigation.gotoPage("/sr/jira.issueviews:searchrequest-xml/temp/SearchRequest.xml?jqlQuery=reporter+in+%28currentUser%28%29%29&tempMax=1000");
        textAssertions.assertTextPresent(tester.getDialog().getResponseText(), "jql=reporter+in+%28currentUser%28%29%29");
        textAssertions.assertTextPresent(tester.getDialog().getResponseText(), "HSP-3");
        textAssertions.assertTextPresent(tester.getDialog().getResponseText(), "HSP-2");
        textAssertions.assertTextPresent(tester.getDialog().getResponseText(), "HSP-1");

        navigation.logout();
        navigation.gotoPage("/sr/jira.issueviews:searchrequest-xml/temp/SearchRequest.xml?reporterSelect=issue_current_user&tempMax=1000");
        textAssertions.assertTextPresent(tester.getDialog().getResponseText(), "jql=reporter+%3D+currentUser%28%29");
        textAssertions.assertTextNotPresent(tester.getDialog().getResponseText(), "<item>");
        Assert.assertEquals(200, tester.getDialog().getResponse().getResponseCode());

        navigation.gotoPage("/sr/jira.issueviews:searchrequest-xml/temp/SearchRequest.xml?jqlQuery=reporter+in+%28currentUser%28%29%29&tempMax=1000");
        textAssertions.assertTextPresent(tester.getDialog().getResponseText(), "jql=reporter+in+%28currentUser%28%29%29");
        textAssertions.assertTextNotPresent(tester.getDialog().getResponseText(), "<item>");
        Assert.assertEquals(200, tester.getDialog().getResponse().getResponseCode());
    }
}
