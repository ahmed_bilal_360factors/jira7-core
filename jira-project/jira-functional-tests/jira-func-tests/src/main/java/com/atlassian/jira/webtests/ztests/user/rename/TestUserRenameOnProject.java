package com.atlassian.jira.webtests.ztests.user.rename;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.admin.user.DeleteUserPage;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.framework.util.form.FormParameterUtil;
import com.atlassian.jira.webtests.table.HtmlTable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v6.0
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS, Category.RENAME_USER, Category.PROJECTS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUserRenameOnProject extends BaseJiraFuncTest {

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("user_rename.xml");

        //    KEY       USERNAME    NAME
        //    bb	    betty	    Betty Boop
        //    ID10001	bb	        Bob Belcher
        //    cc	    cat	        Crazy Cat
        //    ID10101	cc	        Candy Chaos
    }

    @Test
    public void testJqlFunctionsFindRenamedProjectLeads() {
        backdoor.project().setProjectLead(10000, "cat");
        renameUser("cat", "crazy");
        renameUser("bb", "cat");
        // Check recycled user does not inherit their forebear's projects
        navigation.gotoResource("../../../rest/api/2/search?jql=project%20in%20projectsLeadByUser(cat)");
        tester.assertTextPresent("\"total\":0");

        // Check renamed user is still recognised as leading the same projects
        navigation.gotoResource("../../../rest/api/2/search?jql=project%20in%20projectsLeadByUser(crazy)");
        tester.assertTextPresent("\"total\":4");
    }

    @Test
    public void testRenamedDefaultProjectLeadAsDefaultAssignee() {
        // Set Bob as the lead by name
        tester.gotoPage("secure/project/EditProjectLeadAndDefaultAssignee!default.jspa?pid=10000");
        FormParameterUtil leadEditForm = getLeadEditForm();
        leadEditForm.setFormElement("lead", "bb");
        leadEditForm.submitForm();
        // Check Bob is the assignee for a new issue
        String dummyIssueId = backdoor.issues().createIssue("COW", "Chew on some grass").key();
        navigation.gotoPage(String.format("browse/%s", dummyIssueId));
        assertions.assertNodeByIdHasText("assignee-val", "Bob Belcher");

        // Set Betty as the lead by name
        tester.gotoPage("secure/project/EditProjectLeadAndDefaultAssignee!default.jspa?pid=10000");
        leadEditForm = getLeadEditForm();
        leadEditForm.setFormElement("lead", "betty");
        leadEditForm.submitForm();
        // Check Betty is the assignee for a new issue
        dummyIssueId = backdoor.issues().createIssue("COW", "Chew on some more grass").key();
        navigation.gotoPage(String.format("browse/%s", dummyIssueId));
        assertions.assertNodeByIdHasText("assignee-val", "Betty Boop");
    }

    @Test
    public void testUserRenameOnCreateProjectScreens() {
        backdoor.project().addProject("Feline", "CAT", "betty");

        //On roles page
        tester.gotoPage("plugins/servlet/project-config/CAT/roles");
        assertions.assertNodeByIdHasText("projectLead_betty", "Betty Boop");

        //On summary page
        tester.gotoPage("plugins/servlet/project-config/CAT/summary");
        assertions.assertNodeByIdHasText("projectLead_betty", "Betty Boop");

        //On project list page
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_PROJECTS);
        HtmlTable projectsSummaryTable = page.getHtmlTable("project-list");
        Assert.assertEquals("Betty Boop", projectsSummaryTable.getRow(2).getCellForHeading("Project Lead"));

        //Check the new value is preserved when we go back to the edit screen
        tester.gotoPage("secure/project/EditProjectLeadAndDefaultAssignee!default.jspa?pid=10100");
        assertOptionTextSelected("lead", "Betty Boop");

        renameUser("betty", "bboop");

        //Check that Betty is preserved across all the screens

        //On roles page
        tester.gotoPage("plugins/servlet/project-config/CAT/roles");
        assertions.assertNodeByIdHasText("projectLead_bboop", "Betty Boop");

        //On summary page
        tester.gotoPage("plugins/servlet/project-config/CAT/summary");
        assertions.assertNodeByIdHasText("projectLead_bboop", "Betty Boop");

        //On project list page
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_PROJECTS);
        projectsSummaryTable = page.getHtmlTable("project-list");
        Assert.assertEquals("Betty Boop", projectsSummaryTable.getRow(2).getCellForHeading("Project Lead"));

        //Check the new value is preserved when we go back to the edit screen
        tester.gotoPage("secure/project/EditProjectLeadAndDefaultAssignee!default.jspa?pid=10100");
        assertOptionTextSelected("lead", "Betty Boop");
    }

    @Test
    public void testUserRenameOnUpdateProjectScreens() {
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_PROJECTS);
        HtmlTable projectsSummaryTable = page.getHtmlTable("project-list");
        Assert.assertEquals("Adam Ant", projectsSummaryTable.getRow(1).getCellForHeading("Project Lead"));

        tester.gotoPage("secure/project/EditProjectLeadAndDefaultAssignee!default.jspa?pid=10000");
        assertOptionTextSelected("lead", "Adam Ant");
        FormParameterUtil leadEditForm = getLeadEditForm();
        leadEditForm.setFormElement("lead", "bb");
        leadEditForm.submitForm();

        //On roles page
        tester.gotoPage("plugins/servlet/project-config/COW/roles");
        assertions.assertNodeByIdHasText("projectLead_bb", "Bob Belcher");

        //On summary page
        tester.gotoPage("plugins/servlet/project-config/COW/summary");
        assertions.assertNodeByIdHasText("projectLead_bb", "Bob Belcher");

        //On project list page
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_PROJECTS);
        projectsSummaryTable = page.getHtmlTable("project-list");
        Assert.assertEquals("Bob Belcher", projectsSummaryTable.getRow(1).getCellForHeading("Project Lead"));

        //Check the new value is preserved when we go back to the edit screen
        tester.gotoPage("secure/project/EditProjectLeadAndDefaultAssignee!default.jspa?pid=10000");
        assertOptionTextSelected("lead", "Bob Belcher");

        leadEditForm = getLeadEditForm();
        leadEditForm.setFormElement("lead", "betty");
        leadEditForm.submitForm();

        //On roles page
        tester.gotoPage("plugins/servlet/project-config/COW/roles");
        assertions.assertNodeByIdHasText("projectLead_betty", "Betty Boop");

        //On summary page
        tester.gotoPage("plugins/servlet/project-config/COW/summary");
        assertions.assertNodeByIdHasText("projectLead_betty", "Betty Boop");

        //On project list page
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_PROJECTS);
        projectsSummaryTable = page.getHtmlTable("project-list");
        Assert.assertEquals("Betty Boop", projectsSummaryTable.getRow(1).getCellForHeading("Project Lead"));

        //Check the new value is preserved when we go back to the edit screen
        tester.gotoPage("secure/project/EditProjectLeadAndDefaultAssignee!default.jspa?pid=10000");
        assertOptionTextSelected("lead", "Betty Boop");
    }

    @Test
    public void testRenamedProjectLeadCanNotBeDeleted() {
        administration.restoreData("user_rename_doggy_components.xml");

        final String bettysProjectName = "Canine";

        DeleteUserPage deleteUserPage = navigation.gotoPageWithParams(DeleteUserPage.class, DeleteUserPage.generateDeleteQueryParameters("betty"));
        textAssertions.assertTextPresent(deleteUserPage.getProjectLink(), bettysProjectName);

        deleteUserPage = navigation.gotoPageWithParams(DeleteUserPage.class, DeleteUserPage.generateDeleteQueryParameters("bb"));
        textAssertions.assertTextNotPresent(deleteUserPage.getProjectLink(), bettysProjectName); // The user currently known as 'bb' does not have an associated project

        renameUser("bb", "belchyman");
        renameUser("betty", "bb");

        deleteUserPage = navigation.gotoPageWithParams(DeleteUserPage.class, DeleteUserPage.generateDeleteQueryParameters("bb"));
        textAssertions.assertTextPresent(deleteUserPage.getProjectLink(), bettysProjectName); // The user formerly known as 'betty' should be known as 'bb' now.
    }

    private void assertOptionTextSelected(String selectId, String textToTest) {
        final XPathLocator locator = new XPathLocator(tester, String.format("//select[@id='%s']/option[@selected='selected']", selectId));
        Assert.assertTrue(locator.exists());
        textAssertions.assertTextPresent(locator, textToTest);
    }

    private void assertNodeHasText(final String xpath, final String textToTest) {
        final XPathLocator locator = new XPathLocator(tester, xpath);
        Assert.assertTrue(locator.exists());
        textAssertions.assertTextPresent(locator, textToTest);
    }

    private FormParameterUtil getLeadEditForm() {
        FormParameterUtil formParameterUtil = new FormParameterUtil(tester, "project-edit-lead-and-default-assignee", "Update");
        formParameterUtil.addOptionToHtmlSelect("lead", new String[]{ADMIN_USERNAME, "Adam Ant"});
        formParameterUtil.addOptionToHtmlSelect("lead", new String[]{"betty", "Betty Boop"});
        formParameterUtil.addOptionToHtmlSelect("lead", new String[]{"bb", "Bob Belcher"});
        formParameterUtil.addOptionToHtmlSelect("lead", new String[]{"cat", "Crazy Cat"});
        formParameterUtil.addOptionToHtmlSelect("lead", new String[]{"cc", "Candy Chaos"});
        formParameterUtil.addOptionToHtmlSelect("lead", new String[]{ADMIN_USERNAME});
        return formParameterUtil;
    }

    private void renameUser(String from, String to) {
        navigation.gotoPage(String.format("secure/admin/user/EditUser!default.jspa?editName=%s", from));
        tester.setFormElement("username", to);
        tester.submit("Update");
    }
}
