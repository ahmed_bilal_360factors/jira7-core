package com.atlassian.jira.functest.framework;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This rule clears the instance with the default data set. The restore will be executed before each test is run.
 *
 * @since v6.5
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface RestoreBlankInstance {
}