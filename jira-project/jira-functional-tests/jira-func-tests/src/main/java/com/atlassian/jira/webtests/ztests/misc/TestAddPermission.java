package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Tests the AddPermission action.
 */
@WebTest({Category.FUNC_TEST, Category.PERMISSIONS})
@LoginAs(user = ADMIN_USERNAME)
public class TestAddPermission extends BaseJiraFuncTest {
    private static final String PROJECT_PERMISSION_KEY = "func.test.project.permission";
    private static final String PROJECT_PERMISSION_KEY_COMPLETE = "com.atlassian.jira.dev.func-test-plugin:func.test.project.permission";
    private static final String PERMISSION_SCHEMES_SINGLE_PAGE_DISABLED = "com.atlassian.jira.permission-schemes.single-page-ui.disabled";

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
        backdoor.darkFeatures().enableForSite(PERMISSION_SCHEMES_SINGLE_PAGE_DISABLED);
    }

    @Test
    public void testValidationOnPermission() {
        administration.permissionSchemes().defaultScheme();

        tester.clickLinkWithText("Grant permission");
        tester.checkCheckbox("type", "group");

        // if we don't choose any permissions, we should get a validation error
        tester.submit(" Add ");
        textAssertions.assertTextPresent("Errors");
        textAssertions.assertTextPresent("You must select a permission to add.");
    }

    @Test
    public void testCannotAddEntryToDisabledPermission() {
        backdoor.plugins().enablePluginModule(PROJECT_PERMISSION_KEY_COMPLETE);

        administration.permissionSchemes().defaultScheme();
        tester.clickLink("add_perm_" + PROJECT_PERMISSION_KEY);

        tester.checkCheckbox("type", "group");

        backdoor.plugins().disablePluginModule(PROJECT_PERMISSION_KEY_COMPLETE);

        tester.submit(" Add ");
        textAssertions.assertTextPresent("Errors");
        textAssertions.assertTextPresent("Permission with key &#39;" + PROJECT_PERMISSION_KEY + "&#39; doesn&#39;t exist.");
    }
}
