package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.EmailBaseFuncTestCase;
import com.atlassian.jira.webtests.Permissions;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client.Notification;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client.NotifyClient;
import com.icegreen.greenmail.store.FolderException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static javax.ws.rs.core.Response.Status;
import static org.apache.commons.lang.StringUtils.isNotBlank;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Func tests for issue watching use cases.
 *
 * @since v5.2
 */
@WebTest({Category.FUNC_TEST, Category.REST, Category.EMAIL})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestIssueResourceNotify.xml")
public final class TestIssueResourceNotify extends EmailBaseFuncTestCase {
    private static final String HSP_1_KEY = "HSP-1";
    private static final String HSP_1_SUMMARY = "i think i'm being watched";

    public static final String NOTIFICATION_GROUP = "notification-group";

    public static final String JACK_EMAIL = "jack@example.com";

    private final Notification notification = new Notification().textBody("Text Body").htmlBody("<strong>Html Body</strong>");
    private NotifyClient notifyClient;

    @Test
    public void testNotificationUsingNotificationScheme() throws Exception {
        testNotification(notification, true, JACK_EMAIL, FRED_EMAIL, "scheme@example.com");
    }

    @Test
    @Ignore
    public void testNotificationToEmail() throws Exception {
        testNotification(notification.toEmail(ADMIN_EMAIL), true, ADMIN_EMAIL);
    }

    @Test
    public void testNotificationToUser() throws Exception {
        testNotification(notification.toUser(FRED_USERNAME), true, FRED_EMAIL);
    }

    @Test
    public void testNotificationToUserAsText() throws Exception {
        backdoor.userProfile().changeUserNotificationType(FRED_USERNAME, "text");
        testNotification(notification.toUser(FRED_USERNAME), false, FRED_EMAIL);
    }

    @Test
    public void testNotificationToGroup() throws Exception {
        testNotification(notification.toGroup(NOTIFICATION_GROUP), true, FRED_EMAIL);
    }

    @Test
    public void testNotificationToReporter() throws Exception {
        testNotification(notification.toReporter(), true, JACK_EMAIL);
    }

    @Test
    public void testNotificationToAssignee() throws Exception {
        testNotification(notification.toAssignee(), true, FRED_EMAIL);
    }

    @Test
    public void testNotificationToWatchers() throws Exception {
        testNotification(notification.toWatchers(), true, JACK_EMAIL, FRED_EMAIL);
    }

    @Test
    public void testNotificationToVoters() throws Exception {
        testNotification(notification.toVoters(), true, FRED_EMAIL);
    }

    @Test
    public void testNotificationRestrictingByGroup() throws Exception {
        testNotification(notification.toWatchers().restrictToGroup(NOTIFICATION_GROUP), true, FRED_EMAIL);
    }

    @Test
    public void testNotificationRestrictingByPermissionId() throws Exception {
        backdoor.usersAndGroups().removeUserFromGroup(FRED_USERNAME, JIRA_USERS_GROUP); // this removes the browse permission for the user
        testNotification(notification.toWatchers().restrictToPermission(Permissions.BROWSE), true, JACK_EMAIL);
    }

    @Test
    public void testNotificationRestrictingByPermissionName() throws Exception {
        backdoor.usersAndGroups().removeUserFromGroup(FRED_USERNAME, JIRA_USERS_GROUP); // this removes the browse permission for the user
        testNotification(notification.toWatchers().restrictToPermission("BROWSE"), true, JACK_EMAIL);
    }

    @Test
    public void testNotificationWithNonDefaultSubject() throws Exception {
        testNotification(notification.toUser(FRED_USERNAME).subject("A non default subject"), true, FRED_EMAIL);
    }

    private void testNotification(Notification notification, boolean html, String... mailboxes) throws Exception {
        final Response response = notifyClient.loginAs(ADMIN_USERNAME).postResponse(HSP_1_KEY, notification);
        assertThat(response.statusCode, equalTo(Status.NO_CONTENT.getStatusCode()));

        flushMailQueueAndWait(mailboxes.length);

        for (String mailbox : mailboxes) {
            checkMailBox(mailbox, notification, html);
        }
        assertThat(mailService.getReceivedMessages().length, equalTo(0)); // no more messages
    }

    @Test
    public void testNotificationOnNonExistingIssue() throws Exception {
        final Response response = notifyClient.loginAs(ADMIN_USERNAME).postResponse("NO-1", notification);
        assertThat(response.statusCode, equalTo(Status.NOT_FOUND.getStatusCode()));
    }

    @Test
    public void testNotificationWithNoBrowsePermissionOnIssue() throws Exception {
        final Response response = notifyClient.loginAs("luser").postResponse("MKY-1", notification);
        assertThat(response.statusCode, equalTo(Status.FORBIDDEN.getStatusCode()));
    }

    @Test
    public void testNotificationWithNonExistingUser() throws Exception {
        final Response response = notifyClient.loginAs(ADMIN_USERNAME).postResponse("MKY-1", notification.toUser("a-funky-user-name"));
        assertThat(response.statusCode, equalTo(Status.BAD_REQUEST.getStatusCode()));
    }

    @Test
    public void testNotificationWithNonExistingGroup() throws Exception {
        final Response response = notifyClient.loginAs(ADMIN_USERNAME).postResponse("MKY-1", notification.toGroup("a-funky-group-name"));
        assertThat(response.statusCode, equalTo(Status.BAD_REQUEST.getStatusCode()));
    }

    @Test
    public void testNotificationWithNonExistingRestrictingGroup() throws Exception {
        final Response response = notifyClient.loginAs(ADMIN_USERNAME).postResponse("MKY-1", notification.restrictToGroup("a-funky-group-name"));
        assertThat(response.statusCode, equalTo(Status.BAD_REQUEST.getStatusCode()));
    }

    @Test
    public void testNotificationWithNonExistingPermission() throws Exception {
        final Response response = notifyClient.loginAs(ADMIN_USERNAME).postResponse("MKY-1", notification.restrictToPermission("a-funky-permission-name"));
        assertThat(response.statusCode, equalTo(Status.BAD_REQUEST.getStatusCode()));
    }

    private void checkMailBox(String email, Notification notification, boolean html) throws FolderException, MessagingException {
        final MailBox mailBox = getMailBox(email);
        MimeMessage message = mailBox.awaitMessage();
        String actual = isNotBlank(notification.subject) ? DEFAULT_SUBJECT_PREFIX + " " + notification.subject : DEFAULT_SUBJECT_PREFIX + " (" + HSP_1_KEY + ") " + HSP_1_SUMMARY;
        assertEquals(message.getSubject(), actual);
        assertMessageAndType(message, html ? notification.htmlBody : notification.textBody, html);

        mailBox.clear();
    }

    @Before
    public void setUpTest() {
        notifyClient = new NotifyClient(getEnvironmentData());
        configureAndStartSmtpServer();
    }
}
