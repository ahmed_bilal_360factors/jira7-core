package com.atlassian.jira.webtests.ztests.admin.issuessecurity;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.admin.IssueSecurityLevel;
import com.atlassian.jira.functest.framework.admin.IssueSecurityLevel.IssueSecurity;
import com.atlassian.jira.functest.framework.admin.IssueSecuritySchemes;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.framework.util.form.FormParameterUtil;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.meterware.httpunit.WebLink;
import net.sourceforge.jwebunit.HttpUnitDialog;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Test web functionality for adding/removing issue securities
 * to/from security levels.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestAddIssueSecurity extends BaseJiraFuncTest {
    private static final String TEST_SCHEME_NAME = "test-scheme";
    private static final String TEST_SECURITY_LEVEL_NAME = "test-level";
    private static final String ADD_SECURITY_LINK_ID = "add_" + TEST_SECURITY_LEVEL_NAME;

    private static final String SCHEME_ID_REGEX = "schemeId=(\\d)+";
    private static final String LEVEL_ID_REGEX = "security=(\\d)+";

    private static final String ISSUE_SECURITY_PAGE_INDICATOR = "reporter_id";
    private static final String ISSUE_SECURITY_TYPE_RADIO_NAME = "type";
    private static final String ISSUE_SECURITY_TEST_GROUP = "issueSecurityGroup";

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.issueSecuritySchemes().newScheme(TEST_SCHEME_NAME, "blah").newLevel(TEST_SECURITY_LEVEL_NAME,
                "blah");
        backdoor.usersAndGroups().addGroup(ISSUE_SECURITY_TEST_GROUP);
        backdoor.usersAndGroups().addUser("user1");
        backdoor.usersAndGroups().addUser("user2");
    }

    @Test
    public void testAttemptAddWithoutIssueSecurityScheme() throws Exception {
        goToTestScheme();
        String url = getLinkUrl(ADD_SECURITY_LINK_ID);
        logger.log("Add security link URL=" + url);
        navigation.gotoResource(removeSchemeId(url));
        chooseIssueSecurityType(IssueSecurity.REPORTER.typeId());
        tester.submit();
        assertIsOnAddIssueSecurityPage();
        assertIsError("You must select a scheme to add the issue security to");
    }

    @Test
    public void testAttemptAddWithoutIssueSecurityLevel() throws Exception {
        goToTestScheme();
        String url = getLinkUrl(ADD_SECURITY_LINK_ID);
        logger.log("Add security link URL=" + url);
        navigation.gotoResource(removeLevelId(url));
        chooseIssueSecurityType(IssueSecurity.REPORTER.typeId());
        tester.submit();
        assertIsOnAddIssueSecurityPage();
        assertIsError("You must select an issue security level for this issue security.");
    }

    @Test
    public void testAttemptAddGivenNoIssueSecurityTypeSelected() throws Exception {
        goToAddSecurityIssue();
        tester.setWorkingForm("jiraform");
        tester.submit();
        assertIsOnAddIssueSecurityPage();
        assertIsError("You must select a type for this issue security.");
    }

    @Test
    public void testAddParameterlessIssueSecurity() {
        goToTestLevel().addIssueSecurity(IssueSecurity.REPORTER).addIssueSecurity(IssueSecurity.CURRENT_ASIGNEE)
                .addIssueSecurity(IssueSecurity.PROJECT_LEAD);
        goToAddSecurityIssue();
        assertCannotAddIssueSecurity(IssueSecurity.REPORTER);
        assertCannotAddIssueSecurity(IssueSecurity.CURRENT_ASIGNEE);
        assertCannotAddIssueSecurity(IssueSecurity.PROJECT_LEAD);
    }

    @Test
    public void testAddParameterizedIssueSecurity() {
        goToTestLevel().addIssueSecurity(IssueSecurity.GROUP, ISSUE_SECURITY_TEST_GROUP)
                .addIssueSecurity(IssueSecurity.USER, "user1")
                .addIssueSecurity(IssueSecurity.USER, "user2");
        goToAddSecurityIssue();
        assertCannotAddIssueSecurity(IssueSecurity.GROUP, ISSUE_SECURITY_TEST_GROUP);
        assertCannotAddIssueSecurity(IssueSecurity.USER, "user1");
        assertCannotAddIssueSecurity(IssueSecurity.USER, "user2");
    }

    private IssueSecuritySchemes.IssueSecurityScheme goToTestScheme() {
        return administration.issueSecuritySchemes().getScheme(TEST_SCHEME_NAME);
    }

    private IssueSecurityLevel goToTestLevel() {
        return goToTestScheme().getLevel(TEST_SECURITY_LEVEL_NAME);
    }

    private void goToAddSecurityIssue() {
        goToTestScheme();
        tester.clickLink(ADD_SECURITY_LINK_ID);
    }

    private String getLinkUrl(String linkId) throws Exception {
        WebLink link = tester.getDialog().getResponse().getLinkWithID(linkId);
        assertNotNull(link);
        assertNotNull(link.getURLString());
        return link.getURLString();
    }

    private String removeSchemeId(String url) {
        assertTrue(url.matches(".*" + SCHEME_ID_REGEX + ".*"));
        return url.replaceAll(SCHEME_ID_REGEX, "");
    }

    private String removeLevelId(String url) {
        assertTrue(url.matches(".*" + LEVEL_ID_REGEX + ".*"));
        return url.replaceAll(LEVEL_ID_REGEX, "");
    }


    private void chooseIssueSecurityType(String type) {
        assertIsOnAddIssueSecurityPage();
        getDialog().setFormParameter(ISSUE_SECURITY_TYPE_RADIO_NAME, type);
        tester.assertRadioOptionSelected(ISSUE_SECURITY_TYPE_RADIO_NAME, type);
    }

    private void assertCannotAddIssueSecurity(IssueSecurity is) {
        chooseIssueSecurityType(is.typeId());
        tester.submit();
        assertIsOnAddIssueSecurityPage();
        assertIsError("This issue security already exists");
    }

    private void assertCannotAddIssueSecurity(IssueSecurity is, String param) {
        if (is == IssueSecurity.GROUP) {
            //have to add security manually, because FormPrameterUtil doesn't reload a dialog
            goToTestLevel();
            tester.clickLink("add_" + TEST_SECURITY_LEVEL_NAME);
            tester.setFormElement("type", "group");
            final FormParameterUtil formParameterUtil = new FormParameterUtil(tester, "jiraform", " Add ");
            formParameterUtil.addOptionToHtmlSelect("group", new String[]{param});
            formParameterUtil.setFormElement("group", param);
            Node result = formParameterUtil.submitForm();
            final String xpath = "//*[contains(@class, 'aui-messag') and contains(@class, 'error')]";
            XPathLocator locator = new XPathLocator(result, xpath);
            assertions.getTextAssertions().assertTextPresent(locator, "This issue security already exists");
        } else {
            goToTestLevel().addIssueSecurity(is, param);
            assertIsOnAddIssueSecurityPage();
            assertIsError("This issue security already exists");
        }
    }

    private void assertIsOnAddIssueSecurityPage() {
        tester.assertElementPresent(ISSUE_SECURITY_PAGE_INDICATOR);
    }

    private void assertIsError(String msg) {
        assertions.getJiraFormAssertions().assertFormErrMsg(msg);
    }

    private HttpUnitDialog getDialog() {
        return tester.getDialog();
    }
}
