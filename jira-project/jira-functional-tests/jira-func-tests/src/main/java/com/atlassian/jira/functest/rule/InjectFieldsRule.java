package com.atlassian.jira.functest.rule;

import com.google.inject.Injector;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * Rule that can be used to inject fields to test class. Internally guice is used to populate given object.
 *
 * @since v6.5
 */
public class InjectFieldsRule implements TestRule {
    private final Injector injector;
    private final Object testInstance;

    public InjectFieldsRule(final Injector injector, final Object testInstance) {
        this.injector = injector;
        this.testInstance = testInstance;
    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new StatementDecorator(base, () -> injector.injectMembers(testInstance));
    }
}
