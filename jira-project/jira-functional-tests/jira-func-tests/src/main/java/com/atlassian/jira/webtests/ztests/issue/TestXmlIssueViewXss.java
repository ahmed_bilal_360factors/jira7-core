package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HttpUnitConfiguration;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringEscapeUtils;
import org.hamcrest.core.StringContains;
import org.junit.After;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BUILT_IN_CUSTOM_FIELD_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_MULTIUSERPICKER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_USERPICKER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

/**
 * Test for XmlIssueView XSS security.
 *
 * @since v4.4
 */
@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
@HttpUnitConfiguration(throwOnErrorStatus = false)
public class TestXmlIssueViewXss extends BaseJiraFuncTest {

    private static final String XSS_ALERT_RAW = "\"alert('surprise!')";
    private static final String XSS_ALERT_XML_ESCAPED = "&quot;alert(&apos;surprise!&apos;)";

    // should be double slash here, but http unit will strip it
    private static final String HTML_FRAGMENT = "/--><html><body>hi</body>;<!--";
    private static final String HTML_FRAGMENT_ESCAPED = StringEscapeUtils.escapeXml(HTML_FRAGMENT);
    @Inject
    private Administration administration;

    @After
    public void tearDownTest() {
        navigation.login(ADMIN_USERNAME);
    }

    @Test
    public void testXssInModuleKeyParam() throws IOException {
        tester.gotoPage("/si/jira.issueviews:<script>alert('XSS')<script>/HSP-1/HSP-1.xml");
        assertFalse(tester.getDialog().getResponse().getText().contains("<script>alert('XSS')<script>"));
    }

    @Test
    public void testXssInIssueKeyParam() throws IOException {
        tester.gotoPage("/si/jira.issueviews:" + PROJECT_HOMOSAP_KEY + "/<script>alert('XSS')<script>");
        assertFalse(tester.getDialog().getResponse().getText().contains("<script>alert('XSS')<script>"));
    }

    @Test
    public void testUsernameAndFullnameEscaping() {
        administration.usersAndGroups().addUser(XSS_ALERT_RAW, "password", XSS_ALERT_RAW, "xss@xss.com");
        navigation.login(XSS_ALERT_RAW, "password");
        final String issueKey = navigation.issue().createIssue(PROJECT_MONKEY, null, "Just a bug");
        navigation.issue().viewXml(issueKey);
        assertions.getTextAssertions().assertTextPresent(XSS_ALERT_XML_ESCAPED);
        assertions.getTextAssertions().assertTextNotPresent(XSS_ALERT_RAW);
    }

    @Test
    public void testUsernameAndFullnameEscapingOnUserPicker() {
        administration.usersAndGroups().addUser(XSS_ALERT_RAW, "password", XSS_ALERT_RAW, "xss@xss.com");
        final String cfId = administration.customFields().addCustomField(builtInCustomFieldKey(CUSTOM_FIELD_TYPE_USERPICKER), "test-xss");
        final String issueKey = navigation.issue().createIssue(PROJECT_MONKEY, null, "Just a bug", ImmutableMap.<String, String[]>of(
                cfId, new String[]{XSS_ALERT_RAW}
        ));
        navigation.issue().viewXml(issueKey);
        assertions.getTextAssertions().assertTextPresent(XSS_ALERT_XML_ESCAPED);
        assertions.getTextAssertions().assertTextNotPresent(XSS_ALERT_RAW);
    }

    private String builtInCustomFieldKey(String cfType) {
        return String.format("%s:%s", BUILT_IN_CUSTOM_FIELD_KEY, cfType);
    }

    @Test
    public void testUsernameAndFullnameEscapingOnMultiUserPicker() {
        administration.usersAndGroups().addUser(XSS_ALERT_RAW, "password", XSS_ALERT_RAW, "xss@xss.com");
        final String cfId = administration.customFields().addCustomField(builtInCustomFieldKey(CUSTOM_FIELD_TYPE_MULTIUSERPICKER), "test-xss");
        final String issueKey = navigation.issue().createIssue(PROJECT_MONKEY, null, "Just a bug", ImmutableMap.<String, String[]>of(
                cfId, new String[]{XSS_ALERT_RAW, ADMIN_USERNAME}
        ));
        navigation.issue().viewXml(issueKey);
        // for multiuserpicker we use CDATA
        assertions.getTextAssertions().assertTextPresent("<customfieldvalue><![CDATA[" + XSS_ALERT_RAW + "]]></customfieldvalue>");
        assertions.getTextAssertions().assertTextPresentNumOccurences(XSS_ALERT_RAW, 1);
    }

    @Test
    public void testIssueXMLViewEncodesQueryString() throws IOException {
        String linkKey = navigation.issue().createIssue("homosapien", "Bug", "Test issue to link to");

        tester.getDialog().gotoPage(tester.getTestContext().getBaseUrl() + String.format("/si/jira.issueviews:issue-xml/%s/%s.xml?%s", linkKey, linkKey, HTML_FRAGMENT));

        String responseText = tester.getDialog().getResponse().getText();

        assertThat(responseText, StringContains.containsString(HTML_FRAGMENT_ESCAPED));
        assertThat(responseText, not(StringContains.containsString(HTML_FRAGMENT)));
    }
}
