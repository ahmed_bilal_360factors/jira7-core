package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.apache.axis.utils.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@WebTest({Category.FUNC_TEST, Category.ISSUES})
@LoginAs(user = ADMIN_USERNAME)
public class TestManageLinkClosedIssues extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("TestManageLinksClosedIssues.xml");
    }

    @Test
    public void testManageLinkClosedIssue() throws SAXException {
        navigation.issue().gotoIssue("HSP-4");
        List<String> classNames = Arrays.asList(StringUtils.split(tester.getDialog().getResponse().getLinkWith("HSP-5").getClassName(), ' '));
        assertTrue("Expected one of the classes to be 'resolution'", classNames.contains("resolution"));
    }

    @Test
    public void testManageLinkNonClosedIssue() throws SAXException {
        navigation.issue().gotoIssue("HSP-5");
        List<String> classNames = Arrays.asList(StringUtils.split(tester.getDialog().getResponse().getLinkWith("HSP-4").getClassName(), ' '));
        assertFalse("Did not expect the 'resolution' class", classNames.contains("resolution"));
        textAssertions.assertTextPresent(locator.page(), "HSP-4");
    }
}
