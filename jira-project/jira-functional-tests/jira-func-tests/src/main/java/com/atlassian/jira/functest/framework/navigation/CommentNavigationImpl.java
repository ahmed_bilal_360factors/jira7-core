package com.atlassian.jira.functest.framework.navigation;

import com.atlassian.jira.functest.framework.Navigation;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;

/**
 * Implementation for comment navigation.
 *
 * @since v6.5
 */
public class CommentNavigationImpl implements CommentNavigation {
    private final Navigation navigation;
    private final WebTester tester;

    @Inject
    public CommentNavigationImpl(final Navigation navigation, final WebTester tester) {
        this.navigation = navigation;
        this.tester = tester;
    }

    @Override
    public void enableCommentGroupVisibility(final Boolean enable) {
        navigation.gotoAdminSection(Navigation.AdminSection.GENERAL_CONFIGURATION);
        tester.clickLink("edit-app-properties");
        tester.setFormElement("title", "jWebTest JIRA installation");
        tester.checkCheckbox("groupVisibility", enable.toString());
        tester.submit("Update");
    }
}
