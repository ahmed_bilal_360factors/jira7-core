package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.WebTesterFactory;
import com.meterware.httpunit.WebLink;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.junit.Assert.assertEquals;

@WebTest({Category.FUNC_TEST, Category.BROWSING})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestSessionIdInUrl extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testBrowseIssue() throws SAXException {
        administration.restoreBlankInstance();

        navigation.logout();

        // Ensure the "Remember Me" option is checked
        tester.beginAt("/login.jsp");
        tester.setFormElement("os_username", ADMIN_USERNAME);
        tester.setFormElement("os_password", ADMIN_PASSWORD);
        tester.checkCheckbox("os_cookie", "true");
        tester.setWorkingForm("login-form");
        tester.submit();

        navigation.issue().goToCreateIssueForm(null, null);
        tester.setFormElement("summary", "Bug 1");
        tester.selectOption("versions", "New Version 4");
        tester.selectOption("fixVersions", "New Version 5");
        tester.selectOption("components", "New Component 1");
        tester.selectOption("components", "New Component 2");
        tester.submit("Create");

        textAssertions.assertTextPresent(locator.page(), "New Component 2");

        textAssertions.assertTextPresent(locator.page(), "New Version 4");
        textAssertions.assertTextPresent(locator.page(), "New Version 5");
        tester.assertLinkPresentWithText("New Component 2");
        tester.assertLinkNotPresentWithText("New Version 4");
        tester.assertLinkPresentWithText("New Version 5");

        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_PERM_SCHEME_ID, BROWSE_PROJECTS);

        // Delete all cookies - including the session cookie - to replicate the case where the users session has finished
        // and they direct their browser to view an issue directly.

        tester.getDialog().getWebClient().clearContents();
        // Need to ensure that the tester is still pointed at the right tenant though, which clearContents() clears.
        WebTesterFactory.setupWebTester(tester, environmentData);

        tester.gotoPage("/browse/HSP-1");

        textAssertions.assertTextPresent(locator.page(), "New Component 2");
        textAssertions.assertTextPresent(locator.page(), "New Version 4");
        textAssertions.assertTextPresent(locator.page(), "New Version 5");
        tester.assertLinkPresentWithText("New Component 2");
        tester.assertLinkNotPresentWithText("New Version 4");
        tester.assertLinkPresentWithText("New Version 5");

        final WebLink componentLink = tester.getDialog().getResponse().getLinkWith("New Component 2");
        verifyUrl(componentLink.getURLString());

        final WebLink fixVersionLink = tester.getDialog().getResponse().getLinkWith("New Version 5");
        verifyUrl(fixVersionLink.getURLString());
    }

    private void verifyUrl(String url) {
        final String JSESSIONID = "jsessionid";
        int index = url.indexOf(JSESSIONID);
        int index2 = url.lastIndexOf(JSESSIONID);
        assertEquals(index, index2);
    }
}
