package com.atlassian.jira.webtests.ztests.navigator.jql.changehistory.status;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * Responsible for verifying that a user is able to query all issues that had status changes made by a specific user.
 * <p>
 * Story @ JRADEV-3736
 *
 * @since v4.4
 */
@WebTest({Category.FUNC_TEST, Category.JQL, Category.CHANGE_HISTORY})
@Restore("TestWasSearch.xml")
public class TestSearchChangesInStatusByASpecificUser extends BaseJiraFuncTest {

    @Inject
    private IssueTableAssertions issueTableAssertions;

    @Before
    public void setUpTest() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    public void testOnlyReturnsTheIssuesChangedToAStatusByTheSpecifiedUser() {
        navigation.login(FRED_USERNAME);
        navigation.issue().closeIssue("HSP-1", "Fixed", "Fixed");

        navigation.login(ADMIN_USERNAME);
        navigation.issue().closeIssue("HSP-2", "Fixed", "Fixed");

        assertExactSearchWithResults("status was Closed order by key asc", "HSP-1", "HSP-2");

        issueTableAssertions.assertSearchWithResults("status was Closed by fred", "HSP-1");
        issueTableAssertions.assertSearchWithResults("status was Closed by admin", "HSP-2");
    }

    /**
     * Runs the specified search exactly as passed in - it is your responsibility to provide an order by cluase in the
     * query if ordering is important.
     *
     * @param jqlString the jql to search
     * @param issueKeys the issue keys to assert in the result
     */
    private void assertExactSearchWithResults(String jqlString, String... issueKeys) {
        navigation.issueNavigator().createSearch(jqlString);
        assertions.getIssueNavigatorAssertions().assertExactIssuesInResults(issueKeys);
    }
}
