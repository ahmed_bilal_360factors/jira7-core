package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookRegistrationClient;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.matcher.JSONObjectContains;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Version;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.Matchers;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookRegistrationClient.Registration;
import static com.atlassian.jira.testkit.client.util.JsonMatchers.hasField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@WebTest({Category.FUNC_TEST, Category.WEBHOOKS, Category.REFERENCE_PLUGIN})
public class TestWebHookResponse extends AbstractWebHookTest {

    private WebHookRegistrationClient client;

    @Before
    @Override
    public void setUpTest() {
        super.setUpTest();
        backdoor.restoreBlankInstance();
        this.client = new WebHookRegistrationClient(environmentData);
    }

    @Test
    public void testRemoteNotificationOnCommentIssueEvent() throws IOException, InterruptedException, JSONException {
        Registration registration = new Registration();
        registration.name = "Comment Issue Web Hook";
        registration.events = new String[]{"jira:issue_updated"};
        responseTester.registerWebhook(registration);

        backdoor.issues().createIssue("HSP", "This is first issue");
        backdoor.issues().commentIssue("HSP-1", "My comments");

        final JSONObject event = new JSONObject(responseTester.getResponseData().getJson());
        assertThat(event, hasField("issue"));
        assertThat(event.getJSONObject("issue"), hasField("fields"));
        assertThat(event.getJSONObject("issue").getJSONObject("fields"), hasField("comment"));

        final JSONObject commentBlock = event.getJSONObject("issue").getJSONObject("fields").getJSONObject("comment");
        assertEquals(1, commentBlock.get("total"));
        final JSONObject comment = commentBlock.getJSONArray("comments").getJSONObject(0);

        // Perform a smoke check that the generated comment has the fields we expect
        assertThat(comment, new JSONObjectContains("id", "body", "created", "updated", "self", "author", "updateAuthor", "visibility"));

        // Assert some of the values to sanity check the result. (Things like created/updated are too tricky to test reliably so
        // we ignore them. And the user blob is too big for us to care much about.
        assertEquals("10000", comment.get("id"));
        assertEquals("My comments", comment.get("body"));
        assertThat(comment.getString("self"), Matchers.endsWith("rest/api/2/issue/10000/comment/10000"));
    }

    @Test
    public void testEventNotSentAfterRemovingWebHook() throws IOException, InterruptedException {
        final Registration registration = new Registration();
        registration.name = "Comment Issue Web Hook";
        registration.events = new String[]{"jira:issue_updated"};

        final String uri = responseTester.registerWebhook(registration).self;

        client.delete(new File(uri).getName());
        backdoor.issues().createIssue("HSP", "This is first issue");
        backdoor.issues().commentIssue("HSP-1", "My comments");

        assertNull(responseTester.getResponseData());
    }

    @Test
    public void testFilteringOut() throws IOException, InterruptedException {
        final Registration registration = new Registration();
        registration.name = "Comment Issue Web Hook";
        registration.events = new String[]{"jira:issue_updated"};
        registration.setFilterForIssueSection("project = MKY");
        responseTester.registerWebhook(registration);

        backdoor.issues().createIssue("HSP", "This is first issue");
        backdoor.issues().commentIssue("HSP-1", "My comments");

        assertNull(responseTester.getResponseData());
    }

    @Test
    public void testFiltering() throws InterruptedException, JSONException {
        final Registration registration = new Registration();
        registration.name = "Comment Issue Web Hook";
        registration.events = new String[]{"jira:issue_updated"};
        registration.setFilterForIssueSection("project = MKY");
        responseTester.registerWebhook(registration);

        backdoor.issues().createIssue("MKY", "This is first mky issue");
        backdoor.issues().commentIssue("MKY-1", "My sample comment");

        final JSONObject event = new JSONObject(responseTester.getResponseData().getJson());
        assertThat(event, hasField("issue.fields.comment"));

        final JSONObject commentBlock = event.getJSONObject("issue").getJSONObject("fields").getJSONObject("comment");
        assertEquals(1, commentBlock.get("total"));
        final JSONObject comment = commentBlock.getJSONArray("comments").getJSONObject(0);

        // Perform a smoke check that the generated comment has the fields we expect
        assertThat(comment, new JSONObjectContains("id", "body", "created", "updated", "self", "author", "updateAuthor", "visibility"));

        // Assert some of the values to sanity check the result. (Things like created/updated are too tricky to test reliably so
        // we ignore them. And the user blob is too big for us to care much about.
        assertEquals("10000", comment.get("id"));
        assertEquals("My sample comment", comment.get("body"));
        assertThat(comment.getString("self"), Matchers.endsWith("rest/api/2/issue/10000/comment/10000"));

        // negative tests
        assertNull(responseTester.getResponseData());
        backdoor.issues().createIssue("HSP", "This is hsp issue, no webhook for this");
        backdoor.issues().commentIssue("HSP-1", "No webhook triggered for this update");
        assertNull(responseTester.getResponseData());
    }

    @Test
    public void testWebHookDisabling() throws IOException, InterruptedException, JSONException {
        final Registration registration = new Registration();
        registration.name = "Comment Issue Web Hook";
        registration.events = new String[]{"jira:issue_updated"};
        final String uri = responseTester.registerWebhook(registration).self;

        backdoor.issues().createIssue("HSP", "This is first issue");
        backdoor.issues().commentIssue("HSP-1", "My comments");

        final JSONObject event = new JSONObject(responseTester.getResponseData().getJson());
        assertThat(event, hasField("issue.fields.comment"));

        final JSONObject commentBlock = event.getJSONObject("issue").getJSONObject("fields").getJSONObject("comment");
        assertEquals(1, commentBlock.get("total"));
        final JSONObject comment = commentBlock.getJSONArray("comments").getJSONObject(0);

        // Perform a smoke check that the generated comment has the fields we expect
        assertThat(comment, new JSONObjectContains("id", "body", "created", "updated", "self", "author", "updateAuthor", "visibility"));

        assertEquals("10000", comment.get("id"));
        assertEquals("My comments", comment.get("body"));
        assertThat(comment.getString("self"), Matchers.endsWith("rest/api/2/issue/10000/comment/10000"));

        final WebHookRegistrationClient client = new WebHookRegistrationClient(environmentData);
        final String id = new File(uri).getName();
        client.disable(id);
        backdoor.issues().commentIssue("HSP-1", "Second comment");
        assertNull(responseTester.getResponseData());

        client.enable(id);
        backdoor.issues().commentIssue("HSP-1", "Third comment");
        final JSONObject issueCommentedEvent = new JSONObject(responseTester.getResponseData().getJson());

        assertThat(issueCommentedEvent, hasField("issue.fields.comment"));

        final JSONObject issueCommentedEventCommentBlock = issueCommentedEvent.getJSONObject("issue").getJSONObject("fields").getJSONObject("comment");
        assertEquals(3, issueCommentedEventCommentBlock.get("total"));
        final JSONObject issueCommentedEventComment = issueCommentedEventCommentBlock.getJSONArray("comments").getJSONObject(2);

        assertThat(issueCommentedEventComment, new JSONObjectContains("id", "body", "created", "updated", "self", "author", "updateAuthor", "visibility"));
        assertEquals("10002", issueCommentedEventComment.get("id"));
        assertEquals("Third comment", issueCommentedEventComment.get("body"));
        assertThat(issueCommentedEventComment.getString("self"), Matchers.endsWith("rest/api/2/issue/10000/comment/10002"));
    }

    @Test
    public void testDisabledWebHookNotPublishAfterEdit() throws IOException, InterruptedException, JSONException {
        final HttpResponseTester enabledWebHookTester = HttpResponseTester.createTester(environmentData);
        addToCleaunpList(enabledWebHookTester);

        final Registration enabledListener = new Registration();
        enabledListener.name = "Canary Webhook";
        enabledListener.events = new String[]{"jira:issue_updated", "jira:issue_created"};
        enabledWebHookTester.registerWebhook(enabledListener);

        final Registration disabledWebHook = new Registration();
        disabledWebHook.name = "Comment Issue Web Hook";
        disabledWebHook.events = new String[]{"jira:issue_updated"};
        final String uri = responseTester.registerWebhook(disabledWebHook).self;

        final String id = new File(uri).getName();
        client.disable(id);

        backdoor.issues().createIssue("HSP", "This is first issue");
        final String response1 = enabledWebHookTester.getResponseData().getJson();
        assertNotNull(response1);
        assertEquals("jira:issue_created", new JSONObject(response1).get("webhookEvent"));

        backdoor.issues().commentIssue("HSP-1", "My comments");
        final String response2 = enabledWebHookTester.getResponseData().getJson();
        assertNotNull(response2);
        assertEquals("jira:issue_updated", new JSONObject(response2).get("webhookEvent"));

        assertNull(responseTester.getResponseData());
    }

    @Test
    public void testExcludeIssueBodyInResponse() throws IOException, InterruptedException {
        final Registration registration = new Registration();
        registration.name = "Comment Issue Web Hook";
        registration.events = new String[]{"jira:issue_updated"};
        registration.excludeBody = true;
        responseTester.registerWebhook(registration);

        backdoor.issues().createIssue("HSP", "This is first issue");
        backdoor.issues().commentIssue("HSP-1", "My comments");

        final String response = responseTester.getResponseData().getJson();
        assertTrue(StringUtils.isEmpty(response));
    }

    @Test
    public void testWithIssueDeletedEvent() throws IOException, InterruptedException, JSONException {
        final Registration registration = new Registration();
        registration.name = "Issue Deleted Web Hook";
        final String webhookEventId = "jira:issue_deleted";
        registration.events = new String[]{webhookEventId};
        responseTester.registerWebhook(registration);

        final IssueCreateResponse createdIssue = backdoor.issues().createIssue("HSP", "This is first issue");
        backdoor.issues().deleteIssue(createdIssue.key(), true);

        final JSONObject event = new JSONObject(responseTester.getResponseData().getJson());
        assertEquals(webhookEventId, event.get("webhookEvent"));

        assertThat(event, hasField("user"));
        assertEquals("admin", event.getJSONObject("user").get("name"));

        assertThat(event, hasField("issue"));
        assertEquals(createdIssue.id(), event.getJSONObject("issue").get("id"));
    }

    @Test
    public void testWithIssueDeletedEventAndJqlFilteringWithMatch()
            throws IOException, InterruptedException, JSONException {
        final Registration registration = new Registration();
        registration.name = "Issue Deleted Web Hook";
        final String webhookEventId = "jira:issue_deleted";
        registration.events = new String[]{webhookEventId};
        registration.setFilterForIssueSection("summary ~ \"SomeSummaryToMatch\"");
        responseTester.registerWebhook(registration);

        final IssueCreateResponse createdIssue = backdoor.issues().createIssue("HSP", "SomeSummaryToMatch");
        backdoor.issues().deleteIssue(createdIssue.key, true);

        final JSONObject event = new JSONObject(responseTester.getResponseData().getJson());
        assertEquals(webhookEventId, event.get("webhookEvent"));

        assertThat(event, hasField("user"));
        assertEquals("admin", event.getJSONObject("user").get("name"));

        assertThat(event, hasField("issue"));
        assertEquals(createdIssue.id(), event.getJSONObject("issue").get("id"));
    }

    @Test
    public void testWithIssueDeletedEventAndJqlFilteringWithoutMatch() throws IOException, InterruptedException {
        final Registration registration = new Registration();
        registration.name = "Issue Deleted Web Hook";
        final String webhookEventId = "jira:issue_deleted";
        registration.events = new String[]{webhookEventId};
        registration.setFilterForIssueSection("summary ~ \"SomeSummaryToMatch\"");
        responseTester.registerWebhook(registration);

        final IssueCreateResponse createdIssue = backdoor.issues().createIssue("HSP", "ShouldNotMatch");
        backdoor.issues().deleteIssue(createdIssue.key, true);

        assertNull("Expected that webhook won't be triggered.", responseTester.getResponseData());
    }

    @Test
    public void testWebHookWithWhiteSpaceInUrl() throws InterruptedException, JSONException {
        final Registration registration = new Registration();
        final String webhookEventId = "jira:issue_created";
        registration.name = "Comment Issue Web Hook";

        registration.events = new String[]{webhookEventId};
        registration.path = "project%20${issue.key}";
        responseTester.registerWebhook(registration);
        final IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "SomeSummaryToMatch");
        final WebHookResponseData webHookResponse = responseTester.getResponseData();
        final JSONObject event = new JSONObject(webHookResponse.getJson());
        assertThat(webHookResponse.getUri().getRawPath(), Matchers.endsWith("project%20" + issue.key()));
        assertEquals(webhookEventId, event.get("webhookEvent"));

        assertThat(event, hasField("user"));
        assertEquals("admin", event.getJSONObject("user").get("name"));

        assertThat(event, hasField("issue"));
        assertEquals(issue.id(), event.getJSONObject("issue").get("id"));
    }

    @Test
    public void testVersionWebhookFires() throws Exception {
        final Registration registration = new Registration();
        registration.name = "Version Updated Web Hook";
        registration.events = new String[]{"jira:version_created"};
        responseTester.registerWebhook(registration);

        Version createdVersion = versionControl.createVersion("TestVer", "A Test version", "HSP");

        checkVersionWebhook("TestVer", createdVersion.id, "jira:version_created");

        // negative tests
        assertNull(responseTester.getResponseData());
        backdoor.issues().createIssue("HSP", "This is hsp issue, no webhook for this");
        backdoor.issues().commentIssue("HSP-1", "No webhook triggered for this update");
        assertNull(responseTester.getResponseData());
    }

    @Test
    public void testVersionUnreleasedWebHookFires() throws Exception {
        final Registration registration1 = new Registration();
        registration1.name = "Version Released Web Hook 1";
        registration1.events = new String[]{"jira:version_unreleased"};
        responseTester.registerWebhook(registration1);

        Version createdVersion1 = versionControl.createVersion("TestVer3", "A Test version", "HSP");

        // Test that only one hook gets hit
        versionControl.releaseVersion(createdVersion1.id);
        assertNull(responseTester.getResponseData());
        versionControl.unreleaseVersion(createdVersion1.id);
        checkVersionWebhook("TestVer3", createdVersion1.id, "jira:version_unreleased");
        assertNull(responseTester.getResponseData());
    }

    private void checkVersionWebhook(String name, long versionId, String eventName) throws Exception {
        final JSONObject event = new JSONObject(responseTester.getResponseData().getJson());
        assertThat(event, hasField("version.name"));
        assertEquals(name, event.getJSONObject("version").get("name"));

        assertThat(event, hasField("webhookEvent"));
        assertEquals(eventName, event.get("webhookEvent"));

        assertThat(event.getJSONObject("version"), hasField("id"));
        assertEquals(Long.valueOf(versionId), Long.valueOf(event.getJSONObject("version").getLong("id")));
    }
}
