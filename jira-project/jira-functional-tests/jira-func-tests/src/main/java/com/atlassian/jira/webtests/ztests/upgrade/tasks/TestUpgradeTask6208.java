package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.page.ViewIssuePage;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.UPGRADE_TASKS;

/**
 * @since v6.2
 */
@WebTest({FUNC_TEST, UPGRADE_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUpgradeTask6208 extends BaseJiraFuncTest {
    private Map<String, String> issueCreators;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Before
    public void setUpTest() {
        issueCreators = MapBuilder.newBuilder("HSP-1", "Anonymous")
                .add("HSP-2", "Barney Rubble")
                .add("HSP-3", "BamBam Rubble")
                .add("HSP-4", "betty")
                .add("HSP-5", "Fred Flinstone")
                .add("HSP-6", "Pebble Flinstone")
                .add("HSP-7", "Wilma Flinstone")
                .add("HSP-8", "Wilma Flinstone")
                .toMap();
    }

    @Test
    public void testCreatorsCreatedAsExpected() {
        administration.restoreData("TestUpgradeTask6208.xml");
        for (Map.Entry<String, String> entry : issueCreators.entrySet()) {
            assertIssueCreator(entry.getKey(), entry.getValue());
        }
    }

    private void assertIssueCreator(final String issueKey, final String creator) {
        ViewIssuePage viewIssuePage = navigation.issue().viewIssue(issueKey).openTabWithId("changehistory-tabpanel");
        Assert.assertEquals(creator, viewIssuePage.getCreatorUserName());
    }

    private List<String> getTextFromNodes(final String selector) {
        Node[] nodes = locator.css(selector).getNodes();
        return Lists.transform(Lists.newArrayList(nodes), new Function<Node, String>() {
            @Override
            public String apply(@Nullable final Node item) {
                return (item == null) ? null : String.valueOf(item.getNodeValue());
            }
        });
    }
}
