package com.atlassian.jira.webtests.ztests.workflow;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.admin.ViewWorkflows;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * The Workflow Designer is a (bundled) plugin
 * <p/>
 * We are testing it here because we can't figure out how to make tests work inside the actual plugin
 * (integration tests get run as unit tests, or not at all, weird problems)
 *
 * @since v4.4
 */
@WebTest({Category.FUNC_TEST, Category.WORKFLOW})
@RestoreBlankInstance
@LoginAs(user = ADMIN_USERNAME)
public class TestWorkflowDesigner extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Test
    public void testWorkflowDesignerValidWorkflowName() {
        ViewWorkflows viewWorkflows = administration.workflows().goTo();

        final String name = "UNIQUE_WORKFLOW_NAME";
        final String description = "UNIQUE_WORKFLOW_DESCRIPTION";

        viewWorkflows.addWorkflow(name, description).goTo();

        viewWorkflows.launchDesigner(name);

        tester.assertTextPresent(name);
        tester.assertTextPresent(description);
    }

    @Test
    public void testWorkflowDesignerInvalidWorkflowName() {
        final String name = "UNIQUE_WORKFLOW_NAME";

        navigation.gotoPage("secure/admin/workflows/WorkflowDesigner.jspa?wfName=" + name);

        tester.assertTextPresent("No workflow with name '" + name + "' could be found.");
    }

    @Test
    public void testWorkflowDesignerNoWorkflowName() {
        administration.workflows().goTo();

        final String name = "";

        navigation.gotoPage("secure/admin/workflows/WorkflowDesigner.jspa?wfName=" + name);

        tester.assertTextPresent("No workflow with name '" + name + "' could be found.");
    }

    @Test
    public void testWorkflowDesignerScriptTagInNameAndDescription() {
        ViewWorkflows viewWorkflows = administration.workflows().goTo();

        final String name = "\"><script>alert('hello');</script>";
        final String description = "\"><script>alert('world');</script>";

        viewWorkflows.addWorkflow(name, description).goTo();

        viewWorkflows.launchDesigner(name);

        tester.assertTextNotPresent(name);
        tester.assertTextNotPresent(description);

        tester.assertTextPresent("&gt;&lt;script&gt;alert(&#39;hello&#39;);&lt;/script&gt;");
        tester.assertTextPresent("&gt;&lt;script&gt;alert(&#39;world&#39;);&lt;/script&gt;");
    }

    @Test
    public void testOldWorkflowEditorLinksToDesigner() {
        ViewWorkflows viewWorkflows = administration.workflows().goTo();

        final String name = "UNIQUE_WORKFLOW_NAME";
        final String description = "UNIQUE_WORKFLOW_DESCRIPTION";

        viewWorkflows.addWorkflow(name, description).goTo();

        viewWorkflows.workflowSteps(name);

        tester.assertLinkPresentWithText("Diagram");
        tester.clickLinkWithText("Diagram");
        tester.assertTextPresent("Workflow Designer");
        tester.assertTextPresent(name);
    }
}
