package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BUTTON_NAME_NEXT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.webtests.Groups.USERS;

@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUserGroupPicker extends BaseJiraFuncTest {
    private static final String USER_FIELD_NAME = "User field";
    private static final String SUMMARY_FRED = "Issue with user picker fred";
    private static final String SUMMARY_BOTH = "Issue with user picker admin, fred";
    private static final String SUMMARY_ADMIN = "Issue with user picker admin";
    private static final String THREE_ISSUES = "all 3 issue(s)";
    private static final String TWO_ISSUES = "all 2 issue(s)";
    private static final String USER_FIELD_ID = "customfield_10000";

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        administration.restoreData("blankprojects.xml");
        administration.addGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
    }

    @After
    public void tearDown() {
        administration.removeGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
    }

    @Test
    public void testUserGroupPicker() throws Exception {
        _testCustomFieldSetup();
        _testCreateIssueWithField();
    }

    private void _testCustomFieldSetup() {
        logger.log("Adding Mutli user field and Group searcher");

        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        tester.clickLink("add_custom_fields");
        tester.checkCheckbox("fieldType", "com.atlassian.jira.plugin.system.customfieldtypes:multiuserpicker");
        tester.submit(BUTTON_NAME_NEXT);
        tester.setFormElement("fieldName", USER_FIELD_NAME);
        tester.selectOption("searcher", "User Picker & Group Searcher");
        tester.submit(BUTTON_NAME_NEXT);
        tester.checkCheckbox("associatedScreens", "1");
        tester.submit("Update");
    }

    private void _testCreateIssueWithField() {
        logger.log("Creating issues with Mutli user field and Group searcher");

        _testCreateIssueWithUser(ADMIN_USERNAME, ADMIN_FULLNAME);
        _testCreateIssueWithUser(FRED_USERNAME, FRED_FULLNAME);

        _createIssueWithUser(ADMIN_USERNAME + ", " + FRED_USERNAME);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), USER_FIELD_NAME, ADMIN_FULLNAME);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), USER_FIELD_NAME, FRED_FULLNAME);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), ADMIN_FULLNAME, FRED_FULLNAME);

    }

    // -------------------------------------------------------------------------------------------------- private helpers
    private void _testCreateIssueWithUser(final String username, final String fullName) {
        _createIssueWithUser(username);

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), USER_FIELD_NAME, fullName);
        tester.assertTextPresent("Issue with user picker " + username);
    }

    private void _createIssueWithUser(String username) {
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, ISSUE_TYPE_BUG);
        tester.setFormElement("summary", "Issue with user picker " + username);
        tester.assertTextPresent(USER_FIELD_NAME);
        tester.setFormElement(USER_FIELD_ID, username);
        tester.submit("Create");
    }

}
