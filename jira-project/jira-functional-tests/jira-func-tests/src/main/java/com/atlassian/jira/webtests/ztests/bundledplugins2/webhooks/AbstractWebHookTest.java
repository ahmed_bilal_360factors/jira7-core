package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.backdoor.VersionControl;
import com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookRegistrationClient;
import com.google.common.base.Supplier;
import com.google.gson.Gson;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractWebHookTest extends BaseJiraFuncTest {
    protected HttpResponseTester responseTester;
    protected VersionControl versionControl;
    private final List<HttpResponseTester> cleanupList;
    private static final Gson GSON = new Gson();

    protected final Supplier<String> receivedByModuleDescriptorListener(final String webhookPath) {
        return () -> getWebHookResponseFromTestServlet(webhookPath);
    }

    protected class WebHookResponseSupplier implements Supplier<String> {
        public <T> T get(Class<T> theClass) {
            String jsonString = get();
            return GSON.fromJson(jsonString, theClass);
        }

        @Override
        public String get() {
            return getWebHookResponse().getJson();
        }
    }

    protected final WebHookResponseSupplier receivedByPersistentListener = new WebHookResponseSupplier();

    public AbstractWebHookTest() {
        this.cleanupList = new ArrayList<>();
    }

    protected void setUpTest() {
        this.versionControl = new VersionControl(getEnvironmentData());
        this.responseTester = HttpResponseTester.createTester(getEnvironmentData());
        cleanupList.add(responseTester);
        cleanReceivedWebHookQueue();
    }

    private void cleanReceivedWebHookQueue() {
        final HttpClient client = new HttpClient(new HttpClientParams());
        try {
            client.executeMethod(new DeleteMethod(webHookListenerServletPath()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected WebHookResponseData getWebHookResponse() {
        try {
            return responseTester.getResponseData();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

    }

    protected String getWebHookResponseFromTestServlet(String webhookId) {
        return getFromServlet(webHookListenerServletPath(), webhookId);
    }

        protected WebHookResponseData getWebHookResponseFromReferencePlugin(String webHookId) {
        final String responseFromTestServlet = getWebHookResponseFromTestServlet(webHookId);
        return new WebHookResponseData(responseFromTestServlet, URI.create(getWebHookFromReferencePluginUri(webHookListenerServletPath(), webHookId)));
    }

    /**
     * This method returns a map of URL paramters supplied to the webhook listener.
     * <p>
     * <p>
     * It returns a JSON instead of a map so that we can leverage powerful Matchers.
     * </p>
     *
     * @return
     * @throws JSONException
     */
    protected JSONObject getUrlParametersOfReceivedWebHook(String webhookPath) throws JSONException {
        return new JSONObject(getFromServlet(webHookListenerServletPath() + "?params=true", webhookPath));
    }

    private String getFromServlet(String url, String webhookPath) {
        final HttpClient client = new HttpClient(new HttpClientParams());
        final GetMethod getMethod = new GetMethod(getWebHookFromReferencePluginUri(url, webhookPath));
        try {
            client.executeMethod(getMethod);
            return getMethod.getResponseBodyAsString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String getWebHookFromReferencePluginUri(final String url, final String webhookPath) {
        return url.contains("?") ? url + "&bucket=" + webhookPath : url + "?bucket=" + webhookPath;
    }

    public void addToCleaunpList(HttpResponseTester tester) {
        cleanupList.add(tester);
    }

    @After
    public void stopHttpResponseTester() {
        for (HttpResponseTester httpResponseTester : cleanupList) {
            httpResponseTester.stop();
        }
    }

    protected void registerWebHook(final String webHook) {
        final WebHookRegistrationClient.Registration registration = new WebHookRegistrationClient.Registration();
        registration.name = "Webhook " + RandomStringUtils.randomAlphabetic(10);
        registration.events = new String[]{webHook};
        responseTester.registerWebhook(registration);
    }

    private String webHookListenerServletPath() {
        return environmentData.getBaseUrl() + "/plugins/servlet/jiraWebHook";
    }
}
