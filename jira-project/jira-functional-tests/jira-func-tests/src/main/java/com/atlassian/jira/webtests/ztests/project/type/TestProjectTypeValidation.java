package com.atlassian.jira.webtests.ztests.project.type;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.test.util.lic.core.CoreLicenses;
import com.atlassian.jira.testkit.client.ProjectControl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@WebTest({Category.FUNC_TEST})
@LoginAs(user = ADMIN_USERNAME)
public class TestProjectTypeValidation extends BaseJiraFuncTest {
    private static final String PROJECT_NAME = "Test project";
    private static final String PROJECT_KEY = "TST";
    private static final String BUSINESS_PROJECT_TYPE = "business";
    private static final String UNKNOWN_PROJECT_TYPE = "unknown-type";
    private static final String ADMIN = "admin";

    private ProjectControl projectControl;

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstanceWithLicense(CoreLicenses.LICENSE_CORE);
        projectControl = backdoor.project();

    }

    @Test
    public void testAProjectWithAValidProjectTypeCanBeCreatedWhenValidationIsEnabled() throws Exception {
        long projectId = projectControl.addProject(PROJECT_NAME, PROJECT_KEY, ADMIN, BUSINESS_PROJECT_TYPE);

        ProjectTypeKey projectType = projectControl.getProjectType(projectId);
        assertThat(projectType.getKey(), is(BUSINESS_PROJECT_TYPE));
    }

    @Test
    public void testAProjectWithAnInvalidProjectTypeCanNotBeCreatedWhenValidationIsEnabled() throws Exception {
        try {
            projectControl.addProject(PROJECT_NAME, PROJECT_KEY, ADMIN, UNKNOWN_PROJECT_TYPE);
            Assert.fail("An exception was expected, since you should not be able  to create a project with an unknown type when the validation is enabled");
        } catch (Exception e) {
            // do nothing
        }
    }
}
