package com.atlassian.jira.webtests.ztests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.BaseTestWorkflowSchemeResource;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.SimpleIssueType;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.SimpleProject;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.SimpleWorkflow;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.SimpleWorkflowScheme;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.WorkflowSchemeResource;
import com.sun.jersey.api.client.ClientResponse;
import org.junit.Test;

import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @since v5.2
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@Restore("TestProjectWorkflowSchemeResource.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestProjectWorkflowSchemeResource extends BaseTestWorkflowSchemeResource {

    @Test
    public void testGetWithDefaultWorkflowScheme() {
        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        SimpleWorkflowScheme hsp = client.getWorkflowScheme(HSP.getKey());

        SimpleIssueType bug = bug();
        bug.setDefaultIssueType(true);
        final SimpleIssueType improvement = improvement();
        final SimpleIssueType monkey = monkey();
        final SimpleIssueType newFeature = newFeature();
        final SimpleIssueType task = task();
        final SimpleIssueType ignored = ignored();

        List<SimpleIssueType> issueTypes = asList(bug, ignored, improvement, monkey, newFeature, task);
        List<SimpleProject> projects = asList(PROJECT_USING_DEFAULT_WORKFLOW_SCHEME);
        assertEquals(issueTypes, hsp.getIssueTypes());
        assertNotNull(hsp.getShared());
        assertEquals(projects, hsp.getShared().getSharedWithProjects());
        assertEquals("Default Workflow Scheme", hsp.getName());
        assertEquals("This is the default Workflow Scheme. Any new projects that are created will be assigned this scheme.",
                hsp.getDescription());
        assertTrue(hsp.isAdmin());
        assertTrue(hsp.isDefaultScheme());
        assertFalse(hsp.isDraftScheme());
        assertNull(hsp.getLastModifiedDate());
        assertNull(hsp.getLastModifiedUser());
        assertEquals(ADMIN_USERNAME, hsp.getCurrentUser());

        SimpleWorkflow workflow = new SimpleWorkflow("jira", "The default JIRA workflow.", true, true,
                asList(bug.getId(), ignored.getId(), improvement.getId(), monkey.getId(), newFeature.getId(), task.getId()), true);
        assertEquals(asList(workflow), hsp.getMappings());
    }

    @Test
    public void testGetWithComplexScheme() {
        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        SimpleWorkflowScheme mky = client.getWorkflowScheme(MKY.getKey());

        final SimpleIssueType bug = bug();
        final SimpleIssueType improvment = improvement();
        final SimpleIssueType monkey = monkey();
        final SimpleIssueType newFeature = newFeature();
        final SimpleIssueType task = task();

        List<SimpleIssueType> issueTypes = asList(bug, improvment, monkey, newFeature, task);
        assertEquals(issueTypes, mky.getIssueTypes());
        assertEquals("monkey workflow scheme 3", mky.getName());
        assertNull(mky.getDescription());
        assertTrue(mky.isAdmin());
        assertFalse(mky.isDefaultScheme());
        assertNotNull(mky.getShared());
        assertEquals(asList(TEST), mky.getShared().getSharedWithProjects());
        assertFalse(mky.isDraftScheme());
        assertNull(mky.getLastModifiedDate());
        assertNull(mky.getLastModifiedUser());
        assertEquals(ADMIN_USERNAME, mky.getCurrentUser());

        SimpleWorkflow jiraWorkflow = new SimpleWorkflow("jira", "The default JIRA workflow.", true, true,
                asList(bug.getId(), monkey.getId(), newFeature.getId()), true);
        SimpleWorkflow monkeyWorkflow = new SimpleWorkflow(MONKEY_2_WORKFLOW, null, false, false,
                asList(improvment.getId(), task.getId()));
        assertEquals(asList(jiraWorkflow, monkeyWorkflow), mky.getMappings());
    }

    @Test
    public void testGetWithComplexDraftScheme() {
        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        SimpleWorkflowScheme pwdws = client.getWorkflowScheme(PROJECT_WITH_DRAFT_WORKFLOW_SCHEME.getKey());

        final SimpleIssueType bug = bug();
        bug.setDefaultIssueType(true);
        final SimpleIssueType improvment = improvement();
        final SimpleIssueType monkey = monkey();
        final SimpleIssueType newFeature = newFeature();
        final SimpleIssueType task = task();
        final SimpleIssueType ignored = ignored();

        List<SimpleIssueType> issueTypes = asList(bug, ignored, improvment, monkey, newFeature, task);
        assertEquals(issueTypes, pwdws.getIssueTypes());
        assertEquals("monkey workflow scheme 4", pwdws.getName());
        assertNull(pwdws.getDescription());
        assertTrue(pwdws.isAdmin());
        assertFalse(pwdws.isDefaultScheme());
        assertNotNull(pwdws.getShared());
        assertEquals(emptyList(), pwdws.getShared().getSharedWithProjects());
        assertTrue(pwdws.isDraftScheme());
        assertEquals("10/Aug/11 3:00 PM", pwdws.getLastModifiedDate());
        assertEquals(ADMIN_USER, pwdws.getLastModifiedUser());
        assertEquals(ADMIN_USERNAME, pwdws.getCurrentUser());

        SimpleWorkflow jiraWorkflow = new SimpleWorkflow("jira", "The default JIRA workflow.", true, true,
                asList(bug.getId(), monkey.getId(), newFeature.getId()), true);
        SimpleWorkflow monkey2Workflow = new SimpleWorkflow(MONKEY_2_WORKFLOW, null, false, false,
                asList(improvment.getId(), task.getId()));
        SimpleWorkflow monkeyWorkflow = new SimpleWorkflow(MONKEY_WORKFLOW, null, false, false,
                asList(ignored.getId()));
        assertEquals(asList(jiraWorkflow, monkey2Workflow, monkeyWorkflow), pwdws.getMappings());
    }

    @Test
    public void testGetNoPermissionAnonymous() {
        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        Response response = client.anonymous().getWorkflowResponse(MKY.getKey());
        assertEquals(UNAUTHORIZED.getStatusCode(), response.statusCode);
    }

    @Test
    public void testGetNoPermissionLoggedIn() {
        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        Response response = client.loginAs(FRED_USERNAME).getWorkflowResponse(MKY.getKey());
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);
        assertTrue(response.entity.errorMessages.contains("You cannot edit the configuration of this project."));
    }

    @Test
    public void testGetNoProject() {
        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        Response response = client.getWorkflowResponse("ASDF");
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);
        assertTrue(response.entity.errorMessages.contains("No project could be found with key 'ASDF'."));
    }

    @Test
    public void testGetProjectAdmin() {
        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        SimpleWorkflowScheme mky = client.loginAs(PROJECT_ADMIN_USERNAME).getWorkflowScheme(MKY.getKey());

        final SimpleIssueType bug = bug();
        final SimpleIssueType improvment = improvement();
        final SimpleIssueType monkey = monkey();
        final SimpleIssueType newFeature = newFeature();
        final SimpleIssueType task = task();

        List<SimpleIssueType> issueTypes = asList(bug, improvment, monkey, newFeature, task);
        List<SimpleProject> projects = emptyList();
        assertEquals(issueTypes, mky.getIssueTypes());
        assertEquals(projects, mky.getShared().getSharedWithProjects());
        assertEquals("monkey workflow scheme 3", mky.getName());
        assertNull(mky.getDescription());
        assertFalse(mky.isAdmin());
        assertFalse(mky.isDefaultScheme());
        assertFalse(mky.isDraftScheme());
        assertNull(mky.getLastModifiedDate());
        assertNull(mky.getLastModifiedUser());
        assertEquals(PROJECT_ADMIN_USERNAME, mky.getCurrentUser());

        SimpleWorkflow jiraWorkflow = new SimpleWorkflow("jira", "The default JIRA workflow.", true, true,
                asList(bug.getId(), monkey.getId(), newFeature.getId()), true);
        SimpleWorkflow monkeyWorkflow = new SimpleWorkflow(MONKEY_2_WORKFLOW, null, false, false,
                asList(improvment.getId(), task.getId()));
        assertEquals(asList(jiraWorkflow, monkeyWorkflow), mky.getMappings());

        Response response = client.getWorkflowResponse(HSP.getKey());
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);
        assertTrue(response.entity.errorMessages.contains("You cannot edit the configuration of this project."));
    }

    @Test
    public void testGetWithDefault() {
        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        SimpleWorkflowScheme scheme = client.getWorkflowScheme(PROJECT_WITH_DEFAULT_WORKFLOW.getKey());

        final SimpleIssueType bug = bug();
        bug.setDefaultIssueType(true);
        final SimpleIssueType improvment = improvement();
        final SimpleIssueType monkey = monkey();
        final SimpleIssueType newFeature = newFeature();
        final SimpleIssueType task = task();
        final SimpleIssueType ignored = ignored();

        List<SimpleIssueType> issueTypes = asList(bug, ignored, improvment, monkey, newFeature, task);
        List<SimpleProject> projects = asList(PROJECT_WITHOUT_DEFAULT_WORKFLOW);

        assertEquals(issueTypes, scheme.getIssueTypes());
        assertNotNull(scheme.getShared());
        assertEquals(projects, scheme.getShared().getSharedWithProjects());
        assertEquals("monkey workflow scheme 2", scheme.getName());
        assertNull(scheme.getDescription());
        assertTrue(scheme.isAdmin());
        assertFalse(scheme.isDefaultScheme());
        assertFalse(scheme.isDraftScheme());
        assertNull(scheme.getLastModifiedDate());
        assertNull(scheme.getLastModifiedUser());
        assertEquals(ADMIN_USERNAME, scheme.getCurrentUser());

        SimpleWorkflow monkeyWorkflow = new SimpleWorkflow(MONKEY_WORKFLOW, null, true, false,
                asList(ignored.getId(), monkey.getId(), newFeature.getId(), task.getId()));
        SimpleWorkflow jira = new SimpleWorkflow("jira", "The default JIRA workflow.", false, true,
                asList(bug.getId()), true);
        SimpleWorkflow monkey2Workflow = new SimpleWorkflow(MONKEY_2_WORKFLOW, null, false, false,
                asList(improvment.getId()));
        assertEquals(asList(monkeyWorkflow, jira, monkey2Workflow), scheme.getMappings());
    }

    @Test
    public void testGetNoDefault() {
        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        SimpleWorkflowScheme scheme = client.getWorkflowScheme(PROJECT_WITHOUT_DEFAULT_WORKFLOW.getKey());

        final SimpleIssueType bug = bug();
        final SimpleIssueType improvment = improvement();

        List<SimpleIssueType> issueTypes = asList(bug, improvment);
        List<SimpleProject> projects = asList(PROJECT_WITH_DEFAULT_WORKFLOW);

        assertEquals(issueTypes, scheme.getIssueTypes());
        assertNotNull(scheme.getShared());
        assertEquals(projects, scheme.getShared().getSharedWithProjects());
        assertEquals("monkey workflow scheme 2", scheme.getName());
        assertNull(scheme.getDescription());
        assertTrue(scheme.isAdmin());
        assertFalse(scheme.isDefaultScheme());
        assertFalse(scheme.isDraftScheme());
        assertNull(scheme.getLastModifiedDate());
        assertNull(scheme.getLastModifiedUser());
        assertEquals(ADMIN_USERNAME, scheme.getCurrentUser());

        SimpleWorkflow jira = new SimpleWorkflow("jira", "The default JIRA workflow.", false, true,
                asList(bug.getId()), true);
        SimpleWorkflow monkey2Workflow = new SimpleWorkflow(MONKEY_2_WORKFLOW, null, false, false,
                asList(improvment.getId()));
        assertEquals(asList(jira, monkey2Workflow), scheme.getMappings());
    }

    @Test
    public void testCreateDraftWorkflowSchemeWhileUsingDefaultWorkflowScheme() {
        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);

        checkWorkflowScheme(client.createDraftWorkflowScheme(PROJECT_USING_DEFAULT_WORKFLOW_SCHEME.getKey()));
        checkWorkflowScheme(client.createDraftWorkflowScheme(PROJECT_USING_DEFAULT_WORKFLOW_SCHEME.getKey()));
    }

    private void checkWorkflowScheme(SimpleWorkflowScheme scheme) {
        SimpleIssueType bug = bug();
        bug.setDefaultIssueType(true);
        final SimpleIssueType improvment = improvement();
        final SimpleIssueType monkey = monkey();
        final SimpleIssueType issueType = newFeature();
        final SimpleIssueType task = task();
        final SimpleIssueType ignored = ignored();

        List<SimpleIssueType> issueTypes = asList(bug, ignored, improvment, monkey, issueType, task);
        List<SimpleProject> projects = emptyList();
        assertEquals(issueTypes, scheme.getIssueTypes());
        assertEquals(projects, scheme.getShared().getSharedWithProjects());
        assertEquals(PROJECT_USING_DEFAULT_WORKFLOW_SCHEME.getName() + " Workflow Scheme", scheme.getName());
        assertNull(scheme.getDescription());
        assertTrue(scheme.isAdmin());
        assertFalse(scheme.isDefaultScheme());
        assertTrue(scheme.isDraftScheme());
        assertNotNull(scheme.getLastModifiedDate());
        assertEquals(ADMIN_USER, scheme.getLastModifiedUser());
        assertEquals(ADMIN_USERNAME, scheme.getCurrentUser());

        SimpleWorkflow workflow = new SimpleWorkflow("jira", "The default JIRA workflow.", true, true,
                asList(bug.getId(), ignored.getId(), improvment.getId(), monkey.getId(), issueType.getId(), task.getId()), true);
        assertEquals(asList(workflow), scheme.getMappings());
    }

    @Test
    public void testCreateDraftWorkflowSchemeWhileUsingNonDefaultWorkflowScheme() {
        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        SimpleWorkflowScheme scheme = client.createDraftWorkflowScheme(PROJECT_NOT_USING_DEFAULT_WORKFLOW_SCHEME.getKey());

        SimpleIssueType bug = bug();
        bug.setDefaultIssueType(true);
        final SimpleIssueType improvment = improvement();
        final SimpleIssueType monkey = monkey();
        final SimpleIssueType issueType = newFeature();
        final SimpleIssueType task = task();
        final SimpleIssueType ignored = ignored();

        List<SimpleIssueType> issueTypes = asList(bug, ignored, improvment, monkey, issueType, task);

        List<SimpleProject> projects = emptyList();
        assertEquals(issueTypes, scheme.getIssueTypes());
        assertEquals(projects, scheme.getShared().getSharedWithProjects());
        assertEquals(PROJECT_NOT_USING_DEFAULT_WORKFLOW_SCHEME.getName() + " Workflow Scheme", scheme.getName());
        assertNull(scheme.getDescription());
        assertTrue(scheme.isAdmin());
        assertFalse(scheme.isDefaultScheme());
        assertTrue(scheme.isDraftScheme());
        assertNotNull(scheme.getLastModifiedDate());
        assertEquals(ADMIN_USER, scheme.getLastModifiedUser());
        assertEquals(ADMIN_USERNAME, scheme.getCurrentUser());

        SimpleWorkflow workflow = new SimpleWorkflow(PROJECT_NOT_USING_DEFAULT_WORKFLOW_SCHEME_WORKFLOW, null, true, false,
                asList(bug.getId(), ignored.getId(), improvment.getId(), monkey.getId(), issueType.getId(), task.getId()));
        assertEquals(asList(workflow), scheme.getMappings());
    }

    @Test
    public void testCreateNoPermissionAnonymous() {
        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        Response response = client.anonymous().createDraftWorkflowSchemeResponse(MKY.getKey());
        assertEquals(UNAUTHORIZED.getStatusCode(), response.statusCode);
    }

    @Test
    public void testCreateNoPermissionLoggedIn() {
        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        Response response = client.loginAs(FRED_USERNAME).createDraftWorkflowSchemeResponse(MKY.getKey());
        assertEquals(ClientResponse.Status.FORBIDDEN.getStatusCode(), response.statusCode);
        assertTrue(response.entity.errorMessages.contains("You do not have permission to edit this workflow scheme."));
    }

    @Test
    public void testCreateNoProject() {
        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        Response response = client.createDraftWorkflowSchemeResponse("ASDF");
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);
        assertTrue(response.entity.errorMessages.contains("You do not have permission to edit this project."));
    }

    @Test
    public void testDiscardDraftWorkflowScheme() {
        final String projectKey = "DDWS";
        backdoor.project().addProject("testDiscardDraftWorkflowScheme", projectKey, ADMIN_USERNAME);

        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        SimpleWorkflowScheme oldOriginal = client.getWorkflowScheme(projectKey);
        SimpleWorkflowScheme draft = client.createDraftWorkflowScheme(projectKey);

        assertThat(draft.isDraftScheme(), equalTo(true));
        assertThat(draft.isDefaultScheme(), equalTo(false));
        assertThat(draft.getMappings(), equalTo(oldOriginal.getMappings()));

        SimpleWorkflowScheme newOriginal = client.discardDraftWorkflowScheme(projectKey);

        assertThat(newOriginal.isDraftScheme(), equalTo(false));
        assertThat(newOriginal.isDefaultScheme(), equalTo(false));
        assertThat(newOriginal.getMappings(), equalTo(oldOriginal.getMappings()));

        assertThat(client.getWorkflowScheme(projectKey), equalTo(newOriginal));
    }

    @Test
    public void testDiscardNoPermissionAnonymous() {
        final String projectKey = "DDWSA";
        backdoor.project().addProject("testDiscardNoPermissionAnonymous", projectKey, ADMIN_USERNAME);

        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        client.createDraftWorkflowScheme(projectKey);
        Response response = client.anonymous().discardDraftWorkflowSchemeResponse(projectKey);
        assertEquals(UNAUTHORIZED.getStatusCode(), response.statusCode);
    }

    @Test
    public void testDiscardNoPermissionLoggedIn() {
        final String projectKey = "DDWSF";
        backdoor.project().addProject("testDiscardNoPermissionLoggedIn", projectKey, ADMIN_USERNAME);

        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        client.createDraftWorkflowScheme(projectKey);
        Response response = client.loginAs(FRED_USERNAME).discardDraftWorkflowSchemeResponse(projectKey);
        assertEquals(ClientResponse.Status.FORBIDDEN.getStatusCode(), response.statusCode);
        assertTrue(response.entity.errorMessages.contains("You cannot edit the configuration of this project."));
    }

    @Test
    public void testDiscardNoProject() {
        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        Response response = client.discardDraftWorkflowSchemeResponse("ASDF");
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);
        assertTrue(response.entity.errorMessages.contains("No project could be found with key 'ASDF'."));
    }

    @Test
    public void testDiscardNoDraft() {
        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        Response response = client.discardDraftWorkflowSchemeResponse(MKY.getKey());
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);
        assertTrue(response.entity.errorMessages.contains("The workflow scheme does not have a draft."));
    }

    @Test
    public void testGetOriginalScheme() {
        WorkflowSchemeResource client = new WorkflowSchemeResource(environmentData);
        SimpleWorkflowScheme oldOriginal = client.getWorkflowScheme(MKY.getKey());
        try {
            SimpleWorkflowScheme draft = client.createDraftWorkflowScheme(MKY.getKey());
            SimpleWorkflowScheme newOriginal = client.getOriginalWorkflowScheme(MKY.getKey());
            assertThat(newOriginal, not(equalTo(draft)));
            assertThat(newOriginal, equalTo(oldOriginal));
        } finally {
            client.discardDraftWorkflowScheme(MKY.getKey());
        }
    }
}
