package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestXsrfAdminMailServers extends BaseJiraFuncTest {
    @Inject
    protected Administration administration;
    @Inject
    private Form form;

    @Test
    public void testAdminMailServer() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck("Add Mail Server", new XsrfCheck.Setup() {
                    public void setup() {
                        administration.mailServers().Smtp().goTo();
                        tester.gotoPage("AddSmtpMailServer!default.jspa");
                        tester.setFormElement("name", "name");
                        tester.setFormElement("description", "description");
                        tester.setFormElement("from", "brad@atlassian.com");
                        tester.setFormElement("prefix", "prefix");
                        tester.setFormElement("serverName", "server.example.com");
                    }
                }, new XsrfCheck.FormSubmission("Add"))
                ,
                new XsrfCheck("Edit Mail Server", new XsrfCheck.Setup() {
                    public void setup() {
                        administration.mailServers().Smtp().goTo();
                        tester.gotoPage("UpdateSmtpMailServer!default.jspa?id=10000");
                        tester.setFormElement("name", "nameX");
                    }
                }, new XsrfCheck.FormSubmission("Update"))
                ,
                new XsrfCheck("Send Test Mail Server", new XsrfCheck.Setup() {
                    public void setup() {
                        administration.mailServers().Smtp().goTo();
                        tester.gotoPage("SendTestMail!default.jspa");
                    }
                }, new XsrfCheck.FormSubmission("Send"))
                ,
                new XsrfCheck("Delete Mail Server", new XsrfCheck.Setup() {
                    public void setup() {
                        administration.mailServers().Smtp().goTo();
                        tester.gotoPage("DeleteMailServer!default.jspa?id=10000");
                    }
                }, new XsrfCheck.FormSubmission("Delete"))


        ).run(tester, navigation, form);
    }
}