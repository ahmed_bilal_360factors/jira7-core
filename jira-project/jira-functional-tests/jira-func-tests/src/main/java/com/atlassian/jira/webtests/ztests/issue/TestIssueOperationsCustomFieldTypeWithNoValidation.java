package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Tests create/edit/resolve issue with a custom field type that does not validate its length works properly (by displaying error message with "text is too long").
 */
@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.REFERENCE_PLUGIN})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueOperationsCustomFieldTypeWithNoValidation extends BaseJiraFuncTest {
    public static final String CUSTOM_FIELD_WITHOUT_LENGTH_VALIDATION_ID = "customfield_10100";
    private static final int LIMIT = 255;
    private static final String SOME_LONG_TEXT = StringUtils.repeat("yaddi yaddi yadda ", 15);
    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreData("TestCreateIssueWithDegeneratedCustomFieldType.xml");
        backdoor.advancedSettings().setTextFieldCharacterLengthLimit(LIMIT);
    }

    @Test
    public void testCreateIssueFailsWhenBrokenCustomFieldExceedsCharacterLimit() {
        navigation.issue().goToCreateIssueForm("homosapien", null);
        tester.setFormElement("summary", "Test Bug 202020");
        tester.setFormElement(CUSTOM_FIELD_WITHOUT_LENGTH_VALIDATION_ID, SOME_LONG_TEXT);
        tester.submit("Create");

        assertErrorTextPresent();
    }

    @Test
    public void testEditIssueFailsWhenBrokenCustomFieldExceedsCharacterLimit() {
        navigation.issue().gotoEditIssue("HSP-3");
        tester.setFormElement("summary", "Test Bug 202020");
        tester.setFormElement(CUSTOM_FIELD_WITHOUT_LENGTH_VALIDATION_ID, SOME_LONG_TEXT);
        tester.submit("Update");

        assertErrorTextPresent();
    }

    @Test
    public void testResolveIssueFailsWhenBrokenCustomFieldExceedsCharacterLimit() {
        navigation.issue().gotoIssue("HSP-3");
        navigation.issue().resolveIssue("HSP-3", "Fixed", ImmutableMap.of(CUSTOM_FIELD_WITHOUT_LENGTH_VALIDATION_ID, SOME_LONG_TEXT));

        assertErrorTextPresent();
    }

    private void assertErrorTextPresent() {
        tester.assertTextPresent("The entered text is too long. It exceeds the allowed limit of " + LIMIT + " characters.");
    }
}
