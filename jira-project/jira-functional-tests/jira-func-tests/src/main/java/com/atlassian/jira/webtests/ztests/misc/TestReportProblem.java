package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SYS_ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SYS_ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;

@WebTest({FUNC_TEST})
public class TestReportProblem extends BaseJiraFuncTest {
    private static final String SUPPORT_TOOLS_PLUGIN_PATH = "/plugins/servlet/stp/view/";
    private static final String CONTACT_ADMIN_PATH = "/secure/ContactAdministrators!default.jspa";
    private static final String CREATE_ISSUE_PATH = "/secure/CreateIssue!default.jspa";
    private static final String JIRA_SHOW_CONTACT_ADMINISTRATORS_FORM = "jira.show.contact.administrators.form";

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    @Restore("TestWithSystemAdmin.xml")
    public void testReportProblemDoesNotRedirectToSupportToolsPluginForJiraAdmin() {
        try {
            navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
            textAssertions.assertTextNotPresent(getReportProblemHref(), SUPPORT_TOOLS_PLUGIN_PATH);
        } finally {
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    @Restore("TestWithSystemAdmin.xml")
    public void testReportProblemRedirectsToSupportToolsPluginForSysAdmin() {
        try {
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            textAssertions.assertTextPresent(getReportProblemHref(), SUPPORT_TOOLS_PLUGIN_PATH);
        } finally {
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    @RestoreBlankInstance
    public void testReportProblemRedirectsToContactAdminWhenEnabledForLoggedInUsers() {
        try {
            setShowContactAdminForm(true);
            navigation.login(FRED_USERNAME, FRED_PASSWORD);
            textAssertions.assertTextPresent(getReportProblemHref(), CONTACT_ADMIN_PATH);
        } finally {
            setShowContactAdminForm(false);
        }
    }

    @Test
    @RestoreBlankInstance
    public void testReportProblemRedirectsToContactAdminWhenEnabledForLoggedOutUsers() {
        setShowContactAdminForm(true);
        try {
            navigation.logout();
            textAssertions.assertTextPresent(getReportProblemHref(), CONTACT_ADMIN_PATH);
        } finally {
            setShowContactAdminForm(false);
        }
    }

    @Test
    @RestoreBlankInstance
    public void testReportProblemRedirectsToCreateIssueWhenContactAdminDisabledForLoggedInUsers() {
        setShowContactAdminForm(false);
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        textAssertions.assertTextPresent(getReportProblemHref(), CREATE_ISSUE_PATH);
    }

    @Test
    @RestoreBlankInstance
    public void testReportProblemRedirectsToCreateIssueWhenContactAdminDisabledForLoggedOutUsers() {
        navigation.logout();
        textAssertions.assertTextPresent(getReportProblemHref(), CREATE_ISSUE_PATH);
    }

    private String getReportProblemHref() {
        return page.getFooter().getReportProblemLink().getURLString();
    }

    private void setShowContactAdminForm(boolean show) {
        backdoor.applicationProperties().setOption(JIRA_SHOW_CONTACT_ADMINISTRATORS_FORM, show);
    }
}
