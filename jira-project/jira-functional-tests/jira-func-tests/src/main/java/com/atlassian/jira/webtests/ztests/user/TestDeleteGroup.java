package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.framework.util.form.FormParameterUtil;
import com.atlassian.jira.webtests.Groups;
import com.meterware.httpunit.WebTable;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.permission.GlobalPermissionKey.SYSTEM_ADMIN;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * These tests cover the deletion of groups through the Group Browser
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
@LoginAs(user = ADMIN_USERNAME)
public class TestDeleteGroup extends BaseJiraFuncTest {
    private static final String ADMIN_GROUP_2 = "jira-admin-2";
    private static final String ADMIN_USER = ADMIN_USERNAME;
    private static final String OTHER_GROUP = "jira-developers";
    private static final String ADMIN_GROUP = "jira-administrators";

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Test
    @RestoreBlankInstance
    public void testOneAdminGroupOneNonAdminGroup() {
        //Setup test data for TC1&2 (One admin group, user is a member)

        //Test Case 1: User attempts to delete the admin group (they are a member).
        testGroupIsNotDeletable(ADMIN_GROUP);

        //Test Case 2: User then attempts to delete a different (non-admin) group they aren't a part of.
        attemptToDeleteGroup(OTHER_GROUP);
        tester.submit("Delete");
        tester.assertTextNotPresent(OTHER_GROUP);
    }

    @Test
    @RestoreBlankInstance
    public void testTwoAdminGroupsUserIsMemberOfOne() {
        //Setup test data for TC3&4 (Two admin groups, user is a member of one)
        backdoor.usersAndGroups().addGroup(ADMIN_GROUP_2);
        administration.addGlobalPermission(SYSTEM_ADMIN, ADMIN_GROUP_2);

        //Test Case 3: User attempts to delete the admin group they are a part of.
        testGroupIsNotDeletable(ADMIN_GROUP);

        //Test Case 4: User then attempts to delete the admin group they aren't a part of.
        attemptToDeleteGroup(ADMIN_GROUP_2);
        tester.submit("Delete");
        tester.assertTextNotPresent(ADMIN_GROUP_2);
    }

    @Test
    @RestoreBlankInstance
    public void testTwoSysAdminGroupsUserIsMemberOfBoth() {
        //Setup test data for TC5&6 (Two admin groups, user is a member of both)
        backdoor.usersAndGroups().addGroup(ADMIN_GROUP_2);
        administration.addGlobalPermission(SYSTEM_ADMIN, ADMIN_GROUP_2);
        administration.usersAndGroups().addUserToGroup(ADMIN_USER, ADMIN_GROUP_2);

        //Test Case 5: User attempts to delete one of their admin groups.
        attemptToDeleteGroup(ADMIN_GROUP);
        tester.submit("Delete");
        tester.assertTextNotPresent(ADMIN_GROUP);

        //Test Case 6: User attempts to delete their other admin group.
        testGroupIsNotDeletable(ADMIN_GROUP_2);
    }

    @Test
    @Restore("TestWithSystemAdmin.xml")
    public void testNoSysAdminGroupOneAdminGroupOneOther() {
        try {
            //Setup test data for TC1&2 (One admin group, user is a member)
            //Test Case 1: User attempts to delete the admin group (they are a member).
            testGroupIsNotDeletable(ADMIN_GROUP);

            //Test Case 2: User then attempts to delete a different (non-admin) group they aren't a part of.
            attemptToDeleteGroup(OTHER_GROUP);
            tester.submit("Delete");
            tester.assertTextNotPresent(OTHER_GROUP);
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login("root", "root");
            administration.restoreBlankInstance();
        }
    }

    @Test
    @Restore("TestWithSystemAdmin.xml")
    public void testNoSysAdminGroupTwoAdminGroupUserIsMemberOfBoth() {
        try {
            //Setup test data for TC1&2 (One admin group, user is a member)
            backdoor.usersAndGroups().addGroup(ADMIN_GROUP_2);
            giveAdminPermission(ADMIN_GROUP_2);
            administration.usersAndGroups().addUserToGroup(ADMIN_USER, ADMIN_GROUP_2);

            //Test Case 5: User attempts to delete one of their admin groups.
            attemptToDeleteGroup(ADMIN_GROUP);
            tester.submit("Delete");
            tester.assertTextNotPresent(ADMIN_GROUP);

            //Test Case 6: User attempts to delete their other admin group.
            testGroupIsNotDeletable(ADMIN_GROUP_2);
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login("root", "root");
            administration.restoreBlankInstance();
        }
    }

    @Test
    @Restore("TestWithSystemAdmin.xml")
    public void testDeleteSysAdminGroupAsAdmin() {
        try {
            //Setup test data for TC1&2 (One admin group, user is a member)
            // Try to delete a group we should not have permission to delete
            tester.gotoPage("/secure/admin/user/DeleteGroup!default.jspa?name=jira-sys-admins");

            tester.assertTextPresent("Cannot delete group, only System Administrators can delete groups associated with the System Administrators global permission.");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login("root", "root");
            administration.restoreBlankInstance();
        }
    }

    @Test
    @Restore("TestWithSystemAdmin.xml")
    public void testSysAdminGroupsOperationsDoNotShowToAdmins() throws SAXException {
        try {
            //Setup test data for TC1&2 (One admin group, user is a member)
            navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);

            final int actionsCol = 4;
            final WebTable groupTable = tester.getDialog().getResponse().getTableWithID("group_browser_table");
            assertions.getTableAssertions().assertTableCellHasText(groupTable, 1, 0, "jira-administrators");
            assertions.getTableAssertions().assertTableCellHasText(groupTable, 1, actionsCol, "Delete");
            assertions.getTableAssertions().assertTableCellHasText(groupTable, 1, actionsCol, "Edit members");
            tester.assertLinkPresent("del_jira-administrators");
            tester.assertLinkPresent("edit_members_of_jira-administrators");

            assertions.getTableAssertions().assertTableCellHasText(groupTable, 2, 0, "jira-developers");
            assertions.getTableAssertions().assertTableCellHasText(groupTable, 2, actionsCol, "Delete");
            assertions.getTableAssertions().assertTableCellHasText(groupTable, 2, actionsCol, "Edit members");
            tester.assertLinkPresent("del_jira-developers");
            tester.assertLinkPresent("edit_members_of_jira-developers");

            assertions.getTableAssertions().assertTableCellHasText(groupTable, 3, 0, "jira-sys-admins");
            assertTrue("shouldn't have delete operation", !groupTable.getCellAsText(3, actionsCol).contains("Delete"));
            assertTrue("shouldn't have edit operation", !groupTable.getCellAsText(3, actionsCol).contains("Edit members"));
            tester.assertLinkNotPresent("del_jira-sys-admins");
            tester.assertLinkNotPresent("edit_members_of_jira-sys-admins");

            assertions.getTableAssertions().assertTableCellHasText(groupTable, 4, 0, "jira-users");
            assertions.getTableAssertions().assertTableCellHasText(groupTable, 4, actionsCol, "Delete");
            assertions.getTableAssertions().assertTableCellHasText(groupTable, 4, actionsCol, "Edit members");
            tester.assertLinkPresent("del_jira-users");
            tester.assertLinkPresent("edit_members_of_jira-users");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login("root", "root");
            administration.restoreBlankInstance();
        }
    }

    @Test
    @Restore("TestDeleteGroupSwapGroup.xml")
    public void testDeleteGroupSwapGroup() {
        navigation.issue().gotoIssue("HSP-1");
        tester.assertTextPresent("Test Comment Visibility");

        attemptToDeleteGroup(Groups.ADMINISTRATORS);
        tester.selectOption("swapGroup", "other-admins");
        tester.submit("Delete");

        navigation.issue().gotoIssue("HSP-1");
        tester.assertTextPresent("Test Comment Visibility");
    }

    @Test
    @Restore("TestDeleteGroupSwapGroup.xml")
    public void testDeleteGroupSwapGroupSameGroup() {
        navigation.issue().gotoIssue("HSP-1");
        tester.assertTextPresent("Test Comment Visibility");

        tester.gotoPage(page.addXsrfToken("/secure/admin/user/DeleteGroup.jspa?name=jira-administrators&swapGroup=jira-administrators"));
        tester.assertTextPresent("You cannot swap comments/worklogs to the group you are deleting.");
    }

    @Test
    @RestoreBlankInstance
    @LoginAs(user = FRED_USERNAME)
    public void testDeleteGroupAsFred() {
        tester.gotoPage(page.addXsrfToken("/secure/admin/user/DeleteGroup!default.jspa?name=jira-administrators"));
        tester.assertTextPresent("my login on this computer");
    }

    @Test
    @RestoreBlankInstance
    public void testDeleteInvalidGroup() {
        tester.gotoPage(page.addXsrfToken("/secure/admin/user/DeleteGroup.jspa?name=invalid"));
        assertions.getJiraFormAssertions().assertFormErrMsg("The group 'invalid' is not a valid group.");
    }

    private void testGroupIsNotDeletable(@Nonnull final String groupName) {
        navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);

        final CssLocator element = locator.css("#del_" + groupName);
        if (!element.exists()) {
            fail("Expected element with id not found in response: [del_" + groupName + "]");
        }
    }

    private void attemptToDeleteGroup(final String groupName) {
        navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
        final String linkId = "del_" + groupName;
        tester.clickLink(linkId);
    }

    private void giveAdminPermission(final String groupName) {
        navigation.gotoAdminSection(Navigation.AdminSection.GLOBAL_PERMISSIONS);
        tester.selectOption("globalPermType", "JIRA Administrators");
        final FormParameterUtil formParameterUtil = new FormParameterUtil(tester, "jiraform", "Add");
        formParameterUtil.addOptionToHtmlSelect("groupName", new String[]{groupName});
        formParameterUtil.setFormElement("groupName", groupName);
        formParameterUtil.submitForm();
        navigation.gotoAdminSection(Navigation.AdminSection.GLOBAL_PERMISSIONS);
    }
}
