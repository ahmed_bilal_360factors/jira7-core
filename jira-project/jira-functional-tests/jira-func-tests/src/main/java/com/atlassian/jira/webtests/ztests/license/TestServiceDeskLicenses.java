package com.atlassian.jira.webtests.ztests.license;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.backdoor.SalLicenseControl;
import com.atlassian.jira.functest.framework.backdoor.SingleProductLicenseDetailsViewTO;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.test.util.lic.ApplicationLicenseConstants;
import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses;
import com.atlassian.jira.webtests.LicenseKeys;
import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests for v1, v2 and v3 new Service Desk licenses.
 *
 * @since 7.0
 */
@WebTest({Category.FUNC_TEST, Category.LICENSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestServiceDeskLicenses extends BaseJiraFuncTest {
    private SalLicenseControl license;

    @Before
    public void setUpTest() {
        license = new SalLicenseControl(getEnvironmentData());
        backdoor.restoreBlankInstance();
    }

    @After
    public void tearDownTest() {
        backdoor.license().set(LicenseKeys.COMMERCIAL);
    }

    @Test
    public void testServiceDeskLicenses() {
        final List<License> licenses = Arrays.asList(ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP,
                ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP_EVAL,
                ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP_UNLIMITED,
                ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP,
                ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP_EVAL,
                ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP,
                ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP_EVAL);

        for (final License lic : licenses) {
            final SalLicenseControl.ValidationResultTO to
                    = license.validateLicenseString(ApplicationLicenseConstants.SERVICE_DESK_KEY, lic, Locale.ENGLISH);
            if (!to.isValid()) {
                final StringWriter out = new StringWriter();
                PrintWriter writer = new PrintWriter(out);
                writer.printf("License %s is invalid.%n", StringUtils.abbreviate(lic.getLicenseString(), 20));
                writer.println("Errors:");
                for (String s : to.getErrorMessages()) {
                    writer.print("\t");
                    writer.println(s);
                }
                writer.println("Warnings:");
                for (String s : to.getWarningMessages()) {
                    writer.print("\t");
                    writer.println(s);
                }
                writer.close();
                fail(out.toString());
            }
            assertTrue(license.addProductLicense(ApplicationLicenseConstants.SERVICE_DESK_KEY, lic));
            assertLicenseEqual(lic, license.getProductLicenseDetails(ApplicationLicenseConstants.SERVICE_DESK_KEY));
        }
    }

    @Test
    public void testServiceDeskExpiredEvalLicenseIsRejected() {
        //This license has expired cannot be added.
        assertInvalidLicense(ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP_EVAL_EXPIRED);
    }

    @Test
    public void testMixingCommercialWithAcademicIsValid() {
        //This is valid because both licenses are paid types.
        assertValidLicense(ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP_ACADEMIC);
    }

    @Test
    public void testMixingCommercialWithCommunityIsRejected() {
        //This community license won't mix with the commercial license already generated (not paid vs. paid).
        assertInvalidLicense(ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP_COMMUNITY);
    }

    private void assertInvalidLicense(final License licenseKey) {
        final SalLicenseControl.ValidationResultTO to
                = license.validateLicenseString(ApplicationLicenseConstants.SERVICE_DESK_KEY, licenseKey, Locale.ENGLISH);
        assertFalse(to.isValid());
    }

    private void assertValidLicense(final License licenseKey) {
        final SalLicenseControl.ValidationResultTO to
                = license.validateLicenseString(ApplicationLicenseConstants.SERVICE_DESK_KEY, licenseKey, Locale.ENGLISH);
        assertTrue(to.isValid());
    }

    private static void assertLicenseEqual(License expectedLicense, SingleProductLicenseDetailsViewTO actual) {
        assertEquals(expectedLicense.getDescription(), actual.description);
        assertEquals(expectedLicense.getOrganisation(), actual.organisationName);
        assertEquals(expectedLicense.getSen(), actual.supportEntitlementNumber);
        assertEquals(expectedLicense.isEvaluation(), actual.isEvaluationLicense);
        assertEquals(expectedLicense.getSubscriptionExpiryDate().isEmpty(), actual.isPerpetualLicense);

        final License.Role role = Iterables.getOnlyElement(expectedLicense.getRoles().values());

        //Check the role details.
        assertEquals(role.getProduct(), actual.productKey);
        assertEquals(role.isUnlimitedUsers(), actual.isUnlimitedNumberOfUsers);
        assertEquals(role.getNumberOfUsers(), actual.numberOfUsers);
    }
}
