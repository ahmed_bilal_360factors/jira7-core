package com.atlassian.jira.webtests.ztests.dashboard.reports;

import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;
import org.w3c.dom.Element;
import org.w3c.dom.html.HTMLInputElement;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.REPORTS})
@LoginAs(user = ADMIN_USERNAME)
public class TestConfigureReportHiddenField extends AbstractConfigureReportFieldTestCase {

    private Element fieldContainer() {
        return (Element)new XPathLocator(tester, "//fieldset[@class = 'group'][input[@type = 'hidden'][@id = 'aHidden']]").getNode();
    }

    @Test
    public void fieldExists() {
        tester.assertFormElementPresent("aHidden");
    }

    @Test
    public void valueIsFilledIn() {
        HTMLInputElement element = (HTMLInputElement)tester.getDialog().getElement("aHidden");
        assertThat(element.getValue(), is("Hidden Field Value"));
    }

    @Test
    public void fieldLabelRendered() {
        textAssertions.assertTextPresent(new XPathLocator(fieldContainer(), "legend"), "A hidden");
    }

    @Test
    public void validationErrorsRendered() {
        tester.gotoPage("/secure/ConfigureReport.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.dev.func-test-plugin:fieldtest-report&aString=fail");
        textAssertions.assertTextPresent(new XPathLocator(fieldContainer(), "*[@class='errMsg']"), "This hidden field has an error");
    }

    //Hidden fields did not render a description label

    /**
     * Hidden field value cannot change, so it must equal the default value.
     */
    @Test
    public void valueIsSubmittedToReportBean() {
        tester.submit();
        textAssertions.assertTextPresent(new XPathLocator(tester, "//tr[th = 'aHidden']/td"), "Hidden Field Value");
    }

}
