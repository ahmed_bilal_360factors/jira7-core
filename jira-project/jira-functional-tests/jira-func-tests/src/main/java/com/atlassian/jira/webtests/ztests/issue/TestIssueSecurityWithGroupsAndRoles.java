package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.admin.IssueSecurityLevel;
import com.atlassian.jira.functest.framework.admin.Project;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.Groups;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_ADMIN_ROLE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SECURITY_LEVEL_ONE_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SECURITY_LEVEL_THREE_DESC;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SECURITY_LEVEL_THREE_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SECURITY_LEVEL_TWO_DESC;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SECURITY_LEVEL_TWO_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SECURITY_SCHEME_NAME;
import static com.atlassian.jira.webtests.Groups.DEVELOPERS;

/**
 * Tests that modifications to group or role membership will flush the cache by checking that the security level field
 * has the correct values reflected.
 */
@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.ROLES, Category.SECURITY, Category.USERS_AND_GROUPS})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestIssueSecurityWithGroupsAndRoles.xml")
public class TestIssueSecurityWithGroupsAndRoles extends BaseJiraFuncTest {

    private static final String[] EXPECTED_SECURITY_LEVEL_NONE = new String[]{"None"};
    private static final String[] EXPECTED_SECURITY_LEVEL_RED = new String[]{"None", SECURITY_LEVEL_ONE_NAME};
    private static final String[] EXPECTED_SECURITY_LEVEL_GREEN = new String[]{"None", SECURITY_LEVEL_THREE_NAME};
    private static final String NEW_PROJECT_NAME = "New Project";
    private static final String NEW_GROUP_NAME = "securityGroup";
    private static final String NEW_SECURITY_SCHEME_NAME = "security scheme copy";

    @Inject
    private Administration administration;

    @Test
    public void testAddingAndRemovingUserFromGroupFlushesSecurityCache() {
        //initially no roles are added, so we can only see 'None'
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);

        //add 'developers' group to role, so it is displayed on the field
        addGroupToProjectRole(Groups.DEVELOPERS);
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_RED);

        //remove user from the developers group, so we are back to the single value 'None'
        administration.usersAndGroups().removeUserFromGroup(ADMIN_USERNAME, DEVELOPERS);
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);

        //re-add user to the developers group, to check we have the security level again
        administration.usersAndGroups().addUserToGroup(ADMIN_USERNAME, DEVELOPERS);
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_RED);
    }

    private void addGroupToProjectRole(final String group) {
        backdoor.projectRole().addActors(PROJECT_HOMOSAP_KEY, JIRA_ADMIN_ROLE, new String[]{group}, null);
    }

    /**
     * Check if user is added or removed from a role, the issue security level cache is flushed
     */
    @Test
    public void testAddingAndRemovingUserFromRolesFlushesIssueSecurityCache() {
        //initially no roles are added, so we can only see 'None'
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);

        //add a new role, so it is displayed on the field
        backdoor.projectRole().addActors(PROJECT_HOMOSAP_KEY, JIRA_ADMIN_ROLE, null, new String[]{ADMIN_USERNAME});
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_RED);

        //remove the role from the project, so we are back to the single value 'None'
        backdoor.projectRole().deleteUser(PROJECT_HOMOSAP_KEY, JIRA_ADMIN_ROLE, ADMIN_USERNAME);
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);
    }

    /**
     * Check if group is added or removed from a role, the issue security level cache is flushed
     */
    @Test
    public void testAddingAndRemovingGroupRolesFromProjectFlushesIssueSecurityCache() {
        //initially no roles are added, so we can only see 'None'
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);

        //add group to the security, so it is displayed on the field
        addGroupToProjectRole(Groups.USERS);
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_RED);

        //remove the role from the project, so we are back to the single value 'None'
        backdoor.projectRole().deleteGroup(PROJECT_HOMOSAP_KEY, JIRA_ADMIN_ROLE, Groups.USERS);
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);
    }

    /**
     * Check if a role is removed from jira, the issue security level cache is flushed
     */
    @Test
    public void testRemovingRoleFlushesIssueSecurityCache() {
        //initially no roles are added, so we can only see 'None'
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);

        //add group to the securitty, so it is displayed on the field
        addGroupToProjectRole(Groups.USERS);
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_RED);

        //remove the role, ie. remove the role from all schemes and check the only value is 'None'
        navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_ROLE_BROWSER);
        tester.clickLink("delete_" + JIRA_ADMIN_ROLE);
        tester.submit("Delete");
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);
    }

    /**
     * Check if a group is removed from jira, the issue security level cache is flushed
     */
    @Test
    public void testRemovingGroupFlushesIssueSecurityCache() {
        //initially no roles are added, so we can only see 'None'
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);

        //add a new group, so it is displayed on the field
        administration.usersAndGroups().addGroup(NEW_GROUP_NAME);
        administration.issueSecuritySchemes().getScheme(SECURITY_SCHEME_NAME).getLevel(SECURITY_LEVEL_ONE_NAME).
                addIssueSecurity(IssueSecurityLevel.IssueSecurity.GROUP, NEW_GROUP_NAME);
        administration.usersAndGroups().addUserToGroup(ADMIN_USERNAME, NEW_GROUP_NAME);
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_RED);

        //remove the group, ie. remove the group from all schemes and check the only value is 'None'
        administration.usersAndGroups().deleteGroup(NEW_GROUP_NAME);
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);
    }

    /**
     * Check that newly created projects have default roles setup by checking admin can see the security level and users
     * can only see None
     */
    @Test
    public void testNewProjectsWithDefaultRolesHaveCorrectSecurityLevel1() {
        //create and setup a new project (so it gets the default roles)
        administration.project().addProject(NEW_PROJECT_NAME, "NEW", ADMIN_USERNAME);
        administration.project().associateIssueLevelSecurityScheme(NEW_PROJECT_NAME, SECURITY_SCHEME_NAME);
        _testNewProjectsWithDefaultRolesHaveCorrectSecurityLevel();
    }

    /**
     * same as {@link #testNewProjectsWithDefaultRolesHaveCorrectSecurityLevel1()} but instead of associating the
     * security scheme with the project after its created, we do it during the creation
     */
    @Test
    public void testNewProjectsWithDefaultRolesHaveCorrectSecurityLevel2() {
        //create and setup a new project (so it gets the default roles)
        Project project = administration.project();
        project.addProject(NEW_PROJECT_NAME, "NEW", ADMIN_USERNAME);
        administration.project().associateIssueLevelSecurityScheme(NEW_PROJECT_NAME, SECURITY_SCHEME_NAME);
        _testNewProjectsWithDefaultRolesHaveCorrectSecurityLevel();
    }

    /**
     * Check updating the security scheme does not affect the security level options.
     */
    @Test
    public void testRenamingSecuritySchemeFlushesCache() {
        //check we can see more than 'None' for the security level
        addGroupToProjectRole(Groups.USERS);
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_RED);

        //update the security scheme and check we can still see the same security
        navigation.gotoAdminSection(Navigation.AdminSection.SECURITY_SCHEMES);
        tester.clickLink("edit_10000"); //there should only be one scheme
        tester.setFormElement("name", SECURITY_SCHEME_NAME + "edited");
        tester.submit("Update");
        tester.assertLinkPresentWithText(SECURITY_SCHEME_NAME + "edited");
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_RED);
    }

    /**
     * Check that if the associated security scheme of a project is unassociated, that the security levels are no longer
     * visible visible
     */
    @Test
    public void testAddingAndRemovingSecuritySchemeFromProject() {
        //initiailly we can see the security level field
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);

        //once we unassociate the security scheme, we can no longer see the field
        administration.project().removeAssociationOfIssueLevelSecurityScheme(PROJECT_HOMOSAP);
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");
        tester.getDialog().setWorkingForm("issue-create");
        tester.assertFormElementNotPresent("security");

        //associate the secruity scheme again and check its back
        administration.project().associateIssueLevelSecurityScheme(PROJECT_HOMOSAP, SECURITY_SCHEME_NAME);
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);
    }

    /**
     * Swap the security scheme with a new scheme and check that the security levels are updated
     */
    @Test
    public void testSwappingSecuritySchemeFlushesCache() {
        //initiailly we can see the security level field
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);

        //create a new security scheme
        //add a visible security level so its different to the original scheme
        administration.issueSecuritySchemes().newScheme(NEW_SECURITY_SCHEME_NAME, "new scheme for testing").
                newLevel(SECURITY_LEVEL_TWO_NAME, SECURITY_LEVEL_TWO_DESC).
                addIssueSecurity(IssueSecurityLevel.IssueSecurity.GROUP, Groups.USERS);
        administration.issueSecuritySchemes().getScheme(NEW_SECURITY_SCHEME_NAME).
                newLevel(SECURITY_LEVEL_THREE_NAME, SECURITY_LEVEL_THREE_DESC).
                addIssueSecurity(IssueSecurityLevel.IssueSecurity.GROUP, DEVELOPERS);

        //associate the project with the new scheme and check the levels are updated
        administration.project().associateIssueLevelSecurityScheme(PROJECT_HOMOSAP, NEW_SECURITY_SCHEME_NAME);
        assertIssueSecurityLevelOptions(new String[]{"None", SECURITY_LEVEL_THREE_NAME, SECURITY_LEVEL_TWO_NAME});

        //associate back to the original scheme and check the levels are updated again
        administration.project().associateIssueLevelSecurityScheme(PROJECT_HOMOSAP, SECURITY_SCHEME_NAME);
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);
    }

    @Test
    public void testAddingAndRemovingSecurityLevelFlushesCache() {
        //initiailly we can see the security level field
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);

        //add a new security level to the scheme
        administration.issueSecuritySchemes()
                .getScheme(SECURITY_SCHEME_NAME)
                .newLevel(SECURITY_LEVEL_THREE_NAME, SECURITY_LEVEL_THREE_DESC);
        //there is no user/role/group associated yet, so should not have changed
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);
        //add group to security level that we can see with
        administration.issueSecuritySchemes()
                .getScheme(SECURITY_SCHEME_NAME)
                .getLevel(SECURITY_LEVEL_THREE_NAME)
                .addIssueSecurity(IssueSecurityLevel.IssueSecurity.GROUP, Groups.USERS);
        //now check that the level has been added
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_GREEN);

        //add another level (this time with no permission to view it)
        administration.issueSecuritySchemes()
                .getScheme(SECURITY_SCHEME_NAME)
                .newLevel(SECURITY_LEVEL_TWO_NAME, SECURITY_LEVEL_TWO_DESC);
        //this should be the same as the previous
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_GREEN);
        //add role that we are not part of

        administration.issueSecuritySchemes()
                .getScheme(SECURITY_SCHEME_NAME)
                .getLevel(SECURITY_LEVEL_THREE_NAME)
                .addRole(JIRA_ADMIN_ROLE);
        //now check that the level has not changed (since we cannot view the level)
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_GREEN);

        //remove the security level we can see and check it updates the list
        administration.issueSecuritySchemes()
                .getScheme(SECURITY_SCHEME_NAME)
                .getLevel(SECURITY_LEVEL_THREE_NAME)
                .delete();
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);

        //remove the security level we cannot see and check it didnt change anything
        administration.issueSecuritySchemes()
                .getScheme(SECURITY_SCHEME_NAME)
                .getLevel(SECURITY_LEVEL_TWO_NAME)
                .delete();
        assertIssueSecurityLevelOptions(EXPECTED_SECURITY_LEVEL_NONE);
    }

    private void _testNewProjectsWithDefaultRolesHaveCorrectSecurityLevel() {
        //check that as the admin we can see all security levels
        assertIssueSecurityLevelOptions(NEW_PROJECT_NAME, ISSUE_TYPE_BUG, EXPECTED_SECURITY_LEVEL_RED);

        //login as a user and check we can only see 'None'
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        assertIssueSecurityLevelOptions(NEW_PROJECT_NAME, ISSUE_TYPE_BUG, EXPECTED_SECURITY_LEVEL_NONE);
    }

    private void assertIssueSecurityLevelOptions(String[] expectedSecurityLevels) {
        assertIssueSecurityLevelOptions(PROJECT_HOMOSAP, ISSUE_TYPE_BUG, expectedSecurityLevels);
    }

    /**
     * checks that the security level select field on the create issue details page has the expectedSecurityLevels
     *
     * @param projectName            name of the project to select in the create issue page
     * @param issueType              issue type to select in the create issue page
     * @param expectedSecurityLevels the expected values of the security level select field
     */
    private void assertIssueSecurityLevelOptions(String projectName, String issueType, String[] expectedSecurityLevels) {
        navigation.issue().goToCreateIssueForm(projectName, issueType);
        tester.getDialog().setWorkingForm("issue-create");
        tester.assertOptionsEqual("security", expectedSecurityLevels);
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("security", "None");
    }
}
