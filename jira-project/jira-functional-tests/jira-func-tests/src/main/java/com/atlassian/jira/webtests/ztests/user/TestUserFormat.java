package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.admin.TimeTracking;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.TableCellLocator;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.meterware.httpunit.WebLink;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.permission.ProjectPermissions.ADD_COMMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.EDIT_OWN_COMMENTS;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Func test to ensure that all pluginised profile page links in JIRA are being displayed correctly.
 *
 * @since v3.13
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestUserFormat extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
    }

    @Test
    public void testAnonymous() {
        //test anonymous comment.
        String issueKey = navigation.issue().createIssue(PROJECT_HOMOSAP, null, "First test bug");
        navigation.issue().viewIssue(issueKey);
        //create a comment
        tester.clickLink("footer-comment-button");
        tester.setFormElement("comment", "My first test comment");
        tester.submit();

        //comments
        assertions.assertProfileLinkPresent("commentauthor_10000_verbose", ADMIN_FULLNAME);

        //grant anyone browse issue and comment permissions.
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_PERM_SCHEME_ID, BROWSE_PROJECTS);
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_PERM_SCHEME_ID, ADD_COMMENTS);

        navigation.logout();
        navigation.issue().viewIssue(issueKey);
        tester.clickLink("footer-comment-button");
        tester.setFormElement("comment", "My second anoymous test comment");
        tester.submit();

        tester.assertLinkNotPresent("Anonymous");
        textAssertions.assertTextSequence(new WebPageLocator(tester), "Anonymous", "added a comment");
    }

    @Test
    public void testCommentPanel() {
        //need to grant anyone edit comment permissions.
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_PERM_SCHEME_ID, EDIT_OWN_COMMENTS);

        String issueKey = navigation.issue().createIssue(PROJECT_HOMOSAP, null, "First test bug");
        navigation.issue().viewIssue(issueKey);
        //create a comment
        tester.clickLink("footer-comment-button");
        tester.setFormElement("comment", "My first test comment");
        tester.submit();

        //check the edit comment view
        tester.clickLink("edit_comment_10000");
        tester.setWorkingForm("comment-edit");
        assertions.assertProfileLinkPresent("comment_summary_admin", ADMIN_FULLNAME);
        tester.setFormElement("comment", "My first test comment edited...");
        //This is here because of MySQL. MySQL does not store the milliseconds in a datetime field.
        //If this test edits the comment within 1 second, then the jiraaction table will store the same
        //creation and edit time and thus the second "comment_summary_updated_admin" link will not be displayed.

        try {
            Thread.sleep(1001L);
        } catch (InterruptedException ignored) {

        }
        tester.submit("Save");

        //now we should have an updated author as well
        tester.clickLink("edit_comment_10000");
        tester.setWorkingForm("comment-edit");
        assertions.assertProfileLinkPresent("comment_summary_admin", ADMIN_FULLNAME);
        assertions.assertProfileLinkPresent("comment_summary_updated_admin", ADMIN_FULLNAME);

        //now the delete comment view
        navigation.issue().viewIssue(issueKey);
        tester.clickLink("delete_comment_10000");
        assertions.assertProfileLinkPresent("comment_summary_admin", ADMIN_FULLNAME);
        assertions.assertProfileLinkPresent("comment_summary_updated_admin", ADMIN_FULLNAME);
    }

    //viewprojects.jsp
    @Test
    public void testViewProjects() {
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_PROJECTS);

        assertions.assertProfileLinkPresent("view_HSP_projects_admin", ADMIN_FULLNAME);
    }

    //viewvoters.jsp
    @Test
    public void testViewVoters() {
        String issueKey = navigation.issue().createIssue(PROJECT_HOMOSAP, null, "First test bug");

        navigation.logout();
        navigation.login(FRED_USERNAME);
        navigation.issue().viewIssue(issueKey);
        tester.clickLink("toggle-vote-issue");

        navigation.logout();
        navigation.login(ADMIN_USERNAME);
        navigation.issue().viewIssue(issueKey);
        tester.clickLink("view-voters");

        assertions.assertProfileLinkPresent("voter_link_fred", FRED_FULLNAME);
    }

    //tests anything under /views/user
    @Test
    public void testFullProfileIsSafeFromXSS() {
        tester.gotoPage("/secure/EditProfile!default.jspa?username=admin");
        tester.setFormElement("fullName", ADMIN_USERNAME + " \"<script>alert('owned')</script>\"");
        tester.setFormElement("email", "\"<script>alert('owned')</script>\"@localhost");
        tester.setFormElement("password", "admin");
        tester.submit();

        tester.gotoPage("/secure/ViewProfile.jspa");
        boolean condition3 = tester.getDialog().getResponseText().indexOf("User Profile: " + ADMIN_USERNAME + " &quot;&lt;script&gt;alert(&#39;owned&#39;)&lt;/script&gt;&quot;") != -1;
        assertTrue(condition3);
        boolean condition2 = tester.getDialog().getResponseText().indexOf("User Profile: " + ADMIN_USERNAME + " \"<script>alert(&#39;owned&#39;)</script>\"") == -1;
        assertTrue(condition2);
        boolean condition1 = tester.getDialog().getResponseText().indexOf("mailto:&quot;&lt;script&gt;alert(&#39;owned&#39;)&lt;/script&gt;&quot;") != -1;
        assertTrue(condition1);
        boolean condition = tester.getDialog().getResponseText().indexOf("mailto:\"<script>alert('owned')</script>\"") == -1;
        assertTrue(condition);
    }

    //IssueSummaryWebComponent
    @Test
    public void testIssueSummary() {
        //allow all user's to be assigned issues
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGNABLE_USER);

        String issueKey = navigation.issue().createIssue(PROJECT_HOMOSAP, null, "First test bug");
        navigation.issue().viewIssue(issueKey);
        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field");
            //assign the issue to fred
            tester.clickLink("assign-issue");
            tester.selectOption("assignee", FRED_FULLNAME);
            tester.submit("Assign");
        } finally {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field");
        }

        assertions.assertNodeByIdHasText("issue_summary_assignee_fred", FRED_FULLNAME);
        assertions.assertNodeByIdHasText("issue_summary_reporter_admin", ADMIN_FULLNAME);
    }

    //assignee-columnview.vm & reporter-columnview.vm
    @Test
    public void testColumnView() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        navigation.issue().createIssue(PROJECT_HOMOSAP, null, "First test bug");

        navigation.issueNavigator().displayAllIssues();
        assertions.assertProfileLinkPresent("assignee_admin", ADMIN_FULLNAME);
        assertions.assertProfileLinkPresent("reporter_admin", ADMIN_FULLNAME);
    }

    //assignee-columnview.vm & reporter-columnview.vm - JRA-15578
    @Test
    public void testColumnViewUnAssigned() throws SAXException {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        navigation.gotoAdminSection(Navigation.AdminSection.GENERAL_CONFIGURATION);
        tester.clickLink("edit-app-properties");
        tester.checkCheckbox("allowUnassigned", "true");
        tester.submit("Update");

        navigation.issue().goToCreateIssueForm(null, null);
        tester.setFormElement("summary", "Unassigned bug");
        tester.selectOption("assignee", "Unassigned");
        tester.submit("Create");

        navigation.issueNavigator().displayAllIssues();
        final WebLink link = tester.getDialog().getResponse().getLinkWithID("assignee_admin");
        assertNull(link);
        textAssertions.assertTextPresent(new TableCellLocator(tester, "issuetable", 1, 3), "Unassigned");

        assertions.assertProfileLinkPresent("reporter_admin", ADMIN_FULLNAME);
    }

    //pickertable.vm
    @Test
    public void testIssueWatchers() {
        navigation.userProfile().changeAutowatch(false);
        final String issueKey = navigation.issue().createIssue(PROJECT_HOMOSAP, null, "First test bug");
        navigation.issue().viewIssue(issueKey);

        //start watching
        tester.clickLink("toggle-watch-issue");

        tester.clickLink("manage-watchers");
        assertions.assertProfileLinkPresent("watcher_link_admin", ADMIN_FULLNAME);
    }

    //view-multiuser.vm & view-user.vm
    @Test
    public void testCustomFields() {
        //add custom fields
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        //add a single user
        tester.clickLink("add_custom_fields");
        tester.checkCheckbox("fieldType", "com.atlassian.jira.plugin.system.customfieldtypes:userpicker");
        tester.submit("nextBtn");
        tester.setFormElement("fieldName", "Single User");
        tester.submit("nextBtn");
        tester.checkCheckbox("associatedScreens", "1");
        tester.submit("Update");

        //add a multi-user
        tester.clickLink("add_custom_fields");
        tester.checkCheckbox("fieldType", "com.atlassian.jira.plugin.system.customfieldtypes:multiuserpicker");
        tester.submit("nextBtn");
        tester.setFormElement("fieldName", "Multi User");
        tester.submit("nextBtn");
        tester.checkCheckbox("associatedScreens", "1");
        tester.submit("Update");

        final String issueKey = navigation.issue().createIssue(PROJECT_HOMOSAP, null, "First test bug");
        navigation.issue().viewIssue(issueKey);

        //give the custom fields some values
        tester.clickLink("edit-issue");
        tester.setFormElement("customfield_10000", FRED_USERNAME);
        tester.setFormElement("customfield_10001", "admin, fred");
        tester.submit("Update");

        assertions.assertNodeByIdHasText("user_cf_fred", FRED_FULLNAME);
        assertions.assertNodeByIdHasText("multiuser_cf_fred", FRED_FULLNAME);
        assertions.assertNodeByIdHasText("multiuser_cf_admin", ADMIN_FULLNAME);
    }

    //macros.vm, changehistory.vm, worklog.vm
    @Test
    public void testActionHeaders() {
        //enable time tracking
        administration.timeTracking().enable(TimeTracking.Mode.LEGACY);

        String issueKey = navigation.issue().createIssue(PROJECT_HOMOSAP, null, "First test bug");
        navigation.issue().viewIssue(issueKey);
        //create a comment
        tester.clickLink("footer-comment-button");
        tester.setFormElement("comment", "My first test comment");
        tester.submit();
        //log some work
        tester.clickLink("log-work");
        tester.setFormElement("timeLogged", "1h");
        tester.clickButton("log-work-submit");
        //change something
        tester.clickLink("edit-issue");
        tester.setFormElement("summary", "First test bug really");
        tester.submit("Update");

        tester.gotoPage("/browse/HSP-1?page=com.atlassian.jira.plugin.system.issuetabpanels%3Aall-tabpanel");

        //comments
        assertions.assertProfileLinkPresent("commentauthor_10000_verbose", ADMIN_FULLNAME);

        //changehistory
        assertions.assertProfileLinkPresent("changehistoryauthor_10000", ADMIN_FULLNAME);
        assertions.assertProfileLinkPresent("changehistoryauthor_10001", ADMIN_FULLNAME);

        //worklog
        assertions.assertProfileLinkPresent("worklogauthor_10000", ADMIN_FULLNAME);
    }

    //developer-workload-report.vm
    @Test
    public void testDeveloperWorkloadReport() {
        //enable time tracking
        administration.timeTracking().enable(TimeTracking.Mode.LEGACY);

        String issueKey = navigation.issue().createIssue(PROJECT_HOMOSAP, null, "First test bug");
        navigation.issue().viewIssue(issueKey);
        //log some work
        tester.clickLink("log-work");
        tester.setFormElement("timeLogged", "1h");
        tester.clickButton("log-work-submit");

        //go to the developer workload report
        navigation.runReport((long) 10000, "com.atlassian.jira.jira-core-reports-plugin:developer-workload");
        tester.setFormElement("developer", ADMIN_USERNAME);
        tester.submit("Next");

        assertions.assertProfileLinkPresent("dev_wl_report_admin", ADMIN_FULLNAME);
    }

    // JRA-15748
    @Test
    public void testDifferentUsersInIterator() {
        administration.restoreData("TestFormatUserDifferentUsersInIterator.xml");
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_PROJECTS);

        assertions.assertProfileLinkPresent("view_HSP_projects_admin", ADMIN_FULLNAME);
        assertions.assertProfileLinkPresent("view_MKY_projects_fred", FRED_FULLNAME);

        tester.gotoPage("/secure/ViewVoters!default.jspa?id=10001");
        assertions.assertProfileLinkPresent("voter_link_admin", ADMIN_FULLNAME);
        assertions.assertProfileLinkPresent("voter_link_fred", FRED_FULLNAME);
    }
}
