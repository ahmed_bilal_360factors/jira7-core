package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestXsrfProjectCategories extends BaseJiraFuncTest {

    @Inject
    private Form form;

    @Test
    public void testProjectCategoryAdministration() throws Exception {
        // Note: Associating categories with projects is already tested in TestXsrfProject

        new XsrfTestSuite(
                new XsrfCheck("AddCategory", new ProjectCategorySetup() {
                    public void setup() {
                        super.setup();
                        tester.setFormElement("name", "New Category");
                    }
                }, new XsrfCheck.FormSubmission("Add")),
                new XsrfCheck("EditCategory", new ProjectCategorySetup() {
                    public void setup() {
                        super.setup();
                        navigation.clickLinkWithExactText("Edit");
                        tester.setFormElement("description", "This is a category!");
                    }
                }, new XsrfCheck.FormSubmission("Update")),
                new XsrfCheck("DeleteCategory", new ProjectCategorySetup() {
                    public void setup() {
                        super.setup();
                        navigation.clickLinkWithExactText("Delete");
                    }
                }, new XsrfCheck.FormSubmission("Delete"))
        ).run(getTester(), navigation, form);
    }

    class ProjectCategorySetup implements XsrfCheck.Setup {
        public void setup() {
            navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CATEGORIES);
            tester.setWorkingForm("jiraform");
        }
    }
}