package com.atlassian.jira.webtests.ztests.workflow;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @since v3.13
 */
public class ExpectedChangeHistoryItem {
    private final String fieldName;
    private final List<String> oldValue;
    private final List<String> newValue;

    public ExpectedChangeHistoryItem(final String fieldName, final String oldValue, final String newValue) {
        this.fieldName = fieldName;
        this.oldValue = Collections.singletonList(oldValue);
        this.newValue = Collections.singletonList(newValue);
    }

    public ExpectedChangeHistoryItem(final String fieldName, final Iterable<String> oldValue, final Iterable<String> newValue) {
        this.fieldName = fieldName;
        this.oldValue = ImmutableList.copyOf(oldValue);
        this.newValue = ImmutableList.copyOf(newValue);
    }

    public String getFieldName() {
        return fieldName;
    }

    public Collection<String> getOldValues() {
        return oldValue;
    }

    public Collection<String> getNewValues() {
        return newValue;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
