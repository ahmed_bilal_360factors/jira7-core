package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.admin.IssueSecurityLevel;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.ResourceRef;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.webtests.Groups;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_FIELD_CONFIGURATION;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.WIKI_STYLE_RENDERER;
import static com.atlassian.jira.functest.framework.Navigation.AdminSection.FIELD_CONFIGURATION;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.junit.Assert.assertNull;

@WebTest({Category.FUNC_TEST, Category.ISSUES})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestInlineIssueLinking extends BaseJiraFuncTest {

    String summary1;
    String summary2;
    String issueKey1;
    String issueKey2;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        if (administration.project().projectWithKeyExists(PROJECT_HOMOSAP_KEY)) {
            logger.log("Project: " + PROJECT_HOMOSAP + " exists");
        } else {
            administration.project().addProject(PROJECT_HOMOSAP, PROJECT_HOMOSAP_KEY, ADMIN_USERNAME);
        }

        administration.issueSecuritySchemes().newScheme("admin_only_scheme", "").newLevel("admin_only", "").
                addIssueSecurity(IssueSecurityLevel.IssueSecurity.GROUP, Groups.ADMINISTRATORS);
        administration.project().associateIssueLevelSecurityScheme(PROJECT_HOMOSAP, "admin_only_scheme");

        administration.usersAndGroups().addUser("link_user", "link_user", "link_user", "link_user@local");

        // Check summary does not show when there is no user
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_PERM_SCHEME_ID, BROWSE_PROJECTS);
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY);

        summary1 = "summary1";
        summary2 = "summary2";

        issueKey1 = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, summary1, ADMIN_USERNAME, "Minor", "Bug").key();
        backdoor.issues().setIssueFields(issueKey1,
                new IssueFields()
                        .environment("test environment 1")
                        .description("test description to clone 1")
                        .securityLevel(ResourceRef.withName("admin_only")));
        issueKey2 = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, summary2, ADMIN_USERNAME, "Minor", "Bug").key();
        backdoor.issues().setIssueFields(issueKey2,
                new IssueFields()
                        .environment("test environment 1")
                        .description(issueKey1));
    }

    @Test
    public void testSummaryOnLink() throws SAXException {
        checkForUserWithPerm();
        checkForNoUser();
        checkForUserNoPerm();

        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);

        setFieldConfigurationFieldToRenderer(DEFAULT_FIELD_CONFIGURATION, "description", WIKI_STYLE_RENDERER);

        checkForUserWithPerm();
        checkForNoUser();
        checkForUserNoPerm();
    }

    private void checkForUserNoPerm() throws SAXException {
        // Check summary does not exist for user without permission
        navigation.logout();
        navigation.login("link_user", "link_user");
        navigation.issue().gotoIssue(issueKey2);
        tester.assertTextPresent("Log Out");
        tester.assertTextNotPresent("title=\"" + summary1 + "\""); // check summary is not there
        assertNull("Link should not be there", tester.getDialog().getResponse().getLinkWith(issueKey1));
    }

    private void checkForNoUser() throws SAXException {
        navigation.logout();

        navigation.issue().gotoIssue(issueKey2);
        tester.assertTextPresent("Log In");
        tester.assertTextNotPresent("title=\"" + summary1 + "\""); // check summary is not there
        assertNull("Link should not be there", tester.getDialog().getResponse().getLinkWith(issueKey1)); // check link is not there (JRA-14893)
    }

    private void checkForUserWithPerm() {
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.issue().gotoIssue(issueKey2);
        tester.assertTextPresent("title=\"" + summary1 + "\""); // check summary is there
        tester.assertTextPresent("/browse/" + issueKey1); //check link is there
    }

    private void setFieldConfigurationFieldToRenderer(String configuration, String fieldId, String renderer) {
        navigation.gotoPage(FIELD_CONFIGURATION.getUrl());
        textAssertions.assertTextPresent("View Field Configurations");
        tester.clickLink("configure-" + configuration);
        textAssertions.assertTextPresent(configuration);

        tester.clickLink("renderer_" + fieldId);
        tester.assertTextPresent("Edit Field Renderer");
        tester.selectOption("selectedRendererType", renderer);
        tester.submit("Update");
        tester.assertTextPresent("Edit Field Renderer Confirmation");
        tester.assertTextPresent(renderer);
        tester.submit("Update");
        logger.log("Set " + fieldId + " to renderer type " + renderer + " in the " + configuration + " configuration.");
    }
}
