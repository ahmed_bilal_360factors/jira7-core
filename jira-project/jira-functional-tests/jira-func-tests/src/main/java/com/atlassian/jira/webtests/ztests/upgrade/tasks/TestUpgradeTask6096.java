package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.UPGRADE_TASKS, Category.TIME_TRACKING, Category.WORKLOGS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestUpgradeTask6096 extends BaseJiraFuncTest {
    private static final String HSP_1 = "HSP-1";

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("TestUpgradeTask6096.xml");
    }

    @Test
    public void testWorklogsResolveMixedCaseUsers() {
        navigation.issue().viewIssue(HSP_1);
        clickWorkLogLink();

        // Make sure the names are mapped correctly
        textAssertions.assertTextPresent("Fred Normal");
        textAssertions.assertTextPresent("Recycled User");
        textAssertions.assertTextPresent("missing");
        textAssertions.assertTextPresent("mel role");
        textAssertions.assertTextPresent("deleted_user");

        // And that keys are not showing up unexpectedly
        textAssertions.assertTextNotPresent("ID12345");
        textAssertions.assertTextNotPresent("zapped");
        textAssertions.assertTextNotPresent("frEd");
        // don't try this with "meL" ...  that string is present from unrelated JS code
    }

    private void clickWorkLogLink() {
        if (page.isLinkPresentWithExactText("Work Log")) {
            getTester().clickLinkWithText("Work Log");
        }
    }
}
