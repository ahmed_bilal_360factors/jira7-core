package com.atlassian.jira.webtests.ztests.attachment;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SYS_ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SYS_ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.ATTACHMENTS, Category.ISSUES})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueFileAttachments extends BaseJiraFuncTest {
    private static final String ATTACHMENT_SETTINGS_TABLE_ID = "table-AttachmentSettings";
    private static final int ALLOW_ATTACHMENTS_VALUE_COLUMN_NUMBER = 1;
    private static final int ALLOW_ATTACHMENTS_ROW_NUMBER = 0;
    private static final int ENABLE_THUMBNAILS_ROW_NUMBER = 3;
    private static final int ENABLE_THUMBNAILS_VALUE_COLUMN_NUMBER = 1;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testIssueFileAttachmentEnableThumbnailsAsAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            administration.attachments().enable();

            navigation.logout();
            navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);

            navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);
            assertThatAttachmentsAreEnabled();

            navigation.clickLinkWithExactText("Edit Settings");
            tester.checkCheckbox("thumbnailsEnabled", "false");
            tester.submit("Update");
            assertThatThumbnailsAreDisabled();
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testCreateAttachmentsWhenAttachmentsAreDisabled() {
        administration.restoreData("TestDeleteAttachments.xml");
        tester.gotoPage("/secure/AttachFile!default.jspa?id=10000");
        tester.setWorkingForm("attach-file");
        tester.submit();
        textAssertions.assertTextPresent(locator.page(), "Attachments have been disabled for this instance of JIRA.");
    }

    private void assertThatAttachmentsAreEnabled() {
        textAssertions.assertTextPresent
                (
                        locator.cell
                                (
                                        ATTACHMENT_SETTINGS_TABLE_ID, ALLOW_ATTACHMENTS_ROW_NUMBER,
                                        ALLOW_ATTACHMENTS_VALUE_COLUMN_NUMBER
                                ),
                        "ON"
                );
    }

    private void assertThatThumbnailsAreDisabled() {
        textAssertions.assertTextSequence
                (
                        locator.cell
                                (
                                        ATTACHMENT_SETTINGS_TABLE_ID,
                                        ENABLE_THUMBNAILS_ROW_NUMBER, ENABLE_THUMBNAILS_VALUE_COLUMN_NUMBER
                                ),
                        "OFF"
                );
    }

}
