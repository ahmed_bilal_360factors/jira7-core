package com.atlassian.jira.webtests.ztests.tpm.ldap;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * Run a mock server that will return empty users list on REST call to usermanagment API
 *
 * @since v7.2
 */
class MockCrowdServerRule implements TestRule {

    private static final int PORT = 44455;

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try(AutoCloseable ignored = withServer()) {
                    base.evaluate();
                }
            }
        };
    }

    public String url() {
        return "http://localhost:" + PORT;
    }

    private AutoCloseable withServer() throws Exception {
        final Server server = new Server(PORT);
        final HandlerList list = new HandlerList();

        list.addHandler(createContext(new UserManagment(), "/", "/rest/usermanagement/1/search"));

        server.setHandler(list);
        server.start();
        return server::stop;
    }

    private ServletContextHandler createContext(final HttpServlet handler, final String contextPath, final String pathSpec) {
        final ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath(contextPath);
        context.addServlet(new ServletHolder(handler), pathSpec);
        return context;
    }

    private class UserManagment extends HttpServlet {
        private static final String EMBEDDED_CROWD_VERSION_NAME = "X-Embedded-Crowd-Version";
        private static final String RANDOM_VERSION = "1";

        @Override
        protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
            response.setHeader(EMBEDDED_CROWD_VERSION_NAME, RANDOM_VERSION);
            respondWithData(response, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><users expand=\"user\"/>");
        }
    }

    private void respondWithData(final HttpServletResponse response, final String responseText) throws IOException {
        response.setContentType(MediaType.APPLICATION_XML);
        response.getWriter().write(responseText);
        response.getWriter().close();
    }
}
