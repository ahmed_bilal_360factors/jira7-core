package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookRegistrationClient;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.IssueUpdateRequest;
import com.atlassian.jira.rest.api.issue.ResourceRef;
import com.atlassian.jira.testkit.client.restclient.IssueTransitionsMeta;
import com.atlassian.jira.testkit.client.restclient.TransitionsClient;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@WebTest({Category.FUNC_TEST, Category.WEBHOOKS, Category.REFERENCE_PLUGIN})
public class TestWebHookPostFunction extends AbstractWebHookTest {
    private TransitionsClient transitionsClient;

    @Before
    @Override
    public void setUpTest() {
        super.setUpTest();
        transitionsClient = new TransitionsClient(environmentData);
    }

    /*
         Monkey Post function web hook
             Start Progress of monkey Workflow
             Start Progress of homosapien Workflow
             Resolve Issue of monkey Workflow
             Close Issue of monkey Workflow
         Homosapien Post function web hook
            Start Progress of monkey Workflow
            Start Progress of homosapien Workflow
            Stop Progress of homosapien Workflow
            Close Issue of monkey Workflow
     */

    @Test
    public void testFiringWebHookPostFunction() throws IOException, InterruptedException {
        backdoor.restoreDataFromResource("TestWebHookPostFunction.zip");

        final HttpResponseTester homosapienTester = new HttpResponseTester(environmentData);
        final HttpResponseTester monkeyTester = new HttpResponseTester(environmentData);

        homosapienTester.start();
        monkeyTester.start();

        final WebHookRegistrationClient client = new WebHookRegistrationClient(environmentData);
        final WebHookRegistrationClient.RegistrationResponse homosapienWebHook = client.getWebHook("1");
        final WebHookRegistrationClient.Registration homosapienWebHookRegistration = new WebHookRegistrationClient.Registration();
        homosapienWebHookRegistration.setFilterForIssueSection(homosapienWebHook.getFilterForIssueSection());
        homosapienWebHookRegistration.name = homosapienWebHook.name;
        homosapienWebHookRegistration.url = homosapienTester.getPath();
        client.update("1", homosapienWebHookRegistration);

        final WebHookRegistrationClient.RegistrationResponse monkeyWebHook = client.getWebHook("2");
        final WebHookRegistrationClient.Registration monkeyWebHookRegistration = new WebHookRegistrationClient.Registration();
        monkeyWebHookRegistration.setFilterForIssueSection(monkeyWebHook.getFilterForIssueSection());
        monkeyWebHookRegistration.name = monkeyWebHook.name;
        monkeyWebHookRegistration.url = monkeyTester.getPath();
        client.update("2", monkeyWebHookRegistration);

        IssueCreateResponse hsp1 = backdoor.issues().createIssue("HSP", "HSP issue");
        startProgress(hsp1);

        // both monkey and homosapien listeners listen for startprogress of HSP project.
        // however, monkey doesn't match homosapien, because its filter is project = monkey
        WebHookResponseData monkeyTesterResponseData = monkeyTester.getResponseData();
        assertNull(monkeyTesterResponseData);

        // homosapien listener matches monkey
        WebHookResponseData homosapienTesterResponseData = homosapienTester.getResponseData();
        assertNotNull(homosapienTesterResponseData);

        stopProgress(hsp1);
        assertNull(monkeyTester.getResponseData());
        assertNotNull(homosapienTester.getResponseData());

        IssueCreateResponse monkey1 = backdoor.issues().createIssue("MKY", "MKY issue");

        startProgress(monkey1);
        monkeyTesterResponseData = monkeyTester.getResponseData();
        assertNotNull(monkeyTesterResponseData);
        homosapienTesterResponseData = homosapienTester.getResponseData();
        assertNull(homosapienTesterResponseData);

        resolveIssue(monkey1);
        monkeyTesterResponseData = monkeyTester.getResponseData();
        assertNotNull(monkeyTesterResponseData);
        homosapienTesterResponseData = homosapienTester.getResponseData();
        assertNull(homosapienTesterResponseData);

        closeIssue(monkey1);
        homosapienTesterResponseData = homosapienTester.getResponseData();
        assertNull(homosapienTesterResponseData);

        homosapienTester.stop();
        monkeyTester.stop();
    }

    private void startProgress(final IssueCreateResponse issueCreateResponse) {
        final IssueTransitionsMeta issueTransitionsMeta = transitionsClient.get(issueCreateResponse.id());
        final IssueUpdateRequest transition = getTransitionByName(issueTransitionsMeta, "Start Progress");
        transitionsClient.postResponse(issueCreateResponse.key(), transition);
    }

    private void stopProgress(final IssueCreateResponse issueCreateResponse) {
        final IssueTransitionsMeta issueTransitionsMeta = transitionsClient.get(issueCreateResponse.id());
        final IssueUpdateRequest transition = getTransitionByName(issueTransitionsMeta, "Stop Progress");
        transitionsClient.postResponse(issueCreateResponse.key(), transition);
    }

    private void resolveIssue(final IssueCreateResponse issueCreateResponse) {
        final IssueTransitionsMeta issueTransitionsMeta = transitionsClient.get(issueCreateResponse.id());
        final IssueUpdateRequest transition = getTransitionByName(issueTransitionsMeta, "Resolve Issue");
        transitionsClient.postResponse(issueCreateResponse.key(), transition);
    }

    private void closeIssue(final IssueCreateResponse issueCreateResponse) {
        final IssueTransitionsMeta issueTransitionsMeta = transitionsClient.get(issueCreateResponse.id());
        final IssueUpdateRequest transition = getTransitionByName(issueTransitionsMeta, "Close Issue");
        transitionsClient.postResponse(issueCreateResponse.key(), transition);
    }

    private static IssueUpdateRequest getTransitionByName(IssueTransitionsMeta issueTransitionsMeta, String transitionName) {
        for (IssueTransitionsMeta.Transition transition : issueTransitionsMeta.transitions) {
            if (transition.name.equals(transitionName)) {
                final IssueUpdateRequest issueUpdateRequest = new IssueUpdateRequest();
                issueUpdateRequest.transition(ResourceRef.withId(String.valueOf(transition.id)));
                issueUpdateRequest.fields(new IssueFields());
                return issueUpdateRequest;
            }
        }
        return null;
    }

}
