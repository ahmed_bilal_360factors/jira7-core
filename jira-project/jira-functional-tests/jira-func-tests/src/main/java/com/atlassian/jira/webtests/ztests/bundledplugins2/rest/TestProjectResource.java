package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FuncTestUrlHelper;
import com.atlassian.jira.functest.framework.backdoor.ManagedPermissions;
import com.atlassian.jira.functest.framework.rule.IssueTypeUrls;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.v2.issue.project.ProjectBean;
import com.atlassian.jira.rest.v2.issue.project.ProjectIdentity;
import com.atlassian.jira.rest.v2.issue.project.ProjectInputBean;
import com.atlassian.jira.testkit.client.JiraHttpClient;
import com.atlassian.jira.testkit.client.restclient.Avatar;
import com.atlassian.jira.testkit.client.restclient.Component;
import com.atlassian.jira.testkit.client.restclient.Errors;
import com.atlassian.jira.testkit.client.restclient.IssueType;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.atlassian.jira.testkit.client.restclient.ProjectClient;
import com.atlassian.jira.testkit.client.restclient.ProjectUpdateField;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.testkit.client.restclient.Version;
import com.atlassian.jira.testkit.client.restclient.matcher.CorrectlyUpdatedProjectMatcher;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.UniformInterfaceException;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.matchers.RegexMatchers.regexMatches;
import static com.atlassian.jira.testkit.client.restclient.ProjectClient.UpdateBean;
import static com.atlassian.jira.testkit.client.restclient.ProjectClient.UpdateBean.UpdateBeanBuilder;
import static com.google.common.base.Strings.nullToEmpty;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;
import static com.sun.jersey.api.client.ClientResponse.Status.BAD_REQUEST;
import static com.sun.jersey.api.client.ClientResponse.Status.CREATED;
import static com.sun.jersey.api.client.ClientResponse.Status.FORBIDDEN;
import static com.sun.jersey.api.client.ClientResponse.Status.NOT_FOUND;
import static com.sun.jersey.api.client.ClientResponse.Status.NO_CONTENT;
import static com.sun.jersey.api.client.ClientResponse.Status.OK;
import static com.sun.jersey.api.client.ClientResponse.Status.UNAUTHORIZED;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Func test for ProjectResource.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@Restore("TestProjectResource.xml")
public class TestProjectResource extends BaseJiraFuncTest {
    @Rule
    public IssueTypeUrls issueTypeUrls = new IssueTypeUrls();

    @Inject
    private FuncTestUrlHelper urlHelper;
    
    public static final GenericType<List<Project>> PROJECT_LIST_TYPE = new GenericType<List<Project>>() {
    };

    private enum User {
        ADMIN("admin"), FRED("fred"), DODO("dodo"), ANONYMOUS(null);

        public final String loginName;

        private User(final String loginName) {
            this.loginName = loginName;
        }
    }

    /**
     * The fields available to be expanded.
     */
    private static final String EXPECTED_EXPAND = "description,lead,url,projectKeys";

    public static final String LARGE_PROJECT_KEY = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private Long defaultAvatarId;

    private ProjectClient projectClient;
    private ProjectCategoryClient categoryClient;

    @Test
    public void testCorrectInfoIsReturnedWhenUserHasAccessToProject() throws Exception {
        assertCorrectProjectIsReturned(projectMKY(), User.ADMIN, User.FRED);
        assertCorrectProjectIsReturned(projectATL(), User.ADMIN, User.FRED, User.ANONYMOUS);
        assertCorrectProjectIsReturned(projectHID(), User.ADMIN);
        assertCorrectProjectIsReturned(projectFRED(), User.ADMIN, User.FRED);
        assertCorrectProjectIsReturned(projectDODO(), User.ADMIN);
    }

    @Test
    public void testUserCannotAccessProjectByKeyIfTheyDoNotHavePermissions() throws Exception {
        assertCantSeeProject(User.FRED, "HID");
        assertCantSeeProject(User.ANONYMOUS, "MKY");
        assertCantSeeProject(User.ANONYMOUS, "HID");
        assertCantSeeProject(User.ANONYMOUS, "FRED");
    }

    @Test
    public void testUserCannotAccessProjectByIdIfTheyDoNotHavePermissions() throws Exception {
        assertCantSeeProject(User.FRED, 10110L);
        assertCantSeeProject(User.ANONYMOUS, 10001L);
        assertCantSeeProject(User.ANONYMOUS, 10110L);
        assertCantSeeProject(User.ANONYMOUS, 10111L);
    }

    @Test
    public void testAllProjectsUserHasAccessToAreReturnedWhenAskingForAllProjects() throws Exception {
        assertGetAllProjectsReturns(User.ADMIN, projectATL(), projectDODO(), projectFRED(), projectHID(), projectHSP(), projectMKY());
        assertGetAllProjectsReturns(User.FRED, projectATL(), projectFRED(), projectHSP(), projectMKY());
        assertGetAllProjectsReturns(User.ANONYMOUS, projectATL());
    }

    @Test
    public void testThatRecentProjectsAreReturned() {
        createHttpClient().loginAs("admin").browseProject("MKY").browseProject("HSP").browseProject("ATL");
        assertGetRecentProjectsReturns(User.ADMIN, projectATL(), projectHSP(), projectMKY());
    }

    @Test
    public void testThatOnlyRecentProjectsTheUserHasAccessToAreReturned() {
        ManagedPermissions permissions = backdoor.permissionsOf(User.FRED.loginName).forProject("HSP");
        permissions.add(ProjectPermissions.BROWSE_PROJECTS);
        createHttpClient().loginAs(User.FRED.loginName).browseProject("HSP");

        assertGetRecentProjectsReturns(User.FRED, projectHSP());

        permissions.remove(ProjectPermissions.BROWSE_PROJECTS);

        assertGetRecentProjectsReturns(User.FRED);
    }

    @Test
    public void testNothingBadHappensWhenGettingRecentlyAccessedAndThenDeletedProject() {
        createHttpClient().loginAs("admin").browseProject("HSP");
        assertGetRecentProjectsReturns(User.ADMIN, projectHSP());
        backdoor.project().deleteProject("HSP");
        assertGetRecentProjectsReturns(User.ADMIN);
    }

    @Test
    public void testThatExpandingFieldsWorksAsExpectedForRecentProjects() {
        createHttpClient().loginAs(User.FRED.loginName).browseProject("HSP").browseProject("MKY");
        assertEquals(newArrayList(transform(asList(projectMKY(), projectHSP()), toSimpleWithDescription)),
                loggedAs(User.FRED).getRecentProjects("description", 5).getEntity(PROJECT_LIST_TYPE));
        assertEquals(newArrayList(transform(asList(projectMKY(), projectHSP()), toSimpleWithLead)),
                loggedAs(User.FRED).getRecentProjects("lead", 5).getEntity(PROJECT_LIST_TYPE));
        assertEquals(newArrayList(transform(asList(projectMKY(), projectHSP()), toSimpleWithDescriptionAndLead)),
                loggedAs(User.FRED).getRecentProjects("lead,description", 5).getEntity(PROJECT_LIST_TYPE));
    }

    @Test
    public void testAnonymousGetsRecentProjectsBasedOnSession() {
        CookiePolicy.registerCookieSpec(CookiePolicy.DEFAULT, CookiePolicy.getCookieSpec(CookiePolicy.BROWSER_COMPATIBILITY).getClass());

        JiraHttpClient client = createHttpClient();

        assertThat(client.getRecentProjectsViaRestAPI(5), hasSize(0));
        client.browseProject("ATL");
        assertThat(client.getRecentProjectsViaRestAPI(5), hasItem(Matchers.equalTo(toSimple.apply(projectATL()))));

        assertThat(createHttpClient().getRecentProjectsViaRestAPI(5), hasSize(0)); // new client, different session
    }

    @Test
    public void testEmptyListIsReturnedWhenUserDoesNotHaveAnyRecentProjects() {
        assertGetRecentProjectsReturns(User.FRED);
    }

    private void assertGetAllProjectsReturns(final User user, final Project... projects) {
        final List<Project> actual = loggedAs(user).getProjects();
        assertEquals(newArrayList(transform(asList(projects), toSimple)), actual);
    }

    private void assertGetRecentProjectsReturns(final User user, final Project... projects) {
        final List<Project> actual = loggedAs(user).getRecentProjects(20).getEntity(PROJECT_LIST_TYPE);
        assertEquals(newArrayList(transform(asList(projects), toSimple)), actual);
    }

    @Test
    public void testNotFoundResponseIsReturnedWhenAskingForNonExistingProject() throws Exception {

        checkProjectNotFound("key", "XXX");
        checkProjectNotFound("id", "20000");
    }

    @Test
    public void testErrorIsReturnedWhenTryingToGetNegativeNumberOfRecentProjects() {
        assertResponseContainsAnyOfErrorMessages(projectClient.getRecentProjects(-1), "Number must not be negative");
    }

    private void checkProjectNotFound(final String fieldName, final String projectIdOrKey) {
        Response respXXX = projectClient.getResponse(projectIdOrKey);
        assertEquals(404, respXXX.statusCode);
        assertEquals(1, respXXX.entity.errorMessages.size());
        assertThat(respXXX.entity.errorMessages, contains(String.format("No project could be found with %s '%s'.", fieldName, projectIdOrKey)));
    }

    @Test
    public void testViewProjectVersions() throws Exception {
        //Make sure no versions works.
        assertTrue(projectClient.getVersions("10001").isEmpty());
        assertTrue(projectClient.getVersions("MKY").isEmpty());

        //Make sure it works for a particular project.
        assertEquals(createVersionsAtl(), projectClient.getVersions("10010"));
        assertEquals(createVersionsAtl(), projectClient.getVersions("ATL"));
    }

    @Test
    public void testViewProjectVersionsAnonymous() throws Exception {
        assertEquals(createVersionsAtl(), projectClient.anonymous().getVersions("10010"));
        assertEquals(createVersionsAtl(), projectClient.anonymous().getVersions("ATL"));

        checkProjectNotFoundForVersions("id", "10001");
        checkProjectNotFoundForVersions("key", "MKY");
    }

    private void checkProjectNotFoundForVersions(final String field, final String projectIdOrKey) {
        Response response = projectClient.getVersionsResponse(projectIdOrKey);
        assertEquals(404, response.statusCode);
        assertThat(response.entity.errorMessages, hasItem(String.format("No project could be found with %s '%s'.", field, projectIdOrKey)));
    }

    @Test
    public void testViewProjectComponents() throws Exception {
        //Make sure no components works.
        assertTrue(projectClient.getComponents("10001").isEmpty());
        assertTrue(projectClient.getComponents("MKY").isEmpty());

        //Make sure it works for a particular project.
        assertEquals(createComponentsHsp(), projectClient.getComponents("10000"));
        assertEquals(createComponentsHsp(), projectClient.getComponents("HSP"));
    }

    @Test
    public void testViewProjectComponentsAnonymous() throws Exception {
        assertEquals(createComponentsAtlFull(), projectClient.anonymous().getComponents("10010"));
        assertEquals(createComponentsAtlFull(), projectClient.anonymous().getComponents("ATL"));

        checkProjectNotFoundForComponents("id", "10001");
        checkProjectNotFoundForComponents("key", "MKY");
    }

    private void checkProjectNotFoundForComponents(final String field, final String projectIdOrKey) {
        try {
            projectClient.getComponents(projectIdOrKey);
            fail("Should throw exception.");
        } catch (UniformInterfaceException e) {
            final ClientResponse response = e.getResponse();
            assertEquals(404, response.getStatus());
            assertThat(response.getEntity(Errors.class).errorMessages, hasItem(String.format("No project could be found with %s '%s'.", field, projectIdOrKey)));
        }
    }

    @Test
    public void testAnonymousUserGetsAppropriateErrorWhenTryingToCreateProject() {
        ClientResponse response = loggedAs(User.ANONYMOUS).create(ProjectInputBean.CREATE_EXAMPLE);
        assertNotAuthenticatedResponse(response);
        assertProjectWasNotCreated(ProjectInputBean.CREATE_EXAMPLE);
    }


    @Test
    public void testNonAdminUserGetsAppropriateErrorWhenTryingToCreateProject() {
        ClientResponse response = loggedAs(User.FRED).create(ProjectInputBean.CREATE_EXAMPLE);
        assertInsufficientPrivilegesResponse(response);
        assertProjectWasNotCreated(ProjectInputBean.CREATE_EXAMPLE);
    }


    @Test
    public void testAdminCanCreateSimpleProject() {
        ProjectInputBean projectToCreate = sampleProject("New project").build();
        assertCreationSuccess(projectToCreate, projectClient.create(projectToCreate));
    }

    @Test
    public void testAdminCanCreateProjectUsingOnlyRequiredFields() {
        ProjectInputBean projectToCreate = ProjectInputBean.builder().setKey("REQ").setName("Only required").setLeadName("admin").setProjectTypeKey("business").build();
        assertCreationSuccess(projectToCreate, projectClient.create(projectToCreate));
    }

    @Test
    public void testWhenCreatingProjectWithNonExistingAvatarIdInvalidRequestResponseIsReturned() {
        ProjectInputBean projectWithNonExistentAvatar = sampleProject("New project").setAvatarId(78L).build();
        ClientResponse response = loggedAs(User.ADMIN).create(projectWithNonExistentAvatar);
        assertThat(response.getClientResponseStatus(), equalTo(ClientResponse.Status.BAD_REQUEST));
        assertResponseContainsAnyOfErrorMessages(response, "Avatar with id '78' does not exist.");
        assertProjectWasNotCreated(projectWithNonExistentAvatar);
    }

    @Test
    public void testCreatingProjectWithTheNameThatAlreadyExistsFails() {
        ProjectInputBean.Builder project = sampleProject("New project");
        assertResponseContainsAnyOfErrorMessages(projectClient.create(project.setName("Atlassian").build()), "A project with that name already exists.");
        assertProjectWasNotCreated(project.build());
    }

    @Test
    public void testCreatingProjectWithTheKeyThatAlreadyExistsFails() {
        ProjectInputBean.Builder project = sampleProject("New Project");
        assertResponseContainsAnyOfErrorMessages(projectClient.create(project.setKey("HSP").build()), "Project 'homosapien' uses this project key.");
        assertProjectWasNotCreated(project.build());
    }

    @Test
    public void testCreatingProjectWithNonExistingLeadFails() {
        ProjectInputBean project = sampleProject("Hello").setLeadName("Dude").build();
        assertResponseContainsAnyOfErrorMessages(projectClient.create(project), "You must specify a valid project lead.");
        assertProjectWasNotCreated(project);
    }

    @Test
    public void testCreatingProjectWithNonExistingSchemesFails() {
        assertResponseContainsAnyOfErrorMessages(projectClient.create(sampleProject("Hello").setIssueSecurityScheme(75L).build()), "Unable to validate, issue security scheme could not be retrieved.");
        assertResponseContainsAnyOfErrorMessages(projectClient.create(sampleProject("Hello").setNotificationScheme(75L).build()), "Unable to validate, notification scheme could not be retrieved.");
        assertResponseContainsAnyOfErrorMessages(projectClient.create(sampleProject("Hello").setPermissionScheme(75L).build()), "Unable to validate, permission scheme could not be retrieved.");
        assertProjectWasNotCreated(sampleProject("Hello").build());
    }

    @Test
    public void testCreatingProjectWithExistingSchemesSucceeds() {
        ProjectInputBean project = sampleProject("Hello").setIssueSecurityScheme(10000L).setNotificationScheme(10000L).setPermissionScheme(10100L).build();
        assertCreationSuccess(project, projectClient.create(project));
    }

    @Test
    public void testCreatingProjectWithNonExistentCategoryFails() {
        ProjectInputBean projectWithNonExistentCategory = sampleProject("Hello").setCategoryId(10042L).build();
        ClientResponse response = projectClient.create(projectWithNonExistentCategory);
        assertThat(response.getClientResponseStatus(), Matchers.equalTo(ClientResponse.Status.BAD_REQUEST));
        assertResponseContainsAnyOfErrorMessages(response, "The project category does not exist.");
        assertProjectWasNotCreated(projectWithNonExistentCategory);
    }

    @Test
    public void testProjectCanBeCreatedWithExistingCategory() {
        Long newCategory = categoryClient.createCategoryAndReturnId("newCategory", "Category for test");
        projectClient.create(sampleProject("Hello").setCategoryId(newCategory).build());
        Project createdProject = backdoor.project().getProject("HELL");
        assertThat(createdProject.projectCategory.name, equalTo("newCategory"));
        assertThat(createdProject.projectCategory.description, equalTo("Category for test"));
    }

    private void assertProjectWasNotCreated(final ProjectInputBean project) {
        try {
            Project returnedProject = backdoor.project().getProject(project.getKey());
            assertNotNull(returnedProject);
        } catch (UniformInterfaceException ex) {
            assertThat(ex.getResponse().getStatus(), equalTo(404));
        }
    }

    @Test
    public void testEachFieldCanBeUpdatedSeparately() {
        Map<ProjectUpdateField, String> updateFields = generateTestUpdateValues();
        for (ProjectUpdateField field : updateFields.keySet()) {
            Project originalProject = projectClient.getAndExpandAll(projectATL().key);
            UpdateBean updateBean = UpdateBean.builder().with(field, updateFields.get(field)).build();
            ClientResponse clientResponse = loggedAs(User.ADMIN).update(projectATL().key, updateBean);

            assertThat(clientResponse.getClientResponseStatus(), equalTo(OK));
            assertThat(projectClient.getAndExpandAll(originalProject.key), isUpdatedCorrectlyInAccordanceWith(updateBean, originalProject));
            clientResponse.close();
        }
    }

    @Test
    public void testAllFieldsCanBeUpdatedAtOnce() {
        Map<ProjectUpdateField, String> updateFields = generateTestUpdateValues();
        UpdateBeanBuilder updateBean = UpdateBean.builder();
        for (ProjectUpdateField field : updateFields.keySet()) {
            updateBean = updateBean.with(field, updateFields.get(field));
        }
        ClientResponse response = loggedAs(User.ADMIN).update(projectATL().key, updateBean.build());
        assertThat(response.getClientResponseStatus(), equalTo(OK));
        assertThat(projectClient.getAndExpandAll(projectATL().key), isUpdatedCorrectlyInAccordanceWith(updateBean.build(), projectATL()));
    }

    @Test
    public void testAnonymousGetsNotLoggedInResponseWhenUpdatingProject() {
        ClientResponse response = projectClient.anonymous().update("ATL", UpdateBean.builder().with(ProjectUpdateField.URL, "http://i-am-not-logged.in").build());
        assertNotAuthenticatedResponse(response);
    }

    @Test
    public void testNonAdminUserGetsForbiddenResponseWhenUpdatingProject() {
        ClientResponse response = loggedAs(User.FRED).update("ATL", UpdateBean.builder().with(ProjectUpdateField.URL, "http://i-am.fred").build());
        assertInsufficientPrivilegesResponse(response);
    }

    @Test
    public void testBadRequestResponseWhenUpdateDataIsNotRight() {
        ClientResponse response = loggedAs(User.ADMIN).update("ATL", UpdateBean.builder().with(ProjectUpdateField.URL, "incorrect-url").build());
        assertThat(response.getClientResponseStatus(), equalTo(BAD_REQUEST));
        assertResponseContainsAnyOfErrorMessages(response, "The URL specified is not valid - it must start with http://");
    }

    @Test
    public void test404WhenTryingToUpdateProjectThatDoesNotExist() {
        ClientResponse response = loggedAs(User.ADMIN).update("NULL", UpdateBean.builder().build());
        assertThat(response.getClientResponseStatus(), Matchers.equalTo(NOT_FOUND));
    }

    private Map<ProjectUpdateField, String> generateTestUpdateValues() {
        Map<ProjectUpdateField, String> result = Maps.newEnumMap(ProjectUpdateField.class);
        result.put(ProjectUpdateField.KEY, "TEST");
        result.put(ProjectUpdateField.ASSIGNEE_TYPE, "UNASSIGNED");
        result.put(ProjectUpdateField.AVATAR_ID, "10010");
        result.put(ProjectUpdateField.LEAD, "fred");
        result.put(ProjectUpdateField.URL, "http://new-url.test");
        result.put(ProjectUpdateField.DESCRIPTION, "Test description");

        Long categoryId = categoryClient.createCategoryAndReturnId("Test", "Test");
        result.put(ProjectUpdateField.CATEGORY_ID, categoryId.toString());

        Long issueSecurityScheme = backdoor.getTestkit().issueSecuritySchemes().createScheme("Test", "Test scheme description");
        result.put(ProjectUpdateField.ISSUE_SECURITY_SCHEME, issueSecurityScheme.toString());

        Long notificationScheme = backdoor.getTestkit().notificationSchemes().copyDefaultScheme("Test");
        result.put(ProjectUpdateField.NOTIFICATION_SCHEME, notificationScheme.toString());

        Long permissionsScheme = backdoor.permissionSchemes().copyDefaultScheme("Test");
        result.put(ProjectUpdateField.PERMISSION_SCHEME, permissionsScheme.toString());
        return result;
    }

    private Matcher<Project> isUpdatedCorrectlyInAccordanceWith(UpdateBean updateBean, Project originalProject) {
        return CorrectlyUpdatedProjectMatcher.create(updateBean, originalProject, backdoor.getTestkit());
    }

    @Test
    public void testDeletingProjectAsAnonymousFailsWith401() {
        assertNotAuthenticatedResponse(loggedAs(User.ANONYMOUS).delete("HSP"));
    }


    @Test
    public void testDeletingNonExistingProjectAsAnonymousFailsWith401() {
        assertNotAuthenticatedResponse(loggedAs(User.ANONYMOUS).delete("NOPE"));
    }

    @Test
    public void testDeletingProjectAsNonAdminFailsWith403() {
        assertInsufficientPrivilegesResponse(loggedAs(User.FRED).delete("HSP"));
    }

    @Test
    public void testDeletingNonExistingProjectAsNonAdminFailsWith404() {
        assertCannotFindProject(loggedAs(User.FRED).delete("NOPE"), "NOPE");
    }

    @Test
    public void testDeletingProjectThatUserCannotSeeFailsWith404() {
        assertCannotFindProject(loggedAs(User.FRED).delete("HID"), "HID");
    }

    @Test
    public void testDeletingProjectWithNonsenseKeyFailsWith404() {
        assertCannotFindProject(loggedAs(User.ADMIN).delete("THIS_IS_QUITE_INVALID_KEY"), "THIS_IS_QUITE_INVALID_KEY");
    }

    @Test
    public void testDeletingExistingProjectByKeyAsAdminSucceedsWithNoContentResponse() {
        assertSuccessfulDelete(projectATL(), projectATL().key);
    }

    @Test
    public void testDeletingExistingProjectByIdAsAdminSucceedsWithNoContentResponse() {
        assertSuccessfulDelete(projectATL(), projectATL().id);
    }

    private void assertSuccessfulDelete(Project project, String idOrKey) {
        assertCorrectProjectIsReturned(project, User.FRED);
        assertThat(loggedAs(User.ADMIN).delete(idOrKey).getClientResponseStatus(), equalTo(NO_CONTENT));
        assertCantSeeProject(idOrKey, User.FRED, "(key|id)");
    }


    private void assertNotAuthenticatedResponse(final ClientResponse response) {
        assertThat(response.getClientResponseStatus(), equalTo(UNAUTHORIZED));
        assertResponseContainsAnyOfErrorMessages(response, "You are not authenticated. Authentication required to perform this operation.");
    }


    private void assertInsufficientPrivilegesResponse(final ClientResponse response) {
        assertThat(response.getClientResponseStatus(), equalTo(FORBIDDEN));
        assertResponseContainsAnyOfErrorMessages(response, "You must have global administrator rights in order to modify projects.", "You cannot edit the configuration of this project.");
    }

    private void assertCannotFindProject(final ClientResponse response, String key) {
        assertThat(response.getClientResponseStatus(), equalTo(NOT_FOUND));
        assertResponseContainsAnyOfErrorMessages(response, String.format("No project could be found with key '%s'.", key));
    }

    private void assertCreationSuccess(final ProjectInputBean projectToCreate, final ClientResponse response) {
        assertThat(response.getClientResponseStatus(), equalTo(CREATED));

        List<String> locationHeaders = response.getHeaders().get("location");
        assertThat(locationHeaders, hasSize(1));

        ProjectIdentity projectId = response.getEntity(ProjectIdentity.class);

        Project project = projectClient.get(projectId.getKey());

        assertThat(locationHeaders.get(0), equalTo(projectId.getSelf().toString()));
        assertThat(nullToEmpty(project.description), equalTo(nullToEmpty(projectToCreate.getDescription())));
        assertThat(project.name, equalTo(projectToCreate.getName()));
        assertThat(project.assigneeType.name(), equalTo(projectToCreate.getAssigneeType().name()));
        assertThat(project.lead.name, equalTo(projectToCreate.getLead()));
        assertThat(nullToEmpty(project.url), equalTo(nullToEmpty(projectToCreate.getUrl())));
    }


    private ProjectInputBean.Builder sampleProject(String projectName) {
        return ProjectInputBean.builder().setName(projectName).setKey(projectName.replace(" ", "").toUpperCase().substring(0, 4)).setDescription("description of " + projectName)
                .setLeadName("admin")
                .setUrl("http://atlassian.com")
                .setProjectTypeKey("business")
                .setAssigneeType(ProjectBean.AssigneeType.PROJECT_LEAD);
    }

    private ProjectClient loggedAs(User user) {
        return user.loginName != null ? projectClient.loginAs(user.loginName) : projectClient.anonymous();
    }

    @Test
    public void testGetAvatars() throws Exception {
        checkGetAvatars("10000");
        checkGetAvatars("HSP");
    }

    private void checkGetAvatars(final String projectIdOrKey) {
        Map<String, List<Avatar>> avatars = projectClient.getAvatars(projectIdOrKey);

        List<Avatar> systemAvatars = avatars.get("system");
        List<Avatar> customAvatars = avatars.get("custom");

        assertThat(systemAvatars.size(), equalTo(26));
        assertThat(customAvatars.size(), equalTo(1));
    }

    @Test
    public void testViewProjectWithEditedKey() throws Exception {
        Project expectedProject = projectMKY().key("TST");
        backdoor.project().editProjectKey(Long.valueOf(expectedProject.id), "TST");
        assertThat(loggedAs(User.ADMIN).get("TST"), equalTo(expectedProject));
        assertThat(loggedAs(User.ADMIN).get("MKY"), equalTo(expectedProject));
    }

    @Test
    public void testViewProjectsExpandDescription() throws Exception {
        assertEquals(newArrayList(transform(asList(projectATL(), projectDODO(), projectFRED(), projectHID(), projectHSP(), projectMKY()), toSimpleWithDescription)),
                projectClient.getProjects("description"));
    }

    @Test
    public void testViewProjectsExpandLead() throws Exception {
        assertEquals(newArrayList(transform(asList(projectATL(), projectDODO(), projectFRED(), projectHID(), projectHSP(), projectMKY()), toSimpleWithLead)),
                projectClient.getProjects("lead"));
    }

    @Test
    public void testViewProjectsExpandDescriptionAndLead() throws Exception {
        assertEquals(newArrayList(transform(asList(projectATL(), projectDODO(), projectFRED(), projectHID(), projectHSP(), projectMKY()), toSimpleWithDescriptionAndLead)),
                projectClient.getProjects("description,lead"));
    }

    private final static Function<Project, Project> toSimple = new Function<Project, Project>() {
        @Override
        public Project apply(final Project project) {
            return project.email(null).components(null).assigneeType(null).description(null).lead(null).versions(null).issueTypes(null).roles(null);
        }
    };

    private final static Function<Project, Project> toSimpleWithDescription = new Function<Project, Project>() {
        @Override
        public Project apply(final Project project) {
            return project.email(null).components(null).assigneeType(null).lead(null).versions(null).issueTypes(null).roles(null);
        }
    };

    private final static Function<Project, Project> toSimpleWithLead = new Function<Project, Project>() {
        @Override
        public Project apply(final Project project) {
            return project.email(null).components(null).assigneeType(null).description(null).versions(null).issueTypes(null).roles(null);
        }
    };

    private final static Function<Project, Project> toSimpleWithDescriptionAndLead = new Function<Project, Project>() {
        @Override
        public Project apply(final Project project) {
            return project.email(null).components(null).assigneeType(null).versions(null).issueTypes(null).roles(null);
        }
    };

    private void assertCantSeeProject(User user, String key) {
        assertCantSeeProject(key, user, "key");
    }

    private void assertCantSeeProject(User user, Long id) {
        assertCantSeeProject(Long.toString(id), user, "id");
    }

    private void assertCantSeeProject(final String projectIdOrKey, final User user, final String field) {
        Response response = loggedAs(user).getResponse(projectIdOrKey);
        assertEquals(404, response.statusCode);
        assertThat(response.entity.errorMessages, hasItem(regexMatches(String.format("No project could be found with %s '%s'.", field, projectIdOrKey))));
    }

    private void assertCorrectProjectIsReturned(Project expectedProject, User... visibleTo) {
        checkProjectByKeyOrId(expectedProject.id, expectedProject, asList(visibleTo));
        checkProjectByKeyOrId(expectedProject.key, expectedProject, asList(visibleTo));
    }

    private void checkProjectByKeyOrId(final String projectKeyOrId, final Project expectedProject, List<User> visibleTo) {
        for (User user : visibleTo) {
            Project actualProject = loggedAs(user).get(projectKeyOrId);
            assertThat(actualProject, equalTo(expectedProject));
        }
    }

    private List<IssueType> createStandardIssueTypes() {
        return ImmutableList.of(
                new IssueType().self(urlHelper.getRestApiUrl("issuetype/1")).id("1").name("Bug").avatarId(issueTypeUrls.getAvatarId("bug")).iconUrl(urlHelper.getBaseUrlPlus(issueTypeUrls.getIssueTypeUrl("bug"))).description("A problem which impairs or prevents the functions of the product."),
                new IssueType().self(urlHelper.getRestApiUrl("issuetype/2")).id("2").name("New Feature").avatarId(issueTypeUrls.getAvatarId("new feature")).iconUrl(urlHelper.getBaseUrlPlus(issueTypeUrls.getIssueTypeUrl("new feature"))).description("A new feature of the product, which has yet to be developed."),
                new IssueType().self(urlHelper.getRestApiUrl("issuetype/3")).id("3").name("Task").avatarId(issueTypeUrls.getAvatarId("task")).iconUrl(urlHelper.getBaseUrlPlus(issueTypeUrls.getIssueTypeUrl("task"))).description("A task that needs to be done."),
                new IssueType().self(urlHelper.getRestApiUrl("issuetype/4")).id("4").name("Improvement").avatarId(issueTypeUrls.getAvatarId("improvement")).iconUrl(urlHelper.getBaseUrlPlus(issueTypeUrls.getIssueTypeUrl("improvement"))).description("An improvement or enhancement to an existing feature or task.")
        );
    }

    private Map<String, String> createStandardRoles(String projectKey) {
        return MapBuilder.<String, String>newBuilder()
                .add("Users", urlHelper.getRestApiUri("project", projectKey, "role", "10000").toString())
                .add("Developers", urlHelper.getRestApiUri("project", projectKey, "role", "10001").toString())
                .add("Administrators", urlHelper.getRestApiUri("project", projectKey, "role", "10002").toString())
                .toMap();
    }

    private Project projectMKY() {
        return new Project().self(urlHelper.getRestApiUri("project/10001")).key("MKY").name("monkey")
                .expand(EXPECTED_EXPAND)
                .id("10001")
                .email("mky@example.com")
                .lead(userAdmin()).description("project for monkeys")
                .assigneeType(Project.AssigneeType.PROJECT_LEAD)
                .issueTypes(createStandardIssueTypes())
                .roles(createStandardRoles("10001"))
                .components(Collections.<Component>emptyList()).versions(Collections.<Version>emptyList())
                .avatarUrls(createProjectAvatarUrls(10001L, 10011L))
                .projectTypeKey("software");
    }

    private Project projectHID() {
        return new Project().self(urlHelper.getRestApiUri("project/10110")).key("HID").name("HIDDEN")
                .expand(EXPECTED_EXPAND)
                .id("10110")
                .description("")
                .components(Collections.<Component>emptyList()).versions(Collections.<Version>emptyList())
                .assigneeType(Project.AssigneeType.PROJECT_LEAD)
                .roles(createStandardRoles("10110"))
                .issueTypes(createStandardIssueTypes())
                .lead(userAdmin())
                .avatarUrls(createProjectAvatarUrls(10110L, 10011L))
                .projectTypeKey("software");
    }

    private Project projectHSP() {
        return new Project().self(urlHelper.getRestApiUri("project/10000")).key("HSP").name("homosapien")
                .expand(EXPECTED_EXPAND)
                .id("10000")
                .description("project for homosapiens")
                .versions(createVersionsHsp()).components(createComponentsHsp())
                .issueTypes(createStandardIssueTypes())
                .assigneeType(Project.AssigneeType.PROJECT_LEAD)
                .roles(createStandardRoles("10000"))
                .lead(userAdmin())
                .avatarUrls(createProjectAvatarUrls(10000L, 10140L))
                .projectTypeKey("software");
    }

    private Project projectFRED() {
        return new Project().self(urlHelper.getRestApiUri("project/10111")).key("FRED").name("Fred")
                .expand(EXPECTED_EXPAND)
                .id("10111")
                .description("")
                .components(Collections.<Component>emptyList())
                .versions(Collections.<Version>emptyList())
                .issueTypes(createStandardIssueTypes())
                .assigneeType(Project.AssigneeType.PROJECT_LEAD)
                .roles(createStandardRoles("10111"))
                .lead(userFred())
                .avatarUrls(createProjectAvatarUrls(10111L, 10011L))
                .projectTypeKey("software");
    }

    private Project projectDODO() {
        return new Project().self(urlHelper.getRestApiUri("project/10112")).key("DD").name("Dead Leader")
                .expand(EXPECTED_EXPAND)
                .id("10112")
                .description("")
                .components(Collections.<Component>emptyList())
                .versions(Collections.<Version>emptyList())
                .assigneeType(Project.AssigneeType.PROJECT_LEAD)
                .issueTypes(createStandardIssueTypes())
                .roles(createStandardRoles("10112"))
                .lead(userDodo())
                .avatarUrls(createProjectAvatarUrls(10112L, 10011L))
                .projectTypeKey("software");
    }

    private Project projectATL() {
        return new Project().self(urlHelper.getRestApiUri("project/10010")).key("ATL").name("Atlassian")
                .expand(EXPECTED_EXPAND)
                .id("10010")
                .description("")
                .lead(userAdmin()).components(createComponentsAtlShort())
                .assigneeType(Project.AssigneeType.PROJECT_LEAD)
                .issueTypes(createStandardIssueTypes())
                .roles(createStandardRoles("10010"))
                .versions(createVersionsAtl())
                .avatarUrls(createProjectAvatarUrls(10010L, 10011L))
                .projectTypeKey("software");
    }

    private Map<String, String> createProjectAvatarUrls(final Long projectId, final Long avatarId) {
        final ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
        if (defaultAvatarId.equals(avatarId)) {
            builder.put("24x24", urlHelper.getBaseUrlPlus("secure/projectavatar?size=small&avatarId=" + avatarId))
                    .put("16x16", urlHelper.getBaseUrlPlus("secure/projectavatar?size=xsmall&avatarId=" + avatarId))
                    .put("32x32", urlHelper.getBaseUrlPlus("secure/projectavatar?size=medium&avatarId=" + avatarId))
                    .put("48x48", urlHelper.getBaseUrlPlus("secure/projectavatar?avatarId=" + avatarId));
        } else {
            builder.put("24x24", urlHelper.getBaseUrlPlus("secure/projectavatar?size=small&pid=" + projectId + "&avatarId=" + avatarId))
                    .put("16x16", urlHelper.getBaseUrlPlus("secure/projectavatar?size=xsmall&pid=" + projectId + "&avatarId=" + avatarId))
                    .put("32x32", urlHelper.getBaseUrlPlus("secure/projectavatar?size=medium&pid=" + projectId + "&avatarId=" + avatarId))
                    .put("48x48", urlHelper.getBaseUrlPlus("secure/projectavatar?pid=" + projectId + "&avatarId=" + avatarId));
        }
//  TODO JRADEV-20790 - Re-enable the larger avatar sizes.
//            .put("64x64", getBaseUrlPlus("secure/projectavatar?size=xlarge&pid="+projectId+"&avatarId="+avatarId))
//            .put("96x96", getBaseUrlPlus("secure/projectavatar?size=xxlarge&pid="+projectId+"&avatarId="+avatarId))
//            .put("128x128", getBaseUrlPlus("secure/projectavatar?size=xxxlarge&pid=" + projectId + "&avatarId=" + avatarId))
//            .put("192x192", getBaseUrlPlus("secure/projectavatar?size=xxlarge%402x&pid=" + projectId + "&avatarId=" + avatarId)) // %40 == "@"
//            .put("256x256", getBaseUrlPlus("secure/projectavatar?size=xxxlarge%402x&pid=" + projectId + "&avatarId=" + avatarId))
        return builder.build();
    }

    private Map<String, String> createUserAvatarUrls(Long avatarId) {
        return ImmutableMap.<String, String>builder()
                .put("24x24", urlHelper.getBaseUrlPlus("secure/useravatar?size=small&avatarId=" + avatarId))
                .put("16x16", urlHelper.getBaseUrlPlus("secure/useravatar?size=xsmall&avatarId=" + avatarId))
                .put("32x32", urlHelper.getBaseUrlPlus("secure/useravatar?size=medium&avatarId=" + avatarId))
                .put("48x48", urlHelper.getBaseUrlPlus("secure/useravatar?avatarId=" + avatarId))
//  TODO JRADEV-20790 - Re-enable the larger avatar sizes.
//            .put("64x64", getBaseUrlPlus("secure/useravatar?size=xlarge&avatarId="+avatarId))
//            .put("96x96", getBaseUrlPlus("secure/useravatar?size=xxlarge&avatarId="+avatarId))
//            .put("128x128", getBaseUrlPlus("secure/useravatar?size=xxxlarge&avatarId="+avatarId))
//            .put("192x192", getBaseUrlPlus("secure/useravatar?size=xxlarge%402x&avatarId="+avatarId)) // %40 == "@"
//            .put("256x256", getBaseUrlPlus("secure/useravatar?size=xxxlarge%402x&avatarId="+avatarId))
                .build();
    }

    private List<Version> createVersionsAtl() {
        CollectionBuilder<Version> builder = CollectionBuilder.newBuilder();

        builder.add(new Version().self(createVersionUri(10014)).archived(true)
                .released(false).name("Five").description("Five").id(10014L).projectId(10010l));

        builder.add(new Version().self(createVersionUri(10013)).archived(true)
                .released(true).name("Four").description("Four")
                .releaseDate("09/Mar/11").id(10013L).projectId(10010l));

        builder.add(new Version().self(createVersionUri(10012)).archived(false)
                .released(true).name("Three")
                .releaseDate("09/Mar/11").id(10012L).projectId(10010l));

        builder.add(new Version().self(createVersionUri(10011)).archived(false)
                .released(false).name("Two").description("Description").id(10011L).projectId(10010l));

        builder.add(new Version().self(createVersionUri(10010)).archived(false)
                .released(false).name("One").releaseDate("01/Mar/11").overdue(true).id(10010L).projectId(10010l));

        return builder.asList();
    }

    private List<Version> createVersionsHsp() {
        CollectionBuilder<Version> builder = CollectionBuilder.newBuilder();

        builder.add(new Version().self(createVersionUri(10000)).archived(false)
                .released(false).name("New Version 1").description("Test Version Description 1").id(10000L));

        builder.add(new Version().self(createVersionUri(10001)).archived(false)
                .released(false).name("New Version 4").description("Test Version Description 4").id(10001L));

        builder.add(new Version().self(createVersionUri(10002)).archived(false)
                .released(false).name("New Version 5").description("Test Version Description 5").id(10002L));

        return builder.asList();
    }

    private List<Component> createComponentsHsp() {
        final ImmutableList.Builder<Component> builder = ImmutableList.builder();

        builder.add(new Component().self(createComponentUri(10000)).id(10000L).name("New Component 1").project("HSP").projectId(10000).assigneeType(Component.AssigneeType.PROJECT_DEFAULT)
                .assignee(userAdmin()).realAssigneeType(Component.AssigneeType.PROJECT_DEFAULT).realAssignee(userAdmin()).isAssigneeTypeValid(true));
        builder.add(new Component().self(createComponentUri(10001)).id(10001L).name("New Component 2").project("HSP").projectId(10000).assigneeType(Component.AssigneeType.PROJECT_DEFAULT)
                .assignee(userAdmin()).realAssigneeType(Component.AssigneeType.PROJECT_DEFAULT).realAssignee(userAdmin()).isAssigneeTypeValid(true));
        builder.add(new Component().self(createComponentUri(10002)).id(10002L).name("New Component 3").project("HSP").projectId(10000).assigneeType(Component.AssigneeType.PROJECT_DEFAULT)
                .assignee(userAdmin()).realAssigneeType(Component.AssigneeType.PROJECT_DEFAULT).realAssignee(userAdmin()).isAssigneeTypeValid(true));

        return builder.build();
    }

    private List<Component> createComponentsAtlFull() {
        CollectionBuilder<Component> builder = CollectionBuilder.newBuilder();

        builder.add(new Component().self(createComponentUri(10003)).id(10003L).name("New Component 4").project("ATL").projectId(10010).assigneeType(Component.AssigneeType.PROJECT_DEFAULT)
                .assignee(userAdmin()).realAssigneeType(Component.AssigneeType.PROJECT_DEFAULT).realAssignee(userAdmin()).isAssigneeTypeValid(true));
        builder.add(new Component().self(createComponentUri(10004)).id(10004L).name("New Component 5").project("ATL").projectId(10010).assigneeType(Component.AssigneeType.PROJECT_DEFAULT)
                .assignee(userAdmin()).realAssigneeType(Component.AssigneeType.PROJECT_DEFAULT).realAssignee(userAdmin()).isAssigneeTypeValid(true));

        return builder.asList();
    }

    private List<Component> createComponentsAtlShort() {
        CollectionBuilder<Component> builder = CollectionBuilder.newBuilder();

        builder.add(new Component().self(createComponentUri(10003)).id(10003L).name("New Component 4"));
        builder.add(new Component().self(createComponentUri(10004)).id(10004L).name("New Component 5"));

        return builder.asList();
    }

    private com.atlassian.jira.testkit.client.restclient.User userAdmin() {

        return new com.atlassian.jira.testkit.client.restclient.User()
                .self(createUserUri("admin"))
                .name("admin")
                .key("admin")
                .displayName("Administrator")
                .active(true)
                .avatarUrls(createUserAvatarUrls(10062L));
    }

    private com.atlassian.jira.testkit.client.restclient.User userFred() {
        return new com.atlassian.jira.testkit.client.restclient.User()
                .self(createUserUri("fred"))
                .name("fred")
                .key("fred")
                .displayName("Fred Normal")
                .active(true)
                .avatarUrls(createUserAvatarUrls(10062L));
    }

    private com.atlassian.jira.testkit.client.restclient.User userDodo() {
        return new com.atlassian.jira.testkit.client.restclient.User()
                .self(createUserUri("dodo"))
                .name("dodo")
                .key("dodo")
                .displayName("dodo")
                .active(false)
                .avatarUrls(createUserAvatarUrls(10063L));
    }

    private URI createVersionUri(long id) {
        return urlHelper.getRestApiUri("version", String.valueOf(id));
    }

    private URI createComponentUri(long id) {
        return urlHelper.getRestApiUri("component", String.valueOf(id));
    }

    private URI createUserUri(String name) {
        return urlHelper.getRestApiUri(String.format("user?username=%s", name));
    }

    @Before
    public void setUpTest() {
        projectClient = new ProjectClient(environmentData);
        categoryClient = new ProjectCategoryClient(environmentData);
        defaultAvatarId = Long.parseLong(backdoor.applicationProperties().getString("jira.avatar.default.id"));
    }

    @Test
    public void testChangeLeadOfProjectWithTooLongKey() {
        final long id = getBackdoor().project().getProjectId("MKY");
        getBackdoor().project().editProjectKeyNoWaitForReindex(id, LARGE_PROJECT_KEY);
        Project project = projectClient.get(LARGE_PROJECT_KEY);
        assertEquals("admin", project.lead.name);
        projectClient.update(LARGE_PROJECT_KEY, UpdateBean.builder().with(ProjectUpdateField.LEAD, "fred").build());
        Project withNewLead = projectClient.get(LARGE_PROJECT_KEY);
        assertEquals("fred", withNewLead.lead.name);
    }

    @After
    public void tearDownTest() {
        projectClient.cleanUp();
        categoryClient.cleanUp();
    }

    protected final void assertResponseContainsAnyOfErrorMessages(final ClientResponse response, final String... errorMessageRegexp) {
        Errors errors = response.getEntity(Errors.class);

        Iterable<Matcher<? super String>> regexpMatchers = Iterables.transform(Arrays.asList(errorMessageRegexp), new Function<String, Matcher<? super String>>() {
            @Override
            public Matcher<String> apply(@Nullable final String input) {
                return regexMatches(input);
            }
        });
        assertThat(ImmutableList.builder().addAll(errors.errorMessages).addAll(errors.errors.values()).build(), hasItem(Matchers.anyOf(regexpMatchers)));
    }

    protected final JiraHttpClient createHttpClient() {
        return new JiraHttpClient(environmentData.getBaseUrl().toExternalForm());
    }
}
