package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.locator.Locator;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.webtests.Groups;
import com.meterware.httpunit.TableCell;
import com.meterware.httpunit.WebTable;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_SUB_TASK;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Mode.LEGACY;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.ISSUES;
import static com.atlassian.jira.functest.framework.suite.Category.TIME_ZONES;
import static com.atlassian.jira.permission.ProjectPermissions.DELETE_ALL_WORKLOGS;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@WebTest({FUNC_TEST, ISSUES})
@LoginAs(user = ADMIN_USERNAME)
public class TestViewIssue extends BaseJiraFuncTest {
    private static final String CLASSNAME_SUBTASK_PERCENTAGE_CELL = "progress";

    /**
     * Ensure that non-existant assignee and reporter users dont break the view issue page - JRA-12360
     */
    @Inject
    private Form form;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    @Restore("TestViewIssueWithInvalidUsersProEnt.xml")
    public void testViewIssuePageWithInvalidAssigneeAndReporters() {
        //import pro/ent data (with subtasks)
        backdoor.attachments().enable(); //set the attachment paths to be valid
        _testViewIssuePageWithInvalidAssigneeAndReportersStandard();
        _testViewIssuePageWithInvalidAssigneeAndReportersProEnt();
    }

    /**
     * check that if the date formats end with a double quot and a space that the UI does not break and the quotes are
     * html encoded. - JRA-13104
     */
    @Test
    @Restore("TestEditedCommentAndWorklogWithMalformedDateFormat.xml")
    public void testEditedCommentVisibleWithDoubleQuotesInDateFormat() {
        navigation.issue().gotoIssueComments("HSP-1");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "this comment is edited", "this comment will not be edited");
        tester.assertTextPresent("title=\"" + ADMIN_FULLNAME + " - 24/Jul/07 09:47 AM&quot; \"");
        // make sure stuff is encoded
        assertThat(locator.css(".commentdate_10012_concise span time").getText(), containsString("25/Jul/07 09:48 AM\""));
        assertThat(locator.css(".commentdate_10012_verbose span time").getText(), containsString("25/Jul/07 09:48 AM\""));

        navigation.issue().gotoIssueWorkLog("HSP-1");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "this work log is edited", "this work log will not be edited");
        tester.assertTextPresent("title=\"" + ADMIN_FULLNAME + " - 18/Jul/07 09:42 AM&quot; \"");
        assertThat(locator.css("#worklog-10001 .actionContainer .action-details .date").getText(), containsString("18/Jul/07 09:43 AM\""));
    }

    private void _testViewIssuePageWithInvalidAssigneeAndReportersStandard() {
        navigation.issue().gotoIssue("HSP-1");
        tester.assertTextPresent("Issue with valid user"); //summary
        //check that the assignee and reporter are shown
        Locator locator = new IdLocator(tester, "assignee-val");
        textAssertions.assertTextPresent(locator, ADMIN_FULLNAME);
        locator = new IdLocator(tester, "reporter-val");
        textAssertions.assertTextPresent(locator, ADMIN_FULLNAME);

        //check that the custom user pickers are displayed correctly

        textAssertions.assertTextPresent(new XPathLocator(tester, "//span[@id='customfield_10000-val']"), ADMIN_FULLNAME);

        textAssertions.assertTextPresent(new XPathLocator(tester, "//span[@id='customfield_10001-val']"), ADMIN_FULLNAME);

        //check that the workflow actions are available
        tester.assertLinkPresentWithText("Stop Progress");
        tester.assertLinkPresentWithText("Resolve Issue");
        tester.assertLinkPresentWithText("Close Issue");

        //check that the operations are available
        tester.assertLinkPresentWithText("Assign");
        tester.assertLinkPresentWithText("Attach file");
        tester.assertLinkPresentWithText("Clone");
        tester.assertLinkPresentWithText("Comment");
        tester.assertLinkPresentWithText("Delete");
        tester.assertLinkPresentWithText("Edit");
        tester.assertLinkPresent("view-voters");
        tester.assertLinkPresent("manage-watchers");
        tester.assertLinkPresentWithText("Log work");

        navigation.issue().gotoIssue("HSP-2");
        tester.assertTextPresent("Issue with invalid users"); //summary
        //check that the assignee and reporter are shown
        textAssertions.assertTextPresent(new IdLocator(tester, "assignee-val"), "deletedassignee");
        textAssertions.assertTextPresent(new IdLocator(tester, "reporter-val"), "deletedreporter");

        //check that the custom user pickers are displayed correctly
        assertions.assertNodeByIdExists("customfield_10000-val");
        assertions.assertNodeByIdExists("customfield_10001-val");
        assertions.assertNodeDoesNotExist("//dd[@id='customfield_10001-val']//a");

        //check that the workflow actions are available
        tester.assertLinkPresentWithText("Stop Progress");
        tester.assertLinkPresentWithText("Resolve Issue");
        tester.assertLinkPresentWithText("Close Issue");

        //check that the operations are available
        tester.assertLinkPresentWithText("Assign");
        tester.assertLinkPresentWithText("Attach file");
        tester.assertLinkPresentWithText("Clone");
        tester.assertLinkPresentWithText("Comment");
        tester.assertLinkPresentWithText("Delete");
        tester.assertLinkPresentWithText("Edit");
        tester.assertLinkPresent("view-voters");
        tester.assertLinkPresent("manage-watchers");
        tester.assertLinkPresentWithText("Log work");
    }

    /**
     * Additional asserts to {@link #_testViewIssuePageWithInvalidAssigneeAndReportersStandard()} for professional and
     * enterprise editions
     */
    private void _testViewIssuePageWithInvalidAssigneeAndReportersProEnt() {
        navigation.issue().gotoIssue("HSP-2");

        //check additional operations are available
        tester.assertLinkPresentWithText("sub-task");

        //check link to the subtask is available
        tester.assertLinkPresentWithText("Sub task with invalid user");


        navigation.issue().gotoIssue("HSP-3");
        tester.assertTextPresent("Sub task with invalid user"); //summary
        //check that the assignee and reporter are shown
        textAssertions.assertTextPresent(new IdLocator(tester, "assignee-val"), "deletedsubtaskuser");
        textAssertions.assertTextPresent(new IdLocator(tester, "reporter-val"), "deletedsubtaskuser");

        //check that the custom user pickers are displayed correctly
        assertions.assertNodeByIdExists("customfield_10000-val");
        assertions.assertNodeByIdExists("customfield_10001-val");
        assertions.assertNodeDoesNotExist("//dd[@id='customfield_10001-val']//a");


        //check that the workflow actions are available
        tester.assertLinkNotPresentWithText("Stop Progress");
        tester.assertLinkPresentWithText("Resolve Issue");
        tester.assertLinkPresentWithText("Close Issue");

        //check that the operations are available
        tester.assertLinkPresentWithText("Assign");
        tester.assertLinkPresentWithText("Attach file");
        tester.assertLinkPresentWithText("Clone");
        tester.assertLinkPresentWithText("Comment");
        tester.assertLinkPresentWithText("Delete");
        tester.assertLinkPresentWithText("Edit");
        tester.assertLinkPresent("view-voters");
        tester.assertLinkPresent("manage-watchers");
        tester.assertLinkPresentWithText("Log work");
    }

    /**
     * Tests the correct conditions under which the subtask progress percentage graph is present. The graph should be
     * present if *any* subtask has any timetracking data - either original estimate or work logged.
     */
    @Test
    @RestoreBlankInstance
    public void testSubtaskPercentageGraphPresence() {
        administration.subtasks().enable();
        administration.timeTracking().enable(LEGACY);
        final String parentKey = navigation.issue().createIssue(PROJECT_MONKEY, ISSUE_TYPE_BUG, "bonobo");
        final String sub1Key = navigation.issue().createSubTask(parentKey, ISSUE_TYPE_SUB_TASK, "sub1", "subdesc1");
        navigation.issue().gotoIssue(parentKey);
        assertFalse(isSubtaskPercentageGraphPresent());
        // add a second subtask to check we see percentage bars even though not all subtasks get issue tracking
        navigation.issue().createSubTask(parentKey, ISSUE_TYPE_SUB_TASK, "sub2", "subdesc2");
        navigation.issue().gotoIssue(parentKey);
        assertFalse(isSubtaskPercentageGraphPresent());
        // now change the original estimate of a subtask - it should make the subtask progress graph visible
        navigation.issue().gotoIssue(sub1Key);
        setEstimate("1m");
        navigation.issue().gotoIssue(parentKey);
        assertTrue(isSubtaskPercentageGraphPresent());
        // log some work so complete subtask timetracking is present (orig, remaining and time spent)
        navigation.issue().logWork(sub1Key, "1m", "1m");
        tester.clickLink("parent_issue_summary");
        assertTrue(isSubtaskPercentageGraphPresent());
        tester.assertTextPresent("50%");

        // remove the logged work on the subtask and confirm the graph remains
        enableDeleteAllWorklogInDefaultPermissionScheme(Groups.ADMINISTRATORS);
        navigation.issue().gotoIssue(sub1Key);
        // now delete the worklog
        if (tester.getDialog().isLinkPresentWithText("Work Log")) {
            // get to the worklog issue tab panel
            tester.clickLinkWithText("Work Log");
        }
        tester.clickLink("delete_worklog_10000");
        tester.submit("Delete");
        navigation.issue().gotoIssue(parentKey);
        assertTrue("Expected to still see graph after worklog deletion, original estimate remains", isSubtaskPercentageGraphPresent());
    }

    /**
     * JRA-14794: if user does not have permission to browse a project, the project CF value should not be linked
     */
    @Test
    @Restore("TestProjectCFWithNoPermission.xml")
    public void testProjectCFNotLinkedWithNoPermission() {
        // data contains:
        // * 2 projects HSP, MKY. HSP has default permissions, MKY is only browsable by admin user
        // * project CF configured for all projects
        // * 2 issues: one which has project CF set to MKY, one with it set to HSP
        // since 'fred' cannot see MKY, the project CF value should not be linked
        navigation.login(FRED_USERNAME);

        // project CF is visible in HSP-2
        navigation.issue().gotoIssue("HSP-2");
        tester.assertLinkPresentWithText("homosapien");

        // project CF is not visible in HSP-1
        navigation.issue().gotoIssue("HSP-1");
        tester.assertLinkNotPresentWithText("monkey");
        tester.assertTextPresent("monkey");

        // project CF is visible in HSP-1 to admin
        navigation.login(ADMIN_USERNAME);
        navigation.issue().gotoIssue("HSP-1");
        tester.assertLinkPresentWithText("monkey");
    }

    /**
     * JRA-15011 - ensure ordering of Components is fixed
     */
    @Test
    @RestoreBlankInstance
    public void testComponentOrdering() {
        logger.log("Testing ordering for components field");
        navigation.issue().goToCreateIssueForm(null, null);
        tester.setFormElement("summary", "Test issue");

        // issue initially has Componet 1 & 3 selected
        form.selectOptionsByDisplayName("components", "New Component 1", "New Component 3");
        tester.submit("Create");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Component/s", "New Component 1", "New Component 3"});

        // change components to 1 & 2 & 3
        tester.clickLink("edit-issue");
        tester.setWorkingForm("issue-edit");
        form.selectOptionsByDisplayName("components", "New Component 2", "New Component 1", "New Component 3");
        tester.submit("Update");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Component/s", "New Component 1", "New Component 2", "New Component 3"});
    }

    /**
     * JRA-15011 - ensure ordering of Versions is fixed
     */
    @Test
    @RestoreBlankInstance
    public void testVersionOrdering() {
        final String[] versionFields = new String[]{"versions", "fixVersions"};
        final String[] versionFieldNames = new String[]{"Affects Version/s", "Fix Version/s"};
        for (int i = 0; i < versionFields.length; i++) {
            final String versionField = versionFields[i];
            final String versionFieldName = versionFieldNames[i];
            logger.log("Testing ordering for versions field '" + versionField + "'");
            tester.clickLink("create_link");
            tester.submit("Next");
            tester.setFormElement("summary", "Test issue");

            // issue initially has Version 1 & 3 selected
            form.selectOptionsByDisplayName(versionField, "New Version 1", "New Version 5");
            tester.submit("Create");
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{versionFieldName, "New Version 1", "New Version 5"});

            // change versions to 1 & 2 & 3
            tester.clickLink("edit-issue");
            tester.setWorkingForm("issue-edit");
            form.selectOptionsByDisplayName(versionField, "New Version 4", "New Version 1", "New Version 5");
            tester.submit("Update");
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{versionFieldName, "New Version 1", "New Version 4", "New Version 5"});
        }
    }

    /*
     * Test the state of custom field tabs.
     */
    @Test
    @Restore("TestIssueFields.xml")
    public void testFieldTabs() throws Exception {

        //This issue should have no custom fields.
        navigation.issue().gotoIssue("HSP-1");
        assertions.assertNodeByIdDoesNotExist("customfieldmodule");

        //This issue should have two tabs.
        navigation.issue().gotoIssue("HSP-3");
        assertions.assertNodeByIdExists("customfieldmodule");
        textAssertions.assertTextPresent(createCFTabLocator("customfield-panel-1"), "Tab1");
        textAssertions.assertTextPresent(createCFValueLocator("customfield-panel-1", 10000), "Tab1CF");
        textAssertions.assertTextPresent(createCFTabLocator("customfield-panel-2"), "Tab2");
        textAssertions.assertTextPresent(createCFValueLocator("customfield-panel-2", 10001), "Tab2CF");

        //This issue should have only one tab.
        navigation.issue().gotoIssue("HSP-2");
        assertions.assertNodeByIdExists("customfieldmodule");
        //This issue should have only one tab
        tester.assertLinkNotPresentWithText("Tab2");
        textAssertions.assertTextPresent(createCFValueLocator("customfield-panel-1", 10001), "Tab2CF");

    }

    /*
     * Test the state of custom field tabs when working with date custom fields. The custom field dates are not rendered
     * with the other fields.
     */
    @Test
    @Restore("TestIssueFields.xml")
    public void testDateCustomFieldTabs() throws Exception {
        //Make sure that we don't display any date custom fields when there are no values.
        navigation.issue().gotoIssue("HSP-3");
        assertions.assertNodeDoesNotExist("//*[@id='datesmodule']//*[contains(@id, 'customfield')]");

        //This issue has a date custom field value but no others.
        navigation.issue().gotoIssue("HSP-4");
        assertions.assertNodeByIdDoesNotExist("customfieldmodule");
        textAssertions.assertTextSequence(new IdLocator(tester, "datesmodule"), "DateCFTab2", "26/Jan/10");

        //This issue has two date fields on different tabs. They should both be displayed.
        navigation.issue().gotoIssue("HSP-5");
        assertions.assertNodeByIdDoesNotExist("customfieldmodule");
        textAssertions.assertTextSequence(new IdLocator(tester, "datesmodule"), "DateTimeCFTab1", "03/Jan/10", "DateTimeCFTab2", "29/Jan/10");

        //This issuse have multiple custom field tabs and date fields.
        navigation.issue().gotoIssue("HSP-6");
        assertions.assertNodeByIdExists("customfieldmodule");
        textAssertions.assertTextPresent(createCFTabLocator("customfield-panel-1"), "Tab1");
        textAssertions.assertTextPresent(createCFValueLocator("customfield-panel-1", 10000), "Tab1CF");
        textAssertions.assertTextPresent(createCFTabLocator("customfield-panel-2"), "Tab2");
        textAssertions.assertTextPresent(createCFValueLocator("customfield-panel-2", 10001), "Tab2CF");
        textAssertions.assertTextSequence(new IdLocator(tester, "datesmodule"), "DateTimeCFTab1", "04/Jan/10", "DateTimeCFTab2", "12/Jan/10", "DateCFTab2", "19/Jan/10");
    }

    /*
     * Test the state of custom field tabs when working with user custom fields. The custom field dates are not rendered
     * with the other fields.
     */
    @Test
    @Restore("TestIssueFields.xml")
    public void testUserCustomFieldTabs() throws Exception {
        //Make sure that we don't display any user custom fields when there are no values.
        navigation.issue().gotoIssue("HSP-3");
        assertions.assertNodeDoesNotExist("//*[@id='peopledetails']//*[contains(@id, 'customfield')]");


        //This issue has a user custom field value but no others.
        navigation.issue().gotoIssue("HSP-7");
        assertions.assertNodeByIdDoesNotExist("customfieldmodule");
        textAssertions.assertTextSequence(new IdLocator(tester, "peopledetails"), "UserPickerCF", ADMIN_FULLNAME);

        //This issuse have multiple custom field tabs and user fields fields.
        navigation.issue().gotoIssue("HSP-8");
        assertions.assertNodeByIdExists("customfieldmodule");
        textAssertions.assertTextPresent(createCFTabLocator("customfield-panel-1"), "Tab1");
        textAssertions.assertTextPresent(createCFValueLocator("customfield-panel-1", 10000), "Tab1CF");
        textAssertions.assertTextPresent(createCFTabLocator("customfield-panel-2"), "Tab2");
        textAssertions.assertTextPresent(createCFValueLocator("customfield-panel-2", 10001), "Tab2CF");

        textAssertions.assertTextSequence(new IdLocator(tester, "peopledetails"), "UserPickerCF", ADMIN_FULLNAME,
                "MultiGroupPickerCF", "jira-developers", "jira-users", "GroupPickerCF", "jira-administrators",
                "MultiUserPickerCF", ADMIN_FULLNAME, FRED_FULLNAME);
    }

    /*
     * Test the state of custom field tabs when working with user custom fields. The custom field dates are not rendered
     * with the other fields.
     */
    @Test
    @Restore("TestIssueFields.xml")
    public void testUserCustomFieldWithMissingUsers() throws Exception {
        //This issuse have multiple custom field tabs and user fields fields.
        navigation.issue().gotoIssue("HSP-9");
        assertions.assertNodeByIdExists("customfieldmodule");
        textAssertions.assertTextPresent(createCFTabLocator("customfield-panel-1"), "Tab1");
        textAssertions.assertTextPresent(createCFValueLocator("customfield-panel-1", 10000), "Tab1CF");
        textAssertions.assertTextPresent(createCFTabLocator("customfield-panel-2"), "Tab2");
        textAssertions.assertTextPresent(createCFValueLocator("customfield-panel-2", 10001), "Tab2CF");

        textAssertions.assertTextSequence(new IdLocator(tester, "peopledetails"), "UserPickerCF", "admin-xx",
                "MultiGroupPickerCF", "jira-developers", "jira-users", "GroupPickerCF", "jira-administrators",
                "MultiUserPickerCF", ADMIN_FULLNAME, "fred-xx");

        tester.assertLinkNotPresentWithText("admin-xx");
        tester.assertLinkNotPresentWithText("fred-xx");
    }

    @Test
    @WebTest(TIME_ZONES)
    @LoginAs(user = ADMIN_USERNAME)
    @Restore("TestIssueFields.xml")
    public void testDatesShouldBeDisplayedInUserTimeZoneInViewIssuePage() throws Exception {
        final String MKY_1 = "MKY-1";

        final String HONG_KONG = "Asia/Hong_Kong";
        final String HONOLULU = "Pacific/Honolulu";
        final String PAPUA_NEW_GUINEA = "Pacific/Port_Moresby";

        final String CREATED_HK = "06/Mar/11 9:36 AM";
        final String CREATED_HON = "05/Mar/11 3:36 PM";
        final String CREATED_PNG = "06/Mar/11 11:36 AM";

        final String UPDATED_HK = "06/Mar/11 9:43 AM";
        final String UPDATED_HON = "05/Mar/11 3:43 PM";
        final String UPDATED_PNG = "06/Mar/11 11:43 AM";

        final String RESOLVED_HK = "06/Mar/11 9:41 AM";
        final String RESOLVED_HON = "05/Mar/11 3:41 PM";
        final String RESOLVED_PNG = "06/Mar/11 11:41 AM";

        final String DATETIME_CF_HK = "09/Jun/11 6:30 AM";
        final String DATETIME_CF_HON = "08/Jun/11 12:30 PM";
        final String DATETIME_CF_PNG = "09/Jun/11 8:30 AM";

        final String WORKLOG_HK = "06/Mar/11 9:38 AM";
        final String WORKLOG_HON = "05/Mar/11 3:38 PM";
        final String WORKLOG_PNG = "06/Mar/11 11:38 AM";

        final String HISTORY_HK = "06/Mar/11 9:38 AM";
        final String HISTORY_HON = "05/Mar/11 3:38 PM";
        final String HISTORY_PNG = "06/Mar/11 11:38 AM";

        administration.generalConfiguration().setDefaultUserTimeZone(HONG_KONG);

        // admin is in Hong Kong (GMT+11)
        navigation.issue().gotoIssue(MKY_1);
        assertThat(createdDate(), equalTo(CREATED_HK));
        assertThat(updatedDate(), equalTo(UPDATED_HK));
        assertThat(resolvedDate(), equalTo(RESOLVED_HK));
        assertThat(dateTimeCfTab1(), equalTo(DATETIME_CF_HK));
        navigation.issue().gotoIssueWorkLog(MKY_1);
        assertThat(worklogDate(), equalTo(WORKLOG_HK));
        navigation.issue().gotoIssueChangeHistory(MKY_1);
        assertThat(changeHistoryDate(), equalTo(HISTORY_HK));

        // admin now moves to Port Moresby
        navigation.userProfile().changeUserTimeZone(PAPUA_NEW_GUINEA);
        navigation.issue().gotoIssue(MKY_1);
        assertThat(createdDate(), equalTo(CREATED_PNG));
        assertThat(updatedDate(), equalTo(UPDATED_PNG));
        assertThat(resolvedDate(), equalTo(RESOLVED_PNG));
        assertThat(dateTimeCfTab1(), equalTo(DATETIME_CF_PNG));
        navigation.issue().gotoIssueWorkLog(MKY_1);
        assertThat(worklogDate(), equalTo(WORKLOG_PNG));
        navigation.issue().gotoIssueChangeHistory(MKY_1);
        assertThat(changeHistoryDate(), equalTo(HISTORY_PNG));

        // fred is in Honolulu (GMT-10)
        navigation.login("fred");
        navigation.userProfile().changeUserTimeZone(HONOLULU);
        navigation.issue().gotoIssue(MKY_1);
        assertThat(createdDate(), equalTo(CREATED_HON));
        assertThat(updatedDate(), equalTo(UPDATED_HON));
        assertThat(resolvedDate(), equalTo(RESOLVED_HON));
        assertThat(dateTimeCfTab1(), equalTo(DATETIME_CF_HON));
        navigation.issue().gotoIssueWorkLog(MKY_1);
        assertThat(worklogDate(), equalTo(WORKLOG_HON));
        navigation.issue().gotoIssueChangeHistory(MKY_1);
        assertThat(changeHistoryDate(), equalTo(HISTORY_HON));

        // anonymous should see HK time, which is the default
        navigation.logout();
        navigation.issue().gotoIssue(MKY_1);
        assertThat(createdDate(), equalTo(CREATED_HK));
        assertThat(updatedDate(), equalTo(UPDATED_HK));
        assertThat(resolvedDate(), equalTo(RESOLVED_HK));
        assertThat(dateTimeCfTab1(), equalTo(DATETIME_CF_HK));
        navigation.issue().gotoIssueWorkLog(MKY_1);
        assertThat(worklogDate(), equalTo(WORKLOG_HK));
        navigation.issue().gotoIssueChangeHistory(MKY_1);
        assertThat(changeHistoryDate(), equalTo(HISTORY_HK));
    }

    // JRA-14238
    @Test
    @Restore("TestImageUrlXss.xml")
    public void testXssInImageUrls() throws Exception {
        navigation.issue().gotoIssue("HSP-1");

        // priority icon URL
        tester.assertTextNotPresent("\"'/><script>alert('prioritiezz');</script>");
        tester.assertTextPresent("&quot;'/&gt;&lt;script&gt;alert('prioritiezz');&lt;/script&gt;");

        // issue type icon URL
        tester.assertTextNotPresent("\"'/><script>alert('issue typezz');</script>");
        tester.assertTextPresent("&quot;'/&gt;&lt;script&gt;alert('issue typezz');&lt;/script&gt;");
    }

    @Test
    @WebTest(TIME_ZONES)
    @LoginAs(user = ADMIN_USERNAME)
    @Restore("TestIssueFields.xml")
    public void testDueDateShouldBeDisplayedInSystemTimeZone() throws Exception {
        final String MKY_1 = "MKY-1";

        final String HONG_KONG = "Asia/Hong_Kong";
        final String PAPUA_NEW_GUINEA = "Pacific/Port_Moresby";

        final String DUE_DATE_STRING = "10/Jan/99";

        // set due date and read it back
        navigation.issue().gotoIssue(MKY_1);
        navigation.issue().setDueDate(MKY_1, DUE_DATE_STRING);
        assertThat(dueDate(), equalTo(DUE_DATE_STRING));

        // change the default user time zone, then make sure due date is not affected by this change
        administration.generalConfiguration().setDefaultUserTimeZone(HONG_KONG);
        navigation.issue().gotoIssue(MKY_1);
        assertThat(dueDate(), equalTo(DUE_DATE_STRING));

        // setting it should work as before
        navigation.issue().setDueDate(MKY_1, DUE_DATE_STRING);
        assertThat(dueDate(), equalTo(DUE_DATE_STRING));

        // change the test user's time zone, then make sure due date was not affected
        navigation.userProfile().changeUserTimeZone(PAPUA_NEW_GUINEA);
        navigation.issue().gotoIssue(MKY_1);
        assertThat(dueDate(), equalTo(DUE_DATE_STRING));

        // setting it should still work in the system time zone
        navigation.issue().setDueDate(MKY_1, DUE_DATE_STRING);
        assertThat(dueDate(), equalTo(DUE_DATE_STRING));
    }

    private String createdDate() {
        return locator.css("#created-val").getText();
    }

    private String dueDate() {
        return locator.css("#due-date").getText();
    }

    private String updatedDate() {
        return locator.css("#updated-val").getText();
    }

    private String resolvedDate() {
        return locator.css("#resolutiondate-val").getText();
    }

    private String dateTimeCfTab1() {
        return locator.css("#customfield_10010-val").getText();
    }

    private String worklogDate() {
        return locator.xpath("//*[@id='worklog-10000']//*[@class='subText']").getText();
    }

    private String changeHistoryDate() {
        return locator.css("#changehistorydetails_10030 time").getText();
    }

    private Locator createCFValueLocator(final String tab, final int id) {
        return new XPathLocator(tester, String.format("//*[@id='%s']//*[@id='customfield_%d-val']", tab, id));
    }

    private Locator createCFTabLocator(final String tab) {
        return new XPathLocator(tester, String.format("//a[@href='#%s']", tab));
    }

    private void enableDeleteAllWorklogInDefaultPermissionScheme(final String groupName) {
        logger.log("enabling delete all worklog deletion in default permission scheme for group " + groupName);
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, DELETE_ALL_WORKLOGS, groupName);
    }

    private boolean isSubtaskPercentageGraphPresent() {
        final WebTable issueSummary = tester.getDialog().getWebTableBySummaryOrId("issuetable");
        final TableCell percentageCell = issueSummary.getTableCell(0, 5);
        return CLASSNAME_SUBTASK_PERCENTAGE_CELL.equals(percentageCell.getClassName());
    }

    protected void setEstimate(final String time) {
        tester.clickLink("edit-issue");
        tester.setFormElement("timetracking", time);
        tester.submit("Update");
    }
}
