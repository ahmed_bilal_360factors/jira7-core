package com.atlassian.jira.webtests.ztests.i18n;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.backdoor.ResourceBundleCacheControl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @since v6.4
 */
@WebTest({Category.FUNC_TEST, Category.I18N})
@LoginAs(user = ADMIN_USERNAME)
public class TestResourceBundleCache extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
    }

    @Ignore("Test started to fail, ignored until fixed: jdog.jira-dev.com/browse/JPA-808")
    @Test
    public void testResourceBundleCacheIsClearedFromPluginBundles() {
        administration.generalConfiguration().setJiraLocale("Deutsch (Deutschland)");
        navigation.dashboard();

        final ResourceBundleCacheControl resourceBundleCacheControl = backdoor.resourceBundleCacheControl();
        final Iterable<ResourceBundleCacheControl.CachedPluginBundle> cachedPluginResourceBundles =
                resourceBundleCacheControl.getCachedPluginResourceBundles();

        assertThat("ResourceBundle internal cache can not contain any plugin bundles to avoid duplication",
                cachedPluginResourceBundles, Matchers.<ResourceBundleCacheControl.CachedPluginBundle>emptyIterable());
    }

}
