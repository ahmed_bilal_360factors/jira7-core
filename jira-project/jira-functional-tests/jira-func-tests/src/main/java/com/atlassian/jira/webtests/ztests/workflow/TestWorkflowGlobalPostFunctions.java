package com.atlassian.jira.webtests.ztests.workflow;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.WORKFLOW})
@LoginAs(user = ADMIN_USERNAME)
public class TestWorkflowGlobalPostFunctions extends BaseJiraFuncTest {
    private static final String workflowName = "GlobalPostFunctions";

    @Inject
    private Administration administration;

    @Before
    public void setUp() {
        administration.restoreData("TestWorkflowGlobalPostFunctions.xml");
    }

    @Test
    public void testGlobalPostFunctionsCreatedFromCustomImportedWorkflowXMLDisplayOnOtherTab() {
        administration.workflows().goTo().workflowSteps(workflowName);

        tester.assertTextPresent("Workflow");
        assertions.assertNodeHasText(new CssLocator(tester, "h2 .workflow-name"), "GlobalPostFunctions");
        tester.clickLinkWithText("Move to Intermediate");

        assertions.assertNodeHasText(new CssLocator(tester, ".aui-page-header h2"), "Move to Intermediate");
        assertions.assertNodeHasText(new CssLocator(tester, ".menu-item #view_other"), "Other");
        tester.clickLink("view_other");

        assertions.assertNodeHasText(new CssLocator(tester, ".active-tab #view_other"), "Other");
        assertions.assertNodeHasText(new CssLocator(tester, ".active-pane .criteria-group-actions"), "Global Post Functions");
    }
}
