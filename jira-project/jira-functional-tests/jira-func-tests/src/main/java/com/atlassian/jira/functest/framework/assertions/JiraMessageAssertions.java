package com.atlassian.jira.functest.framework.assertions;

import com.google.inject.ImplementedBy;

/**
 * Assertions for common messages displayed
 *
 * @since v4.3
 */
@ImplementedBy(JiraMessageAssertionsImpl.class)
public interface JiraMessageAssertions {
    /**
     * Assertion about expected title.
     *
     * @param expectedTitle expected title
     */
    void assertHasTitle(String expectedTitle);

    void assertHasMessage(String expectedMsg);


}
