package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.testkit.client.restclient.User;
import com.atlassian.jira.testkit.client.restclient.WatchersClient;
import com.atlassian.jira.testkit.client.restclient.Watches;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.testkit.client.restclient.matcher.HasErrorMessage.hasErrorMessage;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Func tests for issue watching use cases.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestIssueResourceWatchers.xml")
public class TestIssueResourceWatchers extends BaseJiraFuncTest {

    /**
     * The user that is logged in.
     */
    private static final String loginUser = FRED_USERNAME;
    private static final String slashName = "kelpie/trevor";

    /**
     * The other user.
     */
    private final String anotherUser = "luser";

    private IssueClient issueClient;
    private WatchersClient watchersClient;

    @Inject
    private Administration administration;

    @Before
    public void setUp() {
        issueClient = new IssueClient(getEnvironmentData());
        watchersClient = new WatchersClient(getEnvironmentData());
    }

    @Test
    public void testWatchingDisabled() throws Exception {
        disableWatching();

        Issue issue = issueClient.get("HSP-1");
        assertNull(issue.fields.watches);
    }

    @Test
    public void testViewWatchersRequestExpanded() throws Exception {
        Issue issue = issueClient.get("HSP-1");
        assertEquals("HSP-1", issue.key);
        assertEquals(environmentData.getBaseUrl() + "/rest/api/2/issue/HSP-1/watchers", issue.fields.watches.self);
    }

    @Test
    public void testViewWatchersNoPermissionToViewButCanStillSeeCount() throws Exception {
        Issue issue = issueClient.loginAs("luser").get("HSP-1");
        assertEquals("HSP-1", issue.key);

        // should view watcher count only
        assertEquals((long) 2, issue.fields.watches.watchCount);
    }

    @Test
    public void testViewWatchersNoPermissionToViewButCanStillSeeMyselfAndCount() throws Exception {
        Issue issue = issueClient.loginAs("luser").get("HSP-2");
        assertEquals("HSP-2", issue.key);

        Watches watchers = issue.fields.watches;
        assertEquals(environmentData.getBaseUrl() + "/rest/api/2/issue/HSP-2/watchers", watchers.self);

        // should view watcher count only
        assertEquals((long) 2, watchers.watchCount);
    }

    @Test
    public void testViewWatchersIsWatching() throws Exception {
        Issue issue = issueClient.loginAs("luser").get("HSP-2");
        assertEquals("HSP-2", issue.key);

        Watches watchers = issue.fields.watches;
        assertEquals(environmentData.getBaseUrl() + "/rest/api/2/issue/HSP-2/watchers", watchers.self);
        assertTrue(watchers.isWatching);
    }

    @Test
    public void testViewWatchersIsNotWatching() throws Exception {
        Issue issue = issueClient.loginAs("luser").get("HSP-1");
        assertEquals("HSP-1", issue.key);

        Watches watchers = issue.fields.watches;
        assertEquals(environmentData.getBaseUrl() + "/rest/api/2/issue/HSP-1/watchers", watchers.self);
        assertFalse(watchers.isWatching);
    }

    @Test
    public void testWatchers_Anonymous() throws Exception {
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_PERM_SCHEME_ID, BROWSE_PROJECTS);

        Watches watchers = watchersClient.anonymous().get("HSP-1");
        assertEquals(environmentData.getBaseUrl() + "/rest/api/2/issue/HSP-1/watchers", watchers.self);
        assertEquals((long) 2, watchers.watchCount);
        assertFalse(watchers.isWatching);
        assertEquals(0, watchers.watchers.size());
    }

    @Test
    public void testViewWatchersSubresourceExpanded() throws Exception {
        Watches watchers = watchersClient.get("HSP-1");
        assertEquals(environmentData.getBaseUrl() + "/rest/api/2/issue/HSP-1/watchers", watchers.self);

        List<User> watcherList = watchers.watchers;
        assertEquals(2, watcherList.size());

        List<String> names = watcherList.stream().map(watcher -> watcher.name).collect(Collectors.toList());

        assertTrue(names.contains(FRED_USERNAME));
        assertTrue(names.contains(ADMIN_USERNAME));
    }

    @Test
    public void testViewWatchersSubresourceExpandedIssueDoesNotExist() throws Exception {
        Response response = watchersClient.getResponse("HSP-999");
        assertEquals(404, response.statusCode);
    }

    @Test
    public void testAddWatchWhenWatchingIsDisabled() throws Exception {
        disableWatching();

        Response resp = watchersClient.loginAs(loginUser).postResponse("HSP-2", loginUser);
        assertEquals(404, resp.statusCode);
    }

    @Test
    public void testAddWatchToIssueThatDoesNotExist() throws Exception {
        Response resp = watchersClient.loginAs(loginUser).postResponse("HSP-999", loginUser);
        assertEquals(404, resp.statusCode);
    }

    @Test
    public void testAddWatchToIssueThatIAmAlreadyWatching() throws Exception {
        Response resp = watchersClient.loginAs(loginUser).postResponse("HSP-1", loginUser);
        assertEquals(204, resp.statusCode);
    }

    @Test
    public void testAddWatchToIssue() throws Exception {
        Response resp = watchersClient.loginAs(loginUser).postResponse("HSP-2", loginUser);
        assertEquals(204, resp.statusCode);
        assertTrue(isWatching("HSP-2", loginUser));
    }

    @Test
    public void testAddWatchToIssue_emptyPOST() throws Exception {
        Response resp = watchersClient.loginAs(loginUser).postResponse("HSP-2", null);
        assertEquals(204, resp.statusCode);
        assertTrue(isWatching("HSP-2", loginUser));
    }

    @Test
    public void testAddWatchForAnotherUser() throws Exception {
        Response resp = watchersClient.loginAs(anotherUser).postResponse("HSP-2", loginUser);
        assertEquals(401, resp.statusCode);
    }

    @Test
    public void testAddWatchForAnotherUserCannotView() throws Exception {
        Response resp = watchersClient.postResponse("MKY-1", "jack");
        assertEquals(401, resp.statusCode);
        assertEquals("The user \"jack\" does not have permission to view this issue. This user will not be added to the watch list.", resp.entity.errorMessages.get(0));
    }

    @Test
    public void testRemoveWatchForAnotherUserCannotView() throws Exception {
        // This tests that we can remove a watcher that no longer has permission to view this issue.  (He cannot be added back)

        Response resp = watchersClient.deleteResponse("MKY-2", "jack");
        assertEquals(204, resp.statusCode);
        resp = watchersClient.postResponse("MKY-2", "jack");
        assertEquals(401, resp.statusCode);
        assertEquals("The user \"jack\" does not have permission to view this issue. This user will not be added to the watch list.", resp.entity.errorMessages.get(0));
    }

    @Test
    public void testRemoveWatchWhenWatchingIsDisabled() throws Exception {
        disableWatching();

        Response resp = watchersClient.loginAs(loginUser).deleteResponse("HSP-2", loginUser);
        assertEquals(404, resp.statusCode);
    }

    @Test
    public void testRemoveWatchToIssueThatDoesNotExist() throws Exception {
        Response resp = watchersClient.loginAs(loginUser).deleteResponse("HSP-999", loginUser);
        assertEquals(404, resp.statusCode);
    }

    @Test
    public void testRemoveWatchFromIssueThatIAmNotWatching() throws Exception {
        assertFalse(isWatching("HSP-2", loginUser));

        Response resp = watchersClient.loginAs(loginUser).deleteResponse("HSP-2", loginUser);
        assertEquals(204, resp.statusCode);
        assertFalse(isWatching("HSP-2", loginUser));
    }

    @Test
    public void testRemoveWatchForAnotherUser_Denied() throws Exception {
        Response resp = watchersClient.loginAs(anotherUser).deleteResponse("HSP-2", loginUser);
        assertEquals(401, resp.statusCode);
        assertThat(resp, hasErrorMessage("User 'luser' is not allowed to remove watchers from issue 'HSP-2'"));
    }

    @Test
    public void testRemoveWatchForAnotherUser_Allowed() throws Exception {
        Response resp = watchersClient.deleteResponse("HSP-2", loginUser);
        assertEquals(204, resp.statusCode);
        assertFalse(isWatching("HSP-2", loginUser));
    }

    @Test
    public void testRemoveWatch() throws Exception {
        assertTrue(isWatching("HSP-1", loginUser));

        Response resp = watchersClient.deleteResponse("HSP-1", loginUser);
        assertEquals(204, resp.statusCode);
        assertFalse(isWatching("HSP-1", loginUser));
    }

    @Test
    public void testSlashNamesWatchIssue() throws Exception {
        Response resp = watchersClient.postResponse("HSP-2", slashName);
        assertEquals(204, resp.statusCode);
        assertTrue(isWatching("HSP-2", slashName));

        resp = watchersClient.deleteResponse("HSP-2", slashName);
        assertEquals(204, resp.statusCode);
        assertFalse(isWatching("HSP-2", slashName));
    }

    @Test
    public void testAddingOrRemovingWatcherFromIssuesThatIAmNotAllowedToSeeReturns401() throws Exception {
        // add/remove a vote as anonymous
        {
            Response addResponse = watchersClient.anonymous().postResponse("HSP-1", loginUser);
            assertThat(addResponse.statusCode, equalTo(401));
            assertThat(addResponse, hasErrorMessage("You do not have the permission to see the specified issue."));

            Response delResponse = watchersClient.anonymous().deleteResponse("HSP-1", loginUser);
            assertThat(delResponse.statusCode, equalTo(401));
            assertThat(addResponse, hasErrorMessage("You do not have the permission to see the specified issue."));
        }

        // add/remove a vote as jack
        {
            Response addResponse = watchersClient.loginAs("jack").postResponse("HSP-1", loginUser);
            assertThat(addResponse.statusCode, equalTo(401));
            assertThat(addResponse, hasErrorMessage("User 'jack' is not allowed to add watchers to issue 'HSP-1'"));

            Response delResponse = watchersClient.loginAs("jack").deleteResponse("HSP-1", loginUser);
            assertThat(delResponse.statusCode, equalTo(401));
            assertThat(delResponse, hasErrorMessage("User 'jack' is not allowed to remove watchers from issue 'HSP-1'"));
        }
    }

    private boolean isWatching(String issueKey, String username) {
        Watches watchers = watchersClient.get(issueKey);
        for (User watcher : watchers.watchers) {
            if (username.equals(watcher.name)) {
                return true;
            }
        }

        return false;
    }

    private void disableWatching() {
        administration.generalConfiguration().disableWatching();
    }
}
