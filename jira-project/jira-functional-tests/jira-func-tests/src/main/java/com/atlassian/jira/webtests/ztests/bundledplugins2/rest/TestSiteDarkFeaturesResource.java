package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.DarkFeature;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client.SiteDarkFeaturesClientExt;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static java.lang.Boolean.TRUE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Func tests for SiteDarkFeaturesResource.
 *
 * @since v5.2
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@RestoreBlankInstance
public class TestSiteDarkFeaturesResource extends BaseJiraRestTest {
    private static final String DARK_FEATURE_KEY = "my.dark.feature";
    private static final String DARK_FEATURE_OTHER_KEY = "my.other.dark.feature";

    private SiteDarkFeaturesClientExt client;

    @Before
    public void setUpTest() {
        client = new SiteDarkFeaturesClientExt(environmentData);
    }

    /**
     * Tests getting dark feature
     */
    @Test
    public void testGet() throws Exception {
        client.loginAs("admin", "admin");
        DarkFeature darkFeature = client.get(DARK_FEATURE_KEY);
        assertFalse(darkFeature.enabled);

        backdoor.darkFeatures().enableForSite(DARK_FEATURE_KEY);
        darkFeature = client.get(DARK_FEATURE_KEY);
        assertTrue(darkFeature.enabled);
    }

    /**
     * Tests enabling dark feature
     */
    @Test
    public void testEnable() throws Exception {
        client.loginAs("admin", "admin");
        assertFalse(backdoor.darkFeatures().isGlobalEnabled(DARK_FEATURE_KEY));

        client.put(DARK_FEATURE_KEY, true);
        assertTrue(backdoor.darkFeatures().isGlobalEnabled(DARK_FEATURE_KEY));
    }

    /**
     * Tests disabling
     */
    @Test
    public void testDisable() throws Exception {
        backdoor.darkFeatures().enableForSite(DARK_FEATURE_KEY);
        assertTrue(backdoor.darkFeatures().isGlobalEnabled(DARK_FEATURE_KEY));

        client.loginAs("admin", "admin");
        client.put(DARK_FEATURE_KEY, false);
        assertFalse(backdoor.darkFeatures().isGlobalEnabled(DARK_FEATURE_KEY));
    }

    /**
     * Tests non-admin permissions
     */
    @Test
    public void testNonAdministrator() throws Exception {
        client.loginAs("fred");

        Response response = client.putResponse(DARK_FEATURE_KEY, true);
        assertEquals(403, response.statusCode);

        response = client.putResponse(DARK_FEATURE_KEY, false);
        assertEquals(403, response.statusCode);

        response = client.getResponse(DARK_FEATURE_KEY);
        assertEquals(403, response.statusCode);
    }

    @Test
    public void testBatchUpdateDarkFeature() throws Exception {
        // first enable both dark features
        Map<String, DarkFeature> enabledFeatures_1 = client.post(ImmutableMap.of(
                DARK_FEATURE_KEY, new DarkFeature(true),
                DARK_FEATURE_OTHER_KEY, new DarkFeature(true)
        )).siteFeatures;

        assertThat(enabledFeatures_1, allOf(hasKey(DARK_FEATURE_KEY), hasKey(DARK_FEATURE_OTHER_KEY)));
        assertThat(enabledFeatures_1.get(DARK_FEATURE_KEY).enabled, is(TRUE));
        assertThat(enabledFeatures_1.get(DARK_FEATURE_OTHER_KEY).enabled, is(TRUE));

        // now disable one of them
        Map<String, DarkFeature> enabledFeatures_2 = client.post(ImmutableMap.of(
                DARK_FEATURE_KEY, new DarkFeature(false),
                DARK_FEATURE_OTHER_KEY, new DarkFeature(true)
        )).siteFeatures;

        assertThat(enabledFeatures_2, allOf(not(hasKey(DARK_FEATURE_KEY)), hasKey(DARK_FEATURE_OTHER_KEY)));
        assertTrue(enabledFeatures_2.get(DARK_FEATURE_OTHER_KEY).enabled);

        // now toggle them
        Map<String, DarkFeature> enabledFeatures_3 = client.post(ImmutableMap.of(
                DARK_FEATURE_KEY, new DarkFeature(true),
                DARK_FEATURE_OTHER_KEY, new DarkFeature(false)
        )).siteFeatures;

        assertThat(enabledFeatures_3, allOf(hasKey(DARK_FEATURE_KEY), not(hasKey(DARK_FEATURE_OTHER_KEY))));
        assertTrue(enabledFeatures_3.get(DARK_FEATURE_KEY).enabled);
    }
}
