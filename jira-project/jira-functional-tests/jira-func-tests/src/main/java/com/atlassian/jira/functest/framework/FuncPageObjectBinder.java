package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.framework.page.WebTestPage;
import com.google.inject.Injector;

import javax.inject.Inject;

public class FuncPageObjectBinder {

    private final Injector injector;

    @Inject
    public FuncPageObjectBinder(final Injector injector) {
        this.injector = injector;
    }

    public <T extends WebTestPage> T injectFields(final T page) {
        injector.injectMembers(page);
        return page;
    }
}
