package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.util.issue.IssueInlineEdit;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.ISSUES})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestInlineEditIssueFieldsExceedingLimit extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Before
    public void setUpTest() {
        administration.restoreData("TestInlineEditIssueFieldsExceedingLimit.xml");
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void testInlineEditTextArea() throws Exception {
        testInlineEditField("customfield_10000", "customfield_10000-val");
    }

    @Test
    public void testInlineEditTextField() throws Exception {
        testInlineEditField("customfield_10001", "customfield_10001-val");
    }

    @Test
    public void testInlineEditUrlField() throws Exception {
        testInlineEditField("customfield_10002", "environment-val");
    }

    @Test
    public void testInlineEditEnvironment() throws Exception {
        testInlineEditField("environment", "environment-val");
    }

    @Test
    public void testInlineEditDescription() throws Exception {
        testInlineEditField("description", "description-val");
    }

    @Test
    public void testInlineEditSummary() throws Exception {
        // summary is not validated with jira character limit, edit succeeds
        testInlineEditField("summary", "summary-val", false);
    }

    private void testInlineEditField(String fieldName, String fieldId)
            throws Exception {
        testInlineEditField(fieldName, fieldId, true);
    }

    private void testInlineEditField(String fieldName, String fieldId, boolean shouldFail)
            throws Exception {
        navigation.issue().gotoIssue("HSP-2");
        final String oldValue = locator.id(fieldId).getText();
        final String newValue = oldValue + "s";

        // make sure the field is just at its limit
        backdoor.advancedSettings().setTextFieldCharacterLengthLimit(oldValue.length());

        IssueInlineEdit inlineEdit = new IssueInlineEdit(locator, tester, environmentData);
        inlineEdit.inlineEditField("10100", fieldName, newValue);

        // Refresh the page
        navigation.issue().gotoIssue("HSP-2");

        if (shouldFail) {
            // Make sure the field has the value unchanged
            assertions.assertNodeByIdEquals(fieldId, oldValue);
        } else {
            assertions.assertNodeByIdEquals(fieldId, newValue);
        }
    }
}
