package com.atlassian.jira.functest.matcher;

import com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookListener;
import com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookRegistrationClient;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.List;

import static com.google.common.collect.Iterables.any;

public class WebHookMatchers {
    public static Matcher<List<WebHookRegistrationClient.RegistrationResponse>> containsWebHook(final WebHookListener listener) {
        return new TypeSafeMatcher<List<WebHookRegistrationClient.RegistrationResponse>>() {
            @Override
            protected boolean matchesSafely(final List<WebHookRegistrationClient.RegistrationResponse> registrationResponses) {
                return any(registrationResponses, new Predicate<WebHookRegistrationClient.RegistrationResponse>() {
                    @Override
                    public boolean apply(final WebHookRegistrationClient.RegistrationResponse response) {
                        return matches(response);
                    }
                });
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("WebHook response doesn't contain " + listener);
            }

            public boolean matches(final WebHookRegistrationClient.RegistrationResponse response) {
                return response.name.equals(listener.getName())
                        && response.url.equals(listener.getUrl())
                        && Sets.newHashSet(response.events).equals(Sets.newHashSet(listener.getEvents()))
                        && response.getFilterForIssueSection().equals(listener.getFilter())
                        && Boolean.valueOf(response.excludeBody).equals(listener.isExcludeIssueDetails())
                        && response.self.endsWith("/" + listener.getId());
            }
        };
    }
}
