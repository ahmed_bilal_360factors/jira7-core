package com.atlassian.jira.webtests.ztests.fields;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Tests behaviour of the Screens page in case of invalid workflows.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.FIELDS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestFieldScreensInvalidWorkflow extends BaseJiraFuncTest {
    private static final String SCREEN_TABLE_LOCATOR = "//table[@class='gridBox']";

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Test
    public void testViewFieldScreensWithInvalidWorkflow() {
        administration.restoreData("InvalidWorkflow.xml");
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        administration.viewFieldScreens().goTo();
        assertTransitionWithoutStepsNotVisible();
    }

    private void assertTransitionsWithStepsVisible() {
        assertions.getTextAssertions().assertTextPresent(locator.xpath(SCREEN_TABLE_LOCATOR), "(Close Issue)");
        assertions.getTextAssertions().assertTextPresent(locator.xpath(SCREEN_TABLE_LOCATOR), "(Resolve Issue)");
        assertions.getTextAssertions().assertTextPresent(locator.xpath(SCREEN_TABLE_LOCATOR), "(This task is done!)");
        assertions.getTextAssertions().assertTextPresent(locator.xpath(SCREEN_TABLE_LOCATOR), "(Reopen Issue)");
    }

    private void assertTransitionWithoutStepsNotVisible() {
        assertions.getTextAssertions().assertTextNotPresent(locator.xpath(SCREEN_TABLE_LOCATOR), "(Try Again!)");
    }
}
