package com.atlassian.jira.webtests.ztests.admin.index;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.ProgressBar;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;
import java.net.URL;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests admin reindex page
 *
 * @since v3.13
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.BROWSING})
@Restore("TestIndexAdmin.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestIndexAdmin extends BaseJiraFuncTest {
    private static final int RETRY_COUNT = 100;
    private static final int SLEEP_TIME = 1000;
    private static final String REFRESH_BUTTON = "Refresh";
    private static final String ACKNOWLEDGE_BUTTON = "Acknowledge";
    private static final String REINDEX_BUTTON = "reindex";
    private static final String DONE_BUTTON = "Done";

    @Inject
    private ProgressBar progressBar;

    /**
     * Make sure that a SystemAdmin can re-index.
     */
    @Test
    public void testReindexAsSystemAdmin() {
        gotoIndexing();

        tester.assertTextPresent("JIRA will be unavailable to all users until the re-index is complete.");
        executeReindex();
    }


    /**
     * Now that nobody can change index paths under normal circumstances we don't show the silly message any more.
     * so this is the same as a sysadmin.
     */
    @Test
    public void testReindexAsNormalAdmin() {
        //login as a user who only has administrator permissions.
        navigation.login("miniadmin", "miniadmin");

        gotoIndexing();

        tester.assertTextPresent("JIRA will be unavailable to all users until the re-index is complete.");

        executeReindex();
    }

    /**
     * Test task acknowledgement.
     */
    @Test
    public void testTaskAcknowledgement() {
        gotoIndexing();

        tester.assertTextPresent("JIRA will be unavailable to all users until the re-index is complete.");

        tester.submit(REINDEX_BUTTON);
        String taskPage = waitForIndexCompletetion();

        //switch users.
        navigation.login("miniadmin", "miniadmin");

        //this user should only see a reindex page
        tester.gotoPage(taskPage);
        tester.assertSubmitButtonPresent(DONE_BUTTON);
        tester.assertSubmitButtonNotPresent(ACKNOWLEDGE_BUTTON);
        tester.assertTextPresent("who started this task should acknowledge it.");
        progressBar.validateProgressBarUI(DONE_BUTTON);
        //the user who started the task should be mentioned.
        tester.assertLinkPresentWithText(ADMIN_USERNAME, 0);
        tester.assertLinkPresentWithText(ADMIN_USERNAME, 1);

        //okay lets go back to the index screen.
        tester.submit(DONE_BUTTON);
        assertTrue(getRedirect().contains("IndexAdmin.jspa?reindexTime="));

        //lets now ack the task and make sure that it still works.
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        tester.gotoPage(taskPage);
        acknowledgeSuccessfulTask();

        //make sure the submitting user cannot see the an acked task.
        tester.gotoPage(taskPage);
        checkNoTaskPage();

        //make sure the submitting user other user cannot see the acked task.
        navigation.login("miniadmin", "miniadmin");
        tester.gotoPage(taskPage);
        checkNoTaskPage();
    }

    private void checkNoTaskPage() {
        tester.assertTextPresent("A task could not be found for the given task id");
        tester.assertTextPresent("Task Not Found");

        tester.submit(DONE_BUTTON);

        assertTrue(getRedirect().contains("IndexAdmin.jspa"));

        tester.assertTextNotPresent("successful");
    }

    private void gotoIndexing() {
        navigation.gotoAdminSection(Navigation.AdminSection.INDEXING);
    }

    private void executeReindex() {

        tester.submit(REINDEX_BUTTON);

        waitForIndexCompletetion();
        acknowledgeSuccessfulTask();
    }

    private String waitForIndexCompletetion() {
        return this.waitForIndexCompletetion(SLEEP_TIME, RETRY_COUNT);
    }

    private String waitForIndexCompletetion(long sleepTime, int retryCount) {
        String redirect = getRedirect();
        assertTrue(redirect.contains("IndexProgress.jspa?taskId="));

        for (int i = 0; i < retryCount; i++) {
            if (tester.getDialog().hasSubmitButton(REFRESH_BUTTON)) {
                progressBar.validateProgressBarUI(REFRESH_BUTTON);
                tester.submit(REFRESH_BUTTON);
            } else if (tester.getDialog().hasSubmitButton(ACKNOWLEDGE_BUTTON)) {
                progressBar.validateProgressBarUI(ACKNOWLEDGE_BUTTON);
                //we should be the submitting user.
                tester.assertTextNotPresent("who started this task should acknowledge it.");

                return redirect;
            } else {
                fail("Unexpected button on progress screen.");
            }

            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                //ignore me.
            }
        }

        fail("Index operation did not complete after " + (sleepTime * retryCount / 1000d) + " sec.");

        return redirect;
    }

    private void acknowledgeSuccessfulTask() {
        tester.submit(ACKNOWLEDGE_BUTTON);

        //do we get redirected to the correct page.
        assertTrue(getRedirect().contains("IndexAdmin.jspa?reindexTime="));

        //we should be successful. 
        tester.assertTextPresent("successful");
    }

    private String getRedirect() {
        URL url = tester.getDialog().getResponse().getURL();
        String queryString = url.getQuery() != null ? '?' + url.getQuery() : "";
        return url.getPath() + queryString;
    }

}
