package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.atlassian.jira.webtests.util.LocalTestEnvironmentData;
import com.google.inject.Binder;
import com.google.inject.Module;

import java.util.function.Supplier;


/**
 * Guice module providing backdoor and environment data initialisation.
 *
 * @since v6.5
 */
public class BackdoorModule implements Module, Supplier<Backdoor> {
    private static final JIRAEnvironmentData environmentData = LocalTestEnvironmentData.DEFAULT;
    private final Backdoor backdoor = new Backdoor(environmentData);

    public JIRAEnvironmentData getEnvironmentData() {
        return environmentData;
    }

    public Backdoor getBackdoor() {
        return backdoor;
    }

    @Override
    public void configure(final Binder binder) {
        binder.bind(Backdoor.class).toInstance(backdoor);
        binder.bind(com.atlassian.jira.testkit.client.Backdoor.class).toInstance(backdoor.getTestkit());
        binder.bind(JIRAEnvironmentData.class).toInstance(environmentData);
    }

    @Override
    public Backdoor get() {
        return getBackdoor();
    }
}
