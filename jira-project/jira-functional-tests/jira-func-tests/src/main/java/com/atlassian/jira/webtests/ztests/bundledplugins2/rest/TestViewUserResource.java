package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.test.util.lic.ApplicationLicenseConstants;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.webtests.LicenseKeys;
import com.google.common.collect.ImmutableSet;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

/**
 * @since v7.0
 */
@WebTest({Category.FUNC_TEST, Category.REST, Category.USERS_AND_GROUPS})
public class TestViewUserResource extends BaseJiraRestTest {
    private ManualClient manualClient;

    private final String BOB_USERNAME = "bob";
    private final String KEVIN_USERNAME = "kevin";
    private final String FRED_USERNAME = "fred";
    private final String ADMIN_USERNAME = "admin";

    private final String CORE_GROUP = "jira-users";
    private final String TEST_GROUP = "test-group";
    private final String SOFTWARE_GROUP = "jira-software-group";

    private static final String ENGLISH_LOCALE = "en_US";

    private final Matcher<ApplicationSelection> JIRA_REFERENCE_MATCHER = applicationSelectionMatcher("jira-reference", false, false);
    private final Matcher<ApplicationSelection> JIRA_SERVICE_DESK_MATCHER = applicationSelectionMatcher(ApplicationLicenseConstants.SERVICE_DESK_KEY, false, false);

    @Before
    public void setUpInstance() {
        backdoor.restoreBlankInstance(LicenseKeys.MULTI_ROLE);
        backdoor.applicationRoles().putRole(ApplicationLicenseConstants.SERVICE_DESK_KEY);
        backdoor.applicationRoles().putRole("jira-reference");

        backdoor.usersAndGroups().addGroup(SOFTWARE_GROUP);
        // Overriding the default applications that are set by the database patch
        backdoor.applicationRoles().putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.SOFTWARE_KEY, false, groups(SOFTWARE_GROUP), groups(SOFTWARE_GROUP));

        backdoor.usersAndGroups().addUser(BOB_USERNAME);
        backdoor.usersAndGroups().addUserToGroup(BOB_USERNAME, SOFTWARE_GROUP);

        backdoor.usersAndGroups().addGroup(TEST_GROUP);
        backdoor.applicationRoles().putRoleAndSetDefault(ApplicationLicenseConstants.TEST_KEY, TEST_GROUP);

        backdoor.usersAndGroups().addUser(KEVIN_USERNAME);
        backdoor.usersAndGroups().addUserToGroup(KEVIN_USERNAME, TEST_GROUP);

        manualClient = new ManualClient(environmentData);

        /**
         * EXPECTED STATE:
         * bob - JIRA Software access
         * kevin - Test Product access
         * fred - JIRA Core access
         *
         * GROUPS:
         * JIRA Core - jira-users
         * JIRA Software - jira-software-group
         * JIRA Test App - test-group
         */
    }

    @Test
    public void shouldAddBobToApplicationAndReturnHisCurrentStatus() {
        final UserApplicationUpdateBean userApplicationUpdateBean = manualClient.addUserToApplication(BOB_USERNAME, ApplicationLicenseConstants.TEST_KEY);

        assertThat(userApplicationUpdateBean.getSelectableApplications(), Matchers.containsInAnyOrder(
                applicationSelectionMatcher(ApplicationLicenseConstants.TEST_KEY, true, true),
                applicationSelectionMatcher(ApplicationLicenseConstants.CORE_KEY, true, false),
                applicationSelectionMatcher(ApplicationLicenseConstants.SOFTWARE_KEY, true, true),
                JIRA_REFERENCE_MATCHER, JIRA_SERVICE_DESK_MATCHER
        ));

        assertThat(userApplicationUpdateBean.getUserGroups(), Matchers.containsInAnyOrder(
                groupViewMatcher(TEST_GROUP),
                groupViewMatcher(SOFTWARE_GROUP)
        ));
    }

    @Test
    public void shouldChangeFredApplicationAccessAndReturnHisCurrentStatus() {
        UserApplicationUpdateBean userApplicationUpdateBean = manualClient.addUserToApplication(FRED_USERNAME, ApplicationLicenseConstants.TEST_KEY);

        assertThat(userApplicationUpdateBean.getSelectableApplications(), Matchers.containsInAnyOrder(
                applicationSelectionMatcher(ApplicationLicenseConstants.TEST_KEY, true, true),
                applicationSelectionMatcher(ApplicationLicenseConstants.CORE_KEY, true, true),
                applicationSelectionMatcher(ApplicationLicenseConstants.SOFTWARE_KEY, true, false),
                JIRA_REFERENCE_MATCHER, JIRA_SERVICE_DESK_MATCHER
        ));
        assertThat(userApplicationUpdateBean.getUserGroups(), Matchers.containsInAnyOrder(
                groupViewMatcher(TEST_GROUP),
                groupViewMatcher(CORE_GROUP)
        ));

        userApplicationUpdateBean = manualClient.removeUserFromApplication(FRED_USERNAME, ApplicationLicenseConstants.CORE_KEY);

        assertThat(userApplicationUpdateBean.getSelectableApplications(), Matchers.containsInAnyOrder(
                applicationSelectionMatcher(ApplicationLicenseConstants.TEST_KEY, true, true),
                applicationSelectionMatcher(ApplicationLicenseConstants.CORE_KEY, true, false),
                applicationSelectionMatcher(ApplicationLicenseConstants.SOFTWARE_KEY, true, false),
                JIRA_REFERENCE_MATCHER, JIRA_SERVICE_DESK_MATCHER
        ));

        assertThat(userApplicationUpdateBean.getUserGroups(), Matchers.contains(
                groupViewMatcher(TEST_GROUP)
        ));
    }

    @Test
    public void shouldRemoveKevinFromApplicationAndReturnHisCurrentStatus() {
        final UserApplicationUpdateBean userApplicationUpdateBean = manualClient.removeUserFromApplication(KEVIN_USERNAME, ApplicationLicenseConstants.TEST_KEY);

        assertThat(userApplicationUpdateBean.getSelectableApplications(), Matchers.containsInAnyOrder(
                applicationSelectionMatcher(ApplicationLicenseConstants.TEST_KEY, true, false),
                applicationSelectionMatcher(ApplicationLicenseConstants.CORE_KEY, true, false),
                applicationSelectionMatcher(ApplicationLicenseConstants.SOFTWARE_KEY, true, false),
                JIRA_REFERENCE_MATCHER, JIRA_SERVICE_DESK_MATCHER
        ));

        assertThat(userApplicationUpdateBean.getUserGroups(), Matchers.empty());
    }

    @Test
    public void shouldNotRemoveAdminFromCoreAndReturnHisCurrentStatus() {
        final UserApplicationUpdateBean errorResponse = manualClient.removeUserFromApplicationWithAnError(ADMIN_USERNAME, ApplicationLicenseConstants.CORE_KEY, ClientResponse.Status.BAD_REQUEST);
        final List<String> errorMessages = errorResponse.getErrorMessages();

        assertThat(ImmutableSet.of(backdoor.i18n().getText("admin.errors.application.cannot.remove.application.last.admin.group", ENGLISH_LOCALE)), Matchers.containsInAnyOrder(errorMessages.toArray()));
    }

    final class ManualClient extends RestApiClient<ManualClient> {
        protected ManualClient(final JIRAEnvironmentData environmentData) {
            super(environmentData);
        }

        protected WebResource createResource() {
            return resourceRoot(environmentData.getBaseUrl().toExternalForm()).path("rest").path("internal").path("2").path("viewuser");
        }

        private WebResource.Builder applicationAccessResource(final String username, final String applicationKey) {
            return createResource().path("application").path(applicationKey).queryParam("username", username).accept(new MediaType[]{MediaType.APPLICATION_JSON_TYPE}).type(MediaType.APPLICATION_JSON_TYPE);
        }

        public UserApplicationUpdateBean addUserToApplication(final String username, final String applicationKey) {
            return applicationAccessResource(username, applicationKey).post(UserApplicationUpdateBean.class);
        }

        public UserApplicationUpdateBean removeUserFromApplication(final String username, final String applicationKey) {
            return applicationAccessResource(username, applicationKey).delete(UserApplicationUpdateBean.class);
        }

        public UserApplicationUpdateBean removeUserFromApplicationWithAnError(final String username, final String applicationKey, final ClientResponse.Status expectedResponseStatus) {
            final ClientResponse clientResponse = applicationAccessResource(username, applicationKey).delete(ClientResponse.class);
            assertEquals(expectedResponseStatus, clientResponse.getClientResponseStatus());
            return clientResponse.getEntity(UserApplicationUpdateBean.class);
        }
    }

    @JsonAutoDetect
    static class UserApplicationUpdateBean {
        private List<String> errorMessages;
        private boolean isEditable;
        private List<ApplicationSelection> selectableApplications;
        private List<GroupView> userGroups;

        public List<String> getErrorMessages() {
            return errorMessages;
        }

        public boolean isEditable() {
            return isEditable;
        }

        public List<ApplicationSelection> getSelectableApplications() {
            return selectableApplications;
        }

        public List<GroupView> getUserGroups() {
            return userGroups;
        }
    }

    private static Matcher<ApplicationSelection> applicationSelectionMatcher(final String key, final boolean selectable, final boolean selected) {
        return new TypeSafeMatcher<ApplicationSelection>() {
            @Override
            protected boolean matchesSafely(final ApplicationSelection item) {
                return item.selected == selected && item.selectable == selectable && StringUtils.equals(key, item.getKey());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("ApplicationSelection with key ").appendValue(key).appendText(", ").appendText(selectable ? "" : "non ").appendText("selectable and ").appendText(selected ? "" : "not ").appendText("selected");
            }
        };
    }

    @JsonAutoDetect
    static class ApplicationSelection {
        private String key;
        private boolean selectable;
        private boolean selected;

        public String getKey() {
            return key;
        }

        public boolean isSelectable() {
            return selectable;
        }

        public boolean isSelected() {
            return selected;
        }

        @Override
        public String toString() {
            return "ApplicationSelection{" +
                    "key='" + key + '\'' +
                    ", selectable=" + selectable +
                    ", selected=" + selected +
                    '}';
        }
    }

    private static Matcher<GroupView> groupViewMatcher(final String name) {
        return new TypeSafeMatcher<GroupView>() {
            @Override
            protected boolean matchesSafely(final GroupView item) {
                return StringUtils.equals(item.getName(), name);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("GroupView with name ").appendValue(name);
            }
        };
    }

    @JsonAutoDetect
    static class GroupView {
        private String name;

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "GroupView{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

    private static Iterable<String> groups(String... groups) {
        return ImmutableSet.copyOf(groups);
    }
}
