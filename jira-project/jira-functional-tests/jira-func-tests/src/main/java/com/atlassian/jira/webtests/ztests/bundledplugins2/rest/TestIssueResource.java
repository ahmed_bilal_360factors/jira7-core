package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FuncTestRestClient;
import com.atlassian.jira.functest.framework.FuncTestUrlHelper;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.google.common.collect.ImmutableSet;
import com.meterware.httpunit.WebResponse;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.inject.Inject;
import java.io.IOException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static java.util.Collections.singletonMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Func tests for IssueResource.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestIssueResource extends BaseJiraFuncTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Inject
    private FuncTestRestClient restClient;

    @Inject
    private Navigation navigation;

    @Inject
    private Administration administration;

    private IssueClient issueClient;
    private FuncTestUrlHelper funcTestUrlHelper;

    @Before
    public void createIssueClient() {
        issueClient = new IssueClient(environmentData);
        funcTestUrlHelper = new FuncTestUrlHelper(environmentData);
    }

    @Test
    public void testViewIssueNotFound() throws Exception {

        final WebResponse resp123 = restClient.GET("rest/api/2/issue/1");
        assertEquals(404, resp123.getResponseCode());
        assertNoLongerExistsError(resp123);

        final WebResponse resp123Xml = restClient.GET("rest/api/2/issue/1", singletonMap("Accept", "application/xml;q=0.9,*/*;q=0.8"));
        assertEquals(404, resp123Xml.getResponseCode());
        assertNoLongerExistsError(resp123Xml);

        exception.expect(IOException.class);
        exception.expectMessage("406");
        restClient.GET("rest/api/2/issue/1", singletonMap("Accept", "application/xml;q=0.9"));
    }

    // ensure we get a 401 and not a 404 if the issue actually exists but we are not logged in.
    @Test
    public void testNotLoggedIn_IssueExists() throws Exception {

        final String key = navigation.issue().createIssue("monkey", "Bug", "test bug");
        navigation.logout();

        final WebResponse response = restClient.GET("/rest/api/2/issue/" + key);
        assertEquals(401, response.getResponseCode());
    }

    @Test
    public void testCaseInsensitiveKeyLookup() throws Exception {
        final String key = navigation.issue().createIssue("monkey", "Bug", "test bug");

        final Issue realissue = issueClient.get(key);

        Issue issue = issueClient.get(key.toLowerCase());
        assertEquals(realissue.id, issue.id);
        assertEquals(key, issue.key);

        issue = issueClient.get(key.toUpperCase());
        assertEquals(realissue.id, issue.id);
        assertEquals(key, issue.key);
    }

    @Test
    public void testMovedIssueKeyLookup() throws Exception {

        final String key = navigation.issue().createIssue("monkey", "Bug", "test bug");

        final Issue origissue = issueClient.get(key);

        navigation.issue().viewIssue(key);
        tester.clickLink("move-issue");
        // Select 'Bovine' from select box 'pid'.
        navigation.issue().selectProject("homosapien");
        tester.submit("Next >>");
        tester.submit("Next >>");
        tester.submit("Move");

        Issue issue = issueClient.get(key);
        assertEquals(origissue.id, issue.id);
        assertEquals("HSP-1", issue.key);

        issue = issueClient.get(key.toLowerCase());
        assertEquals(origissue.id, issue.id);
        assertEquals("HSP-1", issue.key);

        issue = issueClient.get(key.toUpperCase());
        assertEquals(origissue.id, issue.id);
        assertEquals("HSP-1", issue.key);
    }

    @Test
    public void testUnassignedIssueHasNoValue() throws Exception {
        administration.generalConfiguration().setAllowUnassignedIssues(true);
        final String key = navigation.issue().createIssue("monkey", "Bug", "test bug");
        navigation.issue().assignIssue(key, "my comment", "Unassigned");

        final Issue issue = issueClient.get(key);
        assertNull(issue.fields.assignee);
    }

    @Test
    @Restore("TestIssueResourceTransitions.xml")
    public void testIssueResourceAcceptsExpandingTransitions() throws Exception {
        final Issue issue = issueClient.getFromURL(getUrl("HSP-1", "transitions"));

        assertEquals(3, issue.transitions.size());
        assertEquals("Resolve Issue", issue.transitions.get(1).name);
        assertNull(issue.transitions.get(1).fields);
    }

    @Test
    @Restore("TestIssueResourceTransitions.xml")
    public void testIssueResourceAcceptsExpandingTransitionsFields() throws Exception {
        final Issue issue = issueClient.getFromURL(getUrl("HSP-1", "transitions.fields"));

        assertEquals(3, issue.transitions.size());
        assertEquals(ImmutableSet.of("fixVersions", "customfield_10000", "assignee", "resolution"), issue.transitions.get(1).fields.keySet());
        assertTrue("There is a transition requiring 'resolution'", issue.transitions.get(1).fields.get("resolution").required);
    }

    private String getUrl(String issueKey, String expand) {
        String path = "/rest/api/2/issue/" + issueKey;

        if (StringUtils.isNotEmpty(expand)) {
            path += "?expand=" + expand;
        }

        return funcTestUrlHelper.getBaseUrlPlus(path);
    }

    private void assertNoLongerExistsError(final WebResponse resp123) throws JSONException, IOException {
        // {"errorMessages":["The issue no longer exists."],"errors":[]}
        final JSONObject content = new JSONObject(resp123.getText());
        assertEquals(1, content.getJSONArray("errorMessages").length());
        assertEquals("Issue Does Not Exist", content.getJSONArray("errorMessages").getString(0));
    }
}
