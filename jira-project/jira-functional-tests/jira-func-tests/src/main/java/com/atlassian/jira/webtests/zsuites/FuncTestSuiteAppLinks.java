package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * AppLinks-related tests.
 *
 * @since v4.3
 */
public class FuncTestSuiteAppLinks {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.bundledplugins2.applinks", true))
                .build();
    }
}
