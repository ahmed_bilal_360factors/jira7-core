package com.atlassian.jira.webtests.ztests.customfield;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.EDIT_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.MOVE_ISSUES;

/**
 * Test for JRA-13808
 */
@WebTest({Category.FUNC_TEST, Category.CUSTOM_FIELDS, Category.FIELDS, Category.PERMISSIONS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestCustomFieldsNoSearcherPermissions extends BaseJiraFuncTest {
    private static final Long SCHEME_ID = 10000L;
    private static final Long SCHEME_SECURITY_LEVEL_ID = 10000L;
    private static final String USER_PICKER_CUSTOM_FIELD = "customfield_10000";
    private static final String GROUP_PICKER_CUSTOM_FIELD = "customfield_10001";


    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreData("TestCustomFieldsNoSearcherPermissions.xml");
    }

    /**
     * Tests to ensure that setting the searcher of a customfield that's used in a permission scheme or issue level
     * security scheme will throw an error.
     */
    @Test
    public void testEditCustomFieldSetSearcherToNone() {
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        tester.clickLink("edit_User picker");
        tester.setFormElement("name", "User picker");
        //set the searcher to None.
        tester.selectOption("searcher", "None");
        tester.submit("Update");

        //check that the update didn't succeed.
        tester.assertTextPresent("Search Template cannot be set to &#39;None&#39; because this custom field is used in the following Permission Scheme(s): Default Permission Scheme");
        tester.assertTextPresent("Search Template cannot be set to &#39;None&#39; because this custom field is used in the following Issue Level Security Scheme(s): TestScheme");

        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        tester.clickLink("edit_multigrouppicker");
        tester.setFormElement("name", "multigrouppicker");
        tester.selectOption("searcher", "None");
        tester.submit("Update");

        tester.assertTextPresent("Search Template cannot be set to &#39;None&#39; because this custom field is used in the following Permission Scheme(s): Default Permission Scheme");
        tester.assertTextPresent("Search Template cannot be set to &#39;None&#39; because this custom field is used in the following Issue Level Security Scheme(s): TestScheme");
    }

    /**
     * Tests to ensure that deleting a customfield that's used in a permission scheme or issue level
     * security scheme will throw an error.
     */
    @Test
    public void testDeleteCustomField() {
        //try deleting the user picker field
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        tester.clickLink("del_" + USER_PICKER_CUSTOM_FIELD);
        tester.submit("Delete");
        tester.assertTextPresent("Custom field cannot be deleted because it is used in the following Permission Scheme(s): Default Permission Scheme");
        tester.assertTextPresent("Custom field cannot be deleted because it is used in the following Issue Level Security Scheme(s): TestScheme");

        //try deleting the group picker field.
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        tester.clickLink("del_" + GROUP_PICKER_CUSTOM_FIELD);
        tester.submit("Delete");
        tester.assertTextPresent("Custom field cannot be deleted because it is used in the following Permission Scheme(s): Default Permission Scheme");
        tester.assertTextPresent("Custom field cannot be deleted because it is used in the following Issue Level Security Scheme(s): TestScheme");
    }

    @Test
    public void testAddCustomFieldWithoutSearcherToPermission() {
        backdoor.darkFeatures().enableForSite("com.atlassian.jira.permission-schemes.single-page-ui.disabled");
        // We shouldn't be able to use the "nosearchercf" Custom Field in a Permission scheme because it has no searcher.
        navigation.gotoAdminSection(Navigation.AdminSection.PERMISSION_SCHEMES);
        tester.clickLinkWithText("Default Permission Scheme");
        tester.clickLink("add_perm_" + CREATE_ISSUES.permissionKey());
        tester.checkCheckbox("type", "userCF");
        tester.selectOption("userCF", "nosearchercf");
        tester.submit(" Add ");
        tester.assertTextPresent("Custom field &#39;nosearchercf&#39; is not indexed for searching - please add a Search Template to this Custom Field.");
    }

    @Test
    public void testAddCustomFieldWithoutSearcherToIssueLevelPermission() {
        // We shouldn&#39;t be able to use the "nosearchercf" Custom Field in an Issue Level Permission because it has no searcher.
        navigation.gotoAdminSection(Navigation.AdminSection.SECURITY_SCHEMES);
        tester.clickLinkWithText("Security Levels");
        tester.clickLink("add_TestLevel");
        tester.checkCheckbox("type", "userCF");
        tester.selectOption("userCF", "nosearchercf");
        tester.submit(" Add ");
        tester.assertTextPresent("Custom field &#39;nosearchercf&#39; is not indexed for searching - please add a Search Template to this Custom Field.");
    }

    /**
     * Test that adding a searcher to the customfield, makes it possible for that customfield to be added to a permission.
     */
    @Test
    public void testAddingSearcherToCustomField() {
        backdoor.darkFeatures().enableForSite("com.atlassian.jira.permission-schemes.single-page-ui.disabled");
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        tester.clickLink("edit_nosearchercf");
        tester.selectOption("searcher", "User Picker & Group Searcher");
        tester.submit("Update");
        tester.clickLink("permission_schemes");
        tester.clickLink("0_edit");
        tester.clickLink("add_perm_" + MOVE_ISSUES.permissionKey());
        tester.checkCheckbox("type", "userCF");
        tester.selectOption("userCF", "nosearchercf");
        tester.submit(" Add ");
        tester.assertTextPresent("Default Permission Scheme");
        final String response = tester.getDialog().getResponseText();
        assertions.text().assertTextSequence(response, "Move Issues", "nosearchercf");
    }

    /**
     * Test that we can remove a customfield, after we&#39;ve removed it from permission and issuelevelschemes
     */
    @Test
    public void testRemovingCustomField() {
        //remove from permission scheme
        backdoor.permissionSchemes().removeUserCustomFieldPermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES, "customfield_10000");

        backdoor.issueSecuritySchemesControl().deleteSecurityLevel(SCHEME_ID, SCHEME_SECURITY_LEVEL_ID);

        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        administration.customFields().removeCustomField(USER_PICKER_CUSTOM_FIELD);

        tester.assertTextPresent("custom fields");
        tester.assertTextNotPresent("User picker");
    }

    /**
     * Tests to ensure that deleting a customfield that&#39;s used in a permission scheme or issue level
     * security scheme will throw an error.
     */
    @Test
    public void testDeleteIssueSecurityLevelFlushesCache() {
        //grant the admin user permission to set issue security and add the scheme for the HSP project
        backdoor.permissionSchemes().addUserPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, ADMIN_USERNAME);
        administration.project().associateIssueLevelSecurityScheme("homosapien", "TestScheme");

        backdoor.issueSecuritySchemesControl().addUserToSecurityLevel(SCHEME_ID, SCHEME_SECURITY_LEVEL_ID, ADMIN_USERNAME);


        //check that the level is present.
        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("edit-issue");
        tester.assertTextPresent("TestLevel");

        //add the admin to the user CF
        tester.setFormElement(USER_PICKER_CUSTOM_FIELD, ADMIN_USERNAME);
        tester.submit("Update");


        //now lets remove the user -> admin security level.  The level should still be available due to the
        //user CF
        backdoor.issueSecuritySchemesControl().deleteUserFromSecurityLevel(SCHEME_ID, SCHEME_SECURITY_LEVEL_ID, ADMIN_USERNAME);

        //check that the level is present.
        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("edit-issue");
        tester.assertTextPresent("TestLevel");

        //now lets delete the user CF.  Level should no longer be present in the issue afterwards. - except for a temporary fix to JRA-14323
        backdoor.issueSecuritySchemesControl().deleteUserCustomFieldFromSecurityLevel(SCHEME_ID, SCHEME_SECURITY_LEVEL_ID, USER_PICKER_CUSTOM_FIELD);

        // TODO: Remove this with the proper fix to  JRA-14323.
        backdoor.issueSecuritySchemesControl().deleteSecurityLevel(SCHEME_ID, SCHEME_SECURITY_LEVEL_ID);

        //check that the level is no longer present.
        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("edit-issue");
        tester.assertTextNotPresent("TestLevel");
    }

}
