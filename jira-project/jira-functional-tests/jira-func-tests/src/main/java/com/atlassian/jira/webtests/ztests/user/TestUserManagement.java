package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.admin.user.DeleteUserPage;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.net.URL;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_EMAIL;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Functional test case for user management pages.
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUserManagement extends BaseJiraFuncTest {
    public static final String JIRA_DEVELOPERS_GROUP_NAME = "jira-developers";
    public static final String JIRA_ADMINISTRATORS_GROUP_NAME = "jira-administrators";
    private static final String DUPLICATE_GROUP_NAME = "duplicate_group";

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        backdoor.darkFeatures().enableForSite(DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG);
    }

    @Test
    @RestoreBlankInstance
    public void testUserManagement() {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        if (tester.getDialog().isLinkPresentWithText(BOB_USERNAME)) {
            backdoor.usersAndGroups().deleteUser(BOB_USERNAME);
        }
        //the following tests are dependant on each other. (eg. all require user Bob created from createUser()) 
        createUser();
        createValidGroup();
        createInvalidUsers();
        createInvalidGroups();
        addUserToGroup();
        loginWithNewUser();
        removeUserFromGroup();
        setUserPassword();
        deleteUser();
        loginWithInvalidUser();
    }

    public void createInvalidUsers() {
        logger.log("Testing User Creation Validation");
        addUser("", BOB_PASSWORD, "No Username", BOB_EMAIL);
        tester.assertTextPresent("You must specify a username.");

        addUser(BOB_USERNAME, BOB_PASSWORD, "duplicate_user", BOB_EMAIL);
        addUser(BOB_USERNAME, BOB_PASSWORD, "duplicate_user", BOB_EMAIL);
        tester.assertTextPresent("A user with that username already exists.");

        addUser(BOB_USERNAME, BOB_PASSWORD, BOB_FULLNAME, "");
        tester.assertTextPresent("You must specify an email address.");
        addUser(BOB_USERNAME, BOB_PASSWORD, BOB_FULLNAME, "asf.com");
        tester.assertTextPresent("You must specify a valid email address.");
    }

    //temporary until we can delete addUser from JIRAWebTest
    @SuppressWarnings("Duplicates")
    public void addUser(final String username, final String password, final String fullname, final String emailAddress) {
        logger.log("Creating User " + username);
        navigation.gotoAdminSection(Navigation.AdminSection.CREATE_USER);
        tester.setFormElement("username", username);
        tester.setFormElement("password", password);
        tester.setFormElement("fullname", fullname);
        tester.setFormElement("email", emailAddress);
        tester.submit("Create");
    }

    public void createInvalidGroups() {
        logger.log("Testing Group Creation Validation");
        //create group with already existing group name
        backdoor.usersAndGroups().addGroup(DUPLICATE_GROUP_NAME);
        addGroup(DUPLICATE_GROUP_NAME);
        tester.assertTextPresent("A group or user with this name already exists.");
        backdoor.usersAndGroups().deleteGroup(DUPLICATE_GROUP_NAME);
    }

    private void addGroup(final String groupName) {
        navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
        tester.setFormElement("addName", groupName);
        tester.submit();
    }

    public void createUser() {
        administration.usersAndGroups().addUser(BOB_USERNAME, BOB_PASSWORD, BOB_FULLNAME, BOB_EMAIL);
        tester.assertTextPresent("User: " + BOB_FULLNAME);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Username:", BOB_USERNAME);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Email", BOB_EMAIL);
    }

    public void createValidGroup() {
        administration.usersAndGroups().addGroup("Valid Group");
        administration.usersAndGroups().deleteGroup("Valid Group");
    }

    public void addUserToGroup() {
        administration.usersAndGroups().addUserToGroup(BOB_USERNAME, JIRA_DEVELOPERS_GROUP_NAME);
        administration.usersAndGroups().addUserToGroup(BOB_USERNAME, JIRA_ADMINISTRATORS_GROUP_NAME);
    }

    public void loginWithNewUser() {
        // log out from default login fired in setUp()
        navigation.logout();
        navigation.login(BOB_USERNAME, BOB_PASSWORD);
        final URL url = tester.getDialog().getResponse().getURL();
        assertEquals("Invalid context", getEnvironmentData().getContext() + "/", url.getPath());
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    public void removeUserFromGroup() {
        administration.usersAndGroups().removeUserFromGroup(BOB_USERNAME, JIRA_ADMINISTRATORS_GROUP_NAME);
    }

    public void setUserPassword() {
        final String NEW_PASSWORD = "new";
        final String DIFFERENT_PASSWORD = "diff";

        //check set user password validation
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.clickLink(BOB_USERNAME);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "User:", BOB_FULLNAME);
        tester.clickLinkWithText("Set Password");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Set Password:", BOB_FULLNAME);

        //error validation case 1 (empty input)
        tester.setFormElement("password", "");
        tester.setFormElement("confirm", "");
        tester.submit("Update");
        tester.assertTextPresent("You must specify a password");

        //error validation case 2 (only one of the fields entered)
        tester.setFormElement("password", "");
        tester.setFormElement("confirm", NEW_PASSWORD);
        tester.submit("Update");
        tester.assertTextPresent("You must specify a password");
        tester.setFormElement("password", NEW_PASSWORD);
        tester.setFormElement("confirm", "");
        tester.submit("Update");
        tester.assertTextPresent("The two passwords entered do not match.");

        //error validation case 3 (mismatching password)
        tester.setFormElement("password", NEW_PASSWORD);
        tester.setFormElement("confirm", DIFFERENT_PASSWORD);
        tester.submit("Update");
        tester.assertTextPresent("The two passwords entered do not match.");

        //successful validation and change
        tester.setFormElement("password", NEW_PASSWORD);
        tester.setFormElement("confirm", NEW_PASSWORD);
        tester.submit("Update");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Password for user " + BOB_USERNAME + " has successfully been set", BOB_FULLNAME);

        //check that the new password has been set for the user (ie. logout and login with the user)
        navigation.logout();
        navigation.loginAttempt(BOB_USERNAME, BOB_PASSWORD);
        tester.assertTextPresent("Sorry, your username and password are incorrect - please try again.");
        navigation.login(BOB_USERNAME, NEW_PASSWORD);

        assertEquals(BOB_FULLNAME, navigation.userProfile().userName());

        //log back in as admin for subsequent tests
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }


    @Test
    @Restore("TestUserManagement.xml")
    public void testDeleteUserProjectLead() {
        final DeleteUserPage deleteUserPage = navigation.gotoPageWithParams(DeleteUserPage.class, DeleteUserPage.generateDeleteQueryParameters("detkin"));
        assertThat(deleteUserPage.getUserDeletionError(), equalTo(deleteUserPage.getUserCannotBeDeleteMessage("detkin")));
        textAssertions.assertTextPresent(deleteUserPage.getProjectLink(), "Another Project");
        textAssertions.assertTextPresent(deleteUserPage.getProjectLink(), "Project 3");
    }

    @Test
    @Restore("TestUserManagementComponentLead.xml")
    public void testDeleteUserComponentLead() {

        navigateToUser("detkin");
        tester.clickLink("deleteuser_link");

        // there are six components in the imported XML file
        final int NUMBER_OF_COMPONENTS = 6;
        int count = 0;
        for (int i = 1; i <= NUMBER_OF_COMPONENTS; i++) {
            try {
                tester.assertLinkPresentWithText("comp " + i);
                count++;
            } catch (final AssertionError e) {
                // do nothing, not all components are shown and the order is not guaranteed
            }
        }

        // number of displayed components is same
        assertTrue(count == NUMBER_OF_COMPONENTS);

        tester.assertSubmitButtonPresent("Delete");
    }

    private void deleteUser() {
        logger.log("Deleting User " + BOB_USERNAME);
        navigateToUser(BOB_USERNAME);
        tester.clickLink("deleteuser_link");
        tester.submit("Delete");
        tester.assertTextNotPresent(BOB_USERNAME);
        tester.assertTextPresent("UserBrowser");
        tester.assertTextPresent("Displaying users");

        // Restore user Bob for later use
        administration.usersAndGroups().addUser(BOB_USERNAME, BOB_PASSWORD, BOB_FULLNAME, BOB_EMAIL);
    }

    private void navigateToUser(final String username) {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.clickLink(username);
    }

    private void loginWithInvalidUser() {
        // log out from default login fired in setUp()
        navigation.logout();
        navigation.loginAttempt(BOB_USERNAME, null);
        tester.assertTextPresent("Sorry, your username and password are incorrect - please try again.");
    }
}
