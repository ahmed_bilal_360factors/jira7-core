package com.atlassian.jira.webtests.ztests.ao;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.client.apache.ApacheHttpClient;
import com.sun.jersey.client.apache.config.ApacheHttpClientConfig;
import com.sun.jersey.client.apache.config.DefaultApacheHttpClientConfig;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.core.Is.is;

/**
 * Simple sanity test for AO.
 *
 * @since v7.0
 */
@WebTest({Category.FUNC_TEST, Category.PLUGINS, Category.ACTIVE_OBJECTS, Category.REFERENCE_PLUGIN})
@LoginAs(user = ADMIN_USERNAME)
public class TestActiveObjectsTransactions extends BaseJiraFuncTest {
    private static final GenericType<List<String>> REF_ENTITY_LIST = new GenericType<List<String>>() {
    };

    private Client client;
    private String ref1 = "Entity 1";
    private String ref2 = "Entity 2";

    @Before
    public void setUpTest() {
        ApacheHttpClientConfig config = new DefaultApacheHttpClientConfig();
        config.getProperties().put(ApacheHttpClientConfig.PROPERTY_PREEMPTIVE_AUTHENTICATION, Boolean.TRUE);
        config.getState().setCredentials(null, null, -1, "admin", "admin");
        config.getClasses().add(JacksonJaxbJsonProvider.class);

        client = ApacheHttpClient.create(config);
    }

    @After
    public void tearDownTest() {
        client.destroy();
    }

    @Test
    public void testAdd() throws Exception {
        final WebResource resource = createResource();
        deleteAll(resource);

        ClientResponse response = resource.queryParam("description", ref1).type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);
        Assert.assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response.getStatus());

        List<String> refs = resource.get(REF_ENTITY_LIST);
        assertThat(refs.size(), is(1));
        assertThat(refs, contains(ref1));
    }

    @Test
    public void testAddInTransaction() throws Exception {
        final WebResource resource = createResource();
        deleteAll(resource);

        ClientResponse response = resource.path("withTransaction").queryParam("description", ref1).type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);
        Assert.assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response.getStatus());

        List<String> refs = resource.get(REF_ENTITY_LIST);
        assertThat(refs.size(), is(1));
        assertThat(refs, contains(ref1));
    }

    @Test
    public void testAddInExternalTransaction() throws Exception {
        final WebResource resource = createResource();
        deleteAll(resource);

        ClientResponse response = resource.path("withExternalTransaction").queryParam("description", ref1).type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);
        Assert.assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response.getStatus());

        List<String> refs = resource.get(REF_ENTITY_LIST);
        assertThat(refs.size(), is(1));
        assertThat(refs, contains(ref1));

        deleteAll(resource);

        response = resource.path("withExternalTransaction").queryParam("description", ref1).queryParam("description2", ref2).type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);
        Assert.assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response.getStatus());

        refs = resource.get(REF_ENTITY_LIST);
        assertThat(refs.size(), is(2));
        assertThat(refs, contains(ref1, ref2));
    }

    @Test
    public void testRollbackExternal() throws Exception {
        final WebResource resource = createResource();
        deleteAll(resource);

        ClientResponse response = resource.path("butRollbackExternal").queryParam("description", ref1).type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);
        Assert.assertEquals(Response.Status.NOT_MODIFIED.getStatusCode(), response.getStatus());

        List<String> refs = resource.get(REF_ENTITY_LIST);
        Assert.assertEquals(0, refs.size());
    }

    private void deleteAll(WebResource resource) {
        final ClientResponse deleteResponse = resource.delete(ClientResponse.class);
        Assert.assertEquals(Response.Status.NO_CONTENT.getStatusCode(), deleteResponse.getStatus());
    }

    private WebResource createResource() {
        return client.resource(environmentData.getBaseUrl().toExternalForm()).path("rest")
                .path("reference-plugin").path("latest").path("refentity");
    }
}
