package com.atlassian.jira.webtests.ztests.database;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.backdoor.EntityEngineControl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Test that OfBiz handles queries with large 'IN' portions.  e.g. ... where project.id in (1, 2, 3...).
 * Certain databases don't cope well with these sorts of queries and require workarounds.
 *
 * @since 6.4
 */
@WebTest({Category.FUNC_TEST, Category.REST, Category.DATABASE})
@RestoreBlankInstance
public class TestDatabaseLargeInQueries extends BaseJiraFuncTest {
    /**
     * Test with a large number of 'IN' values of numeric type.
     */
    @Test
    public void testLargeInQueryNumeric() {
        List<Long> values = new ArrayList<Long>();
        for (long value = 8000L; value < 50001L; value++) {
            values.add(value);
        }

        List<Map<String, String>> results = backdoor.entityEngine().findByValueList("Project", "id", values,
                ImmutableList.of("id", "name"), EntityEngineControl.ValueType.NUMBER);
        assertThat(
                results,
                containsInAnyOrder(
                        ImmutableMap.of("id", 10000, "name", "homosapien"),
                        ImmutableMap.of("id", 10001, "name", "monkey")
                )
        );
    }

    /**
     * Test with a large number of 'IN' values of string type.
     */
    @Test
    public void testLargeInQueryString() {
        List<String> values = new ArrayList<String>();
        values.add("homosapien");
        for (int i = 0; i < 42050; i++) {
            values.add(String.valueOf(i));
        }

        List<Map<String, String>> results = backdoor.entityEngine().findByValueList("Project", "name", values,
                ImmutableList.of("id", "name"), EntityEngineControl.ValueType.STRING);
        assertEquals(ImmutableList.of(ImmutableMap.of("id", 10000, "name", "homosapien")), results);
    }

    /**
     * The IN(...) fragments each have less than 2000 parameters but in total there are more than 2000 parameters.
     */
    @Test
    public void testLargeNumberOfParametersWithSmallerInFragments() {
        List<String> values = new ArrayList<String>();
        values.add("homosapien");
        for (int i = 0; i < 1200; i++) {
            values.add(String.valueOf(i));
        }

        List<Map<String, String>> results = backdoor.entityEngine().findByValueList2Fields("Project", "lead", "name", values,
                ImmutableList.of("id", "name"), EntityEngineControl.ValueType.STRING);
        assertEquals(ImmutableList.of(ImmutableMap.of("id", 10000, "name", "homosapien")), results);
    }
}
