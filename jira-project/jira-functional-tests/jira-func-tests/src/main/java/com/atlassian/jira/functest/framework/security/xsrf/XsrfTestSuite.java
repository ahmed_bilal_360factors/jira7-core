package com.atlassian.jira.functest.framework.security.xsrf;

import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.Navigation;
import com.google.common.collect.ImmutableList;
import net.sourceforge.jwebunit.WebTester;

import java.util.Collection;
import java.util.List;

/**
 * XsrfCheck test suite class that runs multiple checks in sequence.
 *
 * @since v4.1
 */
public class XsrfTestSuite {
    private final List<XsrfCheck> checks;

    public XsrfTestSuite(final Collection<XsrfCheck> checks) {
        if (checks == null || checks.isEmpty()) {
            throw new IllegalArgumentException("Please pass in at least one XsrfCheck");
        }
        this.checks = ImmutableList.copyOf(checks);
    }

    public XsrfTestSuite(final XsrfCheck... checks) {
        if (checks == null || checks.length == 0) {
            throw new IllegalArgumentException("Please pass in at least one XsrfCheck");
        }
        this.checks = ImmutableList.copyOf(checks);
    }

    public void run(final WebTester tester, final Navigation navigation, final Form form) throws Exception {
        for (final XsrfCheck check : checks) {
            check.init(tester, navigation, form);
            check.run();
        }
    }

    public void run(final WebTester tester, final Navigation navigation, final Form form,
                    final String xsrfError) throws Exception {
        for (final XsrfCheck check : checks) {
            check.init(tester, navigation, form);
            check.run(xsrfError);
        }
    }
}
