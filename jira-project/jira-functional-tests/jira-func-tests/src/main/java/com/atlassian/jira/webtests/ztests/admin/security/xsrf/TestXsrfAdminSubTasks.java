package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck.LinkWithTextSubmission;

/**
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestXsrfAdminSubTasks extends BaseJiraFuncTest {
    @Inject
    private Form form;

    @Test
    public void testSubTasks() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck("SubTask Enablement", new XsrfCheck.Setup() {
                    public void setup() {
                        gotoSubTasks();
                    }
                }, new LinkWithTextSubmission("Enable"))
                ,
                new XsrfCheck("Edit Sub Tasks", new XsrfCheck.Setup() {
                    public void setup() {
                        gotoSubTasks();
                        tester.clickLink("edit_Sub-task");
                    }
                }, new XsrfCheck.FormSubmission("Update"))
                ,
                new XsrfCheck("Add Sub Task Type", new XsrfCheck.Setup() {
                    public void setup() {
                        gotoSubTasks();
                        tester.clickLink("add-subtask-type");
                        tester.setFormElement("name", "name");
                        tester.setFormElement("description", "desc");
                    }
                },
                        new XsrfCheck.FormSubmission("Add")),

                new XsrfCheck("Delete Sub Task Type", new XsrfCheck.Setup() {
                    public void setup() {
                        gotoSubTasks();
                        tester.clickLinkWithText("Delete");
                    }
                }, new XsrfCheck.FormSubmission("Delete"))
                ,

                new XsrfCheck("Disable Sub Tasks", new XsrfCheck.Setup() {
                    public void setup() {
                        gotoSubTasks();
                    }
                }, new XsrfCheck.LinkWithIdSubmission("disable_subtasks"))

        ).run(tester, navigation, form);
    }

    private void gotoSubTasks() {
        navigation.gotoAdminSection(Navigation.AdminSection.SUBTASKS);
    }

}