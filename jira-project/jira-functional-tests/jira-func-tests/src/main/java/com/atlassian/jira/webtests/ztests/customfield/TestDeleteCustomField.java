package com.atlassian.jira.webtests.ztests.customfield;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Func test for Deleting Custom Fields.
 * Originally written for JRA-13904.
 *
 * @since v3.13
 */
@WebTest({Category.FUNC_TEST, Category.CUSTOM_FIELDS, Category.FIELDS})
@LoginAs(user = ADMIN_USERNAME)
public class TestDeleteCustomField extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("TestDeleteCustomField.xml");
    }

    /**
     * This test makes sure that if a Custom field is used in a notification schem, then deleting the Custom Field
     * will also delete the notification entity.
     */
    @Test
    public void testRemoveCustomFieldFromNotifications() {
        // Go to notification_schemes
        navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);
        tester.clickLink("10000_edit");
        textAssertions.assertTextPresent(locator.page(), "(My Best Group)");
        textAssertions.assertTextPresent(locator.page(), "(My Friend)");

        // Delete the Group picker Custom Field
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        tester.clickLink("del_customfield_10041");
        tester.submit("Delete");

        navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);
        tester.clickLink("10000_edit");

        // Assert group picker is gone, and the user picker is still there.
        textAssertions.assertTextPresent(locator.page(), "(My Friend)");
        textAssertions.assertTextNotPresent(locator.page(), "(My Best Group)");

        // Delete the user picker Custom Field
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        tester.clickLink("del_customfield_10040");
        tester.submit("Delete");

        navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);
        tester.clickLink("10000_edit");

        // Assert user picker gone.
        textAssertions.assertTextNotPresent(locator.page(), "(My Friend)");
        textAssertions.assertTextNotPresent(locator.page(), "(My Best Group)");
    }
}
