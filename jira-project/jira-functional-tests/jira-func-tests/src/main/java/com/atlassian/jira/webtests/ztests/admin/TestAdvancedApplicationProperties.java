package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.admin.AdvancedApplicationProperties;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @since v4.4.4
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION})
@LoginAs(user = ADMIN_USERNAME)
public class TestAdvancedApplicationProperties extends BaseJiraFuncTest {
    private static final String SUBTASK_KEY = "jira.table.cols.subtasks";
    private static final String SUBTASK_COLUMN = "customfield_10000";


    @Inject
    private Administration administration;

    @Test
    public void testAdvancedSettings() {
        administration.restoreBlankInstance();
        AdvancedApplicationProperties properties = administration.advancedApplicationProperties();
        assertTrue(properties.getApplicationProperties().size() > 0);
    }

    @Test
    public void testSubTaskColumnsAreVisibleWithoutRestart() {
        administration.restoreData("TestAdvancedApplicationProperties.xml");
        AdvancedApplicationProperties properties = administration.advancedApplicationProperties();
        String subtaskColumns = properties.getApplicationProperty(SUBTASK_KEY);
        subtaskColumns += String.format(", %s", SUBTASK_COLUMN);
        properties.setApplicationProperty(SUBTASK_KEY, subtaskColumns);
        navigation.issue().viewIssue("HSP-1");
        getAssertions().assertNodeHasText(new CssLocator(tester, "." + SUBTASK_COLUMN), "You should see this in the subtask column view.");
    }

    @Test
    public void testJiraCharacterLimitDefaultValue() {
        administration.restoreBlankInstance();
        final String defaultLimit = administration.advancedApplicationProperties().getApplicationProperty(APKeys.JIRA_TEXT_FIELD_CHARACTER_LIMIT);
        assertThat(defaultLimit, is("32767"));
    }

}
