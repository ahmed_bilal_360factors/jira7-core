package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.REFERENCE_PLUGIN})
@RestoreBlankInstance
@LoginAs(user = FunctTestConstants.ADMIN_USERNAME)
public class TestAdminMenuLinks extends BaseJiraFuncTest {

    @Test
    public void testClickingLinkKeepsAdminContext() {
        navigation.gotoAdminSection(Navigation.AdminSection.GENERAL_CONFIGURATION);
        isAdminTab("System");

        tester.clickLink("reference-admin-servlet-link-from-system-tab");
        isAdminTab("System");
        isLinkSelected("Reference Admin Servlet System tab");

        navigation.gotoAdminSection(Navigation.AdminSection.FIND_MORE_ADMIN_TOOLS);

        tester.clickLink("reference-admin-servlet-link-from-add-on-tab");
        isAdminTab("Add-ons");
        isLinkSelected("Reference Admin Servlet Add-on tab");
    }

    @Test
    public void testProjectAdminDoesNotHaveNavigationMenu() {
        goToProjectAdministrationSummary(FunctTestConstants.PROJECT_HOMOSAP_KEY);
        // this is a now deprecated section, so it should not be present
        assertions.assertNodeByIdDoesNotExist("admin-nav-heading");
    }

    public void goToProjectAdministrationSummary(final String projectKey) {
        navigation.gotoPage("plugins/servlet/project-config/" + projectKey + "/summary");
    }

    private void isAdminTab(final String tab) {
        assertions.assertNodeHasText(new CssLocator(tester, ".aui-nav-selected"), tab);
    }

    private void isLinkSelected(final String link) {
        assertions.assertNodeHasText(new CssLocator(tester, ".aui-nav-selected"), link);
    }
}
