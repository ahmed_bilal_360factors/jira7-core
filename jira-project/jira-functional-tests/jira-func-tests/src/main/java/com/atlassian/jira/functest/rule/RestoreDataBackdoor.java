package com.atlassian.jira.functest.rule;

import com.atlassian.jira.functest.framework.BackdoorModule;
import com.atlassian.jira.functest.framework.FuncTestWebClientListener;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * Rule that handles {@link com.atlassian.integrationtesting.runner.restore.RestoreOnce} for class and provides backdoor
 * implementation
 *
 * @since v7.1
 */
public class RestoreDataBackdoor implements TestRule {
    final BackdoorModule backdoor = new BackdoorModule();
    private final RuleChain innerChain;

    public RestoreDataBackdoor() {
        innerChain = RuleChain.emptyRuleChain()
                .around(new EnsureJiraSetupRule(backdoor, backdoor::getEnvironmentData, () -> {
                    //we need to initialize jwebunit just to click trough setup process
                    HttpUnitConfigurationRule.restoreDefaults();
                    return new WebTesterRule(backdoor::getEnvironmentData, FuncTestWebClientListener::new).get();
                }))
                .around(new RestoreDataOnlyOnce(backdoor::getBackdoor));
    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return innerChain.apply(base, description);
    }

    public Backdoor getBackdoor() {
        return backdoor.getBackdoor();
    }

    public JIRAEnvironmentData getEnvironmentData() {
        return backdoor.getEnvironmentData();
    }
}
