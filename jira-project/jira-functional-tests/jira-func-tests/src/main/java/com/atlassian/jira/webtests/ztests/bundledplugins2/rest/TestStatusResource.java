package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.FuncTestUrlHelper;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.testkit.client.restclient.Status;
import com.atlassian.jira.testkit.client.restclient.StatusClient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;

/**
 * Func tests for StatusResource.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@Restore("TestStatusResource.xml")
public class TestStatusResource extends BaseJiraRestTest {
    /**
     * The id of a status that is visible by admin, but not by fred.
     */
    private static final String STATUS_ID = "10000";
    private static final String STATUS_NAME = "Insane";
    private static final String STATUS_NAME_TRANSLATED = "NotStarted";

    private StatusClient statusClient;

    @Inject
    private FuncTestUrlHelper urlHelper;

    @Before
    public void setUpTest() {
        statusClient = new StatusClient(environmentData);
    }

    /**
     * Verifies that the user is able to retrieve a visible status.
     *
     * @throws Exception if anything goes wrong
     */
    @Test
    public void testStatusReturned() throws Exception {
        // this is what we expect:
        //
        // {
        //   "self": "http://localhost:8090/jira/rest/api/2/status/10000",
        //   "description": "Custom status",
        //   "iconUrl": "http://localhost:8090/jira/images/icons/statuses/generic.png",
        //   "name": "Insane"
        // }

        Status status = statusClient.get(STATUS_ID);
        Assert.assertEquals(urlHelper.getBaseUrlPlus("rest/api/2/status/" + STATUS_ID), status.self());
        Assert.assertEquals("Custom status", status.description());
        Assert.assertEquals(urlHelper.getBaseUrlPlus("images/icons/statuses/generic.png"), status.iconUrl());
        Assert.assertEquals("Insane", status.name());
        Assert.assertEquals(STATUS_ID, status.id());

        // And test also by name
        status = statusClient.get(STATUS_NAME);
        Assert.assertEquals(urlHelper.getBaseUrlPlus("rest/api/2/status/" + STATUS_ID), status.self());
        Assert.assertEquals("Custom status", status.description());
        Assert.assertEquals(urlHelper.getBaseUrlPlus("images/icons/statuses/generic.png"), status.iconUrl());
        Assert.assertEquals("Insane", status.name());
        Assert.assertEquals(STATUS_ID, status.id());

        // And test also by translated name
        status = statusClient.get(STATUS_NAME_TRANSLATED);
        Assert.assertEquals(urlHelper.getBaseUrlPlus("rest/api/2/status/" + "1"), status.self());
        Assert.assertEquals("Translated version of Open", status.description());
        Assert.assertEquals(urlHelper.getBaseUrlPlus("images/icons/statuses/open.png"), status.iconUrl());
        Assert.assertEquals("NotStarted", status.name());
        Assert.assertEquals("1", status.id());

        // And test also by untranslated name where there is a translation present
        status = statusClient.get("Open");
        Assert.assertEquals(urlHelper.getBaseUrlPlus("rest/api/2/status/" + "1"), status.self());
        Assert.assertEquals("Translated version of Open", status.description());
        Assert.assertEquals(urlHelper.getBaseUrlPlus("images/icons/statuses/open.png"), status.iconUrl());
        Assert.assertEquals("NotStarted", status.name());
        Assert.assertEquals("1", status.id());
    }

    /**
     * Get all statuses
     *
     * @throws Exception if anything goes wrong
     */
    @Test
    public void testAllStatuses() throws Exception {
        List<Status> statuses = statusClient.get();
        Assert.assertEquals(6, statuses.size());
        assertStatusesContain(statuses, "1");
        assertStatusesContain(statuses, "3");
        assertStatusesContain(statuses, "4");
        assertStatusesContain(statuses, "5");
        assertStatusesContain(statuses, "6");
        assertStatusesContain(statuses, "10000");
    }

    private void assertStatusesContain(List<Status> statuses, String id) {
        for (Status status : statuses) {
            if (status.id().equals(id)) {
                return;
            }
        }
        Assert.fail("Status " + id + " not in list");
    }


    /**
     * Verifies that the user is not able to see a status that is not active on any of his projects.
     *
     * @throws Exception if anything goes wrong
     */
    @Test
    public void testStatusFilteredByPermissions() throws Exception {
        Response response = statusClient.loginAs(FRED_USERNAME).getResponse(STATUS_ID);
        Assert.assertEquals(404, response.statusCode);

        response = statusClient.loginAs(FRED_USERNAME).getResponse(STATUS_NAME);
        Assert.assertEquals(404, response.statusCode);

        List<Status> statuses = statusClient.get();
        Assert.assertEquals(5, statuses.size());
        assertStatusesContain(statuses, "1");
        assertStatusesContain(statuses, "3");
        assertStatusesContain(statuses, "4");
        assertStatusesContain(statuses, "5");
        assertStatusesContain(statuses, "6");
    }

    @Test
    public void testStatusDoesntExist() throws Exception {
        // {"errorMessages":["The status with id '123' does not exist"],"errors":[]}
        Response resp123 = statusClient.getResponse("123");
        Assert.assertEquals(404, resp123.statusCode);
        Assert.assertEquals("The status with id '123' does not exist", resp123.entity.errorMessages.get(0));

        // {"errorMessages":["The status with id 'abc' does not exist"],"errors":[]}
        Response respAbc = statusClient.getResponse("abc");
        Assert.assertEquals(404, respAbc.statusCode);
        Assert.assertEquals("The status with id 'abc' does not exist", respAbc.entity.errorMessages.get(0));
    }
}
