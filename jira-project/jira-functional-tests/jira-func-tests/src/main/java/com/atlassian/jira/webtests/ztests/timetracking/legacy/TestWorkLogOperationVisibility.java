package com.atlassian.jira.webtests.ztests.timetracking.legacy;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.TIME_TRACKING, Category.WORKLOGS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestWorkLogOperationVisibility extends BaseJiraFuncTest {
    private static final String BUG = "HSP-1";
    private static final String NEW_FEATURE = "HSP-2";

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        // restore a backup that hides time tracking field for all issue types except bugs in Homosapien project
        administration.restoreData("TestWorkLogVisibility.xml");
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
    }

    @Test
    public void testWorkLogOperationVisibility() throws Exception {
        navigation.issue().viewIssue(BUG);
        assertLogWorkOperationPresent();

        navigation.issue().viewIssue(NEW_FEATURE);
        assertLogWorkOperationNotPresent();

        flipVisibilityInFieldConfigEnterprise();

        navigation.issue().viewIssue(BUG);
        assertLogWorkOperationNotPresent();

        navigation.issue().viewIssue(NEW_FEATURE);
        assertLogWorkOperationPresent();
    }

    @Test
    public void testCreateWorkActionVisibility() throws Exception {
        navigation.issue().viewIssue(BUG);

        assertLogWorkOperationPresent();
        getTester().clickLink("log-work");
        assertWorkLogFormPresent();

        // go to new feature HSP-2 create worklog directly as there is no link to click
        getTester().gotoPage("/secure/CreateWorklog!default.jspa?id=10010");
        assertWorkLogFormNotPresent();

        // go to new feature HSP-2 create worklog directly as there is no link to click
        getTester().gotoPage(page.addXsrfToken("/secure/CreateWorklog.jspa?id=10010&timeLogged=1h&startDate=26/Sep/07%2005:41%20PM"));
        assertWorkLogFormNotPresent();

        flipVisibilityInFieldConfigEnterprise();

        // go to bug HSP-1 create worklog directly as there is no link to click
        getTester().gotoPage("/secure/CreateWorklog!default.jspa?id=10000");
        assertWorkLogFormNotPresent();

        // go to bug HSP-1 create worklog directly as there is no link to click
        getTester().gotoPage(page.addXsrfToken("/secure/CreateWorklog.jspa?id=10000&timeLogged=1h&startDate=26/Sep/07%2005:41%20PM"));
        assertWorkLogFormNotPresent();

        navigation.issue().viewIssue(NEW_FEATURE);

        assertLogWorkOperationPresent();
        getTester().clickLink("log-work");
        assertWorkLogFormPresent();
    }

    private void flipVisibilityInFieldConfigEnterprise() {
        navigation.gotoAdminSection(Navigation.AdminSection.FIELD_CONFIGURATION);
        getTester().clickLink("configure-Default Field Configuration");
        getTester().clickLink("show_18"); // show time tracking
        getTester().clickLink("view_fieldlayouts");
        getTester().clickLink("configure-Bug Field Configuration");
        getTester().clickLink("hide_18");   // hide time tracking
    }

    private void assertWorkLogFormPresent() {
        textAssertions.assertTextPresent("Log work");
        textAssertions.assertTextNotPresent("Access Denied");
        textAssertions.assertTextNotPresent("It seems that you have tried to perform an operation which you are not permitted to perform.");
    }

    private void assertWorkLogFormNotPresent() {
        textAssertions.assertTextNotPresent("This form allows you to log work that you have done on this issue.");
        textAssertions.assertTextPresent("It seems that you have tried to perform an operation which you are not permitted to perform.");
    }

    private void assertLogWorkOperationNotPresent() {
        getTester().assertLinkNotPresent("log-work");
        getTester().assertLinkNotPresentWithText("Log work");
    }

    private void assertLogWorkOperationPresent() {
        getTester().assertLinkPresent("log-work");
        getTester().assertLinkPresentWithText("Log work");
    }
}
