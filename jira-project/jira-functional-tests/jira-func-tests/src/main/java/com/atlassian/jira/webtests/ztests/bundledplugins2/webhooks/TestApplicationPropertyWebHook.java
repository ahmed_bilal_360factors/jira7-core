package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.atlassian.jira.testkit.client.util.JsonMatchers.hasField;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.WEBHOOKS, Category.REFERENCE_PLUGIN})
public class TestApplicationPropertyWebHook extends AbstractWebHookTest {
    private static final List<WebHookPropertyTest> webHookPropertyTests = ImmutableList.of(
            new WebHookPropertyTest("option_voting_changed", APKeys.JIRA_OPTION_VOTING),
            new WebHookPropertyTest("option_unassigned_issues_changed", APKeys.JIRA_OPTION_ALLOWUNASSIGNED),
            new WebHookPropertyTest("option_subtasks_changed", APKeys.JIRA_OPTION_ALLOWSUBTASKS),
            new WebHookPropertyTest("option_attachments_changed", APKeys.JIRA_OPTION_ALLOWATTACHMENTS),
            new WebHookPropertyTest("option_issuelinks_changed", APKeys.JIRA_OPTION_ISSUELINKING),
            new WebHookPropertyTest("option_timetracking_changed", APKeys.JIRA_OPTION_TIMETRACKING));

    @Before
    @Override
    public void setUpTest() {
        super.setUpTest();
        backdoor.restoreBlankInstance();
    }

    @Test
    public void testWebHookSentToPersistentListenerOnApplicationPropertyChange() throws JSONException {
        for (WebHookPropertyTest webHookPropertyTest : webHookPropertyTests) {
            raiseAndCheckEvent(webHookPropertyTest, receivedByPersistentListener);
        }
    }

    @Test
    public void testWebHookSentToModuleDescriptorListenerOnApplicationPropertyChange() throws JSONException {
        for (WebHookPropertyTest webHookPropertyTest : webHookPropertyTests) {
            raiseAndCheckEvent(webHookPropertyTest, receivedByModuleDescriptorListener(webHookPropertyTest.webHookId));
        }
    }

    private void raiseAndCheckEvent(final WebHookPropertyTest webHookPropertyTest, final Supplier<String> listener)
            throws JSONException {
        registerWebHook(webHookPropertyTest.getWebHookId());
        final ExpectedValues expectedValues = changeProperty(webHookPropertyTest.getOptionKey());
        assertApplicationPropertyEventReceived(webHookPropertyTest.getWebHookId(), listener, expectedValues);
    }

    private void assertApplicationPropertyEventReceived(final String webHookEvent,
                                                        final Supplier<String> receivedWebhook,
                                                        final ExpectedValues expectedValues)
            throws JSONException {
        JSONObject json = new JSONObject(receivedWebhook.get());

        assertThat(json, hasField("timestamp"));
        assertThat(json, hasField("webhookEvent").equalTo(webHookEvent));
        assertThat(json, hasField("property.key").equalTo(expectedValues.key));
        assertThat(json, hasField("property.value").equalTo(expectedValues.value));
    }

    private ExpectedValues changeProperty(final String property) {
        final boolean option = backdoor.applicationProperties().getOption(property);
        backdoor.applicationProperties().setOption(property, !option);
        return new ExpectedValues(property, !option);
    }

    private static class ExpectedValues {
        private final String key;
        private final String value;

        private ExpectedValues(final String key, final boolean value) {
            this.key = key;
            this.value = String.valueOf(value);
        }
    }

    private static class WebHookPropertyTest {
        private final String webHookId;
        private final String optionKey;

        private WebHookPropertyTest(final String webHookId, final String optionKey) {
            this.webHookId = webHookId;
            this.optionKey = optionKey;
        }

        public String getWebHookId() {
            return webHookId;
        }

        public String getOptionKey() {
            return optionKey;
        }
    }
}
