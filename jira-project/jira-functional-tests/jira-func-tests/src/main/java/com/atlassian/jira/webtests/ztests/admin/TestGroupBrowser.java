package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.LicenseKeys;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.GROUP_LABEL_ADMIN;
import static com.atlassian.jira.functest.framework.FunctTestConstants.GROUP_LABEL_FUNC_TEST;
import static com.atlassian.jira.functest.framework.FunctTestConstants.GROUP_LABEL_JIRA_CORE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.GROUP_LABEL_MULTI;

/**
 * Test related to the admin Group Browser page
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.USERS_AND_GROUPS})
public class TestGroupBrowser extends BaseJiraFuncTest {
    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        backdoor.restoreBlankInstance();
        navigation.login(ADMIN_USERNAME);
    }

    @Test
    public void testGroupPagingWorks() {
        addSomeGroups();

        navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);

        assertGroupNamesIsPresent(0, 19);
        assertNextOnlyIsPresent();

        clickNext();
        assertGroupNamesIsPresent(20, 39);
        assertNextAndPrevOnlyIsPresent();

        clickNext();
        assertGroupNamesIsPresent(40, 49);
        assertPrevOnlyIsPresent();

        clickPrevious();
        assertGroupNamesIsPresent(20, 39);
        assertNextAndPrevOnlyIsPresent();

        clickPrevious();
        assertGroupNamesIsPresent(0, 19);
        assertNextOnlyIsPresent();
    }

    @Test
    public void testGroupFilteringPagingWorks() {
        addSomeGroups();

        navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);

        assertGroupNamesIsPresent(0, 19);
        assertNextOnlyIsPresent();

        setFilter("03");

        assertGroupNamesIsPresent(3, 3);
        assertGroupNamesIsPresent(30, 39);
        assertNextAndPrevNotPresent();

        // now set a filter that pages
        setFilter("group");

        assertGroupNamesIsPresent(0, 19);
        assertNextOnlyIsPresent();

        clickNext();
        assertGroupNamesIsPresent(20, 39);
        assertNextAndPrevOnlyIsPresent();

        clickNext();
        assertGroupNamesIsPresent(40, 49);
        assertPrevOnlyIsPresent();

        clickPrevious();
        assertGroupNamesIsPresent(20, 39);
        assertNextAndPrevOnlyIsPresent();

        clickPrevious();
        assertGroupNamesIsPresent(0, 19);
        assertNextOnlyIsPresent();
    }

    @Test
    public void testApplicationAccessLozenges() {
        backdoor.license().set(LicenseKeys.CORE_AND_TEST_ROLES);

        backdoor.usersAndGroups().addGroup("jira-core-users");
        backdoor.usersAndGroups().addGroup("jira-test-users");
        backdoor.usersAndGroups().addGroup("multi-app-access");

        backdoor.applicationRoles().putRole("jira-core", "jira-core-users", "jira-administrators", "multi-app-access");
        backdoor.applicationRoles().putRole("jira-func-test", "jira-test-users", "multi-app-access");

        navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);

        assertGroupLabels("jira-administrators", GROUP_LABEL_ADMIN);
        assertGroupLabels("jira-core-users", GROUP_LABEL_JIRA_CORE);
        assertGroupLabels("jira-test-users", GROUP_LABEL_FUNC_TEST);
        assertGroupLabels("multi-app-access", GROUP_LABEL_MULTI);
    }

    private void assertGroupLabels(String groupName, String... labels) {
        final String rowXPath = String.format("//table[@id='group_browser_table']//a[text()='%s']/../..", groupName);
        textAssertions.assertTextSequence(new XPathLocator(tester, rowXPath), groupName, labels);
    }

    private void setFilter(final String filterStr) {
        tester.setFormElement("nameFilter", filterStr);
        tester.submit("filter");
    }

    private void assertPrevOnlyIsPresent() {
        tester.assertTextNotPresent("Next");
        tester.assertTextPresent("Previous");
    }

    private void assertNextAndPrevOnlyIsPresent() {
        tester.assertTextPresent("Next");
        tester.assertTextPresent("Previous");
    }

    private void assertNextAndPrevNotPresent() {
        tester.assertTextNotPresent("Next");
        tester.assertTextNotPresent("Previous");
    }

    private void assertNextOnlyIsPresent() {
        tester.assertTextPresent("Next");
        tester.assertTextNotPresent("Previous");
    }


    private void clickNext() {
        tester.clickLinkWithText("Next >>");
    }

    private void clickPrevious() {
        tester.clickLinkWithText("Previous");
    }

    private void assertGroupNamesIsPresent(final int start, final int end) {
        List<String> listOfGroupNames = new ArrayList<String>();
        for (int i = start; i <= end; i++) {
            listOfGroupNames.add(makeGroupName("group", i));
        }
        final String[] groupNames = listOfGroupNames.toArray(new String[listOfGroupNames.size()]);
        textAssertions.assertTextSequence(new XPathLocator(tester, "//table[@id='group_browser_table']"), groupNames);
    }

    private void addSomeGroups() {
        for (int i = 0; i < 50; i++) {
            backdoor.usersAndGroups().addGroup(makeGroupName("group", i));
        }
    }

    private String makeGroupName(final String userName, final int i) {
        return userName + new DecimalFormat("000").format(i);
    }

    @Test
    public void testAddBlankGroup() {
        navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
        tester.setFormElement("addName", "");
        tester.submit("add_group");

        tester.assertTextPresent("You must specify valid group name.");
    }

    @Test
    public void testAddGroup() {
        navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
        tester.setFormElement("addName", "my-test-group");
        tester.submit("add_group");

        textAssertions.assertTextPresent(new IdLocator(tester, "group_browser_table"), "my-test-group");
    }

}
