package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.sun.jersey.api.client.GenericType;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.stream.Stream;

public class OutgoingMailControl extends BackdoorControl<OutgoingMailControl> {
    public OutgoingMailControl(final JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public void enable() {
        createResource().path("outgoingmail/enable").post();
    }

    public void disable() {
        createResource().path("outgoingmail/disable").post();
    }

    public void clearMessages() {
        createResource().path("outgoingmail/clear-messages").post();
    }

    public Stream<MimeMessage> getMails() {
        return createResource().path("outgoingmail/mails").get(new GenericType<List<String>>() {
        })
                .stream().map(this::parseMessage);
    }

    public String getMessagePreviewURI(MimeMessage message) throws MessagingException {
        return createResource().path("outgoingmail/preview")
                .queryParam("messageId", message.getMessageID())
                .getURI().toString()
                        // remove context path, because it will be applied later
                .replace(rootPath, "");
    }

    private MimeMessage parseMessage(final String rawMessage) {
        try {
            return new MimeMessage((Session) null, new ByteArrayInputStream(rawMessage.getBytes("UTF-8")));
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

}
