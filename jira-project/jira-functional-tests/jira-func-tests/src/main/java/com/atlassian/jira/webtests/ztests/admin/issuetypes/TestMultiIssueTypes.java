package com.atlassian.jira.webtests.ztests.admin.issuetypes;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.webtests.Groups;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BULK_CHANGE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY_KEY;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.ISSUES, Category.ISSUE_TYPES})
@LoginAs(user = ADMIN_USERNAME)
public class TestMultiIssueTypes extends BaseJiraFuncTest {
    private static final String AUTO_CREATED_SCHEME_PREFIX = "New issue type scheme for project ";
    private static final String CREATE_ISSUE_FORM_HEADER_CSS_LOCATOR = "#content > header.aui-page-header h1";
    private static final String VIEW_ISSUE_PAGE_ISSUE_TYPE_LABEL_ID = "type-val";
    @Inject
    private Form form;
    @Inject
    private FuncTestLogger logger;
    @Inject
    private Administration administration;
    @Inject
    private LocatorFactory locator;
    @Inject
    private TextAssertions textAssertions;

    public TestMultiIssueTypes() {
        super();
    }

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
    }

    @After
    public void tearDownTest() {
        administration.restoreBlankInstance();
    }

    @Test
    public void testMultiIssueTypes() throws Exception {
        _testCreateNewIssueTypeScheme();
        _testSameAsProject();
        _testChooseScheme();
    }

    private void _testChooseScheme() {
        logger.log("Testing choosing scheme from a list & that all available projects share the same config");

        Long mkyProjectId = backdoor.project().getProjectId(PROJECT_MONKEY_KEY);
        tester.gotoPage("/secure/admin/SelectIssueTypeSchemeForProject!default.jspa?projectId=" + mkyProjectId);

        tester.checkCheckbox("createType", "createScheme");
        tester.selectOption("selectedOptions", "Task");
        tester.submit();

        assertThat(backdoor.project().getSchemes(mkyProjectId).issueTypeScheme.name, equalTo(AUTO_CREATED_SCHEME_PREFIX + PROJECT_MONKEY));


        Long hspProjectId = backdoor.project().getProjectId(PROJECT_HOMOSAP_KEY);
        tester.gotoPage("/secure/admin/SelectIssueTypeSchemeForProject!default.jspa?projectId=" + hspProjectId);

        tester.selectOption("schemeId", AUTO_CREATED_SCHEME_PREFIX + PROJECT_MONKEY);
        tester.submit();

        assertThat(backdoor.project().getSchemes(hspProjectId).issueTypeScheme.name, equalTo(AUTO_CREATED_SCHEME_PREFIX + PROJECT_MONKEY));

        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Task");
        textAssertions.assertTextPresent(locator.css(CREATE_ISSUE_FORM_HEADER_CSS_LOCATOR), "Create Issue");
    }

    private void _testSameAsProject() {
        Long projectId = backdoor.project().getProjectId(PROJECT_HOMOSAP_KEY);
        tester.gotoPage("/secure/admin/SelectIssueTypeSchemeForProject!default.jspa?projectId=" + projectId);

        logger.log("Choose issue type scheme same as another project");

        tester.checkCheckbox("createType", "chooseProject");
        tester.selectOption("sameAsProjectId", PROJECT_MONKEY);
        tester.submit();
        textAssertions.assertTextPresent(locator.page(), "Default Issue Type Scheme");

        // Test issue creation should pass
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Task");
        textAssertions.assertTextPresent(locator.css(CREATE_ISSUE_FORM_HEADER_CSS_LOCATOR), "Create Issue");
    }

    private void _testCreateNewIssueTypeScheme() {
        logger.log("Create a new issue type scheme");

        Long projectId = backdoor.project().getProjectId(PROJECT_HOMOSAP_KEY);
        tester.gotoPage("/secure/admin/SelectIssueTypeSchemeForProject!default.jspa?projectId=" + projectId);

        // Select options for the new scheme
        tester.checkCheckbox("createType", "createScheme");
        form.selectOptionsByDisplayName("selectedOptions", new String[]{"Bug", "Improvement"});
        //selectMultiOption("selectedOptions", "Bug");
        //selectMultiOption("selectedOptions", "Improvement");
        tester.submit();

        assertThat(backdoor.project().getSchemes(projectId).issueTypeScheme.name, equalTo(AUTO_CREATED_SCHEME_PREFIX + PROJECT_HOMOSAP));

        // Test issue creation should fail
        if (new IdLocator(tester, "leave_admin").hasNodes()) {
            tester.clickLink("leave_admin");
        }
        tester.clickLink("create_link");
        navigation.issue().selectProject(PROJECT_HOMOSAP);
        navigation.issue().selectIssueType("Task");
        tester.submit();
        textAssertions.assertTextPresent(locator.page(), "The issue type selected is invalid");
        navigation.issue().selectIssueType("Bug");
        tester.submit();
        textAssertions.assertTextPresent(locator.css(CREATE_ISSUE_FORM_HEADER_CSS_LOCATOR), "Create Issue");
    }

    // Ensure that BULK CHANGE global permission is not required for issue type scheme association
    @Test
    public void testMultipleIssueTypeSchemesWithoutBulkChangePermission() {
        administration.restoreData("TestIssueTypesSchemes.xml");
        administration.removeGlobalPermission(BULK_CHANGE, Groups.USERS);

        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_TYPE_SCHEMES);
        tester.clickLink("associate_10011");
        tester.selectOption("projects", "homosapien");
        tester.submit("Associate");
        tester.assertTextPresent("homosapien");
        tester.assertTextPresent("Bug");
        tester.assertTextPresent("1");
        tester.submit("nextBtn");
        tester.submit("nextBtn");
        tester.submit("nextBtn");
        tester.assertTextPresent("homosapien");
        tester.assertTextPresent("New Feature");
        tester.submit("nextBtn");

        navigation.issue().viewIssue("HSP-1");
        textAssertions.assertTextPresent(locator.id(VIEW_ISSUE_PAGE_ISSUE_TYPE_LABEL_ID), "New Feature");
    }
}
