package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.testkit.client.util.JsonMatchers.hasField;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.WEBHOOKS, Category.REFERENCE_PLUGIN})
public class TestWebHookInModuleDescriptor extends AbstractWebHookTest {
    @Before
    public void setUp() throws Exception {
        super.setUpTest();
    }

    @Test
    public void testIssueCreatedWebHookFired() throws Exception {
        backdoor.restoreBlankInstance();

        backdoor.issues().createIssue("MKY", "New issue");

        final JSONObject event = new JSONObject(getWebHookResponseFromTestServlet("issue_created"));
        assertThat(event, hasField("issue"));
    }
}
