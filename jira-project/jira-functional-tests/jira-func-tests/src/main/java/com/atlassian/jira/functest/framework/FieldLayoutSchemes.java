package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.Navigation.AdminSection.FIELD_CONFIGURATION;
import static com.atlassian.jira.functest.framework.Navigation.AdminSection.ISSUE_FIELDS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class FieldLayoutSchemes {

    private final Navigation navigation;
    private final WebTester tester;
    private final Backdoor backdoor;

    @Inject
    public FieldLayoutSchemes(final Navigation navigation, final WebTester tester, final Backdoor backdoor) {
        this.navigation = navigation;
        this.tester = tester;
        this.backdoor = backdoor;
    }

    public void gotoFieldLayoutSchemes() {
        navigation.gotoAdminSection(ISSUE_FIELDS);
    }

    public void addFieldLayoutScheme(final String scheme_name, final String scheme_desc) {
        gotoFieldLayoutSchemes();
        tester.clickLink("add-field-configuration-scheme");
        tester.setFormElement("fieldLayoutSchemeName", scheme_name);
        tester.setFormElement("fieldLayoutSchemeDescription", scheme_desc);
        tester.submit("Add");
    }

    public void deleteFieldLayoutScheme(final String scheme_name) {
        gotoFieldLayoutSchemes();
        tester.clickLink("del_" + scheme_name);
        tester.assertTextPresent("Delete Field Configuration Scheme");
        tester.assertTextPresent(scheme_name);
        tester.submit("Delete");
    }

    public void addFieldLayoutSchemeEntry(final String issueTypeName, final String fieldLayoutName, final String schemeName) {
        gotoFieldLayoutSchemes();
        tester.clickLinkWithText(schemeName);
        tester.clickLink("add-issue-type-field-configuration-association");
        tester.selectOption("issueTypeId", issueTypeName);
        tester.selectOption("fieldConfigurationId", fieldLayoutName);
        tester.submit();
        tester.assertTextPresent(issueTypeName);
    }

    public void associateFieldLayoutScheme(final String projectKey, final String scheme_name) {
        final String projectId = backdoor.project().getProjectId(projectKey).toString();
        tester.gotoPage("/secure/admin/SelectFieldLayoutScheme!default.jspa?projectId=" + projectId);
        tester.assertTextPresent("Field Layout Configuration Association");
        tester.selectOption("schemeId", scheme_name);
        tester.submit("Associate");
        assertThat(backdoor.project().getSchemes(projectKey).fieldConfigurationScheme.name, equalTo(scheme_name));
    }

    public void associateWithDefaultFieldLayout(final String projectKey) {
        final String projectId = backdoor.project().getProjectId(projectKey).toString();
        tester.gotoPage("/secure/admin/SelectFieldLayoutScheme!default.jspa?projectId=" + projectId);
        tester.assertTextPresent("Field Layout Configuration Association");
        tester.selectOption("schemeId", "System Default Field Configuration");
        tester.submit("Associate");
    }

    public void copyFieldLayout(String fieldLayoutName) {
        navigation.gotoAdminSection(FIELD_CONFIGURATION);
        tester.assertTextPresent("View Field Configurations");
        tester.clickLinkWithText("Copy");
        tester.assertTextPresent("Copy Field Configuration:");
        tester.setFormElement("fieldLayoutName", fieldLayoutName);
        tester.submit();
    }
}
