package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import org.junit.Test;

import javax.inject.Inject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Checks the import and upgrade of users and groups for the migration to Embedded Crowd..
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.UPGRADE_TASKS})
@Restore("TestUpgradeTask602.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestUpgradeTask602 extends BaseJiraFuncTest {
    public static final String PAGE_GROUP_BROWSER = "/secure/admin/user/GroupBrowser.jspa";

    private static final String USERNAME_PREFIX = "Username:";
    private static final String FULL_NAME_PREFIX = "Full name:";
    private static final String NOT_RECORDED = "Not recorded";

    @Inject
    private FuncTestLogger logger;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testUserDataiIsCorrect() throws Exception {
        /* For efficiency, this is all in a single test.  We just goto each user and test that the data in their profile is as expected. */

        navigateToUser("watson");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), USERNAME_PREFIX, "watson");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), FULL_NAME_PREFIX, "Shane Watson");
        tester.assertLinkPresentWithText("swatson@shartrec.com");
        textAssertions.assertTextPresent(new IdLocator(tester, "loginCount"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "lastLogin"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "previousLogin"), NOT_RECORDED);

        navigateToUser("katich");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), USERNAME_PREFIX, "katich");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), FULL_NAME_PREFIX, "Simon Katich");
        tester.assertLinkPresentWithText("skatich@shartrec.com");
        textAssertions.assertTextPresent(new IdLocator(tester, "loginCount"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "lastLogin"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "previousLogin"), NOT_RECORDED);

        navigateToUser("ponting");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), USERNAME_PREFIX, "ponting");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), FULL_NAME_PREFIX, "Ricky Ponting");
        tester.assertLinkPresentWithText("rponting@shartrec.com");
        textAssertions.assertTextPresent(new IdLocator(tester, "loginCount"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "lastLogin"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "previousLogin"), NOT_RECORDED);

        navigateToUser("hussey");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), USERNAME_PREFIX, "hussey");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), FULL_NAME_PREFIX, "Michael Hussey");
        tester.assertLinkPresentWithText("mhussey@shartrec.com");
        textAssertions.assertTextPresent(new IdLocator(tester, "loginCount"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "lastLogin"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "previousLogin"), NOT_RECORDED);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "TESTattrib1", "Value1");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "TESTattrib2", "Value2");

        navigateToUser("haddin");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), USERNAME_PREFIX, "haddin");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), FULL_NAME_PREFIX, "Brad Haddin");
        tester.assertLinkPresentWithText("bhaddin@shartrec.com");
        textAssertions.assertTextPresent(new IdLocator(tester, "loginCount"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "lastLogin"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "previousLogin"), NOT_RECORDED);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "TESTattrib1", "Value11");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "TESTattrib2", "Value12");

        navigateToUser("johnson");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), USERNAME_PREFIX, "johnson");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), FULL_NAME_PREFIX, "michael johnson");
        tester.assertLinkPresentWithText("mjohnson@shartrec.com");
        textAssertions.assertTextPresent(new IdLocator(tester, "loginCount"), "2");
        textAssertions.assertTextNotPresent(new IdLocator(tester, "lastLogin"), NOT_RECORDED);
        textAssertions.assertTextNotPresent(new IdLocator(tester, "previousLogin"), NOT_RECORDED);

        navigateToUser("clarke");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), USERNAME_PREFIX, "clarke");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), FULL_NAME_PREFIX, "Michael Clark");
        tester.assertLinkPresentWithText("mclarke@shartrec.com");
        textAssertions.assertTextPresent(new IdLocator(tester, "loginCount"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "lastLogin"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "previousLogin"), NOT_RECORDED);

        navigateToUser("north");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), USERNAME_PREFIX, "north");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), FULL_NAME_PREFIX, "Marcus North");
        tester.assertLinkPresentWithText("MNORTH@shartrec.com");
        textAssertions.assertTextPresent(new IdLocator(tester, "loginCount"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "lastLogin"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "previousLogin"), NOT_RECORDED);

        navigateToUser("hauritz");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), USERNAME_PREFIX, "hauritz");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), FULL_NAME_PREFIX, "Nathan Hauritz");
        textAssertions.assertTextPresent(new IdLocator(tester, "loginCount"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "lastLogin"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "previousLogin"), NOT_RECORDED);

        navigateToUser("siddle");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), USERNAME_PREFIX, "siddle");
        tester.assertLinkPresentWithText("psiddle@shartrec.com");
        textAssertions.assertTextPresent(new IdLocator(tester, "loginCount"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "lastLogin"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "previousLogin"), NOT_RECORDED);

        navigateToUser("bollinger");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), USERNAME_PREFIX, "bollinger");
        textAssertions.assertTextPresent(new IdLocator(tester, "loginCount"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "lastLogin"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "previousLogin"), NOT_RECORDED);

        navigateToUser("Yousef");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), USERNAME_PREFIX, "Yousef");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), FULL_NAME_PREFIX, "Mohammad Yousuf");
        tester.assertLinkPresentWithText("myousef@pcb.com.pk");
        textAssertions.assertTextPresent(new IdLocator(tester, "loginCount"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "lastLogin"), NOT_RECORDED);
        textAssertions.assertTextPresent(new IdLocator(tester, "previousLogin"), NOT_RECORDED);

    }

    @Test
    public void testGroupDataiIsCorrect() throws Exception {
        /* For efficiency, this is all in a single test.  We just goto each user and test that the data in their profile is as expected. */
        // Now for the Groups
        navigateToGroup("group1");
        tester.assertTextPresent("watson");
        tester.assertTextPresent("katich");
        textAssertions.assertTextPresent(new WebPageLocator(tester), "Displaying users 1 to 2 of 2");

        navigateToGroup("Group2");
        tester.assertTextPresent("watson");
        tester.assertTextPresent("katich");
        tester.assertTextPresent("hussey");
        textAssertions.assertTextPresent(new WebPageLocator(tester), "Displaying users 1 to 3 of 3");

        navigateToGroup("group3");
        tester.assertTextPresent("watson");
        textAssertions.assertTextPresent(new WebPageLocator(tester), "Displaying users 1 to 1 of 1");

        navigateToGroup("GrouP4");
        textAssertions.assertTextPresent(new WebPageLocator(tester), "Displaying users 0 to 0 of 0");

        navigateToGroup("jira-users");
        tester.assertTextPresent("watson");
        tester.assertTextPresent("katich");
        tester.assertTextPresent("hussey");
        tester.assertTextPresent("ponting");
        tester.assertTextPresent("haddin");
        tester.assertTextPresent("johnson");
        tester.assertTextPresent("clarke");
        tester.assertTextPresent("north");
        tester.assertTextPresent("hauritz");
        tester.assertTextPresent("siddle");
        tester.assertTextPresent("bollinger");
        tester.assertTextPresent("Yousef");
        textAssertions.assertTextPresent(new WebPageLocator(tester), "Displaying users 1 to 13 of 13");

    }

    private void navigateToGroup(String groupName) {
        logger.log("Navigating in GroupBrowser to Group " + groupName);
        tester.gotoPage(PAGE_GROUP_BROWSER);
        tester.clickLinkWithText(groupName);
        tester.clickLink("view_group_members");
    }

    private void navigateToUser(final String username) {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.clickLink(username);
    }

    /**
     * This converts a local time string (Which is what we have in the export file we are testing against, to a string in the
     * format we expect to be stored in User Attributes.
     *
     * @param localTimeString Date/Time as a String in the system local time
     * @return DateTime as a String in UTC time zone
     */
    private String convertLocalTimeStringToUTC(String localTimeString) throws Exception {
        DateFormat localDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        localDf.setTimeZone(TimeZone.getDefault());
        Date date = localDf.parse(localTimeString);

        DateFormat utcDf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        utcDf.setTimeZone(TimeZone.getTimeZone("UTC"));

        return utcDf.format(date);
    }

}
