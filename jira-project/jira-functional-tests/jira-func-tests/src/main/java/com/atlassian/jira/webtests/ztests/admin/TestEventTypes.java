package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.meterware.httpunit.TableCell;
import com.meterware.httpunit.WebTable;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.EVENT_TYPE_ACTIVE_STATUS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.EVENT_TYPE_INACTIVE_STATUS;
import static org.junit.Assert.assertTrue;

@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.BROWSING})
@RestoreBlankInstance
@LoginAs(user = ADMIN_USERNAME)
public class TestEventTypes extends BaseJiraFuncTest {
    private final String CUSTOM_EVENT_TYPE_NAME = "Custom Event Type Name";
    private final String CUSTOM_EVENT_TYPE_DESC = "Custom Event Type Description";

    private final String EVENT_TYPE_TABLE = "eventTypeTable";
    private final int EVENT_TYPE_TABLE_NAME_COL = 0;

    private final String CREATE_EVENT_TYPE_NAME = "Issue Created";
    private final String COMMENT_EVENT_TYPE_NAME = "Issue Commented";
    private final String COMMENT_EDITED_EVENT_TYPE_NAME = "Issue Comment Edited";
    private final String REOPENED_EVENT_TYPE_NAME = "Issue Reopened";
    private final String WORKLOG_UPDATED_EVENT_TYPE_NAME = "Issue Worklog Updated";
    private final String WORKLOG_DELETED_EVENT_TYPE_NAME = "Issue Worklog Deleted";
    private final String GENERIC_EVENT_TYPE_NAME = "Generic Event";

    private final String[] eventOrder = new String[]{CREATE_EVENT_TYPE_NAME, COMMENT_EVENT_TYPE_NAME, COMMENT_EDITED_EVENT_TYPE_NAME,
            REOPENED_EVENT_TYPE_NAME, WORKLOG_UPDATED_EVENT_TYPE_NAME,
            WORKLOG_DELETED_EVENT_TYPE_NAME, GENERIC_EVENT_TYPE_NAME, CUSTOM_EVENT_TYPE_NAME};

    private final String GENERIC_EVENT_TEMPLATE = "Generic Event";

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testAddEventType() {
        addEventType(CUSTOM_EVENT_TYPE_NAME, CUSTOM_EVENT_TYPE_DESC, GENERIC_EVENT_TEMPLATE);
    }

    @Test
    public void testAddEventTypeWithErrors() {
        navigation.gotoAdminSection(Navigation.AdminSection.EVENTTYPES);
        tester.setFormElement("name", "");
        tester.submit("Add");
        tester.assertTextPresent("You must specify an event name");
        tester.assertTextPresent("You must select a default template to associate with this event");
        tester.setFormElement("name", "Issue Created");
        tester.submit("Add");
        tester.assertTextPresent("An event with this name already exists");
        tester.assertTextPresent("You must select a default template to associate with this event");
    }

    @Test
    public void testDeleteEventType() {
        addEventType(CUSTOM_EVENT_TYPE_NAME, CUSTOM_EVENT_TYPE_DESC, GENERIC_EVENT_TEMPLATE);
        deleteEventType(CUSTOM_EVENT_TYPE_NAME);
        tester.assertTextNotPresent(CUSTOM_EVENT_TYPE_NAME);
    }

    @Test
    public void testEditEventType() {
        addEventType(CUSTOM_EVENT_TYPE_NAME, CUSTOM_EVENT_TYPE_DESC, GENERIC_EVENT_TEMPLATE);

        navigation.gotoAdminSection(Navigation.AdminSection.EVENTTYPES);
        tester.clickLink("edit_" + CUSTOM_EVENT_TYPE_NAME);
        tester.assertTextPresent("Update");

        final String NEW_NAME = "Custom Event Type New Name";
        final String NEW_DESC = "Custom Event Type New Description";
        final String NEW_TEMPLATE = "Issue Created";

        tester.setFormElement("name", NEW_NAME);
        tester.setFormElement("description", NEW_DESC);
        tester.selectOption("templateId", NEW_TEMPLATE);
        tester.submit("Update");

        checkEventTypeDetails(NEW_NAME, NEW_DESC, EVENT_TYPE_INACTIVE_STATUS, NEW_TEMPLATE, null, null);
    }

    @Test
    public void testCreateNotificationAssociation() {
        // Add the event type
        addEventType(CUSTOM_EVENT_TYPE_NAME, CUSTOM_EVENT_TYPE_DESC, GENERIC_EVENT_TEMPLATE);
        String eventTypeId = getEventTypeIDWithName(CUSTOM_EVENT_TYPE_NAME);

        // Create the notification association
        navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);
        tester.clickLinkWithText("Default Notification Scheme");
        tester.clickLink("add_" + eventTypeId);
        tester.checkCheckbox("type", "Current_Assignee");
        tester.submit("Add");

        // check the notification has been made
        checkNotificationForEvent(CUSTOM_EVENT_TYPE_NAME, "Current Assignee", GENERIC_EVENT_TEMPLATE);

        checkEventTypeDetails(CUSTOM_EVENT_TYPE_NAME, CUSTOM_EVENT_TYPE_DESC, EVENT_TYPE_ACTIVE_STATUS, GENERIC_EVENT_TEMPLATE, "Default Notification Scheme", null);
    }

    @Test
    public void testEventDefsOrder() {
        addEventType(CUSTOM_EVENT_TYPE_NAME, CUSTOM_EVENT_TYPE_DESC, GENERIC_EVENT_TEMPLATE);

        navigation.gotoAdminSection(Navigation.AdminSection.EVENTTYPES);

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), eventOrder);
    }

    @Test
    public void testNotificationsEventOrder() {
        addEventType(CUSTOM_EVENT_TYPE_NAME, CUSTOM_EVENT_TYPE_DESC, GENERIC_EVENT_TEMPLATE);
        navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);
        tester.clickLinkWithText("Default Notification Scheme");

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), eventOrder);

        tester.clickLink("add_1");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), eventOrder);

    }

    // JRA-10274: custom events with an apostrophe in their name do not display correctly due to a bug in the
    // I18nBean.getText(String) method.
    @Test
    public void testEventWithApostropheDisplaysCorrectly() {
        navigation.gotoAdminSection(Navigation.AdminSection.EVENTTYPES);
        tester.setFormElement("name", "This event isn't named nicely");
        tester.selectOption("templateId", "Generic Event");
        tester.submit("Add");
        tester.assertTextPresent("This event isn&#39;t named nicely");

        // check Notification Schemes page
        navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);
        tester.clickLinkWithText("Default Notification Scheme");
        tester.assertTextPresent("This event isn&#39;t named nicely");

        // check View Workflow Transition page

        // copy the current workflow
        administration.workflows().goTo().copyWorkflow("jira", "Copy of jira").textView().goTo();

        // add a new transition
        tester.clickLink("add_trans_3");
        tester.setFormElement("transitionName", "Test transition");
        tester.setFormElement("description", "");
        tester.submit("Add");
        tester.clickLinkWithText("Test transition");

        // edit the post functions to fire our new event
        tester.clickLinkWithText("Post Functions");
        tester.clickLinkWithText("Edit", 1);
        tester.selectOption("eventTypeId", "This event isn't named nicely");
        tester.submit("Update");
        tester.assertTextPresent("This event isn&#39;t named nicely");
    }

    private void addEventType(String name, String description, String template) {
        navigation.gotoAdminSection(Navigation.AdminSection.EVENTTYPES);
        tester.setFormElement("name", name);
        tester.setFormElement("description", description);
        tester.selectOption("templateId", template);
        tester.submit("Add");
        checkEventTypeDetails(name, description, EVENT_TYPE_INACTIVE_STATUS, template, null, null);

        WebTable fieldTable;
        try {
            fieldTable = tester.getDialog().getResponse().getTableWithID(EVENT_TYPE_TABLE);

            String eventTypeCellText = fieldTable.getCellAsText(fieldTable.getRowCount() - 1, EVENT_TYPE_TABLE_NAME_COL);
            assertTrue(eventTypeCellText.contains(name));
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    public void deleteEventType(String name) {
        navigation.gotoAdminSection(Navigation.AdminSection.EVENTTYPES);
        tester.clickLink("del_" + name);
        tester.assertTextPresent("Please confirm that you wish to delete the event: <b>" + name + "</b>.");
        tester.submit("Delete");
        tester.assertTextNotPresent(name);
    }

    public String getEventTypeIDWithName(String name) {
        navigation.gotoAdminSection(Navigation.AdminSection.EVENTTYPES);

        tester.clickLink("edit_" + name);

        return tester.getDialog().getFormParameterValue("eventTypeId");
    }

    // Check the event type table details for the specified event type
    public void checkEventTypeDetails(String eventTypeName, String eventTypeDesc, String status, String template, String notificationScheme, String workflow) {
        navigation.gotoAdminSection(Navigation.AdminSection.EVENTTYPES);

        WebTable fieldTable;
        try {
            fieldTable = tester.getDialog().getResponse().getTableWithID(EVENT_TYPE_TABLE);

            // First row is a headings row so skip it
            for (int i = 1; i < fieldTable.getRowCount(); i++) {
                String field = fieldTable.getCellAsText(i, EVENT_TYPE_TABLE_NAME_COL);
                if (field.contains(eventTypeName)) {
                    int EVENT_TYPE_TABLE_DESC_COL = 1;
                    String eventTypeCellText = fieldTable.getCellAsText(i, EVENT_TYPE_TABLE_DESC_COL);
                    assertTrue(eventTypeCellText.contains(eventTypeDesc));
                    int EVENT_TYPE_TABLE_STATUS_COL = 2;
                    eventTypeCellText = fieldTable.getCellAsText(i, EVENT_TYPE_TABLE_STATUS_COL);
                    assertTrue(eventTypeCellText.contains(status));
                    int EVENT_TYPE_TABLE_TEMPLATE_COL = 3;
                    eventTypeCellText = fieldTable.getCellAsText(i, EVENT_TYPE_TABLE_TEMPLATE_COL);
                    assertTrue(eventTypeCellText.contains(template));

                    if (notificationScheme != null && !notificationScheme.equals("")) {
                        int EVENT_TYPE_TABLE_NOTIFIC_COL = 4;
                        eventTypeCellText = fieldTable.getCellAsText(i, EVENT_TYPE_TABLE_NOTIFIC_COL);
                        assertTrue(eventTypeCellText.contains(notificationScheme));
                    }

                    if (workflow != null && !workflow.equals("")) {
                        int EVENT_TYPE_TABLE_WORKFLOW_COL = 5;
                        eventTypeCellText = fieldTable.getCellAsText(i, EVENT_TYPE_TABLE_WORKFLOW_COL);
                        assertTrue(eventTypeCellText.contains(workflow));
                    }
                }
            }
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    public void checkNotificationForEvent(String eventTypeName, String notificationType, String template) {
        navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);
        tester.clickLinkWithText("Default Notification Scheme");

        WebTable fieldTable;
        try {
            final String NOTIFICATION_SCHEME_TABLE = "notificationSchemeTable";
            fieldTable = tester.getDialog().getResponse().getTableWithID(NOTIFICATION_SCHEME_TABLE);

            // First row is a headings row so skip it
            for (int i = 1; i < fieldTable.getRowCount(); i++) {
                int NOTIFICATION_TABLE_NAME_COL = 0;
                String field = fieldTable.getCellAsText(i, NOTIFICATION_TABLE_NAME_COL);
                if (field.contains(eventTypeName)) {
                    int NOTIFICATION_TABLE_TYPE_COL = 1;
                    TableCell notificationCell = fieldTable.getTableCell(i, NOTIFICATION_TABLE_TYPE_COL);

                    if (notificationType == null) {
                        assertTrue(!notificationCell.asText().contains(notificationType));
                    } else {
                        assertTrue(notificationCell.asText().contains(notificationType));
                    }
                }
            }

            navigation.gotoAdminSection(Navigation.AdminSection.EVENTTYPES);
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }
}
