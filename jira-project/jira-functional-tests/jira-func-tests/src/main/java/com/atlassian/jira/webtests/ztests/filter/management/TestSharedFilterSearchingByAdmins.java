package com.atlassian.jira.webtests.ztests.filter.management;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.parser.filter.FilterItem;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.google.common.base.Predicates.in;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.all;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Responsible for verifying that an administrator is able to search for all the shared filters in JIRA.
 *
 * @since v4.4.1
 */
@WebTest({Category.FUNC_TEST, Category.FILTERS})
@LoginAs(user = ADMIN_USERNAME)
public class TestSharedFilterSearchingByAdmins extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreData("TestSharedFilterSearchingByAdmins.xml");
    }

    @Test
    public void testAnAdminIsAbleToSearchForFiltersSharedWithAGroupHeDoesNotBelongTo() {
        final List<FilterItem> expectedFilterItems =
                ImmutableList.of
                        (
                                new FilterItem.Builder().
                                        id(10006).
                                        name("Shared Filter With Group jira-developers owned by developer").
                                        owner("Developer")
                                        .build()
                        );

        final List<FilterItem> actualFilterItems = administration.sharedFilters().goTo().
                searchAll().filters().list();

        assertNotNull(actualFilterItems);
        assertTrue(actualFilterItems.size() > 0);
        assertTrue(all(expectedFilterItems, in(actualFilterItems)));
    }

    @Test
    public void testAnAdminIsAbleToSearchForFiltersSharedWithAGroupHeBelongsTo() throws Exception {
        final List<FilterItem> expectedFilterItems =
                ImmutableList.of
                        (
                                new FilterItem.Builder().
                                        id(10010).
                                        name("Shared Filter with Anyone owned by developer").
                                        owner("Developer")
                                        .build(),
                                new FilterItem.Builder().
                                        id(10009).
                                        name("Shared Filter With group jira-administrators owned by admin").
                                        owner("Administrator")
                                        .build(),
                                new FilterItem.Builder().
                                        id(10002).
                                        name("Shared Filter With Group jira-users owned by fred").
                                        owner("Fred Normal")
                                        .build()
                        );

        final List<FilterItem> actualFilterItems = administration.sharedFilters().goTo().
                searchAll().filters().list();

        assertNotNull(actualFilterItems);
        assertTrue(actualFilterItems.size() > 0);
        assertTrue(all(expectedFilterItems, in(actualFilterItems)));
    }

    @Test
    public void testAnAdminIsAbleToSearchForFiltersSharedWithARoleHeIsNotPartOf() throws Exception {
        final List<FilterItem> expectedFilterItems =
                ImmutableList.of
                        (
                                new FilterItem.Builder().
                                        id(10004).
                                        name("Shared Filter With Role Developers on homosapien owned by developer").
                                        owner("Developer")
                                        .build()
                        );

        final List<FilterItem> actualFilterItems = administration.sharedFilters().goTo().
                searchAll().filters().list();

        assertNotNull(actualFilterItems);
        assertTrue(actualFilterItems.size() > 0);
        assertTrue(all(expectedFilterItems, in(actualFilterItems)));
    }

    @Test
    public void testAnAdminIsAbleToSearchForFiltersSharedWithARoleHeIsPartOf() throws Exception {
        final List<FilterItem> expectedFilterItems =
                ImmutableList.of
                        (
                                new FilterItem.Builder().
                                        id(10001).
                                        name("Shared Filter With Role Users on homosapien owned by fred").
                                        owner("Fred Normal")
                                        .build(),
                                new FilterItem.Builder().
                                        id(10007).
                                        name("Shared Filter With All Roles on monkey owned by developer").
                                        owner("Developer")
                                        .build(),
                                new FilterItem.Builder().
                                        id(10005).
                                        name("Shared Filter With Role Developers on monkey owned by developer").
                                        owner("Developer")
                                        .build()
                        );

        final List<FilterItem> actualFilterItems = administration.sharedFilters().goTo().
                searchAll().filters().list();

        assertNotNull(actualFilterItems);
        assertTrue(actualFilterItems.size() > 0);
        assertTrue(all(expectedFilterItems, in(actualFilterItems)));
    }

    @Test
    public void testAnAdminIsNotAbleToSearchForFiltersThatArePrivate() throws Exception {
        final List<FilterItem> nonExpectedFilterItems =
                ImmutableList.of
                        (
                                new FilterItem.Builder().
                                        id(10003).
                                        name("Private Filter Owned by developer").
                                        owner("Developer")
                                        .build(),
                                new FilterItem.Builder().
                                        id(10000).
                                        name("Private Filter Owned by fred").
                                        owner("Fred Normal")
                                        .build(),
                                new FilterItem.Builder().
                                        id(10008).
                                        name("Private Filter owned by admin").
                                        owner("Administrator")
                                        .build()
                        );

        final List<FilterItem> actualFilterItems = administration.sharedFilters().goTo().
                searchAll().filters().list();

        assertNotNull(actualFilterItems);
        assertTrue(actualFilterItems.size() > 0);
        assertTrue(all(nonExpectedFilterItems, not(in(actualFilterItems))));
    }
}
