package com.atlassian.jira.webtests.ztests.customfield;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperations;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.HttpUnitConfiguration;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.admin.CustomFields;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.testkit.beans.CustomFieldResponse;
import com.atlassian.jira.testkit.client.IssueTypeControl;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.google.common.collect.ImmutableList;
import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.WebLink;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_PREFIX;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_CASCADINGSELECT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_DATEPICKER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_DATETIME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_FLOAT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_FREETEXT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_MULTICHECKBOXES;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_MULTISELECT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_MULTIUSERPICKER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_PROJECT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_RADIO;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_SELECT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_TEXTFIELD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_URL;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_USERPICKER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_VERSION;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_FIELD_SCREEN_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_NEO;
import static com.atlassian.jira.functest.framework.admin.CustomFields.builtInCustomFieldKey;
import static com.atlassian.jira.functest.framework.admin.CustomFields.numericCfId;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.functest.framework.suite.Category.CUSTOM_FIELDS;
import static com.atlassian.jira.functest.framework.suite.Category.FIELDS;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.TIME_ZONES;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@WebTest({FUNC_TEST, CUSTOM_FIELDS, FIELDS})
@LoginAs(user = ADMIN_USERNAME)
public class TestCustomFields extends BaseJiraFuncTest {

    private static final String TEST_CUSTOM_FIELDS_XML_PRJ_HOMOSAP_ID = "10000";
    private static final String TEST_CUSTOM_FIELDS_XML_PRJ_NEO_ID = "10010";
    private static final String TEST_CUSTOM_FIELDS_XML_VERSION_1_DOT_1_ID = "10010";
    private static final String TEST_CUSTOM_FIELDS_XML_VERSION_1_DOT_2_ID = "10011";
    private static final String TEST_CUSTOM_FIELDS_XML_ISSUE_KEY_1 = "HSP-1";
    private static final String TEST_CUSTOM_FIELDS_XML_ISSUE_KEY_2 = "HSP-2";
    private static final String TEST_CUSTOM_FIELDS_XML_ISSUE_KEY_3 = "NDT-1";

    private static final String CUSTOM_FIELD_NAME_ONE = "Assigned to Section";
    private static final String CUSTOM_FIELD_NAME_TWO = "Affects Project";
    private static final String CUSTOM_FIELD_NAME_THREE = "Time created";
    private static final String CUSTOM_FIELD_NAME_CASCADING_SELECT = "cascading select field name";
    private static final String CUSTOM_FIELD_NAME_MULTI_CHECKBOX = "multicheckboxes field name";
    private static final String CUSTOM_FIELD_NAME_MULTI_SELECT = "multiselect field name";
    private static final String CUSTOM_FIELD_NAME_PROJECT_PICKER = "project picker field name";
    private static final String CUSTOM_FIELD_NAME_TEXT_FIELD = "text field field name";
    private static final String CUSTOM_FIELD_NAME_DATE_TIME = "date time field name";
    private static final String CUSTOM_FIELD_NAME_USER_PICKER = "user picker field name";
    private static final String CUSTOM_FIELD_NAME_MULTI_USER_PICKER = "multi user picker field name";
    private static final String CUSTOM_FIELD_NAME_DATE_PICKER = "date picker field name";
    private static final String CUSTOM_FIELD_NAME_FREE_TEXT = "free text field name";
    private static final String CUSTOM_FIELD_NAME_NUMBER = "number field name";
    private static final String CUSTOM_FIELD_NAME_RADIO_BUTTONS = "radio buttons field name";
    private static final String CUSTOM_FIELD_NAME_SELECT_LIST = "select list field name";
    private static final String CUSTOM_FIELD_NAME_URL_FIELD = "url field name";
    private static final String CUSTOM_FIELD_NAME_VERSION_PICKER = "version picker field name";

    private static final List<String> customFieldNames = ImmutableList.<String>builder().add(
            CUSTOM_FIELD_NAME_CASCADING_SELECT, CUSTOM_FIELD_NAME_MULTI_CHECKBOX, CUSTOM_FIELD_NAME_MULTI_SELECT,
            CUSTOM_FIELD_NAME_PROJECT_PICKER, CUSTOM_FIELD_NAME_TEXT_FIELD, CUSTOM_FIELD_NAME_DATE_TIME,
            CUSTOM_FIELD_NAME_USER_PICKER, CUSTOM_FIELD_NAME_DATE_PICKER, CUSTOM_FIELD_NAME_FREE_TEXT,
            CUSTOM_FIELD_NAME_NUMBER, CUSTOM_FIELD_NAME_RADIO_BUTTONS, CUSTOM_FIELD_NAME_SELECT_LIST,
            CUSTOM_FIELD_NAME_URL_FIELD, CUSTOM_FIELD_NAME_VERSION_PICKER).build();

    private static final String freeTextEntry = "This is the default text area value\n with newlines possible\n and stuff.";
    private static final String numberEntry = "43";
    private static final String textEntry = "Text Field - This is the default text";
    private static final String urlEntry = "http://www.testhis.com";
    private static final String userEntry = "custom_field_user";
    private static final String version_oneDotOne = "1.1";
    private static final String version_oneDotTwo = "1.2";
    private static final String summary = "This is the summary of this issue.";

    private String fieldId_global;
    private String fieldId_issue;
    private String fieldId_project;
    private String cascadingSelectId;
    private String datePickerId;
    private String userPickerId;
    private String versionPickerId;
    private String freeTextId;
    private String multiSelectId;
    private String multiCheckboxId;
    private String projectPickerId;
    private String radioButtonId;
    private String selectListId;
    private String textFieldId;
    private String urlId;
    private String dateTimeId;
    private String numberId;

    @Inject
    private HtmlPage page;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private BulkOperations bulkOperations;

    @Inject
    private Form form;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    @HttpUnitConfiguration(enableScripting = true)
    @RestoreBlankInstance
    public void testReturnUrl() {
        // Click Link 'administration' (id='admin_link').
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        // Click Link 'Add Custom Field' (id='add_custom_fields').
        tester.clickLink("add_custom_fields");
        tester.checkCheckbox("fieldType", "com.atlassian.jira.plugin.system.customfieldtypes:cascadingselect");

        pressCancel();

        tester.assertTitleEquals("Custom fields - jWebTest JIRA installation");
    }

    private void pressCancel() {
        assertTrue("Scripting must be enabled in the HttpUnit for cancel to work correctly", HttpUnitOptions.isScriptingEnabled());
        tester.setWorkingForm("jiraform");
        tester.clickLink("cancelButton");
    }

    @Test
    @Restore("TestCustomFields.xml")
    public void testCustomFields() {
        customFieldsAddField();
        customFieldsDeleteField();
        customFieldsAddCustomFieldOption();
        customFieldsDelCustomFieldOption();
        customFieldCreateIssueWithCustomFields();
        customFieldCreateIssueWithFieldScope();
        customFieldsEditIssueWithCustomFields();
        customFieldEditIssueWithFieldScope();
        customFieldsMoveIssueWithCustomFieldsforProject();
        customFieldWithFieldScreenSchemes();

        customFieldsMoveIssueWithCustomFieldsforIssueType();
        customFieldCreateSubTaskWithCustomFields();
    }

    ////    **************  Test All Custom Field Test  **************

    // 1. Add one of each custom field (expect read-only custom fields) and add to default screen
    // 2. Create an issue - ensure default values selected
    // 3. View issue - ensure selected values displayed
    // 4. Edit Issue - ensure selected values displayed
    // 5. View Issue - ensure selected values displayed
    // 6. View Issue in Issue Navigator with all custom field columns present - ensure selected values displayed
    // 7. Remove project, version and all custom fields

    @Test
    @Restore("TestCustomFields.xml")
    public void testAllCustomFields() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        setupAllCustomFields();

        createAndValidateIssue();

        viewAndValidateIssue(false);

        editAndValidateIssue();

        viewAndValidateIssue(true);

        viewIssueInNavigator();
    }

    // JRA-16005: XSS bug
    @Test
    @RestoreBlankInstance
    public void testNumberCustomField() {
        setupNumberField("");

        final String xsrfToken = page.getXsrfToken();
        tester.gotoPage("/secure/CreateIssueDetails.jspa?pid=10000&issuetype=2&atl_token=" + xsrfToken);

        tester.setFormElement("customfield_10000", "\"><script>alert(document.cookie);</script>");
        tester.submit("Create");

        //ensure the warning is encoded.
        tester.assertTextPresent("&#39;&quot;&gt;&lt;script&gt;alert(document.cookie);&lt;/script&gt;&#39; is an invalid number");
        //ensure the field value is in the input text (note that webunit decodes it which is why it's not html escaped here)
        // if the value wasn't encoded properly however, then the value would show up empty here!
        assertEquals(form.switchTo("issue-create").getParameterValue("customfield_10000"), "\"><script>alert(document.cookie);</script>");
    }

    @Test
    @RestoreBlankInstance
    public void testUserCustomFieldWithInvalidDefaultValue() {
        final String userPickerId = addCustomField(CUSTOM_FIELD_TYPE_USERPICKER, CUSTOM_FIELD_NAME_USER_PICKER);
        administration.customFields().setDefaultValue(userPickerId, "user_does_not_exist");
        assertions.getTextAssertions().assertTextPresentHtmlEncoded("User 'user_does_not_exist' was not found in the system");
    }

    @Test
    @RestoreBlankInstance
    public void testMultiUserCustomFieldWithInvalidDefaultValue() {
        final String multiUserPickerId = addCustomField(CUSTOM_FIELD_TYPE_MULTIUSERPICKER, CUSTOM_FIELD_NAME_MULTI_USER_PICKER);
        administration.customFields().setDefaultValue(multiUserPickerId, "user_does_not_exist");
        tester.assertTextPresent("Could not find usernames: user_does_not_exist");
    }

    // Setup each custom field available (except for read-only fields)
    // Default Values set for all fields
    private void setupAllCustomFields() {
        setupCascadingSelect();
        setupDateTime("24/Aug/2005 6:00 AM");
        setupMultiSelect(ImmutableList.of("MultiSelect One", "MultiSelect Two", "MultiSelect Three"));
        setupProjectPicker(TEST_CUSTOM_FIELDS_XML_PRJ_HOMOSAP_ID);
        setupTextField(textEntry);
        setupUserPicker(userEntry);
        setupDatePicker("23/Aug/2005");
        setupFreeText(freeTextEntry);
        setupMultiCheckboxes(ImmutableList.of("MultiCheckBox One", "MultiCheckBox Two", "MultiCheckBox Three"));
        setupNumberField(numberEntry);
        setupRadioButtons(ImmutableList.of("Radio One", "Radio Two", "Radio Three"));
        setupSelectList(ImmutableList.of("Select List One", "Select List Two", "Select List Three"));
        setupURLField(urlEntry);
        setupVersionPicker(TEST_CUSTOM_FIELDS_XML_VERSION_1_DOT_1_ID);
    }

    private void viewIssueInNavigator() {
        final Map<String, CustomFieldResponse> customFields = backdoor.customFields().getCustomFields().stream()
                .collect(toMap(CustomFieldResponse::getName, identity()));

        backdoor.columnControl().addLoggedInUserColumns(customFieldNames.stream()
                .map(customFields::get)
                .map(customFieldResponse -> customFieldResponse.id)
                .collect(CollectorsUtil.toImmutableListWithSizeOf(customFieldNames)));
        navigation.issueNavigator().createSearch("project = " + TEST_CUSTOM_FIELDS_XML_PRJ_HOMOSAP_ID);
        viewAndValidateIssue(true);
        backdoor.columnControl().restoreLoggedInUserColumns();
    }

    // Create an issue and validate custom field values
    private void createAndValidateIssue() {
        // Create first step of issue and ensure that all values are available
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");
        tester.assertTextPresent("CreateIssueDetails.jspa");

        // Check that default values are present and select other values
        validateAndSetCustomFieldValues(false);
    }

    private void validateAndSetCustomFieldValues(final boolean edit) {
        tester.assertTextPresent(CUSTOM_FIELD_NAME_CASCADING_SELECT);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_DATE_PICKER);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_DATE_TIME);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_FREE_TEXT);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_MULTI_CHECKBOX);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_MULTI_SELECT);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_NUMBER);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_PROJECT_PICKER);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_RADIO_BUTTONS);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_SELECT_LIST);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_TEXT_FIELD);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_URL_FIELD);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_USER_PICKER);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_VERSION_PICKER);

        if (edit) {
            tester.setWorkingForm("issue-edit");
            tester.selectOption(CUSTOM_FIELD_PREFIX + cascadingSelectId, "Cascade One");
            tester.selectOption(CUSTOM_FIELD_PREFIX + cascadingSelectId + ":1", "Cascade B");
            tester.assertOptionEquals(CUSTOM_FIELD_PREFIX + cascadingSelectId, "Cascade One");
            tester.assertOptionEquals(CUSTOM_FIELD_PREFIX + cascadingSelectId + ":1", "Cascade B");

            tester.setFormElement(CUSTOM_FIELD_PREFIX + datePickerId, "22/Aug/05");
            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + datePickerId, "22/Aug/05");

            tester.setFormElement(CUSTOM_FIELD_PREFIX + dateTimeId, "25/Aug/05 7:00 AM");
            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + dateTimeId, "25/Aug/05 7:00 AM");

            tester.setFormElement(CUSTOM_FIELD_PREFIX + freeTextId, freeTextEntry + " edited.");
            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + freeTextId, freeTextEntry + " edited.");

            tester.setFormElement(CUSTOM_FIELD_PREFIX + multiCheckboxId, "10017");
            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + multiCheckboxId, "10017");

            tester.setFormElement(CUSTOM_FIELD_PREFIX + multiSelectId, "10013");
            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + multiSelectId, "10013");

            tester.setFormElement(CUSTOM_FIELD_PREFIX + numberId, numberEntry + "3");
            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + numberId, numberEntry + "3");

            tester.setFormElement(CUSTOM_FIELD_PREFIX + projectPickerId, TEST_CUSTOM_FIELDS_XML_PRJ_NEO_ID);
            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + projectPickerId, TEST_CUSTOM_FIELDS_XML_PRJ_NEO_ID);

            tester.setFormElement(CUSTOM_FIELD_PREFIX + radioButtonId, "10020");
            tester.assertRadioOptionSelected(CUSTOM_FIELD_PREFIX + radioButtonId, "10020");

            tester.selectOption(CUSTOM_FIELD_PREFIX + selectListId, "Select List Three");
            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + selectListId, "10023");

            tester.setFormElement(CUSTOM_FIELD_PREFIX + textFieldId, textEntry + " edited.");
            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + textFieldId, textEntry + " edited.");

            tester.setFormElement(CUSTOM_FIELD_PREFIX + userPickerId, ADMIN_USERNAME);
            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + userPickerId, ADMIN_USERNAME);

            tester.selectOption(CUSTOM_FIELD_PREFIX + versionPickerId, version_oneDotTwo);
            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + versionPickerId, TEST_CUSTOM_FIELDS_XML_VERSION_1_DOT_2_ID);

            tester.setFormElement(CUSTOM_FIELD_PREFIX + urlId, urlEntry + ".au");
            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + urlId, urlEntry + ".au");
        } else {
            tester.assertOptionEquals(CUSTOM_FIELD_PREFIX + cascadingSelectId, "Cascade Three");
            tester.assertOptionEquals(CUSTOM_FIELD_PREFIX + cascadingSelectId + ":1", "Cascade Beta");

            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + datePickerId, "23/Aug/05");

            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + dateTimeId, "24/Aug/05 6:00 AM");

            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + freeTextId, "\n" + freeTextEntry); //new line solves JRA-11257

            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + multiCheckboxId, "10016");

            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + multiSelectId, "10014");

            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + numberId, numberEntry);

            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + projectPickerId, TEST_CUSTOM_FIELDS_XML_PRJ_HOMOSAP_ID);

            tester.assertRadioOptionSelected(CUSTOM_FIELD_PREFIX + radioButtonId, "10019");

            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + selectListId, "10022");

            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + textFieldId, textEntry);

            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + urlId, urlEntry);

            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + userPickerId, userEntry);

            tester.assertFormElementEquals(CUSTOM_FIELD_PREFIX + versionPickerId, TEST_CUSTOM_FIELDS_XML_VERSION_1_DOT_1_ID);
        }

        tester.setFormElement("summary", summary);
        tester.submit();
    }

    // Validate that the correct values are shown when viewing the issue
    private void viewAndValidateIssue(final boolean edited) {
        tester.assertTextPresent(CUSTOM_FIELD_NAME_CASCADING_SELECT);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_DATE_PICKER);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_DATE_TIME);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_FREE_TEXT);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_MULTI_CHECKBOX);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_MULTI_SELECT);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_NUMBER);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_PROJECT_PICKER);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_RADIO_BUTTONS);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_SELECT_LIST);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_TEXT_FIELD);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_URL_FIELD);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_USER_PICKER);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_VERSION_PICKER);

        if (edited) {
            tester.assertTextPresent("Cascade One");
            tester.assertTextPresent("Cascade B");
            tester.assertTextPresent("22/Aug/05");
            tester.assertTextPresent("25/Aug/05 7:00 AM");
            tester.assertTextPresent(textEntry);
            tester.assertTextPresent("MultiCheckBox Three");
            tester.assertTextPresent("MultiSelect Two");
            tester.assertTextPresent(numberEntry);
            tester.assertTextPresent(PROJECT_HOMOSAP);
            tester.assertTextPresent("Radio Three");
            tester.assertTextPresent("Select List Three");
            tester.assertTextPresent(textEntry);
            tester.assertTextPresent(urlEntry);
            tester.assertTextPresent(ADMIN_USERNAME);
            tester.assertTextPresent(version_oneDotTwo);
        } else {
            tester.assertTextPresent("Cascade Three");
            tester.assertTextPresent("Cascade Beta");
            tester.assertTextPresent("23/Aug/05");
            tester.assertTextPresent("24/Aug/05 6:00 AM");
            tester.assertTextPresent(textEntry);
            tester.assertTextPresent("MultiCheckBox Two");
            tester.assertTextPresent("MultiSelect Three");
            tester.assertTextPresent(numberEntry);
            tester.assertTextPresent(PROJECT_HOMOSAP);
            tester.assertTextPresent("Radio Two");
            tester.assertTextPresent("Select List Two");
            tester.assertTextPresent(textEntry);
            tester.assertTextPresent(urlEntry);
            tester.assertTextPresent(userEntry);
            tester.assertTextPresent(version_oneDotOne);
        }
    }

    // Ensure all custom field values are initially correct on edit display
    private void editAndValidateIssue() {
        tester.clickLink("edit-issue");
        validateAndSetCustomFieldValues(true);
    }

    private void setupVersionPicker(final String version) {
        versionPickerId = addCustomFieldInProjects(CUSTOM_FIELD_TYPE_VERSION, CUSTOM_FIELD_NAME_VERSION_PICKER, PROJECT_HOMOSAP_KEY);
        administration.customFields().setDefaultValue(versionPickerId, version);

        // Add to Default Screen
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_VERSION_PICKER);
    }

    private void setupURLField(final String url) {
        urlId = addCustomField(CUSTOM_FIELD_TYPE_URL, CUSTOM_FIELD_NAME_URL_FIELD);
        administration.customFields().setDefaultValue(urlId, url);

        // Add to Default Screen
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_URL_FIELD);
    }

    private void setupSelectList(final Collection<String> options) {
        selectListId = addCustomField(CUSTOM_FIELD_TYPE_SELECT, CUSTOM_FIELD_NAME_SELECT_LIST);
        options.forEach(option -> administration.customFields().addOptions(selectListId, option));

        administration.customFields().setDefaultValue(selectListId, "10022");

        // Add to Default Screen
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_SELECT_LIST);
    }

    private void setupRadioButtons(final Collection<String> options) {
        radioButtonId = addCustomField(CUSTOM_FIELD_TYPE_RADIO, CUSTOM_FIELD_NAME_RADIO_BUTTONS);
        options.forEach(option -> administration.customFields().addOptions(radioButtonId, option));

        administration.customFields().setDefaultValue(radioButtonId, "10019");

        // Add to Default Screen
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_RADIO_BUTTONS);
    }

    private void setupNumberField(final String defaultValue) {
        final CustomFields customFields = administration.customFields();

        numberId = addCustomField(CUSTOM_FIELD_TYPE_FLOAT, CUSTOM_FIELD_NAME_NUMBER);
        customFields.setCustomFieldSearcher(numberId, "com.atlassian.jira.plugin.system.customfieldtypes:numberrange");
        if (StringUtils.isNotEmpty(defaultValue)) {
            customFields.setDefaultValue(numberId, defaultValue);
        }

        // Add to Default Screen
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_NUMBER);
    }

    private void setupMultiCheckboxes(final Collection<String> options) {
        multiCheckboxId = addCustomField(CUSTOM_FIELD_TYPE_MULTICHECKBOXES, CUSTOM_FIELD_NAME_MULTI_CHECKBOX);
        options.forEach(option -> administration.customFields().addOptions(multiCheckboxId, option));

        administration.customFields().setDefaultValue(multiCheckboxId, "10016");

        // Add to Default Screen
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_MULTI_CHECKBOX);
    }

    private void setupFreeText(final String text) {
        freeTextId = addCustomField(CUSTOM_FIELD_TYPE_FREETEXT, CUSTOM_FIELD_NAME_FREE_TEXT);
        administration.customFields().setDefaultValue(freeTextId, text);

        // Add to Default Screen
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_FREE_TEXT);
    }

    private void setupDatePicker(final String date) {
        datePickerId = addCustomField(CUSTOM_FIELD_TYPE_DATEPICKER, CUSTOM_FIELD_NAME_DATE_PICKER);
        administration.customFields().setDefaultValue(datePickerId, date);

        // Add to Default Screen
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_DATE_PICKER);
    }

    private void setupUserPicker(final String user) {
        userPickerId = addCustomField(CUSTOM_FIELD_TYPE_USERPICKER, CUSTOM_FIELD_NAME_USER_PICKER);
        administration.customFields().setDefaultValue(userPickerId, user);

        // Add to Default Screen
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_USER_PICKER);
    }

    private void setupTextField(final String text) {
        textFieldId = addCustomField(CUSTOM_FIELD_TYPE_TEXTFIELD, CUSTOM_FIELD_NAME_TEXT_FIELD);
        administration.customFields().setDefaultValue(textFieldId, text);

        // Add to Default Screen
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_TEXT_FIELD);
    }

    private void setupProjectPicker(final String projectId) {
        projectPickerId = addCustomField(CUSTOM_FIELD_TYPE_PROJECT, CUSTOM_FIELD_NAME_PROJECT_PICKER);
        administration.customFields().setDefaultValue(projectPickerId, projectId);

        // Add to Default Screen
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_PROJECT_PICKER);
    }

    private void setupMultiSelect(final Collection<String> options) {
        // Multi Select
        multiSelectId = addCustomField(CUSTOM_FIELD_TYPE_MULTISELECT, CUSTOM_FIELD_NAME_MULTI_SELECT);
        options.forEach(option -> administration.customFields().addOptions(multiSelectId, option));

        administration.customFields().setDefaultValue(multiSelectId, "10014");

        // Add to Default Screen
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_MULTI_SELECT);
    }

    private void setupDateTime(final String dateTime) {
        // Date Time
        dateTimeId = addCustomField(CUSTOM_FIELD_TYPE_DATETIME, CUSTOM_FIELD_NAME_DATE_TIME);
        administration.customFields().setDefaultValue(dateTimeId, dateTime);

        // Add to Default Screen
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_DATE_TIME);
    }

    private void setupCascadingSelect() {
        // Cascading Select
        cascadingSelectId = addCustomField(CUSTOM_FIELD_TYPE_CASCADINGSELECT, CUSTOM_FIELD_NAME_CASCADING_SELECT);
        administration.customFields().addOptions(cascadingSelectId, "Cascade One", "Cascade Two", "Cascade Three");
        tester.clickLinkWithText("Cascade One");
        tester.setFormElement("addValue", "Cascade A");
        tester.submit("Add");
        tester.setFormElement("addValue", "Cascade B");
        tester.submit("Add");
        tester.setFormElement("addValue", "Cascade C");
        tester.submit("Add");
        tester.clickLinkWithText("View Custom Field Configuration");
        tester.clickLinkWithText("Edit Options");
        tester.clickLinkWithText("Cascade Two");
        tester.setFormElement("addValue", "Cascade I");
        tester.submit("Add");
        tester.setFormElement("addValue", "Cascade II");
        tester.submit("Add");
        tester.setFormElement("addValue", "Cascade III");
        tester.submit("Add");
        tester.clickLinkWithText("View Custom Field Configuration");
        tester.clickLinkWithText("Edit Options");
        tester.clickLinkWithText("Cascade Three");
        tester.setFormElement("addValue", "Cascade Alhpa");
        tester.submit("Add");
        tester.setFormElement("addValue", "Cascade Beta");
        tester.submit("Add");
        tester.setFormElement("addValue", "Cascade Gamma");
        tester.submit("Add");

        administration.customFields().setCascadingSelectDefaultValue(cascadingSelectId, "Cascade Three", "Cascade Beta");

        // Add to Default Screen
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_CASCADING_SELECT);
    }

    @Test
    @Restore("TestCustomFields.xml")
    public void testAddingACustomFieldAndEditingTheConfiguration() {
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        // Add a Version Picker custom field
        tester.clickLink("add_custom_fields");
        tester.checkCheckbox("fieldType", "com.atlassian.jira.plugin.system.customfieldtypes:multiversion");
        tester.submit("nextBtn");

        // Add specific information for the custom field
        tester.setFormElement("fieldName", "Version Picker Custom Field");
        // Set the 'context'
        // Assign the CF to the Bug type
        tester.selectOption("issuetypes", "Bug");
        // Assign it to the homosapien project
        tester.checkCheckbox("global", "false");
        tester.selectOption("projects", "homosapien");
        tester.submit("nextBtn");

        // Associate with all screens
        tester.checkCheckbox("associatedScreens", "1");
        tester.checkCheckbox("associatedScreens", "2");
        tester.checkCheckbox("associatedScreens", "3");
        tester.submit("Update");

        // Configure and set the default value for the field
        tester.clickLink("config_customfield_10000");
        tester.clickLinkWithText("Edit Default Value");
        tester.selectOption("customfield_10000", "New Version 1");
        tester.submit();

        // Re-edit the configuration
        tester.clickLink("edit_10010");
        tester.selectOption("issuetypes", "Bug");
        tester.selectOption("issuetypes", "Improvement");
        tester.submit();
    }

    ////    **************  End Test All Custom Field Test  **************
    @Test
    @RestoreBlankInstance
    public void testCustomFieldsOrderingMultiCheckboxes() {
        logger.log("Testing Ordering of options for multicheckboxes field");

        final String fieldId = addCustomField(CUSTOM_FIELD_TYPE_MULTICHECKBOXES, CUSTOM_FIELD_NAME_MULTI_CHECKBOX);
        startCustomFieldsOrderingTest(fieldId, null);
    }

    @Test
    @RestoreBlankInstance
    public void testCustomFieldsOrderingCascadingSelect() {
        logger.log("Testing Ordering of options for cascading select field");

        final String fieldId = addCustomField(CUSTOM_FIELD_TYPE_CASCADINGSELECT, CUSTOM_FIELD_NAME_CASCADING_SELECT);
        final String parentOption = startCustomFieldsOrderingTest(fieldId, null);

        logger.log("Testing Ordering of options for a child option of the cascading select field");
        startCustomFieldsOrderingTest(fieldId, parentOption);
    }

    private String startCustomFieldsOrderingTest(final String fieldId, final String parentOption) {
        //add 'numberOfOptions' amount of option values to this custom field
        final int numberOfOptions = 6;
        final String[] optionValue = new String[numberOfOptions];
        final String[] optionId = new String[numberOfOptions];
        int i;
        for (i = 1; i < numberOfOptions; i++) {
            optionValue[i] = "Value_" + i;
            navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
            tester.clickLink("config_" + CUSTOM_FIELD_PREFIX + fieldId);
            tester.clickLinkWithText("Edit Options");
            tester.assertTextPresent("Edit Options for Custom Field");
            if (parentOption != null) {
                tester.clickLinkWithText(parentOption);
            }
            tester.setFormElement("addValue", optionValue[i]);
            tester.submit("Add");
            tester.assertTextPresent(optionValue[i]);

        }
        // Gather all the delete links
        i = 1;
        try {
            final WebLink[] links = tester.getDialog().getResponse().getLinks();
            for (final WebLink link : links) {
                if (link.getID().startsWith("del_")) {
                    optionId[i] = link.getID().substring(4, 9);
                    i++;
                }
            }
        } catch (final SAXException e) {
            e.printStackTrace();
        }

        tester.clickLinkWithText("Sort options alphabetically");
        final ArrowControlledOrdering arrowControlledOrdering = new ArrowControlledOrdering(tester, textAssertions, page, logger, form);
        arrowControlledOrdering.checkOrderingUsingArrows(optionValue, optionId);

        return arrowControlledOrdering.checkOrderingUsingMoveToPos(optionValue, optionId, "Option");
    }

    /* -------- Custom Fields tests -------- */

    public void customFieldsAddField() {
        logger.log("Test adding a custom field");
        fieldId_global = addCustomField(CUSTOM_FIELD_TYPE_RADIO, CUSTOM_FIELD_NAME_ONE);
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_ONE);

        // Add custom field to the default screen
        // The default screen has id of 1
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_ONE);
    }

    public void customFieldsDeleteField() {
        logger.log("Test deleting a custom field");
        administration.customFields().removeCustomField(CUSTOM_FIELD_PREFIX + fieldId_global);
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        tester.assertTextNotPresent(CUSTOM_FIELD_NAME_ONE);

        fieldId_global = addCustomField(CUSTOM_FIELD_TYPE_RADIO, CUSTOM_FIELD_NAME_ONE);
        logger.log("Field: " + fieldId_global);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_ONE);
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_ONE);

        fieldId_issue = addCustomFieldForIssueTypeNames(CUSTOM_FIELD_TYPE_PROJECT, CUSTOM_FIELD_NAME_TWO, "Bug");
        logger.log("Field: " + fieldId_issue);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_TWO);
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_TWO);

        fieldId_project = addCustomFieldInProjects(CUSTOM_FIELD_TYPE_DATETIME, CUSTOM_FIELD_NAME_THREE, PROJECT_HOMOSAP_KEY);
        logger.log("Field: " + fieldId_project);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_THREE);
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_THREE);
    }

    public void customFieldsAddCustomFieldOption() {
        logger.log("Test adding a custom field option");
        administration.customFields().addOptions(fieldId_global, "Blue");
        tester.assertTextPresent("Blue");
    }

    public void customFieldsDelCustomFieldOption() {
        logger.log("Test deleting a custom field option");
        administration.customFields().removeOptions(fieldId_global, "10000");
        tester.assertTextPresent("There are currently no options available for this select list");

        administration.customFields().addOptions(fieldId_global, "Blue", "Red", "White");
    }

    public void customFieldCreateIssueWithCustomFields() {
        logger.log("Create an issue with custom fields");
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");

        tester.setFormElement("summary", "issue with custom fields");
        tester.selectOption("priority", "Minor");
        tester.getDialog().setFormParameter(CUSTOM_FIELD_PREFIX + fieldId_global, "10001");
        tester.assertRadioOptionSelected(CUSTOM_FIELD_PREFIX + fieldId_global, "10001");
        tester.selectOption(CUSTOM_FIELD_PREFIX + fieldId_issue, PROJECT_HOMOSAP);
        tester.setFormElement(CUSTOM_FIELD_PREFIX + fieldId_project, "27/Jan/05 6:00 am");

        tester.submit("Create");

        tester.assertTextPresent("Details");
        tester.assertTextPresent(CUSTOM_FIELD_NAME_ONE);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_TWO);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_THREE);
        tester.clickLink("delete-issue");
        tester.submit("Delete");
    }

    public void customFieldCreateIssueWithFieldScope() {
        logger.log("Test the availibility of custom fields using field scope for creating an issue");
        navigation.issue().goToCreateIssueForm(PROJECT_NEO, "Bug");

        // project scope
        tester.assertFormElementNotPresent(CUSTOM_FIELD_PREFIX + fieldId_project);
        tester.assertFormElementPresent(CUSTOM_FIELD_PREFIX + fieldId_issue);

        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Improvement");

        // issue type scope
        tester.assertFormElementNotPresent(CUSTOM_FIELD_PREFIX + fieldId_issue);
        tester.assertFormElementPresent(CUSTOM_FIELD_PREFIX + fieldId_project);
    }

    public void customFieldsEditIssueWithCustomFields() {
        logger.log("Edit Issue: " + TEST_CUSTOM_FIELDS_XML_ISSUE_KEY_1 + " with custom fields");
        navigation.issue().gotoIssue(TEST_CUSTOM_FIELDS_XML_ISSUE_KEY_1);
        tester.clickLink("edit-issue");

        tester.getDialog().setFormParameter(CUSTOM_FIELD_PREFIX + fieldId_global, "10001");
        tester.assertRadioOptionSelected(CUSTOM_FIELD_PREFIX + fieldId_global, "10001");
        tester.selectOption(CUSTOM_FIELD_PREFIX + fieldId_issue, PROJECT_HOMOSAP);
        tester.setFormElement(CUSTOM_FIELD_PREFIX + fieldId_project, "27/Jan/05 6:00 am");

        tester.submit("Update");

        tester.assertTextPresent("Details");
        tester.assertTextPresent(CUSTOM_FIELD_NAME_ONE);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_TWO);
        tester.assertTextPresent(CUSTOM_FIELD_NAME_THREE);
    }

    public void customFieldEditIssueWithFieldScope() {
        logger.log("Test the availibility of custom fields using field scope for updating an issue");

        navigation.issue().gotoIssue(TEST_CUSTOM_FIELDS_XML_ISSUE_KEY_3);
        tester.clickLink("edit-issue");
        // project scope
        tester.assertFormElementNotPresent(CUSTOM_FIELD_PREFIX + fieldId_project);
        tester.assertFormElementPresent(CUSTOM_FIELD_PREFIX + fieldId_issue);

        navigation.issue().gotoIssue(TEST_CUSTOM_FIELDS_XML_ISSUE_KEY_2);
        tester.clickLink("edit-issue");
        // issue type scope
        tester.assertFormElementNotPresent(CUSTOM_FIELD_PREFIX + fieldId_issue);
        tester.assertFormElementPresent(CUSTOM_FIELD_PREFIX + fieldId_project);
    }

    public void customFieldsMoveIssueWithCustomFieldsforProject() {
        logger.log("Test the availibility of custom fields using field scope for moving an issue to a different project");

        // towards project
        navigation.issue().gotoIssue(TEST_CUSTOM_FIELDS_XML_ISSUE_KEY_3);
        tester.clickLink("move-issue");

        navigation.issue().selectProject(PROJECT_HOMOSAP);
        tester.submit();

        tester.assertTextPresent("Update the fields of the issue to relate to the new project.");

        tester.assertFormElementPresent(CUSTOM_FIELD_PREFIX + fieldId_project);
        // Global fields should only appear in the move if the field is required in the target project and does not have a current value
        tester.assertFormElementNotPresent(CUSTOM_FIELD_PREFIX + fieldId_global);
        // Issue type custom field should only appear if issue type is being changed or if the field has no current value for the issue
        // and is required in teh destination field configuration
        tester.assertFormElementNotPresent(CUSTOM_FIELD_PREFIX + fieldId_issue);

        // away from project
        navigation.issue().gotoIssue(TEST_CUSTOM_FIELDS_XML_ISSUE_KEY_2);
        tester.clickLink("move-issue");

        navigation.issue().selectProject(PROJECT_NEO);
        navigation.issue().selectIssueType("Bug");
        tester.submit();

        tester.assertTextPresent("Update the fields of the issue to relate to the new project.");

        tester.assertFormElementPresent(CUSTOM_FIELD_PREFIX + fieldId_issue);
        // Project custom field should only appear if project is being changed or if the field has no current value for the issue
        // and is required in teh destination field configuration
        tester.assertFormElementNotPresent(CUSTOM_FIELD_PREFIX + fieldId_project);
        // Global fields should only appear in the move if the field is required in the target project and does not have a current value
        tester.assertFormElementNotPresent(CUSTOM_FIELD_PREFIX + fieldId_global);
    }

    public void customFieldsMoveIssueWithCustomFieldsforIssueType() {
        logger.log("Test the availibility of custom fields using field scope for moving an issue to a different issue type");

        navigation.issue().gotoIssue(TEST_CUSTOM_FIELDS_XML_ISSUE_KEY_3);
        tester.clickLink("move-issue");

        navigation.issue().selectProject(PROJECT_HOMOSAP);
        tester.submit();

        tester.assertTextPresent("Update the fields of the issue to relate to the new project.");

        tester.assertFormElementNotPresent(CUSTOM_FIELD_PREFIX + fieldId_issue);
        tester.assertFormElementNotPresent(CUSTOM_FIELD_PREFIX + fieldId_global);
        tester.assertFormElementPresent(CUSTOM_FIELD_PREFIX + fieldId_project);
    }

    public void customFieldCreateSubTaskWithCustomFields() {
        try {
            administration.subtasks().enable();

            navigation.issue().gotoIssue(TEST_CUSTOM_FIELDS_XML_ISSUE_KEY_1);
            tester.clickLink("create-subtask");

            tester.assertTextPresent("Create Sub-Task");

            tester.assertFormElementPresent(CUSTOM_FIELD_PREFIX + fieldId_project);
            tester.assertFormElementNotPresent(CUSTOM_FIELD_PREFIX + fieldId_issue);

            navigation.issue().gotoIssue(TEST_CUSTOM_FIELDS_XML_ISSUE_KEY_3);
            tester.clickLink("create-subtask");

            tester.assertTextPresent("Create Sub-Task");

            tester.assertFormElementNotPresent(CUSTOM_FIELD_PREFIX + fieldId_project);
            tester.assertFormElementNotPresent(CUSTOM_FIELD_PREFIX + fieldId_issue);
        } finally {
            administration.subtasks().disable();
        }
    }

    public void customFieldWithFieldScreenSchemes() {
        logger.log("Test the availabilty of custom fields using the field screen schemes");

        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");
        tester.assertFormElementPresent(CUSTOM_FIELD_PREFIX + fieldId_global);
        tester.assertFormElementPresent(CUSTOM_FIELD_PREFIX + fieldId_project);
        tester.assertFormElementPresent(CUSTOM_FIELD_PREFIX + fieldId_issue);

        backdoor.screens().removeFieldFromScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_ONE);
        backdoor.screens().removeFieldFromScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_TWO);
        backdoor.screens().removeFieldFromScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_THREE);

        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");
        tester.assertFormElementNotPresent(CUSTOM_FIELD_PREFIX + fieldId_global);
        tester.assertFormElementNotPresent(CUSTOM_FIELD_PREFIX + fieldId_project);
        tester.assertFormElementNotPresent(CUSTOM_FIELD_PREFIX + fieldId_issue);

        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_ONE);
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_TWO);
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME_THREE);
    }

    @Test
    @Restore("TestVersionCustomFields.xml")
    public void testVersionCustomFieldPromptsForValuesInMove() {
        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("move-issue");
        navigation.issue().selectProject(PROJECT_MONKEY);
        tester.submit();
        tester.assertTextPresent("multi version picker cf");
        tester.selectOption("customfield_10000", "New Version 3");
        tester.assertTextPresent("single version picker cf");
        tester.selectOption("customfield_10001", "New Version 3");
        tester.submit();
        tester.submit("Move");
        navigation.issue().gotoIssue("MKY-1");
        tester.assertTextPresent("New Version 3");
        tester.assertTextNotPresent("New Version 2");
        tester.assertTextNotPresent("New Version 1");
    }

    @Test
    @Restore("TestVersionCustomFields.xml")
    public void testVersionCustomFieldPromptsForValuesInBulkMove() {
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        bulkOperations.bulkChangeSelectIssue("HSP-1");
        bulkOperations.chooseOperationBulkMove();
        navigation.issue().selectProject(PROJECT_MONKEY, "10010_1_pid");
        navigation.clickOnNext();
        tester.assertTextPresent("multi version picker cf");
        tester.selectOption("customfield_10000_10010", "New Version 3");
        tester.selectOption("customfield_10000_10011", "New Version 3");
        tester.assertTextPresent("single version picker cf");
        tester.selectOption("customfield_10001_10011", "New Version 3");
        navigation.clickOnNext();
        navigation.clickOnNext();
        bulkOperations.waitAndReloadBulkOperationProgressPage();
        navigation.issue().gotoIssue("MKY-1");
        textAssertions.assertTextPresent(new IdLocator(tester, "rowForcustomfield_10000"), "New Version 3");
        textAssertions.assertTextPresent(new IdLocator(tester, "rowForcustomfield_10001"), "New Version 3");
        tester.assertTextNotPresent("New Version 2");
        tester.assertTextNotPresent("New Version 1");
    }

    @Test
    @Restore("multiuser_cf_issue_perm.xml")
    @LoginAs(user = BOB_USERNAME)
    public void testViewIssueMultiUserCFIssueLevelSecurity() {
        _testViewIssuePerm();
    }

    @Test
    @Restore("multiuser_cf_perm_scheme.xml")
    @LoginAs(user = BOB_USERNAME)
    public void testViewIssueMultiUserCFPermissionScheme() {
        _testViewIssuePerm();
    }

    private void _testViewIssuePerm() {
        // make sure that we can see the issue since we have permission at the user cf level
        final String issueKey = "HSP-1";
        navigation.issue().gotoIssue(issueKey);
        tester.assertTextPresent("This is a test issue");
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);

        // make sure that we can see the issue through the issue navigator
        navigation.issueNavigator().createSearch("project=HSP AND issuetype=bug");
        tester.assertTextPresent("This is a test issue");

        // remove bob from the multiuser cf
        backdoor.issues().setIssueFields(issueKey, new IssueFields().customField(10001L, null));
        navigation.logout();
        navigation.login(BOB_USERNAME, BOB_PASSWORD);
        navigation.issue().gotoIssue(issueKey);
        assertions.getViewIssueAssertions().assertIssueNotFound();

        // make sure that we can not see the issue through the issue navigator
        // make sure that we can see the issue through the issue navigator
        navigation.issueNavigator().createSearch("project=HSP AND issuetype=bug");
        tester.assertTextNotPresent("This is a test issue");
    }

    /**
     * Editing issue types caused a custom field context configuration to lose its association with the issue type
     * because the (mutable) GenericValue was used as a key in the configs map (cache). Subsequent calls to see if the
     * exact same issue type key returned a field config failed because the key was then in the wrong hash bucket. This
     * resulted in the custom field not being shown on the view issue screen. Test test checks to see if this problem
     * occurs.
     * <p/>
     * NOTE that this test can produce false negatives (incorrect test passes) due to the necessity of causing the jdk
     * implementation to use a different bucket for the new issue type. Of course this doesn't happen in the fixed
     * implementation of the production code so there should be no false positives (incorrect failures)
     */
    @Test
    @RestoreBlankInstance
    public void testEditIssueTypeDoesNotCauseCustomFieldToDisappear() {
        // create an issue type
        final String issueTypeName = "todo";
        final String issueTypeId = backdoor.issueType().createIssueType(issueTypeName).getId();

        // now create a custom field associated to the issue type
        final String customFieldName = "TodoTextField";
        addCustomFieldForIssueTypeIds(CUSTOM_FIELD_TYPE_TEXTFIELD, customFieldName, issueTypeId);
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, customFieldName);

        // now create an issue and confirm the custom field is shown
        navigation.issue().goToCreateIssueForm("monkey", issueTypeName);
        tester.setFormElement("summary", "testing todo");
        tester.setFormElement("customfield_10000", "contents of the todo field");
        tester.submit("Create");
        tester.assertTextPresent("contents of the todo field");

        // now edit the issue type hoping its new hashcode changes which bucket it goes in
        // this update has proven sufficiently bucket-shifting
        navigation.gotoPage("/secure/admin/EditIssueType!default.jspa?id=" + issueTypeId);
        tester.setFormElement("name", issueTypeName);
        tester.setFormElement("description", "a todo item");
        tester.submit("Update");

        navigation.issue().gotoIssue("MKY-1"); // this should be the issue key

        // check we can still see our custom field
        tester.assertTextPresent("contents of the todo field");
        tester.assertTextPresent("TodoTextField:");
    }

    @Test
    @Restore("TestSelectCustomFieldBroken.xml")
    public void testEditSelectWithMultipleValuesFixesField() {
        navigation.issue().gotoIssue("HSP-1");

        tester.clickLinkWithText("HSP-1");
        tester.clickLink("edit-issue");
        tester.selectOption("customfield_10010", "dude");
        tester.submit("Update");

        tester.assertTextPresent("dude");
        tester.assertTextPresent("select cf");
    }

    // This test verifies JRA-12868 has been fixed. We make sure that even though the custom fields data is
    // invalid that the user can fix/edit the field in the UI.
    @Test
    @Restore("TestUserCustomFieldBroken.xml")
    public void testEditUserCFWithDeletedUsersValuesSelected() {
        navigation.issue().gotoIssue("HSP-1");

        tester.assertTextNotPresent(FRED_FULLNAME);
        tester.assertTextPresent(FRED_USERNAME);
        tester.assertTextPresent("user cf");

        tester.clickLinkWithText("HSP-1");
        tester.clickLink("edit-issue");
        tester.setFormElement("customfield_10000", ADMIN_USERNAME);
        tester.submit("Update");

        tester.assertTextPresent(ADMIN_FULLNAME);
        tester.assertTextPresent("user cf");
    }

    /**
     * Test that JRA-10461 has been fixed.  If issue types are deleted and a custom field is configured to use them then
     * the "edit configuration page" should be able to handle this.
     */
    @Test
    @RestoreBlankInstance
    public void testMissingIssueTypesForCustomField() {
        // add some issue types that will later be deleted
        final String issueTypeId = backdoor.issueType().createIssueType("A Thorn").getId();
        final String bugId = backdoor.issueType().getIssueTypes().stream().filter(it -> it.getName().equals("Bug")).findFirst().orElseThrow(AssertionError::new).getId();

        // config a custom field to use this issue type only
        addCustomFieldInProjectHomosapForIssueTypeIds(CUSTOM_FIELD_TYPE_VERSION, "versionP1", issueTypeId);
        addCustomFieldInProjectHomosapForIssueTypeIds(CUSTOM_FIELD_TYPE_VERSION, "versionP2", issueTypeId, bugId);

        // now delete that issue type
        tester.gotoPage("secure/admin/DeleteIssueType!default.jspa?id=" + issueTypeId);
        tester.submit("Delete");

        // and try to edit the custom field again.  It should succeed
        tester.clickLink("view_custom_fields");
        final String srcText = tester.getDialog().getResponseText();
        textAssertions.assertTextSequence(srcText, "versionP1", "Not configured for any context");
        textAssertions.assertTextSequence(srcText, "versionP2", "Issue type", "Project");
        textAssertions.assertTextSequence(srcText, "Not configured for any context", "versionP2");

        // and try to edit the custom field again.  It should succeed
        tester.clickLink("view_custom_fields");
        tester.clickLink("config_customfield_10001");
        tester.clickLink("edit_10011");
        tester.submit("Modify");

        tester.assertTextPresent("Default Configuration Scheme for versionP2");
    }

    @WebTest(TIME_ZONES)
    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testDateTimeCustomFieldShouldRespectUserTimeZone() throws Exception {
        final String HSP_1 = "HSP-1";
        final String SYDNEY_TZ = "Australia/Sydney";
        final String ROME_TZ = "Europe/Rome";

        administration.restoreData("TestCustomFields.xml");
        administration.generalConfiguration().setDefaultUserTimeZone(SYDNEY_TZ);

        // set up: create the custom field
        final String CF_NAME = "tzAwareDateTime";
        final String CF_ID = addCustomField(CUSTOM_FIELD_TYPE_DATETIME, CF_NAME);
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CF_NAME);

        final String DATE_1_SYDNEY = "10/Jan/99 12:00 PM";
        final String DATE_1_ROME = "10/Jan/99 2:00 AM";

        // 1. set DATE_1 from Sydney
        setCustomFieldValue(HSP_1, CF_ID, DATE_1_SYDNEY);

        // date should still show in Sydney time zone
        assertThat(customField(HSP_1, CF_ID).getText(), equalTo(DATE_1_SYDNEY));

        // 2. now fly over to Rome
        navigation.userProfile().changeUserTimeZone(ROME_TZ);

        // 3. read DATE_1 from Rome
        assertThat(customField(HSP_1, CF_ID).getText(), equalTo(DATE_1_ROME));

        final String DATE_2_ROME = "05/Jan/99 1:00 AM";
        final String DATE_2_SYDNEY = "05/Jan/99 11:00 AM";

        // 4. now enter a new date from Rome
        setCustomFieldValue(HSP_1, CF_ID, DATE_2_ROME);
        assertThat(customField(HSP_1, CF_ID).getText(), equalTo(DATE_2_ROME));

        // 5. fly back to Sydney and make sure the date is good
        navigation.userProfile().changeUserTimeZone(SYDNEY_TZ);
        assertThat(customField(HSP_1, CF_ID).getText(), equalTo(DATE_2_SYDNEY));
    }

    @WebTest(TIME_ZONES)
    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testDateCustomFieldShouldBeDisplayedInSystemTimeZone() throws Exception {
        final String HSP_1 = "HSP-1";
        final String ROME_TZ = "Europe/Rome";

        administration.restoreData("TestCustomFields.xml");

        // set up: create the date custom field
        final String CF_NAME = "dateOnly";
        final String CF_ID = addCustomField(CUSTOM_FIELD_TYPE_DATEPICKER, CF_NAME);
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CF_NAME);

        final String DATE_STRING_SYDNEY = "10/Jan/99";

        // set the date and check that it was saved correctly
        setCustomFieldValue(HSP_1, CF_ID, DATE_STRING_SYDNEY);
        assertThat(customField(HSP_1, CF_ID).getText(), equalTo(DATE_STRING_SYDNEY));

        // changing the default user time zone should *not* affect how dates are displayed
        administration.generalConfiguration().setDefaultUserTimeZone(ROME_TZ);
        assertThat(customField(HSP_1, CF_ID).getText(), equalTo(DATE_STRING_SYDNEY));

        // or how they are saved
        setCustomFieldValue(HSP_1, CF_ID, DATE_STRING_SYDNEY);
        assertThat(customField(HSP_1, CF_ID).getText(), equalTo(DATE_STRING_SYDNEY));

        // changing a user's time zone should still not affect how dates are displayed for that user
        navigation.userProfile().changeUserTimeZone(ROME_TZ);
        assertThat(customField(HSP_1, CF_ID).getText(), equalTo(DATE_STRING_SYDNEY));

        // or how they are saved
        setCustomFieldValue(HSP_1, CF_ID, DATE_STRING_SYDNEY);
        assertThat(customField(HSP_1, CF_ID).getText(), equalTo(DATE_STRING_SYDNEY));
    }

    private String addCustomField(final String type, final String name) {
        final String fullFieldType = builtInCustomFieldKey(type);
        final String id = administration.customFields().addCustomField(fullFieldType, name);
        return numericCfId(id);
    }

    private String addCustomFieldForIssueTypeNames(final String type, final String name, final String... issueTypeNames) {
        final Map<String, IssueTypeControl.IssueType> issueTypes = backdoor.issueType().getIssueTypes().stream()
                .collect(toMap(IssueTypeControl.IssueType::getName, identity()));
        final String[] issueTypeIds = Arrays.asList(issueTypeNames).stream()
                .map(issueTypeName -> issueTypes.get(issueTypeName).getId())
                .collect(CollectorsUtil.toImmutableListWithCapacity(issueTypeNames.length))
                .toArray(new String[issueTypeNames.length]);
        return addCustomFieldForIssueTypeIds(type, name, issueTypeIds);
    }

    private String addCustomFieldForIssueTypeIds(final String type, final String name, final String... issueTypeIds) {
        final String fullFieldType = builtInCustomFieldKey(type);
        final String id = administration.customFields().addCustomField(fullFieldType, name, issueTypeIds, new String[]{});
        return numericCfId(id);
    }

    private String addCustomFieldInProjects(final String type, final String name, final String... projectKeys) {
        final String[] projectIds = Arrays.asList(projectKeys).stream()
                .map(key -> backdoor.project().getProjectId(key).toString())
                .collect(CollectorsUtil.toImmutableListWithCapacity(projectKeys.length))
                .toArray(new String[projectKeys.length]);

        final String fullFieldType = builtInCustomFieldKey(type);
        final String id = administration.customFields().addCustomField(fullFieldType, name, new String[]{}, projectIds);
        return numericCfId(id);
    }

    private String addCustomFieldInProjectHomosapForIssueTypeIds(final String type, final String name, final String... issueTypeIds) {
        final String homosapId = backdoor.project().getProjectId(PROJECT_HOMOSAP_KEY).toString();
        final String fullFieldType = builtInCustomFieldKey(type);
        final String id = administration.customFields().addCustomField(fullFieldType, name, issueTypeIds, new String[]{homosapId});
        return numericCfId(id);
    }

    private void setCustomFieldValue(final String issueKey, final String cfId, final String dateString) {
        navigation.issue().gotoEditIssue(issueKey);
        tester.setFormElement(String.format("customfield_%s", cfId), dateString);
        tester.submit();
    }

    private CssLocator customField(final String issueKey, final String cfId) {
        navigation.issue().gotoIssue(issueKey);
        return locator.css(String.format("#customfield_%s-val", cfId));
    }
}
