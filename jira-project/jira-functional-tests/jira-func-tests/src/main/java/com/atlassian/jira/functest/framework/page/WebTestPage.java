package com.atlassian.jira.functest.framework.page;


/**
 * @since v6.0
 */
public interface WebTestPage {
    String baseUrl();

}
