package com.atlassian.jira.webtests.ztests.bigpipe;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import javax.inject.Inject;

import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.HTTP})
public class TestBigPipe extends BaseJiraFuncTest {
    @Inject
    private LocatorFactory locator;
    private BigPipeFeatureControl state;

    @Before
    public void setUpTest() {
        backdoor.restoreDataFromResource("TestEditIssue.xml");
        state = new BigPipeFeatureControl(backdoor);
    }

    @After
    public void tearDownTest() {
        state.restore();
    }

    /**
     * Bigpipe placeholder should be rendered on the page initially.
     */
    @Test
    public void testPlaceholderRendered() {
        state.setBigPipeKillswitchEnabled(false);

        navigation.login("admin");
        navigation.issue().viewIssue("HSP-1");

        Node placeholderNode = locator.xpath("//big-pipe[@data-id='activity-panel-pipe-id']").getNode();
        assertThat("Missing placeholder.", placeholderNode, notNullValue());
        assertThat("Wrong placeholder content.", placeholderNode.getTextContent().trim(), isEmptyString());
    }

    @Test
    public void testBigPipeDisabledByKillSwitch() {
        state.setBigPipeKillswitchEnabled(true);

        navigation.login("admin");
        navigation.issue().viewIssue("HSP-1");

        Node placeholderNode = locator.xpath("//big-pipe[@data-id='activity-panel-pipe-id']").getNode();
        assertThat("Bigpipe placeholder should not exist.", placeholderNode, nullValue());
    }
}