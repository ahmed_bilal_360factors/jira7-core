package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.common.collect.Maps;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import java.util.Map;

class ProjectCategoryClient extends RestApiClient<ProjectCategoryClient> {
    private static final String PROJECT_CATEGORY = "projectCategory";

    ProjectCategoryClient(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public ClientResponse getAllCategories() {
        final WebResource webResource = createResource().path(PROJECT_CATEGORY);
        return registerResponse(webResource.get(ClientResponse.class));
    }

    public ClientResponse getCategory(final String id) {
        final WebResource webResource = createResource().path(PROJECT_CATEGORY + "/" + id);
        return registerResponse(webResource.get(ClientResponse.class));
    }

    public ClientResponse createCategory(final String name, final String description) {
        final WebResource webResource = createResource().path(PROJECT_CATEGORY);

        final Map<String, String> postBody = Maps.newHashMap();
        postBody.put("name", name);
        postBody.put("description", description);

        return registerResponse(webResource.accept("application/json").type("application/json").post(ClientResponse.class, postBody));
    }

    public Long createCategoryAndReturnId(String name, String description) {
        ClientResponse response = createCategory(name, description);
        Map<String, String> json = response.getEntity(Map.class);
        return Long.parseLong(json.get("id"));
    }

    public ClientResponse deleteCategory(final String id) {
        final WebResource webResource = createResource().path(PROJECT_CATEGORY + "/" + id);
        return registerResponse(webResource.delete(ClientResponse.class));
    }
}
