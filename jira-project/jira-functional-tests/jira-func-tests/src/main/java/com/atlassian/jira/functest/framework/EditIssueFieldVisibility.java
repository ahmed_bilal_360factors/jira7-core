package com.atlassian.jira.functest.framework;

import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.meterware.httpunit.TableCell;
import com.meterware.httpunit.WebLink;
import com.meterware.httpunit.WebTable;
import net.sourceforge.jwebunit.WebTester;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.io.IOException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.AFFECTS_VERSIONS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.COMPONENTS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DUE_DATE_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_TABLE_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIX_VERSIONS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SECURITY_LEVEL_FIELD_ID;
import static org.junit.Assert.fail;

/**
 * @since v7.1
 */
public class EditIssueFieldVisibility {

    private static final String HIDE_FIELD_OPERATION_NAME = "Hide";
    private static final String SHOW_FIELD_OPERATION_NAME = "Show";
    private static final String OPTIONAL_FIELD_OPERATION_NAME = "Optional";

    private static final int FIELD_TABLE_FIELD_NAME_COLUMN_INDEX = 0;
    private static final int FIELD_TABLE_OPERATIONS_COLUMN_INDEX = 2;

    private static final String PAGE_ENTERPRISE_FIELD_CONFIGURATIONS = "/secure/admin/ViewFieldLayouts.jspa";

    private final Navigation navigation;
    private final WebTester tester;
    private final FuncTestLogger logger;

    @Inject
    public EditIssueFieldVisibility(final Navigation navigation,
                                    final WebTester tester,
                                    final FuncTestLogger logger) {
        this.navigation = navigation;
        this.tester = tester;
        this.logger = logger;
    }

    // changes a field from hide to show or vice wersa
    public void setHiddenFieldsOnEnterprise(final String fieldLayoutName, final String fieldName) {
        gotoFieldLayoutOnEnterprise(fieldLayoutName);
        assertViewIssueFields();
        doFieldOperation(fieldName, HIDE_FIELD_OPERATION_NAME);
    }

    private void gotoFieldLayoutOnEnterprise(final String fieldLayoutName) {
        navigation.gotoPage(PAGE_ENTERPRISE_FIELD_CONFIGURATIONS);
        tester.assertTextPresent("View Field Configurations");
        tester.clickLinkWithText(fieldLayoutName);
    }

    public void setShownFieldsOnEnterprise(final String fieldLayoutName, final String fieldName) {
        gotoFieldLayoutOnEnterprise(fieldLayoutName);
        assertViewIssueFields();
        doFieldOperation(fieldName, SHOW_FIELD_OPERATION_NAME);
    }

    public void setRequiredFieldsOnEnterprise(final String fieldLayoutName, final String fieldName) {
        gotoFieldLayoutOnEnterprise(fieldLayoutName);
        assertViewIssueFields();
        setRequiredField(fieldName);
    }

    public void setOptionalFieldsOnEnterprise(final String fieldLayoutName, final String fieldName) {
        gotoFieldLayoutOnEnterprise(fieldLayoutName);
        assertViewIssueFields();
        doFieldOperation(fieldName, OPTIONAL_FIELD_OPERATION_NAME);
    }

    public void setRequiredField(final String fieldName) {
        assertViewIssueFields();

        try {
            final WebTable fieldTable = tester.getDialog().getResponse().getTableWithID(FIELD_TABLE_ID);
            // First row is a headings row so skip it
            for (int i = 1; i < fieldTable.getRowCount(); i++) {
                final String field = fieldTable.getCellAsText(i, FIELD_TABLE_FIELD_NAME_COLUMN_INDEX);
                if (field.contains(fieldName)) {
                    final TableCell linkCell = fieldTable.getTableCell(i, FIELD_TABLE_OPERATIONS_COLUMN_INDEX);
                    final WebLink requiredLink = linkCell.getLinkWith("Required");
                    if (requiredLink == null) {
                        fail("Cannot find 'required' link for field '" + fieldName + "'.");
                    } else {
                        requiredLink.click();
                        return;
                    }
                }
            }

            fail("Cannot find field with id '" + fieldName + "'.");
        } catch (final SAXException e) {
            fail("Cannot find table with id '" + FIELD_TABLE_ID + "'.");
            e.printStackTrace();
        } catch (final IOException e) {
            fail("Cannot click 'required' link for field id '" + fieldName + "'.");
        }
    }

    // Sets fields to be required
    public void setRequiredFields() {
        resetFields();

        logger.log("Set fields to be required");

        gotoViewIssueFields();
        setRequiredField(AFFECTS_VERSIONS_FIELD_ID);
        setRequiredField(FIX_VERSIONS_FIELD_ID);
        setRequiredField(COMPONENTS_FIELD_ID);
    }

    public void setHiddenFields(final String fieldName) {
        logger.log("Hide field " + fieldName);
        gotoViewIssueFields();
        assertViewIssueFields();
        doFieldOperation(fieldName, HIDE_FIELD_OPERATION_NAME);
    }

    public void setShownFields(final String fieldName) {
        logger.log("Show field " + fieldName);
        gotoViewIssueFields();
        assertViewIssueFields();
        doFieldOperation(fieldName, SHOW_FIELD_OPERATION_NAME);
    }

    public void doFieldOperation(final String fieldName, final String linkName) {
        assertViewIssueFields();

        try {
            final WebTable fieldTable = tester.getDialog().getResponse().getTableWithID(FIELD_TABLE_ID);
            // First row is a headings row so skip it
            for (int i = 1; i < fieldTable.getRowCount(); i++) {
                final String field = fieldTable.getCellAsText(i, FIELD_TABLE_FIELD_NAME_COLUMN_INDEX);
                if (field.contains(fieldName)) {
                    final TableCell linkCell = fieldTable.getTableCell(i, FIELD_TABLE_OPERATIONS_COLUMN_INDEX);
                    final WebLink link = linkCell.getLinkWith(linkName);
                    if (link == null) {
                        // This is usually OK, as this happens when e.g. hiding a field that is already hidden
                        logger.log("Link with name '" + linkName + "' does not exist.");
                        return;
                    } else {
                        link.click();
                        return;
                    }
                }
            }
            fail("Cannot find field with id '" + fieldName + "'.");
        } catch (final SAXException e) {
            fail("Cannot find table with id '" + FIELD_TABLE_ID + "'.");
            e.printStackTrace();
        } catch (final IOException e) {
            fail("Cannot click '" + linkName + "' link for field id '" + fieldName + "'.");
        }
    }

    public void resetFields() {
        logger.log("Restore default field settings");
        gotoViewIssueFields();
        if (tester.getDialog().isLinkPresentWithText("Restore Defaults")) {
            tester.clickLinkWithText("Restore Defaults");
        }
    }

    public void setDueDateToRequried() {
        resetFields();
        logger.log("Set 'Due Date' Field to required");
        gotoViewIssueFields();
        setRequiredField(DUE_DATE_FIELD_ID);

    }

    public void setSecurityLevelToRequried() {
        resetFields();
        logger.log("Set 'Security Level' Field to required");
        gotoViewIssueFields();

        setRequiredField(SECURITY_LEVEL_FIELD_ID);
    }

    public void assertViewIssueFields() {
        tester.assertTextPresent("View Field Configuration");
    }

    private void gotoViewIssueFields() {
        navigation.gotoPage(PAGE_ENTERPRISE_FIELD_CONFIGURATIONS);
        tester.assertTextPresent("View Field Configurations");
        tester.clickLinkWithText("Configure");
        tester.assertTextPresent("View Field Configuration");
    }
}
