package com.atlassian.jira.webtests.ztests.email;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.backdoor.ApplicationRoleControl;
import com.atlassian.jira.functest.framework.page.ViewIssuePage;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.test.util.lic.ApplicationLicenseConstants;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.webtests.EmailBaseFuncTestCase;
import com.atlassian.jira.webtests.JIRAServerSetup;
import com.atlassian.jira.webtests.LicenseKeys;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ISSUES;
import static com.jayway.awaitility.Awaitility.await;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.EMAIL})
@LoginAs(user = FunctTestConstants.ADMIN_USERNAME)
/**
 * If you're running JIRA through jmake for this test, use "jmake debug --enable-mail" to enable mail.
 */
public class TestHandlers extends EmailBaseFuncTestCase {
    // things are slow, 20 seconds is enough, if it becomes flakey, please increase timeout or
    // find better way of detecting whether mailbox was cleared or not
    private static final int MAILBOX_PROCESSING_TIMEOUT = 20;

    private static final String NEW_USER_NAME = "freddie@example.com";
    private static final String EXISTING_USER_NAME = "fred@example.com";
    private static final String INCOMING_EMAIL = "admin@example.com";
    private static final String ISSUE_SUMMARY = "the monkeys escaped";
    private static final String OTHER_ISSUE_SUMMARY = "boiling eggs";

    @Inject
    private Administration administration;

    /**
     * As part of https://jdog.atlassian.com/browse/JRADEV-7738 port was removed from service settings
     */
    @Test
    public void testPortsNotInService() {
        backdoor.restoreBlankInstance();

        configureAndStartMailServers(INCOMING_EMAIL, "PRE", JIRAServerSetup.SMTP, JIRAServerSetup.IMAP, JIRAServerSetup.POP3);
        setupPopService();
        tester.assertTextNotPresent("port:</strong> " + mailService.getPop3Port());

        setupImapService();
        tester.assertTextNotPresent("port:</strong> " + mailService.getImapPort());
    }


    @Test
    public void testCreateIssueFromEmail() throws Exception {
        backdoor.restoreBlankInstance();

        configureMailAndSendMessage("fred@example.com");

        processIncomingMail();

        final ViewIssuePage viewIssuePage = navigation.issue().viewIssue("MKY-1");
        assertEquals("Fred Normal", viewIssuePage.getReporter());
    }

    @Test
    public void testMailHandlerShouldRespectApplicationAccess() throws Exception {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_AND_TEST_ROLES);

        final ApplicationRoleControl roleClient = backdoor.applicationRoles();

        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.CORE_KEY, false, ImmutableList.of(JIRA_USERS_GROUP), ImmutableList.of(JIRA_USERS_GROUP));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, true, ImmutableList.of(JIRA_DEV_GROUP), ImmutableList.of(JIRA_DEV_GROUP));

        final ApplicationRoleControl.ApplicationRoleBean testRole = roleClient.getRole(ApplicationLicenseConstants.TEST_KEY);

        addCreatePermissionToGroup(JIRA_DEV_GROUP);

        configureMailAndSendMessage(NEW_USER_NAME);

        processIncomingMail();

        final List<String> expectedGroups = testRole.getDefaultGroups();
        final List<String> actualGroups = testRole
                .getDefaultGroups().stream()
                .filter(groupName -> backdoor.usersAndGroups().isUserInGroup(NEW_USER_NAME, groupName))
                .collect(CollectorsUtil.toImmutableList());

        assertEquals("User was added to default app default groups", expectedGroups, actualGroups);
        assertFalse("User was not added to non default app default groups", backdoor.usersAndGroups().isUserInGroup(NEW_USER_NAME, JIRA_USERS_GROUP));

        flushMailQueueAndWait(1);
    }

    @Test
    public void testMailHandlerShouldCreateIssueWhenNonDefaultAppLicenseExceeded() throws Exception {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_AND_TEST_ROLES);

        final ApplicationRoleControl roleClient = backdoor.applicationRoles();

        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.CORE_KEY, false, ImmutableList.of(JIRA_USERS_GROUP), ImmutableList.of(JIRA_USERS_GROUP));

        backdoor.usersAndGroups().addUsersWithGroup("user", "User", 3, JIRA_USERS_GROUP);

        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, true, ImmutableList.of(JIRA_DEV_GROUP), ImmutableList.of(JIRA_DEV_GROUP));
        addCreatePermissionToGroup(JIRA_DEV_GROUP);

        configureMailAndSendMessage(NEW_USER_NAME);

        processIncomingMail();

        assertTrue("User was created", backdoor.usersAndGroups().userExists(NEW_USER_NAME));
        assertNotNull("Issue was created", getIssue(ISSUE_SUMMARY));

        flushMailQueueAndWait(1);
    }

    @Test
    public void testMailHandlerShouldNotCreateIssueWhenDefaultAppLicenseExceeded() throws Exception {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_AND_TEST_ROLES);

        final ApplicationRoleControl roleClient = backdoor.applicationRoles();

        // Test Product is default and has exceeded license limit,
        // however at the same time we should be able to create issues
        // for an user which belonges to JIRA Core
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.CORE_KEY, false, ImmutableList.of(JIRA_USERS_GROUP), ImmutableList.of(JIRA_USERS_GROUP));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, true, ImmutableList.of(JIRA_DEV_GROUP), ImmutableList.of(JIRA_DEV_GROUP));

        backdoor.usersAndGroups().addUsersWithGroup("dev", "Developer", 3, JIRA_DEV_GROUP);

        addCreatePermissionToGroup(JIRA_DEV_GROUP);

        configureMailAndSendMessage(NEW_USER_NAME);
        // send additional mail that will be consumed
        // this way we can detect whether inbox was processed
        mailService.sendTextMessage(INCOMING_EMAIL, EXISTING_USER_NAME, OTHER_ISSUE_SUMMARY, "nothing");
        flushMailQueue();

        runProcessingService();
        waitUntilInboxClear(1);

        assertFalse("User was not created", backdoor.usersAndGroups().userExists(NEW_USER_NAME));
        assertNull("Issue was not created", getIssue(ISSUE_SUMMARY));
        assertNotNull("Other issue was created", getIssue(OTHER_ISSUE_SUMMARY));

        // one mail should be out from queue
        // this is important, if there is something left in queue after test,
        // it will interfere with next test that involves sending mail,
        // since restore instance does not clear mail queue
        flushMailQueueAndWait(1);
    }

    private Issue getIssue(final String issueSummary) {
        return backdoor.search()
                .postSearch(new SearchRequest().jql(String.format("summary ~ \"%s\"", issueSummary)))
                .issues.stream()
                .findFirst()
                .orElse(null);
    }

    private void processIncomingMail() throws Exception {
        runProcessingService();

        waitUntilInboxClear(0);
    }

    private void runProcessingService() {
        final long serviceId = administration.services().goTo().getIdForServiceName("imap");
        administration.utilities().runServiceNow(serviceId);
    }

    private void configureMailAndSendMessage(final String sender) {
        configureAndStartMailServers(INCOMING_EMAIL, "PRE", JIRAServerSetup.SMTP, JIRAServerSetup.IMAP);
        mailService.addUser(INCOMING_EMAIL, ADMIN_USERNAME, ADMIN_PASSWORD);
        setupImapService();
        mailService.sendTextMessage(INCOMING_EMAIL, sender, ISSUE_SUMMARY, "aarrrgh!");
        flushMailQueue();
    }

    private void waitUntilInboxClear(final int expectedMessageCount) throws Exception {
        await().atMost(MAILBOX_PROCESSING_TIMEOUT, TimeUnit.SECONDS)
                .until(() -> mailService.getUserInbox(INCOMING_EMAIL).getMessageCount(), equalTo(expectedMessageCount));
    }

    private void addCreatePermissionToGroup(final String group) {
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, CREATE_ISSUES, group);
    }
}
