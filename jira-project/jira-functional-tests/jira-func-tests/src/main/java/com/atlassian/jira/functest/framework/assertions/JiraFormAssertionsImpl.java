package com.atlassian.jira.functest.framework.assertions;

import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LocatorFactoryImpl;
import com.atlassian.jira.functest.framework.locator.Locator;
import com.atlassian.jira.functest.framework.locator.LocatorEntry;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;
import org.junit.Assert;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * And implementation of {@link JiraFormAssertions}.
 *
 * @since v3.13
 */
public class JiraFormAssertionsImpl implements JiraFormAssertions {
    private static final String AUI_MESSAGE_ERROR_CSS = ".aui-message.error";
    private static final String AUI_MESSAGE_WARNING_CSS = ".aui-message.warning";
    private static final String AUI_MESSAGE_INFO_CSS = ".aui-message.info";

    private final TextAssertions textAssertions;
    private final WebTester tester;
    private final LocatorFactory locators;

    @Inject
    public JiraFormAssertionsImpl(final TextAssertions textAssertions, final WebTester tester, final JIRAEnvironmentData environmentData) {
        this.textAssertions = textAssertions;
        this.tester = tester;
        this.locators = new LocatorFactoryImpl(tester);
    }

    public void assertFieldErrMsg(final String expectedText) {
        final Locator error = createFieldErrorMessageLocator();
        textAssertions.assertTextPresent(error, expectedText);
    }

    public void assertAuiFieldErrMsg(final String expectedText) {
        final Locator error = createAuiFieldErrorMessageLocator();
        textAssertions.assertTextPresent(error, expectedText);
    }

    public void assertFormErrMsg(final String expectedText) {
        final Locator error = createFormErrorMessageLocator();
        textAssertions.assertTextPresent(error, expectedText);
    }

    public void assertFormErrMsgContainsLink(final String linkExactText) {
        final Locator linksInErrorMsg = locators.css(AUI_MESSAGE_ERROR_CSS + " a");
        if (!nodeWithTextExists(linkExactText, linksInErrorMsg)) {
            fail("Link with text '" + linkExactText + "' not found in any error message");
        }
    }

    private boolean nodeWithTextExists(final String linkExactText, final Locator linksInErrorMsg) {
        for (final LocatorEntry link : linksInErrorMsg.allMatches()) {
            if (linkExactText.equals(link.getText())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void assertNoFieldErrMsg(final String notExpectedText) {
        textAssertions.assertTextNotPresent(createFieldErrorMessageLocator(), notExpectedText);
    }

    @Override
    public void assertNoFormErrMsg(final String notExpectedText) {
        textAssertions.assertTextNotPresent(createFormErrorMessageLocator(), notExpectedText);
    }

    @Override
    public void assertNoErrorsPresent() {
        Locator errorLocator = createFieldErrorMessageLocator();
        Assert.assertNull("Expected no errors on the page, but there was a field with an error.", errorLocator.getNode());

        errorLocator = createFormErrorMessageLocator();
        Assert.assertNull("Expected no errors on the page, but the page had a global error.", errorLocator.getNode());
    }

    @Override
    public void assertFormNotificationMsg(final String expectedText) {
        final Locator notification = createFormNotificationMessageLocator();
        textAssertions.assertTextPresent(notification, expectedText);
    }

    @Override
    public void assertNoFormNotificationMsg(final String notExpectedText) {
        textAssertions.assertTextNotPresent(createFormNotificationMessageLocator(), notExpectedText);
    }

    public void assertFormNotificationMsgContainsLink(final String linkExactText) {
        final Locator linksInErrorMsg = locators.css(AUI_MESSAGE_INFO_CSS + " a");
        if (!nodeWithTextExists(linkExactText, linksInErrorMsg)) {
            fail("Link with text '" + linkExactText + "' not found in any notification message");
        }
    }

    @Override
    public void assertFormSuccessMsg(final String expectedText) {
        final Locator notification = createFormSuccessMessageLocator();
        textAssertions.assertTextPresent(notification, expectedText);
    }

    @Override
    public void assertFormWarningMessage(final String messageText) {
        textAssertions.assertTextPresent(createFormWarningMessageLocator(), messageText);
    }

    @Override
    public void assertNoFormWarningMessage(final String notExpectedText) {
        textAssertions.assertTextNotPresent(createFormWarningMessageLocator(), notExpectedText);
    }

    public void assertFormWarningMessageContainsLink(final String linkExactText) {
        final Locator linksInMsg = locators.css(AUI_MESSAGE_WARNING_CSS + " a");
        if (!nodeWithTextExists(linkExactText, linksInMsg)) {
            fail("Link with text '" + linkExactText + "' not found in any warning message");
        }
    }

    @Override
    public void assertSelectElementHasOptionSelected(final String selectElementName, final String optionName) {
        final String actual = tester.getDialog().getSelectedOption(selectElementName);
        Assert.assertEquals(
                "Expected option selected '" + optionName + "' was not selected in form element '" + selectElementName + "'. Actual selected option was '" + actual + "'.",
                optionName, actual);
    }

    @Override
    public void assertSelectElementByIdHasOptionSelected(final String selectElementId, final String optionName) {
        final List<String> selectedOptionIds = Arrays.asList(tester.getDialog().getForm().getParameterValues(selectElementId));
        final String[] optionValues = tester.getDialog().getOptionsFor(selectElementId);//display names
        for (int i = 0; i < optionValues.length; i++) {
            final String optionValue = optionValues[i];
            if (optionValue.equals(optionName)) {
                final String expectedValueId = tester.getDialog().getOptionValuesFor(selectElementId)[i];
                assertTrue("Expected option '" + optionName + "' for element '" + selectElementId + "' was not selected", selectedOptionIds.contains(expectedValueId));
                return;
            }
        }

        fail("Expected option value: '" + optionName + "' is not a selected value option for '" + selectElementId + "'");
    }

    @Override
    public void assertSelectElementByIdHasOptionSelectedById(final String selectElementId, final String optionId) {
        final List<String> selectedOptionIds = Arrays.asList(tester.getDialog().getForm().getParameterValues(selectElementId));
        assertTrue("Expected selected option '" + optionId + "' for element '" + selectElementId + "'.", selectedOptionIds.contains(optionId));
    }

    private Locator createFieldErrorMessageLocator() {
        return new XPathLocator(tester, "//span[@class='errMsg']");
    }

    private Locator createAuiFieldErrorMessageLocator() {
        return new XPathLocator(tester, "//form[@class='aui']//div[@class='field-group']/div[@class='error']");
    }

    private Locator createFormErrorMessageLocator() {
        return locators.css(AUI_MESSAGE_ERROR_CSS);
    }

    private Locator createFormNotificationMessageLocator() {
        return locators.css(AUI_MESSAGE_INFO_CSS);
    }

    private Locator createFormSuccessMessageLocator() {
        return locators.css(".aui-message.success");
    }

    private Locator createFormWarningMessageLocator() {
        return locators.css(AUI_MESSAGE_WARNING_CSS);
    }
}