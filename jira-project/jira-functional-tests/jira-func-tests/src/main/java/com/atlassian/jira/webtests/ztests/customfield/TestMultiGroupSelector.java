package com.atlassian.jira.webtests.ztests.customfield;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HttpUnitConfiguration;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.Groups;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_PREFIX;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_MULTIGROUPPICKER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_FIELD_SCREEN_NAME;

@WebTest({Category.FUNC_TEST, Category.BROWSING, Category.CUSTOM_FIELDS})
@RestoreBlankInstance
@HttpUnitConfiguration(enableScripting = true)
@LoginAs(user = ADMIN_USERNAME)
public class TestMultiGroupSelector extends BaseJiraFuncTest {
    public static final String MULTGROUP_PICKER_CF_NAME = "mypickerofmultigroups";
    public static final String INVALID_GROUP_NAME = "invalid_group_name";
    public static final String ISSUE_SUMMARY = "This is my summary";

    private String multigroupPickerId = null;

    @Inject
    private Administration administration;

    @Before
    public void setUp() {
        // Add GroupPicker custom field
        final String customFieldId = administration.customFields().addCustomField("com.atlassian.jira.plugin.system.customfieldtypes:" + CUSTOM_FIELD_TYPE_MULTIGROUPPICKER, MULTGROUP_PICKER_CF_NAME);
        multigroupPickerId = customFieldId.split("_")[1];
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, MULTGROUP_PICKER_CF_NAME);
    }

    @Test
    public void testCreateIssueWithMultiGroupPicker() throws SAXException {
        // Start the create issue operation
        navigation.issue().goToCreateIssueForm(null, null);
        tester.setFormElement("summary", ISSUE_SUMMARY);

        // Assert that the group picker link is available
        tester.assertLinkPresent(CUSTOM_FIELD_PREFIX + multigroupPickerId + "-trigger");

        // Attempt to add invalid group name
        tester.setFormElement(CUSTOM_FIELD_PREFIX + multigroupPickerId, INVALID_GROUP_NAME + ", " + INVALID_GROUP_NAME);
        tester.submit("Create");
        tester.assertTextPresent("Could not find group names: " + INVALID_GROUP_NAME + ", " + INVALID_GROUP_NAME);

        tester.setFormElement(CUSTOM_FIELD_PREFIX + multigroupPickerId, Groups.USERS + ", " + Groups.DEVELOPERS);
        tester.submit("Create");

        tester.assertTextPresent(ISSUE_SUMMARY);
        tester.assertTextPresent(MULTGROUP_PICKER_CF_NAME);
        tester.assertTextPresent(Groups.USERS);
        tester.assertTextPresent(Groups.DEVELOPERS);
    }

    @Test
    public void testIssueNavWithMultiGroupPicker() {
        // Create issue with JIRA-USER group selected
        navigation.issue().goToCreateIssueForm(null, null);
        tester.setWorkingForm("issue-create");
        tester.setFormElement("summary", ISSUE_SUMMARY);
        tester.setFormElement(CUSTOM_FIELD_PREFIX + multigroupPickerId, Groups.USERS + ", " + Groups.DEVELOPERS);
        tester.submit("Create");

        tester.clickLink("find_link");
        tester.assertTextPresent(MULTGROUP_PICKER_CF_NAME);

        // Search for valid group
        navigation.issueNavigator().createSearch(MULTGROUP_PICKER_CF_NAME + " = " + Groups.USERS);
        tester.assertTextPresent("HSP-1");

        navigation.issueNavigator().createSearch(MULTGROUP_PICKER_CF_NAME + " = " + Groups.DEVELOPERS);
        tester.assertTextPresent("HSP-1");
    }
}
