package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestOut;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.atlassian.jira.webtests.util.LocalTestEnvironmentData;
import com.atlassian.jira.webtests.util.TestClassUtils;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * A base class for JIRA functional test suites.  It handles TestCase classes and ensures that a test instance is never
 * added more than once.  It also keeps track of the various JIRA editions and what test should run within them.
 * <p>
 * Under the covers it uses LinkedHashSets which means the test are maintained in "insertion order" but not test is
 * returned more than once.
 *
 * @since v4.0
 */
public class FuncTestSuite {
    private final Set<Class<?>> funcTests = new LinkedHashSet<>();
    private final Set<Class<?>> bundledPlugins2Tests = new LinkedHashSet<>();
    private final Set<Class<?>> tpmLdapTests = new LinkedHashSet<>();

    /**
     * This gets the test suite based on the func test settings that you currently have.
     *
     * @return a suite of test classes to run
     */
    public TestSuite createTest() {
        return createTest(LocalTestEnvironmentData.DEFAULT);
    }

    /**
     * This gets the suite of tests based on the passed environment.
     *
     * @param environment the environment to used to select the tests.
     * @return a Test suite of classes to run
     */
    public TestSuite createTest(JIRAEnvironmentData environment) {
        final Set<Class<?>> tests = getTests(environment);

        TestSuiteBuilder builder = createFuncTestBuilder();

        builder.addTests(tests);

        return builder.build();
    }

    /**
     * Returns a set of test classes that are deemed as test that should run on based on the JIRA edition inside the
     * JIRAEnvironmentData object.
     *
     * @param environmentData the edition to use is in here
     * @return the of test classes that are deemed as test that should run on based on the JIRA edition, in insertion
     * order
     */
    public Set<Class<?>> getTests(JIRAEnvironmentData environmentData) {
        Set<Class<?>> tests;
        if (environmentData.isSingleNamedTest()) {
            // ignore configuration and just add a specifically named test
            tests = new LinkedHashSet<>();
            tests.add(environmentData.getSingleTestClass());
        } else if (environmentData.isAllTests()) {
            tests = new LinkedHashSet<>();
            tests.addAll(getFuncTests());
            tests.addAll(getBundledPlugins2Tests());
        } else if (environmentData.isBundledPluginsOnly()) {
            tests = getBundledPlugins2Tests();
        } else if (environmentData.isTpmLdapTests()) {
            tests = getTpmLdapTests();
        } else {
            tests = getFuncTests();
        }
        return tests;
    }

    protected TestSuiteBuilder createFuncTestBuilder() {
        final String numberOfBatches = System.getProperty("atlassian.test.suite.numbatches");
        final String batchNumber = System.getProperty("atlassian.test.suite.batch");
        if (numberOfBatches != null && batchNumber != null) {
            String batchInfo = "Batch " + batchNumber + " of " + numberOfBatches;
            try {
                int numBatches = Integer.parseInt(numberOfBatches);
                int batch = Integer.parseInt(batchNumber);
                if (batch > 0 && batch <= numBatches) {
                    FuncTestOut.out.println(batchInfo);
                    return new TestSuiteBuilder(batch, numBatches).log(true);
                } else {
                    FuncTestOut.out.println("Batch mode FAIL. Batch information looks wrong-arse: " + batchInfo);
                }
            } catch (NumberFormatException e) {
                FuncTestOut.err.println("Batch mode FAIL. Batch information cannot be properly interpreted: " + batchInfo);
                e.printStackTrace(FuncTestOut.err);
                // will fall back to unbatched mode
            }
        }
        return new TestSuiteBuilder().log(true);
    }

    /**
     * @return the set of test classes that are deemed as test that should run on JIRA ENTERPRISE edition, in insertion
     * order.
     */
    public Set<Class<?>> getFuncTests() {
        return new LinkedHashSet<>(funcTests);
    }

    /**
     * @return the set of test classes that are deemed as test that should run for bundled plugins in insertion order.
     */
    public Set<Class<?>> getBundledPlugins2Tests() {
        return new LinkedHashSet<>(bundledPlugins2Tests);
    }

    public Set<Class<?>> getTpmLdapTests() {
        return new LinkedHashSet<>(tpmLdapTests);
    }

    public FuncTestSuite addBundledPluginsTests(List<Class<?>> testClasses) {
        testClasses.forEach(this::addBundledPluginsTest);
        return this;
    }

    /**
     * This will add the test class into the suite as a bundled plugins 2.0 test only.
     *
     * @param testClass the test case class to add
     * @return <b>this</b> (in order to create a fluent style)
     */
    public FuncTestSuite addBundledPluginsTest(Class<?> testClass) {
        assertItsATest(testClass);
        bundledPlugins2Tests.add(testClass);
        return this;
    }

    /**
     * This will add the test class into the suite.
     *
     * @param testClass the test case class to add
     * @return <b>this</b> (in order to create a fluent style)
     */
    public FuncTestSuite addTest(Class<?> testClass) {
        assertItsATest(testClass);
        if (isLdapTest(testClass)) {
            tpmLdapTests.add(testClass);
        } else {
            funcTests.add(testClass);
        }
        return this;
    }

    private boolean isLdapTest(final Class<?> testClass) {
        final WebTest annotation = testClass.getAnnotation(WebTest.class);
        return annotation != null && Arrays.stream(annotation.value()).anyMatch(category -> category == Category.LDAP);
    }

    /**
     * This will add a collection of test classes into the suite as a STANDARD, PROFESSIONAL and ENTERPRISE tests.
     *
     * @param testClasses a collection of test case classes
     * @return <b>this</b> (in order to create a fluent style)
     */
    public FuncTestSuite addTests(final Collection<Class<?>> testClasses) {
        for (final Class<?> testCaseClass : testClasses) {
            addTest(testCaseClass);
        }
        return this;
    }

    private static void assertItsATest(final Class<?> testCaseClass) {
        if (!(Test.class.isAssignableFrom(testCaseClass) || TestClassUtils.isJunit4TestClas(testCaseClass))) {
            throw new IllegalArgumentException("The class must be an instanceof of junit.framework.Test to be added - " + testCaseClass);
        }
    }

    /**
     * Returns all test classes in a package, excluding any that are marked with @Ignore.
     *
     * @param packageName a String containing a package name
     * @param recursive   a boolean indicating whether to search recursively
     * @return a List containing all test classes  in a package, sorted by name
     */
    public static List<Class<?>> getTestClasses(String packageName, boolean recursive) {
        List<Class<? extends TestCase>> junit3TestClasses = TestClassUtils.getJUnit3TestClasses(packageName, recursive);
        List<Class<?>> jUnit4TestClasses = TestClassUtils.getJUnit4TestClasses(packageName, recursive);

        // Add the tests in a predictable order, just their name order.
        return Ordering.from(new Comparator<Class<?>>() {
            @Override
            public int compare(final Class<?> o1, final Class<?> o2) {
                return o1.getName().compareTo(o2.getName());
            }
        }).sortedCopy(Iterables.concat(junit3TestClasses, jUnit4TestClasses));
    }
}
