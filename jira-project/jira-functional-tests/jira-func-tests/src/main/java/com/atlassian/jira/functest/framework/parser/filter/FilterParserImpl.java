package com.atlassian.jira.functest.framework.parser.filter;

import com.atlassian.jira.functest.framework.locator.TableLocator;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;

public class FilterParserImpl implements FilterParser {

    private final WebTester tester;

    public FilterParserImpl(WebTester tester, JIRAEnvironmentData environmentData, int logIndentLevel) {
        this(tester, environmentData);
    }

    @Inject
    public FilterParserImpl(WebTester tester, JIRAEnvironmentData environmentData) {
        this.tester = tester;
    }

    public FilterList parseFilterList(final String tableId) {
        TableLocator locator = new TableLocator(tester, tableId);
        if (locator.getTable() == null) {
            return null;
        } else {
            return new FilterList(locator);
        }
    }
}