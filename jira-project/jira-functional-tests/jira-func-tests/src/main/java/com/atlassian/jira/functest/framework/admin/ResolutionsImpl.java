package com.atlassian.jira.functest.framework.admin;

import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.NavigationImpl;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.util.dom.DomKit;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;
import org.junit.Assert;
import org.w3c.dom.Element;

import javax.inject.Inject;

/**
 * @since v4.0
 */
public class ResolutionsImpl implements Resolutions {

    private final WebTester tester;
    private final JIRAEnvironmentData environmentData;
    private Navigation navigation;

    @Inject
    public ResolutionsImpl(WebTester tester, JIRAEnvironmentData environmentData) {
        this.tester = tester;
        this.environmentData = environmentData;
        this.navigation = null;
    }

    public String addResolution(final String name) {
        getNavigation().gotoAdminSection(Navigation.AdminSection.RESOLUTIONS);
        tester.setWorkingForm("jiraform");
        tester.setFormElement("name", name);
        tester.submit("Add");

        // get the id of the newly added resolution
        XPathLocator locator = new XPathLocator(tester, "//tr/td/b[contains(.,'" + name + "')]");
        if (locator.getNodes().length != 1) {
            Assert.fail("Could not find the newly created resolution '" + name + "'");
        }

        final Element rowParent = DomKit.getFirstParentByTag((Element) locator.getNodes()[0], "tr");
        locator = new XPathLocator(rowParent, ".//a[contains(.,'Edit')]/@href");
        if (locator.getNodes().length != 1) {
            Assert.fail("Could not find edit link for the newly created resolution '" + name + "'");
        }

        // href should look something like this -- EditResolution!default.jspa?id=6
        final String href = locator.getNodes()[0].getNodeValue();
        return href.substring(href.indexOf("=") + 1);
    }

    private Navigation getNavigation() {
        if (navigation == null) {
            navigation = new NavigationImpl(tester, environmentData);
        }
        return navigation;
    }
}
