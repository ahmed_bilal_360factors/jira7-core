package com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client;

import com.atlassian.jira.issue.fields.rest.json.beans.IssueRefJsonBean;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.List;

/**
 * Client for the issue subtask resource.
 *
 * @since v7.2
 */
public class IssueSubTaskClient extends RestApiClient<IssueSubTaskClient> {
    private static final GenericType<List<IssueRefJsonBean>> SUBTASKS_GENERIC_TYPE = new GenericType<List<IssueRefJsonBean>>() {
    };

    /**
     * Constructs a new IssueClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    public IssueSubTaskClient(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public Response getResponse(String issueKey) throws UniformInterfaceException {
        return toResponse(() -> issueSubTaskResource(issueKey).get(ClientResponse.class));
    }

    /**
     * GETs the issue with the given key.
     *
     * @param issueKey a String containing an issue key
     * @return an Issue
     * @throws UniformInterfaceException if there's a problem getting the issue
     */
    public List<IssueRefJsonBean> get(String issueKey) throws UniformInterfaceException {
        return issueSubTaskResource(issueKey).get(SUBTASKS_GENERIC_TYPE);
    }

    public WebResource issueSubTaskResource(String issueKey) {
        return createResourceWithIssueKey(issueKey);
    }

    private WebResource createResourceWithIssueKey(String issueKey) {
        return createResource().path(issueKey).path("subtask");
    }

    @Override
    protected WebResource createResource() {
        return super.createResource().path("issue");
    }

    /**
     * Move subtask from position 'from' to position 'to' in parent issue 'parentKey'
     *
     * @param parentKey parent issue containing subtask to move
     * @param from original sequence number of subtask
     * @param to new sequence number of subtask
     * @return empty response.
     */
    public Response reorder(String parentKey, Long from, Long to) {
        return toResponse(() -> createResourceWithIssueKey(parentKey).path("move")
                .type(MediaType.APPLICATION_JSON_TYPE)
                .post(ClientResponse.class, new HashMap<String, Object>(){{
                    put("original", from);
                    put("current", to);
                }}));
    }
}
