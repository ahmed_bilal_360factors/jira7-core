package com.atlassian.jira.functest.framework.admin;

import com.google.inject.ImplementedBy;

/**
 * Represents functionality of the 'Screens' admin page.
 *
 * @since v4.2
 */
@ImplementedBy(ViewFieldScreensImpl.class)
public interface ViewFieldScreens {

    /**
     * Navigates to the 'Screens' page.
     *
     * @return this ViewFieldScreens instance
     */
    public ViewFieldScreens goTo();


    /**
     * Go to the 'Configure screen' page for a given <tt>screenName</tt>.
     *
     * @param screenName name of the screen to configure
     * @return {@link ConfigureScreen} instance for given screen.
     */
    public ConfigureScreen configureScreen(String screenName);
}
