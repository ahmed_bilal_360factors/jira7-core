package com.atlassian.jira.webtests.ztests.application;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.backdoor.ApplicationRoleControl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.matcher.ApplicationRoleBeanMatcher;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.LicenseKeys;
import com.google.common.collect.ImmutableSet;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.CORE_KEY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;

@WebTest({Category.FUNC_TEST, Category.REST})
public class TestApplicationRoleResource extends BaseJiraFuncTest {
    private static final String USERS = "jira-users";
    private static final String DEVELOPERS = "jira-developers";
    private static final String ADMINS = "jira-administrators";

    private ApplicationRoleControl roleClient;

    @Before
    public void setUpTest() {
        roleClient = backdoor.applicationRoles();
    }

    /**
     * Happy path test of the end-point. Edge cases covered in the unit-tests.
     */
    @Test
    public void testAdminHappyPath() {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_ROLE);

        ApplicationRoleControl.ApplicationRoleBean roleBean = roleClient.getRole(CORE_KEY);

        //jira-users set as default during migration
        ApplicationRoleBeanMatcher matcher = ApplicationRoleBeanMatcher.forCore();
        matcher.setDefaultGroups(USERS);
        //jira-administrators associated with JIRA Core during migration
        matcher.setGroups(USERS, ADMINS);
        matcher.setSelectedByDefault(true);
        assertThat(roleBean, matcher);
        assertThat(roleClient.getRolesMap(), Matchers.hasEntry(equalTo(CORE_KEY), matcher));

        //Add some roles without primary set
        roleBean = roleClient.putRole(CORE_KEY, USERS, DEVELOPERS);
        matcher.setDefaultGroups();
        matcher.setSelectedByDefault(false);
        assertThat(roleBean, matcher.setGroups(USERS, DEVELOPERS));
        assertThat(roleClient.getRole(CORE_KEY), matcher);
        assertThat(roleClient.getRolesMap(), Matchers.hasEntry(equalTo(CORE_KEY), matcher));

        //Add some groups, remove some.
        roleBean = roleClient.putRoleWithDefaults(CORE_KEY, groups(USERS, ADMINS), groups(USERS));
        assertThat(roleBean, matcher.setGroups(USERS, ADMINS).setDefaultGroups(USERS));
        assertThat(roleClient.getRole(CORE_KEY), matcher);
        assertThat(roleClient.getRolesMap(), Matchers.hasEntry(equalTo(CORE_KEY), matcher));

        //Set the default groups
        roleBean = roleClient.putRoleWithDefaults(CORE_KEY, groups(USERS, ADMINS), groups(USERS, ADMINS));
        assertThat(roleBean, matcher.setGroups(USERS, ADMINS).setDefaultGroups(USERS, ADMINS));
        assertThat(roleClient.getRole(CORE_KEY), matcher);
        assertThat(roleClient.getRolesMap(), Matchers.hasEntry(equalTo(CORE_KEY), matcher));

        //Remove all roles.
        roleBean = roleClient.putRole(CORE_KEY);
        assertThat(roleBean, matcher.setGroups().setDefaultGroups());
        assertThat(roleClient.getRole(CORE_KEY), matcher);
        assertThat(roleClient.getRolesMap(), Matchers.hasEntry(equalTo(CORE_KEY), matcher));

        //Set role to be default one
        roleBean = roleClient.putRoleSelectedByDefault(CORE_KEY, true);
        assertThat(roleBean, matcher.setSelectedByDefault(true));
        assertThat(roleClient.getRole(CORE_KEY), matcher);
        assertThat(roleClient.getRolesMap(), Matchers.hasEntry(equalTo(CORE_KEY), matcher));

        //Unset role to be default
        roleBean = roleClient.putRoleSelectedByDefault(CORE_KEY, false);
        assertThat(roleBean, matcher.setSelectedByDefault(false));
        assertThat(roleClient.getRole(CORE_KEY), matcher);
        assertThat(roleClient.getRolesMap(), Matchers.hasEntry(equalTo(CORE_KEY), matcher));
    }

    /**
     * JIRA core is always sorted last in results, the rest in alphabetical order.
     */
    @Test
    public void testResultOrder() {
        backdoor.restoreBlankInstance(LicenseKeys.COMMERCIAL);
        backdoor.license().set(LicenseKeys.CORE_AND_TEST_ROLES);

        String coreRoleName = roleClient.getCore().getName();

        List<String> roleNames = roleClient.getRoles().stream()
                .map(ApplicationRoleControl.ApplicationRoleBean::getName)
                .collect(Collectors.toList());

        assertThat(roleNames, contains("JIRA Software", "Test Product", coreRoleName));
    }

    @Test
    public void testWebsudo() {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_ROLE);
        backdoor.websudo().enable();
        try {
            Response<?> response = roleClient.getRoleResponse(CORE_KEY);
            assertThat(response.statusCode, equalTo(401));

            response = roleClient.getRolesResponse();
            assertThat(response.statusCode, equalTo(401));

            response = roleClient.putRoleResponse(CORE_KEY, USERS);
            assertThat(response.statusCode, equalTo(401));

        } finally {
            backdoor.websudo().disable();
        }
    }

    @Test
    public void test403ReturnedForNonAdmin() {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_ROLE);
        Response<?> response = roleClient.loginAs(FRED_USERNAME).getRoleResponse(CORE_KEY);
        assertThat(response.statusCode, equalTo(403));

        response = roleClient.getRolesResponse();
        assertThat(response.statusCode, equalTo(403));

        response = roleClient.putRoleResponse(CORE_KEY, USERS);
        assertThat(response.statusCode, equalTo(403));
    }

    private static Set<String> groups(String... groups) {
        return ImmutableSet.copyOf(groups);
    }
}
