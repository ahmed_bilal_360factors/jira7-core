package com.atlassian.jira.webtests.ztests.bulk;

import com.atlassian.httpclient.api.HttpStatus;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperationProgress;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.navigation.BulkChangeWizard;
import com.atlassian.jira.functest.framework.navigation.IssueNavigatorNavigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY_KEY;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static org.junit.Assert.fail;

@WebTest({Category.FUNC_TEST, Category.BULK_OPERATIONS, Category.ISSUES})
@LoginAs(user = ADMIN_USERNAME)
public class TestBulkDeleteIssues extends BaseJiraFuncTest {
    private static final String SESSION_TIMEOUT_MESSAGE = "Your session timed out while performing bulk operation on issues.";

    /**
     * SETUP_ISSUE_COUNT is the number of 'known' issues to add<br> 'known' issues are issues that are used to control
     * some of the events<br> and to validate through the bulk edit process
     */
    private static final int SETUP_ISSUE_COUNT = 51;

    @Inject
    private BulkOperationProgress bulkOperationProgress;

    /**
     * Setup for an actual test
     */
    @Inject
    private FuncTestLogger logger;

    @Before
    public void setUpTest() {
        backdoor.restoreBlankInstance();
        produceIssues(PROJECT_HOMOSAP_KEY, SETUP_ISSUE_COUNT);
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }


    private void produceIssues(final String projectKey, final int howMany) {
        for (int i = 0; i < howMany; i++) {
            final String summary = Integer.toBinaryString(i);
            if (backdoor.issues().createIssue(projectKey, summary).id() == null) {
                fail(String.format("Failed at adding issue: '%s' while adding %d out of %d issues.", summary, i + 1, SETUP_ISSUE_COUNT));
            }
        }
    }

    /*
     * Tests that the bulk operation limits work on issue navigator and through the bulk delete wizard.
     * NOTE!! If this test runs out of memory, increase the amount of heap for the web client process. 256m is good.
     */
    @Test
    public void testBulkDeleteIssuesLimited() throws Exception {
        // test for JRA-9828 OOME on bulk delete
        produceIssues(PROJECT_MONKEY_KEY, 123);

        final String overflowProtectionPropertyKey = "jira.bulk.edit.limit.issue.count";
        final String currentLimitSetting = backdoor.applicationProperties().getString(overflowProtectionPropertyKey);
        backdoor.applicationProperties().setString(overflowProtectionPropertyKey, "100");
        try {
            navigation.issueNavigator().runSearch("project=" + PROJECT_MONKEY_KEY);
            final BulkChangeWizard wizard = navigation.issueNavigator()
                    .bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES);

            tester.assertTextPresent("Bulk changes are currently limited to 100 issues."); // tooltip

            wizard.selectAllIssues().chooseOperation(BulkChangeWizard.BulkOperationsImpl.DELETE).complete();
            bulkOperationProgress.waitAndReloadBulkOperationProgressPage();
            tester.assertLinkPresentWithText("MKY-23");
            tester.assertLinkNotPresentWithText("MKY-24");
            assertions.getURLAssertions().assertCurrentURLMatchesRegex(".*/issues/\\?jql=project.*MKY");
        } finally {
            backdoor.applicationProperties().setString(overflowProtectionPropertyKey, currentLimitSetting);
        }
    }

    /**
     * tests to see if deleting all issues in the current page works.
     */
    @Test
    public void testBulkDeleteAllIssuesInCurrentPage() {
        navigation.issueNavigator().displayAllIssues();

        navigation.issueNavigator()
                .bulkChange(IssueNavigatorNavigation.BulkChangeOption.CURRENT_PAGE)
                .selectAllIssues()
                .chooseOperation(BulkChangeWizard.BulkOperationsImpl.DELETE)
                .complete();
        bulkOperationProgress.waitAndReloadBulkOperationProgressPage();

        // goes back to issue nav?
        assertions.getURLAssertions().assertCurrentURLMatchesRegex(".*/issues/\\?jql.*");
        navigation.issueNavigator().displayAllIssues();
        tester.assertLinkNotPresentWithText(Integer.toBinaryString(25));
        tester.assertLinkPresentWithText(Integer.toBinaryString(0));
        assertIssueNotIndexed("HSP-10");
    }

    /**
     * tests to see if deleting all issues in all the pages works.<br> ie. deletes all issues
     */
    @Test
    public void testBulkDeleteAllIssuesInAllPages() {
        navigation.issueNavigator().displayAllIssues();

        navigation.issueNavigator()
                .bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES)
                .selectAllIssues()
                .chooseOperation(BulkChangeWizard.BulkOperationsImpl.DELETE)
                .complete();
        bulkOperationProgress.waitAndReloadBulkOperationProgressPage();

        // goes back to issue nav?
        assertions.getURLAssertions().assertCurrentURLMatchesRegex(".*/issues/\\?jql.*");

        navigation.issueNavigator().displayAllIssues();
        // no issues:
        tester.assertElementNotPresent("issuetable");
    }

    @Test
    public void testBulkDeleteSessionTimeouts() {
        logger.log("Bulk Delete - Test that you get redirected to the session timeout page when jumping into the wizard");

        navigation.gotoPage("secure/views/bulkedit/BulkDeleteDetails.jspa");
        tester.assertTextPresent(SESSION_TIMEOUT_MESSAGE);
        navigation.gotoPage("secure/BulkDeleteDetailsValidation.jspa");
        tester.assertTextPresent(SESSION_TIMEOUT_MESSAGE);
    }

    private void assertIssueNotIndexed(final String key) {
        logger.log("Checking that item " + key + " was deleted in the index.");
        navigation.gotoPage("/si/jira.issueviews:issue-xml/" + key + "/" + key + ".xml?jira.issue.searchlocation=index");
        assertions.assertHttpStatusCode(HttpStatus.NOT_FOUND);
    }
}
