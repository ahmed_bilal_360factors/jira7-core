package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.backdoor.ApplicationRoleControl;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.LicenseKeys;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.UPGRADE_TASKS;
import static org.junit.Assert.assertTrue;

@WebTest({FUNC_TEST, UPGRADE_TASKS})
public class TestUpgradeTask70102 extends BaseJiraFuncTest {
    @Test
    public void testDefaultApplicationCore() {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_ROLE);

        final ApplicationRoleControl.ApplicationRoleBean applicationRoleBean = backdoor.applicationRoles().getRole("jira-core");
        assertTrue("JIRA Core has to be the default application", applicationRoleBean.isSelectedByDefault());
    }

    @Test
    public void testDefaultApplicationMultiRole() {
        backdoor.restoreBlankInstance(LicenseKeys.MULTI_ROLE);

        final ApplicationRoleControl.ApplicationRoleBean applicationRoleBean = backdoor.applicationRoles().getRole("jira-software");
        assertTrue("JIRA Software has to be the default application", applicationRoleBean.isSelectedByDefault());
    }
}
