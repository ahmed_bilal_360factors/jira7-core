package com.atlassian.jira.webtests.ztests.license;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.webtests.LicenseKeys;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_USERS_GROUP;
import static org.junit.Assert.assertEquals;

/**
 * Test that the user limited licenses such as Sprout Starter licences  and Commercial Limited work.
 */
@WebTest({Category.FUNC_TEST, Category.LICENSING})
@RestoreBlankInstance
@LoginAs(user = ADMIN_USERNAME)
public class TestUserLimitedLicense extends TestCreateUserHelper {

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void disableContactAdminForm() {
        backdoor.generalConfiguration().setContactAdminFormOff();
    }

    @Test
    public void testCreateIssueOverLicenseLimit_Starter() {
        testCreateIssueOverLicenseLimit(LicenseKeys.STARTER);
    }

    @Test
    public void testCreateIssueOverLicenseLimit_CommercialLimited() {
        testCreateIssueOverLicenseLimit(LicenseKeys.COMMERCIAL_LIMITED);
    }

    private void testCreateIssueOverLicenseLimit(final License license) {
        administration.switchToLicense(license);

        //lets try to create an issue and make sure everything works fine!
        navigation.issue().goToCreateIssueForm(null, null);
        tester.setFormElement("summary", "A little bug");
        tester.submit("Create");
        //check that we made it to the view issue page!
        tester.assertTextPresent("A little bug");
        tester.assertTextPresent("Details");

        //now lets add a user.  We should be at the user limit of the license and should still be able to create
        //issues!
        backdoor.usersAndGroups().addUser(BOB_USERNAME);
        navigation.issue().goToCreateIssueForm(null, null);
        tester.setFormElement("summary", "This is a big one!");
        tester.submit("Create");
        //check that we made it to the view issue page!
        tester.assertTextPresent("This is a big one!");
        tester.assertTextPresent("Details");

        //lets import data with one too many users!
        administration.restoreData("TestStarterLicenseTooManyUsers.xml");
        administration.switchToLicense(license);

        //make sure we're no longer in the admin section (where the create issue link is no longer displayed).
        if (tester.getDialog().isLinkPresent("leave_admin")) {
            tester.clickLink("leave_admin");
        }
        tester.clickLink("create_link");
        textAssertions.assertTextPresent(locator.css(".aui-message"),
                "You are not able to create new issues because your JIRA application license's user limit has been exceeded. Please contact your JIRA administrators.");

        //try to hack the URL.
        final String atlToken = page.getXsrfToken();
        tester.gotoPage("/secure/CreateIssueDetails.jspa?atl_token=" + atlToken);
        tester.assertTextPresent("You have not selected a valid project to create an issue in.");
        tester.assertElementNotPresent("details-module");

        //remove one of the users from the jira-users group to reduce the number of active users.
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        // Click Link 'Groups' (id='editgroups_michael').
        tester.clickLink("editgroups_michael");
        // Select 'jira-users' from select box 'groupsToLeave'.
        tester.selectOption("groupsToLeave", "jira-users");
        tester.submit("leave");

        //check creating an issue works again!
        navigation.issue().goToCreateIssueForm(null, null);
        tester.setFormElement("summary", "Let's create another bug!");
        tester.submit("Create");
        //check that we made it to the view issue page!
        assertions.getTextAssertions().assertTextPresentHtmlEncoded("Let's create another bug!");
        tester.assertTextPresent("Details");
    }

    @Test
    public void testAddUserOverLimitShowsWarning_Starter() {
        testAddUserOverLimitShowsWarning(LicenseKeys.STARTER);
    }

    @Test
    public void testAddUserOverLimitShowsWarning_CommercialLimited() {
        testAddUserOverLimitShowsWarning(LicenseKeys.COMMERCIAL_LIMITED);
    }

    private void testAddUserOverLimitShowsWarning(final License license) {
        administration.switchToLicense(license);
        addUsersWhileUnderTheLimit("devman");
        addUsersWhileUnderTheLimit("prodman");
        addUsersWhileUnderTheLimit("fatman");


        // now we are at the limit, should not be able to create the next user
        navigation.gotoAdminSection(Navigation.AdminSection.CREATE_USER);
        tester.setFormElement("username", "barney");
        tester.setFormElement("password", "barney");
        tester.setFormElement("fullname", "Barney");
        tester.setFormElement("email", "barney@example.com");
        tester.submit("Create");
        gotoViewUserPage("barney");
        textAssertions.assertTextNotPresent(new WebPageLocator(tester), "jira-users");

        navigation.logout();
        navigation.gotoDashboard();
        navigation.loginAttempt("barney", "barney");
        tester.assertTextPresent("You do not have a permission to log in");
    }

    private void addUsersWhileUnderTheLimit(final String userName) {
        this.createUser(userName);
        assertUserInGroups(userName, JIRA_USERS_GROUP);
    }

    @Test
    public void testSignupOverLicenseLimit_Starter() {
        testSignupOverLicenseLimit(LicenseKeys.STARTER);
    }

    @Test
    public void testSignupOverLicenseLimit_CommercialLimited() {
        testSignupOverLicenseLimit(LicenseKeys.COMMERCIAL_LIMITED);
    }

    private void testSignupOverLicenseLimit(final License license) {
        administration.switchToLicense(license);
        // add 2 users to bring it up to 4 users in the system
        addUsersWhileUnderTheLimit("dudeman");
        addUsersWhileUnderTheLimit("prodman");

        // sign up a user when under limit
        navigation.logout();

        tester.gotoPage("login.jsp");
        tester.clickLink("signup");
        tester.setFormElement("username", "devman");
        tester.setFormElement("password", "devman");
        tester.setFormElement("fullname", "Devman");
        tester.setFormElement("email", "devman@example.com");
        tester.submit();
        assertions.assertNodeHasText(locator.css("#content .aui-message.aui-message-success"),
                "You have successfully signed up. If you forget your password, you can request a new one.");
        tester.clickLinkWithText("Click here to log in");

        navigation.login("devman", "devman");
        assertEquals("Devman", navigation.userProfile().userName());
        navigation.logout();

        // now sign up again - should be over limit
        tester.gotoPage("login.jsp");
        tester.clickLink("signup");
        tester.assertTextPresent("We can't sign you up right now as there are too many users in the system. You could let the administrator know you can't sign up right now or try again later.");

        // try to make the signup action execute manually
        tester.gotoPage("/secure/Signup.jspa");
        tester.assertTextPresent("We can't sign you up right now as there are too many users in the system. You could let the administrator know you can't sign up right now or try again later.");
    }
}