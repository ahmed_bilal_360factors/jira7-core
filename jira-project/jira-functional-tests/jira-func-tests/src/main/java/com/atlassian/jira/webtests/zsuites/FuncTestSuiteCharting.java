package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.charts.TestCharting;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Test for JIRA charting.
 *
 * @since v4.0
 */
public class FuncTestSuiteCharting {

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestCharting.class)
                .build();
    }
}