package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.api.issue.IssueUpdateRequest;
import com.atlassian.jira.rest.api.util.StringList;
import com.atlassian.jira.testkit.client.UserHistoryControl;
import com.atlassian.jira.testkit.client.restclient.BeanClient;
import com.atlassian.jira.testkit.client.restclient.FieldMetaData;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.IssueBean;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.jira.testkit.client.restclient.IssuePickerBean;
import com.atlassian.jira.testkit.client.restclient.LabelSuggestionsBean;
import com.atlassian.jira.testkit.client.restclient.SectionBean;
import com.atlassian.jira.testkit.client.restclient.User;
import com.atlassian.jira.testkit.client.restclient.UserPickerResultBean;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.util.EnumSet;
import java.util.List;

import static com.atlassian.jira.rest.api.issue.ResourceRef.withId;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @since v5.0
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@Restore("TestIssueResourceEditMeta.xml")
public class TestIssueResourceEditMeta extends BaseJiraRestTest {
    private IssueClient issueClient;
    private BeanClient beanClient;

    @Before
    public void setup() {
        issueClient = new IssueClient(environmentData);
        beanClient = new BeanClient(environmentData);
        backdoor.restoreDataFromResource("TestIssueResourceEditMeta.xml");
    }

    @Test
    public void testEditmetaViaExpand() throws Exception {
        Issue issue = issueClient.get("TST-1");
        assertNull(issue.editmeta);

        issue = issueClient.get("TST-1", Issue.Expand.editmeta);
        assertNotNull(issue.editmeta);
        assertThat(issue.editmeta.fields, allOf(hasKey("summary"),
                hasKey("description"),
                hasKey("timetracking"),
                hasKey("issuetype"),
                hasKey("labels"),
                hasKey("assignee"),
                hasKey("resolution"),
                hasKey("attachment"),
                hasKey("comment"),
                hasKey("worklog"),
                hasKey("security")));

        issue = issueClient.getPartially("TST-1", EnumSet.of(Issue.Expand.editmeta), StringList.fromList("summary", "description"));
        assertNotNull(issue.editmeta);
        assertThat(issue.editmeta.fields, allOf(hasKey("summary"), hasKey("description")));
        assertThat(issue.editmeta.fields.keySet(), Matchers.hasSize(2));
    }

    @Test
    public void testEditMetaAutoCompleteUrls() throws Exception {
        getUserHistoryClient().addIssue("admin", "TST-2");
        getUserHistoryClient().addIssue("admin", "TST-3");

        Issue issue = issueClient.get("TST-1");
        assertNull(issue.editmeta);

        issue = issueClient.get("TST-1", Issue.Expand.editmeta);
        assertNotNull(issue.editmeta);
        FieldMetaData assignee = issue.editmeta.fields.get("assignee");
        assertTrue(StringUtils.isNotBlank(assignee.autoCompleteUrl));
        List<User> assigneeSuggestions = beanClient.getUsersFromUrl(assignee.autoCompleteUrl + "a");

        assertEquals(1, assigneeSuggestions.size());
        assertEquals("admin", assigneeSuggestions.get(0).displayName);

        FieldMetaData reporter = issue.editmeta.fields.get("reporter");
        assertTrue(StringUtils.isNotBlank(reporter.autoCompleteUrl));
        List<User> reporterSuggestions = beanClient.getUsersFromUrl(reporter.autoCompleteUrl + "a");
        assertEquals(1, reporterSuggestions.size());
        assertEquals("admin", reporterSuggestions.get(0).displayName);

        FieldMetaData issueLinks = issue.editmeta.fields.get("issuelinks");
        assertTrue(StringUtils.isNotBlank(issueLinks.autoCompleteUrl));
        IssuePickerBean issuePickerBean = beanClient.getIssueSuggestionsFromUrl(issueLinks.autoCompleteUrl + "TS");
        assertEquals(2, issuePickerBean.sections.get(0).issues.size());
        SectionBean sectionBean = issuePickerBean.sections.get(0);
        assertTrue(containsBeanWithKey(sectionBean.issues, "TST-2"));
        assertTrue(containsBeanWithKey(sectionBean.issues, "TST-3"));

        FieldMetaData labels = issue.editmeta.fields.get("labels");
        assertTrue(StringUtils.isNotBlank(labels.autoCompleteUrl));
        LabelSuggestionsBean labelSuggestionBean = beanClient.getLabelSuggestionsFromUrl(labels.autoCompleteUrl + "b");
        assertEquals(2, labelSuggestionBean.suggestions.size());
        assertEquals("bar", labelSuggestionBean.suggestions.get(0).label);
        assertEquals("bob", labelSuggestionBean.suggestions.get(1).label);

        FieldMetaData userPickerCF = issue.editmeta.fields.get("customfield_10010");
        assertTrue(StringUtils.isNotBlank(userPickerCF.autoCompleteUrl));
        UserPickerResultBean userPickerResultBean = beanClient.getUserPickResultsFromUrl(userPickerCF.autoCompleteUrl + "a");
        assertEquals(1, userPickerResultBean.users.size());
        assertEquals("admin", userPickerResultBean.users.get(0).name);

        FieldMetaData multiUserPickerCF = issue.editmeta.fields.get("customfield_10110");
        assertTrue(StringUtils.isNotBlank(multiUserPickerCF.autoCompleteUrl));
        UserPickerResultBean multiUserPickerResultBean = beanClient.getUserPickResultsFromUrl(multiUserPickerCF.autoCompleteUrl + "a");
        assertEquals(1, multiUserPickerResultBean.users.size());
        assertEquals("admin", multiUserPickerResultBean.users.get(0).name);

        FieldMetaData labelCF = issue.editmeta.fields.get("customfield_10210");
        assertTrue(StringUtils.isNotBlank(labelCF.autoCompleteUrl));
        LabelSuggestionsBean labelSuggestionsBean = beanClient.getLabelSuggestionsFromUrl(labelCF.autoCompleteUrl + "b");
        assertEquals(1, labelSuggestionsBean.suggestions.size());
        assertEquals("bob", labelSuggestionsBean.suggestions.get(0).label);


    }

    private UserHistoryControl getUserHistoryClient() {
        return backdoor.getTestkit().userHistory();
    }

    @Test
    public void testFieldsFilteredByPermissions() {
        Issue issue = issueClient.get("TST-1");
        assertNull(issue.editmeta);

        issue = issueClient.loginAs("fry").get("TST-1", Issue.Expand.editmeta);
        assertNotNull(issue.editmeta);
        assertThat(issue.editmeta.fields, allOf(hasKey("summary"),
                hasKey("description"),
                hasKey("timetracking"),
                hasKey("issuetype"),
                hasKey("labels"),
                hasKey("resolution"),
                hasKey("attachment"),
                hasKey("security")));

        assertThat(issue.editmeta.fields, not(allOf(hasKey("comment"),
                hasKey("worklog"),
                hasKey("assignee"))));
    }

    @Test
    public void testNothingWhenNoEditPermissions() {
        Issue issue = issueClient.loginAs("fry").get("PH-1", Issue.Expand.editmeta);

        assertNotNull(issue.editmeta);
        assertTrue("Editmeta is empty", issue.editmeta.fields.isEmpty());
    }

    @Test
    public void testResponseIsEmptyWhenJiraIssueEditableIsSetToFalseOnWorkflow() {
        // closed issue have property jira.issue.editable set to false
        String closeIssueTransition = "2";
        IssueUpdateRequest request = new IssueUpdateRequest();
        backdoor.permissionSchemes().addUserPermission(0l, ProjectPermissions.CLOSE_ISSUES, "admin");
        backdoor.permissionSchemes().addUserPermission(0l, ProjectPermissions.RESOLVE_ISSUES, "admin");
        backdoor.permissionSchemes().addUserPermission(0l, ProjectPermissions.TRANSITION_ISSUES, "admin");

        request.transition(withId(closeIssueTransition));
        issueClient.transition("TST-1", request);
        Issue issue = issueClient.get("TST-1", Issue.Expand.editmeta);

        assertNotNull(issue.editmeta);
        assertThat(issue.editmeta.fields.keySet(), Matchers.empty());
    }

    private boolean containsBeanWithKey(List<IssueBean> issueBeans, String key) {
        for (IssueBean issueBean : issueBeans) {
            if (issueBean.key.equals(key)) {
                return true;
            }
        }
        return false;
    }
}
