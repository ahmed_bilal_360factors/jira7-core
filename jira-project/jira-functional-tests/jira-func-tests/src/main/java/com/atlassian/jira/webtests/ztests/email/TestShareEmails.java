package com.atlassian.jira.webtests.ztests.email;

import com.atlassian.jira.functest.framework.BaseJiraEmailTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.rule.MailTest;
import com.atlassian.jira.testkit.client.log.FuncTestLoggerImpl;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;

import javax.mail.internet.MimeMessage;
import java.util.Collection;
import java.util.Set;

import static com.atlassian.jira.functest.matcher.MimeMessageMatchers.containsString;
import static com.atlassian.jira.functest.matcher.MimeMessageMatchers.hasHtmlContentType;
import static com.atlassian.jira.functest.matcher.MimeMessageMatchers.hasTextContentType;
import static com.atlassian.jira.functest.matcher.MimeMessageMatchers.withBodyMatching;
import static com.atlassian.jira.functest.matcher.MimeMessageMatchers.withSubjectEqualTo;
import static com.atlassian.jira.testkit.client.IssuesControl.HSP_PROJECT_ID;
import static org.junit.Assert.assertThat;

/**
 * @since v5.0
 */
@RestoreBlankInstance
@WebTest({Category.FUNC_TEST, Category.EMAIL})
public class TestShareEmails extends BaseJiraEmailTest implements FunctTestConstants {
    private FuncTestLoggerImpl logger = new FuncTestLoggerImpl(1);
    private ShareClient shareClient;
    private String issueKey;

    public void log(Object logData) {
        logger.log(logData);
    }

    @Before
    public void setUpTest() {
        issueKey = backdoor.issues().createIssue(HSP_PROJECT_ID, "Issue 1", ADMIN_USERNAME).key();
        shareClient = new ShareClient(getEnvironmentData());
        backdoor.usersAndGroups().addUser(BOB_USERNAME, BOB_PASSWORD, BOB_FULLNAME, BOB_EMAIL);

        backdoor.userProfile().changeUserNotificationType(BOB_USERNAME, "text");
        backdoor.userProfile().changeUserNotificationType(FRED_USERNAME, "html");
    }

    @Test
    @MailTest
    public void testAll() throws Exception {
        logger.log("Running _testShareIssue");
        _testShareIssue();
        logger.log("Running _testShareSavedSearch");
        _testShareSavedSearch();
        logger.log("Running _testShareJqlSearch");
        _testShareJqlSearch();
    }

    public void _testShareIssue() throws Exception {
        Set<String> usernames = Sets.newHashSet(FRED_USERNAME, BOB_USERNAME);
        Set<String> emails = Sets.newHashSet("fake@example.com");
        String comment = "I thought you should know";
        shareClient.shareIssue(issueKey, usernames, emails, comment);

        Collection<MimeMessage> mimeMessages = mailHelper.flushMailQueueAndWait(3);
        mailHelper.clearOutgoingMailQueue();

        // 1. Check the HTML email
        MimeMessage message = mailHelper.getMessageForAddress(mimeMessages, FRED_EMAIL);
        assertThat(message, withSubjectEqualTo("[JIRATEST] Administrator shared \"HSP-1: Issue 1\" with you"));

        assertThat(message, hasHtmlContentType());
        assertThat(message, withSubjectEqualTo("[JIRATEST] Administrator shared \"HSP-1: Issue 1\" with you"));
        assertThat(message, containsString("shared</strong> an issue with you"));
        assertThat(message, containsString(comment));
        assertThat(message, withBodyMatching("/browse/HSP-1\".*>HSP-1</a>"));

        // 2. Check the Text email
        message = mailHelper.getMessageForAddress(mimeMessages, BOB_EMAIL);
        assertThat(message, hasTextContentType());
        assertThat(message, withSubjectEqualTo("[JIRATEST] Administrator shared \"HSP-1: Issue 1\" with you"));
        assertThat(message, containsString("Administrator shared an issue with you"));
        assertThat(message, containsString("/browse/HSP-1"));
        assertThat(message, containsString(comment));
        assertThat(message, containsString("Key: HSP-1"));
        assertThat(message, containsString("Project: homosapien"));

        // 3. Check that the email sent to the email address is HTML
        message = mailHelper.getMessageForAddress(mimeMessages, "fake@example.com");
        assertThat(message, hasHtmlContentType());
    }

    public void _testShareSavedSearch() throws Exception {
        String searchJql = "project = HSP";
        String searchName = "Funky Homosapiens";
        String searchDescription = "Find those dudes!";
        String jsonShareString = "[{\"type\":\"global\"}]";
        String filterId = backdoor.searchRequests().createFilter("admin", searchJql, searchName, searchDescription, jsonShareString);

        Set<String> usernames = Sets.newHashSet(FRED_USERNAME, BOB_USERNAME);
        Set<String> emails = Sets.newHashSet("fake@example.com");
        String comment = "I thought you should know";
        shareClient.shareSavedSearch(filterId, usernames, emails, comment);

        Collection<MimeMessage> mimeMessages = mailHelper.flushMailQueueAndWait(3);
        mailHelper.clearOutgoingMailQueue();

        // 1. Check the HTML email
        MimeMessage message = mailHelper.getMessageForAddress(mimeMessages, FRED_EMAIL);
        assertThat(message, withSubjectEqualTo("[JIRATEST] Administrator shared the filter \"Funky Homosapiens\" with you"));
        assertThat(message, containsString("Administrator</a>"));
        assertThat(message, containsString("<b>shared</b> a filter with you"));
        assertThat(message, containsString("I thought you should know"));
        assertThat(message, withBodyMatching("/issues/\\?filter=" + filterId + "\" style=\".*\">" + searchName));

        // 2. Check the Text email
        message = mailHelper.getMessageForAddress(mimeMessages, BOB_EMAIL);
        assertThat(message, hasTextContentType());
        assertThat(message, withSubjectEqualTo("[JIRATEST] Administrator shared the filter \"Funky Homosapiens\" with you"));
        assertThat(message, containsString("Administrator shared a filter with you"));
        assertThat(message, containsString("I thought you should know"));
        assertThat(message, containsString("/issues/?filter=" + filterId));

        // 3. Check that the email sent to the email address links to JQL, not the filter
        message = mailHelper.getMessageForAddress(mimeMessages, "fake@example.com");
        assertThat(message, hasHtmlContentType());
        assertThat(message, withSubjectEqualTo("[JIRATEST] Administrator shared a search result with you"));
        assertThat(message, containsString("Administrator</a>"));
        assertThat(message, containsString("<b>shared</b> a search result with you"));
        assertThat(message, containsString("I thought you should know"));
        assertThat(message, withBodyMatching("/issues/\\?jql=project\\+%3D\\+HSP\""));
    }

    public void _testShareJqlSearch() throws Exception {
        String searchJql = "project = HSP";

        Set<String> usernames = Sets.newHashSet(FRED_USERNAME, BOB_USERNAME);
        String comment = "I thought you should know";
        shareClient.shareSearchQuery(searchJql, usernames, null, comment);

        Collection<MimeMessage> mimeMessages = mailHelper.flushMailQueueAndWait(2);
        mailHelper.clearOutgoingMailQueue();

        // 1. Check the HTML email
        MimeMessage message = mailHelper.getMessageForAddress(mimeMessages, FRED_EMAIL);
        assertThat(message, hasHtmlContentType());
        assertThat(message, withSubjectEqualTo("[JIRATEST] Administrator shared a search result with you"));
        assertThat(message, containsString("Administrator</a>"));
        assertThat(message, containsString("<b>shared</b> a search result with you"));
        assertThat(message, containsString("I thought you should know"));
        assertThat(message, withBodyMatching("/issues/\\?jql=project\\+%3D\\+HSP\" style=\".*\">View search results"));

        // 2. Check the Text email
        message = mailHelper.getMessageForAddress(mimeMessages, BOB_EMAIL);
        //assertMessageIsText(mimeMessages[0]);
        assertThat(message, hasTextContentType());
        assertThat(message, withSubjectEqualTo("[JIRATEST] Administrator shared a search result with you"));
        assertThat(message, containsString("Administrator shared a search result with you"));
        assertThat(message, containsString(comment));
        assertThat(message, containsString("/issues/?jql=project+%3D+HSP"));
    }
}
