package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.joda.time.DateTime;

/**
 * Client to hit the Server Info REST resource.
 *
 * @since v7.0
 */
public class ServerInfoControl extends RestApiClient<ApplicationRoleControl> {
    public ServerInfoControl(final JIRAEnvironmentData environmentData) {
        super(environmentData, "2");
    }

    public ServerInfo get() {
        return createResource().get(ServerInfo.class);
    }

    protected WebResource createResource() {
        return super.createResource().path("serverInfo");
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ServerInfo {
        @JsonProperty
        private String version;

        @JsonProperty
        private DateTime buildDate;

        public String getVersion() {
            return version;
        }

        public DateTime getBuildDate() {
            return buildDate;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }
}
