package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

import java.util.Collection;
import java.util.List;

/**
 * @since v7.0
 */
public class GroupManagerControl extends BackdoorControl<GroupManagerControl> {
    public GroupManagerControl(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public List<String> getNamesOfDirectMembersOfGroups(List<String> groupNames, Integer limit) {
        WebResource resource = createResource().path("groupManager/directMembers");
        for (String groupName : groupNames) {
            resource = resource.queryParam("groupNames", groupName);
        }
        return resource.queryParam("limit", limit.toString()).get(new GenericType<List<String>>() {
        });
    }

    public Collection<String> filterUsersInAllGroupsDirect(Collection<String> userNames, Collection<String> groupNames) {
        WebResource resource = createResource().path("groupManager/filteredUsers");
        for (String userName : userNames) {
            resource = resource.queryParam("userNames", userName);
        }
        for (String groupName : groupNames) {
            resource = resource.queryParam("groupNames", groupName);
        }
        return resource.get(new GenericType<Collection<String>>() {
        });
    }
}
