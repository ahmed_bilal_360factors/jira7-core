package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.help.TestCoreHelpLinks;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * A suite of tests around JIRA help urls.
 *
 * @since 7.0
 */
public class FuncTestSuiteHelpUrls {
    public static List<Class<?>> suite() {
        return Lists.newArrayList(
                TestCoreHelpLinks.class
        );
    }
}
