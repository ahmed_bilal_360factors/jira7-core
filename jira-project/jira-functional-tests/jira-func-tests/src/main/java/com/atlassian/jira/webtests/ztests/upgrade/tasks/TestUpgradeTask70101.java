package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.project.type.ProjectTypeKey;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.UPGRADE_TASKS;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@WebTest({FUNC_TEST, UPGRADE_TASKS})
public class TestUpgradeTask70101 extends BaseJiraFuncTest {
    private static final long PROJECT_1 = 10000L;
    private static final long PROJECT_2 = 10001L;
    private static final long PROJECT_3 = 10002L;
    private static final long PROJECT_4 = 10003L;
    private static final long PROJECT_5 = 10004L;

    @Before
    public void setUp() {
        backdoor.restoreBlankInstance();
    }

    @Test
    public void projectsReceivedExpectedProjectType() {
        backdoor.restoreDataFromResource("TestUpgradeTask70101.zip");

        // project1 had a business type already, it should be upgraded to software.
        assertProjectHasType(PROJECT_1, "software");
        // project2 had no project type, and no entry on the ServiceDesk table at all, so it should be marked as Software
        assertProjectHasType(PROJECT_2, "software");
        // project3 had no project type, and an entry on the ServiceDesk table where the "DISABLED" field is null, so it should be marked as Service Desk
        assertProjectHasType(PROJECT_3, "service_desk");
        // project4 had no project type, and an entry on the ServiceDesk table where the "DISABLED" field is true, so it should be marked as Software
        assertProjectHasType(PROJECT_4, "software");
        // project5 had no project type, and an entry on the ServiceDesk table where the "DISABLED" field is false, so it should be marked as Service Desk
        assertProjectHasType(PROJECT_5, "service_desk");
    }

    private void assertProjectHasType(long projectId, String expectedType) {
        ProjectTypeKey projectType = backdoor.project().getProjectType(projectId);
        assertThat(projectType.getKey(), is(expectedType));
    }
}
