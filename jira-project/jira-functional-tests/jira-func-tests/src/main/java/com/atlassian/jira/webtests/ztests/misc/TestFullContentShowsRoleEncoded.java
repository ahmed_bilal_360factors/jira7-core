package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.Locator;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.ROLES})
@LoginAs(user = ADMIN_USERNAME)
public class TestFullContentShowsRoleEncoded extends BaseJiraFuncTest {
    /* JRA-14541 & JRA-14827 */
    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testFullContentViewShowsCommentRoleInEncodedForm() {
        administration.restoreData("TestFullContentShowsRoleEncoded.xml");
        navigation.issueNavigator().displayFullContentAllIssues();
        Locator page = new WebPageLocator(tester);
        textAssertions.assertTextPresent(page.getHTML(), "Users &lt;b&gt;Test&lt;/b&gt; &amp;copy;");
    }
}
