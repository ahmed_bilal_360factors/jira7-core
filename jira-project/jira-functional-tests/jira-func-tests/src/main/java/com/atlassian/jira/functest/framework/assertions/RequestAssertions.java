package com.atlassian.jira.functest.framework.assertions;

import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;

import static org.junit.Assert.assertFalse;

/**
 * Assertions about requests
 *
 * @since v7.1
 */
public class RequestAssertions {

    private final WebTester tester;

    @Inject
    public RequestAssertions(WebTester tester, JIRAEnvironmentData environmentData) {
        this.tester = tester;
    }

    /**
     * Asserts that the Cache-control header in the response *is not* set to any one of "no-cache", "no-store" or
     * "must-revalidate". The choice of these 3 headers is directly related to the implementation of
     * com.atlassian.core.filters.AbstractEncodingFilter.setNonCachingHeaders(HttpServletResponse)
     */
    public void assertResponseCanBeCached() {
        final String cacheControl = tester.getDialog().getResponse().getHeaderField("Cache-control");
        final String[] values = new String[]{"no-cache", "no-store"};
        if (cacheControl != null && StringUtils.isNotEmpty(cacheControl)) {
            // the presence of any of these headers means the response is not cacheable - ensure none of them are present
            for (String value : values) {
                assertFalse("Response cannot be cached: found '" + value + "' in Cache-control header", cacheControl.contains(value));
            }
        }
    }

}
