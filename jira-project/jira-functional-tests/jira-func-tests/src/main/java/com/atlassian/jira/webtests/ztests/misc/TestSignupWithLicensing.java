package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.backdoor.ApplicationRoleControl;
import com.atlassian.jira.functest.framework.locator.Locator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.test.util.lic.ApplicationLicenseConstants;
import com.atlassian.jira.webtests.LicenseKeys;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Tests the Signup action with licensing enabled
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS, Category.LICENSING})
public class TestSignupWithLicensing extends BaseJiraFuncTest {
    private static final String USERNAME = "user";
    private static final String ENGLISH_LOCALE = "en_US";
    private ApplicationRoleControl roleClient;

    @Inject
    private Assertions assertions;

    @Inject
    private LocatorFactory locator;

    private static String removeI18nTags(final String i18nString) {
        return i18nString.replaceAll("\\{[0-9]+\\}", "");
    }

    @Before
    public void setUpTest() {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_AND_TEST_ROLES.getLicenseString());
        backdoor.generalConfiguration().setContactAdminFormOff();

        roleClient = backdoor.applicationRoles();

        backdoor.generalConfiguration().setContactAdminFormOn();
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
        backdoor.darkFeatures().enableForSite(FunctTestConstants.DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG);
    }

    @Test
    public void testSignupForDefaultApplicationRoles() {
        checkSuccessUserCreate();
        assertTrue("User is member of jira-users group", backdoor.getTestkit().usersAndGroups().isUserInGroup(USERNAME, "jira-users"));
        assertFalse("User is not member of jira-developers group", backdoor.getTestkit().usersAndGroups().isUserInGroup(USERNAME, "jira-developers"));
    }

    @Test
    public void shouldCreateUserWhenGroupsExclusive() {
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.CORE_KEY, true, ImmutableList.of("jira-users"), ImmutableList.of("jira-users"));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, true, ImmutableList.of("jira-developers"), ImmutableList.of("jira-developers"));

        prepareToSignUp();

        assertThat(getDefaultGroups(), is(ImmutableSet.of("jira-developers", "jira-users")));
        checkSuccessUserCreate();

        assertTrue("User is member of jira-users group", backdoor.getTestkit().usersAndGroups().isUserInGroup(USERNAME, "jira-users"));
        assertTrue("User is member of jira-developers group", backdoor.getTestkit().usersAndGroups().isUserInGroup(USERNAME, "jira-developers"));
    }

    @Test
    public void shouldSignupDisplayExceededMessageWhenOneApplicationExceeded() {
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.CORE_KEY, true, ImmutableList.of("jira-users"), ImmutableList.of("jira-users"));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, true, ImmutableList.of("jira-developers"), ImmutableList.of("jira-developers"));
        addUsersWithGroups(3, ImmutableList.of("jira-users"));
        navigation.logout();

        gotoSignupPage();

        assertCannotSignUpTitle();
        assertMessageContent("signup.limit.exceeded");
    }

    @Test
    public void shouldSignupFailWhenOneApplicationExceeded() {
        prepareToSignUp();
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.CORE_KEY, true, ImmutableList.of("jira-users"), ImmutableList.of("jira-users"));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, true, ImmutableList.of("jira-developers"), ImmutableList.of("jira-developers"));
        addUsersWithGroups(3, ImmutableList.of("jira-users"));

        submitSignup();

        assertCannotSignUpTitle();
        assertMessageContent("signup.limit.exceeded");
    }

    @Test
    public void shouldSignupDisplayAppAccessErrorMessageUpfrontWhenNoDefaultGroups() {
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.CORE_KEY, true, ImmutableList.of("jira-users"), ImmutableList.of("jira-users"));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, true, ImmutableList.of("jira-developers"), ImmutableList.of());
        navigation.logout();

        gotoSignupPage();

        assertCannotSignUpTitle();
        assertMessageContent("signup.app.access.error.upfront");

        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, true, ImmutableList.of("jira-developers"), ImmutableList.of("jira-developers"));

        gotoSignupPage();

        assertions.assertNodeHasText(getTitleLocator(), "Sign up");
    }

    @Test
    public void shouldSignupDisplayAppAccessErrorMessageAfterSubmitWhenNoDefaultGroups() {
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.CORE_KEY, false, ImmutableList.of("jira-users"), ImmutableList.of("jira-users"));
        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, true, ImmutableList.of("jira-developers"), ImmutableList.of("jira-developers"));
        navigation.logout();

        gotoSignupPage();

        assertions.assertNodeHasText(getTitleLocator(), "Sign up");

        roleClient.putRoleWithDefaultsSelectedByDefault(ApplicationLicenseConstants.TEST_KEY, true, ImmutableList.of("jira-developers"), ImmutableList.of());
        submitSignup();

        assertCannotSignUpTitle();
        assertMessageContent("signup.app.access.error.submit");
    }

    private void assertMessageContent(final String i18nKey) {
        assertions.assertNodeHasText(locator.css("#content p"), removeI18nTags(backdoor.i18n().getText(i18nKey, ENGLISH_LOCALE)));
    }

    private void assertCannotSignUpTitle() {
        assertions.assertNodeHasText(getTitleLocator(), backdoor.i18n().getText("signup.cannot.signup.now", ENGLISH_LOCALE));
    }

    private Locator getTitleLocator() {
        return locator.css("#content h2");
    }

    private void addUsersWithGroups(int numberOfUsers, Iterable<String> groupNames) {
        for (int i = 0; i < numberOfUsers; i++) {
            final String userName = String.format("user-%d", i);
            backdoor.usersAndGroups().addUser(userName);
            groupNames.forEach(groupName -> backdoor.usersAndGroups().addUserToGroup(userName, groupName));
        }
    }

    private Set<String> getDefaultGroups() {
        return roleClient.getRoles().stream().filter(role -> role.isSelectedByDefault()).flatMap(role -> role.getDefaultGroups().stream()).collect(Collectors.toSet());
    }

    private void prepareToSignUp() {
        gotoSignupPage();
        assertions.assertNodeHasText(locator.css("h2"), "Sign up");
        tester.setWorkingForm("signup");
        tester.assertButtonPresent("signup-submit");
    }

    private void gotoSignupPage() {
        tester.gotoPage("secure/Signup!default.jspa");
    }


    private void checkSuccessUserCreate() {
        prepareToSignUp();

        submitSignup();

        assertions.assertNodeHasText(getTitleLocator(), "Congratulations!");
        assertions.assertNodeHasText(locator.css("#content .aui-message.aui-message-success"),
                removeI18nTags(backdoor.i18n().getText("signup.success", ENGLISH_LOCALE)));
    }

    private void submitSignup() {
        tester.setFormElement("username", USERNAME);
        tester.setFormElement("password", "password");
        tester.setFormElement("fullname", "User Tested");
        tester.setFormElement("email", "username@email.com");
        tester.submit();
    }
}
