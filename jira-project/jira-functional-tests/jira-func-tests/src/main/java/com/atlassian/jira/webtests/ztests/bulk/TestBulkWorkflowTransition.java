package com.atlassian.jira.webtests.ztests.bulk;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperations;
import com.atlassian.jira.functest.framework.EditIssueFieldVisibility;
import com.atlassian.jira.functest.framework.Indexing;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.SessionFactory;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ATTACHMENT_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BUTTON_NEXT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.webtests.Groups.USERS;

@WebTest({Category.FUNC_TEST, Category.BULK_OPERATIONS, Category.WORKFLOW})
@LoginAs(user = ADMIN_USERNAME)
public class TestBulkWorkflowTransition extends BaseJiraFuncTest {

    private static final String NOT_AVAILABLE_BULK_EDIT = "NOTE: This field is not available for bulk update operations.";
    private static final String WORKFLOW_TRANSITION_CHOOSE_ERROR_TEXT = "Please select a transition to execute";
    private static final String WORKFLOW_TRANSITION_EDIT_TEXT = "Select and edit the fields available on this transition.";
    private static final String WORKFLOW_TRANSITION_MULTI_PROJECT_ERROR = "NOTE: This operation can be performed only on issues from ONE project.";
    private static final String WORKFLOW_TRANSITION_SELECTION_TEXT = "Select the workflow transition to execute on the associated issues";
    private static final String COMMENT_1 = "This issue is resolved now.";
    private static final String COMMENT_2 = "Viewable by developers group.";
    private static final String COMMENT_3 = "Viewable by Developers role.";
    private static final String TABLE_EDITFIELDS_ID = "screen-tab-1-editfields";
    @Inject
    protected TextAssertions textAssertions;
    @Inject
    private Indexing indexing;
    @Inject
    private EditIssueFieldVisibility editIssueFieldVisibility;
    @Inject
    private BulkOperations bulkOperations;
    @Inject
    private FuncTestLogger logger;
    @Inject
    private Administration administration;
    @Inject
    private SessionFactory sessionFactory;

    @After
    public void tearDown() {
        backdoor.darkFeatures().disableForSite("jira.no.frother.reporter.field");
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        administration.removeGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
    }

    @Test
    @RestoreBlankInstance
    public void testBulkTransitionSessionTimeouts() {
        logger.log("Bulk Transition - Test that you get redirected to the session timeout page when jumping into the wizard");

        tester.beginAt("secure/views/bulkedit/BulkWorkflowTransitionDetails.jspa");
        verifyAtSessionTimeoutPage();
        tester.beginAt("secure/views/bulkedit/BulkWorkflowTransitionDetailsValidation.jspa");
        verifyAtSessionTimeoutPage();
        tester.beginAt("secure/views/bulkedit/BulkWorkflowTransitionEditValidation.jspa");
        verifyAtSessionTimeoutPage();
    }

    /**
     * Validate non editable fields and project specific fields Time Tracking - not editable Attachments - not editable
     * Summary - not editable Components - project specific Versions - project specific
     */
    @Test
    @Restore("TestBulkWorkflowTransition.xml")
    public void testNotAvailableFields() {
        administration.addGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
        // get to the issue display page
        _testToOperationDetailsWorkflowTranisition();
        navigation.workflows().chooseWorkflowAction("classic default workflow_2_6");
        tester.assertTextPresent(WORKFLOW_TRANSITION_EDIT_TEXT);

        // make sure that Attachment, Summary and Timetracking are not available for edit.
        tester.assertTableRowsEqual(TABLE_EDITFIELDS_ID, 14, new String[][]
                {{"N/A", "Change Attachment", NOT_AVAILABLE_BULK_EDIT}});
        tester.assertTableRowsEqual(TABLE_EDITFIELDS_ID, 25, new String[][]
                {{"N/A", "Change Summary", NOT_AVAILABLE_BULK_EDIT}});
        tester.assertTableRowsEqual(TABLE_EDITFIELDS_ID, 27, new String[][]
                {{"N/A", "Change Time Tracking", NOT_AVAILABLE_BULK_EDIT}});

        // assert project specific fields
        tester.assertTableRowsEqual(TABLE_EDITFIELDS_ID, 1, new String[][]
                {{"N/A", "Change Fix Version/s", WORKFLOW_TRANSITION_MULTI_PROJECT_ERROR}});
        tester.assertTableRowsEqual(TABLE_EDITFIELDS_ID, 5, new String[][]
                {{"N/A", "Change Patched Version", WORKFLOW_TRANSITION_MULTI_PROJECT_ERROR}});
        tester.assertTableRowsEqual(TABLE_EDITFIELDS_ID, 15, new String[][]
                {{"N/A", "Change Component/s", WORKFLOW_TRANSITION_MULTI_PROJECT_ERROR}});

        administration.removeGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
    }

    /**
     * Bulk edit all fields (system and custom fields) - check that all fields are edited correctly - check correct
     * fields available with permission schemes
     */
    @Test
    @Restore("TestBulkWorkflowTransition.xml")
    public void testWorkflowTransitionCompleteWalkthrough() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
        backdoor.darkFeatures().enableForSite("jira.no.frother.reporter.field");
        // Enable attachments
        administration.attachments().enable();
        editIssueFieldVisibility.setShownFields(ATTACHMENT_FIELD_ID);

        administration.addGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
        _testToOperationDetailsWorkflowTranisition();
        navigation.workflows().chooseWorkflowAction("classic default workflow_5_5");
        tester.assertTextPresent(WORKFLOW_TRANSITION_EDIT_TEXT);
        assertAllFieldsPresent();
        assertWikiRendererCommentField();
        editAllFields();
        // bulkWorkflowTransitionOperationDetails(true);
        tester.submit(BUTTON_NEXT);
        tester.assertTextPresent("Please confirm the details of this operation");

        // assert all edit fields are to be changed
        assertAllEditFieldsInUpdatedFieldTable();

        tester.submit(BUTTON_NEXT);
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        tester.assertTextInTable("issuetable", "Resolved");
        tester.assertTextInTable("issuetable", "Fixed");

        // Go to one issue and validate all changed
        navigation.issue().gotoIssue("TSTWO-6");

        // assert modified custom fields
        textAssertions.assertTextPresent(new IdLocator(tester, "customfield_10000-val"), "29/Nov/05 4:27 PM");
        textAssertions.assertTextPresent(new IdLocator(tester, "customfield_10002-val"), "Linux");
        textAssertions.assertTextPresent(new IdLocator(tester, "customfield_10003-val"), "1");
        textAssertions.assertTextPresent(new IdLocator(tester, "customfield_10004-val"), "http://www.atlassian.com");
        textAssertions.assertTextPresent(new IdLocator(tester, "customfield_10007-val"), "Functional test - management comment");
        textAssertions.assertTextPresent(new IdLocator(tester, "customfield_10008-val"), "Development");
        textAssertions.assertTextPresent(new IdLocator(tester, "customfield_10009-val"), ADMIN_FULLNAME);
        textAssertions.assertTextPresent(new IdLocator(tester, "customfield_10010-val"), "Test Project 1");
        textAssertions.assertTextPresent(new IdLocator(tester, "customfield_10013-val"), ADMIN_FULLNAME);
        textAssertions.assertTextPresent(new IdLocator(tester, "customfield_10014-val"), "Func Test - Limited text Field");
        textAssertions.assertTextPresent(new IdLocator(tester, "customfield_10006-val"), "21/Dec/05");

        IdLocator locator = new IdLocator(tester, "type-val");
        textAssertions.assertTextPresent(locator, "New Feature");

        locator = new IdLocator(tester, "status-val");
        textAssertions.assertTextPresent(locator, "Resolved");

        locator = new IdLocator(tester, "resolution-val");
        textAssertions.assertTextPresent(locator, "Fixed");

        locator = new IdLocator(tester, "priority-val");
        textAssertions.assertTextPresent(locator, "Critical");


        // Assert that a comment was added
        tester.assertTextPresent("Bulk Edit Comment");
        administration.removeGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);

        //assert that the index was correctly updated
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("status", "Resolved", "resolution", "Fixed"), null, "TSTWO-6");

        //assert bulk edit comment was indexed
        indexing.assertIndexedFieldCorrect("//comments", ImmutableMap.of("comment", "&lt;p&gt;Bulk Edit Comment&lt;/p&gt;"), null, "TSTWO-6");

        //assert customfield was indexed correctly
        indexing.assertIndexedFieldCorrect("//customfield/customfieldvalues", ImmutableMap.of("customfieldvalue", "Linux"), null, "TSTWO-6");

    }

    @Test
    @Restore("TestBulkTransition.xml")
    public void testWorkflowTransitionConcurrentIssueUpdate() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        tester.checkCheckbox("bulkedit_10011", "on");
        tester.checkCheckbox("bulkedit_10010", "on");
        tester.checkCheckbox("bulkedit_10000", "on");
        tester.submit(BUTTON_NEXT);
        tester.checkCheckbox("operation", "bulk.workflowtransition.operation.name");
        tester.submit(BUTTON_NEXT);
        tester.checkCheckbox("wftransition", "classic default workflow_2_6");
        tester.submit(BUTTON_NEXT);
        tester.selectOption("resolution", "Won't Fix");
        tester.submit(BUTTON_NEXT);

        changeOneOfTheIssuesConcurrently();

        // and then fire off our bulk transition
        tester.submit(BUTTON_NEXT);
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        tester.assertTextNotInTable("issueTable", new String[]{"Open", "Reopen"});
    }

    private void changeOneOfTheIssuesConcurrently() {
        sessionFactory.begin().withSession(() -> {
            tester.beginAt("/");
            navigation.login("admin");

            navigation.issue().gotoIssue("HSP-2");
            tester.clickLink("action_id_2");
            tester.setWorkingForm("issue-workflow-transition");
            tester.submit("Transition");
        });
    }

    /**
     * Check errors generated correctly (e.g. incorrect date input, etc.)
     */
    @Test
    @Restore("TestBulkWorkflowTransition.xml")
    public void testWorkflowTransitionEditFieldErrors() {
        administration.addGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
        _testToOperationDetailsWorkflowTranisition();
        navigation.workflows().chooseWorkflowAction("classic default workflow_5_5");
        tester.assertTextPresent(WORKFLOW_TRANSITION_EDIT_TEXT);
        generateMultipleInputErrors();
        administration.removeGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
    }

    @Test
    @Restore("TestBulkWorkflowTransition.xml")
    public void testWorkflowTransitionErrorOnWorkflowSelection() {
        administration.addGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
        _testToOperationDetailsWorkflowTranisition();
        tester.submit(BUTTON_NEXT);
        tester.assertTextPresent(WORKFLOW_TRANSITION_CHOOSE_ERROR_TEXT);
        administration.removeGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
    }

    @Test
    @Restore("TestBulkWorkflowTransition.xml")
    public void testBulkTransitionWithCommentVisibility() {
        navigation.comment().enableCommentGroupVisibility(Boolean.TRUE);

        ////////////////////// ALL User Comments /////////////////////////////
        // Do first two issues with comments viewable to all.
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        tester.checkCheckbox("bulkedit_10009", "on");
        tester.checkCheckbox("bulkedit_10008", "on");
        tester.submit(BUTTON_NEXT);

        // choose to transition issues
        tester.assertTextPresent("Choose the operation you wish to perform");
        bulkOperations.chooseOperationExecuteWorfklowTransition();
        navigation.workflows().assertStepOperationDetails();
        tester.assertTextPresent(WORKFLOW_TRANSITION_SELECTION_TEXT);

        // Choose the workflow to transition
        navigation.workflows().chooseWorkflowAction("classic default workflow_5_5");

        // edit fields
        tester.selectOption("resolution", "Fixed");
        tester.checkCheckbox("commentaction", "comment");
        tester.setFormElement("comment", COMMENT_1);
        // Submit to confirmation page
        tester.submit(BUTTON_NEXT);
        tester.assertTextPresent("Please confirm the details of this operation");
        // Final submit
        tester.submit(BUTTON_NEXT);
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        ////////////////////// Group Comments /////////////////////////////
        // Now transition next two issues with group comment
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        tester.checkCheckbox("bulkedit_10009", "on");
        tester.checkCheckbox("bulkedit_10008", "on");
        tester.submit(BUTTON_NEXT);

        // choose to transition issues
        tester.assertTextPresent("Choose the operation you wish to perform");
        bulkOperations.chooseOperationExecuteWorfklowTransition();
        navigation.workflows().assertStepOperationDetails();
        tester.assertTextPresent(WORKFLOW_TRANSITION_SELECTION_TEXT);

        // Choose the workflow to transition
        navigation.workflows().chooseWorkflowAction("classic default workflow_3_4");

        // edit the fields
        tester.checkCheckbox("commentaction", "comment");
        tester.selectOption("commentLevel", "jira-developers");
        tester.setFormElement("comment", COMMENT_2);
        // Submit to confirmation page
        tester.submit(BUTTON_NEXT);
        tester.assertTextPresent("Please confirm the details of this operation");
        // Final submit
        tester.submit(BUTTON_NEXT);
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        ////////////////////// ROLE Comments /////////////////////////////
        // Now transition next two issues with role comment
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        tester.checkCheckbox("bulkedit_10009", "on");
        tester.checkCheckbox("bulkedit_10008", "on");
        tester.submit(BUTTON_NEXT);

        // choose to transition issues
        tester.assertTextPresent("Choose the operation you wish to perform");
        bulkOperations.chooseOperationExecuteWorfklowTransition();
        navigation.workflows().assertStepOperationDetails();
        tester.assertTextPresent(WORKFLOW_TRANSITION_SELECTION_TEXT);

        // Choose the workflow to transition
        navigation.workflows().chooseWorkflowAction("classic default workflow_5_5");

        // edit the fields
        tester.selectOption("resolution", "Fixed");
        tester.checkCheckbox("commentaction", "comment");
        tester.selectOption("commentLevel", "Developers");
        tester.setFormElement("comment", COMMENT_3);
        // Submit to confirmation page
        tester.submit(BUTTON_NEXT);
        tester.assertTextPresent("Please confirm the details of this operation");
        // Final submit
        tester.submit(BUTTON_NEXT);
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        // verify that Fred can see general comment but not others as he is not in the visibility groups
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue("TSTWO-4");
        tester.assertTextPresent(COMMENT_1);
        tester.assertTextNotPresent(COMMENT_2);
        tester.assertTextNotPresent(COMMENT_3);
        navigation.issue().gotoIssue("TSTWO-5");
        tester.assertTextPresent(COMMENT_1);
        tester.assertTextNotPresent(COMMENT_2);
        tester.assertTextNotPresent(COMMENT_3);
        navigation.issue().gotoIssue("TSTWO-6");
        tester.assertTextNotPresent(COMMENT_1);
        tester.assertTextNotPresent(COMMENT_2);
        tester.assertTextNotPresent(COMMENT_3);

        final String xpathFormat = "//comments/comment[.=&quot;&lt;p&gt;%s&lt;/p&gt;&quot;]";

        //assert that the bulk editted workflow has been updated in the index and that Fred's permissions are applied to the index
        indexing.assertIndexContainsItem(String.format(xpathFormat, COMMENT_1), "TSTWO-4");
        indexing.assertIndexNotContainsItem(String.format(xpathFormat, COMMENT_2), "TSTWO-4");
        indexing.assertIndexNotContainsItem(String.format(xpathFormat, COMMENT_3), "TSTWO-4");

        indexing.assertIndexContainsItem(String.format(xpathFormat, COMMENT_1), "TSTWO-5");
        indexing.assertIndexNotContainsItem(String.format(xpathFormat, COMMENT_2), "TSTWO-5");
        indexing.assertIndexNotContainsItem(String.format(xpathFormat, COMMENT_3), "TSTWO-5");

        indexing.assertIndexNotContainsItem(String.format(xpathFormat, COMMENT_1), "TSTWO-6");
        indexing.assertIndexNotContainsItem(String.format(xpathFormat, COMMENT_2), "TSTWO-6");
        indexing.assertIndexNotContainsItem(String.format(xpathFormat, COMMENT_3), "TSTWO-6");

        // verify that Admin can see all comments as he is not in all visibility groups
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.issue().gotoIssue("TSTWO-4");
        tester.assertTextPresent(COMMENT_1);
        tester.assertTextPresent(COMMENT_2);
        tester.assertTextPresent(COMMENT_3);
        navigation.issue().gotoIssue("TSTWO-5");
        tester.assertTextPresent(COMMENT_1);
        tester.assertTextPresent(COMMENT_2);
        tester.assertTextPresent(COMMENT_3);
        navigation.issue().gotoIssue("TSTWO-6");
        tester.assertTextNotPresent(COMMENT_1);
        tester.assertTextNotPresent(COMMENT_2);
        tester.assertTextNotPresent(COMMENT_3);

        indexing.assertIndexContainsItem(String.format(xpathFormat, COMMENT_1), "TSTWO-4");
        indexing.assertIndexContainsItem(String.format(xpathFormat, COMMENT_2), "TSTWO-4");
        indexing.assertIndexContainsItem(String.format(xpathFormat, COMMENT_3), "TSTWO-4");

        indexing.assertIndexContainsItem(String.format(xpathFormat, COMMENT_1), "TSTWO-5");
        indexing.assertIndexContainsItem(String.format(xpathFormat, COMMENT_2), "TSTWO-5");
        indexing.assertIndexContainsItem(String.format(xpathFormat, COMMENT_3), "TSTWO-5");

        indexing.assertIndexNotContainsItem(String.format(xpathFormat, COMMENT_1), "TSTWO-6");
        indexing.assertIndexNotContainsItem(String.format(xpathFormat, COMMENT_2), "TSTWO-6");
        indexing.assertIndexNotContainsItem(String.format(xpathFormat, COMMENT_3), "TSTWO-6");
    }

    private void assertAllEditFieldsInUpdatedFieldTable() {
        tester.assertTableRowsEqual("updatedfields", 1, new String[][]
                {{"Assignee", ADMIN_FULLNAME}});
        tester.assertTableRowsEqual("updatedfields", 2, new String[][]
                {{"Test Run Date", "29/Nov/05 4:27 PM"}});
        tester.assertTableRowsEqual("updatedfields", 3, new String[][]
                {{"Test Run Number", "1"}});
        tester.assertTableRowsEqual("updatedfields", 4, new String[][]
                {{"Release Date", "21/Dec/05"}});
        tester.assertTableRowsEqual("updatedfields", 5, new String[][]
                {{"Management Comments", "Functional test - management comment"}});
        tester.assertTableRowsEqual("updatedfields", 6, new String[][]
                {{"Affected Business Units", "Development"}});
        tester.assertTableRowsEqual("updatedfields", 7, new String[][]
                {{"Affected Users", ADMIN_FULLNAME}});
        tester.assertTableRowsEqual("updatedfields", 8, new String[][]
                {{"Related Projects", "Test Project 1"}});
        tester.assertTableRowsEqual("updatedfields", 9, new String[][]
                {{"End User", ADMIN_FULLNAME}});
        tester.assertTableRowsEqual("updatedfields", 10, new String[][]
                {{"LImtied Text Field", "Func Test - Limited text Field"}});
        tester.assertTableRowsEqual("updatedfields", 11, new String[][]
                {{"Description", "Func Test - Description"}});
        tester.assertTableRowsEqual("updatedfields", 12, new String[][]
                {{"Due Date", "29/Dec/05"}});
        tester.assertTableRowsEqual("updatedfields", 13, new String[][]
                {{"Environment", "Func test environment"}});
        tester.assertTableRowsEqual("updatedfields", 14, new String[][]
                {{"Issue Type", "New Feature"}});
        tester.assertTableRowsEqual("updatedfields", 15, new String[][]
                {{"Operating System", "Linux"}});
        tester.assertTableRowsEqual("updatedfields", 16, new String[][]
                {{"Priority", "Critical"}});
        tester.assertTableRowsEqual("updatedfields", 17, new String[][]
                {{"Reporter", ADMIN_FULLNAME}});
        tester.assertTableRowsEqual("updatedfields", 18, new String[][]
                {{"Web Address", "http://www.atlassian.com"}});
        tester.assertTableRowsEqual("updatedfields", 19, new String[][]
                {{"Resolution", "Fixed"}});
        tester.assertTableRowsEqual("updatedfields", 20, new String[][]
                {{"Comment", "Bulk Edit Comment"}});
    }

    private void assertAllFieldsPresent() {
        tester.assertTextPresent("Change Resolution");
        tester.assertTextPresent("Change Fix Version/s");
        tester.assertTextPresent("Change Assignee");
        tester.assertTextPresent("Change Test Run Date");
        tester.assertTextPresent("Change Test Run Number");
        tester.assertTextPresent("Change Patched Version");
        tester.assertTextPresent("Change Release Date");
        tester.assertTextPresent("Change Management Comments");
        tester.assertTextPresent("Change Affected Business Units");
        tester.assertTextPresent("Change Affected Users");
        tester.assertTextPresent("Change Related Projects");
        tester.assertTextPresent("Change End User");
        tester.assertTextPresent("Change LImtied Text Field");
        tester.assertTextPresent("Change Affects Version/s");
        tester.assertTextPresent("Change Component/s");
        tester.assertTextPresent("Change Description");
        tester.assertTextPresent("Change Due Date");
        tester.assertTextPresent("Change Environment");
        tester.assertTextPresent("Change Import Id");
        tester.assertTextPresent("Change Issue Type");
        tester.assertTextPresent("Change Operating System");
        tester.assertTextPresent("Change Priority");
        tester.assertTextPresent("Change Read Only Field");
        tester.assertTextPresent("Change Reporter");
        tester.assertTextPresent("Change Summary");
        tester.assertTextPresent("Change Web Address");
        tester.assertTextPresent("Change Time Tracking");
        tester.assertTextPresent("Change Comment");
    }

    private void assertWikiRendererCommentField() {
        tester.assertTextPresent("comment-preview_link");
        tester.assertLinkPresent("viewHelp");
    }

    private void editAllFields() {
        tester.checkCheckbox("actions", "resolution");

        tester.selectOption("resolution", "Fixed");

        tester.checkCheckbox("actions", "assignee");
        tester.selectOption("assignee", ADMIN_FULLNAME);

        tester.checkCheckbox("actions", "customfield_10000");
        tester.setFormElement("customfield_10000", "29/Nov/2005 04:27 PM");

        tester.checkCheckbox("actions", "customfield_10003");
        tester.setFormElement("customfield_10003", "1");

        tester.checkCheckbox("actions", "customfield_10006");
        tester.setFormElement("customfield_10006", "21/Dec/2005");

        tester.checkCheckbox("actions", "customfield_10007");
        tester.setFormElement("customfield_10007", "Functional test - management comment");

        tester.checkCheckbox("actions", "customfield_10008");
        tester.checkCheckbox("customfield_10008", "10006");

        tester.checkCheckbox("actions", "customfield_10009");
        tester.setFormElement("customfield_10009", ADMIN_USERNAME);

        tester.checkCheckbox("actions", "customfield_10010");
        tester.selectOption("customfield_10010", "Test Project 1");

        tester.checkCheckbox("actions", "customfield_10013");
        tester.setFormElement("customfield_10013", ADMIN_USERNAME);

        tester.checkCheckbox("actions", "customfield_10014");
        tester.setFormElement("customfield_10014", "Func Test - Limited text Field");

        tester.checkCheckbox("actions", "description");
        tester.setFormElement("description", "Func Test - Description");

        tester.checkCheckbox("actions", "duedate");
        tester.setFormElement("duedate", "29/Dec/2005");

        tester.checkCheckbox("actions", "environment");
        tester.setFormElement("environment", "Func test environment");

        tester.checkCheckbox("actions", "issuetype");
        navigation.issue().selectIssueType("New Feature", "issuetype");

        tester.checkCheckbox("actions", "customfield_10002");
        tester.selectOption("customfield_10002", "Linux");

        tester.checkCheckbox("actions", "priority");
        tester.selectOption("priority", "Critical");

        tester.checkCheckbox("actions", "reporter");
        tester.setFormElement("reporter", ADMIN_USERNAME);

        tester.checkCheckbox("actions", "customfield_10004");
        tester.setFormElement("customfield_10004", "http://www.atlassian.com");

        tester.checkCheckbox("commentaction", "comment");
        tester.setFormElement("comment", "Bulk Edit Comment");
    }

    private void generateMultipleInputErrors() {
        // Set invalid data
        tester.checkCheckbox("actions", "resolution");
        tester.selectOption("resolution", "Fixed");
        tester.checkCheckbox("actions", "duedate");
        tester.setFormElement("duedate", "functest");
        tester.checkCheckbox("actions", "customfield_10000");
        tester.setFormElement("customfield_10000", "functest");
        tester.checkCheckbox("actions", "customfield_10004");
        tester.setFormElement("customfield_10004", "functest");
        tester.checkCheckbox("actions", "customfield_10006");
        tester.setFormElement("customfield_10006", "functest");
        tester.checkCheckbox("actions", "customfield_10009");
        tester.setFormElement("customfield_10009", "functest");

        tester.submit(BUTTON_NEXT);

        // Assert error messages:
        tester.assertTextPresent("You did not enter a valid date. Please enter the date in the format");
        tester.assertTextPresent("Not a valid URL");
        tester.assertTextPresent("Invalid date format. Please enter the date in the format &quot;d/MMM/yy&quot;.");
    }

    private void verifyAtSessionTimeoutPage() {
        tester.assertTextPresent("Your session timed out while performing bulk operation on issues.");
    }

    private void _testToOperationDetailsWorkflowTranisition() {
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        tester.assertTextPresent("Step 1 of 4: Choose Issues");
        bulkOperations.bulkChangeChooseIssuesAll();
        tester.assertTextPresent("Choose the operation you wish to perform");
        bulkOperations.chooseOperationExecuteWorfklowTransition();
        navigation.workflows().assertStepOperationDetails();
        tester.assertTextPresent(WORKFLOW_TRANSITION_SELECTION_TEXT);
    }

    /**
     * Multiple workflows - Make sure multiple present - only select one transition
     */
    @Test
    @Restore("TestBulkWorkflowTransitionEnterprise.xml")
    public void testBulkWorkFlowTransitionMultipleWorkflowsExists() {
        navigation.gotoDashboard();
        administration.addGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
        _testToOperationDetailsWorkflowTranisition();
        tester.submit(BUTTON_NEXT);
        navigation.workflows().assertStepOperationDetails();
        // Assert that two tables exist - thus two workflows (Enterprise feature)
        tester.assertTablePresent("workflow_0");
        tester.assertTablePresent("workflow_1");
    }

    @Test
    @Restore("TestBulkWorkflowTransitionEnterprise4.xml")
    public void testFunkyWorkflowName() {
        navigation.gotoDashboard();
        administration.addGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
        _testToOperationDetailsWorkflowTranisition();
        tester.checkCheckbox("wftransition", "Second_Workflow_4_3");
        tester.submit(BUTTON_NEXT);
        tester.assertTextPresent(WORKFLOW_TRANSITION_EDIT_TEXT);
    }

    @Test
    @Restore("TestBulkWorkflowTranistionForCustomFieldContextAndIssueType.xml")
    public void testCustomFieldContextAndIssueTypeBehaviour() {
        // Display all the
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();

        // Select the two 'Monkey' Issues
        tester.checkCheckbox("bulkedit_10002", "on");
        tester.checkCheckbox("bulkedit_10001", "on");
        tester.submit(BUTTON_NEXT);

        // Select the transition workflow operation
        tester.checkCheckbox("operation", "bulk.workflowtransition.operation.name");
        tester.submit(BUTTON_NEXT);

        // Select to resolve these two issues
        navigation.workflows().chooseWorkflowAction("classic default workflow_5_5");

        // Make sure we are on the edit screen for the bulk workflow transition operation.
        tester.assertTextPresent(WORKFLOW_TRANSITION_EDIT_TEXT);

        // Now check which fields should be displayed/not displayed
        // Select Resolution
        tester.checkCheckbox("actions", "resolution");
        tester.selectOption("resolution", "Fixed");

        // We can edit the Monkey Custom field (context = Project Monkey)
        tester.checkCheckbox("actions", "customfield_10000");
        tester.setFormElement("customfield_10000", "Setting Monkey Custom Field");

        // We can't edit Man Custom Field (context = Project Homosapien, Issue Type = Bug)
        tester.assertTableRowsEqual(TABLE_EDITFIELDS_ID, 4, new String[][]{
                {"N/A", "Change Man Custom Field", "NOTE: The field is not available for all issues with the same configuration."}});

        // We can edit the Man and Monkey custom field (context is for both projects - Not global though!)
        tester.checkCheckbox("actions", "customfield_10010");
        tester.checkCheckbox("customfield_10010", "10001");

        // We can edit the Global Field since it is global
        tester.checkCheckbox("actions", "customfield_10011");
        tester.setFormElement("customfield_10011", "Setting the global field");

        // We can edit the 'Bug Only Field' since it is available for Issues of type Bug in a global context.
        tester.checkCheckbox("actions", "customfield_10012");
        tester.setFormElement("customfield_10012", "8/Sep/06");

        tester.submit(BUTTON_NEXT);

        // Assert that all the fields that are going to be updated are present.
        tester.assertTextInTable("updatedfields", "Fixed");
        tester.assertTextInTable("updatedfields", "Setting Monkey Custom Field");
        tester.assertTextInTable("updatedfields", "Monkey");
        tester.assertTextInTable("updatedfields", "Setting the global field");
        tester.assertTextInTable("updatedfields", "08/Sep/06");

        // Submit the form
        tester.submit(BUTTON_NEXT);

        bulkOperations.waitAndReloadBulkOperationProgressPage();

        // Go to one of the issues and make sure it has been correctly transitioned and updated
        navigation.issue().gotoIssue("MKY-2");

        textAssertions.assertTextPresent(new IdLocator(tester, "rowForcustomfield_10000"), "Setting Monkey Custom Field");
        textAssertions.assertTextPresent(new IdLocator(tester, "rowForcustomfield_10010"), "Monkey");
        textAssertions.assertTextPresent(new IdLocator(tester, "rowForcustomfield_10011"), "Setting the global field");
        textAssertions.assertTextPresent(new IdLocator(tester, "customfield_10012-val"), "08/Sep/06");

        //assert that that workflow transitions have been applied and index is updated
        indexing.assertIndexContainsItem("//customfields/customfield/customfieldvalues/customfieldvalue[.=&quot;Setting Monkey Custom Field&quot;]", "MKY-2");
        indexing.assertIndexContainsItem("//customfields/customfield/customfieldvalues/customfieldvalue[.=&quot;Setting the global field&quot;]", "MKY-2");
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("resolution", "Fixed", "status", "Resolved"), null, "MKY-2");
    }

    @Test
    @Restore("TestBulkWorkflowTransition.xml")
    public void testMultipleOriginStatusesShownDuringTransitionScreen() {
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        tester.checkCheckbox("bulkedit_10010", "on"); // TSTWO-6 - status: REOPENED
        tester.checkCheckbox("bulkedit_10008", "on"); // TSTWO-4 - status: OPEN
        tester.checkCheckbox("bulkedit_10002", "on"); // TST-3   - status: RESOLVED
        tester.submit(BUTTON_NEXT);

        // Select the transition workflow operation.
        tester.checkCheckbox("operation", "bulk.workflowtransition.operation.name");
        tester.submit(BUTTON_NEXT);

        // Start checking.
        final IdLocator row0 = new IdLocator(tester, "origin_0");
        textAssertions.assertTextPresent(row0.getNodes()[0].getChildNodes().item(1).getTextContent(), "Open");
        textAssertions.assertTextPresent(row0.getNodes()[0].getChildNodes().item(3).getTextContent(), "In Progress");
        textAssertions.assertTextPresent(row0.getNodes()[0].getChildNodes().item(5).getTextContent(), "Reopened");
        textAssertions.assertTextPresent(new IdLocator(tester, "target_0"), "Closed");
        textAssertions.assertTextPresent(new IdLocator(tester, "short_classic default workflow_2_6"), "TSTWO-4");
        textAssertions.assertTextPresent(new IdLocator(tester, "short_classic default workflow_2_6"), "TSTWO-6");

        final IdLocator row1 = new IdLocator(tester, "origin_1");
        textAssertions.assertTextPresent(row1.getNodes()[0].getChildNodes().item(1).getTextContent(), "Resolved");
        textAssertions.assertTextPresent(row1.getNodes()[0].getChildNodes().item(3).getTextContent(), "Closed");
        textAssertions.assertTextPresent(new IdLocator(tester, "target_1"), "Reopened");
        textAssertions.assertTextPresent(new IdLocator(tester, "short_classic default workflow_3_4"), "TST-3");

        final IdLocator row2 = new IdLocator(tester, "origin_2");
        textAssertions.assertTextPresent(row2.getNodes()[0].getChildNodes().item(1).getTextContent(), "Open");
        textAssertions.assertTextPresent(row2.getNodes()[0].getChildNodes().item(3).getTextContent(), "Reopened");
        textAssertions.assertTextPresent(new IdLocator(tester, "target_2"), "In Progress");
        textAssertions.assertTextPresent(new IdLocator(tester, "short_classic default workflow_4_3"), "TSTWO-4");
        textAssertions.assertTextPresent(new IdLocator(tester, "short_classic default workflow_4_3"), "TSTWO-6");

        final IdLocator row3 = new IdLocator(tester, "origin_3");
        textAssertions.assertTextPresent(row3.getNodes()[0].getChildNodes().item(1).getTextContent(), "Open");
        textAssertions.assertTextPresent(row3.getNodes()[0].getChildNodes().item(3).getTextContent(), "In Progress");
        textAssertions.assertTextPresent(row3.getNodes()[0].getChildNodes().item(5).getTextContent(), "Reopened");
        textAssertions.assertTextPresent(new IdLocator(tester, "target_3"), "Resolved");
        textAssertions.assertTextPresent(new IdLocator(tester, "short_classic default workflow_5_5"), "TSTWO-4");
        textAssertions.assertTextPresent(new IdLocator(tester, "short_classic default workflow_5_5"), "TSTWO-6");

        final IdLocator row4 = new IdLocator(tester, "origin_4");
        textAssertions.assertTextPresent(row4.getNodes()[0].getChildNodes().item(1).getTextContent(), "Resolved");
        textAssertions.assertTextPresent(new IdLocator(tester, "target_4"), "Closed");
        textAssertions.assertTextPresent(new IdLocator(tester, "short_classic default workflow_701_6"), "TST-3");

        tester.assertTableRowsEqual("workflow_0", 5, new String[][]{{"Close Issue", "Resolved", "Closed", "TST-3"}});

        navigation.workflows().chooseWorkflowAction("classic default workflow_2_6");

        final IdLocator editsRow = new IdLocator(tester, "workflow-transition-edit-fields-status-list");
        textAssertions.assertTextPresent(editsRow.getNodes()[0].getChildNodes().item(1).getTextContent(), "Open");
        textAssertions.assertTextPresent(editsRow.getNodes()[0].getChildNodes().item(3).getTextContent(), "In Progress");
        textAssertions.assertTextPresent(editsRow.getNodes()[0].getChildNodes().item(5).getTextContent(), "Reopened");
        textAssertions.assertTextPresent(editsRow.getNodes()[0].getChildNodes().item(9).getTextContent(), "Closed");

        tester.checkCheckbox("actions", "resolution");
        tester.selectOption("resolution", "Fixed");
        tester.submit(BUTTON_NEXT);

        final IdLocator confirmationRow = new IdLocator(tester, "workflow-transition-confirmation-status-list");
        textAssertions.assertTextPresent(confirmationRow.getNodes()[0].getChildNodes().item(1).getTextContent(), "Open");
        textAssertions.assertTextPresent(confirmationRow.getNodes()[0].getChildNodes().item(3).getTextContent(), "In Progress");
        textAssertions.assertTextPresent(confirmationRow.getNodes()[0].getChildNodes().item(5).getTextContent(), "Reopened");
        textAssertions.assertTextPresent(confirmationRow.getNodes()[0].getChildNodes().item(9).getTextContent(), "Closed");
    }
}
