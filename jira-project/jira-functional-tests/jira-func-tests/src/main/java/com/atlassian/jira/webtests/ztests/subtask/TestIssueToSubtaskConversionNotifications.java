package com.atlassian.jira.webtests.ztests.subtask;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraEmailTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.rule.MailTest;
import org.hamcrest.Matchers;
import org.junit.Test;

import javax.mail.internet.MimeMessage;
import java.util.Collection;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.matcher.MimeMessageMatchers.containsString;
import static com.atlassian.jira.functest.matcher.MimeMessageMatchers.withRecipientEqualTo;
import static org.hamcrest.MatcherAssert.assertThat;

@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.SUB_TASKS})
@LoginAs(user = ADMIN_USERNAME)
@MailTest
@Restore("TestIssueToSubtaskConversionNotifications.xml")
public class TestIssueToSubtaskConversionNotifications extends BaseJiraEmailTest {
    private static final String ISSUE_TO_CONVERT_ID = "10001";
    private static final String PARENT_ISSUE = "TEST-1";
    private static final String SUBTASK_TYPE_ID = "5";

    public static final String EMAIL_ADDRESS = "admin@example.com";

    @Test
    public void testEmailIsSentWhenIssueIsConvertedToSubtask() throws Exception {
        convertIssueToSubtask(ISSUE_TO_CONVERT_ID);

        assertOneEmailWasSent(EMAIL_ADDRESS, "updated", "an issue", "Change By:", "Parent:", "TEST-1", "TEST-2", "Bug",
                "Sub-task", "Add Comment", "ViewProfile.jspa?name=admin", "This message was sent by Atlassian JIRA");
    }

    public void convertIssueToSubtask(String issueId) {
        tester.gotoPage("/secure/ConvertIssueSetIssueType.jspa?id=" + issueId + "&parentIssueKey=" + PARENT_ISSUE +
                "&issuetype=" + SUBTASK_TYPE_ID);
        tester.submit("Next >>");
        tester.submit("Finish");
    }

    private void assertOneEmailWasSent(String emailAddress, String... expectedTexts) throws Exception {
        final Collection<MimeMessage> emails = mailHelper.flushMailQueueAndWait(1);
        final MimeMessage email = emails.iterator().next();

        for (String expectedText : expectedTexts) {
            assertThat(email, Matchers.allOf(withRecipientEqualTo(emailAddress), containsString(expectedText)));
        }
    }
}
