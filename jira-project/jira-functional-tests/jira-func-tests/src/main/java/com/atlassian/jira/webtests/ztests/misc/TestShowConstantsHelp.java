package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;

/**
 *
 */
@WebTest({Category.FUNC_TEST, Category.BROWSING})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestShowConstantsHelp.xml")
public class TestShowConstantsHelp extends BaseJiraFuncTest {

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testShowConstantsHelpHasPermission() {
        tester.gotoPage("secure/ShowConstantsHelp.jspa");
        tester.assertTextPresent("JIRA can be used to track many different types of issues");
    }

    @Test
    @RestoreBlankInstance
    public void testShowConstantsHelpDoesNotHavePermission() {
        navigation.logout();
        tester.gotoPage("secure/ShowConstantsHelp.jspa");
        tester.assertTextPresent("You are not logged in");
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void testShowConstantsHelpSecurityLevels() {
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");
        tester.gotoPage("secure/ShowConstantsHelp.jspa");
        tester.assertTextPresent("Security Levels");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Security Level 1", "Only developers are allowed to view this issue");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Security Level 2", "All users are allowed to view this issue");
    }

    @Test
    @LoginAs(user = FRED_USERNAME)
    public void testShowConstantsHelpSecurityLevelsSecurity() {
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");
        tester.gotoPage("secure/ShowConstantsHelp.jspa");
        tester.assertTextPresent("Security Levels");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Security Level 2", "All users are allowed to view this issue");
        tester.assertTextNotPresent("Security Level 1");
        tester.assertTextNotPresent("Only developers are allowed to view this issue");
    }
}
