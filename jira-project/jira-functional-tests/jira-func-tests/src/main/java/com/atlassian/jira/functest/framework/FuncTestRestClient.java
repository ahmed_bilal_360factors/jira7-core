package com.atlassian.jira.functest.framework;

import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.HeaderOnlyWebRequest;
import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.PutMethodWebRequest;
import com.meterware.httpunit.WebResponse;
import net.sourceforge.jwebunit.WebTester;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.Collections;
import java.util.Map;

/**
 * Externalised from RestFuncTest junit 3 base class. Inject this into your test class.
 *
 * @since v7.1
 */
public class FuncTestRestClient {

    private final WebTester tester;
    private final FuncTestUrlHelper funcTestUrlHelper;

    @Inject
    public FuncTestRestClient(final WebTester tester, final JIRAEnvironmentData environmentData) {
        this.tester = tester;
        this.funcTestUrlHelper = new FuncTestUrlHelper(environmentData);
    }

    public JSONObject getJSON(final String url, final String... expand) throws JSONException {
        final String queryString = (expand != null && expand.length > 0) ? ("?expand=" + StringUtils.join(expand, ',')) : "";
        tester.gotoPage(url + queryString);
        return new JSONObject(tester.getDialog().getResponseText());
    }

    public WebResponse GET(final String url) throws IOException, SAXException {
        return GET(url, Collections.<String, String>emptyMap());
    }

    public WebResponse GET(final String url, final Map<String, String> headers) throws IOException, SAXException {
        tester.getDialog().getWebClient().setExceptionsThrownOnErrorStatus(false);
        HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
        for (final Map.Entry<String, String> headerField : headers.entrySet()) {
            tester.getDialog().getWebClient().setHeaderField(headerField.getKey(), headerField.getValue());
        }

        final GetMethodWebRequest request = new GetMethodWebRequest(funcTestUrlHelper.getBaseUrlPlus(url));
        return tester.getDialog().getWebClient().sendRequest(request);
    }

    public WebResponse DELETE(final String url) throws IOException, SAXException {
        tester.getDialog().getWebClient().setExceptionsThrownOnErrorStatus(false);
        HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);

        final HeaderOnlyWebRequest delete = new HeaderOnlyWebRequest(funcTestUrlHelper.getBaseUrlPlus(url)) {
            @Override
            public String getMethod() {
                return "DELETE";
            }

            // If you don't override this then the above getMethod() never gets called and the request goes through
            // as a GET. Thanks httpunit.
            @Override
            protected void completeRequest(final URLConnection connection) throws IOException {
                ((HttpURLConnection) connection).setRequestMethod(getMethod());
                super.completeRequest(connection);
            }
        };

        return tester.getDialog().getWebClient().sendRequest(delete);
    }

    public WebResponse POST(final String url, final JSONObject json) throws IOException, SAXException {
        return POST(url, json.toString());
    }

    public WebResponse POST(final String url, final String postBody) throws IOException, SAXException {
        tester.getDialog().getWebClient().setExceptionsThrownOnErrorStatus(false);
        HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);

        final PostMethodWebRequest request = new PostMethodWebRequest(funcTestUrlHelper.getBaseUrlPlus(url), new ByteArrayInputStream(postBody.getBytes()), "application/json");
        return tester.getDialog().getWebClient().sendRequest(request);
    }

    public WebResponse PUT(final String url, final JSONObject json) throws IOException, SAXException {
        return PUT(url, json.toString());
    }

    public WebResponse PUT(final String url, final String postBody) throws IOException, SAXException {
        tester.getDialog().getWebClient().setExceptionsThrownOnErrorStatus(false);
        HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);

        final PutMethodWebRequest request = new PutMethodWebRequest(funcTestUrlHelper.getBaseUrlPlus(url), new ByteArrayInputStream(postBody.getBytes()), "application/json");
        return tester.getDialog().getWebClient().sendRequest(request);
    }
}
