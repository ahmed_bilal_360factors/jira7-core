package com.atlassian.jira.webtests.ztests.email;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BulkOperationProgress;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.navigation.IssueNavigatorNavigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.EmailBaseFuncTestCase;
import com.atlassian.jira.webtests.util.issue.IssueInlineEdit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@WebTest({Category.FUNC_TEST, Category.EMAIL})
@LoginAs(user = FunctTestConstants.ADMIN_USERNAME)
@Restore(value = "TestCurrentAssigneeNotifications.xml")
public class TestCurrentAssigneeNotifications extends EmailBaseFuncTestCase {

    public static final String HOMER = "homer";

    @Inject
    private BulkOperationProgress bulkOperationProgress;

    @Inject
    private LocatorFactory locator;

    @Before
    public void disableFrotherAssigneeField() {
        backdoor.darkFeatures().enableForSite("no.frother.assignee.field");
    }

    @Before
    public void restoreDataAndConfigureSmtp() {
        configureAndStartSmtpServer();
        backdoor.darkFeatures().enableForSite("no.frother.assignee.field");
    }

    @After
    public void enableFrotherAssigneeField() {
        backdoor.darkFeatures().disableForSite("no.frother.assignee.field");
    }

    @Test
    public void testAssignIssueSendsEmailToNewAndOldAssignees() throws Exception {
        navigation.issue().assignIssue("HSP-1", "This has been re-assigned", HOMER);
        assertHomerAndBartsEmails(1, 1);
    }

    @Test
    public void testAssignIssueTriggersIssueAssignedEvent() throws Exception {
        navigation.issue().assignIssue("HSP-1", "This has been re-assigned", HOMER);

        assertEmailSentToUserListeningOnlyToIssueAssignedEvents();
    }

    @Test
    public void testEditIssueChangingAssigneeSendsEmailToNewAndOldAssignees() throws Exception {
        editIssueAndChangeAssignee("HSP-1", "This has been re-assigned", HOMER);
        assertHomerAndBartsEmails(1, 1);
    }

    @Test
    public void testEditIssueChangingAssigneeTriggersIssueAssignedEvent() throws Exception {
        editIssueAndChangeAssignee("HSP-1", "This has been re-assigned", HOMER);

        assertEmailSentToUserListeningOnlyToIssueAssignedEvents();
    }

    @Test
    public void testTransitionIssueChangingAssigneeSendsEmailToNewAndOldAssignees() throws Exception {
        workflowIssueAndChangeAssignee("HSP-1", HOMER);
        assertHomerAndBartsEmails(1, 1);
    }

    @Test
    public void testTransitionIssueChangingAssigneeTriggersIssueAssignedEvent() throws Exception {
        workflowIssueAndChangeAssignee("HSP-1", HOMER);

        assertEmailSentToUserListeningOnlyToIssueAssignedEvents();
    }

    @Test
    public void testBulkEditIssueChangingAssigneeSendsEmailToNewAndOldAssignees() throws Exception {
        bulkEditHSP_1(HOMER);
        assertHomerAndBartsEmails(1, 1);
    }

    @Test
    public void testBulkEditIssueChangingAssigneeTriggersIssueAssignedEvent() throws Exception {
        bulkEditHSP_1(HOMER);

        assertEmailSentToUserListeningOnlyToIssueAssignedEvents();
    }

    @Test
    public void testBulkTransitionIssueChangingAssigneeSendsEmailToNewAndOldAssignees() throws Exception {
        bulkTransitionHSP_1(HOMER);
        assertHomerAndBartsEmails(1, 1);
    }

    @Test
    public void testBulkTransitionIssueChangingAssigneeTriggersIssueAssignedEvent() throws Exception {
        bulkTransitionHSP_1(HOMER);

        assertEmailSentToUserListeningOnlyToIssueAssignedEvents();
    }

    @Test
    public void testInlineEditingAssigneeSendsEmailToNewAndOldAssignees() throws Exception {
        inlineAssign("HSP-1", "10000", HOMER);

        assertHomerAndBartsEmails(1, 1);
    }

    @Test
    public void testInlineEditingAssigneeTriggersIssueAssignedEvent() throws Exception {
        inlineAssign("HSP-1", "10000", HOMER);

        assertEmailSentToUserListeningOnlyToIssueAssignedEvents();
    }

    private void inlineAssign(final String issueKey, final String issueId, final String newAssignee) throws Exception {
        navigation.issue().gotoIssue(issueKey);

        final IssueInlineEdit inlineEdit = new IssueInlineEdit(locator, tester, environmentData);
        inlineEdit.inlineEditField(issueId, "assignee", newAssignee);
    }

    private void bulkEditHSP_1(final String newAssigneeName) {
        // Click Link 'find issues' (id='find_link').
        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator().bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES);
        tester.checkCheckbox("bulkedit_10000", "on");
        tester.submit("Next");
        tester.checkCheckbox("operation", "bulk.edit.operation.name");
        tester.submit("Next");
        tester.checkCheckbox("actions", "assignee");
        // Select 'Administrator' from select box 'assignee'.
        tester.selectOption("assignee", newAssigneeName);
        tester.checkCheckbox("sendBulkNotification", "true");
        tester.submit("Next");
        tester.submit("Confirm");

        bulkOperationProgress.waitAndReloadBulkOperationProgressPage();
    }

    private void bulkTransitionHSP_1(final String newAssigneeName) {
        // Click Link 'find issues' (id='find_link').
        navigation.issueNavigator().displayAllIssues();
        // Click Link 'all 1 issue(s)' (id='bulkedit_all').
        navigation.issueNavigator().bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES);
        tester.checkCheckbox("bulkedit_10000", "on");
        tester.submit("Next");
        tester.checkCheckbox("operation", "bulk.workflowtransition.operation.name");
        tester.submit("Next");
        tester.checkCheckbox("wftransition", "jira_5_5");
        tester.submit("Next");
        // Select 'Won't Fix' from select box 'resolution'.
        tester.checkCheckbox("actions", "resolution");
        tester.selectOption("resolution", "Won't Fix");

        tester.checkCheckbox("actions", "assignee");
        tester.selectOption("assignee", newAssigneeName);
        tester.checkCheckbox("sendBulkNotification", "true");
        tester.submit("Next");
        tester.submit("Next");

        bulkOperationProgress.waitAndReloadBulkOperationProgressPage();
    }

    private void editIssueAndChangeAssignee(final String issueKey, final String commentStr, final String newAssigneeName) {
        navigation.issue().viewIssue(issueKey);
        tester.clickLink("edit-issue");

        tester.selectOption("assignee", newAssigneeName);
        tester.setFormElement("comment", commentStr);
        tester.submit("Update");
    }

    private void workflowIssueAndChangeAssignee(final String issueKey, final String newAssigneeName) {
        navigation.issue().viewIssue(issueKey);
        tester.clickLink("action_id_5");
        tester.setWorkingForm("issue-workflow-transition");
        tester.selectOption("resolution", "Won't Fix");
        tester.selectOption("assignee", newAssigneeName);
        tester.submit("Transition");
    }


    private void assertHomerAndBartsEmails(final int expectedHomerEmailCount, final int expectedBartEmailCount)
            throws MessagingException, InterruptedException, IOException {
        flushMailQueueAndWaitForRecipients(5000, "homer@localhost", "bart@localhost");
        assertHomerWasAssignedEmails("homer@localhost", expectedHomerEmailCount);
        assertHomerWasAssignedEmails("bart@localhost", expectedBartEmailCount);
    }

    private void assertHomerWasAssignedEmails(final String emailAddress, final int expectedEmailCount)
            throws MessagingException, IOException {
        final List messagesForRecipient = getMessagesForRecipient(emailAddress);
        assertEquals(expectedEmailCount, messagesForRecipient.size());

        for (final Object msg : messagesForRecipient) {
            final MimeMessage message = (MimeMessage) msg;
            final String subject = message.getSubject();
            assertTrue(subject.contains("HSP-1"));
            assertEmailBodyContains(message, "HSP-1");
            assertEmailBodyContains(message, "Assignee:");
            assertEmailBodyContainsLine(message, ".*diffremovedchars.*bart.*");
            assertEmailBodyContainsLine(message, ".*diffaddedchars.*homer.*");
        }
    }

    private void assertEmailSentToUserListeningOnlyToIssueAssignedEvents() throws Exception {
        flushMailQueueAndWaitForRecipients(5000, "test@test.com");

        final List<MimeMessage> messagesForRecipient = getMessagesForRecipient("test@test.com");
        assertThat(messagesForRecipient.size(), is(1));
    }
}
