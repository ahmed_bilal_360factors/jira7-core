package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FuncTestRestClient;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.SkipSetup;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.setup.JiraSetupInstanceHelper;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.rule.SkipCacheCheck;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.meterware.httpunit.WebResponse;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.collect.Iterables.transform;
import static org.apache.commons.lang3.StringUtils.join;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @since v4.3
 */
@WebTest({Category.FUNC_TEST, Category.SETUP, Category.DATABASE})
@SkipSetup
@SkipCacheCheck
public class TestDatabaseSetup extends BaseJiraFuncTest {

    @Inject
    private FuncTestRestClient restClient;

    @Inject
    private FuncTestLogger logger;

    /**
     * Because we cannot guarantee the order of test executions and at the end we need to have the db configure
     */
    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testAll() throws IOException, SAXException, JSONException {
        if (getEnvironmentData().getProperty("databaseType") != null) {
            tester.getDialog().getWebClient().getClientProperties().setHiddenFieldsEditable(true);
            // Direct JDBC tests
            _testDirectJdbcSuccessful();
            _testDirectJdbcMissingHostname();
            _testDirectJdbcMissingPortNumber();
            _testDirectJdbcInvalidPort();
            _testDirectJdbcIncorrectPort();
            _testDirectJdbcMissingUsername();
            _testDirectJdbcInvalidCredential();

            // Set up database properly
            configureDirectJdbc();
        } else {
            logger.log("Skipping TestDatabaseSetup: Internal DB configured.");
        }
    }

    private void configureDirectJdbc() {
        navigation.gotoPage("/secure/SetupDatabase!default.jspa");
        JiraSetupInstanceHelper.setupDirectJDBCConnection(tester, getEnvironmentData());
        textAssertions.assertTextPresent(locator.page(), "Set up application properties");
    }

    private void _testDirectJdbcSuccessful() throws IOException, SAXException, JSONException {
        final Map<String, String> query = fillValidDirectJdbcValues();
        assertTestConnectionSuccessful(query);
    }

    private void _testDirectJdbcMissingHostname() throws IOException, SAXException {
        final Map<String, String> query = fillValidDirectJdbcValues();
        // Make username field empty (most likely missing field since it is not auto-populated)
        query.put("jdbcHostname", "");
        textAssertions.assertTextPresent(testConnection(query), "Hostname required");
    }

    private void _testDirectJdbcMissingPortNumber() throws IOException, SAXException {
        final Map<String, String> query = fillValidDirectJdbcValues();
        // Make username field empty (most likely missing field since it is not auto-populated)
        query.put("jdbcPort", "");
        textAssertions.assertTextPresent(testConnection(query), "Port required");
    }

    private void _testDirectJdbcInvalidPort() throws IOException, SAXException, JSONException {
        final Map<String, String> query = fillValidDirectJdbcValues();
        // Make URL invalid
        query.put("jdbcPort", "not-a-number");
        assertTestConnectionFailed(query);
    }

    private void _testDirectJdbcIncorrectPort() throws IOException, SAXException, JSONException {
        final Map<String, String> query = fillValidDirectJdbcValues();
        // Make URL invalid
        query.put("jdbcPort", "999");
        assertTestConnectionFailed(query);
    }

    private void _testDirectJdbcInvalidCredential() throws IOException, SAXException, JSONException {
        final Map<String, String> query = fillValidDirectJdbcValues();
        // Modify password to invalidate credentials
        query.put("jdbcPassword", getEnvironmentData().getProperty("password") + "extra-text-to-invalidate-password");
        assertTestConnectionFailed(query);
    }

    private void _testDirectJdbcMissingUsername() throws IOException, SAXException {
        final Map<String, String> query = fillValidDirectJdbcValues();
        // Make username field empty (most likely missing field since it is not auto-populated)
        query.put("jdbcUsername", "");
        textAssertions.assertTextPresent(testConnection(query), "Username required");
    }

    private Map<String, String> fillValidDirectJdbcValues() throws IOException, SAXException {
        final Map<String, String> query = new HashMap<String, String>();
        query.put("databaseOption", "external");
        query.put("databaseType", getEnvironmentData().getProperty("databaseType"));
        query.put("jdbcHostname", getEnvironmentData().getProperty("db.host"));
        query.put("jdbcPort", getEnvironmentData().getProperty("db.port"));
        // SID is only used for Oracle
        query.put("jdbcSid", getEnvironmentData().getProperty("db.instance"));
        // Database is used for all DBs except Oracle
        query.put("jdbcDatabase", getEnvironmentData().getProperty("db.instance"));

        query.put("jdbcUsername", getEnvironmentData().getProperty("username"));
        query.put("jdbcPassword", getEnvironmentData().getProperty("password"));
        query.put("schemaName", getEnvironmentData().getProperty("schema-name"));
        return query;
    }

    private void assertTestConnectionSuccessful(final Map<String, String> query)
            throws IOException, SAXException, JSONException {
        assertTrue(new JSONObject(testConnection(query)).getJSONObject("data").getBoolean("dbTestSuccessful"));
    }

    private void assertTestConnectionFailed(final Map<String, String> query)
            throws IOException, SAXException, JSONException {
        assertFalse(new JSONObject(testConnection(query)).getJSONObject("data").getBoolean("dbTestSuccessful"));
    }

    private String testConnection(final Map<String, String> query) throws IOException, SAXException {
        final WebResponse response = restClient.GET("/secure/SetupDatabase!connectionCheck.jspa?" + toQueryString(query));
        return response.getText();
    }

    private String toQueryString(final Map<String, String> query) {
        return join(transform(query.entrySet(), entry -> entry.getKey() + "=" + entry.getValue()), '&');
    }
}
