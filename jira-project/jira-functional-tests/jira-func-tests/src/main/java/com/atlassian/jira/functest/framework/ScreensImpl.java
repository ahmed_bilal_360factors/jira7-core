package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.testkit.client.log.FuncTestLoggerImpl;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.meterware.httpunit.WebTable;
import net.sourceforge.jwebunit.WebTester;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.IntStream;

import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_SCREEN_SCHEME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_TABLE_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SCREEN_TABLE_NAME_COLUMN_INDEX;
import static com.atlassian.jira.functest.framework.Navigation.AdminSection.FIELD_SCREENS;
import static com.atlassian.jira.functest.framework.Navigation.AdminSection.FIELD_SCREEN_SCHEME;
import static com.atlassian.jira.functest.framework.Navigation.AdminSection.ISSUE_TYPE_SCREEN_SCHEME;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;
import static org.junit.Assert.fail;

/**
 * @since 7.2
 */
public class ScreensImpl implements Screens {
    private final Navigation navigation;
    private final WebTester tester;
    private final TextAssertions textAssertions;
    private final FuncTestLogger logger;

    @Inject
    public ScreensImpl(JIRAEnvironmentData environmentData, Navigation navigation, WebTester tester, TextAssertions textAssertions) {
        this.navigation = navigation;
        this.tester = tester;
        this.textAssertions = textAssertions;
        this.logger = new FuncTestLoggerImpl(2);
    }

    @Override
    public void addScreen(String screenName, String screenDescription) {
        navigation.gotoPage(FIELD_SCREENS.getUrl());
        tester.clickLink("add-field-screen");
        tester.setWorkingForm("field-screen-add");
        tester.setFormElement("fieldScreenName", screenName);
        tester.setFormElement("fieldScreenDescription", screenDescription);
        tester.submit("Add");
    }

    @Override
    public void deleteScreen(String screenName) {
        navigation.gotoPage(FIELD_SCREENS.getUrl());
        tester.clickLink("delete_fieldscreen_" + screenName);
        tester.submit("Delete");
    }

    @Override
    public void addFieldScreenScheme(String schemeName, String schemeDescription) {
        navigation.gotoPage(FIELD_SCREEN_SCHEME.getUrl());
        tester.clickLink("add-field-screen-scheme");
        tester.setWorkingForm("field-screen-scheme-add");
        tester.setFormElement("fieldScreenSchemeName", schemeName);
        tester.setFormElement("fieldScreenSchemeDescription", schemeDescription);
        tester.submit("Add");
    }

    @Override
    public void copyFieldScreenScheme(String copiedSchemeName, String schemeName, String schemeDescription) {
        navigation.gotoPage(FIELD_SCREEN_SCHEME.getUrl());
        tester.clickLink("copy_fieldscreenscheme_" + copiedSchemeName);
        tester.setFormElement("fieldScreenSchemeName", schemeName);
        tester.setFormElement("fieldScreenSchemeDescription", schemeDescription);
        tester.submit("Copy");
    }

    @Override
    public void deleteFieldScreenScheme(String schemeName) {
        navigation.gotoPage(FIELD_SCREEN_SCHEME.getUrl());
        try {
            tester.assertLinkPresent("configure_fieldscreenscheme_" + schemeName);
        } catch (AssertionError e) {
            logger.log("Scheme does not exist");
        }
        tester.clickLink("delete_fieldscreenscheme_" + schemeName);
        tester.submit("Delete");
    }

    @Override
    public void removeAllRemainingScreenAssociationsFromDefault() {
        navigation.gotoPage(FIELD_SCREEN_SCHEME.getUrl());
        tester.clickLink("configure_fieldscreenscheme_" + DEFAULT_SCREEN_SCHEME);

        if (tester.getDialog().isLinkPresentWithText("Delete")) {
            tester.clickLinkWithText("Delete");
            removeAllRemainingScreenAssociationsFromDefault();
        }
    }

    @Override
    public void removeAllRemainingFieldScreens() {
        navigation.gotoPage(FIELD_SCREENS.getUrl());

        if (tester.getDialog().isLinkPresentWithText("Delete")) {
            tester.clickLinkWithText("Delete");
            tester.submit("Delete");
            removeAllRemainingFieldScreens();
        }
    }

    @Override
    public void addIssueTypeFieldScreenScheme(String schemeName, String schemeDescription, String defaultScreenScheme) {
        navigation.gotoPage(ISSUE_TYPE_SCREEN_SCHEME.getUrl());
        tester.clickLink("add-issue-type-screen-scheme");
        tester.setFormElement("schemeName", schemeName);
        tester.setFormElement("schemeDescription", schemeDescription);
        tester.selectOption("fieldScreenSchemeId", defaultScreenScheme);
        tester.submit("Add");
    }

    @Override
    public void deleteIssueTypeFieldScreenScheme(String schemeId) {
        navigation.gotoPage(ISSUE_TYPE_SCREEN_SCHEME.getUrl());
        tester.clickLink("delete_issuetypescreenscheme_" + schemeId);
        tester.submit("Delete");
    }

    @Override
    public void copyIssueTypeFieldScreenSchemeName(String copiedSchemeId, String schemeName, String schemeDescription) {
        navigation.gotoPage(ISSUE_TYPE_SCREEN_SCHEME.getUrl());
        tester.clickLink("copy_issuetypescreenscheme_" + copiedSchemeId);
        tester.setFormElement("schemeName", schemeName);
        tester.setFormElement("schemeDescription", schemeDescription);
        tester.submit("Copy");
    }

    @Override
    public void addIssueOperationToScreenAssociation(String schemeName, String issueOperation, String screenName) {
        logger.log("Adding screen " + screenName + " to operation '" + issueOperation + "'.");
        gotoFieldScreenScheme(schemeName);

        tester.clickLink("add-screen-scheme-item");
        tester.selectOption("issueOperationId", issueOperation);
        tester.selectOption("fieldScreenId", screenName);
        tester.submit("Add");
    }

    @Override
    public void deleteIssueOperationFromScreenAssociation(String schemeName, String issueOperation) {
        logger.log("Deleting operation '" + issueOperation + "' from scheme " + schemeName + ".");
        gotoFieldScreenScheme(schemeName);
        try {
            tester.clickLink("delete_fieldscreenscheme_" + issueOperation);
        } catch (AssertionError e) {
            logger.log("Issue Operation not configured");
        }
//        assertLinkNotPresent("delete_fieldscreenscheme_" + issueOperation);
    }

    @Override
    public void deleteTabFromScreen(String screenName, String tabName) {
        gotoFieldScreenTab(screenName, tabName);
        tester.clickLinkWithText("Delete");
        tester.submit("Delete");
    }

    private String findRowWithName(final String fieldTableName, final int column, final String fieldName) {
        try {
            final WebTable fieldTable = tester.getDialog().getResponse().getTableWithID(fieldTableName);
            return fieldTable == null ? null : IntStream.range(1, fieldTable.getRowCount()).boxed()
                    .collect(toMap(identity(), i -> fieldTable.getCellAsText(i, column), (x, y) -> x, TreeMap::new))
                    .entrySet().stream()
                    .filter(entry -> entry.getValue().contains(fieldName))
                    .map(Map.Entry::getKey)
                    .map(i -> i - 1)
                    .map(Object::toString)
                    .findFirst().orElse(null);
        } catch (final SAXException ignored) {
            fail("Cannot find table with id '" + FIELD_TABLE_ID + "'.");
            return null;
        }
    }

    @Override
    public void removeFieldFromFieldScreenTab(String screenName, String tabName, String[] fieldNames) {
        gotoFieldScreenTab(screenName, tabName);
        tester.assertTextPresent("Configure Screen");
        for (String fieldName : fieldNames) {
            String indexName = findRowWithName(FIELD_TABLE_ID, SCREEN_TABLE_NAME_COLUMN_INDEX, fieldName);
            if (indexName != null) {
                int index = Integer.parseInt(indexName);
                tester.checkCheckbox("removeField_" + index);
                tester.assertCheckboxSelected("removeField_" + index);
            } else {
                logger.log("Field " + fieldName + " not present");
            }
        }

        tester.submit("deleteFieldsFromTab");
    }

    private void gotoFieldScreenScheme(String schemeName) {
        navigation.gotoPage(FIELD_SCREEN_SCHEME.getUrl());
        tester.clickLink("configure_fieldscreenscheme_" + schemeName);
    }

    private void gotoFieldScreenTab(String screenName, String tabName) {
        gotoFieldScreen(screenName);
        try {
            tester.assertTextPresent("Add one or more fields to the");
            textAssertions.assertTextSequence("<b>" + tabName + "</b>", "tab.");
        } catch (AssertionError e) {
            tester.clickLinkWithText(tabName);
        }
    }

    private void gotoFieldScreen(String screenName) {
        navigation.gotoPage(FIELD_SCREENS.getUrl());
        tester.assertTextPresent("View Screens");
        tester.assertLinkPresent("configure_fieldscreen_" + screenName);
        tester.clickLink("configure_fieldscreen_" + screenName);
    }
}
