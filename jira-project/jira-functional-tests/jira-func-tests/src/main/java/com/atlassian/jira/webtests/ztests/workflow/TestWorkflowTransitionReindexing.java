package com.atlassian.jira.webtests.ztests.workflow;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.SearchRestClient;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.SearchResult;
import com.atlassian.jira.rest.client.api.domain.Transition;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;

import java.net.URI;
import java.util.stream.StreamSupport;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.WORKFLOW})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestWorkflowTransitionReindexing extends BaseJiraFuncTest {

    @Test
    public void testReIndexingFieldsSetDuringTransition() throws Exception {
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_TIMETRACKING, true);
        backdoor.screens().addFieldToScreen("Resolve Issue Screen", "Log Work");
        backdoor.screens().addFieldToScreen("Resolve Issue Screen", "Summary");
        backdoor.screens().addFieldToScreen("Resolve Issue Screen", "Comment");

        final JiraRestClient restClient = createRestClient();
        IssueRestClient issueClient = restClient.getIssueClient();

        IssueInput issueInput = new IssueInputBuilder("HSP", 1L)
                .setSummary("new issue")
                .setDescription("issue description")
                .build();

        Issue issue = issueClient.getIssue(issueClient.createIssue(issueInput).claim().getKey()).claim();
        Iterable<Transition> transitions = issueClient.getTransitions(issue).claim();
        Transition resolveIssue = StreamSupport.stream(transitions.spliterator(), false)
                .filter(transition -> transition.getName().contains("Resolve"))
                .findFirst()
                .get();

        navigation.issue().gotoIssue(issue.getKey());
        tester.clickLink("action_id_" + resolveIssue.getId());
        tester.setWorkingForm("issue-workflow-transition");

        String changedSummaryValue = "changed summary";
        tester.setFormElement("comment", "some random comment");
        tester.setFormElement("worklog_timeLogged", "2h");
        tester.setFormElement("summary", changedSummaryValue);

        tester.submit();

        SearchRestClient searchClient = restClient.getSearchClient();

        SearchResult searchResult = searchClient.searchJql("timespent > 1h").claim();
        assertThat(searchResult.getIssues(), contains(issue(issue.getKey())));

        searchResult = searchClient.searchJql("summary ~ \"" + changedSummaryValue + "\"").claim();
        assertThat(searchResult.getIssues(), contains(issue(issue.getKey())));

        searchResult = searchClient.searchJql("summary ~ issue").claim();
        assertThat(searchResult.getIssues(), Matchers.<Issue>emptyIterable());

        searchResult = searchClient.searchJql("comment ~ random").claim();
        assertThat(searchResult.getIssues(), contains(issue(issue.getKey())));
    }

    private JiraRestClient createRestClient() {
        final AsynchronousJiraRestClientFactory restClientFactory = new AsynchronousJiraRestClientFactory();
        try {
            final URI baseUri = environmentData.getBaseUrl().toURI();
            return restClientFactory.createWithBasicHttpAuthentication(baseUri, ADMIN_USERNAME, ADMIN_PASSWORD);
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Matcher<Issue> issue(final String issueKey) {
        return new TypeSafeMatcher<Issue>() {
            @Override
            protected boolean matchesSafely(final Issue issue) {
                return issue.getKey().equals(issueKey);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("Issue").appendValue(issueKey).appendText("not on the list");
            }
        };
    }
}
