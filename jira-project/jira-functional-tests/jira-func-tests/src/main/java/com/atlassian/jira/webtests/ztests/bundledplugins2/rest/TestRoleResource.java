package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.project.ProjectRoleBean;
import com.atlassian.jira.testkit.beans.PermissionGrantBean;
import com.atlassian.jira.testkit.client.restclient.Comment;
import com.atlassian.jira.testkit.client.restclient.CommentClient;
import com.atlassian.jira.testkit.client.restclient.GenericRestClient;
import com.atlassian.jira.testkit.client.restclient.PermissionSchemeRestClient;
import com.atlassian.jira.testkit.client.restclient.ProjectRole;
import com.atlassian.jira.testkit.client.restclient.ProjectRoleActorsBean;
import com.atlassian.jira.testkit.client.restclient.ProjectRoleClient;
import com.atlassian.jira.testkit.client.restclient.RoleClient;
import com.atlassian.jira.util.json.JSONException;
import com.sun.jersey.api.client.UniformInterfaceException;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.hamcrest.BaseMatcher;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @since 6.4
 */
@WebTest({Category.FUNC_TEST, Category.REST})
public class TestRoleResource extends BaseJiraFuncTest {
    private RoleClient roleClient;
    private PermissionSchemeRestClient permissionSchemeRestClient;
    private CommentClient commentClient;

    @Before
    public void setUpTest() {
        backdoor.restoreBlankInstance();
        roleClient = new RoleClient(getEnvironmentData());
        permissionSchemeRestClient = new PermissionSchemeRestClient(getEnvironmentData());
        commentClient = new CommentClient(getEnvironmentData());
    }

    @Test
    public void testGetAllRoles() {
        List<ProjectRole> projectRoles = roleClient.get();

        Matcher[] expectedProjectRoles =
                {
                        projectRole(10000L, "Users", "A project role that represents users in a project", "jira-users"),
                        projectRole(10001L, "Developers", "A project role that represents developers in a project", "jira-developers"),
                        projectRole(10002L, "Administrators", "A project role that represents administrators in a project", "jira-administrators")
                };

        Matcher<Iterable<? super ProjectRole>> values = CoreMatchers.hasItems(expectedProjectRoles);
        assertThat(projectRoles, values);
    }

    @Test
    public void testAnonymousGet() {
        assertFailingStatusCode(
                () -> {
                    roleClient.anonymous().get();
                }, Response.Status.UNAUTHORIZED);
    }

    @Test
    public void testNormalUserGet() {
        assertFailingStatusCode(
                () -> {
                    backdoor.usersAndGroups().addUser("user");
                    roleClient.loginAs("user").get();
                }, Response.Status.FORBIDDEN,
                () -> {
                    backdoor.usersAndGroups().deleteUser("user");
                });
    }

    @Test
    public void testGetSelfForEachRole() throws IOException, SAXException, JSONException {
        List<ProjectRole> projectRoles = roleClient.get();

        for (ProjectRole projectRole : projectRoles) {
            //We validate the webresource
            ProjectRole projectRoleFromGet = roleClient.get(projectRole.id.toString());
            assertNotNull(projectRoleFromGet);
            assertEquals(projectRole.description, projectRoleFromGet.description);
            assertEquals(projectRole.name, projectRoleFromGet.name);
            assertEquals(projectRole.self, projectRoleFromGet.self);
            assertEquals(projectRole.id, projectRoleFromGet.id);

            //We validate the self to see that it returns the same info
            com.atlassian.jira.testkit.client.restclient.Response<ProjectRoleBean> projectRoleBeanResponse = new GenericRestClient().get(URI.create(projectRole.self), ProjectRoleBean.class);
            assertThat(projectRoleBeanResponse.statusCode, equalTo(200));
            ProjectRoleBean bean = projectRoleBeanResponse.body;

            assertEquals(projectRole.description, bean.description);
            assertEquals(projectRole.name, bean.name);
            assertEquals(projectRole.self, bean.self.toString());
            assertEquals(projectRole.id, bean.id);
        }
    }

    @Test
    public void testGetNonExistingRole() throws IOException, SAXException {
        assertFailingStatusCode(() -> roleClient.get("12312321312"), Response.Status.NOT_FOUND);
    }

    @Test
    public void testCreateRoleWorks() {
        ProjectRole expected = new ProjectRole().name("name").description("description");

        ProjectRole createResult = roleClient.create(expected.getName(), expected.getDescription());
        assertThat(createResult, matchesByNameAndDescription(expected));

        ProjectRole getResult = roleClient.get(String.valueOf(createResult.id));
        assertThat(getResult, matchesByNameAndDescription(expected));

        assertTrue(getResult.getActors() == null);
    }

    @Test
    public void testCreatingWithExistingNameYieldsConflict() {
        ProjectRole expected = new ProjectRole().name("name").description("description");

        roleClient.create(expected.getName(), expected.getDescription());

        assertFailingStatusCode(
                () -> {
                    roleClient.create(expected.getName(), expected.getDescription());
                }, Response.Status.CONFLICT);
    }

    @Test
    public void testNoAccessForAnonymousForCreateRole() {
        assertFailingStatusCode(
                () -> {
                    roleClient.anonymous().create("a", "b");
                }, Response.Status.UNAUTHORIZED);
    }

    @Test
    public void testNoAccessForNormalUserForCreateRole() {
        assertFailingStatusCode(
                () -> {
                    backdoor.usersAndGroups().addUser("user");
                    roleClient.loginAs("user").create("a", "b");
                }, Response.Status.FORBIDDEN,
                () -> {
                    backdoor.usersAndGroups().deleteUser("user");
                });
    }

    @Test
    public void testBadRequestForCreateRoleWithoutName() {
        assertFailingStatusCode(() -> roleClient.create(null, "b"), Response.Status.BAD_REQUEST);
    }

    @Test
    public void testRenameRoleWorks() {
        ProjectRole newRole = roleClient.create("role", "");
        String expectedName = "newName";

        ProjectRole newName = roleClient.updatePartial(newRole.id, expectedName, null);

        assertEquals(expectedName, newName.getName());
    }

    @Test
    public void testPartialUpdateRoleNeedsNameOrDescription() {
        ProjectRole newRole = roleClient.create("role", "");

        assertFailingStatusCode(() -> roleClient.updatePartial(newRole.id, null, null), Response.Status.BAD_REQUEST);
    }

    @Test
    public void testFullUpdateRoleNeedsNameAndDescription() {
        ProjectRole newRole = roleClient.create("role", "");

        assertFailingStatusCode(() -> roleClient.updateFull(newRole.id, "a", null), Response.Status.BAD_REQUEST);
        assertFailingStatusCode(() -> roleClient.updateFull(newRole.id, null, "a"), Response.Status.BAD_REQUEST);
    }

    @Test
    public void testDeleteRoleWorks() {
        ProjectRole newRole = roleClient.create("to-be-deleted", "");

        roleClient.deleteProjectRole(newRole.getId());

        assertFailingStatusCode(() -> roleClient.get(String.valueOf(newRole.getId())), Response.Status.NOT_FOUND);
    }

    @Test
    public void testDeleteWorksWithLinkedSchemesAndSwapQueryParam() {
        ProjectRole newRole = roleClient.create("to-be-deleted", "");
        ProjectRole replacementRole = roleClient.create("replacement", "");
        Long schemeId = backdoor.permissionSchemes().createScheme("new-scheme", "");
        backdoor.permissionSchemes().addProjectRolePermission(schemeId, ProjectPermissions.CLOSE_ISSUES, newRole.getId());

        roleClient.deleteProjectRole(newRole.getId(), replacementRole.getId());

        List<PermissionGrantBean> permissions = permissionSchemeRestClient.getPermissions(schemeId).body.permissions;
        assertThat(permissions, contains(projectRolePermissionGrantBean(replacementRole.getId())));
    }

    @Test
    public void testDeleteFailsWithLinkedSchemesWithoutSwapQueryParam() {
        ProjectRole role = roleClient.create(RandomStringUtils.randomAlphabetic(6), "");

        Long schemeId = backdoor.permissionSchemes().createScheme("new-scheme", "");
        backdoor.permissionSchemes().addProjectRolePermission(schemeId, ProjectPermissions.CLOSE_ISSUES, role.getId());

        assertFailingStatusCode(() -> roleClient.deleteProjectRole(role.getId()), Response.Status.CONFLICT);
    }

    @Test
    public void testDeleteFailsWithProtectedCommentsWithoutSwapQueryParam() {
        ProjectRole role = roleClient.create(RandomStringUtils.randomAlphabetic(6), "");

        IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "summary");

        ProjectRoleClient projectRoleClient = new ProjectRoleClient(getEnvironmentData());
        projectRoleClient.addActors(PROJECT_HOMOSAP_KEY, role.getName(), new String[0], new String[]{"admin"});

        backdoor.issues().commentIssueWithVisibility(issue.key, "restricted-comment", "role", role.getName());

        assertFailingStatusCode(() -> roleClient.deleteProjectRole(role.getId()), Response.Status.CONFLICT);
    }

    @Test
    public void testDeleteWorksWithProtectedCommentsWithSwapQueryParam() {
        ProjectRole role = roleClient.create(RandomStringUtils.randomAlphabetic(6), "");
        ProjectRole roleReplacement = roleClient.create(RandomStringUtils.randomAlphabetic(6), "");

        IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "summary");

        ProjectRoleClient projectRoleClient = new ProjectRoleClient(getEnvironmentData());
        projectRoleClient.addActors(PROJECT_HOMOSAP_KEY, role.getName(), new String[0], new String[]{"admin"});
        projectRoleClient.addActors(PROJECT_HOMOSAP_KEY, roleReplacement.getName(), new String[0], new String[]{"fred"});

        backdoor.issues().commentIssueWithVisibility(issue.key, "restricted-comment", "role", role.getName());

        assertCannotSeeCommentsOnIssue("fred", issue.key);
        assertCanSeeCommentsOnIssue("admin", issue.key);

        roleClient.deleteProjectRole(role.getId(), roleReplacement.getId());

        assertCannotSeeCommentsOnIssue("admin", issue.key);
        assertCanSeeCommentsOnIssue("fred", issue.key);
    }

    @Test
    public void testDeleteWorksAndDoesNotModifyGroupRestrictions() {
        ProjectRole role = roleClient.create(RandomStringUtils.randomAlphabetic(6), "");
        ProjectRole roleReplacement = roleClient.create(RandomStringUtils.randomAlphabetic(6), "");
        String groupRestriction = "jira-administrators";

        IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "summary");

        ProjectRoleClient projectRoleClient = new ProjectRoleClient(getEnvironmentData());
        projectRoleClient.addActors(PROJECT_HOMOSAP_KEY, role.getName(), new String[0], new String[]{"admin"});

        backdoor.issues().commentIssueWithVisibility(issue.key, "restricted-comment", "role", role.getName());
        backdoor.issues().commentIssueWithVisibility(issue.key, "restricted-comment-to-group", "group", groupRestriction);

        roleClient.deleteProjectRole(role.getId(), roleReplacement.getId());

        projectRoleClient.addActors(PROJECT_HOMOSAP_KEY, roleReplacement.getName(), new String[0], new String[]{"admin"});
        assertCommentContainsGroupRestriction(issue.key, groupRestriction);
    }

    private void assertCommentContainsGroupRestriction(final String key, final String groupRestriction) {
        List<Comment> comments = commentClient.getComments(key).body.getComments();
        assertEquals("group", comments.get(1).visibility.type);
        assertEquals(groupRestriction, comments.get(1).visibility.value);
    }

    @Test
    public void testDeleteFailsWhenRoleUsedInWorkflowWithoutSwapQueryParam() {
        Long roleId = 10610l;
        backdoor.restoreDataFromResource("xml/TestRoleResourceSwapRoleInWorkflows.xml");

        assertFailingStatusCode(() -> roleClient.deleteProjectRole(roleId), Response.Status.CONFLICT);
    }

    @Test
    public void testDeleteWorksWhenRoleUsedInWorkflowWithSwapQueryParam() {
        Long roleId = 10610l;
        backdoor.restoreDataFromResource("xml/TestRoleResourceSwapRoleInWorkflows.xml");

        ProjectRole roleReplacement = roleClient.create(RandomStringUtils.randomAlphabetic(6), "");
        roleClient.deleteProjectRole(roleId, roleReplacement.getId());

        verifyReplacementWorked(roleReplacement);
    }

    @Test
    public void getDefaultActorsOfNewRoleReturnsEmpty() {
        ProjectRole projectRole = roleClient.create(RandomStringUtils.randomAlphabetic(6), "");

        ProjectRoleActorsBean defaultActorsForRole = roleClient.getDefaultActorsForRole(projectRole.getId());

        assertThat(defaultActorsForRole.getActors(), empty());
    }

    @Test
    public void getDefaultActorsOfNonExistingRoleReturnsNotFound() {
        assertFailingStatusCode(() -> roleClient.getDefaultActorsForRole(999l), Response.Status.NOT_FOUND);
    }

    @Test
    public void addDefaultActorsToNewRoleWorks() {
        ProjectRole projectRole = roleClient.create(RandomStringUtils.randomAlphabetic(6), "");

        ProjectRoleActorsBean result = roleClient.addDefaultActorsToRole(projectRole.getId(), new String[]{"admin"}, null);

        assertThat(result.getActors(), Matchers.contains(actor("admin")));
    }

    @Test
    public void addDefaultActorsRequiresUserOrGroup() {
        ProjectRole projectRole = roleClient.create(RandomStringUtils.randomAlphabetic(6), "");

        assertFailingStatusCode(() -> roleClient.addDefaultActorsToRole(projectRole.getId(), null, null), Response.Status.BAD_REQUEST);
    }

    @Test
    public void addNonExistentActorFails() {
        ProjectRole projectRole = roleClient.create(RandomStringUtils.randomAlphabetic(6), "");

        assertFailingStatusCode(() -> roleClient.addDefaultActorsToRole(projectRole.getId(), new String[]{"nonexistent"}, null), Response.Status.BAD_REQUEST);
    }

    @Test
    public void addNonExistentActorFailsAndLeavesDefaultActorsAsIs() {
        ProjectRole projectRole = roleClient.create(RandomStringUtils.randomAlphabetic(6), "");

        assertFailingStatusCode(() -> roleClient.addDefaultActorsToRole(projectRole.getId(), new String[]{"nonexistent", "admin"}, new String[]{"jira-users"}), Response.Status.BAD_REQUEST);

        ProjectRoleActorsBean defaultActorsForRole = roleClient.getDefaultActorsForRole(projectRole.getId());

        assertThat(defaultActorsForRole.getActors(), empty());
    }

    @Test
    public void deleteDefaultActorsFromNewRoleWorks() {
        ProjectRole projectRole = roleClient.create(RandomStringUtils.randomAlphabetic(6), "");

        ProjectRoleActorsBean result = roleClient.addDefaultActorsToRole(projectRole.getId(), new String[]{"admin"}, null);
        assertThat(result.getActors(), Matchers.contains(actor("admin")));

        ProjectRoleActorsBean updatedBean = roleClient.deleteDefaultActorsToRole(projectRole.getId(), "admin", null);

        assertThat(updatedBean.getActors(), empty());
    }

    @Test
    public void deleteDefaultActorsRequiresUserOrGroup() {
        ProjectRole projectRole = roleClient.create(RandomStringUtils.randomAlphabetic(6), "");

        assertFailingStatusCode(() -> roleClient.deleteDefaultActorsToRole(projectRole.getId(), null, null), Response.Status.BAD_REQUEST);
    }

    @Test
    public void deleteDefaultActorsFailsWhenGivenBothUserAndGroup() {
        ProjectRole projectRole = roleClient.create(RandomStringUtils.randomAlphabetic(6), "");

        assertFailingStatusCode(() -> roleClient.deleteDefaultActorsToRole(projectRole.getId(), "admin", "jira-users"), Response.Status.BAD_REQUEST);
    }

    private Matcher<? super ProjectRole.Actor> actor(final String name) {
        return new TypeSafeMatcher<ProjectRole.Actor>() {
            @Override
            protected boolean matchesSafely(final ProjectRole.Actor item) {
                return item.name.equals(name);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("actor with name").appendValue(name);
            }
        };
    }

    private int getNumberOfVisibleCommentsForUser(String username, String issueKey) {
        return commentClient.loginAs(username).getComments(issueKey).body.getTotal();
    }

    private void assertCanSeeCommentsOnIssue(String username, String issueKey) {
        assertTrue(getNumberOfVisibleCommentsForUser(username, issueKey) != 0);
    }

    private void assertCannotSeeCommentsOnIssue(String username, String issueKey) {
        assertTrue(getNumberOfVisibleCommentsForUser(username, issueKey) == 0);
    }

    private void verifyReplacementWorked(final ProjectRole roleReplacement) {
        assertFailingStatusCode(() -> roleClient.deleteProjectRole(roleReplacement.getId()), Response.Status.CONFLICT);
    }

    private Matcher<PermissionGrantBean> projectRolePermissionGrantBean(final Long roleId) {
        return new TypeSafeMatcher<PermissionGrantBean>() {
            @Override
            protected boolean matchesSafely(final PermissionGrantBean item) {
                return item.getHolder().getType().equals("projectRole") && item.getHolder().getParameter().equals(roleId.toString());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("permission grant bean with role type and id equal to").appendValue(roleId);
            }
        };
    }

    private void assertFailingStatusCode(Runnable function, Response.Status statusCode) {
        assertFailingStatusCode(function, statusCode, () -> {
        });
    }

    private void assertFailingStatusCode(Runnable function, Response.Status statusCode, Runnable finallyFunction) {
        try {
            function.run();
            fail("Should result in " + statusCode.toString());
        } catch (UniformInterfaceException e) {
            assertEquals(statusCode.getStatusCode(), e.getResponse().getStatus());
        } finally {
            finallyFunction.run();
        }
    }

    private Matcher<ProjectRole> matchesByNameAndDescription(ProjectRole expected) {
        return new TypeSafeMatcher<ProjectRole>() {
            @Override
            public void describeTo(final Description description) {
                description.appendText("name: ").appendValue(expected.getName())
                        .appendText("description: ").appendValue(expected.getDescription());
            }

            @Override
            protected boolean matchesSafely(final ProjectRole projectRole) {
                return new EqualsBuilder()
                        .append(projectRole.getName(), expected.getName())
                        .append(projectRole.getDescription(), expected.getDescription())
                        .isEquals();
            }
        };
    }

    private BaseMatcher<ProjectRole> projectRole(
            @Nonnull final Long id, @Nonnull final String name, @Nonnull final String description, @Nonnull final String actorName) {
        return new BaseMatcher<ProjectRole>() {
            @Override
            public boolean matches(final Object o) {
                if (!(o instanceof ProjectRole)) {
                    return false;
                }

                final ProjectRole changedValue = (ProjectRole) o;
                boolean result = name.equals(changedValue.name)
                        && id.equals(changedValue.id)
                        && description.equals(changedValue.description)
                        && containsActor(changedValue.actors, actorName);
                return result;
            }

            @Override
            public void describeTo(final Description descr) {
                descr.appendText(new ProjectRole().name(name).id(id).description(description).toString());
            }

            private boolean containsActor(final List<ProjectRole.Actor> actors, final String actor) {
                for (ProjectRole.Actor roleActor : actors) {
                    if (actor.equalsIgnoreCase(roleActor.name)) {
                        return true;
                    }
                }
                return false;
            }
        };
    }
}
