package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.admin.IssueSecurityLevel;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.webtests.LicenseKeys;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.rest.api.issue.ResourceRef.withId;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.SOFTWARE_KEY;
import static com.atlassian.jira.webtests.Groups.DEVELOPERS;
import static com.atlassian.jira.webtests.Groups.USERS;
import static com.google.common.collect.ImmutableList.of;
import static org.junit.Assert.assertEquals;

@WebTest({Category.FUNC_TEST, Category.PERMISSIONS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestApplicationRoleBasedPermissions extends BaseJiraFuncTest {
    private static final String INACCESSIBLE_ISSUE_ID = "";
    private static final String APPLICATION_ROLE_ANY = "";
    private static final String SOFTWARE_ONLY_USER = "dev";
    private static final String CORE_ONLY_USER = "fred";

    private String issueKey;
    private String issueId;

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        backdoor.restoreBlankInstance(LicenseKeys.MULTI_ROLE);

        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, USERS);

        IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "A test issue!");
        issueKey = issue.key;
        issueId = issue.id;
    }

    @Test
    public void testAnyProjectRoleGrantsAccessToAnyLoggedInUser() {
        removeAllBrowsePermissions();
        assertIssueAccessDenied(issueKey);

        grantBrowseAccessToApplicationRole(APPLICATION_ROLE_ANY);
        assertIssueCanBeAccessed(issueKey);
    }

    @Test
    public void testSpecificProjectRoleGrantsAccessToRoleMembers() {
        setupSoftwareOnlyUser();
        removeAllBrowsePermissions();

        navigation.login(SOFTWARE_ONLY_USER);
        assertIssueAccessDenied(issueKey);

        navigation.login(ADMIN_USERNAME);
        grantBrowseAccessToApplicationRole(SOFTWARE_KEY);

        navigation.login(SOFTWARE_ONLY_USER);
        assertIssueCanBeAccessed(issueKey);

        navigation.login(CORE_ONLY_USER);
        assertIssueAccessDenied(issueKey);
    }

    @Test
    public void testIssueSecurityLevelWithAnyProjectRoleGrantsAccessToAnyLoggedInUser() {
        grantBrowseAccessToAnonymous();

        navigation.logout();
        assertIssueCanBeAccessed(issueKey);

        createAndSetIssueLevelSecurityWithAppliationRole(APPLICATION_ROLE_ANY);
        assertIssueCanBeAccessed(issueKey);

        navigation.logout();
        assertIssueAccessDenied(issueKey);
    }

    @Test
    public void testIssueSecurityLevelWithSpecificProjectRoleGrantsAccessToRoleMembers() {
        setupSoftwareOnlyUser();

        navigation.login(CORE_ONLY_USER);
        assertIssueCanBeAccessed(issueKey);

        navigation.login(ADMIN_USERNAME);
        grantBrowseAccessToApplicationRole(APPLICATION_ROLE_ANY);
        createAndSetIssueLevelSecurityWithAppliationRole(SOFTWARE_KEY);

        navigation.login(CORE_ONLY_USER);
        assertIssueAccessDenied(issueKey);

        navigation.login(SOFTWARE_ONLY_USER);
        assertIssueCanBeAccessed(issueKey);
    }

    private void createAndSetIssueLevelSecurityWithAppliationRole(final String applicationRoleValue) {
        navigation.login(ADMIN_USERNAME);
        //creates an issue security level with id 10000 in a new scheme.
        administration.issueSecuritySchemes().
                newScheme("Application Role Scheme", "").
                newLevel("Application Role Level", "").
                addIssueSecurity(IssueSecurityLevel.IssueSecurity.APPLICATION_ROLE, applicationRoleValue);
        administration.project().associateIssueLevelSecurityScheme(PROJECT_HOMOSAP, "Application Role Scheme");

        final String ISSUE_SECURITY_LEVEL_ID_JUST_CREATED = "10000";
        IssueFields issueFields = new IssueFields();
        issueFields.securityLevel(withId(ISSUE_SECURITY_LEVEL_ID_JUST_CREATED));
        backdoor.issues().setIssueFields(issueKey, issueFields);
    }

    private void setupSoftwareOnlyUser() {
        backdoor.applicationRoles().putRoleWithDefaultsSelectedByDefault(SOFTWARE_KEY, true, of(DEVELOPERS), of(DEVELOPERS));
        backdoor.usersAndGroups().addUser(SOFTWARE_ONLY_USER);
        backdoor.usersAndGroups().addUserToGroup(SOFTWARE_ONLY_USER, DEVELOPERS);
    }

    private void grantBrowseAccessToAnonymous() {
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.BROWSE_PROJECTS);
    }

    private void grantBrowseAccessToApplicationRole(final String roleFormValue) {
        backdoor.permissionSchemes().addApplicationRolePermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.BROWSE_PROJECTS, roleFormValue);
    }

    private void removeAllBrowsePermissions() {
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.BROWSE_PROJECTS, USERS);
    }

    private void assertIssueAccessDenied(String issueKey) {
        final String actualIssueId = navigation.issue().viewIssue(issueKey).getIssueId();
        assertEquals(INACCESSIBLE_ISSUE_ID, actualIssueId);
    }

    private void assertIssueCanBeAccessed(String issueKey) {
        final String actualIssueId = navigation.issue().viewIssue(issueKey).getIssueId();
        assertEquals(issueId, actualIssueId);
    }
}
