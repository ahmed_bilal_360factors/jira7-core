package com.atlassian.jira.webtests.ztests.timetracking.modern;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import com.atlassian.jira.testkit.client.restclient.Worklog;
import com.atlassian.jira.testkit.client.restclient.WorklogClient;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;

import java.text.SimpleDateFormat;

abstract class AbstractWorklogSearchingTest extends BaseJiraRestTest {
    protected static final String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    protected static final long DEFAULT_PERMISSION_SCHEME_ID = 0;

    protected final String defaultStartDate = "2014-08-06";
    protected WorklogClient worklogClient;
    protected IssueClient issueClient;
    protected final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");

    @Before
    public void setUpTest() {
        backdoor.restoreBlankInstance();
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_TIMETRACKING, true);
        worklogClient = new WorklogClient(environmentData);
        issueClient = new IssueClient(environmentData);
    }

    protected Worklog createWorklog(String issueKey, String date) {
        return createWorklog(issueKey, date, "admin");
    }

    protected Worklog createWorklog(String issueKey, String date, String userName) {
        return createWorklog(issueKey, dateTimeFormatter.parseDateTime(date), userName);
    }

    protected Worklog createWorklog(String issueKey, String date, String userName, String comment) {
        return createWorklog(issueKey, dateTimeFormatter.parseDateTime(date), userName, comment);
    }

    protected Worklog createWorklogAsUser(String issueKey, String userName) {
        return createWorklog(issueKey, DateTime.now(), userName);
    }

    protected Worklog createWorklogAsUserWithComment(String issueKey, String userName, String comment) {
        return createWorklog(issueKey, DateTime.now(), userName, comment);
    }

    private Worklog createWorklog(String issueKey, DateTime date, String userName) {
        return createWorklog(issueKey, date, userName, "");
    }

    protected Worklog createWorklog(String issueKey, DateTime date, String userName, String comment) {
        final Worklog worklog = new Worklog();
        worklog.started = new SimpleDateFormat(TIME_FORMAT).format(date.toDate());
        worklog.timeSpent = "1h";
        worklog.comment = comment;
        return worklogClient.loginAs(userName).post(issueKey, worklog).body;
    }

    protected void deleteWorklog(String issueKey, final Worklog worklog) {
        worklogClient.loginAs("admin").delete(issueKey, worklog);
    }

    protected void updateWorklog(String issueKey, final Worklog worklog) {
        worklogClient.loginAs("admin").put(issueKey, worklog);
    }


    protected String getDateFromString(final String date) {
        final DateTime dateTime = dateTimeFormatter.parseDateTime(date);
        return new SimpleDateFormat(TIME_FORMAT).format(dateTime.toDate());
    }

    protected String createDefaultIssue() {
        return backdoor.issues().createIssue("HSP", "test issue!", "admin").key;
    }

    protected SearchResult executeEqualWorklogDateSearch(String date) {
        return backdoor.search().loginAs("fred", "fred").getSearch(new SearchRequest().jql("worklogDate=" + date));
    }

    protected SearchResult executeEqualWorklogDateSearchAsUser(String date, String user) {
        return backdoor.search().loginAs(user, user).getSearch(new SearchRequest().jql("worklogDate=" + date));
    }

    protected SearchResult executeJqlSearch(String query) {
        return executeJqlSearchAsUser(query, "fred");
    }

    protected SearchResult executeJqlSearchAsUser(String query, String user) {
        return backdoor.search().loginAs(user).getSearch(new SearchRequest().jql(query));
    }

    protected int getResponseCode(final String jql) throws Exception {
        final Response response = backdoor.search().loginAs("fred").getSearchResponse(new SearchRequest().jql(jql));
        return response.statusCode;
    }
}
