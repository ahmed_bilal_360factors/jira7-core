package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import javax.annotation.Nullable;
import java.util.Date;
import java.util.List;

/**
 * @since v7.0.0
 */
public class SchedulerControl extends BackdoorControl<LicenseControl> {
    public static final GenericType<List<String>> LIST_OF_STRING = new GenericType<List<String>>() {
    };
    public static final GenericType<List<JobBean>> LIST_OF_JOB_BEAN = new GenericType<List<JobBean>>() {
    };

    SchedulerControl(final JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public List<String> getJobRunnerKeys() {
        return scheduler().path("jobRunnerKeys").get(LIST_OF_STRING);
    }

    @Nullable
    public JobBean getJob(JobId jobId) {
        return scheduler().path("job").path(jobId.toString()).get(JobBean.class);
    }

    public List<JobBean> getJobsByJobRunnerKey(JobRunnerKey jobRunnerKey) {
        return scheduler().path("jobs").path(jobRunnerKey.toString()).get(LIST_OF_JOB_BEAN);
    }

    public List<JobBean> getAllJobs() {
        return scheduler().path("jobs").get(LIST_OF_JOB_BEAN);
    }

    @Nullable
    public JobRunBean getLastRun(JobId jobId) {
        try {
            return scheduler().path("job").path(jobId.toString()).path("last").get(JobRunBean.class);
        } catch (UniformInterfaceException e) {
            return handleRestException(e);
        }
    }

    @Nullable
    public JobRunBean getLastSuccessfulRun(JobId jobId) {
        try {
        return scheduler().path("job").path(jobId.toString()).path("lastSuccessful").get(JobRunBean.class);
        } catch (UniformInterfaceException e) {
            return handleRestException(e);
        }
    }

    private static JobRunBean handleRestException(final UniformInterfaceException e) {
        if (e.getResponse().getStatusInfo() == ClientResponse.Status.NO_CONTENT) {
            return null;
        }
        throw e;
    }

    private WebResource scheduler() {
        return createResource().path("scheduler");
    }

    public static class JobRunBean {
        public String message;
        public Date startTime;
        public long durationInMillis;
        public String runOutcome;

        @Override
        public String toString() {
            return "JobRunBean{" +
                    "message='" + message + '\'' +
                    ", startTime=" + startTime +
                    ", durationInMillis=" + durationInMillis +
                    ", runOutcome='" + runOutcome + '\'' +
                    '}';
        }
    }

    public static class JobBean {
        public String jobId;
        public String jobRunnerKey;
        public Long nextRunTime;
        public String runMode;
        public String scheduleType;
        public Long intervalInMillis;
        public Long firstRunTime;
        public String cronExpression;
        public String timeZoneId;
        public boolean runnable;
    }

}
