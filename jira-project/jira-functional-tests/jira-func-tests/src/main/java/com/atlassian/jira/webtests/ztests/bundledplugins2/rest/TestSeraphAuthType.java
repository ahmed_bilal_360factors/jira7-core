package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.WebTesterFactory;
import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.WebResponse;
import net.sourceforge.jwebunit.WebTester;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * This *should* live in com.atlassian.jira.webtests.ztests.misc.TestSeraphAuthType but those tests don't run
 * with bundled plugins enabled and this depends on the atlassian-rest plugin existing.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
public class TestSeraphAuthType extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
        navigation.logout();

        tester.getDialog().getWebClient().setExceptionsThrownOnErrorStatus(false);
        HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
    }

    @Test
    public void testDefault_REST() throws Exception {
        WebTester webTester = WebTesterFactory.createNewWebTester(environmentData);
        webTester.getTestContext().addCookie("JSESSIONID", "bad-cookie");

        // Under /rest/ it should be treated as ANY even if you don't specify anything.
        tester.beginAt("/rest/api/latest/user?username=admin");
        final WebResponse response = tester.getDialog().getResponse();
        Assert.assertEquals(401, response.getResponseCode());
    }

}
