package com.atlassian.jira.webtests.ztests.dashboard.reports.security.xss;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;
import org.w3c.dom.html.HTMLOptionElement;
import org.w3c.dom.html.HTMLSelectElement;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertTrue;

/**
 * Test case for XSS exploits in the ConfigureReport page
 *
 * @since v5.2
 */
@WebTest({Category.FUNC_TEST, Category.REPORTS, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
public class TestXssInConfigureReport extends BaseJiraFuncTest {

    private static final String XSS_ID = "__xss_script_injected_into_the_page__";
    private static final String XSS = "\"/><script id='" + XSS_ID + "'></script>";
    private static final String XSS_ENCODED = "&quot;/&gt;&lt;script id=&#39;__xss_script_injected_into_the_page__&#39;&gt;&lt;/script&gt;";

    @Inject
    private Administration administration;

    @Test
    @Restore("TestConfigureReport.xml")
    public void testConfigureReportXSS() {
        assertXssNotInPage("/secure/ConfigureReport.jspa?atl_token=Pi7Pim9fcC&versionId=10000&sortingOrder=least&completedFilter=all&subtaskInclusion='all\"" + XSS + "'&selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:time-tracking&Next=Next");
    }

    @Test
    @RestoreBlankInstance
    public void testConfigureReportXssFromCustomFieldName() {
        administration.customFields().addCustomField("com.atlassian.jira.plugin.system.customfieldtypes:userpicker", XSS);
        assertXssNotInPage("/secure/ConfigureReport!default.jspa?reportKey=com.atlassian.jira.jira-core-reports-plugin:pie-report");
    }

    private void assertXssNotInPage(final String url) {
        tester.gotoPage(url);
        tester.assertElementNotPresent(XSS_ID);
        tester.assertTextNotPresent(XSS);

        //Can be either directly in the page or in an option
        List<String> optionLabels = readOptionLabels("statistictype_select");
        if (!optionLabels.isEmpty()) {
            assertTrue("Option " + XSS_ENCODED + " not found in page from " + optionLabels, optionLabels.stream().anyMatch(optionLabel -> optionLabel.contains(XSS_ENCODED)));
        } else {
            tester.assertTextPresent(XSS_ENCODED);
        }
    }

    private List<String> readOptionLabels(String selectId) {
        List<String> optionValues = new ArrayList<>();
        HTMLSelectElement select = (HTMLSelectElement)tester.getDialog().getElement(selectId);
        if (select == null) {
            return Collections.emptyList();
        }
        for (int i = 0; i < select.getOptions().getLength(); i++) {
            HTMLOptionElement option = (HTMLOptionElement)select.getOptions().item(i);
            optionValues.add(option.getText());
        }
        return optionValues;
    }
}
