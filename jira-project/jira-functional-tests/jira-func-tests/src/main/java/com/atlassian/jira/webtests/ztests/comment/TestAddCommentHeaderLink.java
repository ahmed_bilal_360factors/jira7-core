package com.atlassian.jira.webtests.ztests.comment;

import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.COMMENTS})
@LoginAs(user = ADMIN_USERNAME)
public class TestAddCommentHeaderLink extends TestAddComment {
    public TestAddCommentHeaderLink() {
        super("comment-issue");
    }
}
