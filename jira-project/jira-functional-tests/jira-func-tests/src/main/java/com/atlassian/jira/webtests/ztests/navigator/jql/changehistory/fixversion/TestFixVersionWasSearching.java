package com.atlassian.jira.webtests.ztests.navigator.jql.changehistory.fixversion;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.ChangeHistoryAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Set;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * @since v4.4
 */
@WebTest({Category.FUNC_TEST, Category.JQL, Category.CHANGE_HISTORY})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestFixVersionWasSearching.xml")
public class TestFixVersionWasSearching extends BaseJiraFuncTest {

    private static final String FIELD_NAME = "fixversion";
    private static final String VERSION_1 = "'New Version 1'";
    private static final String VERSION_2 = "'New Version 4'";
    private static final String VERSION_3 = "'New Version 5'";

    @Inject
    private ChangeHistoryAssertions history;

    @Before
    public void setUp() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    public void testWasEmptySearch() {
        String[] issueKeys = {"HSP-12", "HSP-11", "HSP-10", "HSP-9", "HSP-8", "HSP-7", "HSP-6", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1"};
        history.assertWasEmptySearchReturnsEmptyValuesUsingEmptyKeyword(FIELD_NAME, issueKeys);
    }

    @Test
    public void testWasNotEmptySearch() {
        String[] issueKeys = {"HSP-13"};
        history.assertWasNotEmptySearchReturnsNotEmptyValuesWithEmptyKeyword(FIELD_NAME, issueKeys);
    }

    @Test
    public void testWasSearchUsingSingleValueOperandsReturnsExpectedValues() {
        history.assertWasSearchReturnsExpectedValues(FIELD_NAME, VERSION_1, "HSP-13", "HSP-12", "HSP-11", "HSP-10", "HSP-1");
        history.assertWasSearchReturnsExpectedValues(FIELD_NAME, VERSION_2, "HSP-12", "HSP-11", "HSP-10", "HSP-3");
        history.assertWasSearchReturnsExpectedValues(FIELD_NAME, VERSION_3, "HSP-12", "HSP-11", "HSP-3");
    }

    @Test
    public void testWasSearchUsingListOperands() {
        Set versions = Sets.newHashSet(VERSION_1, VERSION_2);
        history.assertWasInSearchReturnsExpectedValues(FIELD_NAME, versions, "HSP-13", "HSP-12", "HSP-11", "HSP-10", "HSP-3", "HSP-1");
        versions = Sets.newHashSet(VERSION_2, VERSION_3);
        history.assertWasInSearchReturnsExpectedValues(FIELD_NAME, versions, "HSP-12", "HSP-11", "HSP-10", "HSP-3");
    }

    @Test
    public void testWasNotInSearchUsingListOperands() {
        Set versions = Sets.newHashSet(VERSION_1, VERSION_3);
        String[] expected = {"HSP-9", "HSP-8", "HSP-7", "HSP-6", "HSP-5", "HSP-4", "HSP-2"};
        history.assertWasNotInSearchReturnsExpectedValues(FIELD_NAME, versions, expected);
    }


    @Test
    public void testWasSearchUsingByPredicate() {
        String[] expected = {"HSP-13", "HSP-12", "HSP-11", "HSP-10", "HSP-1"};
        history.assertWasBySearchReturnsExpectedValues(FIELD_NAME, VERSION_1, "admin", expected);
        expected = new String[]{};
        history.assertWasBySearchReturnsExpectedValues(FIELD_NAME, VERSION_2, "fred", expected);
        expected = new String[]{"HSP-9"};
        history.assertWasBySearchUsingListOperandsReturnsExpectedValues(FIELD_NAME, "empty", Sets.newHashSet("fred", "bob"), expected);
    }

    @Test
    public void testWasSearchUsingDuringPredicate() {
        String[] expected = {"HSP-1"};
        history.assertWasDuringSearchReturnsExpectedValues(FIELD_NAME, VERSION_1, "'2011/05/01'", "'2011/05/31'", expected);
    }

    @Test
    public void testWasSearchUsingBeforePredicate() {
        String[] expected = {};
        history.assertWasBeforeSearchReturnsExpectedValues(FIELD_NAME, VERSION_2, "'2011/05/01'", expected);
        expected = new String[]{"HSP-1"};
        history.assertWasBeforeSearchReturnsExpectedValues(FIELD_NAME, VERSION_1, "'2011/05/19 10:55'", expected);
    }

    @Test
    public void testWasSearchUsingAfterPredicate() {
        String[] expected = {"HSP-13", "HSP-12", "HSP-11", "HSP-10", "HSP-1"};
        history.assertWasAfterSearchReturnsExpectedValues(FIELD_NAME, VERSION_1, "'2011/05/01'", expected);
        expected = new String[]{"HSP-13", "HSP-12", "HSP-11", "HSP-1"};
        history.assertWasAfterSearchReturnsExpectedValues(FIELD_NAME, VERSION_1, "'2011/08/26 14:29'", expected);
    }

    @Test
    public void testWasSearchUsingOnPredicate() {
        String[] expected = {};
        history.assertWasOnSearchReturnsExpectedValues(FIELD_NAME, VERSION_1, "'2011/05/01'", expected);
        expected = new String[]{"HSP-1"};
        history.assertWasOnSearchReturnsExpectedValues(FIELD_NAME, VERSION_1, "'2011/06/01'", expected);
    }

    @Test
    public void testWasSearchUsingLongOperandsIsValid() {
        history.assertWasSearchReturnsExpectedValues(FIELD_NAME, "10000", "HSP-13", "HSP-12", "HSP-11", "HSP-10", "HSP-1");
    }

    @Test
    public void testWasSearchUsingUnclosedListIsInvalid() {
        // invalid list
        String expectedError = "Error in the JQL Query: Expecting ')' before the end of the query.";
        history.assertInvalidSearchProducesError(FIELD_NAME, "(fred, bob", "", expectedError);
    }

    @Test
    public void testWasSearchUsingIncorrectPredicateIsInvalid() {
        // invalid predicate
        String expectedError = "Error in the JQL Query: Expecting either 'OR' or 'AND' but got 'at'. (line 1, character 28)";
        history.assertInvalidSearchProducesError(FIELD_NAME, "(fred, bob)", "at '10:55'", expectedError);
    }
}
