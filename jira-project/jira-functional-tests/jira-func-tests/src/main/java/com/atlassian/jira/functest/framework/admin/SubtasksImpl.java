package com.atlassian.jira.functest.framework.admin;

import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.NavigationImpl;
import com.atlassian.jira.testkit.client.log.FuncTestLoggerImpl;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;

/**
 * @since v4.0
 */
public class SubtasksImpl implements Subtasks {

    private final WebTester tester;
    private final JIRAEnvironmentData environmentData;
    private final FuncTestLoggerImpl logger;
    private Navigation navigation;

    public SubtasksImpl(WebTester tester, JIRAEnvironmentData environmentData) {
        this.tester = tester;
        this.environmentData = environmentData;
        this.logger = new FuncTestLoggerImpl(2);
    }

    @Override
    public void enable() {
        getNavigation().gotoAdminSection(Navigation.AdminSection.SUBTASKS);

        // if the link doesn't exist, we're already ON
        if (tester.getDialog().isLinkPresent("enable_subtasks")) {
            logger.log("Enabling sub-tasks");
            tester.clickLink("enable_subtasks");
        }
    }

    @Override
    public void disable() {
        getNavigation().gotoAdminSection(Navigation.AdminSection.SUBTASKS);

        // if the link doesn't exist, we're already OFF
        if (tester.getDialog().isLinkPresent("disable_subtasks")) {
            logger.log("Disabling sub-tasks");
            tester.clickLink("disable_subtasks");
        }
    }

    @Override
    public boolean isEnabled() {
        getNavigation().gotoAdminSection(Navigation.AdminSection.SUBTASKS);
        final HtmlPage page = new HtmlPage(tester);
        return page.isLinkPresentWithExactText("Disable");
    }

    @Override
    public void addSubTaskType(final String subTaskName, final String subTaskDescription) {
        enable();
        tester.clickLink("add-subtask-type");
        tester.setFormElement("name", subTaskName);
        tester.setFormElement("description", subTaskDescription);
        tester.submit("Add");
    }

    @Override
    public void deleteSubTaskType(final String subTaskName) {
        enable();
        tester.clickLink("del_" + subTaskName);
        tester.submit("Delete");
    }

    private Navigation getNavigation() {
        if (navigation == null) {
            navigation = new NavigationImpl(tester, environmentData);
        }
        return navigation;
    }
}
