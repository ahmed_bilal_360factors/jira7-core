package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.bulk.TestBulkOperationIssueNavigator;
import com.atlassian.jira.webtests.ztests.fields.TestResolutionDateField;
import com.atlassian.jira.webtests.ztests.issue.TestSearchXmlView;
import com.atlassian.jira.webtests.ztests.misc.TestReplacedLocalVelocityMacros;
import com.atlassian.jira.webtests.ztests.navigator.TestCustomFieldsVisibilityOnIssueTable;
import com.atlassian.jira.webtests.ztests.navigator.TestCustomFieldsVisibilityWhenExportingAllColumnsToExcel;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigator;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorColumnLinks;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorCsvView;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorExcelView;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorFullContentView;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorPrintableView;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorRedirects;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorRssView;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorWordView;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorXmlView;
import com.atlassian.jira.webtests.ztests.navigator.TestIssueNavigatorXmlViewTimeTracking;
import com.atlassian.jira.webtests.ztests.navigator.TestNavigationBarWebFragment;
import com.atlassian.jira.webtests.ztests.navigator.TestSearchXmlViewErrors;
import com.atlassian.jira.webtests.ztests.user.TestAutoWatches;
import com.atlassian.jira.webtests.ztests.user.TestUserNavigationBarWebFragment;
import com.atlassian.jira.webtests.ztests.user.TestUserVotes;
import com.atlassian.jira.webtests.ztests.user.TestUserWatches;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of tests related to the issue navigator
 *
 * @since v4.0
 */
public class FuncTestSuiteIssueNavigator {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestNavigationBarWebFragment.class)
                .add(TestUserNavigationBarWebFragment.class)
                .add(TestIssueNavigator.class)
                .add(TestIssueNavigatorColumnLinks.class)
                .add(TestIssueNavigatorXmlView.class)
                .add(TestIssueNavigatorPrintableView.class)
                .add(TestIssueNavigatorXmlViewTimeTracking.class)
                .add(TestIssueNavigatorRedirects.class)
                .add(TestIssueNavigatorExcelView.class)
                .add(TestIssueNavigatorCsvView.class)
                .add(TestIssueNavigatorRssView.class)
                .add(TestIssueNavigatorFullContentView.class)
                .add(TestIssueNavigatorWordView.class)
                .add(TestBulkOperationIssueNavigator.class)
                .add(TestResolutionDateField.class)
                .add(TestSearchXmlView.class)
                .add(TestSearchXmlViewErrors.class)
                .add(TestAutoWatches.class)
                .add(TestUserWatches.class)
                .add(TestUserVotes.class)
                .add(TestReplacedLocalVelocityMacros.class)
                .add(TestCustomFieldsVisibilityWhenExportingAllColumnsToExcel.class)
                .add(TestCustomFieldsVisibilityOnIssueTable.class)
                .build();
    }
}
