package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

/**
 * TODO: Document this class / interface here
 *
 * @since v7.0
 */
@WebTest({Category.FUNC_TEST, Category.BROWSING})
@RestoreBlankInstance
@LoginAs(user = FunctTestConstants.ADMIN_USERNAME)
public class TestCategorySelection {

    @Test
    public void artifitialTest1() {
        System.out.println("Artifitial test fired!");
    }

    @Test
    public void artifitialTest2() {
        System.out.println("Artifitial test fired!");
    }

}
