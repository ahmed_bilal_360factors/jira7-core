package com.atlassian.jira.webtests.ztests.timetracking.modern;

import com.atlassian.fugue.Effect;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import com.atlassian.jira.testkit.client.restclient.Worklog;
import com.atlassian.jira.webtests.Groups;
import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.matchers.SearchResultMatchers.sizeEquals;
import static com.atlassian.jira.util.SearchResultMatcher.issues;
import static org.junit.Assert.assertThat;

/**
 * Func test for searching worklogs by worklog comment (description)s.
 *
 * @since v7.0
 */
@WebTest({Category.FUNC_TEST, Category.REST, Category.WORKLOGS})
public final class TestWorklogCommentSearching extends AbstractWorklogSearchingTest {
    @Before
    public void setUp() {
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERMISSION_SCHEME_ID, ProjectPermissions.EDIT_OWN_WORKLOGS, Groups.USERS);
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERMISSION_SCHEME_ID, ProjectPermissions.DELETE_OWN_WORKLOGS, Groups.USERS);
        backdoor.usersAndGroups().addUserToGroup("fred", "jira-developers");
    }

    @Test
    public void testEqualsOneCommentOperator() throws Exception {
        final String issueKey1 = createDefaultIssue();
        final String issueKey2 = createDefaultIssue();
        createWorklogAsUserWithComment(issueKey1, "admin", "I worked hard");
        createWorklogAsUserWithComment(issueKey2, "fred", "MyExactComment");
        SearchResult searchResult = executeJqlSearch("worklogComment ~ MyExactComment");
        assertThat(searchResult, sizeEquals(1));
        assertThat(searchResult, issues(issueKey2));
    }

    @Test
    public void testEmptyCommentOperator() throws Exception {
        final String issueKey1 = createDefaultIssue();
        final String issueKey2 = createDefaultIssue();
        createWorklogAsUserWithComment(issueKey1, "admin", "I worked hard");
        createWorklogAsUserWithComment(issueKey2, "fred", "");
        SearchResult searchResult = executeJqlSearch("worklogComment is empty");
        assertThat(searchResult, sizeEquals(1));
        assertThat(searchResult, issues(issueKey2));
    }

    @Test
    public void testNotEqualsCommentOperator() throws Exception {
        final String issueKey1 = createDefaultIssue();
        final String issueKey2 = createDefaultIssue();
        createWorklogAsUserWithComment(issueKey1, "admin", "I worked hard");
        createWorklogAsUserWithComment(issueKey2, "fred", "MyExactComment");
        SearchResult searchResult = executeJqlSearch("worklogComment !~ MyExactComment");
        assertThat(searchResult, sizeEquals(1));
        assertThat(searchResult, issues(issueKey1));
    }

    @Test
    public void testEqualsTwoCommentsOperator() throws Exception {
        final String issueKey1 = createDefaultIssue();
        final String issueKey2 = createDefaultIssue();
        createWorklogAsUserWithComment(issueKey1, "admin", "MyExactComment alpha");
        createWorklogAsUserWithComment(issueKey2, "fred", "MyExactComment beta");
        SearchResult searchResult = executeJqlSearch("worklogComment ~ MyExactComment");
        assertThat(searchResult, sizeEquals(2));
        assertThat(searchResult, issues(issueKey1, issueKey2));
    }

    @Test
    public void testDeleteCommentReindexesIssue() {
        final String issueKey1 = createDefaultIssue();
        final Worklog worklog = createWorklogAsUserWithComment(issueKey1, "admin", "I worked hard");
        SearchResult searchResult = executeJqlSearch("worklogComment ~ worked");
        assertThat(searchResult, sizeEquals(1));
        assertThat(searchResult, issues(issueKey1));

        // Delete the worklog
        deleteWorklog(issueKey1, worklog);

        // Check to see that the issue is not returned in the search (i.e. it has reindexed)
        searchResult = executeJqlSearch("worklogComment ~ worked");
        assertThat(searchResult, sizeEquals(0));
    }

    @Test
    public void testUpdateCommentReindexesIssue() {
        final String issueKey1 = createDefaultIssue();
        final Worklog worklog = createWorklogAsUserWithComment(issueKey1, "admin", "I worked hard");
        SearchResult searchResult = executeJqlSearch("worklogComment ~ worked");
        assertThat(searchResult, sizeEquals(1));
        assertThat(searchResult, issues(issueKey1));

        // Update the worklog
        worklog.comment = "newComment";
        worklog.timeSpentSeconds = null;
        updateWorklog(issueKey1, worklog);

        // Check to see that the issue is not returned in the search (i.e. it has reindexed)
        searchResult = executeJqlSearch("worklogComment ~ worked");
        assertThat(searchResult, sizeEquals(0));
        searchResult = executeJqlSearch("worklogComment ~ newComment");
        assertThat(searchResult, sizeEquals(1));
    }

    @Test
    public void testSearchingWorklogInDeletedIssue() throws Exception {
        final String issueKey = createDefaultIssue();
        createWorklogAsUserWithComment(issueKey, "admin", "I worked on an issue that will be deleted");

        MatcherAssert.assertThat(executeJqlSearch("worklogComment ~ deleted"), sizeEquals(1));

        issueClient.delete(issueKey, null);

        MatcherAssert.assertThat(executeJqlSearch("worklogComment ~ deleted"), sizeEquals(0));
    }

    @Test
    public void testAuthorAndDateConditionSimple() throws Exception {
        final String issue = createDefaultIssue();
        createWorklog(issue, "2014-08-10", "admin", "comment1");
        createWorklog(issue, "2014-08-13", "fred", "comment2");


        SearchResult searchResult = executeJqlSearch("worklogDate = 2014-08-10 AND worklogAuthor = admin AND worklogComment ~ comment1");
        MatcherAssert.assertThat(searchResult, sizeEquals(1));
        MatcherAssert.assertThat(searchResult, issues(issue));

        searchResult = executeJqlSearch("worklogDate = 2014-08-10 AND worklogComment ~ comment2");
        MatcherAssert.assertThat(searchResult, sizeEquals(0));

        searchResult = executeJqlSearch("worklogAuthor = fred AND worklogComment ~ comment1");
        MatcherAssert.assertThat(searchResult, sizeEquals(0));
    }

    @Test
    public void testOrConditions() throws Exception {
        final String issueWithWorklogsOn10thAnd13th = createDefaultIssue();
        createWorklog(issueWithWorklogsOn10thAnd13th, "2014-08-10", "admin", "comment1");
        createWorklog(issueWithWorklogsOn10thAnd13th, "2014-08-13", "admin", "comment2");
        final String issueWithWorklogOn9thAnd14th = createDefaultIssue();
        createWorklog(issueWithWorklogOn9thAnd14th, "2014-08-09", "fred", "comment3");
        createWorklog(issueWithWorklogOn9thAnd14th, "2014-08-14", "fred", "comment4");

        final Effect<String> testBothIssuesReturnedForQuery = new Effect<String>() {
            @Override
            public void apply(final String query) {
                SearchResult searchResult = executeJqlSearchAsUser(query, "admin");
                MatcherAssert.assertThat(searchResult, sizeEquals(2));
                MatcherAssert.assertThat(searchResult, issues(issueWithWorklogsOn10thAnd13th, issueWithWorklogOn9thAnd14th));
            }
        };

        testBothIssuesReturnedForQuery.apply("worklogComment ~ comment1 or worklogComment ~ comment3");
        testBothIssuesReturnedForQuery.apply("worklogDate = 2014-08-10 OR worklogComment ~ comment4");
        testBothIssuesReturnedForQuery.apply("worklogAuthor = admin OR worklogComment ~ comment3");
        testBothIssuesReturnedForQuery.apply("worklogAuthor = admin AND worklogComment ~ comment1 OR worklogAuthor = fred AND worklogComment ~ comment3");
    }

    @Test
    public void testSimpleTextSearchingInDescriptionAndWorklog() throws Exception {
        testSimpleTextSearchingInDescriptionAndWorklogWithText("bears");
    }

    @Test
    public void testSimpleTextSearchingComplexTextInDescriptionAndWorklog() throws Exception {
        testSimpleTextSearchingInDescriptionAndWorklogWithText("white bears");
    }

    private void testSimpleTextSearchingInDescriptionAndWorklogWithText(final String toSearch) {
        String issueWithSummary = backdoor.issues().createIssue("HSP", "test issue! " + toSearch, "admin").key;
        String issueOther = backdoor.issues().createIssue("HSP", "some other issue ", "admin").key;
        String issueOtherAuthor = backdoor.issues().createIssue("HSP", "some other issue ", "fred").key;

        String issueWithWorklog = createDefaultIssue();
        createWorklog(issueWithWorklog, "2014-08-13", "admin", toSearch + " comment");
        createWorklog(issueWithWorklog, "2014-08-13", "admin", "some other admin worklog");
        createWorklog(issueWithWorklog, "2014-08-09", "fred", "comment " + toSearch);

        SearchResult searchResult = executeJqlSearch(String.format("worklogAuthor = admin", toSearch));
        MatcherAssert.assertThat(searchResult, sizeEquals(1));

        searchResult = executeJqlSearch(String.format("text ~ '%s'", toSearch));
        MatcherAssert.assertThat(searchResult, sizeEquals(2));

        searchResult = executeJqlSearch(String.format("worklogAuthor = admin AND text ~ '%s'", toSearch));
        MatcherAssert.assertThat(searchResult, sizeEquals(1));
    }
}
