package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Indexing;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CLONERS_INWARD_LINK_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CLONERS_LINK_TYPE_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CLONERS_OUTWARD_LINK_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.LINK_CLONE_ISSUE;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ISSUES;
import static com.atlassian.jira.webtests.Groups.USERS;
import static org.junit.Assert.assertThat;

/**
 * Copyright (c) 2002-2004
 * All rights reserved.
 */
@WebTest({Category.FUNC_TEST, Category.CLONE_ISSUE, Category.ISSUES, Category.SUB_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestCloneIssueWithSubTasks extends AbstractCloneIssueTest {
    protected static final String CLONE_LINKS_CHECKBOX_LABEL = "Clone links";
    protected static final String CLONE_SUBTASKS_CHECKBOX_LABEL = "Clone sub-tasks";
    protected static final String CLONE_SUMMARY_PREFIX = "CLONE - ";
    protected static final String SUMMARY_FIELD_NAME = "summary";
    protected static final String SUBTASKS_TEXT = "Sub-Tasks";
    protected static final String LINKS_TEXT = "Issue Links";
    protected static final String CLONE_LINKS_CHECKBOX_NAME = "cloneLinks";
    private static final String CLONE_SUBTASKS_CHECKBOX_NAME = "cloneSubTasks";

    @Inject
    private Indexing indexing;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        // Restore different data to the parent test, as this test tests sub-tasks as well.
        administration.restoreData("TestCloneIssueWithSubTasks.xml");
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
    }

    @Test
    public void testCloneNoLinks() {
        // Test cloning an issue with no links and no sub-tasks
        gotoIssueAndClickClone("HSP-1");
        // Ensure linsk chekcbox is not shown as there are no links to clone
        tester.assertTextNotPresent(CLONE_LINKS_CHECKBOX_LABEL);
        // Ensure sub-tasks checkbox is not there as there are no sub-tasks to clone
        tester.assertTextNotPresent(CLONE_SUBTASKS_CHECKBOX_LABEL);

        String cloneKey = cloneIssue("HSP", "Test Issue", false);

        // Ensure that all fields were cloned correctly
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Type", "Task");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Status", "Open");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Priority", "Minor");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Assignee", ADMIN_FULLNAME);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Reporter", ADMIN_FULLNAME);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Vote", "0");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Watch", "0");

        // Ordering fixed for JRA-15011
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Component/s", "New Component 1", "New Component 2"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Affects Version/s", "New Version 1", "New Version 4"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Fix Version/s", "New Version 4", "New Version 5"});

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Original Estimate", "4 days", "Remaining Estimate", "4 days", "Time Spent", "Not Specified"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Environment", "Test Environment"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Description", "Test Description");

        // Check that sub-tasks did not magically appear
        tester.assertTextNotPresent(SUBTASKS_TEXT);
        // Check that links did not magically appear
        tester.assertTextNotPresent(LINKS_TEXT);

        //check that the item details were clone is reflected in the index
        indexing.assertIndexedFieldCorrect("//item", EasyMap.build("type", "Task", "status", "Open", "priority", "Minor", "votes", "0", "description", "Test Description"), null, cloneKey);
    }

    @Test
    public void testClonePermission() {
        // The user should only be allowed to clone issues if they have Create permission in the issue's project
        navigation.issue().gotoIssue("HSP-1");
        // ensure the clone link is there
        tester.assertLinkPresent(LINK_CLONE_ISSUE);
        // Remove the create permission
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, CREATE_ISSUES, USERS);
        // Navigate back to the issue
        navigation.issue().gotoIssue("HSP-1");
        // Ensure teh clone link is not there
        tester.assertLinkNotPresent(LINK_CLONE_ISSUE);
    }

    @Test
    public void testCloneWithLinks() {
        // Test cloning issue with links and no sub-tasks
        gotoIssueAndClickClone("HSP-2");
        // Ensure the links checkbox is shown as the issue has links
        tester.assertTextPresent(CLONE_LINKS_CHECKBOX_LABEL);
        // Ensure the sub-taks checkbox is not shown as the issue has no sub-tasks to clone
        tester.assertTextNotPresent(CLONE_SUBTASKS_CHECKBOX_LABEL);

        // Select that we do want to clone links
        tester.checkCheckbox(CLONE_LINKS_CHECKBOX_NAME, "true");

        String clonedKey = cloneIssue("HSP", "Test Issue with link", false);

        // Ensure that sub-tasks did not magically appear from somewhere :)
        tester.assertTextNotPresent(SUBTASKS_TEXT);

        // Ensure that all the linsk were copied across
        tester.assertTextPresent(LINKS_TEXT);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"out", "HSP-3"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"in", "HSP-4"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"outward", "HSP-5"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"inward", "HSP-6"});

        //test that the cloned issue links and summary have been indexed correctly
        indexing.assertIndexedFieldCorrect("//item", EasyMap.build("summary", "CLONE - Test Issue with link", "resolution", "Unresolved"), null, clonedKey);
        indexing.assertIndexedFieldCorrect("//item/issuelinks/issuelinktype", EasyMap.build("name", "Test Link 1"), null, clonedKey);

    }

    @Test
    public void testCloneIssueLinksDisabled() {
        // Disable issue links in admin section
        administration.issueLinking().disable();

        // Test cloning issue which has links and sub-tasks
        gotoIssueAndClickClone("HSP-2");

        // Ensure links checkbox is not present as links are disabled
        tester.assertTextNotPresent(CLONE_LINKS_CHECKBOX_LABEL);

        cloneIssue("HSP", "Test Issue with link", false);

        // Ensure links were not cloned
        tester.assertTextNotPresent(LINKS_TEXT);
    }

    @Test
    public void testCloneWithClonersLink() {
        createClonersLinkType();

        // Test cloning an issue with the cloners link present
        String originalIssueKey = "HSP-1";
        gotoIssueAndClickClone(originalIssueKey);

        // Ensure links checkbox is not shown as there are no links to clone
        tester.assertTextNotPresent(CLONE_LINKS_CHECKBOX_LABEL);
        // Ensure sub-tasks checkbox is not there as there are no sub-tasks to clone
        tester.assertTextNotPresent(CLONE_SUBTASKS_CHECKBOX_LABEL);

        String cloneIssueKey = cloneIssue("HSP", "Test Issue", true);

        // Check that sub-tasks did not magically appear
        tester.assertTextNotPresent(SUBTASKS_TEXT);

        // Check that the cloners link was created
        tester.assertTextPresent(LINKS_TEXT);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{CLONERS_OUTWARD_LINK_NAME, originalIssueKey});

        // Ensure the Cloners link appears on the issue that was cloned
        navigation.issue().gotoIssue(originalIssueKey);
        tester.assertTextPresent(LINKS_TEXT);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{CLONERS_INWARD_LINK_NAME, cloneIssueKey});
    }


    @Test
    public void testCloneNoLinksWithSubTask() {
        // Test cloning an issue with sub-tasks and no links
        gotoIssueAndClickClone("HSP-7");
        // Ensure links check box is not there as there are no links to clone
        tester.assertTextNotPresent(CLONE_LINKS_CHECKBOX_LABEL);
        // Ensure sub-tasks check box is there as there are sub-tasks to clone
        tester.assertTextPresent(CLONE_SUBTASKS_CHECKBOX_LABEL);

        // Ensure that sub-tasks check box is checked
        tester.assertFormElementEquals(CLONE_SUBTASKS_CHECKBOX_NAME, "true");
        // As sub-tasks form element is checked by default no need to select it

        cloneIssue("HSP", "Test Issue with sub task", false);

        // Test that links did not magically appear
        tester.assertTextNotPresent(LINKS_TEXT);

        // Ensure all sub-tasks are there
        tester.assertTextPresent(SUBTASKS_TEXT);
        tester.assertTextPresent("Test Sub task");
        tester.assertTextPresent("Another Sub Task");
    }

    @Test
    public void testCloneWithLinksAndSubTasks() {
        // Test cloning with links and sub-tasks
        gotoIssueAndClickClone("HSP-10");
        // Ensure links checkbox is there as there are links to clone
        tester.assertTextPresent(CLONE_LINKS_CHECKBOX_LABEL);
        // Ensure sub-tasks checkbox is there as there are links to clone
        tester.assertTextPresent(CLONE_SUBTASKS_CHECKBOX_LABEL);

        // Check the clone links checkbox. The sub-tasks checkbox should be checked automatically
        tester.checkCheckbox(CLONE_LINKS_CHECKBOX_NAME, "true");

        cloneIssue("HSP", "Issue with link and sub-task", false);

        // Ensure links were cloned correctly
        tester.assertTextPresent(LINKS_TEXT);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"out", "HSP-3"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"inward", "HSP-6"});

        // Ensure sub-tasks were cloned correctly
        tester.assertTextPresent(SUBTASKS_TEXT);
        tester.assertTextPresent("Another test subtask");
        tester.assertTextPresent("Second sub-task");
    }

    @Test
    public void testCloneIssueWithLinksAndSubTasksNoLinks() {
        // Test cloning issue which has links and sub-tasks
        gotoIssueAndClickClone("HSP-10");
        // Ensure links checkbox is there as there are links to clone
        tester.assertTextPresent(CLONE_LINKS_CHECKBOX_LABEL);
        // Ensure sub-tasks checkbox is there as there are links to clone
        tester.assertTextPresent(CLONE_SUBTASKS_CHECKBOX_LABEL);

        // Do not check the links checkbox
        // Subtasks check box is checked by default

        cloneIssue("HSP", "Issue with link and sub-task", false);

        // Ensure links were not cloned
        tester.assertTextNotPresent(LINKS_TEXT);

        // Ensure sub-tasks were cloned correctly
        tester.assertTextPresent(SUBTASKS_TEXT);
        tester.assertTextPresent("Another test subtask");
        tester.assertTextPresent("Second sub-task");
    }

    @Test
    public void testCloneIssueWithLinksAndSubTasksNoSubTasks() {
        // Test cloning issue which has links and sub-tasks
        gotoIssueAndClickClone("HSP-10");
        // Ensure links checkbox is there as there are links to clone
        tester.assertTextPresent(CLONE_LINKS_CHECKBOX_LABEL);
        // Ensure sub-tasks checkbox is there as there are links to clone
        tester.assertTextPresent(CLONE_SUBTASKS_CHECKBOX_LABEL);

        // Check the links checkbox
        tester.checkCheckbox(CLONE_LINKS_CHECKBOX_NAME, "true");

        // Uncheck the sub-tasks checkbox
        tester.uncheckCheckbox(CLONE_SUBTASKS_CHECKBOX_NAME);

        cloneIssue("HSP", "Issue with link and sub-task", false);

        // Ensure links were not cloned
        tester.assertTextPresent(LINKS_TEXT);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"out", "HSP-3"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"inward", "HSP-6"});

        // Ensure sub-tasks were not cloned
        tester.assertTextNotPresent(SUBTASKS_TEXT);
    }

    @Test
    public void testCloneIssueWithLinksAndSubTasksNoSubTasksNoLinks() {
        // Test cloning issue which has links and sub-tasks
        gotoIssueAndClickClone("HSP-10");
        // Ensure links checkbox is there as there are links to clone
        tester.assertTextPresent(CLONE_LINKS_CHECKBOX_LABEL);
        // Ensure sub-tasks checkbox is there as there are links to clone
        tester.assertTextPresent(CLONE_SUBTASKS_CHECKBOX_LABEL);

        // Do not check the links checkbox
        // Uncheck the sub-tasks checkbox
        tester.uncheckCheckbox(CLONE_SUBTASKS_CHECKBOX_NAME);

        cloneIssue("HSP", "Issue with link and sub-task", false);

        // Ensure links were not cloned
        tester.assertTextNotPresent(LINKS_TEXT);
        // Ensure sub-tasks were not cloned
        tester.assertTextNotPresent(SUBTASKS_TEXT);
    }

    @Test
    public void testCloneIssueWithSubTaskWithLink() {
        gotoIssueAndClickClone("HSP-13");
        // Ensure links checkbox is there as an issue has a sub task with a link
        tester.assertTextPresent(CLONE_LINKS_CHECKBOX_LABEL);
        // Ensure sub-tasks checkbox is there as there are links to clone
        tester.assertTextPresent(CLONE_SUBTASKS_CHECKBOX_LABEL);

        // Check the links checkbox
        tester.checkCheckbox(CLONE_LINKS_CHECKBOX_NAME, "true");

        cloneIssue("HSP", "Test issue with subtask which has a link", false);

        // Ensure links are not there as the cloned issue does not have any links
        tester.assertTextNotPresent(LINKS_TEXT);
        // Ensure sub-tasks were cloned correctly
        tester.assertTextPresent(SUBTASKS_TEXT);
        String subTaskWithLinkSummary = "Sub task with link";
        tester.assertTextPresent(subTaskWithLinkSummary);
        tester.assertTextPresent("One more subtask");

        tester.clickLinkWithText(subTaskWithLinkSummary);
        tester.assertTextPresent(LINKS_TEXT);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"in", "HSP-4"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"outward", "HSP-5"});
    }

    @Test
    public void testCloneIssueLinksDisabledWithSubTasks() {
        // Disable issue links in admin section
        administration.issueLinking().disable();

        // Test cloning issue which has links and sub-tasks
        gotoIssueAndClickClone("HSP-10");

        // Ensure links checkbox is not present as links are disabled
        tester.assertTextNotPresent(CLONE_LINKS_CHECKBOX_LABEL);

        // Ensure the sub-task check box is there as there are sub-tasks to clone
        tester.assertTextPresent(CLONE_SUBTASKS_CHECKBOX_LABEL);

        cloneIssue("HSP", "Issue with link and sub-task", false);

        // Ensure links were not cloned
        tester.assertTextNotPresent(LINKS_TEXT);

        // Ensure sub-tasks were cloned correctly
        tester.assertTextPresent(SUBTASKS_TEXT);
        tester.assertTextPresent("Another test subtask");
        tester.assertTextPresent("Second sub-task");
    }

    //JRA-18731
    @Test
    public void testCloneResolvedIssueClearsResolutionDate() {
        navigation.issue().gotoIssue("HSP-1");
        //first we need to resolve the issue, so we can test cloning a resolved issue.
        tester.clickLink("action_id_5");
        tester.setWorkingForm("issue-workflow-transition");
        tester.setFormElement("comment", "This issue is now done!");
        tester.submit("Transition");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Test Issue", "Due", "Created", "Updated", "Resolved"});

        //now lets clone the issue!
        tester.clickLink("clone-issue");
        tester.setFormElement("summary", "CLONE - Test Issue");
        tester.submit("Create");

        tester.assertTextPresent("HSP-17");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"CLONE - Test Issue", "Due", "Created", "Updated"});
        assertThat(tester.getDialog().getElement("status-val").getTextContent(), Matchers.not(Matchers.containsString("Resolved")));
    }

    /**
     * Creates the Cloners link type that JIRA creates between cloned issues. If this link type does not exist the clone
     * of teh issue is not linked to the original issue.
     */
    private void createClonersLinkType() {
        navigation.gotoAdminSection(Navigation.AdminSection.LINKING);
        textAssertions.assertTextPresent("Issue Linking");
        textAssertions.assertTextPresent("ON");

        tester.setFormElement("name", CLONERS_LINK_TYPE_NAME);
        tester.setFormElement("outward", CLONERS_OUTWARD_LINK_NAME);
        tester.setFormElement("inward", CLONERS_INWARD_LINK_NAME);
        tester.submit("Add");
    }
}
