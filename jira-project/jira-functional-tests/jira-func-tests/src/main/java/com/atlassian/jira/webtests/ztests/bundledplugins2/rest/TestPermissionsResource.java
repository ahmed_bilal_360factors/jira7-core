package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.v2.permission.UserPermissionJsonBean;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.common.collect.ImmutableMap;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jackson.annotate.JsonProperty;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.matcher.PermissionMatcher.deprecatedKey;
import static com.atlassian.jira.functest.matcher.PermissionMatcher.description;
import static com.atlassian.jira.functest.matcher.PermissionMatcher.hasNotEmptyDescription;
import static com.atlassian.jira.functest.matcher.PermissionMatcher.havePermission;
import static com.atlassian.jira.functest.matcher.PermissionMatcher.id;
import static com.atlassian.jira.functest.matcher.PermissionMatcher.key;
import static com.atlassian.jira.functest.matcher.PermissionMatcher.name;
import static com.atlassian.jira.functest.matcher.PermissionMatcher.type;
import static com.atlassian.jira.permission.ProjectPermissions.ADD_COMMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.DELETE_ALL_COMMENTS;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestPermissionsRest.xml")
public class TestPermissionsResource extends BaseJiraRestTest {
    private PermissionsClient permissionsClient;

    @Before
    public void setUpTest() {
        permissionsClient = new PermissionsClient(environmentData);
        // The restore didn't restart the plugin system, but we need this module restarted
        backdoor.plugins().disablePluginModule("com.atlassian.jira.dev.func-test-plugin:func.test.global.permission");
        backdoor.plugins().enablePluginModule("com.atlassian.jira.dev.func-test-plugin:func.test.global.permission");
    }

    @Test
    public void testPermissionsEndpointIsForAdministratorsOnly() {
        final Response<PermissionsOuter> allPermissionsForAnon = permissionsClient.anonymous().getAllPermissions();
        assertEquals(allPermissionsForAnon.statusCode, 401);

        backdoor.usersAndGroups().addUser("non-admin-user");
        final Response<PermissionsOuter> allPermissionsForUser = permissionsClient.loginAs("non-admin-user").getAllPermissions();
        assertEquals(allPermissionsForUser.statusCode, 403);
    }

    @Test
    public void testCustomPermissionIsReturnedByAllPermissionsEndpoint() {
        final Response<PermissionsOuter> outer = permissionsClient.getAllPermissions();

        assertThat(outer.body.permissions.get("func.test.global.permission"), allOf(
                key("func.test.global.permission"),
                name("func.test.global.permission.name"),
                type("GLOBAL"),
                description("func.test.global.permission.description")));
    }

    @Test
    public void testGlobalPermissionsAreReturnedByAllPermissionsEndpoint() {
        final Response<PermissionsOuter> outer = permissionsClient.getAllPermissions();

        assertThat(outer.body.permissions.get("SYSTEM_ADMIN"), allOf(
                key("SYSTEM_ADMIN"),
                name("JIRA System Administrators"),
                type("GLOBAL"),
                hasNotEmptyDescription()));
    }

    @Test
    public void testProjectPermissionsAreReturnedByAllPermissionsEndpoint() {
        final Response<PermissionsOuter> outer = permissionsClient.getAllPermissions();

        assertThat(outer.body.permissions.get("LINK_ISSUES"), allOf(
                key("LINK_ISSUES"),
                name("Link Issues"),
                type("PROJECT"),
                hasNotEmptyDescription()));
        assertThat(outer.body.permissions.get("BROWSE_PROJECTS"), allOf(
                key("BROWSE_PROJECTS"),
                name("Browse Projects"),
                type("PROJECT"),
                hasNotEmptyDescription()));
    }

    @Test
    public void testCustomProjectPermissionsAreReturnedByAllPermissionsEndpoint() {
        final Response<PermissionsOuter> outer = permissionsClient.getAllPermissions();

        assertThat(outer.body.permissions.get("func.test.project.permission"), allOf(
                key("func.test.project.permission"),
                name("func.test.project.permission.name"),
                type("PROJECT"),
                description("func.test.project.permission.description")));
    }

    @Test
    public void testGlobalPermissions() {
        Response<PermissionsOuter> outer = permissionsClient.getPermissions();

        assertThat(outer.body.permissions.get("ADMINISTER"), allOf(
                key("ADMINISTER"),
                id("0"),
                name("JIRA Administrators"),
                type("GLOBAL"),
                description("Ability to perform most administration functions (excluding Import & Export, SMTP Configuration, etc.)."),
                deprecatedKey(null),
                havePermission(true)));

        //try a user without these permissions
        outer = permissionsClient.loginAs("user", "user").getPermissions();
        assertThat(outer.body.permissions.get("ADMINISTER"), havePermission(false));

        // test a plugged global permission
        assertThat(outer.body.permissions.get("func.test.global.permission"), allOf(
                key("func.test.global.permission"),
                name("func.test.global.permission.name"),
                type("GLOBAL"),
                description("func.test.global.permission.description"),
                deprecatedKey(null),
                havePermission(false)));
    }

    @Test
    public void testGetLegacyProjectPermissions() {
        Response<PermissionsOuter> outer = permissionsClient.getPermissions();

        // test a project permission
        assertThat(outer.body.permissions.get("BROWSE"), allOf(
                id("10"),
                key("BROWSE"),
                name("Browse Projects"),
                type("PROJECT"),
                description("Ability to browse projects and the issues within them."),
                deprecatedKey(true),
                havePermission(true)));

        // test an issue permission
        assertThat(outer.body.permissions.get("COMMENT_ISSUE"), allOf(
                id("15"),
                key("COMMENT_ISSUE"),
                name("Add Comments"),
                type("PROJECT"),
                description("Ability to comment on issues."),
                deprecatedKey(true),
                havePermission(true)));

        //try a user without these permissions
        outer = permissionsClient.loginAs("user", "user").getPermissions();

        // test a project permission
        assertThat(outer.body.permissions.get("BROWSE"), havePermission(false));

        // test an issue permission
        assertThat(outer.body.permissions.get("COMMENT_ISSUE"), havePermission(false));
    }

    @Test
    public void testGetProjectPermissions() {
        Response<PermissionsOuter> outer = permissionsClient.getPermissions();

        // test a project permission
        assertThat(outer.body.permissions.get(BROWSE_PROJECTS.permissionKey()), allOf(
                id("10"),
                key(BROWSE_PROJECTS.permissionKey()),
                name("Browse Projects"),
                type("PROJECT"),
                description("Ability to browse projects and the issues within them."),
                deprecatedKey(null),
                havePermission(true)));

        // test an issue permission
        assertThat(outer.body.permissions.get(ADD_COMMENTS.permissionKey()), allOf(
                id("15"),
                key(ADD_COMMENTS.permissionKey()),
                name("Add Comments"),
                type("PROJECT"),
                description("Ability to comment on issues."),
                deprecatedKey(null),
                havePermission(true)));

        //try a user without these permissions
        outer = permissionsClient.loginAs("user", "user").getPermissions();

        // test a project permission
        assertThat(outer.body.permissions.get(BROWSE_PROJECTS.permissionKey()), havePermission(false));

        // test an issue permission
        assertThat(outer.body.permissions.get(ADD_COMMENTS.permissionKey()), havePermission(false));

        // test a plugged project permission
        assertThat(outer.body.permissions.get("func.test.global.permission"), allOf(
                key("func.test.global.permission"),
                name("func.test.global.permission.name"),
                type("GLOBAL"),
                description("func.test.global.permission.description"),
                deprecatedKey(null),
                havePermission(false)));
    }

    @Test
    public void testLegacyProjectFilter() {
        assertThat(permissionsClient.getPermissions(ImmutableMap.of("projectKey", "PROJECTA")).body.permissions.get("BROWSE"), allOf(
                id("10"),
                name("Browse Projects"),
                havePermission(true)));
        assertThat(permissionsClient.getPermissions(ImmutableMap.of("projectKey", "PROJECTB")).body.permissions.get("BROWSE"), allOf(
                id("10"),
                name("Browse Projects"),
                havePermission(false)));
        assertThat(permissionsClient.getPermissions(ImmutableMap.of("projectId", "10000")).body.permissions.get("BROWSE"), allOf(
                id("10"),
                name("Browse Projects"),
                havePermission(true)));
        assertThat(permissionsClient.getPermissions(ImmutableMap.of("projectId", "10001")).body.permissions.get("BROWSE"), allOf(
                id("10"),
                name("Browse Projects"),
                havePermission(false)));
    }

    @Test
    public void testProjectFilter() {
        assertThat(permissionsClient.getPermissions(ImmutableMap.of("projectKey", "PROJECTA")).body.permissions.get(BROWSE_PROJECTS.permissionKey()), allOf(
                id("10"),
                name("Browse Projects"),
                havePermission(true)));
        assertThat(permissionsClient.getPermissions(ImmutableMap.of("projectKey", "PROJECTB")).body.permissions.get(BROWSE_PROJECTS.permissionKey()), allOf(
                id("10"),
                name("Browse Projects"),
                havePermission(false)));
        assertThat(permissionsClient.getPermissions(ImmutableMap.of("projectId", "10000")).body.permissions.get(BROWSE_PROJECTS.permissionKey()), allOf(
                id("10"),
                name("Browse Projects"),
                havePermission(true)));
        assertThat(permissionsClient.getPermissions(ImmutableMap.of("projectId", "10001")).body.permissions.get(BROWSE_PROJECTS.permissionKey()), allOf(
                id("10"),
                name("Browse Projects"),
                havePermission(false)));
    }

    @Test
    public void testLegacyIssueFiltering() {
        Response<PermissionsOuter> outer = permissionsClient.getPermissions();
        assertThat(outer.body.permissions.get("COMMENT_DELETE_ALL"), allOf(
                id("36"),
                key("COMMENT_DELETE_ALL"),
                name("Delete All Comments"),
                type("PROJECT"),
                description("Ability to delete all comments made on issues."),
                deprecatedKey(true),
                havePermission(true)));

        assertThat(permissionsClient.getPermissions(ImmutableMap.of("projectKey", "PROJECTA")).body.permissions.get("COMMENT_DELETE_ALL"), allOf(
                id("36"),
                key("COMMENT_DELETE_ALL"),
                havePermission(true)));
        assertThat(permissionsClient.getPermissions(ImmutableMap.of("issueKey", "PROJECTA-3")).body.permissions.get("COMMENT_DELETE_ALL"), allOf(
                id("36"),
                key("COMMENT_DELETE_ALL"),
                havePermission(true)));
        assertThat(permissionsClient.getPermissions(ImmutableMap.of("issueKey", "PROJECTA-4")).body.permissions.get("COMMENT_DELETE_ALL"), allOf(
                id("36"),
                key("COMMENT_DELETE_ALL"),
                havePermission(false)));
        assertThat(permissionsClient.getPermissions(ImmutableMap.of("issueId", "10200")).body.permissions.get("COMMENT_DELETE_ALL"), allOf(
                id("36"),
                key("COMMENT_DELETE_ALL"),
                havePermission(true)));
        assertThat(permissionsClient.getPermissions(ImmutableMap.of("issueId", "10201")).body.permissions.get("COMMENT_DELETE_ALL"), allOf(
                id("36"),
                key("COMMENT_DELETE_ALL"),
                havePermission(false)));
    }

    @Test
    public void testIssueFiltering() {
        Response<PermissionsOuter> outer = permissionsClient.getPermissions();

        assertThat(outer.body.permissions.get(DELETE_ALL_COMMENTS.permissionKey()), allOf(
                id("36"),
                key(DELETE_ALL_COMMENTS.permissionKey()),
                name("Delete All Comments"),
                type("PROJECT"),
                description("Ability to delete all comments made on issues."),
                deprecatedKey(null),
                havePermission(true)));

        assertThat(permissionsClient.getPermissions(ImmutableMap.of("projectKey", "PROJECTA")).body.permissions.get(DELETE_ALL_COMMENTS.permissionKey()), allOf(
                id("36"),
                key(DELETE_ALL_COMMENTS.permissionKey()),
                havePermission(true)));
        assertThat(permissionsClient.getPermissions(ImmutableMap.of("issueKey", "PROJECTA-3")).body.permissions.get(DELETE_ALL_COMMENTS.permissionKey()), allOf(
                id("36"),
                key(DELETE_ALL_COMMENTS.permissionKey()),
                havePermission(true)));
        assertThat(permissionsClient.getPermissions(ImmutableMap.of("issueKey", "PROJECTA-4")).body.permissions.get(DELETE_ALL_COMMENTS.permissionKey()), allOf(
                id("36"),
                key(DELETE_ALL_COMMENTS.permissionKey()),
                havePermission(false)));
        assertThat(permissionsClient.getPermissions(ImmutableMap.of("issueId", "10200")).body.permissions.get(DELETE_ALL_COMMENTS.permissionKey()), allOf(
                id("36"),
                key(DELETE_ALL_COMMENTS.permissionKey()),
                havePermission(true)));
        assertThat(permissionsClient.getPermissions(ImmutableMap.of("issueId", "10201")).body.permissions.get(DELETE_ALL_COMMENTS.permissionKey()), allOf(
                id("36"),
                key(DELETE_ALL_COMMENTS.permissionKey()),
                havePermission(false)));
    }

    private class PermissionsClient extends RestApiClient<PermissionsClient> {
        protected PermissionsClient(JIRAEnvironmentData environmentData) {
            super(environmentData);
        }

        private Response<PermissionsOuter> getPermissions() {
            return toResponse(new Method() {
                @Override
                public ClientResponse call() {
                    return createResource().path("mypermissions").get(ClientResponse.class);
                }
            }, PermissionsOuter.class);
        }

        private Response<PermissionsOuter> getPermissions(final Map<String, String> params) {
            return toResponse(new Method() {
                @Override
                public ClientResponse call() {
                    WebResource path = createResource().path("mypermissions");
                    for (Map.Entry<String, String> entry : params.entrySet()) {
                        path = path.queryParam(entry.getKey(), entry.getValue());
                    }
                    return path.get(ClientResponse.class);
                }
            }, PermissionsOuter.class);
        }

        private Response<PermissionsOuter> getAllPermissions() {
            return toResponse(new Method() {
                @Override
                public ClientResponse call() {
                    return createResource().path("permissions").get(ClientResponse.class);
                }
            }, PermissionsOuter.class);
        }
    }

    private static class PermissionsOuter {
        @JsonProperty
        HashMap<String, UserPermissionJsonBean> permissions;
    }
}
