package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestXsrfGlobalPermissions extends BaseJiraFuncTest {

    @Inject
    private Form form;

    @Test
    public void testGlobalPermissionsAdministration() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck("AddGlobalPermission", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.GLOBAL_PERMISSIONS);
                        tester.setWorkingForm("jiraform");
                        tester.selectOption("globalPermType", "Browse Users");
                        tester.selectOption("groupName", "Anyone");

                    }
                }, new XsrfCheck.FormSubmission("Add")),
                new XsrfCheck("RemoveGlobalPermission", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.GLOBAL_PERMISSIONS);
                        tester.clickLink("del_USER_PICKER_");
                    }
                }, new XsrfCheck.FormSubmission("Delete"))
        ).run(getTester(), navigation, form);
    }
}
