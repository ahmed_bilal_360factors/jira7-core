package com.atlassian.jira.functest.framework.page;

import com.atlassian.jira.webtests.table.HtmlTable;

import java.util.List;

import static com.atlassian.collectors.CollectorsUtil.toImmutableListWithCapacity;

/**
 * @since v6.0
 */
public class ViewVotersPage extends AbstractWebTestPage {
    @Override
    public String baseUrl() {
        return "ViewVoters!default.jspa";
    }

    public List<String> getCurrentVoters() {
        final HtmlTable table = getTableWithId("voter-list");
        return table.getRows().stream()
                .filter(row -> row.getRowIndex() > 0)
                .map(row -> row.getCellAsText(0))
                .collect(toImmutableListWithCapacity(table.getRowCount() - 1));
    }
}
