package com.atlassian.jira.functest.framework.backdoor;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @since v7.0
 */
public class SingleProductLicenseDetailsViewTO extends SalLicenseControl.BaseLicenseDetailsTO {
    @JsonProperty
    public String productKey;
    @JsonProperty
    public boolean isUnlimitedNumberOfUsers;
    @JsonProperty
    public int numberOfUsers;
    @JsonProperty
    public String productDisplayName;
}
