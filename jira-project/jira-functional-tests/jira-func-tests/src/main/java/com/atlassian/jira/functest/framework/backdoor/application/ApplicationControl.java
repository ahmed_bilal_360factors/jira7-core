package com.atlassian.jira.functest.framework.backdoor.application;

import com.atlassian.fugue.Option;
import com.atlassian.jira.functest.framework.backdoor.BackdoorControl;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.sun.jersey.api.client.WebResource;

import java.util.List;

import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.CORE_KEY;

/**
 * This control deals with installed applications, not licensed applications. It's possible to have an application
 * installed but no license for that application, and it's also possible to have a license installed for an application
 * but not have that application installed. To deal with licensed applications use the production REST resources (for
 * example ApplicationRoleResource).
 *
 * @since v7.0
 */
public class ApplicationControl extends BackdoorControl<ApplicationControl> {
    public ApplicationControl(final JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    /**
     * @return all installed applications, whether they are licensed or not
     */
    public List<ApplicationBean> getApplications() {
        return createApplicationResource().get(ApplicationBean.LIST_TYPE);
    }

    /**
     * @param appKey the application to look up
     * @return the specified application if installed
     */
    public ApplicationBean getApplication(String appKey) {
        return createApplicationResource(appKey).get(ApplicationBean.class);
    }

    public String getCount(final String key, final Option<Integer> count) {
        final WebResource resource = createApplicationResource(key).path("count");
        if (count.isDefined()) {
            return resource.queryParam("count", String.valueOf(count.get())).get(String.class);
        } else {
            return resource.get(String.class);
        }
    }

    public ApplicationAccessStatus getApplicationAccess(String appKey, String user) {
        return ApplicationAccessStatus.valueOf(createApplicationResource(appKey)
                .path("access")
                .queryParam("userKey", user)
                .get(String.class));
    }

    public PluginApplicationBean getPlugin(String key) {
        return (PluginApplicationBean) getApplication(key);
    }

    public PlatformApplicationBean getCore() {
        return (PlatformApplicationBean) getApplication(CORE_KEY);
    }

    public void clearConfiguration(final String appKey) {
        createApplicationResource(appKey).path("clearConfiguration").put();
    }

    private WebResource createApplicationResource() {
        return createResource().path("application");
    }

    private WebResource createApplicationResource(final String appKey) {
        return createApplicationResource().path(appKey);
    }
}
