package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.Set;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.FIELDS, Category.CUSTOM_FIELDS})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestLabelSuggestions extends BaseJiraFuncTest {

    private static final String PROJECT_KEY = "HSP";
    private static final String UPPERCASE_LABEL = "Android";
    private static final String LOWERCASE_LABEL = "android";
    private static final String OTHER_LABEL = "other";
    private static final String LOWERCASE_PREFIX = "and";
    private static final String UPPERCASE_PREFIX = "And";
    private static final String EMPTY_SUGGESTIONS_RESPONSE = "\"suggestions\":[]";
    private static final String CUSTOM_LABEL_FIELD_NAME = "custom-label";
    private static final String CUSTOM_LABEL_FIELD_DESC = "Custom label field";
    private static final String DEFAULT_FIELD_SCREEN_NAME = "Default Screen";
    private static final String LABELS_TYPE_KEY = "com.atlassian.jira.plugin.system.customfieldtypes:labels";
    private static final String LABELS_SEARCHER_KEY = "com.atlassian.jira.plugin.system.customfieldtypes:labelsearcher";
    private static final String UPPERCASE_PREFIX_RESPONSE = "{\"label\":\"Android\",\"html\":\"<b>And</b>roid\"},{\"label\":\"android\",\"html\":\"<b>and</b>roid\"}";
    private static final String LOWERCASE_PREFIX_RESPONSE = "{\"label\":\"android\",\"html\":\"<b>and</b>roid\"},{\"label\":\"Android\",\"html\":\"<b>And</b>roid\"}";
    private static final String UPPERCASE_PREFIX_JQL_RESPONSE = "{\"value\":\"android\",\"displayName\":\"<b>and</b>roid\"},{\"value\":\"Android\",\"displayName\":\"<b>And</b>roid\"}";
    private static final String LOWERCASE_PREFIX_JQL_RESPONSE = "{\"value\":\"Android\",\"displayName\":\"<b>And</b>roid\"},{\"value\":\"android\",\"displayName\":\"<b>and</b>roid\"}";

    private IssueCreateResponse existingIssue;
    private IssueCreateResponse existingIssueWithCustomField;
    private Long customFieldId;

    @Before
    public void setUp() {
        existingIssue = createIssueWithLabels(ImmutableSet.of(UPPERCASE_LABEL, LOWERCASE_LABEL), "Base test issue");
        customFieldId = createCustomField();
    }

    private Long createCustomField() {
        final String customField = backdoor.customFields().createCustomField(CUSTOM_LABEL_FIELD_NAME, CUSTOM_LABEL_FIELD_DESC,
                LABELS_TYPE_KEY, LABELS_SEARCHER_KEY);

        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_LABEL_FIELD_NAME);

        return CustomFieldUtils.getCustomFieldId(customField);
    }

    private void createIssueWithCustomFields(Set<String> labelValues) {
        existingIssueWithCustomField = createIssueWithLabels(Collections.emptySet(), "Issue with custom fields");

        backdoor.issues().setIssueFields(
                existingIssueWithCustomField.key,
                new IssueFields().customField(customFieldId, labelValues)
        );
    }

    @Test
    public void testLabelSuggestionsCaseSensitivity() {
        final IssueCreateResponse newIssue = createIssueWithLabels(ImmutableSet.of(OTHER_LABEL), "Case sensitivity test");

        tester.gotoPage(createSuggestionQueryUrl(newIssue, LOWERCASE_LABEL));
        tester.assertTextPresent(createLabelAssertionString(UPPERCASE_LABEL));

        tester.gotoPage(createSuggestionQueryUrl(newIssue, UPPERCASE_LABEL));
        tester.assertTextPresent(createLabelAssertionString(LOWERCASE_LABEL));
    }

    @Test
    public void testSkippingExisitingLabels() {
        tester.gotoPage(createSuggestionQueryUrl(existingIssue, OTHER_LABEL));
        tester.assertTextPresent(EMPTY_SUGGESTIONS_RESPONSE);
    }

    @Test
    public void testSuggestionsSortOrder() {
        final IssueCreateResponse issue = createIssueWithLabels(ImmutableSet.of(OTHER_LABEL), "Sort order test");

        tester.gotoPage(createSuggestionQueryUrl(issue, UPPERCASE_PREFIX));
        tester.assertTextPresent(UPPERCASE_PREFIX_RESPONSE);

        tester.gotoPage(createSuggestionQueryUrl(issue, LOWERCASE_PREFIX));
        tester.assertTextPresent(LOWERCASE_PREFIX_RESPONSE);
    }

    @Test
    public void testCustomFieldSuggestions() {
        createIssueWithCustomFields(ImmutableSet.of(UPPERCASE_LABEL));
        tester.gotoPage(createCustomFieldSuggestionQueryUrl(existingIssueWithCustomField, LOWERCASE_LABEL));
        tester.assertTextPresent(createLabelAssertionString(UPPERCASE_LABEL));

        createIssueWithCustomFields(ImmutableSet.of(LOWERCASE_LABEL));
        tester.gotoPage(createCustomFieldSuggestionQueryUrl(existingIssueWithCustomField, UPPERCASE_LABEL));
        tester.assertTextPresent(createLabelAssertionString(LOWERCASE_LABEL));
    }

    @Test
    public void testCustomFieldSuggestionsSortOrder() {
        createIssueWithCustomFields(ImmutableSet.of(LOWERCASE_LABEL, UPPERCASE_LABEL));
        final IssueCreateResponse issue = createIssueWithLabels(Collections.emptySet(), "Another issue with custom fields");

        backdoor.issues().setIssueFields(
                issue.key,
                new IssueFields().customField(customFieldId, ImmutableSet.of(OTHER_LABEL))
        );

        tester.gotoPage(createCustomFieldSuggestionQueryUrl(issue, LOWERCASE_PREFIX));
        tester.assertTextPresent(LOWERCASE_PREFIX_RESPONSE);

        tester.gotoPage(createCustomFieldSuggestionQueryUrl(issue, UPPERCASE_PREFIX));
        tester.assertTextPresent(UPPERCASE_PREFIX_RESPONSE);
    }

    @Test
    public void testJqlLabelSuggestions() {
        tester.gotoPage(createJqlLabelsSuggestionUrl(LOWERCASE_PREFIX));
        tester.assertTextPresent(UPPERCASE_PREFIX_JQL_RESPONSE);

        tester.gotoPage(createJqlLabelsSuggestionUrl(UPPERCASE_PREFIX));
        tester.assertTextPresent(LOWERCASE_PREFIX_JQL_RESPONSE);
    }

    private IssueCreateResponse createIssueWithLabels(Set<String> labels, String summary) {
        final IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_KEY, summary);

        labels.forEach(label -> backdoor.issues().addLabel(issue.key, label));

        return issue;
    }

    private String createLabelAssertionString(final String uppercaseLabel) {
        return String.format("\"label\":\"%s\"", uppercaseLabel);
    }

    private String createSuggestionQueryUrl(final IssueCreateResponse issue, final String query) {
        return String.format("/rest/api/1.0/labels/%s/suggest?query=%s", issue.id, query);
    }

    private String createCustomFieldSuggestionQueryUrl(final IssueCreateResponse issue, final String query) {
        return String.format("/rest/api/1.0/labels/%s/suggest?customFieldId=10000&query=%s", issue.id, query);
    }

    private String createJqlLabelsSuggestionUrl(final String query) {
        return String.format("/rest/api/2/jql/autocompletedata/suggestions?fieldName=labels&fieldValue=%s", query);
    }

}
