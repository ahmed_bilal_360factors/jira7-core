package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.atlassian.jira.webtests.ztests.plugin.TestBundledPlugins;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * @since v4.4.3
 */
public class FuncTestSuitePlugins {
    public static List<Class<?>> bundledPluginsTests() {
        return ImmutableList.<Class<?>>builder()
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.ao", true))
                .add(TestBundledPlugins.class)
                .build();
    }
}