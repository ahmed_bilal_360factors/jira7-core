package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.bundledplugins2.gadget.TestRoadMapGadget;
import com.atlassian.jira.webtests.ztests.cache.VCacheSmokeTest;
import com.atlassian.jira.webtests.ztests.imports.project.TestProjectImportRespectsJiraCharacterLimit;
import com.atlassian.jira.webtests.ztests.issue.TestPermissionConditions;
import com.atlassian.jira.webtests.ztests.misc.TestAddPermissionNonStandardPermissionTypes;
import com.atlassian.jira.webtests.ztests.misc.TestCategorySelection;
import com.atlassian.jira.webtests.ztests.misc.TestWebResourceLinks;
import com.atlassian.jira.webtests.ztests.redirects.TestMyJiraHomeDashboradRedirect;
import com.atlassian.jira.webtests.ztests.servermetrics.TestServerMetricsEvents;
import com.atlassian.jira.webtests.ztests.statistics.TestComponentStatisticsManager;
import com.atlassian.jira.webtests.ztests.statistics.TestProjectStatisticsManager;
import com.atlassian.jira.webtests.ztests.user.TestViewUserHoverAction;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * These are tests that has not been added to any suites. Please move them where they belong.
 *
 * @since v7.2
 */
public class FuncTestSuiteForgotten {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestAddPermissionNonStandardPermissionTypes.class)
                .add(TestComponentStatisticsManager.class)
                .add(TestPermissionConditions.class)
                .add(TestProjectImportRespectsJiraCharacterLimit.class)
                .add(TestProjectStatisticsManager.class)
                .add(TestViewUserHoverAction.class)
                .add(TestWebResourceLinks.class)
                .add(VCacheSmokeTest.class)
                .add(TestCategorySelection.class)
                .add(TestServerMetricsEvents.class)
                .add(TestRoadMapGadget.class)
                .add(TestMyJiraHomeDashboradRedirect.class)
                .build();
    }
}

