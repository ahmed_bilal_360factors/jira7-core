package com.atlassian.jira.webtests.ztests.projectconfig.framework;

import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * @since v5.2
 */
public class WorkflowSchemeResource extends RestApiClient<WorkflowSchemeResource> {
    private final String root;

    public WorkflowSchemeResource(JIRAEnvironmentData environmentData) {
        super(environmentData);
        root = environmentData.getBaseUrl().toExternalForm();
    }

    @Override
    protected WebResource createResource() {
        return resourceRoot(root).path("rest").path("projectconfig").path("latest").path("workflowscheme");
    }

    public SimpleWorkflowScheme getWorkflowScheme(String projectKey) {
        return createResource().path(projectKey).get(SimpleWorkflowScheme.class);
    }

    public SimpleWorkflowScheme getOriginalWorkflowScheme(String projectKey) {
        return createResource().path(projectKey).queryParam("original", Boolean.TRUE.toString()).get(SimpleWorkflowScheme.class);
    }

    public SimpleWorkflowScheme createDraftWorkflowScheme(String projectKey) {
        return createResource().path(projectKey).post(SimpleWorkflowScheme.class);
    }

    public SimpleWorkflowScheme discardDraftWorkflowScheme(String projectKey) {
        return createResource().path(projectKey).delete(SimpleWorkflowScheme.class);
    }

    public Response createDraftWorkflowSchemeResponse(final String projectKey) {
        return toResponse(new Method() {
            @Override
            public ClientResponse call() {
                return createResource().path(projectKey).post(ClientResponse.class);
            }
        });
    }

    public Response getWorkflowResponse(final String projectKey) {
        return toResponse(new Method() {
            @Override
            public ClientResponse call() {
                return createResource().path(projectKey).get(ClientResponse.class);
            }
        });
    }

    public Response discardDraftWorkflowSchemeResponse(final String projectKey) {
        return toResponse(new Method() {
            @Override
            public ClientResponse call() {
                return createResource().path(projectKey).delete(ClientResponse.class);
            }
        });
    }
}
