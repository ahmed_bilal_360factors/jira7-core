package com.atlassian.jira.functest.matcher;

import com.atlassian.jira.rest.v2.permission.PermissionJsonBean;
import com.atlassian.jira.rest.v2.permission.UserPermissionJsonBean;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 * @since v7.0
 */
public class PermissionMatcher {

    private static class HasKeyMatcher extends TypeSafeMatcher<PermissionJsonBean> {

        private final String key;

        public HasKeyMatcher(final String key) {
            this.key = key;
        }

        @Override
        protected boolean matchesSafely(final PermissionJsonBean permissionJsonBean) {
            return key.equals(permissionJsonBean.key());
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("expected key value is: ").appendValue(key);
        }
    }

    private static class HasNameMatcher extends TypeSafeMatcher<PermissionJsonBean> {

        private final String name;

        public HasNameMatcher(final String name) {
            this.name = name;
        }

        @Override
        protected boolean matchesSafely(final PermissionJsonBean permissionJsonBean) {
            return name.equals(permissionJsonBean.name);
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("expected name value is: ").appendValue(name);
        }

    }

    private static class HasTypeMatcher extends TypeSafeMatcher<PermissionJsonBean> {

        private final String type;

        public HasTypeMatcher(final String type) {
            this.type = type;
        }

        @Override
        protected boolean matchesSafely(final PermissionJsonBean permissionJsonBean) {
            return type.equals("" + permissionJsonBean.type);
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("expected type value is: ").appendValue(type);
        }

    }

    private static class HasDescriptionMatcher extends TypeSafeMatcher<PermissionJsonBean> {

        private final String description;

        public HasDescriptionMatcher(final String description) {
            this.description = description;
        }

        @Override
        protected boolean matchesSafely(final PermissionJsonBean permissionJsonBean) {
            return description.equals(permissionJsonBean.description);
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("expected description value is: ").appendValue(this.description);
        }
    }

    private static class NotEmptyDescriptionMatcher extends TypeSafeMatcher<PermissionJsonBean> {

        @Override
        protected boolean matchesSafely(final PermissionJsonBean permissionJsonBean) {
            return StringUtils.isNotEmpty(permissionJsonBean.description);
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("description should not be empty");
        }
    }

    private static class NoDeprecatedKeyMatcher extends TypeSafeMatcher<UserPermissionJsonBean> {

        private final Boolean value;

        public NoDeprecatedKeyMatcher(final Boolean value) {
            this.value = value;
        }

        @Override
        protected boolean matchesSafely(final UserPermissionJsonBean item) {
            return value == null ? item.deprecatedKey == null : value.equals(item.deprecatedKey);
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("has not deprecatedKey present");
        }
    }

    private static class HasIdMatcher extends TypeSafeMatcher<PermissionJsonBean> {

        private final String id;

        private HasIdMatcher(final String id) {
            this.id = id;
        }

        @Override
        protected boolean matchesSafely(final PermissionJsonBean item) {
            return id.equals(item.id());
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("has id with value: ").appendValue(id);
        }
    }

    private static class HavePermissionMatcher extends TypeSafeMatcher<UserPermissionJsonBean> {
        private final boolean havePermission;

        private HavePermissionMatcher(final boolean havePermission) {
            this.havePermission = havePermission;
        }


        @Override
        protected boolean matchesSafely(final UserPermissionJsonBean item) {
            return havePermission == item.havePermission;
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("havePermission with value: ").appendValue(havePermission);
        }
    }

    public static Matcher<PermissionJsonBean> key(String key) {
        return new HasKeyMatcher(key);
    }

    public static Matcher<PermissionJsonBean> name(String name) {
        return new HasNameMatcher(name);
    }

    public static Matcher<PermissionJsonBean> type(String type) {
        return new HasTypeMatcher(type);
    }

    public static Matcher<PermissionJsonBean> description(String description) {
        return new HasDescriptionMatcher(description);
    }

    public static Matcher<PermissionJsonBean> hasNotEmptyDescription() {
        return new NotEmptyDescriptionMatcher();
    }

    public static Matcher<PermissionJsonBean> id(String id) {
        return new HasIdMatcher(id);
    }

    public static Matcher<UserPermissionJsonBean> deprecatedKey(Boolean value) {
        return new NoDeprecatedKeyMatcher(value);
    }

    public static Matcher<UserPermissionJsonBean> havePermission(boolean value) {
        return new HavePermissionMatcher(value);
    }
}
