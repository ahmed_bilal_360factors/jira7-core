package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.Set;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.REST;
import static com.google.common.collect.Iterables.filter;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@WebTest({FUNC_TEST, REST})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueResourceNames extends BaseJiraFuncTest {
    private IssueClient issueClient;

    @Inject
    private Administration administration;

    @Test
    public void testIssueRepresentationShouldContainExpandableNamesField() throws Exception {
        administration.restoreData("TestIssueResourceCustomFields.xml");

        Issue minimal = issueClient.get("HSP-1");
        assertThat("names should not be expanded by default", minimal.names, equalTo(null));
        assertThat(minimal.expand, containsString(Issue.Expand.names.name()));

        Issue expanded = issueClient.get("HSP-1", Issue.Expand.names);
        Set<String> fields = Sets.newHashSet(filter(expanded.fields.idSet(), new NonNullFields(expanded)));
        Set<String> names = expanded.names.keySet();
        assertTrue("Found in 'fields' but not in 'names': " + Sets.difference(fields, names), Sets.difference(fields, names).isEmpty());
    }

    @Before
    public void setUpTest() {
        issueClient = new IssueClient(getEnvironmentData());
    }

    private static class NonNullFields implements Predicate<String> {
        private final Issue expanded;

        public NonNullFields(Issue expanded) {
            this.expanded = expanded;
        }

        @Override
        public boolean apply(@Nullable String fieldId) {
            return expanded.fields.get(fieldId) != null;
        }
    }
}
