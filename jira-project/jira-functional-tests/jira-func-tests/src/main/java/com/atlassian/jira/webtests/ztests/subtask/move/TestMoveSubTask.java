package com.atlassian.jira.webtests.ztests.subtask.move;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.webtests.ztests.workflow.ExpectedChangeHistoryItem;
import com.atlassian.jira.webtests.ztests.workflow.ExpectedChangeHistoryRecord;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SUB_TASK_DEFAULT_TYPE;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.permission.ProjectPermissions.MOVE_ISSUES;
import static com.atlassian.jira.webtests.Groups.DEVELOPERS;

@WebTest({Category.FUNC_TEST, Category.SUB_TASKS})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestMoveSubTask extends BaseJiraFuncTest {
    private static final String NEW_SUBTASK_TYPE = "newSubTaskType";
    private static final String STEP_ONE_TITLE = "Choose Operation";
    private static final String STEP_TWO_TITLE = "Operation Details";
    private static final String STEP_THREE_TITLE = "Update Fields";
    private static final String STEP_FOUR_TITLE = "Confirmation";
    private static final String SUBTASK_SUMMARY = "this subtask is moved";
    private String parentIssue1;
    private String parentIssue2;
    private String parentIssue3;
    private String subtask;
    private String subtask2;
    private String subtask3;
    @Inject
    private Form form;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
        backdoor.subtask().enable();
    }

    @Test
    public void testMoveSubTask() {
//        subTaskMoveSubTask();
//        subTaskMoveIssueWithSubTask();

        parentIssue1 = navigation.issue().createIssue(PROJECT_HOMOSAP, "Bug", "control parent issue in project homosap");
        subtask = navigation.issue().createSubTask(parentIssue1, SUB_TASK_DEFAULT_TYPE, SUBTASK_SUMMARY, "description: Test subject");
        _testMoveSubTaskOperationListVisibility();
        _testSidePanelLinksForMoveSubTaskType();
//        _testWizardCancelForMoveSubTaskType(); //todo - test cant find any button with value: Cancel
        _testSuccessfulMoveOfSubTaskType();

        parentIssue2 = navigation.issue().createIssue(PROJECT_HOMOSAP, "Bug", "control parent issue 2 in project homosap");
        parentIssue3 = navigation.issue().createIssue(PROJECT_MONKEY, "Bug", "control parent issue 3 in project monkey");
        subtask2 = navigation.issue().createSubTask(parentIssue1, SUB_TASK_DEFAULT_TYPE, "control subtask of parent 1 (homosap)", "description");
        subtask3 = navigation.issue().createSubTask(parentIssue3, SUB_TASK_DEFAULT_TYPE, "control subtask of parent 3 (monkey)", "description");
        _testSidePanelLinksForMoveSubTaskParent();
//        _testWizardCancelForMoveSubTaskParent();//todo - test cant find any button with value: Cancel
        _testValidateMoveSubTaskParent();
        _testSuccessfulMoveOfSubTaskParent();
        administration.subtasks().deleteSubTaskType(NEW_SUBTASK_TYPE);
    }

    private void _testMoveSubTaskOperationListVisibility() {
        //test no permissions
        logger.log("Check move_issue link is hidden with no permission");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, MOVE_ISSUES, DEVELOPERS);
        navigation.issue().gotoIssue(subtask);
        tester.assertLinkNotPresent("move-issue");
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, MOVE_ISSUES, DEVELOPERS);

        logger.log("Check move subtask type operation cannot be perfomed");
        navigation.issue().gotoIssue(subtask);
        tester.clickLink("move-issue");
        //test operations canPerform()
        //initially theres is only one subtask - hence only move subtask type should be disabled
        tester.assertRadioOptionNotPresent("operation", "move.subtask.type.operation.name");
        tester.assertRadioOptionPresent("operation", "move.subtask.parent.operation.name");

        logger.log("Check move subtask type operation can be performed");
        addNewSubTaskType();
        navigation.issue().gotoIssue(subtask);
        tester.clickLink("move-issue");
        tester.assertRadioOptionPresent("operation", "move.subtask.type.operation.name");
        tester.assertRadioOptionPresent("operation", "move.subtask.parent.operation.name");
    }

    private void _testSuccessfulMoveOfSubTaskType() {
        //make a successful change of issue type
        addNewSubTaskType();
        gotoStep1ofMoveSubTaskType();
        tester.assertFormElementPresent("issuetype");
        navigation.issue().selectIssueType(NEW_SUBTASK_TYPE, "issuetype");
        tester.submit("Next >>");
        tester.submit("Next >>");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Type", SUB_TASK_DEFAULT_TYPE);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), SUB_TASK_DEFAULT_TYPE, NEW_SUBTASK_TYPE);
        tester.submit("Move");
        assertions.assertLastChangeHistoryRecords(subtask, new ExpectedChangeHistoryRecord(new ExpectedChangeHistoryItem("Issue Type", SUB_TASK_DEFAULT_TYPE, NEW_SUBTASK_TYPE)));
    }

    private void addNewSubTaskType() {
        administration.subtasks().addSubTaskType(NEW_SUBTASK_TYPE, "temporary subtask type");
    }

    private void _testSidePanelLinksForMoveSubTaskType() {
        //in step 2, go back to step 1
        gotoStep2ofMoveSubTaskType();
        tester.assertTextPresent("Step 1 of 4");

        //in step 3, go back to step 1
        gotoStep3ofMoveSubTaskType();
        tester.clickLinkWithText(STEP_ONE_TITLE);
        tester.assertTextPresent("Step 1 of 4");

        //in step 3, go back to step 2
        gotoStep3ofMoveSubTaskType();
        tester.clickLinkWithText(STEP_TWO_TITLE);
        tester.assertTextPresent("Step 2 of 4");

        //in step 4, go back to step 1
        gotoStep4ofMoveSubTaskType();
        tester.clickLinkWithText(STEP_ONE_TITLE);
        tester.assertTextPresent("Step 1 of 4");

        //in step 4, go back to step 2
        gotoStep4ofMoveSubTaskType();
        tester.clickLinkWithText(STEP_TWO_TITLE);
        tester.assertTextPresent("Step 2 of 4");

        //in step 4, go back to step 3
        gotoStep4ofMoveSubTaskType();
        tester.clickLinkWithText(STEP_THREE_TITLE);
        tester.assertTextPresent("Step 3 of 4");
    }

    /**
     * this is a failing test, since it cannot click the 'Cancel' button
     */
    private void _testWizardCancelForMoveSubTaskType() {
        gotoMoveSubTaskChooseOperation();
        checkCancelRedirectsViewIssue();

        gotoStep1ofMoveSubTaskType();
        checkCancelRedirectsViewIssue();

        gotoStep2ofMoveSubTaskType();
        checkCancelRedirectsViewIssue();

        gotoStep3ofMoveSubTaskType();
        checkCancelRedirectsViewIssue();

        gotoStep4ofMoveSubTaskType();
        checkCancelRedirectsViewIssue();
    }

    private void checkCancelRedirectsViewIssue() {
        tester.assertFormElementPresent("Cancel");
        form.clickAnyButtonWithValue("Cancel");
        tester.assertTextNotPresent("Step ");
        tester.assertLinkPresentWithText(subtask);
        tester.assertLinkPresentWithText("move-issue");
    }

    private void gotoMoveSubTaskChooseOperation() {
        navigation.issue().gotoIssue(subtask);
        tester.clickLink("move-issue");
        tester.assertTextPresent("Step 1 of 4");
    }

    private void gotoStep1ofMoveSubTaskType() {
        gotoMoveSubTaskChooseOperation();
        tester.checkCheckbox("operation", "move.subtask.type.operation.name");
        tester.submit("Next >>");
    }

    private void gotoStep2ofMoveSubTaskType() {
        gotoStep1ofMoveSubTaskType();
        tester.assertLinkPresentWithText(STEP_ONE_TITLE);
        tester.assertLinkNotPresentWithText(STEP_TWO_TITLE);
        tester.assertLinkNotPresentWithText(STEP_THREE_TITLE);
        tester.assertLinkNotPresentWithText(STEP_FOUR_TITLE);
        tester.clickLinkWithText(STEP_ONE_TITLE);
    }

    private void gotoStep3ofMoveSubTaskType() {
        gotoStep1ofMoveSubTaskType();
        tester.submit("Next >>");
        tester.assertLinkPresentWithText(STEP_ONE_TITLE);
        tester.assertLinkPresentWithText(STEP_TWO_TITLE);
        tester.assertLinkNotPresentWithText(STEP_THREE_TITLE);
        tester.assertLinkNotPresentWithText(STEP_FOUR_TITLE);
    }

    private void gotoStep4ofMoveSubTaskType() {
        gotoStep1ofMoveSubTaskType();
        tester.submit("Next >>");
        tester.submit("Next >>");
        tester.assertLinkPresentWithText(STEP_ONE_TITLE);
        tester.assertLinkPresentWithText(STEP_TWO_TITLE);
        tester.assertLinkPresentWithText(STEP_THREE_TITLE);
        tester.assertLinkNotPresentWithText(STEP_FOUR_TITLE);
    }

    private void gotoStep4ofMoveSubTaskParent() {
        gotoMoveSubTaskChooseOperation();
        tester.checkCheckbox("operation", "move.subtask.parent.operation.name");
        tester.submit("Next >>");
    }

    private void _testValidateMoveSubTaskParent() {
        gotoStep4ofMoveSubTaskParent();

        tester.setFormElement("parentIssue", ""); //null input
        tester.submit("Change Parent");
        tester.assertTextPresent("Parent Issue is a required field");

        tester.setFormElement("parentIssue", "dontExist"); //no such issue
        tester.submit("Change Parent");
        tester.assertTextPresent("The issue key &quot;dontExist&quot; does not exist");

        tester.setFormElement("parentIssue", subtask); //itself
        tester.submit("Change Parent");
        tester.assertTextPresent("Issue cannot be its own parent");

        tester.setFormElement("parentIssue", parentIssue1); //current parent
        tester.submit("Change Parent");
        tester.assertTextPresent("Already linked to this parent issue");

        tester.setFormElement("parentIssue", parentIssue3); //parent, different project
        tester.submit("Change Parent");
        tester.assertTextPresent("Cannot link Parent issue from a different project");

        tester.setFormElement("parentIssue", subtask2); //subtask, same project
        tester.submit("Change Parent");
        tester.assertTextPresent("Subtasks cannot be a parent issue");

        tester.setFormElement("parentIssue", subtask3); //subtask, different project
        tester.submit("Change Parent");
        tester.assertTextPresent("Subtasks cannot be a parent issue");
    }

    /**
     * this is a failing test, since it cannot click the 'Cancel' button
     */
    private void _testWizardCancelForMoveSubTaskParent() {
        gotoMoveSubTaskChooseOperation();
        checkCancelRedirectsViewIssue();

        gotoStep4ofMoveSubTaskParent();
        checkCancelRedirectsViewIssue();
    }

    private void _testSuccessfulMoveOfSubTaskParent() {
        gotoStep4ofMoveSubTaskParent();
        tester.setFormElement("parentIssue", parentIssue2);
        tester.submit("Change Parent");
        tester.assertTextNotPresent("Step 4 of 4");

        assertions.assertLastChangeHistoryRecords(subtask, new ExpectedChangeHistoryRecord(new ExpectedChangeHistoryItem("Parent Issue", parentIssue1, parentIssue2)));
    }

    private void _testSidePanelLinksForMoveSubTaskParent() {
        //in step 4, go back to step 1
        gotoStep4ofMoveSubTaskParent();
        tester.assertLinkPresentWithText(STEP_ONE_TITLE);
        tester.assertLinkNotPresentWithText(STEP_TWO_TITLE);
        tester.assertLinkNotPresentWithText(STEP_THREE_TITLE);
        tester.assertLinkNotPresentWithText(STEP_FOUR_TITLE);
        tester.clickLinkWithText(STEP_ONE_TITLE);
        tester.assertTextPresent("Step 1 of 4");
        tester.assertLinkNotPresentWithText(STEP_ONE_TITLE);
        tester.assertLinkNotPresentWithText(STEP_TWO_TITLE);
        tester.assertLinkNotPresentWithText(STEP_THREE_TITLE);
        tester.assertLinkNotPresentWithText(STEP_FOUR_TITLE);
    }

    /**
     * Checks that links are maintained between subtasks after moving to a new
     * parent.
     */
    @Test
    public void testMoveParentSubTaskLinks() {

        String origParentName = "First Parent";
        String firstParent = navigation.issue().createIssue(PROJECT_MONKEY, "Bug", origParentName);
        String newParent = navigation.issue().createIssue(PROJECT_MONKEY, "Bug", "Second Parent");
        String childKey = navigation.issue().createSubTask(firstParent, "Sub-task", "subtask originally on First Parent", "sub bug");
        String linkageName = "linkoid";
        String linkLabel = "is linked to";
        backdoor.issueLinking().createIssueLinkType(linkageName, linkLabel, linkLabel);
        navigation.issue().linkIssueWithComment(childKey, linkLabel, firstParent, null, null);
        // this appears necessary because linkIssue turns the bugger off !?
        administration.issueLinking().enable();

        navigation.issue().gotoIssue(childKey);
        tester.clickLink("move-issue");
        tester.checkCheckbox("operation", "move.subtask.parent.operation.name");
        tester.submit("Next >>");
        tester.setFormElement("parentIssue", newParent);
        tester.submit("Change Parent");

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{linkLabel, firstParent, origParentName});
    }
}
