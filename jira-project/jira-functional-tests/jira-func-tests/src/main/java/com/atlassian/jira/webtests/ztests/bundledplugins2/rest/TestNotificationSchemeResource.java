package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.matcher.PageBeanMatcher;
import com.atlassian.jira.rest.api.notification.NotificationSchemeExpandParam;
import com.atlassian.jira.testkit.client.NotificationSchemesControl;
import com.atlassian.jira.testkit.client.restclient.NotificationBean;
import com.atlassian.jira.testkit.client.restclient.NotificationSchemeBean;
import com.atlassian.jira.testkit.client.restclient.NotificationSchemeClient;
import com.atlassian.jira.testkit.client.restclient.NotificationSchemeEventBean;
import com.atlassian.jira.testkit.client.restclient.NotificationSchemePageBean;
import com.atlassian.jira.testkit.client.restclient.PageBean;
import com.atlassian.jira.testkit.client.restclient.ProjectRole;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.util.PropertyAssertions;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nullable;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static java.util.stream.IntStream.range;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @since 7.0
 */
@WebTest({Category.FUNC_TEST, Category.SCHEMES, Category.REST})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestNotificationSchemeResource extends BaseJiraFuncTest {
    public static final String SCHEME_NAME = "New notification scheme";
    private NotificationSchemeClient notificationSchemeClient;
    private NotificationSchemesControl notificationSchemesControl;

    @Before
    public void setUpTest() {
        backdoor.restoreBlankInstance();
        this.notificationSchemeClient = new NotificationSchemeClient(getEnvironmentData());
        this.notificationSchemesControl = new NotificationSchemesControl(environmentData);
    }

    @Test
    public void testGettingNotificationSchemeWithProjectRole() {
        Long newSchemeId = createNotificationScheme();
        ProjectRole projectRole = backdoor.projectRole().get("MKY", "Users");

        notificationSchemesControl.addProjectRoleNotification(newSchemeId, EventType.ISSUE_CREATED_ID, projectRole.id);

        NotificationSchemeBean notificationScheme = notificationSchemeClient.getNotificationScheme(newSchemeId, "projectRole");

        assertThat(notificationScheme, matchesScheme(is(SCHEME_NAME), hasEvent(EventType.ISSUE_CREATED_ID, projectRoleMatcher("Users"))));
    }

    @Test
    public void testGettingNotificationSchemeWithDefaultIssueUpdatedNotification() {
        Long schemeId = createNotificationScheme();
        NotificationSchemeBean notificationScheme = notificationSchemeClient.getNotificationScheme(schemeId, "notificationSchemeEvents");

        assertThat(notificationScheme, matchesScheme(is(SCHEME_NAME), Matchers.allOf(
                hasEvent(EventType.ISSUE_UPDATED_ID, simpleNotificationMatcher("CurrentAssignee")),
                hasEvent(EventType.ISSUE_UPDATED_ID, simpleNotificationMatcher("Reporter")),
                hasEvent(EventType.ISSUE_UPDATED_ID, simpleNotificationMatcher("AllWatchers"))
        )));
    }

    @Test
    public void testGettingNotificationSchemeWithUser() {
        Long eventId = EventType.ISSUE_DELETED_ID;
        String username = "username";
        Long newSchemeId = createNotificationScheme();

        backdoor.usersAndGroups().addUser(username);
        notificationSchemesControl.addUserNotification(newSchemeId, eventId, username);

        NotificationSchemeBean notificationScheme = notificationSchemeClient.getNotificationScheme(newSchemeId, "user");

        assertThat(notificationScheme, matchesScheme(is(SCHEME_NAME), hasEvent(eventId, userMarcher(username))));
    }

    @Test
    public void testGettingNotificationSchemeAndExpandingAll() {
        Long newSchemeId = createNotificationScheme();

        backdoor.usersAndGroups().addUser("user");
        notificationSchemesControl.addUserNotification(newSchemeId, EventType.ISSUE_DELETED_ID, "user");

        ProjectRole projectRole = backdoor.projectRole().get("MKY", "Users");
        notificationSchemesControl.addProjectRoleNotification(newSchemeId, EventType.ISSUE_DELETED_ID, projectRole.id);

        NotificationSchemeBean notificationScheme = notificationSchemeClient.getNotificationScheme(newSchemeId, "all");
        assertThat(notificationScheme, matchesScheme(is(SCHEME_NAME), hasEvent(EventType.ISSUE_DELETED_ID, userMarcher("user"))));
        assertThat(notificationScheme, matchesScheme(is(SCHEME_NAME), hasEvent(EventType.ISSUE_DELETED_ID, projectRoleMatcher("Users"))));
    }

    @Test
    public void testProvidingInvalidExpandParameter() {
        PropertyAssertions.assertUniformInterfaceException(() -> {
            notificationSchemeClient.getNotificationScheme(0L, "user, zuo");
            return null;
        }, Response.Status.BAD_REQUEST, "Unrecognized expand parameter 'zuo', supported parameters are: " + Arrays.toString(NotificationSchemeExpandParam.values()));
    }

    @Test
    public void testGettingNotificationSchemeWithGroup() {
        String groupname = "groupname";
        Long eventId = EventType.ISSUE_DELETED_ID;
        Long newSchemeId = createNotificationScheme();

        backdoor.usersAndGroups().addGroup(groupname);
        notificationSchemesControl.addGroupNotification(newSchemeId, eventId, groupname);

        NotificationSchemeBean notificationScheme = notificationSchemeClient.getNotificationScheme(newSchemeId, "group");

        assertThat(notificationScheme, matchesScheme(is(SCHEME_NAME), hasEvent(eventId, groupMatcher(groupname))));
    }

    @Test
    public void testGettingNotificationSchemeWithEmailAddress() {
        String emailAddress = "emailAddress";
        Long eventId = EventType.ISSUE_DELETED_ID;
        Long newSchemeId = createNotificationScheme();

        notificationSchemesControl.addEmailNotification(newSchemeId, eventId, emailAddress);

        NotificationSchemeBean notificationScheme = notificationSchemeClient.getNotificationScheme(newSchemeId, "notificationSchemeEvents");

        assertThat(notificationScheme, matchesScheme(is(SCHEME_NAME), hasEvent(eventId, emailAddressMatcher(emailAddress))));
    }


    @Test
    public void testGettingNotificationSchemeWithoutPermissions() {
        Long newSchemeId = createNotificationScheme();
        PropertyAssertions.assertUniformInterfaceException(() -> {
            notificationSchemeClient.loginAs("fred").getNotificationScheme(newSchemeId, "notificationSchemeEvents");
            return null;
        }, Response.Status.NOT_FOUND);
    }

    @Test
    public void testGettingNotificationSchemeAnonymously() {
        Long newSchemeId = createNotificationScheme();
        PropertyAssertions.assertUniformInterfaceException(() -> {
            notificationSchemeClient.anonymous().getNotificationScheme(newSchemeId, null);
            return null;
        }, Response.Status.UNAUTHORIZED);
    }

    @Test
    public void testGettingNotificationSchemeWhichDoesntExist() {
        PropertyAssertions.assertUniformInterfaceException(() -> {
            notificationSchemeClient.getNotificationScheme(-1l, null);
            return null;
        }, Response.Status.NOT_FOUND);
    }

    @Test
    public void testGettingNotificationSchemesPages() {
        int maxSize = 20;
        long total = 100l;

        List<String> allSchemeNames = createNotificationSchemes(range(0, (int) total), " scheme");
        List<Matcher<? super NotificationSchemeBean>> expectedFirstPage = allSchemeNames.stream()
                .limit(maxSize)
                .map(TestNotificationSchemeResource::schemeNameMatcher)
                .collect(Collectors.toList());

        PageBean<NotificationSchemeBean> notificationSchemes = notificationSchemeClient.getNotificationSchemes(0, maxSize, null);

        assertThat(notificationSchemes, PageBeanMatcher.matcher(is(0l), is(total), is(maxSize), contains(expectedFirstPage)));

        int secondPageMaxSize = 50;
        PageBean<NotificationSchemeBean> secondPage = notificationSchemeClient.getNotificationSchemes(maxSize, secondPageMaxSize, null);

        List<Matcher<? super NotificationSchemeBean>> expectedSecondPage = allSchemeNames.stream()
                .skip(maxSize)
                .limit(secondPageMaxSize)
                .map(TestNotificationSchemeResource::schemeNameMatcher)
                .collect(Collectors.toList());

        assertThat(secondPage, PageBeanMatcher.matcher(is((long) maxSize), is(total), is(secondPageMaxSize), contains(expectedSecondPage)));
    }

    @Test
    public void testGettingTooManyNotificationSchemes() {
        int total = 10;
        List<String> schemeNames = createNotificationSchemes(range(0, total), " scheme");

        PageBean<NotificationSchemeBean> notificationSchemes = notificationSchemeClient.getNotificationSchemes(5, total, null);

        List<Matcher<? super NotificationSchemeBean>> expectedPageContent = schemeNames.stream()
                .skip(5)
                .limit(total)
                .map(TestNotificationSchemeResource::schemeNameMatcher)
                .collect(Collectors.toList());

        assertThat(notificationSchemes, PageBeanMatcher.matcher(is(5l), is(10l), is(5), contains(expectedPageContent)));
    }

    @Test
    public void testGettingEmptyNotificationSchemesPage() {
        PageBean<NotificationSchemeBean> notificationSchemes = notificationSchemeClient.getNotificationSchemes(10, 10, null);

        assertThat(notificationSchemes, PageBeanMatcher.matcher(is(10l), is(1l), is(0), Matchers.<NotificationSchemeBean>emptyIterable()));
    }

    @Test
    public void testGettingNotificationSchemesPageRespectMaximum() {
        List<String> notificationSchemes = createNotificationSchemes(range(0, 100), "scheme");

        NotificationSchemePageBean page = notificationSchemeClient.getNotificationSchemes(0, 100, null);

        assertThat(page, PageBeanMatcher.matcher(is(0l), is(100l), is(50), Matchers.not(Matchers.emptyIterable())));
    }

    private List<String> createNotificationSchemes(final IntStream range, final String nameSuffix) {
        List<String> names = range.mapToObj(index -> index + nameSuffix).collect(Collectors.toList());
        names.forEach(this::createNotificationScheme);
        notificationSchemesControl.deleteScheme(10000l);

        return names.stream().sorted(new Comparator<String>() {
            @Override
            public int compare(final String o1, final String o2) {
                return o1.compareToIgnoreCase(o2);
            }
        }).collect(Collectors.toList());
    }

    private Long createNotificationScheme() {
        return createNotificationScheme(SCHEME_NAME);
    }

    private Long createNotificationScheme(String name) {
        return notificationSchemesControl.copyDefaultScheme(name);
    }

    private static Matcher<NotificationBean> simpleNotificationMatcher(final String notificationTypeName) {
        return Matchers.hasProperty("notificationType", Matchers.is(notificationTypeName));
    }

    private static Matcher<NotificationSchemeBean> schemeNameMatcher(final String name) {
        return Matchers.hasProperty("name", Matchers.is(name));
    }

    private static Matcher<NotificationBean> userMarcher(final String username) {
        final Matcher<NotificationBean> userMatcher = Matchers.<NotificationBean>allOf(
                Matchers.hasProperty("parameter"),
                Matchers.hasProperty("user",
                        Matchers.allOf(
                                Matchers.hasProperty("name", Matchers.is(username)),
                                Matchers.hasProperty("key"),
                                Matchers.hasProperty("self")
                        )
                )
        );
        return Matchers.both(simpleNotificationMatcher("User")).and(userMatcher);
    }

    private static Matcher<NotificationBean> groupMatcher(final String groupname) {
        final Matcher<NotificationBean> groupMatcher = Matchers.<NotificationBean>allOf(
                Matchers.hasProperty("parameter", Matchers.is(groupname)),
                Matchers.hasProperty("group",
                        Matchers.allOf(
                                Matchers.hasProperty("name", Matchers.is(groupname)),
                                Matchers.hasProperty("self")
                        )
                )
        );
        return Matchers.both(simpleNotificationMatcher("Group")).and(groupMatcher);
    }

    private static Matcher<NotificationBean> emailAddressMatcher(final String emailAddress) {
        final Matcher<NotificationBean> emailMatcher = Matchers.<NotificationBean>allOf(
                Matchers.hasProperty("emailAddress", Matchers.is(emailAddress)));
        return Matchers.both(simpleNotificationMatcher("EmailAddress")).and(emailMatcher);
    }

    private static Matcher<NotificationBean> projectRoleMatcher(final String projectRoleName) {
        final Matcher<NotificationBean> projectRoleMatcher = Matchers.<NotificationBean>allOf(
                Matchers.hasProperty("parameter"),
                Matchers.hasProperty("projectRole",
                        Matchers.allOf(Matchers.hasProperty("name", Matchers.is(projectRoleName)),
                                Matchers.hasProperty("self"),
                                Matchers.hasProperty("id"),
                                Matchers.hasProperty("description")
                        )
                )
        );
        return Matchers.<NotificationBean>both(simpleNotificationMatcher("ProjectRole")).and(projectRoleMatcher);
    }

    private static Matcher<NotificationSchemeBean> matchesScheme(final Matcher<? super String> nameMatcher,
                                                                 @Nullable final Matcher<List<NotificationSchemeEventBean>> eventsMatcher) {
        return new TypeSafeMatcher<NotificationSchemeBean>() {
            @Override
            protected boolean matchesSafely(final NotificationSchemeBean notificationSchemeBean) {
                return nameMatcher.matches(notificationSchemeBean.getName())
                        && (eventsMatcher == null || eventsMatcher.matches(notificationSchemeBean.getNotificationSchemeEvents()));
            }

            @Override
            public void describeTo(final Description description) {
                description.appendValue(eventsMatcher);
            }
        };
    }

    private static Matcher<List<NotificationSchemeEventBean>> hasEvent(final Long eventTypeId,
                                                                       final Matcher<NotificationBean> notificationBeanMatcher) {
        return new TypeSafeMatcher<List<NotificationSchemeEventBean>>() {
            @Override
            protected boolean matchesSafely(final List<NotificationSchemeEventBean> notificationSchemeEventBeans) {
                return Iterables.any(notificationSchemeEventBeans, new Predicate<NotificationSchemeEventBean>() {
                    @Override
                    public boolean apply(final NotificationSchemeEventBean bean) {
                        return bean.getEvent().getId().equals(eventTypeId)
                                && Matchers.hasItem(notificationBeanMatcher).matches(bean.getNotifications());
                    }
                });
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("Notification scheme doesn't contain event wit id").appendValue(eventTypeId);
            }
        };
    }


}
