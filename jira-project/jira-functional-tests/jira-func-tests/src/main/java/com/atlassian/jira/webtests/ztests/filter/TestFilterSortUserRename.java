package com.atlassian.jira.webtests.ztests.filter;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.meterware.httpunit.WebTable;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertTrue;

/**
 * Test to ensure that a user's name is indexed rather than their full name.
 *
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.FILTERS})
@LoginAs(user = ADMIN_USERNAME)
public class TestFilterSortUserRename extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Test
    public void testRenameUserSortChanges() {
        administration.restoreData("sharedfilters/TestFilterSharing.xml");
        navigation.manageFilters().searchFilters();
        tester.setFormElement("searchName", "");
        tester.submit("Search");
        tester.clickLink("filter_sort_owner");

        WebTable mf_browse = tester.getDialog().getWebTableBySummaryOrId("mf_browse");
        assertTrue("Cell (1, 0) in table 'mf_browse' should be '", mf_browse.getCellAsText(1, 0).trim().contains("All Projects All Issues"));
        assertTrue("Cell (2, 0) in table 'mf_browse' should be '", mf_browse.getCellAsText(2, 0).trim().contains("PublicBugShare"));

        // edit the user
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.clickLink("edituser_link_user_can_share_filters");
        tester.setFormElement("fullName", "Aamon Buchanan");
        tester.submit("Update");

        //go back and search again
        navigation.issueNavigator().displayAllIssues();
        navigation.manageFilters().searchFilters();
        tester.submit("Search");
        tester.clickLink("filter_sort_owner");

        mf_browse = tester.getDialog().getWebTableBySummaryOrId("mf_browse");
        assertTrue("Cell (1, 0) in table 'mf_browse' should be '", mf_browse.getCellAsText(1, 0).trim().contains("PublicBugShare"));
        assertTrue("Cell (2, 0) in table 'mf_browse' should be '", mf_browse.getCellAsText(2, 0).trim().contains("All Projects All Issues"));
    }
}
