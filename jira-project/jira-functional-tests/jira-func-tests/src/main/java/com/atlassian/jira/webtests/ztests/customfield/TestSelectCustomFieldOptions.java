package com.atlassian.jira.webtests.ztests.customfield;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.EditIssueFieldVisibility;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.navigation.IssueNavigatorNavigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

@WebTest({Category.FUNC_TEST, Category.BROWSING, Category.CUSTOM_FIELDS})
@LoginAs(user = ADMIN_USERNAME)
public class TestSelectCustomFieldOptions extends BaseJiraFuncTest {
    private static final String CUSTOM_FIELD_ID = "10007";
    private static final String CUSTOM_FIELD_NAME = "SelectList";
    private static final String OPTION_00 = "10022";
    private static final String OPTION_01 = "10023";
    private static final String OPTION_02 = "10024";
    private static final String FIELD_CONFIGURATION_NAME_ONE = "Default Field Configuration";

    @Inject
    private EditIssueFieldVisibility editIssueFieldVisibility;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        // Clean JIRA instance
        administration.restoreData("TestSelectCustomFieldOptions.xml");
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    public void testEditOptions() throws SAXException {
        // Edit options
        administration.customFields().editOptionValue(CUSTOM_FIELD_ID, OPTION_00, "Honda");
        administration.customFields().editOptionValue(CUSTOM_FIELD_ID, OPTION_01, "Kawasaki");
        administration.customFields().editOptionValue(CUSTOM_FIELD_ID, OPTION_02, "Yamaha");

        // We should be on the custom field edit page and should be able to see the option text
        tester.gotoPage("/secure/admin/ConfigureCustomField!default.jspa?customFieldId=" + CUSTOM_FIELD_ID);
        tester.assertTextPresent("Honda");
        tester.assertTextPresent("Kawasaki");
        tester.assertTextPresent("Yamaha");

        // Should be able to change these again
        administration.customFields().editOptionValue(CUSTOM_FIELD_ID, OPTION_00, "Fiat");
        administration.customFields().editOptionValue(CUSTOM_FIELD_ID, OPTION_01, "Lambretta");
        administration.customFields().editOptionValue(CUSTOM_FIELD_ID, OPTION_02, "Vespa");

        // We should be on the custom field edit page and should be able to see the option text
        tester.gotoPage("/secure/admin/ConfigureCustomField!default.jspa?customFieldId=" + CUSTOM_FIELD_ID);
        tester.assertTextPresent("Fiat");
        tester.assertTextPresent("Lambretta");
        tester.assertTextPresent("Vespa");
    }

    @Test
    public void testDefaultShowsUpdatedOption() throws SAXException {
        // Set the default
        administration.customFields().setDefaultValue(CUSTOM_FIELD_ID, OPTION_00);
        tester.gotoPage("/secure/admin/ConfigureCustomField!default.jspa?customFieldId=" + CUSTOM_FIELD_ID);
        // Check the default is set
        locator.id("customfield_" + CUSTOM_FIELD_ID + "-value-default").getText().contains("option 00");

        // Edit options
        administration.customFields().editOptionValue(CUSTOM_FIELD_ID, OPTION_00, "Honda");
        administration.customFields().editOptionValue(CUSTOM_FIELD_ID, OPTION_01, "Kawasaki");
        administration.customFields().editOptionValue(CUSTOM_FIELD_ID, OPTION_02, "Yamaha");
        // Check the default now says Honda
        locator.id("customfield_" + CUSTOM_FIELD_ID + "-value-default").getText().contains("Honda");
    }

    @Test
    public void testDisable() throws SAXException {
        administration.customFields().disableOptions(CUSTOM_FIELD_ID, OPTION_00);
        tester.gotoPage("/secure/admin/ConfigureCustomField!default.jspa?customFieldId=" + CUSTOM_FIELD_ID);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"option 00", "(disabled)"});
        administration.customFields().enableOptions(CUSTOM_FIELD_ID, OPTION_00);
        tester.gotoPage("/secure/admin/ConfigureCustomField!default.jspa?customFieldId=" + CUSTOM_FIELD_ID);
        tester.assertTextNotPresent("(disabled)");
    }

    @Test
    public void testSearchNewValues() throws SAXException {
        // Edit options
        administration.customFields().editOptionValue(CUSTOM_FIELD_ID, OPTION_00, "Honda");
        administration.customFields().editOptionValue(CUSTOM_FIELD_ID, OPTION_01, "Kawasaki");
        administration.customFields().editOptionValue(CUSTOM_FIELD_ID, OPTION_02, "Yamaha");
        // Check the default now says Honda
        locator.id("customfield_" + CUSTOM_FIELD_ID + "-value-default").getText().contains("Honda");

        administration.reIndex();
        // Search for Hondas, there should be 1
        createSearchAndAssertIssues("SelectList = Honda ORDER BY key ASC", "HSP-11");
        // Search for Kawasaki, there should be 2
        createSearchAndAssertIssues("SelectList = Kawasaki ORDER BY key ASC", "HSP-10", "HSP-12");
    }

    @Test
    public void testDisabledNotValidForCreate() throws SAXException {
        // Disable "option 00"
        administration.customFields().disableOptions(CUSTOM_FIELD_ID, OPTION_00);
        // go to create issue
        navigation.issue().goToCreateIssueForm("homosapien", "Bug");
        // Check selectList values
        tester.assertOptionsEqual("customfield_" + CUSTOM_FIELD_ID, new String[]{"None", "option 01", "option 02"});

        // Enable "option 00"
        administration.customFields().enableOptions(CUSTOM_FIELD_ID, OPTION_00);
        // go to create issue
        navigation.issue().goToCreateIssueForm("homosapien", "Bug");
        // Check selectList values
        tester.assertOptionsEqual("customfield_" + CUSTOM_FIELD_ID, new String[]{"None", "option 00", "option 01", "option 02"});
    }

    @Test
    public void testDisabledNotValidForEdit() throws SAXException {
        // Disable "option 00"
        administration.customFields().disableOptions(CUSTOM_FIELD_ID, OPTION_00);
        // go to edit issue option 00 not already the selected value.
        navigation.issue().gotoEditIssue("HSP-10");
        // Check selectList values
        tester.assertOptionsEqual("customfield_" + CUSTOM_FIELD_ID, new String[]{"None", "option 01", "option 02"});
        // go to edit issue option 00 is already the selected value.
        navigation.issue().gotoEditIssue("HSP-11");
        // Check selectList values
        tester.assertOptionsEqual("customfield_" + CUSTOM_FIELD_ID, new String[]{"None", "option 00", "option 01", "option 02"});

        // Enable "option 00"
        administration.customFields().enableOptions(CUSTOM_FIELD_ID, OPTION_00);
        // go to edit issue option 00 not already the selected value.
        navigation.issue().gotoEditIssue("HSP-10");
        // Check selectList values
        tester.assertOptionsEqual("customfield_" + CUSTOM_FIELD_ID, new String[]{"None", "option 00", "option 01", "option 02"});
    }

    @Test
    public void testDisabledValidForSearch() throws SAXException {
        // Disable "option 00"
        administration.customFields().disableOptions(CUSTOM_FIELD_ID, OPTION_00);
        // go to edit issue option 00 not already the selected value.
        navigation.issueNavigator().gotoNewMode(IssueNavigatorNavigation.NavigatorEditMode.SIMPLE);
        // Check selectList values
        tester.gotoPage("secure/QueryComponentRendererEdit!Default.jspa?fieldId=customfield_" + CUSTOM_FIELD_ID + "&decorator=none&jqlContext=");
        tester.assertTextPresent("option 00");
        tester.assertTextPresent("option 01");
        tester.assertTextPresent("option 02");
    }

    @Test
    public void testRequiredFieldWithDefaultValue() throws SAXException {
        //set the default value for the SelectList
        administration.customFields().setDefaultValue(CUSTOM_FIELD_ID, OPTION_00);

        //configure the field to be 'Required'
        editIssueFieldVisibility.setRequiredFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, CUSTOM_FIELD_NAME);

        //Edit an issue and check that 'None' is not an Option
        navigation.issue().gotoEditIssue("HSP-11");

        tester.assertOptionsEqual("customfield_" + CUSTOM_FIELD_ID, new String[]{"option 00", "option 01", "option 02"});


    }

    @Test
    public void testRequiredFieldWithNoDefaultValue() throws SAXException {
        //un-set the default value for the SelectList
        administration.customFields().setDefaultValue(CUSTOM_FIELD_ID, "-1");

        //configure the field to be 'Required'
        editIssueFieldVisibility.setRequiredFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, CUSTOM_FIELD_NAME);

        //Edit an issue and check that 'None' is not an Option
        navigation.issue().gotoEditIssue("HSP-11");

        tester.assertOptionsEqual("customfield_" + CUSTOM_FIELD_ID, new String[]{"None", "option 00", "option 01", "option 02"});


    }

    private void createSearchAndAssertIssues(String jqlQuery, String... keys) {
        //Make sure we find the issues in the past.
        navigation.issueNavigator().createSearch(jqlQuery);
        assertions.getIssueNavigatorAssertions().assertExactIssuesInResults(keys);
    }

}
