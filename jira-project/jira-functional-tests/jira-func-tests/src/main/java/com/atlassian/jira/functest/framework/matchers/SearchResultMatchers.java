package com.atlassian.jira.functest.framework.matchers;

import com.atlassian.jira.testkit.client.restclient.SearchResult;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 * Matchers for {@link SearchResult} from testkit.
 *
 * @since v7.0
 */
public class SearchResultMatchers {
    public static Matcher<SearchResult> sizeEquals(final int size) {
        return new TypeSafeMatcher<SearchResult>() {
            @Override
            protected boolean matchesSafely(final SearchResult searchResult) {
                return searchResult.total == size;
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("Search results of size ").appendValue(size);
            }
        };
    }
}
