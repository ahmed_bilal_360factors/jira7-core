package com.atlassian.jira.webtests.ztests.navigator.jql;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.JqlAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.assertions.IssueNavigatorAssertions.FilterFormParam.createFilterFormParam;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.JQL})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestSystemFieldDoesItFitSingleManipulateData extends BaseJiraFuncTest {

    @Inject
    JqlAssertions jqlAssertions;

    @Inject
    private Administration administration;

    @Test
    public void testResolution() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String resolution1 = administration.resolutions().addResolution("unRESOLVED");
        final String resolution2 = administration.resolutions().addResolution("\"UNresolved\"");

        jqlAssertions.assertFitsFilterForm("resolution in (\"\\\"unRESOLVED\\\"\", \"\\\"\\\"UNresolved\\\"\\\"\")", createFilterFormParam("resolution", resolution1, resolution2));
    }
}
