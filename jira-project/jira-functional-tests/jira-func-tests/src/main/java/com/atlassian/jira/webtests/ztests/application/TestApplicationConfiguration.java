package com.atlassian.jira.webtests.ztests.application;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.backdoor.SalLicenseControl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.testkit.client.restclient.Group;
import com.atlassian.jira.testkit.client.restclient.User.Expand;
import com.atlassian.jira.testkit.client.restclient.UserClient;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static com.atlassian.jira.functest.framework.backdoor.SalLicenseControl.ValidationResultTO;
import static com.atlassian.jira.permission.GlobalPermissionKey.GLOBAL_PERMISSION_ID_TRANSLATION;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.SERVICE_DESK_KEY;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.TEST_KEY;
import static com.atlassian.jira.webtests.LicenseKeys.MULTI_ROLE;
import static com.atlassian.jira.webtests.LicenseKeys.TEST_ROLE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v7.0
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@RestoreBlankInstance
@LoginAs(user = ADMIN_USERNAME)
public class TestApplicationConfiguration extends BaseJiraRestTest {
    private SalLicenseControl salLicenseControl;

    @Before
    public void setUpTest() {
        backdoor.darkFeatures().enableForSite("com.atlassian.jira.config.CoreFeatures.LICENSE_ROLES_ENABLED");
        salLicenseControl = new SalLicenseControl(environmentData);
    }

    @Test
    public void testThatAdminGetsAddedToTheDefaultGroupOfTheLicense() {
        final UserClient userClient = new UserClient(environmentData);

        assertThat(backdoor.applicationRoles().getRoles().stream()
                .anyMatch(role -> role.getKey().equals(SERVICE_DESK_KEY)), is(false));

        assertThat(salLicenseControl.addProductLicense(SERVICE_DESK_KEY, MULTI_ROLE.getLicenseString()), is(true));

        final List<String> serviceDeskDefaultGroups = backdoor.applicationRoles()
                .getRole(SERVICE_DESK_KEY).getDefaultGroups();

        final List<String> testAppDefaultGroups = backdoor.applicationRoles()
                .getRole(TEST_KEY).getDefaultGroups();

        assertThat(serviceDeskDefaultGroups, is(not(empty())));
        assertThat(testAppDefaultGroups, is(not(empty())));

        final List<String> userGroups = userClient.get(ADMIN_USERNAME, Expand.groups).groups.items.stream()
                .map(Group::getName)
                .collect(Collectors.toList());

        assertThat(userGroups.containsAll(serviceDeskDefaultGroups), is(true));
        assertThat(userGroups.containsAll(testAppDefaultGroups), is(true));

        assertPermissionsConfigured(serviceDeskDefaultGroups);
        assertPermissionsConfigured(testAppDefaultGroups);
    }

    @Test
    public void testThatConfigurationFailsWhenGroupExist() {
        final String groupName = String.format("%s-users", SERVICE_DESK_KEY);
        backdoor.usersAndGroups().deleteGroup(groupName);

        ValidationResultTO validationResultTO =
                salLicenseControl.validateLicenseString(SERVICE_DESK_KEY,
                        MULTI_ROLE.getLicenseString(), Locale.ENGLISH);

        assertThat(validationResultTO.getWarningMessages().isEmpty(), is(true));

        backdoor.usersAndGroups().addGroup(groupName);

        validationResultTO = salLicenseControl.validateLicenseString(SERVICE_DESK_KEY,
                MULTI_ROLE.getLicenseString(), Locale.ENGLISH);

        assertThat(validationResultTO.hasWarnings(), is(false));
        assertThat(validationResultTO.hasErrors(), is(false));

        backdoor.usersAndGroups().deleteGroup(groupName);
    }

    @Test
    public void testThatExistingConfigurationIsPreserved() {
        final String myGroup = "my-group";
        backdoor.usersAndGroups().addGroup(myGroup);
        assertThat(salLicenseControl.addProductLicense(TEST_KEY, TEST_ROLE.getLicenseString()), is(true));
        backdoor.applicationRoles().putRoleWithDefaults(TEST_KEY, Sets.newHashSet(myGroup), Sets.newHashSet(myGroup));
        salLicenseControl.deleteProductLicense(TEST_KEY);
        assertThat(salLicenseControl.addProductLicense(TEST_KEY, TEST_ROLE.getLicenseString()), is(true));

        final List<String> testAppDefaultGroups = backdoor.applicationRoles()
                .getRole(TEST_KEY).getDefaultGroups();
        assertThat(testAppDefaultGroups, contains(is(myGroup)));
    }

    private void assertPermissionsConfigured(final List<String> groups) {
        for (GlobalPermissionKey gpKey : GlobalPermissionKey.DEFAULT_APP_GLOBAL_PERMISSIONS) {
            assertThat(groupFor(gpKey).containsAll(groups), is(true));
        }
    }

    private List<String> groupFor(GlobalPermissionKey permKey) {
        return backdoor.permissions().getGlobalPermissionGroups(GLOBAL_PERMISSION_ID_TRANSLATION
                .inverse().get(permKey));
    }

}