package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Test case to verify that the user counts are correct when updating global user preferences.
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
@Restore("TestGlobalUserPreferences.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestGlobalUserPreferences extends BaseJiraFuncTest {

    //Update everyone to html
    @Test
    public void testUpdateEmailMimeTypeToHtmlThenText() {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_DEFAULTS);
        tester.assertTextPresent("html");
        tester.clickLinkWithText("Apply");

        tester.assertTextPresent("receive 'text' email to receive 'html' email instead");
        tester.assertTextPresent("A total of 1 user");
        tester.submit("Update");

        //check that the users were updated.
        tester.clickLinkWithText("Apply");
        tester.assertTextPresent("A total of 0 users");
    }

    @Test
    public void testUpdateEmailMimeTypeToTextThenHtml() {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_DEFAULTS);
        tester.clickLinkWithText("Edit default values");
        tester.selectOption("preference", "text");
        tester.submit("Update");

        tester.assertTextPresent("text");
        tester.clickLinkWithText("Apply");

        tester.assertTextPresent("receive 'html' email to receive 'text' email instead");
        tester.assertTextPresent("A total of 1 user");
        tester.submit("Update");

        //check that the users were updated.
        tester.clickLinkWithText("Apply");
        tester.assertTextPresent("A total of 0 users");
    }
}
