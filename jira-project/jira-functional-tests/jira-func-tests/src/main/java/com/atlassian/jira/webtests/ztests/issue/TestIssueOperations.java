package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_ASSIGNEE_ERROR_MESSAGE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Mode.MODERN;
import static com.atlassian.jira.permission.ProjectPermissions.ASSIGN_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ATTACHMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.WORK_ON_ISSUES;
import static com.atlassian.jira.webtests.Groups.DEVELOPERS;
import static com.atlassian.jira.webtests.Groups.USERS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;

@WebTest({Category.FUNC_TEST, Category.ISSUES})
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueOperations extends BaseJiraFuncTest {

    private static final String ISSUE_KEY_1 = "HSP-1";
    private static final String ISSUE_KEY_2 = "HSP-2";
    private static final String TEST_USER_1 = "testuser";
    private static final String TEST_USER_2 = "testuser2";

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testIssueOperations() {
        administration.restoreData("TestIssueOperations.xml");
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
        administration.attachments().enable();

        issueOperationsWithWorkOnPermission(ISSUE_KEY_1);
        issueOperationWithUnassignableCurrentAssignee(ISSUE_KEY_1);
        issueOperationWithAssignPermission(ISSUE_KEY_1);
        issueOperationWithCreateAttachmentsPermission(ISSUE_KEY_1);
//        issueOperationAttachFileWithDoubleQuote(issueKey);
        issueOperationCacheControl(ISSUE_KEY_1);
    }

    //JRADEV-610
    @Test
    public void testIssueLinksForOperations() {
        administration.restoreData("TestIssueIconOperations.xml");

        //check issue links, attachments and time tracking.  First with a user that has access
        navigation.login(FRED_USERNAME);
        navigation.issue().gotoIssue("HSP-1");

        //first of all check links are present
        tester.assertLinkPresent("aszip");
        tester.assertLinkPresent("manage-attachment-link");
        tester.assertLinkPresent("add-attachments-link");

        tester.assertLinkPresent("add-links-link");

        tester.assertLinkPresent("log-work-link");

        //then check they go to the right pagesx
        tester.clickLink("manage-attachment-link");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Manage Attachments", "This page allows you to manage the attachments", "foobar.json"});
        tester.assertLinkPresent("aszipbutton");
        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("add-attachments-link");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Attach Files"});
        navigation.issue().gotoIssue("HSP-1");

        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("add-links-link");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Link"});
        navigation.issue().gotoIssue("HSP-1");

        tester.clickLink("log-work-link");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Log work"});

        //try a user with permissions for nothing.
        navigation.login("user", "user");
        navigation.issue().gotoIssue("HSP-1");
        tester.assertLinkPresent("aszip");
        tester.assertLinkNotPresent("manage-attachment-link");
        tester.assertLinkNotPresent("add-attachments-link");
        tester.assertLinkPresent("add-links-link");
        tester.assertLinkNotPresent("log-work-link");

        //try a user with manage attachments permission (can delete), but no add permission
        navigation.login(ADMIN_USERNAME);
        navigation.issue().gotoIssue("HSP-1");
        tester.assertLinkPresent("aszip");
        tester.assertLinkPresent("manage-attachment-link");
        tester.assertLinkNotPresent("add-attachments-link");
        tester.assertLinkPresent("add-links-link");
        tester.assertLinkNotPresent("log-work-link");

        // ensure that if ZIP support is disabled, then the link is not present
        administration.attachments().disableZipSupport();
        navigation.issue().gotoIssue("HSP-1");
        tester.assertLinkNotPresent("aszip");
        tester.clickLink("manage-attachment-link");
        tester.assertLinkNotPresent("aszipbutton");
    }

    /**
     * Tests the availability of 'Log Work' Link with 'Work On Issues' permission removed
     */
    public void issueOperationsWithWorkOnPermission(String issueKey) {
        logger.log("Issue Operation: Test availability of Log Work Link with 'Work On Issues' permission.");
        administration.timeTracking().enable(MODERN);
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, WORK_ON_ISSUES, DEVELOPERS);
        navigation.issue().gotoIssue(issueKey);
        tester.assertLinkNotPresent("log-work");

        // Grant Work issue Permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, WORK_ON_ISSUES, DEVELOPERS);
        navigation.issue().gotoIssue(issueKey);
        tester.assertLinkPresent("log-work");
        administration.timeTracking().disable();
    }

    @Test
    public void testLogWorkOperationTimeTrackingDisabled() {
        administration.restoreData("TestIssueOperations.xml");
        administration.timeTracking().disable();
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");

        navigation.issue().gotoIssue(ISSUE_KEY_1);
        //log work operation/permission message should not be displayed at all
        tester.assertLinkNotPresent("log-work");
    }

    @Test
    public void testLogWorkOperationAnonymous() {
        administration.restoreData("TestIssueOperations.xml");
        administration.timeTracking().enable(MODERN);
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");

        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, BROWSE_PROJECTS, "group");

        navigation.logout();

        tester.gotoPage("/browse/HSP-1");

        //log work operation/permission message should not be displayed at all
        tester.assertLinkNotPresent("log-work");
    }

    @Test
    public void testLogWorkOperationIssueInNonEditableState() {
        administration.restoreData("TestIssueOperations.xml");
        administration.timeTracking().enable(MODERN);
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");

        navigation.issue().gotoIssue(ISSUE_KEY_1);
        tester.clickLinkWithText("Close Issue");
        tester.setWorkingForm("issue-workflow-transition");
        tester.submit("Transition");

        //log work operation/permission message should not be displayed at all
        tester.assertLinkNotPresent("log-work");
    }

    @Test
    public void testLogWorkOperationHappyPath() {
        administration.restoreData("TestIssueOperations.xml");
        administration.timeTracking().enable(MODERN);
        navigation.issue().gotoIssue(ISSUE_KEY_1);

        //log work operation/permission message should not be displayed at all
        tester.assertLinkPresent("log-work");
    }

    /**
     * Attempts to assign an issue from the issue page with the 'Assignable User' permission removed
     */
    public void issueOperationWithUnassignableCurrentAssignee(String issueKey) {
        logger.log("Issue Operation: Attempt to set the assignee to be an unassignable user ...");

        // Remove assignable permission
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGNABLE_USER, DEVELOPERS);
        navigation.issue().gotoIssue(issueKey);

        tester.clickLink("assign-issue");
        tester.setWorkingForm("assign-issue");
        tester.submit();

        // Even though admin is no longer assignable - they are allowed to REMAIN the current assignee (at least in edit etc)
        tester.assertTextNotPresent(DEFAULT_ASSIGNEE_ERROR_MESSAGE);
        // But in Assign operation this is considered stupid
        tester.assertTextPresent("Issue already assigned to Administrator (admin)");

        //Restore permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGNABLE_USER, DEVELOPERS);
    }

    /**
     * Tests if the 'Assign' link is available with the 'Assign Issues' Permission removed
     */
    public void issueOperationWithAssignPermission(String issueKey) {
        logger.log("Issue Operation: Test the availability of the 'Assign Link' with 'Assign Issues' Permission.");

        navigation.issue().gotoIssue(issueKey);
        tester.assertLinkPresent("assign-issue");

        // Remove assignable permission
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ASSIGN_ISSUES, DEVELOPERS);
        navigation.issue().gotoIssue(issueKey);
        tester.assertLinkNotPresent("assign-issue");

        //Restore permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ASSIGN_ISSUES, DEVELOPERS);
    }

    /**
     * Tests if the 'Attach File' link is available with the 'Create Attachments' Permission removed
     */
    public void issueOperationWithCreateAttachmentsPermission(String issueKey) {
        logger.log("Issue Operation: Test the availability of the 'Attach Link' with 'Create Attachments' Permission.");
        // Attach screenshots only works for Windows
        // Remove assignable permission
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, CREATE_ATTACHMENTS, USERS);
        navigation.issue().gotoIssue(issueKey);
        tester.assertLinkNotPresent("attach-file");
        //        assertLinkNotPresent("attach_screenshot");

        //Restore permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, CREATE_ATTACHMENTS, USERS);
        navigation.issue().gotoIssue(issueKey);
        tester.assertLinkPresent("attach-file");
        //        assertLinkPresent("attach_screenshot");
    }

    /**
     * Tests that cache control is correct for /browse/ and issue navigator pages
     */
    public void issueOperationCacheControl(String issueKey) {
        logger.log("Issue Operation: Test cache control for Issue Navigator pages.");
        navigation.issueNavigator().displayAllIssues();


        String cache = tester.getDialog().getResponse().getHeaderField("Cache-Control");

        assertThat(cache, is("no-cache, no-store, must-revalidate"));

        navigation.issue().gotoIssue(issueKey);
        cache = tester.getDialog().getResponse().getHeaderField("Cache-Control");
        assertThat(cache, is("no-store, no-cache, must-revalidate"));
    }

    /**
     * Tests the "Manage Watchers" permission when the user is a 'Reporter' and not a reporter
     */
    @Test
    public void testIssueOperationManageWatcherList() {
        administration.restoreData("TestIssueOperationsWithReporter.xml");
        administration.attachments().enable();

        // go to issue as a reporter
        navigation.logout();
        navigation.login(TEST_USER_1, TEST_USER_1);
        navigation.issue().gotoIssue(ISSUE_KEY_2);
        tester.assertLinkPresent("manage-watchers");
        navigation.logout();
        navigation.login(TEST_USER_2, TEST_USER_2);
        tester.assertLinkNotPresent("manage-watchers");
        navigation.logout();
    }

    @Test
    public void testIssueOperationsWithLongNames() {
        administration.restoreData("TestIssueOperationsWithLongTransitions.xml");
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
        navigation.issue().gotoIssue(ISSUE_KEY_1);

        IdLocator locator = new IdLocator(tester, "action_id_711");
        textAssertions.assertTextPresent(locator, "Another really long on...");
        assertEquals("Another really long one that has a description as well - Yep, this is the description", locator.getNode().getAttributes().getNamedItem("title").getNodeValue());

        locator = new IdLocator(tester, "action_id_4");
        textAssertions.assertTextPresent(locator, "Start Progress that is...");
        assertEquals("Start Progress that is...", locator.getNode().getTextContent());

        locator = new IdLocator(tester, "action_id_5");
        textAssertions.assertTextPresent(locator, "Resolve Issue");
        assertEquals("Resolve Issue - We can still give it a description", locator.getNode().getAttributes().getNamedItem("title").getNodeValue());

        locator = new IdLocator(tester, "action_id_2");
        textAssertions.assertTextPresent(locator, "Close Issue");
        assertEquals(null, locator.getNode().getAttributes().getNamedItem("title"));
    }
}
