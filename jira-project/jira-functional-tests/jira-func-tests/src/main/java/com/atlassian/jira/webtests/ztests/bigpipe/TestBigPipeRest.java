package com.atlassian.jira.webtests.ztests.bigpipe;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.backdoor.BackdoorControl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@WebTest({Category.FUNC_TEST, Category.REST})
@RestoreBlankInstance
public class TestBigPipeRest extends BaseJiraRestTest {

    private BigPipeFeatureControl state;

    @Before
    public void setUp() {
        state = new BigPipeFeatureControl(backdoor);
    }

    @After
    public void tearDown() {
        state.restore();
    }

    @Test
    public void testBigpipeDisabled() {
        state.setBigPipeEnabled(true);
        state.setBigPipeKillswitchEnabled(false);

        String response = new BigPipeClient(environmentData).getPanel();

        assertThat(response, is("<div id=\"bigpipe-panel\"></div>"));
    }

    private static class BigPipeClient extends BackdoorControl<BigPipeClient> {

        BigPipeClient(final JIRAEnvironmentData environmentData) {
            super(environmentData);
        }

        String getPanel() {
            return createResource()
                    .path("bigpipe")
                    .get(String.class);
        }
    }
}
