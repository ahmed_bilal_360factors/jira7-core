package com.atlassian.jira.webtests.ztests.subtask;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;

import org.junit.Test;

@WebTest({Category.FUNC_TEST, Category.SUB_TASKS})
@Restore("TestWorklogAndTimeTracking.xml")
public class TestCreateSubTasksAndWorklog extends BaseJiraFuncTest {
    @Test
    public void testCreateSubTasLogWorkDelete() {
        navigation.login("admin", "admin");
        backdoor.subtask().enable();

        Long projectId = backdoor.project().getProjectId("HSP");
        final IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "Parent issue");
        final IssueCreateResponse subtask = backdoor.issues().createSubtask(projectId.toString(), issue.key, "subtask");

        navigation.issue().gotoEditIssue(subtask.key);
        tester.setFormElement("timetracking", "30m");
        tester.setFormElement("worklog_activate", "true");
        tester.setFormElement("worklog_timeLogged", "5m");
        tester.submit();

        navigation.issue().deleteIssue(subtask.key);
    }
}
