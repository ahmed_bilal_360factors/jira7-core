package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Responsible for holding tests which verify that the Shared Filters Administration actions are not susceptible to
 * XSRF attacks.
 *
 * @since v6.0.2
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestXsrfAdminSharedFilters extends BaseJiraFuncTest {
    @Inject
    protected Administration administration;
    @Inject
    private Form form;

    private String addFilterSharedWithAllJiraUsers() {
        return backdoor.filters().createFilter("", Data.SHARED_PUBLIC_FILTER_NAME, "admin", "jira-users");
    }

    @Test
    public void testSharedFilterOperations() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck(
                        "Delete Shared Filter",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                final String filterId = addFilterSharedWithAllJiraUsers();
                                administration.sharedFilters().goTo();
                                tester.clickLink("delete_" + filterId);
                                tester.setWorkingForm("delete-filter-confirm-form-" + filterId);
                            }
                        },
                        new XsrfCheck.FormSubmissionWithId("delete-filter-submit")
                )
        ).
                run(tester, navigation, form);
    }

    private static class Data {
        private static final String SHARED_PUBLIC_FILTER_NAME = "Public Filter - Owner: Admin";
    }
}
