package com.atlassian.jira.functest.framework;

import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.testkit.client.log.FuncTestLoggerImpl;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;

/**
 * Navigation actions that pertain to workflows and bulk migrations/transitions.
 *
 * @since v4.0
 */
public class WorkflowsImpl implements Workflows {

    private final WebTester tester;
    private final FuncTestLogger logger;
    private final Navigation navigation;

    @Inject
    public WorkflowsImpl(final WebTester tester, final JIRAEnvironmentData environmentData, final Navigation navigation) {
        this.tester = tester;
        this.navigation = navigation;
        this.logger = new FuncTestLoggerImpl(2);
    }

    public void chooseWorkflowAction(final String workflowFormElement) {
        assertStepOperationDetails();
        tester.setFormElement(BULK_TRANSITION_ELEMENT_NAME, workflowFormElement);
        tester.assertRadioOptionSelected("wftransition", workflowFormElement);
        logger.log("Workflow action selected");
        navigation.clickOnNext();
    }

    public void assertStepOperationDetails() {
        tester.assertTextPresent(STEP_PREFIX + STEP_OPERATION_DETAILS);
    }
}
