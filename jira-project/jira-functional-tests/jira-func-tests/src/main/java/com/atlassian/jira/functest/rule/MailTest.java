package com.atlassian.jira.functest.rule;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that the test should turn on email queue logging, clear quqeue, perform the test, and stop email quqeue after finishing tests.
 *
 * @since v7.1
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface MailTest {
}
