package com.atlassian.jira.webtests.ztests.fields;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperations;
import com.atlassian.jira.functest.framework.FieldLayoutSchemes;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.meterware.httpunit.HttpUnitOptions;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.BulkOperations.SAME_FOR_ALL;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_PREFIX;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_FREETEXT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_TEXTFIELD;
import static com.atlassian.jira.functest.framework.admin.CustomFields.builtInCustomFieldKey;
import static com.atlassian.jira.functest.framework.admin.CustomFields.numericCfId;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.webtests.Groups.USERS;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertFalse;

/**
 * A test suite that exercises the renderer framework and implmentations.
 */
@WebTest({Category.FUNC_TEST, Category.FIELDS})
@RestoreBlankInstance
@LoginAs(user = ADMIN_USERNAME)
public class TestFieldRenderers extends BaseJiraFuncTest {
    private static final String CUSTOM_FIELD_NAME_TEXT_FIELD = "Custom Field Text Field";
    private static final String CUSTOM_FIELD_NAME_TEXT_AREA_FIELD = "Custom Field Text Area Field";
    private static final String DEFAULT_FIELD_CONFIGURATION = "Default Field Configuration";
    private static final String CUSTOM_FIELD_CONFIGURATION = "Renderer Custom Field Configuration";
    private static final String WIKI_STYLE_RENDERER = "Wiki Style Renderer";
    private static final String DEFAULT_TEXT_RENDERER = "Default Text Renderer";
    private static final String CUSTOM_FIELD_CONFIGURATION_SCHEME = "Renderer Custom Field Configuration Scheme";
    private static final String PROJECT_DOG = "Canine";
    private static final String PROJECT_DOG_KEY = "DOG";
    private static final String OTHER_PROJECT = "Renderer Test Project";
    private static final String OTHER_PROJECT_KEY = "OTH";
    private static final String HTML_CODE = "<b>testWikiRendererBadLink</b>";
    private static final String TEXT_AREA_CF_ID = "customfield_10000";
    private static final String TEXT_FIELD_CF_ID = "customfield_10001";
    private static final String COMMENT_FIELD_ID = "comment";
    private static final String DESCRIPTION_ID = "description";
    private static final String ENVIRONMENT_ID = "environment";
    private String issueKey;
    private String textAreaCustomFieldId;
    private String textCustomFieldId;
    private long projectId;
    @Inject
    private FieldLayoutSchemes fieldLayoutSchemes;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private BulkOperations bulkOperations;

    @Inject
    private Administration administration;

    @Before
    public void setUp() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        projectId = administration.project().addProject(PROJECT_DOG, PROJECT_DOG_KEY, ADMIN_USERNAME);
        setFieldConfigurationFieldToRenderer(DEFAULT_FIELD_CONFIGURATION, COMMENT_FIELD_ID, WIKI_STYLE_RENDERER);
        setFieldConfigurationFieldToRenderer(DEFAULT_FIELD_CONFIGURATION, DESCRIPTION_ID, WIKI_STYLE_RENDERER);
        setFieldConfigurationFieldToRenderer(DEFAULT_FIELD_CONFIGURATION, ENVIRONMENT_ID, WIKI_STYLE_RENDERER);

        textAreaCustomFieldId = numericCfId(administration.customFields().addCustomField(builtInCustomFieldKey(CUSTOM_FIELD_TYPE_FREETEXT), CUSTOM_FIELD_NAME_TEXT_AREA_FIELD));
        textCustomFieldId = numericCfId(administration.customFields().addCustomField(builtInCustomFieldKey(CUSTOM_FIELD_TYPE_TEXTFIELD), CUSTOM_FIELD_NAME_TEXT_FIELD));
        addFieldsToFieldScreen("Workflow Screen", new String[]{CUSTOM_FIELD_NAME_TEXT_AREA_FIELD, CUSTOM_FIELD_NAME_TEXT_FIELD});
        addFieldsToFieldScreen("Default Screen", new String[]{CUSTOM_FIELD_NAME_TEXT_AREA_FIELD, CUSTOM_FIELD_NAME_TEXT_FIELD});
        addFieldsToFieldScreen("Resolve Issue Screen", new String[]{CUSTOM_FIELD_NAME_TEXT_AREA_FIELD, CUSTOM_FIELD_NAME_TEXT_FIELD});

        setFieldConfigurationFieldToRenderer(DEFAULT_FIELD_CONFIGURATION, TEXT_AREA_CF_ID, WIKI_STYLE_RENDERER);
        setFieldConfigurationFieldToRenderer(DEFAULT_FIELD_CONFIGURATION, TEXT_FIELD_CF_ID, WIKI_STYLE_RENDERER);

        navigation.gotoAdminSection(Navigation.AdminSection.FIELD_CONFIGURATION);
        tester.clickLink("copy-" + DEFAULT_FIELD_CONFIGURATION);
        tester.setFormElement("fieldLayoutName", CUSTOM_FIELD_CONFIGURATION);
        tester.submit("Copy");

        // create an issue to play around with
        issueKey = backdoor.issues().createIssue(PROJECT_DOG_KEY, "This is a test issue", ADMIN_USERNAME, "Major", "Bug").key();
        backdoor.issues().setIssueFields(issueKey, new IssueFields()
                .environment("test environment")
                .description("description for this is a test issue"));

        // Ensure that attachments are enabled
        administration.attachments().enable();
        administration.addGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
    }

    @Test
    public void testFieldRenderers() {
        logger.log("-- begin testFieldRenderers");

        _testWikiRendererVisible();
        _testRendererConfiguration();
        _testRendererPluginConfiguration();
        _testRendererWithBulkOperations();
        _testRendererAlternativeViews();
        _testWikiRendererBadLink();
        _testWikiRendererBadMacro();

        logger.log("-- end   testFieldRenderers");
    }

    public void _testRendererAlternativeViews() {
        _testRendererRssView();
        _testIssueNavigatorColumnView();
    }

    public void _testIssueNavigatorColumnView() {
        try {
            this.navigation.issue().gotoIssue(issueKey);
            tester.clickLink("edit-issue");
            tester.setFormElement(DESCRIPTION_ID, "{color:blue}blue functional test text{color}");
            tester.submit("Update");
            tester.assertTextPresent("<font color=\"blue\">blue functional test text</font>");

            backdoor.columnControl().addLoggedInUserColumns(singletonList(DESCRIPTION_ID));
            this.navigation.issueNavigator().displayAllIssues();
            tester.assertTextPresent("<font color=\"blue\">blue functional test text</font>");
        } finally {
            backdoor.columnControl().restoreLoggedInUserColumns();
        }
    }

    public void _testRendererRssView() {
        logger.log("--- begin testRendererRssView");

        // test the issue rss
        try {
            this.navigation.issue().gotoIssue(issueKey);
            tester.clickLink("edit-issue");
            tester.setFormElement(DESCRIPTION_ID, "{color:blue}blue functional test text{color}");
            tester.submit("Update");
            tester.assertTextPresent("<font color=\"blue\">blue functional test text</font>");
            tester.clickLinkWithText("XML");
            tester.assertTextPresent("&lt;font color=&quot;blue&quot;&gt;blue functional test text&lt;/font&gt;");
        } finally {
            // browse back to the application since we are now viewing the xml for the issue
            tester.beginAt("/secure/Dashboard.jspa");
        }

        try {
            // test the issue navigator rss
            tester.gotoPage("/sr/jira.issueviews:searchrequest-xml/temp/SearchRequest.xml?jqlQuery=&tempMax=1000");
            tester.assertTextPresent("&lt;font color=&quot;blue&quot;&gt;blue functional test text&lt;/font&gt;");
        } finally {
            // browse back to the application since we are now viewing the xml for the issue
            tester.beginAt("/secure/Dashboard.jspa");
        }

        logger.log("--- end   testRendererRssView");
    }


    public void _testRendererWithBulkOperations() {
        _testBulkEditWithSameRendererType();
        _testBulkOperationsWithDifferentRendererTypes();
    }

    public void _testBulkOperationsWithDifferentRendererTypes() {
        String otherProjectIssueKey = null;
        try {
            // now we will create an issue in another project that is linked to another fieldLayoutScheme, give it
            // a different renderer type.
            administration.fieldConfigurationSchemes().addFieldConfigurationScheme(CUSTOM_FIELD_CONFIGURATION_SCHEME, "random description");
            fieldLayoutSchemes.addFieldLayoutScheme(CUSTOM_FIELD_CONFIGURATION_SCHEME, "random description");
            administration.project().addProject(OTHER_PROJECT, OTHER_PROJECT_KEY, ADMIN_USERNAME);

            // change the field configuration scheme to use the custom field configuration
            associatedSchemeWithConfiguration(CUSTOM_FIELD_CONFIGURATION_SCHEME, CUSTOM_FIELD_CONFIGURATION);
            fieldLayoutSchemes.associateFieldLayoutScheme(OTHER_PROJECT_KEY, CUSTOM_FIELD_CONFIGURATION_SCHEME);

            // set the renderer types to different renderer types for the two configurations
            setFieldConfigurationFieldToRenderer(CUSTOM_FIELD_CONFIGURATION, TEXT_AREA_CF_ID, DEFAULT_TEXT_RENDERER);
            setFieldConfigurationFieldToRenderer(DEFAULT_FIELD_CONFIGURATION, TEXT_AREA_CF_ID, WIKI_STYLE_RENDERER);

            setFieldConfigurationFieldToRenderer(CUSTOM_FIELD_CONFIGURATION, TEXT_FIELD_CF_ID, DEFAULT_TEXT_RENDERER);
            setFieldConfigurationFieldToRenderer(DEFAULT_FIELD_CONFIGURATION, TEXT_FIELD_CF_ID, WIKI_STYLE_RENDERER);

            // create an issue to play around with
            otherProjectIssueKey = backdoor.issues().createIssue(OTHER_PROJECT_KEY, "This is a test issue", ADMIN_USERNAME, "Major", "Bug").key();
            backdoor.issues().setIssueFields(otherProjectIssueKey, new IssueFields()
                    .environment("test environment")
                    .description("description for this is a test issue")
                    .customField(Long.decode(textAreaCustomFieldId), "testValue"));

            _testBulkEditWithDifferentRendererTypes();
            _testBulkMoveWithDifferentRendererTypes();
        } finally {
            // tear down this tests setup
            if (otherProjectIssueKey != null) {
                this.navigation.issue().deleteIssue(otherProjectIssueKey);
            }
            if (administration.project().projectWithKeyExists(OTHER_PROJECT_KEY)) {
                fieldLayoutSchemes.associateWithDefaultFieldLayout(OTHER_PROJECT_KEY);
                administration.project().deleteProject(OTHER_PROJECT);
            }
            fieldLayoutSchemes.deleteFieldLayoutScheme(CUSTOM_FIELD_CONFIGURATION_SCHEME);
        }
    }

    private void associatedSchemeWithConfiguration(final String schemeName, final String fieldConfigurationName) {
        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_FIELDS);
        tester.clickLinkWithText(schemeName);
        tester.clickLink("edit_fieldlayoutschemeentity");
        tester.selectOption("fieldConfigurationId", fieldConfigurationName);
        tester.submit("Update");
    }

    public void _testBulkMoveWithDifferentRendererTypes() {
        logger.log("--- begin testBulkMoveWithDifferentRendererTypes");

        try {
            // we will test that when the renderer type is different for where you are going
            // that you will receive a warning message.
            this.navigation.issueNavigator().displayAllIssues();
            bulkOperations.bulkChangeIncludeAllPages();
            // Only turn this on so that the wiki renderer stuff will showup
            HttpUnitOptions.setScriptingEnabled(true);
            bulkOperations.bulkChangeChooseIssuesAll();
            bulkOperations.isStepChooseOperation();
            bulkOperations.chooseOperationBulkMove();

            navigation.issue().selectProject(PROJECT_DOG, projectId + "_1_pid");
            tester.checkCheckbox(SAME_FOR_ALL, projectId + "_1_");
            navigation.clickOnNext();

            // check that there is a warning for the text area custom field and not for the text field
            tester.assertTextPresent("warning-" + CUSTOM_FIELD_NAME_TEXT_AREA_FIELD);
            tester.assertTextNotPresent("warning-" + CUSTOM_FIELD_NAME_TEXT_FIELD);

            bulkOperations.bulkChangeCancel();
        } finally {
            HttpUnitOptions.setScriptingEnabled(false);
        }
        logger.log("--- end   testBulkMoveWithDifferentRendererTypes");
    }

    public void _testBulkEditWithDifferentRendererTypes() {
        logger.log("--- begin testBulkEditWithDifferentRendererTypes");

        try {
            // confirm that we can not bulk edit the field.
            this.navigation.issueNavigator().displayAllIssues();
            bulkOperations.bulkChangeIncludeAllPages();
            // Only turn this on so that the wiki renderer stuff will showup
            HttpUnitOptions.setScriptingEnabled(true);
            bulkOperations.bulkChangeChooseIssuesAll();
            bulkOperations.isStepChooseOperation();
            bulkOperations.bulkChangeChooseOperationEdit();

            // this could check a bit better that the warning message is for the specific field
            tester.assertTextPresent("This field has inconsistent renderer types for the project(s) of the selected issues.");

            bulkOperations.bulkChangeCancel();
        } finally {
            HttpUnitOptions.setScriptingEnabled(false);
        }
        logger.log("--- end   testBulkEditWithDifferentRendererTypes");
    }

    public void _testBulkEditWithSameRendererType() {
        logger.log("--- begin testBulkEditWithSameRendererType");

        try {
            // test that we can correctly edit a custom field value through bulk edit
            this.navigation.issueNavigator().displayAllIssues();
            bulkOperations.bulkChangeIncludeAllPages();
            // Only turn this on so that the wiki renderer stuff will showup
            HttpUnitOptions.setScriptingEnabled(true);
            bulkOperations.bulkChangeChooseIssuesAll();
            bulkOperations.isStepChooseOperation();
            bulkOperations.bulkChangeChooseOperationEdit();

            // do the validation we came here to do, we pass the existing issue key because
            // for bulk edit the first selected issue will be the key if all renderer types match
            validateWikiRendererForField(CUSTOM_FIELD_PREFIX + textAreaCustomFieldId, CUSTOM_FIELD_NAME_TEXT_AREA_FIELD, issueKey);
            validateWikiRendererForField(CUSTOM_FIELD_PREFIX + textCustomFieldId, CUSTOM_FIELD_NAME_TEXT_FIELD, issueKey);

            bulkOperations.bulkChangeCancel();
        } finally {
            HttpUnitOptions.setScriptingEnabled(false);
        }
        logger.log("--- end   testBulkEditWithSameRendererType");
    }

    private void _testRendererPluginConfiguration() {
        _testTextRendererDoesNotAllowDisable();
        _testWikiRendererDisabled();
        _testWikiRendererMacroDisabled();
    }

    public void _testWikiRendererMacroDisabled() {

        logger.log("--- begin testWikiRendererMacroDisabled");
        try {
            this.navigation.issue().gotoIssue(issueKey);
            tester.clickLink("edit-issue");
            tester.setFormElement(DESCRIPTION_ID, "{color:blue}blue functional test text{color}");
            tester.submit("Update");
            tester.assertTextPresent("<font color=\"blue\">blue functional test text</font>");

            administration.plugins().disablePluginModule("com.atlassian.jira.plugin.system.renderers.wiki.macros", "com.atlassian.jira.plugin.system.renderers.wiki.macros:color");

            this.navigation.issue().gotoIssue(issueKey);
            tester.assertTextPresent("{color:blue}blue functional test text{color}");

        } finally {
            administration.plugins().enablePluginModule("com.atlassian.jira.plugin.system.renderers.wiki.macros", "com.atlassian.jira.plugin.system.renderers.wiki.macros:color");
        }
        logger.log("--- end   testWikiRendererMacroDisabled");
    }

    public void _testTextRendererDoesNotAllowDisable() {
        logger.log("--- begin _testTextRendererDoesNotAllowDisable");
        assertFalse(administration.plugins().canDisablePluginModule("com.atlassian.jira.plugin.system.jirarenderers", "com.atlassian.jira.plugin.system.jirarenderers:jira-text-renderer"));
        logger.log("--- end   _testTextRendererDoesNotAllowDisable");
    }

    public void _testWikiRendererDisabled() {
        logger.log("--- begin testWikiRendererDisabled");
        administration.plugins().disablePluginModule("com.atlassian.jira.plugin.system.jirarenderers", "com.atlassian.jira.plugin.system.jirarenderers:atlassian-wiki-renderer");

        // now wander off to the edit screen and make sure the text renderer is used and that we have a warninig
        this.navigation.issue().gotoIssue(issueKey);
        tester.clickLink("edit-issue");
        validateNoWikiRendererForField(DESCRIPTION_ID, "Description", issueKey);
        tester.assertTextPresent("This field is configured to use the \"atlassian-wiki-renderer\" which is not currently available, using \"Default Text Renderer\" instead.");
        // turn the renderer back on
        administration.plugins().enablePluginModule("com.atlassian.jira.plugin.system.jirarenderers", "com.atlassian.jira.plugin.system.jirarenderers:atlassian-wiki-renderer");
        logger.log("--- end   testWikiRendererDisabled");
    }

    public void _testRendererConfiguration() {
        _testRendererConfigurationWarningMessage();
        _testRendererConfigurationChangeRendererType();
        _testCustomLayoutRendererConfiguration();
    }

    public void _testCustomLayoutRendererConfiguration() {
        logger.log("--- begin testCustomLayoutRendererConfiguration");

        // Make sure configuring renderers on a layout other than the default works
        // this will also test that the warning does not show up on the edit pages where
        // there are not effected issues, this is the only safe place for this test since we
        // now that there are no issues associated with this layout yet.

        setFieldConfigurationFieldToRenderer(CUSTOM_FIELD_CONFIGURATION, DESCRIPTION_ID, WIKI_STYLE_RENDERER, true);
        setFieldConfigurationFieldToRenderer(CUSTOM_FIELD_CONFIGURATION, DESCRIPTION_ID, DEFAULT_TEXT_RENDERER, true);
        logger.log("--- end   testCustomLayoutRendererConfiguration");
    }

    public void _testRendererConfigurationChangeRendererType() {
        logger.log("--- begin testRendererConfigurationChangeRendererType");
        try {
            HttpUnitOptions.setScriptingEnabled(true);
            setFieldConfigurationFieldToRenderer(DEFAULT_FIELD_CONFIGURATION, COMMENT_FIELD_ID, DEFAULT_TEXT_RENDERER);
            this.navigation.issue().gotoIssue(issueKey);
            validateNoWikiRendererForField(COMMENT_FIELD_ID, "Comment", issueKey);
            setFieldConfigurationFieldToRenderer(DEFAULT_FIELD_CONFIGURATION, COMMENT_FIELD_ID, WIKI_STYLE_RENDERER);
        } finally {
            HttpUnitOptions.setScriptingEnabled(false);
        }
        logger.log("--- end   testRendererConfigurationChangeRendererType");
    }

    public void _testRendererConfigurationWarningMessage() {
        logger.log("--- begin testRendererConfigurationWarningMessage");
        gotoFieldLayoutConfiguration(DEFAULT_FIELD_CONFIGURATION);
        tester.clickLink("renderer_description");
        tester.assertTextPresent("Edit Field Renderer");
        tester.assertTextPresent("A renderer determines how the value of a field will be displayed within the system.");
        logger.log("--- end   testRendererConfigurationWarningMessage");
    }

    // this will not work since it seems httpunit does not support ajax style requests.
    public void _testWikiPreview() {
        logger.log("--- begin testWikiPreview");
        try {
            // turn on javascript so we can check the comment field on the view issue screen
            HttpUnitOptions.setScriptingEnabled(true);
            this.navigation.issue().gotoIssue(issueKey);
            tester.clickLink("edit-issue");

            tester.setFormElement(DESCRIPTION_ID, ":)");
            tester.clickLink("description-preview_link");
            tester.assertTextPresent("<p><img class=\"emoticon\" src=\"http://localhost:8080/images/icons/emoticons/smile.gif\" alt=\"\" align=\"middle\" border=\"0\" height=\"20\" width=\"20\"></p>");
        } finally {
            HttpUnitOptions.setScriptingEnabled(false);
        }
        logger.log("--- end   testWikiPreview");
    }

    public void _testWikiRendererVisible() {
        _testWikiRendererVisibleOnViewIssue();
        _testWikiRendererVisibleOnEditIssue();
        _testWikiRendererVisibleOnCreateIssue();
        _testWikiRendererVisibleOnAssignIssue();
        _testWikiRendererVisibleOnAttachFile();
        _testWikiRendererVisibleOnLinkIssue();
    }

    public void _testWikiRendererVisibleOnLinkIssue() {
        logger.log("--- begin testWikiRendererVisibleOnLinkIssue");
        try {
            createIssueLinkType();
            // turn on javascript so we can check the comment field on the view issue screen
            HttpUnitOptions.setScriptingEnabled(true);
            this.navigation.issue().gotoIssue(issueKey);
            tester.clickLink("link-issue");
            validateWikiRendererForField(COMMENT_FIELD_ID, "Comment", issueKey);
        } finally {
            HttpUnitOptions.setScriptingEnabled(false);
            administration.issueLinking().disable();
        }
        logger.log("--- end   testWikiRendererVisibleOnLinkIssue");
    }

    public void _testWikiRendererVisibleOnAssignIssue() {
        logger.log("--- begin testWikiRendererVisibleOnAttachFile");
        try {
            // turn on javascript so we can check the comment field on the view issue screen
            HttpUnitOptions.setScriptingEnabled(true);
            this.navigation.issue().gotoIssue(issueKey);
            tester.clickLink("assign-issue");
            validateWikiRendererForField(COMMENT_FIELD_ID, "Comment", issueKey);
        } finally {
            HttpUnitOptions.setScriptingEnabled(false);
        }
        logger.log("--- end   testWikiRendererVisibleOnAttachFile");
    }

    public void _testWikiRendererVisibleOnAttachFile() {
        logger.log("--- begin testWikiRendererVisibleOnAssignIssue");
        try {
            // turn on javascript so we can check the comment field on the view issue screen
            HttpUnitOptions.setScriptingEnabled(true);
            this.navigation.issue().gotoIssue(issueKey);
            tester.clickLink("attach-file");
            validateWikiRendererForField(COMMENT_FIELD_ID, "Comment", issueKey);
        } finally {
            HttpUnitOptions.setScriptingEnabled(false);
        }
        logger.log("--- end   testWikiRendererVisibleOnAssignIssue");
    }

    public void _testWikiRendererVisibleOnCreateIssue() {
        logger.log("--- begin testWikiRendererVisibleOnCreateIssue");
        try {
            HttpUnitOptions.setScriptingEnabled(true);
            navigation.issue().goToCreateIssueForm(PROJECT_DOG, "Bug");
            tester.assertTextPresent("CreateIssueDetails.jspa");

            validateWikiRendererForField(DESCRIPTION_ID, "Description", "");
            validateWikiRendererForField(ENVIRONMENT_ID, "Environment", "");
            validateWikiRendererForField(CUSTOM_FIELD_PREFIX + textAreaCustomFieldId, CUSTOM_FIELD_NAME_TEXT_AREA_FIELD, "");
            validateWikiRendererForField(CUSTOM_FIELD_PREFIX + textCustomFieldId, CUSTOM_FIELD_NAME_TEXT_FIELD, "");
        } finally {
            HttpUnitOptions.setScriptingEnabled(false);
        }
        logger.log("--- end   testWikiRendererVisibleOnCreateIssue");
    }

    public void _testWikiRendererVisibleOnEditIssue() {
        logger.log("--- begin testWikiRendererVisibleOnEditIssue");
        try {
            // turn on javascript so we can check the comment field on the view issue screen
            HttpUnitOptions.setScriptingEnabled(true);
            this.navigation.issue().gotoIssue(issueKey);
            tester.clickLink("edit-issue");

            validateWikiRendererForField(COMMENT_FIELD_ID, "Comment", issueKey);
            validateWikiRendererForField(DESCRIPTION_ID, "Description", issueKey);
            validateWikiRendererForField(ENVIRONMENT_ID, "Environment", issueKey);
            validateWikiRendererForField(CUSTOM_FIELD_PREFIX + textAreaCustomFieldId, CUSTOM_FIELD_NAME_TEXT_AREA_FIELD, issueKey);
            validateWikiRendererForField(CUSTOM_FIELD_PREFIX + textCustomFieldId, CUSTOM_FIELD_NAME_TEXT_FIELD, issueKey);
        } finally {
            HttpUnitOptions.setScriptingEnabled(false);
        }
        logger.log("--- end   testWikiRendererVisibleOnEditIssue");
    }

    public void _testWikiRendererVisibleOnViewIssue() {
        logger.log("--- begin testWikiRendererVisibleOnViewIssue");
        try {
            // turn on javascript so we can check the comment field on the view issue screen
            HttpUnitOptions.setScriptingEnabled(true);
            this.navigation.issue().gotoIssue(issueKey);
            validateWikiRendererForField(COMMENT_FIELD_ID, "Comment", issueKey);
        } finally {
            HttpUnitOptions.setScriptingEnabled(false);
        }
        logger.log("--- end   testWikiRendererVisibleOnViewIssue");
    }

    /**
     * When a link using [square brackets] fails to render, the contents of the
     * link are printed literally, creating a vulnerability.
     */
    public void _testWikiRendererBadLink() {
        // test correct use of link
        assertValidIssueDescription("[" + HTML_CODE + "|http://www.google.com/]", HTML_CODE);
        // test incorrect use of link
        assertValidIssueDescription("[" + HTML_CODE + "]", HTML_CODE);
    }

    /**
     * When a macro fails to render, the contents of the macro tags is printed literally
     * creating a vulnerability. (See JRA-9090)
     */
    public void _testWikiRendererBadMacro() {
        // test valid macro
        assertValidIssueDescription("{code}" + HTML_CODE + "{code}", HTML_CODE);
        // test invalid macro
        assertValidIssueDescription("{noSuchMacro}" + HTML_CODE + "{noSuchMacro}", HTML_CODE);
        try {
            administration.plugins().disablePluginModule("com.atlassian.jira.plugin.system.renderers.wiki.macros", "com.atlassian.jira.plugin.system.renderers.wiki.macros:color");
            assertValidIssueDescription("{color:orange}" + HTML_CODE + "{color:orange}", HTML_CODE);
        } finally {
            administration.plugins().enablePluginModule("com.atlassian.jira.plugin.system.renderers.wiki.macros", "com.atlassian.jira.plugin.system.renderers.wiki.macros:color");
        }

    }

    /**
     * Creates an issue with the given description and asserts the given text is not present.
     */
    private void assertValidIssueDescription(final String descriptionToEnter, final String assertNotPresent) {
        final String issueKey = backdoor.issues().createIssue(PROJECT_DOG_KEY, "testing cross site scripting", ADMIN_USERNAME, "Major", "Bug").key();
        backdoor.issues().setIssueFields(issueKey, new IssueFields().description(descriptionToEnter));
        navigation.issue().gotoIssue(issueKey);
        tester.assertTextNotPresent(assertNotPresent);
    }

    // Create an issue link type 'Duplicate' - should be only called once!
    private void createIssueLinkType() {
        administration.issueLinking().enable();
        tester.setFormElement("name", "Duplicate");
        tester.setFormElement("outward", "is a duplicate of");
        tester.setFormElement("inward", "duplicates");
        tester.submit();
    }

    private void validateNoWikiRendererForField(final String field, final String fieldDisplayName, final String issueKey) {
        tester.assertTextNotPresent("<dd>" + field + "-preview_link</dd>");
        tester.assertTextNotPresent("<dd>" + fieldDisplayName + "</dd>");
        tester.assertTextNotPresent("<dd>" + issueKey + "</dd>");
    }

    private void validateWikiRendererForField(final String field, final String fieldDisplayName, final String issueKey) {
        tester.assertTextPresent("<dd>" + field + "-preview_link</dd>");
        tester.assertTextPresent("<dd>" + fieldDisplayName + "</dd>");
        tester.assertTextPresent("<dd>" + issueKey + "</dd>");
    }

    public void setFieldConfigurationFieldToRenderer(final String configuration, final String fieldId, final String renderer) {
        setFieldConfigurationFieldToRenderer(configuration, fieldId, renderer, false);
    }

    public void setFieldConfigurationFieldToRenderer(final String configuration, final String fieldId,
                                                     final String renderer, final boolean assertWarningNotPresent) {
        gotoFieldLayoutConfiguration(configuration);
        tester.clickLink("renderer_" + fieldId);
        tester.assertTextPresent("Edit Field Renderer");
        tester.selectOption("selectedRendererType", renderer);
        if (assertWarningNotPresent) {
            tester.assertTextNotPresent("Changing the renderer will effect the display of all ");
        }
        tester.submit("Update");
        tester.assertTextPresent("Edit Field Renderer Confirmation");
        tester.assertTextPresent(renderer);
        tester.submit("Update");
        logger.log("Set " + fieldId + " to renderer type " + renderer + " in the " + configuration + " configuration.");
    }

    public void gotoFieldLayoutConfiguration(final String configuration) {
        navigation.gotoAdminSection(Navigation.AdminSection.FIELD_CONFIGURATION);
        tester.clickLink("configure-" + configuration);
        tester.assertTextPresent(configuration);
    }

    public void addFieldsToFieldScreen(final String screenName, final String[] fieldNames) {
        for (final String fieldName : fieldNames) {
            backdoor.screens().addFieldToScreen(screenName, fieldName);
        }
    }
}
