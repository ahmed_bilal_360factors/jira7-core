package com.atlassian.jira.webtests.ztests.bundledplugins2.streams;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Functional tests for Activity Streams
 *
 * @since v6.4
 */
@WebTest({Category.FUNC_TEST, Category.ACTIVITY_STREAMS})
public class TestActivityStreams extends BaseJiraFuncTest {
    private final HttpClient client = new HttpClient();

    /*
     * A test for JRA-42660, checking that Content-Encoding either not present
     * or not empty if gzip is accepted
     */
    @Test
    public void contentEncodingIsNotEmpty() throws Exception {
        final GetMethod request = new GetMethod(tester.getTestContext().getBaseUrl() +
                "plugins/servlet/streams?maxResults=10&relativeLinks=true");
        request.setRequestHeader("Accept-Encoding", "gzip");
        final int status = client.executeMethod(request);
        assertEquals(HttpStatus.SC_OK, status);
        Header responseHeader = request.getResponseHeader("Content-Encoding");
        if (responseHeader == null) {
            return;
        }
        assertNotNull("It's not possible for a header to have a NULL value", responseHeader.getValue());
        assertThat(responseHeader.getValue(), not(equalTo("")));
    }
}