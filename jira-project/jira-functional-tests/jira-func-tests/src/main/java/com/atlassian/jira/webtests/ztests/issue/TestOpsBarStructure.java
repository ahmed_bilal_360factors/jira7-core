package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@WebTest({Category.FUNC_TEST, Category.ISSUES})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestOpsBarStructure extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("TestOpsBar.xml");
    }

    @Test
    public void testWorkflows() {
        // may be invoked with reference plugin - this adds a reference Action
        // there will therefore be an extra transition

        navigation.issue().viewIssue("HSP-1");
        assertExtraTransitionsExist();

        navigation.issue().viewIssue("HSP-2");
        assertOnlyReferenceTransitionExists();

        navigation.issue().viewIssue("HSP-3");
        assertOnlyReferenceTransitionExists();

        navigation.issue().viewIssue("HSP-4");
        assertOnlyReferenceTransitionExists();
    }

    private void assertExtraTransitionsExist() {
        assertions.assertNodeDoesNotExist("//span[@id='opsbar-transitions_more']");
        assertions.assertNodeExists("//a[@id='opsbar-transitions_more']");
    }

    private boolean referencePluginEnabled() {
        return new XPathLocator(tester, "//a[@id='reference-operation']").exists();
    }

    private void checkReferencePluginIsEnabled() {
        if (!referencePluginEnabled()) {
            // this causes confusion between CI and local running. Force people to run with the RP.
            fail("You need to have the reference plugin installed when you run this test. To run JIRA with the ref plugin user './jmake debug -rp'");
        }
    }

    private void assertOnlyReferenceTransitionExists() {
        checkReferencePluginIsEnabled();
        assertions.assertNodeExists(new CssLocator(tester, "div.aui-dropdown2-section > ul > li > #reference-operation"));
    }

    @Test
    public void testConjoined() {
        navigation.issue().viewIssue("HSP-5");

        String editGroup = "//div[@class='ops-menus aui-toolbar']/div/ul[1]";
        assertions.assertNodeHasText(editGroup + "/li[1]", "Edit");

        String commentGroup = "//div[@class='ops-menus aui-toolbar']/div/ul[2]";
        assertions.assertNodeHasText(commentGroup + "/li[1]", "Comment");

        String operationsGroup = "//div[@class='ops-menus aui-toolbar']/div/ul[3]";
        assertions.assertNodeHasText(operationsGroup + "/li[1]", "Assign");
        assertions.assertNodeDoesNotHaveText(operationsGroup + "/li[1]", "More");
        assertions.assertNodeByIdExists("opsbar-operations_more");
    }

    @Test
    public void testLoginButton() {
        navigation.logout();
        navigation.issue().viewIssue("ANONED-1");

        IdLocator locator = new IdLocator(tester, "ops-login-lnk");
        assertEquals(0, locator.getNodes().length);

        navigation.issue().viewIssue("ANON-1");

        locator = new IdLocator(tester, "ops-login-lnk");
        assertEquals(1, locator.getNodes().length);

        tester.clickLink("ops-login-lnk");

        tester.setFormElement("os_username", ADMIN_USERNAME);
        tester.setFormElement("os_password", ADMIN_USERNAME);
        tester.setWorkingForm("login-form");
        tester.submit();

        textAssertions.assertTextPresent(new CssLocator(tester, "#content header h1"), "Anon viewable issue");

        locator = new IdLocator(tester, "ops-login-lnk");
        assertEquals(0, locator.getNodes().length);
    }

    @Test
    public void testOpsbarTransitionOrder() {
        final String workflowName = "Copy of jira";
        administration.restoreData("TestOpsbarTransitionOrder.xml");

        navigation.issue().viewIssue("HSP-1");
        final String operationsGroup = "//div[@class='ops-menus aui-toolbar']/div/ul[4]";
        final String[] originalOrder = {"Start Progress", "Resolve Issue", "Close Issue"};
        final String[] newOrder = {"Close Issue", "Resolve Issue", "Start Progress"};
        assertTransitionOrder(operationsGroup, originalOrder);

        //now change the order!
        //go to workflows admin section
        //create a draft workflow
        administration.workflows().goTo().createDraft(workflowName);

        //change the order of start progress to 100.
        backdoor.workflow().setTransitionProperty(workflowName, true, 4, "opsbar-sequence", 100);

        //change the order of Close issue to 20
        backdoor.workflow().setTransitionProperty(workflowName, true, 2, "opsbar-sequence", 20);

        //now publish the edited workflow
        administration.workflows().goTo().publishDraft(workflowName).publish();

        //go back to the issue and assert the order has changed!
        navigation.issue().viewIssue("HSP-1");
        assertTransitionOrder(operationsGroup, newOrder);

    }

    private void assertTransitionOrder(final String operationsGroup, final String[] expectedText) {
        assertions.getTextAssertions().assertTextSequence(new XPathLocator(tester, operationsGroup), expectedText);
    }
}
