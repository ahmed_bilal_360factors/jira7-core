package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v3.13.3
 */
@WebTest({Category.FUNC_TEST, Category.FIELDS})
@LoginAs(user = ADMIN_USERNAME)
public class TestDefaultTextRenderer extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Test
    public void testIssueLinksWithMultipleKeysInsideUrl() throws Exception {
        administration.restoreBlankInstance();
        String linkKey = navigation.issue().createIssue("homosapien", "Bug", "Test issue to link to");
        final String badUrl = "http://example/" + linkKey + "/" + linkKey;

        String testKey = navigation.issue().createIssue("homosapien", "Bug", "Test issue to test");
        navigation.issue().viewIssue(testKey);
        tester.clickLink("edit-issue");
        tester.setFormElement("environment", "Some bad stuff " + badUrl + " and more");
        tester.submit("Update");
        assertions.getLinkAssertions().assertLinkLocationEndsWith(badUrl, badUrl);
    }
}
