package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Raw-database releated tests.
 *
 * @since 6.3
 */
public class FuncTestSuiteDatabase {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.database", true))
                .build();
    }
}
