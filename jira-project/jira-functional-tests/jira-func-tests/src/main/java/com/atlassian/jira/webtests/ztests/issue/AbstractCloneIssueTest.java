package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.testkit.client.log.FuncTestOut;

import java.io.IOException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.CLONERS_LINK_TYPE_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.LINK_CLONE_ISSUE;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Common parentfor clone issue func tests.
 *
 * @since v7.1.1
 */
public abstract class AbstractCloneIssueTest extends BaseJiraFuncTest {

    protected void gotoIssueAndClickClone(String issueKey) {
        navigation.issue().gotoIssue(issueKey);
        tester.clickLink(LINK_CLONE_ISSUE);
    }

    protected String cloneIssue(String projectKey, String summary, boolean cloneLinkPresent) {
        summary = TestCloneIssueWithSubTasks.CLONE_SUMMARY_PREFIX + summary;
        tester.assertFormElementEquals(TestCloneIssueWithSubTasks.SUMMARY_FIELD_NAME, summary);

        if (cloneLinkPresent) {
            tester.assertTextNotPresent("The clone link type \"" + CLONERS_LINK_TYPE_NAME + "\" does not exist. A link to the original issue will not be created.");
        } else {
            tester.assertTextPresent("The clone link type \"" + CLONERS_LINK_TYPE_NAME + "\" does not exist. A link to the original issue will not be created.");
        }

        tester.submit();

        waitForProgress();
        // Ensure we are on the browse issue page
        assertTrue(tester.getDialog().getResponse().getURL().getPath().contains("/browse"));

        // Check that the the issue was cloned properly
        tester.assertTextPresent(summary);

        try {
            return extractIssueKey(projectKey);
        } catch (Exception e) {
            fail("Unable to retrieve issue key" + e.getMessage());
            return "fail";
        }
    }

    protected void waitForProgress() {
        // Wait for the progress page to finish if it is present
        final int maxWaitTimeMs = 2 * 60 * 1000;
        final int snoozeMs = 100;
        int timeWaitedMs = 0;
        while (true) {
            if (!tester.getDialog().hasSubmitButton("Refresh")) {
                break;
            }
            if (timeWaitedMs > maxWaitTimeMs) {
                throw new RuntimeException("Waiting timed out.");
            }
            try {
                Thread.sleep(snoozeMs);
                timeWaitedMs += snoozeMs;
                tester.submit("Refresh");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private String extractIssueKey(String projectKey) throws IOException {
        String text;
        String issueKey;
        text = tester.getDialog().getResponse().getText();
        int projectIdLocation = text.indexOf("[" + projectKey) + 1;
        int endOfIssueKey = text.indexOf("]", projectIdLocation);
        issueKey = text.substring(projectIdLocation, endOfIssueKey);
        if (!issueKey.matches(projectKey + "-\\d+")) {
            fail("Invalid issue key: " + issueKey);
        }
        FuncTestOut.log("issueKey = " + issueKey);
        return issueKey;
    }
}
