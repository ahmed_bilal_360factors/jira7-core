package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @since v6.1
 */
@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.INDEXING})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestEditIssueOnIndexing extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
    }

    @Test
    public void testUpdatedTimeInDBInSyncWithIndex() throws Exception {
        backdoor.issues().createIssue("MKY", "Test issue");
        waitForSecondSinceUpdatedFieldIsStoredInIndexWithPerSecondResolution();
        backdoor.issues().setDescription("MKY-1", "Test description!");
        assertThat(backdoor.indexing().isIndexUpdatedFieldConsistent(), equalTo(true));

        waitForSecondSinceUpdatedFieldIsStoredInIndexWithPerSecondResolution();
        //we make edit that does not change contents of issue and expect that index will be still in sync with db
        backdoor.issues().setDescription("MKY-1", "Test description!");

        assertThat(backdoor.indexing().isIndexUpdatedFieldConsistent(), equalTo(true));
    }

    private void waitForSecondSinceUpdatedFieldIsStoredInIndexWithPerSecondResolution() throws InterruptedException {
        // dates only have second precision in lucene. so wait a 1.1 seconds to make sure we don't
        // hit that window.
        Thread.sleep(1100);
    }

}
