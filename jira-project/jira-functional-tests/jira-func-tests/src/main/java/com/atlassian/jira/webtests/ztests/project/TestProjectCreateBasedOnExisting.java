package com.atlassian.jira.webtests.ztests.project;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.testkit.beans.ProjectSchemesBean;
import com.atlassian.jira.testkit.client.restclient.Project;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/*
* This creates a "Custom (CUS)" project with custom:
* - Permission scheme (10000)
* - Notification Scheme (10010)
* - Workflow Scheme (10100)
* - Screen Scheme
* - Issue Level security scheme (10000)
* - Field configuration scheme (10000)
* - Issue Type scheme (10010)
*/
@Restore("TestProjectCreateBasedOnExisting.xml")
@WebTest({Category.FUNC_TEST, Category.PROJECTS})
public class TestProjectCreateBasedOnExisting extends BaseJiraFuncTest {
    private static final String EXISTING_PROJECT_KEY = "CUS";

    @Before
    public void setUp() throws Exception {
        //the project type upgrade task changes the project type to software currently.
        //once the test data is upgraded beyond upgrade task 70101 this can be removed.
        final Project existingProject = backdoor.project().getProject(EXISTING_PROJECT_KEY);
        backdoor.project().updateProjectType(Long.valueOf(existingProject.id), new ProjectTypeKey("business"));
    }

    @Test
    public void testSchemesAreSharedForNewProject() {
        final String newProjectKey = backdoor.project().addProjectBasedOnExisting(EXISTING_PROJECT_KEY,
                "SHRD", "Shared Project", "admin");

        Project project = backdoor.project().getProject(newProjectKey);
        ProjectSchemesBean schemes = backdoor.project().getSchemes(newProjectKey);

        assertThat(project.id, is("10110"));
        assertThat(schemes.permissionScheme.id, is(10000L));
        assertThat(schemes.notificationScheme.id, is(10010L));
        assertThat(schemes.workflowScheme.getId(), is(10100L));
        assertThat(schemes.issueSecurityScheme.id, is(10000L));
        assertThat(schemes.issueTypeScheme.id, is(10010L));
        assertThat(schemes.fieldConfigurationScheme.id, is(10000L));
    }
}
