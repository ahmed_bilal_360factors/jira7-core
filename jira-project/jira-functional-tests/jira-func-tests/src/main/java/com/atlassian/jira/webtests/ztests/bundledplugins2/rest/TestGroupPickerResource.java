package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.GroupPickerClient;
import com.atlassian.jira.testkit.client.restclient.GroupSuggestion;
import com.atlassian.jira.testkit.client.restclient.GroupSuggestions;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Func test for GroupPickerResource
 *
 * @since v4.4
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
public class TestGroupPickerResource extends BaseJiraFuncTest {
    private GroupPickerClient groupPickerClient;

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        groupPickerClient = new GroupPickerClient(environmentData);
    }

    @Test
    @RestoreBlankInstance
    public void testGroupSuggestionsOrder() {
        administration.usersAndGroups().addGroup("jara-users");
        administration.usersAndGroups().addGroup("zara-users");

        List<GroupSuggestion> expectedSuggestions = Lists.newArrayList(
                new GroupSuggestion().name("jara-users").html("jara-<b>users</b>"),
                new GroupSuggestion().name("jira-users").html("jira-<b>users</b>"),
                new GroupSuggestion().name("zara-users").html("zara-<b>users</b>")
        );

        GroupSuggestions suggestions = groupPickerClient.get("users");

        Assert.assertEquals("Showing 3 of 3 matching groups", suggestions.header);
        Assert.assertEquals(expectedSuggestions, suggestions.groups);
    }

    @Test
    @RestoreBlankInstance
    public void testGroupSuggestionsAreHtmlEncoded() {
        administration.usersAndGroups().addGroup("<script>alert('wtf')</script>");

        List<GroupSuggestion> expectedSuggestions = Lists.newArrayList(
                new GroupSuggestion().name("<script>alert('wtf')</script>").html("&lt;script&gt;alert(&#39;<b>wtf</b>&#39;)&lt;/script&gt;")
        );

        GroupSuggestions suggestions = groupPickerClient.get("wtf");

        Assert.assertEquals("Showing 1 of 1 matching groups", suggestions.header);
        Assert.assertEquals(expectedSuggestions, suggestions.groups);
    }

    @Test
    @RestoreBlankInstance
    public void testGroupSuggestionsNoQueryString() {
        List<GroupSuggestion> expectedSuggestions = Lists.newArrayList(
                new GroupSuggestion().name("jira-administrators").html("jira-administrators"),
                new GroupSuggestion().name("jira-developers").html("jira-developers"),
                new GroupSuggestion().name("jira-users").html("jira-users")
        );

        GroupSuggestions suggestions = groupPickerClient.get(null);

        Assert.assertEquals("Showing 3 of 3 matching groups", suggestions.header);
        Assert.assertEquals(expectedSuggestions, suggestions.groups);
    }

    @Test
    @RestoreBlankInstance
    public void testFindGroupsWithNoMatch() {
        GroupSuggestions suggestions = groupPickerClient.get("lalala");

        Assert.assertEquals("Showing 0 of 0 matching groups", suggestions.header);
        Assert.assertEquals(Collections.<GroupSuggestion>emptyList(), suggestions.groups);
    }

    @Test
    @Restore("TestGroupPickerResource.xml")
    public void testFindGroupsExcessResults() {
        // What I would give for some way to change the application properties
        GroupSuggestions suggestions = groupPickerClient.get("z");

        List<GroupSuggestion> expectedSuggestions = Lists.newArrayList();
        for (int i = 0; i < 20; ++i) {
            String expectedName = "z" + String.format("%02d", i);
            String expectedHtml = "<b>z</b>" + String.format("%02d", i);
            expectedSuggestions.add(new GroupSuggestion().name(expectedName).html(expectedHtml));
        }

        Assert.assertEquals("Showing 20 of 21 matching groups", suggestions.header);
        Assert.assertEquals(expectedSuggestions, suggestions.groups);

    }
}
