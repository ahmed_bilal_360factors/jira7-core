package com.atlassian.jira.functest.framework.assertions;

import com.google.inject.ImplementedBy;

/**
 * Assertions for sidebar-related content
 *
 * @since v7.1
 */
@ImplementedBy(SidebarAssertionsImpl.class)
public interface SidebarAssertions {
    /**
     * Checks if provided project name matches what was rendered in the sidebar
     *
     * @param name project name
     */
    void assertProjectName(String name);
}
