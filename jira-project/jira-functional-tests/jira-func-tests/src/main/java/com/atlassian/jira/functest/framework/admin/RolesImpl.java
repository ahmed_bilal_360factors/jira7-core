package com.atlassian.jira.functest.framework.admin;

import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.NavigationImpl;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.testkit.client.log.FuncTestLoggerImpl;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.atlassian.jira.testkit.client.restclient.ProjectClient;
import com.atlassian.jira.testkit.client.restclient.ProjectRoleClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;
import org.junit.Assert;

import javax.inject.Inject;
import java.util.List;

public class RolesImpl implements Roles {
    private static final String DELETE_ROLE = "/secure/project/DeleteProjectRole!default.jspa?id=";
    private static final String ADD_ROLE = "/secure/project/UserRoleActorAction!addUsers.jspa?projectRoleId=";

    private final WebTester tester;
    private final JIRAEnvironmentData environmentData;
    private final RoleDetails roleDetails;
    private final FuncTestLogger logger;
    private Navigation navigation;

    public RolesImpl(WebTester tester, JIRAEnvironmentData environmentData, int logIndentLevel) {
        this(tester, environmentData);
    }

    @Inject
    public RolesImpl(WebTester tester, JIRAEnvironmentData environmentData) {
        this.tester = tester;
        this.environmentData = environmentData;
        roleDetails = new DefaultRoleDetails(tester);
        this.logger = new FuncTestLoggerImpl(2);
        this.navigation = null;
    }

    public void delete(long roleId) {
        logger.log("Deleting project role " + roleId);
        tester.gotoPage(DELETE_ROLE + roleId);
        tester.submit("Delete");
    }

    public void delete(final String name) {
        logger.log("Deleting project role:" + name);

        gotoProjectRolesScreen();

        tester.clickLink("delete_" + name);
        tester.submit("Delete");
    }

    private void gotoProjectRolesScreen() {
        getNavigation().gotoAdminSection(Navigation.AdminSection.PROJECT_ROLE_BROWSER);
    }

    private Navigation getNavigation() {
        if (navigation == null) {
            navigation = new NavigationImpl(tester, environmentData);
        }
        return navigation;
    }

    public void create(String name, String description) {
        gotoProjectRolesScreen();

        tester.setFormElement("name", name);
        tester.setFormElement("description", description);
        tester.submit("Add Project Role");
        tester.assertTextPresent(name);
        tester.assertTextPresent(description);
    }

    public RoleDetails edit(String name) {
        gotoProjectRolesScreen();
        tester.clickLink("edit_" + name);
        return roleDetails;
    }

    private com.atlassian.jira.testkit.client.restclient.Project getProjectByName(String projectName) {
        ProjectClient projectClient = new ProjectClient(environmentData);

        final List<com.atlassian.jira.testkit.client.restclient.Project> projects = projectClient.getProjects();
        for (Project project : projects) {
            if (project.name.equals(projectName)) {
                return project;
            }
        }
        return null;

    }

    private void addUserToProjectRole(String userName, String projectName, String roleName) {
        final Project projectByName = getProjectByName(projectName);

        if (projectByName == null) {
            Assert.fail("A project with the name '" + projectName + "' does not exist.");
        }

        ProjectRoleClient projectRoleClient = new ProjectRoleClient(environmentData);
        final Response response = projectRoleClient.addActors(projectByName.key, roleName, null, new String[]{userName});
    }

    public void addProjectRoleForUser(String projectName, String roleName, String userName) {
        addUserToProjectRole(userName, projectName, roleName);
    }
}