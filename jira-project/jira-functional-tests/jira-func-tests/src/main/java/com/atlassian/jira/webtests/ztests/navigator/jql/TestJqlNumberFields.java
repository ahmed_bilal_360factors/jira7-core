package com.atlassian.jira.webtests.ztests.navigator.jql;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * @since v6.4
 */
@WebTest({Category.FUNC_TEST, Category.JQL})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestJqlNumberFields.xml")
public class TestJqlNumberFields extends BaseJiraFuncTest {

    @Inject
    private IssueTableAssertions jql;

    @Before
    public void setUp() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    public void testNumberField() {
        // Test sequence
        jql.assertOrderedSearchWithResults("order by testnumberfield DESC", "HSP-7", "HSP-3", "HSP-2", "HSP-1", "HSP-8", "HSP-6", "HSP-9", "HSP-4", "HSP-5");
        jql.assertOrderedSearchWithResults("order by testnumberfield ASC", "HSP-5", "HSP-4", "HSP-9", "HSP-6", "HSP-8", "HSP-1", "HSP-2", "HSP-3", "HSP-7");
        // Test equality
        jql.assertSearchWithResults("testnumberfield = 40", "HSP-3");
        jql.assertSearchWithResults("testnumberfield = -50", "HSP-5");
        jql.assertSearchWithResults("testnumberfield = -12.1", "HSP-4");
        jql.assertSearchWithResults("testnumberfield = 0", "HSP-6");
        jql.assertSearchWithResults("testnumberfield = 102");
        jql.assertSearchWithResults("testnumberfield = -102");
        jql.assertSearchWithResults("testnumberfield IN (40, -1)", "HSP-9", "HSP-3");

        // Test inequality
        jql.assertSearchWithResults("testnumberfield != 40", "HSP-9", "HSP-8", "HSP-6", "HSP-5", "HSP-4", "HSP-2", "HSP-1");
        jql.assertSearchWithResults("testnumberfield != -50", "HSP-9", "HSP-8", "HSP-6", "HSP-4", "HSP-3", "HSP-2", "HSP-1");
        jql.assertSearchWithResults("testnumberfield != -12.1", "HSP-9", "HSP-8", "HSP-6", "HSP-5", "HSP-3", "HSP-2", "HSP-1");

        // Test range queries
        jql.assertSearchWithResults("testnumberfield > 0", "HSP-8", "HSP-3", "HSP-2", "HSP-1");
        jql.assertSearchWithResults("testnumberfield < 0", "HSP-9", "HSP-5", "HSP-4");
        jql.assertSearchWithResults("testnumberfield > 20", "HSP-3", "HSP-2");
        jql.assertSearchWithResults("testnumberfield > 30", "HSP-3");
        jql.assertSearchWithResults("testnumberfield >=30", "HSP-3", "HSP-2");
        jql.assertSearchWithResults("testnumberfield < -10", "HSP-5", "HSP-4");
        jql.assertSearchWithResults("testnumberfield <= -12", "HSP-5", "HSP-4");
        jql.assertSearchWithResults("testnumberfield <= -12.5", "HSP-5");
        jql.assertSearchWithResults("testnumberfield > -15", "HSP-9", "HSP-8", "HSP-6", "HSP-4", "HSP-3", "HSP-2", "HSP-1");
        jql.assertSearchWithResults("testnumberfield <= -1", "HSP-9", "HSP-5", "HSP-4");
        jql.assertSearchWithResults("testnumberfield > -1", "HSP-8", "HSP-6", "HSP-3", "HSP-2", "HSP-1");
    }

}
