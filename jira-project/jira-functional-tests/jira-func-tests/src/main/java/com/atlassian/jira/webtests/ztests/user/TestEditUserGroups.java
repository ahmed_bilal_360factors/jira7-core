package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.framework.util.form.FormParameterUtil;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.webtests.Groups;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SYS_ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SYS_ADMIN_USERNAME;
import static com.atlassian.jira.permission.GlobalPermissionKey.SYSTEM_ADMIN;
import static org.junit.Assert.assertFalse;

/**
 * Tests user group add/remove.
 *
 * @since v3.12
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
@LoginAs(user = ADMIN_USERNAME)
public class TestEditUserGroups extends BaseJiraFuncTest {
    public static final String ERROR_LEAVING_ALL_ADMIN_GROUPS = "You are trying to leave all of the administration groups jira-administrators. You cannot delete your own administration permission";
    public static final String ERROR_LEAVING_ALL_SYS_ADMIN_GROUPS = "You are trying to leave all of the system administration groups jira-administrators. You cannot delete your own system administration permission";

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Test
    @RestoreBlankInstance
    public void testEditUserGroupsJoinAndLeaveAtSameTime() throws SAXException {
        navigateToUser(ADMIN_USERNAME);
        tester.clickLink("editgroups_link");
        tester.selectOption("groupsToLeave", Groups.DEVELOPERS);
        tester.submit("leave");
        tester.assertRadioOptionValuePresent("groupsToLeave", "jira-administrators");
        tester.assertRadioOptionValuePresent("groupsToLeave", "jira-users");

        // Now go back and select to join while some to leave are selectgged, we should only join
        tester.selectOption("groupsToLeave", Groups.USERS);
        final FormParameterUtil form = new FormParameterUtil(tester, "user-edit-groups", "join");
        form.addOptionToHtmlSelect("groupsToJoin", new String[]{Groups.DEVELOPERS});
        form.setFormElement("groupsToJoin", Groups.DEVELOPERS);
        form.submitForm();
        tester.getDialog().hasRadioOptionValue("groupsToLeave", "jira-developers");
        tester.getDialog().hasRadioOptionValue("groupsToLeave", "jira-administrators");
        tester.getDialog().hasRadioOptionValue("groupsToLeave", "jira-users");
    }

    @Test
    @RestoreBlankInstance
    public void testEditUserGroupsRemoveLastSysAdminGroup() {
        navigateToUser(ADMIN_USERNAME);
        tester.clickLink("editgroups_link");
        tester.selectOption("groupsToLeave", Groups.ADMINISTRATORS);
        tester.submit("leave");
        tester.assertTextPresent(ERROR_LEAVING_ALL_SYS_ADMIN_GROUPS);
        tester.assertRadioOptionValuePresent("groupsToLeave", Groups.ADMINISTRATORS);
    }

    @Test
    @RestoreBlankInstance
    public void testEditUserGroupsRemoveSysAdminGroupWithAnotherPresent() {
        administration.usersAndGroups().addGroup("sys-admin-group2");
        administration.addGlobalPermission(SYSTEM_ADMIN, "sys-admin-group2");
        administration.usersAndGroups().addUserToGroup(ADMIN_USERNAME, "sys-admin-group2");

        administration.usersAndGroups().removeUserFromGroup(ADMIN_USERNAME, Groups.ADMINISTRATORS);
    }

    @Test
    @Restore("TestWithSystemAdmin.xml")
    public void testEditUserGroupsWithNoSysAdminPermRemoveLastAdmin() {
        try {
            navigateToUser(ADMIN_USERNAME);
            tester.clickLink("editgroups_link");
            tester.selectOption("groupsToLeave", Groups.ADMINISTRATORS);
            tester.submit("leave");
            tester.assertTextPresent(ERROR_LEAVING_ALL_ADMIN_GROUPS);
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    @Restore("TestWithSystemAdmin.xml")
    public void testEditUserGroupsWithNoSysAdminPermRemoveAdmin() {
        try {
            administration.usersAndGroups().addGroup("admin-group2");
            administration.addGlobalPermission(GlobalPermissionKey.ADMINISTER, "admin-group2");
            administration.usersAndGroups().addUserToGroup(ADMIN_USERNAME, "admin-group2");

            administration.usersAndGroups().removeUserFromGroup(ADMIN_USERNAME, Groups.ADMINISTRATORS);
            assertFalse(tester.getDialog().hasRadioOptionValue("groupsToLeave", "jira-administrators"));
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    @Restore("TestWithSystemAdmin.xml")
    @LoginAs(user = SYS_ADMIN_USERNAME)
    public void testSysAdminEditGroups() {
        try {
            // admin should not be in sysadmins group
            navigateToUser(ADMIN_USERNAME);
            tester.clickLink("editgroups_link");
            tester.assertRadioOptionValueNotPresent("groupsToLeave", "jira-sys-admins");

            navigateToUser(SYS_ADMIN_USERNAME);
            tester.clickLink("editgroups_link");
            tester.assertRadioOptionValuePresent("groupsToLeave", "jira-sys-admins");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    @Restore("TestWithSystemAdmin.xml")
    public void testAdminEditGroups() {
        try {
            // admin should not be in sysadmins group
            navigateToUser(ADMIN_USERNAME);
            tester.clickLink("editgroups_link");
            assertFalse(tester.getDialog().hasRadioOptionValue("groupsToJoin", "jira-sys-admins"));
            tester.assertRadioOptionValueNotPresent("groupsToLeave", "jira-sys-admins");

            // validate that I can't fudge the url to add myself to the sys admins
            tester.gotoPage(page.addXsrfToken("/secure/admin/user/EditUserGroups.jspa?name=" + ADMIN_USERNAME + "&groupsToJoin=jira-sys-admins&join=true"));
            tester.assertTextPresent("You cannot add users to groups which are not visible to you.");

            // validate that as admin I can't see the sys-admin groups
            navigateToUser(SYS_ADMIN_USERNAME);
            tester.clickLink("editgroups_link");
            assertFalse(tester.getDialog().hasRadioOptionValue("groupsToJoin", "jira-sys-admins"));
            tester.assertRadioOptionValueNotPresent("groupsToLeave", "jira-sys-admins");

            // Cheat and try to remove the sys-admin group by url
            tester.gotoPage(page.addXsrfToken("/secure/admin/user/EditUserGroups.jspa?name=root&groupsToLeave=jira-sys-admins&leave=true"));
            tester.assertTextPresent("You can not remove a group from this user as it is not visible to you.");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    @Restore("TestWithSystemAdmin.xml")
    // Be the user that has no perms
    @LoginAs(user = FRED_USERNAME)
    public void testFredEditGroups() {
        try {
            // validate that I can't fudge the url to add myself to the sys admins
            tester.gotoPage(page.addXsrfToken("/secure/admin/user/EditUserGroups.jspa?name=" + ADMIN_USERNAME + "&groupsToJoin=jira-sys-admins&join=true"));
            tester.assertTextPresent("my login on this computer");

            // Cheat and try to remove the sys-admin group by url
            tester.gotoPage(page.addXsrfToken("/secure/admin/user/EditUserGroups.jspa?name=root&groupsToLeave=jira-sys-admins&leave=true"));
            tester.assertTextPresent("my login on this computer");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    @RestoreBlankInstance
    public void testEditGroupsUserDoesNotExist() {
        tester.gotoPage(page.addXsrfToken("/secure/admin/user/EditUserGroups.jspa?name=foo&groupsToJoin=jira-developers&join=true"));
        assertions.getJiraFormAssertions().assertFormErrMsg("This user does not exist please select a user from the user browser.");

        tester.gotoPage(page.addXsrfToken("/secure/admin/user/EditUserGroups.jspa?name=foo&groupsToLeave=jira-developers&leave=true"));
        assertions.getJiraFormAssertions().assertFormErrMsg("This user does not exist please select a user from the user browser.");
    }

    @Test
    @RestoreBlankInstance
    public void testEditGroupsGroupDoesNotExist() {
        tester.gotoPage(page.addXsrfToken("/secure/admin/user/EditUserGroups.jspa?name=admin&groupsToJoin=invalid&join=true"));
        assertions.getJiraFormAssertions().assertFormErrMsg("The group 'invalid' is not a valid group.");

        tester.gotoPage(page.addXsrfToken("/secure/admin/user/EditUserGroups.jspa?name=admin&groupsToLeave=invalid&leave=true"));
        assertions.getJiraFormAssertions().assertFormErrMsg("The group 'invalid' is not a valid group.");
    }

    @Test
    @RestoreBlankInstance
    public void testEditGroupsCanNotJoinAlreadyAMemeber() {
        tester.gotoPage(page.addXsrfToken("/secure/admin/user/EditUserGroups.jspa?name=admin&groupsToJoin=jira-administrators&join=true"));
        assertions.getJiraFormAssertions().assertFormErrMsg("Cannot add user 'admin', user is already a member of 'jira-administrators'");
    }

    @Test
    @RestoreBlankInstance
    public void testEditGroupsCanNotLeaveNotAMemeber() {
        tester.gotoPage(page.addXsrfToken("/secure/admin/user/EditUserGroups.jspa?name=fred&groupsToLeave=jira-administrators&leave=true"));
        assertions.getJiraFormAssertions().assertFormErrMsg("Cannot remove user 'fred' from group 'jira-administrators' since user is not a member of 'jira-administrators'");
    }

    @Test
    @RestoreBlankInstance
    public void testEditGroupsMustSelectAGroup() {
        tester.gotoPage(page.addXsrfToken("/secure/admin/user/EditUserGroups.jspa?name=fred"));
        // note: nothing is selected
        tester.submit("join");
        tester.assertTextPresent("You must select at least one group to join.");
        tester.submit("leave");
        tester.assertTextPresent("You must select at least one group to leave.");
    }

    private void navigateToUser(final String username) {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.clickLink(username);
    }
}
