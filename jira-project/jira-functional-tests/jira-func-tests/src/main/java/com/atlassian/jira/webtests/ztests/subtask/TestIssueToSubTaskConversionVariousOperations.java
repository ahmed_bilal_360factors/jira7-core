package com.atlassian.jira.webtests.ztests.subtask;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;

/**
 * This test ensures that an issue is inserted in the correct order in a subtask.
 * There are also a number of test cases to ensure canceling the subtask to issue conversion works.
 * <p/>
 * It also tests that the appropriate warning is shown if you do not have browse permission for an issue.
 */
@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.SUB_TASKS})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestIssueToSubtaskConversionVariousOperations.xml")
public class TestIssueToSubTaskConversionVariousOperations extends BaseJiraFuncTest {

    @Inject
    SubTaskAssertions subTaskAssertions;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testAddNewSubtaskToExistingIssueWithSubtask() {
        navigation.issue().gotoIssue("HSP-2");

        //check that the issue already has some subtasks
        tester.assertTextPresent("Sub-Tasks");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "1.", "My sub-task summary");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "2.", "Second subtask");
        tester.assertTextNotPresent("A new issue");

        // Now lets convert issue HSP-1 to be a subtask of HSP-2...
        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("issue-to-subtask");
        tester.setFormElement("parentIssueKey", "HSP-2");
        tester.submit("Next >>");
        tester.selectOption("components", "New Component 1");
        tester.submit("Next >>");
        tester.submit("Finish");

        //go back to the original issue and ensure it has the correct order of subtasks.
        navigation.issue().gotoIssue("HSP-2");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "1.", "My sub-task summary");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "2.", "Second subtask");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "3.", "A new issue");

        //lets create a new subtask and make sure it goes at the very end
        tester.clickLink("stqc_show");
        tester.submit("Create");
        tester.setFormElement("summary", "The new manually added subtask");
        tester.setFormElement("description", "A test Desc");
        tester.selectOption("components", "New Component 1");
        tester.selectOption("versions", "New Version 1");
        tester.submit("Create");

        navigation.issue().gotoIssue("HSP-2");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "1.", "My sub-task summary");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "2.", "Second subtask");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "3.", "A new issue");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "4.", "The new manually added subtask");
    }

    @Test
    public void testCancelFirstStep() {
        navigation.issue().gotoIssue("HSP-1");

        //go to the first step of the wizard.
        tester.clickLink("issue-to-subtask");
        tester.setFormElement("parentIssueKey", "HSP-2");

        //cancel and check we get back to the issue screen
        tester.gotoPage("/secure/ConvertIssue!cancel.jspa?id=10000");
        tester.assertTextPresent("HSP-1");
        tester.assertTextPresent("A new issue");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Type", "Bug");

        //start the convert issue operation again, and see if we can succeed
        tester.clickLink("issue-to-subtask");
        tester.setFormElement("parentIssueKey", "HSP-2");
        tester.submit("Next >>");
        tester.selectOption("components", "New Component 1");
        tester.submit("Next >>");
        tester.submit("Finish");

        textAssertions.assertTextPresent(new IdLocator(tester, "parent_issue_summary"), "HSP-2" + " A second issue");
        textAssertions.assertTextPresent(new IdLocator(tester, "key-val"), "HSP-1");
        textAssertions.assertTextPresent(new CssLocator(tester, "#content header h1"), "A new issue");
        textAssertions.assertTextPresent(new IdLocator(tester, "type-val"), "Sub-task");
    }

    @Test
    public void testCancelThirdStep() {
        navigation.issue().gotoIssue("HSP-1");

        //go to the first step of the wizard.
        tester.clickLink("issue-to-subtask");
        subTaskAssertions.assertSubTaskConversionPanelSteps("HSP-1", 1);
        tester.setFormElement("parentIssueKey", "HSP-2");

        //go to the next step (step 3)
        tester.submit("Next >>");
        subTaskAssertions.assertSubTaskConversionPanelSteps("HSP-1", 3);

        //cancel and check we get back to the issue screen
        tester.gotoPage("/secure/ConvertIssue!cancel.jspa?id=10000");
        tester.assertTextPresent("HSP-1");
        tester.assertTextPresent("A new issue");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Type", "Bug");

        //start the convert issue operation again, and see if we can succeed
        tester.clickLink("issue-to-subtask");
        tester.setFormElement("parentIssueKey", "HSP-2");
        tester.submit("Next >>");
        tester.selectOption("components", "New Component 1");
        tester.submit("Next >>");
        tester.submit("Finish");

        textAssertions.assertTextPresent(new IdLocator(tester, "parent_issue_summary"), "HSP-2" + " A second issue");
        textAssertions.assertTextPresent(new IdLocator(tester, "key-val"), "HSP-1");
        textAssertions.assertTextPresent(new CssLocator(tester, "#content header h1"), "A new issue");
        textAssertions.assertTextPresent(new IdLocator(tester, "type-val"), "Sub-task");
    }

    @Test
    public void testCancelLastStep() {
        navigation.issue().gotoIssue("HSP-1");

        //go to the first step of the wizard.
        tester.clickLink("issue-to-subtask");
        subTaskAssertions.assertSubTaskConversionPanelSteps("HSP-1", 1);
        tester.setFormElement("parentIssueKey", "HSP-2");

        //go to the next step (step 3)
        tester.submit("Next >>");
        subTaskAssertions.assertSubTaskConversionPanelSteps("HSP-1", 3);

        //go to the last step
        tester.selectOption("components", "New Component 1");
        tester.submit("Next >>");
        subTaskAssertions.assertSubTaskConversionPanelSteps("HSP-1", 4);

        //cancel and check we get back to the issue screen
        tester.gotoPage("/secure/ConvertIssue!cancel.jspa?id=10000");
        tester.assertTextPresent("HSP-1");
        tester.assertTextPresent("A new issue");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Type", "Bug");

        //start the convert issue operation again, and see if we can succeed
        tester.clickLink("issue-to-subtask");
        tester.setFormElement("parentIssueKey", "HSP-2");
        tester.submit("Next >>");
        tester.selectOption("components", "New Component 1");
        tester.submit("Next >>");
        tester.submit("Finish");
        textAssertions.assertTextPresent(new IdLocator(tester, "parent_issue_summary"), "HSP-2" + " A second issue");
        textAssertions.assertTextPresent(new IdLocator(tester, "key-val"), "HSP-1");
        textAssertions.assertTextPresent(new CssLocator(tester, "#content header h1"), "A new issue");
        textAssertions.assertTextPresent(new IdLocator(tester, "type-val"), "Sub-task");

    }

    @Test
    public void testConversionWithoutBrowsePermission() {
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);

        //lets go to the conversion operation of an issue I can't browse.
        tester.gotoPage("/secure/ConvertIssue.jspa?id=10080");

        //choose a parent issue
        tester.setFormElement("parentIssueKey", "CAT-3");
        tester.submit("Next >>");
        tester.submit("Next >>");
        tester.submit("Finish");

        //now check that the correct warning is shown about not being able to browse the converted issue.
        tester.assertTextPresent("You have successfully converted the issue (CAT-1), however you do not have the permission to view the converted issue");
    }


    @Test
    public void testConversionWithHiddenField() {
        //first set an environment.
        navigation.issue().gotoIssue("CAT-1");
        tester.clickLink("edit-issue");
        tester.setFormElement("environment", "Test environment");
        tester.submit("Update");

        //now lets hide the environment.
        navigation.gotoAdminSection(Navigation.AdminSection.FIELD_CONFIGURATION);
        tester.clickLinkWithText("Default Field Configuration");
        tester.clickLink("hide_7");

        //lets convert the issue and ensure that the environment field is not part of the list.
        navigation.issue().gotoIssue("CAT-1");
        tester.clickLink("issue-to-subtask");
        tester.setFormElement("parentIssueKey", "CAT-2");
        tester.submit("Next >>");
        tester.submit("Next >>");
        subTaskAssertions.assertSubTaskConversionPanelSteps("CAT-1", 4);
        tester.assertTextPresent("Environment");
    }
}
