package com.atlassian.jira.webtests.ztests.database;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.parser.SystemInfoParser;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isOneOf;
import static org.hamcrest.Matchers.not;

/**
 * A func test which checks if the database collation was read successfully
 *
 * @since v6.4
 */
@WebTest({Category.FUNC_TEST, Category.DATABASE})
@RestoreBlankInstance
@LoginAs(user = ADMIN_USERNAME)
public class TestDatabaseCollationReader extends BaseJiraFuncTest {

    @Inject
    private SystemInfoParser systemInfoParser;

    @Test
    public void testDatabaseCollationReadSuccessfully() {
        if (getDatabaseType().startsWith("hsql") || getDatabaseType().startsWith("h2")) {
            // Reading HSQL or H2 collations is not possible yet
            return;
        }

        String databaseCollation = systemInfoParser.getSystemInfo().getProperty("Database collation");

        assertThat("Could not read collation for database type " + getDatabaseType(), databaseCollation, not(isOneOf(null, "", "null", "Unknown")));
    }

    private String getDatabaseType() {
        return systemInfoParser.getSystemInfo().getDatabaseType();
    }
}
