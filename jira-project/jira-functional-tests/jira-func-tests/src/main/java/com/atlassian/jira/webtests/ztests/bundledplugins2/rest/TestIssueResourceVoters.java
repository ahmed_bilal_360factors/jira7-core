package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Vote;
import com.atlassian.jira.testkit.client.restclient.VotesClient;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Tests scenarios where the project has a permission scheme that sets the "View Voters and Watchers" permission to
 * check for the value of the User Picker custom field.
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestIssueResourceVoters.xml")
public class TestIssueResourceVoters extends BaseJiraFuncTest {
    private static final String ISSUE_KEY = "TP-1";
    private static final String USER_NOT_SELECTED_ON_USER_PICKER = "admin";
    private static final String USER_SELECTED_ON_USER_PICKER = "test";

    private VotesClient votesClient;

    @Before
    public void setUp() {
        votesClient = new VotesClient(getEnvironmentData());
    }

    @Test
    public void testVotersCanNotBeReadByADifferentUserThanTheOneSpecifiedOnTheUserPickerCustomField() {
        votesClient.loginAs(USER_NOT_SELECTED_ON_USER_PICKER);

        Vote vote = votesClient.get(ISSUE_KEY);

        assertTrue(vote.voters.isEmpty());
    }

    @Test
    public void testVotersCanBeReadByTheUserThatIsSpecifiedOnTheUserPickerCustomField() {
        votesClient.loginAs(USER_SELECTED_ON_USER_PICKER);

        Vote vote = votesClient.get(ISSUE_KEY);

        assertThat(vote.voters.size(), is(1));
    }
}
