package com.atlassian.jira.functest.framework.assertions;

import com.sun.jersey.api.client.UniformInterfaceException;

import javax.ws.rs.core.Response;
import java.util.concurrent.Callable;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class WebHookAssert {
    public static void assertUniformInterfaceException(final Response.Status expectedStatus, final Callable<Void> callable) {
        try {
            callable.call();
            fail("Expected uniform interface exception with status " + expectedStatus.toString());
        } catch (UniformInterfaceException e) {
            assertThat(e.getResponse().getStatus(), is(expectedStatus.getStatusCode()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
