package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.EntityPropertyClient;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.jira.util.json.JSONObject;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.REST, Category.ENTITY_PROPERTIES})
public class TestIssueResourceIssueProperties extends BaseJiraFuncTest {
    private IssueClient issueClient;
    private EntityPropertyClient propertyClient;

    @Before
    public void setUp() throws Exception {
        backdoor.restoreBlankInstance();
        issueClient = new IssueClient(environmentData);
        propertyClient = new EntityPropertyClient(environmentData, "issue");
        backdoor.usersAndGroups().addUserToGroup("fred", "jira-developers");
    }

    @Test
    public void userCanGetOneIssueProperty() {
        backdoor.issues().createIssue("HSP", "fred's issue", "admin");
        propertyClient.put("HSP-1", "prop", value(5));

        Issue issue = issueClient.getWithProperties("HSP-1", newArrayList("prop"));

        assertThat(issue.getProperty("prop"), equalTo(value(5)));
    }

    @Test
    public void onlyRequestedPropertiesAreReturned() {
        backdoor.issues().createIssue("HSP", "fred's issue", "admin");
        propertyClient.put("HSP-1", "prop1", value(1));
        propertyClient.put("HSP-1", "prop2", value(2));
        propertyClient.put("HSP-1", "prop3", value(3));

        Issue issue = issueClient.getWithProperties("HSP-1", newArrayList("prop2, prop3"));

        assertThat(issue.properties.map.keySet(), hasSize(2));
        assertThat(issue.getProperty("prop2"), equalTo(value(2)));
        assertThat(issue.getProperty("prop3"), equalTo(value(3)));
    }

    @Test
    public void invalidPropertyIsSimplyNotReturned() {
        backdoor.issues().createIssue("HSP", "fred's issue", "admin");
        propertyClient.put("HSP-1", "prop", value(5));

        Issue issue = issueClient.getWithProperties("HSP-1", newArrayList("prop, invalid"));

        assertThat(issue.properties.map.keySet(), hasSize(1));
        assertThat(issue.getProperty("prop"), equalTo(value(5)));
    }

    @Test
    public void nothingIsReturnedWhenPropertiesAreNotRequested() {
        backdoor.issues().createIssue("HSP", "fred's issue", "admin");
        propertyClient.put("HSP-1", "prop", value(5));

        Map issue = issueClient.issueResource("HSP-1").get(Map.class);

        assertThat(issue.get("properties"), equalTo(null));
    }

    @Test
    public void emptyMapIsReturnedWhenPropertiesAreRequestedButNoKeyIsValid() {
        backdoor.issues().createIssue("HSP", "fred's issue", "admin");
        propertyClient.put("HSP-1", "prop", value(5));

        Map issue = issueClient.issueResource("HSP-1").queryParam("properties", "inalidKey").get(Map.class);

        assertThat(issue.get("properties"), not(equalTo(null)));
    }

    @Test
    public void allPropertiesMayBeRequestedWithAsteriskAll() {
        backdoor.issues().createIssue("HSP", "fred's issue", "admin");
        propertyClient.put("HSP-1", "prop1", value(1));
        propertyClient.put("HSP-1", "prop2", value(2));
        propertyClient.put("HSP-1", "prop3", value(3));

        Issue issue = issueClient.issueResource("HSP-1").queryParam("properties", "*all").get(Issue.class);

        assertThat(issue.properties.map.keySet(), hasSize(3));
    }

    @Test
    public void whenRequestingAllPropertiesSomeMayBeExcluded() {
        backdoor.issues().createIssue("HSP", "fred's issue", "admin");
        propertyClient.put("HSP-1", "prop1", value(1));
        propertyClient.put("HSP-1", "prop2", value(2));
        propertyClient.put("HSP-1", "prop3", value(3));

        Issue issue = issueClient.issueResource("HSP-1").queryParam("properties", "*all, -prop2").get(Issue.class);

        assertThat(issue.properties.map.keySet(), hasSize(2));
        assertThat(issue.getProperty("prop1"), equalTo(value(1)));
        assertThat(issue.getProperty("prop3"), equalTo(value(3)));
    }

    @Test
    public void queryParamsAsArrayWorks() {
        backdoor.issues().createIssue("HSP", "fred's issue", "admin");
        propertyClient.put("HSP-1", "prop1", value(1));
        propertyClient.put("HSP-1", "prop2", value(2));
        propertyClient.put("HSP-1", "prop3", value(3));

        Issue issue = issueClient.issueResource("HSP-1")
                .queryParam("properties", "prop1,prop2")
                .queryParam("properties", "prop3")
                .get(Issue.class);

        assertThat(issue.properties.map.keySet(), hasSize(3));
        assertThat(issue.getProperty("prop1"), equalTo(value(1)));
        assertThat(issue.getProperty("prop2"), equalTo(value(2)));
        assertThat(issue.getProperty("prop3"), equalTo(value(3)));
    }

    private JSONObject value(final int value) {
        return new JSONObject(ImmutableMap.of("value", value));
    }
}
