package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Responsible for holding tests which verify that the User Administration actions are not susceptible to XSRF attacks.
 *
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestEditCustomFieldDescription.xml")
public class TestXsrfAdminScreenSchemes extends BaseJiraFuncTest {
    private static final String SCREEN_SCHEME_NAME = "villainous scheme";

    @Inject
    private Form form;

    @Test
    public void testScreenSchemeOperations() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck(
                        "Add Screen Scheme",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                navigation.gotoAdminSection(Navigation.AdminSection.FIELD_SCREEN_SCHEME);
                                tester.clickLink("add-field-screen-scheme");
                                tester.setFormElement("fieldScreenSchemeName", SCREEN_SCHEME_NAME);
                            }
                        },
                        new XsrfCheck.FormSubmission("Add")),
                new XsrfCheck(
                        "Edit Screen Scheme",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                navigation.gotoAdminSection(Navigation.AdminSection.FIELD_SCREEN_SCHEME);
                                tester.clickLink("edit_fieldscreenscheme_" + SCREEN_SCHEME_NAME);
                            }
                        },
                        new XsrfCheck.FormSubmission("Update")),
                new XsrfCheck(
                        "Copy Screen Scheme",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                navigation.gotoAdminSection(Navigation.AdminSection.FIELD_SCREEN_SCHEME);
                                tester.clickLink("copy_fieldscreenscheme_" + SCREEN_SCHEME_NAME);
                            }
                        },
                        new XsrfCheck.FormSubmission("Copy")),
                new XsrfCheck(
                        "Delete Screen Scheme",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                navigation.gotoAdminSection(Navigation.AdminSection.FIELD_SCREEN_SCHEME);
                                tester.clickLink("delete_fieldscreenscheme_" + SCREEN_SCHEME_NAME);
                            }
                        },
                        new XsrfCheck.FormSubmission("Delete"))

        ).run(tester, navigation, form);
    }

    @Test
    public void testScreenSchemeConfigOperations() throws Exception {
        addScreen();

        new XsrfTestSuite(
                new XsrfCheck(
                        "Configure Screen Scheme Add",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                gotoConfigure();
                                tester.clickLink("add-screen-scheme-item");
                            }
                        },
                        new XsrfCheck.FormSubmission("Add")),
                new XsrfCheck(
                        "Configure Screen Scheme Edit",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                gotoConfigure();
                                tester.clickLink("edit_fieldscreenscheme_Create Issue");
                            }
                        },
                        new XsrfCheck.FormSubmission("Update")),
                new XsrfCheck(
                        "Configure Screen Scheme Delete",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                gotoConfigure();
                            }
                        },
                        new XsrfCheck.LinkWithIdSubmission("delete_fieldscreenscheme_Create Issue"))

        ).run(tester, navigation, form);
    }

    private void addScreen() {
        navigation.gotoAdminSection(Navigation.AdminSection.FIELD_SCREEN_SCHEME);
        tester.clickLink("add-field-screen-scheme");
        tester.setFormElement("fieldScreenSchemeName", SCREEN_SCHEME_NAME);
        tester.submit("Add");
    }

    private void gotoConfigure() {
        navigation.gotoAdminSection(Navigation.AdminSection.FIELD_SCREEN_SCHEME);
        tester.clickLink("configure_fieldscreenscheme_" + SCREEN_SCHEME_NAME);
    }
}
