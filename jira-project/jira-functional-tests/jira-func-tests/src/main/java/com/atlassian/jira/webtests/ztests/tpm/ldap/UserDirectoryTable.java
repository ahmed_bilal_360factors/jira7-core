package com.atlassian.jira.webtests.ztests.tpm.ldap;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.locator.NodeLocator;
import com.atlassian.jira.functest.framework.locator.TableLocator;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.meterware.httpunit.TableCell;
import com.meterware.httpunit.WebTable;
import net.sourceforge.jwebunit.WebTester;
import org.w3c.dom.Node;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Used to make assertions on the Web table for the View User Directories page.
 *
 * @since v4.3
 */
public class UserDirectoryTable {
    private final WebTable table;
    private final Assertions assertions;
    private final FuncTestLogger logger;

    public UserDirectoryTable(final BaseJiraFuncTest funcTestCase, final FuncTestLogger logger, final Assertions assertions) {
        this(funcTestCase.getTester(), logger, assertions);
    }

    /**
     *
     * This constructor was removed in JIRA 7.x. If you need to use it, please refer to LegacyUserDirectoryTable available
     * in compatibility module.
     *
     */

//    public UserDirectoryTable(final FuncTestCase funcTestCase) {
//        logger = funcTestCase.logger;
//        table = new TableLocator(funcTestCase.getTester(), "directory-list").getTable();
//        assertions = funcTestCase.getAssertions();
//        // check the Header Row
//        checkTheHeaderRow(assertions);
//    }

    public UserDirectoryTable(final WebTester tester, final FuncTestLogger logger, final Assertions assertions) {
        this.logger = logger;
        table = new TableLocator(tester, "directory-list").getTable();
        this.assertions = assertions;
        // check the Header Row
        checkTheHeaderRow(assertions);
    }

    private void checkTheHeaderRow(final Assertions assertions) {
        assertions.getTableAssertions().assertTableRowEqualsCollapsed(table, 0, new String[]{"ID", "Directory Name", "Type", "Order", "Operations"});
    }

    public TableCell getTableCell(final int row, final int column) {
        return table.getTableCell(row, column);
    }

    public RowAssertions assertRow(final int rowNum) {
        return new RowAssertions(rowNum);
    }

    public class RowAssertions {
        private int rowNum;

        public RowAssertions(final int rowNum) {
            this.rowNum = rowNum;
        }

        public RowAssertions contains(final String id, final String name, final String type) {
            // Do the assertion
            assertions.getTableAssertions().assertTableRowEqualsCollapsed(table, rowNum, new String[]{id, null, type, null, null});
            // Directory name may be accompanied by other information in the same cell, so we assert it separately
            assertions.getTableAssertions().assertTableCellHasText(table, rowNum, 1, name);
            // return this for fluidity
            return this;
        }

        public RowAssertions hasMoveUp(final boolean enabled) {
            // Find the Order cell (ie with move Up and Move Down buttons)
            final Node cellNode = table.getTableCell(rowNum, 3).getDOM();
            final String cellText = new NodeLocator(cellNode).getHTML();
            final boolean containsArrow = cellText.contains("<SPAN class=\"aui-icon aui-icon-small aui-iconfont-up icon-default icon-move-up\">Move Up</SPAN>");

            if (enabled) {
                assertTrue("Enabled Move Up button expected. Found '" + cellText + "'", containsArrow);
            } else {
                assertFalse("No Move Up button expected. Found '" + cellText + "'", containsArrow);
            }
            // return this for fluidity
            return this;
        }

        public RowAssertions hasMoveDown(final boolean enabled) {
            // Find the Order cell (ie with move Up and Move Down buttons)
            final Node cellNode = table.getTableCell(rowNum, 3).getDOM();
            final String cellText = new NodeLocator(cellNode).getHTML();
            final boolean containsArrow = cellText.contains("<SPAN class=\"aui-icon aui-icon-small aui-iconfont-down icon-default icon-move-down\">Move Down</SPAN>");

            if (enabled) {
                assertTrue("Enabled Move Down button expected. Found '" + cellText + "'", containsArrow);
            } else {
                assertFalse("No Move Down button expected. Found '" + cellText + "'", containsArrow);
            }
            // return this for fluidity
            return this;
        }

        public void hasOnlyEditOperation() {
            final String cellText = table.getCellAsText(rowNum, 4);
            assertTrue("Edit operation not found. Found: '" + cellText + "'.", cellText.contains("Edit"));
        }

        public void hasDisableEditOperations() {
            final String cellText = table.getCellAsText(rowNum, 4);
            assertTrue("Disable operation not found. Found: '" + cellText + "'.", cellText.contains("Disable"));
            assertFalse("Enable operation found but not expected.", cellText.contains("Enable"));
            assertTrue("Edit operation not found. Found: '" + cellText + "'.", cellText.contains("Edit"));
            assertFalse("Synchronise operation found but not expected.", cellText.contains("Synchronise"));
        }

        /**
         * Asserts that this row has the disable and edit operations, and that this Directory is synchronisable.
         * <p>
         * Note that the synchronise test asserts that either a synchronise operation is currently available or the
         * directory is currently synchronising and therefore makes no guarantees that the synchronise link is there to
         * be clicked right now.
         */
        public void hasDisableEditSynchroniseOperations() {
            final String cellText = table.getCellAsText(rowNum, 4);
            assertTrue("Disable operation not found. Found: '" + cellText + "'.", cellText.contains("Disable"));
            assertFalse("Enable operation found but not expected.", cellText.contains("Enable"));
            assertTrue("Edit operation not found. Found: '" + cellText + "'.", cellText.contains("Edit"));
            hasSynchroniseOperationOrIsSynchronising();
        }

        /**
         * Asserts that this row has the enable and edit operations, and that this Directory is synchronisable.
         * <p>
         * Note that the synchronise test asserts that either a synchronise operation is currently available or the
         * directory is currently synchronising and therefore makes no guarantees that the synchronise link is there to
         * be clicked right now.
         */
        public void hasEnableEditRemoveSynchroniseOperations() {
            final String cellText = table.getCellAsText(rowNum, 4);
            assertTrue("Enable operation not found. Found: '" + cellText + "'.", cellText.contains("Enable"));
            assertFalse("Disable operation found but not expected.", cellText.contains("Disable"));
            assertTrue("Edit operation not found. Found: '" + cellText + "'.", cellText.contains("Edit"));
            assertTrue("Remove operation not found. Found: '" + cellText + "'.", cellText.contains("Remove"));
            hasSynchroniseOperationOrIsSynchronising();
        }

        public void hasEnableEditRemoveOperations() {
            final String cellText = table.getCellAsText(rowNum, 4);
            assertTrue("Enable operation not found. Found: '" + cellText + "'.", cellText.contains("Enable"));
            assertFalse("Disable operation found but not expected.", cellText.contains("Disable"));
            assertTrue("Edit operation not found. Found: '" + cellText + "'.", cellText.contains("Edit"));
            assertTrue("Remove operation not found. Found: '" + cellText + "'.", cellText.contains("Remove"));
            assertFalse("Synchronise operation found but not expected.", cellText.contains("Synchronise"));
        }

        private void hasSynchroniseOperationOrIsSynchronising() {
            final String cellText = table.getCellAsText(rowNum, 4);
            if (cellText.contains("Synchronising for")) {
                logger.log("The user directory is already synchronising.");
            } else {
                assertTrue("Synchronise operation not found. Found: '" + cellText + "'.", cellText.contains("Synchronise"));
            }
        }
    }

}
