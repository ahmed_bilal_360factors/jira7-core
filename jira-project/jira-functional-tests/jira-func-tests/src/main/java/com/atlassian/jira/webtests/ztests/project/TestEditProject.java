package com.atlassian.jira.webtests.ztests.project;

import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.atlassian.jira.testkit.client.restclient.ProjectClient;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * @since v3.13
 */
@WebTest({Category.FUNC_TEST, Category.PROJECTS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestEditProject extends BaseJiraFuncTest {

    public static final String EDIT_PROJECT_PAGE_URL = "/secure/project/EditProject!default.jspa?pid=";
    public static final String PROJ_NAME = "Canine";
    public static final Object NONE_PROJECT_CATEGORY = null;

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreData("TestEditProject.xml");
    }

    @Test
    public void testProjectDoesNotExistAdmin() throws Exception {
        tester.gotoPage(EDIT_PROJECT_PAGE_URL + "999999");
        tester.assertTextPresent("There is not a project with the specified id. Perhaps it was deleted.");
    }

    @Test
    public void testProjectDoesNotExistNonAdmin() throws Exception {
        // First try anonymous
        navigation.logout();
        tester.gotoPage("secure/project/EditProject!default.jspa?pid=999999");
        tester.assertTextPresent("You must log in to access this page.");

        // Now log in as non-admin
        navigation.login("gandhi");
        tester.gotoPage("secure/project/EditProject!default.jspa?pid=999999");
        tester.assertTextPresent("Access Denied");
        tester.assertTextPresent("It seems that you have tried to perform an operation which you are not permitted to perform.");
        tester.assertTextNotPresent("You cannot view this URL as a guest.");
    }

    @Test
    public void testEditProjectSuccess() {
        backdoor.project().addProjectCategory("Test Project Category", "Test description");

        ProjectClient pc = new ProjectClient(environmentData);

        Project dog = pc.get("DOG");
        assertEquals("DOG", dog.key);
        assertEquals(PROJ_NAME, dog.name);
        assertEquals("", dog.description);
        assertEquals(null, dog.url);
        assertEquals("murray", dog.lead.name);
        assertEquals(NONE_PROJECT_CATEGORY, dog.projectCategory);
        assertEquals(Project.AssigneeType.PROJECT_LEAD, dog.assigneeType);

        tester.gotoPage(EDIT_PROJECT_PAGE_URL + dog.id);
        tester.assertTextPresent(PROJ_NAME);

        //lets change all fields
        tester.setFormElement("name", "Dogs");
        tester.setFormElement("url", "http://www.dogs.com");
        tester.setFormElement("description", "This is the dog project.");
        tester.selectOption(ProjectService.PROJECT_CATEGORY_ID, "Test Project Category");
        tester.submit();

        dog = pc.get("DOG");
        assertThat(dog.name, equalTo("Dogs"));

        //now check that all information has been updated correctly.
        final Project newDog = pc.get("DOG");
        assertEquals("DOG", newDog.key);
        assertEquals("Dogs", newDog.name);
        assertEquals("This is the dog project.", newDog.description);
        assertEquals("http://www.dogs.com", newDog.url);
        assertEquals("murray", newDog.lead.name);
        assertEquals("Test Project Category", dog.projectCategory.name);
        assertEquals(Project.AssigneeType.PROJECT_LEAD, newDog.assigneeType);
    }

    @Test
    public void testEditProjectNameExists() {
        Long projectId = backdoor.project().getProjectId("DOG");
        tester.gotoPage(EDIT_PROJECT_PAGE_URL + projectId);
        tester.assertTextPresent(PROJ_NAME);
        tester.setFormElement("name", "Bovine");
        tester.submit();

        tester.assertTextPresent("A project with that name already exists.");
    }

    @Test
    public void testEditProjectValidation() {
        backdoor.project().addProjectCategory("Test Project Category", "Test description");
        Long projectId = backdoor.project().getProjectId("DOG");
        tester.gotoPage(EDIT_PROJECT_PAGE_URL + projectId);
        backdoor.project().deleteProjectCategory("10000");
        tester.assertTextPresent(PROJ_NAME);
        tester.setFormElement("name", "");
        tester.setFormElement("url", "badURL");
        tester.setFormElement(ProjectService.PROJECT_CATEGORY_ID, "10000");
        tester.submit();

        tester.assertTextPresent("You must specify a valid project name.");
        tester.assertTextPresent("The URL specified is not valid - it must start with http://");
        tester.assertTextPresent("That project category is no longer valid, it may have been deleted. Please pick another project category.");
    }

    @Test
    public void testEditProjectNoAccess() {
        navigation.logout();
        navigation.login("cow", "cow");

        //try to create a project without the right permissions.
        tester.gotoPage("secure/admin/EditProject.jspa?pid=10020&name=newproject&lead=admin&atl_token=" + page.getXsrfToken());
        tester.assertTextPresent("Welcome to Dev JIRA");
        tester.assertTextNotPresent(PROJ_NAME);
        tester.assertTextNotPresent("Use this page to update your project details.");
    }

    @Test
    public void testEditProjectDoesntExist() {
        tester.gotoPage("secure/admin/EditProject.jspa?pid=10025&name=newproject&lead=admin&atl_token=" + page.getXsrfToken());
        tester.assertTextPresent("There is not a project with the specified id. Perhaps it was deleted.");
    }

    @Test
    public void testEditProjectChangeNothing() {
        Long projectId = backdoor.project().getProjectId("DOG");
        tester.gotoPage(EDIT_PROJECT_PAGE_URL + projectId);
        tester.assertTextPresent(PROJ_NAME);

        //lets change all fields
        tester.setFormElement("name", PROJ_NAME);
        tester.setFormElement("url", "");
        tester.setFormElement("description", "");
        tester.submit();

        //verify all the project data is untouched.
        //now check that all information has been updated correctly.
        ProjectClient pc = new ProjectClient(environmentData);

        final Project dog = pc.get("DOG");
        assertEquals("DOG", dog.key);
        assertEquals(PROJ_NAME, dog.name);
        assertEquals("", dog.description);
        assertEquals(null, dog.url);
        assertEquals("murray", dog.lead.name);
        assertEquals(null, dog.projectCategory);
        assertEquals(Project.AssigneeType.PROJECT_LEAD, dog.assigneeType);
    }

    @Test
    public void testEditWithFieldsExceedingLimits() throws Exception {
        ProjectClient pc = new ProjectClient(environmentData);

        final Project dog = pc.get("DOG");
        assertEquals("DOG", dog.key);
        assertEquals(PROJ_NAME, dog.name);
        assertEquals("", dog.description);
        assertEquals(null, dog.url);
        assertEquals("murray", dog.lead.name);
        assertEquals(NONE_PROJECT_CATEGORY, dog.projectCategory);
        assertEquals(Project.AssigneeType.PROJECT_LEAD, dog.assigneeType);

        Long projectId = backdoor.project().getProjectId("DOG");
        tester.gotoPage(EDIT_PROJECT_PAGE_URL + projectId);

        tester.assertTextPresent(PROJ_NAME);

        tester.setFormElement("name", StringUtils.repeat("N", 81));
        tester.setFormElement("url", StringUtils.repeat("U", 256));
        tester.submit();

        tester.assertTextPresent("The project name must not exceed 80 characters in length");
        tester.assertTextPresent("The URL must not exceed 255 characters in length");
    }

    @Test
    public void testEditProjectWithoutBrowseProjectPermission() throws Exception {
        Long projectId = backdoor.project().getProjectId("VG");
        backdoor.project().addProjectCategory("Test Project Category", "Test description");
        tester.gotoPage(EDIT_PROJECT_PAGE_URL + projectId);
        tester.setFormElement("name", "Vegans");
        tester.selectOption(ProjectService.PROJECT_CATEGORY_ID, "Test Project Category");
        tester.submit();

        //verify the project name has been updated.
        ProjectClient pc = new ProjectClient(environmentData);

        final Project dog = pc.get("VG");
        assertEquals("VG", dog.key);
        assertEquals("Vegans", dog.name);
        assertEquals("Test Project Category", dog.projectCategory.name);
    }

    @Test
    public void testProjectCategoriesShowNoneWhenNoCategoriesExist() {
        Long projectId = backdoor.project().getProjectId("VG");
        tester.gotoPage(EDIT_PROJECT_PAGE_URL + projectId);
        tester.assertOptionsEqual(ProjectService.PROJECT_CATEGORY_ID, new String[] { "None" });
    }

    @Test
    public void testProjectCategoriesShowCategoriesWhenTheyExist() {
        Long projectId = backdoor.project().getProjectId("VG");
        backdoor.project().addProjectCategory("A", "Test description A");
        backdoor.project().addProjectCategory("B", "Test description B");
        backdoor.project().addProjectCategory("C", "Test description C");
        backdoor.project().addProjectCategory("D", "Test description D");
        String[] projectCategoriesOptions = {"None", "A", "B", "C", "D"};

        tester.gotoPage(EDIT_PROJECT_PAGE_URL + projectId);
        tester.assertOptionsEqual(ProjectService.PROJECT_CATEGORY_ID, projectCategoriesOptions);
    }
}
