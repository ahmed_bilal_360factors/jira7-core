package com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client;

import com.atlassian.fugue.Option;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.restclient.EntityPropertyClient;
import com.atlassian.jira.util.json.JSONObject;
import com.sun.jersey.api.client.WebResource;

public final class UserPropertyClient extends EntityPropertyClient {

    private UserIdentificationMode mode = UserIdentificationMode.userKey;

    public UserPropertyClient(final JIRAEnvironmentData environmentData) {
        super(environmentData, "user");
    }

    public UserPropertyClient identifyUserBy(UserIdentificationMode mode) {
        this.mode = mode;
        return this;
    }

    public void put(String entityKeyOrId, String propertyKey, String value) {
        resource(entityKeyOrId, propertyKey).header("Content-Type", "application/json").put(value);
    }

    @Override
    protected WebResource resource(final Option<String> entityKeyOrId, final Option<String> propertyKey) {
        return createResource().path(entityName).path("properties/").path(propertyKey.getOrElse("")).queryParam(mode.toString(), entityKeyOrId.getOrNull());
    }

    public enum UserIdentificationMode {
        userKey, username
    }
}
