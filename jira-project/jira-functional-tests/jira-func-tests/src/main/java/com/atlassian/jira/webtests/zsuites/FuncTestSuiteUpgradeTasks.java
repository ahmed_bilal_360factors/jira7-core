package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.upgrade.TestNoNewSalUpgrades;
import com.atlassian.jira.webtests.ztests.upgrade.TestRenaissanceMigrationSummary;
import com.atlassian.jira.webtests.ztests.upgrade.TestUpgradeTasksReindexSuppression;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestPluginStateMigration;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask552;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask6038;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask6039;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask6047;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask606;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask6085and6153;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask6096;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask6123;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask6134;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask6137;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask6140;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask6152;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask6206;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask6208;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask6327;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask64002;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask64015;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask641;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask65001;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask70010;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask70011;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask70025;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask701;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask70101;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask70102;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask707;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask807;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask813;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTask849and6153;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeTasks752To754;
import com.atlassian.jira.webtests.ztests.upgrade.tasks.TestUpgradeWorkflowsWithWhitespacesInNames;
import com.atlassian.jira.webtests.ztests.user.TestUpgradeTask602;
import com.atlassian.jira.webtests.ztests.user.TestUpgradeTask_Build64001;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of test related to Upgrade Tasks
 *
 * @since v4.0
 */
public class FuncTestSuiteUpgradeTasks {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestUpgradeTask6038.class)
                .add(TestUpgradeTask6039.class)
                .add(TestUpgradeTask6047.class)
                .add(TestUpgradeTask602.class)
                .add(TestUpgradeTask606.class)
                .add(TestUpgradeTask552.class)
                .add(TestUpgradeTask641.class)
                .add(TestUpgradeTask701.class)
                .add(TestUpgradeTask707.class)
                .add(TestUpgradeTasks752To754.class)
                .add(TestUpgradeTask813.class)
                .add(TestUpgradeTask807.class)
                .add(TestUpgradeTask849and6153.class)
                .add(TestUpgradeTask6085and6153.class)
                .add(TestUpgradeTask6096.class)
                .add(TestUpgradeTask6123.class)
                .add(TestUpgradeTask6134.class)
                .add(TestUpgradeTask6137.class)
                .add(TestUpgradeTask6140.class)
                .add(TestUpgradeTask6152.class)
                .add(TestUpgradeTask6206.class)
                .add(TestUpgradeTask6208.class)
                .add(TestUpgradeTask6327.class)
                .add(TestUpgradeTask64002.class)
                .add(TestUpgradeTask64015.class)
                .add(TestUpgradeTask65001.class)
                .add(TestRenaissanceMigrationSummary.class)
                .add(TestNoNewSalUpgrades.class)
                .add(TestUpgradeTask70010.class)
                .add(TestUpgradeTask70011.class)
                .add(TestUpgradeTask70025.class)
                .add(TestUpgradeTask70101.class)
                .add(TestUpgradeTask70102.class)
                .add(TestUpgradeTask_Build64001.class)
                .add(TestUpgradeWorkflowsWithWhitespacesInNames.class)
                .add(TestPluginStateMigration.class)
                .add(TestUpgradeTasksReindexSuppression.class)
                .build();
    }
}
