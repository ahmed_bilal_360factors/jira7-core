package com.atlassian.jira.functest.framework.admin;

import net.sourceforge.jwebunit.WebTester;

/**
 * Default implementation of
 * {@link IssueSecurityLevel}.
 *
 * @since v4.2
 */
public class IssueSecurityLevelImpl implements IssueSecurityLevel {
    private static final String ADD_SECURITY_LINK_ID_FORMAT = "add_%s";

    private final IssueSecuritySchemesImpl schemes;
    private final String name;
    private final String addSecurityLinkId;
    private final WebTester tester;

    public IssueSecurityLevelImpl(final WebTester tester,
                                  final IssueSecuritySchemesImpl schemes,
                                  final String name) {
        this.tester = tester;
        this.schemes = schemes;
        this.name = name;
        this.addSecurityLinkId = createLinkId();
    }

    private String createLinkId() {
        return String.format(ADD_SECURITY_LINK_ID_FORMAT, this.name);
    }

    public IssueSecuritySchemes.IssueSecurityScheme scheme() {
        return schemes;
    }

    public IssueSecurityLevel addIssueSecurity(IssueSecurity issueSecurity) {
        tester.clickLink(addSecurityLinkId);
        issueSecurity.chooseOnForm(tester);
        issueSecurity.submitForm(tester);
        return this;
    }

    public IssueSecurityLevel addIssueSecurity(IssueSecurity issueSecurity, String paramValue) {
        tester.clickLink(addSecurityLinkId);
        issueSecurity.chooseOnForm(tester, paramValue);
        issueSecurity.submitForm(tester);
        return this;
    }

    @Override
    public void delete() {
        tester.clickLink("delLevel_" + name);
        tester.assertTextPresent("Delete Issue Security Level: " + name);
        tester.submit("Delete");
    }

    @Override
    public void addRole(final String roleName) {
        tester.clickLink("add_" + name);
        tester.getDialog().setFormParameter("type", "projectrole");
        tester.assertRadioOptionSelected("type", "projectrole");
        tester.selectOption("projectrole", roleName);
        tester.submit();
    }
}
