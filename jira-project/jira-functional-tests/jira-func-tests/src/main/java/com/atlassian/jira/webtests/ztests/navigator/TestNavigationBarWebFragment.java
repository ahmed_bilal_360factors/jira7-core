package com.atlassian.jira.webtests.ztests.navigator;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_DEV_GROUP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_USERS_GROUP;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ISSUES;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Simple test case that checks the links on the top system navigation bar
 * is visible with correct permissions.
 */
@WebTest({Category.FUNC_TEST, Category.ISSUE_NAVIGATOR})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestNavigationBarWebFragment extends BaseJiraFuncTest {

    private static final long USERS_PROJECT_ROLE_ID = 10000;

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreData("TestNavigationBarWebFragment.xml");
    }

    @After
    public void tearDownTest() {
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        administration.restoreBlankInstance();
    }

    @Test
    public void testNavigationBarWebFragment() {
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        tester.assertLinkPresent("home_link"); //always visible

        _checkBrowseAndFindIssueLinksVisiblity();
        _checkCreateIssueLinkVisiblity();
        _checkAdminLinkVisiblityToProjectAdmin();
        _checkAdminLinkVisiblityToSystemAdmin();
    }

    public void _checkBrowseAndFindIssueLinksVisiblity() {
        //check we have the browse/find link to start with.
        tester.assertLinkPresent("browse_link");
        tester.assertLinkPresent("find_link");

        //remove the browse project permission and assert links are not available
        removeBrowsePermission();
        tester.assertLinkNotPresent("find_link");

        //add the browse permission back and check its displayed correctly.
        addBrowsePermission();
        navigation.gotoDashboard();
        tester.assertLinkPresent("find_link");
        tester.assertLinkPresent("browse_link");
        tester.assertLinkPresentWithText("Projects");

        navigation.gotoDashboard();
        tester.assertLinkPresent("browse_link");
        tester.assertLinkPresentWithText("Projects");
    }

    public void _checkCreateIssueLinkVisiblity() {
        //make sure we're no longer in the admin section (where the create issue link is no longer displayed).
        if (tester.getDialog().isLinkPresent("leave_admin")) {
            tester.clickLink("leave_admin");
        }
        //check we have the create issue link to start with.
        tester.assertLinkPresent("create_link");

        //remove the permission and assert link is not present
        removeCreatePermission();
        //make sure we're no longer in the admin section (where the create issue link is no longer displayed).
        if (tester.getDialog().isLinkPresent("leave_admin")) {
            tester.clickLink("leave_admin");
        }
        tester.assertLinkNotPresent("create_link");

        //readd the permission and assert its back
        addCreatePermission();
        navigation.gotoDashboard();
        //make sure we're no longer in the admin section (where the create issue link is no longer displayed).
        if (tester.getDialog().isLinkPresent("leave_admin")) {
            tester.clickLink("leave_admin");
        }
//        tester.getDialog().getWebClient().re
        tester.assertLinkPresent("create_link");
    }

    public void _checkAdminLinkVisiblityToProjectAdmin() {
        navigation.login("project_admin", "project_admin");
        assertTrue(administration.link().isPresent());

        //login as admin and remove the project admin permission from user: project_admin
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        removeProjectAdminPermission();
        navigation.gotoDashboard();
        assertTrue(administration.link().isPresent());

        //log back in as the project_admin, and assert admin link is not available
        navigation.logout();//must explicitly logout to invalidate session (SessionKeys.USER_PROJECT_ADMIN)
        navigation.login("project_admin", "project_admin");
        assertFalse(administration.link().isPresent());

        //login as admin and add the project admin permission for user: project_admin
        navigation.logout();//not neccessary but safe to logout here also
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        addProjectAdminPermission();
        navigation.gotoDashboard();
        assertTrue(administration.link().isPresent());

        //log back in as project_admin and assert link is back
        navigation.logout();//must explicitly logout to create new session (SessionKeys.USER_PROJECT_ADMIN)
        navigation.login("project_admin", "project_admin");
        assertTrue(administration.link().isPresent());
    }

    public void _checkAdminLinkVisiblityToSystemAdmin() {
        navigation.login("system_admin", "system_admin");
        assertTrue(administration.link().isPresent());

        //login as admin and remove the system_admin from the administrators group
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        administration.usersAndGroups().removeUserFromGroup("system_admin", "jira-administrators");
        navigation.gotoDashboard();
        assertTrue(administration.link().isPresent());

        //log back in as the system_admin, and assert admin link is not available
        navigation.logout();//must explicitly logout to invalidate session (SessionKeys.USER_PROJECT_ADMIN)
        navigation.login("system_admin", "system_admin");
        assertFalse(administration.link().isPresent());

        //login as admin and add the system_admin permission for user: system_admin
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        administration.usersAndGroups().addUserToGroup("system_admin", "jira-administrators");
        navigation.gotoDashboard();
        assertTrue(administration.link().isPresent());

        //log back in as project_admin and assert link is back
        navigation.login("system_admin", "system_admin");
        assertTrue(administration.link().isPresent());
    }

    public void removeBrowsePermission() {
        backdoor.permissionSchemes().removeProjectRolePermission(DEFAULT_PERM_SCHEME_ID, BROWSE_PROJECTS, USERS_PROJECT_ROLE_ID);
        navigation.gotoDashboard();
    }

    public void removeCreatePermission() {
        backdoor.permissionSchemes().removeProjectRolePermission(DEFAULT_PERM_SCHEME_ID, CREATE_ISSUES, USERS_PROJECT_ROLE_ID);
        navigation.gotoDashboard();
    }

    public void removeProjectAdminPermission() {
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ADMINISTER_PROJECTS, JIRA_DEV_GROUP);
        navigation.gotoDashboard();
    }

    public void addBrowsePermission() {
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, BROWSE_PROJECTS, JIRA_USERS_GROUP);
    }

    public void addCreatePermission() {
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, CREATE_ISSUES, JIRA_USERS_GROUP);
    }

    public void addProjectAdminPermission() {
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ADMINISTER_PROJECTS, JIRA_DEV_GROUP);
    }
}
