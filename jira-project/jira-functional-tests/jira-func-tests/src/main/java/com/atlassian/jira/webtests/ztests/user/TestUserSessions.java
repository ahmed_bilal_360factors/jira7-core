package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.webtests.WebTesterFactory;
import net.sourceforge.jwebunit.WebTester;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.text.DecimalFormat;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestUserSessions extends BaseJiraFuncTest {
    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreData("TestUserSessions.xml");
    }

    @Test
    public void testSessionsAppear() {
        startSessionsForUsers(1, 150);

        gotoUserSessions();

        assertLinkPresent("gotoNext", "gotoEnd");
        assertLinkNotPresent("gotoStart", "gotoPrev");

        tester.clickLink("gotoNext");
        assertLinkPresent("gotoStart", "gotoPrev", "gotoNext", "gotoEnd");

        tester.clickLink("gotoPrev");
        assertLinkPresent("gotoNext", "gotoEnd");
        assertLinkNotPresent("gotoStart", "gotoPrev");

        tester.clickLink("gotoEnd");
        assertLinkPresent("gotoStart", "gotoPrev");
        assertLinkNotPresent("gotoEnd", "gotoNext");

    }

    @Test
    public void testNotAccessibleForAdmin() {
        navigation.login("justadmin");
        tester.gotoPage("/secure/admin/CurrentUsersList.jspa");
        assertions.getJiraFormAssertions().assertFormWarningMessage("'Just Admin' does not have permission to access this page.");
    }

    private void assertLinkPresent(final String... args) {
        for (final String arg : args) {
            tester.assertLinkPresent(arg);
        }
    }

    private void assertLinkNotPresent(final String... args) {
        for (final String arg : args) {
            tester.assertLinkNotPresent(arg);
        }
    }

    private void startSessionsForUsers(final int start, final int end) {
        for (int i = start; i <= end; i++) {
            loginAs(makeUserName(i));
        }
    }

    private void loginAs(final String userName) {
        final WebTester tester = WebTesterFactory.createNewWebTester(environmentData);
        tester.beginAt("/login.jsp");
        tester.setFormElement("os_username", userName);
        tester.setFormElement("os_password", userName);
        tester.setWorkingForm("login-form");
        tester.submit();
        logger.log("Started session for " + userName);
    }

    private void gotoUserSessions() {
        navigation.gotoAdminSection(Navigation.AdminSection.USERSESSIONS);
    }

    private String makeUserName(final int j) {
        return "user-" + new DecimalFormat("000").format(j);
    }
}
