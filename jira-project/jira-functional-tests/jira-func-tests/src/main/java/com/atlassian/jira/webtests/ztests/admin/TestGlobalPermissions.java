package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.BROWSING})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestGlobalPermissions extends BaseJiraFuncTest {
    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Test
    public void testErrorOnSysAdminDelete() {
        administration.restoreBlankInstance();
        //shouldn't be able to delte admin group as there is only one admin group
        gotoGlobalPermissions();
        tester.assertTextPresent("JIRA System Administrators");
        tester.assertTextPresent("jira-administrators");
        tester.clickLink("del_SYSTEM_ADMIN_jira-administrators");
        tester.assertTextPresent("You cannot delete this permission. You are not a member of any of the other system administration permissions.");
    }

    @Test
    public void testAddThenDeletePermission() {
        administration.restoreBlankInstance();
        //Check that you can add a permission then delete it.
        gotoGlobalPermissions();
        tester.assertTextPresent("Browse Users");
        tester.assertTextPresent("jira-developers");

        //Check for the delete link
        tester.assertLinkPresent("del_USER_PICKER_jira-developers");
        tester.selectOption("globalPermType", "Browse Users");
        tester.selectOption("groupName", "Anyone");
        tester.submit("Add");                                       //add the group
        tester.assertLinkPresent("del_USER_PICKER_");

        //Delete the group
        tester.clickLink("del_USER_PICKER_");
        tester.assertTextPresent("Delete Global Permission");
        tester.assertTextPresent("Are you sure you want to delete the");   //check for confirmation
        tester.assertTextPresent("Anyone");
        tester.assertTextPresent("group from the");
        tester.assertTextPresent("Browse Users");
        tester.assertTextPresent("permission?");
        tester.submit("Delete");

        // Make sure the delete link for the Anyone permission does not exist
        tester.assertLinkNotPresent("del_USER_PICKER_");
    }

    @Test
    public void testAddNoPermission() {
        administration.restoreBlankInstance();
        //should be prompted to select a permission from the dropdown
        gotoGlobalPermissions();
        tester.submit("Add");
        tester.assertTextPresent("You must select a permission");

        navigation.gotoAdminSection(Navigation.AdminSection.GLOBAL_PERMISSIONS);
        tester.assertTextNotPresent("You must select a permission");
        tester.selectOption("globalPermType", "Please select a permission");
        tester.submit("Add");
        tester.assertTextPresent("You must select a permission");
    }

    @Test
    public void testNotAllowedToAddAnyoneToJiraUsers() {
        administration.restoreBlankInstance();
        gotoGlobalPermissions();
        tester.assertTextPresent("JIRA Administrators");
        tester.assertTextPresent("jira-administrators");
        assertCannotAddAnyoneToJiraAdministrators();
        assertCannotAddAnyoneToSystemAdministrators();
    }

    private void assertCannotAddAnyoneToJiraUsers() {
        tester.selectOption("globalPermType", "JIRA Users");
        tester.selectOption("groupName", "Anyone");
        tester.submit("Add");
        assertions.getJiraFormAssertions().assertFieldErrMsg("The group 'Anyone' is not allowed to be added to the permission");
    }

    /**
     * JRA-26627 - no longer allow anyone to be added to the Administrators group
     */
    private void assertCannotAddAnyoneToJiraAdministrators() {
        tester.selectOption("globalPermType", "JIRA Administrators");
        tester.selectOption("groupName", "Anyone");
        tester.submit("Add"); //add the group
        assertions.getJiraFormAssertions().assertFieldErrMsg("The group 'Anyone' is not allowed to be added to the permission");
    }

    private void assertCannotAddAnyoneToSystemAdministrators() {
        tester.selectOption("globalPermType", "JIRA System Administrators");
        tester.selectOption("groupName", "Anyone");
        tester.submit("Add"); //add the group
        assertions.getJiraFormAssertions().assertFieldErrMsg("The group 'Anyone' is not allowed to be added to the permission");
    }


    @Test
    public void testSystemAdminNotVisibleToNonAdmins() {
        try {
            // restore data that has the admin user not as a sys admin
            administration.restoreData("TestWithSystemAdmin.xml");

            // Confirm that we are not able to see the sys admin stuff
            gotoGlobalPermissions();
            tester.assertTextNotPresent("<b>JIRA System Administrators</b>");

            // Try to add something we are not allowed to add
            final String addUrl = page.addXsrfToken("/secure/admin/jira/GlobalPermissions.jspa?action=add&globalPermType=SYSTEM_ADMIN&groupName=jira-users");
            tester.gotoPage(addUrl);
            tester.assertTextPresent("You can not add a group to a global permission you do not have permission to see.");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login("root", "root");
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testAdminCannotDeleteSysAdminGroups() {
        try {
            // restore data that has the admin user not as a sys admin
            administration.restoreData("TestWithSystemAdmin.xml");

            final String removeUrl = page.addXsrfToken("/secure/admin/jira/GlobalPermissions.jspa?globalPermType=SYSTEM_ADMIN&action=del&groupName=jira-sys-admins");
            tester.gotoPage(removeUrl);
            tester.assertTextPresent("Only system administrators can delete groups from the system administrator permission.");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login("root", "root");
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testFilterPermsHaveCorrectVisibility() {
        administration.restoreBlankInstance();
        gotoGlobalPermissions();

        tester.assertTextPresent("Create Shared Objects");
        tester.assertTextPresent("Manage Group Filter Subscriptions");
    }

    @Test
    public void testRemoveGroupDoesntExist() {
        administration.restoreData("TestRemoveGroupDoesntExist.xml");
        navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
        tester.assertTextNotPresent("Stuff");

        //now check it's present in the global permissions.
        gotoGlobalPermissions();
        tester.assertTextPresent("Stuff");

        //try to remove the group stuff.  It doesn't exist any longer.
        tester.clickLink("del_USER_PICKER_Stuff");
        tester.assertTextPresent("Delete Global Permission");
        assertions.text().assertTextSequence(tester.getDialog().getResponseText(),
                "Are you sure you want to delete the",
                "Stuff",
                "group from the",
                "Browse Users",
                "permission");
        tester.submit("Delete");

        tester.assertTextPresent("Global Permissions");
        tester.assertTextNotPresent("Stuff");

        //also try removing a group that's not a member of a permission which should throw an error.
        final String removeUrl = page.addXsrfToken("secure/admin/jira/GlobalPermissions.jspa?groupName=bad&globalPermType=SYSTEM_ADMIN&action=confirm");
        tester.gotoPage(removeUrl);
        assertions.getJiraFormAssertions().assertFormErrMsg("Group 'bad' cannot be removed from permission "
                + "'JIRA System Administrators' since it is not a member of this permission.");
    }

    private void gotoGlobalPermissions() {
        navigation.gotoAdminSection(Navigation.AdminSection.GLOBAL_PERMISSIONS);
        tester.assertTextPresent("Global Permissions");
    }

}
