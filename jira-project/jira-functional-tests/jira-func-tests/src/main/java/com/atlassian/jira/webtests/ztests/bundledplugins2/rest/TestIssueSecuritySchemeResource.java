package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.testkit.beans.IssueSecuritySchemeBean;
import com.atlassian.jira.testkit.beans.IssueSecurityType;
import com.atlassian.jira.testkit.client.restclient.IssueSecuritySchemeClient;
import com.atlassian.jira.testkit.client.restclient.IssueSecuritySchemes;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.REST})
@RestoreBlankInstance
public class TestIssueSecuritySchemeResource extends BaseJiraRestTest {
    private IssueSecuritySchemeClient client;

    @Before
    public void setup() {
        client = new IssueSecuritySchemeClient(environmentData);
    }

    @Test
    public void testListReturnsEmpty() throws Exception {
        Response<IssueSecuritySchemes> response = client.getAllSecuritySchemes();
        assertThat(response.statusCode, isOK());

        IssueSecuritySchemes list = response.body;
        assertThat(list.getIssueSecuritySchemes(), hasSize(0));
    }

    @Test
    public void testListReturnsOneScheme() throws Exception {
        IssueSecuritySchemeBean expectedScheme = new IssueSecuritySchemeBean(null, "A");
        expectedScheme.description = "a";

        backdoor.getTestkit().issueSecuritySchemes().createScheme(expectedScheme.name, expectedScheme.description);

        Response<IssueSecuritySchemes> response = client.getAllSecuritySchemes();
        assertThat(response.statusCode, isOK());

        IssueSecuritySchemes list = response.body;
        assertThat(list.getIssueSecuritySchemes(), contains(matchesWithoutID(expectedScheme)));
    }

    @Test
    public void testListForbiddenForNotAdmin() throws Exception {
        Response<IssueSecuritySchemes> response = asFred().getAllSecuritySchemes();
        assertThat(response.statusCode, isForbidden());
    }

    @Test
    public void testGetReturnsSchemeWithLevels() throws Exception {
        IssueSecuritySchemeBean expectedScheme = getTestIssueSecuritySchemeBean();

        Long scheme = backdoor.getTestkit().issueSecuritySchemes().createScheme(expectedScheme.name, expectedScheme.description);

        for (IssueSecurityType level : expectedScheme.levels) {
            backdoor.getTestkit().issueSecuritySchemes().addSecurityLevel(scheme, level.name, level.description);
        }

        Response<IssueSecuritySchemeBean> response = client.get(scheme);
        assertThat(response.statusCode, isOK());

        IssueSecuritySchemeBean result = response.body;
        assertThat(result, matchesWithoutID(expectedScheme));
    }

    @Test
    public void testGetSuccessfulForNotAdminWithProjectAdminPermissions() {
        IssueSecuritySchemeBean expectedScheme = getTestIssueSecuritySchemeBean();

        Long scheme = backdoor.getTestkit().issueSecuritySchemes().createScheme(expectedScheme.name, expectedScheme.description);

        backdoor.permissionSchemes().addUserPermission(0, ProjectPermissions.ADMINISTER_PROJECTS, "fred");
        long projectId = backdoor.project().addProject("name", "KEY", "admin");
        backdoor.project().setIssueSecurityScheme(projectId, scheme);

        for (IssueSecurityType level : expectedScheme.levels) {
            backdoor.getTestkit().issueSecuritySchemes().addSecurityLevel(scheme, level.name, level.description);
        }

        Response<IssueSecuritySchemeBean> response = client.loginAs("fred").get(scheme);
        assertThat(response.statusCode, isOK());

        IssueSecuritySchemeBean result = response.body;
        assertThat(result, matchesWithoutID(expectedScheme));
    }

    @Test
    public void testGetForbiddenForNotAdmin() throws Exception {
        Response<IssueSecuritySchemeBean> response = asFred().get(0);
        assertThat(response.statusCode, isForbidden());
    }

    @Test
    public void testGettingSecuritySchemeForProject() {
        final IssueSecuritySchemeBean expectedSecurityScheme = getTestIssueSecuritySchemeBean();
        Long scheme = backdoor.getTestkit().issueSecuritySchemes().createScheme(expectedSecurityScheme.name, expectedSecurityScheme.description);

        for (IssueSecurityType level : expectedSecurityScheme.levels) {
            backdoor.getTestkit().issueSecuritySchemes().addSecurityLevel(scheme, level.name, level.description);
        }

        backdoor.permissionSchemes().addUserPermission(0, ProjectPermissions.ADMINISTER_PROJECTS, "fred");
        long projectId = backdoor.project().addProject("name", "KEY", "admin");
        backdoor.project().setIssueSecurityScheme(projectId, scheme);

        final Response<IssueSecuritySchemeBean> response = client.getForProject("KEY");
        assertThat(response.statusCode, isOK());

        IssueSecuritySchemeBean securitySchemeBean = response.body;
        assertThat(securitySchemeBean, matchesWithoutID(expectedSecurityScheme));
    }

    @Test
    public void testNoSecuritySchemeForProject() throws Exception {
        final Response<IssueSecuritySchemeBean> forProject = client.getForProject("MKY");

        assertThat(forProject.statusCode, isNotFound());
        assertThat(forProject.entity.errorMessages, hasItem("Security level for project MKY does not exist."));
    }

    @Test
    public void testForbiddenForUsersWithoutAdministrativePermissionsOnProject() {
        final Response<IssueSecuritySchemeBean> forProject = asFred().getForProject("MKY");
        assertThat(forProject.statusCode, isForbidden());
    }

    @Test
    public void testProjectDoesNotExist() {
        final Response<IssueSecuritySchemeBean> nonExistingProject = client.getForProject("MKY2");
        assertThat(nonExistingProject.statusCode, isNotFound());
    }

    @Test
    public void testNoSecuritySchemeForProjectIdentifiedById() throws Exception {
        final String projectId = backdoor.project().getProject("MKY").id;
        final Response<IssueSecuritySchemeBean> forProject = client.getForProject(projectId);

        assertThat(forProject.statusCode, isNotFound());
        assertThat(forProject.entity.errorMessages, hasItem("Security level for project " + projectId + " does not exist."));
    }

    private IssueSecuritySchemeBean getTestIssueSecuritySchemeBean() {
        IssueSecuritySchemeBean expectedScheme = new IssueSecuritySchemeBean(null, "A");
        expectedScheme.description = "a";
        expectedScheme.levels = createSecurityLevelTestList();

        return expectedScheme;
    }

    private List<IssueSecurityType> createSecurityLevelTestList() {
        IssueSecurityType securityLevel = new IssueSecurityType().name("level").description("desc");
        IssueSecurityType securityLevel2 = new IssueSecurityType().name("level").description("desc");

        return Arrays.asList(securityLevel, securityLevel2);
    }

    private Matcher<? super Integer> isOK() {
        return is(javax.ws.rs.core.Response.Status.OK.getStatusCode());
    }

    private Matcher<? super Integer> isForbidden() {
        return is(javax.ws.rs.core.Response.Status.FORBIDDEN.getStatusCode());
    }

    private Matcher<? super Integer> isNotFound() {
        return is(javax.ws.rs.core.Response.Status.NOT_FOUND.getStatusCode());
    }

    private IssueSecuritySchemeClient asFred() {
        return client.loginAs("fred");
    }

    private TypeSafeMatcher<IssueSecuritySchemeBean> matchesWithoutID(final IssueSecuritySchemeBean expected) {
        return new TypeSafeMatcher<IssueSecuritySchemeBean>() {
            @Override
            protected boolean matchesSafely(final IssueSecuritySchemeBean issueSecuritySchemeBean) {
                return new EqualsBuilder()
                        .append(issueSecuritySchemeBean.name, expected.name)
                        .append(issueSecuritySchemeBean.description, expected.description)
                        .isEquals() && levelsMatch(issueSecuritySchemeBean.levels);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("Scheme: ")
                        .appendValue(expected.name)
                        .appendValue(expected.description)
                        .appendValueList("[", ",", "]", expected.levels);
            }

            private boolean levelsMatch(Collection<IssueSecurityType> levels) {
                if (levels == null) {
                    return expected.levels == null;
                }

                Iterable<Matcher<? super IssueSecurityType>> matchers = Iterables.transform(levels, new Function<IssueSecurityType, Matcher<? super IssueSecurityType>>() {
                    @Override
                    public Matcher<IssueSecurityType> apply(final IssueSecurityType issueSecurityType) {
                        return levelMatchesWithoutId(issueSecurityType);
                    }
                });
                Matcher[] matcherArray = Iterables.toArray(matchers, Matcher.class);
                return IsIterableContainingInOrder.contains(matcherArray).matches(expected.levels);
            }
        };
    }

    private TypeSafeMatcher<IssueSecurityType> levelMatchesWithoutId(final IssueSecurityType expected) {
        return new TypeSafeMatcher<IssueSecurityType>() {
            @Override
            protected boolean matchesSafely(final IssueSecurityType issueSecurityType) {
                return new EqualsBuilder()
                        .append(expected.name, issueSecurityType.name)
                        .append(expected.description, issueSecurityType.description)
                        .isEquals();
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("Level:")
                        .appendValue(expected.name)
                        .appendValue(expected.description);
            }
        };
    }
}
