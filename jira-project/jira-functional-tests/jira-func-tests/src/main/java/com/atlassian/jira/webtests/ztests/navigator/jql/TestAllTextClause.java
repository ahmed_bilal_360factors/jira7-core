package com.atlassian.jira.webtests.ztests.navigator.jql;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * Tests the "text" clause in JQL, which searches across all system text fields and custom free text fields that the user
 * can see.
 *
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.JQL})
@Restore("TestAllTextClauseCorrectness.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestAllTextClause extends BaseJiraFuncTest {

    @Inject
    private IssueTableAssertions issueTableAssertions;

    @Inject
    private Administration administration;

    @Test
    public void testCorrectness() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        // no results to start out with
        issueTableAssertions.assertSearchWithResults("text ~ something");

        final String issue1 = navigation.issue().createIssue("homosapien", "Bug", "this summary has something in it");
        final String issue2 = navigation.issue().createIssue("homosapien", "Bug", "this summary has bananas in it");
        final String issue3 = navigation.issue().createIssue("homosapien", "Bug", "this summary 3");
        final String issue4 = navigation.issue().createIssue("homosapien", "Bug", "this summary 4");
        issueTableAssertions.assertSearchWithResults("text ~ something", issue1);

        // try some range queries
        issueTableAssertions.assertSearchWithResults("text ~ \"[bananaa TO bananaz]\"", issue2);
        issueTableAssertions.assertSearchWithResults("text ~ \"{ha TO haz}\"", issue2, issue1);

        // ensure it is and based searching
        issueTableAssertions.assertSearchWithResults("text ~ \"summary something\"", issue1);
        issueTableAssertions.assertSearchWithResults("text ~ \"bananas something\"");
        issueTableAssertions.assertSearchWithResults("text ~ \"this summary has in it\"", issue2, issue1);

        navigation.issue().setDescription(issue1, "cheese");
        issueTableAssertions.assertSearchWithResults("text ~ cheese", issue1);

        navigation.issue().setEnvironment(issue2, "toast");
        issueTableAssertions.assertSearchWithResults("text ~ toast", issue2);

        navigation.issue().addComment(issue3, "monkey", null);
        issueTableAssertions.assertSearchWithResults("text ~ monkey", issue3);

        // add a custom field with a free text searcher
        final String customFieldId = administration.customFields().addCustomField("com.atlassian.jira.plugin.system.customfieldtypes:textarea", "MyText");
        final String numericCustomFieldId = customFieldId.split("_", 2)[1];
        navigation.issue().setFreeTextCustomField(issue4, customFieldId, "gojira");
        issueTableAssertions.assertSearchWithResults("text ~ gojira", issue4);

        // change custom field configuration so that it is only in project MKY, which we can't see
        administration.customFields().editConfigurationSchemeContextByLabel(numericCustomFieldId, "Default Configuration Scheme for MyText", null, null, new String[]{"10001"});
        issueTableAssertions.assertSearchWithResults("text ~ gojira");

        // re-add a global context to the custom field
        administration.customFields().editConfigurationSchemeContextByLabel(numericCustomFieldId, "Default Configuration Scheme for MyText", null, new String[]{}, new String[]{});
        issueTableAssertions.assertSearchWithResults("text ~ gojira", issue4);

        // add a URL field - check that it is not included
        final String urlCFId = administration.customFields().addCustomField("com.atlassian.jira.plugin.system.customfieldtypes:url", "MyURL");
        navigation.issue().setFreeTextCustomField(issue4, urlCFId, "http://www.atlassian.com");
        issueTableAssertions.assertSearchWithResults("text ~ 'http://www.atlassian.com'");

        // set search term on multiple issues to assert that merging results is done correctly
        navigation.issue().addComment(issue4, "something", null);
        issueTableAssertions.assertSearchWithResults("text ~ something", issue4, issue1);

        navigation.issue().setDescription(issue2, "something");
        issueTableAssertions.assertSearchWithResults("text ~ something", issue4, issue2, issue1);

        navigation.issue().setEnvironment(issue3, "something");
        issueTableAssertions.assertSearchWithResults("text ~ something", issue4, issue3, issue2, issue1);

        // start hiding stuff

        // hide description in one field config, should still be searched
        administration.fieldConfigurations().fieldConfiguration("A Config").hideFields("Description");
        issueTableAssertions.assertSearchWithResults("text ~ cheese", issue1);

        // hide description in second field config, should be ignored
        administration.fieldConfigurations().defaultFieldConfiguration().hideFields("Description");
        issueTableAssertions.assertSearchWithResults("text ~ cheese");

        // hide environment in one field config, should still be searched
        administration.fieldConfigurations().fieldConfiguration("A Config").hideFields("Environment");
        issueTableAssertions.assertSearchWithResults("text ~ toast", issue2);

        // hide environment in second field config, should be ignored
        administration.fieldConfigurations().defaultFieldConfiguration().hideFields("Environment");
        issueTableAssertions.assertSearchWithResults("text ~ toast");

        // hide MyText in one field config, should still be searched
        administration.fieldConfigurations().fieldConfiguration("A Config").hideFields("MyText");
        issueTableAssertions.assertSearchWithResults("text ~ gojira", issue4);

        // hide MyText in second field config, should be ignored
        administration.fieldConfigurations().defaultFieldConfiguration().hideFields("MyText");
        issueTableAssertions.assertSearchWithResults("text ~ gojira");
    }

    @Test
    public void testErrorMessagesWithInvalidRangeQuery() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        // try some range queries
        assertSearchWithIncorrectRangeFreeTextJql("[wrong");
        assertSearchWithIncorrectRangeFreeTextJql("[wrong]");
        assertSearchWithIncorrectRangeFreeTextJql("[wrong TO]");
        assertSearchWithIncorrectRangeFreeTextJql("[TO wrong]");
        assertSearchWithIncorrectRangeFreeTextJql("[TO wrong");

        assertSearchWithIncorrectRangeFreeTextJql("{wrong");
        assertSearchWithIncorrectRangeFreeTextJql("{wrong}");
        assertSearchWithIncorrectRangeFreeTextJql("{wrong TO}");
        assertSearchWithIncorrectRangeFreeTextJql("{TO wrong}");
        assertSearchWithIncorrectRangeFreeTextJql("{TO wrong");

        assertSearchWithIncorrectRangeFreeTextJql("[wrong TO query}");
        assertSearchWithIncorrectRangeFreeTextJql("{wrong TO query]");
    }

    private void assertSearchWithIncorrectRangeFreeTextJql(final String rangeQuery) {
        issueTableAssertions.assertSearchWithError(String.format("text ~ \"%s\"", rangeQuery),
                String.format("The text query '%s' for field 'text' is not valid: probably your range query is incorrect.", rangeQuery));
    }
}
