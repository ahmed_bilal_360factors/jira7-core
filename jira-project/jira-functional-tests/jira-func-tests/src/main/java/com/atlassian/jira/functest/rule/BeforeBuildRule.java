package com.atlassian.jira.functest.rule;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import org.junit.rules.TestRule;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.function.Supplier;

/**
 * Ensures tests are ignored if the currently running JIRA is on an newer build number than when the test was written,
 * and the tested product/feature has changed after this build.
 *
 * Why not just remove that test; why is this necessary?
 *
 * During a Zero Downtime Upgrade (ZDU), a JIRA cluster passes through a state where the binaries are on 'latest',
 * but the Upgrade Tasks haven't been run yet, hence the database is on 'old'.
 * In this situation, we want to ensure 'old' behaviour still works properly from a 'new' binary.
 *
 * Use the @BeforeBuild annotation on existing tests that test 'old' behaviour.
 *
 * Have a look at its counterpart {@link SinceBuildRule}
 */
public class BeforeBuildRule extends AbstractBuildNumberRule<BeforeBuildRule.BeforeBuild> implements TestRule {

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    public @interface BeforeBuild {
        int buildNumber();
    }

    public BeforeBuildRule(final Supplier<Backdoor> backdoor) {
        super(backdoor, BeforeBuild.class, (annotation, dbBuildNumber) -> annotation.buildNumber() <= dbBuildNumber, BeforeBuild::buildNumber);
    }

    @Override
    String describeUnsatisfiedRule(String testName, int annotationBuildNumber, int dbBuildNumber) {
        return testName + " was ignored as it only exists before " + annotationBuildNumber + " (current: " + dbBuildNumber + ")";
    }
}
