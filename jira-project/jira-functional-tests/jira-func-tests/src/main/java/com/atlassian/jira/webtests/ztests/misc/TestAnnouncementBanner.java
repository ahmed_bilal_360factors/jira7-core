package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertTrue;

/**
 * Tests the functionality of the announcement banner
 */
@WebTest({Category.FUNC_TEST, Category.BROWSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestAnnouncementBanner extends BaseJiraFuncTest {
    private static final String ANNOUNCEMENT = "<p>JIRA 3.4 IS NOW INSTALLED " +
            "<a href=\"http://confluence.atlassian.com/display/JIRA/JIRA+3.4+and+3.4.1+Release%20Notes\"" +
            " target=\"_blank\">Review Release Notes Here</a></p>";

    @Inject
    private LocatorFactory locator;

    @Test
    public void testAnnouncementBanner() {
        try {
            setBannerText(ANNOUNCEMENT);
        } finally {
            clearBannerText();
        }
    }

    @Test
    public void testAnnouncementBannerDoesNotShowInPrivateModeWithNoUser() {
        try {
            setBannerText(ANNOUNCEMENT);
            setBannerDisplayMode(false);
            navigation.logout();
            tester.beginAt("/secure/Dashboard.jspa");
            tester.assertTextNotPresent(ANNOUNCEMENT);
        } finally {
            navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
            clearBannerText();
        }
    }

    @Test
    public void testAnnouncementBannerShownInPublicModeWithNoUser() {
        try {
            setBannerText(ANNOUNCEMENT);
            setBannerDisplayMode(true);
            navigation.logout();
            tester.beginAt("/secure/Dashboard.jspa");
            tester.assertTextPresent(ANNOUNCEMENT);
        } finally {
            navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
            clearBannerText();
        }
    }

    private void setJIRAModePublic(boolean publicMode) {
        navigation.gotoAdminSection(Navigation.AdminSection.GENERAL_CONFIGURATION);
        tester.clickLink("edit-app-properties");
        tester.selectOption("mode", (publicMode) ? "Public" : "Private");
        tester.submit();
    }

    // Set the banner mode to be public or private
    protected void setBannerDisplayMode(boolean isPublic) {
        navigation.gotoAdminSection(Navigation.AdminSection.EDIT_ANNOUNCEMENT);
        tester.setWorkingForm("announcementbanner-edit");
        if (isPublic)
            tester.checkCheckbox("bannerVisibility", "public");
        else
            tester.checkCheckbox("bannerVisibility", "private");
        tester.submit();
    }

    protected void clearBannerText() {
        // clear the banner text
        gotoBannerPage();
        tester.setWorkingForm("announcementbanner-edit");
        tester.setFormElement("announcement", "");
        tester.submit();
    }

    protected void setBannerText(String bannerText) {
        gotoBannerPage();
        tester.setWorkingForm("announcementbanner-edit");
        tester.setFormElement("announcement", bannerText);
        tester.submit();

        CssLocator css = locator.css(".alertHeader");
        assertTrue(css.getHTML().contains(bannerText));
    }

    // NOTE: to use the preview link this method should be called with javascript turned on
    protected void gotoBannerPage() {
        navigation.gotoAdminSection(Navigation.AdminSection.EDIT_ANNOUNCEMENT);
    }

    // This does not work because of the javascript used to set the preview value
//   @Test public void testAnnouncementBannerPreview()
//    {
//        try
//        {
//            HttpUnitOptions.setScriptingEnabled(true);
//            gotoBannerPage();
//            setFormElement("announcement", ANNOUNCEMENT);
//            clickButton("previewButton");
//            assertTextPresent(ANNOUNCEMENT_DISPLAY);
//        }
//        finally
//        {
//            HttpUnitOptions.setScriptingEnabled(false);
//        }
//    }

}
