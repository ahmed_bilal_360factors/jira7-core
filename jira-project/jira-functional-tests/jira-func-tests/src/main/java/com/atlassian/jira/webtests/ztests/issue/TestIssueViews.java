/**
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@WebTest({Category.FUNC_TEST, Category.ISSUES})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestIssueViews.xml")
public class TestIssueViews extends BaseJiraFuncTest {
    private String exportsContent;

    @Inject
    private Administration administration;

    @Test
    public void testViewLinksChangeForModifiedFilter() {
        getViewsOption(null, "10000", null);

        // Check that the links contain the id of the saved filter.
        assertTextPresentInExportsContent("/sr/jira.issueviews:searchrequest-xml/10000/SearchRequest-10000.xml");
        assertTextPresentInExportsContent("/sr/jira.issueviews:searchrequest-rss/10000/SearchRequest-10000.xml");
        assertTextPresentInExportsContent("/sr/jira.issueviews:searchrequest-comments-rss/10000/SearchRequest-10000.xml");
        assertTextPresentInExportsContent("/sr/jira.issueviews:searchrequest-printable/10000/SearchRequest-10000.html");
        assertTextPresentInExportsContent("/sr/jira.issueviews:searchrequest-word/10000/SearchRequest-10000.doc");
        assertTextPresentInExportsContent("/sr/jira.issueviews:searchrequest-fullcontent/10000/SearchRequest-10000.html");
        // Check that the temporary filter links are not present
        assertTextNotPresentInExportsContent("/sr/jira.issueviews:searchrequest-xml/temp/SearchRequest.xml?");
        assertTextNotPresentInExportsContent("/sr/jira.issueviews:searchrequest-rss/temp/SearchRequest.xml?");
        assertTextNotPresentInExportsContent("/sr/jira.issueviews:searchrequest-comments-rss/temp/SearchRequest.xml?");
        assertTextNotPresentInExportsContent("/sr/jira.issueviews:searchrequest-printable/temp/SearchRequest.xml?");
        assertTextNotPresentInExportsContent("/sr/jira.issueviews:searchrequest-word/temp/SearchRequest.xml?");
        assertTextNotPresentInExportsContent("/sr/jira.issueviews:searchrequest-fullcontent/temp/SearchRequest.xml?");

        // Now lets modify the filter.
        getViewsOption("ORDER BY key DESC", "10000", "true");

        // All the issueview links should change to temporary links
        assertTextNotPresentInExportsContent("/sr/jira.issueviews:searchrequest-xml/10000/SearchRequest-10000.xml");
        assertTextNotPresentInExportsContent("/sr/jira.issueviews:searchrequest-rss/10000/SearchRequest-10000.xml");
        assertTextNotPresentInExportsContent("/sr/jira.issueviews:searchrequest-comments-rss/10000/SearchRequest-10000.xml");
        assertTextNotPresentInExportsContent("/sr/jira.issueviews:searchrequest-printable/10000/SearchRequest-10000.html");
        assertTextNotPresentInExportsContent("/sr/jira.issueviews:searchrequest-word/10000/SearchRequest-10000.doc");
        assertTextNotPresentInExportsContent("/sr/jira.issueviews:searchrequest-fullcontent/10000/SearchRequest-10000.html");

        // Check that the temp filter links are present
        assertTextPresentInExportsContent("/sr/jira.issueviews:searchrequest-xml/temp/SearchRequest.xml?");
        assertTextPresentInExportsContent("/sr/jira.issueviews:searchrequest-rss/temp/SearchRequest.xml?");
        assertTextPresentInExportsContent("/sr/jira.issueviews:searchrequest-comments-rss/temp/SearchRequest.xml?");
        assertTextPresentInExportsContent("/sr/jira.issueviews:searchrequest-printable/temp/SearchRequest.html?");
        assertTextPresentInExportsContent("/sr/jira.issueviews:searchrequest-word/temp/SearchRequest.doc?");
        assertTextPresentInExportsContent("/sr/jira.issueviews:searchrequest-fullcontent/temp/SearchRequest.html?");
    }

    @Test
    public void testCsvExportDisplayDependingOnDarkFeatures() {
        final String CSV_LINK = "sr/jira.issueviews:searchrequest-csv-all-fields/10000/SearchRequest-10000.csv";
        final String DISABLE_CSV = "jira.export.csv.disabled";

        backdoor.darkFeatures().enableForSite(DISABLE_CSV);

        getViewsOption(null, "10000", null);

        assertTextNotPresentInExportsContent(CSV_LINK);

        backdoor.darkFeatures().disableForSite(DISABLE_CSV);

        getViewsOption(null, "10000", null);

        assertTextPresentInExportsContent(CSV_LINK);
    }

    @Test
    public void testPermissionErrorWithGzipEnabled() {
        //JRADEV-3406
        administration.generalConfiguration().turnOnGZipCompression();

        navigation.issue().viewIssue("HSP-1");
        navigation.logout();

        navigation.gotoPage("/si/jira.issueviews:issue-html/HSP-1/HSP-1.html");
        tester.assertTextPresent("You must log in to access this page.");
    }

    @Test
    public void testEnableDisableIssueViewsPlugin() {
        getViewsOption();

        //Test that all links are present
        assertViewsLinkPresent("printable");
        assertViewsLinkPresent("fullContent");
        assertViewsLinkPresent("xml");
        assertViewsLinkPresent("rssIssues");
        assertViewsLinkPresent("rssComments");
        assertViewsLinkPresent("word");

        // Go through each of the different views and disable them one by one. Check that the
        // appropriate link no longer appears in the issue navigator if disabled
        togglePluginModule(false, "printable");
        getViewsOption();
        assertViewsLinkNotPresent("printable");
        assertViewsLinkPresent("fullContent");
        assertViewsLinkPresent("xml");
        assertViewsLinkPresent("rssIssues");
        assertViewsLinkPresent("rssComments");
        assertViewsLinkPresent("word");
        togglePluginModule(true, "printable");

        togglePluginModule(false, "fullcontent");
        getViewsOption();
        assertViewsLinkPresent("printable");
        assertViewsLinkNotPresent("fullContent");
        assertViewsLinkPresent("xml");
        assertViewsLinkPresent("rssIssues");
        assertViewsLinkPresent("rssComments");
        assertViewsLinkPresent("word");
        togglePluginModule(true, "fullcontent");

        togglePluginModule(false, "xml");
        getViewsOption();
        assertViewsLinkPresent("printable");
        assertViewsLinkPresent("fullContent");
        assertViewsLinkNotPresent("xml");
        assertViewsLinkPresent("rssIssues");
        assertViewsLinkPresent("rssComments");
        assertViewsLinkPresent("word");
        togglePluginModule(true, "xml");

        togglePluginModule(false, "rss");
        getViewsOption();
        assertViewsLinkPresent("printable");
        assertViewsLinkPresent("fullContent");
        assertViewsLinkPresent("xml");
        assertViewsLinkNotPresent("rssIssues");
        assertViewsLinkPresent("rssComments");
        assertViewsLinkPresent("word");
        togglePluginModule(true, "rss");

        togglePluginModule(false, "comments-rss");
        getViewsOption();
        assertViewsLinkPresent("printable");
        assertViewsLinkPresent("fullContent");
        assertViewsLinkPresent("xml");
        assertViewsLinkPresent("rssIssues");
        assertViewsLinkNotPresent("rssComments");
        assertViewsLinkPresent("word");
        togglePluginModule(true, "comments-rss");

        togglePluginModule(false, "word");
        getViewsOption();
        assertViewsLinkPresent("printable");
        assertViewsLinkPresent("fullContent");
        assertViewsLinkPresent("xml");
        assertViewsLinkPresent("rssIssues");
        assertViewsLinkPresent("rssComments");
        assertViewsLinkNotPresent("word");
        togglePluginModule(true, "word");

        // Now test special cases such as RSS and Excel, where if both modules are disabled,
        //nothing should be displayed (e.g.: RSS (Issues, Comments) should not be shown if
        // Issues and Comments are disabled)
        togglePluginModule(false, "rss");
        togglePluginModule(false, "comments-rss");
        getViewsOption();
        assertViewsLinkPresent("printable");
        assertViewsLinkPresent("fullContent");
        assertViewsLinkPresent("xml");
        assertTextNotPresentInExportsContent("| RSS");
        assertViewsLinkNotPresent("rssIssues");
        assertViewsLinkNotPresent("rssComments");
        assertViewsLinkPresent("word");
        togglePluginModule(true, "rss");
        togglePluginModule(true, "comments-rss");
    }


    private void getViewsOption() {
        getViewsOption("", null, null);
    }

    private void getViewsOption(String jql, String filterId, String modified) {
        exportsContent = backdoor.issueNavControl().getExportOptions(jql, filterId, modified);
    }

    public void assertTextPresentInExportsContent(String text) {
        assertTrue(exportsContent.contains(text));
    }

    public void assertTextNotPresentInExportsContent(String text) {
        assertFalse(exportsContent.contains(text));
    }

    private void assertViewsLinkPresent(String id) {
        assertTextPresentInExportsContent("\"" + id + "\"");
    }

    private void assertViewsLinkNotPresent(String id) {
        assertTextNotPresentInExportsContent("\"" + id + "\"");
    }

    private void togglePluginModule(boolean enable, String module) {
        if (enable) {
            administration.plugins().enablePluginModule("jira.issueviews", "jira.issueviews:searchrequest-" + module);
        } else {
            administration.plugins().disablePluginModule("jira.issueviews", "jira.issueviews:searchrequest-" + module);
        }
    }
}
