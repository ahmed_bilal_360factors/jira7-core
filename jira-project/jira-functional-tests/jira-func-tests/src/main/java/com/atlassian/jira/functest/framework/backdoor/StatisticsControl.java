package com.atlassian.jira.functest.framework.backdoor;

import java.util.Collection;
import java.util.Map;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

public class StatisticsControl extends BackdoorControl<StatisticsControl> {

    public StatisticsControl(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public Collection<String> getProjectsResultingFrom(String jql) {
        return (Collection<String>) createResource().path("statistics").path("projects").queryParam("jql", jql).get(Collection.class);
    }

    public Collection<String> getProjectsResultingFrom() {
        return (Collection<String>) createResource().path("statistics").path("projects").get(Collection.class);
    }

    public Map<String, Map<String, Integer>> getComponentsResultingFrom(String jql) {
        return (Map<String, Map<String, Integer>>) createResource().path("statistics").path("components").queryParam("jql", jql).get(Map.class);
    }
}
