package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.application.TestApplicationConfiguration;
import com.atlassian.jira.webtests.ztests.application.TestApplicationRoleCache;
import com.atlassian.jira.webtests.ztests.application.TestApplicationRoleResource;
import com.atlassian.jira.webtests.ztests.application.TestAtlassianApplication;
import com.atlassian.jira.webtests.ztests.license.TestCreateUserInMultipleApplicationRoles;
import com.atlassian.jira.webtests.ztests.license.TestCreateUserMultiApplicationInstance;
import com.atlassian.jira.webtests.ztests.license.TestCreateUserSingleApplicationInstance;
import com.atlassian.jira.webtests.ztests.license.TestLicenseFooters;
import com.atlassian.jira.webtests.ztests.license.TestLicenseMessages;
import com.atlassian.jira.webtests.ztests.license.TestPersonalLicense;
import com.atlassian.jira.webtests.ztests.license.TestServiceDeskLicenses;
import com.atlassian.jira.webtests.ztests.license.TestUserLimitedLicense;
import com.atlassian.jira.webtests.ztests.misc.TestSetup;
import com.atlassian.jira.webtests.ztests.misc.TestSignupWithLicensing;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of tests around JIRA licence handling
 *
 * @since v4.0
 */
public class FuncTestSuiteLicencing {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestCreateUserSingleApplicationInstance.class)
                .add(TestCreateUserMultiApplicationInstance.class)
                .add(TestCreateUserInMultipleApplicationRoles.class)
                .add(TestApplicationRoleCache.class)
                .add(TestApplicationRoleResource.class)
                .add(TestApplicationConfiguration.class)
                .add(TestAtlassianApplication.class)
                .add(TestPersonalLicense.class)
                .add(TestUserLimitedLicense.class)
                .add(TestLicenseMessages.class)
                .add(TestSetup.class)
                .add(TestLicenseFooters.class)
                .add(TestServiceDeskLicenses.class)
                .add(TestSignupWithLicensing.class)
                .build();
    }
}