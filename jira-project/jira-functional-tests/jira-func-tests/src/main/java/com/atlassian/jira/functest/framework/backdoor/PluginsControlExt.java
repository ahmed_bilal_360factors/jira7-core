package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.PluginsControl;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.sun.jersey.api.client.WebResource;

import java.util.ArrayList;
import java.util.List;

/**
 * Extended PluginsControl.
 *
 * @since v6.0
 */
public class PluginsControlExt extends PluginsControl {
    private static final String ISSUE_NAV_PLUGIN_KEY = "com.atlassian.jira.jira-issue-nav-plugin";

    public PluginsControlExt(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    /**
     * Refrain from using this. As of JIRA 6.0, jira-issue-nav-plugin is an integral part of JIRA.
     * <p>
     * The behaviour of disabling the plugin is undefined and Atlassian does not support such configuration.
     *
     * @deprecated
     */
    public PluginsControlExt enableIssueNavPlugin() {
        enablePlugin(ISSUE_NAV_PLUGIN_KEY);
        return this;
    }

    /**
     * Refrain from using this. As of JIRA 6.0, jira-issue-nav-plugin is an integral part of JIRA.
     * <p>
     * The behaviour of disabling the plugin is undefined and Atlassian does not support such configuration.
     *
     * @deprecated
     */
    public PluginsControlExt disableIssueNavPlugin() {
        disablePlugin(ISSUE_NAV_PLUGIN_KEY);
        return this;
    }

    /**
     * @return list of installed plugin keys
     */
    public List<String> listPlugins() {
        try {
            final JSONArray plugins = new JSONObject(upmResource().get(String.class)).getJSONArray("plugins");
            List<String> ret = new ArrayList<>(plugins.length());
            for (int i = 0; i < plugins.length(); i++) {
                JSONObject plugin = plugins.getJSONObject(i);
                ret.add(plugin.getString("key"));
            }
            return ret;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private WebResource upmResource() {
        return resourceRoot(rootPath).path("rest").path("plugins").path("1.0/");
    }

}
