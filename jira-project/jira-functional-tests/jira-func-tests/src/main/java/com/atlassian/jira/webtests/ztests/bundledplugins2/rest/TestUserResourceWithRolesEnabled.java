package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.beans.ApplicationRole;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.testkit.client.restclient.User;
import com.atlassian.jira.testkit.client.restclient.UserBean;
import com.atlassian.jira.testkit.client.restclient.UserClient;
import com.atlassian.jira.webtests.LicenseKeys;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Test for user resource with enabled license roles
 *
 * @since v7.0
 */
@RestoreBlankInstance
@WebTest({Category.FUNC_TEST, Category.REST})
public class TestUserResourceWithRolesEnabled extends BaseJiraRestTest {
    public static final String FRED_USERNAME = "fred";
    public static final String CHARLIE_USERNAME = "charlie";
    public static final String SUPERUSER_USERNAME = "superuser";

    private UserClient userClient;

    @Before
    public void setUpInstance() {
        backdoor.restoreBlankInstance(LicenseKeys.MULTI_ROLE.getLicenseString());

        backdoor.usersAndGroups().addGroup("jira-software-group");
        backdoor.applicationRoles().putRoleAndSetDefault("jira-software", "jira-software-group");
        backdoor.usersAndGroups().addUserToGroup(FRED_USERNAME, "jira-software-group");

        backdoor.usersAndGroups().addGroup("test-group");
        backdoor.applicationRoles().putRoleAndSetDefault("jira-func-test", "test-group");

        userClient = new UserClient(environmentData);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void shouldReturnUserRoles() throws Exception {
        final User user = userClient.get(FRED_USERNAME, User.Expand.applicationRoles);

        assertThat(user.applicationRoles.items, Matchers.containsInAnyOrder(
                roleWithKeyAndName("jira-core", "JIRA Core"),
                roleWithKeyAndName("jira-software", "JIRA Software")));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void shouldAddUserToApplication() throws Exception {
        backdoor.usersAndGroups().addUser(CHARLIE_USERNAME);
        userClient.addUserToApplication(CHARLIE_USERNAME, "jira-core");

        User user = userClient.get(CHARLIE_USERNAME, User.Expand.applicationRoles);
        assertThat(user.applicationRoles.items, Matchers.contains(
                roleWithKeyAndName("jira-core", "JIRA Core")));

        userClient.addUserToApplication(CHARLIE_USERNAME, "jira-func-test");
        user = userClient.get(CHARLIE_USERNAME, User.Expand.applicationRoles);
        assertThat(user.applicationRoles.items, Matchers.containsInAnyOrder(
                roleWithKeyAndName("jira-core", "JIRA Core"),
                roleWithKeyAndName("jira-func-test", "Test Product")));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void shouldRemoveUserFromApplication() throws Exception {
        final UserBean userBean = UserBean.builder()
                .setName(SUPERUSER_USERNAME)
                .setDisplayName(SUPERUSER_USERNAME)
                .setEmailAddress("email@email.com")
                .setApplicationKeys(ImmutableList.of("jira-core", "jira-func-test"))
                .build();

        userClient.createUser(userBean);
        User user = userClient.get(SUPERUSER_USERNAME, User.Expand.applicationRoles);
        assertThat(user.applicationRoles.items, Matchers.contains(
                roleWithKeyAndName("jira-func-test", "Test Product"),
                roleWithKeyAndName("jira-core", "JIRA Core")));

        userClient.removeUserFromApplication(SUPERUSER_USERNAME, "jira-func-test");
        user = userClient.get(SUPERUSER_USERNAME, User.Expand.applicationRoles);
        assertThat(user.applicationRoles.items, Matchers.contains(
                roleWithKeyAndName("jira-core", "JIRA Core")));

        userClient.removeUserFromApplication(SUPERUSER_USERNAME, "jira-core");
        user = userClient.get(SUPERUSER_USERNAME, User.Expand.applicationRoles);
        assertThat(user.applicationRoles.items, Matchers.empty());
    }

    @Test
    public void shouldDoNothingWhenUserIsNotInApplicationAndTryToRemove() {
        final UserBean userBean = UserBean.builder()
                .setName("user-without-application")
                .setDisplayName("display-name")
                .setEmailAddress("email@email.address.com")
                .setApplicationKeys(ImmutableList.of("jira-core"))
                .build();

        userClient.createUser(userBean);

        userClient.removeUserFromApplication(userBean.getName(), "jira-func-test");
        final User user = userClient.get(userBean.getName(), User.Expand.applicationRoles);
        assertThat(user.applicationRoles.items, Matchers.contains(
                roleWithKeyAndName("jira-core", "JIRA Core")));
    }

    @Test
    public void shouldReturnErrorWhenTryToAddUserToApplication() {
        final UserBean userBean = createUserWithoutApplications("user-without-application");

        final Response response = userClient.removeUserFromApplicationResponse(userBean.getName(), "non-existing-application");
        assertEquals(response.statusCode, 400);
    }

    @Test
    public void shouldNotAddUserToApplicationWhenUserIsNotAdmin() {
        final UserBean userBean = createUserWithoutApplications("user");

        final String NONADMIN_USERNAME = "nonadmin";
        backdoor.usersAndGroups().addUser(NONADMIN_USERNAME, NONADMIN_USERNAME, NONADMIN_USERNAME, NONADMIN_USERNAME + "@nonadmin.com");

        userClient.loginAs(NONADMIN_USERNAME);
        final Response response = userClient.addUserToApplicationResponse(userBean.getName(), "jira-core");
        assertEquals(response.statusCode, 401);
    }

    private UserBean createUserWithoutApplications(final String username) {
        final UserBean userBean = UserBean.builder()
                .setName(username)
                .setDisplayName(username + "display-name")
                .setEmailAddress(username + "email@email.address.com")
                .build();

        userClient.createUser(userBean);
        return userBean;
    }

    private static Matcher<ApplicationRole> roleWithKeyAndName(final String key, final String name) {
        return new TypeSafeMatcher<ApplicationRole>() {
            @Override
            protected boolean matchesSafely(final ApplicationRole item) {
                return item.name.equals(name) && item.key.equals(key);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("ApplicationRole with key ").appendValue(key).appendText(" and name ").appendValue(name);
            }
        };
    }
}
