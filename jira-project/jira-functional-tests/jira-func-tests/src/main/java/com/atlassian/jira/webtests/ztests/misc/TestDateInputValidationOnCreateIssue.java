package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.BROWSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestDateInputValidationOnCreateIssue extends BaseJiraFuncTest {
    private static final String ISSUE_SUMMARY_FORM_ELEMENT = "summary";
    private static final String ISSUE_DUE_DATE_FORM_ELEMENT = "duedate";

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
    }

    @Test
    public void testDueDateInputValidation() {
        _testDueDateValidationWith("00/ABC/07");
        _testDueDateValidationWith("A/06/07");
        _testDueDateValidationWith("30/00/07");
        _testDueDateValidationWith("XX/XX/XX");
        _testDueDateValidationWith("0/00/07");
        _testDueDateValidationWith("-15/JUL/07");
        _testDueDateValidationWith("50/JAN/07");
        _testDueDateValidationWith("163/06/07");
        _testDueDateValidationWith("50/JAN/06");
        _testDueDateValidationWith("100/FEB/05");
        _testDueDateValidationWith("32/DEC/06");
    }

    public void _testDueDateValidationWith(String dueDate) {
        navigation.issue().goToCreateIssueForm(null, null);
        tester.setFormElement(ISSUE_SUMMARY_FORM_ELEMENT, "Testing Date");
        tester.setFormElement(ISSUE_DUE_DATE_FORM_ELEMENT, dueDate);
        tester.submit("Create");

        textAssertions.assertTextPresent(locator.page(), "You did not enter a valid date");
    }
}

