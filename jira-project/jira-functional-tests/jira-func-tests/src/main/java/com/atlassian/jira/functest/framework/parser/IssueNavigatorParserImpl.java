package com.atlassian.jira.functest.framework.parser;

import com.atlassian.jira.functest.framework.navigator.NavigatorCondition;
import com.atlassian.jira.functest.framework.navigator.NavigatorSearch;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;
import java.util.Collection;

/**
 * Simple implementation of the {@link com.atlassian.jira.functest.framework.parser.IssueNavigatorParser} interface.
 *
 * @since v3.13
 */
public class IssueNavigatorParserImpl implements IssueNavigatorParser {

    public IssueNavigatorParserImpl(WebTester tester, JIRAEnvironmentData environmentData, int logIndentLevel) {
        this();
    }

    @Inject
    public IssueNavigatorParserImpl() {
    }

    public NavigatorSearch parseSettings(WebTester tester, Collection /*<NavigatorCondition>*/ conditions) {
        for (final Object condition : conditions) {
            NavigatorCondition navigatorCondition = (NavigatorCondition) condition;
            navigatorCondition.parseCondition(tester);
        }

        return new NavigatorSearch(conditions);
    }
}
