package com.atlassian.jira.webtests.ztests.customfield;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.admin.CustomFields;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_CASCADING_SELECT_SEARCHER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_MULTI_SELECT_SEARCHER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_CASCADINGSELECT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_CHECKBOX;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_MULTICHECKBOXES;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_MULTISELECT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_RADIO;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_SELECT;
import static com.atlassian.jira.functest.framework.suite.Category.CUSTOM_FIELDS;
import static com.atlassian.jira.functest.framework.suite.Category.FIELDS;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.SECURITY;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


@WebTest({FUNC_TEST, CUSTOM_FIELDS, FIELDS, SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestCustomFieldOptionsXss extends BaseJiraFuncTest {

    private static final String RAW_OPTION_TEMPLATE = "<div><b>%s</b></div>";
    private static final String HTML_OPTION_TEMPLATE = "<div><b>%s</b></div>";
    private static final String ESCAPED_OPTION_TEMPLATE = "&lt;div&gt;&lt;b&gt;%s&lt;/b&gt;&lt;/div&gt;";

    private static final Iterable<String> CUSTOM_FIELD_TYPES = ImmutableList.of(
            CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_SELECT),
            CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_RADIO),
            CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_CHECKBOX),
            CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_MULTISELECT)
    );

    private static final Map<String, List<String>> SEARCHERS = ImmutableMap.<String, List<String>>builder()
            .put(CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_MULTI_SELECT_SEARCHER), toBuiltInCustomFieldKeys(CUSTOM_FIELD_TYPE_SELECT, CUSTOM_FIELD_TYPE_RADIO, CUSTOM_FIELD_TYPE_MULTICHECKBOXES))
            .put(CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_CASCADING_SELECT_SEARCHER), toBuiltInCustomFieldKeys(CUSTOM_FIELD_TYPE_CASCADINGSELECT))
            .build();

    private static List<String> toBuiltInCustomFieldKeys(final String... keys) {
        return Arrays.stream(keys)
                .map(CustomFields::builtInCustomFieldKey)
                .collect(Collectors.toList());
    }

    private static String optionValue(final String fieldId) {
        return String.format(RAW_OPTION_TEMPLATE, fieldId);
    }

    private static String optionValueHtml(final String fieldId) {
        return String.format(HTML_OPTION_TEMPLATE, fieldId);
    }

    private static String optionValueEscaped(final String fieldId) {
        return String.format(ESCAPED_OPTION_TEMPLATE, fieldId);
    }

    @Before
    public void setUp() throws Exception {
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_HTML_IN_CUSTOM_FIELDS_ENABLED, true);
    }

    @Test
    public void testCustomFieldDescriptionsCanBeRenderedAsRawHtmlOrWikiMarkup() throws Exception {
        for (final String type : CUSTOM_FIELD_TYPES) {
            testSingleCustomFieldDescriptionOnCustomFieldsScreen(type);
        }
    }

    @Test
    public void testCustomFieldDescriptionsCanBeRenderedAsRawHtmlOrWikiMarkUpInIssueNavigator() throws Exception {
        for (final Map.Entry<String, List<String>> entry : SEARCHERS.entrySet()) {
            testSingleCustomFieldDescriptionOnIssueNavigatorScreen(entry.getValue(), entry.getKey());
        }
    }

    private void testSingleCustomFieldDescriptionOnIssueNavigatorScreen(final List<String> customFieldTypes, final String customFieldSearcher) {
        for (final String customFieldType : customFieldTypes) {
            testSingleCustomFieldDescriptionOnIssueNavigatorScreen(customFieldType, customFieldSearcher);
        }
    }

    private void testSingleCustomFieldDescriptionOnIssueNavigatorScreen(final String customFieldType, final String customFieldSearcher) {
        final String fieldId = backdoor.customFields().createCustomField(customFieldType + "-name", "description" + customFieldType, customFieldType, customFieldSearcher);
        backdoor.customFields().addOption(fieldId, optionValue(customFieldType));

        // test that the description is rendered as raw HTML
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_HTML_IN_CUSTOM_FIELDS_ENABLED, true);
        tester.gotoPage("secure/QueryComponentRendererEdit!Default.jspa?fieldId=" + fieldId + "&decorator=none&jqlContext=");
        assertTrue("HTML in Custom Fields is enabled so the options should be rendered as raw HTML for " + customFieldSearcher, getPageSource().contains(optionValueHtml(customFieldType)));

        // now test that the description is rendered as Wiki markup
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_HTML_IN_CUSTOM_FIELDS_ENABLED, false);
        tester.gotoPage("secure/QueryComponentRendererEdit!Default.jspa?fieldId=" + fieldId + "&decorator=none&jqlContext=");
        assertTrue("HTML in Custom Fields is disabled so the optons should be escaped for " + customFieldSearcher, getPageSource().contains(optionValueEscaped(customFieldType)));
        assertFalse("HTML in Custom Fields is disabled so the optons should be escaped for " + customFieldSearcher, getPageSource().contains(optionValueHtml(customFieldType)));

        backdoor.customFields().deleteCustomField(fieldId);
    }

    private void testSingleCustomFieldDescriptionOnCustomFieldsScreen(final String customFieldType) {
        final String fieldId = backdoor.customFields().createCustomField(customFieldType + "-name", "description" + customFieldType, customFieldType, null);
        backdoor.customFields().addOption(fieldId, optionValue(customFieldType));

        // test that the description is rendered as raw HTML
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_HTML_IN_CUSTOM_FIELDS_ENABLED, true);
        goToConfigureCustomFields(fieldId);
        assertTrue("HTML in Custom Fields is enabled so the options should be rendered as raw HTML for " + customFieldType, getPageSource().contains(optionValueHtml(customFieldType)));

        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_HTML_IN_CUSTOM_FIELDS_ENABLED, false);
        goToConfigureCustomFields(fieldId);
        assertTrue("HTML in Custom Fields is disabled so the options should be escaped for " + customFieldType, getPageSource().contains(optionValueEscaped(customFieldType)));
        assertFalse("HTML in Custom Fields is disabled so the options should be escaped for " + customFieldType, getPageSource().contains(optionValueHtml(customFieldType)));

        backdoor.customFields().deleteCustomField(fieldId);
    }

    private void goToConfigureCustomFields(final String customFieldId) {
        tester.gotoPage("/secure/admin/ConfigureCustomField!default.jspa?customFieldId=" + CustomFieldUtils.getCustomFieldId(customFieldId));
    }

    private String getPageSource() {
        return tester.getDialog().getResponseText();
    }
}
