package com.atlassian.jira.functest.rule;

import org.junit.runners.model.Statement;

/**
 * Decorates statement with code that can be executed before statement
 *
 * @since v6.5
 */
public class StatementDecorator extends Statement {
    private final Statement delegate;
    private final Action beforeAction;

    public StatementDecorator(final Statement delegate, final Action beforeAction) {
        this.delegate = delegate;
        this.beforeAction = beforeAction;
    }

    @Override
    public void evaluate() throws Throwable {
        beforeAction.execute();
        delegate.evaluate();
    }

    public interface Action {
        void execute() throws Throwable;
    }
}
