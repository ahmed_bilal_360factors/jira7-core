package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.issue.TestIssueSecurityWithGroupsAndRoles;
import com.atlassian.jira.webtests.ztests.issue.TestIssueSecurityWithRoles;
import com.atlassian.jira.webtests.ztests.misc.TestFullContentShowsRoleEncoded;
import com.atlassian.jira.webtests.ztests.project.TestMultipleProjectsWithIssueSecurityWithRoles;
import com.atlassian.jira.webtests.ztests.project.TestProjectRoles;
import com.atlassian.jira.webtests.ztests.project.TestViewProjectRoleUsage;
import com.atlassian.jira.webtests.ztests.user.TestEditUserProjectRoles;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of tests related to Roles
 *
 * @since v4.0
 */
public class FuncTestSuiteRoles {

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestEditUserProjectRoles.class)
                .add(TestViewProjectRoleUsage.class)
                .add(TestProjectRoles.class)
                .add(TestIssueSecurityWithGroupsAndRoles.class)
                .add(TestIssueSecurityWithRoles.class)
                .add(TestMultipleProjectsWithIssueSecurityWithRoles.class)
                .add(TestFullContentShowsRoleEncoded.class)
                .build();
    }
}