package com.atlassian.jira.functest.framework.admin;

import com.atlassian.jira.functest.config.ConfigFile;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.project.type.ProjectTypeKey;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;

import java.io.File;
import java.util.List;

/**
 * This is a bit of a hack until renaissance mode is turned on by default.  For now we change the backup so that all
 * projects have the 'business' type.  Otherwise the upgrade task setting project types to 'software' stops the project
 * import from happening.
 * <p>
 * This can be deleted once we're past renaissance.
 */
public class ProjectImportTypeFix {
    private static final String BUSINESS = "business";
    private final Backdoor backdoor;

    public ProjectImportTypeFix(final Backdoor backdoor) {
        this.backdoor = backdoor;
    }

    public void updateAllProjectsToBusinessType() {
        for (com.atlassian.jira.testkit.client.restclient.Project project : backdoor.project().getProjects()) {
            backdoor.project().updateProjectType(Long.valueOf(project.id), new ProjectTypeKey(BUSINESS));
        }
    }

    public void rewriteProjectTypes(final File backupFile) {
        final ConfigFile configFile = ConfigFile.create(backupFile);
        final Document document = configFile.readConfig();
        boolean updatedProjects = false;

        @SuppressWarnings("unchecked")
        final List<Element> projects = document.getRootElement().elements("Project");
        for (Element project : projects) {
            final Attribute projecttype = project.attribute("projecttype");
            if (projecttype != null) {
                projecttype.setValue(BUSINESS);
                updatedProjects = true;
            }
        }
        if (updatedProjects) {
            configFile.writeFile(document);
        }
    }
}
