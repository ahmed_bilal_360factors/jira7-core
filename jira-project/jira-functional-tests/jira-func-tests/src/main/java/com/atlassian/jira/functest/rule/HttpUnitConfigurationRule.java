package com.atlassian.jira.functest.rule;

import com.atlassian.jira.functest.framework.HttpUnitConfiguration;
import com.atlassian.jira.functest.framework.TestAnnotationsExtractor;
import com.meterware.httpunit.HttpUnitOptions;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * @since v7.1
 */
public class HttpUnitConfigurationRule implements TestRule {

    @Override
    public Statement apply(final Statement statement, final Description description) {

        return new Statement() {
            @Override
            public void evaluate() throws Throwable {

                HttpUnitOptions.reset();
                overrideHttpUnitSettings(description);
                try  {
                    statement.evaluate();
                } finally {
                    Defaults.restoreDefaults();
                }
            }
        };
    }

    private void overrideHttpUnitSettings(final Description description) {
        overrideHttpUnitSettings(new TestAnnotationsExtractor(description)
                .findAnnotationFor(HttpUnitConfiguration.class)
                .orElseGet(Defaults::getDefaults));
    }

    public static void restoreDefaults() {
        HttpUnitOptions.reset();
        Defaults.restoreDefaults();
    }

    private static void overrideHttpUnitSettings(final HttpUnitConfiguration configuration) {
        if (configuration.characterSet().isEmpty()) {
            HttpUnitOptions.resetDefaultCharacterSet();
        } else {
            HttpUnitOptions.setDefaultCharacterSet(configuration.characterSet());
        }
        HttpUnitOptions.setExceptionsThrownOnErrorStatus(configuration.throwOnErrorStatus());
        HttpUnitOptions.setScriptingEnabled(configuration.enableScripting());
        HttpUnitOptions.setExceptionsThrownOnScriptError(configuration.throwOnScriptError());
    }

    /**
     * This class is here so we can annotate it with the annotation to get its defaults.
     */
    @HttpUnitConfiguration
    private static class Defaults {
        public static void restoreDefaults() {
            overrideHttpUnitSettings(getDefaults());
        }

        private static HttpUnitConfiguration getDefaults() {
            return Defaults.class.getAnnotation(HttpUnitConfiguration.class);
        }
    }
}
