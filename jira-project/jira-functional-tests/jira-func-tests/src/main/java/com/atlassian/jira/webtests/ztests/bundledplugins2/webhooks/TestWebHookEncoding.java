package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookRegistrationClient;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.testkit.client.util.JsonMatchers.hasField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.WEBHOOKS, Category.REFERENCE_PLUGIN})
public class TestWebHookEncoding extends AbstractWebHookTest {
    @Before
    public void setUp() throws Exception {
        super.setUpTest();
    }

    @Test
    public void testUTF8Encoding() throws InterruptedException, JSONException {
        backdoor.restoreBlankInstance();

        final WebHookRegistrationClient.Registration registration = new WebHookRegistrationClient.Registration();
        registration.name = "Comment Issue Web Hook";
        registration.events = new String[]{"jira:issue_updated", "jira:issue_created"};
        responseTester.registerWebhook(registration);

        final IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "This is issue with utf-8 chars čšžćđ");
        final JSONObject event = new JSONObject(getWebHookResponse().getJson());

        assertThat(event, hasField("issue.fields.summary"));
        assertEquals("This is issue with utf-8 chars čšžćđ", event.getJSONObject("issue").getJSONObject("fields").getString("summary"));

        backdoor.issues().setDescription(issue.key(), "Description with ğĞçüöİıÖ");
        final JSONObject updateEvent = new JSONObject(getWebHookResponse().getJson());

        assertThat(updateEvent, hasField("issue.fields.description"));
        assertEquals("Description with ğĞçüöİıÖ", updateEvent.getJSONObject("issue").getJSONObject("fields").getString("description"));
    }
}
