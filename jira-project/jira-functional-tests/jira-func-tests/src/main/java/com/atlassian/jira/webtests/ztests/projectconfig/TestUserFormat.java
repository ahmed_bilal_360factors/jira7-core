package com.atlassian.jira.webtests.ztests.projectconfig;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.BROWSING})
@RestoreBlankInstance
@LoginAs(user = ADMIN_USERNAME)
public class TestUserFormat extends BaseJiraFuncTest {

    @Test
    public void testProjectSummary() {
        tester.gotoPage("/plugins/servlet/project-config/HSP/roles");
        assertions.assertProfileLinkPresent("projectLead_admin", ADMIN_FULLNAME);
    }
}
