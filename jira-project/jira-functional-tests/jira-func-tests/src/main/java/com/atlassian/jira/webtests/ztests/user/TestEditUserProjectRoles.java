package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.ProjectRole;
import com.atlassian.jira.testkit.client.restclient.ProjectRoleClient;
import com.meterware.httpunit.WebTable;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;

import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_ADMIN_ROLE_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@WebTest({Category.FUNC_TEST, Category.PROJECTS, Category.ROLES, Category.USERS_AND_GROUPS})
@Restore("TestEditUserProjectRoles.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestEditUserProjectRoles extends BaseJiraFuncTest {
    private static final String ICON_DIRECT_MEMBER = "/images/icons/emoticons/user_16.gif";
    private static final String ICON_NOT_MEMBER = "/images/icons/emoticons/user_bw_16.gif";
    private static final String ICON_GROUP_MEMBER = "/images/icons/emoticons/group_16.gif";

    @Test
    public void testNoProjectAssociationsForViewPage() {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.clickLink("tester");
        tester.clickLink("viewprojectroles_link");
        tester.assertTextPresent("There are currently no project role associations for this user.");
    }

    @Ignore("JRADEV-15506 fix this test - there is no longer one big table - there are multiple and they have the same class"
            + " will need to add some unique identifier to the tables on viewuserprojectroles.jsp")
    @Test
    public void testAddAssociationToProjects() throws SAXException {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.clickLink("john");
        tester.clickLink("viewprojectroles_link");
        tester.assertTextNotPresent("There are currently no project role associations for this user.");
        tester.clickLinkWithText("Edit Project Roles");
        tester.assertTextNotPresent("There are currently no project role associations for this user.");

        // Add an association to Chimps
        tester.gotoPage("secure/admin/user/EditUserProjectRoles!refresh.jspa?projects_to_add=10010&name=john");
        tester.assertCheckboxNotSelected("10010_10011");
        tester.assertCheckboxNotSelected("10010_10010");
        tester.assertCheckboxNotSelected("10010_10012");
        tester.checkCheckbox("10010_10011", "on");
        tester.checkCheckbox("10010_10010", "on");
        tester.assertCheckboxSelected("10010_10011");
        tester.assertCheckboxSelected("10010_10010");

        tester.submit("Save");

        final WebTable userProjectRolesTable = tester.getDialog().getResponse().getTableWithID("projecttable");
        assertions.getTableAssertions().assertTableCellHasText(userProjectRolesTable, 2, 0, "Chimps");
        assertions.getTableAssertions().assertTableCellHasImage(userProjectRolesTable, 2, 1, ICON_DIRECT_MEMBER);
        assertions.getTableAssertions().assertTableCellHasImage(userProjectRolesTable, 2, 2, ICON_DIRECT_MEMBER);
        assertions.getTableAssertions().assertTableCellHasImage(userProjectRolesTable, 2, 3, ICON_NOT_MEMBER);
        assertions.getTableAssertions().assertTableCellHasText(userProjectRolesTable, 3, 0, "Donkeys");
        assertions.getTableAssertions().assertTableCellHasImage(userProjectRolesTable, 3, 1, ICON_DIRECT_MEMBER);
        assertions.getTableAssertions().assertTableCellHasImage(userProjectRolesTable, 3, 1, ICON_GROUP_MEMBER);
        assertions.getTableAssertions().assertTableCellHasImage(userProjectRolesTable, 3, 2, ICON_DIRECT_MEMBER);
        assertions.getTableAssertions().assertTableCellHasImage(userProjectRolesTable, 3, 3, ICON_DIRECT_MEMBER);
        assertions.getTableAssertions().assertTableCellHasText(userProjectRolesTable, 4, 0, PROJECT_HOMOSAP);
        assertions.getTableAssertions().assertTableCellHasImage(userProjectRolesTable, 4, 1, ICON_GROUP_MEMBER);
        assertions.getTableAssertions().assertTableCellHasImage(userProjectRolesTable, 4, 2, ICON_NOT_MEMBER);
        assertions.getTableAssertions().assertTableCellHasImage(userProjectRolesTable, 4, 3, ICON_NOT_MEMBER);

        // Now go to the project and see that the association is present in the Project Role Browser
        final ProjectRoleClient prc = new ProjectRoleClient(environmentData);
        final ProjectRole projectRole = prc.get("HSP", "Administrators");
        assertEquals("admin", projectRole.actors.get(0).name);
    }

    /**
     * The import xml used in the setup has the following:
     * <ul>
     * <li> Has a user with username 'user1' and fullname 'SameName'
     * <li> Another user with username 'user2' and fullname 'SameName'
     * <li> user 'user1' is already a member of Role 'Developers'
     * </ul>
     * This test will try to add user 'user2' to the 'Developers' Role.
     * <p/>
     * Before release of 3.7, 'user2' could not be added to the 'Developers' role
     * because a user with the same fullname was already added.
     *
     * @throws org.xml.sax.SAXException caused from table cell checks
     */
    @Ignore("JRADEV-15506 fix this test - there is no longer one big table - there are multiple and they have the same class"
            + " will need to add some unique identifier to the tables on viewuserprojectroles.jsp")
    @Test
    public void testAddAssociationToProjectThatHasAMemberWithTheSameFullname() throws SAXException {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.clickLink("user2");
        tester.clickLink("viewprojectroles_link");
        tester.clickLinkWithText("Edit Project Roles");
        tester.assertTextPresent("There are currently no project role associations for this user.");

        // Add an association to project Donkeys
        tester.gotoPage("secure/admin/user/EditUserProjectRoles!refresh.jspa?projects_to_add=10020&name=user2");
        tester.assertCheckboxNotSelected("10020_10011");
        tester.assertCheckboxNotSelected("10020_10010");
        tester.assertCheckboxNotSelected("10020_10012");
        tester.checkCheckbox("10020_10011", "on");
        tester.checkCheckbox("10020_10010", "on");
        tester.assertCheckboxNotSelected("10020_10012"); //dont select users still

        tester.submit("Save");
        tester.assertTextPresent("Donkeys");
        final WebTable userProjectRolesTable = tester.getDialog().getResponse().getTableWithID("projecttable");
        assertions.getTableAssertions().assertTableCellHasText(userProjectRolesTable, 2, 0, "Donkeys");
        assertions.getTableAssertions().assertTableCellHasImage(userProjectRolesTable, 2, 1, ICON_DIRECT_MEMBER);
        assertions.getTableAssertions().assertTableCellHasImage(userProjectRolesTable, 2, 2, ICON_DIRECT_MEMBER);
        assertions.getTableAssertions().assertTableCellHasImage(userProjectRolesTable, 2, 3, ICON_NOT_MEMBER);
        tester.assertTextNotPresent("There are currently no project role associations for this user.");

        // Now go to the project and see that the association is present in the Project Role Browser
        final ProjectRoleClient prc = new ProjectRoleClient(environmentData);
        final Map dky = prc.get("DKY");
        assertEquals(3, dky.size());

        ProjectRole projectRole = prc.get("DKY", "Administrators");

        assertEquals(4, projectRole.actors.size());
        assertEquals("user1", projectRole.actors.get(3).name);
        assertEquals("user2", projectRole.actors.get(2).name);

        projectRole = prc.get("DKY", "Developers");

        assertEquals(3, projectRole.actors.size());
        assertEquals("user1", projectRole.actors.get(2).name);
        assertEquals("user2", projectRole.actors.get(1).name);

        projectRole = prc.get("DKY", "Users");

        assertEquals(2, projectRole.actors.size());
        assertEquals("user1", projectRole.actors.get(1).name);
        assertFalse("user2".equals(projectRole.actors.get(0).name));
    }

    @Test
    public void testRemoveAssociationFromProject() {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.clickLink(ADMIN_USERNAME);
        tester.clickLink("viewprojectroles_link");
        tester.clickLinkWithText("Edit Project Roles");

        // Assert that the two projects we expect to be there are present
        tester.assertTextPresent(PROJECT_MONKEY);
        tester.assertTextPresent(PROJECT_HOMOSAP);

        // remove the roles for monkey
        tester.uncheckCheckbox("10001_10011");
        tester.submit("Save");

        // Verify that only the homosap project is present
        tester.assertTextPresent(PROJECT_HOMOSAP);
        tester.assertTextNotPresent(PROJECT_MONKEY);

        // Make sure that only homosap shows on the edit page as well
        tester.clickLinkWithText("Edit Project Roles");

        tester.assertTextInTable("projecttable", PROJECT_HOMOSAP);
        tester.assertTextNotInTable("projecttable", PROJECT_MONKEY);
    }

    @Test
    public void testGlobalAdminCanRemoveAnyoneFromRole() {
        navigation.gotoAdmin();

        //First lets remove the various groups from the Admin role.
        backdoor.getTestkit().projectRole().deleteGroup("DKY", "Administrators", "jira-administrators");
        backdoor.getTestkit().projectRole().deleteGroup(PROJECT_HOMOSAP_KEY, "Administrators", "jira-developers");

        //add the administer projects permission for the admin role.
        backdoor.permissionSchemes().addProjectRolePermission(DEFAULT_PERM_SCHEME_ID, ADMINISTER_PROJECTS, JIRA_ADMIN_ROLE_ID);


        //now lets try to remove the admin user from the admin role.  This should not produce an error
        //(namely You can not remove a user/group that will result in completely removing yourself from this role.)
        //See JRA-12528.
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.gotoPage("/secure/admin/user/ViewUserProjectRoles!default.jspa?returnUrl=UserBrowser.jspa&name=admin");
        tester.clickLinkWithText("Edit Project Roles");
        tester.uncheckCheckbox("10000_10011");
        tester.uncheckCheckbox("10001_10011");
        tester.submit("Save");
        tester.assertTextNotPresent("You can not remove a user/group that will result in completely removing yourself from this role.");
        tester.assertTextPresent("View Project Roles for User");
    }

    @Test
    public void testGroupAssociationPresentInEditProjectRolesForUser() {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.clickLink(ADMIN_USERNAME);
        tester.clickLink("viewprojectroles_link");
        tester.assertTextPresent("jira-developers");
    }

    @Test
    public void testGroupAssociationPresentInViewProjectRolesForUser() {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.clickLink("barney");
        tester.clickLink("viewprojectroles_link");
        tester.assertTextPresent("jira-developers");
    }
}
