package com.atlassian.jira.webtests.ztests.crowd.embedded;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.3
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
@Restore("TestCrowdServerConfiguration.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestCrowdServerConfiguration extends BaseJiraFuncTest {
    private static final String MY_APP = "My App";
    private static final String MY_APP_ADDRESS = "192.168.0.1";

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void goToCrowdServerConfiguration() {
        navigation.gotoPage("secure/project/ConfigureCrowdServer.jspa");
    }

    @Test
    public void testAddApplication() throws Exception {
        tester.clickLink("crowd-add-application");

        final String appName = "FishEye";
        final String appAddress = "127.0.0.1";

        // first try to create with invalid values
        submitEditForm("", "", "not_an_address");
        textAssertions.assertTextPresent(locator.css("#edit-crowd-application-name-error"), "You must provide an application name.");
        textAssertions.assertTextPresent(locator.css("#edit-crowd-application-credential-error"), "A password is required.");
        textAssertions.assertTextPresent(locator.css("#edit-crowd-application-remoteAddresses-error"), "'not_an_address' is not a valid IP address.");

        // fill in the form, then check that the app has been created and is shown in the list
        submitEditForm(appName, "pass", appAddress);
        assertAppPresent(appName, appAddress);
    }

    /*
     * Tests that we can add an app, then edit it, and then delete it.
     */
    @Test
    public void testEditApplication() throws Exception {
        final String newAppName = "MyNewApp";
        final String newAppAddress = "127.0.0.127";

        // try to edit with invalid values
        tester.clickLink("crowd-edit-application-" + MY_APP);
        submitEditForm("", "", "not_an_address");
        textAssertions.assertTextPresent(locator.css("#edit-crowd-application-name-error"), "You must provide an application name.");
        textAssertions.assertTextNotPresent(locator.css("#edit-crowd-application-credential-error"), "A password is required.");
        textAssertions.assertTextPresent(locator.css("#edit-crowd-application-remoteAddresses-error"), "'not_an_address' is not a valid IP address.");

        // rename the application, then check that the app has been renamed (even if no password provided)
        submitEditForm(newAppName, "", newAppAddress);
        assertAppPresent(newAppName, newAppAddress);
        assertAppNotPresent(MY_APP, MY_APP_ADDRESS);
    }

    /*
     * Tests that we can add an app, then edit it, and then delete it.
     */
    @Test
    public void testDeleteApplication() throws Exception {
        tester.clickLink("crowd-delete-application-" + MY_APP);
        assertAppNotPresent(MY_APP, MY_APP_ADDRESS);
    }

    private void submitEditForm(final String name, final String credential, final String remoteAddresses) {
        tester.setWorkingForm("edit-crowd-application");
        tester.setFormElement("name", name);
        tester.setFormElement("credential", credential);
        tester.setFormElement("remoteAddresses", remoteAddresses);
        tester.clickButton("edit-crowd-application-submit");
    }

    private void assertAppPresent(final String appName, final String appAddress) {
        final String appRowLocator = String.format("#crowd-app-%s", appName);
        textAssertions.assertTextPresent(locator.css(appRowLocator + " td[headers=application]"), appName);
        textAssertions.assertTextPresent(locator.css(appRowLocator + " td[headers=address]"), appAddress);
    }

    private void assertAppNotPresent(final String appName, final String appAddress) {
        final String appRowLocator = String.format("#crowd-app-%s", appName);
        textAssertions.assertTextNotPresent(locator.css(appRowLocator + " td[headers=application]"), appName);
        textAssertions.assertTextNotPresent(locator.css(appRowLocator + " td[headers=address]"), appAddress);
    }
}
