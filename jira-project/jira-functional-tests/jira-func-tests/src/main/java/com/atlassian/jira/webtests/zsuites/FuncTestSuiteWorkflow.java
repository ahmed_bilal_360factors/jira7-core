package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.admin.security.xsrf.TestXsrfWorkflow;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkWorkflowTransition;
import com.atlassian.jira.webtests.ztests.email.TestBulkWorkflowTransitionNotification;
import com.atlassian.jira.webtests.ztests.issue.TestCloneIssueWithValidation;
import com.atlassian.jira.webtests.ztests.workflow.TestAddWorkflowTransition;
import com.atlassian.jira.webtests.ztests.workflow.TestCustomWorkflow;
import com.atlassian.jira.webtests.ztests.workflow.TestCustomWorkflowScreenLocalization;
import com.atlassian.jira.webtests.ztests.workflow.TestDraftWorkflow;
import com.atlassian.jira.webtests.ztests.workflow.TestDraftWorkflowSchemeMigration;
import com.atlassian.jira.webtests.ztests.workflow.TestDraftWorkflowSchemeMultipleProjectsMigration;
import com.atlassian.jira.webtests.ztests.workflow.TestEditWorkflowDispatcher;
import com.atlassian.jira.webtests.ztests.workflow.TestTransitionWorkflowScreen;
import com.atlassian.jira.webtests.ztests.workflow.TestWorkFlowActions;
import com.atlassian.jira.webtests.ztests.workflow.TestWorkFlowSchemes;
import com.atlassian.jira.webtests.ztests.workflow.TestWorkflowBasedPermissions;
import com.atlassian.jira.webtests.ztests.workflow.TestWorkflowConditions;
import com.atlassian.jira.webtests.ztests.workflow.TestWorkflowDesigner;
import com.atlassian.jira.webtests.ztests.workflow.TestWorkflowEditing;
import com.atlassian.jira.webtests.ztests.workflow.TestWorkflowEditor;
import com.atlassian.jira.webtests.ztests.workflow.TestWorkflowGlobalPostFunctions;
import com.atlassian.jira.webtests.ztests.workflow.TestWorkflowMigration;
import com.atlassian.jira.webtests.ztests.workflow.TestWorkflowNameEditing;
import com.atlassian.jira.webtests.ztests.workflow.TestWorkflowTransitionPermission;
import com.atlassian.jira.webtests.ztests.workflow.TestWorkflowTransitionReindexing;
import com.atlassian.jira.webtests.ztests.workflow.TestWorkflowTransitionView;
import com.atlassian.jira.webtests.ztests.workflow.TestWorkflowWithOriginalStepTransitions;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A FuncTestSuite of Workflow related tests
 *
 * @since v4.0
 */
public class FuncTestSuiteWorkflow {

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestWorkFlowActions.class)
                .add(TestTransitionWorkflowScreen.class)

                .add(TestWorkFlowSchemes.class)
                .add(TestCustomWorkflow.class)
                .add(TestAddWorkflowTransition.class)
                .add(TestWorkflowMigration.class)
                .add(TestDraftWorkflowSchemeMigration.class)
                .add(TestDraftWorkflowSchemeMultipleProjectsMigration.class)
                .add(TestWorkflowNameEditing.class)

                .add(TestBulkWorkflowTransition.class)
                .add(TestWorkflowBasedPermissions.class)

                .add(TestWorkflowTransitionView.class)
                .add(TestWorkflowEditing.class)
                .add(TestWorkflowConditions.class)
                .add(TestWorkflowTransitionPermission.class)

                .add(TestWorkflowEditor.class)
                .add(TestWorkflowGlobalPostFunctions.class)
                .add(TestWorkflowWithOriginalStepTransitions.class)
                .add(TestBulkWorkflowTransitionNotification.class)
                .add(TestDraftWorkflow.class)
                .add(TestCustomWorkflowScreenLocalization.class)

                .add(TestWorkflowDesigner.class)
                .add(TestEditWorkflowDispatcher.class)
                .add(TestXsrfWorkflow.class)
                .add(TestWorkflowTransitionReindexing.class)

                .add(TestCloneIssueWithValidation.class)
                .build();
    }
}
