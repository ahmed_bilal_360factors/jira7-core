package com.atlassian.jira.webtests.ztests.dashboard.reports;

import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;
import org.w3c.dom.Element;
import org.w3c.dom.html.HTMLTextAreaElement;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.REPORTS})
@LoginAs(user = ADMIN_USERNAME)
public class TestConfigureReportTextField extends AbstractConfigureReportFieldTestCase {

    private Element fieldContainer() {
        return (Element)new XPathLocator(tester, "//fieldset[@class = 'group'][textarea[@id = 'aText']]").getNode();
    }

    @Test
    public void fieldExists() {
        tester.assertFormElementPresent("aText");
    }

    @Test
    public void defaultValueIsFilledIn() {
        HTMLTextAreaElement element = (HTMLTextAreaElement)tester.getDialog().getElement("aText");
        assertThat(element.getTextContent(), is("Cockatoo"));
    }

    @Test
    public void fieldLabelRendered() {
        textAssertions.assertTextPresent(new XPathLocator(fieldContainer(), "legend"), "A text");
    }

    @Test
    public void fieldDescriptionRendered() {
        textAssertions.assertTextPresent(new XPathLocator(fieldContainer(), "descendant::label[@for='aText']"), "This is a text field");
    }

    @Test
    public void validationErrorsRendered() {
        tester.gotoPage("/secure/ConfigureReport.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.dev.func-test-plugin:fieldtest-report&aString=fail");
        textAssertions.assertTextPresent(new XPathLocator(fieldContainer(), "*[@class='errMsg']"), "This text field has an error");
    }

    @Test
    public void valueIsSubmittedToReportBean() {
        tester.setFormElement("aText", "MyTextValue");
        tester.submit();
        textAssertions.assertTextPresent(new XPathLocator(tester, "//tr[th = 'aText']/td"), "MyTextValue");
    }
}
