package com.atlassian.jira.webtests.ztests.bundledplugins2.rest.sal;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.backdoor.SalLicenseControl;
import com.atlassian.jira.functest.framework.backdoor.SingleProductLicenseDetailsViewTO;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.test.util.lic.LicenseReader;
import com.atlassian.jira.webtests.LicenseKeys;
import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.SalLicenseControl.MultiProductLicenseDetailsTO;
import static com.atlassian.jira.functest.framework.backdoor.SalLicenseControl.ValidationResultTO;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.CORE_KEY;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.SERVICE_DESK_KEY;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.SOFTWARE_KEY;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @since v7.0
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class SalLicenseTest extends BaseJiraRestTest {
    /**
     * Software unlimited users.
     */
    private static final License SOFTWARE_SUPER = readLicense("software-super.lic");

    /**
     * - JIRA Core (2 users)
     * - JIRA Software (3 users)
     * - JIRA Servicedesk (1 user)
     */
    private static final License MULTIPLE_PRODUCTS = readLicense("multiple-products.lic");

    /**
     * JIRA Servicedesk (5 users)
     */
    private static final License SERVICE_DESK = readLicense("service-desk.lic");

    /**
     * JIRA Core (3 users)
     * JIRA Software (4 users)
     */
    private static final License CORE_AND_SOFTWARE = readLicense("core-and-software.lic");

    /**
     * Yuk...connie license.
     */
    private static final String CONFLUENCE_2000_EVALUATION = readLicenseString("confluence-2000-evaluation.lic");

    /**
     * ELA JIRA Software License (2 users)
     */
    private static final License SOFTWARE_ELA_SUBSCRIPTION = readLicense("software-ela-subscription.lic");

    /**
     * - JIRA Core (2 users)
     * - JIRA Software (4 users)
     * - JIRA Servicedesk (0 user) (* invalid *)
     */
    private static final License CORE_SOFT_INVALID_SD = readLicense("core-soft-invalid-sd.lic");

    /**
     * - JIRA Core (4 users) expired on 01-01-2001.
     */
    private static final License CORE_EXPIRED = readLicense("core-expired.lic");

    /**
     * - JIRA Core (2 users) for starter.
     */
    private static final License STARTER_CORE_LICENSE = readLicense("starter-core-license.lic");

    /**
     * - JIRA Core (2 users) for academic.
     */
    private static final License ACADEMIC_CORE_LICENSE = readLicense("academic-core-license.lic");

    /**
     * - JIRA Core (2 users) for community.
     */
    private static final License COMMUNITY_CORE_LICENSE = readLicense("community-core-license.lic");

    /**
     * Software license with small user count
     */
    private static final License SOFTWARE_SMALL_USER_COUNT = readLicense("software-small-user-limit.lic");

    private static License readLicense(String name) {
        //This is because the "resources" are packaged into the "resources" directory of the func-tests JAR for
        //some strange reason.
        final String prefix = "/resources/" + SalLicenseTest.class.getPackage().getName().replace('.', '/');
        return LicenseReader.readLicense(prefix + "/" + name, SalLicenseTest.class);
    }

    private static String readLicenseString(String name) {
        //This is because the "resources" are packaged into the "resources" directory of the func-tests JAR for
        //some strange reason.
        final String prefix = "/resources/" + SalLicenseTest.class.getPackage().getName().replace('.', '/');
        return LicenseReader.readLicenseEncodedString(prefix + "/" + name, SalLicenseTest.class);
    }

    private SalLicenseControl salLicenseControl;


    @Before
    public void setUpTest() {
        salLicenseControl = new SalLicenseControl(environmentData);
    }

    public void prepareCleanInstance() {
        backdoor.license().replace(LicenseKeys.COMMERCIAL);
    }

    @Test
    public void testPing() {
        final String response = salLicenseControl.ping();
        assertEquals("pong", response);
    }

    @Test
    public void testThatJiraAllowsCustomProducts() {
        assertTrue(salLicenseControl.hostAllowsCustomProducts());
    }

    @Test
    public void testThatJiraAllowsMultipleLicenses() {
        assertTrue(salLicenseControl.hostAllowsMultipleLicenses());
    }

    @Test
    public void testSEN() {
        prepareCleanInstance();
        assertTrue(salLicenseControl.addProductLicense(SERVICE_DESK_KEY, SERVICE_DESK));
        assertThat("TestSEN", is(salLicenseControl.getProductLicenseDetails(SERVICE_DESK_KEY).supportEntitlementNumber));
    }

    @Test
    public void testThatExistingLicenseIsRetrievableAsSoftwareLicense() {
        prepareCleanInstance();
        final SingleProductLicenseDetailsViewTO softwareLicense = salLicenseControl.getProductLicenseDetails(SOFTWARE_KEY);
        assertNotNull(softwareLicense);
        assertTrue(softwareLicense.numberOfUsers != 0);
    }

    @Test
    public void testRetrieveRawLicenseString() {
        prepareCleanInstance();
        assertTrue(salLicenseControl.addProductLicense(SERVICE_DESK_KEY, SERVICE_DESK));
        assertEquals(StringUtils.remove(SERVICE_DESK.getLicenseString(), "\n"), salLicenseControl.getRawProductLicense(SERVICE_DESK_KEY));
    }

    @Test
    public void testUnableToRemoveLastLicense() {
        backdoor.license().replace(SOFTWARE_SMALL_USER_COUNT);
        int initialLicenseCount = salLicenseControl.getProductKeys().size();
        assertEquals(1, initialLicenseCount);
        final Boolean deleted = salLicenseControl.deleteProductLicense(SOFTWARE_KEY);
        assertFalse(deleted);
        int licenseCount = salLicenseControl.getProductKeys().size();
        assertEquals(1, licenseCount);
    }

    @Test
    public void testRemoveProductLicense() {
        prepareCleanInstance();
        assertTrue(salLicenseControl.addProductLicense(SERVICE_DESK_KEY, SERVICE_DESK));
        assertTrue(salLicenseControl.addProductLicense(SOFTWARE_KEY, CORE_AND_SOFTWARE));
        int licenseCount = salLicenseControl.getProductKeys().size();
        assertEquals(3, licenseCount);
        final Boolean deleted = salLicenseControl.deleteProductLicense(SERVICE_DESK_KEY);
        assertTrue(deleted);
        licenseCount = salLicenseControl.getProductKeys().size();
        assertEquals(2, licenseCount);
    }

    @Test
    public void testRetrieveRawLicenseStringMultiProducts() {
        prepareCleanInstance();
        assertTrue(salLicenseControl.addProductLicense(SERVICE_DESK_KEY, SERVICE_DESK));
        assertTrue(salLicenseControl.addProductLicense(SOFTWARE_KEY, CORE_AND_SOFTWARE));
        assertEquals(StringUtils.remove(SERVICE_DESK.getLicenseString(), "\n"), salLicenseControl.getRawProductLicense(SERVICE_DESK_KEY));
        assertEquals(StringUtils.remove(CORE_AND_SOFTWARE.getLicenseString(), "\n"), salLicenseControl.getRawProductLicense(SOFTWARE_KEY));
        assertEquals(StringUtils.remove(CORE_AND_SOFTWARE.getLicenseString(), "\n"), salLicenseControl.getRawProductLicense(CORE_KEY));
    }

    @Test
    public void testDecodeLicenseString() {
        prepareCleanInstance();
        final MultiProductLicenseDetailsTO multiProductLicenseDetailsTO = salLicenseControl.decodeLicenseDetails(SERVICE_DESK);
        assertNotNull(multiProductLicenseDetailsTO);
        assertEquals(1, multiProductLicenseDetailsTO.productLicenses.size());
        assertEquals(SERVICE_DESK_KEY, multiProductLicenseDetailsTO.productLicenses.get(0).productKey);
        assertEquals("JIRA Enterprise: Commercial Server", multiProductLicenseDetailsTO.description);
    }

    @Test
    public void testDecodeLicenseStringWithMultipleProducts() {
        prepareCleanInstance();
        final MultiProductLicenseDetailsTO multiProductLicenseDetailsTO = salLicenseControl.decodeLicenseDetails(MULTIPLE_PRODUCTS);
        assertNotNull(multiProductLicenseDetailsTO);
        assertEquals(3, multiProductLicenseDetailsTO.productLicenses.size());
        assertNotNull(multiProductLicenseDetailsTO.getProductByKey(CORE_KEY));
        assertNotNull(multiProductLicenseDetailsTO.getProductByKey(SERVICE_DESK_KEY));
        assertNotNull(multiProductLicenseDetailsTO.getProductByKey(SOFTWARE_KEY));
        assertEquals("JIRA Enterprise: Commercial Server", multiProductLicenseDetailsTO.description);
    }

    @Test
    public void testSingleLicenseWithMultipleProducts() {
        prepareCleanInstance();
        assertTrue(salLicenseControl.addProductLicense(SERVICE_DESK_KEY, MULTIPLE_PRODUCTS));
        final List<String> productKeys = salLicenseControl.getProductKeys();
        assertThat(Arrays.asList(SERVICE_DESK_KEY, CORE_KEY, SOFTWARE_KEY), containsInAnyOrder(productKeys.toArray(new String[productKeys.size()])));
        assertEquals(1, salLicenseControl.getProductLicenseDetails(SERVICE_DESK_KEY).numberOfUsers);
        assertEquals(2, salLicenseControl.getProductLicenseDetails(CORE_KEY).numberOfUsers);
        assertEquals(3, salLicenseControl.getProductLicenseDetails(SOFTWARE_KEY).numberOfUsers);
        final List<MultiProductLicenseDetailsTO> allProductLicenses = salLicenseControl.getAllProductLicenses();
        assertEquals(1, allProductLicenses.size());
        assertEquals("JIRA Enterprise: Commercial Server", allProductLicenses.get(0).description);
        assertEquals(3, allProductLicenses.get(0).productLicenses.size());
    }

    @Test
    public void testMultipleLicenseWithMultipleProducts() {
        prepareCleanInstance();
        assertTrue(salLicenseControl.addProductLicense(SERVICE_DESK_KEY, SERVICE_DESK));
        assertTrue(salLicenseControl.addProductLicense(SOFTWARE_KEY, CORE_AND_SOFTWARE));
        final List<String> productKeys = salLicenseControl.getProductKeys();
        assertThat(Arrays.asList(SERVICE_DESK_KEY, CORE_KEY, SOFTWARE_KEY),
                containsInAnyOrder(productKeys.toArray(new String[productKeys.size()])));
        assertEquals(5, salLicenseControl.getProductLicenseDetails(SERVICE_DESK_KEY).numberOfUsers);
        assertEquals(3, salLicenseControl.getProductLicenseDetails(CORE_KEY).numberOfUsers);
        assertEquals(4, salLicenseControl.getProductLicenseDetails(SOFTWARE_KEY).numberOfUsers);
        final List<MultiProductLicenseDetailsTO> allProductLicenses = salLicenseControl.getAllProductLicenses();
        assertEquals(2, allProductLicenses.size());
        assertEquals("JIRA Enterprise: Commercial Server", allProductLicenses.get(0).description);
    }

    @Test
    public void testBaseLicenseProperties() {
        prepareCleanInstance();
        assertTrue(salLicenseControl.addProductLicense(SOFTWARE_KEY, MULTIPLE_PRODUCTS));
        final MultiProductLicenseDetailsTO detailsTO = salLicenseControl.getAllProductLicenses().get(0);

        assertEquals("JIRA Enterprise: Commercial Server", detailsTO.description);
        assertEquals("Atlassian", detailsTO.organisationName);
        assertEquals("TestSEN", detailsTO.supportEntitlementNumber);
        assertEquals("COMMERCIAL", detailsTO.licenseTypeName);
        assertFalse(detailsTO.isDataCenter);
        assertFalse(detailsTO.isPerpetualLicense);
        assertEquals(3, detailsTO.productLicenses.size());
        assertFalse(detailsTO.isEnterpriseLicensingAgreement);
    }

    @Test
    public void testSuperLicense() {
        prepareCleanInstance();
        assertTrue(salLicenseControl.addProductLicense(SOFTWARE_KEY, SOFTWARE_SUPER));
        final List<MultiProductLicenseDetailsTO> allProductLicenses = salLicenseControl.getAllProductLicenses();
        for (MultiProductLicenseDetailsTO productLicenseDetailsTO : allProductLicenses) {
            if (!productLicenseDetailsTO.productLicenses.isEmpty()) {
                assertTrue(productLicenseDetailsTO.getProductByKey(SOFTWARE_KEY).isUnlimitedNumberOfUsers);
                assertFalse(productLicenseDetailsTO.isPerpetualLicense);
                assertFalse(productLicenseDetailsTO.isEnterpriseLicensingAgreement);
                return;
            }
        }
        fail();
    }

    @Test
    public void testExpiredELALicense() {
        prepareCleanInstance();
        backdoor.license().set(SOFTWARE_ELA_SUBSCRIPTION);
        final List<MultiProductLicenseDetailsTO> allProductLicenses = salLicenseControl.getAllProductLicenses();
        for (MultiProductLicenseDetailsTO productLicenseDetailsTO : allProductLicenses) {
            if (!productLicenseDetailsTO.productLicenses.isEmpty()) {
                assertFalse(productLicenseDetailsTO.getProductByKey(SOFTWARE_KEY).isUnlimitedNumberOfUsers);
                assertTrue(productLicenseDetailsTO.isEnterpriseLicensingAgreement);
                return;
            }
        }
        fail();
    }

    @Test
    public void testThatPreRenaissanceAreInterpretedAsSoftwareLicense() {
        prepareCleanInstance();
        final ValidationResultTO validationResultTO
                = salLicenseControl.validateLicenseString(SOFTWARE_KEY, LicenseKeys.COMMERCIAL, Locale.ENGLISH);
        assertValidationResultValid(validationResultTO);
    }

    @Test
    public void testThatPreRenaissanceNotInterpretedAsNonSoftwareLicense() {
        prepareCleanInstance();
        final ValidationResultTO validationResultTO
                = salLicenseControl.validateLicenseString(SERVICE_DESK_KEY, LicenseKeys.COMMERCIAL, Locale.ENGLISH);
        assertValidationResultContainsSingleError(validationResultTO);
    }

    @Test
    public void testPreRenaissanceReturnedAsSoftwareWhenMultiLicense() {
        prepareCleanInstance();
        assertTrue(salLicenseControl.addProductLicense(SERVICE_DESK_KEY, SERVICE_DESK));
        int licenseCount = salLicenseControl.getProductKeys().size();
        assertEquals(2, licenseCount);
        final List<MultiProductLicenseDetailsTO> allProductLicenses = salLicenseControl.getAllProductLicenses();
        assertEquals(2, allProductLicenses.size());

        final SingleProductLicenseDetailsViewTO softwareLicense = salLicenseControl.getProductLicenseDetails(SOFTWARE_KEY);
        assertNotNull(softwareLicense);
        assertTrue(softwareLicense.numberOfUsers != 0);

        final SingleProductLicenseDetailsViewTO serviceDeskLicense = salLicenseControl.getProductLicenseDetails(SERVICE_DESK_KEY);
        assertNotNull(serviceDeskLicense);
        assertTrue(serviceDeskLicense.numberOfUsers != 0);
    }

    @Test
    public void testThatSoftwareReplacesOnlyPreRenaissance() {
        prepareCleanInstance();
        final SingleProductLicenseDetailsViewTO softwareLicense = salLicenseControl.getProductLicenseDetails(SOFTWARE_KEY);
        assertNotNull(softwareLicense);
        int expected = -1;
        assertEquals(expected, softwareLicense.numberOfUsers);

        assertTrue(salLicenseControl.addProductLicense(SERVICE_DESK_KEY, SERVICE_DESK));

        assertTrue(salLicenseControl.addProductLicense(SOFTWARE_KEY, SOFTWARE_SMALL_USER_COUNT));

        final SingleProductLicenseDetailsViewTO softwareLicenseAfterReplace = salLicenseControl.getProductLicenseDetails(SOFTWARE_KEY);
        assertNotNull(softwareLicenseAfterReplace);
        assertEquals(6, softwareLicenseAfterReplace.numberOfUsers);

        final SingleProductLicenseDetailsViewTO serviceDeskLicenseAfterReplace = salLicenseControl.getProductLicenseDetails(SERVICE_DESK_KEY);
        assertNotNull(serviceDeskLicenseAfterReplace);
        assertEquals(5, serviceDeskLicenseAfterReplace.numberOfUsers);
    }

    @Test
    public void testThatPreRenaissanceLicenseReplacesSoftwareLicense() {
        prepareCleanInstance();
        assertTrue(salLicenseControl.addProductLicense(SOFTWARE_KEY, SOFTWARE_SMALL_USER_COUNT));

        final SingleProductLicenseDetailsViewTO softwareLicenseAfterReplace = salLicenseControl.getProductLicenseDetails(SOFTWARE_KEY);
        assertNotNull(softwareLicenseAfterReplace);
        assertEquals(6, softwareLicenseAfterReplace.numberOfUsers);

        assertTrue(salLicenseControl.addProductLicense(SOFTWARE_KEY, LicenseKeys.COMMERCIAL));

        final SingleProductLicenseDetailsViewTO softwareLicenseAfterReplaceWithCommercial = salLicenseControl.getProductLicenseDetails(SOFTWARE_KEY);
        assertNotNull(softwareLicenseAfterReplaceWithCommercial);
        int expected = -1;
        assertEquals(expected, softwareLicenseAfterReplaceWithCommercial.numberOfUsers);
    }

    @Test
    public void testThatPreRenaissanceLicenseReplacesMultiSoftwareLicense() {
        prepareCleanInstance();
        assertTrue(salLicenseControl.addProductLicense(SOFTWARE_KEY, MULTIPLE_PRODUCTS));

        final SingleProductLicenseDetailsViewTO softwareLicenseAfterReplace = salLicenseControl.getProductLicenseDetails(SOFTWARE_KEY);
        assertNotNull(softwareLicenseAfterReplace);
        assertEquals(3, softwareLicenseAfterReplace.numberOfUsers);

        assertTrue(salLicenseControl.addProductLicense(SOFTWARE_KEY, LicenseKeys.COMMERCIAL));

        final SingleProductLicenseDetailsViewTO softwareLicenseAfterReplaceWithCommercial = salLicenseControl.getProductLicenseDetails(SOFTWARE_KEY);
        assertNotNull(softwareLicenseAfterReplaceWithCommercial);
        int expected = -1;
        assertEquals(expected, softwareLicenseAfterReplaceWithCommercial.numberOfUsers);
    }

    @Test
    public void testValidLicense() {
        prepareCleanInstance();
        final ValidationResultTO validationResultTO
                = salLicenseControl.validateLicenseString(SOFTWARE_KEY, SOFTWARE_SUPER, Locale.ENGLISH);
        assertValidationResultValid(validationResultTO);
    }

    @Test
    public void testLicenseNotForSpecifiedProduct() {
        prepareCleanInstance();
        final ValidationResultTO validationResultTO
                = salLicenseControl.validateLicenseString(SOFTWARE_KEY, SERVICE_DESK, Locale.ENGLISH);
        assertValidationResultContainsSingleError(validationResultTO);
    }

    @Test
    public void testLicenseOtherThanJIRA() {
        prepareCleanInstance();
        final ValidationResultTO validationResultTO
                = salLicenseControl.validateLicenseString(SOFTWARE_KEY, CONFLUENCE_2000_EVALUATION, Locale.ENGLISH);
        assertValidationResultContainsSingleError(validationResultTO);
    }

    @Test
    public void testRandomCharsAsLicense() {
        prepareCleanInstance();
        final ValidationResultTO validationResultTO
                = salLicenseControl.validateLicenseString(SOFTWARE_KEY, "234yu2345onhoi231u4", Locale.ENGLISH);
        assertValidationResultContainsSingleError(validationResultTO);
    }

    @Test
    public void testExpiredLicense() {
        prepareCleanInstance();
        final ValidationResultTO validationResultTO =
                salLicenseControl.validateLicenseString(CORE_KEY, CORE_EXPIRED, Locale.ENGLISH);
        assertValidationResultContainsSingleError(validationResultTO);
    }

    @Test
    public void testUnsupportedV1License() {
        prepareCleanInstance();
        final ValidationResultTO validationResultTO =
                salLicenseControl.validateLicenseString(CORE_KEY, LicenseKeys.V1_ENTERPRISE, Locale.ENGLISH);
        assertValidationResultContainsSingleError(validationResultTO);
    }

    @Test
    public void testSwitchingToStarterShouldBeOk() {
        prepareCleanInstance();
        salLicenseControl.validateLicenseString(SOFTWARE_KEY, SOFTWARE_SUPER, Locale.ENGLISH);
        final ValidationResultTO validationResultTO =
                salLicenseControl.validateLicenseString(CORE_KEY, STARTER_CORE_LICENSE, Locale.ENGLISH);
        assertValidationResultValid(validationResultTO);
    }

    @Test
    public void testSwitchingBetweenTypesIsPrevented() {
        prepareCleanInstance();
        salLicenseControl.validateLicenseString(SOFTWARE_KEY, SOFTWARE_SUPER, Locale.ENGLISH);
        final ValidationResultTO validationResultTO =
                salLicenseControl.validateLicenseString(CORE_KEY, COMMUNITY_CORE_LICENSE, Locale.ENGLISH);
        assertValidationResultContainsSingleError(validationResultTO);

        String actual = validationResultTO.getErrorMessages().iterator().next();
        assertThat(getMessageForValidationResult(validationResultTO), actual,
                startsWith("This license does not match the type of JIRA currently licensed. This is a license for COMMUNITY JIRA,"
                        + " which does not match your existing license for COMMERCIAL JIRA. "
                        + "Please remove all other licenses for COMMERCIAL JIRA if you'd like to change the type of your JIRA instance."));

        Matcher matcher = Pattern.compile("href=\"([^\"]+)\"").matcher(actual);
        if (matcher.find()) {
            String helpLink = matcher.group(1);
            assertThat("Help link is not pointing at admin space", helpLink,
                    startsWith("https://docs.atlassian.com/jira/jadm-docs-"));
            assertThat("Help link is pointing to the wrong article", helpLink,
                    endsWith("License+compatibility"));
        } else {
            fail("Message did not provide a help link.");
        }
    }

    @Test
    public void testSingleLicenseKeyWithMultipleProductsServiceDeskInvalidUserLimit() {
        prepareCleanInstance();
        ValidationResultTO validationResultTO =
                salLicenseControl.validateLicenseString(SOFTWARE_KEY, CORE_SOFT_INVALID_SD, Locale.ENGLISH);
        assertValidationResultValid(validationResultTO);
        validationResultTO = salLicenseControl.validateLicenseString(CORE_KEY, CORE_SOFT_INVALID_SD, Locale.ENGLISH);
        assertValidationResultValid(validationResultTO);
        validationResultTO = salLicenseControl.validateLicenseString(SERVICE_DESK_KEY, CORE_SOFT_INVALID_SD, Locale.ENGLISH);
        assertValidationResultContainsSingleError(validationResultTO);
    }

    private void assertValidationResultValid(final ValidationResultTO validationResult) {
        assertTrue(getMessageForValidationResult(validationResult),validationResult.isValid());
        assertFalse(getMessageForValidationResult(validationResult),validationResult.hasErrors());
        assertFalse(getMessageForValidationResult(validationResult),validationResult.hasWarnings());
        assertEquals(getMessageForValidationResult(validationResult),0, Iterables.size(validationResult.getErrorMessages()));
        assertEquals(getMessageForValidationResult(validationResult),0, Iterables.size(validationResult.getWarningMessages()));
    }

    private void assertValidationResultContainsSingleError(final ValidationResultTO validationResult) {
        assertFalse(getMessageForValidationResult(validationResult),validationResult.isValid());
        assertTrue(getMessageForValidationResult(validationResult),validationResult.hasErrors());
        assertFalse(getMessageForValidationResult(validationResult),validationResult.hasWarnings());
        assertEquals(getMessageForValidationResult(validationResult),1, Iterables.size(validationResult.getErrorMessages()));
        assertEquals(getMessageForValidationResult(validationResult),0, Iterables.size(validationResult.getWarningMessages()));
    }

    private String getMessageForValidationResult(ValidationResultTO validationResult) {
        final StringBuilder stb = new StringBuilder();
        stb.append(validationResult.toString());
        stb.append("  Licensed products: ");
        stb.append(salLicenseControl.getProductKeys());
        return stb.toString();
    }

}