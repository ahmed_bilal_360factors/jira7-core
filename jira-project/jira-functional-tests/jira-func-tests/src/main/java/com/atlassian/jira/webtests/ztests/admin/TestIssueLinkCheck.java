package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.ISSUES})
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueLinkCheck extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("TestIssueLinkCheck.xml");
    }

    @Test
    public void testIssueLinkCheck() {
        navigation.gotoAdminSection(Navigation.AdminSection.INTEGRITY_CHECKER);

        tester.checkCheckbox("integrity_check_1_3", "3");
        tester.submit("check");
        textAssertions.assertTextPresent(locator.page(), "Choose the errors you would like to fix, or return to the previous screen");
        textAssertions.assertTextPresent(locator.page(), "The following Issue Link will be removed due to a related invalid issue: IssueLink (ID:10002)");
        textAssertions.assertTextPresent(locator.page(), "The following Issue Link will be removed due to a related invalid issue: IssueLink (ID:10003)");
        textAssertions.assertTextPresent(locator.page(), "The following Issue Link will be removed due to a related invalid issue: IssueLink (ID:10004)");

        tester.checkCheckbox("integrity_check_1_3", "3");
        tester.submit("fix");
        textAssertions.assertTextPresent(locator.page(), "3 error(s) were corrected");
        textAssertions.assertTextPresent(locator.page(), "The following Issue Link has been removed due to a related invalid issue: IssueLink (ID:10002)");
        textAssertions.assertTextPresent(locator.page(), "The following Issue Link has been removed due to a related invalid issue: IssueLink (ID:10003)");
        textAssertions.assertTextPresent(locator.page(), "The following Issue Link has been removed due to a related invalid issue: IssueLink (ID:10004)");

        navigation.gotoAdminSection(Navigation.AdminSection.INTEGRITY_CHECKER);

        tester.checkCheckbox("integrity_check_1_3", "3");
        tester.submit("check");
        textAssertions.assertTextPresent(locator.page(), "No errors were found");
        textAssertions.assertTextSequence(locator.page(), "PASSED", "Check that all Issue Links are associated with valid issues");
    }
}
