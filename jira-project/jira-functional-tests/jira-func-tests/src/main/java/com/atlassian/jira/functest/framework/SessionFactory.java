package com.atlassian.jira.functest.framework;

/**
 *
 * @since v7.2
 */
public interface SessionFactory {
    Session begin();
}
