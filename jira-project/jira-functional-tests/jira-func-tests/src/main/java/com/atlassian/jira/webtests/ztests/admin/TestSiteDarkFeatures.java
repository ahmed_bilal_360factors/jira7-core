package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestSiteDarkFeatures extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testAdminUI() {
        administration.restoreBlankInstance();

        navigation.gotoPage("/secure/admin/SiteDarkFeatures!default.jspa");
        textAssertions.assertTextPresent(new IdLocator(tester, "site-disabled-features"), "jira.site.darkfeature.admin");

        backdoor.darkFeatures().enableForSite("jira.site.darkfeature.admin");
        navigation.gotoPage("/secure/admin/SiteDarkFeatures!default.jspa");
        textAssertions.assertTextNotPresent(new IdLocator(tester, "site-disabled-features"), "jira.site.darkfeature.admin");
        textAssertions.assertTextPresent(new IdLocator(tester, "site-enabled-features"), "jira.site.darkfeature.admin");

        //check admin permissions are required.
        navigation.logout();
        navigation.login("fred");
        navigation.gotoPage("/secure/admin/SiteDarkFeatures!default.jspa");
        textAssertions.assertTextPresent("Welcome to jWebTest JIRA installation");
        textAssertions.assertTextNotPresent("System Property Dark Features");
    }

    @Test
    public void testAddSiteFeature() {
        // The clear cache call in quick restore means this test would always succeed, so use slowOldWay
        administration.restoreDataSlowOldWay("blankprojects.xml");

        navigation.gotoPage("/secure/admin/SiteDarkFeatures!default.jspa");


        backdoor.darkFeatures().enableForSite("jira.site.feature.new");
        navigation.gotoPage("/secure/admin/SiteDarkFeatures!default.jspa");

        textAssertions.assertTextPresent(new IdLocator(tester, "site-enabled-features"), "jira.site.feature.new");
    }
}
