package com.atlassian.jira.webtests.ztests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.beans.WorkflowSchemeData;
import com.atlassian.jira.testkit.client.IssueTypeControl;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.SimpleIssueType;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.SimpleProject;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.SimpleWorkflow;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.SimpleWorkflowScheme;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonProperty;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.sun.jersey.api.client.ClientResponse.Status.BAD_REQUEST;
import static com.sun.jersey.api.client.ClientResponse.Status.FORBIDDEN;
import static com.sun.jersey.api.client.ClientResponse.Status.NOT_FOUND;
import static com.sun.jersey.api.client.ClientResponse.Status.OK;
import static com.sun.jersey.api.client.ClientResponse.Status.UNAUTHORIZED;
import static java.lang.String.valueOf;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@WebTest({Category.FUNC_TEST, Category.REST})
@RestoreOnce("TestWorkflowSchemeEditorResource.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestWorkflowSchemeEditorResource extends BaseJiraFuncTest {

    private static Map<String, String> issueTypeUrls;

    @Before
    public void setUpTest() {
        initIssueTypeUrls();
    }

    private void initIssueTypeUrls() {
        issueTypeUrls = new HashMap<String, String>();

        for (final IssueTypeControl.IssueType it : backdoor.issueType().getIssueTypes()) {
            issueTypeUrls.put(it.getName(), it.getIconUrl());
        }
    }

    @Test
    public void testGetWorkflowScheme() {
        //Try to get a 404.
        EditorResource resource = new EditorResource(environmentData);
        Response<com.atlassian.jira.webtests.ztests.projectconfig.framework.SimpleWorkflowScheme> response = resource.getScheme(101010101L);
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);

        //Lets try an empty scheme.
        AssignableWorkflowScheme scheme = new AssignableWorkflowScheme().setName("EmptyScheme")
                .setDescription("This is an empty scheme.").save();

        ResponseAssertions assertions = new ResponseAssertions()
                .setIssueTypes(IssueType.ALL)
                .setScheme(scheme)
                .setUser(ADMIN_USERNAME)
                .assertResponse(resource.getScheme(scheme.id));

        //Lets try with a default.
        scheme = scheme.setDefaultWorkflow(Workflow.JIRA).save();
        assertions.assertResponse(resource.getScheme(scheme.id));

        //Lets try with some mappings
        scheme = scheme.setMapping(IssueType.BUG, Workflow.JIRA)
                .setMapping(IssueType.IMPROVEMENT, Workflow.TWO)
                .setMapping(IssueType.NEW_FEATURE, Workflow.TWO).save();
        assertions.assertResponse(resource.getScheme(scheme.id));

        //Lets try without the default.
        scheme = scheme.removeDefaultWorkflow().save();
        assertions.assertResponse(resource.getScheme(scheme.id));

        //Assign the scheme and lets see what happens.
        Project project = new Project().setKey("TGWS").setName("testGetWorkflowScheme")
                .create().setWorkflowScheme(scheme);

        assertions.setProjects(project);
        assertions.assertResponse(resource.getScheme(scheme.id));

        //Create a draft scheme and make sure it is returned.
        DraftWorkflowScheme draft = scheme.createDraft();
        assertions.assertResponse(resource.getScheme(scheme.id));

        //Check an empty scheme.
        draft = draft.clear().removeDefault().save();
        assertions.assertResponse(resource.getScheme(scheme.id));

        //Check something with only a default.
        draft = draft.setDefaultWorkflow(Workflow.TWO).save();
        assertions.assertResponse(resource.getScheme(scheme.id));

        //Check something with only a default.
        draft = draft.setMapping(IssueType.BUG, Workflow.TWO).setMapping(IssueType.IMPROVEMENT, Workflow.ONE).save();
        assertions.assertResponse(resource.getScheme(scheme.id));

        //Remove the draft to make sure that the parent is returned.
        scheme = draft.discard();
        assertions.assertResponse(resource.getScheme(scheme.id));

        //Try multiple projects.
        Project projectShared = new Project().setKey("TGWSS").setName("testGetWorkflowSchemeShared")
                .create().setWorkflowScheme(scheme);

        assertions.setProjects(project, projectShared).assertResponse(resource.getScheme(scheme.id));

        project.delete();
        projectShared.delete();

        assertions.setProjects().assertResponse(resource.getScheme(scheme.id));

        scheme.delete();

        response = resource.getScheme(scheme.getId());
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);
    }

    @Test
    public void testGetWorkflowSchemeNonAdmin() {
        EditorResource resource = new EditorResource(environmentData).loginAs(FRED_USERNAME);
        Response<SimpleWorkflowScheme> scheme = resource.getScheme(101010101L);
        assertEquals(FORBIDDEN.getStatusCode(), scheme.statusCode);
    }

    @Test
    public void testGetWorkflowSchemeAnonymous() {
        EditorResource resource = new EditorResource(environmentData).anonymous();
        Response<SimpleWorkflowScheme> scheme = resource.getScheme(101010101L);
        assertEquals(UNAUTHORIZED.getStatusCode(), scheme.statusCode);
    }

    @Test
    public void testDeleteIssueTypeNonAdmin() {
        EditorResource resource = new EditorResource(environmentData).loginAs(FRED_USERNAME);
        Response<SimpleWorkflowScheme> scheme = resource.deleteIssueType(101010101L, "0", "workflow");
        assertEquals(FORBIDDEN.getStatusCode(), scheme.statusCode);
    }

    @Test
    public void testDeleteIssueTypeAnonymous() {
        EditorResource resource = new EditorResource(environmentData).anonymous();
        Response<SimpleWorkflowScheme> scheme = resource.deleteIssueType(101010101L, "0", "workflow");
        assertEquals(UNAUTHORIZED.getStatusCode(), scheme.statusCode);
    }

    @Test
    public void testDeleteIssueType() {
        //Try to get a 404.
        EditorResource resource = new EditorResource(environmentData);
        Response<SimpleWorkflowScheme> response = resource.deleteIssueType(101010101L, "0", "workflow");
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);

        //Lets try an empty scheme.
        AssignableWorkflowScheme scheme = new AssignableWorkflowScheme().setName("testDeleteIssueType")
                .setDescription("Scheme to test the deletion of an issue type.").save();

        //deleting a non-existent issue type should not throw an error
        ResponseAssertions assertions = new ResponseAssertions()
                .setUser(ADMIN_USERNAME)
                .setIssueTypes(IssueType.ALL)
                .setScheme(scheme)
                .assertResponse(resource.deleteIssueType(scheme.id, IssueType.BUG.id, "workflow"));

        //Test deleting an issue type that exists in the scheme but not in the specified workflow
        scheme.setDefaultWorkflow(Workflow.TWO).setMapping(IssueType.BUG, Workflow.ONE).save();
        assertions.assertResponse(resource.deleteIssueType(scheme.id, IssueType.BUG.id, "a workflow that doesn't exist!"));

        //Test normal operation
        scheme.removeMapping(IssueType.BUG);
        assertions.assertResponse(resource.deleteIssueType(scheme.id, IssueType.BUG.id, Workflow.ONE.name));

        scheme.setMapping(IssueType.BUG, Workflow.ONE).save();
        Project project = new Project().setKey("TDIT").setName("testDeleteIssueType")
                .create().setWorkflowScheme(scheme);


        //Test resource creating draft when active.
        DraftWorkflowScheme draftWorkflowScheme = scheme.localDraft();
        draftWorkflowScheme.removeMapping(IssueType.BUG);

        assertions.setProjects(project);
        response = resource.deleteIssueType(scheme.id, IssueType.BUG.id, Workflow.ONE.name);
        draftWorkflowScheme.setId(response.body.getId());
        draftWorkflowScheme.setUsername(ADMIN_USERNAME);
        assertions.assertResponse(response);

        //Test resource editing draft that already exists.
        draftWorkflowScheme.setMapping(IssueType.BUG, Workflow.ONE).save();
        assertions.assertResponse(resource.deleteIssueType(scheme.id, IssueType.BUG.id, "workflow"));
    }

    @Test
    public void testDeleteWorkflow() {
        //Try to get a 404.
        EditorResource resource = new EditorResource(environmentData);
        Response<SimpleWorkflowScheme> response = resource.deleteIssueType(101010101L, "0", "workflow");
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);

        //Lets try an empty scheme.
        AssignableWorkflowScheme scheme = new AssignableWorkflowScheme().setName("testDeleteWorkflow")
                .setDescription("Scheme to test the deletion of a workflow.").save();

        //deleting a non-existent workflow should not throw an error
        ResponseAssertions assertions = new ResponseAssertions()
                .setUser(ADMIN_USERNAME)
                .setIssueTypes(IssueType.ALL)
                .setScheme(scheme)
                .assertResponse(resource.deleteWorkflow(scheme.id, Workflow.ONE.name));

        scheme.setDefaultWorkflow(Workflow.TWO).setMapping(IssueType.BUG, Workflow.ONE).save().removeMapping(IssueType.BUG);

        assertions.assertResponse(resource.deleteWorkflow(scheme.id, Workflow.ONE.name));

        scheme.setMapping(IssueType.BUG, Workflow.ONE).save();
        Project project = new Project().setKey("TDW").setName("testDeleteWorkflow")
                .create().setWorkflowScheme(scheme);

        //Test resource creating draft when active.
        DraftWorkflowScheme draftWorkflowScheme = scheme.localDraft();
        draftWorkflowScheme.removeMapping(IssueType.BUG);

        assertions.setProjects(project);
        response = resource.deleteIssueType(scheme.id, IssueType.BUG.id, Workflow.ONE.name);
        draftWorkflowScheme.setId(response.body.getId());
        draftWorkflowScheme.setUsername(ADMIN_USERNAME);
        assertions.assertResponse(response);

        //Test resource editing draft that already exists.
        draftWorkflowScheme.setMapping(IssueType.BUG, Workflow.ONE).save().removeMapping(IssueType.BUG);
        assertions.assertResponse(resource.deleteIssueType(scheme.id, IssueType.BUG.id, Workflow.ONE.name));
    }

    @Test
    public void testDeleteWorkflowReassignsDefaultWithoutSpecifyingOne() {
        EditorResource resource = new EditorResource(environmentData);

        AssignableWorkflowScheme scheme = new AssignableWorkflowScheme().setName("testDeleteDefaultWithoutSpecifyingDefault")
                .setDefaultWorkflow(Workflow.TWO).setMapping(IssueType.BUG, Workflow.ONE).setMapping(IssueType.IMPROVEMENT, Workflow.JIRA).save();

        ResponseAssertions assertions =
                new ResponseAssertions().setIssueTypes(IssueType.ALL).setScheme(scheme).setUser(ADMIN_USERNAME);

        scheme.setDefaultWorkflow(Workflow.JIRA);
        assertions.assertResponse(resource.deleteWorkflow(scheme.id, Workflow.TWO.name));
    }

    @Test
    public void testDeleteWorkflowReassignsDefaultBySpecifyingOne() {
        EditorResource resource = new EditorResource(environmentData);

        AssignableWorkflowScheme scheme = new AssignableWorkflowScheme().setName("testDeleteDefaultBySpecifyingDefault")
                .setDefaultWorkflow(Workflow.TWO).setMapping(IssueType.BUG, Workflow.ONE).setMapping(IssueType.IMPROVEMENT, Workflow.JIRA).save();

        ResponseAssertions assertions =
                new ResponseAssertions().setIssueTypes(IssueType.ALL).setScheme(scheme).setUser(ADMIN_USERNAME);

        scheme.setDefaultWorkflow(Workflow.ONE);
        assertions.assertResponse(resource.deleteWorkflow(scheme.id, Workflow.TWO.name, Workflow.ONE.name));

        //test that specifying a workflow that is not already mapped to the scheme is like not specifying one at all
        scheme.removeMapping(IssueType.BUG).setDefaultWorkflow(Workflow.JIRA);
        assertions.assertResponse(resource.deleteWorkflow(scheme.id, Workflow.ONE.name, Workflow.ONE.name));
    }

    @Test
    public void testDeleteWorkflowNonAdmin() {
        EditorResource resource = new EditorResource(environmentData).loginAs(FRED_USERNAME);
        Response<SimpleWorkflowScheme> scheme = resource.deleteWorkflow(101010101L, "0");
        assertEquals(FORBIDDEN.getStatusCode(), scheme.statusCode);
    }

    @Test
    public void testDeleteWorkflowAnonymous() {
        EditorResource resource = new EditorResource(environmentData).anonymous();
        Response<SimpleWorkflowScheme> scheme = resource.deleteWorkflow(101010101L, "0");
        assertEquals(UNAUTHORIZED.getStatusCode(), scheme.statusCode);
    }

    @Test
    public void testAssignIssueTypes() {
        EditorResource resource = new EditorResource(environmentData);
        AssignableWorkflowScheme scheme = new AssignableWorkflowScheme().setName("testAssignIssueTypes").setDescription("Scheme to test the assignment of issue types.").save();
        ResponseAssertions assertions = new ResponseAssertions().setIssueTypes(IssueType.ALL).setScheme(scheme).setUser(ADMIN_USERNAME);

        // assigning to a non-existent scheme should result in a 404
        Response<SimpleWorkflowScheme> response = resource.assignIssueTypes(101010101L, "workflow", asList("0"), false);
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);

        // assigning a non-existent workflow to the scheme
        response = resource.assignIssueTypes(scheme.id, "this workfow doesn't exist", asList(IssueType.BUG.id), false);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);

        // assigning a non-existent issue type to the scheme
        response = resource.assignIssueTypes(scheme.id, Workflow.ONE.name, asList("100"), false);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);

        // assigning an issue type to a new workflow (i.e. adding a workflow to the scheme)
        scheme.setMapping(IssueType.BUG, Workflow.ONE);
        assertions.assertResponse(resource.assignIssueTypes(scheme.id, Workflow.ONE.name, asList(IssueType.BUG.id), false));

        // assigning an issue type to an existing workflow
        scheme.setMapping(IssueType.IMPROVEMENT, Workflow.ONE);
        assertions.assertResponse(resource.assignIssueTypes(scheme.id, Workflow.ONE.name, asList(IssueType.IMPROVEMENT.id), false));

        // reassigning an issue type that is already assigned to another workflow
        scheme.setMapping(IssueType.BUG, Workflow.TWO);
        assertions.assertResponse(resource.assignIssueTypes(scheme.id, Workflow.TWO.name, asList(IssueType.BUG.id), false));

        // reassigning the default workflow
        scheme.setDefaultWorkflow(Workflow.TWO);
        assertions.assertResponse(resource.assignIssueTypes(scheme.id, Workflow.TWO.name, null, true));

        // now do them both at once!
        scheme.setMapping(IssueType.NEW_FEATURE, Workflow.ONE).setDefaultWorkflow(Workflow.ONE);
        assertions.assertResponse(resource.assignIssueTypes(scheme.id, Workflow.ONE.name, asList(IssueType.NEW_FEATURE.id), true));

        // make sure we create a draft when scheme is active and draft does not exist
        Project project = new Project().setKey("TAIT").setName("testAssignIssueTypes")
                .create().setWorkflowScheme(scheme);
        DraftWorkflowScheme draftWorkflowScheme = scheme.localDraft().setMapping(IssueType.TASK, Workflow.TWO);
        assertions.setProjects(project);
        response = resource.assignIssueTypes(scheme.id, Workflow.TWO.name, asList(IssueType.TASK.id), false);
        draftWorkflowScheme.setId(response.body.getId());
        draftWorkflowScheme.setUsername(ADMIN_USERNAME);
        assertions.assertResponse(response);

        // make sure we reuse the draft since it already exists
        // at the same time, assign multiple issue types at once
        draftWorkflowScheme.setMapping(IssueType.BUG, Workflow.ONE).setMapping(IssueType.TASK, Workflow.ONE);
        assertions.assertResponse(resource.assignIssueTypes(scheme.id, Workflow.ONE.name, asList(IssueType.BUG.id, IssueType.TASK.id), false));
    }

    @Test
    public void testAssignIssueTypesNonAdmin() {
        EditorResource resource = new EditorResource(environmentData).loginAs(FRED_USERNAME);
        Response<SimpleWorkflowScheme> scheme = resource.assignIssueTypes(101010101L, "workflow", asList("0"), false);
        assertEquals(FORBIDDEN.getStatusCode(), scheme.statusCode);
    }

    @Test
    public void testAssignIssueTypesAnonymous() {
        EditorResource resource = new EditorResource(environmentData).anonymous();
        Response<SimpleWorkflowScheme> scheme = resource.assignIssueTypes(101010101L, "workflow", asList("0"), false);
        assertEquals(UNAUTHORIZED.getStatusCode(), scheme.statusCode);
    }

    @Test
    public void testDiscardDraft() {
        EditorResource resource = new EditorResource(environmentData);
        AssignableWorkflowScheme scheme = new AssignableWorkflowScheme().setName("testDiscardDraft").setDescription("Scheme to test the assignment of issue types.").save();
        ResponseAssertions assertions = new ResponseAssertions().setIssueTypes(IssueType.ALL).setScheme(scheme).setUser(ADMIN_USERNAME);

        // discard on a non-existent scheme should result in a 404
        Response<SimpleWorkflowScheme> response = resource.discardDraft(101010101L);
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);

        //Discarding a scheme with no draft should just work fine as a noop call.
        assertions.assertResponse(resource.discardDraft(scheme.getId()));

        Project project = new Project().setKey("TDD").setName("testDiscardDraft").create().setWorkflowScheme(scheme);
        final DraftWorkflowScheme draftWorkflowScheme = scheme.createDraft().setMapping(IssueType.TASK, Workflow.TWO).setUsername(ADMIN_USERNAME).save();

        //Lets double check to make sure we have a draft.
        assertions.setProjects(project).assertResponse(resource.getScheme(scheme.getId()));

        //Discarding an actual draft should work as expected.
        draftWorkflowScheme.discardLocal();
        assertions.assertResponse(resource.discardDraft(scheme.getId()));
    }

    @Test
    public void testDiscardDraftNonAdmin() {
        EditorResource resource = new EditorResource(environmentData).loginAs(FRED_USERNAME);
        Response<SimpleWorkflowScheme> scheme = resource.discardDraft(101010101L);
        assertEquals(FORBIDDEN.getStatusCode(), scheme.statusCode);
    }

    @Test
    public void testDiscardDraftAnonymous() {
        EditorResource resource = new EditorResource(environmentData).anonymous();
        Response<SimpleWorkflowScheme> scheme = resource.discardDraft(101010101L);
        assertEquals(UNAUTHORIZED.getStatusCode(), scheme.statusCode);
    }

    @Test
    public void testUpdateName() {
        AssignableWorkflowScheme scheme = new AssignableWorkflowScheme().setName("testUpdateName").save();
        EditorResource resource = new EditorResource(environmentData);

        // permission check
        Response<SimpleWorkflowScheme> response = resource.anonymous().updateName(10000L, "workflow scheme");
        assertEquals(UNAUTHORIZED.getStatusCode(), response.statusCode);

        response = resource.loginAs(FRED_USERNAME).updateName(10000L, "workflow scheme");
        assertEquals(FORBIDDEN.getStatusCode(), response.statusCode);

        // wrong scheme check
        response = resource.loginAs(ADMIN_USERNAME).updateName(12345L, "workflow scheme");
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);

        // test setting null (invalid)
        response = resource.updateName(scheme.id, null);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);

        // test setting empty string (invalid)
        response = resource.updateName(scheme.id, StringUtils.EMPTY);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);

        // test setting valid name
        ResponseAssertions assertions = new ResponseAssertions()
                .setIssueTypes(IssueType.ALL)
                .setScheme(scheme)
                .setUser(ADMIN_USERNAME);
        scheme.setName("new scheme name");
        assertions.assertResponse(resource.updateName(scheme.id, "new scheme name"));
    }

    @Test
    public void testUpdateDescription() {
        AssignableWorkflowScheme scheme = new AssignableWorkflowScheme().setName("testUpdateDescription").setDescription("project for testing the update of descriptions").save();
        EditorResource resource = new EditorResource(environmentData);

        // permission check
        Response<SimpleWorkflowScheme> response = resource.anonymous().updateDescription(10000L, "workflow scheme");
        assertEquals(UNAUTHORIZED.getStatusCode(), response.statusCode);

        response = resource.loginAs(FRED_USERNAME).updateDescription(10000L, "workflow scheme");
        assertEquals(FORBIDDEN.getStatusCode(), response.statusCode);

        // wrong scheme check
        response = resource.loginAs(ADMIN_USERNAME).updateDescription(12345L, "workflow scheme");
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);

        // test setting null (valid)
        ResponseAssertions assertions = new ResponseAssertions()
                .setIssueTypes(IssueType.ALL)
                .setScheme(scheme)
                .setUser(ADMIN_USERNAME);
        scheme.setDescription(null);
        assertions.assertResponse(resource.updateDescription(scheme.id, null));

        // test setting empty string (valid)
        assertions.assertResponse(resource.updateDescription(scheme.id, StringUtils.EMPTY));

        // test setting valid name
        scheme.setDescription("new scheme name");
        assertions.assertResponse(resource.updateDescription(scheme.id, "new scheme name"));
    }

    public static class ResponseAssertions {
        private List<IssueType> issueTypes = Lists.newArrayList();
        private AssignableWorkflowScheme scheme;
        private List<Project> projects = Lists.newArrayList();
        private String user;

        public ResponseAssertions setIssueTypes(List<IssueType> types) {
            this.issueTypes = types;
            return this;
        }

        public ResponseAssertions setScheme(AssignableWorkflowScheme scheme) {
            this.scheme = scheme;
            return this;
        }

        public ResponseAssertions setProjects(Project... projects) {
            this.projects = Lists.newArrayList(asList(projects));
            return this;
        }

        public ResponseAssertions setUser(String user) {
            this.user = user;
            return this;
        }

        public ResponseAssertions assertResponse(Response<SimpleWorkflowScheme> response) {
            assertEquals(OK.getStatusCode(), response.statusCode);
            return assertResponse(response.body);
        }

        public ResponseAssertions assertResponse(SimpleWorkflowScheme workflow) {
            if (scheme.draft != null) {
                scheme.draft.assertSimpleScheme(workflow);
                scheme.mappings.assertOriginalMappings(workflow);
            } else {
                scheme.assertSimpleScheme(workflow);
            }

            assertThat(workflow.getCurrentUser(), equalTo(user));
            assertThat(workflow.getIssueTypes().size(), equalTo(issueTypes.size()));
            Iterator<SimpleIssueType> actualIterator = workflow.getIssueTypes().iterator();
            for (IssueType issueType : issueTypes) {
                issueType.assertIssueType(actualIterator.next());
            }

            assertThat(workflow.getShared().getSharedWithProjects().size(), equalTo(projects.size()));
            Iterator<SimpleProject> actualProjectIterator = workflow.getShared().getSharedWithProjects().iterator();
            for (Project project : projects) {
                project.assertProject(actualProjectIterator.next());
            }

            return this;
        }
    }

    public class Project {
        private String name;
        private String key;
        private Long id;

        public Project() {
        }

        public Project setName(String name) {
            this.name = name;
            return this;
        }

        public Project setKey(String key) {
            this.key = key;
            return this;
        }

        public Project setId(Long id) {
            this.id = id;
            return this;
        }

        public String getName() {
            return name;
        }

        public String getKey() {
            return key;
        }

        public Long getId() {
            return id;
        }

        public Project create() {
            id = backdoor.project().addProject(name, key, ADMIN_USERNAME);
            return this;
        }

        public Project setWorkflowScheme(AssignableWorkflowScheme scheme) {
            backdoor.project().setWorkflowScheme(key, scheme.getId());
            return this;
        }

        public Project assertProject(SimpleProject actualProject) {
            assertThat(actualProject.getId(), equalTo(actualProject.getId()));
            assertThat(actualProject.getName(), equalTo(actualProject.getName()));
            assertThat(actualProject.getKey(), equalTo(actualProject.getKey()));
            assertThat(actualProject.getUrl(), nullValue());

            return this;
        }

        public void delete() {
            backdoor.project().deleteProject(key);
        }
    }

    private class WorkflowSchemeMappings {
        private Workflow defaultWorkflow;
        private Map<IssueType, Workflow> mappings;

        public WorkflowSchemeMappings() {
            mappings = Maps.newHashMap();
        }

        public WorkflowSchemeMappings(WorkflowSchemeMappings source) {
            this.mappings = Maps.newHashMap(source.mappings);
            this.defaultWorkflow = source.defaultWorkflow;
        }

        public Map<IssueType, Workflow> getMappings() {
            return mappings;
        }

        public WorkflowSchemeMappings setMappings(Map<IssueType, Workflow> mappings) {
            this.mappings = Maps.newHashMap(mappings);
            return this;
        }

        public WorkflowSchemeMappings setMapping(IssueType type, Workflow workflow) {
            this.mappings.put(type, workflow);
            return this;
        }

        public WorkflowSchemeMappings removeMapping(IssueType type) {
            this.mappings.remove(type);
            return this;
        }

        public WorkflowSchemeMappings clear() {
            this.mappings.clear();
            return this;
        }

        public Workflow getDefaultWorkflow() {
            return defaultWorkflow;
        }

        public WorkflowSchemeMappings setDefaultWorkflow(Workflow defaultWorkflow) {
            this.defaultWorkflow = defaultWorkflow;
            return this;
        }

        public WorkflowSchemeMappings removeDefaultWorkflow() {
            this.defaultWorkflow = null;
            return this;
        }

        public WorkflowSchemeData addMappingsTo(WorkflowSchemeData data) {
            if (defaultWorkflow != null) {
                data.setDefaultWorkflow(defaultWorkflow.name);
            }
            for (Map.Entry<IssueType, Workflow> entry : mappings.entrySet()) {
                data.setMapping(entry.getKey().id, entry.getValue().name);
            }
            return data;
        }

        public WorkflowSchemeMappings setMappingsFrom(WorkflowSchemeData data) {
            defaultWorkflow = Workflow.fromName(data.getDefaultWorkflow());
            if (defaultWorkflow == null) {
                defaultWorkflow = Workflow.JIRA;
            }
            mappings.clear();

            for (Map.Entry<String, String> entry : data.getMappings().entrySet()) {
                mappings.put(IssueType.fromName(entry.getKey()), Workflow.fromName(entry.getValue()));
            }
            return this;
        }

        public WorkflowSchemeMappings assertMappings(SimpleWorkflowScheme actualScheme) {
            return assertMappings(actualScheme.getMappings());
        }

        public WorkflowSchemeMappings assertOriginalMappings(SimpleWorkflowScheme actualScheme) {
            return assertMappings(actualScheme.getOriginalMappings());
        }

        private WorkflowSchemeMappings assertMappings(List<SimpleWorkflow> input) {
            Workflow defaultWorkflow = null;
            Map<IssueType, Workflow> calculatedScheme = Maps.newHashMap();
            for (SimpleWorkflow mapping : input) {
                Workflow workflow = Workflow.fromName(mapping.getName());
                if (mapping.isDefaultWorkflow()) {
                    if (defaultWorkflow != null) {
                        fail("Workflow '" + defaultWorkflow + "' and '" + workflow + "' both reported as the default.");
                    } else {
                        defaultWorkflow = workflow;
                    }
                }

                for (String issueType : mapping.getIssueTypes()) {
                    IssueType type = IssueType.fromId(issueType);
                    Workflow currentWorkflow = calculatedScheme.get(type);

                    if (currentWorkflow != null) {
                        fail("Response has two mappings for the issue type '" + type + "'.");
                    }
                    calculatedScheme.put(type, workflow);
                }
            }

            if (defaultWorkflow == null) {
                defaultWorkflow = Workflow.JIRA;
            }

            assertThat(calculatedScheme, equalTo(mappings));
            assertThat(defaultWorkflow, equalTo(this.defaultWorkflow));

            return this;
        }
    }

    public class DraftWorkflowScheme {
        private Long id;
        private String username;

        private WorkflowSchemeMappings mappings = new WorkflowSchemeMappings();
        private AssignableWorkflowScheme parent;

        public DraftWorkflowScheme(AssignableWorkflowScheme parent) {
            this.parent = parent;
            this.mappings = new WorkflowSchemeMappings(parent.mappings);
        }

        public DraftWorkflowScheme setWorkflowSchemeData(WorkflowSchemeData data) {
            setId(data.getId());
            setUsername(data.getLastModifiedUser());
            mappings.setMappingsFrom(data);
            return this;
        }

        public DraftWorkflowScheme setId(Long id) {
            this.id = id;
            return this;
        }

        public DraftWorkflowScheme setUsername(String username) {
            this.username = username;
            return this;
        }

        public DraftWorkflowScheme assertSimpleScheme(SimpleWorkflowScheme actualScheme) {
            assertThat(actualScheme.getId(), equalTo(id));
            assertThat(actualScheme.getName(), equalTo(parent.name));
            assertThat(actualScheme.getDescription(), equalTo(parent.description));
            assertThat(actualScheme.getLastModifiedUser().getName(), equalTo(username));
            assertThat(actualScheme.isDraftScheme(), equalTo(true));
            assertThat(actualScheme.isDefaultScheme(), equalTo(false));
            assertThat(actualScheme.getLastModifiedDate(), notNullValue());
            mappings.assertMappings(actualScheme);

            return this;
        }

        public DraftWorkflowScheme clear() {
            mappings.clear();
            return this;
        }

        public DraftWorkflowScheme removeDefault() {
            mappings.removeDefaultWorkflow();
            return this;
        }

        public DraftWorkflowScheme setDefaultWorkflow(Workflow workflow) {
            mappings.setDefaultWorkflow(workflow);
            return this;
        }

        public DraftWorkflowScheme setMapping(IssueType issueType, Workflow workflow) {
            mappings.setMapping(issueType, workflow);
            return this;
        }

        public AssignableWorkflowScheme discard() {
            backdoor.workflowSchemes().discardDraftScheme(parent.id);
            return discardLocal();
        }

        public DraftWorkflowScheme save() {
            WorkflowSchemeData data = new WorkflowSchemeData();
            mappings.addMappingsTo(data);
            setWorkflowSchemeData(backdoor.workflowSchemes().updateDraftScheme(parent.id, data));
            return this;
        }

        public DraftWorkflowScheme removeMapping(IssueType issueType) {
            mappings.removeMapping(issueType);
            return this;
        }

        public AssignableWorkflowScheme discardLocal() {
            parent.draft = null;
            return parent;
        }
    }

    public class AssignableWorkflowScheme {
        private Long id;
        private String name;
        private String description;
        private WorkflowSchemeMappings mappings = new WorkflowSchemeMappings();
        private DraftWorkflowScheme draft;

        public String getDescription() {
            return description;
        }

        public AssignableWorkflowScheme setDescription(String description) {
            this.description = description;
            return this;
        }

        public String getName() {
            return name;
        }

        public AssignableWorkflowScheme setName(String name) {
            this.name = name;
            return this;
        }

        public Map<IssueType, Workflow> getMappings() {
            return mappings.getMappings();
        }

        public AssignableWorkflowScheme setMappings(Map<IssueType, Workflow> mappings) {
            this.mappings.setMappings(mappings);
            return this;
        }

        public AssignableWorkflowScheme setMapping(IssueType type, Workflow workflow) {
            this.mappings.setMapping(type, workflow);
            return this;
        }

        public AssignableWorkflowScheme removeMapping(IssueType type) {
            this.mappings.removeMapping(type);
            return this;
        }

        public AssignableWorkflowScheme clear() {
            this.mappings.clear();
            return this;
        }

        public Workflow getDefaultWorkflow() {
            return this.mappings.defaultWorkflow;
        }

        public AssignableWorkflowScheme setDefaultWorkflow(Workflow defaultWorkflow) {
            this.mappings.setDefaultWorkflow(defaultWorkflow);
            return this;
        }

        public AssignableWorkflowScheme removeDefaultWorkflow() {
            this.mappings.removeDefaultWorkflow();
            return this;
        }

        private AssignableWorkflowScheme setId(Long id) {
            this.id = id;
            return this;
        }

        public AssignableWorkflowScheme save() {
            if (id == null) {
                setWorkflowSchemeData(backdoor.workflowSchemes().createScheme(asWorkflowSchemeData()));
            } else {
                setWorkflowSchemeData(backdoor.workflowSchemes().updateScheme(asWorkflowSchemeData()));
            }
            return this;
        }

        public AssignableWorkflowScheme setWorkflowSchemeData(WorkflowSchemeData data) {
            setId(data.getId());
            setName(data.getName()).setDescription(data.getDescription());
            mappings.setMappingsFrom(data);
            return this;
        }

        public WorkflowSchemeData asWorkflowSchemeData() {
            WorkflowSchemeData data = new WorkflowSchemeData();
            data.setName(name).setDescription(description).setId(id);
            return mappings.addMappingsTo(data);
        }

        public AssignableWorkflowScheme assertSimpleScheme(SimpleWorkflowScheme actualScheme) {
            assertThat(actualScheme.getName(), equalTo(name));
            assertThat(actualScheme.getDescription(), equalTo(description));
            assertThat(actualScheme.isDraftScheme(), equalTo(false));
            assertThat(actualScheme.isDefaultScheme(), equalTo(false));
            assertThat(actualScheme.getLastModifiedDate(), nullValue());
            assertThat(actualScheme.getLastModifiedUser(), nullValue());

            mappings.assertMappings(actualScheme);

            return this;
        }

        public Long getId() {
            return id;
        }

        public DraftWorkflowScheme createDraft() {
            WorkflowSchemeData data = backdoor.workflowSchemes().createDraft(asWorkflowSchemeData());
            return draft = new DraftWorkflowScheme(this).setWorkflowSchemeData(data);
        }

        public DraftWorkflowScheme localDraft() {
            return draft = new DraftWorkflowScheme(this);
        }

        public void delete() {
            backdoor.workflowSchemes().deleteScheme(id);
        }
    }

    public static class Workflow {
        private static Workflow JIRA = new Workflow("jira", "JIRA Workflow (jira)", "The default JIRA workflow.");
        private static Workflow ONE = new Workflow("One", "Description for One");
        private static Workflow TWO = new Workflow("Two", null);

        private static List<Workflow> ALL = ImmutableList.of(JIRA, ONE, TWO);

        private final String name;
        private final String displayName;
        private final String description;

        public Workflow(String name, String description) {
            this(name, name, description);
        }

        public Workflow(String name, String displayName, String description) {
            this.name = name;
            this.displayName = displayName;
            this.description = description;
        }

        public static Workflow fromName(String workflow) {
            if (workflow == null) {
                return null;
            }
            for (Workflow wf : ALL) {
                if (wf.name.equals(workflow)) {
                    return wf;
                }
            }
            throw new RuntimeException("Workflow of name '" + workflow + "' not found.");
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            Workflow workflow = (Workflow) o;

            return name.equals(workflow.name);
        }

        @Override
        public int hashCode() {
            return name.hashCode();
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public static class IssueType {
        private static IssueType BUG = new IssueType(1, "Bug", "A problem which impairs or prevents the functions of the product.");
        private static IssueType IMPROVEMENT = new IssueType(4, "Improvement", "An improvement or enhancement to an existing feature or task.");
        private static IssueType NEW_FEATURE = new IssueType(2, "New Feature", "A new feature of the product, which has yet to be developed.");
        private static IssueType TASK = new IssueType(3, "Task", "A task that needs to be done.");

        private static List<IssueType> ALL = ImmutableList.of(BUG, IMPROVEMENT, NEW_FEATURE, TASK);

        private final String id;
        private final String name;
        private final String description;

        private IssueType(long id, String name, String description) {
            this.id = String.valueOf(id);
            this.name = name;
            this.description = description;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            IssueType issueType = (IssueType) o;

            return id.equals(issueType.id);
        }

        @Override
        public int hashCode() {
            return id.hashCode();
        }

        public static IssueType fromId(String id) {
            if (id == null) {
                return null;
            }

            for (IssueType issueType : ALL) {
                if (issueType.id.equals(id)) {
                    return issueType;
                }
            }
            throw new RuntimeException("Unable to find an issue type with id '" + id + "'");
        }

        public static IssueType fromName(String id) {
            if (id == null) {
                return null;
            }

            for (IssueType issueType : ALL) {
                if (issueType.name.equals(id)) {
                    return issueType;
                }
            }
            throw new RuntimeException("Unable to find an issue type with id '" + id + "'");
        }

        public void assertIssueType(SimpleIssueType actualIssueType) {
            assertThat(actualIssueType.getName(), equalTo(name));
            assertThat(actualIssueType.getDescription(), equalTo(description));
            assertThat(actualIssueType.getId(), equalTo(id));
            assertThat(actualIssueType.getIconUrl(), endsWith(issueTypeUrls.get(name)));
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public class EditorResource extends RestApiClient<EditorResource> {
        private final String root;

        public EditorResource(JIRAEnvironmentData environmentData) {
            super(environmentData);
            root = environmentData.getBaseUrl().toExternalForm();
        }

        @Override
        protected WebResource createResource() {
            return resourceRoot(root).path("rest").path("globalconfig").path("latest").path("workflowschemeeditor");
        }

        public WebResource createResource(long id) {
            return createResource().path(valueOf(id));
        }

        public Response<SimpleWorkflowScheme> getScheme(final long id) {
            return toResponse(new Method() {
                @Override
                public ClientResponse call() {
                    return createResource(id).get(ClientResponse.class);
                }
            }, SimpleWorkflowScheme.class);
        }

        public Response<SimpleWorkflowScheme> deleteIssueType(final long id, final String issueTypeId, final String workflow) {
            return toResponse(new Method() {
                @Override
                public ClientResponse call() {
                    return createResource(id).path("issuetype").type(MediaType.APPLICATION_JSON_TYPE).delete(ClientResponse.class, new WorkflowSchemeRequest(workflow, asList(issueTypeId)));
                }
            }, SimpleWorkflowScheme.class);
        }

        public Response<SimpleWorkflowScheme> deleteWorkflow(final long id, final String workflow) {
            return toResponse(new Method() {
                @Override
                public ClientResponse call() {
                    return createResource(id).path("workflow").type(MediaType.APPLICATION_JSON_TYPE).delete(ClientResponse.class, new RemoveWorkflowRequest(workflow));
                }
            }, SimpleWorkflowScheme.class);
        }

        public Response<SimpleWorkflowScheme> deleteWorkflow(final long id, final String workflow, final String nextDefaultWorkflow) {
            return toResponse(new Method() {
                @Override
                public ClientResponse call() {
                    return createResource(id).path("workflow").type(MediaType.APPLICATION_JSON_TYPE).delete(ClientResponse.class, new RemoveWorkflowRequest(workflow, nextDefaultWorkflow));
                }
            }, SimpleWorkflowScheme.class);
        }

        public Response<SimpleWorkflowScheme> assignIssueTypes(final long id, final String workflow, final Collection<String> issueTypes, final boolean defaultWorkflow) {
            return toResponse(new Method() {
                @Override
                public ClientResponse call() {
                    return createResource(id).type(MediaType.APPLICATION_JSON_TYPE).put(ClientResponse.class, new WorkflowSchemeRequest(workflow, issueTypes, defaultWorkflow));
                }
            }, SimpleWorkflowScheme.class);
        }

        public Response<SimpleWorkflowScheme> discardDraft(final long id) {
            return toResponse(new Method() {
                @Override
                public ClientResponse call() {
                    return createResource(id).path("draft").type(MediaType.APPLICATION_JSON_TYPE).delete(ClientResponse.class);
                }
            }, SimpleWorkflowScheme.class);
        }

        public Response<SimpleWorkflowScheme> updateName(final long id, final String name) {
            return toResponse(new Method() {
                @Override
                public ClientResponse call() {
                    return createResource(id).path("name").type(MediaType.APPLICATION_JSON_TYPE).put(ClientResponse.class, name);
                }
            }, SimpleWorkflowScheme.class);
        }

        public Response<SimpleWorkflowScheme> updateDescription(final long id, final String description) {
            return toResponse(new Method() {
                @Override
                public ClientResponse call() {
                    return createResource(id).path("description").type(MediaType.APPLICATION_JSON_TYPE).put(ClientResponse.class, description);
                }
            }, SimpleWorkflowScheme.class);
        }
    }

    public static class WorkflowSchemeRequest {
        @JsonProperty
        private final String workflow;

        @JsonProperty
        private final Collection<String> issueTypes;

        @JsonProperty
        private final boolean defaultWorkflow;

        public WorkflowSchemeRequest(String workflow, Collection<String> issueTypes, boolean defaultWorkflow) {
            this.workflow = workflow;
            this.issueTypes = issueTypes;
            this.defaultWorkflow = defaultWorkflow;
        }

        public WorkflowSchemeRequest(String workflow, Collection<String> issueTypes) {
            this(workflow, issueTypes, false);
        }

        public WorkflowSchemeRequest(String workflow) {
            this(workflow, null, false);
        }
    }


    public static class RemoveWorkflowRequest {
        @JsonProperty
        private String workflow;

        @JsonProperty
        private String nextDefaultWorkflow;

        public RemoveWorkflowRequest(String workflow) {
            this(workflow, null);
        }

        public RemoveWorkflowRequest(String workflow, String nextDefaultWorkflow) {
            this.workflow = workflow;
            this.nextDefaultWorkflow = nextDefaultWorkflow;
        }
    }
}
