package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.email.OutgoingMailHelper;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.icegreen.greenmail.util.GreenMailUtil;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

import static org.junit.Assert.assertTrue;

/**
 * Base class for testing email notifications
 *
 * @since 7.1
 */
public class BaseJiraEmailTest extends BaseJiraFuncTest {

    @Inject
    protected Assertions assertions;

    @Inject
    protected OutgoingMailHelper mailHelper;

    protected void assertEmailTitleEquals(MimeMessage message, String title) throws MessagingException {
        String previewUrl = backdoor.outgoingMailControl().getMessagePreviewURI(message);
        // use dialog directly so it does not apply contextPath
        tester.gotoPage(previewUrl);
        assertions.assertNodeHasText(new CssLocator(tester, ".page-title-pattern-header a"), title);
    }

}
