package com.atlassian.jira.webtests.ztests.workflow;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperations;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_ASSIGNEE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.LINK_ASSIGN_ISSUE;

/**
 * Loads data with a slightly customized permission scheme and a 'ChangeRequest' workflow XML with added <meta> attributes
 * to restrict permissions per status.
 */
@WebTest({Category.FUNC_TEST, Category.PERMISSIONS, Category.WORKFLOW})
@Restore("WorkflowBasedPermissions.zip")
@LoginAs(user = ADMIN_USERNAME)
public class TestWorkflowBasedPermissions extends BaseJiraFuncTest {

    @Inject
    private BulkOperations bulkOperations;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        backdoor.darkFeatures().enableForSite("no.frother.assignee.field");
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
        // Ensure attachments point to the correct directory
        administration.attachments().enable();
    }


    @Test
    public void testWorkflowPermissions() {
        _testSingleIssuePermissions();

        _testBulkEdit();
    }

    private void _testSingleIssuePermissions() {
        logger.log("Check that non-overridden 'assignables' are correct");
        navigation.login("test", "test");
        createIssueStep1("Test Project", "Bug");
        tester.assertFormElementPresent("assignee");
        tester.assertOptionValuesEqual("assignee", new String[]{"-1", "assignable", "devadmin", "developer", "test"});

        logger.log("Test that the 'assignables' list is overridden when creating issues");
        createIssueStep1("Test Project", "ChangeRequest");
        tester.assertOptionValuesEqual("assignee", new String[]{"-1", "assignable", "devadmin", "developer"});

        logger.log("Testing normal permissions on Open state");
        // Check that the normal permissions are all working
        //                                                  browse, assign, attach, clone,  comment,create, delete, edit,   link,   move
        assertIssuePermissions("test", "TP-8", new boolean[]{true, true, true, true, true, true, true, true, true, true});
        assertIssuePermissions("test", "TP-9", new boolean[]{true, true, true, true, true, false, true, true, true, true});
        navigation.issue().gotoIssue("TP-8");
        tester.clickLink("assign-issue");
        tester.assertOptionValuesEqual("assignee", new String[]{"-1", "developer", "test", "assignable", "devadmin", "developer", "test"});

        logger.log("Set the assignee to 'developer'");
        tester.selectOption("assignee", "Joe Developer");
        tester.submit();
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Assignee:", "Joe Developer");

        logger.log("Progress workflow to 'Approved' status");
        tester.clickLinkWithText("Approved");
        tester.setFormElement("summary", "Approved CR");
        tester.submit();

        //                                                          browse, assign, attach, clone,  comment,    create, delete, edit,   link,   move
        assertIssuePermissions("test", "TP-8", new boolean[]{true, false, true, true, true, true, true, false, false, false});
        assertIssuePermissions("user1", "TP-8", new boolean[]{true, false, false, true, false, true, false, false, false, false});
        assertIssuePermissions("qa", "TP-8", new boolean[]{true, false, false, true, false, true, false, false, false, false});
        assertIssuePermissions("assignable", "TP-8", new boolean[]{true, true, true, true, false, true, false, false, false, false});
        assertIssuePermissions("manager", "TP-8", new boolean[]{true, false, false, true, false, true, false, false, true, false});
        assertIssuePermissions("devadmin", "TP-8", new boolean[]{true, false, true, true, false, true, false, false, false, true});

        // Subtasks can only be browsed, and cloned (since they can be created in the parent).
        assertIssuePermissions("test", "TP-9", new boolean[]{true, false, false, true, false, false, false, false, false, false});

        navigation.issue().gotoIssue("TP-8");
        logger.log("Check that permissions are back to normal in 'In Progress' state");
        tester.clickLinkWithText("Start Progress");
        tester.setFormElement("summary", "In Progress CR");
        tester.submit();
        assertIssuePermissions("test", "TP-8", new boolean[]{true, true, true, true, true, true, true, true, true, true});

        navigation.issue().gotoIssue("TP-8");
        logger.log("Check that the correct users are assignable in 'In Progress' state");
        tester.clickLink("assign-issue");
        tester.assertOptionValuesEqual("assignee", new String[]{"-1", "developer", "test", "assignable", "devadmin", "developer", "test"});

        navigation.issue().gotoIssue("TP-10");
        logger.log("Check that a subtask in the 'Approved' state becomes invisible");
        tester.clickLinkWithText("Approved");
        tester.setFormElement("summary", "hideme");
        tester.submit();
        assertions.getViewIssueAssertions().assertIssueNotFound();

        navigation.issue().gotoIssue("TP-8");
        logger.log("Check that on 'Resolve' transition screen, we see subset of assignees permissible for Resolved issues");
        tester.clickLinkWithText("Resolve");
        tester.assertOptionValuesEqual("assignee", new String[]{"-1", "developer", "developer"});

        logger.log("Check that we can resolve the issue");
        tester.setFormElement("summary", "Resolved CR");
        tester.selectOption("assignee", "Joe Developer");
        tester.setFormElement("customfield_10000", "developer"); // Set the 'Customer' field to developer; only this user will be allowed to edit
        tester.submit();
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Assignee:", "Joe Developer");

        logger.log("Check that in the 'Resolved' state, editing is limited to developer (custom field val), and permissions are otherwise normal");
        //                                                          browse, assign, attach, clone,  comment,    create, delete, edit,   link,   move
        assertIssuePermissions("test", "TP-8", new boolean[]{true, true, true, true, true, true, true, false, true, true});
        assertIssuePermissions("developer", "TP-8", new boolean[]{true, true, true, true, true, true, false, true, true, true});
        assertIssuePermissions("qa", "TP-8", new boolean[]{true, false, true, true, true, true, false, false, false, false});
        assertIssuePermissions("assignable", "TP-8", new boolean[]{true, true, true, true, true, true, false, false, true, true});
        assertIssuePermissions("manager", "TP-8", new boolean[]{true, false, true, true, true, true, false, false, true, false});
        assertIssuePermissions("devadmin", "TP-8", new boolean[]{true, true, true, true, true, true, false, false, true, true});
    }

    private void assertIssuePermissions(String user, String key, boolean[] permissions) {
        navigation.login(user, user);
        navigation.issue().gotoIssue(key);
        logger.log("Testing " + key + " permissions for " + user);
        if (permissions[0]) { // browse
            if (permissions[1]) {
                tester.assertLinkPresent("assign-issue");
            } else {
                tester.assertLinkNotPresent("assign-issue");
            }
            if (permissions[2]) {
                tester.assertLinkPresent("attach-file");
            } else {
                tester.assertLinkNotPresent("attach-file");
            }
            if (permissions[3]) {
                tester.assertLinkPresent("clone-issue");
            } else {
                tester.assertLinkNotPresent("clone-issue");
            }
            if (permissions[4]) {
                tester.assertLinkPresent("comment-issue");
                tester.assertLinkPresent("footer-comment-button");
            } else {
                tester.assertLinkNotPresent("comment-issue");
                tester.assertLinkNotPresent("footer-comment-button");
            }
            if (permissions[5]) {
                tester.assertLinkPresent("create-subtask");
            } else {
                tester.assertLinkNotPresent("create-subtask");
            }
            if (permissions[6]) {
                tester.assertLinkPresent("delete-issue");
            } else {
                tester.assertLinkNotPresent("delete-issue");
            }
            if (permissions[7]) {
                tester.assertLinkPresent("edit-issue");
            } else {
                tester.assertLinkNotPresent("edit-issue");
            }
            if (permissions[8]) {
                tester.assertLinkPresent("link-issue");
            } else {
                tester.assertLinkNotPresent("link-issue");
            }
            if (permissions[9]) {
                tester.assertLinkPresent("move-issue");
            } else {
                tester.assertLinkNotPresent("move-issue");
            }
        } else {
            tester.assertTextPresent("Permission violation");
        }
    }

    private void _testBulkEdit() {
        // Ensure that bulk
        tester.clickLink(LINK_ASSIGN_ISSUE);
        tester.assertOptionValuesEqual("assignee", new String[]{"-1", "developer", "developer"});

        navigation.issue().gotoIssue("TP-11");
        tester.clickLink(LINK_ASSIGN_ISSUE);
        tester.assertOptionValuesEqual("assignee", new String[]{"-1", "test", "assignable", "devadmin", "developer", "test"});

        navigation.issueNavigator().displayAllIssues();

        bulkOperations.bulkChangeIncludeAllPages();

        bulkOperations.bulkChangeSelectIssues(Arrays.asList("TP-8", "TP-11"));

        bulkOperations.bulkChangeChooseOperationEdit();

        // Ensure only 'developer' option is available as TP-8 is in the Resolved status
        tester.assertOptionValuesEqual("assignee", new String[]{"-1", "developer", "developer"});

        Map<String, String> fields = new HashMap<String, String>();
        fields.put(FIELD_ASSIGNEE, "Joe Developer");
        bulkOperations.bulkEditOperationDetailsSetAs(fields);

        fields = new HashMap<String, String>();
        fields.put(FIELD_ASSIGNEE, "Joe Developer");
        bulkOperations.bulkEditConfirmEdit(fields);
        bulkOperations.bulkChangeConfirm();
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        navigation.issue().gotoIssue("TP-8");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Assignee", "Joe Developer");

        navigation.issue().gotoIssue("TP-11");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Assignee", "Joe Developer");

        // Now ensure that if only one issue is selected all of its assignees are available
        navigation.issueNavigator().displayAllIssues();

        bulkOperations.bulkChangeIncludeAllPages();

        bulkOperations.bulkChangeSelectIssues(Arrays.asList("TP-11"));

        bulkOperations.bulkChangeChooseOperationEdit();

        tester.assertOptionValuesEqual("assignee", new String[]{"-1", "developer", "assignable", "devadmin", "developer"});

        fields = new HashMap<String, String>();
        fields.put(FIELD_ASSIGNEE, "Dev-Admin");
        bulkOperations.bulkEditOperationDetailsSetAs(fields);

        fields = new HashMap<String, String>();
        fields.put(FIELD_ASSIGNEE, "Dev-Admin");
        bulkOperations.bulkEditConfirmEdit(fields);
        bulkOperations.bulkChangeConfirm();
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        navigation.issue().gotoIssue("TP-11");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Assignee", "Dev-Admin");
    }

    private void createIssueStep1(String project, String issueType) {
        navigation.issue().goToCreateIssueForm(project, issueType);
    }

}
