package com.atlassian.jira.webtests.ztests.email;

import com.atlassian.jira.functest.framework.BaseJiraEmailTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.rule.MailTest;
import com.icegreen.greenmail.util.GreenMailUtil;
import org.junit.Test;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Optional;

/**
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.EMAIL})
@RestoreBlankInstance
public class TestUserSignup extends BaseJiraEmailTest {
    private static final String FULL_NAME = "full name";

    private static final String SECRET_PASSWORD = "secretpassword";
    private static final String USER1 = "user1";

    private static final String USER1_EMAIL = "user1@example.com";
    private static final String USER2 = "user2";

    private static final String USER2_EMAIL = "user2@example.org";

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    @MailTest
    public void testUserSignup() throws InterruptedException, MessagingException, IOException {
        signupAs(USER1, USER1_EMAIL);
        assertSignupEmail(flushAndGetLastMessage(), USER1, USER1_EMAIL);

        signupAs(USER2, USER2_EMAIL);
        assertSignupEmail(flushAndGetLastMessage(), USER2, USER2_EMAIL);
    }

    private MimeMessage flushAndGetLastMessage() throws InterruptedException {
        final Optional<MimeMessage> message = mailHelper.flushMailQueueAndWait(1).stream().findFirst();
        backdoor.outgoingMailControl().clearMessages();
        return message.get();
    }

    private void assertSignupEmail(final MimeMessage message, final String userName, final String emailAddress) throws MessagingException, IOException {
        String toAddr = message.getHeader("To", "XXX");
        String body = GreenMailUtil.getBody(message);

        textAssertions.assertTextSequence(toAddr, emailAddress);
        textAssertions.assertTextSequence(body, "Username:", userName);
        textAssertions.assertTextSequence(body, "Email:", emailAddress);
        textAssertions.assertTextSequence(body, "Full Name:", userName + FULL_NAME);

        // assert that we dont expose the password eg JRA-6175
        textAssertions.assertTextNotPresent(body, SECRET_PASSWORD);
    }

    private void signupAs(final String userName, final String emailAddress) {
        navigation.logout();
        tester.gotoPage("secure/Signup!default.jspa");
        tester.setFormElement("username", userName);
        tester.setFormElement("password", SECRET_PASSWORD);
        tester.setFormElement("fullname", userName + FULL_NAME);
        tester.setFormElement("email", emailAddress);
        tester.submit();

        assertions.assertNodeHasText(locator.css("#content .aui-message.aui-message-success"),
                "You have successfully signed up. If you forget your password, you can request a new one.");
        navigation.gotoPage("/");
    }
}