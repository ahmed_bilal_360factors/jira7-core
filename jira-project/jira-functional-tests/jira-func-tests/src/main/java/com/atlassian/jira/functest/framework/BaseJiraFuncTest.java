package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.functest.rule.DisableBigPipeRule;
import com.atlassian.jira.functest.rule.RestoreDataBackdoor;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.rules.RuleChain;

import javax.inject.Inject;

/**
 * Handy class for all func tests that requires HtmlUnit objects.
 *
 * @since v6.5
 */
public class BaseJiraFuncTest {
    @ClassRule
    public static final RuleChain initClass = RuleChain.emptyRuleChain()
            .around(new RestoreDataBackdoor())
            .around(new DisableBigPipeRule(new BackdoorModule()));
    @Rule
    public RuleChain testRule = FuncTestRuleChain.forTest(this);

    @Inject
    protected Backdoor backdoor;
    @Inject
    protected Navigation navigation;
    @Inject
    protected WebTester tester;
    @Inject
    protected JIRAEnvironmentData environmentData;
    @Inject
    protected Assertions assertions;

    public JIRAEnvironmentData getEnvironmentData() {
        return environmentData;
    }

    public Backdoor getBackdoor() {
        return backdoor;
    }

    public WebTester getTester() {
        return tester;
    }

    public Assertions getAssertions() {
        return assertions;
    }
}
