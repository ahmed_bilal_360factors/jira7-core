package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.matcher.PageBeanMatcher;
import com.atlassian.jira.functest.matcher.UserJsonMatcher;
import com.atlassian.jira.testkit.beans.UserDTO;
import com.atlassian.jira.testkit.client.restclient.GenericRestClient;
import com.atlassian.jira.testkit.client.restclient.UserJson;
import com.sun.jersey.api.client.ClientResponse;
import org.apache.http.HttpHeaders;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.atlassian.jira.webtests.ztests.bundledplugins2.rest.util.PropertyAssertions.assertUniformInterfaceException;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.REST, Category.USERS_AND_GROUPS})
@RestoreBlankInstance
public class TestGroupResourceFunc extends BaseJiraFuncTest {
    private GroupClient groupClient;
    private GenericRestClient genericRestClient;

    @Before
    public void setup() {
        groupClient = new GroupClient(environmentData);
        genericRestClient = new GenericRestClient();
    }

    @After
    public void teardown() {
        groupClient.close();
    }

    @Test
    public void testGroupResourceAddAndRemoveUserHappyPath() {
        createGroup("jedi");

        ensureGroupExists("jedi");
        addUserToGroup("jedi", "fred");
        ensureCantAddUserToGroup("jedi", "fred");

        removeUserFromGroup("jedi", "fred");
        ensureCantRemoveUserFromGroup("jedi", "fred");
    }

    @Test
    public void testGroupResourceDeleteGroupHappyPath() {
        createGroup("jedi");

        ensureGroupExists("jedi");
        deleteGroup("jedi");

        ensureNoGroup("jedi");
        ensureCantDeleteGroup("jedi");
    }

    /**
     * Test 'self' and 'nextPage' links can be used to navigate across pages of users
     */
    @Test
    public void testGettingUsersFromGroupPaginatedSelfAndNextPageLinks() {
        final String groupname = "testGroup";

        backdoor.usersAndGroups().addGroup(groupname);

        List<String> usernames = createUsersInGroup(groupname, 3);

        final UsersPageBean page1 = groupClient.getPaginatedUsersForGroup(groupname, true, 0L, 2L);
        assertThat("Self link is present on first page", page1.getSelf(), notNullValue());

        UsersPageBean samePage1 = genericRestClient.get(page1.getSelf(), UsersPageBean.class).body;
        assertThat("Self link gives same result for first page", samePage1.getValues(), equalTo(page1.getValues()));
        assertThat("nextPage link is present on first page", page1.getNextPage(), notNullValue());

        UsersPageBean page2 = genericRestClient.get(page1.getNextPage(), UsersPageBean.class).body;
        assertThat("nextPage link finds last user on last page", page2.getValues(), hasItems(UserJsonMatcher.matcher(is(usernames.get(2)), is(true))));
        assertThat("Self link is present on last page", page2.getSelf(), notNullValue());

        UsersPageBean samePage2 = genericRestClient.get(page2.getSelf(), UsersPageBean.class).body;
        assertThat("Self link gives same result for last page", samePage2.getValues(), equalTo(page2.getValues()));
        assertThat("nextPage link is NOT present on last page", page2.getNextPage(), nullValue());
    }

    @Test
    public void testGettingUsersFromGroupPaginated() {
        String groupname = "testGroup";

        backdoor.usersAndGroups().addGroup(groupname);

        List<String> usernames = createUsersInGroup(groupname, 100);

        assertGetPage(0, 10, 100, usernames, groupname, false);
        assertGetPage(0, 20, 100, usernames, groupname, false);
        assertGetPage(10, 10, 100, usernames, groupname, false);
        assertGetPage(10, 10, 100, usernames, groupname, true);

        UserDTO userToDeactive = backdoor.usersAndGroups().getUserByName("0testuser");

        backdoor.usersAndGroups().updateUser(new UserDTO(false,
                userToDeactive.getDirectoryId(),
                userToDeactive.getDisplayName(),
                userToDeactive.getEmail(),
                userToDeactive.getKey(),
                userToDeactive.getName(),
                userToDeactive.getUsername(),
                null));

        assertThat(groupClient.getPaginatedUsersForGroup(groupname, true, 0l, 1l).getValues(),
                Matchers.contains(UserJsonMatcher.matcher(is("0testuser"), is(false))));
    }

    @Test
    public void testGettingPaginatedUsersFromGroupForNotExistingGroupReturnsNotFound() {
        assertUniformInterfaceException(() -> {
            groupClient.getPaginatedUsersForGroup("not_existing_group", false, 0l, 2l);
            return null;
        }, Response.Status.NOT_FOUND);
    }

    @Test
    public void testGettingPaginatedUsersWithoutPermissionsReturnsForbidden() {
        String groupName = "new_group";

        backdoor.usersAndGroups().addGroup(groupName);
        assertUniformInterfaceException(() -> {
            backdoor.usersAndGroups().addUser("not_admin");
            groupClient.loginAs("not_admin", "not_admin").getPaginatedUsersForGroup(groupName, false, 0l, 2l);
            return null;
        }, Response.Status.FORBIDDEN);
    }

    @Test
    public void testGettingPaginatesUsersAnonymouslyReturnsUnathorized() {
        String groupName = "new_group";

        backdoor.usersAndGroups().addGroup(groupName);
        assertUniformInterfaceException(() -> {
            groupClient.anonymous().getPaginatedUsersForGroup(groupName, false, 0l, 2l);
            return null;
        }, Response.Status.UNAUTHORIZED);
    }

    @Test
    public void testGettingPaginatesUsersWithIllegalGroupNameReturnsBadRequest() {
        assertUniformInterfaceException(() -> {
            groupClient.getPaginatedUsersForGroup("", false, 0l, 2l);
            return null;
        }, Response.Status.BAD_REQUEST);
    }

    private void assertGetPage(final int startAt,
                               final int maxResults,
                               final int total,
                               final List<String> usernames,
                               final String groupname,
                               boolean includeInactive) {
        UsersPageBean usersForGroup = groupClient.getPaginatedUsersForGroup(groupname, includeInactive, (long) startAt, (long) maxResults);

        List<Matcher<? super UserJson>> matchers = usernames.stream()
                .skip(startAt)
                .limit(maxResults)
                .map(username -> UserJsonMatcher.matcher(is(username), is(true)))
                .collect(Collectors.toList());

        assertThat(usersForGroup, PageBeanMatcher.matcher(is((long) startAt), is((long) total), is(maxResults), Matchers.contains(matchers)));
    }

    private List<String> createUsernames(final IntStream range) {
        return range.mapToObj(index -> index + "testuser")
                .sorted(new Comparator<String>() {
                    @Override
                    public int compare(final String o1, final String o2) {
                        return o1.compareToIgnoreCase(o2);
                    }
                })
                .collect(Collectors.toList());
    }

    /**
     * Create <code>count</code> users and add them to the group
     *
     * @param groupName the group name
     * @param count     the number of users to create
     **/
    private List<String> createUsersInGroup(final String groupName, int count) {
        List<String> userNames = createUsernames(IntStream.range(0, count));

        userNames.forEach(username -> {
            backdoor.usersAndGroups().addUser(username, username, username, username + "@atlassian.com", false);
            backdoor.usersAndGroups().addUserToGroup(username, groupName);
        });
        return userNames;
    }

    private void ensureCantRemoveUserFromGroup(final String group, final String user) {
        final ClientResponse response = groupClient.removeUserFromGroup(group, user);
        assertThat(response.getStatus(), equalTo(Response.Status.BAD_REQUEST.getStatusCode()));
        response.close();
    }

    private void ensureGroupExists(final String group) {
        final ClientResponse response = groupClient.getGroup(group);
        assertThat(response.getStatus(), equalTo(Response.Status.OK.getStatusCode()));
        response.close();
    }

    private void ensureNoGroup(final String group) {
        final ClientResponse response = groupClient.getGroup(group);
        assertThat(response.getStatus(), equalTo(Response.Status.NOT_FOUND.getStatusCode()));
        response.close();
    }

    private void createGroup(final String group) {
        final ClientResponse response = groupClient.createGroup(group);
        assertThat(response.getStatus(), equalTo(Response.Status.CREATED.getStatusCode()));
        final String createResult = response.getHeaders().get(HttpHeaders.LOCATION).get(0);
        final String expectedUrl = environmentData.getBaseUrl() + "/rest/api/2/group?groupname=" + group;
        assertThat(createResult, startsWith(expectedUrl));
        response.close();
    }

    private void addUserToGroup(final String group, final String user) {
        final ClientResponse response = groupClient.addUserToGroup(group, user);
        assertThat(response.getStatus(), equalTo(Response.Status.CREATED.getStatusCode()));

        final String createResult = response.getHeaders().get(HttpHeaders.LOCATION).get(0);
        final String expectedUrl = environmentData.getBaseUrl() + "/rest/api/2/group?groupname=" + group;
        assertThat(createResult, startsWith(expectedUrl));
        response.close();
    }

    private void ensureCantAddUserToGroup(final String group, final String user) {
        final ClientResponse response = groupClient.addUserToGroup(group, user);
        assertThat(response.getStatus(), equalTo(Response.Status.BAD_REQUEST.getStatusCode()));
        response.close();
    }

    private void deleteGroup(final String group) {
        final ClientResponse response = groupClient.deleteGroup(group);
        assertThat(response.getStatus(), equalTo(Response.Status.OK.getStatusCode()));
        response.close();
    }

    private void ensureCantDeleteGroup(final String group) {
        final ClientResponse response = groupClient.deleteGroup(group);
        assertThat(response.getStatus(), equalTo(Response.Status.NOT_FOUND.getStatusCode()));
        response.close();
    }

    private void removeUserFromGroup(final String group, final String user) {
        final ClientResponse response = groupClient.removeUserFromGroup(group, user);
        assertThat(response.getStatus(), equalTo(Response.Status.OK.getStatusCode()));
        response.close();
    }
}
