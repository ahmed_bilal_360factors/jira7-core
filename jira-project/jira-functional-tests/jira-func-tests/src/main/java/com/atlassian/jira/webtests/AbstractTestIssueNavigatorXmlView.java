package com.atlassian.jira.webtests;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.StringReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AbstractTestIssueNavigatorXmlView extends AbstractTestIssueNavigatorView {

    public String assertAndGetLinkToFilterWithId(final String filterId) throws DocumentException {
        final String previousViewUrl = getLinkToIssueNavigator();
        //check that the link does not contain the reset parameter
        assertTrue(!previousViewUrl.contains("reset="));
        //check that the link has the correct filter id
        assertEquals(getEnvironmentData().getBaseUrl().toString() + "/issues/?filter=" + filterId, previousViewUrl);
        return previousViewUrl;
    }

    public String getLinkToIssueNavigator() throws DocumentException {
        final SAXReader saxReader = new SAXReader();
        final org.dom4j.Document xmlResponse = saxReader.read(new StringReader(tester.getDialog().getResponseText()));
        final Element linkElement = xmlResponse.getRootElement().element("channel").element("link");
        // Do NOT trim the text, as if we do have spaces in the text, then some clients (e.g. Confluence's JIRA issues macro
        // will fail). So we need to do assertions against the raw value. JRA-16175
        return linkElement.getText();
    }
}
