/*
 * Copyright � 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.DarkFeature;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import javax.ws.rs.core.MediaType;
import java.util.Map;

/**
 * Client for the site dark features resource.
 *
 * @since v7.1
 */
public class FeatureKeyClient extends RestApiClient<FeatureKeyClient> {
    /**
     * Constructs a new FeatureKeyClient for a JIRA instance.
     *
     * @param environmentData The JIRA environment data
     */
    public FeatureKeyClient(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    /**
     * PUTs the feature key and enablement
     *
     * @param featureKey feature key
     * @param enabled    whether to enable or disable the dark feature
     * @throws com.sun.jersey.api.client.UniformInterfaceException if there's a problem enabling the dark feature
     */
    public void put(String featureKey, boolean enabled) throws UniformInterfaceException {
        featureFlagWithKey(featureKey).type(MediaType.APPLICATION_JSON_TYPE).put(new DarkFeature(enabled));
    }

    /**
     * PUTs the featureKey and enablement, returning a Response object.
     *
     * @param featureKey feature key
     * @param enabled    whether to enable or disable the dark feature
     * @return a Response
     */
    public Response putResponse(final String featureKey, final boolean enabled) {
        return toResponse(() -> featureFlagWithKey(featureKey).type(MediaType.APPLICATION_JSON_TYPE).put(ClientResponse.class, new DarkFeature(enabled)));
    }

    /**
     * GETs all the defined feature flags
     *
     * @return the dark feature
     * @throws com.sun.jersey.api.client.UniformInterfaceException if there's a problem enabling the dark feature
     */
    public Map<?, ?> get() throws UniformInterfaceException {
        return featureFlags().get(Map.class);
    }

    /**
     * GETs all the defined feature flags returning a Response object.
     *
     * @return a Response
     */
    public Response getResponse() {
        return toResponse(() -> featureFlags().get(ClientResponse.class));
    }

    /**
     * GETs the dark feature with the given key.
     *
     * @param featureKey the feature key
     * @return the dark feature
     * @throws com.sun.jersey.api.client.UniformInterfaceException if there's a problem enabling the dark feature
     */
    public DarkFeature get(String featureKey) throws UniformInterfaceException {
        return featureFlagWithKey(featureKey).get(DarkFeature.class);
    }

    /**
     * GETs the featureKey, returning a Response object.
     *
     * @param featureKey feature key
     * @return a Response
     */
    public Response getResponse(final String featureKey) {
        return toResponse(() -> featureFlagWithKey(featureKey).get(ClientResponse.class));
    }

    /**
     * Returns a WebResource for site dark features.
     *
     * @return a WebResource
     */
    protected WebResource featureFlags() {
        return createResourceInternal().path("featureFlag");
    }

    /**
     * Returns a WebResource for site dark features for the given key.
     *
     * @param featureKey feature key
     * @return a WebResource
     */
    protected WebResource featureFlagWithKey(String featureKey) {
        return featureFlags().path(featureKey);
    }
}
