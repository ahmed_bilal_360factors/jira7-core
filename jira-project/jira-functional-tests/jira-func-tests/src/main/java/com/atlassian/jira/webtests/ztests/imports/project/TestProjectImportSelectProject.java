package com.atlassian.jira.webtests.ztests.imports.project;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY_KEY;

/**
 * Tests the 2nd screen of the project import wizard.
 *
 * @since v3.13
 */
@WebTest({Category.FUNC_TEST, Category.PROJECT_IMPORT})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestProjectImportSelectProject.xml")
public class TestProjectImportSelectProject extends BaseJiraProjectImportFuncTest {
    private File tempFile;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        final String fileName = "TestProjectImportSelectProject_out.xml";
        logger.log(String.format("Using temporary file '%s'.", fileName));
        final File zipFile = this.administration.exportDataToFile(fileName);
        tempFile = copyFileToJiraImportDirectory(zipFile);
    }

    @After
    public void tearDownTest() {
        if (!tempFile.delete()) {
            logger.log(String.format("Unable to delete file '%s'.", tempFile.getAbsoluteFile().getPath()));
        }
    }

    @Test
    public void testJumpToSelectProjectScreen() {
        tester.gotoPage("/secure/admin/ProjectImportSelectProject!default.jspa");
        tester.assertTextPresent("There are no projects to display. Perhaps your session has timed out, please restart the project import wizard.");
    }

    @Test
    public void testValidationErrors() {
        final Long projectId = backdoor.project().getProjectId(PROJECT_MONKEY_KEY);
        tester.gotoPage("/secure/project/EditProjectLeadAndDefaultAssignee!default.jspa?pid=" + projectId);

        tester.selectOption("assigneeType", "Project Lead");
        tester.submit("Update");

        // Now toggle the allow unassigned issues setting
        this.administration.generalConfiguration().setAllowUnassignedIssues(false);

        // Lets try our import
        this.navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_IMPORT);

        // Get to the project select page
        tester.setWorkingForm("project-import");
        tester.assertTextPresent("Project Import: Select Backup File");
        tester.setFormElement("backupPath", tempFile.getName());
        tester.submit();

        advanceThroughWaitingPage();
        tester.assertTextPresent("Project Import: Select Project to Import");

        tester.selectOption("projectKey", "homosapien");
        tester.submit("Next");

        // There should be errors on this page
        textAssertions.assertTextPresentHtmlEncoded("The existing project with key 'HSP' contains '26' issues. You can not import a backup project into a project that contains existing issues.");
        textAssertions.assertTextPresentHtmlEncoded("The existing project with key 'HSP' contains '3' versions. You can not import a backup project into a project that contains existing versions.");
        textAssertions.assertTextPresentHtmlEncoded("The existing project with key 'HSP' contains '3' components. You can not import a backup project into a project that contains existing components.");

        // try to import the monkey project and get the unassgined error
        tester.selectOption("projectKey", "monkey");
        tester.submit("Next");

        textAssertions.assertTextPresentHtmlEncoded("The backup project 'monkey' has 'unassigned' default assignee, but this JIRA instance does not allow unassigned issues.");
    }

    @Test
    public void testCustomFieldsWrongVersion() throws IOException {
        // modify the XML file on disk so that the custom field versions are different
        modifyCustomFieldPluginVersion();

        // Lets try our import
        this.navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_IMPORT);

        // Get to the project select page
        tester.setWorkingForm("project-import");
        tester.assertTextPresent("Project Import: Select Backup File");
        tester.setFormElement("backupPath", tempFile.getName());
        tester.submit();

        advanceThroughWaitingPage();
        tester.assertTextPresent("Project Import: Select Project to Import");

        tester.selectOption("projectKey", "monkey");
        tester.submit("Next");

        // There should be errors on this page
        textAssertions.assertTextPresentHtmlEncoded("The backup project 'monkey' requires custom field named 'text cf' with full key 'com.atlassian.jira.plugin.system.customfieldtypes:textarea'. In the current instance of JIRA the plugin is at version '1.0', but in the backup it is at version '10.0'.");
        textAssertions.assertTextPresentHtmlEncoded("The backup project 'monkey' requires custom field named 'target_milestone' with full key 'com.atlassian.jira.plugin.system.customfieldtypes:select'. In the current instance of JIRA the plugin is at version '1.0', but in the backup it is at version '10.0'.");
        textAssertions.assertTextPresentHtmlEncoded("The backup project 'monkey' requires custom field named 'fgsdfgsd' with full key 'com.atlassian.jira.plugin.system.customfieldtypes:textfield'. In the current instance of JIRA the plugin is at version '1.0', but in the backup it is at version '10.0'.");
        textAssertions.assertTextPresentHtmlEncoded("The backup project 'monkey' requires custom field named 'number cf' with full key 'com.atlassian.jira.plugin.system.customfieldtypes:float'. In the current instance of JIRA the plugin is at version '1.0', but in the backup it is at version '10.0'.");
    }

    @Test
    public void testCustomFieldsPluginDoesNotExist() throws IOException {
        // modify the XML file on disk so that the custom field versions are different
        deleteCustomFieldPluginVersion();

        // Lets try our import
        this.navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_IMPORT);

        // Get to the project select page
        tester.setWorkingForm("project-import");
        tester.assertTextPresent("Project Import: Select Backup File");
        tester.setFormElement("backupPath", tempFile.getName());
        tester.submit();

        advanceThroughWaitingPage();
        tester.assertTextPresent("Project Import: Select Project to Import");

        tester.selectOption("projectKey", "monkey");
        tester.submit("Next");

        // There should be errors on this page
        textAssertions.assertTextPresentHtmlEncoded("The backup project 'monkey' requires custom field named 'fgsdfgsd' with full key 'com.atlassian.jira.plugin.system.customfieldtypes:textfield'. In the current instance of JIRA the plugin is at version '1.0', but this custom field was not installed in the backup data. You may want to create an XML backup with this version of the plugin installed.");
        textAssertions.assertTextPresentHtmlEncoded("The backup project 'monkey' requires custom field named 'number cf' with full key 'com.atlassian.jira.plugin.system.customfieldtypes:float'. In the current instance of JIRA the plugin is at version '1.0', but this custom field was not installed in the backup data. You may want to create an XML backup with this version of the plugin installed.");
        textAssertions.assertTextPresentHtmlEncoded("The backup project 'monkey' requires custom field named 'target_milestone' with full key 'com.atlassian.jira.plugin.system.customfieldtypes:select'. In the current instance of JIRA the plugin is at version '1.0', but this custom field was not installed in the backup data. You may want to create an XML backup with this version of the plugin installed.");
        textAssertions.assertTextPresentHtmlEncoded("The backup project 'monkey' requires custom field named 'text cf' with full key 'com.atlassian.jira.plugin.system.customfieldtypes:textarea'. In the current instance of JIRA the plugin is at version '1.0', but this custom field was not installed in the backup data. You may want to create an XML backup with this version of the plugin installed.");
    }

    @Test
    public void testParseExceptionInJIRAData() throws Exception {
        final File file = importAndExportBackupAndSetupCurrentInstance("TestProjectImportParseExceptionScreen2.xml", "TestProjectImportSummaryNoCustomFields2.xml");
        try {
            // Lets try our import
            this.navigation.gotoAdminSection(Navigation.AdminSection.PROJECT_IMPORT);

            // Get to the project select page
            tester.setWorkingForm("project-import");
            tester.assertTextPresent("Project Import: Select Backup File");
            tester.setFormElement("backupPath", file.getAbsolutePath());
            tester.submit();

            advanceThroughWaitingPage();
            tester.assertTextPresent("Project Import: Select Project to Import");

            tester.selectOption("projectKey", "monkey");
            tester.submit("Next");

            advanceThroughWaitingPage();

            // Make sure we see the parse error on the screen
            tester.assertTextPresent("Project Import: Select Project to Import");
            tester.assertTextPresent("There was a problem parsing the backup XML file at");
            // We don't assert the actual pathname because absolute pathing has platform dependent weirdness.
            tester.assertTextPresent("A worklog must have an issue id specified.");
        } finally {
            file.delete();
        }

    }

    private void modifyCustomFieldPluginVersion() throws IOException {
        final ZipFile zip = new ZipFile(tempFile);
        String ofbizData = getZipEntryAsString(zip, "entities.xml");
        ofbizData = StringUtils.replace(ofbizData, "<PluginVersion id=\"10002\" name=\"Custom Field Types &amp; Searchers\" key=\"com.atlassian.jira.plugin.system.customfieldtypes\" version=\"1.0\"/>", "<PluginVersion id=\"10002\" name=\"Custom Field Types &amp; Searchers\" key=\"com.atlassian.jira.plugin.system.customfieldtypes\" version=\"10.0\"/>");
        final String aoData = getZipEntryAsString(zip, "activeobjects.xml");

        rebuildZipFile(ofbizData, aoData);
    }

    private void deleteCustomFieldPluginVersion() throws IOException {
        final ZipFile zip = new ZipFile(tempFile);
        String ofbizData = getZipEntryAsString(zip, "entities.xml");
        ofbizData = StringUtils.replace(ofbizData, "<PluginVersion id=\"10002\" name=\"Custom Field Types &amp; Searchers\" key=\"com.atlassian.jira.plugin.system.customfieldtypes\" version=\"1.0\"/>", "");
        final String aoData = getZipEntryAsString(zip, "activeobjects.xml");

        rebuildZipFile(ofbizData, aoData);
    }

    private void rebuildZipFile(final String ofbizData, final String aoData) throws IOException {
        final ZipOutputStream os = new ZipOutputStream(new FileOutputStream(tempFile));
        final Writer fw = new OutputStreamWriter(os);
        os.putNextEntry(new ZipEntry("entities.xml"));
        fw.write(ofbizData);
        fw.flush();
        os.closeEntry();
        os.putNextEntry(new ZipEntry("activeobjects.xml"));
        fw.write(aoData);
        fw.flush();
        os.closeEntry();
        os.close();
    }

    private String getZipEntryAsString(final ZipFile zip, final String entryName) throws IOException {
        final ZipEntry entry = zip.getEntry(entryName);
        final StringWriter stringWriter = new StringWriter();
        final InputStreamReader in = new InputStreamReader(zip.getInputStream(entry));
        copy(in, stringWriter, 1024 * 4);
        in.close();
        final String s = stringWriter.toString();
        stringWriter.close();
        return s;
    }

    private void copy(final Reader input, final Writer output, final int bufferSize)
            throws IOException {
        final char[] buffer = new char[bufferSize];
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
        }
    }

}
