package com.atlassian.jira.webtests.ztests.navigator.jql;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.meterware.httpunit.WebTable;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * Test date fields such as Created.
 */
@WebTest({Category.FUNC_TEST, Category.JQL})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestDateField.xml")
public class TestDateField extends BaseJiraFuncTest {

    @Before
    public void setUp() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    /**
     * Confirm that Look and Feel date formats are being html encoded when rendered.
     * JRA-32516
     *
     * @throws Exception
     */
    @Test
    public void testHtmlEncodedDateFormat() throws Exception {
        navigation.issueNavigator().createSearch("");
        WebTable issueTable = tester.getDialog().getWebTableBySummaryOrId("issuetable");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 8, "10/Aug/09 <script></script>");
    }
}
