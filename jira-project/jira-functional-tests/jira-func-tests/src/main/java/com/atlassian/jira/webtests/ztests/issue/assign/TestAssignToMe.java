package com.atlassian.jira.webtests.ztests.issue.assign;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_FULLNAME;
import static com.atlassian.jira.permission.ProjectPermissions.ASSIGN_ISSUES;
import static com.atlassian.jira.webtests.Groups.DEVELOPERS;
import static com.atlassian.jira.webtests.Groups.USERS;

/**
 * <p> Responsible for verifying the behaviour of the assign to me operation. </p> <p> <p> Assign to Me should assign an
 * issue to the user that's currently logged in. </p>
 */
@WebTest({Category.FUNC_TEST, Category.BROWSING})
@Restore("TestAssignToMe.xml")
public class TestAssignToMe extends BaseJiraFuncTest {

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        navigation.login(ADMIN_USERNAME);
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
    }

    /**
     * Verifies that the assign to me operation works when the current logged in user is the reporter of the issue.
     */
    @Test
    public void testAssignToMeWhenTheUserReportedTheIssue() {
        navigation.issue().gotoIssue("MKY-1");
        textAssertions.assertTextPresent(locator.id("assignee-val"), FRED_FULLNAME);
        textAssertions.assertTextPresent(locator.id("reporter-val"), ADMIN_FULLNAME);

        tester.clickLink("assign-to-me");

        textAssertions.assertTextPresent(locator.id("assignee-val"), ADMIN_FULLNAME);
        textAssertions.assertTextPresent(locator.id("reporter-val"), ADMIN_FULLNAME);

        tester.assertLinkNotPresent("assign-to-me");
    }

    /**
     * Verifies that the assign to me operation works correctly for usernames which contain non-alphanumeric characters.
     * e.g. #
     */
    @Test
    public void testAssignToMeWhenUserNamesContainNonAlphaNumericCharacters() {
        navigation.logout();
        navigation.login("#test");

        navigation.issue().gotoIssue("MKY-1");
        textAssertions.assertTextPresent(locator.id("assignee-val"), FRED_FULLNAME);
        textAssertions.assertTextPresent(locator.id("reporter-val"), ADMIN_FULLNAME);

        tester.clickLink("assign-to-me");

        textAssertions.assertTextPresent(locator.id("assignee-val"), "#test");
        tester.assertLinkNotPresent("assign-to-me");
        textAssertions.assertTextPresent(locator.id("reporter-val"), ADMIN_FULLNAME);

        navigation.logout();
        navigation.login(ADMIN_USERNAME);
    }

    @Test
    public void testAssignToMeNotVisibleIfNoAssignIssuePermission() {
        navigation.issue().gotoIssue("MKY-1");
        tester.assertLinkPresent("assign-to-me");

        backdoor.permissionSchemes().removeGroupPermission(0, ASSIGN_ISSUES, DEVELOPERS);
        navigation.issue().gotoIssue("MKY-1");
        tester.assertLinkNotPresent("assign-to-me");
    }

    @Test
    public void testAssignToMeNotVisibleIfNoAssignableUserPermission() {
        navigation.issue().gotoIssue("MKY-1");
        tester.assertLinkPresent("assign-to-me");

        backdoor.permissionSchemes().removeGroupPermission(0, ProjectPermissions.ASSIGNABLE_USER, DEVELOPERS);
        backdoor.permissionSchemes().removeGroupPermission(0, ProjectPermissions.ASSIGNABLE_USER, USERS);
        navigation.issue().gotoIssue("MKY-1");
        tester.assertLinkNotPresent("assign-to-me");
    }

    @Test
    public void testAssignToMeNotVisibleIfIssueIsNotEditable() {
        navigation.issue().gotoIssue("MKY-1");
        tester.assertLinkPresent("assign-to-me");

        navigation.issue().closeIssue("MKY-1", "Fixed", "Fixed");

        navigation.issue().gotoIssue("MKY-1");
        tester.assertLinkNotPresent("assign-to-me");
    }

    @Test
    public void testAssignToMeNotVisibleIfAnonymousUser() {
        navigation.issue().gotoIssue("MKY-1");
        tester.assertLinkPresent("assign-to-me");

        navigation.logout();
        navigation.issue().gotoIssue("MKY-1");
        textAssertions.assertTextPresent(locator.id("assignee-val"), FRED_FULLNAME);
        tester.assertLinkNotPresent("assign-to-me");
    }
}