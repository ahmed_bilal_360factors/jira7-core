package com.atlassian.jira.webtests.ztests.navigator.jql;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.navigator.ContainsIssueKeysCondition;
import com.atlassian.jira.functest.framework.navigator.NumberOfIssuesCondition;
import com.atlassian.jira.functest.framework.navigator.SearchResultsCondition;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_FULLNAME;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Format;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Mode;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Unit;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * Tests the interpretations of different literals (operands) for each system field clause.
 * <p/>
 * This does not test resolution of domain objects based on permissions. It does however test resolution of domain
 * objects based on ids vs names.
 *
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.JQL})
@LoginAs(user = ADMIN_USERNAME)
public class TestSystemFieldLiterals extends BaseJiraFuncTest {

    private static final String ORIGINAL_ESTIMATE = "originalEstimate";
    private static final String REMAINING_ESTIMATE = "remainingEstimate";
    private static final String TIME_SPENT = "timeSpent";

    @Inject
    private Administration administration;

    @Test
    @Restore("TestSystemFieldLiteralsVersion.xml")
    public void testAffectedVersion() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "affectedVersion";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'New Version 1'", new Result("HSP-1"));
        matrix.put("'10000'", new Result("HSP-2"));
        matrix.put("'10001'", new Result("HSP-2"));
        matrix.put("'Bad String'", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("''", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("10000", new Result("HSP-1"));
        matrix.put("10001", new Result("HSP-2"));
        matrix.put("666", new Result(ErrorType.ID_NOT_FOUND));
        matrix.put("EMPTY", new Result("HSP-3"));

        _testLiteralMatrix(fieldName, matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsAssignee.xml")
    public void testAssignee() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "assignee";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("administrator", new Result("HSP-1"));
        matrix.put(ADMIN_USERNAME, new Result("HSP-2"));
        matrix.put("'" + FRED_FULLNAME + "'", new Result("HSP-3"));
        matrix.put("'Bad String'", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("''", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("EMPTY", new Result("HSP-4"));
        matrix.put("12345", new Result("HSP-5"));
        matrix.put("'12345'", new Result("HSP-5"));
        matrix.put("666", new Result(ErrorType.ID_NOT_FOUND));

        _testLiteralMatrix(fieldName, matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsCategory.xml")
    public void testCategory() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "category";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'Category1'", new Result("CAT-1"));
        matrix.put("'10000'", new Result("CATTWO-1"));
        matrix.put("'10001'", new Result("CATTWO-1"));
        matrix.put("'Bad String'", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("''", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("10000", new Result("CAT-1"));
        matrix.put("10001", new Result("CATTWO-1"));
        matrix.put("666", new Result(ErrorType.ID_NOT_FOUND));
        matrix.put("EMPTY", new Result("NOT-1"));

        _testLiteralMatrix(fieldName, matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsComment.xml")
    public void testComment() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "comment";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'Test'", new Result("COM-1"));
        matrix.put("'-Test'", new Result("COM-2"));
        matrix.put("'123456'", new Result("COM-2"));
        matrix.put("'?'", new Result(ErrorType.INVALID_START_CHAR));
        matrix.put("'BAD +'", new Result(ErrorType.CANT_PARSE_QUERY));
        matrix.put("''", new Result(ErrorType.EMPTY_STRING_NOT_SUPPORTED));
        matrix.put("Test", new Result("COM-1"));
        matrix.put("123456", new Result("COM-2"));
        matrix.put("EMPTY", new Result(ErrorType.EMPTY_NOT_SUPPORTED));

        _testLiteralMatrix(fieldName, "~", matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsComponent.xml")
    public void testComponent() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "component";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'Component 1'", new Result("CMP-1"));
        matrix.put("'10010'", new Result("CMP-2"));
        matrix.put("'10011'", new Result("CMP-2"));
        matrix.put("'Bad String'", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("''", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("10010", new Result("CMP-1"));
        matrix.put("10011", new Result("CMP-2"));
        matrix.put("EMPTY", new Result("CMP-3"));
        matrix.put("666", new Result(ErrorType.ID_NOT_FOUND));

        _testLiteralMatrix(fieldName, matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsCreated.xml")
    public void testCreated() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "created";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("1242604510000", new Result("CRE-3", "CRE-2", "CRE-1"));
        matrix.put("'1d'", new Result("CRE-3", "CRE-2", "CRE-1"));
        matrix.put("'dd'", new Result(ErrorType.INVALID_DATE_FORMAT));
        matrix.put("'2009/05/13 18:50'", new Result("CRE-1"));
        matrix.put("'2009-05-13 18:50'", new Result("CRE-1"));
        matrix.put("'2009/05/14'", new Result("CRE-1"));
        matrix.put("'2009-05-14'", new Result("CRE-1"));
        matrix.put("'14/May/09'", new Result(ErrorType.INVALID_DATE_FORMAT));
        matrix.put("'09/1/1'", new Result(ErrorType.INVALID_DATE_FORMAT));
        matrix.put("'2009/05/14 bad'", new Result(ErrorType.INVALID_DATE_FORMAT));
        matrix.put("'2010/02/35'", new Result(ErrorType.INVALID_DATE_FORMAT));
        matrix.put("''", new Result(ErrorType.INVALID_DATE_FORMAT));

        _testLiteralMatrix(fieldName, "<", matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsDescription.xml")
    public void testDescription() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "description";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'Test'", new Result("HSP-1"));
        matrix.put("'-Test'", new Result("HSP-2"));
        matrix.put("'123456'", new Result("HSP-2"));
        matrix.put("'?'", new Result(ErrorType.INVALID_START_CHAR));
        matrix.put("'BAD +'", new Result(ErrorType.CANT_PARSE_QUERY));
        matrix.put("''", new Result(ErrorType.EMPTY_STRING_NOT_SUPPORTED));
        matrix.put("Test", new Result("HSP-1"));
        matrix.put("123456", new Result("HSP-2"));
        matrix.put("EMPTY", new Result("HSP-3"));

        _testLiteralMatrix(fieldName, "~", matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsDueDate.xml")
    public void testDueDate() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "due";

        Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("1242204510000", new Result("HSP-1"));

        matrix.put("'dd'", new Result(ErrorType.INVALID_RELATIVE_DATE_FORMAT));
        matrix.put("'2009/05/13'", new Result("HSP-1"));
        matrix.put("'2009-05-13'", new Result("HSP-1"));
        matrix.put("'2009/05/14'", new Result("HSP-2"));
        matrix.put("'2009-05-14'", new Result("HSP-2"));
        matrix.put("'14/May/09'", new Result(ErrorType.INVALID_RELATIVE_DATE_FORMAT));
        matrix.put("''", new Result(ErrorType.INVALID_RELATIVE_DATE_FORMAT));

        _testLiteralMatrix(fieldName, "=", matrix);

        matrix = new LinkedHashMap<>();
        matrix.put("'1d'", new Result("HSP-3", "HSP-2", "HSP-1"));
        _testLiteralMatrix(fieldName, "<", matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsEnvironment.xml")
    public void testEnvironment() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "environment";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'Test'", new Result("HSP-1"));
        matrix.put("'-Test'", new Result("HSP-2"));
        matrix.put("'123456'", new Result("HSP-2"));
        matrix.put("'?'", new Result(ErrorType.INVALID_START_CHAR));
        matrix.put("'BAD +'", new Result(ErrorType.CANT_PARSE_QUERY));
        matrix.put("''", new Result(ErrorType.EMPTY_STRING_NOT_SUPPORTED));
        matrix.put("Test", new Result("HSP-1"));
        matrix.put("123456", new Result("HSP-2"));
        matrix.put("EMPTY", new Result("HSP-3"));

        _testLiteralMatrix(fieldName, "~", matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsVersion.xml")
    public void testFixVersion() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "fixVersion";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'New Version 1'", new Result("HSP-1"));
        matrix.put("'10000'", new Result("HSP-2"));
        matrix.put("'10001'", new Result("HSP-2"));
        matrix.put("'Bad String'", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("''", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("10000", new Result("HSP-1"));
        matrix.put("10001", new Result("HSP-2"));
        matrix.put("EMPTY", new Result("HSP-3"));
        matrix.put("666", new Result(ErrorType.ID_NOT_FOUND));

        _testLiteralMatrix(fieldName, matrix);
    }

    @Test
    @Restore("TestSystemFieldOperatorsIssue.xml")
    public void testIssue() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "issue";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'10000'", new Result(ErrorType.ISSUE_KEY_INVALID));
        matrix.put("''", new Result(ErrorType.ISSUE_KEY_INVALID));
        matrix.put("'HSP-2'", new Result("HSP-2"));
        matrix.put("10000", new Result("HSP-1"));
        matrix.put("10001", new Result("HSP-2"));
        matrix.put("666", new Result(ErrorType.ID_NOT_FOUND));
        matrix.put("EMPTY", new Result());

        _testLiteralMatrix(fieldName, matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsLevel.xml")
    public void testLevel() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "level";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'Level 1'", new Result("HSP-1"));
        matrix.put("'10000'", new Result("HSP-2"));
        matrix.put("'10010'", new Result("HSP-2"));
        matrix.put("'Bad String'", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("''", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("10000", new Result("HSP-1"));
        matrix.put("10010", new Result("HSP-2"));
        matrix.put("666", new Result(ErrorType.ID_NOT_FOUND));
        matrix.put("EMPTY", new Result("HSP-3"));

        _testLiteralMatrix(fieldName, matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsTime.xml")
    public void testTimeTracking() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'60'", new Result("HSP-1"));
        matrix.put("'2h'", new Result("HSP-2"));
        matrix.put("'0h'", new Result("HSP-5"));
        matrix.put("'hh'", new Result(ErrorType.INVALID_DURATION_FORMAT));
        matrix.put("''", new Result(ErrorType.INVALID_DURATION_FORMAT));
        matrix.put("240", new Result("HSP-3"));
        matrix.put("0", new Result("HSP-5"));
        matrix.put("'-240'", new Result(ErrorType.INVALID_DURATION_FORMAT));
        matrix.put("EMPTY", new Result("HSP-4"));
        //JRA-25572: 1d = 8 hrs as configured by time tracking.
        matrix.put("1d", new Result("HSP-7"));
        //JRA-25572: 1w = 40 hrs as configured by time tracking.
        matrix.put("1w", new Result("HSP-8"));
        _testLiteralMatrix(ORIGINAL_ESTIMATE, matrix);

        matrix = new LinkedHashMap<>();
        matrix.put("'40'", new Result("HSP-1"));
        matrix.put("'1h'", new Result("HSP-3", "HSP-2"));
        matrix.put("'0m'", new Result("HSP-5"));
        matrix.put("'hh'", new Result(ErrorType.INVALID_DURATION_FORMAT));
        matrix.put("''", new Result(ErrorType.INVALID_DURATION_FORMAT));
        matrix.put("60", new Result("HSP-3", "HSP-2"));
        matrix.put("0", new Result("HSP-5"));
        matrix.put("'-60'", new Result(ErrorType.INVALID_DURATION_FORMAT));
        matrix.put("EMPTY", new Result("HSP-4"));
        //JRA-25572: 1d = 8 hrs as configured by time tracking.
        matrix.put("1d", new Result("HSP-7"));
        //JRA-25572: 1w = 40 hrs as configured by time tracking.
        matrix.put("1w", new Result("HSP-8"));
        _testLiteralMatrix(REMAINING_ESTIMATE, matrix);

        matrix = new LinkedHashMap<>();
        matrix.put("'20'", new Result("HSP-1"));
        matrix.put("'1h'", new Result("HSP-2"));
        matrix.put("'0d'", new Result());
        matrix.put("'hh'", new Result(ErrorType.INVALID_DURATION_FORMAT));
        matrix.put("''", new Result(ErrorType.INVALID_DURATION_FORMAT));
        matrix.put("180", new Result("HSP-3"));
        matrix.put("0", new Result());
        matrix.put("'-180'", new Result(ErrorType.INVALID_DURATION_FORMAT));
        matrix.put("EMPTY", new Result("HSP-4"));
        //JRA-25572: 1d = 8 hrs as configured by time tracking.
        matrix.put("1d", new Result("HSP-7"));
        //JRA-25572: 1w = 40 hrs as configured by time tracking.
        matrix.put("1w", new Result("HSP-8"));
        _testLiteralMatrix(TIME_SPENT, matrix);

        matrix = new LinkedHashMap<>();
        matrix.put("'33'", new Result("HSP-1"));
        matrix.put("'33.3'", new Result(ErrorType.INVALID_INTEGER_FORMAT));
        matrix.put("''", new Result(ErrorType.INVALID_INTEGER_FORMAT));
        matrix.put("0", new Result());
        matrix.put("EMPTY", new Result("HSP-4"));
        _testLiteralMatrix("workRatio", matrix);

        //JRA-25572: We are going to change time tracking configuration to 1d = 6d; 1w = 2d; default unit = hours.
        administration.timeTracking().disable();
        administration.timeTracking().enable("6", "2", Format.PRETTY, Unit.HOUR, Mode.MODERN);
        administration.reIndex();

        matrix = new LinkedHashMap<>();
        matrix.put("1d", new Result("HSP-6"));
        matrix.put("1w", new Result("HSP-10"));
        matrix.put("8", new Result("HSP-7"));

        _testLiteralMatrix(TIME_SPENT, matrix);
        _testLiteralMatrix(ORIGINAL_ESTIMATE, matrix);
        _testLiteralMatrix(REMAINING_ESTIMATE, matrix);
    }

    @Test
    @Restore("TestSystemFieldOperatorsParent.xml")
    public void testParent() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "parent";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'10000'", new Result(ErrorType.ISSUE_KEY_INVALID));
        matrix.put("''", new Result(ErrorType.ISSUE_KEY_INVALID));
        matrix.put("'HSP-2'", new Result("HSP-4"));
        matrix.put("10000", new Result("HSP-3"));
        matrix.put("10001", new Result("HSP-4"));
        matrix.put("666", new Result(ErrorType.ID_NOT_FOUND));
        matrix.put("EMPTY", new Result(ErrorType.EMPTY_NOT_SUPPORTED));

        _testLiteralMatrix(fieldName, matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsPriority.xml")
    public void testPriority() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "priority";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'Blocker'", new Result("HSP-1"));
        matrix.put("'1'", new Result("HSP-2", "HSP-1"));
        matrix.put("'2'", new Result("HSP-2"));
        matrix.put("'Bad String'", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("''", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("1", new Result("HSP-1"));
        matrix.put("2", new Result("HSP-2"));
        matrix.put("666", new Result(ErrorType.ID_NOT_FOUND));
        matrix.put("EMPTY", new Result());

        _testLiteralMatrix(fieldName, matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsProject.xml")
    public void testProject() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "project";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'10010'", new Result("NUM-1"));
        matrix.put("'10011'", new Result("NUM-1"));
        matrix.put("'DUP'", new Result("DUP-1"));
        matrix.put("'HSP'", new Result("HSP-1"));
        matrix.put("'homosapien'", new Result("HSP-1"));
        matrix.put("'Bad String'", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("''", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("10010", new Result("HSP-1"));
        matrix.put("10011", new Result("NUM-1"));
        matrix.put("666", new Result(ErrorType.ID_NOT_FOUND));
        matrix.put("EMPTY", new Result());

        _testLiteralMatrix(fieldName, matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsAssignee.xml")
    public void testReporter() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "reporter";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("administrator", new Result("HSP-1"));
        matrix.put(ADMIN_USERNAME, new Result("HSP-2"));
        matrix.put("'" + FRED_FULLNAME + "'", new Result("HSP-3"));
        matrix.put("'Bad String'", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("''", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("EMPTY", new Result("HSP-4"));
        matrix.put("12345", new Result("HSP-5"));
        matrix.put("666", new Result(ErrorType.ID_NOT_FOUND));
        matrix.put("'12345'", new Result("HSP-5"));

        _testLiteralMatrix(fieldName, matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsResolution.xml")
    public void testResolution() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "resolution";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'Fixed'", new Result("HSP-1"));
        matrix.put("'1'", new Result("HSP-2", "HSP-1"));
        matrix.put("'3'", new Result("HSP-3"));
        matrix.put("'Bad String'", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("''", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("1", new Result("HSP-1"));
        matrix.put("2", new Result("HSP-2"));
        matrix.put("666", new Result(ErrorType.ID_NOT_FOUND));
        matrix.put("EMPTY", new Result("HSP-4"));

        _testLiteralMatrix(fieldName, matrix);

        // test the correctness of results using the "unresolved" operand

        assertSearchResults("resolution = unresolved", "HSP-4");
        assertSearchResults("resolution in (unresolved)", "HSP-4");
        assertSearchResults("resolution in (unresolved, Fixed)", "HSP-4", "HSP-1");
        assertSearchResults("resolution != unresolved", "HSP-3", "HSP-2", "HSP-1");
        assertSearchResults("resolution not in (unresolved, Fixed)", "HSP-3", "HSP-2");
        assertSearchAndErrorMessagePresent("resolution = '\"unresolved\"'");

        // add "unresolved"
        administration.resolutions().addResolution("unRESOLVED");

        assertSearchResults("resolution = unresolved", "HSP-4");
        assertSearchResults("resolution in (unresolved)", "HSP-4");
        assertSearchResults("resolution in (unresolved, Fixed)", "HSP-4", "HSP-1");
        assertSearchResults("resolution != unresolved", "HSP-3", "HSP-2", "HSP-1");
        assertSearchResults("resolution not in (unresolved)", "HSP-3", "HSP-2", "HSP-1");
        assertSearchResults("resolution not in (unresolved, Fixed)", "HSP-3", "HSP-2");

        assertSearchResults("resolution = '\"unresolved\"'");
        assertSearchResults("resolution in ('\"unresolved\"')");
        assertSearchResults("resolution in ('\"unresolved\"', Fixed)", "HSP-1");
        assertSearchResults("resolution != '\"unresolved\"'", "HSP-3", "HSP-2", "HSP-1");
        assertSearchResults("resolution not in ('\"unresolved\"')", "HSP-3", "HSP-2", "HSP-1");
        assertSearchResults("resolution not in ('\"unresolved\"', Fixed)", "HSP-3", "HSP-2");

        // set the resolution of an issue to "unresolved"
        String issue = navigation.issue().createIssue("homosapien", "Bug", "test");
        navigation.issue().resolveIssue(issue, "unRESOLVED", "test");

        assertSearchResults("resolution = unresolved", "HSP-4");
        assertSearchResults("resolution in (unresolved)", "HSP-4");
        assertSearchResults("resolution in (unresolved, Fixed)", "HSP-4", "HSP-1");
        assertSearchResults("resolution != unresolved", issue, "HSP-3", "HSP-2", "HSP-1");
        assertSearchResults("resolution not in (unresolved)", issue, "HSP-3", "HSP-2", "HSP-1");
        assertSearchResults("resolution not in (unresolved, Fixed)", issue, "HSP-3", "HSP-2");

        assertSearchResults("resolution = '\"unresolved\"'", issue);
        assertSearchResults("resolution in ('\"unresolved\"')", issue);
        assertSearchResults("resolution in ('\"unresolved\"', Fixed)", issue, "HSP-1");
        assertSearchResults("resolution != '\"unresolved\"'", "HSP-3", "HSP-2", "HSP-1");
        assertSearchResults("resolution not in ('\"unresolved\"')", "HSP-3", "HSP-2", "HSP-1");
        assertSearchResults("resolution not in ('\"unresolved\"', Fixed)", "HSP-3", "HSP-2");

        // add "'unresolved'"
        assertSearchAndErrorMessagePresent("resolution > unresolved");
        assertSearchAndErrorMessagePresent("resolution >= unresolved");
        assertSearchAndErrorMessagePresent("resolution < unresolved");
        assertSearchAndErrorMessagePresent("resolution <= unresolved");
        assertSearchAndErrorMessagePresent("resolution = \"\\\"\\\"unresolved\\\"\\\"\"");

        administration.resolutions().addResolution("\"unresolved\"");

        assertSearchResults("resolution = \"\\\"\\\"unresolved\\\"\\\"\"");

        issue = navigation.issue().createIssue("homosapien", "Bug", "test");
        navigation.issue().resolveIssue(issue, "\"unresolved\"", "test");

        assertSearchResults("resolution = \"\\\"\\\"unresolved\\\"\\\"\"", issue);
    }

    @Test
    @Restore("TestSystemFieldOperatorsResolution.xml")
    public void testResolutionDate() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "resolved";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("1242604510000", new Result("HSP-3", "HSP-2", "HSP-1"));
        matrix.put("'1d'", new Result("HSP-3", "HSP-2", "HSP-1"));
        matrix.put("'dd'", new Result(ErrorType.INVALID_DATE_FORMAT));
        matrix.put("'2009/05/13 18:50'", new Result("HSP-1"));
        matrix.put("'2009-05-13 18:50'", new Result("HSP-1"));
        matrix.put("'2009/05/14'", new Result("HSP-1"));
        matrix.put("'2009-05-14'", new Result("HSP-1"));
        matrix.put("'14/May/09'", new Result(ErrorType.INVALID_DATE_FORMAT));
        matrix.put("'09/1/1'", new Result(ErrorType.INVALID_DATE_FORMAT));
        matrix.put("'2009/05/14 bad'", new Result(ErrorType.INVALID_DATE_FORMAT));
        matrix.put("'2010/02/35'", new Result(ErrorType.INVALID_DATE_FORMAT));
        matrix.put("''", new Result(ErrorType.INVALID_DATE_FORMAT));

        _testLiteralMatrix(fieldName, "<", matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsSavedFilter.xml")
    public void testSavedFilter() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "savedFilter";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'10000'", new Result("HSP-2"));
        matrix.put("'10001'", new Result("HSP-2"));
        matrix.put("'Bugs'", new Result("HSP-1"));
        matrix.put("'Bad String'", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("''", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("10000", new Result("HSP-1"));
        matrix.put("10001", new Result("HSP-2"));
        matrix.put("999", new Result("HSP-3"));
        matrix.put("666", new Result(ErrorType.ID_NOT_FOUND));
        matrix.put("EMPTY", new Result(ErrorType.EMPTY_NOT_SUPPORTED));

        _testLiteralMatrix(fieldName, matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsStatus.xml")
    public void testStatus() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "status";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'1'", new Result("HSP-2", "HSP-1"));
        matrix.put("'3'", new Result("HSP-2"));
        matrix.put("'Open'", new Result("HSP-1"));
        matrix.put("'Bad String'", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("''", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("1", new Result("HSP-1"));
        matrix.put("3", new Result("HSP-2"));
        matrix.put("999", new Result("HSP-3"));
        matrix.put("666", new Result(ErrorType.ID_NOT_FOUND));
        matrix.put("EMPTY", new Result());

        _testLiteralMatrix(fieldName, matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsSummary.xml")
    public void testSummary() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "summary";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'OneTwoThree'", new Result("HSP-1"));
        matrix.put("'-OneTwoThree'", new Result("HSP-2"));
        matrix.put("'456'", new Result("HSP-2"));
        matrix.put("OneTwoThree", new Result("HSP-1"));
        matrix.put("456", new Result("HSP-2"));
        matrix.put("-456", new Result("HSP-1"));
        matrix.put("'?'", new Result(ErrorType.INVALID_START_CHAR));
        matrix.put("'BAD +'", new Result(ErrorType.CANT_PARSE_QUERY));
        matrix.put("''", new Result(ErrorType.EMPTY_STRING_NOT_SUPPORTED));
        matrix.put("EMPTY", new Result());

        _testLiteralMatrix(fieldName, "~", matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsSummary.xml")
    public void testText() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "text";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'OneTwoThree'", new Result("HSP-1"));
        matrix.put("'-OneTwoThree'", new Result("HSP-2"));
        matrix.put("'456'", new Result("HSP-2"));
        matrix.put("OneTwoThree", new Result("HSP-1"));
        matrix.put("456", new Result("HSP-2"));
        matrix.put("-456", new Result("HSP-1"));
        matrix.put("'?'", new Result(ErrorType.INVALID_START_CHAR));
        matrix.put("'BAD +'", new Result(ErrorType.CANT_PARSE_QUERY));
        matrix.put("''", new Result(ErrorType.EMPTY_STRING_NOT_SUPPORTED));
        matrix.put("EMPTY", new Result(ErrorType.EMPTY_NOT_SUPPORTED));

        _testLiteralMatrix(fieldName, "~", matrix);
    }

    @Test
    @Restore("TestSystemFieldLiteralsType.xml")
    public void testType() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "type";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'1'", new Result("HSP-2", "HSP-1"));
        matrix.put("'2'", new Result("HSP-2"));
        matrix.put("'Bug'", new Result("HSP-1"));
        matrix.put("'Bad String'", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("''", new Result(ErrorType.NAME_NOT_FOUND));
        matrix.put("1", new Result("HSP-1"));
        matrix.put("2", new Result("HSP-2"));
        matrix.put("999", new Result("HSP-3"));
        matrix.put("666", new Result(ErrorType.ID_NOT_FOUND));
        matrix.put("EMPTY", new Result());

        _testLiteralMatrix(fieldName, matrix);
    }

    @Test
    @Restore("TestSystemFieldOperatorsResolution.xml")
    public void testUpdated() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "updated";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("1242604510000", new Result("HSP-4", "HSP-3", "HSP-2", "HSP-1"));
        matrix.put("'1d'", new Result("HSP-4", "HSP-3", "HSP-2", "HSP-1"));
        matrix.put("'dd'", new Result(ErrorType.INVALID_DATE_FORMAT));
        matrix.put("'2009/05/13 18:50'", new Result("HSP-1"));
        matrix.put("'2009-05-13 18:50'", new Result("HSP-1"));
        matrix.put("'2009/05/14'", new Result("HSP-1"));
        matrix.put("'2009-05-14'", new Result("HSP-1"));
        matrix.put("'14/May/09'", new Result(ErrorType.INVALID_DATE_FORMAT));
        matrix.put("'09/1/1'", new Result(ErrorType.INVALID_DATE_FORMAT));
        matrix.put("'2009/05/14 bad'", new Result(ErrorType.INVALID_DATE_FORMAT));
        matrix.put("'2010/02/35'", new Result(ErrorType.INVALID_DATE_FORMAT));
        matrix.put("''", new Result(ErrorType.INVALID_DATE_FORMAT));

        _testLiteralMatrix(fieldName, "<", matrix);
    }

    @Test
    @Restore("TestSystemFieldOperatorsVotes.xml")
    public void testVotes() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        final String fieldName = "votes";

        final Map<String, Result> matrix = new LinkedHashMap<>();
        matrix.put("'0'", new Result("HSP-3"));
        matrix.put("'1'", new Result("HSP-1"));
        matrix.put("'-3'", new Result(ErrorType.INVALID_VOTES_FORMAT));
        matrix.put("''", new Result(ErrorType.INVALID_VOTES_FORMAT));
        matrix.put("2", new Result("HSP-2"));
        matrix.put("-3", new Result(ErrorType.INVALID_VOTES_FORMAT_WITH_QUOTES));
        matrix.put("EMPTY", new Result(ErrorType.INVALID_VOTES_FORMAT_WITH_QUOTES));

        _testLiteralMatrix(fieldName, matrix);
    }

    private void _testLiteralMatrix(final String fieldName, final Map<String, Result> matrix) {
        _testLiteralMatrix(fieldName, "=", matrix);
    }

    private void _testLiteralMatrix(final String fieldName, final String operator, final Map<String, Result> matrix) {
        for (Map.Entry<String, Result> entry : matrix.entrySet()) {
            final String operand = entry.getKey();
            final String jqlQuery = String.format("%s %s %s", fieldName, operator, operand);
            final Result result = entry.getValue();

            if (result.issueKeys == null && result.errorType != null) {
                assertSearchAndErrorMessagePresent(jqlQuery);
            } else {
                assertSearchResults(jqlQuery, result.issueKeys);
            }
        }
    }

    /**
     * Executes a JQL query search and asserts the expected issue keys from the result set.
     *
     * @param jqlQuery  the query to execute
     * @param issueKeys the issue keys expected in the result set. Ordering is important, and the number of keys specified
     *                  will be asserted against the number of results
     */
    private void assertSearchResults(final String jqlQuery, final String... issueKeys) {
        final List<SearchResultsCondition> conditions = new ArrayList<>();
        conditions.add(new ContainsIssueKeysCondition(assertions.getTextAssertions(), issueKeys));
        conditions.add(new NumberOfIssuesCondition(assertions.getTextAssertions(), issueKeys.length));

        navigation.issueNavigator().createSearch(jqlQuery);
        assertions.getIssueNavigatorAssertions().assertSearchResults(conditions);
    }

    private void assertSearchAndErrorMessagePresent(final String jqlQuery) {
        navigation.issueNavigator().createSearch(jqlQuery);
        //Ideally this should be checking the actual error message, but this requires conversion to webdriver
        tester.assertElementNotPresent("issuetable");
    }

    private enum ErrorType {
        EMPTY_NOT_SUPPORTED(),
        EMPTY_STRING_NOT_SUPPORTED(),
        ISSUE_KEY_INVALID(),
        INVALID_VOTES_FORMAT(),
        INVALID_VOTES_FORMAT_WITH_QUOTES(),
        INVALID_INTEGER_FORMAT(),
        INVALID_DURATION_FORMAT(),
        INVALID_DATE_FORMAT(),
        INVALID_RELATIVE_DATE_FORMAT(),
        CANT_PARSE_QUERY(),
        INVALID_START_CHAR(),
        NAME_NOT_FOUND(),
        ID_NOT_FOUND()
    }

    private static class Result {
        private String[] issueKeys = null;
        private ErrorType errorType = null;

        private Result(final String... issueKeys) {
            this.issueKeys = issueKeys;
        }

        private Result(final ErrorType errorType) {
            this.errorType = errorType;
        }
    }
}
