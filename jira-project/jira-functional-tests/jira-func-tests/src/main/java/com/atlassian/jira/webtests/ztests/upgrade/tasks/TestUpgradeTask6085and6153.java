package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.rule.JiraRestClientSupplier;
import com.atlassian.jira.rest.client.api.domain.IssuelinksType;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.RENAME_USER;
import static com.atlassian.jira.functest.framework.suite.Category.UPGRADE_TASKS;
import static com.atlassian.jira.functest.matcher.IssueLinksTypeMatcher.issueLinkType;
import static org.junit.Assert.assertThat;

/**
 * @since v6.0
 */
@WebTest({FUNC_TEST, RENAME_USER, UPGRADE_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUpgradeTask6085and6153 extends BaseJiraFuncTest {

    @Rule
    public JiraRestClientSupplier jiraRestClientSupplier = new JiraRestClientSupplier(this::getEnvironmentData);

    @Inject
    private Administration administration;

    @Test
    public void testLegacyDirectionPropertyTrueIfClonersSwapped() {
        administration.restoreDataWithBuildNumber("ClonersSwapped_Post5.2.6.xml", 855);
        final Iterable<IssuelinksType> issuelinksTypes = jiraRestClientSupplier.get().getMetadataClient().getIssueLinkTypes().claim();
        assertThat(issuelinksTypes, Matchers.contains(issueLinkType("Cloners", "is cloned by", "clones")));
    }

    @Test
    public void testLegacyDirectionPropertyFalseIfClonersChanged() {
        administration.restoreDataWithBuildNumber("ClonersModified_Post5.2.6.xml", 855);
        final Iterable<IssuelinksType> issuelinksTypes = jiraRestClientSupplier.get().getMetadataClient().getIssueLinkTypes().claim();
        //in this case this upgrade task guesses wrong because there is no way to tell how Cloners was configured
        assertThat(issuelinksTypes, Matchers.contains(issueLinkType("Cloners", "copies", "is copied by")));
    }

    @Test
    public void testLegacyDirectionPropertyFalseIfClonersUnmodified() {
        administration.restoreDataWithBuildNumber("ClonersUnmodified_Post5.2.6.xml", 855);
        final Iterable<IssuelinksType> issuelinksTypes = jiraRestClientSupplier.get().getMetadataClient().getIssueLinkTypes().claim();
        assertThat(issuelinksTypes, Matchers.contains(issueLinkType("Cloners", "is cloned by", "clones")));
    }
}
