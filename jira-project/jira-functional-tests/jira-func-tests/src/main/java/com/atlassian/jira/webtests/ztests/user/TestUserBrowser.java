package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.BROWSING})
public class TestUserBrowser extends BaseJiraFuncTest {
    private static final List<String> EVERYONE = Arrays.asList(ADMIN_USERNAME, "anteater", "antoinette", "anton", "antone", "barney", "detkin");
    private static final String USER_SEARCH_FILTER = "userSearchFilter";
    private static final String GROUP_FILTER = "group";
    private static final String MAX = "max";

    @Test
    public void testFindByFiltering() {
        backdoor.restoreDataFromResource("TestUserBrowser.xml");
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);

        gotoUserBrowser();
        resetAll();

        assertUserNameColumn(EVERYONE);

        filterBy(USER_SEARCH_FILTER, "ant");
        assertUserNameColumn(Arrays.asList("anteater", "antoinette", "anton", "antone"));

        filterBy(USER_SEARCH_FILTER, "anto");
        assertUserNameColumn(Arrays.asList("antoinette", "anton", "antone"));

        filterBy(USER_SEARCH_FILTER, "e@");
        assertUserNameColumn(Arrays.asList("antoinette", "antone"));

        resetFilterBy(USER_SEARCH_FILTER);
        assertUserNameColumn(EVERYONE);

        // now just by full name
        filterBy(USER_SEARCH_FILTER, "Etkin");
        assertUserNameColumn(Arrays.asList("detkin"));

        resetFilterBy(USER_SEARCH_FILTER);
        filterBy(USER_SEARCH_FILTER, "d");
        assertUserNameColumn(Arrays.asList(ADMIN_USERNAME, "detkin"));

        filterBy(USER_SEARCH_FILTER, "atlassian");
        assertUserNameColumn(Arrays.asList("admin", "detkin"));
    }

    @Test
    public void testPaging() {
        backdoor.restoreDataFromResource("TestUserBrowserManyUsers.xml");
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);

        gotoUserBrowser();
        resetAll();

        filterBy(MAX, "10");
        assertUserNameColumn(asList(EVERYONE, userList(0, 2)));

        filterBy(MAX, "20");
        assertUserNameColumn(asList(EVERYONE, userList(0, 12)));

        filterBy(MAX, "50");
        assertUserNameColumn(asList(EVERYONE, userList(0, 42)));

        filterBy(MAX, "100");
        assertUserNameColumn(asList(EVERYONE, userList(0, 92)));

        filterBy(MAX, "All");
        assertUserNameColumn(asList(EVERYONE, userList(0, 120)));


        resetAll();
        filterBy(USER_SEARCH_FILTER, "username");

        assertUserNameColumn(userList(0, 19));
        gotoNext();
        assertUserNameColumn(userList(20, 39));
        gotoPrev();
        assertUserNameColumn(userList(0, 19));
        gotoNext();
        gotoNext();
        assertUserNameColumn(userList(40, 59));
        gotoPrev();
        assertUserNameColumn(userList(20, 39));

        resetAll();
        filterBy(USER_SEARCH_FILTER, "odd");
        assertUserNameColumn(userListBy2(1, 39));
        gotoNext();
        assertUserNameColumn(userListBy2(41, 79));
        gotoNext();
        assertUserNameColumn(userListBy2(81, 119));
        gotoPrev();
        assertUserNameColumn(userListBy2(41, 79));
        gotoPrev();
        assertUserNameColumn(userListBy2(1, 39));
    }

    @Test
    public void testCreatedUser() {
        backdoor.restoreDataFromResource("TestUserBrowserManyUsers.xml");
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);

        gotoUserBrowser();
        resetAll();

        filterBy(USER_SEARCH_FILTER, "ant");
        assertUserNameColumn(Arrays.asList("anteater", "antoinette", "anton", "antone"));

        tester.gotoPage("secure/admin/user/UserBrowser.jspa?createdUser=username069&createdUser=username070&createdUser=username069");
        // after creation of new user all filters are going back to defaults
        assertUserNameColumn(Arrays.asList("username069", "username070", "admin", "anteater", "antoinette", "anton", "antone", "barney"));
    }

    @Test
    public void testFilteringByGroupWorksForLargeGroups() {
        backdoor.restoreDataFromResource("TestUserBrowser35kUsersTwoDirectories.xml");
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);

        gotoUserBrowser("jira-servicedesk-users");
        assertThat(
                new XPathLocator(tester, "//table[@id='user_browser_table']/tbody/tr").getNodes().length,
                Matchers.equalTo(20)
        );
    }

    private void gotoPrev() {
        tester.clickLinkWithText("<< Previous");
    }

    private void gotoNext() {
        tester.clickLinkWithText("Next >>");
    }

    private List<String> userList(final int startI, final int endI) {
        return userListBy(startI, endI, 1);
    }

    private List<String> userListBy2(final int startI, final int endI) {
        return userListBy(startI, endI, 2);
    }

    private List<String> userListBy(final int startI, final int endI, final int by) {
        List<String> users = new ArrayList<String>();
        for (int i = startI; i <= endI; i += by) {
            String padNum = StringUtils.leftPad(String.valueOf(i), 3, '0');
            users.add("username" + padNum);
        }
        return users;
    }

    private List<String> asList(final List<String>... lists) {
        final ArrayList<String> list = new ArrayList<String>();
        for (List<String> args : lists) {
            list.addAll(args);
        }
        return list;
    }


    private void assertUserNameColumn(final List<String> values) {
        assertColumnImpl(2, 1, values);
    }

    private void assertColumnImpl(int col, final int startRow, final List<String> values) {
        int row = startRow;
        for (String value : values) {
            XPathLocator locator = new XPathLocator(tester, "//table[@id='user_browser_table']//tr[" + row + "]/td[" + col + "]/div/span");
            assertEquals("Asserting user browser row[" + row + "] col[" + col + "]", value, locator.getText());
            row++;
        }
    }

    private void filterBy(final String fieldName, final String value) {
        if (MAX.equals(fieldName)) {
            tester.selectOption(fieldName, value);
        } else {
            tester.setFormElement(fieldName, value);
        }
        tester.submit("");

    }

    private void resetAll() {
        resetFilterBy(USER_SEARCH_FILTER);
        filterBy(MAX, "20");
    }

    private void resetFilterBy(final String fieldName) {
        tester.setFormElement(fieldName, "");
        tester.submit("");
    }

    private void gotoUserBrowser() {
        tester.gotoPage("secure/admin/user/UserBrowser.jspa");
    }

    private void gotoUserBrowser(final String group) {
        tester.gotoPage(format("secure/admin/user/UserBrowser.jspa?group=%s", group));
    }
}
