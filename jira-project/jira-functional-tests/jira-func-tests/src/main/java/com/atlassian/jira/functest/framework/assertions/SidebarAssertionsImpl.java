package com.atlassian.jira.functest.framework.assertions;

import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Default implementation of {@link SidebarAssertions}
 *
 * @since 7.1
 */
public class SidebarAssertionsImpl implements SidebarAssertions {
    private final LocatorFactory locator;

    @Inject
    public SidebarAssertionsImpl(final LocatorFactory locator, final WebTester tester, final JIRAEnvironmentData environmentData) {
        this.locator = locator;
    }

    @Override
    public void assertProjectName(final String name) {
        final CssLocator projectHeader = locator.css(".aui-sidebar-body h1");
        assertThat("element that contains the project name could not be found", projectHeader.getNodes().length > 0, is(true));
        assertThat("project name does not match expected value", projectHeader.getText(), containsString(name));
    }
}
