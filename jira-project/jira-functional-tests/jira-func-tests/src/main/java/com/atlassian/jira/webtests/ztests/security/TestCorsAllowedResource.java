package com.atlassian.jira.webtests.ztests.security;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.plugins.rest.common.security.CorsHeaders;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@WebTest({Category.FUNC_TEST, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
public class TestCorsAllowedResource extends BaseJiraFuncTest {
    private static final String allowedOrigin = "http://localhost:8550/";
    private static final String notAllowedOrigin = "http://localhost:8560/";
    private final HttpClient client = new HttpClient();
    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreData("TestCorsAllowedResource.xml");
    }

    @Test
    public void testRequestToResourceWithCorsAllowedAnnotationFromAllowedOrigin() throws IOException, URISyntaxException {
        final String requestURI = restURI("/rest/api/2/project").toString();
        final GetMethod getRequest = new GetMethod(requestURI);
        getRequest.addRequestHeader(CorsHeaders.ORIGIN.value(), allowedOrigin);

        int statusCode = client.executeMethod(getRequest);
        assertEquals(HttpStatus.SC_OK, statusCode);

        final Header accessControlResponseHeader = getRequest.getResponseHeader(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN.value());
        assertEquals(allowedOrigin, accessControlResponseHeader.getValue());
    }

    @Test
    public void testRequestToResourceWithCorsAllowedAnnotationFromNotAllowedOrigin()
            throws IOException, URISyntaxException {
        final String requestURI = restURI("/rest/api/2/project").toString();
        final GetMethod getRequest = new GetMethod(requestURI);
        getRequest.addRequestHeader(CorsHeaders.ORIGIN.value(), notAllowedOrigin);

        int statusCode = client.executeMethod(getRequest);
        assertEquals(HttpStatus.SC_OK, statusCode);

        final Header accessControlResponseHeader = getRequest.getResponseHeader(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN.value());
        assertNull(accessControlResponseHeader);
    }

    @Test
    public void testRequestToResourceWithCorsAllowedAnnotationFromAllowedOriginWithPreflight()
            throws IOException, URISyntaxException {
        final String requestURI = restURI("/rest/api/2/project").toString();
        final GetMethod getRequest = new GetMethod(requestURI);
        getRequest.addRequestHeader(CorsHeaders.ORIGIN.value(), allowedOrigin);

        int statusCode = client.executeMethod(getRequest);
        assertEquals(HttpStatus.SC_OK, statusCode);

        final Header accessControlResponseHeader = getRequest.getResponseHeader(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN.value());

        assertNotNull(accessControlResponseHeader);
    }

    @Test
    public void testRequestToResourceWithCorsAllowedAnnotationFromNotAllowedOriginWithPreflight()
            throws IOException, URISyntaxException {
        final String requestURI = restURI("/rest/api/2/project").toString();
        final GetMethod getRequest = new GetMethod(requestURI);
        getRequest.addRequestHeader(CorsHeaders.ORIGIN.value(), notAllowedOrigin);

        int statusCode = client.executeMethod(getRequest);
        assertEquals(HttpStatus.SC_OK, statusCode);

        final Header accessControlResponseHeader = getRequest.getResponseHeader(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN.value());

        assertNull(accessControlResponseHeader);
    }

    @Test
    public void testRequestToResourceWithoutCorsAllowedAnnotationFromAllowedOrigin()
            throws IOException, URISyntaxException {
        client.getParams().setAuthenticationPreemptive(true);
        client.getState().setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("admin", "admin"));
        final String requestURI = restURI("/rest/internal/1.0/darkFeatures/jira.user.darkfeature.admin").toString();
        final GetMethod getRequest = new GetMethod(requestURI);
        getRequest.addRequestHeader(CorsHeaders.ORIGIN.value(), allowedOrigin);

        int statusCode = client.executeMethod(getRequest);
        assertEquals(HttpStatus.SC_OK, statusCode);

        final Header accessControlResponseHeader = getRequest.getResponseHeader(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN.value());
        assertNull(accessControlResponseHeader);
    }

    @Test
    public void testRequestToResourceWithoutCorsAllowedAnnotationFromNotAllowedOrigin()
            throws IOException, URISyntaxException {
        client.getParams().setAuthenticationPreemptive(true);
        client.getState().setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("admin", "admin"));
        final String requestURI = restURI("/rest/internal/1.0/darkFeatures/jira.user.darkfeature.admin").toString();
        final GetMethod getRequest = new GetMethod(requestURI);
        getRequest.addRequestHeader(CorsHeaders.ORIGIN.value(), notAllowedOrigin);

        int statusCode = client.executeMethod(getRequest);
        assertEquals(HttpStatus.SC_OK, statusCode);

        final Header accessControlResponseHeader = getRequest.getResponseHeader(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN.value());
        assertNull(accessControlResponseHeader);
    }

    private URI restURI(final String resourceURI) throws URISyntaxException {
        final URI jiraURI = environmentData.getBaseUrl().toURI();
        return UriBuilder.fromUri(jiraURI)
                .path(resourceURI)
                .build();
    }

}
