package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.ISSUES})
@Restore("TestCantViewCreatedIssue.xml")
public class TestCreateIssueNoBrowseProjectPermission extends BaseJiraFuncTest {
    private static final String LOGIN = "log in";

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        backdoor.darkFeatures().enableForSite("jira.no.frother.reporter.field");
    }

    @After
    public void tearDown() {
        backdoor.darkFeatures().disableForSite("jira.no.frother.reporter.field");
    }

    /**
     * Test that user is redirected if user does not have the permission to view the issue they created. JRA-7684
     */
    @Test
    public void testCreateIssueWithNoBrowsePermission() {
        logger.log("Create Issue: Adding issue with no browse permission for user");

        //setup
        administration.restoreData("TestCantViewCreatedIssue.xml");

        //Start of tests
        //create issue - not logged in, no browse permission
        navigation.logout();
        navigation.gotoDashboard();
        navigation.issue().createIssue("noBrowseProject", "Bug", "test1");
        assertIssueCreatedButCannotView(false);
        //log in as user with no browse permission
        tester.clickLinkWithText(LOGIN);
        tester.setFormElement("os_username", "nobrowseuser");
        tester.setFormElement("os_password", "nobrowseuser");
        tester.setWorkingForm("login-form");
        tester.submit();
        assertIssueCreatedButCannotView(true);

        //create issue - logged in, no browse permission
        navigation.issue().createIssue("noBrowseProject", "Bug", "test1");
        assertIssueCreatedButCannotView(true);

        //create issue - not logged in, no browse permission
        navigation.logout();
        navigation.gotoDashboard();
        navigation.issue().createIssue("noBrowseProject", "Bug", "test1");
        assertIssueCreatedButCannotView(false);
        //log in as admin with browse permission
        tester.clickLinkWithText(LOGIN);
        tester.setFormElement("os_username", ADMIN_USERNAME);
        tester.setFormElement("os_password", ADMIN_USERNAME);
        tester.setWorkingForm("login-form");
        tester.submit();
        tester.assertTextNotPresent("Issue Created Successfully");
        tester.assertLinkNotPresentWithText(LOGIN);
        tester.assertTextPresent("Details");

        //tear down
        administration.restoreBlankInstance();
    }

    private void assertIssueCreatedButCannotView(final boolean loggedIn) {
        tester.assertTextPresent("Issue Created Successfully");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "You have successfully created the issue (", "), however you do not have the permission to view the created issue.");
        if (!loggedIn) {
            tester.assertLinkPresentWithText(LOGIN);
        }
    }

}
