package com.atlassian.jira.webtests.ztests.navigator.jql;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * Test realated to relative Date formats.
 * JRA-18205, CORE-131
 *
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.JQL})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestRelativeDates.xml")
public class TestRelativeDates extends BaseJiraFuncTest {

    @Inject
    private IssueTableAssertions issueTableAssertions;

    @Before
    public void setUp() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    public void testRelativeDates() throws Exception {
        navigation.issue().viewIssue("TV-1");

        tester.clickLink("edit-issue");
        tester.setWorkingForm("issue-edit");
        tester.setFormElement("duedate", "12/Aug/09");
        tester.submit("Update");

        //minutes
        issueTableAssertions.assertSearchWithResults("project = \"TV-Bunnies\" and updated >= \"-100m\"", "TV-1");
        issueTableAssertions.assertSearchWithResults("project = \"TV-Bunnies\" and updated >= \"-100minutes\"", "TV-1");
        issueTableAssertions.assertSearchWithError("project = \"TV-Bunnies\" and updated >= \"-100milestone\"", "Date value '-100milestone' for field 'updated' is invalid. Valid formats include: 'yyyy/MM/dd HH:mm', 'yyyy-MM-dd HH:mm', 'yyyy/MM/dd', 'yyyy-MM-dd', or a period format e.g. '-5d', '4w 2d'.");

        //hours
        issueTableAssertions.assertSearchWithResults("project = \"TV-Bunnies\" and updated >= \"-4h\"", "TV-1");
        issueTableAssertions.assertSearchWithResults("project = \"TV-Bunnies\" and updated >= \"-4hours\"", "TV-1");
        issueTableAssertions.assertSearchWithError("project = \"TV-Bunnies\" and updated >= \"-4humor\"", "Date value '-4humor' for field 'updated' is invalid. Valid formats include: 'yyyy/MM/dd HH:mm', 'yyyy-MM-dd HH:mm', 'yyyy/MM/dd', 'yyyy-MM-dd', or a period format e.g. '-5d', '4w 2d'.");

        //days
        issueTableAssertions.assertSearchWithResults("project = \"TV-Bunnies\" and updated >= \"-4d\"", "TV-1");
        issueTableAssertions.assertSearchWithResults("project = \"TV-Bunnies\" and updated >= \"-4days\"", "TV-1");
        issueTableAssertions.assertSearchWithError("project = \"TV-Bunnies\" and updated >= \"-4dummies\"", "Date value '-4dummies' for field 'updated' is invalid. Valid formats include: 'yyyy/MM/dd HH:mm', 'yyyy-MM-dd HH:mm', 'yyyy/MM/dd', 'yyyy-MM-dd', or a period format e.g. '-5d', '4w 2d'.");

        //weeks
        issueTableAssertions.assertSearchWithResults("project = \"TV-Bunnies\" and updated >= \"-10w, 3minutes\"", "TV-1");
        issueTableAssertions.assertSearchWithResults("project = \"TV-Bunnies\" and updated >= \"-10weeks, 2d\"", "TV-1");
        issueTableAssertions.assertSearchWithError("project = \"TV-Bunnies\" and updated >= \"-10wombat\"", "Date value '-10wombat' for field 'updated' is invalid. Valid formats include: 'yyyy/MM/dd HH:mm', 'yyyy-MM-dd HH:mm', 'yyyy/MM/dd', 'yyyy-MM-dd', or a period format e.g. '-5d', '4w 2d'.");
        issueTableAssertions.assertSearchWithError("project = \"TV-Bunnies\" and updated >= \"-10whatever\"", "Date value '-10whatever' for field 'updated' is invalid. Valid formats include: 'yyyy/MM/dd HH:mm', 'yyyy-MM-dd HH:mm', 'yyyy/MM/dd', 'yyyy-MM-dd', or a period format e.g. '-5d', '4w 2d'.");
    }
}
