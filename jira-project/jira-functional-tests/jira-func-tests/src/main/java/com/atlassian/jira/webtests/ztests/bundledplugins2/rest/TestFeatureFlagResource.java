package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.DarkFeature;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client.FeatureKeyClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;

/**
 * Func tests for FeatureFlagResource.
 *
 * @since v7.1
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestFeatureFlagResource extends BaseJiraRestTest {
    // These are defined in system-feature-keys-plugin.xml so we use 2 we already have rather than adding extras
    private static final String FEATURE_KEY = "jira.instrumentation.laas";
    public static final String FULL_DARK_FEATURE_KEY_ENABLED = FEATURE_KEY + ".enabled";
    public static final String FULL_DARK_FEATURE_KEY_DISABLED = FEATURE_KEY + ".disabled";

    private FeatureKeyClient client;
    private boolean initialEnabled;
    private boolean initialDisabled;

    @Before
    public void setUpTest() {
        client = new FeatureKeyClient(environmentData);

        // Because we are setting existing feature flags we will be careful to restore them to their initial state each time
        initialEnabled = backdoor.darkFeatures().isGlobalEnabled(FULL_DARK_FEATURE_KEY_ENABLED);
        initialDisabled = backdoor.darkFeatures().isGlobalEnabled(FULL_DARK_FEATURE_KEY_DISABLED);

        setViaBackDoor(FULL_DARK_FEATURE_KEY_ENABLED, false);
        setViaBackDoor(FULL_DARK_FEATURE_KEY_DISABLED, false);
    }

    @After
    public void tearDownTest() {
        setViaBackDoor(FULL_DARK_FEATURE_KEY_ENABLED, initialEnabled);
        setViaBackDoor(FULL_DARK_FEATURE_KEY_DISABLED, initialDisabled);
    }

    /**
     * Tests getting a feature flag
     */
    @Test
    public void testGet() throws Exception {
        client.loginAs("admin", "admin");
        Response response = client.getResponse(FEATURE_KEY);
        assertThat(response.statusCode, is(200));

        DarkFeature feature = client.get(FEATURE_KEY);
        // We turn this off in the setup, so test it is false.
        assertThat(feature, notNullValue());
        assertThat(feature.enabled, is(false));
    }

    /**
     * Tests getting a feature flag
     */
    @Test
    public void testGetAll() throws Exception {
        client.loginAs("admin", "admin");
        Response response = client.getResponse();
        assertThat(response.statusCode, is(200));
        Map<String, Map<String, Boolean>> featureFlags = (Map<String, Map<String, Boolean>>) client.get();

        Map<String, Boolean> feature = featureFlags.get(FEATURE_KEY);
        assertThat(feature, notNullValue());
        // We turn this off in the setup, so test it is false.
        assertThat(feature.get("enabled"), is(false));
    }

    /**
     * Tests setting feature flag
     */
    @Test
    public void testEnable() throws Exception {
        client.loginAs("admin", "admin");
        //Set the dark features to turn the feature OFF
        setViaBackDoor(FULL_DARK_FEATURE_KEY_ENABLED, false);
        setViaBackDoor(FULL_DARK_FEATURE_KEY_DISABLED, true);
        DarkFeature featureFlag = client.get(FEATURE_KEY);
        assertThat(featureFlag.enabled, is(false));

        client.put(FEATURE_KEY, true);
        assertViaBackDoor(FEATURE_KEY, true);
    }

    /**
     * Tests disabling
     */
    @Test
    public void testDisable() throws Exception {
        client.loginAs("admin", "admin");
        //Set the dark features to turn the feature ON
        setViaBackDoor(FULL_DARK_FEATURE_KEY_ENABLED, true);
        setViaBackDoor(FULL_DARK_FEATURE_KEY_DISABLED, false);
        DarkFeature featureFlag = client.get(FEATURE_KEY);
        assertThat(featureFlag.enabled, is(true));

        client.put(FEATURE_KEY, false);
        assertViaBackDoor(FEATURE_KEY, false);
    }

    /**
     * Tests non-admin permissions
     */
    @Test
    public void testNonAdministrator() throws Exception {
        client.loginAs("fred");

        Response response = client.putResponse(FEATURE_KEY, true);
        assertEquals("Expected 402 as status code for non admin", 403, response.statusCode);

        response = client.putResponse(FEATURE_KEY, false);
        assertEquals("Expected 402 as status code for non admin", 403, response.statusCode);

        response = client.getResponse(FEATURE_KEY);
        assertEquals("Expected 402 as status code for non admin", 403, response.statusCode);

        response = client.getResponse();
        assertEquals("Expected 402 as status code for non admin", 403, response.statusCode);
    }

    private void assertViaBackDoor(String featureKey, boolean enabled) {
        // This is complicated as the result of this test depends upon the default state.
        // enabled is either, enabled = true or disabled = false
        // disabled is either, enabled = false or enabled = true

        final String fullDarkFeatureKeyEnabled = featureKey + ".enabled";
        final String fullDarkFeatureKeyDisabled = featureKey + ".disabled";

        boolean isEnabled = backdoor.darkFeatures().isGlobalEnabled(fullDarkFeatureKeyEnabled);
        boolean isDisabled = backdoor.darkFeatures().isGlobalEnabled(fullDarkFeatureKeyDisabled);

        if (enabled) {
            assertThat(isEnabled | !isDisabled, is(true));
        } else {
            assertThat(!isEnabled | isDisabled, is(true));
        }
    }

    private void setViaBackDoor(final String feature, boolean enabled) {
        if (enabled) {
            backdoor.darkFeatures().enableForSite(feature);
        } else {
            backdoor.darkFeatures().disableForSite(feature);
        }
    }

}
