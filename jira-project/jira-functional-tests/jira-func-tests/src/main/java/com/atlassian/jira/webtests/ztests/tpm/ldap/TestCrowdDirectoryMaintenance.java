package com.atlassian.jira.webtests.ztests.tpm.ldap;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.meterware.httpunit.WebLink;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.3
 */
@WebTest({Category.FUNC_TEST, Category.LDAP, Category.TPM})
@RestoreBlankInstance
@LoginAs(user = ADMIN_USERNAME)
public class TestCrowdDirectoryMaintenance extends BaseJiraFuncTest {
    private static final String JAACS_APPLICATION_NAME = "CousinMabel";
    private static final String JAACS_APPLICATION_CREDENTIAL = "secret";

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Assertions assertions;

    @Inject
    private TextAssertions textAssertions;

    @Rule
    public final MockCrowdServerRule mockCrowdServerRule = new MockCrowdServerRule();

    @Before
    public void setUpTest() {
        setUpJaacsServer();
    }

    @Test
    public void testAddCrowdDirectory() {
        addDirectory();

        // assert we are back on the "View Directories" page.
        textAssertions.assertTextPresent("The table below shows the user directories currently configured for JIRA.");
        // assert the new directory is added at the end
        UserDirectoryTable userDirectoryTable = new UserDirectoryTable(this, logger, assertions);
        userDirectoryTable.assertRow(1).contains("1", "JIRA Internal Directory", "Internal").hasMoveUp(false).hasMoveDown(true).hasOnlyEditOperation();
        userDirectoryTable.assertRow(2).contains("10000", "First Crowd", "Atlassian Crowd").hasMoveUp(true).hasMoveDown(false).hasDisableEditSynchroniseOperations();
    }

    private void addDirectory() {
        // We go directly to the add page, because we need JavaScript to drive the normal add UI.
        navigation.gotoPage("/plugins/servlet/embedded-crowd/configure/crowd/");
        textAssertions.assertTextPresent(new IdLocator(tester, "embcwd"), "Server Settings");

        // Test for all mandatory fields
        tester.setWorkingForm("configure-crowd-form");
        tester.setFormElement("name", "");
        tester.submit("test");
        // Check for the errors
        textAssertions.assertTextPresent("Name is a required field.");
        textAssertions.assertTextPresent("Server URL is a required field.");
        textAssertions.assertTextPresent("Application name is a required field.");
        textAssertions.assertTextPresent("Application password is a required field.");

        tester.setWorkingForm("configure-crowd-form");
        tester.setFormElement("name", "First Crowd");
        tester.setFormElement("crowdServerUrl", mockCrowdServerRule.url());
        tester.setFormElement("applicationName", JAACS_APPLICATION_NAME);
        tester.setFormElement("applicationPassword", JAACS_APPLICATION_CREDENTIAL);

        tester.submit("test");

        tester.assertTextPresent("Connection test successful");

        tester.submit("save");
    }

    @Test
    public void testEditCrowdDirectory() {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_DIRECTORIES);
        textAssertions.assertTextPresent("The table below shows the user directories currently configured for JIRA.");

        addDirectory();

        UserDirectoryTable userDirectoryTable = new UserDirectoryTable(this, logger, assertions);
        WebLink link = userDirectoryTable.getTableCell(2, 4).getLinkWith("edit");
        navigation.clickLink(link);

        textAssertions.assertTextPresent(new IdLocator(tester, "embcwd"), "Server Settings");

        // Change the contents and save
        tester.setFormElement("name", "First Crowd X");
        tester.setFormElement("crowdServerUrl", mockCrowdServerRule.url());
        tester.setFormElement("applicationName", JAACS_APPLICATION_NAME);
        tester.setFormElement("applicationPassword", JAACS_APPLICATION_CREDENTIAL);
        tester.submit("test");
        tester.submit("save");

        // assert we are back on the "View Directories" page.
        textAssertions.assertTextPresent("The table below shows the user directories currently configured for JIRA.");
        // assert the new directory is added at the end
        userDirectoryTable = new UserDirectoryTable(this, logger, assertions);
        userDirectoryTable.assertRow(1).contains("1", "JIRA Internal Directory", "Internal").hasMoveUp(false).hasMoveDown(true).hasOnlyEditOperation();
        userDirectoryTable.assertRow(2).contains("10000", "First Crowd X", "Atlassian Crowd").hasMoveUp(true).hasMoveDown(false).hasDisableEditSynchroniseOperations();

        // Check for mandatory fields
        link = userDirectoryTable.getTableCell(2, 4).getLinkWith("edit");
        navigation.clickLink(link);

        textAssertions.assertTextPresent(new IdLocator(tester, "embcwd"), "Server Settings");

        // Change the contents and save
        tester.setFormElement("name", "");
        tester.setFormElement("crowdServerUrl", "");
        tester.setFormElement("applicationName", "");
        tester.setFormElement("applicationPassword", "");
        tester.submit("test");

        // Check for the errors
        textAssertions.assertTextPresent("Name is a required field.");
        textAssertions.assertTextPresent("Server URL is a required field.");
        textAssertions.assertTextPresent("Application name is a required field.");

        tester.clickLink("configure-crowd-form-cancel");

        // assert we are back on the "View Directories" page.
        textAssertions.assertTextPresent("The table below shows the user directories currently configured for JIRA.");
        // assert the directory is unchanged
        userDirectoryTable = new UserDirectoryTable(this, logger, assertions);
        userDirectoryTable.assertRow(1).contains("1", "JIRA Internal Directory", "Internal").hasMoveUp(false).hasMoveDown(true).hasOnlyEditOperation();
        userDirectoryTable.assertRow(2).contains("10000", "First Crowd X", "Atlassian Crowd").hasMoveUp(true).hasMoveDown(false).hasDisableEditSynchroniseOperations();
    }

    @Test
    public void testDeleteCrowdDirectory() {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_DIRECTORIES);
        textAssertions.assertTextPresent("The table below shows the user directories currently configured for JIRA.");

        addDirectory();

        UserDirectoryTable userDirectoryTable = new UserDirectoryTable(this, logger, assertions);
        WebLink link = userDirectoryTable.getTableCell(2, 4).getLinkWith("Disable");
        navigation.clickLink(link);

        userDirectoryTable = new UserDirectoryTable(this, logger, assertions);
        userDirectoryTable.assertRow(1).contains("1", "JIRA Internal Directory", "Internal").hasMoveUp(false).hasMoveDown(true).hasOnlyEditOperation();
        userDirectoryTable.assertRow(2).contains("10000", "First Crowd  (inactive)", "Atlassian Crowd").hasMoveUp(true).hasMoveDown(false).hasEnableEditRemoveSynchroniseOperations();

        // Now Delete the directory
        textAssertions.assertTextPresent(new IdLocator(tester, "embcwd"), "First Crowd");
        link = userDirectoryTable.getTableCell(2, 4).getLinkWith("Remove");
        navigation.clickLink(link);

        // assert we are back on the "View Directories" page.
        textAssertions.assertTextPresent("The table below shows the user directories currently configured for JIRA.");
        userDirectoryTable = new UserDirectoryTable(this, logger, assertions);
        userDirectoryTable.assertRow(1).contains("1", "JIRA Internal Directory", "Internal").hasMoveUp(false).hasMoveDown(false).hasOnlyEditOperation();
        // assert the directory is gone
        textAssertions.assertTextNotPresent(new IdLocator(tester, "embcwd"), "First Crowd");

    }

    private void setUpJaacsServer() {
        navigation.gotoPage("secure/project/ConfigureCrowdServer.jspa");
        tester.clickLink("crowd-add-application");
        tester.setWorkingForm("edit-crowd-application");
        tester.setFormElement("name", JAACS_APPLICATION_NAME);
        tester.setFormElement("credential", JAACS_APPLICATION_CREDENTIAL);
        tester.clickButton("edit-crowd-application-submit");
    }
}