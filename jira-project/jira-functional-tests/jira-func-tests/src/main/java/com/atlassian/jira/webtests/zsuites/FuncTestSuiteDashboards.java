package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.dashboard.TestAddPortalPageSharing;
import com.atlassian.jira.webtests.ztests.dashboard.TestDashboardDeleteConfirm;
import com.atlassian.jira.webtests.ztests.dashboard.TestDashboardRelatedEntitiesDelete;
import com.atlassian.jira.webtests.ztests.dashboard.TestEditPortalPage;
import com.atlassian.jira.webtests.ztests.dashboard.TestEditPortalPageSharing;
import com.atlassian.jira.webtests.ztests.dashboard.TestManageDashboardChooseTab;
import com.atlassian.jira.webtests.ztests.dashboard.TestManageDashboardPagePermissions;
import com.atlassian.jira.webtests.ztests.dashboard.TestManageDashboardPages;
import com.atlassian.jira.webtests.ztests.dashboard.TestReorderDashboardPages;
import com.atlassian.jira.webtests.ztests.dashboard.TestSearchDashboardPages;
import com.atlassian.jira.webtests.ztests.dashboard.management.TestDeleteSharedDashboardsByAdmins;
import com.atlassian.jira.webtests.ztests.dashboard.management.TestSharedDashboardSearchingByAdmins;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A func test suite for Dashboards
 *
 * @since v4.0
 */
public class FuncTestSuiteDashboards {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestManageDashboardPages.class)
                .add(TestReorderDashboardPages.class)
                .add(TestDashboardRelatedEntitiesDelete.class)
                .add(TestManageDashboardPagePermissions.class)

                .add(TestAddPortalPageSharing.class)
                .add(TestEditPortalPage.class)
                .add(TestEditPortalPageSharing.class)
                .add(TestManageDashboardChooseTab.class)
                .add(TestSearchDashboardPages.class)
                .add(TestSharedDashboardSearchingByAdmins.class)

                .add(TestDashboardDeleteConfirm.class)
                .add(TestDeleteSharedDashboardsByAdmins.class)
                .build();
    }
}
