package com.atlassian.jira.webtests.ztests.license;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.LicenseKeys;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.CORE_KEY;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.TEST_KEY;
import static com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@WebTest({Category.FUNC_TEST, Category.LICENSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestCreateUserInMultipleApplicationRoles extends TestCreateUserHelper {
    private static final String ADMIN_GROUP = "jira-administrators";

    @Inject
    private LocatorFactory locator;

    @Before
    public void setUp() {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_AND_TEST_ROLES);
        backdoor.applicationRoles().putRole(TEST_KEY);
        backdoor.applicationRoles().putRole(CORE_KEY);
    }

    @Test
    public void testCreateUserWhenOneRoleHasDefaultGroups() {
        backdoor.usersAndGroups().addGroup("platform-users");
        //Admin group added to ensure that admin user can login
        backdoor.applicationRoles().putRoleWithDefaults(CORE_KEY, newHashSet("platform-users", ADMIN_GROUP),
                newHashSet("platform-users"));
        openCreateUserPage();

        assertTrue(hasNoDefaultGroupWarningForApplication(TEST_KEY));
        assertHasCheckbox(CORE_KEY); // Core has no warning

        tester.setFormElement("username", "nobody");
        tester.setFormElement("password", "nobody");
        tester.setFormElement("fullname", "nobody");
        tester.setFormElement("email", "nobody@example.com");
        tester.submit("Create");
        assertUserNotInAnyGroups("nobody");
        assertTrue(parseApplicationKeys(ParseApplicationKeysSelection.SELECTED_KEYS).isEmpty());
    }

    @Test
    public void testCreateUserWhenAllRolesHaveDefaultGroups() {
        backdoor.usersAndGroups().addGroup("platform-users");
        //Admin group added to ensure that admin user can login
        backdoor.applicationRoles().putRoleWithDefaults(CORE_KEY, newHashSet("platform-users", ADMIN_GROUP), newHashSet("platform-users"));
        backdoor.usersAndGroups().addGroup("test-users");
        backdoor.usersAndGroups().addGroup("test-users2");
        backdoor.applicationRoles().putRoleWithDefaults(TEST_KEY, newHashSet("test-users", "test-users2"),
                newHashSet("test-users", "test-users2"));
        openCreateUserPage();
        assertFalse("Create User has no Warning License without Default Group Banner", locator.css(".warning-default-group-missing-multi-role").exists());
        assertFalse("Create User has no warning for license over limit", locator.css(".warning-users-over-limit-multi-role").exists());
        assertFalse("Create User has no Warning License without Default Group Banner", locator.css(".warning-default-group-missing").exists());
        assertFalse("Create User has no warning for license over limit", locator.css(".warning-users-over-limit").exists());
        assertFalse("Create User has no warning for undefined application roles", locator.css(".warning-undefined-role").exists());

        assertThat(parseApplicationKeys(ParseApplicationKeysSelection.ALL_KEYS), contains(TEST_KEY, CORE_KEY));
    }

    @Test
    public void testCreateUserOverSeatLimitInOneRole() {
        backdoor.usersAndGroups().addGroup("platform-users");
        //Admin group added to ensure that admin user can login
        backdoor.applicationRoles().putRoleWithDefaults(CORE_KEY, newHashSet("platform-users", ADMIN_GROUP), newHashSet("platform-users"));
        backdoor.usersAndGroups().addGroup("test-users");
        backdoor.applicationRoles().putRoleAndSetDefault(TEST_KEY, "test-users");

        //license has 5 user capacity for platform.
        backdoor.usersAndGroups().addUser("one");
        backdoor.usersAndGroups().addUser("two");
        backdoor.usersAndGroups().addUser("three");
        backdoor.usersAndGroups().addUserToGroup("one", "platform-users");
        backdoor.usersAndGroups().addUserToGroup("two", "platform-users");
        backdoor.usersAndGroups().addUserToGroup("three", "platform-users");

        openCreateUserPage();
        assertTrue(hasUserLimitReachedWarningForApplication(CORE_KEY));
        assertHasCheckbox(TEST_KEY);

        assertThat(parseApplicationKeys(ParseApplicationKeysSelection.ALL_KEYS), contains(TEST_KEY, CORE_KEY));
    }

    @Test
    public void testCreateUserOverSeatLimitInOneRoleDoesntDrainCore() {
        backdoor.usersAndGroups().addGroup("platform-users");
        //Admin group added to ensure that admin user can login
        backdoor.applicationRoles().putRoleWithDefaults(CORE_KEY, newHashSet("platform-users", ADMIN_GROUP), newHashSet("platform-users"));
        backdoor.usersAndGroups().addGroup("test-users");
        backdoor.applicationRoles().putRoleAndSetDefault(TEST_KEY, "test-users");

        //license has 5 user capacity for platform.
        backdoor.usersAndGroups().addUser("one");
        backdoor.usersAndGroups().addUser("two");
        backdoor.usersAndGroups().addUser("three");
        backdoor.usersAndGroups().addUserToGroup("one", "platform-users");
        backdoor.usersAndGroups().addUserToGroup("two", "platform-users");
        backdoor.usersAndGroups().addUserToGroup("three", "platform-users");
        backdoor.usersAndGroups().addUserToGroup("one", "test-users");
        backdoor.usersAndGroups().addUserToGroup("two", "test-users");
        backdoor.usersAndGroups().addUserToGroup("three", "test-users");

        openCreateUserPage();
        assertTrue(hasUserLimitReachedWarningForApplication(TEST_KEY));
        assertHasCheckbox(CORE_KEY);
        assertThat(parseApplicationKeys(ParseApplicationKeysSelection.ALL_KEYS), containsInAnyOrder(TEST_KEY, CORE_KEY));
    }


    private List<String> parseApplicationKeys(final ParseApplicationKeysSelection applicationKeysSelection) {
        final CssLocator css = locator.css(".application-picker .application, .application-picker .application-warning");
        final Node[] nodes = css.getNodes();
        return Arrays.asList(nodes).stream()
                .filter(node -> {
                    final Node checkedAttribute = node.getAttributes().getNamedItem("checked");
                    return applicationKeysSelection == ParseApplicationKeysSelection.ALL_KEYS || checkedAttribute != null && StringUtils.equals("checked", checkedAttribute.getTextContent());
                })
                .map(node -> node.getAttributes().getNamedItem("data-key").getTextContent())
                .collect(CollectorsUtil.toImmutableList());
    }

    private void openCreateUserPage() {
        navigation.gotoAdminSection(Navigation.AdminSection.CREATE_USER);
    }

    enum ParseApplicationKeysSelection {
        ALL_KEYS,
        SELECTED_KEYS
    }
}