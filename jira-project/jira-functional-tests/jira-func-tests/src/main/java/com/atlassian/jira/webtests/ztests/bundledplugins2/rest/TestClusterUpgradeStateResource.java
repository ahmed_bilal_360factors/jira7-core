package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.FuncTestRestClient;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.meterware.httpunit.WebResponse;
import org.junit.Ignore;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @since v7.3
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestClusterUpgradeStateResource extends BaseJiraRestTest {
    private static final String CLUSTER_UPGRADE_STATE_DARK_FEATURE = "jira.zdu.cluster-upgrade-state";

    @Inject
    private FuncTestRestClient restClient;

    @Test
    public void clusterStateResourceIsAvailableWithDarkFeature() throws Exception {
        enableDarkFeature();

        WebResponse response = getClusterState();

        assertThat(response.getResponseCode(), is(200));
    }

    @Test
    @Ignore("JSEV-1062")
    public void clusterStateResourceIsUnavailableWithoutDarkFeature() throws Exception {
        disableDarkFeature();

        WebResponse response = getClusterState();

        assertThat(response.getResponseCode(), is(404));
    }

    private WebResponse getClusterState() throws Exception {
        return restClient.GET("/rest/api/2/cluster/zdu/state");
    }

    private void enableDarkFeature() {
        backdoor.darkFeatures().enableForSite(CLUSTER_UPGRADE_STATE_DARK_FEATURE);
    }

    private void disableDarkFeature() {
        backdoor.darkFeatures().disableForSite(CLUSTER_UPGRADE_STATE_DARK_FEATURE);
    }
}
