package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.sun.jersey.api.client.UniformInterfaceException;

import javax.ws.rs.core.Response;

/**
 * @since 7.2
 */
public class CacheCheckControl extends BackdoorControl<CacheCheckControl> {
    public CacheCheckControl(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public void checkSerialization() {
        try {
            createResource().path("cachecheck").path("serialization").get(String.class);
        } catch (UniformInterfaceException e) {
            if (e.getResponse().getStatus() == Response.Status.PRECONDITION_FAILED.getStatusCode()) {
                //Caching error
                String content = e.getResponse().getEntity(String.class);
                throw new RuntimeException("A cache had serialization errors.  Values in a replicated/copied cache must be serializable: " + content, e);
            } else {
                throw e;
            }

        }
    }


}
