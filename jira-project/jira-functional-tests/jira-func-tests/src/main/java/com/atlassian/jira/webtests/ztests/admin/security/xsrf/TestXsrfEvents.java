package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestXsrfEvents extends BaseJiraFuncTest {

    @Inject
    private Form form;

    @Test
    public void testEventAdministration() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck("AddEvent", new EventSetup() {
                    public void setup() {
                        super.setup();
                        tester.setWorkingForm("jiraform");
                        tester.setFormElement("name", "New Event");
                        tester.selectOption("templateId", "Issue Created");
                    }
                }, new XsrfCheck.FormSubmission("Add")),
                new XsrfCheck("EditEvent", new EventSetup() {
                    public void setup() {
                        super.setup();
                        tester.clickLink("edit_New Event");
                        tester.setFormElement("description", "This is a New Event");
                    }
                }, new XsrfCheck.FormSubmission("Update")),
                new XsrfCheck("DeleteEvent", new EventSetup() {
                    public void setup() {
                        super.setup();
                        tester.clickLink("del_New Event");
                    }
                }, new XsrfCheck.FormSubmission("Delete"))
        ).run(getTester(), navigation, form);
    }

    private class EventSetup implements XsrfCheck.Setup {
        public void setup() {
            navigation.gotoAdminSection(Navigation.AdminSection.EVENTTYPES);
        }
    }
}


