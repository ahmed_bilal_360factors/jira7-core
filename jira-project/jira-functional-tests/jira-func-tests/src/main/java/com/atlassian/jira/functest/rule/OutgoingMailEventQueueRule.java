package com.atlassian.jira.functest.rule;

import com.atlassian.jira.functest.framework.backdoor.OutgoingMailControl;
import com.atlassian.jira.util.Supplier;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Rule to initialize SMTPBackdoor for listening for emails.
 * Outgoing mail listening queue is turned on and cleared when test is started and listening queue is turned of on test end.
 * Tested methods should be annotated with {@link MailTest}
 *
 * @since v7.1
 */
public class OutgoingMailEventQueueRule extends TestWatcher {
    private Supplier<OutgoingMailControl> outgoingMailControl;

    OutgoingMailEventQueueRule(final Supplier<OutgoingMailControl> outgoingMailControl) {
        this.outgoingMailControl = outgoingMailControl;
    }

    @Override
    protected void starting(final Description description) {
        if (shouldFire(description)) {
            outgoingMailControl.get().enable();
            outgoingMailControl.get().clearMessages();
        }
    }

    @Override
    protected void finished(final Description description) {
        if (shouldFire(description)) {
            outgoingMailControl.get().disable();
        }
    }

    private boolean shouldFire(final Description description) {
        return description.getAnnotation(MailTest.class) != null || description.getTestClass().getAnnotation(MailTest.class) != null;
    }

    public OutgoingMailControl getOutgoingMailControl() {
        return outgoingMailControl.get();
    }
}
