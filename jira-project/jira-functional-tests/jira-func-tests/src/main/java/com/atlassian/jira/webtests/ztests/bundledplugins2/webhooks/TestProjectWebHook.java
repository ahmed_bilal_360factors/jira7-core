package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.testkit.client.util.JsonMatchers.hasField;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.WEBHOOKS, Category.REFERENCE_PLUGIN})
public class TestProjectWebHook extends AbstractWebHookTest {

    public static final String projectName = "test";
    public static final String projectKey = "TEST";

    @Before
    @Override
    public void setUpTest() {
        super.setUpTest();
        backdoor.restoreBlankInstance();
    }

    @Test
    public void testProjectCreatedWebHook() throws Exception {
        final String webHookName = "project_created";
        registerWebHook(webHookName);

        final long projectId = createDefaultProject();

        checkResponseForWebHook(webHookName, projectId);
    }

    @Test
    public void testProjectUpdatedWebHook() throws Exception {
        final String webHookName = "project_updated";
        registerWebHook(webHookName);
        final long projectId = createDefaultProject();

        backdoor.project().setProjectLead(projectId, "fred");

        checkResponseForWebHook(webHookName, projectId);
    }

    @Test
    public void testProjectDeletedWebHook() throws Exception {
        final String webHookName = "project_deleted";
        registerWebHook(webHookName);
        final long projectId = createDefaultProject();

        backdoor.project().deleteProject(projectKey);

        checkResponseForWebHook(webHookName, projectId);
    }

    @Test
    public void testWebHookRegisteredWithModuleDescriptor() throws Exception {
        //precondition: there is a listener registered in module descriptor

        final String webHookName = "project_updated";
        final String newLead = "fred";
        final long projectId = createDefaultProject();

        backdoor.project().setProjectLead(projectId, newLead);

        final JSONObject json = new JSONObject(getWebHookResponseFromTestServlet("project_updated"));
        checkReturnedJson(webHookName, projectId, json);
        final String returnedLead = json.getJSONObject("project").getJSONObject("projectLead").getString("name");
        assertThat(returnedLead, equalTo(newLead));
    }

    @Test
    public void testThatProjectWebHooksAreDistinguishable() throws Exception {
        final String projectCreatedWebHookName = "project_created";
        final String projectUpdatedWebHookName = "project_updated";
        final String projectDeletedWebHookName = "project_deleted";
        registerWebHook(projectCreatedWebHookName);
        registerWebHook(projectUpdatedWebHookName);
        registerWebHook(projectDeletedWebHookName);

        final long projectId = createDefaultProject();
        checkResponseForWebHook(projectCreatedWebHookName, projectId);

        backdoor.project().setProjectLead(projectId, "fred");
        checkResponseForWebHook(projectUpdatedWebHookName, projectId);

        backdoor.project().deleteProject(projectKey);
        checkResponseForWebHook(projectDeletedWebHookName, projectId);
    }

    private long createDefaultProject() {
        return backdoor.project().addProject(projectName, projectKey, "admin");
    }

    private void checkResponseForWebHook(final String eventName, final long id) throws Exception {
        final WebHookResponseData responseData = getWebHookResponse();
        assertThat(responseTester.getResponseData(), nullValue());
        assertThat(responseData, notNullValue());

        checkReturnedJson(eventName, id, new JSONObject(responseData.getJson()));
    }

    private void checkReturnedJson(final String eventName, final long id, final JSONObject event)
            throws JSONException {
        assertThat(event, hasField("webhookEvent").equalTo(eventName));

        final JSONObject project = event.getJSONObject("project");
        assertThat(project.getLong("id"), equalTo(id));
        assertThat(project.getString("key"), equalTo(projectKey));
        assertThat(project.getString("name"), equalTo(projectName));
    }
}
