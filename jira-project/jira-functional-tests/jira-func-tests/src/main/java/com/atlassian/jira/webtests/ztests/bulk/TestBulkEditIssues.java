package com.atlassian.jira.webtests.ztests.bulk;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperations;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.Indexing;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import org.jsoup.Jsoup;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BUTTON_NEXT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.COMPONENT_NAME_ONE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_ASSIGNEE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_COMPONENTS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_FIX_VERSIONS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_PRIORITY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_VERSIONS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_SUB_TASK;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PRIORITY_BLOCKER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PRIORITY_MAJOR;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.VERSION_NAME_ONE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.VERSION_NAME_TWO;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.permission.GlobalPermissionKey.BULK_CHANGE;
import static com.atlassian.jira.webtests.Groups.USERS;

@WebTest({Category.FUNC_TEST, Category.BULK_OPERATIONS, Category.ISSUES})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestBulkChangeSetup.xml")
public class TestBulkEditIssues extends BaseJiraFuncTest {

    private static final String HSP_5 = "HSP-5";
    private static final String TST_1 = "TST-1";
    private static final String COMMENT_1 = "This issue is resolved now.";
    private static final String COMMENT_2 = "Viewable by developers group.";
    private static final String COMMENT_3 = "Viewable by Developers role.";
    private static final String UNAVAILABLE_BULK_EDIT_ACTIONS_TABLE_ID = "unavailableActionsTable";
    private static final String SESSION_TIMEOUT_MESSAGE_CONTAINER_LOCATOR = ".aui-message.warning";
    private static final String OPTION_UNASSIGNED = "Unassigned";
    @Inject
    private Indexing indexing;
    @Inject
    private BulkOperations bulkOperations;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        administration.addGlobalPermission(BULK_CHANGE, USERS);
    }

    @After
    public void tearDown() {
        administration.removeGlobalPermission(BULK_CHANGE, USERS);
    }

    @Test
    public void testBulkEditIssues() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        _testBulkEditOneIssueInCurrentPage();
        _testBulkEditOneIssueInAllPages();
        _testBulkEditAllIssuesInAllPages();
        // At the moment this must go last since as a sideeffect it sets HttpUnitOptions.setScriptingEnabled(false);
        _testBulkEditAllIssuesInCurrentPage();
    }

    /**
     * tests to see if editing one issue in the current page works.
     */
    private void _testBulkEditOneIssueInCurrentPage() {
        logger.log("Bulk Change - Edit Operation: ONE issue from CURRENT page");
        final String summary = "EditOneIssueInCurrentPage";
        addCurrentPageLink();
        final String key = addIssue(summary);
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("key", key), ImmutableMap.of("fixVersion", VERSION_NAME_ONE, "version", VERSION_NAME_TWO), key);
        navigation.issueNavigator().createSearch("");
        bulkOperations.bulkChangeIncludeCurrentPage();
        bulkOperations.bulkChangeSelectIssue(key);

        bulkOperations.bulkChangeChooseOperationEdit();
        Map<String, String> fields = new HashMap<>();
        fields.put(FIELD_FIX_VERSIONS, VERSION_NAME_ONE);
        fields.put(FIELD_VERSIONS, VERSION_NAME_TWO);
        fields.put(FIELD_ASSIGNEE, ADMIN_FULLNAME);
        fields.put(FIELD_PRIORITY, PRIORITY_MAJOR);
        bulkOperations.bulkEditOperationDetailsSetAs(fields);
        fields = new HashMap<>();
        fields.put(FIELD_FIX_VERSIONS, VERSION_NAME_ONE);
        fields.put(FIELD_VERSIONS, VERSION_NAME_TWO);
        fields.put(FIELD_ASSIGNEE, ADMIN_FULLNAME);
        fields.put(FIELD_PRIORITY, PRIORITY_MAJOR);
        bulkOperations.bulkEditConfirmEdit(fields);
        bulkOperations.bulkChangeConfirm();
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        //make sure we were redirected the right page
        assertions.getURLAssertions().assertCurrentURLEndsWith("/issues/?jql=");

        //make sure that the individual issue was updated during the bulk edit
        //assertIndexedFieldCorrect("//item", ImmutableMap.of("fixVersion", VERSION_NAME_ONE, "version", VERSION_NAME_TWO, FIELD_ASSIGNEE, ADMIN_FULLNAME, FIELD_PRIORITY, TYPE_PRIORITY_THREE), null, key);
    }

    /**
     * tests to see if editing one issue in all the pages works.
     */
    private void _testBulkEditOneIssueInAllPages() {
        logger.log("Bulk Change - Edit Operation: ONE issue from ALL pages");
        final String summary = "EditOneIssueInAllPages";
        addCurrentPageLink();
        final String key = addIssue(summary);
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();

        bulkOperations.bulkChangeSelectIssue(key);

        bulkOperations.bulkChangeChooseOperationEdit();

        Map<String, String> fields = new HashMap<>();
        fields.put(FIELD_FIX_VERSIONS, VERSION_NAME_ONE);
        fields.put(FIELD_VERSIONS, VERSION_NAME_ONE);
        fields.put(FIELD_COMPONENTS, COMPONENT_NAME_ONE);
        fields.put(FIELD_ASSIGNEE, ADMIN_FULLNAME);
        fields.put(FIELD_PRIORITY, PRIORITY_BLOCKER);
        bulkOperations.bulkEditOperationDetailsSetAs(fields);
        fields = new HashMap<>();
        fields.put(FIELD_FIX_VERSIONS, VERSION_NAME_ONE);
        fields.put(FIELD_VERSIONS, VERSION_NAME_ONE);
        fields.put(FIELD_COMPONENTS, COMPONENT_NAME_ONE);
        fields.put(FIELD_ASSIGNEE, ADMIN_FULLNAME);
        fields.put(FIELD_PRIORITY, PRIORITY_BLOCKER);
        bulkOperations.bulkEditConfirmEdit(fields);
        bulkOperations.bulkChangeConfirm();
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        //make sure we were redirected the right page
        assertions.getURLAssertions().assertCurrentURLEndsWith("/issues/?jql=");
    }

    /**
     * TODO: clean this up AND DEAL WITH JAVASCRIPT ISSUE
     * tests to see if editing all issue in the current page works.
     */
    private void _testBulkEditAllIssuesInCurrentPage() {
        logger.log("Bulk Change - Edit Operation: ALL issue from CURRENT pages");
        administration.generalConfiguration().setAllowUnassignedIssues(true);
        addCurrentPageLink();
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeCurrentPage();

        bulkOperations.bulkChangeChooseIssuesAll();

        bulkOperations.bulkChangeChooseOperationEdit();

        Map<String, String> fields = new HashMap<>();
        fields.put(FIELD_FIX_VERSIONS, VERSION_NAME_TWO);
        fields.put(FIELD_VERSIONS, VERSION_NAME_TWO);
        fields.put(FIELD_COMPONENTS, COMPONENT_NAME_ONE);
        fields.put(FIELD_ASSIGNEE, OPTION_UNASSIGNED);
        bulkOperations.bulkEditOperationDetailsSetAs(fields);
        fields = new HashMap<>();
        fields.put(FIELD_FIX_VERSIONS, VERSION_NAME_TWO);
        fields.put(FIELD_VERSIONS, VERSION_NAME_TWO);
        fields.put(FIELD_COMPONENTS, COMPONENT_NAME_ONE);
        fields.put(FIELD_ASSIGNEE, OPTION_UNASSIGNED);
        bulkOperations.bulkEditConfirmEdit(fields);
        bulkOperations.bulkChangeConfirm();
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        //make sure we were redirected the right page
        assertions.getURLAssertions().assertCurrentURLEndsWith("/issues/?jql=");

        bulkOperations.deleteAllIssuesInAllPages();
        administration.generalConfiguration().setAllowUnassignedIssues(false);
    }

    /**
     * tests to see if editing all issues in all the pages works.
     */
    private void _testBulkEditAllIssuesInAllPages() {
        logger.log("Bulk Change - Edit Operation: ALL issue from ALL pages");
        addCurrentPageLink();

        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();

        bulkOperations.bulkChangeChooseIssuesAll();

        bulkOperations.bulkChangeChooseOperationEdit();

        Map<String, String> fields = new HashMap<>();
        fields.put(FIELD_VERSIONS, VERSION_NAME_TWO);
        fields.put(FIELD_ASSIGNEE, ADMIN_FULLNAME);
        fields.put(FIELD_PRIORITY, PRIORITY_BLOCKER);
        bulkOperations.bulkEditOperationDetailsSetAs(fields);
        fields = new HashMap<>();
        fields.put(FIELD_VERSIONS, VERSION_NAME_TWO);
        fields.put(FIELD_ASSIGNEE, ADMIN_FULLNAME);
        fields.put(FIELD_PRIORITY, PRIORITY_BLOCKER);
        bulkOperations.bulkEditConfirmEdit(fields);
        bulkOperations.bulkChangeConfirm();
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        //make sure we were redirected the right page
        assertions.getURLAssertions().assertCurrentURLEndsWith("/issues/?jql=");

        //assert that the index has been updated for *some* issues

        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("version", VERSION_NAME_TWO, FIELD_ASSIGNEE, ADMIN_FULLNAME, FIELD_PRIORITY, PRIORITY_BLOCKER), null, "HSP-1");
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("version", VERSION_NAME_TWO, FIELD_ASSIGNEE, ADMIN_FULLNAME, FIELD_PRIORITY, PRIORITY_BLOCKER), null, "HSP-5");
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("version", VERSION_NAME_TWO, FIELD_ASSIGNEE, ADMIN_FULLNAME, FIELD_PRIORITY, PRIORITY_BLOCKER), null, "HSP-9");
    }

    // This test is to cover JRA-10167
    @Test
    @Restore("TestBulkEditIssues.xml")
    public void testBulkEditIssuesIssueTypesWithDiffWorkflows() {
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        bulkOperations.bulkChangeSelectIssue(HSP_5);
        bulkOperations.bulkChangeChooseOperationEdit();
        // Make certain that we only have the three options we expect
        assertions.getProjectFieldsAssertions().assertIssueTypesEquals(new String[]{"Test Bug", "Test Improvment", "Test New Feature"});
    }

    // This test will have
    // This test is to cover JRA-10167
    @Test
    @Restore("TestBulkEditIssues.xml")
    public void testBulkEditIssuesIssueTypesWithDiffWorkflowsMultipleIssues() {
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        bulkOperations.bulkChangeSelectIssues(Arrays.asList("HSP-2", TST_1));
        bulkOperations.bulkChangeChooseOperationEdit();
        assertions.getProjectFieldsAssertions().assertIssueTypesEquals(new String[]{"Task"});
    }

    // This test will have no issuetype options because the issues start from
    // different workflows and have no issue types in their issue type schemes
    // that have that workflow in common. This test is to cover JRA-10167.
    @Test
    @Restore("TestBulkEditIssues.xml")
    public void testBulkEditIssuesIssueTypesWithDifferentWorkflowsNoOptions() {
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        bulkOperations.bulkChangeSelectIssues(Arrays.asList(HSP_5, TST_1));
        bulkOperations.bulkChangeChooseOperationEdit();
        textAssertions.assertTextPresent(locator.id(UNAVAILABLE_BULK_EDIT_ACTIONS_TABLE_ID),
                "There are no issue types available for selected issues.");
    }

    // This test expects there to be only one issuetype option because
    // the BUG and TASK will be dropped out by the fact that it has a
    // different workflow configured for those types and the New Feature
    // option has a different field configuration scheme associated with
    // it.
    @Test
    @Restore("TestBulkEditIssues.xml")
    public void testBulkEditIssuesIssueTypesWithDifferentFieldConfigurationSchemes() {
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        bulkOperations.bulkChangeSelectIssue("TST-3");
        bulkOperations.bulkChangeChooseOperationEdit();
        assertions.getProjectFieldsAssertions().assertIssueTypesEquals(new String[]{"Improvement"});
    }


    // This tests that a sub-task issue type will not be shown if it has a different workflow associated
    // with it.
    @Test
    @Restore("TestBulkEditIssues.xml")
    public void testBulkEditIssuesIssueTypesWithDifferentWorkflowsSubtasks() {
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        bulkOperations.bulkChangeSelectIssue("HSP-6");
        bulkOperations.bulkChangeChooseOperationEdit();
        assertions.getProjectFieldsAssertions().assertIssueTypesEquals(new String[]{"Super Sub-taks"});
    }

    // This tests that a sub-task issue type will not be shown if it has a different field configuration scheme
    // associated with it.
    @Test
    @Restore("TestBulkEditIssues.xml")
    public void testBulkEditIssuesIssueTypesWithDiffFieldConfSchemesSubtasks() {
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        bulkOperations.bulkChangeSelectIssue("TST-4");
        bulkOperations.bulkChangeChooseOperationEdit();
        assertions.getProjectFieldsAssertions().assertIssueTypesEquals(new String[]{ISSUE_TYPE_SUB_TASK});
    }

    @Test
    public void testBulkEditSessionTimeouts() {
        logger.log("Bulk Edit - Test that you get redirected to the session timeout page when jumping into the wizard");

        administration.restoreBlankInstance();
        tester.beginAt("secure/views/bulkedit/BulkEditDetails.jspa");
        verifyAtSessionTimeoutPage();
        final String xsrfToken = page.getXsrfToken();
        tester.beginAt("secure/views/bulkedit/BulkEditDetailsValidation.jspa?atl_token=" + xsrfToken);
        verifyAtSessionTimeoutPage();
    }

    private void verifyAtSessionTimeoutPage() {
        textAssertions.assertTextPresent
                (
                        locator.css(SESSION_TIMEOUT_MESSAGE_CONTAINER_LOCATOR),
                        "Your session timed out while performing bulk operation on issues."
                );
    }

    @Test
    @Restore("TestBulkEditIssues.xml")
    public void testBulkEditWithCommentVisibility() {
        navigation.comment().enableCommentGroupVisibility(Boolean.TRUE);

        // comment visible to all users
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        bulkOperations.bulkChangeSelectIssues(Arrays.asList(HSP_5, TST_1));
        bulkOperations.bulkChangeChooseOperationEdit();
        tester.checkCheckbox("actions", "priority");
        tester.selectOption("priority", "Minor");
        tester.checkCheckbox("actions", "comment");
        tester.setFormElement("comment", COMMENT_1);
        tester.submit(BUTTON_NEXT);
        bulkOperations.bulkChangeConfirm();
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        // comment visible to jira-developers group only
        addComment(COMMENT_2, "jira-developers");
        // comment visible to Developers role only
        addComment(COMMENT_3, "Developers");
        // comment visible to Users role
        addComment("This comment should be visible to users role.", "Users");
        // comment visible to jira users
        addComment("This comment should be visible to jira-users group", "jira-users");
        // comment visible to jira admins
        addComment("this comment should be visible to jira admins", "jira-administrators");
        // comment visible to administrators role
        addComment("this comment should be visible to Administrators role", "Administrators");

        final List<String> userComments = ImmutableList.of("This comment should be visible to users role.", "This comment should be visible to jira-users group", COMMENT_1);
        final List<String> developerComments = ImmutableList.of(COMMENT_2, COMMENT_3);
        final List<String> adminComments = ImmutableList.of("this comment should be visible to jira admins", "this comment should be visible to Administrators role");

        // verify that Fred can see general comment but not others as he is not in the visibility groups
        assertions.comments(userComments).areVisibleTo(FRED_USERNAME, "HSP-5");
        assertions.comments(userComments).areVisibleTo(FRED_USERNAME, "TST-1");
        assertions.comments(Iterables.concat(developerComments, adminComments)).areNotVisibleTo(FRED_USERNAME, "HSP-5");
        assertions.comments(Iterables.concat(developerComments, adminComments)).areNotVisibleTo(FRED_USERNAME, "TST-1");

        //admin only guy should not see developer comments
        assertions.comments(Iterables.concat(adminComments, userComments)).areVisibleTo("adminman", "HSP-5");
        assertions.comments(Iterables.concat(adminComments, userComments)).areVisibleTo("adminman", "TST-1");
        assertions.comments(developerComments).areNotVisibleTo("adminman", "HSP-5");
        assertions.comments(developerComments).areNotVisibleTo("adminman", "TST-1");

        //developer guy should not see admin comments
        assertions.comments(Iterables.concat(userComments, developerComments)).areVisibleTo("devman", "HSP-5");
        assertions.comments(Iterables.concat(userComments, developerComments)).areVisibleTo("devman", "TST-1");
        assertions.comments(adminComments).areNotVisibleTo("devman", "HSP-5");
        assertions.comments(adminComments).areNotVisibleTo("devman", "TST-1");

        // verify that Admin can see all comments as he is in all visibility groups
        assertions.comments(Iterables.concat(developerComments, adminComments, userComments)).areVisibleTo(ADMIN_USERNAME, "HSP-5");
        assertions.comments(Iterables.concat(developerComments, adminComments, userComments)).areVisibleTo(ADMIN_USERNAME, "TST-1");
    }

    private void addComment(final String comment, final String commentLevel) {
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        bulkOperations.bulkChangeSelectIssues(Arrays.asList(HSP_5, TST_1));
        bulkOperations.bulkChangeChooseOperationEdit();
        tester.checkCheckbox("actions", "priority");
        tester.selectOption("priority", "Minor");
        tester.checkCheckbox("actions", "comment");
        tester.setFormElement("comment", comment);
        tester.selectOption("commentLevel", commentLevel);
        tester.submit(BUTTON_NEXT);
        bulkOperations.bulkChangeConfirm();
        bulkOperations.waitAndReloadBulkOperationProgressPage();
    }

    /**
     * adds a new issue with the given 'summary'
     *
     * @param summary issue summary
     */
    private String addIssue(final String summary) {
        final IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, summary, ADMIN_USERNAME, PRIORITY_MAJOR, ISSUE_TYPE_BUG);
        backdoor.issues().setIssueFields(issue.key(), new IssueFields()
                .description("description for " + summary)
                .environment("test environment"));
        return issue.key;
    }

    /**
     * Adds an issue until the issue navigator has a next page
     */
    private void addCurrentPageLink() {
        String tableString = backdoor.issueTableClient().setSessionSearch("").getIssueTable().getTable();
        while (tableString == null || Jsoup.parse(tableString).getElementsByClass("icon-next").size() == 0) {
            addIssue("add current page link");
            tableString = backdoor.issueTableClient().setSessionSearch("").getIssueTable().getTable();
        }
    }
}
