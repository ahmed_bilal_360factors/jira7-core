package com.atlassian.jira.webtests.ztests.dashboard.reports;

import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;
import org.w3c.dom.Element;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.Matchers.arrayContaining;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.REPORTS})
@LoginAs(user = ADMIN_USERNAME)
public class TestConfigureReportSelectField extends AbstractConfigureReportFieldTestCase {

    private Element fieldContainer() {
        return (Element)new XPathLocator(tester, "//fieldset[@class = 'group'][select[@id = 'aSelect_select']]").getNode();
    }

    @Test
    public void fieldExists() {
        tester.assertFormElementPresent("aSelect");
    }

    @Test
    public void defaultValueIsFilledIn() {
        formAssertions.assertSelectElementHasOptionSelected("aSelect", "Two");
    }

    @Test
    public void allItemsAreInList() {
        assertThat(tester.getDialog().getOptionValuesFromSelectList("aSelect"),
                arrayContaining("value-one", "value-two", "value-three", "value-four", "value-five"));
    }

    @Test
    public void fieldLabelRendered() {
        textAssertions.assertTextPresent(new XPathLocator(fieldContainer(), "legend"), "A select");
    }

    @Test
    public void fieldDescriptionRendered() {
        textAssertions.assertTextPresent(new XPathLocator(fieldContainer(), "descendant::label[@for='aSelect_select']"), "This is a select field");
    }

    @Test
    public void validationErrorsRendered() {
        tester.gotoPage("/secure/ConfigureReport.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.dev.func-test-plugin:fieldtest-report&aString=fail");
        textAssertions.assertTextPresent(new XPathLocator(fieldContainer(), "*[@class='errMsg']"), "This select field has an error");
    }

    @Test
    public void valueIsSubmittedToReportBean() {
        tester.setFormElement("aSelect", "value-three");
        tester.submit();
        textAssertions.assertTextPresent(new XPathLocator(tester, "//tr[th = 'aSelect']/td"), "value-three");
    }

}
