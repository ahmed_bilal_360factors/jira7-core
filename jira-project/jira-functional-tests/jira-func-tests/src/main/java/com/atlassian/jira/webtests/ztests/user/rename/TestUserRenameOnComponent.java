package com.atlassian.jira.webtests.ztests.user.rename;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.admin.user.DeleteUserPage;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @since v6.0
 */

@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS, Category.RENAME_USER, Category.PROJECTS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUserRenameOnComponent extends BaseJiraFuncTest {
    private static final String LEAN_MEAN_ID = "10100";
    private static final String SMALL_RATTY_ID = "10000";
    private static final String BIG_WUSSY_ID = "10001";
    private static final String WIRY_SKITTISH_ID = "10002";
    @Inject
    private Administration administration;
    @Inject
    private LocatorFactory locator;
    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("user_rename_doggy_components.xml");

        //    KEY       USERNAME    NAME
        //    bb        betty       Betty Boop
        //    ID10001   bb          Bob Belcher
        //    cc        cat         Crazy Cat
        //    ID10101   cc          Candy Chaos

        //    COMPONENT ID  COMPONENT NAME      LEAD_KEY    LEAD_DISPLAY_NAME
        //    10100         Lean & Mean         ID10101     Candy Chaos
        //    10000         Small & Ratty       ID10001     Bob Belcher
        //    10001         Big & Wussy         bb          Betty Boop
        //    10102         Wiry & Skittish     cc          Crazy Cat
    }

    @Test
    public void testJqlFunctionsFindRenamedComponentLeads() {
        Map<String, String[]> componentParamMap = new HashMap<String, String[]>();
        componentParamMap.put("components", new String[]{WIRY_SKITTISH_ID});
        final String wirySkittishIssueKey = navigation.issue().createIssue("Canine", "Task", "Run in circles on the spot", componentParamMap);
        renameUser("cat", "crazy");
        renameUser("bb", "cat");

        // Check recycled user does not inherit their forbear's components
        navigation.gotoResource("../../../rest/api/2/search?jql=component%20in%20componentsLeadByUser(\"cat\")");
        tester.assertTextPresent("\"total\":0");

        // Check renamed user still keeps their components
        navigation.gotoResource("../../../rest/api/2/search?jql=component%20in%20componentsLeadByUser(\"crazy\")");
        tester.assertTextPresent("\"total\":1");
        tester.assertTextPresent(wirySkittishIssueKey);

    }

    @Test
    public void testRenamedComponentLeadAssignedWhenDefault() {
        navigation.issue().goToCreateIssueForm("Canine", null);
        Map<String, String[]> componentParamMap = new HashMap<String, String[]>();
        componentParamMap.put("components", new String[]{SMALL_RATTY_ID});
        final String smallRattyIssueKey = navigation.issue().createIssue("Canine", "Task", "Yip in an annoying fashion", componentParamMap);
        navigation.issue().gotoIssue(smallRattyIssueKey);
        textAssertions.assertTextPresent(locator.id("assignee-val"), "Bob Belcher");

        navigation.issue().goToCreateIssueForm("Canine", null);
        componentParamMap = new HashMap<String, String[]>();
        componentParamMap.put("components", new String[]{WIRY_SKITTISH_ID});
        final String wirySkittishIssueKey = navigation.issue().createIssue("Canine", "Task", "Run in circles on the spot", componentParamMap);
        navigation.issue().gotoIssue(wirySkittishIssueKey);
        textAssertions.assertTextPresent(locator.id("assignee-val"), "Crazy Cat");

        navigation.issue().goToCreateIssueForm("Canine", null);
        componentParamMap = new HashMap<String, String[]>();
        componentParamMap.put("components", new String[]{LEAN_MEAN_ID});
        final String leanMeanIssueKey = navigation.issue().createIssue("Canine", "Task", "Bite off the postie's arm", componentParamMap);
        navigation.issue().gotoIssue(leanMeanIssueKey);
        textAssertions.assertTextPresent(locator.id("assignee-val"), "Candy Chaos");

    }

    @Test
    public void testRenamedComponentLeadElicitsDeletionWarning() {
        final String bettysComponent = "Big & Wussy";
        final String bbsComponent = "Small & Ratty";

        DeleteUserPage deleteUserPage = navigation.gotoPageWithParams(DeleteUserPage.class, DeleteUserPage.generateDeleteQueryParameters("bb"));
        assertThat(deleteUserPage.getUserDeletionError(), equalTo(deleteUserPage.getUserCannotBeDeleteMessage("bb")));
        textAssertions.assertTextPresent(deleteUserPage.getComponentLink(), bbsComponent);


        deleteUserPage = navigation.gotoPageWithParams(DeleteUserPage.class, DeleteUserPage.generateDeleteQueryParameters("betty"));
        assertThat(deleteUserPage.getUserDeletionError(), equalTo(deleteUserPage.getUserCannotBeDeleteMessage("betty")));
        textAssertions.assertTextPresent(deleteUserPage.getComponentLink(), bettysComponent);

        renameUser("bb", "belchyman");
        renameUser("betty", "bb");

        // The users were switched around, so their components should be switched around as well

        deleteUserPage = navigation.gotoPageWithParams(DeleteUserPage.class, DeleteUserPage.generateDeleteQueryParameters("bb"));
        assertThat(deleteUserPage.getUserDeletionError(), equalTo(deleteUserPage.getUserCannotBeDeleteMessage("bb")));
        textAssertions.assertTextPresent(deleteUserPage.getComponentLink(), bettysComponent);
        textAssertions.assertTextNotPresent(deleteUserPage.getComponentLink(), bbsComponent);

        deleteUserPage = navigation.gotoPageWithParams(DeleteUserPage.class, DeleteUserPage.generateDeleteQueryParameters("belchyman"));
        assertThat(deleteUserPage.getUserDeletionError(), equalTo(deleteUserPage.getUserCannotBeDeleteMessage("belchyman")));
        textAssertions.assertTextPresent(deleteUserPage.getComponentLink(), bbsComponent);
        textAssertions.assertTextNotPresent(deleteUserPage.getComponentLink(), bettysComponent);
    }

    private void renameUser(String from, String to) {
        // Rename bb to bob
        navigation.gotoPage(String.format("secure/admin/user/EditUser!default.jspa?editName=%s", from));
        tester.setFormElement("username", to);
        tester.submit("Update");
    }
}
