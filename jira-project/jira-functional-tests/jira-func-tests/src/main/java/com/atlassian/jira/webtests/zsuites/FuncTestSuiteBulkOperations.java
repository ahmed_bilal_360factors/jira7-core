package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.bulk.TestBulkChangeIssues;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkDeleteIssues;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkEditCustomFieldWithNoValidation;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkEditEnvironment;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkEditIssues;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkEditIssuesXss;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkEditUserGroups;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkMoveAttachments;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkMoveIssues;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkMoveIssuesForEnterprise;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkMoveIssuesNotifications;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkMoveMappingVersionsAndComponents;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkMoveWithMultiContexts;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkOperationCustomField;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkOperationIssueNavigator;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkOperationRedirections;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkOperationsIndexing;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkTransition;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkWorkflowTransition;
import com.atlassian.jira.webtests.ztests.email.TestBulkDeleteIssuesNotifications;
import com.atlassian.jira.webtests.ztests.email.TestBulkWorkflowTransitionNotification;
import com.atlassian.jira.webtests.ztests.email.TestSendBulkMail;
import com.atlassian.jira.webtests.ztests.issue.TestLabelsFormats;
import com.atlassian.jira.webtests.ztests.issue.move.TestPromptUserForSecurityLevelOnBulkMove;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of tests related to Bulk Operations
 *
 * @since v4.0
 */
public class FuncTestSuiteBulkOperations {

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestSendBulkMail.class)
                .add(TestBulkEditUserGroups.class)
                .add(TestBulkChangeIssues.class)
                        // Bulk Move Tests
                .add(TestBulkMoveIssues.class)
                .add(TestBulkMoveMappingVersionsAndComponents.class)
                .add(TestBulkMoveIssuesForEnterprise.class)
                .add(TestBulkMoveIssuesNotifications.class)
                        // NOTE: The Bulk Edit tests delete all previously created issues
                .add(TestBulkEditIssues.class)
                .add(TestBulkEditIssuesXss.class)
                .add(TestBulkDeleteIssues.class)
                .add(TestBulkMoveWithMultiContexts.class)
                .add(TestBulkEditEnvironment.class)
                .add(TestBulkEditCustomFieldWithNoValidation.class)
                        // Used to test the bulk transition functions
                .add(TestBulkOperationRedirections.class)
                .add(TestBulkWorkflowTransition.class)
                .add(TestBulkOperationCustomField.class)
                .add(TestBulkOperationsIndexing.class)
                .add(TestBulkOperationIssueNavigator.class)
                .add(TestBulkWorkflowTransitionNotification.class)
                .add(TestBulkDeleteIssuesNotifications.class)
                .add(TestPromptUserForSecurityLevelOnBulkMove.class)
                .add(TestBulkTransition.class)
                .add(TestLabelsFormats.class)
                .add(TestBulkMoveAttachments.class)
                .build();
    }

}