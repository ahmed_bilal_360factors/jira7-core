package com.atlassian.jira.webtests.ztests.application;

import com.atlassian.fugue.Option;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.backdoor.ApplicationRoleControl;
import com.atlassian.jira.functest.framework.backdoor.LicenseControl;
import com.atlassian.jira.functest.framework.backdoor.ServerInfoControl;
import com.atlassian.jira.functest.framework.backdoor.SingleProductLicenseDetailsViewTO;
import com.atlassian.jira.functest.framework.backdoor.application.ApplicationAccessStatus;
import com.atlassian.jira.functest.framework.backdoor.application.ApplicationBean;
import com.atlassian.jira.functest.framework.backdoor.application.ApplicationControl;
import com.atlassian.jira.functest.framework.backdoor.application.PluginApplicationBean;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.matcher.ApplicationRoleBeanMatcher;
import com.atlassian.jira.testkit.client.UsersAndGroupsControl;
import com.atlassian.jira.webtests.LicenseKeys;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.HashSet;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.application.PluginApplicationBean.PluginBean;
import static com.atlassian.jira.functest.framework.backdoor.application.PluginApplicationBean.PluginType;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.CORE_KEY;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.TEST_KEY;
import static com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

/**
 * @since v7.0
 */
@WebTest({Category.FUNC_TEST, Category.API, Category.LICENSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestAtlassianApplication extends BaseJiraFuncTest {
    private static final String ADMIN_GROUP = "jira-administrators";

    @Test
    public void testCoreExists() {
        backdoor.restoreBlankInstance();
        final ApplicationControl applicationControl = backdoor.getApplicationControl();
        backdoor.license().replace(LicenseKeys.CORE_ROLE.getLicenseString());

        ApplicationBean core = applicationControl.getCore();
        assertThat(core.getKey(), equalTo(CORE_KEY));

        final ServerInfoControl.ServerInfo info = backdoor.serverInfo().get();
        assertThat(core.getVersion(), equalTo(info.getVersion()));
        assertThat(core.getBuildDate(), equalTo(info.getBuildDate()));

        SingleProductLicenseDetailsViewTO license = core.getLicense();
        assertThat(license, notNullValue(SingleProductLicenseDetailsViewTO.class));
        assertThat(license.productKey, equalTo(CORE_KEY));

        //Core is reported even if the role is not licensed.
        backdoor.license().replace(LicenseKeys.SOFTWARE_ROLE.getLicenseString());
        core = applicationControl.getCore();
        assertThat(core.getKey(), equalTo(CORE_KEY));
        assertThat(core.getLicense(), nullValue(SingleProductLicenseDetailsViewTO.class));

        assertThat(applicationControl.getCount(CORE_KEY, Option.some(0)), equalTo("users"));
        assertThat(applicationControl.getCount(CORE_KEY, Option.some(1)), equalTo("user"));
        assertThat(applicationControl.getCount(CORE_KEY, Option.some(2)), equalTo("users"));
        assertThat(applicationControl.getCount(CORE_KEY, Option.none(Integer.class)), equalTo("users"));
    }

    @Test
    public void testCoreApplicationAccess() {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_ROLE.getLicenseString());
        final ApplicationControl applicationControl = backdoor.getApplicationControl();

        //Add jira-developers as default and add back admin group
        backdoor.applicationRoles().putRoleWithDefaults(CORE_KEY, newHashSet("jira-developers", ADMIN_GROUP), newHashSet("jira-developers"));

        ApplicationBean application = applicationControl.getApplication(CORE_KEY);
        assertThat(application.getMaxUserCount(), Matchers.equalTo(5));
        assertThat(application.getCurrentUserCount(), Matchers.equalTo(1));

        assertThat(applicationControl.getApplicationAccess(CORE_KEY, ADMIN_USERNAME),
                Matchers.equalTo(ApplicationAccessStatus.OK));

        assertThat(applicationControl.getApplicationAccess(CORE_KEY, FRED_USERNAME),
                Matchers.equalTo(ApplicationAccessStatus.NO_ACCESS));
    }

    @Test
    public void testDummyApplicationExists() {
        backdoor.restoreBlankInstance();
        //Make sure the test application is unlicensed.
        backdoor.license().replace(LicenseKeys.SOFTWARE_ROLE.getLicenseString());

        final ApplicationControl applicationControl = backdoor.getApplicationControl();
        PluginApplicationBean testApp = applicationControl.getPlugin(TEST_KEY);

        assertTestApplicationState(testApp);
        assertThat(testApp.getLicense(), nullValue(SingleProductLicenseDetailsViewTO.class));

        //Make sure the test application is now licensed.
        backdoor.license().replace(LicenseKeys.TEST_ROLE.getLicenseString());

        testApp = applicationControl.getPlugin(TEST_KEY);
        assertTestApplicationState(testApp);
        assertThat(testApp.getLicense(), notNullValue(SingleProductLicenseDetailsViewTO.class));
        assertThat(testApp.getLicense().productKey, equalTo(TEST_KEY));

        assertThat(applicationControl.getCount(TEST_KEY, Option.some(0)), equalTo("testers"));
        assertThat(applicationControl.getCount(TEST_KEY, Option.some(1)), equalTo("tester"));
        assertThat(applicationControl.getCount(TEST_KEY, Option.some(2)), equalTo("too many testers"));
    }

    @Test
    public void testDummyApplicationAccess() {
        backdoor.restoreBlankInstance(LicenseKeys.CORE_ROLE.getLicenseString());
        final UsersAndGroupsControl usersAndGroups = backdoor.usersAndGroups();
        final ApplicationRoleControl roles = backdoor.applicationRoles();
        final LicenseControl license = backdoor.license();
        final ApplicationControl applicationControl = backdoor.getApplicationControl();

        //Add jira-users as default and add back admin group
        roles.putRoleWithDefaults(CORE_KEY, newHashSet("jira-users", ADMIN_GROUP), newHashSet("jira-users"));

        license.set(LicenseKeys.TEST_ROLE.getLicenseString());

        ApplicationBean application = applicationControl.getApplication(TEST_KEY);
        assertThat(application.getMaxUserCount(), Matchers.equalTo(10));
        //Current admin gets added to the default group of the Test Role
        assertThat(application.getCurrentUserCount(), Matchers.equalTo(1));

        final String testGroup = "test-group";
        final String testuser = "testuser";

        usersAndGroups.addGroup(testGroup);
        usersAndGroups.addUser(testuser);
        usersAndGroups.addUserToGroup(testuser, testGroup);

        //Add group to role.
        roles.putRoleAndSetDefault(TEST_KEY, testGroup);

        //Now check that the group count has changed.
        application = applicationControl.getApplication(TEST_KEY);
        assertThat(application.getMaxUserCount(), Matchers.equalTo(10));
        assertThat(application.getCurrentUserCount(), Matchers.equalTo(1));

        assertThat(applicationControl.getApplicationAccess(TEST_KEY, testuser),
                Matchers.equalTo(ApplicationAccessStatus.OK));

        assertThat(applicationControl.getApplicationAccess(TEST_KEY, ADMIN_USERNAME),
                Matchers.equalTo(ApplicationAccessStatus.NO_ACCESS));

        //Remove the test role
        license.replace(LicenseKeys.CORE_ROLE.getLicenseString());
        assertThat(applicationControl.getApplicationAccess(TEST_KEY, testuser),
                Matchers.equalTo(ApplicationAccessStatus.UNLICENSED));

        assertThat(applicationControl.getApplicationAccess(TEST_KEY, ADMIN_USERNAME),
                Matchers.equalTo(ApplicationAccessStatus.UNLICENSED));
        assertThat(applicationControl.getApplicationAccess(TEST_KEY, ADMIN_USERNAME),
                Matchers.equalTo(ApplicationAccessStatus.UNLICENSED));
    }

    @Test
    public void testClearApplicationConfiguration() {
        backdoor.restoreBlankInstance();
        backdoor.license().replace(LicenseKeys.TEST_ROLE.getLicenseString());

        final ApplicationControl appControl = backdoor.getApplicationControl();
        final ApplicationRoleControl appRoleControl = backdoor.applicationRoles();
        final String grpA = "test-group-a";
        final String grpB = "test-group-b";
        backdoor.usersAndGroups().addGroup(grpA);
        backdoor.usersAndGroups().addGroup(grpB);
        appRoleControl.putRoleWithDefaultsSelectedByDefault(TEST_KEY, true, groups(grpA, grpB), groups(grpA));

        assertThat(appRoleControl.getRole(TEST_KEY), matcherForTestRole()
                .setDefaultGroups(grpA)
                .setGroups(grpA, grpB)
                .setSelectedByDefault(true)
                .setNumberOfSeats(10).setRemainingSeats(10).setUserCount(0));

        appControl.clearConfiguration(TEST_KEY);

        assertThat(appRoleControl.getRole(TEST_KEY), matcherForTestRole());
    }

    private HashSet<String> groups(final String... groupsNames) {
        return Sets.newHashSet(groupsNames);
    }

    private ApplicationRoleBeanMatcher matcherForTestRole() {
        return new ApplicationRoleBeanMatcher(TEST_KEY, "Test Product");
    }

    private void assertTestApplicationState(final PluginApplicationBean application) {
        assertThat(application.getKey(), equalTo(TEST_KEY));
        assertThat(application.getName(), equalTo("Test Product"));
        assertThat(application.getDefinitionModuleKey(), equalTo("com.atlassian.jira.dev.func-test-plugin:application"));
        assertThat(application.getConfigurationURI(), equalTo("/configureMe"));
        assertThat(application.getPostInstallURI(), equalTo("/Dashboard.jspa?plugin=jira-func-test-plugin"));
        assertThat(application.getPostUpdateURI(), equalTo("/postUpdate/actions.do"));
        assertThat(application.getDescription(), equalTo("Description about the test application."));
        assertThat(application.getAccessURI(), equalTo(String.format("/secure/admin/user/UserBrowser.jspa?applicationFilter=%s", TEST_KEY)));
        assertThat(application.getDefaultGroup(), equalTo("jira-test-group"));

        assertThat(getPluginTypes(application.getPlugins(), PluginType.PRIMARY),
                contains("com.atlassian.jira.dev.func-test-plugin"));

        assertThat(getPluginTypes(application.getPlugins(), PluginType.APPLICATION),
                containsInAnyOrder("one", "two", "three"));

        assertThat(getPluginTypes(application.getPlugins(), PluginType.UTILITY),
                containsInAnyOrder("four", "five", "six"));
    }

    private Iterable<String> getPluginTypes(Iterable<PluginBean> beans, final PluginType type) {
        return Iterables.transform(Iterables.filter(beans, typePredicate(type)), PluginBean::getKey);
    }

    private static Predicate<PluginBean> typePredicate(final PluginType type) {
        return input -> input.getType() == type;
    }
}
