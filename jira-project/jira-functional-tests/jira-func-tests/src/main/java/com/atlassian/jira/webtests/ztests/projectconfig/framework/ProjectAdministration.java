package com.atlassian.jira.webtests.ztests.projectconfig.framework;

import com.atlassian.jira.functest.framework.Navigation;
import net.sourceforge.jwebunit.WebTester;

/**
 * Represents the Administration page for a specific JIRA Project.
 *
 * @since v5.2
 */
public class ProjectAdministration {
    private final WebTester tester;

    public ProjectAdministration(WebTester tester) {
        this.tester = tester;
    }

    public Fields fields() {
        return new Fields(tester);
    }

    public Screens screens() {
        return new Screens(tester);
    }

    public Permissions permissions() {
        return new Permissions(tester);
    }

    public IssueSecurity issueSecurity() {
        return new IssueSecurity(tester);
    }

    public Notifications notifications() {
        return new Notifications(tester);
    }

    public static abstract class Panel {
        protected final WebTester tester;

        protected Panel(WebTester tester) {
            this.tester = tester;
        }

        boolean isPresent() {
            throw new UnsupportedOperationException("Not implemented");
        }

        public Panel goTo() {
            if (tester.getDialog().isLinkPresent("view_project_" + id() + "_tab")) {
                tester.clickLink("view_project_" + id() + "_tab");
            } else {
                tester.clickLink("view_project_" + id());
            }
            return this;
        }

        abstract String id();
    }

    public static class Screens extends Panel {
        protected Screens(WebTester tester) {
            super(tester);
        }

        @Override
        String id() {
            return "screens";
        }
    }

    public static class Fields extends Panel {
        protected Fields(WebTester tester) {
            super(tester);
        }

        @Override
        String id() {
            return "fields";
        }
    }

    public static class Permissions extends Panel {
        protected Permissions(WebTester tester) {
            super(tester);
        }

        @Override
        String id() {
            return "permissions";
        }
    }

    public static class IssueSecurity extends Panel {
        protected IssueSecurity(WebTester tester) {
            super(tester);
        }

        @Override
        String id() {
            return "issuesecurity";
        }
    }

    public static class Notifications extends Panel {
        protected Notifications(WebTester tester) {
            super(tester);
        }

        @Override
        String id() {
            return "notifications";
        }
    }

    public static ProjectAdministration navigateToProject(final Navigation navigation, final WebTester tester, final String projectKey) {
        navigation.gotoPage("plugins/servlet/project-config/" + projectKey + "/summary");
        return new ProjectAdministration(tester);
    }
}