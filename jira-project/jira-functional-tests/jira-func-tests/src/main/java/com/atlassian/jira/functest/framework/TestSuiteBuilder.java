package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.framework.util.testcase.TestCaseKit;
import com.atlassian.jira.testkit.client.log.FuncTestOut;
import com.atlassian.jira.webtests.util.TestCaseMethodNameDetector;
import junit.framework.JUnit4TestAdapter;
import junit.framework.JUnit4TestAdapterCache;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.junit.Ignore;
import org.junit.runner.Description;
import org.junit.runner.manipulation.Filter;
import org.junit.runner.manipulation.NoTestsRemainException;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Divides up a set of tests such that they can be performed in parallel. It does this by taking a set of tests and
 * dividing them up into composite test of roughly even size.
 * <p>
 */
public final class TestSuiteBuilder {
    private final Set<Class<?>> testClasses = new LinkedHashSet<>();
    private int maxBatch;
    private int batch;
    private boolean log;
    private JUnit4TestAdapterCache defaultCache;

    /**
     * Create a splitter that will return the given batch from the specified number of total batches.
     *
     * @param batch    the batch the splitter should return. A value of -1 can be specified when maxBatch is also passed
     *                 -1. This indicates that no batching should be performed.
     * @param maxBatch the number of batches that splitter should divide the tests into. A value of -1 can be specified
     *                 when batch is also passed -1. This indicates that no batching should be performed.
     */
    public TestSuiteBuilder(final int batch, final int maxBatch) {
        batch(batch).maxBatch(maxBatch).log(false);
    }

    /**
     * Create a no-op splitter, that is, the passed tests will not be batched. Same as @{code TestSplitter(-1, -1) }.
     */
    public TestSuiteBuilder() {
        this(-1, -1);
    }

    private static void outputTest(final TestSuite suite) {
        final Enumeration<?> enumeration = suite.tests();
        while (enumeration.hasMoreElements()) {
            final Test nextTest = (Test) enumeration.nextElement();
            outputTestCase(nextTest);
        }
    }

    private static void outputTestCase(final Test nextTest) {
        FuncTestOut.log(TestCaseKit.getFullName(nextTest));
    }

    /**
     * Add the passed tests to the splitter for division.
     *
     * @param tests the set of tests to divide.
     * @return a reference to this.
     */
    public TestSuiteBuilder addTests(final Collection<Class<?>> tests) {
        testClasses.addAll(tests);
        return this;
    }

    public TestSuiteBuilder log(final boolean log) {
        this.log = log;
        return this;
    }

    public TestSuiteBuilder batch(final int batch) {
        if (batch == 0) {
            throw new IllegalArgumentException("batch == 0");
        }

        this.batch = batch;
        return this;
    }

    public TestSuiteBuilder maxBatch(final int maxBatch) {
        if (maxBatch == 0) {
            throw new IllegalArgumentException("maxBatch == 0");
        }

        this.maxBatch = maxBatch;
        return this;
    }

    /**
     * Return true if and only if the splitter will be batching tests.
     *
     * @return true if and only if the splitter will be batching tests.
     */
    private boolean isBatchMode() {
        return batch >= 0;
    }

    /**
     * Create the composite test that represents the batch.
     *
     * @return the composite test that represents the batch.
     */
    public TestSuite build() {
        return isBatchMode() ? createTestBatch(batch, maxBatch) : createAllTest();
    }

    private TestSuite createAllTest() {
        final TestSuite suite = new TestSuite();
        // Adding entire Test Classes (normal mode)
        getAllTestsStream()
                .forEach(suite::addTest);
        if (log) {
            FuncTestOut.log("** Tests in global **");
            outputTest(suite);
            FuncTestOut.log("** End tests in global **");
        }

        return suite;
    }

    private Stream<Test> getAllTestsStream() {
        return testClasses.stream()
                .flatMap(this::getTestsForClass)
                .filter(val -> val != null);
    }

    private Stream<Test> getTestsForClass(final Class<?> clazz) {
        if (TestCase.class.isAssignableFrom(clazz)) {
            //handle junit 3
            return Collections.list(new TestSuite(clazz).tests())
                    .stream()
                    .filter(test -> {
                        final TestCaseMethodNameDetector detector = new TestCaseMethodNameDetector((TestCase) test);
                        final Method method = detector.resolve();
                        // junit 3 does not understand Ignore class so we will manually ignore those tests until
                        // we completely switch over to junit4
                        return method != null && method.getAnnotation(Ignore.class) == null;
                    });
        } else {
            //this is junit 4
            //getMethods is good enough here as we do not expect to have private test methods
            return Stream.of(clazz.getMethods())
                    .filter(method -> method.getAnnotation(org.junit.Test.class) != null)
                    .map(Method::getName)
                    .map(mapJunit4MethodToTest(clazz));
        }
    }

    private Function<String, Test> mapJunit4MethodToTest(final Class<?> clazz) {
        return method -> {
            final Description testDescription = Description.createTestDescription(clazz, method);
            final JUnit4TestAdapter jUnit4TestAdapter = new JUnit4TestAdapter(clazz) {
                @Override
                public String toString() {
                    return testDescription.toString();
                }
            };
            try {
                jUnit4TestAdapter.filter(Filter.matchMethodDescription(testDescription));
            } catch (NoTestsRemainException e) {
                throw new IllegalStateException(String.format("We should always end up with one test. Test class=%s, method name=%s", clazz.getName(), method), e);
            }
            return jUnit4TestAdapter;
        };
    }

    private TestSuite createTestBatch(final int batchNo, final int maxBatches) {
        checkBatchState(batchNo, maxBatches);

        final List<Test> tests = getAllTestsStream().collect(Collectors.toList());
        int numberOfTests = tests.size();

        final TestSuite suite = new TestSuite();

        int currentBatchSize = numberOfTests / maxBatches;
        if (numberOfTests % maxBatches > 0) {
            currentBatchSize++;
        }

        //Now we know how many tests there are in total. We now need to div them up. We do this by working out
        //what we expect the current size of the batch should be. We continually add tests to the batch until we reach
        //or exceed this size. We then recalculate the batch size and continue until we have divided up all the
        //batches.

        int batch = 1;
        int size = 0;

        for (final Test test : tests) {
            if (batch <= batchNo) {
                if (batch == batchNo) {
                    suite.addTest(test);
                }
                size += test.countTestCases();
                if (size >= currentBatchSize) {
                    numberOfTests -= size;
                    final int remainingBatches = maxBatches - batch;
                    if (remainingBatches > 0) {
                        currentBatchSize = numberOfTests / remainingBatches;
                        if (numberOfTests % remainingBatches > 0) {
                            currentBatchSize++;
                        }
                    } else {
                        assert numberOfTests == 0;
                        currentBatchSize = 0;
                    }
                    batch++;
                    size = 0;
                }
            }
        }

        if (log) {
            FuncTestOut.log(String.format("** Tests in batch %d of %d **", batchNo, maxBatches));
            outputTest(suite);
            FuncTestOut.log(String.format("** End tests in batch %d of %d **", batchNo, maxBatches));
        }
        return suite;
    }

    private void checkBatchState(final int batchNo, final int maxBatches) {
        if (maxBatches < 0) {
            throw new IllegalStateException(String.format("Invalid maxBatch(%d) when batch(%d) >= 0.", maxBatches, batchNo));
        }

        if (batchNo > maxBatches) {
            throw new IllegalStateException(String.format("batch(%d) > maxBatch(%d).", batchNo, maxBatches));
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
