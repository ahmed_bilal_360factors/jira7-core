package com.atlassian.jira.functest.framework.backdoor.webhooks;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.junit.Assert;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlRootElement;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class WebHookRegistrationClient extends RestApiClient<WebHookRegistrationClient> {
    public static final String ISSUES_SECTION_KEY = "issue-related-events-section";

    private final JIRAEnvironmentData environmentData;

    public WebHookRegistrationClient(final JIRAEnvironmentData environmentData) {
        super(environmentData);
        this.environmentData = environmentData;
    }

    public RegistrationResponse register(Registration registration) {
        final ClientResponse response = createResource().type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, registration);
        try {
            Assert.assertEquals("201 code expected, got " + response.getClientResponseStatus(), 201, response.getStatus());

            final RegistrationResponse entity = response.getEntity(RegistrationResponse.class);
            Assert.assertEquals(entity.self, response.getLocation().toString());

            return entity;
        } finally {
            response.close();
        }
    }

    public RegistrationResponse getWebHook(String id) {
        return createResource().path(id).get(RegistrationResponse.class);
    }

    public void delete(String id) {
        createResource().path(id).delete();
    }

    public List<RegistrationResponse> getAllWebHooks() {
        return createResource().get(new GenericType<List<RegistrationResponse>>() {
        });
    }

    public void updateStatus(String id, Registration registration) {
        createResource().path(id).type(MediaType.APPLICATION_JSON_TYPE).put(registration);
    }

    public RegistrationResponse update(String id, Registration registration) {
        return createResource().path(id).type(MediaType.APPLICATION_JSON_TYPE).put(RegistrationResponse.class, registration);
    }

    @Override
    protected WebResource createResource() {
        return resourceRoot(environmentData.getBaseUrl().toExternalForm()).path("rest").path("webhooks").path("1.0").path("webhook");
    }

    public WebResource createResource(String rawUrl) {
        return resourceRoot(rawUrl);
    }

    public void enable(String id) {
        switchEnablement(id, true);
    }

    public void disable(String id) {
        switchEnablement(id, false);
    }

    private void switchEnablement(String id, boolean state) {
        final Registration registration = new Registration();
        registration.enabled = state;
        updateStatus(id, registration);
    }

    @XmlRootElement
    public static class Registration {
        public String name;
        public String url;
        @JsonIgnore
        public String path;
        public String[] events;
        public boolean excludeBody;
        public Map<String, String> filters;
        public boolean enabled = true;

        public Registration() {
            enabled = true;
        }

        public Registration(Registration registration) {
            name = registration.name;
            url = registration.url;
            path = registration.path;
            events = registration.events;
            excludeBody = registration.excludeBody;
            filters = registration.filters;
            enabled = registration.enabled;
        }

        @JsonIgnore
        public void setFilterForIssueSection(String filter) {
            if (filters == null) {
                filters = Maps.newHashMap();
            }
            filters.put(ISSUES_SECTION_KEY, filter);
        }

        @JsonIgnore
        public String getFilterForIssueSection() {
            return Strings.nullToEmpty(Objects.firstNonNull(filters, Collections.<String, String>emptyMap()).get("issue-related-events-section"));
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
        }
    }


    @XmlRootElement
    public static class RegistrationResponse extends Registration {
        public String self;
        public String lastUpdatedUser;
        public String lastUpdatedDisplayName;
        public long lastUpdated;
        public String lastUpdatedShort;
        public boolean enabled = true;

        public RegistrationResponse() {
        }

        public RegistrationResponse(Registration from) throws InvocationTargetException, IllegalAccessException {
            this.name = from.name;
            this.url = from.url;
            this.events = from.events;
            this.filters = from.filters;
        }

        @Override
        public int hashCode() {
            return name.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return EqualsBuilder.reflectionEquals(this, obj, ImmutableList.of("lastUpdated", "lastUpdatedShort")) &&
                    Math.abs(this.lastUpdated - ((RegistrationResponse) obj).lastUpdated) < 5000;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
        }
    }
}
