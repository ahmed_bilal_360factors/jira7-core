package com.atlassian.jira.webtests.ztests.timetracking.legacy;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.TIME_TRACKING, Category.WORKLOGS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestWorkLogTabPanelVisibility extends BaseJiraFuncTest {
    private static final String BUG = "HSP-1";
    private static final String NEW_FEATURE = "HSP-2";

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {

        // restore a backup that hides time tracking field for all issue types except bugs in Homosapien project
        administration.restoreData("TestWorkLogVisibility.xml");
    }

    @Test
    public void testWorkLogTabPanelVisibility() {
        navigation.issue().viewIssue(BUG);
        assertTabLinkPresent();

        navigation.issue().viewIssue(NEW_FEATURE);
        assertTabLinkNotPresent();

        flipVisibilityInFieldConfigEnterprise();

        navigation.issue().viewIssue(BUG);
        assertTabLinkNotPresent();

        navigation.issue().viewIssue(NEW_FEATURE);
        assertTabLinkPresent();
    }

    @Test
    public void testDirectUrlAccessTakesVisibilityIntoAccount() {
        assertTabPresent(BUG);
        assertTabNotPresent(NEW_FEATURE);

        flipVisibilityInFieldConfigEnterprise();

        assertTabNotPresent(BUG);
        assertTabPresent(NEW_FEATURE);
    }

    private void flipVisibilityInFieldConfigEnterprise() {
        navigation.gotoAdminSection(Navigation.AdminSection.FIELD_CONFIGURATION);
        tester.clickLink("configure-Default Field Configuration");
        tester.clickLink("show_18"); // time tracking
        tester.clickLink("view_fieldlayouts");
        tester.clickLink("configure-Bug Field Configuration");
        tester.clickLink("hide_18");    // hide time tracking
    }

    private void assertTabPresent(String issueKey) {
        tester.gotoPage("/browse/" + issueKey + "?page=com.atlassian.jira.plugin.system.issuetabpanels:worklog-tabpanel");
        textAssertions.assertTextSequence(locator.page(), new String[]{"Time Spent", BUG.equals(issueKey) ? "2 hours" : "1 hour", "No comment"});
    }

    private void assertTabNotPresent(String issueKey) {
        tester.gotoPage("/browse/" + issueKey + "?page=com.atlassian.jira.plugin.system.issuetabpanels:worklog-tabpanel");
        tester.assertTextNotPresent("Time Spent");
        tester.assertTextNotPresent(BUG.equals(issueKey) ? "2 hours" : "1 hour");
        tester.assertTextNotPresent("No comment");
    }

    private void assertTabLinkNotPresent() {
        tester.assertLinkNotPresentWithText("Work Log");
    }

    private void assertTabLinkPresent() {
        tester.assertLinkPresentWithText("Work Log");
    }
}
