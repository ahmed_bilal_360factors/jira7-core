package com.atlassian.jira.webtests.ztests.customfield.security.xsrf;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;

/**
 * Responsible for verifying that editing a custom field's default value is not susceptible to xsrf attacks.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.CUSTOM_FIELDS})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestXsrfEditCustomFieldDefaultValue.xml")
public class TestXsrfEditCustomFieldDefaultValue extends BaseJiraFuncTest {
    /**
     * Performs standard check using the {@link com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck}
     * func-test framework classes.
     *
     * @throws Exception An unexpected error.
     */
    @Inject
    private Form form;

    @Test
    public void testWhenSubmittingADodgyTokenAndAValidToken() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck(
                        "Edit Custom Field Default Value",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
                                tester.clickLink("config_customfield_10000");
                                tester.clickLink("customfield_10000-edit-default");
                                tester.setFormElement("customfield_10000", FRED_USERNAME);
                            }
                        },
                        new XsrfCheck.FormSubmission("Set Default"))
        ).run(tester, navigation, form);
    }
}
