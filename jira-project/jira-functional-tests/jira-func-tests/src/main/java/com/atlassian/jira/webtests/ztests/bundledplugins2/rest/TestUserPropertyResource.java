package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.testkit.beans.UserDTO;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.webtests.Permissions;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client.UserPropertyClient;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static com.atlassian.jira.permission.GlobalPermissionKey.GLOBAL_PERMISSION_ID_TRANSLATION;
import static com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client.UserPropertyClient.UserIdentificationMode.userKey;
import static com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client.UserPropertyClient.UserIdentificationMode.username;
import static com.atlassian.jira.webtests.ztests.bundledplugins2.rest.util.PropertyAssertions.assertUniformInterfaceException;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.REST})
public class TestUserPropertyResource extends BaseJiraFuncTest {
    private static AtomicInteger SEQ = new AtomicInteger(1);

    protected UserPropertyClient propertyClient;

    @Before
    public void setUp() {
        backdoor.restoreBlankInstance();
        backdoor.permissions().addGlobalPermission(Permissions.USER_PICKER, "jira-developers");
        backdoor.usersAndGroups().addUserToGroup("fred", "jira-developers");
        propertyClient = new UserPropertyClient(environmentData);
    }

    @Test
    public void propertyCanBeSetAndRead() {
        propertyClient.put("admin", "myProperty", value(5));
        assertThat(propertyClient.get("admin", "myProperty").value, equalTo(valueAsMap(5)));
    }

    @Test
    public void propertyCanBeRetrievedByUsernameAndByKeyAfterUsernameWasChanged() {
        propertyClient.put("fred", "myProperty", value(5));
        renameUser("fred", "fred2");
        assertThat(propertyClient.identifyUserBy(username).get("fred2", "myProperty").value, equalTo(valueAsMap(5)));
        assertThat(propertyClient.identifyUserBy(userKey).get("fred", "myProperty").value, equalTo(valueAsMap(5)));
    }

    @Test
    public void nonAdminUserIsNotAllowedToModifyOtherUsersProperties() {
        assertUniformInterfaceException(() -> {
            propertyClient.loginAs("fred").put("admin", "rogueProp", value(5));
            return null;
        }, Response.Status.FORBIDDEN, "User 'fred' does not have permissions to edit properties of user 'admin'");
    }

    @Test
    public void regularAdminCanAccessAndModifyOtherUsersProperties() {
        createUserWithPermission("globalAdmin", GlobalPermissionKey.ADMINISTER);
        propertyClient.loginAs("globalAdmin");
        assertCanAccessAndEditPropertiesOf("fred");
    }

    @Test
    public void nonAdminUserCanAccessAndEditTheirOwnProperties() {
        propertyClient.loginAs("fred");
        assertCanAccessAndEditPropertiesOf("fred");
    }

    @Test
    public void notFoundResponseIsReturnedForNonExistingUsersWhenWeAreAdmin() {
        assertThat(propertyClient.getKeys("fred").keys, hasSize(0)); // for existing user we get can get the keys
        assertUniformInterfaceException(() -> {
            propertyClient.loginAs("admin").getKeys("nonExistentUser");
            return null;
        }, Response.Status.NOT_FOUND, "Specified user does not exist or you do not have required permissions");
    }

    @Test
    public void regularAdminIsNotAllowedToEditSystemAdminsProperties() {
        createUserWithPermission("regularAdmin", GlobalPermissionKey.ADMINISTER);
        assertUniformInterfaceException(() -> {
            propertyClient.loginAs("regularAdmin").put("admin", "rogueProp", value(42));
            return null;
        }, Response.Status.FORBIDDEN, "User 'regularAdmin' does not have permissions to edit properties of user 'admin'");
    }

    @Test
    public void nonAdminUserIsNotAllowedToAccessOtherUsersProperties() {
        assertUniformInterfaceException(() -> {
            propertyClient.loginAs("fred").getKeys("admin");
            return null;
        }, Response.Status.FORBIDDEN, "User 'fred' does not have permissions to read properties of user 'admin'");
    }

    @Test
    public void regularAdminIsNotAllowedToReadSystemAdminsProperties() {
        createUserWithPermission("regularAdmin", GlobalPermissionKey.ADMINISTER);
        assertUniformInterfaceException(() -> {
            propertyClient.loginAs("regularAdmin").getKeys("admin");
            return null;
        }, Response.Status.FORBIDDEN);
    }

    @Test
    public void anonymousUserIsNotAllowedToAccessAnyProperties() {
        propertyClient.anonymous();
        assertUniformInterfaceException(() -> {
            propertyClient.getKeys("fred");
            return null;
        }, Response.Status.UNAUTHORIZED);
    }

    @Test
    public void propertiesAreClearedAfterUserIsDeleted() {
        propertyClient.put("fred", "farewellProp", value(-1));
        backdoor.usersAndGroups().deleteUser("fred");

        assertUniformInterfaceException(() -> {
            propertyClient.getKeys("fred");
            return null;
        }, Response.Status.NOT_FOUND);

        backdoor.usersAndGroups().addUser("fred");

        assertThat(propertyClient.getKeys("fred").keys, hasSize(0));
    }

    @Test
    public void notFoundIsReturnedWhenAccessingNonExistentProperty() {
        assertUniformInterfaceException(() -> {
            propertyClient.get("admin", "nonExistentProperty");
            return null;
        }, Response.Status.NOT_FOUND);
    }

    protected final void renameUser(String oldKeyName, String newName) {
        backdoor.usersAndGroups().updateUser(new UserDTO(true, 1, oldKeyName, null, oldKeyName, oldKeyName, newName, null));
    }

    protected final void assertCanAccessAndEditPropertiesOf(String username) {
        propertyClient.put(username, "prop", value(42));
        assertThat(propertyClient.getKeys(username).keys, hasSize(1));
        assertThat(propertyClient.get(username, "prop").value, equalTo(valueAsMap(42)));
    }

    protected final void createUserWithPermission(String username, GlobalPermissionKey permission) {
        backdoor.usersAndGroups().addUser(username);
        String groupName = "userGroup-" + SEQ.incrementAndGet();
        backdoor.usersAndGroups().addGroup(groupName);
        backdoor.usersAndGroups().addUserToGroup(username, groupName);
        backdoor.permissions().addGlobalPermission(GLOBAL_PERMISSION_ID_TRANSLATION.inverse().get(permission), groupName);
    }

    protected final JSONObject value(final int value) {
        return new JSONObject(valueAsMap(value));
    }

    protected final Map<String, Object> valueAsMap(final int value) {
        return ImmutableMap.<String, Object>of("value", value);
    }
}
