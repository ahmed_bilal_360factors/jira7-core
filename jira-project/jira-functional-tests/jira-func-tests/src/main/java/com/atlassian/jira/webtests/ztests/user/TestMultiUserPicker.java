package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.BROWSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestMultiUserPicker extends BaseJiraFuncTest {

    /**
     * This is a func test for JRA-14215. It make sure that a username like ${uname} doesn't cause any issues
     */
    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testMultiUserPickerWithStrangeUsername() {
        administration.restoreBlankInstance();
        administration.usersAndGroups().addUser("${uname}", "password", "${fullname}", "user@example.com");
        //now lets got to the Multi user picker and make sure there's no error!
        tester.gotoPage("secure/popups/UserPickerBrowser.jspa?formName=jiraform&multiSelect=true");
        final WebPageLocator locator = new WebPageLocator(tester);
        textAssertions.assertTextNotPresent(locator, "System Error");
        textAssertions.assertTextPresent(locator, "${uname}");
        textAssertions.assertTextPresent(locator, ADMIN_USERNAME);
    }
}
