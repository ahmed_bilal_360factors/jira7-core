package com.atlassian.jira.webtests.ztests.navigator.jql.changehistory.status.daterange;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.ChangeHistoryAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Responsible for verifying that a user is able to query all the issues that were in a particular status bafore a date.
 *
 * @see <a href="https://jdog.atlassian.com/browse/JRADEV-3740">User Story [JRADEV-3740]</a>
 * @since v4.4
 */
@WebTest({Category.FUNC_TEST, Category.JQL, Category.CHANGE_HISTORY})
@Restore("TestSearchIssueStatusBeforeADate.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestSearchIssueStatusBeforeADate extends BaseJiraFuncTest {

    private static final String FIELD_NAME = "status";

    @Inject
    private ChangeHistoryAssertions changeHistoryAssertions;

    @Before
    public void setUpTest() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    public void testInvalidBeforeInput() {
        String expectedError = "The BEFORE predicate must be supplied with only 1 date value.";
        changeHistoryAssertions.assertInvalidSearchProducesError(FIELD_NAME, "Closed", "BEFORE ('2011-07-01','2011-07-01')", expectedError);
        changeHistoryAssertions.assertInvalidSearchProducesError(FIELD_NAME, "Closed", "BEFORE ('2011-07-01','2011-07-03','2012-07-03')", expectedError);
    }


    @Test
    public void testReturnsTheIssuesThatWereInTheStatusGivenThatTheIssuesTransitionedInAndOutOfTheStatusBeforeTheDate() {
        navigation.issueNavigator().createSearch("project=svb and status was 'in progress' before '2011-02-21 10:30' order by key");
        assertions.getIssueNavigatorAssertions().assertExactIssuesInResults("SVB-1");
    }

    @Test
    public void testReturnsTheIssuesThatWereInTheStatusGivenThatTheIssuesTransitionedDirectlyToTheStatusBeforeTheDate() {
        navigation.issueNavigator().createSearch("project=svb and status was 'resolved' before '2011-02-21 11:30' order by key");
        assertions.getIssueNavigatorAssertions().assertExactIssuesInResults("SVB-3", "SVB-4", "SVB-5");
    }

    @Test
    public void testDoesNotReturnIssuesThatWereNeverInThatStatus() {
        navigation.issueNavigator().createSearch("project=svb and status was 'closed' before '2011-02-21 10:30' order by key");
    }

    @Test
    public void testDoesNotReturnIssuesThatTransitionedToTheSpecifiedStatusAtTheDate() {
        navigation.issueNavigator().createSearch("project=svb and status was 'resolved' before '2011-02-21 10:30' order by key");
        assertions.getIssueNavigatorAssertions().assertSearchResultsDoNotContain("SVB-4");
    }

    @Test
    public void testDoesNotReturnIssuesThatTransitionedToTheSpecifiedStatuesJustAfterTheDate() {
        navigation.issueNavigator().createSearch("project=svb and status was 'resolved' before '2011-02-21 10:30' order by key");
        assertions.getIssueNavigatorAssertions().assertSearchResultsDoNotContain("SVB-5");
    }

    @Test
    public void testReturnsIssuesThatTransitionedToTheSpecifiedStatusJustBeforeTheDate() {
        navigation.issueNavigator().createSearch("project=svb and status was 'resolved' before '2011-02-21 10:30' order by key");
        assertions.getIssueNavigatorAssertions().assertExactIssuesInResults("SVB-3");
    }

    @Test
    public void testReturnsIssuesThatTransitionedToTheSpecifiedStatusBeforeTheDateWhenWeHaveOneOrMoreStatii() {
        navigation.issueNavigator().createSearch("project=svb and status was in ('Closed') before '2011-03-29' order by key");
        assertions.getIssueNavigatorAssertions().assertExactIssuesInResults("SVB-2");

        navigation.issueNavigator().createSearch("project=svb and status was in ('In Progress', 'Closed') before '2011-03-29' order by key");
        assertions.getIssueNavigatorAssertions().assertExactIssuesInResults("SVB-1", "SVB-2");

        // we can have multiple values here
        navigation.issueNavigator().createSearch("project=svb and status was in ('In Progress', 'Closed', 'Resolved') before '2011-03-29' order by key");
        assertions.getIssueNavigatorAssertions().assertExactIssuesInResults("SVB-1", "SVB-2", "SVB-3", "SVB-4", "SVB-5");
    }
}
