package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.core.cron.parser.CronExpressionParser.DEFAULT_CRONSTRING;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.UPGRADE_TASKS;
import static org.junit.Assert.assertEquals;

/**
 * @since v6.3 (as TestUpgradeTask6317)
 */
@WebTest({FUNC_TEST, UPGRADE_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUpgradeTask70011 extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    // For this restore file, upgrade task 6317 never ran, so 70011 does all of the migration work
    @Test
    public void testFiltersSubscriptions() {
        administration.restoreData("TestUpgradeTask70011.xml");
        assertFilterSubscriptions();
    }

    // For this restore file, upgrade task 6317 ran, so the subscriptions have already been migrated
    // from the QRTZ tables to the JQUARTZ tables and already have the form that they need to be in
    // for atlassian-scheduler, so 70010 migrates them directly instead of leaving them for 70011 to
    // clean up after the fact.  To ensure that this actually happens, this restore file still has
    // values in the old QRTZ tables, but they are modified so that the test will fail if those actually
    // get used.
    @Test
    public void testFiltersSubscriptions_after6317() {
        administration.restoreData("TestUpgradeTask70011_after6317.xml");
        assertFilterSubscriptions();
    }

    private void assertFilterSubscriptions() {
        assertEquals("0 3 1 ? * *", backdoor.filterSubscriptions().getCronForSubscription(10010L));
        assertEquals("0 0 4 ? * *", backdoor.filterSubscriptions().getCronForSubscription(10022L));
        assertEquals("0 2 1 ? * *", backdoor.filterSubscriptions().getCronForSubscription(10024L));

        // Either 70010 or 70011 should reset this irreparably broken cron expression to the filter sub default
        assertEquals(DEFAULT_CRONSTRING, backdoor.filterSubscriptions().getCronForSubscription(10021L));

        // Either 70010 or 70011 should fix this cron expression by removing the invalid /60 interval from it
        assertEquals("0 0 5 ? * *", backdoor.filterSubscriptions().getCronForSubscription(10023L));
    }
}
