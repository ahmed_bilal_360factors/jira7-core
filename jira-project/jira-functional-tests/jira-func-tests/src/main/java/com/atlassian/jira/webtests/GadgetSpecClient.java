package com.atlassian.jira.webtests;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.google.common.collect.Lists;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * Simple HTTP client to retrieve a specific gadget spec xml and list of all gadgets in the system.
 * <p>
 * Note that {@link RestApiClient} uses its own user session and does not share user sessions from other parts
 * of the test frameworks, such as {@link com.atlassian.jira.functest.framework.Navigation}.
 * <p>
 * By default all HTTP requests will be made using the "admin" credentials. If a different or anonymous credential
 * is required, {@link RestApiClient#loginAs(String)} can be used to facilitate such requirement.
 */
public class GadgetSpecClient extends RestApiClient<GadgetSpecClient> {
    private final String GADGET_SPEC_BASE_URL = "rest/gadgets/1.0/g/";
    private final ObjectMapper objectMapper;
    private final String rootPath;

    public GadgetSpecClient(JIRAEnvironmentData environmentData) {
        super(environmentData);
        this.objectMapper = new ObjectMapper();
        this.rootPath = environmentData.getBaseUrl().toExternalForm();
    }

    private WebResource createGadgetFeedResource() {
        return resourceRoot(rootPath).path("rest").path("gadgets").path("1.0").path("g")
                                     .path("feed");
    }

    private WebResource createGadgetSpecResource() {
        return resourceRoot(rootPath).path("rest").path("gadgets").path("1.0").path("g");
    }

    private WebResource createAddGadgetDirectoryResource() {
        return resourceRoot(rootPath).path("rest").path("config").path("1.0")
                                     .path("directory.json");
    }

    /**
     * Make a HTTP request for all addable gadgets in the system.
     * <p>
     * Addable gadgets refer to all gadgets that can be added/viewed by currently logged on user.
     *
     * @return List of gadget spec url.
     */
    public List<String> getAddableGadgets() {
        Response directoryResponse =
            makeRequestAndExpectStringResponse(createAddGadgetDirectoryResource());
        JsonNode directoryJson = null;
        try {
            directoryJson = objectMapper.readTree((String) directoryResponse.body);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        JsonNode gadgetsListJson = directoryJson.get("gadgets");

        List<String> gadgetSpecUris = Lists.newArrayList();
        for (Iterator<JsonNode> itr = gadgetsListJson.getElements(); itr.hasNext(); ) {
            JsonNode gadgetJsonNode = itr.next();
            JsonNode gadgetSpecUriJsonNode = gadgetJsonNode.get("gadgetSpecUri");
            if (gadgetSpecUriJsonNode == null) {
                continue;
            }

            gadgetSpecUris
                .add(gadgetSpecUriJsonNode.getTextValue().replace(GADGET_SPEC_BASE_URL, ""));
        }
        return gadgetSpecUris;
    }

    /**
     * Make a HTTP request to retrieve the content of gadget feed resource.
     *
     * @return {@code String} content of the feed resource.
     */
    public String getFeedResourceContent() {
        Response feedResponse = makeRequestAndExpectStringResponse(createGadgetFeedResource());
        return (String) feedResponse.body;
    }

    /**
     * Make a HTTP request for gadget spec XML at the specified {@code publishLocation}
     *
     * @param publishLocation The URL to retrieve the gadget spec.
     *
     * @return {@link Response} result of the HTTP request.
     */
    public Response getGadgetSpecXML(String publishLocation) {
        return makeRequestAndExpectStringResponse(createGadgetSpecResource().path(publishLocation));
    }

    private Response makeRequestAndExpectStringResponse(final WebResource webresource) {
        return toResponse(new Method() {
            @Override
            public ClientResponse call() {
                return webresource.get(ClientResponse.class);
            }
        }, String.class);
    }
}
