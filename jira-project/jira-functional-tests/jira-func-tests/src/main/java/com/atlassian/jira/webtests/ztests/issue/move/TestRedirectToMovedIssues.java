package com.atlassian.jira.webtests.ztests.issue.move;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 *
 */
@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.MOVE_ISSUE})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestRedirectToMovedIssues.xml")
public class TestRedirectToMovedIssues extends BaseJiraFuncTest {

    @Before
    public void setUp() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
    }

    /*
        Test data consists of two chains of issue move events:

            HSP-1 -> MKY-2 -> HSP-3
            and
            MKY-1 -> HSP-2`
     */

    @Test
    public void testRedirectToMovedIssueXML() {
        //goto an issue that's been moved
        tester.gotoPage("/si/jira.issueviews:issue-xml/MKY-1/MKY-1.xml");
        //assert that we've been redirected
        assertions.getURLAssertions().assertCurrentURLEndsWith("/si/jira.issueviews:issue-xml/HSP-2/HSP-2.xml");
        //assert that we're definitely looking at an XML view
        tester.assertTextPresent("This file is an XML representation of an issue");
        //assert that we're definitely looking at the right bug
        tester.assertTextPresent("Test Bug 2");
    }

    @Test
    public void testRedirectToMovedIssuePrintable() {
        tester.gotoPage("/si/jira.issueviews:issue-html/MKY-1/MKY-1.html");
        assertions.getURLAssertions().assertCurrentURLEndsWith("/si/jira.issueviews:issue-html/HSP-2/HSP-2.html");
        //assert that we're definitely looking at a Printable view (back to previous view link should be present)
        tester.assertTextPresent("Back to previous view");
        tester.assertTextPresent("Test Bug 2");

    }

    @Test
    public void testRedirectToMovedIssueWord() {
        tester.gotoPage("/si/jira.issueviews:issue-word/MKY-1/MKY-1.doc");
        assertions.getURLAssertions().assertCurrentURLEndsWith("/si/jira.issueviews:issue-word/HSP-2/HSP-2.doc");
        //assert that we're definitely looking at an MS-Word view (META content header should contain "application/vnd.ms-word")
        tester.assertTextPresent("application/vnd.ms-word");
        tester.assertTextPresent("Test Bug 2");
    }

    @Test
    public void testRedirectToMovedIssueBrowseWithQueryString() {
        //only one query string param
        tester.gotoPage("/browse/MKY-1?jql=haha");
        assertions.getURLAssertions().assertCurrentURLEndsWith("/browse/HSP-2?jql=haha");
    }

    @Test
    public void testRedirectToMovedIssueXMLWithQueryString() {
        //couple of query string params, for good luck
        tester.gotoPage("/si/jira.issueviews:issue-html/MKY-1/MKY-1.html?key1=value1&key2=value2");
        assertions.getURLAssertions().assertCurrentURLEndsWith("/si/jira.issueviews:issue-html/HSP-2/HSP-2.html?key1=value1&key2=value2");
    }

}
