package com.atlassian.jira.functest.framework.page;

import com.atlassian.jira.webtests.table.HtmlTable;
import net.sourceforge.jwebunit.WebTester;
import org.xml.sax.SAXException;

import javax.inject.Inject;

/**
 * @since v6.0
 */
public abstract class AbstractWebTestPage implements WebTestPage {

    @Inject
    private WebTester tester;

    protected HtmlTable getTableWithId(String id) {
        try {
            return new HtmlTable(tester.getDialog().getResponse().getTableWithID(id));
        } catch (SAXException e) {
            throw new IllegalStateException(e);
        }
    }

}
