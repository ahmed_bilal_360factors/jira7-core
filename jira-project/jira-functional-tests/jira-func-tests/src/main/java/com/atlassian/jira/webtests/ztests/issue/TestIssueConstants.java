package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.navigation.IssueNavigatorNavigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.matchers.RegexMatchers.regexMatches;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Tests for issue constants (Issue type, priority, status, resolution).
 */
@WebTest({Category.FUNC_TEST, Category.ISSUES})
@Restore("TestIssueConstants.xml")
public class TestIssueConstants extends BaseJiraFuncTest {
    private static final String INVALID_ELEMENT_NAME = "wrong";
    private static final String SUFFIX_HTML = " </td><td><input name=&quot;" + INVALID_ELEMENT_NAME + "&quot;>";
    private static final String SUFFIX_TEXT = " </td><td><input name=\"" + INVALID_ELEMENT_NAME + "\">";
    private static final String SUFFIX_ESC = " &lt;/td&gt;&lt;td&gt;&lt;input name=&quot;" + INVALID_ELEMENT_NAME + "&quot;&gt;";
    private static final String SUFFIX_JSON_ESC = " &lt;/td&gt;&lt;td&gt;&lt;input name=\\&quot;" + INVALID_ELEMENT_NAME + "\\&quot;&gt;";
    private static final String NAME_PREFIX = "name ";
    private static final String DESC_PREFIX = "desc ";

    private static final String ISSUE_TYPE_HTML_OPTION = NAME_PREFIX + "type" + SUFFIX_TEXT;
    private static final String ISSUE_TYPE_NAME_HTML = NAME_PREFIX + "type" + SUFFIX_HTML;
    private static final String ISSUE_TYPE_DESC_HTML = DESC_PREFIX + "type" + SUFFIX_HTML;
    private static final String ISSUE_TYPE_NAME_HTML_ESC = NAME_PREFIX + "type" + SUFFIX_ESC;
    private static final String ISSUE_TYPE_NAME_JSON_ESC = NAME_PREFIX + "type" + SUFFIX_JSON_ESC;
    private static final String ISSUE_TYPE_DESC_HTML_ESC = DESC_PREFIX + "type" + SUFFIX_ESC;
    private static final String ISSUE_TYPE_NAME_TEXT = "New Feature";

    private static final String SUBTASK_TYPE_NAME_HTML = NAME_PREFIX + "subtype" + SUFFIX_HTML;
    private static final String SUBTASK_TYPE_DESC_HTML = DESC_PREFIX + "subtype" + SUFFIX_HTML;
    private static final String SUBTASK_TYPE_NAME_HTML_ESC = NAME_PREFIX + "subtype" + SUFFIX_ESC;
    private static final String SUBTASK_TYPE_DESC_HTML_ESC = DESC_PREFIX + "subtype" + SUFFIX_ESC;

    private static final String PRIORITY_HTML_OPTION = NAME_PREFIX + "priority" + SUFFIX_TEXT;
    private static final String PRIORITY_NAME_HTML = NAME_PREFIX + "priority" + SUFFIX_HTML;
    private static final String PRIORITY_DESC_HTML = DESC_PREFIX + "priority" + SUFFIX_HTML;
    private static final String PRIORITY_NAME_HTML_ESC = NAME_PREFIX + "priority" + SUFFIX_ESC;
    private static final String PRIORITY_DESC_HTML_ESC = DESC_PREFIX + "priority" + SUFFIX_ESC;
    private static final String PRIORITY_NAME_TEXT = "Major";

    private static final String RESOLUTION_NAME_HTML_ESC = NAME_PREFIX + "resolution" + SUFFIX_ESC;
    private static final String RESOLUTION_DESC_HTML_ESC = DESC_PREFIX + "resolution" + SUFFIX_ESC;

    private static final String STATUS_NAME_HTML = NAME_PREFIX + "status" + SUFFIX_HTML;
    private static final String STATUS_DESC_HTML = DESC_PREFIX + "status" + SUFFIX_HTML;
    private static final String STATUS_NAME_HTML_ESC = NAME_PREFIX + "status" + SUFFIX_ESC;
    private static final String STATUS_DESC_HTML_ESC = DESC_PREFIX + "status" + SUFFIX_ESC;
    private static final String STATUS_NAME_TEXT = "In Progress";

    private static final String RESOLVED_STATUS_NAME_HTML = NAME_PREFIX + "resolved" + SUFFIX_HTML;
    private static final String RESOLVED_STATUS_DESC_HTML = DESC_PREFIX + "resolved" + SUFFIX_HTML;
    private static final String RESOLVED_STATUS_NAME_HTML_ESC = NAME_PREFIX + "resolved" + SUFFIX_ESC;
    private static final String RESOLVED_STATUS_DESC_HTML_ESC = DESC_PREFIX + "resolved" + SUFFIX_ESC;

    private static final String TRANSLATED_BUG_NAME = "French_Bug";
    private static final String TRANSLATED_BUG_DESCRIPTION = "French_Desc";

    private static final String FRENCH_LOCALE = "fr_FR";

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        navigation.login(ADMIN_USERNAME);
    }

    @Test
    public void testIssueConstantsAreEncodedOnReports() {
        //single level group by report
        navigation.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:singlelevelgroupby");
        tester.selectOption("mapper", "Issue Type");
        tester.setFormElement("filterid", "10002");
        tester.submit("Next");
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        assertIssueConstantHTMLTitlesPresent();
    }

    @Test
    public void testIssueConstantsAreEncodedOnBulkOperation() {
        //assert on bulk edit all issues confirmation page
        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator().bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES);
        selectIssuesForBulkOperation();
        tester.checkCheckbox("operation", "bulk.edit.operation.name");
        tester.submit("Next");
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        tester.checkCheckbox("actions", "issuetype");
        navigation.issue().selectIssueType(ISSUE_TYPE_HTML_OPTION, "issuetype");
        tester.checkCheckbox("actions", "priority");
        tester.selectOption("priority", PRIORITY_HTML_OPTION);
        tester.submit("Next");
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        assertIssueConstantHTMLTitlesPresent();

        //assert on bulk move issue
        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator().bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES);
        selectIssuesForBulkOperation();
        tester.checkCheckbox("operation", "bulk.move.operation.name");
        tester.submit("Next");
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        assertIssueConstantStraightHTMLNotPresent();

        //assert on bulk transition issue
        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator().bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES);
        selectIssuesForBulkOperation();
        tester.checkCheckbox("operation", "bulk.workflowtransition.operation.name");
        tester.submit("Next");
        tester.checkCheckbox("wftransition", "jira_4_3");
        tester.submit("Next");
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        tester.assertTextNotPresent(STATUS_NAME_HTML);
        tester.assertTextPresent(STATUS_NAME_HTML_ESC);

        //assert on bulk delete issue
        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator().bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES);
        selectIssuesForBulkOperation();
        tester.checkCheckbox("operation", "bulk.delete.operation.name");
        tester.submit("Next");
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        assertIssueConstantStraightHTMLNotPresent();
    }

    @Test
    public void testIssueConstantsAreEncodedOnViewIssue() {
        //check the standard view issue page
        navigation.issue().viewIssue("HSP-3");
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        assertIssueConstantStraightHTMLNotPresent();
        tester.assertTextPresent("title=\"" + ISSUE_TYPE_NAME_HTML_ESC + " - " + ISSUE_TYPE_DESC_HTML_ESC + "\"");
        tester.assertTextPresent("title=\"" + PRIORITY_NAME_HTML_ESC + " - " + PRIORITY_DESC_HTML_ESC + "\"");
        assertLozengePresent(RESOLVED_STATUS_NAME_HTML_ESC);

        tester.assertTextPresent(RESOLUTION_NAME_HTML_ESC);
        //check the quick subtask create form for professional/enterprise
        tester.assertTextPresent(SUBTASK_TYPE_NAME_HTML_ESC);

        //check the subtask's view issue page (for standard, this is just a plain issue still with the subtask type)
        navigation.issue().viewIssue("HSP-4");
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        tester.assertTextNotPresent(SUBTASK_TYPE_NAME_HTML);
        tester.assertTextNotPresent(PRIORITY_NAME_HTML);
        tester.assertTextNotPresent(RESOLVED_STATUS_NAME_HTML);
        tester.assertTextNotPresent(SUBTASK_TYPE_DESC_HTML);
        tester.assertTextNotPresent(PRIORITY_DESC_HTML);
        tester.assertTextNotPresent(RESOLVED_STATUS_DESC_HTML);
        tester.assertTextPresent("title=\"" + SUBTASK_TYPE_NAME_HTML_ESC + " - " + SUBTASK_TYPE_DESC_HTML_ESC + "\"");
        tester.assertTextPresent("title=\"" + PRIORITY_NAME_HTML_ESC + " - " + PRIORITY_DESC_HTML_ESC + "\"");
        assertLozengePresent(STATUS_NAME_HTML_ESC);

        //check view issue's printable view
        navigation.issue().viewPrintable("HSP-3");
        assertIssueConstantHTMLContentViewPresent();
        textAssertions.assertTextSequence(locator.page().getHTML(), "Resolution:", RESOLUTION_NAME_HTML_ESC);
        tester.assertFormNotPresent();

        //check view issues's XML view
        navigation.issue().viewXml("HSP-3");
        assertIssueConstantHTMLPresentInXML();
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "<status id=\"5\"", ">", RESOLVED_STATUS_NAME_HTML_ESC, "</status>");

        //check view issues's Word view (just like looking at printable view)
        navigation.issue().viewIssue("HSP-3");
        tester.clickLinkWithText("Word");
        assertIssueConstantHTMLContentViewPresent();
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Resolution:", RESOLUTION_NAME_HTML_ESC);
    }

    private void assertLozengePresent(final String lozengeContent) {
        assertThat(tester.getDialog().getResponseText(), regexMatches("<span(.*)class=\"(.*)aui-lozenge(.*)\"(.*)>" + lozengeContent + "</span>"));
    }

    @Test
    public void testIssueConstantsAreEncodedOnAdminPage() {
        //manage issue types page
        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_TYPES);
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        final String issueTypesHTML = locator.page().getHTML();
        textAssertions.assertTextSequence(issueTypesHTML, ISSUE_TYPE_NAME_HTML_ESC, ISSUE_TYPE_DESC_HTML_ESC);
        textAssertions.assertTextSequence(issueTypesHTML, SUBTASK_TYPE_NAME_HTML_ESC, SUBTASK_TYPE_DESC_HTML_ESC);
        //check the translation page for the issue types
        navigation.gotoPage("/secure/admin/ViewTranslations!default.jspa?issueConstantType=issuetype");
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        final String issueTypesTranslationsHTML = locator.page().getHTML();
        textAssertions.assertTextSequence(issueTypesTranslationsHTML, ISSUE_TYPE_NAME_HTML_ESC, ISSUE_TYPE_DESC_HTML_ESC);

        //view priorities
        navigation.gotoAdminSection(Navigation.AdminSection.PRIORITIES);
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        final String prioritiesHTML = locator.page().getHTML();
        textAssertions.assertTextSequence(prioritiesHTML, PRIORITY_NAME_HTML_ESC, PRIORITY_DESC_HTML_ESC);
        navigation.gotoPage("/secure/admin/ViewTranslations!default.jspa?issueConstantType=priority");
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        textAssertions.assertTextSequence(prioritiesHTML, PRIORITY_NAME_HTML_ESC, PRIORITY_DESC_HTML_ESC);

        //view resolutions
        navigation.gotoAdminSection(Navigation.AdminSection.RESOLUTIONS);
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        final String resolutionsHTML = locator.page().getHTML();
        textAssertions.assertTextSequence(resolutionsHTML, RESOLUTION_NAME_HTML_ESC, RESOLUTION_DESC_HTML_ESC);
        navigation.gotoPage("/secure/admin/ViewTranslations!default.jspa?issueConstantType=resolution");
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        textAssertions.assertTextSequence(resolutionsHTML, RESOLUTION_NAME_HTML_ESC, RESOLUTION_DESC_HTML_ESC);

        //view statuses
        navigation.gotoAdminSection(Navigation.AdminSection.STATUSES);
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        final String statusesHTML = locator.page().getHTML();
        textAssertions.assertTextSequence(statusesHTML, STATUS_NAME_HTML_ESC, STATUS_DESC_HTML_ESC);
        textAssertions.assertTextSequence(statusesHTML, RESOLVED_STATUS_NAME_HTML_ESC, RESOLVED_STATUS_DESC_HTML_ESC);
        navigation.gotoPage("/secure/admin/ViewTranslations!default.jspa?issueConstantType=status");
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        textAssertions.assertTextSequence(statusesHTML, STATUS_NAME_HTML_ESC, STATUS_DESC_HTML_ESC);
        textAssertions.assertTextSequence(statusesHTML, RESOLVED_STATUS_NAME_HTML_ESC, RESOLVED_STATUS_DESC_HTML_ESC);
    }

    @Test
    public void testIssueConstantsAreEncodedOnEditIssue() {
        navigation.issue().viewIssue("HSP-3");
        tester.clickLink("edit-issue");
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        //for now just assert more than one issue type is available
        tester.assertTextPresent(ISSUE_TYPE_NAME_JSON_ESC);
        tester.assertTextPresent(ISSUE_TYPE_NAME_TEXT);

        //delete all but 1 issue type and check the edit page displays only one issue type (issuetype-edit-not-allowed.vm)
        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_TYPES);
        navigation.gotoPage("/secure/admin/DeleteIssueType!default.jspa?id=4"); // Improvements
        tester.submit("Delete");
        navigation.gotoPage("/secure/admin/DeleteIssueType!default.jspa?id=3"); // Task
        tester.submit("Delete");
        navigation.gotoPage("/secure/admin/DeleteIssueType!default.jspa?id=2"); // New Feature
        tester.submit("Delete");
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        textAssertions.assertTextSequence(locator.page().getHTML(), ISSUE_TYPE_NAME_HTML_ESC, ISSUE_TYPE_DESC_HTML_ESC);
        //now go back to the edit issue page, and check that issuetype cannot be changed (issuetype-edit-not-allowed.vm)
        navigation.issue().viewIssue("HSP-3");
        tester.clickLink("edit-issue");
        tester.assertFormElementNotPresent(INVALID_ELEMENT_NAME);
        tester.assertTextPresent(ISSUE_TYPE_NAME_HTML_ESC);
        tester.assertTextNotPresent(ISSUE_TYPE_NAME_TEXT);
        tester.assertTextPresent("There are no issue types with compatible field configuration and/or workflow associations.");
    }

    @Test
    public void testIssueConstantsAreTranslated() throws IOException {
        try {
            //goto ViewTranslations and choose French locale
            tester.gotoPage("/secure/admin/ViewTranslations!default.jspa?issueConstantType=issuetype&selectedLocale=" + FRENCH_LOCALE);

            // translate bug name and description name
            tester.setWorkingForm("update");
            tester.setFormElement("jira.translation.Issue Type.1.name", TRANSLATED_BUG_NAME);
            tester.setFormElement("jira.translation.Issue Type.1.desc", TRANSLATED_BUG_DESCRIPTION);
            tester.submit("update");
            tester.assertTextPresent(TRANSLATED_BUG_NAME);
            tester.assertTextPresent(TRANSLATED_BUG_DESCRIPTION);
            backdoor.userProfile().changeUserLanguage(ADMIN_USERNAME, FRENCH_LOCALE);

            tester.gotoPage("secure/ShowConstantsHelp.jspa");
            tester.assertTextPresent(TRANSLATED_BUG_NAME);
            tester.assertTextPresent(TRANSLATED_BUG_DESCRIPTION);

            navigation.issue().viewPrintable("HSP-3");
            textAssertions.assertTextSequence(locator.page().getHTML(), "Type:", TRANSLATED_BUG_NAME);
        } finally {
            backdoor.userProfile().changeUserLanguage(ADMIN_USERNAME, "en_UK");
        }
    }

    //--------------------------------------------------------------------------------------------------- helper methods
    private void assertIssueConstantHTMLTitlesPresent() {
        assertIssueConstantStraightHTMLNotPresent();
        tester.assertTextPresent("title=\"" + ISSUE_TYPE_NAME_HTML_ESC + " - " + ISSUE_TYPE_DESC_HTML_ESC);

        assertLozengePresent(STATUS_NAME_HTML_ESC);
        assertLozengePresent(STATUS_NAME_TEXT);
    }

    private void assertIssueConstantHTMLPresentInXML() {
        assertIssueConstantStraightHTMLNotPresent();
        final String pageHtml = tester.getDialog().getResponseText();
        textAssertions.assertTextSequence(pageHtml, new String[]{"<type id=\"1\"", ">", ISSUE_TYPE_NAME_HTML_ESC, "</type>"});
        textAssertions.assertTextSequence(pageHtml, new String[]{"<priority id=\"1\"", ">", PRIORITY_NAME_HTML_ESC, "</priority>"});
        textAssertions.assertTextSequence(pageHtml, new String[]{"<resolution id=\"1\"", ">", RESOLUTION_NAME_HTML_ESC, "</resolution>"});
    }

    private void assertIssueConstantHTMLContentViewPresent() {
        assertIssueConstantStraightHTMLNotPresent();
        final String pageHtml = tester.getDialog().getResponseText();
        textAssertions.assertTextSequence(pageHtml, "Type:", ISSUE_TYPE_NAME_HTML_ESC);
        textAssertions.assertTextSequence(pageHtml, "Priority:", PRIORITY_NAME_HTML_ESC);
        textAssertions.assertTextSequence(pageHtml, "Status:", STATUS_NAME_HTML_ESC);
    }

    private void assertIssueConstantStraightHTMLNotPresent() {
        tester.assertTextNotPresent(ISSUE_TYPE_NAME_HTML);
        tester.assertTextNotPresent(PRIORITY_NAME_HTML);
        tester.assertTextNotPresent(STATUS_NAME_HTML);

        tester.assertTextNotPresent(ISSUE_TYPE_DESC_HTML);
        tester.assertTextNotPresent(PRIORITY_DESC_HTML);
        tester.assertTextNotPresent(STATUS_DESC_HTML);
    }

    private void selectIssuesForBulkOperation() {
        tester.checkCheckbox("bulkedit_10010", "on"); //HSP-3
        tester.checkCheckbox("bulkedit_10001", "on"); //HSP-2
        tester.checkCheckbox("bulkedit_10000", "on"); //HSP-1
        navigation.clickOnNext();
    }
}
