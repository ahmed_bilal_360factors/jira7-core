package com.atlassian.jira.functest.framework.backdoor;


import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.testkit.client.PermissionSchemesControl;
import com.atlassian.jira.testkit.client.UsersAndGroupsControl;
import org.apache.commons.lang.RandomStringUtils;

/**
 * Class abstracting away a process of setting various permissions to documents, e.g. projects.
 *
 * @since v7.0
 */
public class ManagedPermissions {
    private final String userName;
    private final String groupName = withRandomSuffix("test-group");

    private final UsersAndGroupsControl users;
    private final PermissionSchemesControl schemes;
    private final ProjectControlExt project;
    private final Long managedScheme;

    ManagedPermissions(UsersAndGroupsControl users, PermissionSchemesControl schemes, ProjectControlExt project, String userName) {
        this.users = users;
        this.schemes = schemes;
        this.project = project;
        this.userName = userName;
        String schemeName = withRandomSuffix("test-scheme");
        managedScheme = schemes.createScheme(schemeName, "description for " + schemeName);
        addUserIfNotExists(userName);
        users.addGroup(groupName);
        users.addUserToGroup(userName, groupName);
    }

    private static String withRandomSuffix(String name) {
        return name + "-" + RandomStringUtils.randomAlphabetic(8);
    }

    private void addUserIfNotExists(String userName) {
        if (!users.userExists(userName)) {
            users.addUser(userName, userName, userName, userName + "@dev.null");
        }
    }

    public ManagedPermissions forProject(String projectKey) {
        project.setPermissionScheme(project.getProjectId(projectKey), managedScheme);
        return this;
    }

    public ManagedPermissions add(ProjectPermissionKey permissionKey) {
        schemes.addGroupPermission(managedScheme, permissionKey, groupName);
        return this;
    }

    public ManagedPermissions remove(ProjectPermissionKey permissionKey) {
        schemes.removeGroupPermission(managedScheme, permissionKey, groupName);
        return this;
    }
}
