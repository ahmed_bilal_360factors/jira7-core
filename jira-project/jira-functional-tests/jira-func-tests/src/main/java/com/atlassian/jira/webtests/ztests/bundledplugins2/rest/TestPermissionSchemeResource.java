package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.testkit.beans.PermissionGrantBean;
import com.atlassian.jira.testkit.beans.PermissionHolderBean;
import com.atlassian.jira.testkit.beans.PermissionSchemeBean;
import com.atlassian.jira.testkit.client.PermissionSchemesControl;
import com.atlassian.jira.testkit.client.restclient.GenericRestClient;
import com.atlassian.jira.testkit.client.restclient.PermissionSchemeRestClient;
import com.atlassian.jira.testkit.client.restclient.PermissionSchemeRestClient.Expand;
import com.atlassian.jira.testkit.client.restclient.PermissionSchemeRestClient.PermissionGrantListBean;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_GROUPPICKER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_USERPICKER;
import static com.atlassian.jira.matchers.ReflectionMatchers.hasProperty;
import static javax.ws.rs.core.Response.Status;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.REST})
public final class TestPermissionSchemeResource extends BaseJiraRestTest {
    @Inject
    private JIRAEnvironmentData environmentData;

    private PermissionSchemeRestClient client;
    private PermissionSchemesControl permissionSchemes;
    private GenericRestClient genericRestClient;

    @Before
    public void setUp() {
        client = new PermissionSchemeRestClient(environmentData).loginAs("admin", "admin");
        genericRestClient = new GenericRestClient().loginAs("admin", "admin");
        permissionSchemes = backdoor.permissionSchemes();
        backdoor.restoreBlankInstance();
    }

    @Test
    public void defaultPermissionSchemeIsReturnedOnBlankInstance() {
        List<PermissionSchemeBean> permissionSchemes = client.getSchemes().body.permissionSchemes;
        assertThat(permissionSchemes, hasSize(1));
        assertThat(permissionSchemes.get(0).getName(), equalTo("Default Permission Scheme"));
    }

    @Test
    public void permissionsAreNotExpandedByDefaultWhenGettingSingleScheme() {
        long schemeId = setUpPermissionSchemeWithExpandableHolders();
        assertThat(client.getScheme(schemeId).body, hasProperty("permissions", nullValue()));
    }

    @Test
    public void getReturnsCorrectPermissionScheme() {
        Long schemeId = permissionSchemes.createScheme("new scheme", "scheme desc");
        permissionSchemes.addEveryonePermission(schemeId, ProjectPermissions.ADD_COMMENTS);
        permissionSchemes.addGroupPermission(schemeId, ProjectPermissions.ASSIGN_ISSUES, "jira-developers");

        Response<PermissionSchemeBean> scheme = client.getScheme(schemeId, Expand.permissions);
        assertSchemeEquality(scheme.body, new PermissionSchemeBean()
                        .setName("new scheme")
                        .setDescription("scheme desc")
                        .setPermissions(ImmutableList.of(
                                new PermissionGrantBean()
                                        .setPermission("ADD_COMMENTS")
                                        .setHolder(new PermissionHolderBean().setType("anyone")),
                                new PermissionGrantBean()
                                        .setPermission("ASSIGN_ISSUES")
                                        .setHolder(new PermissionHolderBean().setType("group").setParameter("jira-developers"))))
        );
    }

    @Test
    public void projectAdministratorGetsOnlySchemesWhichAreAssignedToProjectsHeAdministers() {
        Long scheme1 = permissionSchemes.createScheme("scheme1", "permission scheme of a project fred administers");
        Long scheme2 = permissionSchemes.createScheme("scheme2", "another permission scheme from a project fred administers");
        permissionSchemes.createScheme("scheme3", "permission fred cannot see");

        long projectId = backdoor.project().addProject("project", "PR", "admin");
        backdoor.project().setPermissionScheme(projectId, scheme1);
        backdoor.permissionSchemes().addUserPermission(scheme1, ProjectPermissions.ADMINISTER_PROJECTS, "fred");

        long projectId2 = backdoor.project().addProject("another project", "PRA", "admin");
        backdoor.project().setPermissionScheme(projectId2, scheme2);
        backdoor.permissionSchemes().addUserPermission(scheme2, ProjectPermissions.ADMINISTER_PROJECTS, "fred");

        List<PermissionSchemeBean> responseForFred = client.loginAs("fred").getSchemes().body.permissionSchemes;
        assertThat(responseForFred, hasSize(2));
        assertThat(responseForFred.get(0).getId(), equalTo(scheme1));
        assertThat(responseForFred.get(1).getId(), equalTo(scheme2));

        assertThat(client.loginAs("admin").getSchemes().body.permissionSchemes, hasSize(4));
    }

    @Test
    public void userCanGetPermissionSchemeIfTheyAdministerTheAssociatedProject() {
        Long scheme1 = permissionSchemes.createScheme("scheme1", "");
        long projectId = backdoor.project().addProject("project", "PR", "admin");
        backdoor.project().setPermissionScheme(projectId, scheme1);
        backdoor.permissionSchemes().addUserPermission(scheme1, ProjectPermissions.ADMINISTER_PROJECTS, "fred");

        assertThat(client.loginAs("fred").getScheme(scheme1, Expand.permissions).body.getId(), equalTo(scheme1));
    }

    @Test
    public void notFoundResponseIsReturnedWhenUserDoesNotHavePermissionToViewAScheme() {
        Long scheme1 = permissionSchemes.createScheme("scheme1", "");
        assertThat(client.loginAs("fred").getScheme(scheme1).statusCode, equalTo(Status.NOT_FOUND.getStatusCode()));
    }

    @Test
    public void creatingPermissionSchemeWorksCorrectly() {
        PermissionSchemeBean expectedScheme = new PermissionSchemeBean()
                .setName("new scheme")
                .setDescription("scheme desc")
                .addPermission(new PermissionGrantBean()
                        .setPermission("ADMINISTER_PROJECTS")
                        .setHolder(new PermissionHolderBean()
                                .setType("group")
                                .setParameter("jira-administrators")));

        Response<PermissionSchemeBean> createdScheme = client.createScheme(expectedScheme, Expand.permissions);

        assertThat(createdScheme.statusCode, equalTo(Status.CREATED.getStatusCode()));
        assertSchemeEquality(createdScheme.body, expectedScheme);
        assertSchemeEquality(client.getScheme(createdScheme.body.id, Expand.permissions).body, expectedScheme);
    }

    @Test
    public void permissionsAreNotAffectedWhenUpdatingOnlyNameAndDescription() {
        Long schemeId = permissionSchemes.createScheme("oldName", "oldDesc");
        permissionSchemes.addEveryonePermission(schemeId, ProjectPermissions.ADD_COMMENTS);

        client.updateScheme(schemeId, new PermissionSchemeBean().setName("newName").setDescription("newDesc"));

        Response<PermissionSchemeBean> scheme = client.getScheme(schemeId, Expand.permissions);

        assertThat(scheme.statusCode, equalTo(Status.OK.getStatusCode()));
        assertThat(scheme.body.getName(), equalTo("newName"));
        assertThat(scheme.body.getDescription(), equalTo("newDesc"));
        assertThat(scheme.body.getPermissions(), hasSize(1));
    }

    @Test
    public void entireSchemeIsUpdatedWhenPermissionListIsSpecified() {
        Long schemeId = permissionSchemes.createScheme("oldName", "oldDesc");
        permissionSchemes.addEveryonePermission(schemeId, ProjectPermissions.ADD_COMMENTS);
        permissionSchemes.addEveryonePermission(schemeId, ProjectPermissions.BROWSE_PROJECTS);

        client.updateScheme(schemeId,
                new PermissionSchemeBean()
                        .setName("newName")
                        .setDescription("newDesc")
                        .setPermissions(ImmutableList.of(new PermissionGrantBean()
                                .setPermission("ADMINISTER_PROJECTS")
                                .setHolder(new PermissionHolderBean()
                                                .setType("group")
                                                .setParameter("jira-administrators")
                                ))));

        Response<PermissionSchemeBean> scheme = client.getScheme(schemeId, Expand.permissions);

        assertThat(scheme.statusCode, equalTo(Status.OK.getStatusCode()));
        assertThat(scheme.body.getName(), equalTo("newName"));
        assertThat(scheme.body.getDescription(), equalTo("newDesc"));
        assertThat(scheme.body.getPermissions(), hasSize(1));
        assertThat(scheme.body.getPermissions().get(0).getPermission(), equalTo("ADMINISTER_PROJECTS"));
    }

    @Test
    public void updatingNonExistingPermissionReturns404() {
        assertThat(client.updateScheme(44L, new PermissionSchemeBean().setName("name")).statusCode, equalTo(Status.NOT_FOUND.getStatusCode()));
    }

    @Test
    public void deletingPermissionSchemeWorksCorrectly() {
        Long schemeId = permissionSchemes.createScheme("name", "desc");

        assertThat(client.getScheme(schemeId).statusCode, equalTo(Status.OK.getStatusCode()));
        assertThat(client.deleteScheme(schemeId).statusCode, equalTo(Status.NO_CONTENT.getStatusCode()));
        assertThat(client.getScheme(schemeId).statusCode, equalTo(Status.NOT_FOUND.getStatusCode()));
    }

    @Test
    public void gettingPermissionsReturnsCorrectResults() {
        Long schemeId = permissionSchemes.createScheme("name", "desc");
        permissionSchemes.addEveryonePermission(schemeId, ProjectPermissions.ADD_COMMENTS);
        permissionSchemes.addGroupPermission(schemeId, ProjectPermissions.ADMINISTER_PROJECTS, "jira-administrators");
        Response<PermissionGrantListBean> permissions = client.getPermissions(schemeId);
        assertThat(permissions.body.permissions, hasSize(2));

        assertPermissionsEquality(permissions.body.permissions, ImmutableList.of(
                new PermissionGrantBean()
                        .setPermission("ADD_COMMENTS")
                        .setHolder(new PermissionHolderBean().setType("anyone")),
                new PermissionGrantBean()
                        .setPermission("ADMINISTER_PROJECTS")
                        .setHolder(new PermissionHolderBean().setType("group").setParameter("jira-administrators"))
        ));
    }

    @Test
    public void creatingSinglePermissionWorksCorrectly() {
        Long schemeId = permissionSchemes.createScheme("name", "desc");
        Response<PermissionGrantBean> permission = client.createPermission(schemeId, new PermissionGrantBean()
                .setPermission("ADD_COMMENTS")
                .setHolder(new PermissionHolderBean().setType("anyone")));

        assertThat(permission.statusCode, equalTo(Status.CREATED.getStatusCode()));

        Response<PermissionGrantBean> createdPermission = client.getPermission(schemeId, permission.body.getId());
        assertThat(createdPermission.body.getPermission(), equalTo("ADD_COMMENTS"));
        assertThat(createdPermission.body.getHolder().getType(), equalTo("anyone"));

        assertThat(client.getPermissions(schemeId).body.permissions, hasSize(1));
    }

    @Test
    public void deletingPermissionWorksCorrectly() {
        Long schemeId = permissionSchemes.createScheme("name", "desc");
        permissionSchemes.addEveryonePermission(schemeId, ProjectPermissions.ADD_COMMENTS);

        List<PermissionGrantBean> permissions = client.getPermissions(schemeId).body.permissions;
        assertThat(permissions, hasSize(1));
        Response<?> clientResponse = client.deletePermission(schemeId, permissions.get(0).getId());
        assertThat(clientResponse.statusCode, equalTo(Status.NO_CONTENT.getStatusCode()));
        assertThat(client.getPermissions(schemeId).body.permissions, hasSize(0));
    }

    @Test
    public void deletingNonExistentPermissionReturns400() {
        Long schemeId = permissionSchemes.createScheme("name", "desc");

        Response<?> clientResponse = client.deletePermission(schemeId, 23232L);
        assertThat(clientResponse.statusCode, equalTo(Status.BAD_REQUEST.getStatusCode()));
    }

    @Test
    public void selfLinksAreCorrect() {
        Long id = permissionSchemes.createScheme("some scheme", "with description");
        permissionSchemes.addEveryonePermission(id, ProjectPermissions.CREATE_ISSUES);
        for (PermissionSchemeBean schemeBean : client.getSchemes().body.permissionSchemes) {
            PermissionSchemeBean scheme = client.getScheme(schemeBean.getId(), Expand.permissions).body;
            assertThat(scheme.getId(), equalTo(genericRestClient.get(schemeBean.getSelf(), PermissionSchemeBean.class).body.getId()));
            for (PermissionGrantBean permission : scheme.getPermissions()) {
                assertThat(client.getPermission(scheme.getId(), permission.getId()).body,
                        equalTo(genericRestClient.get(permission.getSelf(), PermissionGrantBean.class).body));
            }
        }
    }

    @Test
    public void anonymousGets401forAllOperations() {
        client.anonymous();
        assertNotAuthorizedResponse(client.createPermission(0L, new PermissionGrantBean()));
        assertNotAuthorizedResponse(client.createScheme(new PermissionSchemeBean()));
        assertNotAuthorizedResponse(client.deletePermission(0L, 0L));
        assertNotAuthorizedResponse(client.deletePermission(0L, 0L));
        assertNotAuthorizedResponse(client.getPermission(0L, 0L));
        assertNotAuthorizedResponse(client.getScheme(0L));
    }

    @Test
    public void nonAdminGets403WhenEditingPermissionSchemes() {
        client.loginAs("fred");
        assertForbiddenResponse(client.createPermission(0L, new PermissionGrantBean().setPermission("NOT_IMPORTANT").setHolder(new PermissionHolderBean().setType("anyone"))));
        assertForbiddenResponse(client.createScheme(new PermissionSchemeBean().setName("name")));
        assertForbiddenResponse(client.deletePermission(0L, 0L));
    }

    @Test
    public void invalidInputIsHandledCorrectly() {
        assertInvalidInput(new PermissionSchemeBean(), "name", "Required field 'name' was not provided");
        assertInvalidInput(perfectEntity().setPermission(null), "permission", "Required field 'permission' was not provided");
        assertInvalidInput(perfectEntity().setPermission("NON_EXISTENT"), "permission", "Unrecognized permission: NON_EXISTENT");
        assertInvalidInput(perfectEntity().setHolder(new PermissionHolderBean().setType("anyone").setParameter("aa")), "holder.parameter", "Parameter must not be set for holder type: anyone");
        assertInvalidInput(perfectEntity().setHolder(new PermissionHolderBean().setType("group")), "holder.parameter", "Parameter is required for holder type: group");
        assertInvalidInput(perfectEntity().setHolder(new PermissionHolderBean().setType("aaa")), "holder.type", "Unrecognized permission holder type: aaa");
        assertInvalidInput(perfectEntity().setHolder(new PermissionHolderBean()), "holder.type", "Unrecognized holder type: null");
        assertInvalidInput(perfectEntity().setHolder(null), "holder.type", "Unrecognized holder type: null");
    }

    @Test
    public void creatingSchemeWithAlreadyExistingNameResultsInBadRequestReponse() {
        assertInvalidInput(new PermissionSchemeBean().setName("Default Permission Scheme"), null, "Permission scheme with name 'Default Permission Scheme' already exists");
    }

    @Test
    public void tryingToGrantPermissionToNonExistentGroupResultsInInvalidRequest() {
        assertInvalidInput(perfectEntity().setHolder(new PermissionHolderBean().setType("group").setParameter("non-existent group")),
                "holder.parameter", "Group 'non-existent group' does not exist");
    }

    @Test
    public void creatingPermissionWhichAlreadyExistsShouldResultIn400() {
        Long schemeId = permissionSchemes.createScheme("scheme", "desc");
        permissionSchemes.addEveryonePermission(schemeId, ProjectPermissions.ADD_COMMENTS);
        Response<PermissionGrantBean> result = client.createPermission(schemeId, new PermissionGrantBean().setPermission("ADD_COMMENTS").setHolder(new PermissionHolderBean().setType("anyone")));
        assertThat(result.statusCode, equalTo(Status.BAD_REQUEST.getStatusCode()));
        assertThat(result.entity.errorMessages, hasItem("Permission ADD_COMMENTS for ANYONE (parameter: null) already exists in scheme 10000"));
    }

    @Test
    public void everythingIsExpandedWhenExpandingAll() {
        long schemeId = setUpPermissionSchemeWithExpandableHolders();
        Response<PermissionSchemeBean> scheme = client.getScheme(schemeId, Expand.all);
        assertThat(scheme.body.getPermissions(), hasEverythingExpanded());
    }

    @Test
    public void onlySpecifiedTypesAreExpanded() {
        Long schemeId = setUpPermissionSchemeWithExpandableHolders();
        Response<PermissionSchemeBean> scheme = client.getScheme(schemeId, Expand.user, Expand.projectRole);
        assertThat(scheme.body.getPermissions(), hasItem(hasProperty("holder.user.name", equalTo("admin"))));
        assertThat(scheme.body.getPermissions(), hasItem(hasProperty("holder.projectRole.name", equalTo("Users"))));
        assertThat(scheme.body.getPermissions(), not(hasItem(hasProperty("holder.user.field", not(nullValue())))));
        assertThat(scheme.body.getPermissions(), not(hasItem(hasProperty("holder.user.group", not(nullValue())))));
    }

    @Test
    public void permissionsCanBeExpandedWhenGettingAllPermissionSchemes() {
        assertThat(client.getSchemes().body.permissionSchemes, contains(hasProperty("permissions", nullValue())));
        assertThat(client.getSchemes(Expand.permissions).body.permissionSchemes, contains(hasProperty("permissions", not(nullValue()))));
    }

    @Test
    public void permissionsAreExpandedIfAnyExpandParameterIsSpecifiedWhenGettingAllPermissionSchemes() {
        setUpPermissionSchemeWithExpandableHolders();

        assertThat(client.getSchemes(Expand.user).body.permissionSchemes,
                hasItem(hasProperty("permissions", hasItem(hasProperty("holder.user.name", equalTo("admin"))))));

        assertThat(client.getSchemes(Expand.all).body.permissionSchemes,
                hasItem(hasProperty("permissions", hasEverythingExpanded())));
    }

    @Test
    public void holdersCanBeExpandedWhenCreatingNewPermissionGrant() {
        long schemeId = setUpPermissionSchemeWithExpandableHolders();
        PermissionGrantBean grant = new PermissionGrantBean()
                .setPermission("ADMINISTER_PROJECTS")
                .setHolder(new PermissionHolderBean().setType("group").setParameter("jira-developers"));
        Response<PermissionGrantBean> createdPermission = client.createPermission(schemeId, grant, Expand.group);
        assertThat(createdPermission.body, hasProperty("holder.group.name", equalTo("jira-developers")));
    }

    @Test
    public void holdersCanBeExpandedWhenCreatingNewPermissionScheme() {
        PermissionSchemeBean newScheme = new PermissionSchemeBean()
                .setName("name")
                .setPermissions(ImmutableList.of(
                        new PermissionGrantBean()
                                .setPermission("ADMINISTER_PROJECTS")
                                .setHolder(new PermissionHolderBean().setType("group").setParameter("jira-developers"))
                ));

        Response<PermissionSchemeBean> createdScheme = client.createScheme(newScheme, Expand.group);
        assertThat(createdScheme.body.getPermissions(), contains(hasProperty("holder.group.name", equalTo("jira-developers"))));
    }

    private static Matcher<List<PermissionGrantBean>> hasEverythingExpanded() {
        return allOf(ImmutableList.of(
                hasItem(hasProperty("holder.group.name", equalTo("jira-developers"))),
                hasItem(hasProperty("holder.user.name", equalTo("admin"))),
                hasItem(hasProperty("holder.field.name", equalTo("user-custom-field"))),
                hasItem(hasProperty("holder.field.name", equalTo("group-custom-field"))),
                hasItem(hasProperty("holder.projectRole.name", equalTo("Users")))
        ));
    }

    private String fieldType(String actualType) {
        return FunctTestConstants.BUILT_IN_CUSTOM_FIELD_KEY + ":" + actualType;
    }

    private void assertInvalidInput(PermissionGrantBean bean, String field, String message) {
        Response<PermissionGrantBean> response = client.createPermission(0L, bean);
        assertThat(response.statusCode, equalTo(Status.BAD_REQUEST.getStatusCode()));
        assertThat(response.entity.errors, hasEntry(field, message));
    }

    private void assertInvalidInput(PermissionSchemeBean bean, String field, String message) {
        Response<PermissionSchemeBean> response = client.createScheme(bean);
        assertThat(response.statusCode, equalTo(Status.BAD_REQUEST.getStatusCode()));
        if (field != null) {
            assertThat(response.entity.errors, hasEntry(field, message));
        } else {
            assertThat(response.entity.errorMessages, hasItem(message));
        }
    }

    private PermissionGrantBean perfectEntity() {
        return new PermissionGrantBean().setPermission("ADD_COMMENTS").setHolder(new PermissionHolderBean().setType("anyone"));
    }

    private void assertNotAuthorizedResponse(final Response<?> response) {
        assertThat(response.statusCode, equalTo(Status.UNAUTHORIZED.getStatusCode()));
    }

    private void assertForbiddenResponse(final Response<?> response) {
        assertThat(response.statusCode, equalTo(Status.FORBIDDEN.getStatusCode()));
    }

    private void assertSchemeEquality(final PermissionSchemeBean actual, final PermissionSchemeBean expected) {
        assertThat(actual.name, equalTo(expected.getName()));
        assertThat(actual.description, equalTo(expected.getDescription()));
        assertPermissionsEquality(actual.getPermissions(), expected.getPermissions());
    }

    private void assertPermissionsEquality(Collection<PermissionGrantBean> actual, Collection<PermissionGrantBean> expected) {
        for (PermissionGrantBean expectedEntity : expected) {
            assertThat(actual, (Matcher) hasItem(grantEqualTo(expectedEntity)));
        }

        assertThat(actual, hasSize(expected.size()));
    }

    private Matcher<PermissionGrantBean> grantEqualTo(final PermissionGrantBean expectedEntity) {
        return new TypeSafeMatcher<PermissionGrantBean>() {
            @Override
            protected boolean matchesSafely(final PermissionGrantBean item) {
                return item.getHolder().equals(expectedEntity.getHolder()) && item.getPermission().equals(expectedEntity.getPermission());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText(expectedEntity.toString());
            }
        };
    }

    private long setUpPermissionSchemeWithExpandableHolders() {
        String userField = backdoor.customFields().createCustomField("user-custom-field", "desc", fieldType(CUSTOM_FIELD_TYPE_USERPICKER), null);
        String groupField = backdoor.customFields().createCustomField("group-custom-field", "desc2", fieldType(CUSTOM_FIELD_TYPE_GROUPPICKER), null);

        Long schemeId = permissionSchemes.createScheme("scheme", "desc");
        permissionSchemes.addGroupPermission(schemeId, ProjectPermissions.ADD_COMMENTS, "jira-developers");
        permissionSchemes.addUserPermission(schemeId, ProjectPermissions.ADD_COMMENTS, "admin");
        permissionSchemes.addUserCustomFieldPermission(schemeId, ProjectPermissions.ADD_COMMENTS, userField);
        permissionSchemes.addGroupCustomFieldPermission(schemeId, ProjectPermissions.ADD_COMMENTS, groupField);
        permissionSchemes.addProjectRolePermission(schemeId, ProjectPermissions.ADD_COMMENTS, 10000);

        return schemeId;
    }
}
