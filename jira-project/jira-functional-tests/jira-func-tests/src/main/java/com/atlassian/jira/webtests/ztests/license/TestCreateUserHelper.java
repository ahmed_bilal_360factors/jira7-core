package com.atlassian.jira.webtests.ztests.license;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.admin.user.ViewUserPage;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.locator.Locator;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.testkit.client.restclient.Expando;
import com.atlassian.jira.testkit.client.restclient.Group;
import com.atlassian.jira.testkit.client.restclient.User;
import com.atlassian.jira.testkit.client.restclient.UserClient;
import com.google.common.collect.ImmutableList;
import org.w3c.dom.Node;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public abstract class TestCreateUserHelper extends BaseJiraFuncTest {

    private static final String WARNING_DIALOG_XPATH = "//aui-inline-dialog[contains(@id,'inline-dialog2-%s')]";
    @Inject
    protected LocatorFactory locator;
    @Inject
    protected TextAssertions textAssertions;

    protected void populateCreateUserForm(final String userName) {
        navigation.gotoAdminSection(Navigation.AdminSection.CREATE_USER);
        tester.setFormElement("username", userName);
        tester.setFormElement("password", userName);
        tester.setFormElement("fullname", userName);
        tester.setFormElement("email", userName + "@example.com");
    }

    protected void createUser(final String userName) {
        populateCreateUserForm(userName);
        tester.submit("Create");
    }

    protected void gotoViewUserPage(final String username) {
        navigation.gotoPageWithParams(ViewUserPage.class, ViewUserPage.generateViewUserQueryParameters(username));
    }

    protected void assertUserInGroups(final String username, final String... groupNames) {
        gotoViewUserPage(username);
        final UserClient userClient = new UserClient(environmentData);

        final Locator locator = new CssLocator(tester, "#groups.view-user-module .group-name");
        final List<String> actualGroups = Arrays.stream(locator.getNodes())
                .map(locator::getText)
                .collect(CollectorsUtil.toImmutableList());
        assertions.assertEquals("User member of groups", ImmutableList.<String>copyOf(groupNames), actualGroups);

        final Expando<Group> groups = userClient.get(username, User.Expand.groups).groups;
        final ImmutableList<String> collect = groups.items.stream().map(Group::getName).collect(CollectorsUtil.toImmutableList());
        assertions.assertEquals("Verify groups using backdoor", collect, ImmutableList.<String>copyOf(groupNames));
    }

    protected void assertUserNotInAnyGroups(final String username) {
        gotoViewUserPage(username);

        textAssertions.assertTextSequence(new WebPageLocator(tester), new String[]{"This user does not belong to any group."});

        final UserClient userClient = new UserClient(environmentData);
        final List<Group> groups = userClient.get(username, User.Expand.groups).groups.items;
        assertThat(groups, empty());
    }

    protected void assertHasCheckbox(final String checkBoxValue) {
        assertTrue(checkBoxValue + " checkbox does not exist", locator.xpath("//input[@value='" + checkBoxValue + "']").exists());
    }

    protected void assertHasDisabledCheckbox(final String checkBoxValue) {
        assertTrue("Disabled " + checkBoxValue + " checkbox does not exist", locator.xpath("//input[@value='" + checkBoxValue + "'][@disabled]").exists());
    }

    protected Node getWarningDialog(final String applicationKey) {
        return locator.xpath(String.format(WARNING_DIALOG_XPATH, applicationKey)).getNode();
    }

    protected void assertNoCheckboxButWarningButton(final String applicationKey) {
        final Node hook = locator.xpath("//a[contains(@aria-controls,'inline-dialog2-" + applicationKey + "')]").getNode();
        assertFalse(applicationKey + " checkbox exists while it shouldn't", locator.xpath("//input[@value='" + applicationKey + "'][@disabled]").exists());
        assertNotNull(hook);
        assertNotNull(getWarningDialog(applicationKey));
    }

    protected boolean hasNoDefaultGroupWarningForApplication(final String applicationKey) {
        assertNoCheckboxButWarningButton(applicationKey);
        return locator.xpath(String.format(WARNING_DIALOG_XPATH, applicationKey)).getNode().getTextContent().contains("no default group");
    }

    protected boolean hasUserLimitReachedWarningForApplication(final String applicationKey) {
        assertNoCheckboxButWarningButton(applicationKey);
        return locator.xpath(String.format(WARNING_DIALOG_XPATH, applicationKey)).getNode().getTextContent().contains("licenses remaining");
    }
}