package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.CategoryBasedSuite;
import com.google.common.collect.ImmutableSet;
import junit.framework.Test;
import junit.framework.TestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.AllTests;

/**
 * Tests that are run to ensure changes don't have an impact on Atlassian's financial reporting.
 *
 * @see <a href="https://extranet.atlassian.com/x/HChfng">Engineering, it's time to pull our SOX up</a>
 */
@RunWith(AllTests.class)
public class SoxComplianceTestSuite {

    public static Test suite() {
        final TestSuite testSuite = new TestSuite();
        testSuite.addTest(discoverAllTestsWith(Category.LICENSING));
        testSuite.addTest(discoverAllTestsWith(Category.USERS_AND_GROUPS));
        return testSuite;
    }

    private static TestSuite discoverAllTestsWith(final Category category) {
        return new CategoryBasedSuite(
                "com.atlassian.jira.webtests.ztests",
                ImmutableSet.of(category),  // current logic is to return only tests matching ALL specified categories
                ImmutableSet.of(Category.REFERENCE_PLUGIN)  // exclude tests that require the reference plugin installed
        ).get().createTest();
    }
}
