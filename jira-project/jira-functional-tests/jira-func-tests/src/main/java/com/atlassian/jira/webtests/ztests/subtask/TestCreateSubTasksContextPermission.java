package com.atlassian.jira.webtests.ztests.subtask;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.PERMISSIONS, Category.SUB_TASKS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestCreateSubTasksContextPermission extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreData("TestCreateSubTaskContextPermission.xml");
    }

    @Test
    public void testCreateSubTasks() {
        navigation.issue().createSubTask("HSP-1", "Sub-task", "summary", "Subby 99");
        tester.assertTextPresent("Subby 99");
        tester.assertTextPresent("HSP-1");
    }
}
