package com.atlassian.jira.webtests.ztests.issue.move;


import com.atlassian.core.util.map.EasyMap;
import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperationProgress;
import com.atlassian.jira.functest.framework.EditIssueFieldVisibility;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.Indexing;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.webtests.ztests.workflow.ExpectedChangeHistoryItem;
import com.atlassian.jira.webtests.ztests.workflow.ExpectedChangeHistoryRecord;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.javascript.refactoring.Matchers;
import com.meterware.httpunit.WebTable;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Test;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.AFFECTS_VERSIONS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.COMPONENTS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.COMPONENT_NAME_ONE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.COMPONENT_NAME_TWO;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIX_VERSIONS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_NEO;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_NEO_KEY;
import static com.atlassian.jira.functest.framework.Navigation.AdminSection.PERMISSION_SCHEMES;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.permission.ProjectPermissions.MOVE_ISSUES;
import static com.atlassian.jira.rest.api.issue.ResourceRef.withName;
import static com.atlassian.jira.webtests.Groups.DEVELOPERS;
import static com.atlassian.jira.webtests.Groups.USERS;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.MOVE_ISSUE})
@LoginAs(user = ADMIN_USERNAME)
public class TestMoveIssue extends BaseJiraFuncTest {
    private static final String ISSUE_KEY_TO_MOVE = "HSP-1";
    private static final String ISSUE_KEY_NORMAL = "HSP-2";
    private static final String ANA_3 = "ANA-3";
    private static final String CURRENT_ISSUE_TYPE = "Current Issue Type";
    private static final String ANOTHER_TEST_PROJECT = "another test";
    private static final String NEW_ISSUE_TYPE = "New Issue Type";
    private static final String MORE_TESTS_ISSUE_TYPE = "more tests";
    private static final String ISSUETYPE_REQUEST_PARAM = "issuetype";
    private static final String PID_REQUEST_PARAM = "pid";
    private static final String JIRAFORM = "jiraform";
    private static final String MOVE_SUBMIT = "Move";
    private static final String RESULTING_ISSUE_TST_3 = "TST-3";
    private static final String TEST_PROJECT = "Test";
    private static final String TEST_PROJECT_KEY = "TST";
    private static final String MOVE_ISSUE_LINK = "move-issue";
    private static final String NEXT_GT_GT = "Next >>";
    private static final String NEXT = "Next";

    @Inject
    private HtmlPage page;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Indexing indexing;

    @Inject
    private EditIssueFieldVisibility editIssueFieldVisibility;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Inject
    private BulkOperationProgress bulkOperationProgress;

    @After
    public void restoreDefaultUserPicker() {
        backdoor.darkFeatures().disableForSite("no.frother.assignee.field");
    }

    @Test
    @Restore("TestMoveIssue.xml")
    public void testMoveIssue() {

        moveOperationFunctionality(ISSUE_KEY_TO_MOVE);
        moveOperationWithMoveIssuesPermission(ISSUE_KEY_NORMAL);
        moveOperationWithInvalidDueDate(ISSUE_KEY_NORMAL);
        moveOperationWithDueDateRequired(ISSUE_KEY_NORMAL);
        moveOperationWithRequiredFields(ISSUE_KEY_NORMAL);
        navigation.issue().deleteIssue(ISSUE_KEY_NORMAL);
        navigation.issue().deleteIssue(ISSUE_KEY_TO_MOVE);
    }

    @Test
    @Restore("TestMoveIssueIssueTypeAvailable.xml")
    public void testMoveIssueIssueTypeAvailable() {
        logger.log("Move Operation: Test the visibility of the Issue Type field on move.");
        final String projectId = backdoor.project().getProjectId(TEST_PROJECT_KEY).toString();

        //check that the issue has not been moved yet and the unmoved issue is indexed correctly.
        indexing.assertIndexedFieldCorrect("//item", EasyMap.build("key", ANA_3, "type", "another test"), EasyMap.build("key", "TST-3", "title", "[TST-3] Test"), ANA_3);

        navigation.issue().gotoIssue(ANA_3);
        tester.clickLink(MOVE_ISSUE_LINK);

        // Assert that the issue type is prompted on the move issue screen for prof and ent
        tester.assertTextPresent(CURRENT_ISSUE_TYPE);
        tester.assertTextPresent(ANOTHER_TEST_PROJECT);
        tester.assertTextPresent(NEW_ISSUE_TYPE);

        navigation.issue().selectIssueType(MORE_TESTS_ISSUE_TYPE, ISSUETYPE_REQUEST_PARAM);
        tester.setFormElement(PID_REQUEST_PARAM, projectId);
        tester.submit();

        tester.getDialog().setWorkingForm(JIRAFORM);
        tester.submit();

        // Assert that the issue type change values are shown on the move issue screen for prof and ent
        tester.assertTextPresent(ANOTHER_TEST_PROJECT);
        tester.assertTextPresent(MORE_TESTS_ISSUE_TYPE);
        tester.submit(MOVE_SUBMIT);

        // Make sure the move worked.
        tester.assertTextPresent(RESULTING_ISSUE_TST_3);

        //issue type should have been changed
        indexing.assertIndexedFieldCorrect("//item", EasyMap.build("key", RESULTING_ISSUE_TST_3, "type", MORE_TESTS_ISSUE_TYPE, "priority", "Major"), null, RESULTING_ISSUE_TST_3);
    }

    // this test only runs on enterprise edition since it tests component assignee's
    @Test
    @Restore("TestMoveIssueAutomaticAssigneeWithComponents.xml")
    public void testMoveIssueAutomaticAssigneeWithComponents() {
        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field");

            final String issueKeyToMove = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "test move issue", "user1", "Trivial", "Bug").key();
            backdoor.issues().setIssueFields(issueKeyToMove, new IssueFields()
                    .environment("test environment 5")
                    .description("test description to be moved to another project")
                    .components(withName(COMPONENT_NAME_ONE)));


            //index should not have moved the issue yet- there should be no component yet
            indexing.assertIndexedFieldCorrect("//item", EasyMap.build("key", "HSP-1"), EasyMap.build("key", "NDT-1", "component", "New Component 2"), issueKeyToMove);

            final String projectId = backdoor.project().getProjectId(PROJECT_NEO_KEY).toString();

            // move the issue from project homosap to project_neo
            navigation.issue().gotoIssue(issueKeyToMove);
            tester.clickLink(MOVE_ISSUE_LINK);
            tester.setFormElement(PID_REQUEST_PARAM, projectId);
            tester.submit(NEXT_GT_GT);
            tester.selectOption("components", COMPONENT_NAME_TWO);
            tester.submit(NEXT_GT_GT);
            tester.submit(MOVE_SUBMIT);
            // JRA-39112: user1 doesn't have permissions for this project so it should change to user2, because he is
            // the component's lead
            tester.assertTextPresent("user2");

            //index should have updated the issue- specifically, check that the component is being indexed correctly after a move
            indexing.assertIndexedFieldCorrect("//item", EasyMap.build("key", "NDT-1", "component", "New Component 2"), EasyMap.build("key", "HSP-1"), "NDT-1");
        } finally {
            administration.project().deleteProject(PROJECT_HOMOSAP);
            administration.project().deleteProject(PROJECT_NEO);
            navigation.gotoAdminSection(PERMISSION_SCHEMES);
            tester.clickLink("del_" + "test move perm scheme");
            tester.submit("Delete");
            backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGNABLE_USER, USERS);
            backdoor.usersAndGroups().deleteUser("user1");
            backdoor.usersAndGroups().deleteUser("user2");
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field");
        }
    }

    /**
     * Tests if the move operation operates correctly
     */
    public void moveOperationFunctionality(final String issueKey) {

        logger.log("Move Operation: Test the functionality of the 'Move' operation");
        final String projectId = backdoor.project().getProjectId(PROJECT_NEO_KEY).toString();
        navigation.issue().gotoIssue(issueKey);
        tester.clickLink(MOVE_ISSUE_LINK);
        tester.setFormElement(PID_REQUEST_PARAM, projectId);
        tester.submit();
        tester.getDialog().setWorkingForm(JIRAFORM);
        tester.submit();
        tester.assertTextPresent("New Value (after move)");
        tester.submit(MOVE_SUBMIT);
        tester.assertTextNotPresent(PROJECT_HOMOSAP);
        tester.assertTextPresent(PROJECT_NEO);
    }

    /**
     * Tests if the 'Move' Link is available with the 'Move Issues' permission removed
     */
    public void moveOperationWithMoveIssuesPermission(final String issueKey) {
        logger.log("Move Operation: Test the availability of the 'Move' Link with 'Move Issues' Permission.");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, MOVE_ISSUES, DEVELOPERS);
        navigation.issue().gotoIssue(issueKey);
        tester.assertLinkNotPresentWithText(MOVE_SUBMIT);

        //Restore permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, MOVE_ISSUES, DEVELOPERS);
        navigation.issue().gotoIssue(issueKey);
        tester.assertLinkPresentWithText(MOVE_SUBMIT);
    }

    /**
     * Tests the error handling if an invalid 'Due Date' is selected.
     */
    public void moveOperationWithInvalidDueDate(final String issueKey) {
        // Set 'Due Date to be required
        editIssueFieldVisibility.setDueDateToRequried();

        logger.log("Move Operation: selecting invalid due date");
        final String projectId = backdoor.project().getProjectId(PROJECT_MONKEY_KEY).toString();

        navigation.issue().gotoIssue(issueKey);
        tester.clickLink(MOVE_ISSUE_LINK);
        tester.assertTextPresent("Move Issue");

        tester.setFormElement(PID_REQUEST_PARAM, projectId);
        tester.submit();

        tester.setFormElement("duedate", "stuff");
        tester.submit();

        tester.assertTextPresent("Step 3 of 4");
        tester.assertTextPresent("You did not enter a valid date. Please enter the date in the format &quot;d/MMM/yy&quot;");

        // reset fields to optional
        editIssueFieldVisibility.resetFields();
    }

    /**
     * Tests if the 'Due Date' field is available with 'Due Date' set as required
     */
    public void moveOperationWithDueDateRequired(final String issueKey) {
        // Set 'Due Date to be required
        editIssueFieldVisibility.setDueDateToRequried();
        logger.log("Move Operation: testing the availabilty of the 'Due Date' field with 'Due Date' required");
        final String projectId = backdoor.project().getProjectId(PROJECT_MONKEY_KEY).toString();

        navigation.issue().gotoIssue(issueKey);
        tester.clickLink(MOVE_ISSUE_LINK);
        tester.assertTextPresent("Move Issue");

        tester.setFormElement(PID_REQUEST_PARAM, projectId);

        tester.submit();
        tester.assertFormElementPresent("duedate");

        // reset fields to optional
        editIssueFieldVisibility.resetFields();
        navigation.issue().gotoIssue(issueKey);
        tester.clickLink(MOVE_ISSUE_LINK);
        tester.assertTextPresent("Move Issue");

        tester.setFormElement(PID_REQUEST_PARAM, projectId);

        tester.submit();
        tester.assertFormElementNotPresent("duedate");
    }

    /**
     * Tests the error handling if Components, Affects Versions and Fix Verions are set as required
     * and a project without these is selected
     */
    public void moveOperationWithRequiredFields(final String issueKey) {
        // Set fields to be required
        editIssueFieldVisibility.setRequiredFields();

        logger.log("Move Operation: Moving issue with required fields.");
        final String projectId = backdoor.project().getProjectId(PROJECT_NEO_KEY).toString();

        navigation.issue().gotoIssue(issueKey);

        tester.clickLink(MOVE_ISSUE_LINK);
        tester.assertTextPresent("Move Issue");

        tester.setFormElement(PID_REQUEST_PARAM, projectId);
        tester.submit();

        tester.assertTextPresent("Step 3 of 4");

        tester.setWorkingForm(JIRAFORM);
        tester.submit();
        assertErrorMsgFieldRequired(COMPONENTS_FIELD_ID, PROJECT_NEO, "components");
        assertErrorMsgFieldRequired(FIX_VERSIONS_FIELD_ID, PROJECT_NEO, "versions");
        assertErrorMsgFieldRequired(AFFECTS_VERSIONS_FIELD_ID, PROJECT_NEO, "versions");

        // Reset fields to be optional
        editIssueFieldVisibility.resetFields();
    }

    /**
     * Test mapping of workflows schemes that don't match when moving (enterprise only)
     */
    @Test
    @Restore("TestBulkMoveMapWorkflows.xml")
    public void testMoveWithMappingStatus() {
        indexing.assertIndexedFieldCorrect("//item", EasyMap.build("key", "HSP-13", "summary", "bugs3", "status", "Totally Open"), null, "HSP-13");

        navigation.issue().gotoIssue("HSP-13");
        tester.assertTextPresent("Totally Open");
        tester.clickLinkWithText("Move");
        navigation.issue().selectProject("monkey"); //move it to monkey
        moveIssue();
        final String movedIssueKey = backdoor.issueNavControl().getIssueKeyForSummary("bugs3");
        navigation.issue().gotoIssue(movedIssueKey);
        tester.assertTextNotPresent("Totally Open");
        indexing.assertIndexedFieldCorrect("//item", EasyMap.build("status", "Open", "summary", "bugs3", "key", movedIssueKey), EasyMap.build("status", "Totally Open", "key", "HSP-13"), movedIssueKey);
    }

    @Test
    @Restore("TestMoveIssue.xml")
    public void testMoveIssueWithinProject() {
        //make sure moving within a project runs smoothly
        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("move-issue");
        navigation.issue().selectIssueType("New Feature", "issuetype");
        tester.submit(NEXT_GT_GT);
        tester.submit(NEXT_GT_GT);
        tester.submit("Move");

        //make sure that when a new issue is created, the key isn't unnecessarily incremented (JSP-12195)
        navigation.issue().goToCreateIssueForm(null, null);
        tester.setFormElement("summary", "Test Bug 2");
        tester.submit("Create");
        //New issue key is HSP-3 (after HSP-2 which is already present)
        tester.assertLinkPresentWithText("HSP-3");
    }

    @Test
    @Restore("TestMoveIssueWithSubtasks.xml")
    public void testMoveIssueWithSubtasksBetweenProjects() {
        //make sure moving an issue with subtasks between projects runs smoothly
        navigation.issue().gotoIssue("HSP-3");
        tester.clickLink("move-issue");
        navigation.issue().selectProject("neanderthal", "10000_1_pid");
        navigation.issue().selectIssueType("Bug", "10000_1_issuetype");
        tester.submit(NEXT);
        tester.submit(NEXT);
        tester.submit(NEXT);
        tester.submit(NEXT);
        tester.submit(NEXT);
        bulkOperationProgress.waitAndReloadBulkOperationProgressPage();

        //make sure issue keys were generated correctly for issue & subtasks
        tester.assertLinkPresentWithText("NDT-1");
        tester.clickLinkWithText("Sub-task 1");
        tester.assertLinkPresentWithText("NDT-2");
        assertions.assertLastChangeHistoryRecords("NDT-2", new ExpectedChangeHistoryRecord(
                new ExpectedChangeHistoryItem("Key", "HSP-4", "NDT-2"),
                new ExpectedChangeHistoryItem("Project", FunctTestConstants.PROJECT_HOMOSAP, "neanderthal")));
        tester.clickLink("parent_issue_summary");
        tester.clickLinkWithText("Sub-task 2");
        tester.assertLinkPresentWithText("NDT-3");
        assertions.assertLastChangeHistoryRecords("NDT-3", new ExpectedChangeHistoryRecord(
                new ExpectedChangeHistoryItem("Key", "HSP-5", "NDT-3"),
                new ExpectedChangeHistoryItem("Project", FunctTestConstants.PROJECT_HOMOSAP, "neanderthal")));

        //make sure issue key counter was not incremented unnecessarily for issue's old project (JSP-12195)
        navigation.issue().goToCreateIssueForm("homosapien", null);
        tester.setFormElement("summary", "Test Bug 3");
        tester.submit("Create");
        tester.assertLinkPresentWithText("HSP-6");

        //make sure issue key counter was not incremented unnecessarily for issue's new project (JSP-12195)
        navigation.issue().goToCreateIssueForm("neanderthal", null);
        tester.setFormElement("summary", "Test Bug 4");
        tester.submit("Create");
        tester.assertLinkPresentWithText("NDT-4");
    }

    @Test
    @Restore("jra-14416-workflows.xml")
    public void testMoveSubtaskWithDifferentWorkflowAndStatuses() {
        // same Issue Type, different Workflows, different statuses
        navigation.issue().gotoIssue("AA-1");

        tester.clickLink("move-issue");
        navigation.issue().selectProject("B", "10000_1_pid");
        navigation.issue().selectIssueType("Bug", "10000_1_issuetype");
        tester.submit(NEXT);
        tester.submit(NEXT);
        tester.submit(NEXT);

        tester.assertTextPresent("Map Status for Target Project 'B'");
        tester.assertTextPresent("Step 1 of 2");
        tester.selectOption("3", "Three");
        tester.submit(NEXT);
        tester.submit(NEXT);
        tester.submit(NEXT);
        bulkOperationProgress.waitAndReloadBulkOperationProgressPage();

        navigation.issue().gotoIssue("BB-2");
        tester.assertTextPresent("Three"); // check for correct new status
        tester.assertTextPresent("Go To Fourth Step"); // ensure we moved to correct workflow

        assertions.assertLastChangeHistoryRecords("BB-2", new ExpectedChangeHistoryRecord(
                new ExpectedChangeHistoryItem("Key", "AA-2", "BB-2"),
                new ExpectedChangeHistoryItem("Project", "A", "B"),
                new ExpectedChangeHistoryItem("Workflow", "classic default workflow", "B"),
                new ExpectedChangeHistoryItem("Status", "In Progress", "Three")));
    }


    @Test
    @Restore("jra-14416-statuses.xml")
    public void testMoveSubtaskWithDifferentWorkflows() {
        // same Issue Type, different Workflows, same statuses
        navigation.issue().gotoIssue("AA-1");

        tester.clickLink("move-issue");
        navigation.issue().selectProject("B", "10000_1_pid");
        navigation.issue().selectIssueType("Bug", "10000_1_issuetype");
        tester.submit(NEXT);
        tester.submit(NEXT);
        tester.submit(NEXT);
        tester.submit(NEXT);
        tester.submit(NEXT);
        bulkOperationProgress.waitAndReloadBulkOperationProgressPage();

        navigation.issue().gotoIssue("BB-2");
        tester.assertTextPresent("Mark As Complete"); // check that we're on the correct new workflow
        tester.assertTextPresent("Sub-task");
        tester.assertTextPresent("In Progress");

        assertions.assertLastChangeHistoryRecords("BB-2", new ExpectedChangeHistoryRecord(
                new ExpectedChangeHistoryItem("Key", "AA-2", "BB-2"),
                new ExpectedChangeHistoryItem("Project", "A", "B"),
                new ExpectedChangeHistoryItem("Workflow", "jira", "B")));
    }

    // the difference between this test and "testMoveIssueWithSubtasksBetweenProjects" is that this one
    // uses projects that are very different from one another. different issue types, different workflows, different
    // statuses. This means that, in order to "move subtasks" properly we need to migrate all of those things for the
    // subtasks.
    // This comes from JRA-14416
    @Test
    @Restore("jra-14416.xml")
    public void testMoveIssueWithSubtaskIssueTypesBetweenProjects() {
        navigation.issue().gotoIssue("AL-1");
        tester.clickLink("move-issue");
        navigation.issue().selectProject("Baloney", "10000_7_pid");
        navigation.issue().selectIssueType("Issue Type Bentley", "10000_7_issuetype");
        tester.submit(NEXT);

        tester.assertTextPresent("Select Projects and Issue Types for Sub-Tasks");

        // Assert the table 'issuetypechoices'
        textAssertions.assertTextPresent("Subtask Apple");
        textAssertions.assertTextPresent("Subtask Asterisk");
        navigation.issue().selectIssueType("Subtask Bacon", "10000_6_10001_10000_issuetype");
        navigation.issue().selectIssueType("Subtask Butter", "10000_9_10001_10000_issuetype");
        tester.submit(NEXT);
        tester.submit(NEXT);

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field");
            tester.assertTextPresent("Workflow Brown"); // assert that the correct destination workflow is being displayed
            tester.selectOption("10000", "B-Status-2");
            tester.submit(NEXT);
            tester.submit(NEXT);
            tester.submit(NEXT);
            tester.submit(NEXT);
            bulkOperationProgress.waitAndReloadBulkOperationProgressPage();
        } finally {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field");
        }

        navigation.issue().gotoIssue("BA-2");
        tester.assertTextPresent("Subtask Bacon");
        tester.assertTextPresent("Goto Hajime"); // ensure we're on the correct workflow
        assertions.assertLastChangeHistoryRecords("BA-2", new ExpectedChangeHistoryRecord(
                new ExpectedChangeHistoryItem("Key", "AL-2", "BA-2"),
                new ExpectedChangeHistoryItem("Issue Type", "Subtask Apple", "Subtask Bacon"),
                new ExpectedChangeHistoryItem("Project", "Alabaster", "Baloney"),
                new ExpectedChangeHistoryItem("Fix Version/s", "1.5", ""),
                new ExpectedChangeHistoryItem("Affects Version/s", "1.5", ""),
                new ExpectedChangeHistoryItem("Workflow", "Workflow Astaire", "Workflow Brown"),
                new ExpectedChangeHistoryItem("Status", "A-Status-1", "B-Status-2"),
                new ExpectedChangeHistoryItem("Text A", "custom text field data goes here.", ""),
                new ExpectedChangeHistoryItem(FunctTestConstants.COMPONENTS_FIELD_ID, "Comp A", ""),
                new ExpectedChangeHistoryItem("Assignee", "user_a", "admin")));

        navigation.issue().gotoIssue("BA-3");
        tester.assertTextPresent("Subtask Butter");
        assertions.assertLastChangeHistoryRecords("BA-3", new ExpectedChangeHistoryRecord(
                new ExpectedChangeHistoryItem("Key", "AL-3", "BA-3"),
                new ExpectedChangeHistoryItem("Issue Type", "Subtask Asterisk", "Subtask Butter"),
                new ExpectedChangeHistoryItem("Project", "Alabaster", "Baloney")));
    }

    @Test
    @Restore("TestReindexingSubtasks.xml")
    public void testMoveIssueWithSubtasksBetweenProjectsWithSecurityLevel() throws SAXException {
        //this data is good enough for this test.
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        //lets try moving to a project with no security level. Shouldn't ask for sec level to change
        //goto parent issue. Currently has Level Mouse assigned.
        navigation.issue().gotoIssue("RAT-5");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Security Level", "Level Mouse");

        //lets move it to the PIG project (no security level.
        tester.clickLink("move-issue");
        navigation.issue().selectProject("Porcine", "10022_1_pid");
        navigation.issue().selectIssueType("Bug", "10022_1_issuetype");

        tester.submit(NEXT);
        tester.submit(NEXT);
        tester.assertTextPresent("The value of this field must be changed to be valid in the target project");
        tester.submit(NEXT);
        tester.submit(NEXT);
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), "Target Project", "Porcine",
                "Target Issue Type", "Bug",
                "Security Level", "None");
        tester.submit(NEXT);
        bulkOperationProgress.waitAndReloadBulkOperationProgressPage();
        tester.assertTextPresent("Porcine");
        tester.assertTextNotPresent("Rattus");
        tester.assertTextNotPresent("RAT-");
        tester.assertTextNotPresent("Level Mouse");
        //check the sub-tasks sec level
        navigation.issue().gotoIssue("PIG-11");
        tester.assertTextNotPresent("Security Level");

        //now lets go to the issue navigator, and make sure there's no RAT issues and no security leve set to 'Level Mouse'
        navigation.issueNavigator().displayAllIssues();
        tester.assertTextNotPresent("RAT-");
        tester.assertTextNotPresent("Level Mouse");

        WebTable issuetable = tester.getDialog().getWebTableBySummaryOrId("issuetable");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 1, 1, "PIG-11");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 1, 11, "");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 2, 1, "PIG-10");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 2, 11, "");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 3, 1, "PIG-9");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 3, 11, "");

        //then lets try going to a project with a security level.
        navigation.issue().gotoIssue("PIG-9");
        //lets move the issue to the DOG project
        tester.clickLink("move-issue");
        navigation.issue().selectProject("Canine", "10021_1_pid");
        navigation.issue().selectIssueType("Bug", "10021_1_issuetype");
        tester.submit(NEXT);
        tester.submit(NEXT);
        //need to select a security level now.
        tester.selectOption("security", "Level Green");
        tester.submit(NEXT);
        tester.submit(NEXT);
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), "Target Project", "Canine",
                "Target Issue Type", "Bug",
                "Security Level", "Level Green");
        tester.submit(NEXT);
        bulkOperationProgress.waitAndReloadBulkOperationProgressPage();
        tester.assertTextNotPresent("PIG");
        tester.assertTextNotPresent("Porcine");
        tester.assertTextPresent("Canine");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Security Level:", "Level Green");
        //check the sub-tasks sec level
        navigation.issue().gotoIssue("DOG-11");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Security Level:", "Level Green");

        //check the issue navigator now. There should be no PIG issues and all DOG issues should have Level Green set
        navigation.issueNavigator().displayAllIssues();
        tester.assertTextNotInTable("issuetable", "PIG");

        issuetable = tester.getDialog().getWebTableBySummaryOrId("issuetable");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 1, 1, "DOG-11");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 1, 11, "Level Green");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 2, 1, "DOG-10");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 2, 11, "Level Green");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 3, 1, "DOG-9");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 3, 11, "Level Green");
    }

    //JRA-16007
    @Test
    @Restore("TestMoveIssueWithoutVersionPermission.xml")
    public void testMoveIssueWithoutVersionPermission() throws SAXException {
        navigation.issue().gotoIssue("HSP-1");

        //move the issue to the 'monkey' project (which will be selected by default)
        // Click Link 'Move' (id='move_issue').
        tester.clickLink("move-issue");
        navigation.issue().selectProject("monkey");
        tester.submit(NEXT_GT_GT);
        tester.submit(NEXT_GT_GT);

        //check the confirm screen shows that the versions will be set to nothing.
        final int lastRow = tester.getDialog().getWebTableBySummaryOrId("move_confirm_table").getRowCount() - 1;
        final WebTable moveConfirmTable = tester.getDialog().getWebTableBySummaryOrId("move_confirm_table");
        assertions.getTableAssertions().assertTableCellHasText(moveConfirmTable, 1, 1, "homosapien");
        assertions.getTableAssertions().assertTableCellHasText(moveConfirmTable, 1, 2, "monkey");
        assertions.getTableAssertions().assertTableCellHasText(moveConfirmTable, lastRow, 1, "New Version 1");
        assertions.getTableAssertions().assertTableCellHasText(moveConfirmTable, lastRow, 1, "New Version 4");
        assertions.getTableAssertions().assertTableCellDoesNotHaveText(moveConfirmTable, lastRow, 2, "New Version 1");
        assertions.getTableAssertions().assertTableCellDoesNotHaveText(moveConfirmTable, lastRow, 2, "New Version 4");

        tester.submit("Move");

        //check the moved issue doesn't show the new versions.
        tester.assertTextPresent("MKY-1");
        tester.assertTextPresent("Test issue 1");
        tester.assertTextNotPresent("New Version 1");
        tester.assertTextNotPresent("New Version 4");
    }

    //JRADEV-3273
    @Test
    @Restore("TestMoveLabels.xml")
    public void testMoveIssueWithRequiredLabels() {
        //Try moving an issue w/o labels. We should get prompted to updated
        //labels.  Validation should fail if there's no labels.
        navigation.issue().viewIssue("HSP-10");
        tester.clickLink("move-issue");
        navigation.issue().selectProject("monkey");
        tester.submit(NEXT_GT_GT);

        tester.assertTextPresent("Move Issue: Update Fields");
        tester.assertTextPresent("Labels");
        tester.submit(NEXT_GT_GT);

        tester.assertTextPresent("Labels is required");

        //now try moving an issue that's already got some labels. Screen should not come up
        navigation.issue().viewIssue("HSP-9");
        tester.clickLink("move-issue");
        navigation.issue().selectProject("monkey");
        tester.submit(NEXT_GT_GT);

        tester.assertTextPresent("Move Issue: Update Fields");
        // JRADEV-7741 - not sure why this would start failing... just getting build passing
        tester.assertTextPresent("All fields will be updated automatically");
        tester.submit(NEXT_GT_GT);
        tester.submit("Move");

        tester.assertTextPresent("MKY-8");

        //now also try moving one with subtasks as well
        navigation.issue().viewIssue("HSP-8");
        tester.clickLink("move-issue");
        navigation.issue().selectProject("monkey", "10000_1_pid");
        tester.submit(NEXT);
        tester.submit(NEXT);

        tester.assertTextPresent("Update Fields");
        tester.assertTextPresent("All field values will be retained");
        tester.submit(NEXT);
        tester.submit(NEXT);

        tester.assertTextPresent("Labels is required");

    }

    @Test
    @Restore("JRA-17312.xml")
    public void testMoveIssueWithSubtasksAndComponents() {
        navigation.issue().viewIssue("TWO-1");
        tester.assertTextPresent("Date Custom Field:");
        tester.clickLink("move-issue");
        navigation.issue().selectProject("ONE", "10001_1_pid");
        navigation.issue().selectIssueType("Bug", "10001_1_issuetype");
        tester.submit(NEXT);
        tester.submit(NEXT);

        tester.selectOption("components_10001", "comp-one");
        tester.submit(NEXT);
        tester.selectOption("components_10001", "comp-one");
        tester.submit(NEXT);
        tester.submit(NEXT);
        bulkOperationProgress.waitAndReloadBulkOperationProgressPage();

        navigation.issue().viewIssue("ONE-3");
        tester.assertTextNotPresent("Date Custom Field:");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Component/s", "comp-one"});
        tester.assertTextPresent("Due:");

        navigation.issue().viewIssue("ONE-3");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Component/s", "comp-one"});
        tester.assertTextPresent("Due:");
    }

    @Test
    @Restore("JRA-12479.xml")
    public void testMoveIssueWithRequiredCustomFields() {
        navigation.issue().viewIssue("ONE-1");
        tester.clickLink("move-issue");
        navigation.issue().selectProject("TWO");
        tester.submit(NEXT_GT_GT);

        tester.setFormElement("customfield_10100", "Hello World!");
        tester.submit(NEXT_GT_GT);

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"More Summary CF", "Hello World!"});
    }

    @Test
    @Restore("TestMoveIssue.xml")
    public void testMoveIssueRetainInactiveAssigee() {
        final String targetProject = backdoor.project().getProjectId(PROJECT_NEO_KEY).toString();
        final String assignee = "user1";

        backdoor.usersAndGroups().addUser(assignee);
        backdoor.usersAndGroups().addUserToGroup(assignee, DEVELOPERS);
        backdoor.issues().setIssueFields("HSP-2", new IssueFields().assignee(withName(assignee)));
        backdoor.userManager().setActive(assignee, false);

        assertTrue(issuesInProject(PROJECT_NEO_KEY).isEmpty());
        navigation.issue().viewIssue("HSP-2");
        tester.clickLink(MOVE_ISSUE_LINK);
        tester.setFormElement(PID_REQUEST_PARAM, targetProject);
        tester.submit(NEXT_GT_GT);
        tester.submit(NEXT_GT_GT);
        tester.submit(MOVE_SUBMIT);

        final Issue movedIssue = issuesInProject(PROJECT_NEO_KEY).iterator().next();
        assertEquals(assignee, movedIssue.fields.assignee.name);
    }

    @Test
    @Restore("TestMoveIssue.xml")
    public void testExcludeUserWithoutPermissionAsAssigneeSuggestion() {
        final String assignee = "user1";

        backdoor.darkFeatures().enableForSite("no.frother.assignee.field");
        backdoor.usersAndGroups().addUser(assignee);
        long permissionScheme = backdoor.permissionSchemes().copyDefaultScheme("monkey-scheme");
        backdoor.project().setPermissionScheme(backdoor.project().getProjectId(PROJECT_MONKEY_KEY), permissionScheme);
        backdoor.permissionSchemes().addUserPermission(permissionScheme, ProjectPermissions.ASSIGNABLE_USER, assignee);
        final String issueKeyToMove = backdoor.issues().createIssue(PROJECT_MONKEY_KEY, "test move issue", assignee).key();

        navigation.issue().viewIssue(issueKeyToMove);
        tester.clickLink(MOVE_ISSUE_LINK);
        tester.setFormElement(PID_REQUEST_PARAM, backdoor.project().getProjectId(PROJECT_HOMOSAP_KEY).toString());
        tester.submit(NEXT_GT_GT);
        tester.assertTextPresent("Move Issue: Update Fields");
        List<String> options = selectOptions(tester.getDialog().getElement("assignee"));
        assertThat(options, not(hasItem(assignee)));
    }

    private static List<Node> listNodes(NodeList nodeList) {
        ImmutableList.Builder<Node> builder = ImmutableList.builder();
        for (int i = 0; i < nodeList.getLength(); i++) {
            builder.add(nodeList.item(i));
        }

        return builder.build();
    }

    private static List<String> selectOptions(Element element) {
        Preconditions.checkArgument("select".equals(element.getNodeName().toLowerCase()));
        ImmutableList.Builder<String> builder = ImmutableList.builder();
        for (Node node : listNodes(element.getElementsByTagName("option"))) {
            builder.add(node.getAttributes().getNamedItem("value").getNodeValue());
        }
        return builder.build();
    }

    private List<Issue> issuesInProject(String projectKey) {
        SearchRequest request = new SearchRequest().jql(String.format("project = %s", projectKey));
        return backdoor.search().getSearch(request).issues;
    }

    private void moveIssue() {
        tester.submit(NEXT_GT_GT);
        tester.submit(NEXT_GT_GT);
        tester.submit(NEXT_GT_GT);
        tester.submit("Move");
    }

    private void assertErrorMsgFieldRequired(final String fieldId, final String project, final String fieldDisplayName) {
        tester.assertTextPresent("&quot;" + fieldId + "&quot; field is required and the project &quot;" + project + "&quot; does not have any " + fieldDisplayName);
    }
}
