package com.atlassian.jira.webtests.ztests.fields;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.EditIssueFieldVisibility;
import com.atlassian.jira.functest.framework.FieldLayoutSchemes;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.google.common.collect.Lists;
import com.meterware.httpunit.WebTable;
import com.opensymphony.util.TextUtils;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.io.IOException;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.AFFECTS_VERSIONS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ASSIGNEE_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.COMPONENTS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.COMPONENT_NAME_ONE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DUE_DATE_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_SCHEME_DESC;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_SCHEME_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIX_VERSIONS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PRIORITY_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_NEO;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_NEO_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.RESOLUTION_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SUB_TASK_DEFAULT_TYPE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.VERSION_NAME_ONE;
import static com.atlassian.jira.rest.api.issue.ResourceRef.withName;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.FIELDS, Category.SCHEMES})
@LoginAs(user = ADMIN_USERNAME)
public class TestFieldConfigurationSchemes extends BaseJiraFuncTest {
    private static final String FIELD_CONFIGURATION_NAME_ONE = "Test Field Configuration One";

    @Inject
    private FieldLayoutSchemes fieldLayoutSchemes;

    @Inject
    private FuncTestLogger logger;

    //JRADEV-10348: The associate page did not correctly link to the create "Field Configuration Scheme" page.
    @Inject
    private EditIssueFieldVisibility editIssueFieldVisibility;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Test
    @RestoreBlankInstance
    public void testAssociateFieldConfigurationSchemeWhenNoSchemeExists() {
        final String name = "New testAssociateFieldLayoutSchemeWhenNoSchemeExists Scheme";

        tester.gotoPage("/secure/admin/SelectFieldLayoutScheme!default.jspa?projectId=10000");
        tester.clickLink("add-new-scheme");
        tester.setFormElement("fieldLayoutSchemeName", name);
        tester.submit("Add");

        tester.assertLinkPresent("add-issue-type-field-configuration-association");
        assertEquals(name, locator.xpath("//span[@data-scheme-field='name']").getText());
    }

    @Test
    @RestoreBlankInstance
    public void testFieldConfigurationSchemes() {
        administration.project().addProject(PROJECT_NEO, PROJECT_NEO_KEY, ADMIN_USERNAME);

        editIssueFieldVisibility.resetFields();

        String issueKey = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "test for field layout schemes", ADMIN_USERNAME, "Major", "Bug").key();
        backdoor.issues().setIssueFields(issueKey, new IssueFields()
                .description("test description for field layout schemes")
                .environment("test environment 2"));

        fieldSchemesAddScheme();
        fieldSchemesAddDuplicateScheme();
        fieldSchemesAddInvalidScheme();
        fieldSchemesAssociateWithProject();

        fieldSchemesCreateIssueWithFieldConfigurationSchemeHidden();
        fieldSchemesCreateIssueWithFieldLayoutSchemeRequired();

        checkNavigatorFields();

        fieldSchemesEditIssueWithFieldConfigurationSchemeHidden(issueKey);
        fieldSchemesEditIssueWithFieldLayoutSchemeRequired(issueKey);

        String issueKey2 = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "test for field layout schemes", ADMIN_USERNAME, "Major", "Bug").key();
        backdoor.issues().setIssueFields(issueKey2, new IssueFields()
                .description("test description for field layout schemes")
                .environment("test environment 2")
                .components(withName(COMPONENT_NAME_ONE))
                .versions(withName(VERSION_NAME_ONE))
                .fixVersions(withName(VERSION_NAME_ONE)));

        fieldSchemeMoveIssueWithFieldSchemeHidden(issueKey);
        fieldSchemeMoveIssueWithFieldSchemeRequired(issueKey);

        fieldSchemeCreateSubTaskWithFieldSchemeHidden(issueKey);
        fieldSchemeCreateSubTaskWithFieldSchemeRequired(issueKey);

        fieldSchemesDeleteScheme();

        navigation.issue().deleteIssue(issueKey);
        navigation.issue().deleteIssue(issueKey2);
    }

    private void fieldSchemesAddScheme() {
        logger.log("Field Configuration Scheme: Adding a scheme");
        fieldLayoutSchemes.addFieldLayoutScheme(FIELD_SCHEME_NAME, FIELD_SCHEME_DESC);
        tester.assertTextPresent(FIELD_SCHEME_NAME);
    }

    private void fieldSchemesDeleteScheme() {
        logger.log("Field Configuration Scheme: Deleting a scheme");
        fieldLayoutSchemes.deleteFieldLayoutScheme(FIELD_SCHEME_NAME);
        tester.assertLinkNotPresentWithText(FIELD_SCHEME_NAME);
        tester.assertLinkNotPresentWithText(FIELD_SCHEME_DESC);
    }

    /**
     * Tests the error handling if a duplicate scheme is made
     */
    private void fieldSchemesAddDuplicateScheme() {
        logger.log("Field Configuration Scheme: Adding a scheme with a duplicate name");
        fieldLayoutSchemes.addFieldLayoutScheme(FIELD_SCHEME_NAME, FIELD_SCHEME_DESC);
        tester.assertTextPresent("A field configuration scheme with this name already exists.");
    }

    /**
     * Tests the error handling if a scheme with an invalid name is made
     */
    private void fieldSchemesAddInvalidScheme() {
        logger.log("Field Configuration Scheme: Adding a scheme with a duplicate name");
        fieldLayoutSchemes.addFieldLayoutScheme("", "");
        tester.assertTextPresent("The field configuration scheme name must not be empty.");
    }

    /**
     * Tests the ability to associate a field scheme to an issue type in a project
     */
    private void fieldSchemesAssociateWithProject() {
        logger.log("Field Configuration Scheme: associate a scheme to an issue type in a project");
        fieldLayoutSchemes.associateFieldLayoutScheme(PROJECT_NEO_KEY, FIELD_SCHEME_NAME);
        tester.assertTextPresent(FIELD_SCHEME_NAME);
        fieldLayoutSchemes.associateWithDefaultFieldLayout(PROJECT_NEO_KEY);
        tester.assertTextNotPresent(FIELD_SCHEME_NAME);
    }

    /**
     * Tests the functionality of Field Layout Schemes for 'Create Issue' using 'Required' fields
     */
    private void fieldSchemesCreateIssueWithFieldLayoutSchemeRequired() {
        logger.log("Create Issue: Attempt to create with issue field configuration");
        fieldLayoutSchemes.addFieldLayoutSchemeEntry("Bug", FIELD_CONFIGURATION_NAME_ONE, FIELD_SCHEME_NAME);

        fieldLayoutSchemes.associateFieldLayoutScheme(PROJECT_HOMOSAP_KEY, FIELD_SCHEME_NAME);

        editIssueFieldVisibility.setRequiredFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setRequiredFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setRequiredFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, FIX_VERSIONS_FIELD_ID);

        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");
        tester.setFormElement("summary", "test summary");
        tester.submit();

        tester.assertTextPresent("CreateIssueDetails.jspa");
        tester.assertTextPresent("Component/s is required");
        tester.assertTextPresent("Affects Version/s is required");
        tester.assertTextPresent("Fix Version/s is required");

        fieldLayoutSchemes.associateWithDefaultFieldLayout(PROJECT_HOMOSAP_KEY);
        removeFieldConfigurationSchemeEntry("Bug", FIELD_SCHEME_NAME);

        editIssueFieldVisibility.setOptionalFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setOptionalFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setOptionalFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, FIX_VERSIONS_FIELD_ID);
    }

    /**
     * Tests the functionality of Field Layout Schemes for 'Create Issue' using 'Hidden' fields
     */
    private void fieldSchemesCreateIssueWithFieldConfigurationSchemeHidden() {
        fieldLayoutSchemes.copyFieldLayout(FIELD_CONFIGURATION_NAME_ONE);
        fieldLayoutSchemes.addFieldLayoutSchemeEntry("Bug", FIELD_CONFIGURATION_NAME_ONE, FIELD_SCHEME_NAME);

        fieldLayoutSchemes.associateFieldLayoutScheme(PROJECT_HOMOSAP_KEY, FIELD_SCHEME_NAME);
        // Set fields to be hidden
        editIssueFieldVisibility.setHiddenFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setHiddenFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setHiddenFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, FIX_VERSIONS_FIELD_ID);

        logger.log("Create Issue: Test the creation of am issue using hidden fields");
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");

        tester.assertFormElementNotPresent("components");
        tester.assertFormElementNotPresent("versions");
        tester.assertFormElementNotPresent("fixVersions");

        // Reset fields to be optional
        editIssueFieldVisibility.setShownFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setShownFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setShownFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, FIX_VERSIONS_FIELD_ID);

        fieldLayoutSchemes.associateWithDefaultFieldLayout(PROJECT_HOMOSAP_KEY);
        removeFieldConfigurationSchemeEntry("Bug", FIELD_SCHEME_NAME);
    }

    private void removeFieldConfigurationSchemeEntry(String issueTypeName, String schemeName) {
        fieldLayoutSchemes.gotoFieldLayoutSchemes();
        tester.clickLinkWithText(schemeName);
        tester.assertTextInTable("scheme_entries", issueTypeName);

        try {
            WebTable table = tester.getDialog().getResponse().getTableWithID("scheme_entries");
            for (int i = 0; i < table.getRowCount(); i++) {
                String cellAsText = table.getCellAsText(i, 0);
                if (TextUtils.stringSet(cellAsText) && cellAsText.contains(issueTypeName)) {
                    table.getTableCell(i, 2).getLinkWith("Delete").click();
                }
            }
        } catch (SAXException | IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Tests the functionality of Field Configuration Schemes for 'Create Issue' using 'Hidden' fields
     */
    private void fieldSchemesEditIssueWithFieldConfigurationSchemeHidden(String issueKey) {
        fieldLayoutSchemes.addFieldLayoutSchemeEntry("Bug", FIELD_CONFIGURATION_NAME_ONE, FIELD_SCHEME_NAME);
        fieldLayoutSchemes.associateFieldLayoutScheme(PROJECT_HOMOSAP_KEY, FIELD_SCHEME_NAME);

        // Set fields to be hidden
        editIssueFieldVisibility.setHiddenFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setHiddenFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setHiddenFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, FIX_VERSIONS_FIELD_ID);

        logger.log("Edit Issue: Test the updating of an issue using hidden fields");
        navigation.issue().viewIssue(issueKey);
        tester.clickLink("edit-issue");

        tester.assertFormElementNotPresent("components");
        tester.assertFormElementNotPresent("versions");
        tester.assertFormElementNotPresent("fixVersions");

        // Reset fields to be optional
        editIssueFieldVisibility.setShownFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setShownFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setShownFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, FIX_VERSIONS_FIELD_ID);
        fieldLayoutSchemes.associateWithDefaultFieldLayout(PROJECT_HOMOSAP_KEY);
        removeFieldConfigurationSchemeEntry("Bug", FIELD_SCHEME_NAME);
    }

    private void assertSearcherFields(boolean presenceFlag) {
        String json = tester.getDialog().getElement("criteriaJson").getTextContent();
        //This is a hack, but certainly good enough for this test
        String[] searchers = json.split("},");
        List<String> fieldsToCheck = Lists.newArrayList(
                "\"id\":\"priority\"",
                "\"id\":\"resolution\"",
                "\"id\":\"duedate\"");
        final String presenceString;
        if (presenceFlag) {
            fieldsToCheck.add("\"id\":\"assignee\"");
            presenceString = "\"isShown\":true";
        } else {
            presenceString = "\"isShown\":false";
        }

        int count = 0;
        for (int index = 0; count < fieldsToCheck.size() && index < searchers.length; index++) {
            String s = searchers[index];
            if (s.contains(fieldsToCheck.get(count))) {
                assertThat(s, Matchers.containsString(presenceString));
                count++;
            }
        }
        assertEquals(fieldsToCheck.size(), count);
    }

    // Test for the presence of fields when hidden/displayed in a field layout.
    private void checkNavigatorFields() {
        // Encountered Javascript error when attempting to select project in issue nav.
        // Hence - not testing components/fix for/versions.

        // Test navigator fields with hidden/visible fields
        logger.log("Check Issue Navigator display for hidden/visible fields");

        // Include Sub-task issue type
        administration.subtasks().enable();

        fieldLayoutSchemes.addFieldLayoutSchemeEntry("Bug", FIELD_CONFIGURATION_NAME_ONE, FIELD_SCHEME_NAME);
        fieldLayoutSchemes.associateFieldLayoutScheme(PROJECT_HOMOSAP_KEY, FIELD_SCHEME_NAME);

        // Set field to be hidden
        editIssueFieldVisibility.setHiddenFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, ASSIGNEE_FIELD_ID);
        editIssueFieldVisibility.setHiddenFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, DUE_DATE_FIELD_ID);
        editIssueFieldVisibility.setHiddenFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, PRIORITY_FIELD_ID);
        editIssueFieldVisibility.setHiddenFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, RESOLUTION_FIELD_ID);

        // Field is visible as only hidden in one issue type/project field configuration association
        navigation.issueNavigator().displayAllIssues();
        assertSearcherFields(true);

        fieldLayoutSchemes.associateFieldLayoutScheme(PROJECT_NEO_KEY, FIELD_SCHEME_NAME);
        fieldLayoutSchemes.associateFieldLayoutScheme(PROJECT_MONKEY_KEY, FIELD_SCHEME_NAME);
        fieldLayoutSchemes.addFieldLayoutSchemeEntry("New Feature", FIELD_CONFIGURATION_NAME_ONE, FIELD_SCHEME_NAME);
        fieldLayoutSchemes.addFieldLayoutSchemeEntry("Improvement", FIELD_CONFIGURATION_NAME_ONE, FIELD_SCHEME_NAME);
        fieldLayoutSchemes.addFieldLayoutSchemeEntry("Task", FIELD_CONFIGURATION_NAME_ONE, FIELD_SCHEME_NAME);
        fieldLayoutSchemes.addFieldLayoutSchemeEntry("Sub-task", FIELD_CONFIGURATION_NAME_ONE, FIELD_SCHEME_NAME);

        // Field is not visible as hidden in all issue type/project field configuration associations
        navigation.issueNavigator().displayAllIssues();
        assertSearcherFields(false);

        // Reset field to be optional
        editIssueFieldVisibility.setShownFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, ASSIGNEE_FIELD_ID);
        editIssueFieldVisibility.setShownFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, DUE_DATE_FIELD_ID);
        editIssueFieldVisibility.setShownFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, PRIORITY_FIELD_ID);
        editIssueFieldVisibility.setShownFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, RESOLUTION_FIELD_ID);

        navigation.issueNavigator().displayAllIssues();
        assertSearcherFields(true);

        fieldLayoutSchemes.associateWithDefaultFieldLayout(PROJECT_HOMOSAP_KEY);
        fieldLayoutSchemes.associateWithDefaultFieldLayout(PROJECT_NEO_KEY);
        fieldLayoutSchemes.associateWithDefaultFieldLayout(PROJECT_MONKEY_KEY);
        removeFieldConfigurationSchemeEntry("Bug", FIELD_SCHEME_NAME);
        removeFieldConfigurationSchemeEntry("New Feature", FIELD_SCHEME_NAME);
        removeFieldConfigurationSchemeEntry("Improvement", FIELD_SCHEME_NAME);
        removeFieldConfigurationSchemeEntry("Task", FIELD_SCHEME_NAME);
        removeFieldConfigurationSchemeEntry("Sub-task", FIELD_SCHEME_NAME);

        administration.subtasks().disable();
    }

    /**
     * Tests the functionality of Field Configuration Schemes for 'Create Issue' using 'Required' fields
     */
    private void fieldSchemesEditIssueWithFieldLayoutSchemeRequired(String issueKey) {
        logger.log("Edit Issue: Attempt to edit an issue with issue field configuration");
        fieldLayoutSchemes.addFieldLayoutSchemeEntry("Bug", FIELD_CONFIGURATION_NAME_ONE, FIELD_SCHEME_NAME);

        fieldLayoutSchemes.associateFieldLayoutScheme(PROJECT_HOMOSAP_KEY, FIELD_SCHEME_NAME);
        editIssueFieldVisibility.setRequiredFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setRequiredFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setRequiredFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, FIX_VERSIONS_FIELD_ID);

        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("edit-issue");

        tester.assertTextPresent("Edit Issue");
        tester.submit("Update");

        tester.assertTextPresent("Component/s is required");
        tester.assertTextPresent("Affects Version/s is required");
        tester.assertTextPresent("Fix Version/s is required");

        fieldLayoutSchemes.associateWithDefaultFieldLayout(PROJECT_HOMOSAP_KEY);
        removeFieldConfigurationSchemeEntry("Bug", FIELD_SCHEME_NAME);
        editIssueFieldVisibility.setOptionalFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setOptionalFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setOptionalFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, FIX_VERSIONS_FIELD_ID);
    }

    /**
     * Tests that fields are made hidden when task is moved to a type with a different Field Configuration Scheme
     */
    private void fieldSchemeMoveIssueWithFieldSchemeHidden(String issueKey) {
        logger.log("Move Issue: Test the abilty to hide a field in a particular Field Configuration Scheme");
        fieldLayoutSchemes.addFieldLayoutSchemeEntry("Improvement", FIELD_CONFIGURATION_NAME_ONE, FIELD_SCHEME_NAME);
        fieldLayoutSchemes.associateFieldLayoutScheme(PROJECT_HOMOSAP_KEY, FIELD_SCHEME_NAME);
        editIssueFieldVisibility.setHiddenFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setHiddenFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setHiddenFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, FIX_VERSIONS_FIELD_ID);

        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("move-issue");
        tester.assertTextPresent("Move Issue");
        navigation.issue().selectIssueType("Improvement", "issuetype");
        tester.submit();

        tester.assertTextPresent("All fields will be updated automatically.");

        editIssueFieldVisibility.setShownFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setShownFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setShownFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, FIX_VERSIONS_FIELD_ID);
        fieldLayoutSchemes.associateWithDefaultFieldLayout(PROJECT_HOMOSAP_KEY);
        removeFieldConfigurationSchemeEntry("Improvement", FIELD_SCHEME_NAME);
    }

    /**
     * Tests that fields are made required when task is moved to a type with a different Field Configuration Scheme
     */
    private void fieldSchemeMoveIssueWithFieldSchemeRequired(String issueKey) {
        logger.log("Move Issue: Test the abilty to make a field required in a particular Field Configuration Scheme");
        fieldLayoutSchemes.addFieldLayoutSchemeEntry("Improvement", FIELD_CONFIGURATION_NAME_ONE, FIELD_SCHEME_NAME);
        fieldLayoutSchemes.associateFieldLayoutScheme(PROJECT_HOMOSAP_KEY, FIELD_SCHEME_NAME);
        editIssueFieldVisibility.setRequiredFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setRequiredFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setRequiredFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, FIX_VERSIONS_FIELD_ID);

        navigation.issue().gotoIssue(issueKey);

        tester.clickLink("move-issue");
        tester.assertTextPresent("Move Issue");
        navigation.issue().selectIssueType("Improvement", "issuetype");
        tester.submit();
        tester.assertTextPresent("Step 3 of 4");
        tester.getDialog().setWorkingForm("jiraform");
        tester.submit();

        tester.assertTextPresent("Step 3 of 4");
        tester.assertTextPresent("Component/s is required");
        tester.assertTextPresent("Affects Version/s is required");
        tester.assertTextPresent("Fix Version/s is required");

        editIssueFieldVisibility.setOptionalFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setOptionalFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setOptionalFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, FIX_VERSIONS_FIELD_ID);
        fieldLayoutSchemes.associateWithDefaultFieldLayout(PROJECT_HOMOSAP_KEY);
        removeFieldConfigurationSchemeEntry("Improvement", FIELD_SCHEME_NAME);
    }

    /**
     * Tests that field configuration schemes can be enforced on sub tasks with required fields
     */
    private void fieldSchemeCreateSubTaskWithFieldSchemeRequired(String issueKey) {
        logger.log("Sub Task Create: Enforce Sub Tasks on a field configuration scheme");
        administration.subtasks().enable();
        fieldLayoutSchemes.addFieldLayoutSchemeEntry(SUB_TASK_DEFAULT_TYPE, FIELD_CONFIGURATION_NAME_ONE, FIELD_SCHEME_NAME);
        fieldLayoutSchemes.associateFieldLayoutScheme(PROJECT_HOMOSAP_KEY, FIELD_SCHEME_NAME);
        editIssueFieldVisibility.setRequiredFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setRequiredFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setRequiredFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, FIX_VERSIONS_FIELD_ID);

        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("create-subtask");
        tester.assertTextPresent("Create Sub-Task");
        tester.setFormElement("summary", "test summary");
        tester.submit();

        tester.assertTextPresent("Create Sub-Task");
        tester.assertTextPresent("Component/s is required");
        tester.assertTextPresent("Affects Version/s is required");
        tester.assertTextPresent("Fix Version/s is required");

        editIssueFieldVisibility.setOptionalFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setOptionalFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setOptionalFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, FIX_VERSIONS_FIELD_ID);
        fieldLayoutSchemes.associateWithDefaultFieldLayout(PROJECT_HOMOSAP_KEY);
        removeFieldConfigurationSchemeEntry(SUB_TASK_DEFAULT_TYPE, FIELD_SCHEME_NAME);
        administration.subtasks().disable();
    }

    /**
     * Tests that field configuration schemes can be enforced on sub tasks with hidden field
     */
    private void fieldSchemeCreateSubTaskWithFieldSchemeHidden(String issueKey) {
        logger.log("Sub Task Create: Enforce Sub Tasks on a field configuration scheme");
        administration.subtasks().enable();
        fieldLayoutSchemes.addFieldLayoutSchemeEntry(SUB_TASK_DEFAULT_TYPE, FIELD_CONFIGURATION_NAME_ONE, FIELD_SCHEME_NAME);
        fieldLayoutSchemes.associateFieldLayoutScheme(PROJECT_HOMOSAP_KEY, FIELD_SCHEME_NAME);
        editIssueFieldVisibility.setHiddenFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setHiddenFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setHiddenFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, FIX_VERSIONS_FIELD_ID);

        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("create-subtask");
        tester.assertTextPresent("Create Sub-Task");
        tester.assertFormElementNotPresent("components");
        tester.assertFormElementNotPresent("versions");
        tester.assertFormElementNotPresent("fixVersions");

        editIssueFieldVisibility.setShownFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setShownFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setShownFieldsOnEnterprise(FIELD_CONFIGURATION_NAME_ONE, FIX_VERSIONS_FIELD_ID);
        fieldLayoutSchemes.associateWithDefaultFieldLayout(PROJECT_HOMOSAP_KEY);
        removeFieldConfigurationSchemeEntry(SUB_TASK_DEFAULT_TYPE, FIELD_SCHEME_NAME);
        administration.subtasks().disable();
    }
}
