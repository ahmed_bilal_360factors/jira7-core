package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestXsrfAdminGroups extends BaseJiraFuncTest {
    @Inject
    private Form form;

    @Test
    public void testAdminGroups() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck("Add Group", new XsrfCheck.Setup() {
                    public void setup() {
                        gotoGroupBrowser();
                        tester.setFormElement("addName", "newgroup");
                    }
                }, new XsrfCheck.FormSubmission("add_group"))
                ,
                new XsrfCheck("Delete Group", new XsrfCheck.Setup() {
                    public void setup() {
                        gotoGroupBrowser();
                        tester.clickLink("del_newgroup");
                    }
                }, new XsrfCheck.FormSubmission("Delete"))
                ,
                new XsrfCheck("Bulk Edit User Groups (Leave)", new XsrfCheck.Setup() {
                    public void setup() {
                        gotoGroupBrowser();
                        tester.clickLink("edit_members_of_jira-developers");
                        tester.selectOption("usersToUnassign", ADMIN_USERNAME);
                    }
                }, new XsrfCheck.FormSubmission("unassign"))
                ,
                new XsrfCheck("Bulk Edit User Groups (Join)", new XsrfCheck.Setup() {
                    public void setup() {
                        gotoGroupBrowser();
                        tester.clickLink("edit_members_of_jira-developers");
//                    JRADEV-18305 This won't work because the usersToAssignStr is now a MultiSelect field.
//                    The XSRF token is still checked however
//                    tester.setFormElement("usersToAssignStr", ADMIN_USERNAME);
                    }
                }, new XsrfCheck.FormSubmission("assign"))

        ).run(tester, navigation, form);
    }

    private void gotoGroupBrowser() {
        navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
    }
}