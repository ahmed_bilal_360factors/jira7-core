package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.misc.TestDatabaseSetup;
import com.atlassian.jira.webtests.ztests.misc.TestDefaultJiraDataFromInstall;
import com.atlassian.jira.webtests.ztests.misc.TestJohnsonFiltersWhileNotSetup;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A set of tests that must be run before any JIRA data is imported into it. They will fail if run out of order.
 *
 * @since v4.0
 */
public class FuncTestSuiteSetup {

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                // NOTE: the tests that import data to run should go at the end of the tests, it
                // seems that some of the tests don't like the state of JIRA after one or many
                // of the imports. This needs to be looked at and fixed, but for now do not add
                // any of these tests anywhere other than the end of the list.

                // this test must run before JIRA is setup because it tests the
                // johnson event filter for this situation
                // this test must run before the rest as it sets up the db connection
                .add(TestJohnsonFiltersWhileNotSetup.class)
                        // this test must run before the rest as it sets up the db connection
                .add(TestDatabaseSetup.class)

                /*
                  THESE TESTS MUST RUN FIRST
                  FOR TESTING JIRA BEING SETUP FROM SCRATCH
                  (UPGRADE TASKS etc.)
                */
                .add(TestDefaultJiraDataFromInstall.class)
                .build();
    }
}