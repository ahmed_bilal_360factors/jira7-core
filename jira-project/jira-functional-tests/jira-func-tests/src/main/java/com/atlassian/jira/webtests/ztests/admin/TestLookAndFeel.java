package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * A func test for the LookAndFeel pages
 *
 * @since v3.13
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION})
@LoginAs(user = ADMIN_USERNAME)
public class TestLookAndFeel extends BaseJiraFuncTest {
    public static final String DEFAULT_FAVICON_URL = "/images/64jira.png";
    public static final String LOGO_PREVIEW_IMAGE_CSS_SELECTOR = "img.application-logo.logo-preview";
    public static final String FAVICON_PREVIEW_IMAGE_CSS_SELECTOR = "img.application-logo.favicon-preview";
    private static final String VERSION_ELEMENT_STR = "<dl style=\"display:none;\" id=\"jira.webresource.flushcounter\">";
    private static final String DEFAULT_LOGO_URL = "/images/icon-jira-logo.png";
    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
        navigation.gotoAdminSection(Navigation.AdminSection.LOOKANDFEEL);
    }

    @Test
    public void testHasDefaults() throws Exception {
        assertHasDefaultLookAndFeel();
    }

    private void assertHasDefaultLookAndFeel() {
        assertHasDefaultLogos();
    }

    private void assertHasDefaultLogos() {
        final String logoUrlAsDisplayedInThePreview = locator.css(LOGO_PREVIEW_IMAGE_CSS_SELECTOR).getNode().
                getAttributes().getNamedItem("src").getTextContent();

        Assert.assertTrue(logoUrlAsDisplayedInThePreview.endsWith(DEFAULT_LOGO_URL));

        final String favIconUrlAsDisplayedInThePreview = locator.css(FAVICON_PREVIEW_IMAGE_CSS_SELECTOR).getNode().
                getAttributes().getNamedItem("src").getTextContent();

        Assert.assertTrue(favIconUrlAsDisplayedInThePreview.endsWith(DEFAULT_FAVICON_URL));
    }
}


