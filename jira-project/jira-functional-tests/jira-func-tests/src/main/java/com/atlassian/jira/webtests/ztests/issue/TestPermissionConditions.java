package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;

@WebTest({Category.FUNC_TEST, Category.ENTITY_PROPERTIES, Category.REFERENCE_PLUGIN})
@RestoreBlankInstance
public class TestPermissionConditions extends BaseJiraFuncTest {

    public static final String CONDITIONAL_WEBPANEL_ID_PREFIX = "conditional-webpanel-";
    @Inject
    private Assertions assertions;

    private IssueCreateResponse issue;

    @Before
    public void setUp() throws Exception {
        backdoor.restoreBlankInstance();
        navigation.login(ADMIN_USERNAME);
        issue = backdoor.issues().createIssue(FunctTestConstants.PROJECT_HOMOSAP_KEY, "sum");
    }

    @Test
    public void allPanelsAreShownIfConditionsAreSatisfied() {
        navigation.login(ADMIN_USERNAME);
        panelIsShown("projectPermissionNew");
        panelIsShown("projectPermissionLegacy");
        panelIsShown("globalPermissionNew");
        panelIsShown("globalPermissionLegacy");
    }

    @Test
    public void panelsAreNotShownIfConditionsAreNotSatisfied() {
        navigation.login(FRED_USERNAME); // fred is not a global nor project admin
        panelIsNotShown("projectPermissionNew");
        panelIsNotShown("projectPermissionLegacy");
        panelIsNotShown("globalPermissionNew");
        panelIsNotShown("globalPermissionLegacy");
    }


    private void panelIsShown(String panelId) {
        navigation.issue().viewIssue(issue.key);
        assertions.assertNodeByIdExists(CONDITIONAL_WEBPANEL_ID_PREFIX + panelId);
    }

    private void panelIsNotShown(String panelId) {
        navigation.issue().viewIssue(issue.key);
        assertions.assertNodeByIdDoesNotExist(CONDITIONAL_WEBPANEL_ID_PREFIX + panelId);
    }

}
