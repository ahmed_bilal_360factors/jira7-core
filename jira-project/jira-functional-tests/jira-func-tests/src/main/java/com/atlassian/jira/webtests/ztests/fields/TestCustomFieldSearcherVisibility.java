package com.atlassian.jira.webtests.ztests.fields;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 *
 */
@WebTest({Category.FUNC_TEST, Category.FIELDS})
@LoginAs(user = ADMIN_USERNAME)
public class TestCustomFieldSearcherVisibility extends BaseJiraFuncTest {
    @Inject
    private IssueTableAssertions issueTableAssertions;

    @Inject
    private Administration administration;

    @Test
    public void testJRA_16356() {
        administration.restoreData("TestCustomFieldSearcherVisibility.xml");

        // the fields are start off visible so assert that
        issueTableAssertions.assertSearchersPresent("DatePickerCF", "TextCF");

        // now hide the fields are assert that they are gone
        hideCustomFields();
        issueTableAssertions.assertSearchersNotPresent("DatePickerCF", "TextCF");
    }

    private void hideCustomFields() {
        administration.fieldConfigurations().defaultFieldConfiguration().hideFields("DatePickerCF");
        administration.fieldConfigurations().defaultFieldConfiguration().hideFields("TextCF");
    }
}
