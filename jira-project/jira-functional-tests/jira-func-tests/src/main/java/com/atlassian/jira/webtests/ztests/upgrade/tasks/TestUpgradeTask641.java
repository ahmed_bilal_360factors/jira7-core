package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.webtests.LicenseKeys;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_DEV_ROLE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_USERS_ROLE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_USERS_ROLE_ID;
import static com.atlassian.jira.permission.ProjectPermissions.VIEW_READONLY_WORKFLOW;

/**
 * @since v4.4
 */
@WebTest({Category.FUNC_TEST, Category.UPGRADE_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUpgradeTask641 extends BaseJiraFuncTest {

    public static final String ROWF_PROJECT_NAME = "readOnlyWorkflowTest";
    public static final String ROWF_PROJECT_KEY = "ROWFT";
    public static final String ROWF_ISSUE_KEY = "ROWFT-1";

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Test
    public void testUpgrade() {
        /*
            to truly test the upgrade, we have to start with a new instance.
            Unlike the other upgradetask funcTestHelperFactory tests, restoring data will void this test
         */
        setupJIRAFromScratch();

        createProjectWithOneIssue();

        navigation.issue().viewIssue(ROWF_ISSUE_KEY);
        tester.assertLinkPresentWithText("View Workflow");

        //now remove the permission and check we don't get that link any longer
        backdoor.permissionSchemes().removeProjectRolePermission(DEFAULT_PERM_SCHEME_ID, VIEW_READONLY_WORKFLOW, JIRA_USERS_ROLE_ID);
        navigation.issue().viewIssue(ROWF_ISSUE_KEY);
        tester.assertLinkNotPresentWithText("View Workflow");
    }

    private void setupJIRAFromScratch() {
        administration.restoreNotSetupInstance();

        //step 2
        tester.gotoPage("secure/SetupApplicationProperties.jspa");
        tester.assertTextPresent("Set up application properties");

        // Fill in mandatory fields
        tester.setWorkingForm("jira-setupwizard");
        tester.setFormElement("title", "My JIRA");
        // Submit Step 2 with Default paths.
        tester.submit();

        //step 3
        tester.setWorkingForm("setupLicenseForm");
        tester.setFormElement("setupLicenseKey", LicenseKeys.COMMERCIAL.getLicenseString());
        tester.submit();

        //step 4
        tester.assertTextPresent("Set up administrator account");
        tester.setFormElement("username", ADMIN_USERNAME);
        tester.setFormElement("password", ADMIN_USERNAME);
        tester.setFormElement("confirm", ADMIN_USERNAME);
        tester.setFormElement("fullname", "Mary Magdelene");
        tester.setFormElement("email", "admin@example.com");
        tester.submit();
        tester.assertTextPresent("Set up email notifications");

        //step 5
        logger.log("Noemail");
        tester.submit("finish");
        logger.log("Noemail");
        // During SetupComplete, the user is automatically logged in
        // Assert that the user is logged in by checking if the profile link is present
        tester.assertLinkPresent("header-details-user-fullname");
        navigation.disableWebSudo();
    }

    private void createProjectWithOneIssue() {
        backdoor.project().addProject(ROWF_PROJECT_NAME, ROWF_PROJECT_KEY, ADMIN_USERNAME);
        //due to the removal of the jira-users and jira-developers groups for 7.0 we need to add the admin user to the 'Users' and 'Developers' project role for now.
        backdoor.projectRole().addActors(ROWF_PROJECT_KEY, JIRA_USERS_ROLE, null, new String[]{ADMIN_USERNAME});
        backdoor.projectRole().addActors(ROWF_PROJECT_KEY, JIRA_DEV_ROLE, null, new String[]{ADMIN_USERNAME});

        backdoor.issues().createIssue(ROWF_PROJECT_KEY, "Test Issue for ReadOnly Workflow");
    }
}
