package com.atlassian.jira.webtests.ztests.charts;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.locator.TableCellLocator;
import com.atlassian.jira.functest.framework.locator.TableLocator;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.CHARTING})
@LoginAs(user = ADMIN_USERNAME)
public class TestCharting extends BaseJiraFuncTest {

    public static final String MAIN_CONTENT_AREA = "#content";
    public static final String PAGE_HEADER = "#content header h1";
    public static final String REPORT_HEADER = ".reportHeading h3";

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
    }

    @Test
    public void testCreatedVsResolvedReport() {
        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:createdvsresolved-report");
        tester.assertTextPresent("Report: Created vs Resolved Issues Report");

        //validation
        tester.submit("Next");
        tester.assertTextPresent("Please specify a project or filter");

        tester.setFormElement("daysprevious", "aaa");
        tester.submit("Next");
        tester.assertTextPresent("You must specify a whole number of days.");

        //now lets add the report
        tester.setFormElement("projectOrFilterId", "project-10000");
        tester.setFormElement("daysprevious", "30");
        tester.submit("Next");

        CssLocator locator = new CssLocator(tester, MAIN_CONTENT_AREA);
        textAssertions.assertTextPresent(locator, "There are no matching issues to report on.");

        //now lets create an issue and do the report again.
        navigation.issue().createIssue("homosapien", "Bug", "My first bug");

        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:createdvsresolved-report");
        tester.setFormElement("projectOrFilterId", "project-10000");
        tester.setFormElement("daysprevious", "30");
        tester.submit("Next");

        locator = new CssLocator(tester, MAIN_CONTENT_AREA);
        textAssertions.assertTextPresent(new CssLocator(tester, PAGE_HEADER), "Created vs Resolved Issues Report");
        textAssertions.assertTextSequence(new CssLocator(tester, REPORT_HEADER), "homosapien");
        textAssertions.assertTextSequence(locator, "This chart shows the number of issues", "created",
                "vs the number of issues", "resolved", "in the last", "30", "days.");
        tester.assertTextPresent("Data Table");

        final TableLocator tableLocator = new TableLocator(tester, "createdvsresolved-report-datatable");
        textAssertions.assertTextPresent(tableLocator, "Period");
        textAssertions.assertTextPresent(tableLocator, "Created");
        textAssertions.assertTextPresent(tableLocator, "Resolved");
    }

    @Test
    public void testResolutionTimeReport() {
        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:resolutiontime-report");
        tester.assertTextPresent("Report: Resolution Time Report");

        //validation
        tester.submit("Next");
        tester.assertTextPresent("Please specify a project or filter");

        tester.setFormElement("daysprevious", "aaa");
        tester.submit("Next");
        tester.assertTextPresent("You must specify a whole number of days.");

        //now lets add the report
        tester.setFormElement("projectOrFilterId", "project-10000");
        tester.setFormElement("daysprevious", "30");
        tester.submit("Next");

        WebPageLocator locator = new WebPageLocator(tester);
        textAssertions.assertTextPresent(locator, "There are no matching issues to report on.");

        //can't actually create some data here since the resolution time will be too short.  The TestChartingData
        // test takes care of the data tests though.
    }

    @Test
    public void testPieReport() {
        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:pie-report");
        tester.assertTextPresent("Report: Pie Chart Report");

        //validation
        tester.submit("Next");
        tester.assertTextPresent("Please specify a project or filter");

        //now lets add the report
        tester.setFormElement("projectOrFilterId", "project-10000");
        tester.selectOption("statistictype", "Issue Type");
        tester.submit("Next");

        CssLocator locator = new CssLocator(tester, MAIN_CONTENT_AREA);
        textAssertions.assertTextPresent(new CssLocator(tester, PAGE_HEADER), "Pie Chart Report");
        textAssertions.assertTextSequence(locator, "Project:", "homosapien", "(Issue Type)");
        textAssertions.assertTextPresent(locator, "There are no matching issues to report on.");

        //now lets create some issues
        navigation.issue().createIssue("homosapien", "Bug", "My first bug");
        navigation.issue().createIssue("homosapien", "Improvement", "My first improvement");

        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:pie-report");
        tester.assertTextPresent("Report: Pie Chart Report");

        //now lets add the report
        tester.setFormElement("projectOrFilterId", "project-10000");
        tester.selectOption("statistictype", "Issue Type");
        tester.submit("Next");

        locator = new CssLocator(tester, MAIN_CONTENT_AREA);
        textAssertions.assertTextSequence(locator, "Pie Chart Report");
        textAssertions.assertTextSequence(locator, "Project:", "homosapien", "(Issue Type)");

        tester.assertTextPresent("Data Table");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "singlefieldpie-report-datatable", 0, 1), "Issues");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "singlefieldpie-report-datatable", 0, 2), "%");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "singlefieldpie-report-datatable", 1, 0), "Improvement");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "singlefieldpie-report-datatable", 1, 1), "1");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "singlefieldpie-report-datatable", 1, 2), "50%");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "singlefieldpie-report-datatable", 2, 0), "Bug");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "singlefieldpie-report-datatable", 2, 1), "1");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "singlefieldpie-report-datatable", 2, 2), "50%");

        // click on a pie slice, and check that we get to the issue navigator
        tester.clickLinkWithText("Bug: 1 issues (50%)");
        tester.assertTextPresent("My first bug");
    }

    @Test
    public void testAverageAgeReport() {
        final String name = "Average Age Report";
        assertReportValidation("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:averageage-report", name, "There are no matching issues to report on.");

        textAssertions.assertTextPresent(new CssLocator(tester, PAGE_HEADER), name);
        textAssertions.assertTextSequence(new CssLocator(tester, REPORT_HEADER), "homosapien");

        tester.assertTextPresent("Data Table");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "averageage-report-datatable", 0, 0), "Period");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "averageage-report-datatable", 0, 1), "Issues Unresolved");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "averageage-report-datatable", 0, 2), "Total Age");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "averageage-report-datatable", 0, 3), "Avg. Age");
    }

    @Test
    public void testRecentlyCreatedReport() {
        final String name = "Recently Created Issues Report";
        assertReportValidation("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:recentlycreated-report", name, null);

        textAssertions.assertTextPresent(new CssLocator(tester, PAGE_HEADER), name);
        textAssertions.assertTextSequence(new CssLocator(tester, REPORT_HEADER), "homosapien");

        tester.assertTextPresent("Data Table");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "recentlycreated-report-datatable", 0, 0), "Period");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "recentlycreated-report-datatable", 0, 1), "Created Issues (Unresolved)");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "recentlycreated-report-datatable", 0, 2), "Created Issues (Resolved)");
    }

    @Test
    public void testTimeSinceReport() {
        final String name = "Time Since Issues Report";
        assertReportValidation("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:timesince-report", name, null);

        textAssertions.assertTextPresent(new CssLocator(tester, PAGE_HEADER), name);
        textAssertions.assertTextSequence(new CssLocator(tester, REPORT_HEADER), "homosapien");

        tester.assertTextPresent("Data Table");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "timesince-report-datatable", 0, 0), "Period");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "timesince-report-datatable", 0, 1), "Created");
    }

    private void assertReportValidation(String url, String reportName, String noIssuesMsg) {
        tester.gotoPage(url);
        tester.assertTextPresent("Report: " + reportName);

        //validation
        tester.submit("Next");
        tester.assertTextPresent("Please specify a project or filter");

        tester.setFormElement("daysprevious", "aaa");
        tester.submit("Next");
        tester.assertTextPresent("You must specify a whole number of days.");

        //now lets add the report
        tester.setFormElement("projectOrFilterId", "project-10000");
        tester.setFormElement("daysprevious", "30");
        tester.submit("Next");

        WebPageLocator locator = new WebPageLocator(tester);
        textAssertions.assertTextPresent(new CssLocator(tester, PAGE_HEADER), reportName);
        textAssertions.assertTextSequence(new CssLocator(tester, REPORT_HEADER), "homosapien");

        if (noIssuesMsg != null) {
            textAssertions.assertTextPresent(locator, noIssuesMsg);
        }

        //now lets create some issues
        navigation.issue().createIssue("homosapien", "Bug", "My first bug");
        navigation.issue().createIssue("homosapien", "Improvement", "My first improvement");

        tester.gotoPage(url);
        tester.assertTextPresent("Report: " + reportName);

        //now lets add the report
        tester.setFormElement("projectOrFilterId", "project-10000");
        tester.setFormElement("daysprevious", "30");
        tester.submit("Next");
    }
}
