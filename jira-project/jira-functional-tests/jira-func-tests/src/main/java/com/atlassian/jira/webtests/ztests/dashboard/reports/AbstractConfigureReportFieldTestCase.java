package com.atlassian.jira.webtests.ztests.dashboard.reports;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.assertions.JiraFormAssertions;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import org.junit.Before;

import javax.inject.Inject;

public abstract class AbstractConfigureReportFieldTestCase extends BaseJiraFuncTest {

    @Inject
    protected Administration administration;

    @Inject
    protected TextAssertions textAssertions;

    @Inject
    protected JiraFormAssertions formAssertions;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
        goToConfigureReportPage();
    }

    private void goToConfigureReportPage() {
        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.dev.func-test-plugin:fieldtest-report");
        tester.assertTextPresent("Report: Field Test Report");
        tester.setWorkingForm("configure-report");
        formAssertions.assertNoErrorsPresent();
    }
}
