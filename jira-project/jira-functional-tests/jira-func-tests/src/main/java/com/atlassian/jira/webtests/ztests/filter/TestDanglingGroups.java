package com.atlassian.jira.webtests.ztests.filter;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;

/**
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.FILTERS})
@LoginAs(user = ADMIN_USERNAME)
public class TestDanglingGroups extends BaseJiraFuncTest {
    /**
     * This test uses a specially crafted XML which has a "dangling" group. The group has a subscription
     * but doesn't exist in JIRA. This simulates a group that is externally managed (i.e. via Crowd) being deleted.
     * In that scenario, JIRA never has a chance to "clean up" these groups. But we still want to call them out
     * to the admin in the View Subscriptions screen.
     */
    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testDanglingGroups() {
        administration.restoreData("TestDanglingGroups.xml");

        navigation.manageFilters().goToDefault();
        navigation.manageFilters().manageSubscriptions(10000);

        // assert that the group is shown and is inside a span with a title
        assertEquals(locator.css("span.warning[title]").getNode().getAttributes().getNamedItem("title").getNodeValue(), "Warning! This group has been deleted.");
        textAssertions.assertTextPresent(locator.css("span.warning[title"), "delete-me");
    }
}
