package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.matcher.PermissionSchemesMatcher;
import com.atlassian.jira.permission.JiraPermissionHolderType;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.testkit.beans.PermissionSchemeBean;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static org.junit.Assert.assertTrue;

/**
 * Tests the administration backdoor implementation of PermissionSchemes for external plugins that cannot convert
 * to using the backdoor.
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.PERMISSIONS, Category.SCHEMES})
@RestoreBlankInstance
@LoginAs(user = "admin")
public class TestPermissionSchemesBackdoorImpl extends BaseJiraFuncTest {

    private static final String NEW_SCHEME_NAME = "TestPermissionSchemesBackdoorImplScheme";

    private Long createdSchemeId = -1l;

    @Inject
    private Administration administration;

    @Before
    public void createScheme() {
        createdSchemeId = backdoor.permissionSchemes().createScheme(NEW_SCHEME_NAME, "");
    }

    @After
    public void destroyScheme() {
        backdoor.permissionSchemes().deleteScheme(createdSchemeId);
    }

    @Test
    public void testAddPermissionToDefaultScheme() {
        administration.permissionSchemes().defaultScheme().grantPermissionToGroup(ProjectPermissions.CREATE_ISSUES, FunctTestConstants.JIRA_USERS_GROUP);

        PermissionSchemeBean permissionSchemesBean = backdoor.permissionSchemes().getAssignedPermissions(FunctTestConstants.DEFAULT_PERM_SCHEME_ID);

        assertTrue("Should have assigned the anyone permission to creating issues",
                PermissionSchemesMatcher.hasPermission(ProjectPermissions.CREATE_ISSUES.permissionKey(),
                        JiraPermissionHolderType.GROUP.getKey(), FunctTestConstants.JIRA_USERS_GROUP).matches(permissionSchemesBean));
    }

    @Test
    public void testAddingAnyoneGroupPermission() {
        administration.permissionSchemes().scheme(NEW_SCHEME_NAME).grantPermissionToGroup(ProjectPermissions.CREATE_ISSUES, "");

        PermissionSchemeBean permissionSchemesBean = backdoor.permissionSchemes().getAssignedPermissions(createdSchemeId);

        assertTrue("Should have assigned the anyone permission to creating issues",
                PermissionSchemesMatcher.hasPermission(ProjectPermissions.CREATE_ISSUES.permissionKey(),
                        JiraPermissionHolderType.GROUP.getKey(), null).matches(permissionSchemesBean));
    }

    @Test
    public void testAddingAnyLoggedInUserApplicationRolePermission() {
        administration.permissionSchemes().scheme(NEW_SCHEME_NAME).grantPermissionToApplicationRole(ProjectPermissions.CREATE_ISSUES, "");
        PermissionSchemeBean permissionSchemesBean = backdoor.permissionSchemes().getAssignedPermissions(createdSchemeId);

        assertTrue("Should have assigned the any logged in permission to creating issues",
                PermissionSchemesMatcher.hasPermission(ProjectPermissions.CREATE_ISSUES.permissionKey(),
                        JiraPermissionHolderType.APPLICATION_ROLE.getKey(), null).matches(permissionSchemesBean));
    }
}
