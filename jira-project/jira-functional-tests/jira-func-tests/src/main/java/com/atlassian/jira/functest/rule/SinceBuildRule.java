package com.atlassian.jira.functest.rule;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import org.junit.rules.TestRule;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.function.Supplier;

/**
 * Ensures tests are ignored if the currently running JIRA is on an older build number than when the test was written.
 *
 * This situation arises during a Zero Downtime Upgrade (ZDU), when a JIRA cluster passes through a state where the
 * binaries are on 'latest', but the Upgrade Tasks haven't been run yet, hence the database is on 'old'.
 *
 * Use the @SinceBuild annotation if you're testing:
 * - a feature that is hidden behind a Dark Feature
 * - a database change that requires an Upgrade Task
 *
 * If you're unsure, use the annotation. It won't hurt.
 *
 * Have a look at its counterpart {@link BeforeBuildRule}
 */
public class SinceBuildRule extends AbstractBuildNumberRule<SinceBuildRule.SinceBuild> implements TestRule {

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    public @interface SinceBuild {
        int buildNumber();
    }

    public SinceBuildRule(final Supplier<Backdoor> backdoor) {
        super(backdoor, SinceBuild.class, (annotation, dbBuildNumber) -> annotation.buildNumber() > dbBuildNumber, SinceBuild::buildNumber);
    }

    @Override
    String describeUnsatisfiedRule(String testName, int annotationBuildNumber, int dbBuildNumber) {
        return testName + " was ignored as it only exists since " + annotationBuildNumber + " (current: " + dbBuildNumber + ")";
    }
}
