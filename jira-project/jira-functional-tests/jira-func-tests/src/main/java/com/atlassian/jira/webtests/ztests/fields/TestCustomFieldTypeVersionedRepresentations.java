package com.atlassian.jira.webtests.ztests.fields;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FuncTestRestClient;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.webtests.ztests.plugin.reloadable.ReferencePluginConstants;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Collections;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Test that the extra rest representations of custom fields are properly rendered.
 *
 * @since v6.4
 */
@WebTest({Category.FUNC_TEST, Category.REFERENCE_PLUGIN, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestCustomFieldTypeVersionedRepresentations extends BaseJiraFuncTest {
    public static final String VERSIONED_REPRESENTATIONS = "versionedRepresentations";
    private final Object customFieldValue = "test value";
    @Inject
    FuncTestRestClient funcTestRestClient;
    private String customFieldKey;
    private String issueKey;
    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.plugins().referencePlugin().enable();
        customFieldKey = administration.customFields().addCustomField(ReferencePluginConstants.MULTI_REST_RESPRESENTATION_KEY, "test");
        issueKey = navigation.issue().createIssue(PROJECT_HOMOSAP, ISSUE_TYPE_BUG, "Test summary", Collections.singletonMap(customFieldKey, new String[]{(String) customFieldValue}));
    }

    @Test
    public void testVersionedRepresentations() throws Exception {
        final JSONObject representations = getJsonRepresentations("rest/api/2/issue/" + issueKey + "?expand=" + VERSIONED_REPRESENTATIONS);
        assertThat(representations, is(getVersionedRepresentations()));
    }

    @Test
    public void testVersionedRepresentationsAreInSelectedField() throws Exception {
        final JSONObject representations = getJsonRepresentations("rest/api/2/issue/" + issueKey + "?expand=" + VERSIONED_REPRESENTATIONS + "&fields=" + customFieldKey);
        assertThat(representations, is(getVersionedRepresentations()));
    }

    @Test
    public void testVersionedRepresentationsAreInSearch() throws JSONException {
        final JSONObject json = funcTestRestClient.getJSON("rest/api/2/search?expand=versionedRepresentations&jql=key=" + issueKey);
        final JSONObject representations = json.getJSONArray("issues").getJSONObject(0).getJSONObject(VERSIONED_REPRESENTATIONS).getJSONObject(customFieldKey);
        assertThat(representations, is(getVersionedRepresentations()));
    }


    private JSONObject getJsonRepresentations(final String url) throws JSONException {
        final JSONObject json = funcTestRestClient.getJSON(url);
        return json.getJSONObject(VERSIONED_REPRESENTATIONS).getJSONObject(customFieldKey);
    }

    private JSONObject getVersionedRepresentations() throws JSONException {
        return new JSONObject()
                .put("1", customFieldValue)
                .put("2", new JSONObject().put("value", customFieldValue))
                .put("3", new JSONObject().put("value", customFieldValue).put("type", "custom_text"));
    }
}
