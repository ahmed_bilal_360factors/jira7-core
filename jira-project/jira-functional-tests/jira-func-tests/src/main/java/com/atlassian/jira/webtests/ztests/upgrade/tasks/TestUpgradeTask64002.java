package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FuncTestRestClient;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.meterware.httpunit.WebResponse;
import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.io.IOException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.UPGRADE_TASKS;

/**
 * Ensure the JIRA News gadgets is deleted correctly
 *
 * @since v6.4
 */
@WebTest({FUNC_TEST, UPGRADE_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUpgradeTask64002 extends BaseJiraFuncTest {

    @Inject
    private FuncTestRestClient restClient;


    @Inject
    private Administration administration;

    @Test
    public void testNewsGadgetDeleted() throws IOException, SAXException, JSONException {
        administration.restoreData("TestUpgradeTask64002.xml");

        //retrieve a dashboard that contained 2 news gadgets.
        WebResponse response = restClient.GET("/rest/dashboards/1.0/10100.json");
        Assert.assertEquals("application/json", response.getContentType());

        final JSONObject dashboard = new JSONObject(response.getText());
        assertNoNewsGadgetPresent(dashboard, 3);
    }

    private void assertNoNewsGadgetPresent(final JSONObject contents, final int expectedNumberOfGadgets)
            throws JSONException {
        final JSONArray gadgets = contents.getJSONArray("gadgets");
        Assert.assertEquals(expectedNumberOfGadgets, gadgets.length());
        for (int i = 0; i < gadgets.length(); i++) {
            JSONObject gadget = gadgets.getJSONObject(i);
            Assert.assertFalse(gadget.getString("gadgetUrl").contains("news"));
        }
    }
}
