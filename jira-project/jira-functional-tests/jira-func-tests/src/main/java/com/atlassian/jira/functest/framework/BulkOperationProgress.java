package com.atlassian.jira.functest.framework;

import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.util.ProgressPageControl;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;

/**
 * Class to control the bulk operation progress page in func tests.
 */
public class BulkOperationProgress {

    private final WebTester tester;
    private final FuncTestLogger logger;

    @Inject
    public BulkOperationProgress(final WebTester tester, final FuncTestLogger logger) {
        this.tester = tester;
        this.logger = logger;
    }

    public void waitAndReloadBulkOperationProgressPage() {
        waitAndReloadBulkOperationProgressPage(tester);
    }

    public void waitForOperationProgressPage(final WebTester webTester) {
        webTester.assertTextPresent("Bulk Operation Progress");
        ProgressPageControl.wait(webTester, "bulkoperationprogressform", "Refresh", "Acknowledge");
        logger.log("waitForOperationProgressPage");
    }

    public void waitAndReloadBulkOperationProgressPage(final WebTester webTester) {
        webTester.assertTextPresent("Bulk Operation Progress");
        ProgressPageControl.waitAndReload(webTester, "bulkoperationprogressform", "Refresh", "Acknowledge");
        logger.log("waitAndReloadBulkOperationProgressPage");
    }
}