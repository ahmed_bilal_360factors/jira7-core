package com.atlassian.jira.webtests.ztests.navigator.jql;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * This is test to verify the function of security levels used together
 * with custom fields group selectors such as "single select" or "multi select".
 */
@WebTest({Category.FUNC_TEST, Category.JQL})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestGroupCustomFieldIssueSecurityLevel extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Test
    public void testUserWithCorrectPermissionsShouldSeeTheResult() {
        administration.restoreData("JRA-29196.xml");

        navigation.login("test-user");
        navigation.gotoPage("issues/?jql=");

        tester.assertTextPresent("III-1"); // multi select group field issue
        tester.assertTextPresent("JJJ-1"); // single select group field issue

        //user "dev" with insufficient permissions should not see that issues
        navigation.login("dev");
        navigation.gotoPage("issues/?jql=");

        tester.assertTextNotPresent("III-1");
        tester.assertTextNotPresent("JJJ-1");
    }

}
