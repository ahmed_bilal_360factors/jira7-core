package com.atlassian.jira.functest.framework.util;

import com.atlassian.jira.functest.framework.admin.plugins.ReferencePlugin;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.functest.framework.backdoor.PluginIndexConfigurationControl;

import static com.atlassian.jira.util.IndexDocumentConfigurationControlUtil.loadPropertyFromFile;

public class ReferencePluginReloadHelper {
    public static void updatePluginIndexConfiguration(final Backdoor backdoor, final String pluginKey, final String filename, final PluginIndexConfigurationControl pluginIndexConfigurationControl) {
        /*
            Dear future JIRAn, you may be wondering why we are firing this event here. What this code is supposed to be
            simulating is what would happen when a plugin is installed/updated with different configuration. Because
            we don't really have a way to load and unload a plugin within a func test were awkwardly forcing the running
            system to think that we have loaded a new plugin thus clearing caches and reloading whatnot's
            Specifically we need to make sure that the following events are fired

            onDisable
            class com.atlassian.jira.plugin.index.EntityPropertyIndexDocumentModuleDescriptorImpl
            class com.atlassian.jira.plugin.customfield.CustomFieldSearcherModuleDescriptorImpl x 5


            onEnable
            class com.atlassian.jira.plugin.customfield.CustomFieldSearcherModuleDescriptorImpl x 5
            class com.atlassian.jira.plugin.index.EntityPropertyIndexDocumentModuleDescriptorImpl


        */

        backdoor.plugins().disablePlugin(ReferencePlugin.KEY);
        loadPropertyFromFile(pluginKey, filename, pluginIndexConfigurationControl);
        backdoor.plugins().enablePlugin(ReferencePlugin.KEY);
    }
}
