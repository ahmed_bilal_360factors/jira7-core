package com.atlassian.jira.webtests.ztests.i18n;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.TableCellLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;

@WebTest({Category.FUNC_TEST, Category.I18N})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestI18n500Page extends BaseJiraFuncTest {
    public static final String USERNAME_NON_SYS_ADMIN = "admin_non_sysadmin";
    public static final String PASSWORD_NON_SYS_ADMIN = "admin_non_sysadmin";

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreI18nData("TestI18n.xml");
    }

    @After
    public void tearDownTest() {
        administration.generalConfiguration().setJiraLocaleToSystemDefault();
    }


    /**
     * A user with no predefined language gets the language options in the system's default language
     */
    @Test
    public void testShowsLanguageListInDefaultLanguage() {
        administration.restoreData("TestUserProfileI18n.xml");

        administration.generalConfiguration().setJiraLocale("Deutsch (Deutschland)");

        tester.gotoPage("/internal-error");

        // assert that the page defaults to German
        final int lastRow = page.getHtmlTable("language-info").getRowCount() - 1;
        textAssertions.assertTextPresent(new TableCellLocator(tester, "language-info", lastRow, 1), "Deutsch (Deutschland)");
    }

    /**
     * A user with a language preference that is different from the system's language gets the list of languages in his preferred language.
     */
    @Test
    public void testShowsLanguageListInTheUsersLanguage() {
        administration.restoreData("TestUserProfileI18n.xml");

        // set the system locale to something other than English just to be different
        administration.generalConfiguration().setJiraLocale("Deutsch (Deutschland)");

        navigation.login(FRED_USERNAME);

        tester.gotoPage("/internal-error");

        // assert that the page defaults to Spanish
        final int lastRow = page.getHtmlTable("language-info").getRowCount() - 1;
        textAssertions.assertTextPresent(new TableCellLocator(tester, "language-info", lastRow, 1), "alem\u00e1n (Alemania)");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "language-info", lastRow - 1, 0), "espa\u00f1ol (Espa\u00f1a)");
    }
}

