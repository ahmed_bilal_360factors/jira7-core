package com.atlassian.jira.functest.matcher;

import com.atlassian.jira.functest.framework.backdoor.ApplicationRoleControl;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableSet;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Set;

import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.CORE_KEY;
import static com.google.common.base.Preconditions.checkNotNull;

public class ApplicationRoleBeanMatcher extends TypeSafeDiagnosingMatcher<ApplicationRoleControl.ApplicationRoleBean> {
    private final String key;
    private final String name;
    private Set<String> groups = ImmutableSet.of();
    private Set<String> defaultGroups = ImmutableSet.of();
    private boolean selectedByDefault;
    private Integer numberOfSeats;
    private Integer remainingSeats;
    private Integer userCount;
    private Boolean hasUnlimitedSeats;

    public static ApplicationRoleBeanMatcher forCore() {
        return new ApplicationRoleBeanMatcher(CORE_KEY, "JIRA Core");
    }

    public ApplicationRoleBeanMatcher(String key, String name) {
        this.key = key;
        this.name = name;
    }

    @Override
    protected boolean matchesSafely(final ApplicationRoleControl.ApplicationRoleBean item, final Description mismatchDescription) {
        final boolean nameMatch = Objects.equal(name, item.getName());
        final boolean keyMatch = equalsIgnoreCase(key, item.getKey());
        final boolean groupMatch = sameElements(groups, item.getGroups());
        final boolean defaultGroupMatch = sameElements(defaultGroups, item.getDefaultGroups());
        final boolean selectedByDefaultMatch = sameValue(selectedByDefault, item.isSelectedByDefault());
        final boolean numberOfSeatsMatch = sameValue(numberOfSeats, item.getNumberOfSeats());
        final boolean remainingSeatsMatch = sameValue(remainingSeats, item.getRemainingSeats());
        final boolean userCountMatch = sameValue(userCount, item.getUserCount());
        final boolean hasUnlimitedSeatsMatch = sameValue(hasUnlimitedSeats, item.getHasUnlimitedSeats());
        if (nameMatch
                && keyMatch
                && groupMatch
                && defaultGroupMatch
                && selectedByDefaultMatch
                && numberOfSeatsMatch
                && remainingSeatsMatch
                && userCountMatch
                && hasUnlimitedSeatsMatch) {
            return true;
        } else {
            mismatchDescription.appendText(String.format("[name: %s, groups: %s, key: %s, defaultGroups: %s, selectedByDefault: %b, numberOfSeats: %d, remainingSeats: %d, userCount: %d, hasUnlimitedSeats: %b]",
                    item.getName(), item.getGroups(), item.getKey(), item.getDefaultGroups(), item.isSelectedByDefault(), item.getNumberOfSeats(), item.getRemainingSeats(), item.getUserCount(), item.getHasUnlimitedSeats()));
            mismatchDescription.appendText(System.lineSeparator());
            mismatchDescription.appendText(String.format("[nameMatch: %b, keyMatch %b, groupMatch %b, " +
                    "defaultGroupMatch %b, selectedByDefaultMatch %b, numberOfSeatsMatch %b, remainingSeatsMatch %b, " +
                    "remainingSeatsMatch %b, userCountMatch %b, hasUnlimitedSeatsMatch %b]",
                    nameMatch, keyMatch, groupMatch,
                    defaultGroupMatch, selectedByDefaultMatch, numberOfSeatsMatch, remainingSeatsMatch,
                    remainingSeatsMatch, userCountMatch, hasUnlimitedSeatsMatch));
            return false;
        }
    }

    /**
     * Returns true if element on the left is null, that's because when creating a matcher,
     * we may want to not specify value for some attribute. In such situation test should not fail.
     */
    private static <T> boolean sameValue(T left, T right) {
        return Objects.equal(left != null ? left : checkNotNull(right), right);
    }

    private static boolean sameElements(Collection<?> left, Collection<?> right) {
        return left.size() == right.size() && left.containsAll(right);
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText(String.format("[name: %s, groups: %s, key: %s, defaultGroups: %s, selectedByDefault: %b, numberOfSeats: %d, remainingSeats: %d, userCount: %d, hasUnlimitedSeats: %b]",
                name, groups, key, defaultGroups, selectedByDefault, numberOfSeats, remainingSeats, userCount, hasUnlimitedSeats));
    }

    public ApplicationRoleBeanMatcher setGroups(final String... groups) {
        checkNotNull(groups);
        this.groups = ImmutableSet.copyOf(groups);
        return this;
    }

    public ApplicationRoleBeanMatcher setDefaultGroups(@Nonnull final String... defaultGroups) {
        this.defaultGroups = ImmutableSet.copyOf(defaultGroups);
        return this;
    }

    public ApplicationRoleBeanMatcher setSelectedByDefault(final boolean selectedByDefault) {
        this.selectedByDefault = selectedByDefault;
        return this;
    }

    public ApplicationRoleBeanMatcher setNumberOfSeats(final Integer numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
        return this;
    }

    public ApplicationRoleBeanMatcher setRemainingSeats(final Integer remainingSeats) {
        this.remainingSeats = remainingSeats;
        return this;
    }

    public ApplicationRoleBeanMatcher setUserCount(final Integer userCount) {
        this.userCount = userCount;
        return this;
    }

    private static boolean equalsIgnoreCase(String left, String right) {
        if (left == null) {
            return right == null;
        } else {
            return left.equalsIgnoreCase(right);
        }
    }
}
