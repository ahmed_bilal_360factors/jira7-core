package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Tests that plugin state is successfully migrated from ApplicationProperties to the PluginState table
 *
 * @since v6.1
 */
@WebTest({Category.FUNC_TEST, Category.UPGRADE_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestPluginStateMigration extends BaseJiraFuncTest {

    private static final String PLUGIN_KEY = "com.atlassian.jira.jira-monitoring-plugin";
    ;

    @Inject
    private Administration administration;

    @Test
    public void testUpgrade() {
        administration.restoreDataSlowOldWay("TestPluginStateUpgrade.xml");
        checkPluginStateRespected();
    }

    @Test
    public void testPluginStateImportedCorrectly() {

        administration.plugins().enablePlugin(PLUGIN_KEY);
        administration.restoreData("TestPluginImport.xml");
        checkPluginStateRespected();
    }

    @Test
    public void testPluginStateImportsDotPrefixCorrectly() {
        administration.plugins().enablePlugin(PLUGIN_KEY);
        administration.restoreDataSlowOldWay("TestPluginDotPrefixImport.xml");
        checkPluginStateRespected();
    }

    @After
    public void tearDownTest() {
        administration.plugins().enablePlugin(PLUGIN_KEY);
    }

    private void checkPluginStateRespected() {
        Assert.assertTrue("Plugin should be disabled.", administration.plugins().isPluginDisabled("com.atlassian.jira.jira-monitoring-plugin"));
    }


}
