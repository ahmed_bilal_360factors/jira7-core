package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.testkit.client.log.FuncTestLoggerImpl;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.util.dom.DomKit.getCollapsedText;

/**
 * Default implementation of UserProfile
 *
 * @since v3.13
 */
public class UserProfileImpl implements UserProfile {

    private final WebTester tester;
    private final LocatorFactory locators;
    private final FuncTestLogger logger;

    @Inject
    public UserProfileImpl(final WebTester tester, final JIRAEnvironmentData environmentData, final Navigation navigation) {
        this.tester = tester;
        this.locators = new LocatorFactoryImpl(tester);
        this.logger = new FuncTestLoggerImpl(2);
    }

    @Override
    public String userName() {
        gotoCurrentUserProfile();

        return getCollapsedText(locators.id("up-user-title-name").getNode());
    }

    @Override
    public Link link() {
        return new DefaultLink(tester);
    }

    public void changeUserNotificationType(final boolean useHtml) {
        logger.log("Changing user notification type to " + (useHtml ? "HTML" : "plain text"));

        gotoCurrentUserProfile();
        gotoEditUserPreferences();

        if (useHtml) {
            tester.selectOption("userNotificationsMimeType", "HTML");
        } else {
            tester.selectOption("userNotificationsMimeType", "Text");
        }
        tester.submit();
    }

    public void changeUserSharingType(boolean global) {
        logger.log("changing user sharing default to '" + (global ? "public" : "private") + "'.");

        gotoCurrentUserProfile();
        gotoEditUserPreferences();

        tester.setFormElement("shareDefault", String.valueOf(!global));
        tester.submit();
    }

    public void changeDefaultSharingType(boolean global) {
        logger.log("changing default user sharing to '" + (global ? "public" : "private") + "'.");

        gotoEditDefaultPreferences();

        tester.setFormElement("sharePublic", String.valueOf(!global));
        tester.submit("Update");
    }

    public void changeUserLanguage(final String lang) {
        logger.log("Changing user language to '" + lang + "'");

        gotoCurrentUserProfile();
        gotoEditUserPreferences();

        tester.selectOption("userLocale", lang);
        tester.submit();
    }

    @Override
    public void changeUserLanguageByValue(String langValue) {
        logger.log("Changing user language to '" + langValue + "'");

        gotoCurrentUserProfile();
        gotoEditUserPreferences();

        tester.getDialog().setFormParameter("userLocale", langValue);
        tester.submit();
    }

    @Override
    public void changeUserLanguageToJiraDefault() {
        changeUserLanguageByValue("-1");
    }

    private void gotoEditDefaultPreferences() {
        tester.gotoPage("secure/admin/jira/EditUserDefaultSettings!default.jspa");
    }

    private void gotoEditUserPreferences() {
        tester.clickLink("edit_prefs_lnk");
    }

    public void gotoCurrentUserProfile() {
        tester.clickLink("view_profile");
    }

    public void gotoUserProfile(String userName) {
        tester.gotoPage("/secure/ViewProfile.jspa?name=" + userName);
    }

    @Override
    public UserProfile changeUserTimeZone(String timeZoneID) {
        gotoCurrentUserProfile();
        gotoEditUserPreferences();
        tester.setFormElement("defaultUserTimeZone", timeZoneID == null ? "JIRA" : timeZoneID);
        tester.submit();

        return this;
    }

    @Override
    public UserProfile changeNotifyMyChanges(boolean notify) {
        gotoCurrentUserProfile();
        gotoEditUserPreferences();
        tester.selectOption("notifyOwnChanges", notify ? "Notify me" : "Do not notify me");
        tester.submit();

        return this;
    }

    @Override
    public UserProfile changeAutowatch(boolean autowatch) {
        gotoCurrentUserProfile();
        gotoEditUserPreferences();
        tester.selectOption("autoWatchPreference", autowatch ? "Enabled" : "Disabled");
        tester.submit();

        return this;
    }

    public static class DefaultLink implements Link {
        private final WebTester tester;

        public DefaultLink(WebTester tester) {
            this.tester = tester;
        }

        @Override
        public boolean isPresent() {
            return new IdLocator(tester, "header-details-user-fullname").exists();
        }
    }
}
