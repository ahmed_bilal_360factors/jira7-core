package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.framework.parser.Header;
import com.atlassian.jira.functest.framework.parser.IssueNavigatorParser;
import com.atlassian.jira.functest.framework.parser.IssueNavigatorParserImpl;
import com.atlassian.jira.functest.framework.parser.IssueParser;
import com.atlassian.jira.functest.framework.parser.IssueParserImpl;
import com.atlassian.jira.functest.framework.parser.SystemInfoParser;
import com.atlassian.jira.functest.framework.parser.SystemInfoParserImpl;
import com.atlassian.jira.functest.framework.parser.filter.FilterParser;
import com.atlassian.jira.functest.framework.parser.filter.FilterParserImpl;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;


public class ParserImpl implements Parser {
    private final IssueParser issue;
    private final FilterParser filter;
    private final SystemInfoParser systemInfoParser;
    private final IssueNavigatorParser issueNavigatorParser;
    private final LocatorFactory locators;

    @Inject
    public ParserImpl(final WebTester tester,
                      final JIRAEnvironmentData environmentData) {
        this.issue = new IssueParserImpl(tester, environmentData);
        this.filter = new FilterParserImpl(tester, environmentData);
        this.systemInfoParser = new SystemInfoParserImpl(tester, environmentData, 2, new NavigationImpl(tester, environmentData));
        this.issueNavigatorParser = new IssueNavigatorParserImpl(tester, environmentData, 2);
        this.locators = new LocatorFactoryImpl(tester);
    }


    public IssueParser issue() {
        return issue;
    }

    public FilterParser filter() {
        return filter;
    }

    public SystemInfoParser systemInfo() {
        return systemInfoParser;
    }

    public IssueNavigatorParser navigatorParser() {
        return issueNavigatorParser;
    }

    @Override
    public Header header() {
        return () -> locators.id("header-details-user-fullname").getNode().getAttributes().getNamedItem("data-displayname").getTextContent();
    }
}
