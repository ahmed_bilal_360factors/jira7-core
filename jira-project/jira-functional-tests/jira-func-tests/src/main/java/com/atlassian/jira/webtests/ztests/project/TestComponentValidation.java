package com.atlassian.jira.webtests.ztests.project;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.SessionFactory;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.COMPONENTS_AND_VERSIONS, Category.PROJECTS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestComponentValidation extends BaseJiraFuncTest {

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Inject
    private SessionFactory sessionFactory;

    @Before
    public void setUpTest() {
        administration.restoreData("TestBrowseProjectRoadmapAndChangeLogTab.xml");
        backdoor.usersAndGroups().addUser("dev", "dev", "Developer", "dev@example.com");
        backdoor.usersAndGroups().addUserToGroup("dev", "jira-developers");
    }

    @Test
    public void testComponentValidationSwitchingProjectsUnderneath() {
        navigation.issue().viewIssue("LOTS-1");
        tester.clickLink("edit-issue");

        sessionFactory.begin().withSession(()->{
            navigation.login(ADMIN_USERNAME);
            navigation.issue().viewIssue("LOTS-1");
            tester.clickLink("move-issue");
            tester.setFormElement("pid", "10051");
            tester.submit("Next >>");
            tester.submit("Next >>");
            tester.submit("Move");
            assertions.assertNodeExists("//a[@title='Component 1 ']");
        });

        tester.submit("Update");

        assertions.assertNodeHasText("//*[@class='error']", "Components Component 1(10030), Component 3(10031) are not valid for project 'All Released'.");
        assertions.assertNodeHasText("//*[@id='key-val']", "RELEASED-1");
    }

    @Test
    public void testComponentValidationNonExistantComponent() {
        navigation.login("dev");
        tester.gotoPage(page.addXsrfToken("/secure/EditIssue.jspa?id=10000&summary=LOTS-1&components=99&fixVersions=999&assignee=admin&reporter=admin&issuetype=1"));

        //this validation error now only shows up for non project-admins. Project admins are allowed to create non-existing components.
        assertions.assertNodeHasText("//*[@class='error']", "Component with id '99' does not exist.");
    }
}
