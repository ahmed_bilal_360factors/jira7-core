package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import com.atlassian.jira.project.version.Version;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.net.URI;
import java.util.Date;

public class VersionBean {

    private URI self;

    private String id;

    private String description;

    private String name;

    private boolean archived;

    private boolean released;

    private Date startDate;

    private boolean startDateSet = false;

    private Date releaseDate;

    /**
     * This field is used to trap the fact the Release Date has been set, even though it may have been set to null.
     */
    private boolean releaseDateSet = false;

    private boolean overdue;

    private String userStartDate;

    private String userReleaseDate;

    private Long projectId;

    private URI moveUnfixedIssuesTo;

    public Long getProjectId() {
        return projectId;
    }

    public String getUserStartDate() {
        return userStartDate;
    }

    public String getUserReleaseDate() {
        return userReleaseDate;
    }

    public boolean getOverdue() {
        return overdue;
    }

    public Date getStartDate() {
        return startDate;
    }

    public boolean isStartDateSet() {
        return startDateSet;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public boolean isReleaseDateSet() {
        return releaseDateSet;
    }

    public boolean isReleased() {
        return released;
    }

    public boolean isArchived() {
        return archived;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public URI getSelf() {
        return self;
    }

    public URI getMoveUnfixedIssuesTo() {
        return moveUnfixedIssuesTo;
    }

    public void setSelf(URI self) {
        this.self = self;
    }

    public void setId(Long id) {
        this.id = id == null ? null : id.toString();
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public void setReleased(boolean released) {
        this.released = released;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
        this.startDateSet = true;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
        this.releaseDateSet = true;
    }

    public void setOverdue(boolean overdue) {
        this.overdue = overdue;
    }

    public void setUserStartDate(String userStartDate) {
        this.userStartDate = userStartDate;
    }

    public void setUserReleaseDate(String userReleaseDate) {
        this.userReleaseDate = userReleaseDate;
    }

    public void setMoveUnfixedIssuesTo(URI moveUnfixedIssuesTo) {
        this.moveUnfixedIssuesTo = moveUnfixedIssuesTo;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    //Needed so that JAXB works.
    public VersionBean() {
    }

    private VersionBean(Long id, Long projectId, URI self, String name, String description, boolean archived, boolean released,
                        Date startDate, boolean startDateSet, String userStartDate,
                        Date releaseDate, boolean releaseDateSet, String userReleaseDate,
                        boolean overdue) {
        this.id = id == null ? null : id.toString();
        this.self = self;
        this.description = description;
        this.name = name;
        this.archived = archived;
        this.released = released;

        this.startDate = startDate;
        this.startDateSet = startDateSet;
        this.userStartDate = userStartDate;

        this.releaseDate = releaseDate;
        this.releaseDateSet = releaseDateSet;
        this.userReleaseDate = userReleaseDate;

        this.overdue = overdue;
        this.projectId = projectId;
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VersionBean that = (VersionBean) o;

        if (archived != that.archived) return false;
        if (overdue != that.overdue) return false;
        if (releaseDateSet != that.releaseDateSet) return false;
        if (released != that.released) return false;
        if (startDateSet != that.startDateSet) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (moveUnfixedIssuesTo != null ? !moveUnfixedIssuesTo.equals(that.moveUnfixedIssuesTo) : that.moveUnfixedIssuesTo != null)
            return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (projectId != null ? !projectId.equals(that.projectId) : that.projectId != null) return false;
        if (releaseDate != null ? !releaseDate.equals(that.releaseDate) : that.releaseDate != null) return false;
        if (self != null ? !self.equals(that.self) : that.self != null) return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;
        if (userReleaseDate != null ? !userReleaseDate.equals(that.userReleaseDate) : that.userReleaseDate != null)
            return false;
        if (userStartDate != null ? !userStartDate.equals(that.userStartDate) : that.userStartDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = self != null ? self.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (archived ? 1 : 0);
        result = 31 * result + (released ? 1 : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (startDateSet ? 1 : 0);
        result = 31 * result + (releaseDate != null ? releaseDate.hashCode() : 0);
        result = 31 * result + (releaseDateSet ? 1 : 0);
        result = 31 * result + (overdue ? 1 : 0);
        result = 31 * result + (userStartDate != null ? userStartDate.hashCode() : 0);
        result = 31 * result + (userReleaseDate != null ? userReleaseDate.hashCode() : 0);
        result = 31 * result + (projectId != null ? projectId.hashCode() : 0);
        result = 31 * result + (moveUnfixedIssuesTo != null ? moveUnfixedIssuesTo.hashCode() : 0);
        return result;
    }

    public static class Builder {
        private URI self;
        private Long id;
        private String description;
        private String name;
        private boolean archived;
        private boolean released;
        private Date startDate;
        private boolean startDateSet;
        private Date releaseDate;
        private boolean releaseDateSet;
        private boolean overdue;
        private String userStartDate;
        private String userReleaseDate;
        private Long projectId;
        private String expand;

        public URI getSelf() {
            return self;
        }

        public Builder setSelf(URI self) {
            this.self = self;
            return this;
        }

        public Builder setVersion(Version version) {
            this.id = version.getId();
            this.name = version.getName();
            this.description = StringUtils.stripToNull(version.getDescription());
            this.startDate = version.getStartDate();
            this.releaseDate = version.getReleaseDate();
            this.archived = version.isArchived();
            this.released = version.isReleased();
            return this;
        }

        public Long getId() {
            return id;
        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public String getDescription() {
            return description;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public String getName() {
            return name;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public boolean isArchived() {
            return archived;
        }

        public Builder setArchived(boolean archived) {
            this.archived = archived;
            return this;
        }

        public boolean isReleased() {
            return released;
        }

        public Builder setReleased(boolean released) {
            this.released = released;
            return this;
        }

        public Date getStartDate() {
            return startDate;
        }

        public Builder setStartDate(Date startDate) {
            this.startDate = startDate;
            this.startDateSet = true;
            return this;
        }

        public Date getReleaseDate() {
            return releaseDate;
        }

        public Builder setReleaseDate(Date releaseDate) {
            this.releaseDate = releaseDate;
            this.releaseDateSet = true;
            return this;
        }

        public boolean getOverdue() {
            return overdue;
        }

        public Builder setOverdue(boolean overdue) {
            this.overdue = overdue;
            return this;
        }

        public String getUserStartDate() {
            return userStartDate;
        }

        public Builder setUserStartDate(String userStartDate) {
            this.userStartDate = userStartDate;
            return this;
        }

        public String getUserReleaseDate() {
            return userReleaseDate;
        }

        public Builder setUserReleaseDate(String userReleaseDate) {
            this.userReleaseDate = userReleaseDate;
            return this;
        }


        public Builder setProjectId(final Long projectId) {
            this.projectId = projectId;
            return this;
        }

        public Builder setExpand(final String expand) {
            this.expand = expand;
            return this;
        }

        public VersionBean build() {
            return new VersionBean(id, projectId, self, name, description, archived, released,
                    startDate, startDateSet, userStartDate, releaseDate, releaseDateSet,
                    userReleaseDate, overdue);
        }
    }

}