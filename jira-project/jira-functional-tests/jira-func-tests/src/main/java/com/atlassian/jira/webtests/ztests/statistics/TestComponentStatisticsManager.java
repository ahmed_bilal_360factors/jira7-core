package com.atlassian.jira.webtests.ztests.statistics;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.ResourceRef;
import com.atlassian.jira.testkit.client.restclient.Component;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Map;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.PROJECTS})
public class TestComponentStatisticsManager extends BaseJiraRestTest {
    public static final String HSP_PROJECT = "HSP";
    public static final String MKY_PROJECT = "MKY";
    @Inject
    private Backdoor backdoor;

    @Before
    public void setUpTest() {
        backdoor.restoreDataFromResource("blankprojects.xml");
        
        Component component1 = new Component();
        component1.name("Component 1");
        component1.description("Component 1");
        component1.project(HSP_PROJECT);
        component1 = backdoor.components().create(component1);

        Component component2 = new Component();
        component2.name("Component 2");
        component2.description("Component 2");
        component2.project(HSP_PROJECT);
        component2 = backdoor.components().create(component2);

        Component component3 = new Component();
        component3.name("Component 3");
        component3.description("Component 3");
        component3.project(MKY_PROJECT);
        component3 = backdoor.components().create(component3);

        createIssue("Issue 1", component1);
        createIssue("Issue 2", component1);
        createIssue("Issue 3", component1);
        createIssue("Issue 4", component2);
        createIssue("Issue 5", "HSP");
        createIssue("Issue 6", component3);
        createIssue("Issue 7", component3);
    }

    private String createIssue(String description, String projectKey) {
        return backdoor.issues().createIssue(projectKey, description).key;
    }

    private String createIssue(String description, Component component) {
        String key = createIssue(description, component.project);

        IssueFields fields = new IssueFields();
        fields.components(ResourceRef.withId(component.id.toString()));
        backdoor.issues().setIssueFields(key, fields);

        return key;
    }

    @Test
    public void testCorrectProjectsAndComponents() {
        Map<String, Map<String, Integer>> result = backdoor.statisticsControl().getComponentsResultingFrom("");

        assertThat("Both projects were returned", result.keySet(), contains(HSP_PROJECT, MKY_PROJECT));

        assertThat("Only expected components were returned", result.get(HSP_PROJECT).size(), is(5));
        assertThat("Only expected components were returned", result.get(MKY_PROJECT).size(), is(1));

        assertThat("Empty components are present", result.get(HSP_PROJECT).get("New Component 1"), is(0));
        assertThat("Empty components are present", result.get(HSP_PROJECT).get("New Component 2"), is(0));
        assertThat("Empty components are present", result.get(HSP_PROJECT).get("New Component 3"), is(0));

        assertThat("Non empty components are present", result.get(HSP_PROJECT).get("Component 1"), is(3));
        assertThat("Non empty components are present", result.get(HSP_PROJECT).get("Component 2"), is(1));
        assertThat("Non empty components are present", result.get(MKY_PROJECT).get("Component 3"), is(2));
    }

    @Test
    public void testCorrectProjectsAndComponentsWithQuery() {
        Map<String, Map<String, Integer>> result = backdoor.statisticsControl().getComponentsResultingFrom("project = HSP");

        assertThat("HSP project was returned", result.keySet(), contains(HSP_PROJECT));

        assertThat("Only expected components were returned", result.get(HSP_PROJECT).size(), is(5));

        assertThat("Empty components are present", result.get(HSP_PROJECT).get("New Component 1"), is(0));
        assertThat("Empty components are present", result.get(HSP_PROJECT).get("New Component 2"), is(0));
        assertThat("Empty components are present", result.get(HSP_PROJECT).get("New Component 3"), is(0));

        assertThat("Non empty components are present", result.get(HSP_PROJECT).get("Component 1"), is(3));
        assertThat("Non empty components are present", result.get(HSP_PROJECT).get("Component 2"), is(1));
    }
}
