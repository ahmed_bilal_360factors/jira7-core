package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Errors;
import com.atlassian.jira.testkit.client.restclient.ProjectRole;
import com.atlassian.jira.testkit.client.restclient.ProjectRoleClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.security.roles.ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE;
import static com.atlassian.jira.security.roles.ProjectRoleActor.USER_ROLE_ACTOR_TYPE;
import static java.util.Arrays.asList;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

/**
 * @since v4.4
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestProjectRoleResource.xml")
public class TestProjectRoleResource extends BaseJiraFuncTest {
    private ProjectRoleClient projectRoleClient;

    @Test
    public void testViewProjectRoles() throws Exception {
        viewProjectRoles("10001");
        viewProjectRoles("MKY");
    }

    private void viewProjectRoles(final String projectIdOrKey) throws JSONException {
        final Map<String, String> mky = projectRoleClient.get(projectIdOrKey);
        assertEquals(3, mky.size());

        // We want some rudimentary testing that the URLs actually go someplace.
        List<String> roles = asList("Users", "Administrators", "Developers");
        for (String role : roles) {
            final JSONObject jsonRole = getJSON(mky.get(role));
            assertEquals(role, jsonRole.get("name"));
        }
    }

    @Test
    public void testViewRole() throws Exception {
        viewRole("10001");
        viewRole("MKY");
    }

    private void viewRole(final String projectIdOrKey) {
        final ProjectRole projectRole = projectRoleClient.get(projectIdOrKey, "Users");
        assertEquals("Users", projectRole.name);
        assertEquals("A project role that represents users in a project", projectRole.description);
        assertNotNull(projectRole.self);
        assertNotNull(projectRole.id);

        assertEquals(2, projectRole.actors.size());

        // We turn this into a map so that we can always find the one we care about to perform assertions on
        // without having to worry about order stability making this an intermittently failing test
        final Map<String, ProjectRole.Actor> map = makeMap(projectRole.actors);
        ProjectRole.Actor actor = map.get("admin");

        assertEquals("Administrator", actor.displayName);
        assertEquals(USER_ROLE_ACTOR_TYPE, actor.type);
        assertEquals("admin", actor.name);
    }

    @Test
    public void testViewRoleActorsIsSortedByDisplayName() throws Exception {
        ProjectRole projectRole = projectRoleClient.get("MKY", "Users");

        assertEquals(2, projectRole.actors.size());

        final ProjectRole.Actor admin = projectRole.actors.get(0);

        assertEquals("Administrator", admin.displayName);
        assertEquals(USER_ROLE_ACTOR_TYPE, admin.type);
        assertEquals("admin", admin.name);

        final ProjectRole.Actor actor = projectRole.actors.get(1);

        assertEquals("jira-users", actor.displayName);
        assertEquals(GROUP_ROLE_ACTOR_TYPE, actor.type);
        assertEquals("jira-users", actor.name);

        backdoor.usersAndGroups().addUser("aaaa", "aaaa", "zzzz", "aaa@aaa.com");
        backdoor.usersAndGroups().addUser("zzzz", "zzzz", "aaaa", "zzz@zzz.com");

        projectRoleClient.addActors("MKY", "Users", new String[]{}, new String[]{"aaaa", "zzzz"});

        projectRole = projectRoleClient.get("MKY", "Users");

        assertEquals(4, projectRole.actors.size());

        assertEquals("aaaa", projectRole.actors.get(0).displayName);
        assertEquals("Administrator", projectRole.actors.get(1).displayName);
        assertEquals("jira-users", projectRole.actors.get(2).displayName);
        assertEquals("zzzz", projectRole.actors.get(3).displayName);

    }

    @Test
    public void testSetRoleActors() throws Exception {
        ProjectRole projectRole = projectRoleClient.get("MKY", "Users");

        List<ProjectRole.Actor> actors = projectRole.actors;
        assertEquals(2, actors.size());

        final ProjectRole.Actor admin = actors.get(0);

        assertEquals("Administrator", admin.displayName);
        assertEquals(USER_ROLE_ACTOR_TYPE, admin.type);
        assertEquals("admin", admin.name);

        final ProjectRole.Actor actor = actors.get(1);

        assertEquals("jira-users", actor.displayName);
        assertEquals(GROUP_ROLE_ACTOR_TYPE, actor.type);
        assertEquals("jira-users", actor.name);

        backdoor.usersAndGroups().addUser("aaaa", "aaaa", "zzzz", "aaa@aaa.com");
        backdoor.usersAndGroups().addUser("zzzz", "zzzz", "aaaa", "zzz@zzz.com");
        backdoor.usersAndGroups().addGroup("ladida");

        projectRoleClient.setActors("MKY", "Users", ImmutableMap.<String, String[]>builder()
                .put(USER_ROLE_ACTOR_TYPE, new String[]{"aaaa", "zzzz"})
                .put(GROUP_ROLE_ACTOR_TYPE, new String[]{"ladida"})
                .build());

        projectRole = projectRoleClient.get("MKY", "Users");
        actors = projectRole.actors;

        assertEquals(3, actors.size());

        assertEquals("aaaa", actors.get(0).displayName);
        assertEquals(USER_ROLE_ACTOR_TYPE, actors.get(0).type);
        assertEquals("zzzz", actors.get(0).name);

        assertEquals("ladida", actors.get(1).displayName);
        assertEquals(GROUP_ROLE_ACTOR_TYPE, actors.get(1).type);
        assertEquals("ladida", actors.get(1).name);

        assertEquals("zzzz", actors.get(2).displayName);
        assertEquals(USER_ROLE_ACTOR_TYPE, actors.get(2).type);
        assertEquals("aaaa", actors.get(2).name);

        final Response response = projectRoleClient.setActors("MKY", "Users", ImmutableMap.<String, String[]>builder()
                .put(USER_ROLE_ACTOR_TYPE, new String[]{"aaaa"})
                .put(GROUP_ROLE_ACTOR_TYPE, new String[]{"azza"})
                .build());

        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);
        assertEquals(new Errors().addError("We can't find 'azza' in any accessible user directory. Check they're still active and available, and try again."), response.entity);
    }


    Map<String, ProjectRole.Actor> makeMap(final Collection<ProjectRole.Actor> actors) {
        final Map<String, ProjectRole.Actor> map = new HashMap<>();
        for (ProjectRole.Actor actor : actors) {
            assertFalse(map.containsKey(actor.name));
            map.put(actor.name, actor);
        }
        return map;
    }

    @Test
    public void testDeleteRoleActor() throws Exception {
        final ProjectRole projectRole = projectRoleClient.get("MKY", "Users");
        assertEquals(2, projectRole.actors.size());

        projectRoleClient.deleteGroup("MKY", "Users", "jira-users");
        assertEquals(1, projectRoleClient.get("MKY", "Users").actors.size());

        projectRoleClient.deleteUser("MKY", "Users", "admin");
        assertEquals(0, projectRoleClient.get("MKY", "Users").actors.size());
    }

    @Test
    public void testAddRoleActor() throws Exception {
        ProjectRole projectRole = projectRoleClient.get("MKY", "Developers");
        assertEquals(0, projectRole.actors.size());

        projectRoleClient.addActors("MKY", "Developers", new String[]{"jira-developers"}, null);

        projectRole = projectRoleClient.get("MKY", "Developers");
        assertEquals(1, projectRole.actors.size());
        final ProjectRole.Actor actor = projectRole.actors.get(0);
        assertEquals("jira-developers", actor.name);
        assertEquals("jira-developers", actor.displayName);
        assertEquals(GROUP_ROLE_ACTOR_TYPE, actor.type);
    }

    @Test
    public void testAddRoleActors() throws Exception {
        ProjectRole projectRole = projectRoleClient.get("MKY", "Developers");
        assertEquals(0, projectRole.actors.size());

        projectRoleClient.addActors("MKY", "Developers", new String[]{"jira-users", "jira-administrators", "jira-developers"},
                new String[]{"admin"});

        projectRole = projectRoleClient.get("MKY", "Developers");
        assertEquals(4, projectRole.actors.size());

        final ProjectRole.Actor admin = projectRole.actors.get(0);
        assertEquals("admin", admin.name);
        assertEquals("Administrator", admin.displayName);
        assertEquals(USER_ROLE_ACTOR_TYPE, admin.type);

        final ProjectRole.Actor administrators = projectRole.actors.get(1);
        assertEquals("jira-administrators", administrators.name);
        assertEquals("jira-administrators", administrators.displayName);
        assertEquals(GROUP_ROLE_ACTOR_TYPE, administrators.type);

        final ProjectRole.Actor developers = projectRole.actors.get(2);
        assertEquals("jira-developers", developers.name);
        assertEquals("jira-developers", developers.displayName);
        assertEquals(GROUP_ROLE_ACTOR_TYPE, developers.type);

        final ProjectRole.Actor users = projectRole.actors.get(3);
        assertEquals("jira-users", users.name);
        assertEquals("jira-users", users.displayName);
        assertEquals(GROUP_ROLE_ACTOR_TYPE, users.type);

    }

    @Before
    public void setUp() {
        projectRoleClient = new ProjectRoleClient(environmentData);
    }

    JSONObject getJSON(final String uri) throws JSONException {
        tester.getDialog().gotoPage(uri);
        return new JSONObject(tester.getDialog().getResponseText());
    }
}