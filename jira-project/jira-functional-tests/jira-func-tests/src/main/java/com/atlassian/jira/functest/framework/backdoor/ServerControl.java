package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ClientInfoStatus;

/**
 * System-level control.
 *
 * @since v7.3
 */
public class ServerControl extends BackdoorControl<ServerControl> {
    private static final Logger Log = LoggerFactory.getLogger(ServerControl.class);

    public ServerControl(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    private WebResource serverResource() {
        return createResource().path("server");
    }

    /**
     * Kill the server, an unclean shutdown.
     */
    public void kill() {
        try {
            serverResource().path("kill").post();

            //Should not get here
            throw new RuntimeException("Expected server to be killed.");
        } catch (ClientHandlerException e) {
            //Should get connection refused since the server died if this call is successful
        }
    }

    public void dumpThreads() {
        try {
            serverResource().path("dumpThreads").post();
        } catch (Exception e) {
            Log.warn("Problem trying to dump threads through backdoor, JIRA may not be in a stable state " +
                    "(starting up, shutting down, upgrading, etc.)");
            throw new RuntimeException(e);
        }
    }

    public void recordLogs(boolean enable) {
        if (enable) {
            serverResource().path("enable-logging").post();
        } else {
            serverResource().path("disable-logging").post();
        }
    }

    public String getLogs() {
        try {
            return serverResource().path("get-logs").get(String.class);
        } catch (UniformInterfaceException e) {
            if (e.getResponse().getStatus() == 204) {
                throw new IllegalStateException("Server returned en empty response. Did you forget to call #recordLogs(true)?");
            } else {
                throw e;
            }
        }
    }
}
