package com.atlassian.jira.webtests.ztests.bundledplugins2.gadget;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Tests the "Assigned to Me" gadget.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.GADGETS})
@LoginAs(user = ADMIN_USERNAME)
public class TestAssignedToMeGadget extends BaseJiraFuncTest {
    // JRA-14238
    @Inject
    private Administration administration;

    @Test
    public void testXssInImageUrls() throws Exception {
        administration.restoreData("TestImageUrlXss.xml");

        tester.gotoPage("/rest/gadget/1.0/issueTable/jql?jql=assignee+%3D+currentUser()+AND+resolution+%3D+unresolved+ORDER+BY+priority+DESC,+created+ASC&num=10&addDefault=true&enableSorting=true&sortBy=null&paging=true&startIndex=0&showActions=true");

        // priority icon URL
        tester.assertTextNotPresent("\"'/><script>alert('prioritiezz');</script>");

        // issue type icon URL
        tester.assertTextNotPresent("\"'/><script>alert('issue typezz');</script>");
    }
}
