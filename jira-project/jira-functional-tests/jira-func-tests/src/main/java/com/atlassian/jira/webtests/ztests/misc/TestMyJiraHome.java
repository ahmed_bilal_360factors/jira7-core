package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG;
import static org.junit.Assert.assertTrue;

@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestMyJiraHome extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Test
    public void testHome() throws SAXException {
        administration.restoreBlankInstance();

        // turn on the dark feature
        backdoor.darkFeatures().enableForSite("atlassian.darkfeature.jira.myjirahome");
        backdoor.darkFeatures().enableForSite(DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG);
        navigation.login("admin");

        // set home to issue navigator and then login again
        tester.clickLink("set_my_jira_home_issuenav");
        assertTrue("Expected to be on issue navigator", tester.getDialog().getResponsePageTitle().contains("Issue Navigator"));

        navigation.logout();
        navigation.login("admin");
        // assert we are on the isue nav page
        assertTrue("Expected to be on issue navigator", tester.getDialog().getResponsePageTitle().contains("Issue Navigator"));


        navigation.logout();
        navigation.login("fred");
        // No home set so assert we are on the dashboard
        tester.assertElementPresent("dashboard");


        // Assert can't set home to admin
        tester.assertLinkNotPresent("set_my_jira_home_admin");
        // set home to issue navigator and then login again
        tester.clickLink("set_my_jira_home_issuenav");
        assertTrue("Expected to be on issue navigator", tester.getDialog().getResponsePageTitle().contains("Issue Navigator"));

        navigation.logout();
        navigation.login("fred");
        // assert we are on the isue nav page
        assertTrue("Expected to be on issue navigator", tester.getDialog().getResponsePageTitle().contains("Issue Navigator"));


    }
}
