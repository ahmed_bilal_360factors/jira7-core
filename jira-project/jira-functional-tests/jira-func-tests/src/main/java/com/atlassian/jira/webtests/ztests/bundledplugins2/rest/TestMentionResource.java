package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FuncTestRestClient;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.v2.issue.users.IssueInvolvementBean;
import com.atlassian.jira.rest.v2.issue.users.UserIssueRelevanceBean;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONObject;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.meterware.httpunit.WebResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.Is.is;

@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
public class TestMentionResource extends BaseJiraFuncTest {

    @Inject
    private FuncTestRestClient restClient;

    private String issueKey;

    @Before
    public void setUp() throws Exception {
        backdoor.restoreBlankInstance();

        createUsers();

        issueKey = backdoor.issues().createIssue("HSP", "A test issue for the mention rest test").key();
    }

    @Test
    public void shouldReturnUserWithMultipleRoles() throws Exception {
        final List<UserIssueRelevanceBean> results = getResultsForIssue(issueKey, 1, "");

        Assert.assertThat(results, hasSize(1));
        final List<String> involvements = results.get(0)
                .getIssueInvolvements().stream()
                .map(IssueInvolvementBean::getId)
                .map(String::toLowerCase)
                .collect(CollectorsUtil.toImmutableList());
        Assert.assertThat(involvements, containsInAnyOrder("assignee", "reporter", "watcher"));
    }

    @Test
    public void shouldReturnUserWithMultipleRolesIfSearchedByUserName() throws Exception {
        final List<UserIssueRelevanceBean> results = getResultsForIssue(issueKey, 1, "admin");

        Assert.assertThat(results, hasSize(1));
        final List<String> involvements = results.get(0)
                .getIssueInvolvements().stream()
                .map(IssueInvolvementBean::getId)
                .map(String::toLowerCase)
                .collect(CollectorsUtil.toImmutableList());
        Assert.assertThat(involvements, containsInAnyOrder("assignee", "reporter", "watcher"));
    }

    @Test
    public void shouldReturnUserWithMultipleRolesIfSearchedByRole() throws Exception {
        final List<UserIssueRelevanceBean> results = getResultsForIssue(issueKey, 1, "assignee");

        Assert.assertThat(results, hasSize(1));
        final List<String> involvements = results.get(0)
                .getIssueInvolvements().stream()
                .map(IssueInvolvementBean::getId)
                .map(String::toLowerCase)
                .collect(CollectorsUtil.toImmutableList());
        Assert.assertThat(involvements, containsInAnyOrder("assignee", "reporter", "watcher"));
    }

    @Test
    public void shouldFindUserByUsername() throws Exception {
        final List<UserIssueRelevanceBean> results = getResultsForIssue(issueKey, 1, "admin");

        Assert.assertThat(results.size(), is(1));
    }

    @Test
    public void shouldFindUserByFullname() throws Exception {
        final List<UserIssueRelevanceBean> results = getResultsForIssue(issueKey, 1, "Administrator");

        Assert.assertThat(results.size(), is(1));
    }

    @Test
    public void shouldFilterUsers() throws Exception {
        final List<UserIssueRelevanceBean> results = getResultsForIssue(issueKey, 10, "someStringThatHopefullyNoUserWillEverHave");

        Assert.assertThat(results.size(), is(0));
    }

    @Test
    public void shouldSearchByPreffixMatching() throws Exception {
        final List<UserIssueRelevanceBean> results = getResultsForIssue(issueKey, 10, "dmin");

        Assert.assertThat(results.size(), is(0));
    }

    @Test
    public void shouldReturnSeveralUsersForSomeFilters() throws Exception {
        final List<UserIssueRelevanceBean> results = getResultsForIssue(issueKey, 10, "p");

        //Should find "per%cent" and "pl+us"
        Assert.assertThat(results.size(), is(2));
    }

    @Test
    public void shouldReturnOtherUsersIfInsufficientWithRoles() throws Exception {
        final List<UserIssueRelevanceBean> results = getResultsForIssue(issueKey, 5, "");

        Assert.assertThat(results.size(), is(5));
        //Assert that at least one user is there with no involvement (expect 1-4)
        Assert.assertThat(results.get(1).getIssueInvolvements(), is(empty()));
    }

    @Test
    public void shouldReturnInvolvedUsersBeforeOthers() throws Exception {
        final List<UserIssueRelevanceBean> results = getResultsForIssue(issueKey, 5, "");

        //Expect user 1 has a role defined, user 2-5 doesn't
        Assert.assertThat(results.size(), is(5));
        Assert.assertThat(results.get(0).getIssueInvolvements(), is(not(empty())));
        for (int i = 1; i < 5; i++) {
            Assert.assertThat(results.get(i).getIssueInvolvements(), is(empty()));
        }
    }

    @Test
    public void shouldOnlyReturnMaxResults() throws Exception {
        final List<UserIssueRelevanceBean> resultsMax2 = getResultsForIssue(issueKey, 2, "");
        Assert.assertThat(resultsMax2.size(), is(2));

        final List<UserIssueRelevanceBean> resultsMax1 = getResultsForIssue(issueKey, 1, "");
        Assert.assertThat(resultsMax1.size(), is(1));
    }

    private List<UserIssueRelevanceBean> getResultsForIssue(final String key, final int maxResults, final String search) throws Exception {
        final ImmutableList.Builder<UserIssueRelevanceBean> list = ImmutableList.builder();

        final WebResponse response = restClient.GET("/rest/internal/2/user/mention/search?issueKey=" + key + "&maxResults=" + maxResults + "&query=" + search);
        final JSONArray jsonArray = new JSONArray(response.getText());
        for (int i = 0; i < jsonArray.length(); i++) {
            final JSONObject currentUser = jsonArray.getJSONObject(i);

            final JSONArray involvementArray = currentUser.getJSONArray("issueInvolvements");
            final ImmutableList.Builder<IssueInvolvementBean> involvementList = ImmutableList.builder();
            for (int j = 0; j < involvementArray.length(); j++) {
                final JSONObject involvement = involvementArray.getJSONObject(j);
                involvementList.add(new IssueInvolvementBean(involvement.getString("id"), involvement.getString("label")));
            }

            final UserIssueRelevanceBean bean = new UserIssueRelevanceBean(
                    URI.create(currentUser.getString("self")),
                    currentUser.getString("key"),
                    currentUser.getString("name"),
                    currentUser.getString("displayName"),
                    currentUser.getBoolean("active"),
                    currentUser.getString("emailAddress"),
                    ImmutableMap.of(), //Values not used in test, and saves writing a converter
                    null,
                    null,
                    involvementList.build(),
                    currentUser.has("highestIssueInvolvementRank") ? Optional.of(currentUser.getInt("highestIssueInvolvementRank")) : Optional.empty(),
                    currentUser.has("latestCommentCreationTime") ? Optional.of(currentUser.getLong("latestCommentCreationTime")).map(Date::new) : Optional.empty()
            );
            list.add(bean);
        }

        return list.build();
    }

    private void createUsers() {
        backdoor.usersAndGroups().addUser("a\\b", "pass", "a\\b", "ab@example.com", false);
        backdoor.usersAndGroups().addUserToGroup("a\\b", "jira-developers");

        backdoor.usersAndGroups().addUser("bloblaw", "pass", "Bob Loblaw", "bob@example.com", false);
        backdoor.usersAndGroups().addUserToGroup("bloblaw", "jira-developers");

        backdoor.usersAndGroups().addUser("c/d", "pass", "c/d", "cd@example.com", false);
        backdoor.usersAndGroups().addUserToGroup("c/d", "jira-developers");

        backdoor.usersAndGroups().addUser("per%25cent", "pass", "per%25cent", "pct@example.com", false);

        backdoor.usersAndGroups().addUser("pl+us", "pass", "pl+us", "pl+us@example.com", false);

        backdoor.usersAndGroups().addUser("sp ace", "pass", "sp ace", "space@example.com", false);

        backdoor.usersAndGroups().addUser("%E6%84%9B", "pass", "%E6%84%9B %E6%88%B7", "love@example.com", false);

        backdoor.usersAndGroups().addUser("fredx", "pass", "Fred Obsolete", "fredx@example.com", false);
        backdoor.usersAndGroups().removeUserFromGroup("fredx", "jira-users");
        backdoor.userManager().setActive("fredx", false);
    }
}
