package com.atlassian.jira.functest.framework;

import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;

import static org.junit.Assert.assertTrue;

/**
 * @since v7.2
 */
public class AssertRedirect {

    private final WebTester tester;

    @Inject
    public AssertRedirect(final WebTester tester) {
        this.tester = tester;
    }

    public void assertRedirectAndFollow(final String url, final String redirectRegex) {
        try {
            tester.getTestContext().getWebClient().getClientProperties().setAutoRedirect(false);
            tester.gotoPage(url);
            final String redirectTo = tester.getDialog().getResponse().getHeaderField("Location");
            // check redirect url ends wtih the right thing
            final boolean redirectingToCreateIssueStep2 = redirectTo.matches(redirectRegex);
            assertTrue("expected redirect to create issue, location header is: " + redirectTo, redirectingToCreateIssueStep2);
            tester.gotoPage(redirectTo);
        } finally {
            tester.getTestContext().getWebClient().getClientProperties().setAutoRedirect(true);
        }
    }
}
