package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Responsible for holding tests which verify that the User Administration actions are not susceptible to XSRF attacks.
 *
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY, Category.SCHEMES, Category.ISSUE_TYPES, Category.SCREENS})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestEditCustomFieldDescription.xml")
public class TestXsrfAdminIssueTypeScreenSchemes extends BaseJiraFuncTest {
    private static final String ISSUE_TYPE_SCREEN_SCHEME_NAME = "a frivolous name";

    @Inject
    private Form form;

    @Test
    public void testIssueTypeScreenSchemeOperations() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck(
                        "Add Issue Type Screen Scheme",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                navigate();
                                tester.clickLink("add-issue-type-screen-scheme");
                                tester.setFormElement("schemeName", ISSUE_TYPE_SCREEN_SCHEME_NAME);
                            }
                        },
                        new XsrfCheck.FormSubmission("Add")),
                new XsrfCheck(
                        "Edit Issue Type Screen Scheme",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                navigate();
                                tester.clickLink("edit_issuetypescreenscheme_10000");
                            }
                        },
                        new XsrfCheck.FormSubmission("Update")),
                new XsrfCheck(
                        "Copy Issue Type Screen Scheme",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                navigate();
                                tester.clickLink("copy_issuetypescreenscheme_10000");
                            }
                        },
                        new XsrfCheck.FormSubmission("Copy")),
                new XsrfCheck(
                        "Delete Issue Type Screen Scheme",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                navigate();
                                tester.clickLink("delete_issuetypescreenscheme_10000");
                            }
                        },
                        new XsrfCheck.FormSubmission("Delete"))

        ).run(tester, navigation, form);
    }

    @Test
    public void testIssueTypeScreenSchemeConfigOperations() throws Exception {
        addScreen();

        new XsrfTestSuite(
                new XsrfCheck(
                        "Configure Issue Type Screen Scheme Edit Default",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                gotoConfigure();
                                tester.clickLink("edit_issuetypescreenschemeentity_default");
                            }
                        },
                        new XsrfCheck.FormSubmission("Update")),
                new XsrfCheck(
                        "Configure Issue Type Screen Scheme Add",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                gotoConfigure();
                                tester.clickLink("add-issue-type-screen-scheme-configuration-association");
                            }
                        },
                        new XsrfCheck.FormSubmission("Add")),
                new XsrfCheck(
                        "Configure Issue Type Screen Scheme Edit",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                gotoConfigure();
                                tester.clickLink("edit_issuetypescreenschemeentity_Bug");
                            }
                        },
                        new XsrfCheck.FormSubmission("Update")),
                new XsrfCheck(
                        "Configure Issue Type Screen Scheme Delete",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                gotoConfigure();
                            }
                        },
                        new XsrfCheck.LinkWithIdSubmission("delete_issuetypescreenschemeentity_Bug"))

        ).run(tester, navigation, form);
    }

    private void addScreen() {
        navigate();
        tester.clickLink("add-issue-type-screen-scheme");
        tester.setFormElement("schemeName", ISSUE_TYPE_SCREEN_SCHEME_NAME);
        tester.clickButton("add-issue-type-screen-scheme-form-submit");
    }

    private void gotoConfigure() {
        navigate();
        tester.clickLink("configure_issuetypescreenscheme_10000");
    }

    private void navigate() {
        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_TYPE_SCREEN_SCHEME);
    }
}
