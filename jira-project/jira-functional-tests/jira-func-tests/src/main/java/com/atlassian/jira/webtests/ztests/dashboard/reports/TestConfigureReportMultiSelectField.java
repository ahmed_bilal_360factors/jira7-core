package com.atlassian.jira.webtests.ztests.dashboard.reports;

import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.html.HTMLSelectElement;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.REPORTS})
@LoginAs(user = ADMIN_USERNAME)
public class TestConfigureReportMultiSelectField extends AbstractConfigureReportFieldTestCase {

    @Test
    public void fieldExists() {
        tester.assertFormElementPresent("aMultiSelect");
    }

    @Test
    public void defaultValueIsPresent() {
        HTMLSelectElement element = (HTMLSelectElement) tester.getDialog().getElement("aMultiSelect_select");
        int selectedIndex = element.getSelectedIndex();
        Node selected = element.getOptions().item(selectedIndex);

        assertThat(selected.getTextContent(), is("Green"));
    }

    @Test
    public void fieldLabelRendered() {
        textAssertions.assertTextPresent("A multiselect");
    }

    @Test
    public void fieldDescriptionRendered() {
        textAssertions.assertTextPresent("This is a multiselect field");
    }

    @Test
    public void validationErrorsRendered() {
        tester.gotoPage("/secure/ConfigureReport.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.dev.func-test-plugin:fieldtest-report&aString=fail");

        textAssertions.assertTextPresent(new XPathLocator(fieldContainer(), "*[@class='errMsg']"), "This multi select field has an error");
    }

    @Test
    public void allItemsAreInList() {
        assertThat(tester.getDialog().getOptionValuesFromSelectList("aMultiSelect"),
                arrayContaining("multivalue-red", "multivalue-green", "multivalue-blue", "multivalue-yellow", "multivalue-orange"));
    }

    private Element fieldContainer() {
        return (Element) new XPathLocator(tester, "//fieldset[@class = 'group'][select[@id = 'aMultiSelect_select']]").getNode();
    }

    @Test
    public void valueIsSubmittedToReportBean() {
        tester.setFormElement("aMultiSelect", "multivalue-orange");
        tester.submit();
        textAssertions.assertTextPresent(new XPathLocator(tester, "//tr[th = 'aMultiSelect']/td"), "multivalue-orange");
    }
}
