package com.atlassian.jira.webtests.ztests.projectconfig.framework;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * @since v5.2
 */
public class SimpleWorkflow {
    @JsonProperty
    private String name;

    @JsonProperty
    private String displayName;

    @JsonProperty
    private String description;

    @JsonProperty("default")
    private boolean defaultWorkflow;

    @JsonProperty
    private boolean system;

    @JsonProperty
    private List<String> issueTypes;

    @JsonProperty
    private boolean jiraDefault;

    public SimpleWorkflow() {
    }

    public SimpleWorkflow(String name, String description, boolean defaultWorkflow, boolean system, List<String> issueTypes) {
        this(name, description, defaultWorkflow, system, issueTypes, false);
    }

    public SimpleWorkflow(String name, String description, boolean defaultWorkflow, boolean system, List<String> issueTypes, boolean jiraDefault) {
        this.name = name;
        if (jiraDefault) {
            this.displayName = "JIRA Workflow (jira)";
        } else {
            this.displayName = name;
        }
        this.description = description;
        this.defaultWorkflow = defaultWorkflow;
        this.system = system;
        this.issueTypes = issueTypes;
        this.jiraDefault = jiraDefault;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SimpleWorkflow that = (SimpleWorkflow) o;

        if (defaultWorkflow != that.defaultWorkflow) {
            return false;
        }
        if (jiraDefault != that.jiraDefault) {
            return false;
        }
        if (system != that.system) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (displayName != null ? !displayName.equals(that.displayName) : that.displayName != null) {
            return false;
        }
        if (issueTypes != null ? !issueTypes.equals(that.issueTypes) : that.issueTypes != null) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (displayName != null ? displayName.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (defaultWorkflow ? 1 : 0);
        result = 31 * result + (system ? 1 : 0);
        result = 31 * result + (issueTypes != null ? issueTypes.hashCode() : 0);
        result = 31 * result + (jiraDefault ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isDefaultWorkflow() {
        return defaultWorkflow;
    }

    public boolean isSystem() {
        return system;
    }

    public List<String> getIssueTypes() {
        return issueTypes;
    }

    public boolean isJiraDefault() {
        return jiraDefault;
    }

    public String getDisplayName() {
        return displayName;
    }
}
