package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertTrue;

/**
 * @since v3.13.5
 */
@WebTest({Category.FUNC_TEST, Category.HTTP})
@LoginAs(user = ADMIN_USERNAME)
public class TestResourceHeaders extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Test
    public void testResourcesNotPrivate() {
        administration.restoreBlankInstance();
        tester.gotoPage("/s/" + administration.getBuildNumber() + "/1/_/images/icons/favicon.png");
        String cache = tester.getDialog().getResponse().getHeaderField("Cache-Control");
        boolean condition = (cache == null || cache.indexOf("private") == -1);
        assertTrue(condition);
    }
}
