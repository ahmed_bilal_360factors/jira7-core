package com.atlassian.jira.functest.framework.backdoor.application;

import com.atlassian.jira.functest.framework.backdoor.SingleProductLicenseDetailsViewTO;
import com.sun.jersey.api.client.GenericType;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.joda.time.DateTime;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = PlatformApplicationBean.class, name = "platform"),
        @JsonSubTypes.Type(value = PluginApplicationBean.class, name = "plugin")
})
public class ApplicationBean {
    public static final GenericType<List<ApplicationBean>> LIST_TYPE = new GenericType<List<ApplicationBean>>() {
    };

    @JsonProperty
    private String key;

    @JsonProperty
    private String name;

    @JsonProperty
    private String description;

    @JsonProperty
    private String configurationURI;

    @JsonProperty
    private String postInstallURI;

    @JsonProperty
    private String postUpdateURI;

    @JsonProperty
    private String productHelpServerSpaceURI;

    @JsonProperty
    private String productHelpCloudSpaceURI;

    @JsonProperty
    private SingleProductLicenseDetailsViewTO license;

    @JsonProperty
    private int currentUserCount;

    @JsonProperty
    private int maxUserCount;

    @JsonProperty
    private String accessURI;

    @JsonProperty
    private String defaultGroup;

    @JsonProperty
    private String version;

    @JsonProperty
    private DateTime buildDate;

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getConfigurationURI() {
        return configurationURI;
    }

    public String getPostInstallURI() {
        return postInstallURI;
    }

    public String getPostUpdateURI() {
        return postUpdateURI;
    }

    public SingleProductLicenseDetailsViewTO getLicense() {
        return license;
    }

    public int getCurrentUserCount() {
        return currentUserCount;
    }

    public int getMaxUserCount() {
        return maxUserCount;
    }

    public String getAccessURI() {
        return accessURI;
    }

    public String getDefaultGroup() {
        return defaultGroup;
    }

    public String getVersion() {
        return version;
    }

    public DateTime getBuildDate() {
        return buildDate;
    }

    public String getProductHelpServerSpaceURI() {
        return productHelpServerSpaceURI;
    }

    public String getProductHelpCloudSpaceURI() {
        return productHelpCloudSpaceURI;
    }
}
