package com.atlassian.jira.webtests.ztests.navigator.jql;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.assertions.JqlAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.assertions.IssueNavigatorAssertions.FilterFormParam.createFilterFormParam;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.JQL})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestUserNameSearching.xml")
public class TestUserNameSearching extends BaseJiraFuncTest {

    @Inject
    private JqlAssertions jqlAssertions;

    @Inject
    private IssueTableAssertions issueTableAssertions;

    @Before
    public void setUp() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    public void testSystemFieldSearchByUserName() throws Exception {
        _testSearchByUserNameFits("assignee", "assignee");
        _testSearchByFullNameDoesntFit("assignee");
        _testSearchByEmailDoesntFit("assignee");
        _testSearchByEmailReturnsResultsByEmail("assignee");
    }

    @Test
    public void testUserPickerSearchByUserName() throws Exception {
        _testSearchByUserNameFits("userpicker", "customfield_10000");
        _testSearchByFullNameDoesntFit("userpicker");
        _testSearchByEmailDoesntFit("userpicker");
        _testSearchByEmailReturnsResultsByEmail("userpicker");
    }

    /**
     * JRADEV-21991.
     * When a user gets deleted externally we want to still be able to search for that name.
     *
     * @throws Exception
     */
    @Test
    public void testSearchingUsingDeletedUser() throws Exception {
        issueTableAssertions.assertSearchWithResults("assignee = \"deleteduser\"", "HSP-2");
    }

    @Test
    public void testSearchingUsingDeletedUserDoesntFit() throws Exception {
        String jql = "assignee = \"deleteduser\"";
        jqlAssertions.assertTooComplex(jql);
    }

    @Test
    public void testUserGroupPickerSearchByUserName() throws Exception {
        _testSearchByUserNameFits("usergrouppicker", "customfield_10001");
        _testSearchByFullNameDoesntFit("usergrouppicker");
        _testSearchByEmailDoesntFit("usergrouppicker");
        _testSearchByEmailReturnsResultsByEmail("usergrouppicker");
    }

    private void _testSearchByUserNameFits(String field, String formName) throws Exception {
        String jql = field + " = admin";
        jqlAssertions.assertFitsFilterForm(jql, createFilterFormParam(formName, ADMIN_USERNAME));
        issueTableAssertions.assertSearchWithResults(jql, "HSP-1");
    }

    private void _testSearchByFullNameDoesntFit(String field) throws Exception {
        String jql = field + " = " + ADMIN_FULLNAME;
        jqlAssertions.assertTooComplex(jql);
        issueTableAssertions.assertSearchWithResults(jql, "HSP-1");
    }

    private void _testSearchByEmailDoesntFit(String field) throws Exception {
        String jql = field + " = 'admin@example.com'";
        jqlAssertions.assertTooComplex(jql);
        issueTableAssertions.assertSearchWithResults(jql, "HSP-1");
    }

    private void _testSearchByEmailReturnsResultsByEmail(String field) throws Exception {
        issueTableAssertions.assertSearchWithResults(field + " = 'email@example.com'", "HSP-3");
    }
}
