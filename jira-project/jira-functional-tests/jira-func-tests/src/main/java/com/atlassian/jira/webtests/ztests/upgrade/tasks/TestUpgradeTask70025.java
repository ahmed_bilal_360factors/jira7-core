package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.UPGRADE_TASKS;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@WebTest({FUNC_TEST, UPGRADE_TASKS})
public class TestUpgradeTask70025 extends BaseJiraFuncTest {

    private static final String USER_1 = "user1";
    private static final String USER_2 = "user2";
    private static final String USER_3 = "user3";

    @Test
    public void deleteMinusOneUserLocaleProperty() {
        backdoor.restoreDataFromResource("TestUpgradeTask70025.xml");
        // user1's language is fr_FR instead of -1 , so the upgrade task must respect that
        assertUserLanguage(USER_1, "fr_FR");
        // user2's language is -1 , so the upgrade task must remove it
        assertUserLanguage(USER_2, "");
        // user3's language is -1 , so the upgrade task must remove it
        assertUserLanguage(USER_3, "");
    }

    private void assertUserLanguage(String username, String language) {
        String userLanguage = backdoor.userProfile().getUserLanguage(username);
        assertThat("User's language should be correct as chosen", userLanguage, is(language));
    }
}