package com.atlassian.jira.webtests.ztests.plugin;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Test verifying that the <tt>PrepareAction</tt> interface is supported by JIRA action plugin module.
 *
 * @since v4.3
 */
@WebTest({Category.FUNC_TEST, Category.PLUGINS, Category.REFERENCE_PLUGIN})
@LoginAs(user = ADMIN_USERNAME)
public class TestWebWork1PrepareActionInterface extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testPrepareActionCalled() throws Exception {
        administration.restoreData("TestWebWork1PrepareActionInterface.xml");
        tester.gotoPage("/PreparedReferenceAction.jspa");
        textAssertions.assertTextPresent(locator.id("prepared-message"), "I am ready for anything");
    }

}
