package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertThat;

/**
 * Tests that issue security schemes with roles restrict unauthorized users from seeing issues
 */
@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.ROLES, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueSecurityWithRoles extends BaseJiraFuncTest {
    private static final String HSP_ISSUE_NO_SECURITY = "HSP-1";
    private static final String HSP_ISSUE_DEV_ROLE_SECURITY = "HSP-2";
    private static final String HSP_ISSUE_ADMIN_ROLE_SECURITY = "HSP-3";
    private static final String MKY_ISSUE_NO_SECURITY = "MKY-1";
    private static final String MKY_ISSUE_DEV_ROLE_SECURITY = "MKY-2";
    private static final String ELE_ISSUE_NO_SECURITY = "ELE-1";
    private static final String ELE_ISSUE_DEV_ROLE_SECURITY = "ELE-2";

    @Inject
    private Administration administration;

    @Before
    public void setUp() {
        administration.restoreData("TestIssueSecurityWithRoles.xml");
    }

    /**
     * Tests that issue security schemes with roles restrict unauthorized users from seeing issues
     * in the issue navigator
     */
    @Test
    public void testIssueSecurityWithRolesIssueNavigator() {
        // User with authorized role
        navigation.issueNavigator().displayAllIssues();
        tester.assertLinkPresentWithText(HSP_ISSUE_NO_SECURITY);
        tester.assertLinkPresentWithText(HSP_ISSUE_DEV_ROLE_SECURITY);
        navigation.issue().gotoIssue(HSP_ISSUE_DEV_ROLE_SECURITY);
        tester.assertTextNotPresent("Permission violation");

        //User without authorized role
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issueNavigator().displayAllIssues();
        tester.assertLinkPresentWithText(HSP_ISSUE_NO_SECURITY);
        tester.assertLinkNotPresentWithText(HSP_ISSUE_DEV_ROLE_SECURITY);
        navigation.issue().gotoIssue(HSP_ISSUE_DEV_ROLE_SECURITY);
        assertions.getViewIssueAssertions().assertIssueNotFound();
    }

    /**
     * Tests that issue security schemes with roles restrict unauthorized users from seeing issues
     * across multiple projects with different but equivalent issue security schemes
     */
    @Test
    public void testIssueSecurityWithRolesMultiProject() {
        // User with authorized role
        navigation.issueNavigator().displayAllIssues();
        tester.assertLinkPresentWithText(HSP_ISSUE_NO_SECURITY);
        tester.assertLinkPresentWithText(HSP_ISSUE_DEV_ROLE_SECURITY);
        tester.assertLinkPresentWithText(MKY_ISSUE_NO_SECURITY);
        tester.assertLinkPresentWithText(MKY_ISSUE_DEV_ROLE_SECURITY);
        tester.assertLinkPresentWithText(ELE_ISSUE_NO_SECURITY);
        tester.assertLinkPresentWithText(ELE_ISSUE_DEV_ROLE_SECURITY);
        navigation.issue().gotoIssue(ELE_ISSUE_DEV_ROLE_SECURITY);
        assertions.getViewIssueAssertions().assertOnViewIssuePage(ELE_ISSUE_DEV_ROLE_SECURITY);

        //User without authorized role
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issueNavigator().displayAllIssues();
        tester.assertLinkPresentWithText(HSP_ISSUE_NO_SECURITY);
        tester.assertLinkNotPresentWithText(HSP_ISSUE_DEV_ROLE_SECURITY);
        tester.assertLinkPresentWithText(MKY_ISSUE_NO_SECURITY);
        tester.assertLinkNotPresentWithText(MKY_ISSUE_DEV_ROLE_SECURITY);
        tester.assertLinkPresentWithText(ELE_ISSUE_NO_SECURITY);
        tester.assertLinkNotPresentWithText(ELE_ISSUE_DEV_ROLE_SECURITY);
        navigation.issue().gotoIssue(ELE_ISSUE_DEV_ROLE_SECURITY);
        assertions.getViewIssueAssertions().assertIssueNotFound();
    }

    /**
     * Tests that issue level security field on edit issue shows correct issue levels based on user's role
     * after cache has been reloaded
     */
    @Test
    public void testIssueSecurityWithRolesEditIssueFields() {
        navigation.issueNavigator().displayAllIssues();
        tester.assertLinkPresentWithText(HSP_ISSUE_DEV_ROLE_SECURITY);
        tester.assertLinkPresentWithText(HSP_ISSUE_ADMIN_ROLE_SECURITY);
        navigation.issue().gotoIssue(HSP_ISSUE_ADMIN_ROLE_SECURITY);
        tester.clickLink("edit-issue");
        tester.setWorkingForm("issue-edit");
        assertThat(asList(tester.getDialog().getOptionLabelsFromSelectList("security")),
                Matchers.<String>containsInAnyOrder("None", "My Security Level", "Administrators Security Level"));

        //Remove user from developer role
        backdoor.projectRole().deleteUser(PROJECT_HOMOSAP_KEY, "Developers", ADMIN_USERNAME);
        navigation.issueNavigator().displayAllIssues();
        tester.assertLinkNotPresentWithText(HSP_ISSUE_DEV_ROLE_SECURITY);
        tester.assertLinkPresentWithText(HSP_ISSUE_ADMIN_ROLE_SECURITY);
        navigation.issue().gotoIssue(HSP_ISSUE_ADMIN_ROLE_SECURITY);
        tester.clickLink("edit-issue");
        tester.setWorkingForm("issue-edit");
        assertThat(asList(tester.getDialog().getOptionLabelsFromSelectList("security")),
                Matchers.<String>containsInAnyOrder("None", "Administrators Security Level"));
    }

    /**
     * Tests that issue level security field on edit issue shows correct issue levels based on user's role
     * after cache has been reloaded when going directly to EditIssue
     */
    @Test
    public void testIssueSecurityWithRolesEditIssueFieldsDirectly() {
        tester.gotoPage("/secure/EditIssue!default.jspa?id=10020");
        tester.setWorkingForm("issue-edit");
        assertThat(asList(tester.getDialog().getOptionLabelsFromSelectList("security")),
                Matchers.<String>containsInAnyOrder("None", "My Security Level", "Administrators Security Level"));
        //Remove user from developer role
        backdoor.projectRole().deleteUser(PROJECT_HOMOSAP_KEY, "Developers", ADMIN_USERNAME);
        tester.gotoPage("/secure/EditIssue!default.jspa?id=10020");
        tester.setWorkingForm("issue-edit");
        assertThat(asList(tester.getDialog().getOptionLabelsFromSelectList("security")),
                Matchers.<String>containsInAnyOrder("None", "Administrators Security Level"));
    }
}
