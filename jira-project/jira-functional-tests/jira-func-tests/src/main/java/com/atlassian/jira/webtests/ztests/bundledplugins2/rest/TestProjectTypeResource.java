package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.beans.ProjectTypeBean;
import com.atlassian.jira.testkit.client.restclient.ProjectTypeClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.LicenseKeys;
import org.apache.commons.httpclient.HttpStatus;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;

/**
 * Functional tests for the project types REST entry point
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
public class TestProjectTypeResource extends BaseJiraRestTest {
    private ProjectTypeClient projectTypeClient;
    public static final String BUSINESS = "business";

    @Before
    public void setUp() {
        backdoor.restoreBlankInstance(LicenseKeys.COMMERCIAL);
        projectTypeClient = new ProjectTypeClient(environmentData);
        projectTypeClient.anonymous();
    }

    @Test
    public void testGetAllProjectTypesReturnsAllTheExistingProjectTypes() throws Exception {
        Response<List<ProjectTypeBean>> response = projectTypeClient.getAllProjectTypes();

        assertThat(response.statusCode, is(HttpStatus.SC_OK));
        List<ProjectTypeBean> projectTypes = response.body;
        assertThat(projectTypes.size(), is(3));
        assertProjectTypeIsCorrect(projectTypes.get(0), "first-type", "first.type.desc", "first-type-icon", "#111111");
        assertProjectTypeIsCorrect(projectTypes.get(1), "second-type", "second.type.desc", "second-type-icon", "#222222");
        assertBusinessProjectTypeIsCorrect(projectTypes.get(2));
    }

    @Test
    public void testGetProjectTypeByKeyReturnsTheExpectedTypeWhenItExists() throws Exception {
        Response<ProjectTypeBean> response = projectTypeClient.getByKey("business");

        assertThat(response.statusCode, is(HttpStatus.SC_OK));
        assertBusinessProjectTypeIsCorrect(response.body);
    }

    @Test
    public void testGetProjectTypeByKeyReturns404WhenItDoesNotExist() throws Exception {
        Response<ProjectTypeBean> response = projectTypeClient.getByKey("non-existent");

        assertThat(response.statusCode, is(HttpStatus.SC_NOT_FOUND));
    }

    @Test
    public void testGetAccessibleProjectTypeReturnsTheTypeIfTheUserIsLoggedInAndHasAccessToIt() throws Exception {
        Response<ProjectTypeBean> response = projectTypeClient.loginAs("admin").getAccessibleProjectTypeByKey(BUSINESS);

        assertThat(response.statusCode, is(HttpStatus.SC_OK));
        assertBusinessProjectTypeIsCorrect(response.body);
    }

    @Test
    public void testGetAccessibleProjectTypeReturns404IfTheUserIsLoggedInAndDoesNotHaveAccessToIt() throws Exception {
        Response<ProjectTypeBean> response = projectTypeClient.loginAs("admin").getAccessibleProjectTypeByKey("non-existent");

        assertThat(response.statusCode, is(HttpStatus.SC_NOT_FOUND));
    }

    @Test
    public void testGetAccessibleProjectTypeReturns401IfTheUserIsNotLoggedIn() throws Exception {
        Response<ProjectTypeBean> response = projectTypeClient.getAccessibleProjectTypeByKey(BUSINESS);

        assertThat(response.statusCode, is(HttpStatus.SC_UNAUTHORIZED));
    }

    private void assertProjectTypeIsCorrect(ProjectTypeBean projectType, String expectedKey, String expectedDescription, String expectedIcon, String expectedColor) {
        assertThat(projectType.getKey(), is(expectedKey));
        assertThat(projectType.getDescriptionI18nKey(), is(expectedDescription));
        assertThat(projectType.getIcon(), is(expectedIcon));
        assertThat(projectType.getColor(), is(expectedColor));
    }

    private void assertBusinessProjectTypeIsCorrect(ProjectTypeBean projectType) {
        assertThat(projectType.getKey(), Is.is("business"));
        assertThat(projectType.getDescriptionI18nKey(), is("jira.project.type.business.description"));
        assertThat(projectType.getColor(), is("#1D8832"));
        assertThat(projectType.getIcon().length(), is(greaterThan(0)));
    }
}
