package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.LicenseKeys;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Test to see that importing an xml file into JIRA and then exporting from it results in an XML file
 * <p/>
 * Uses the following xml files:
 * TestImportExport.xml
 * TestImportExport2.xml
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.IMPORT_EXPORT})
@LoginAs(user = ADMIN_USERNAME)
public class TestImportExport extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testXmlImportFromNonImportDirectory() throws Exception {
        File data = new File(getEnvironmentData().getXMLDataLocation(), "EmptyJira.xml");

        // write new data to temp file
        File newData = File.createTempFile("testXmlImportFromNonImportDirectory", ".xml"); //This will be created in the /tmp directory
        try {
            FileUtils.copyFile(data, newData);

            tester.gotoPage("secure/admin/XmlRestore!default.jspa");
            tester.setWorkingForm("restore-xml-data-backup");
            tester.setFormElement("filename", newData.getAbsolutePath());
            tester.submit();

            tester.assertTextPresent("Could not find file at this location");
        } finally {
            newData.delete();
        }
    }

    @Test
    public void testXmlImportWithInvalidIndexDirectory() throws Exception {
        //By creating a file for the index path, we'll force the failure of the index path directory creation
        File indexPath = File.createTempFile("testXmlImportWithInvalidIndexDirectory", null);
        indexPath.createNewFile();
        indexPath.deleteOnExit();

        final String absolutePath = indexPath.getAbsolutePath();
        importExpectingFailure("TestSetupInvalidIndexPath.xml", ImmutableMap.of("@@INDEX_PATH@@", absolutePath));
        tester.assertTextPresent("Cannot write to index directory. Check that the application server and JIRA have permissions to write to: " + absolutePath);
    }

    @Test
    public void testXmlImportWithInvalidAttachmentsDirectory() throws Exception {
        //By creating a file for the index path, we'll force the failure of the index path directory creation
        File attachmentPath = File.createTempFile("testXmlImportWithInvalidAttachmentsDirectory", null);
        attachmentPath.createNewFile();
        attachmentPath.deleteOnExit();

        final String absolutePath = attachmentPath.getAbsolutePath();
        importExpectingFailure("TestSetupInvalidAttachmentPath.xml", ImmutableMap.of("@@ATTACHMENT_PATH@@", absolutePath));
        tester.assertTextPresent("Cannot write to attachment directory. Check that the application server and JIRA have permissions to write to: " + absolutePath);
    }

    private void importExpectingFailure(String xmlFileName, Map<String, String> replacements) throws IOException {
        try {
            administration.restoreDataWithReplacedTokens(xmlFileName, replacements);
        } catch (AssertionError e) {
            //We're expecting an AssertionError here to inform us that the import was not successful
        }
    }

    @Test
    public void testXmlImportFromFuture_Fail() {
        File file = new File(getEnvironmentData().getXMLDataLocation().getAbsolutePath() + "/" + "TestXmlImportFromFuture_Fail.xml");
        // This import file claims it comes from the distant future:
        //        <OSPropertyEntry id="10023" entityName="jira.properties" entityId="1" propertyKey="jira.version.patched" type="5"/>
        //        <OSPropertyString id="10023" value="999999999"/>
        // But it includes an Upgrade Task that needs an explicit downgrade:
        //        <UpgradeHistory id="10061" ... targetbuild="99001" status="complete"/>

        copyToJiraImportDirectory(file);
        tester.gotoPage("secure/admin/XmlRestore!default.jspa");
        tester.setWorkingForm("restore-xml-data-backup");
        tester.setFormElement("filename", file.getName());
        tester.setFormElement("license", LicenseKeys.COMMERCIAL.getLicenseString());
        tester.submit();
        administration.waitForRestore();
        tester.assertTextNotPresent("Your import has been successful");
        tester.assertTextPresent("The file you are trying to import is from JIRA 999.9.9, which is newer than your current version of JIRA and will not work. The import file claims it can be successfully imported into JIRA 99.8.7 or later.");
        tester.assertTextPresent("If JIRA 99.8.7 is not yet available for download, please contact support.");
    }

    @Test
    public void testXmlImportFromFuture_DowngradeNoOp() {
        File file = new File(getEnvironmentData().getXMLDataLocation().getAbsolutePath() + "/" + "TestXmlImportFromFuture.xml");
        // This import file claims it comes from the distant future:
        //        <OSPropertyEntry id="10023" entityName="jira.properties" entityId="1" propertyKey="jira.version.patched" type="5"/>
        //        <OSPropertyString id="10023" value="999999999"/>
        // It includes an Upgrade Tasks that need to be "downgraded", but all of these are a no-op:
        //        <UpgradeHistory id="10061" ... targetbuild="99001" status="pending"/>
        //        <UpgradeHistory id="10062" ... targetbuild="99002" status="complete" downgradetaskrequired="N"/>
        //        <UpgradeHistory id="10063" ... targetbuild="99003" status="pending" downgradetaskrequired="Y"/>

        copyToJiraImportDirectory(file);
        tester.gotoPage("secure/admin/XmlRestore!default.jspa");
        tester.setWorkingForm("restore-xml-data-backup");
        tester.setFormElement("filename", file.getName());
        tester.setFormElement("license", LicenseKeys.COMMERCIAL.getLicenseString());
        tester.submit();
        administration.waitForRestore();
        tester.assertTextPresent("You are attempting to import data from JIRA 999.9.9 which is a more recent version than this version.");
        tester.assertTextPresent("Some features that you may have used before will not be available in this version of JIRA.");
        tester.assertTextPresent("acknowledge this error and proceed anyway.");
    }

    @Test
    public void testXmlImportWithAV1LicenseInIt() throws Exception {
        File file = new File(getEnvironmentData().getXMLDataLocation().getAbsolutePath() + "/" + "oldlicense.xml");
        copyToJiraImportDirectory(file);
        tester.gotoPage("secure/admin/XmlRestore!default.jspa");
        tester.setWorkingForm("restore-xml-data-backup");
        tester.setFormElement("filename", file.getName());
        tester.submit();
        administration.waitForRestore();
        textAssertions.assertTextPresent(new WebPageLocator(tester), "This license is invalid.");
    }

    private void copyToJiraImportDirectory(File file) {
        final File jiraImportDirectory = new File(administration.getJiraHomeDirectory(), "import");
        try {
            FileUtils.copyFileToDirectory(file, jiraImportDirectory);
        } catch (IOException e) {
            throw new RuntimeException("Could not copy file " + file.getAbsolutePath() +
                    " to the import directory in jira home " + jiraImportDirectory, e);
        }
    }
}
