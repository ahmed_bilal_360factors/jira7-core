package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.IssueTypeControl;
import com.atlassian.jira.testkit.client.restclient.EntityProperty;
import com.atlassian.jira.testkit.client.restclient.EntityPropertyClient;
import com.atlassian.jira.testkit.client.restclient.EntityPropertyKeys;
import com.atlassian.jira.util.json.JSONObject;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.concurrent.Callable;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.webtests.ztests.bundledplugins2.rest.util.PropertyAssertions.assertUniformInterfaceException;
import static com.atlassian.jira.webtests.ztests.bundledplugins2.rest.util.PropertyAssertions.propertyKey;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.ENTITY_PROPERTIES, Category.REST})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueTypePropertyResource extends BaseJiraFuncTest {
    public static final String PROPERTY_KEY = "issuetype.cool";
    public static final String PROJECT_KEY = "HSP";
    private EntityPropertyClient client;

    @Before
    public void setUpTest() {
        this.client = new EntityPropertyClient(environmentData, "issuetype");
        backdoor.restoreBlankInstance();
    }

    @Test
    public void testCreatingNewIssueTypeProperty() throws Exception {
        final IssueTypeControl.IssueType issueType = backdoor.issueType().createIssueType("my-type");

        assertHasNoProperties(issueType);

        JSONObject expected = new JSONObject(ImmutableMap.<String, Object>of("visible", 0));
        client.put(issueType.getId(), PROPERTY_KEY, expected);

        EntityProperty result = client.get(issueType.getId(), PROPERTY_KEY);

        assertThat(result.key, is(PROPERTY_KEY));
        assertThat(new JSONObject(result.value), is(expected));

        Iterable<EntityPropertyKeys.EntityPropertyKey> keys = client.getKeys(issueType.getId()).keys;

        Matcher<Iterable<? super EntityPropertyKeys.EntityPropertyKey>> matcher = Matchers.hasItem(propertyKey(PROPERTY_KEY));
        assertThat(keys, matcher);

        assertUniformInterfaceException(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                client.anonymous().getKeys(issueType.getId());
                return null;
            }
        }, Response.Status.NOT_FOUND);
        assertUniformInterfaceException(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                client.anonymous().get(issueType.getId(), PROPERTY_KEY);
                return null;
            }
        }, Response.Status.NOT_FOUND);
    }

    @Test
    public void testDeleteProperty() throws Exception {
        final IssueTypeControl.IssueType issueType = backdoor.issueType().createIssueType("my-type");

        assertHasNoProperties(issueType);

        JSONObject expected = new JSONObject(ImmutableMap.<String, Object>of("visible", 0));
        client.put(issueType.getId(), PROPERTY_KEY, expected);

        EntityProperty result = client.get(issueType.getId(), PROPERTY_KEY);

        assertThat(result.key, is(PROPERTY_KEY));
        assertThat(new JSONObject(result.value), is(expected));

        client.delete(issueType.getId(), PROPERTY_KEY);

        assertHasNoProperties(issueType);

        assertUniformInterfaceException(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                client.get(issueType.getId(), PROPERTY_KEY);
                return null;
            }
        }, Response.Status.NOT_FOUND);
    }

    @Test
    public void testPropertyRemovedWhenIssueTypeRemoved() throws Exception {
        final IssueTypeControl.IssueType issueType = backdoor.issueType().createIssueType("my-type");

        assertHasNoProperties(issueType);

        JSONObject expected = new JSONObject(ImmutableMap.<String, Object>of("visible", 0));
        client.put(issueType.getId(), PROPERTY_KEY, expected);

        EntityProperty result = client.get(issueType.getId(), PROPERTY_KEY);

        assertThat(result.key, is(PROPERTY_KEY));
        assertThat(new JSONObject(result.value), is(expected));

        backdoor.issueType().deleteIssueType(Long.parseLong(issueType.getId()));

        assertUniformInterfaceException(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                client.get(issueType.getId(), PROPERTY_KEY);
                return null;
            }
        }, Response.Status.NOT_FOUND);
    }

    @Test
    public void testSetResultsInForbiddenForNotAdminWithPermissionsToViewIssueType() throws Exception {
        final IssueTypeControl.IssueType issueType = backdoor.issueType().createIssueType("my-type");
        final JSONObject jsonObject = new JSONObject(ImmutableMap.<String, Object>of("visible", 0));

        client.put(issueType.getId(), PROPERTY_KEY, jsonObject);
        client.loginAs("fred").get(issueType.getId(), PROPERTY_KEY);

        assertUniformInterfaceException(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                client.loginAs("fred").put(issueType.getId(), PROPERTY_KEY, jsonObject);
                return null;
            }
        }, Response.Status.FORBIDDEN);
    }

    @Test
    public void testDeleteResultsInForbiddenForNotAdminWithPermissionsToViewIssueType() throws Exception {
        final IssueTypeControl.IssueType issueType = backdoor.issueType().createIssueType("my-type");
        final JSONObject jsonObject = new JSONObject(ImmutableMap.<String, Object>of("visible", 0));

        client.put(issueType.getId(), PROPERTY_KEY, jsonObject);
        client.loginAs("fred").get(issueType.getId(), PROPERTY_KEY);

        assertUniformInterfaceException(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                client.loginAs("fred").delete(issueType.getId(), PROPERTY_KEY);
                return null;
            }
        }, Response.Status.FORBIDDEN);
    }

    @Test
    public void testSetResultsInNotFoundForNotAdminWithoutPermissionsToViewIssueType() throws Exception {
        final IssueTypeControl.IssueType issueType = backdoor.issueType().createIssueType("my-type");
        final JSONObject jsonObject = new JSONObject(ImmutableMap.<String, Object>of("visible", 0));

        client.put(issueType.getId(), PROPERTY_KEY, jsonObject);

        assertUniformInterfaceException(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                client.anonymous().get(issueType.getId(), PROPERTY_KEY);
                return null;
            }
        }, Response.Status.NOT_FOUND);

        assertUniformInterfaceException(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                client.anonymous().put(issueType.getId(), PROPERTY_KEY, jsonObject);
                return null;
            }
        }, Response.Status.NOT_FOUND);
    }

    @Test
    public void testDeleteResultsInNotFoundForNotAdminWithoutPermissionsToViewIssueType() throws Exception {
        final IssueTypeControl.IssueType issueType = backdoor.issueType().createIssueType("my-type");
        final JSONObject jsonObject = new JSONObject(ImmutableMap.<String, Object>of("visible", 0));

        client.put(issueType.getId(), PROPERTY_KEY, jsonObject);

        assertUniformInterfaceException(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                client.anonymous().get(issueType.getId(), PROPERTY_KEY);
                return null;
            }
        }, Response.Status.NOT_FOUND);

        assertUniformInterfaceException(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                client.anonymous().delete(issueType.getId(), PROPERTY_KEY);
                return null;
            }
        }, Response.Status.NOT_FOUND);
    }

    private void assertHasNoProperties(final IssueTypeControl.IssueType issueType) {
        assertThat(client.getKeys(issueType.getId()).keys, Matchers.<EntityPropertyKeys.EntityPropertyKey>empty());
    }
}
