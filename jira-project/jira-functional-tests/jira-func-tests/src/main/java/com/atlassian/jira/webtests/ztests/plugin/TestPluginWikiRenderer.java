package com.atlassian.jira.webtests.ztests.plugin;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Test that plugins that use wiki rendering are called before TokenRendererComponent
 *
 * @since v5.0
 */
@WebTest({Category.FUNC_TEST, Category.PLUGINS})
@LoginAs(user = ADMIN_USERNAME)
public class TestPluginWikiRenderer extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testPluginWikiRendererThatUsesTokenRendererBlock() throws Exception {
        administration.restoreData("TestPluginWikiRenderer.xml");

        navigation.issue().setDescription("HSP-1", "Yo stop");

        textAssertions.assertTextPresent("<h1>Yo stop</h1><h2>Collaborate and listen</h2>");
    }

    @Test
    public void testPluginWikiRendererThatUsesTokenRendererInline() throws Exception {
        administration.restoreData("TestPluginWikiRenderer.xml");

        navigation.issue().setDescription("HSP-1", "Ice is back");

        textAssertions.assertTextPresent("Ice is back with a brand new invention");
    }

}
