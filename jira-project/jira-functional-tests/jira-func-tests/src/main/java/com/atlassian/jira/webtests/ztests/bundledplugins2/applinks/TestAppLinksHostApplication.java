package com.atlassian.jira.webtests.ztests.bundledplugins2.applinks;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FuncTestRestClient;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONObject;
import com.meterware.httpunit.WebResponse;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Functional tests for the JIRA's InternalHostApplication implementation. These tests are mostly smoke tests.
 *
 * @since v4.3
 */
@WebTest({Category.FUNC_TEST, Category.APP_LINKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestAppLinksHostApplication extends BaseJiraFuncTest {
    /**
     * The name of the plugin that must be present.
     */
    private static final String FUNC_TEST_PLUGIN_NAME = "com.atlassian.jira.dev.func-test-plugin";
    private static final String PROJECT_ENTITY_TYPE_NAME = "com.atlassian.applinks.api.application.jira.JiraProjectEntityType";
    private static final Logger log = LoggerFactory.getLogger(TestAppLinksHostApplication.class);

    @Inject
    private FuncTestRestClient restClient;

    @Inject
    private Administration administration;

    @Test
    public void testDocumentationBaseUrlShouldPointToAppLinksDocs() throws Exception {
        if (!isBackdoorAvailable()) {
            return;
        }

        WebResponse docBaseUrl = restClient.GET("rest/func-test/1.0/applinks/getDocumentationBaseUrl");
        assertThat(docBaseUrl.getText(), equalTo("http://confluence.atlassian.com/display/APPLINKS"));
    }

    @Test
    public void testApplicationNameShouldReturnJiraName() throws Exception {
        if (!isBackdoorAvailable()) {
            return;
        }

        WebResponse name = restClient.GET("rest/func-test/1.0/applinks/getName");
        assertThat(name.getText(), equalTo("jWebTest JIRA installation"));
    }

    @Test
    public void testApplicationTypeShouldReturnJira() throws Exception {
        if (!isBackdoorAvailable()) {
            return;
        }

        WebResponse name = restClient.GET("rest/func-test/1.0/applinks/getType");
        JSONObject type = new JSONObject(name.getText());
        assertThat(type.getString("i18nKey"), equalTo("applinks.jira"));
    }

    @Test
    public void testLocalEntitiesShouldReturnAllProjects() throws Exception {
        if (!isBackdoorAvailable()) {
            return;
        }

        WebResponse response = restClient.GET("rest/func-test/1.0/applinks/getLocalEntities");
        JSONArray entities = new JSONObject(response.getText()).getJSONArray("entities");
        assertThat(entities.length(), equalTo(2));

        List<String> projectKeys = new ArrayList<String>();
        for (int i = 0; i < entities.length(); i++) {
            JSONObject project = entities.getJSONObject(i);
            projectKeys.add(project.getString("key"));
        }

        assertThat(projectKeys, Matchers.hasItems("HSP", "MKY"));
    }

    @Test
    public void testHasPublicSignup() throws Exception {
        if (!isBackdoorAvailable()) {
            return;
        }

        WebResponse response = restClient.GET("rest/func-test/1.0/applinks/hasPublicSignup");
        assertThat(response.getText(), equalTo("true"));
    }

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
        navigation.gotoAdmin();

    }

    protected final boolean isBackdoorAvailable() {

        boolean available = administration.plugins().isPluginInstalled(FUNC_TEST_PLUGIN_NAME);
        if (!available) {
            log.info(String.format("Plugin '%s' is not installed", FUNC_TEST_PLUGIN_NAME));
        }

        return available;
    }
}
