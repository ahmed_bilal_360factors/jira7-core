package com.atlassian.jira.webtests.ztests.admin.trustedapps;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.meterware.httpunit.HttpUnitOptions;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.BROWSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestTrustedApplications extends BaseJiraFuncTest {
    private static final String REQUEST_NEW_TRUSTED_APP_DETAILS = "Request New Trusted Application Details";

    @Inject
    private Administration administration;

    @Before
    public void setUp() {
        administration.restoreData("TestTrustedAppsBlank.xml");
    }

    @Test
    public void testAdminLinkWorks() {
        navigation.gotoAdmin();
        gotoViewTrustedAppsConfigs();
        tester.assertTextPresent("View Trusted Applications");
        tester.assertTextPresent(REQUEST_NEW_TRUSTED_APP_DETAILS);
        tester.assertTextPresent("No trusted applications have been configured.");
    }

    private void gotoViewTrustedAppsConfigs() {
        tester.gotoPage("/secure/admin/trustedapps/ViewTrustedApplications.jspa");
    }

    @Test
    public void testRequestSelf() {
        final String trustedAppName = "theTrustedApplicationName";

        navigation.gotoAdmin();
        gotoViewTrustedAppsConfigs();
        tester.setFormElement("trustedAppBaseUrl", getEnvironmentData().getBaseUrl().toExternalForm());
        tester.submit("Send Request");

        tester.setWorkingForm("jiraform");
        tester.assertTextPresent("Add New Trusted Application");
        tester.setFormElement("name", trustedAppName);
        tester.assertTextPresent("Application Name");
        tester.assertTextPresent("Application ID");
        tester.assertTextPresent("Timeout");
        tester.assertTextPresent("IP Address Matches");
        tester.assertTextPresent("URL Paths to Allow");
        tester.assertButtonNotPresent("Update");
        tester.submit("Add");

        tester.assertTextPresent("View Trusted Applications");
        tester.assertTextPresent(trustedAppName);
        tester.assertLinkPresent("edit-10010");
        tester.assertLinkPresent("delete-10010");

        boolean wasEnabled = HttpUnitOptions.isScriptingEnabled();
        HttpUnitOptions.setScriptingEnabled(true);
        try {
            tester.clickLink("edit-10010");

            tester.setWorkingForm("jiraform");
            tester.assertTextPresent("Edit Trusted Application Details");
            tester.assertButtonPresent("update_submit");
            tester.assertButtonNotPresent("add_submit");
            tester.clickLink("cancelButton");

            tester.assertTextPresent("View Trusted Applications");
            tester.clickLink("delete-10010");

            tester.setWorkingForm("jiraform");
            tester.assertTextPresent("Delete Trusted Application:");
            tester.assertTextPresent(trustedAppName);
            tester.assertButtonPresent("delete_submit");
            tester.clickLink("cancelButton");
        } finally {
            HttpUnitOptions.setScriptingEnabled(wasEnabled);
        }

        tester.assertTextPresent("View Trusted Applications");
        tester.clickLink("delete-10010");

        tester.setWorkingForm("jiraform");
        tester.submit("Delete");

        tester.assertTextPresent("View Trusted Applications");
        tester.assertTextNotPresent(trustedAppName);
        tester.assertLinkNotPresent("edit-10010");
        tester.assertLinkNotPresent("delete-10010");
        tester.assertTextPresent("No trusted applications have been configured.");
    }

    @Test
    public void testRequestSelfTwice() {
        navigation.gotoAdmin();
        gotoViewTrustedAppsConfigs();
        tester.setFormElement("trustedAppBaseUrl", getEnvironmentData().getBaseUrl().toExternalForm());
        tester.submit("Send Request");

        tester.setWorkingForm("jiraform");
        tester.assertTextPresent("Add New Trusted Application");
        tester.setFormElement("name", "theTrustedApplicationName");
        tester.submit("Add");
        tester.setFormElement("trustedAppBaseUrl", getEnvironmentData().getBaseUrl().toExternalForm());
        tester.submit("Send Request");

        tester.setWorkingForm("jiraform");
        tester.assertTextPresent("Add New Trusted Application");
        tester.setFormElement("name", "anuvverTrustedApplicationName");
        tester.submit("Add");
        tester.assertTextPresent("The Trusted Application with the specified applicationId");
        tester.assertTextPresent("already exists");
    }

    @Test
    public void testDirectBrowseWithNoPermission() {
        navigation.login("regularadmin", "regularadmin");
        navigation.gotoAdmin();
        tester.assertLinkNotPresent("trusted_apps");

        gotoViewTrustedAppsConfigs();
        tester.assertTextNotPresent("View Trusted Applications");

        tester.gotoPage("/secure/admin/trustedapps/EditTrustedApplication!default.jspa");
        tester.assertTextNotPresent("Edit Trusted Application");

        tester.gotoPage("/secure/admin/trustedapps/DeleteTrustedApplication!default.jspa");
        tester.assertTextNotPresent("Delete Trusted Application");
    }

    @Test
    public void testRequestAppBadUrl() {
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.gotoAdmin();
        gotoViewTrustedAppsConfigs();

        // malformed URL
        tester.setFormElement("trustedAppBaseUrl", "junkUrl-9287349287349");
        tester.submit("Send Request");
        // since url was bad, shouldn't have left the page
        tester.assertTextPresent(REQUEST_NEW_TRUSTED_APP_DETAILS);
        tester.assertTextPresent("java.net.MalformedURLException:");

        // unknown host
        final String unknownUrl = "http://www.something.invalid/";
        tester.setFormElement("trustedAppBaseUrl", unknownUrl);
        tester.submit("Send Request");
        tester.assertTextPresent(REQUEST_NEW_TRUSTED_APP_DETAILS);
        tester.assertTextPresent("The host " + unknownUrl + " could not be found");

        // known host with no certificate
        final String noTrustUrl = "http://www.google.com";
        tester.setFormElement("trustedAppBaseUrl", noTrustUrl);
        tester.submit("Send Request");
        tester.assertTextPresent(REQUEST_NEW_TRUSTED_APP_DETAILS);
        tester.assertTextPresent("The application at URL " + noTrustUrl + " does not support the Trusted Application protocol.");

        // JRA-16003: known host with no certificate and whitespace - should be the same error
        final String noTrustUrlWhitespace = "   http://www.google.com   ";
        tester.setFormElement("trustedAppBaseUrl", noTrustUrlWhitespace);
        tester.submit("Send Request");
        tester.assertTextPresent(REQUEST_NEW_TRUSTED_APP_DETAILS);
        tester.assertTextPresent("The application at URL " + noTrustUrlWhitespace.trim() + " does not support the Trusted Application protocol.");

        // known host which refuses connection - add a 1 to the port - may not work if port isn't explicit
        final String wrongTrustUrl = getUnlistenedURL();
        tester.setFormElement("trustedAppBaseUrl", wrongTrustUrl);
        tester.submit("Send Request");
        tester.assertTextPresent(REQUEST_NEW_TRUSTED_APP_DETAILS);
        tester.assertTextPresent("The host " + wrongTrustUrl + " could not be found.");
    }

    @Test
    public void testAddEditValidation() {
        final String trustedAppName = "theTrustedApplicationName";

        navigation.gotoAdmin();
        gotoViewTrustedAppsConfigs();
        tester.setFormElement("trustedAppBaseUrl", getEnvironmentData().getBaseUrl().toExternalForm());
        tester.submit("Send Request");

        // don't set name
        tester.setWorkingForm("jiraform");
        tester.setFormElement("name", "");
        tester.submit("Add");
        tester.assertTextPresent("Add New Trusted Application");
        tester.assertTextPresent("The Trusted Application Name cannot be blank.");

        // don't set timeout
        tester.setWorkingForm("jiraform");
        tester.setFormElement("name", trustedAppName);
        tester.setFormElement("timeout", "");
        tester.submit("Add");
        tester.assertTextPresent("Add New Trusted Application");
        tester.assertTextPresent("You must specify a positive Timeout value.");

        // set timeout to NaN
        tester.setWorkingForm("jiraform");
        tester.setFormElement("timeout", "asdasd");
        tester.submit("Add");
        tester.assertTextPresent("Add New Trusted Application");
        tester.assertTextPresent("You must specify a positive Timeout value.");

        // set timeout to negative number
        tester.setWorkingForm("jiraform");
        tester.setFormElement("timeout", "-8888");
        tester.submit("Add");
        tester.assertTextPresent("Add New Trusted Application");
        tester.assertTextPresent("You must specify a positive Timeout value.");

        // set timeout to zero
        tester.setWorkingForm("jiraform");
        tester.setFormElement("timeout", "0");
        tester.submit("Add");
        tester.assertTextPresent("Add New Trusted Application");
        tester.assertTextPresent("You must specify a positive Timeout value.");

        // set timeout to larger than Long.MAX_VALUE
        tester.setWorkingForm("jiraform");
        tester.setFormElement("timeout", "9223372036854775808");
        tester.submit("Add");
        tester.assertTextPresent("Add New Trusted Application");
        tester.assertTextPresent("You must specify a positive Timeout value.");

        // IP match string cannot be blank
        tester.setWorkingForm("jiraform");
        tester.setFormElement("timeout", "1000");
        tester.setFormElement("ipMatch", "");
        tester.submit("Add");
        tester.assertTextPresent("Add New Trusted Application");
        tester.assertTextPresent("You must specify IP addresses to match against.");

        // URL match string cannot be blank
        tester.setWorkingForm("jiraform");
        tester.setFormElement("timeout", "1000");
        tester.setFormElement("urlMatch", "");
        tester.submit("Add");
        tester.assertTextPresent("Add New Trusted Application");
        tester.assertTextPresent("You must specify URLs to match against.");

        {
            // malformed IP address
            final String malformedIp = "blah";
            tester.setWorkingForm("jiraform");
            tester.setFormElement("timeout", "1000");
            tester.setFormElement("ipMatch", malformedIp);
            tester.submit("Add");
            tester.assertTextPresent("Add New Trusted Application");
            assertBadIPMessage(malformedIp);
        }
        {
            // non IPv4 address
            final String malformedIp = "123.123.123.123.1.1";
            tester.setWorkingForm("jiraform");
            tester.setFormElement("timeout", "1000");
            tester.setFormElement("ipMatch", malformedIp);
            tester.submit("Add");
            tester.assertTextPresent("Add New Trusted Application");
            assertBadIPMessage(malformedIp);
        }
        {
            // greater than 255
            final String malformedIp = "299.299.299.299";
            tester.setWorkingForm("jiraform");
            tester.setFormElement("timeout", "1000");
            tester.setFormElement("ipMatch", malformedIp);
            tester.submit("Add");
            tester.assertTextPresent("Add New Trusted Application");
            assertBadIPMessage(malformedIp);
        }
        {
            // illegal separator
            final String malformedIp = "192,168,0,1";
            tester.setWorkingForm("jiraform");
            tester.setFormElement("timeout", "1000");
            tester.setFormElement("ipMatch", malformedIp);
            tester.submit("Add");
            tester.assertTextPresent("Add New Trusted Application");
            assertBadIPMessage(malformedIp);
        }
        {
            // illegal wildcard character
            final String malformedIp = "192.168.?.1";
            tester.setWorkingForm("jiraform");
            tester.setFormElement("timeout", "1000");
            tester.setFormElement("ipMatch", malformedIp);
            tester.submit("Add");
            tester.assertTextPresent("Add New Trusted Application");
            assertBadIPMessage(malformedIp);
        }
        {
            // multiple IP addresses (works)
            final String malformedIp = "123.123.123.123\n192.168.0.1";
            tester.setWorkingForm("jiraform");
            tester.setFormElement("timeout", "1000");
            tester.setFormElement("ipMatch", malformedIp);
            tester.setFormElement("urlMatch", "/some/url");
            tester.submit("Add");
            tester.assertTextPresent("View Trusted Applications");
            tester.assertTextPresent(trustedAppName);
            tester.assertLinkPresent("delete-10010");
        }
    }

    private void assertBadIPMessage(String malformedIp) {
        tester.assertTextPresent("The IP address pattern: " + malformedIp + " is invalid.");
    }

    private String getUnlistenedURL() {
        String host = "http://localhost:";
        int port = 8000;
        while (true) {
            URL url;
            try {
                url = new URL(host + ++port);
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            }
            HttpURLConnection connection = null;
            try {
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
            } catch (ConnectException e) {
                return url.toExternalForm();
            } catch (IOException ignoreAndContinue) {
                // ignore and try next port
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
        }
    }
}
