package com.atlassian.jira.webtests.zsuites;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A func test suite for shared entities such as Dashboards and Filters.  Not how this uses composition to create a test
 * suite
 *
 * @since v4.0
 */
public class FuncTestSuiteSharedEntities {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .addAll(FuncTestSuiteDashboards.suite())
                .addAll(FuncTestSuiteFilters.suite())
                .build();
    }
}
