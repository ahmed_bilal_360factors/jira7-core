package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.issue.TestCloneIssueWithSubTasks;
import com.atlassian.jira.webtests.ztests.issue.TestCloneIssueWithValidation;
import com.atlassian.jira.webtests.ztests.issue.clone.TestCloneIssueAttachments;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of SubTask related test
 *
 * @since v4.0
 */
public class FuncTestSuiteCloneIssue {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestCloneIssueWithSubTasks.class)
                .add(TestCloneIssueAttachments.class)
                .add(TestCloneIssueWithValidation.class)
                .build();
    }
}