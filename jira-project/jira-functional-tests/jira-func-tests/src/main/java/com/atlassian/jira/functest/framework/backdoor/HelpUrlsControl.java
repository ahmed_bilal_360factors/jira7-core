package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.webtests.util.JIRAEnvironmentData;

/**
 * HelpUrlsControl.
 *
 * @since 7.0
 */
public class HelpUrlsControl extends BackdoorControl<HelpUrlsControl> {
    public HelpUrlsControl(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public String getHelpUrl(String key) {
        return createResource().path("help/" + key).get(String.class);
    }


}
