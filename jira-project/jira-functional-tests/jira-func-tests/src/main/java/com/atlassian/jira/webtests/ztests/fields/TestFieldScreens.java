package com.atlassian.jira.webtests.ztests.fields;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.Screens;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.atlassian.jira.testkit.client.restclient.ProjectClient;
import com.meterware.httpunit.WebTable;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ASSIGN_FIELD_SCREEN_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CREATE_ISSUE_OPERATION_SCREEN;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_FIELD_SCREEN_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_ISSUE_TYPE_SCREEN_SCHEME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_OPERATION_SCREEN;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_SCREEN_SCHEME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.EDIT_ISSUE_OPERATION_SCREEN;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_TABLE_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_NEO;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_NEO_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SCREEN_TABLE_NAME_COLUMN_INDEX;
import static com.atlassian.jira.functest.framework.FunctTestConstants.VIEW_ISSUE_OPERATION_SCREEN;
import static com.atlassian.jira.functest.framework.Navigation.AdminSection.FIELD_SCREENS;
import static com.atlassian.jira.functest.framework.Navigation.AdminSection.FIELD_SCREEN_SCHEME;
import static com.atlassian.jira.functest.framework.Navigation.AdminSection.ISSUE_TYPE_SCREEN_SCHEME;
import static org.junit.Assert.fail;

/**
 * @since v5.0
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.FIELDS, Category.ISSUE_TYPES, Category.SCREENS})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestFieldScreens extends BaseJiraFuncTest {
    private static final String ADDED_SCREEN_NAME = "Test Add Screen";
    private static final String ADDED_SCREEN_SCHEME_NAME = "Test Add Screen Scheme";
    private static final String COPIED_SCREEN_SCHEME_NAME = "Test Copy Screen Scheme";
    private static final String ADDED_ISSUE_TYPE_SCREEN_SCHEME_NAME = "Test Add Issue Type Screen Scheme";
    private static final String COPIED_ISSUE_TYPE_SCREEN_SCHEME_NAME = "Test Copy Issue Tyep Screen Scheme";
    private static final String CUSTOM_FIELD_NAME = "Animal";
    private String issueKey;
    private String issueKey2;
    private String customFieldId;
    @Inject
    private Screens screens;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        navigation.login(ADMIN_USERNAME);

        resetSettings();
        customFieldId = administration.customFields().addCustomField("com.atlassian.jira.plugin.system.customfieldtypes:textfield", CUSTOM_FIELD_NAME, new String[0], new String[0]);
        issueKey = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "test field screen", ADMIN_USERNAME, "Minor", "Bug").key();
        backdoor.issues().setIssueFields(issueKey, new IssueFields()
                .description("test description for field screens")
                .environment("priority is added to assign issue screen"));
        createIssueWithCustomField();
    }

    @Test
    public void testFieldScreens() {
        fieldScreensAddFieldToFieldScreen();
        fieldScreensSetFieldInWorkflow();
        fieldScreensRemoveFieldFromFieldScreen();
        fieldScreensAddScreen();
        fieldScreensAddScreenCancel();
        fieldScreensAddScreenWithDuplicateName();
        fieldScreensAddScreenWithInvalidName();
        fieldScreensStandardScreens();

        fieldScreensAddScreenScheme();
        fieldScreensAddScreenSchemeWithDuplicateName();
        fieldScreensAddScreenSchemeWithInvalidName();

        fieldScreensAddIssueTypeScreenScheme();
        fieldScreensAddIssueTypeScreenSchemeWithDuplicateName();
        fieldScreensAddIssueTypeToScreenAssociation();

        fieldScreensProjectScreenSchemes();

        fieldScreensIssueTypeScreenSchemes();
        fieldScreensCopyIssueTypeScreenSchemes();
        fieldScreensDeleteIssueTypeScreenSchemes();

        fieldScreensCopyScreenScheme();
        fieldScreensDeleteScreenScheme();

        fieldScreensDeleteScreen();
    }


    @Test
    public void testDeleteIssueTypeSchemeFailsWhenAssignedToProject() {
        Long issueTypeScreenSchemeId = backdoor.issueTypeScreenSchemes().createScheme("DELETEME", "blah", 1L);
        navigation.gotoPage(ISSUE_TYPE_SCREEN_SCHEME.getUrl());
        backdoor.project().setIssueTypeScreenScheme(10000L, issueTypeScreenSchemeId);
        tester.clickLink("delete_issuetypescreenscheme_10000");

        tester.assertTextPresent("Cannot delete an issue type screen scheme that is being used by a project");

        tester.submit("Delete");
        tester.assertTextPresent("Cannot delete an issue type screen scheme that is being used by a project");
    }

    /**
     * Check that fields are added correctly and in the right position
     */
    private void fieldScreensAddFieldToFieldScreen() {
        backdoor.screens().addFieldToScreen(ASSIGN_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME);
        // Add custom field to the default screen
        backdoor.screens().addFieldToScreen(ASSIGN_FIELD_SCREEN_NAME, "Issue Type");
    }

    /**
     * Test the configuration of the assign issue screen
     */
    private void fieldScreensSetFieldInWorkflow() {
        navigation.issue().gotoIssue(issueKey);
        tester.clickLinkWithText("Close Issue");
        tester.assertFormElementNotPresent(customFieldId);
        tester.setWorkingForm("issue-workflow-transition");
        tester.submit("Transition");
        tester.clickLinkWithText("Reopen Issue");
        tester.setFormElement(customFieldId, "Polar Bear");
        tester.setWorkingForm("issue-workflow-transition");
        tester.submit("Transition");
    }

    /**
     * Test that field screens can be removed from field screens
     */
    private void fieldScreensRemoveFieldFromFieldScreen() {
        final String[] fieldNames = new String[]{CUSTOM_FIELD_NAME, "Issue Type"};

        removeFieldFromFieldScreen(ASSIGN_FIELD_SCREEN_NAME, fieldNames);

        final WebTable webTable = assertions.getTableAssertions().getWebTable(FIELD_TABLE_ID);
        if (webTable != null) {
            final String text = webTable.getCellAsText(1, SCREEN_TABLE_NAME_COLUMN_INDEX);
            fail("Field was not removed around here: [" + text + "]");
        }
    }

    private void removeFieldFromFieldScreen(String screenName, String[] fieldNames) {
        logger.log("Removing Fields from field screen.");

        gotoFieldScreen(screenName);
        textAssertions.assertTextPresent("Configure Screen");
        for (String fieldName : fieldNames) {
            backdoor.screens().removeFieldFromScreen(screenName, fieldName);
        }
    }

    private void gotoFieldScreen(String screenName) {
        navigation.gotoPage(FIELD_SCREENS.getUrl());
        textAssertions.assertTextPresent("View Screens");
        tester.assertLinkPresent("configure_fieldscreen_" + screenName);
        tester.clickLink("configure_fieldscreen_" + screenName);
    }

    /**
     * Test that a screen is added properly
     */
    private void fieldScreensAddScreen() {
        screens.addScreen(ADDED_SCREEN_NAME, "");
        navigation.gotoPage(FIELD_SCREENS.getUrl());
        tester.assertLinkPresent("delete_fieldscreen_" + ADDED_SCREEN_NAME);
    }

    /**
     * Test that hitting Cancel goes back to the View Screens page
     */
    private void fieldScreensAddScreenCancel() {
        navigation.gotoPage(FIELD_SCREENS.getUrl());
        tester.clickLink("add-field-screen");
        tester.clickLink("field-screen-add-cancel");
        textAssertions.assertTextPresent(locator.css("header h2"), "View Screens");
    }

    /**
     * Test error checking if a screen is added with a duplicate name
     */
    private void fieldScreensAddScreenWithDuplicateName() {
        screens.addScreen(ADDED_SCREEN_NAME, "");
        tester.assertTextPresent("A Screen with this name already exists.");
    }

    /**
     * Test error checking if a screen is added with a empty string as the name.
     */
    private void fieldScreensAddScreenWithInvalidName() {
        screens.addScreen("", "");
        tester.assertTextPresent("You must enter a valid name.");
    }

    /**
     * Check screen is deleted correctly
     */
    private void fieldScreensDeleteScreen() {
        screens.deleteScreen(ADDED_SCREEN_NAME);
        tester.assertLinkNotPresent("delete_fieldscreen_" + ADDED_SCREEN_NAME);
    }

    /**
     * Check screen is copied correctly
     */


    /**
     * Check a screen scheme is added
     */
    private void fieldScreensAddScreenScheme() {
        screens.addFieldScreenScheme(ADDED_SCREEN_SCHEME_NAME, "");
        navigation.gotoPage(FIELD_SCREEN_SCHEME.getUrl());
        tester.assertLinkPresent("delete_fieldscreenscheme_" + ADDED_SCREEN_SCHEME_NAME);
    }

    /**
     * Test error checking if a screen scheme is added with a duplicate name
     */
    private void fieldScreensAddScreenSchemeWithDuplicateName() {
        screens.addFieldScreenScheme(ADDED_SCREEN_SCHEME_NAME, "");
        tester.assertTextPresent("A Screen Scheme with this name already exists.");
    }

    /**
     * Test error checking if a screen scheme is added with an invalid name
     */
    private void fieldScreensAddScreenSchemeWithInvalidName() {
        screens.addFieldScreenScheme("", "");
        tester.assertTextPresent("You must enter a valid name.");
    }

    /**
     * Check that the screen scheme is copied correctly
     */
    private void fieldScreensCopyScreenScheme() {
        screens.copyFieldScreenScheme(ADDED_SCREEN_SCHEME_NAME, COPIED_SCREEN_SCHEME_NAME, "");
        tester.clickLink("configure_fieldscreenscheme_" + COPIED_SCREEN_SCHEME_NAME);
        tester.assertLinkPresent("edit_fieldscreenscheme_" + DEFAULT_OPERATION_SCREEN);
//        assertLinkPresent("edit_fieldscreenscheme_" + CREATE_ISSUE_OPERATION_SCREEN);
    }

    /**
     * Check that the screen scheme is deleted
     */
    private void fieldScreensDeleteScreenScheme() {
        screens.deleteFieldScreenScheme(COPIED_SCREEN_SCHEME_NAME);
        tester.assertLinkNotPresent("delete_fieldscreenscheme_" + COPIED_SCREEN_SCHEME_NAME);
        screens.deleteFieldScreenScheme(ADDED_SCREEN_SCHEME_NAME);
        tester.assertLinkNotPresent("delete_fieldscreenscheme_" + ADDED_SCREEN_SCHEME_NAME);
    }

    private void fieldScreensAddIssueTypeScreenScheme() {
        screens.addIssueTypeFieldScreenScheme(ADDED_ISSUE_TYPE_SCREEN_SCHEME_NAME, "", DEFAULT_SCREEN_SCHEME);
    }

    private void fieldScreensAddIssueTypeScreenSchemeWithDuplicateName() {
        screens.addIssueTypeFieldScreenScheme(ADDED_ISSUE_TYPE_SCREEN_SCHEME_NAME, "", DEFAULT_SCREEN_SCHEME);
        tester.assertTextPresent("A scheme with this name already exists.");
    }

    private void fieldScreensAddIssueTypeToScreenAssociation() {
        addIssueTypeToScreenAssociation("10000", "Bug", ADDED_SCREEN_SCHEME_NAME);
        tester.assertLinkPresent("delete_issuetypescreenschemeentity_Bug");
    }

    private void fieldScreensCopyIssueTypeScreenSchemes() {
        screens.copyIssueTypeFieldScreenSchemeName("10000", COPIED_ISSUE_TYPE_SCREEN_SCHEME_NAME, "");
        tester.clickLink("configure_issuetypescreenscheme_10001");
        tester.assertLinkPresent("edit_issuetypescreenschemeentity_default");
        tester.assertLinkPresent("edit_issuetypescreenschemeentity_Bug");
    }

    private void fieldScreensDeleteIssueTypeScreenSchemes() {
        screens.deleteIssueTypeFieldScreenScheme("10001");
        tester.assertLinkNotPresent("delete_issuetypescreenscheme_10001");

        associateIssueTypeScreenSchemeToProject(PROJECT_NEO, DEFAULT_ISSUE_TYPE_SCREEN_SCHEME);
        screens.deleteIssueTypeFieldScreenScheme("10000");
        tester.assertLinkNotPresent("delete_issuetypescreenscheme_10000");
    }

    private void addIssueTypeToScreenAssociation(String issueTypeSchemeId, String issueType, String screenSchemeName) {
        gotoIssueTypeScreenScheme(issueTypeSchemeId);
        tester.clickLink("add-issue-type-screen-scheme-configuration-association");
        tester.selectOption("issueTypeId", issueType);
        tester.selectOption("fieldScreenSchemeId", screenSchemeName);
        tester.submit("Add");
    }

    private void associateIssueTypeScreenSchemeToProject(String projectName, String screenScheme) {
        Project project = getProjectByName(projectName);
        tester.gotoPage("/secure/project/SelectIssueTypeScreenScheme!default.jspa?projectId=" + project.id);
        tester.selectOption("schemeId", screenScheme);
        tester.submit("Associate");
    }

    private Project getProjectByName(String projectName) {
        final List<Project> projects = new ProjectClient(environmentData).getProjects();
        return projects.stream()
                .filter(p -> p.name.equals(projectName))
                .findFirst()
                .orElse(null);
    }

    private void gotoIssueTypeScreenScheme(String schemeName) {
        navigation.gotoPage(ISSUE_TYPE_SCREEN_SCHEME.getUrl());
        tester.clickLink("configure_issuetypescreenscheme_" + schemeName);
    }

    /**
     * Check schemes using issue type based schemes
     */
    private void fieldScreensIssueTypeScreenSchemes() {
        logger.log("Check schemes using issue type based schemes");
        associateIssueTypeScreenSchemeToProject(PROJECT_NEO, ADDED_ISSUE_TYPE_SCREEN_SCHEME_NAME);
        backdoor.screens().addFieldToScreen(ADDED_SCREEN_NAME, "Summary");
        backdoor.screens().addFieldToScreen(ADDED_SCREEN_NAME, CUSTOM_FIELD_NAME);

        String issueKeyCustomField = checkCreateIssueScreenScheme(ADDED_SCREEN_SCHEME_NAME, PROJECT_NEO, PROJECT_NEO_KEY, "Bug");
        checkViewIssueScreenScheme(ADDED_SCREEN_SCHEME_NAME, PROJECT_NEO, "Bug", issueKeyCustomField);
        checkEditIssueScreenScheme(ADDED_SCREEN_SCHEME_NAME, PROJECT_NEO, "Bug", issueKeyCustomField);

        screens.addIssueOperationToScreenAssociation(ADDED_SCREEN_SCHEME_NAME, CREATE_ISSUE_OPERATION_SCREEN, ADDED_SCREEN_NAME);
        screens.addIssueOperationToScreenAssociation(ADDED_SCREEN_SCHEME_NAME, VIEW_ISSUE_OPERATION_SCREEN, ADDED_SCREEN_NAME);
        screens.addIssueOperationToScreenAssociation(ADDED_SCREEN_SCHEME_NAME, EDIT_ISSUE_OPERATION_SCREEN, ADDED_SCREEN_NAME);
        checkNoScreenScheme(PROJECT_NEO, "Improvement", issueKey2);
        screens.deleteIssueOperationFromScreenAssociation(ADDED_SCREEN_SCHEME_NAME, CREATE_ISSUE_OPERATION_SCREEN);
        screens.deleteIssueOperationFromScreenAssociation(ADDED_SCREEN_SCHEME_NAME, VIEW_ISSUE_OPERATION_SCREEN);
        screens.deleteIssueOperationFromScreenAssociation(ADDED_SCREEN_SCHEME_NAME, EDIT_ISSUE_OPERATION_SCREEN);

        checkNoScreenScheme(PROJECT_NEO, "Bug", issueKeyCustomField);

        removeFieldFromFieldScreen(ADDED_SCREEN_NAME, new String[]{"Summary", CUSTOM_FIELD_NAME});
        associateIssueTypeScreenSchemeToProject(PROJECT_NEO, DEFAULT_ISSUE_TYPE_SCREEN_SCHEME);
    }

    /**
     * Check screens using project based schemes
     */
    private void fieldScreensProjectScreenSchemes() {
        logger.log("Check screens using project based schemes");
        associateIssueTypeScreenSchemeToProject(PROJECT_HOMOSAP, ADDED_ISSUE_TYPE_SCREEN_SCHEME_NAME);
        backdoor.screens().addFieldToScreen(ADDED_SCREEN_NAME, "Summary");
        backdoor.screens().addFieldToScreen(ADDED_SCREEN_NAME, CUSTOM_FIELD_NAME);

        String issueKeyCustomField = checkCreateIssueScreenScheme(ADDED_SCREEN_SCHEME_NAME, PROJECT_HOMOSAP, PROJECT_HOMOSAP_KEY, "Bug");
        checkViewIssueScreenScheme(ADDED_SCREEN_SCHEME_NAME, PROJECT_HOMOSAP, "Bug", issueKeyCustomField);
        checkEditIssueScreenScheme(ADDED_SCREEN_SCHEME_NAME, PROJECT_HOMOSAP, "Bug", issueKeyCustomField);

        screens.addIssueOperationToScreenAssociation(ADDED_SCREEN_SCHEME_NAME, CREATE_ISSUE_OPERATION_SCREEN, ADDED_SCREEN_NAME);
        screens.addIssueOperationToScreenAssociation(ADDED_SCREEN_SCHEME_NAME, VIEW_ISSUE_OPERATION_SCREEN, ADDED_SCREEN_NAME);
        screens.addIssueOperationToScreenAssociation(ADDED_SCREEN_SCHEME_NAME, EDIT_ISSUE_OPERATION_SCREEN, ADDED_SCREEN_NAME);
        checkNoScreenScheme(PROJECT_NEO, "Bug", issueKey2);
        screens.deleteIssueOperationFromScreenAssociation(ADDED_SCREEN_SCHEME_NAME, CREATE_ISSUE_OPERATION_SCREEN);
        screens.deleteIssueOperationFromScreenAssociation(ADDED_SCREEN_SCHEME_NAME, VIEW_ISSUE_OPERATION_SCREEN);
        screens.deleteIssueOperationFromScreenAssociation(ADDED_SCREEN_SCHEME_NAME, EDIT_ISSUE_OPERATION_SCREEN);

        removeFieldFromFieldScreen(ADDED_SCREEN_NAME, new String[]{"Summary", CUSTOM_FIELD_NAME});
        checkNoScreenScheme(PROJECT_HOMOSAP, "Bug", issueKeyCustomField);

        associateIssueTypeScreenSchemeToProject(PROJECT_HOMOSAP, DEFAULT_ISSUE_TYPE_SCREEN_SCHEME);
    }

    /**
     * Check screen functionality using standard settings
     */
    private void fieldScreensStandardScreens() {
        logger.log("Check screens for standard settings");
        backdoor.screens().addFieldToScreen(ADDED_SCREEN_NAME, "Summary");
        backdoor.screens().addFieldToScreen(ADDED_SCREEN_NAME, CUSTOM_FIELD_NAME);

        String issueKeyCustomField = checkCreateIssueScreenScheme(DEFAULT_SCREEN_SCHEME, PROJECT_HOMOSAP, PROJECT_HOMOSAP_KEY, "Bug");
        checkViewIssueScreenScheme(DEFAULT_SCREEN_SCHEME, PROJECT_HOMOSAP, "Bug", issueKeyCustomField);
        checkEditIssueScreenScheme(DEFAULT_SCREEN_SCHEME, PROJECT_HOMOSAP, "Bug", issueKeyCustomField);

        removeFieldFromFieldScreen(ADDED_SCREEN_NAME, new String[]{"Summary", CUSTOM_FIELD_NAME});
        checkNoScreenScheme(PROJECT_HOMOSAP, "Bug", issueKeyCustomField);
    }

    // Helper Functions
    private void resetSettings() {
        if (administration.project().projectWithKeyExists(PROJECT_HOMOSAP_KEY)) {
            logger.log("Project " + PROJECT_HOMOSAP + " exists");
        } else {
            administration.project().addProject(PROJECT_HOMOSAP, PROJECT_HOMOSAP_KEY, ADMIN_USERNAME);
        }
        if (administration.project().projectWithKeyExists(PROJECT_NEO_KEY)) {
            logger.log("Project: " + PROJECT_NEO + " exists");
        } else {
            administration.project().addProject(PROJECT_NEO, PROJECT_NEO_KEY, ADMIN_USERNAME);
        }

        associateIssueTypeScreenSchemeToProject(PROJECT_HOMOSAP, DEFAULT_ISSUE_TYPE_SCREEN_SCHEME);
        associateIssueTypeScreenSchemeToProject(PROJECT_NEO, DEFAULT_ISSUE_TYPE_SCREEN_SCHEME);
        removeAllRemainingIssueTypeScreenSchemes();

//        removeFieldFromFieldScreen(ASSIGN_FIELD_SCREEN_NAME, new String[] {CUSTOM_FIELD_NAME, "Issue Type"});

        administration.customFields().removeAllCustomFields();

        screens.removeAllRemainingScreenAssociationsFromDefault();
        removeAllRemainingIssueTypeScreenSchemes();
        screens.removeAllRemainingFieldScreens();
    }

    public void removeAllRemainingIssueTypeScreenSchemes() {
        navigation.gotoPage(ISSUE_TYPE_SCREEN_SCHEME.getUrl());

        if (tester.getDialog().isLinkPresentWithText("Delete")) {
            tester.clickLinkWithText("Delete");
            tester.submit("Delete");
            removeAllRemainingIssueTypeScreenSchemes();
        }
    }

    private void createIssueWithCustomField() {
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME);
        navigation.issue().goToCreateIssueForm(PROJECT_NEO, "Improvement");

        tester.setFormElement("summary", "This is an issue in project 2 with a custom field");
        tester.setFormElement(customFieldId, "Elephant");

        tester.submit();

        issueKey2 = getIssueKey(PROJECT_NEO_KEY);

        removeFieldFromFieldScreen(DEFAULT_FIELD_SCREEN_NAME, new String[]{CUSTOM_FIELD_NAME});
    }

    private String getIssueKey(String projectKey) {
        try {
            String text = tester.getDialog().getResponse().getText();
            int projectIdLocation = text.indexOf("[" + projectKey) + 1;
            int endOfIssueKey = text.indexOf("]", projectIdLocation);
            return text.substring(projectIdLocation, endOfIssueKey);
        } catch (IOException e) {
            fail("Unable to retrieve issue key" + e.getMessage());
        }

        return null;
    }

    /**
     * Check that field screen is not shown for all screens
     */
    private void checkNoScreenScheme(String project, String issueType, String issueKeyCustomField) {
        logger.log("Checking scheme association for with no scheme selected");
        navigation.issue().goToCreateIssueForm(project, issueType);

        tester.assertFormElementNotPresent(customFieldId);

        tester.submit();

        navigation.issue().gotoIssue(issueKeyCustomField);
        tester.assertTextNotPresent(CUSTOM_FIELD_NAME);

        tester.clickLink("edit-issue");
        tester.assertFormElementNotPresent(customFieldId);
    }

    /**
     * Check that field screen is shown for the Create Issue screen but not for view issue or edit issue
     */
    private String checkCreateIssueScreenScheme(String screenScheme, String project, String project_key, String issueType) {
        logger.log("Checking scheme association for Create");
        screens.addIssueOperationToScreenAssociation(screenScheme, CREATE_ISSUE_OPERATION_SCREEN, ADDED_SCREEN_NAME);

        navigation.issue().goToCreateIssueForm(project, issueType);

        tester.setFormElement("summary", "This is a test to see if field is shown");
        tester.setFormElement(customFieldId, "Elephant");

        tester.submit();

        tester.assertTextNotPresent("Elephant");
        tester.assertTextNotPresent(CUSTOM_FIELD_NAME);

        String issueKeyCustomField = getIssueKey(project_key);

        tester.clickLink("edit-issue");
        tester.assertFormElementNotPresent(customFieldId);

        screens.deleteIssueOperationFromScreenAssociation(screenScheme, CREATE_ISSUE_OPERATION_SCREEN);

        return issueKeyCustomField;
    }

    /**
     * Check field screen is shown for View issue screen but not create issue or edit issue screens
     */
    private void checkViewIssueScreenScheme(String screenScheme, String project, String issueType, String issueKeyCustomField) {
        logger.log("Checking scheme association for View");
        screens.addIssueOperationToScreenAssociation(screenScheme, VIEW_ISSUE_OPERATION_SCREEN, ADDED_SCREEN_NAME);

        navigation.issue().goToCreateIssueForm(project, issueType);
        tester.assertFormElementNotPresent(customFieldId);

        navigation.issue().gotoIssue(issueKeyCustomField);
        tester.assertTextPresent("Elephant");
        tester.assertTextPresent(CUSTOM_FIELD_NAME);

        tester.clickLink("edit-issue");
        tester.assertFormElementNotPresent(customFieldId);

        screens.deleteIssueOperationFromScreenAssociation(screenScheme, VIEW_ISSUE_OPERATION_SCREEN);
    }

    /**
     * Check field screen is shown for Edit issue screen but not create issue or view issue screens
     */
    private void checkEditIssueScreenScheme(String screenScheme, String project, String issueType, String issueKeyCustomField) {
        logger.log("Checking scheme association for Edit");
        screens.addIssueOperationToScreenAssociation(screenScheme, EDIT_ISSUE_OPERATION_SCREEN, ADDED_SCREEN_NAME);

        navigation.issue().goToCreateIssueForm(project, issueType);
        tester.assertFormElementNotPresent(customFieldId);

        navigation.issue().gotoIssue(issueKeyCustomField);
        tester.clickLink("edit-issue");

        tester.setFormElement(customFieldId, "Whale");
        tester.submit();
        tester.assertTextNotPresent("Whale");
        tester.assertTextNotPresent(CUSTOM_FIELD_NAME);

        screens.deleteIssueOperationFromScreenAssociation(screenScheme, EDIT_ISSUE_OPERATION_SCREEN);
    }
}
