package com.atlassian.jira.functest.rule;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.util.function.Supplier;

/**
 * @since v7.1
 */
public class DisableOnboardingRule implements TestRule {

    public static final String DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG = "jira.onboarding.feature.disabled";

    private final Supplier<Backdoor> backdoorSupplier;

    public DisableOnboardingRule(final Supplier<Backdoor> backdoorSupplier) {
        this.backdoorSupplier = backdoorSupplier;
    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new StatementDecorator(base, this::disableOnboarding);
    }

    private void disableOnboarding() {
        if (getBackdoor().dataImport().isSetUp()) {
            getBackdoor().darkFeatures().enableForSite(DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG);
        }
    }

    private Backdoor getBackdoor() {
        return backdoorSupplier.get();
    }
}
