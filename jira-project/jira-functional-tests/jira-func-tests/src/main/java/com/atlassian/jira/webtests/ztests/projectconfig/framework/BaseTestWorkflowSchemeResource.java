package com.atlassian.jira.webtests.ztests.projectconfig.framework;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.rule.IssueTypeUrls;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_IMPROVEMENT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_NEWFEATURE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TASK;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_IMPROVEMENT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_NEWFEATURE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_TASK;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY_KEY;

public abstract class BaseTestWorkflowSchemeResource extends BaseJiraFuncTest {

    protected static final SimpleProject HSP = new SimpleProject(10000L, PROJECT_HOMOSAP_KEY, PROJECT_HOMOSAP, null, "project for homosapiens");
    protected static final SimpleProject MKY = new SimpleProject(10001L, PROJECT_MONKEY_KEY, PROJECT_MONKEY, null, "project for monkeys");
    protected static final SimpleProject TEST = new SimpleProject(10010L, "TEST", "test", null, null);

    protected static final SimpleProject PROJECT_WITH_DEFAULT_WORKFLOW = new SimpleProject(10110L, "PUDW", "Project Using Default Workflow", null, null);
    protected static final SimpleProject PROJECT_WITHOUT_DEFAULT_WORKFLOW = new SimpleProject(10111L, "PNUDW", "Project Not Using Default Workflow", null, null);
    protected static final SimpleProject PROJECT_WITH_DRAFT_WORKFLOW_SCHEME = new SimpleProject(10210L, "PWDWS", "Project With Draft Workflow Scheme", null, null);
    protected static final SimpleProject PROJECT_USING_DEFAULT_WORKFLOW_SCHEME = new SimpleProject(10310L, "PUDWS", "Project Using Default Workflow Scheme", null, null);
    protected static final SimpleProject PROJECT_NOT_USING_DEFAULT_WORKFLOW_SCHEME = new SimpleProject(10311L, "PNUDWS", "Project Not Using Default Workflow Scheme", null, null);
    protected static final SimpleProject PROJECT_WITH_SHARED_DRAFT_SCHEME = new SimpleProject(10410L, "PWDSBS", "Project With Draft Scheme But Shared", null, null);
    protected static final SimpleProject PROJECT_WITH_WORKFLOW_TO_DELETE = new SimpleProject(10510L, "PWDSWTD", "project with draft scheme and workflow to delete", null, null);

    protected static final String MONKEY_WORKFLOW = "monkey Workflow";
    protected static final String MONKEY_2_WORKFLOW = "monkey 2 Workflow";
    protected static final String PROJECT_NOT_USING_DEFAULT_WORKFLOW_SCHEME_WORKFLOW = "Project Not Using Default Workflow Scheme Workflow";
    protected static final String DEFAULT_WORKFLOW = "jira";

    protected static final SimpleUser ADMIN_USER = new SimpleUser("Administrator", "admin", "", true);

    protected static final String PROJECT_ADMIN_USERNAME = "proj";

    protected IssueTypeUrls issueTypeUrls;

    @Before
    public void setUpTest() {
        issueTypeUrls = IssueTypeUrls.init(backdoor);
    }

    private String getIssueTypeUrl(String issueTypeName) {
        return getIconUrl(issueTypeUrls.getIssueTypeUrl(issueTypeName));
    }

    private String getIconUrl(String url) {
        String context = StringUtils.stripToNull(environmentData.getContext());
        if (context == null) {
            return url;
        } else {
            return StringUtils.removeEnd(context, "/") + url;
        }
    }

    protected SimpleIssueType bug() {
        return new SimpleIssueType(getIssueTypeUrl("bug"), ISSUE_TYPE_BUG, "A problem which impairs or prevents the functions of the product.", ISSUE_BUG, false, false);
    }

    protected SimpleIssueType improvement() {
        return new SimpleIssueType(getIssueTypeUrl("improvement"), ISSUE_TYPE_IMPROVEMENT, "An improvement or enhancement to an existing feature or task.", ISSUE_IMPROVEMENT, false, false);
    }

    protected SimpleIssueType monkey() {
        return new SimpleIssueType(getIssueTypeUrl("monkey"), "Monkey", null, "6", false, false);
    }

    protected SimpleIssueType newFeature() {
        return new SimpleIssueType(getIssueTypeUrl("new feature"), ISSUE_TYPE_NEWFEATURE, "A new feature of the product, which has yet to be developed.", ISSUE_NEWFEATURE, false, false);
    }

    protected SimpleIssueType task() {
        return new SimpleIssueType(getIssueTypeUrl("task"), ISSUE_TYPE_TASK, "A task that needs to be done.", ISSUE_TASK, false, false);
    }

    protected SimpleIssueType ignored() {
        return new SimpleIssueType(getIssueTypeUrl("ignored"), "Ignored", null, "5", false, false);
    }
}
