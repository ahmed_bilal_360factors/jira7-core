package com.atlassian.jira.webtests.ztests.timetracking.legacy;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.permission.ProjectPermissions.DELETE_ALL_WORKLOGS;
import static com.atlassian.jira.permission.ProjectPermissions.DELETE_OWN_WORKLOGS;
import static com.atlassian.jira.permission.ProjectPermissions.EDIT_ALL_WORKLOGS;
import static com.atlassian.jira.permission.ProjectPermissions.EDIT_OWN_WORKLOGS;
import static com.atlassian.jira.permission.ProjectPermissions.WORK_ON_ISSUES;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 *
 */
@WebTest({Category.FUNC_TEST, Category.TIME_TRACKING, Category.WORKLOGS})
@LoginAs(user = ADMIN_USERNAME)
public class TestWorkLogTabPanel extends BaseJiraFuncTest {
    private static final String ADMIN_TIME_WORKED = "4h 30m";
    private static final String ADMIN_TIME_PERFORMED = "18/Jun/07 05:39 PM";
    private static final String ADMIN_COMMENT = "Admin's worklog comment.";
    private static final String ADMIN = ADMIN_USERNAME;
    private static final String FRED_TIME_WORKED = "3d";
    private static final String FRED_TIME_PERFORMED = "20/Jun/07 05:39 PM";
    private static final String FRED_COMMENT = "Fred's worklog comment.";
    private static final String FRED = FRED_USERNAME;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        administration.restoreData("TestLogWork.xml");
        grantPermission(FRED, WORK_ON_ISSUES);
        addTestWorklogs();
    }

    @Test
    public void testViewIssuePermissionOnly() {
        gotoHSP1WorklogTab();
        assertWorklogsPresent();

        //no edit/delete links should be present
        assertFalse(isAdminWorklogEditLinkPresent());
        assertFalse(isAdminWorklogDeleteLinkPresent());
        assertFalse(isFredsWorklogEditLinkPresent());
        assertFalse(isFredsWorklogDeleteLinkPresent());
    }

    @Test
    public void testEditOwnWorklogsPermission() {
        grantPermission(ADMIN, EDIT_OWN_WORKLOGS);

        gotoHSP1WorklogTab();
        assertWorklogsPresent();

        //can edit own
        assertTrue(isAdminWorklogEditLinkPresent());
        assertFalse(isAdminWorklogDeleteLinkPresent());
        assertFalse(isFredsWorklogEditLinkPresent());
        assertFalse(isFredsWorklogDeleteLinkPresent());
    }

    @Test
    public void testEditAllWorklogsPermission() {
        grantPermission(ADMIN, EDIT_ALL_WORKLOGS);

        gotoHSP1WorklogTab();
        assertWorklogsPresent();

        //can edit all
        assertTrue(isAdminWorklogEditLinkPresent());
        assertFalse(isAdminWorklogDeleteLinkPresent());
        assertTrue(isFredsWorklogEditLinkPresent());
        assertFalse(isFredsWorklogDeleteLinkPresent());
    }

    @Test
    public void testDeleteOwnWorklogsPermission() {
        grantPermission(ADMIN, DELETE_OWN_WORKLOGS);

        gotoHSP1WorklogTab();
        assertWorklogsPresent();

        //can delete own
        assertFalse(isAdminWorklogEditLinkPresent());
        assertTrue(isAdminWorklogDeleteLinkPresent());
        assertFalse(isFredsWorklogEditLinkPresent());
        assertFalse(isFredsWorklogDeleteLinkPresent());
    }

    @Test
    public void testDeleteAllWorklogsPermission() {
        grantPermission(ADMIN, DELETE_ALL_WORKLOGS);

        gotoHSP1WorklogTab();
        assertWorklogsPresent();

        //can delete all
        assertFalse(isAdminWorklogEditLinkPresent());
        assertTrue(isAdminWorklogDeleteLinkPresent());
        assertFalse(isFredsWorklogEditLinkPresent());
        assertTrue(isFredsWorklogDeleteLinkPresent());
    }

    @Test
    public void testFullWorklogPermissions() {
        grantPermission(ADMIN, EDIT_ALL_WORKLOGS);
        grantPermission(ADMIN, DELETE_ALL_WORKLOGS);

        gotoHSP1WorklogTab();
        assertWorklogsPresent();

        //can edit/delete all
        assertTrue(isAdminWorklogEditLinkPresent());
        assertTrue(isAdminWorklogDeleteLinkPresent());
        assertTrue(isFredsWorklogEditLinkPresent());
        assertTrue(isFredsWorklogDeleteLinkPresent());
    }

    private void assertWorklogsPresent() {
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{
                ADMIN_FULLNAME, "Time Spent:", "4 hours, 30 minutes", "Admin&#39;s worklog comment.",
                FRED_FULLNAME, "Time Spent:", "3 days", "Fred&#39;s worklog comment."
        });
    }

    private void gotoHSP1WorklogTab() {
        navigation.issue().gotoIssue("HSP-1");
        tester.clickLinkWithText("Work Log");
    }

    private boolean isAdminWorklogEditLinkPresent() {
        logger.log("checking if the edit link for Admin's worklog is present");
        return tester.getDialog().isTextInResponse("/secure/UpdateWorklog!default.jspa?id=10000&worklogId=10000");
    }

    private boolean isAdminWorklogDeleteLinkPresent() {
        logger.log("checking if the delete link for Admin's worklog is present");
        return tester.getDialog().isTextInResponse("/secure/DeleteWorklog!default.jspa?id=10000&worklogId=10000");
    }

    private boolean isFredsWorklogEditLinkPresent() {
        logger.log("checking if the edit link for Fred's worklog is present");
        return tester.getDialog().isTextInResponse("/secure/UpdateWorklog!default.jspa?id=10000&worklogId=10001");
    }

    private boolean isFredsWorklogDeleteLinkPresent() {
        logger.log("checking if the edit link for Fred's worklog is present");
        return tester.getDialog().isTextInResponse("/secure/DeleteWorklog!default.jspa?id=10000&worklogId=10001");
    }

    private void addTestWorklogs() {
        //assumed preconditions: currently logged in as admin, fred has "log work" permission
        logger.log("Adding worklog as admin");

        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("log-work");
        tester.setFormElement("timeLogged", ADMIN_TIME_WORKED);
        tester.setFormElement("startDate", ADMIN_TIME_PERFORMED);
        tester.getDialog().setFormParameter("comment", ADMIN_COMMENT);
        tester.submit("Log");
        navigation.logout();

        logger.log("Adding worklog as fred");
        navigation.login(FRED, FRED);
        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("log-work");
        tester.setFormElement("timeLogged", FRED_TIME_WORKED);
        tester.setFormElement("startDate", FRED_TIME_PERFORMED);
        tester.getDialog().setFormParameter("comment", FRED_COMMENT);
        tester.submit("Log");
        navigation.logout();

        logger.log("Logging back in as admin");
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }


    private void grantPermission(String username, ProjectPermissionKey permission) {
        navigation.gotoAdmin();
        backdoor.permissionSchemes().addUserPermission(DEFAULT_PERM_SCHEME_ID, permission, username);
    }
}
