package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.issue.TestCloneIssueWithSubTasks;
import com.atlassian.jira.webtests.ztests.issue.subtasks.TestMoveSubtask;
import com.atlassian.jira.webtests.ztests.subtask.TestClosedParent;
import com.atlassian.jira.webtests.ztests.subtask.TestCreateSubTasks;
import com.atlassian.jira.webtests.ztests.subtask.TestCreateSubTasksAndWorklog;
import com.atlassian.jira.webtests.ztests.subtask.TestCreateSubTasksContextPermission;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubTaskConversionParentPicker;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubTaskConversionSecurityLevel;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubTaskConversionStep1;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubTaskConversionStep2;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubTaskConversionSystemFields;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubTaskConversionVariousOperations;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubTaskConversionWithFields;
import com.atlassian.jira.webtests.ztests.subtask.TestReindexingSubtasks;
import com.atlassian.jira.webtests.ztests.subtask.TestSecurityLevelOfSubtasks;
import com.atlassian.jira.webtests.ztests.subtask.TestSubTaskActions;
import com.atlassian.jira.webtests.ztests.subtask.TestSubTaskProgressBar;
import com.atlassian.jira.webtests.ztests.subtask.TestSubTaskQuickCreation;
import com.atlassian.jira.webtests.ztests.subtask.TestSubTaskToIssueConversionSecurityLevel;
import com.atlassian.jira.webtests.ztests.subtask.TestSubTaskToIssueConversionStep1;
import com.atlassian.jira.webtests.ztests.subtask.move.TestMoveSubTask;
import com.atlassian.jira.webtests.ztests.subtask.move.TestMoveSubTaskEnterprise;
import com.atlassian.jira.webtests.ztests.subtask.move.TestMoveSubTaskIssueType;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of SubTask related test
 *
 * @since v4.0
 */
public class FuncTestSuiteSubTasks {

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestSubTaskActions.class)
                .add(TestSubTaskProgressBar.class)
                .add(TestCreateSubTasks.class)
                .add(TestSubTaskQuickCreation.class)
                .add(TestMoveSubTask.class)
                .add(TestMoveSubTaskIssueType.class)

                .add(TestCreateSubTasksAndWorklog.class)
                .add(TestCreateSubTasksContextPermission.class)

                .add(TestIssueToSubTaskConversionSystemFields.class)
                .add(TestIssueToSubTaskConversionVariousOperations.class)
                .add(TestIssueToSubTaskConversionStep1.class)
                .add(TestIssueToSubTaskConversionStep2.class)
                .add(TestIssueToSubTaskConversionWithFields.class)
                .add(TestIssueToSubTaskConversionParentPicker.class)
                .add(TestIssueToSubTaskConversionSecurityLevel.class)
                .add(TestSubTaskToIssueConversionStep1.class)
                .add(TestSubTaskToIssueConversionSecurityLevel.class)

                .add(TestCloneIssueWithSubTasks.class)

                .add(TestReindexingSubtasks.class)
                .add(TestSecurityLevelOfSubtasks.class)
                .add(TestMoveSubTaskEnterprise.class)
                .add(TestMoveSubtask.class)
                .add(TestClosedParent.class)
                .build();
    }
}