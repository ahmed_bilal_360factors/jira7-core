package com.atlassian.jira.webtests.ztests.navigator;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.framework.util.Searcher;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.webtests.CustomFieldValue;
import com.google.common.collect.Lists;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Test;
import org.w3c.dom.Document;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_PREFIX;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_CHECKBOX;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_DATEPICKER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_MULTISELECT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_RADIO;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_SELECT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_TEXTFIELD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_USERPICKER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.REPORTER_FIELD_ID;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@WebTest({Category.FUNC_TEST, Category.ISSUE_NAVIGATOR, Category.ISSUES})
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueNavigator extends BaseJiraFuncTest {

    private static final String CUSTOM_FIELD_GLOBAL = "global custom field";
    private static final String CUSTOM_FIELD_ISSUETYPE = "issueType only custom field";
    private static final String CUSTOM_FIELD_PROJECT = "project only custom field";
    private static final String CUSTOM_FIELD_ISSUETYPE_AND_PROJECT = "issue type & project custom field";
    private static final String PROJECT_DOG = "dog";
    private static final String PROJECT_HOMOSAP = "homosapien";
    private static final String homosapId = "10000";
    private static final Long homosapIdLong = 1000L;

    private static final String GROUP_NAME = "test group";

    @SuppressWarnings("unchecked")
    private static final List<CustomFieldValue>[] cfValuesPerIssue = new ArrayList[]{new ArrayList<CustomFieldValue>(), new ArrayList<CustomFieldValue>(), new ArrayList<CustomFieldValue>()};

    private static final String issueKey = "HSP-1";
    private static final String issueKey2 = "HSP-2";
    private static final String issueKey3 = "HSP-3";
    private static final String issueKey4 = "HSP-4";

    private static final String customFieldIdSelectList = "10000";
    private static final String customFieldIdRadioButton = "10001";
    private static final String customFieldIdMultiSelect = "10002";
    private static final String customFieldIdCheckBox = "10003";
    private static final String customFieldIdTextField = "10004";
    private static final String customFieldIdUserPicker = "10005";
    private static final String customFieldIdDatePicker = "10006";

    private static final String CUSTOM_FIELD_SELECT = "Custom Field Select";
    private static final String CUSTOM_FIELD_RADIO = "Custom Field Radio";
    private static final String CUSTOM_FIELD_MULTI_SELECT = "Custom Field Multi Select";
    private static final String CUSTOM_FIELD_TEXTFIELD = "Custom Field Text Field";
    private static final String CUSTOM_FIELD_CHECKBOX = "Custom Field Check Box";
    private static final String CUSTOM_FIELD_USERPICKER = "Custom Field User Picker";
    private static final String CUSTOM_FIELD_DATEPICKER = "Custom Field Date Picker";

    private static final String[] customFieldNames = new String[]{CUSTOM_FIELD_SELECT, CUSTOM_FIELD_RADIO, CUSTOM_FIELD_TEXTFIELD, CUSTOM_FIELD_MULTI_SELECT,
            CUSTOM_FIELD_CHECKBOX, CUSTOM_FIELD_USERPICKER, CUSTOM_FIELD_DATEPICKER};
    private static final String[] customFieldIds = new String[]{customFieldIdSelectList, customFieldIdRadioButton,
            customFieldIdTextField, customFieldIdMultiSelect,
            customFieldIdCheckBox, customFieldIdUserPicker,
            customFieldIdDatePicker};

    @Inject
    private FuncTestLogger logger;

    @Inject
    private IssueTableAssertions issueTableAssertions;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    @Restore("TestIssueNavigatorSubtaskColumnView.xml")
    public void testSubtaskIssueNavigatorColumn() {
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.issueNavigator().createSearch("");
        textAssertions.assertTextPresent("HSP-5");
        textAssertions.assertTextPresent("HSP-6");
        logger.log("Successfully found subtask issue keys in the subtask issue navigator column");
    }

    @Test
    @Restore("TestIssueNavigatorColumnVisibilityForCustomFields.xml")
    public void testNavigatorColumnVisibilityForCustomFields() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
        final String dogProjectId = "10010";

        // run some issue navigator tests and make sure the columns look right
        // since the user has all the custom fields on its column layout, the custom fields will show up in all his searches
        navigation.issueNavigator().displayAllIssues();
        assertTextInIssueTable(PROJECT_HOMOSAP + " cf");
        assertTextInIssueTable(PROJECT_HOMOSAP + " bug cf");
        assertTextInIssueTable(PROJECT_DOG + " cf");
        assertTextInIssueTable(PROJECT_DOG + " bug cf");

        // search only homosap
        navigation.issueNavigator().createSearch("project=" + PROJECT_HOMOSAP);
        assertTextInIssueTable(PROJECT_HOMOSAP + " cf");
        assertTextInIssueTable(PROJECT_HOMOSAP + " bug cf");
        assertTextInIssueTable(PROJECT_DOG + " cf");
        assertTextInIssueTable(PROJECT_DOG + " bug cf");

        // search homosap bugs
        navigation.issueNavigator().createSearch("project=" + PROJECT_HOMOSAP + " AND issuetype= Bug");
        assertTextInIssueTable(PROJECT_HOMOSAP + " cf");
        assertTextInIssueTable(PROJECT_HOMOSAP + " bug cf");
        assertTextInIssueTable(PROJECT_DOG + " cf");
        assertTextInIssueTable(PROJECT_DOG + " bug cf");

        // search only dog
        navigation.issueNavigator().createSearch("project=" + PROJECT_DOG);
        assertTextInIssueTable(PROJECT_HOMOSAP + " cf");
        assertTextInIssueTable(PROJECT_HOMOSAP + " bug cf");
        assertTextInIssueTable(PROJECT_DOG + " cf");
        assertTextInIssueTable(PROJECT_DOG + " bug cf");

        // search dog bugs
        navigation.issueNavigator().createSearch("project=" + PROJECT_DOG + " AND issuetype= Bug");
        assertTextInIssueTable(PROJECT_HOMOSAP + " cf");
        assertTextInIssueTable(PROJECT_HOMOSAP + " bug cf");
        assertTextInIssueTable(PROJECT_DOG + " cf");
        assertTextInIssueTable(PROJECT_DOG + " bug cf");

        // search both projects
        navigation.issueNavigator().createSearch("project in (" + dogProjectId + ", " + homosapId + ")");
        assertTextInIssueTable(PROJECT_HOMOSAP + " cf");
        assertTextInIssueTable(PROJECT_HOMOSAP + " bug cf");
        assertTextInIssueTable(PROJECT_DOG + " cf");
        assertTextInIssueTable(PROJECT_DOG + " bug cf");

        // search both projects and bugs
        navigation.issueNavigator().createSearch("project in (" + dogProjectId + ", " + homosapId + ") AND issuetype= Bug");
        assertTextInIssueTable(PROJECT_HOMOSAP + " cf");
        assertTextInIssueTable(PROJECT_HOMOSAP + " bug cf");
        assertTextInIssueTable(PROJECT_DOG + " cf");
        assertTextInIssueTable(PROJECT_DOG + " bug cf");
    }

    /**
     * Tests to check if the Status searcher is displayed when the SearchContext is made invalid by specifying bad
     * Project IDs via the URL. Note that, if at least one Project ID is valid, then Status should be visible
     */
    @Test
    @Restore("TestIssueNavigatorProjectComponentsVisibility.xml")
    public void testJQLWarningShownForInvalidProject() {
        logger.log("Issue Navigator: Test project componenets visibility");

        // specify one invalid PID - statuses should not appear

        issueTableAssertions.assertSearchWithError("project=99999", "A value with ID '99999' does not exist for the field 'project'.");

        // specify two invalid PIDs - statuses should not appear
        issueTableAssertions.assertSearchWithErrors("project=99999 AND project=88888", Lists.newArrayList("A value with ID '99999' does not exist for the field 'project'.", "A value with ID '88888' does not exist for the field 'project'."));

        // specify two invalid PIDs and one valid PID - statuses should appear
        issueTableAssertions.assertSearchWithErrors("project=99999 AND project=88888 AND project=10010", Lists.newArrayList("A value with ID '99999' does not exist for the field 'project'.", "A value with ID '88888' does not exist for the field 'project'."));
    }

    /**
     * Tests custom fields visibility on the issue navigator
     */
    @Test
    @Restore("TestIssueNavigatorCustomfieldVisibility.xml")
    public void testCustomfieldVisibility() {
        List<Searcher> searchers = backdoor.searchersClient().allSearchers("");
        assertSearcherPresent(searchers, CUSTOM_FIELD_GLOBAL);
        assertSearcherNotPresent(searchers, CUSTOM_FIELD_ISSUETYPE);
        assertSearcherNotPresent(searchers, CUSTOM_FIELD_PROJECT);
        assertSearcherNotPresent(searchers, CUSTOM_FIELD_ISSUETYPE_AND_PROJECT);

        searchers = backdoor.searchersClient().allSearchers("type=bug");
        assertSearcherPresent(searchers, CUSTOM_FIELD_GLOBAL);
        assertSearcherPresent(searchers, CUSTOM_FIELD_ISSUETYPE);
        assertSearcherNotPresent(searchers, CUSTOM_FIELD_PROJECT);
        assertSearcherNotPresent(searchers, CUSTOM_FIELD_ISSUETYPE_AND_PROJECT);

        searchers = backdoor.searchersClient().allSearchers("project=" + homosapId);
        assertSearcherPresent(searchers, CUSTOM_FIELD_GLOBAL);
        assertSearcherNotPresent(searchers, CUSTOM_FIELD_ISSUETYPE);
        assertSearcherPresent(searchers, CUSTOM_FIELD_PROJECT);
        assertSearcherNotPresent(searchers, CUSTOM_FIELD_ISSUETYPE_AND_PROJECT);

        searchers = backdoor.searchersClient().allSearchers("project=" + homosapId + " and type=bug");
        assertSearcherPresent(searchers, CUSTOM_FIELD_GLOBAL);
        assertSearcherPresent(searchers, CUSTOM_FIELD_ISSUETYPE);
        assertSearcherPresent(searchers, CUSTOM_FIELD_PROJECT);
        assertSearcherPresent(searchers, CUSTOM_FIELD_ISSUETYPE_AND_PROJECT);
    }

    private void assertSearcherPresent(List<Searcher> searchers, String name) {
        for (Searcher searcher : searchers) {
            if (name.equals(searcher.getName())) {
                assertTrue("Expected searcher with name " + name + " is shown", searcher.getShown());
                return;
            }
        }
        fail("Searcher with name " + name + " expected in response");
    }

    private void assertSearcherNotPresent(List<Searcher> searchers, String name) {
        searchers.stream()
                .filter(searcher -> name.equals(searcher.getName()))
                .forEach(searcher ->
                        assertFalse("Expected searcher with name " + name + " is not shown", searcher.getShown())
                );
    }

    @Test
    @Restore("TestIssueNavigatorCommon.xml")
    public void testIssueNavigatorSortByCustomField() {
        logger.log("Issue Navigator: Test that the filter correctly orders issues for custom fields.");
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        navigation.issueNavigator().addColumnToIssueNavigator(customFieldNames);

        navigation.issueNavigator().displayAllIssues();//make sure there's a current search request
        for (int i = 0; i < customFieldIds.length; i++) {
            logger.log("Sorting by " + customFieldNames[i]);
            navigation.issueNavigator().sortIssues("cf[" + customFieldIds[i] + "]", "ASC");
            textAssertions.assertTextSequence(new IdLocator(tester, "issuetable"), new String[]{issueKey, issueKey2, issueKey3, issueKey4,});

            navigation.issueNavigator().sortIssues("cf[" + customFieldIds[i] + "]", "DESC");
            textAssertions.assertTextSequence(new IdLocator(tester, "issuetable"), new String[]{issueKey4, issueKey3, issueKey2, issueKey,});
        }
        navigation.issueNavigator().restoreColumnDefaults();
    }

    @Test
    @Restore("TestIssueNavigatorCommon.xml")
    public void testIssueNavigatorSortByComponent() {
        logger.log("Issue Navigator: Test that the filter correctly orders issues for components.");
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        tester.gotoPage("/issues/?jql=ORDER%20BY%20component%20ASC%2C%20key%20ASC");
        textAssertions.assertTextSequence(new IdLocator(tester, "issuetable"), new String[]{issueKey4, issueKey2, issueKey, issueKey3});

        tester.gotoPage("/issues/?jql=ORDER%20BY%20component%20DESC%2C%20key%20DESC");
        textAssertions.assertTextSequence(new IdLocator(tester, "issuetable"), new String[]{issueKey3, issueKey, issueKey2, issueKey4});
    }

    @Test
    @Restore("TestIssueNavigatorCommon.xml")
    public void testIssueNavigatorHideReporter() {
        logger.log("Issue Navigator: Test that the filter correctly hides the reporter field with full content view.");
        administration.fieldConfigurations().defaultFieldConfiguration().hideFields(REPORTER_FIELD_ID);
        navigation.gotoFullContentView("");
        textAssertions.assertTextNotPresent("Reporter");
        tester.clickLinkWithText("test issue 1");

        administration.fieldConfigurations().defaultFieldConfiguration().showFields(REPORTER_FIELD_ID);
        navigation.gotoFullContentView("");
        textAssertions.assertTextPresent("Reporter");
        tester.clickLinkWithText("test issue 1");
    }

    @Test
    @Restore("TestIssueNavigatorCommon.xml")
    public void testIssueNavigatorSelectGroup() {
        logger.log("Issue Navigator: Test that all issues are filtered for a specific group");
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
        backdoor.usersAndGroups().addGroup(GROUP_NAME);
        backdoor.usersAndGroups().addUserToGroup(BOB_USERNAME, GROUP_NAME);
        final String testIssueKey = navigation.issue().createIssue(PROJECT_HOMOSAP, "Bug", "test issue 5");

        backdoor.darkFeatures().enableForSite("no.frother.assignee.field");
        try {
            navigation.issue().assignIssue(testIssueKey, "Assign to Bob", BOB_FULLNAME);
        } finally {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field");
        }

        issueTableAssertions.assertSearchWithResults("assignee in membersOf(\"" + GROUP_NAME + "\")", testIssueKey, issueKey3, issueKey);

        navigation.issue().deleteIssue(testIssueKey);
        administration.usersAndGroups().deleteGroup(GROUP_NAME);
    }

    @Test
    @Restore("TestIssueNavigatorCommon.xml")
    public void testIssueNavigatorXMLViewWithCustomFields() throws Exception {
        logger.log("Issue Navigator: Test that the RSS page correctly shows the custom field information.");
        navigation.issueNavigator().addColumnToIssueNavigator(customFieldNames);
        navigation.gotoXmlView("");

        textAssertions.assertTextPresent("An XML representation of a search request");
        textAssertions.assertTextPresent("[" + issueKey + "] test issue 1");

        final Document doc = XMLUnit.buildControlDocument(tester.getDialog().getResponse().getText());
        for (final List<CustomFieldValue> values : cfValuesPerIssue) {
            for (final CustomFieldValue customFieldValue : values) {
                // Not testing the DatePicker because I don't know what format Jira has put the value into
                if (!customFieldValue.getCfType().equals(CUSTOM_FIELD_TYPE_DATEPICKER)) {
                    logger.log("Searching for existence of xpath " + "//item/customfields/customfield[@id='" + CUSTOM_FIELD_PREFIX + customFieldValue.getCfId() + "'][customfieldname='" + getCustomFieldNameFromType(customFieldValue.getCfType()) + "'][customfieldvalues[customfieldvalue='" + customFieldValue.getCfValue() + "']]");
                    XMLAssert.assertXpathExists("//item/customfields/customfield[@id='" + CUSTOM_FIELD_PREFIX + customFieldValue.getCfId() + "'][customfieldname='" + getCustomFieldNameFromType(customFieldValue.getCfType()) + "'][customfieldvalues[customfieldvalue='" + customFieldValue.getCfValue() + "']]", doc);
                }
            }
        }

    }

    private String getCustomFieldNameFromType(final String type) {
        switch (type) {
            case CUSTOM_FIELD_TYPE_SELECT:
                return CUSTOM_FIELD_SELECT;
            case CUSTOM_FIELD_TYPE_RADIO:
                return CUSTOM_FIELD_RADIO;
            case CUSTOM_FIELD_TYPE_MULTISELECT:
                return CUSTOM_FIELD_MULTI_SELECT;
            case CUSTOM_FIELD_TYPE_CHECKBOX:
                return CUSTOM_FIELD_CHECKBOX;
            case CUSTOM_FIELD_TYPE_TEXTFIELD:
                return CUSTOM_FIELD_TEXTFIELD;
            case CUSTOM_FIELD_TYPE_USERPICKER:
                return CUSTOM_FIELD_USERPICKER;
            case CUSTOM_FIELD_TYPE_DATEPICKER:
                return CUSTOM_FIELD_DATEPICKER;
            default:
                return null;
        }
    }

    //------------------------------------------------------------------- testBackToPreviousViewLinks helper methods END

    /**
     * Tests that the sorting order of the issue navigator is correct and does not crash
     * as in JRA-12974.
     */
    @Test
    @Restore("TestIssueNavigatorCommon.xml")
    public void testNavigatorOrdering() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        tester.gotoPage("/issues/?jql=ORDER%20BY%20summary%20ASC");
        textAssertions.assertTextSequence(new IdLocator(tester, "issuetable"), new String[]{
                "test issue 1",
                "test issue 2",
                "test issue 3",
                "test issue 4",
        });

        tester.gotoPage("/issues/?jql=ORDER%20BY%20assignee%20ASC");
        textAssertions.assertTextSequence(new IdLocator(tester, "issuetable"), new String[]{
                ADMIN_FULLNAME,
                ADMIN_FULLNAME,
                BOB_FULLNAME,
                BOB_FULLNAME,
        });

        tester.gotoPage("/issues/?jql=ORDER%20BY%20summary%20ASC%2C%20duedate%20ASC");
        // what can we test here if nothing is showing?
        textAssertions.assertTextSequence(new IdLocator(tester, "issuetable"), new String[]{
                "test issue 1",
                "test issue 2",
                "test issue 3",
                "test issue 4",
        });

        tester.gotoPage("/issues/?jql=ORDER%20BY%20summary%20ASC%2C%20workratio%20ASC");
        // test that the page doest crash?
        textAssertions.assertTextSequence(new IdLocator(tester, "issuetable"), new String[]{
                "test issue 1",
                "test issue 2",
                "test issue 3",
                "test issue 4",
        });
    }

    @Test
    @Restore("TestIssueNavigatorCommon.xml")
    public void testSearchSortDescriptionForInvalidField() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        long id = Long.parseLong(backdoor.filters().createFilter("ORDER BY cf[10006] ASC", "My Test Filter"));

        tester.gotoPage("/issues/?filter=" + id);
        textAssertions.assertTextSequence(new IdLocator(tester, "issuetable"), new String[]{
                "test issue 1",
                "test issue 2",
                "test issue 3",
                "test issue 4",
        });

        // delete the custom field
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        tester.clickLink("del_customfield_10006");
        tester.submit("Delete");

        // redisplay the filter
        tester.gotoPage("/issues/?filter=" + id);
        tester.assertElementNotPresent("issuetable");
    }

    @Test
    @Restore("TestIssueNavigatorNoColumns.xml")
    public void testNoColumnsDialog() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        navigation.issueNavigator().displayAllIssues();

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(),
                new String[]{"No columns selected"});
    }

    // JRA-20241
    @Test
    @Restore("TestIssueNavigatorTextWithDotAndColon.xml")
    public void testCanSearchForTextWithDotAndColon() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        navigation.issueNavigator().createSearch("description ~ \"d.dude:123\" order by key desc");
        assertions.getIssueNavigatorAssertions().assertExactIssuesInResults("TST-1");
        // Check that the aliases are there as well
        navigation.issueNavigator().createSearch("description ~ \"dude\" order by key asc");
        assertions.getIssueNavigatorAssertions().assertExactIssuesInResults("TST-1", "TST-2");
    }

    // JRA-14238
    @Test
    @Restore("TestImageUrlXss.xml")
    public void testXssInImageUrls() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
        navigation.issueNavigator().createSearch("");

        // priority icon URL
        tester.assertTextNotPresent("\"'/><script>alert('prioritiezz');</script>");

        // issue type icon URL
        tester.assertTextNotPresent("\"'/><script>alert('issue typezz');</script>");
    }

    private void assertTextInIssueTable(String pagetext) {
        textAssertions.assertTextPresent(new XPathLocator(tester, "//table[@id='issuetable']"), pagetext);
    }
}
