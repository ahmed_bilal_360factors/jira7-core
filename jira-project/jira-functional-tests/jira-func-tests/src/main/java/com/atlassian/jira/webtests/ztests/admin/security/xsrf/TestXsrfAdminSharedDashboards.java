package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Dashboard;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.dashboard.DashboardPageInfo;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.Permissions;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.sharing.TestSharingPermissionUtils.createPublicPermissions;
import static com.google.common.collect.Iterables.getFirst;

/**
 * Responsible for holding tests which verify that the Shared Dashboards Administration actions are not susceptible to
 * XSRF attacks.
 *
 * @since v6.0.2
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestXsrfAdminSharedDashboards extends BaseJiraFuncTest {
    @Inject
    protected Administration administration;
    @Inject
    private Form form;

    private Dashboard addSharedPublicDashboard() {
        return navigation.dashboard().addPage(Data.SHARED_PUBLIC_DASHBOARD, null);
    }

    @Test
    public void testSharedDasboardOperations() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck(
                        "Delete Shared Dashboard",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                addSharedPublicDashboard();
                                backdoor.permissions().addGlobalPermission(Permissions.CREATE_SHARED_OBJECT, "jira-users");
                                final Long dashboardId = getFirst(backdoor.dashboard().getOwnedDashboard("admin"), null).getId();
                                administration.sharedDashboards().goTo();
                                tester.clickLink("delete_" + dashboardId);
                                tester.setWorkingForm("delete-portal-page");
                            }
                        },
                        new XsrfCheck.FormSubmissionWithId("delete-portal-page-submit")
                )
        ).
                run(tester, navigation, form);
    }

    private static class Data {
        private static final DashboardPageInfo SHARED_PUBLIC_DASHBOARD =
                new DashboardPageInfo
                        (
                                10014, "Public - Owner: Admin", null, true, createPublicPermissions(), ADMIN_USERNAME,
                                1, DashboardPageInfo.Operation.ALL
                        );
    }
}
