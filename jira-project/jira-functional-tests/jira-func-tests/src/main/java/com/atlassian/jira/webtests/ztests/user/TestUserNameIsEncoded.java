package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUserNameIsEncoded extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testUserNamesAreHtmlEncoded() throws SAXException {
        final String brokenUsername = "&quot;my &lt;input&gt; name";
        administration.restoreData("TestUsernameIsEncoded.xml");
        backdoor.darkFeatures().enableForSite("no.frother.assignee.field");

        navigation.issue().gotoIssue("HSP-1");
        textAssertions.assertTextPresent(locator.page(), "Is my broken-name really bad?");

        // assert that all four links are properly encoded
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Assignee:", brokenUsername);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Reporter:", brokenUsername);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "field1:", brokenUsername);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "field2:", brokenUsername);

        // check the correct values appear on the edit screen
        tester.clickLink("edit-issue");
        textAssertions.assertTextPresent(locator.page(), "Edit Issue");
        tester.selectOption("assignee", "\"my <input> name");
        tester.assertFormElementEquals("reporter", "broken");
        tester.assertFormElementEquals("customfield_10000", "broken");
        tester.assertFormElementEquals("customfield_10001", "admin, broken");
    }
}
