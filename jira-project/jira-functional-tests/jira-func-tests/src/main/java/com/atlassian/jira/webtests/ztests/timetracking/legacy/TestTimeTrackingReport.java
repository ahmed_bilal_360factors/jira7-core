package com.atlassian.jira.webtests.ztests.timetracking.legacy;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.meterware.httpunit.WebTable;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static com.atlassian.jira.config.properties.APKeys.JIRA_OPTION_TIMETRACKING;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Format.DAYS;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Format.HOURS;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Format.PRETTY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@WebTest({Category.FUNC_TEST, Category.REPORTS, Category.TIME_TRACKING})
@LoginAs(user = ADMIN_USERNAME)
public class TestTimeTrackingReport extends BaseJiraFuncTest {
    /**
     * Dash character used to represent negative, won't let a line break in between it and the following character.
     * in html we use &#8209;
     */
    private static final char NEG = 8209;

    /**
     * Captial sigma as defined in JiraWebActionSupport.properties: common.concepts.sum
     */
    private static final String SIGMA = "&Sigma;";
    private static final int PID_MONOTREME = 10010;
    private static final int PID_HOMOSAPIEN = 10000;
    private static final Long PROJECT_MONKEY_ID = (long) 10001;
    @Inject
    private Administration administration;
    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testTimeTrackingReportAvailable() {
        administration.restoreData("TestTimeTrackingReport.xml");
        //go directly to the report URL without specifying a project id (and nothing in session) should fail
        tester.gotoPage(Urls.TIMETRACKING_REPORT_DEFAULTS);
        tester.assertTextPresent("The selected project does not exist, or you do not have permission to view it.");

        //go directly to the report URL with a specified project id (and nothing in session)
        //it should show the report and add the id to the session
        tester.gotoPage(Urls.TIMETRACKING_REPORT_DEFAULTS + "&selectedProjectId=10000");
        tester.assertTextNotPresent("The selected project does not exist, or you do not have permission to view it.");
        tester.assertTextPresent("Time Tracking Report for&nbsp;" + PROJECT_HOMOSAP);
        //browse to the report (project id should have been set in the session already)
        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=" + 10000 + "&reportKey=com.atlassian.jira.jira-core-reports-plugin:time-tracking");
        tester.submit("Next");
        tester.assertTextPresent("Time Tracking Report for&nbsp;" + PROJECT_HOMOSAP);

        //go directly to a report with no project id (so it retrieves from session)
        tester.gotoPage(Urls.TIMETRACKING_REPORT_DEFAULTS);
        tester.assertTextPresent("Time Tracking Report for&nbsp;" + PROJECT_HOMOSAP);
    }

    @Test
    public void testTimeTrackingReportSwitchingBetweenProjects() {
        administration.restoreData("TestTimeTrackingReport.xml");
        tester.gotoPage(Urls.TIMETRACKING_REPORT_DEFAULTS + "&selectedProjectId=10000");
        tester.assertTextPresent("Time Tracking Report for&nbsp;" + PROJECT_HOMOSAP);

        //go directly to a report for another project
        tester.gotoPage(Urls.TIMETRACKING_REPORT_DEFAULTS + "&selectedProjectId=10001");
        tester.assertTextPresent("Time Tracking Report for&nbsp;" + PROJECT_MONKEY);
    }

    @Test
    public void testTimeTrackingReportShowsSubTaskSelector() {
        administration.restoreData("TestTimeTrackingReport.xml");
        tester.gotoPage(Urls.TIMETRACKING_REPORT_DEFAULTS + "&selectedProjectId=10000");
        tester.assertTextNotPresent("Sub-task Inclusion");

        administration.subtasks().enable();
        gotoReportConfig(PID_HOMOSAPIEN);
        tester.assertTextPresent("Sub-task Inclusion");
        tester.selectOption("subtaskInclusion", "Only including sub-tasks with the selected version");
        tester.selectOption("subtaskInclusion", "Also including sub-tasks without a version set");
        tester.selectOption("subtaskInclusion", "Including all sub-tasks");
        tester.assertRadioOptionValueNotPresent("subtaskInclusion", "Sub-tasks are not enabled");
    }

    @Test
    public void testSubTaskAggregatesWithNoSubtasksDisplayed() throws SAXException {
        administration.restoreData("TestTimeTrackingReport.xml");

        administration.subtasks().enable();
        gotoReportConfig(PID_HOMOSAPIEN);
        // defaults
        tester.checkCheckbox("subtaskInclusion", "all");
        tester.checkCheckbox("sortingOrder", "least");
        tester.checkCheckbox("completedFilter", "all");
        tester.submit("Next");
        tester.assertTextPresent("Including all sub-tasks");
        tester.assertTextPresent(SIGMA);

        final WebTable barsSummary = tester.getDialog().getWebTableBySummaryOrId("bars-summary");
        assertions.getTableAssertions().assertTableCellHasText(barsSummary, 0, 0, "Progress: 39%");
        assertions.getTableAssertions().assertTableCellHasText(barsSummary, 1, 0, "Accuracy: 0%");

        final WebTable table = tester.getDialog().getResponse().getTableWithID("timeReport");
        List rowList = getTableRowAsList(table, 1);
        String[] row = (String[]) rowList.toArray(new String[rowList.size()]);

        assertTrue(row[3].contains("massive bug"));
        assertEquals("1w 3d", row[4]);
        assertEquals("1w 3d", row[5]);
        assertEquals("5d 19h 30m", row[6]);
        assertEquals("5d 19h 30m", row[7]);
        assertEquals("4d 4h 30m", row[8]);
        assertEquals("4d 4h 30m", row[9]);
        assertEquals("on track", row[10]);
        assertEquals("on track", row[11]);

        rowList = getTableRowAsList(table, 2);
        row = (String[]) rowList.toArray(new String[rowList.size()]);
        assertTrue(row[3].contains("bug2"));
        assertEquals("1d", row[4]);
        assertEquals("1d", row[5]);
        assertEquals("20h 30m", row[6]);
        assertEquals("20h 30m", row[7]);
        assertEquals("3h 30m", row[8]);
        assertEquals("3h 30m", row[9]);
        assertEquals("on track", row[10]);
        assertEquals("on track", row[11]);

        // TOTALS ROW
        rowList = getTableRowAsList(table, 3);
        row = (String[]) rowList.toArray(new String[rowList.size()]);
        assertEquals("Total", row[3]);
        assertEquals("1w 4d", row[4]);
        assertEquals("6d 16h", row[6]);
        assertEquals("4d 8h", row[8]);
        assertEquals("on track", row[10]);
    }

    @Test
    public void testSubTaskAggregates() throws SAXException {
        administration.restoreData("TestTimeTrackingReportWithSubtasksEnterprise.xml");

        gotoReportConfig(PID_MONOTREME);
        tester.checkCheckbox("versionId", "10010"); // v8
        tester.checkCheckbox("subtaskInclusion", "all");
        tester.checkCheckbox("sortingOrder", "least");
        tester.checkCheckbox("completedFilter", "all");
        tester.submit("Next");

        tester.assertTextPresent("Time Tracking Report for&nbsp;Monotreme");
        tester.assertTextPresent("(v8)");
        tester.assertTextPresent("Including all sub-tasks");

        final WebTable table = tester.getDialog().getResponse().getTableWithID("timeReport");
        List rowList = getTableRowAsList(table, 1);
        String[] row = (String[]) rowList.toArray(new String[rowList.size()]);
        assertTrue(row[3].contains("monotreme parent with fixfor v8"));
        assertEquals("-", row[4]);
        assertEquals("7h 10m", row[5]);
        assertEquals("-", row[6]);
        assertEquals("6h 10m", row[7]);
        assertEquals("-", row[8]);
        assertEquals("5h", row[9]);
        assertEquals("-", row[10]);
        assertEquals(NEG + "4h", row[11]);

        rowList = getTableRowAsList(table, 2);
        row = (String[]) rowList.toArray(new String[rowList.size()]);
        assertTrue(row[3].contains("subtask with no fixfor and parent fixfor v8"));
        assertEquals("6h", row[4]);
        assertEquals("-", row[5]);
        assertEquals("6h", row[6]);
        assertEquals("-", row[7]);
        assertEquals("4h", row[8]);
        assertEquals("-", row[9]);
        assertEquals(NEG + "4h", row[10]);
        assertEquals("-", row[11]);

        rowList = getTableRowAsList(table, 3);
        row = (String[]) rowList.toArray(new String[rowList.size()]);
        assertTrue(row[3].contains("subtask with fixfor v8 same as parent"));
        assertEquals("10m", row[4]);
        assertEquals("-", row[5]);
        assertEquals("10m", row[6]);
        assertEquals("-", row[7]);
        assertEquals("-", row[8]);
        assertEquals("-", row[9]);
        assertEquals("on track", row[10]);
        assertEquals("-", row[11]);

        rowList = getTableRowAsList(table, 4);
        row = (String[]) rowList.toArray(new String[rowList.size()]);
        assertTrue(row[3].contains("subtask complete same fixfor as parent"));
        assertEquals("1h", row[4]);
        assertEquals("-", row[5]);
        assertEquals("0m", row[6]);
        assertEquals("-", row[7]);
        assertEquals("1h", row[8]);
        assertEquals("-", row[9]);
        assertEquals("on track", row[10]);
        assertEquals("-", row[11]);

        //orphan row
        rowList = getTableRowAsList(table, 5);
        row = (String[]) rowList.toArray(new String[rowList.size()]);
        assertTrue(row[3].contains("MON-5"));
        assertTrue(row[3].contains("v12 subtask with v8 fixfor"));
        assertEquals("44m", row[4]);
        assertEquals("44m", row[5]);
        assertEquals("23m", row[6]);
        assertEquals("23m", row[7]);
        assertEquals("21m", row[8]);
        assertEquals("21m", row[9]);
        assertEquals("on track", row[10]);
        assertEquals("on track", row[11]);

        // TOTALS ROW
        rowList = getTableRowAsList(table, 6);
        row = (String[]) rowList.toArray(new String[rowList.size()]);
        assertEquals("Total", row[3]);
        assertEquals("7h 54m", row[4]);
        assertEquals("6h 33m", row[6]);
        assertEquals("5h 21m", row[8]);
        assertEquals(NEG + "4h", row[10]);

        assertSummaryPercentages(44, -50);
        final WebTable barsSummary = tester.getDialog().getWebTableBySummaryOrId("bars-summary");
        assertions.getTableAssertions().assertTableCellHasText(barsSummary, 0, 2, "5h 21m completed from current total estimate of 11h 54m");
        assertions.getTableAssertions().assertTableCellHasText(barsSummary, 1, 2, "Issues in this version are behind the original estimate of 7h 54m by 4 hours.");
    }

    @Test
    public void testSubTaskAggregatesVersionSpecific() throws SAXException {
        administration.restoreData("TestTimeTrackingReportWithSubtasksEnterprise.xml");

        gotoReportConfig(PID_MONOTREME);

        tester.checkCheckbox("versionId", "10010"); // v8
        tester.checkCheckbox("subtaskInclusion", "onlySelected");
        tester.checkCheckbox("sortingOrder", "most");
        tester.checkCheckbox("completedFilter", "all");
        tester.submit("Next");

        tester.assertTextPresent("Time Tracking Report for&nbsp;Monotreme");
        tester.assertTextPresent("(v8)");
        tester.assertTextPresent("Only including sub-tasks with the selected version");

        assertTimeReportCell(1, 3, "monotreme parent with fixfor v8");
        assertTimeReportCell(1, 4, "-");
        assertTimeReportCell(1, 5, "1h 10m");
        assertTimeReportCell(1, 6, "-");
        assertTimeReportCell(1, 7, "10m");
        assertTimeReportCell(1, 8, "-");
        assertTimeReportCell(1, 9, "1h");
        assertTimeReportCell(1, 10, "-");
        assertTimeReportCell(1, 11, "on track");

        assertTimeReportCell(2, 3, "subtask complete same fixfor as parent");
        assertTimeReportCell(2, 4, "1h");
        assertTimeReportCell(2, 5, "-");
        assertTimeReportCell(2, 6, "0m");
        assertTimeReportCell(2, 7, "-");
        assertTimeReportCell(2, 8, "1h");
        assertTimeReportCell(2, 9, "-");
        assertTimeReportCell(2, 10, "on track");
        assertTimeReportCell(2, 11, "-");

        assertTimeReportCell(3, 3, "subtask with fixfor v8 same as parent");
        assertTimeReportCell(3, 4, "10m");
        assertTimeReportCell(3, 5, "-");
        assertTimeReportCell(3, 6, "10m");
        assertTimeReportCell(3, 7, "-");
        assertTimeReportCell(3, 8, "-");
        assertTimeReportCell(3, 9, "-");
        assertTimeReportCell(3, 10, "on track");
        assertTimeReportCell(3, 11, "-");

        // orphan row
        assertTimeReportCell(4, 3, "v12 subtask with v8 fixfor");
        assertTimeReportCell(4, 4, "44m");
        assertTimeReportCell(4, 5, "44m");
        assertTimeReportCell(4, 6, "23m");
        assertTimeReportCell(4, 7, "23m");
        assertTimeReportCell(4, 8, "21m");
        assertTimeReportCell(4, 9, "21m");
        assertTimeReportCell(4, 10, "on track");
        assertTimeReportCell(4, 11, "on track");

        assertTimeReportCell(5, 3, "Total");
        assertTimeReportCell(5, 4, "1h 54m");
        assertTimeReportCell(5, 6, "33m");
        assertTimeReportCell(5, 8, "1h 21");
        assertTimeReportCell(5, 10, "on track");

        tester.assertTextNotPresent("subtask with no fixfor and parent fixfor v8");

        assertSummaryPercentages(71, 0);
    }

    /**
     * Tests subtasks that sum with a parent that also has time tracking data.
     *
     * @throws SAXException in the event of malformed html
     */
    @Test
    public void testSubTaskAggregatesWithVersionAndBlank() throws SAXException {
        administration.restoreData("TestTimeTrackingReportWithSubtasksEnterprise.xml");
        assertSubtaskAggregatesWithVersionAndBlank();
    }

    /**
     * Tests subtasks that sum with a parent that also has time tracking data, also, add a
     * huge complete issue to the version which should not affect the report. Make the same
     * assertions as {@link #testSubTaskAggregatesWithVersionAndBlank()}.
     *
     * @throws SAXException in the event of malformed html
     */
   /* @Test
    public void testSubTaskAggregatesWithVersionAndNoVersionSubtasksAddingHugeCompleteIssue() throws SAXException {
        administration.restoreData("TestTimeTrackingReportWithSubtasksEnterprise.xml");
        backdoor.darkFeatures().enableForSite("no.frother.assignee.field");

        // now create a huge issue on the version which is complete.
        String hugeIssueKey = addIssue("Monotreme", "MON", "Bug", "huge complete issue", "Critical", null, null, new String[]{"v8"}, ADMIN_FULLNAME, "env", "huge estimates", "20w", null, null);
        administration.timeTracking().enable(MODERN);
        logWorkOnIssue(hugeIssueKey, "22w");

        assertSubtaskAggregatesWithVersionAndBlank();
    }*/
    @Test
    public void testSubTasksBothResolvedAndUnresolvedIncluded() throws Exception {
        administration.restoreData("TestTimeTrackingReportSubTasksResolvedAndUnresolved.xml");

        gotoReportConfig(10000);

        tester.checkCheckbox("versionId", "10000");
        tester.checkCheckbox("sortingOrder", "least");
        tester.checkCheckbox("completedFilter", "all");
        tester.checkCheckbox("subtaskInclusion", "all");
        tester.submit("Next");

        // assert all 10 issues are present
        for (int i = 1; i < 11; i++) {
            tester.assertTextPresent("HSP-" + i);
        }
    }

    /**
     * Main body of assertions for tests of subtask aggregates for specified and blank fixfor versions on subtasks.
     */
    private void assertSubtaskAggregatesWithVersionAndBlank() {
        gotoReportConfig(PID_MONOTREME);

        tester.checkCheckbox("versionId", "10011"); // v8
        tester.checkCheckbox("subtaskInclusion", "selectedAndBlank");
        tester.checkCheckbox("sortingOrder", "least");
        tester.checkCheckbox("completedFilter", "incomplete");
        tester.submit("Next");

        tester.assertTextPresent("Time Tracking Report for&nbsp;Monotreme");
        tester.assertTextPresent("(v12)");
        tester.assertTextPresent("Also including sub-tasks without a version set");

        assertTimeReportCell(1, 3, "v12 issue");
        assertTimeReportCell(1, 4, "1d 6h");
        assertTimeReportCell(1, 5, "1d 10h 2m");
        assertTimeReportCell(1, 6, "1d 6h");
        assertTimeReportCell(1, 7, "1d 10h 1m");
        assertTimeReportCell(1, 8, "-");
        assertTimeReportCell(1, 9, "1h 3m");
        assertTimeReportCell(1, 10, "on track");
        assertTimeReportCell(1, 11, NEG + "1h 2m");

        assertTimeReportCell(2, 3, "subtask v12");
        assertTimeReportCell(2, 4, "4h 1m");
        assertTimeReportCell(2, 5, "-");
        assertTimeReportCell(2, 6, "3h 59m");
        assertTimeReportCell(2, 7, "-");
        assertTimeReportCell(2, 8, "3m");
        assertTimeReportCell(2, 9, "-");
        assertTimeReportCell(2, 10, NEG + "1m");
        assertTimeReportCell(2, 11, "-");

        assertTimeReportCell(3, 3, "no fixfor subtask of a v12 issue");
        assertTimeReportCell(3, 4, "1m");
        assertTimeReportCell(3, 5, "-");
        assertTimeReportCell(3, 6, "2m");
        assertTimeReportCell(3, 7, "-");
        assertTimeReportCell(3, 8, "1h");
        assertTimeReportCell(3, 9, "-");
        assertTimeReportCell(3, 10, NEG + "1h 1m");
        assertTimeReportCell(3, 11, "-");

        assertTimeReportCell(4, 3, "Total");
        assertTimeReportCell(4, 4, "1d 10h 2m");
        assertTimeReportCell(4, 6, "1d 10h 1m");
        assertTimeReportCell(4, 8, "1h 3m");
        assertTimeReportCell(4, 10, NEG + "1h 2m");

        assertSummaryPercentages(2, -3);
        final WebTable barsSummary = tester.getDialog().getWebTableBySummaryOrId("bars-summary");
        assertions.getTableAssertions().assertTableCellHasText(barsSummary, 0, 2, "1h 3m completed from current total estimate of 1d 11h 4m");
        assertions.getTableAssertions().assertTableCellHasText(barsSummary, 1, 2, "Issues in this version are behind the original estimate of 1d 10h 2m by 1 hour, 2 minutes.");
    }

    @Test
    public void testPrintFormatInTimeTrackingReport() {
        administration.restoreData("TestTimeTrackingReport.xml");
        //check time tracking report of the monkey project
        administration.timeTracking().switchFormat(DAYS);
        generateTimeTrackingReport(PROJECT_MONKEY_ID);
        tester.assertTextPresent("<b>2d 16h</b> completed from current total estimate of <b>2d 19h</b>");

        administration.timeTracking().switchFormat(HOURS);
        generateTimeTrackingReport(PROJECT_MONKEY_ID);
        tester.assertTextPresent("<b>64h</b> completed from current total estimate of <b>67h</b>");

        administration.timeTracking().switchFormat(PRETTY);
        generateTimeTrackingReport(PROJECT_MONKEY_ID);
        tester.assertTextPresent("<b>2d 16h</b> completed from current total estimate of <b>2d 19h</b>");
    }

    @Test
    public void testPrettyPrintTimeTrackingReports() {
        administration.restoreData("TestTimeTrackingReport.xml");
        backdoor.darkFeatures().enableForSite("no.frother.assignee.field");

        administration.timeTracking().switchFormat(PRETTY);
        generateUserWorkloadReport((long) PID_HOMOSAPIEN, ADMIN_USERNAME);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "1", PROJECT_HOMOSAP);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "1", "5 days, 19 hours, 30 minutes");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "2", PROJECT_MONKEY);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "2", "3 hours");

        //increase the workload and see if the report is adjusted
        final String issueKey1 = navigation.issue().createIssue(PROJECT_HOMOSAP, ISSUE_TYPE_BUG, "increase HSP workload by 4 days");
        setOriginalEstimate(issueKey1, "4d");
        navigation.issue().assignIssue(issueKey1, "assigning to admin", ADMIN_FULLNAME);

        generateUserWorkloadReport((long) PID_HOMOSAPIEN, ADMIN_USERNAME);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "2", PROJECT_HOMOSAP); //there are 2 assigned issues
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "2", "1 week, 2 days, 19 hours, 30 minutes"); //workload is increased by 4days
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "2", PROJECT_MONKEY); //monkey should be the same
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "2", "3 hours");

        navigation.issue().logWork(issueKey1, "9d");//original estimate is 4 days so is can only reduce workload by 4 days
        generateUserWorkloadReport((long) PID_HOMOSAPIEN, ADMIN_USERNAME);
        tester.assertTextPresent("5 days, 19 hours, 30 minutes");
        tester.assertTextPresent("5 days, 22 hours, 30 minutes"); //check total

        administration.timeTracking().switchFormat(DAYS);
        generateUserWorkloadReport((long) PID_HOMOSAPIEN, ADMIN_USERNAME);
        tester.assertTextPresent("5d 19.5h"); //assert total homposaien workload
        tester.assertTextPresent("3h"); //assert total monkey workload
        tester.assertTextPresent("5d 22.5h"); //assert total workload

        //increase workload by 10 hours
        final String issueKey = navigation.issue().createIssue(PROJECT_MONKEY, ISSUE_TYPE_BUG, ISSUE_BUG);
        setOriginalEstimate(issueKey, "10d");
        generateUserWorkloadReport(PROJECT_MONKEY_ID, ADMIN_USERNAME);
        tester.assertTextPresent("5d 19.5h"); //assert total homosapien workload
        tester.assertTextPresent("10d 3h"); //assert total monkey workload
        //this number does not look like it adds up because the dates are rounded but it adds up when looking in pretty format
        tester.assertTextPresent("15d 22.5h"); //assert total workload

    }

    @Test
    public void testPrintHoursTimeTracking() {
        administration.restoreData("TestTimeTrackingReport.xml");
        administration.timeTracking().switchFormat(HOURS);
        generateUserWorkloadReport((long) PID_HOMOSAPIEN, ADMIN_USERNAME);

        tester.assertTextPresent("139.5h"); //assert homosapien workload
        tester.assertTextPresent("3h"); //asert monkey workload
        tester.assertTextPresent("142.5h"); //assert total workload

        String issueKey = navigation.issue().createIssue(PROJECT_HOMOSAP, ISSUE_TYPE_BUG, ISSUE_TYPE_BUG);
        setOriginalEstimate(issueKey, "2h 30m");
        generateUserWorkloadReport(PROJECT_MONKEY_ID, ADMIN_USERNAME);
        tester.assertTextPresent("145h"); //assert total workload increased to by 2.5h

        issueKey = navigation.issue().createIssue(PROJECT_MONKEY, ISSUE_TYPE_BUG, ISSUE_TYPE_BUG);
        setOriginalEstimate(issueKey, "5h");
        generateUserWorkloadReport(PROJECT_MONKEY_ID, ADMIN_USERNAME);
        tester.assertTextPresent("150h"); //assert workload increased by 5h

        //log 5 hours of work and see if workload has been decreased
        navigation.issue().logWork(issueKey, "5h");
        generateUserWorkloadReport(PROJECT_MONKEY_ID, ADMIN_USERNAME);
        tester.assertTextPresent("145h"); //total workload decreased by 5h
    }

    @Test
    public void testTimeTrackingReport() {
        administration.restoreData("TestTimeTrackingReport.xml");
        administration.timeTracking().switchFormat(DAYS);
        generateTimeTrackingReport((long) PID_HOMOSAPIEN);

        //check time spent
        tester.assertTextPresent("4d 8h");
        String issuekey = navigation.issue().createIssue(PROJECT_HOMOSAP, ISSUE_TYPE_BUG, ISSUE_TYPE_BUG);
        setOriginalEstimate(issuekey, "3d");
        navigation.issue().logWork(issuekey, "3d");
        generateTimeTrackingReport((long) PID_HOMOSAPIEN);
        tester.assertTextPresent("7d 8h"); //assert 3 hours more spent on issues

        administration.timeTracking().switchFormat(HOURS);
        generateTimeTrackingReport((long) PID_HOMOSAPIEN);
        tester.assertTextPresent("176h"); //time spent: 7.33 days = 176h
        tester.assertTextPresent("160h"); //time remaining

        issuekey = navigation.issue().createIssue(PROJECT_HOMOSAP, ISSUE_TYPE_BUG, ISSUE_TYPE_BUG);
        setOriginalEstimate(issuekey, "1d"); //24 hours
        generateTimeTrackingReport((long) PID_HOMOSAPIEN);
        tester.assertTextPresent("184h"); //time remaining increased by 24 hours

        navigation.issue().logWork(issuekey, "1d");
        generateTimeTrackingReport((long) PID_HOMOSAPIEN);
        tester.assertTextPresent("160h"); //worked on time for 24 hours

        administration.timeTracking().switchFormat(PRETTY);
        generateTimeTrackingReport((long) PID_HOMOSAPIEN);
        tester.assertTextPresent("1w 1d 8h"); //assert time spent
        tester.assertTextPresent("6d 16h"); //assert time remaining: 160h / 24 = 6.66667days
    }

    @Test
    public void testSingleLevelGroupByReport() {
        administration.restoreData("TestTimeTrackingReport.xml");
        // this only tests that the following page renders
        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:singlelevelgroupby");
        tester.setFormElement("filterid", "10000");
        tester.submit("Next");
        tester.assertTextPresent("Issues {all}");
    }

    @Test
    public void testVersionIsEncoded() {
        administration.restoreData("TestVersionAndComponentsWithHTMLNames.xml");
        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:time-tracking");
        try {
            tester.selectOption("versionId", "- \"version<input >");
            fail();
        } catch (final RuntimeException e) {
            assertEquals("Unable to find option - \"version<input > for versionId", e.getMessage());
        }
        tester.selectOption("versionId", "- &quot;version&lt;input &gt;");
        tester.submit("Next");
        tester.assertTextPresent("&quot;version&lt;input &gt;");
        tester.assertTextNotPresent("\"version<input >");
    }

    @Test
    public void testTimeTrackingReportNotCacheable() {
        administration.restoreData("TestTimeTrackingReport.xml");

        //go directly to the report URL with a specified project id (and nothing in session)
        //it should show the report and add the id to the session
        tester.gotoPage(Urls.TIMETRACKING_REPORT_DEFAULTS + "&selectedProjectId=10000");
        assertResponseCannotBeCached();
    }

    @Test
    public void testTimeTrackingReportSorting() {
        administration.restoreData("TestTimeTrackingReportSorting.xml");
        tester.gotoPage("/browse/AA?selectedTab=com.atlassian.jira.jira-projects-plugin%3Areports-panel");
        tester.clickLinkWithText("Time Tracking Report");
        tester.selectOption("sortingOrder", "Least completed issues first");
        tester.submit("Next");

        assertIssueOrder(new String[]{"AA-11", "AA-9", "AA-8", "AA-7", "AA-6", "AA-5", "AA-4", "AA-3", "AA-2", "AA-1", "AA-10"});

        tester.gotoPage("/browse/AA?selectedTab=com.atlassian.jira.jira-projects-plugin%3Areports-panel");
        tester.clickLinkWithText("Time Tracking Report");
        tester.selectOption("sortingOrder", "Most completed issues first");
        tester.submit("Next");

        assertIssueOrder(new String[]{"AA-10", "AA-1", "AA-2", "AA-3", "AA-4", "AA-5", "AA-6", "AA-7", "AA-8", "AA-9", "AA-11"});

    }

    private void assertIssueOrder(final String[] issueKeys) {
        final XPathLocator xPathLocator = new XPathLocator(tester, "//table[@id='timeReport']/tbody/tr/td[@class='issue-key']/a");
        final Node[] nodes = xPathLocator.getNodes();
        assertEquals(issueKeys.length, nodes.length);
        for (int i = 0; i < nodes.length; i++) {
            final String issueKey = issueKeys[i];
            assertEquals("The " + i + "th row was expected to be " + issueKey, issueKey, xPathLocator.getText(nodes[i]));
        }
    }

    @Test
    public void testTimeTrackingReportConfigPageIsUnavailableWhenTimeTrackingIsDisabled() {
        administration.restoreData("TestTimeTrackingReport.xml");
        disableTimeTracking();
        gotoReportConfig(PID_HOMOSAPIEN);
        assertions.forms().assertFormErrMsg("Found no available report with key 'com.atlassian.jira.jira-core-reports-plugin:time-tracking'");
    }

    @Test
    public void testTimeTrackingReportGenerationIsUnavailableWhenTimeTrackingIsDisabled() {
        administration.restoreData("TestTimeTrackingReport.xml");
        gotoReportConfig(PID_HOMOSAPIEN);
        tester.selectOption("sortingOrder", "Most completed issues first");
        tester.selectOption("completedFilter", "All");
        tester.selectOption("versionId", "No Fix Version");
        disableTimeTracking();
        tester.submit("Next");
        assertions.forms().assertFormErrMsg("Found no available report with key 'com.atlassian.jira.jira-core-reports-plugin:time-tracking'");
    }

    private void disableTimeTracking() {
        backdoor.applicationProperties().setOption(JIRA_OPTION_TIMETRACKING, false);
    }

    private void gotoReportConfig(final int projectId) {
        tester.gotoPage(Urls.TIMETRACKING_REPORT_CONFIG + "&selectedProjectId=" + projectId); // monotremes project
    }

    //-- helpers --------------------------------------------------------------------------------------

    private void assertTimeReportCell(final int row, final int col, final String expectedText) {
        final WebTable timeReport = tester.getDialog().getWebTableBySummaryOrId("timeReport");
        assertions.getTableAssertions().assertTableCellHasText(timeReport, row, col, expectedText);
    }

    private void assertSummaryPercentages(final int progressPercentage, final int accuracyPercentage) {
        final WebTable barsSummary = tester.getDialog().getWebTableBySummaryOrId("bars-summary");
        assertions.getTableAssertions().assertTableCellHasText(barsSummary, 0, 0, "Progress: " + progressPercentage + "%");
        assertions.getTableAssertions().assertTableCellHasText(barsSummary, 1, 0, "Accuracy: " + accuracyPercentage + "%");
    }

    //-- generation of reports
    private void generateTimeTrackingReport(final Long projectId) {
        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=" + projectId + "&reportKey=com.atlassian.jira.jira-core-reports-plugin:time-tracking");
        tester.selectOption("sortingOrder", "Most completed issues first");
        tester.selectOption("completedFilter", "All");
        tester.selectOption("versionId", "No Fix Version");
        tester.submit("Next");
    }

    private void generateUserWorkloadReport(final long projectId, final String user) {
        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=" + projectId + "&reportKey=com.atlassian.jira.jira-core-reports-plugin:developer-workload");
        tester.setFormElement("developer", user);
        tester.submit("Next");
    }

    private void setOriginalEstimate(final String issueKey, final String originalEstimate) {
        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("edit-issue");
        tester.setFormElement("timetracking", originalEstimate);
        tester.submit("Update");
    }

    /**
     * Asserts that the Cache-control header in the response *is* set to any one of "no-cache", "no-store" or
     * "must-revalidate". The choice of these 3 headers is directly related to the implementation of
     * com.atlassian.core.filters.AbstractEncodingFilter.setNonCachingHeaders(HttpServletResponse)
     */
    private void assertResponseCannotBeCached() {
        final String cacheControl = tester.getDialog().getResponse().getHeaderField("Cache-control");
        final String[] values = new String[]{"no-cache", "no-store", "must-revalidate"};
        if (cacheControl != null && StringUtils.isNotEmpty(cacheControl)) {
            // test for presence of any of the 3 headers - we only require 1 to be set for the response to be not cacheable
            boolean found = false;
            for (final String value : values) {
                found = found || (cacheControl.contains(value));
            }

            if (!found) {
                fail("Cache-control header was set, but was not set to 'no-cache', 'no-store' or 'must-revalidate'");
            }
        } else {
            fail("No Cache-control header was set in the response");
        }
    }

    /**
     * Get the specified row from the table as a list of trimmed strings.
     *
     * @param table table to get the row from
     * @param row   the row index starting from 0 to extract the row from
     * @return list of trimmed cell values from the table on specified row.
     */
    private List<String> getTableRowAsList(final WebTable table, final int row) {
        final List<String> tableRow = new ArrayList<>();
        final int maxCol = table.getColumnCount();
        for (int col = 0; col < maxCol; col++) {
            tableRow.add(table.getCellAsText(row, col).trim());
        }
        return tableRow;
    }

    private static class Urls {
        private static final String TIMETRACKING_REPORT_DEFAULTS = "/secure/ConfigureReport.jspa?versionId=-1&sortingOrder=least&completedFilter=all&reportKey=com.atlassian.jira.jira-core-reports-plugin%3Atime-tracking&Next=Next";
        private static final String TIMETRACKING_REPORT_CONFIG = "/secure/ConfigureReport!default.jspa?reportKey=com.atlassian.jira.jira-core-reports-plugin:time-tracking";
    }
}
