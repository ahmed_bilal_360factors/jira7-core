package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.bigpipe.TestBigPipe;
import com.atlassian.jira.webtests.ztests.bigpipe.TestBigPipeRest;
import com.atlassian.jira.webtests.ztests.misc.TestJiraLockedError;
import com.atlassian.jira.webtests.ztests.misc.TestOpenSearchProvider;
import com.atlassian.jira.webtests.ztests.misc.TestReportProblem;
import com.atlassian.jira.webtests.ztests.misc.TestResourceHeaders;
import com.atlassian.jira.webtests.ztests.misc.TestSeraphAuthType;
import com.atlassian.jira.webtests.ztests.misc.TestUserAgent;
import com.atlassian.jira.webtests.ztests.misc.TestXSS;
import com.atlassian.jira.webtests.ztests.sendheadearly.TestCommittedResponseHtmlErrorRecovery;
import com.atlassian.jira.webtests.ztests.sendheadearly.TestSendHeadEarly;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * This is a suite of radom test that have nothing no real place currently. Please don't add anything here is you can
 * avoid it (pretty please?).
 *
 * @since v4.0
 */
public class FuncTestSuiteRandomTests {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestResourceHeaders.class)
                .add(TestXSS.class)
                .add(TestJiraLockedError.class)
                .add(TestUserAgent.class)
                .add(TestSeraphAuthType.class)
                .add(TestOpenSearchProvider.class)
                .add(TestReportProblem.class)
                .add(TestSendHeadEarly.class)
                .add(TestCommittedResponseHtmlErrorRecovery.class)
                .add(TestBigPipe.class)
                .add(TestBigPipeRest.class)
                .build();
    }
}
