package com.atlassian.jira.webtests.ztests.dashboard;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.Locator;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.meterware.httpunit.HttpUnitOptions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * This Func test class tests the "Tab" that we show on the manage Dashborad screen.,
 * See JRA-15370.
 *
 * @since v3.13
 */
@WebTest({Category.FUNC_TEST, Category.DASHBOARDS})
@LoginAs(user = ADMIN_USERNAME)
public class TestManageDashboardChooseTab extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("TestManageDashboardChooseTab.xml");
        HttpUnitOptions.setScriptingEnabled(true);
    }

    @After
    public void tearDownTest() {
        HttpUnitOptions.setScriptingEnabled(false);
    }

    @Test
    public void testFavouritesTabIsShownOnNewSession() throws Exception {
        tester.gotoPage("secure/ConfigurePortalPages!default.jspa");
        // We don't have favourites, or our own Dashboards right now, so the system automatically creates a copy of the
        // System dashboard and makes it a favourite.
        // This means we should end up on the Favourites tab.
        assertFavouritesTabIsShowing();
    }

    private void assertFavouritesTabIsShowing() {
        final Locator locator = new XPathLocator(tester, "//li[@class='active first']");
        textAssertions.assertTextPresent(locator, "Favourite");
    }
}
