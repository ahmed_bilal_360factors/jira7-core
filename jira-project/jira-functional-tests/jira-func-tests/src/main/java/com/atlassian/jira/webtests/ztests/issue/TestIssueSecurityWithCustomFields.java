package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;

/*
 * Tests issue security schemes based on Custom Fields.
 *
 */
@WebTest({Category.FUNC_TEST, Category.FIELDS, Category.ISSUES, Category.SECURITY})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueSecurityWithCustomFields extends BaseJiraFuncTest {
    private static final String GROUP_CUSTOM_FIELD_ID = "customfield_10001";
    private static final String USER_CUSTOM_FIELD_ID = "customfield_10010";
    private static final String GROUP_CF_ISSUE_ID = "HSP-1";
    private static final String USER_CF_ISSUE_ID = "MKY-1";
    private static final String SELECT_CUSTOM_ISSUE_ID_BIGADMIN = "MKY-3";
    private static final String SELECT_CUSTOM_ISSUE_ID_LITTLEADMIN = "MKY-2";
    private static final String DISPLAYING_ISSUES_COUNT = "Displaying issues <span class=\"results-count-start\">1</span> to 1";


    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("TestIssueSecurityWithCustomFields.xml");
        navigation.login(ADMIN_USERNAME);

    }

    /*
     * Test user is prevented from seeing an issue when security scheme is based on a custom field
     */
    @Test
    public void testIssueSecurityWithCustomField() {
        navigation.issueNavigator().displayAllIssues();

        textAssertions.assertTextPresent(GROUP_CF_ISSUE_ID);
        navigation.issue().gotoIssue(GROUP_CF_ISSUE_ID);
        assertions.getViewIssueAssertions().assertOnViewIssuePage(GROUP_CF_ISSUE_ID);

        // Login as a user and check we cannot see the issue
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issueNavigator().displayAllIssues();
        textAssertions.assertTextNotPresent(GROUP_CF_ISSUE_ID);
        navigation.issue().gotoIssue(GROUP_CF_ISSUE_ID);
        assertions.getViewIssueAssertions().assertIssueNotFound();
    }

    /**
     * Test no system error occurs when a security scheme is based on a
     * group selector custom field that has been deleted.
     */
    @Test
    public void testViewIssueWithGroupCustomFieldDeleted() {
        deleteCustomField(GROUP_CUSTOM_FIELD_ID);
        textAssertions.assertTextPresent("Custom field cannot be deleted because it is used in the following Issue Level Security Scheme(s): My Issue Security Scheme");
    }

    /**
     * Test no system error occurs when a security scheme is based on a
     * user selector custom field that has been deleted.
     */
    @Test
    public void testViewIssueWithUserCustomFieldDeleted() {
        deleteCustomField(USER_CUSTOM_FIELD_ID);
        textAssertions.assertTextPresent("Custom field cannot be deleted because it is used in the following Issue Level Security Scheme(s): My Issue Security Scheme");
    }

    private void _testUserOnlySeesCorrectIssue(final String userName, final String issueKey, final String expectedText) {
        navigation.logout();
        navigation.login(userName);
        navigation.issueNavigator().displayAllIssues();
        textAssertions.assertTextNotPresent(issueKey);
        textAssertions.assertTextPresent(expectedText);
    }

    /**
     * Test search results when a security scheme is based on a
     * group selector custom field that has been deleted.
     * TODO: reenable when JRA-12448 is fixed
     */
    public void _testSearchWithGroupCustomFieldDeleted() {
        deleteCustomField(GROUP_CUSTOM_FIELD_ID);
        navigation.issueNavigator().displayAllIssues();
        textAssertions.assertTextNotPresent(GROUP_CF_ISSUE_ID);
    }

    /**
     * Test search results when a security scheme is based on a
     * user selector custom field that has been deleted.
     * TODO: reenable when JRA-12448 is fixed
     */
    public void _testSearchWithUserCustomFieldDeleted() {
        deleteCustomField(USER_CUSTOM_FIELD_ID);
        navigation.issueNavigator().displayAllIssues();
        textAssertions.assertTextNotPresent(USER_CF_ISSUE_ID);
    }

    private void deleteCustomField(String fieldId) {
        administration.customFields().removeCustomField(fieldId);
    }

}
