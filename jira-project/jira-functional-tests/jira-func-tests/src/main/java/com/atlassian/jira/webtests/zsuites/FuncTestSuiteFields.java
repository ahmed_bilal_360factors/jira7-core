package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.atlassian.jira.webtests.ztests.admin.index.TestReindexMessages;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkOperationCustomField;
import com.atlassian.jira.webtests.ztests.customfield.TestCascadingSelectCustomField;
import com.atlassian.jira.webtests.ztests.customfield.TestCustomFields;
import com.atlassian.jira.webtests.ztests.customfield.TestCustomFieldsNoSearcherPermissions;
import com.atlassian.jira.webtests.ztests.customfield.TestDeleteCustomField;
import com.atlassian.jira.webtests.ztests.customfield.TestEditCustomFieldDescription;
import com.atlassian.jira.webtests.ztests.customfield.TestManagedCustomFields;
import com.atlassian.jira.webtests.ztests.customfield.TestVersionCustomField;
import com.atlassian.jira.webtests.ztests.email.TestCustomFieldNotifications;
import com.atlassian.jira.webtests.ztests.issue.TestDefaultTextRenderer;
import com.atlassian.jira.webtests.ztests.issue.TestEditIssueOnIndexing;
import com.atlassian.jira.webtests.ztests.issue.TestEnvironmentField;
import com.atlassian.jira.webtests.ztests.issue.TestIssueSecurityWithCustomFields;
import com.atlassian.jira.webtests.ztests.issue.TestLabelsFormats;
import com.atlassian.jira.webtests.ztests.issue.TestWikiRendererXSS;
import com.atlassian.jira.webtests.ztests.issue.move.TestMoveIssueAndRemoveFields;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubTaskConversionSystemFields;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubTaskConversionWithFields;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of tests related to Fields (and especially custom fields)
 *
 * @since v4.0
 */
public class FuncTestSuiteFields {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestCascadingSelectCustomField.class)
                .add(TestCustomFields.class)
                .add(TestIssueSecurityWithCustomFields.class)
                .add(TestIssueToSubTaskConversionSystemFields.class)
                .add(TestIssueToSubTaskConversionWithFields.class)
                .add(TestBulkOperationCustomField.class)
                .add(TestCustomFieldsNoSearcherPermissions.class)
                .add(TestDeleteCustomField.class)
                .add(TestMoveIssueAndRemoveFields.class)
                .add(TestCustomFieldNotifications.class)
                .add(TestVersionCustomField.class)
                .add(TestEditCustomFieldDescription.class)
                .add(TestManagedCustomFields.class)
                .add(TestEnvironmentField.class)
                .add(TestEditIssueOnIndexing.class)
                .add(TestWikiRendererXSS.class)
                .add(TestDefaultTextRenderer.class)
                .add(TestReindexMessages.class)
                .add(TestLabelsFormats.class)
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.fields", true))
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.screens.tabs", true))
                .build();
    }

}