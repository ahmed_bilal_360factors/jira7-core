package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.v2.issue.project.ProjectBean;
import com.atlassian.jira.rest.v2.issue.project.ProjectIdentity;
import com.atlassian.jira.rest.v2.issue.project.ProjectInputBean;
import com.atlassian.jira.test.util.lic.core.CoreLicenses;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.atlassian.jira.testkit.client.restclient.ProjectClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.sun.jersey.api.client.ClientResponse;
import org.apache.commons.httpclient.HttpStatus;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.core.Is.is;

/**
 * Tests the handling of project types on the {@link com.atlassian.jira.rest.v2.issue.ProjectResource}
 */
@WebTest({Category.FUNC_TEST, Category.REST})
public class TestProjectTypesOnProjectResource extends BaseJiraFuncTest {
    private static final String BUSINESS_PROJECT_KEY = "T1";
    private static final String SOFTWARE_PROJECT_KEY = "T2";

    private ProjectClient projectClient;

    @Before
    public void prepareInstance() {
        projectClient = new ProjectClient(getEnvironmentData());

        backdoor.restoreDataFromResource("TestProjectTypesOnProjectResource.xml");
        backdoor.license().set(CoreLicenses.LICENSE_CORE.getLicenseString());
    }

    @Test
    @LoginAs(user = "admin")
    public void createProjectAcceptsATypeAndResponseForGettingAProjectIncludesIt() {
        Project project = projectClient.get(BUSINESS_PROJECT_KEY);

        assertThat(project, hasTypeOf("business"));
    }

    @Test
    @LoginAs(user = "admin")
    public void createProjectWithTemplateUsesTheProjectTypeDefinedOnTheTemplate() {
        String projectId = createProjectWithTemplate("com.atlassian.jira-core-project-templates:jira-core-task-management");

        Project project = projectClient.get(projectId);

        assertThat(project, hasTypeOf("business"));
    }

    @Test
    @LoginAs(user = "admin")
    public void responseForGettingAllProjectsIncludesTheProjectTypeInEachOfTheProjects() {
        List<Project> projects = projectClient.getProjects();

        assertThat(projects, containsInAnyOrder(projectWithType("business"), projectWithType("software")));
    }

    @Test
    @LoginAs(user = "admin")
    public void updateProjectTypeWhenUserIsNotLoggedInReturnsAnUnauthorizedResponse() throws Exception {
        Response<Project> response = projectClient.anonymous().updateProjectType(BUSINESS_PROJECT_KEY, "any-type");

        assertThat(response.statusCode, is(HttpStatus.SC_UNAUTHORIZED));
    }

    @Test
    @LoginAs(user = "admin")
    public void updateProjectTypeUpdatesTheProjectTypeWhenTheUserIsLoggedInAndHasPermissions() throws Exception {
        Response<Project> response = projectClient.updateProjectType(SOFTWARE_PROJECT_KEY, "business");

        assertThat(response.statusCode, is(HttpStatus.SC_OK));
        assertThat(response.body, hasTypeOf("business"));
    }

    @Test
    @LoginAs(user = "admin")
    public void updateProjectTypeReturnsAnErrorResponseWhenTheUpdateFails() throws Exception {
        Response<Project> response = projectClient.updateProjectType(BUSINESS_PROJECT_KEY, "non-existent-type");

        assertThat(response.statusCode, is(HttpStatus.SC_BAD_REQUEST));
    }

    @Test
    @LoginAs(user = "admin")
    public void updateProjectTypeReturns404IfTheProjectDoesNotExist() throws Exception {
        Response<Project> response = projectClient.updateProjectType("non-existent-project", "any-type");

        assertThat(response.statusCode, is(HttpStatus.SC_NOT_FOUND));
    }

    private String createProjectWithTemplate(String projectTemplateKey) {
        return createProject(projectInputBeanWithTemplate(projectTemplateKey));
    }

    private String createProject(ProjectInputBean project) {
        ClientResponse response = projectClient.loginAs("admin").create(project);
        try {
            return response.getEntity(ProjectIdentity.class).getKey();
        } finally {
            response.close();
        }
    }

    private ProjectInputBean projectInputBeanWithTemplate(String projectTemplateKey) {
        return ProjectInputBean.builder().setProjectTemplateKey(projectTemplateKey)
                .setName("Project created with template")
                .setKey("TEMPLATE")
                .setDescription("description")
                .setLeadName("admin")
                .setUrl("http://atlassian.com")
                .setAssigneeType(ProjectBean.AssigneeType.PROJECT_LEAD)
                .build();
    }

    private ProjectTypeMatcher projectWithType(String expectedProjectType) {
        return hasTypeOf(expectedProjectType);
    }

    private ProjectTypeMatcher hasTypeOf(String expectedProjectType) {
        return new ProjectTypeMatcher(expectedProjectType);
    }

    private class ProjectTypeMatcher extends TypeSafeDiagnosingMatcher<Project> {
        private final String expectedProjectType;

        public ProjectTypeMatcher(String expectedProjectType) {
            this.expectedProjectType = expectedProjectType;
        }

        @Override
        protected boolean matchesSafely(final Project project, final Description mismatchDescription) {
            return expectedProjectType.equals(project.projectTypeKey);
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("project with type: " + expectedProjectType);
        }
    }
}
