package com.atlassian.jira.functest.framework.backdoor.application;

/**
 * @since 7.0
 */
public enum ApplicationAccessStatus {
    UNLICENSED, EXPIRED, VERSION_MISMATCH, USERS_EXCEEDED, NO_ACCESS, OK
}
