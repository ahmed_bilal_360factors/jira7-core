package com.atlassian.jira.functest.framework.backdoor.upgrade;

import com.atlassian.jira.functest.framework.backdoor.BackdoorControl;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.sun.jersey.api.client.WebResource;

public class UpgradeControl extends BackdoorControl<UpgradeControl> {
    public UpgradeControl(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    /**
     * @return the host details, such as current database build number.
     */
    public HostUpgradeDetailsJsonBean getHostDetails() {
        return createUpgradeBackdoorResource()
                .path("hostDetails")
                .get(HostUpgradeDetailsJsonBean.class);
    }

    /**
     * @return the plugin upgrade details - includes plugin build number, SAL build number, and AO build number.
     */
    public PluginUpgradeDetailsJsonBean getPluginDetails(String pluginKey) {
        return createUpgradeBackdoorResource()
                .path("pluginDetails")
                .queryParam("pluginKey", pluginKey)
                .get(PluginUpgradeDetailsJsonBean.class);
    }

    private WebResource createUpgradeBackdoorResource() {
        return createResource().path("upgrade");
    }
}