package com.atlassian.jira.webtests.ztests.navigator.jql;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.assertions.IssueNavigatorAssertions;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.assertions.JqlAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.assertions.IssueNavigatorAssertions.FilterFormParam.createFilterFormParam;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.JQL})
@Restore("TestCustomFieldAliases.xml")
public class TestCustomFieldAliases extends BaseJiraFuncTest {

    @Inject
    private IssueTableAssertions issueTableAssertions;

    @Inject
    private JqlAssertions jqlAssertions;

    @Before
    public void setUpTest() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    public void testCustumFielAliasesAndSpaces() throws Exception {
        issueTableAssertions.assertSearchWithResults("\"With Spaces\" is not empty", "HSP-3", "HSP-2");
        issueTableAssertions.assertSearchWithResults("cf[10001] is not empty", "HSP-3", "HSP-2");
        issueTableAssertions.assertSearchWithResults("\"With Spaces\" ~ \"Blah\" OR cf[10001] ~ \"Hello there\"", "HSP-3", "HSP-2");
        issueTableAssertions.assertSearchWithResults("\"NoSpaces\" is not empty", "HSP-1");
        issueTableAssertions.assertSearchWithResults("cf[10000] is not empty", "HSP-1");
        issueTableAssertions.assertSearchWithResults("\"With Spaces\" is not empty OR \"NoSpaces\" is not empty", "HSP-3", "HSP-2", "HSP-1");
    }

    @Test
    public void testCustomFieldDoesItFitWithAliasesAndFullNames() throws Exception {
        jqlAssertions.assertFitsFilterForm("\"With Spaces\" ~ \"Blah\"", createFilterFormParam("customfield_10001", "Blah"));
        jqlAssertions.assertFitsFilterForm("\"cf[10001]\" ~ \"Blah\"", createFilterFormParam("customfield_10001", "Blah"));
        jqlAssertions.assertFitsFilterForm("\"NoSpaces\" ~ \"HelloThere\"", createFilterFormParam("customfield_10000", "HelloThere"));
        jqlAssertions.assertFitsFilterForm("\"cf[10000]\" ~ \"HelloThere\"", createFilterFormParam("customfield_10000", "HelloThere"));
    }

    @Test
    public void testSameNameSameTypeSameContext() throws Exception {
        jqlAssertions.assertFitsFilterForm("cf[10010] ~ \"Blah\"", createFilterFormParam("customfield_10010", "Blah"));
        jqlAssertions.assertFitsFilterForm("cf[10010] ~ \"Blah\" AND cf[10011] ~ \"Blah\"", createFilterFormParam("customfield_10010", "Blah"), createFilterFormParam("customfield_10011", "Blah"));
        jqlAssertions.assertTooComplex("SameSameSame ~ \"Blah\"");

        issueTableAssertions.assertSearchWithResults("SameSameSame ~ \"value1\"", "HSP-5", "HSP-4");
        issueTableAssertions.assertSearchWithResults("SameSameSame ~ \"value2\"", "HSP-5", "HSP-4");
        issueTableAssertions.assertSearchWithResults("cf[10010] ~ \"value1\"", "HSP-4");
        issueTableAssertions.assertSearchWithResults("cf[10011] ~ \"value1\"", "HSP-5");
    }

    @Test
    public void testSameNameDiffTypeSameContext() throws Exception {
        issueTableAssertions.assertSearchWithError("SameDiffSame ~ 1", "The operator '~' is not supported by the 'SameDiffSame' field.");
        issueTableAssertions.assertSearchWithError("SameDiffSame = 1", "The operator '=' is not supported by the 'SameDiffSame' field.");
        issueTableAssertions.assertSearchWithError("SameDiffSame = 2", "The operator '=' is not supported by the 'SameDiffSame' field.");
        issueTableAssertions.assertSearchWithError("SameDiffSame ~ Value1", "The operator '~' is not supported by the 'SameDiffSame' field.");

        issueTableAssertions.assertSearchWithResults("cf[10020] ~ Value1", "HSP-6");
        jqlAssertions.assertFitsFilterForm("cf[10020] ~ Value1");

        issueTableAssertions.assertSearchWithResults("cf[10020]  ~ 2", "HSP-7");
        jqlAssertions.assertFitsFilterForm("cf[10020]  ~ 2");

        issueTableAssertions.assertSearchWithResults("cf[10021] = 1", "HSP-7");
        jqlAssertions.assertFitsFilterForm("cf[10021] = 1");

        issueTableAssertions.assertSearchWithResults("cf[10021] = 2", "HSP-6");
        jqlAssertions.assertFitsFilterForm("cf[10021] = 2");
    }

    @Test
    public void testSameNameSameTypeSameContextDifferentValues() throws Exception {
        issueTableAssertions.assertSearchWithResults("SelectHidden = \"SameValue\"");
        jqlAssertions.assertTooComplex("SelectHidden = \"SameValue\"");
        issueTableAssertions.assertSearchWithResults("SelectHidden = \"DiffValue1\"", "HSP-8");
        jqlAssertions.assertTooComplex("SelectHidden = \"DiffValue1\"");
        issueTableAssertions.assertSearchWithError("SelectHidden = \"DiffValue2\"", "The option 'DiffValue2' for field 'SelectHidden' does not exist.");

        issueTableAssertions.assertSearchWithResults("\"Select\" = \"SameValue\"", "HSP-10", "HSP-9");
        jqlAssertions.assertTooComplex("\"Select\" = \"SameValue\"");
        jqlAssertions.assertTooComplex("\"Select\" = \"SameValue\" and project = \"homosapien\"");
        jqlAssertions.assertTooComplex("\"Select\" = \"SameValue\" and project = \"monkey\"");

        jqlAssertions.assertFitsFilterForm("cf[10030] = \"SameValue\"");
        jqlAssertions.assertFitsFilterForm("cf[10031] = \"SameValue\"");

        jqlAssertions.assertFitsFilterForm("cf[10030] = \"DiffValue1\"");
        jqlAssertions.assertFitsFilterForm("cf[10031] = \"DiffValue2\"");

        issueTableAssertions.assertSearchWithError("\"Select\" = \"DiffValue1\"", "The option 'DiffValue1' for field 'Select' does not exist.");
        issueTableAssertions.assertSearchWithError("\"Select\" = \"DiffValue2\"", "The option 'DiffValue2' for field 'Select' does not exist.");
    }

    @Test
    public void testSameNameSameTypeDiffContext() throws Exception {
        issueTableAssertions.assertSearchWithResults("SelectContext = 'Option1'", "MKY-1", "HSP-11");
        jqlAssertions.assertTooComplex("SelectContext = 'Option1'");
        issueTableAssertions.assertSearchWithResults("SelectContext = 'Option1' and project = homosapien", "HSP-11");
        jqlAssertions.assertTooComplex("SelectContext = 'Option1' and project = homosapien");
        issueTableAssertions.assertSearchWithResults("SelectContext = 'Option1' and project = monkey", "MKY-1");
        jqlAssertions.assertTooComplex("SelectContext = 'Option1' and project = monkey");

        issueTableAssertions.assertSearchWithError("SelectContext= 'OptionMonkey'", "The option 'OptionMonkey' for field 'SelectContext' does not exist.");
    }

    @Test
    public void testSameNameDiffTypeSameOperator() throws Exception {
        issueTableAssertions.assertSearchWithResults("Picker in (\"TestValue\")", "HSP-17", "HSP-16", "HSP-15", "HSP-14", "HSP-13");
        issueTableAssertions.assertSearchWithResults("cf[10050] in (\"TestValue\")", "HSP-17", "HSP-16", "HSP-15", "HSP-13");
        issueTableAssertions.assertSearchWithResults("cf[10051] in (\"TestValue\")", "HSP-17", "HSP-14", "HSP-13");

        issueTableAssertions.assertSearchWithResults("Picker not in (\"TestValue\")", "HSP-18");
        issueTableAssertions.assertSearchWithResults("cf[10050] not in (\"TestValue\")", "HSP-18");
        issueTableAssertions.assertSearchWithResults("cf[10051] not in (\"TestValue\")", "HSP-18");

        issueTableAssertions.assertSearchWithResults("Picker = \"TestValue\"", "HSP-17", "HSP-16", "HSP-15", "HSP-14", "HSP-13");
        issueTableAssertions.assertSearchWithResults("cf[10050] = \"TestValue\"", "HSP-17", "HSP-16", "HSP-15", "HSP-13");
        issueTableAssertions.assertSearchWithResults("cf[10051] = \"TestValue\"", "HSP-17", "HSP-14", "HSP-13");

        issueTableAssertions.assertSearchWithResults("Picker != \"TestValue\"", "HSP-18");
        issueTableAssertions.assertSearchWithResults("cf[10050] != \"TestValue\"", "HSP-18");
        issueTableAssertions.assertSearchWithResults("cf[10051] != \"TestValue\"", "HSP-18");

        issueTableAssertions.assertSearchWithResults("Picker is empty", "MKY-2", "MKY-1", "HSP-16", "HSP-15", "HSP-14", "HSP-12", "HSP-11", "HSP-10", "HSP-9", "HSP-8", "HSP-7", "HSP-6", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1");
        issueTableAssertions.assertSearchWithResults("cf[10050] is empty", "MKY-2", "MKY-1", "HSP-14", "HSP-12", "HSP-11", "HSP-10", "HSP-9", "HSP-8", "HSP-7", "HSP-6", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1");
        issueTableAssertions.assertSearchWithResults("cf[10051] is empty", "MKY-2", "MKY-1", "HSP-16", "HSP-15", "HSP-12", "HSP-11", "HSP-10", "HSP-9", "HSP-8", "HSP-7", "HSP-6", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1");

        issueTableAssertions.assertSearchWithResults("Picker is not empty", "HSP-18", "HSP-17", "HSP-16", "HSP-15", "HSP-14", "HSP-13");
        issueTableAssertions.assertSearchWithResults("cf[10050] is not empty", "HSP-18", "HSP-17", "HSP-16", "HSP-15", "HSP-13");
        issueTableAssertions.assertSearchWithResults("cf[10051] is not empty", "HSP-18", "HSP-17", "HSP-14", "HSP-13");

        issueTableAssertions.assertSearchWithError("Picker in (\"New Version 1\")", "The value 'New Version 1' does not exist for the field 'Picker'.");
        issueTableAssertions.assertSearchWithError("cf[10051] in (\"New Version 1\")", "The value 'New Version 1' does not exist for the field 'cf[10051]'.");
        issueTableAssertions.assertSearchWithResults("cf[10050] in (\"New Version 1\")", "HSP-18", "HSP-17");

        jqlAssertions.assertFitsFilterForm("cf[10050] = \"New Version 1\"");
        final IssueNavigatorAssertions.FilterFormParam param1 = createFilterFormParam("pid", "10000");
        final IssueNavigatorAssertions.FilterFormParam param2 = createFilterFormParam("customfield_10050", "10000");
        jqlAssertions.assertFitsFilterForm("cf[10050] in (\"New Version 1\") AND project = \"Homosapien\"", param1, param2);

        jqlAssertions.assertTooComplex("cf[10051] in (\"TestValue\")");
        jqlAssertions.assertFitsFilterForm("cf[10051] = \"TestValue\"");
    }

    //
    // Test to ensure that custom field aliases are ignored when:
    //   1. They have the same name as a system field.
    //   2. Someone tries to name their custom field "cf[\d+]".
    //
    @Test
    public void testBadCustomFieldName() throws Exception {
        /*
            cf:10066 -> summary
            cf:10067 -> cf[10067]
         */

        //Make sure the project custom field does not come into play.
        issueTableAssertions.assertSearchWithResults("summary ~ match");
        issueTableAssertions.assertSearchWithResults("cf[10070] ~ match", "HSP-8");
        issueTableAssertions.assertSearchWithResults("cf[10071] ~ match", "HSP-14");
    }
}
