package com.atlassian.jira.webtests.ztests.projectconfig.framework;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Collections;
import java.util.Map;

/**
 * @since v5.2
 */
public class SimpleUser {
    @JsonProperty
    private String name;

    @JsonProperty
    private String displayName;

    @JsonProperty
    private Map<String, String> avatarUrls;

    @JsonProperty
    private boolean active;

    public SimpleUser(String displayName, String name, String avatarUrl, boolean active) {
        this.displayName = displayName;
        this.name = name;
        this.avatarUrls = Collections.singletonMap("16x16", avatarUrl);
        this.active = active;
    }

    public SimpleUser() {
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Map<String, String> getAvatarUrls() {
        return avatarUrls;
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SimpleUser that = (SimpleUser) o;

        if (active != that.active) {
            return false;
        }
        if (displayName != null ? !displayName.equals(that.displayName) : that.displayName != null) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (displayName != null ? displayName.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
