package com.atlassian.jira.webtests.util.issue;

import com.atlassian.jira.functest.framework.FuncTestUrlHelper;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.PostMethodWebRequest;
import net.sourceforge.jwebunit.WebTester;
import org.w3c.dom.Element;

import java.io.ByteArrayInputStream;

/**
 * Class with methods to inline edit fields on an issue.
 */
public class IssueInlineEdit {
    private LocatorFactory locator;
    private WebTester tester;
    private FuncTestUrlHelper funcTestUrlHelper;

    public IssueInlineEdit(LocatorFactory locator, WebTester tester, JIRAEnvironmentData environmentData) {
        this.locator = locator;
        this.tester = tester;
        this.funcTestUrlHelper = new FuncTestUrlHelper(environmentData);
    }

    public void inlineEditField(String issueId, String fieldName, String fieldValue) throws Exception {
        // Get the token to be able to make the next request
        Element node = (Element) locator.css("meta[name=atlassian-token]").getNode();
        String token = node.getAttribute("content");

        // Simulate inline edit of field
        String body = fieldName + "=" + fieldValue + "&"
                + "issueId=" + issueId + "&"
                + "singleFieldEdit=true&"
                + "fieldsToForcePresent=" + fieldName + "&"
                + "atl_token=" + token;
        POST("AjaxIssueAction.jspa?decorator=none", body);
    }

    private void POST(final String url, final String postBody) throws Exception {

        tester.getDialog().getWebClient().setExceptionsThrownOnErrorStatus(false);
        HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);

        PostMethodWebRequest request = new PostMethodWebRequest(funcTestUrlHelper.getBaseUrlPlus(url), new ByteArrayInputStream(postBody.getBytes()), "application/x-www-form-urlencoded");
        tester.getDialog().getWebClient().sendRequest(request);

        //this was inlined from RestRule.after
        HttpUnitOptions.resetDefaultCharacterSet();
    }
}
