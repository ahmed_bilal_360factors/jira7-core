package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import java.net.URI;

import com.atlassian.jira.issue.fields.rest.json.beans.WorklogJsonBean;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class WebHookResponseData {
    private final String json;
    private final URI uri;

    public WebHookResponseData(final String json, final URI uri) {
        this.json = json;
        this.uri = uri;
    }

    public String getJson() {
        return json;
    }

    public URI getUri() {
        return uri;
    }

    public JSONObject asJsonObject() throws JSONException {
        return new JSONObject(json);
    }

    public <T> T asBean(Class<T> clazz, String objName) throws JSONException {
        return new Gson().fromJson(asJsonObject().getJSONObject(objName).toString(), clazz);
    }
}
