package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.TableLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * A func test for the ModzDetector rows in the ViewSystemInfo page
 *
 * @since v3.13
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.BROWSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestModz extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
    }

    @Test
    public void testChangedFiles() {
//        turnOnDebug();
        navigation.gotoAdminSection(Navigation.AdminSection.SYSTEM_INFO);

        TableLocator locator = new TableLocator(tester, "system_info_table");
        textAssertions.assertTextPresent(locator, "There have been no removed files");
        // the files in WEB-INF/classes are to be loaded from the *classloader*. If they are not excluded from the
        // hash-registry generation process for files to be loaded from the *filesystem*, they will be erroneously
        // identified as missing.
        textAssertions.assertTextNotPresent(locator, "WEB-INF/classes");
//        turnOffDebug();
    }

    private void turnOffDebug() {
        tester.gotoPage("/secure/admin/jira/ConfigureLogging!default.jspa?loggerName=com.atlassian");
        tester.selectOption("levelName", "WARN");
    }

    private void turnOnDebug() {
        tester.gotoPage("/secure/admin/jira/ConfigureLogging!default.jspa?loggerName=com.atlassian");
        tester.selectOption("levelName", "DEBUG");
        tester.submit("Update");
    }

}
