package com.atlassian.jira.webtests.ztests.user.rename;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.admin.user.EditUserPage;
import com.atlassian.jira.functest.framework.page.ViewIssuePage;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_ADMIN_ROLE_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_DEV_GROUP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_DEV_ROLE_ID;
import static com.atlassian.jira.permission.ProjectPermissions.EDIT_ISSUES;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @since v6.0
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS, Category.RENAME_USER, Category.PERMISSIONS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestUserRenameOnPermissions extends BaseJiraFuncTest {
    private static final String BETTY_USERNAME = "betty";
    private static final String BETTY2_USERNAME = "betty2";
    private static final String BB_USERNAME = "bb";

    @Inject
    private Administration administration;

    @Test
    public void testSingleUserPermission() {
        administration.restoreData("user_rename_permissions.xml");
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");

        assertTrue(userCanEdit(BETTY_USERNAME));

        // Remove existing permission - project role developer
        removeInitialEditIssuePermissionTypes();

        assertFalse(userCanEdit(BETTY_USERNAME));

        // Single User Permission for betty
        backdoor.permissionSchemes().addUserPermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES, BETTY_USERNAME);
        assertTrue(userCanEdit(BETTY_USERNAME));
        assertFalse(userCanEdit(BB_USERNAME));

        // Rename user betty to betty2
        EditUserPage editUserPage = administration.usersAndGroups().gotoEditUser(BETTY_USERNAME);
        editUserPage.setUsername(BETTY2_USERNAME);
        editUserPage.submitUpdate();

        assertTrue(userCanEdit(BETTY2_USERNAME));
        assertFalse(userCanEdit(BB_USERNAME));

        backdoor.permissionSchemes().removeUserPermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES, BETTY2_USERNAME);
        assertFalse(userCanEdit(BETTY2_USERNAME));

        // Single User Permission for bob (recycled username)
        backdoor.permissionSchemes().addUserPermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES, BB_USERNAME);
        assertTrue(userCanEdit(BB_USERNAME));
        assertFalse(userCanEdit(BETTY2_USERNAME));
    }

    @Test
    public void testReporterPermission() {
        administration.restoreData("user_rename_permissions.xml");
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");

        // Remove existing permission - project role developer
        removeInitialEditIssuePermissionTypes();

        assertFalse(userCanEditIssue("cat", "COW-1"));

        // Add reporter user permission
        backdoor.permissionSchemes().addReporterPermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES);
        assertTrue(userCanEditIssue("cat", "COW-1"));
        assertFalse(userCanEditIssue("cc", "COW-1"));
        assertTrue(userCanEditIssue("cc", "COW-3"));
        assertFalse(userCanEditIssue("cat", "COW-3"));

        // Rename user cat to cat2
        EditUserPage editUserPage = administration.usersAndGroups().gotoEditUser("cat");
        editUserPage.setUsername("cat2");
        editUserPage.submitUpdate();

        // Check that cat2 can still edit
        assertTrue(userCanEditIssue("cat2", "COW-1"));
        assertFalse(userCanEditIssue("cc", "COW-1"));
        assertFalse(userCanEditIssue("cat2", "COW-3"));
    }

    @Test
    public void testAssigneePermission() {
        administration.restoreData("user_rename_permissions.xml");
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");

        // Remove existing permission - project role developer
        removeInitialEditIssuePermissionTypes();

        assertFalse(userCanEditIssue(BETTY_USERNAME, "COW-1"));

        // Add assignee user permission
        backdoor.permissionSchemes().addCurrentAssigneePermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES);
        assertTrue(userCanEditIssue(BETTY_USERNAME, "COW-1"));
        assertFalse(userCanEditIssue(BB_USERNAME, "COW-1"));
        assertTrue(userCanEditIssue(BB_USERNAME, "COW-3"));
        assertFalse(userCanEditIssue(BETTY_USERNAME, "COW-3"));

        // Rename user betty to betty2
        EditUserPage editUserPage = administration.usersAndGroups().gotoEditUser(BETTY_USERNAME);
        editUserPage.setUsername(BETTY2_USERNAME);
        editUserPage.submitUpdate();

        // Check that betty2 can still edit
        assertTrue(userCanEditIssue(BETTY2_USERNAME, "COW-1"));
        assertFalse(userCanEditIssue(BB_USERNAME, "COW-1"));
        assertFalse(userCanEditIssue(BETTY2_USERNAME, "COW-3"));
    }

    @Test
    public void testGroupPermission() {
        administration.restoreData("user_rename_permissions.xml");
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");

        // Remove existing permission - project role developer
        removeInitialEditIssuePermissionTypes();

        assertFalse(userCanEditIssue("cat", "COW-1"));

        // Add group user permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES, JIRA_DEV_GROUP);
        assertTrue(userCanEditIssue("cat", "COW-1"));
        assertFalse(userCanEditIssue("cc", "COW-1"));

        // Rename user
        EditUserPage editUserPage = administration.usersAndGroups().gotoEditUser("cat");
        editUserPage.setUsername("cat2");
        editUserPage.submitUpdate();

        // Check that cat2 can still edit
        assertTrue(userCanEditIssue("cat2", "COW-1"));
        assertFalse(userCanEditIssue("cc", "COW-1"));

        // remove cat2 from jira-dev
        administration.usersAndGroups().removeUserFromGroup("cat2", "jira-developers");
        administration.usersAndGroups().addUserToGroup("cc", "jira-developers");
        assertFalse(userCanEditIssue("cat2", "COW-1"));
        assertTrue(userCanEditIssue("cc", "COW-1"));
    }

    @Test
    public void testProjectLeadPermission() {
        administration.restoreData("user_rename_permissions.xml");
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");

        // Remove existing permission - project role developer
        removeInitialEditIssuePermissionTypes();
        // set cat as project lead
        administration.project().setProjectLead("Bovine", "cat");

        assertFalse(userCanEditIssue("cat", "COW-1"));

        // Add project lead user permission
        backdoor.permissionSchemes().addProjectLeadPermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES);
        assertTrue(userCanEditIssue("cat", "COW-1"));
        assertFalse(userCanEditIssue("cc", "COW-1"));

        // Rename user
        EditUserPage editUserPage = administration.usersAndGroups().gotoEditUser("cat");
        editUserPage.setUsername("cat2");
        editUserPage.submitUpdate();

        // Check that cat2 can still edit
        assertTrue(userCanEditIssue("cat2", "COW-1"));
        assertFalse(userCanEditIssue("cc", "COW-1"));

        // set cc as project lead
        administration.project().setProjectLead("Bovine", "cc");
        assertFalse(userCanEditIssue("cat2", "COW-1"));
        assertTrue(userCanEditIssue("cc", "COW-1"));
    }

    @Test
    public void testUserCFPermission() {
        administration.restoreData("user_rename_permissions.xml");
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");

        // Remove existing permission - project role developer
        removeInitialEditIssuePermissionTypes();

        assertFalse(userCanEditIssue("cat", "COW-2"));
        assertFalse(userCanEditIssue("cc", "COW-1"));

        // Add User CF user permission
        backdoor.permissionSchemes().addUserCustomFieldPermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES, "customfield_10300");
        assertTrue(userCanEditIssue("cat", "COW-2"));
        assertFalse(userCanEditIssue("cc", "COW-2"));
        assertTrue(userCanEditIssue("cc", "COW-1"));
        assertFalse(userCanEditIssue("cat", "COW-1"));

        // Rename user
        EditUserPage editUserPage = administration.usersAndGroups().gotoEditUser("cat");
        editUserPage.setUsername("cat2");
        editUserPage.submitUpdate();

        // Check that cat2 can still edit
        assertTrue(userCanEditIssue("cat2", "COW-2"));
        assertFalse(userCanEditIssue("cat2", "COW-1"));
    }

    @Test
    public void testGroupCFPermission() {
        administration.restoreData("user_rename_permissions.xml");
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");

        // Remove existing permission - project role developer
        removeInitialEditIssuePermissionTypes();

        assertFalse(userCanEditIssue("cat", "COW-5"));

        // Add group CF permission
        backdoor.permissionSchemes().addGroupCustomFieldPermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES, "customfield_10400");
        assertTrue(userCanEditIssue("cat", "COW-5"));
        assertFalse(userCanEditIssue("cc", "COW-5"));

        // Rename user
        EditUserPage editUserPage = administration.usersAndGroups().gotoEditUser("cat");
        editUserPage.setUsername("cat2");
        editUserPage.submitUpdate();

        // Check that cat2 can still edit
        assertTrue(userCanEditIssue("cat2", "COW-5"));
        assertFalse(userCanEditIssue("cc", "COW-5"));

        // remove cat2 from jira-dev
        administration.usersAndGroups().removeUserFromGroup("cat2", "jira-developers");
        administration.usersAndGroups().addUserToGroup("cc", "jira-developers");
        assertFalse(userCanEditIssue("cat2", "COW-5"));
        assertTrue(userCanEditIssue("cc", "COW-5"));
    }

    @Test
    public void testProjectRolePermission() {
        administration.restoreData("user_rename_permissions.xml");
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");

        // Default permission scheme is set up with project role developer permission based on group jira-dev
        assertTrue(userCanEditIssue("cat", "COW-1"));
        assertFalse(userCanEditIssue("cc", "COW-1"));

        // Rename user
        EditUserPage editUserPage = administration.usersAndGroups().gotoEditUser("cat");
        editUserPage.setUsername("cat2");
        editUserPage.submitUpdate();

        // Check that cat2 can still edit
        assertTrue(userCanEditIssue("cat2", "COW-1"));
        assertFalse(userCanEditIssue("cc", "COW-1"));

        // remove cat2 from jira-dev
        administration.usersAndGroups().removeUserFromGroup("cat2", "jira-developers");
        administration.usersAndGroups().addUserToGroup("cc", "jira-developers");
        assertFalse(userCanEditIssue("cat2", "COW-1"));
        assertTrue(userCanEditIssue("cc", "COW-1"));

        // Now let's remove the permissions and replace with the admin permission
        backdoor.permissionSchemes().replaceProjectRolePermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES, JIRA_ADMIN_ROLE_ID);

        assertFalse(userCanEditIssue("cat2", "COW-1"));
        assertFalse(userCanEditIssue("cc", "COW-1"));


        // Swap to a user based role "Cats" which only includes cat
        backdoor.permissionSchemes().replaceProjectRolePermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES, 10003);

        assertTrue(userCanEditIssue("cat2", "COW-1"));
        assertFalse(userCanEditIssue("cc", "COW-1"));

        // Rename user
        editUserPage = administration.usersAndGroups().gotoEditUser("cat2");
        editUserPage.setUsername("cat");
        editUserPage.submitUpdate();

        assertTrue(userCanEditIssue("cat", "COW-1"));
        assertFalse(userCanEditIssue("cc", "COW-1"));


        // Swap to a user based role "Dogs" which only includes Candy
        backdoor.permissionSchemes().replaceProjectRolePermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES, 10004);

        assertFalse(userCanEditIssue("cat", "COW-1"));
        assertTrue(userCanEditIssue("cc", "COW-1"));
    }

    private void removeInitialEditIssuePermissionTypes() {
        backdoor.permissionSchemes().removeProjectRolePermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES, JIRA_DEV_ROLE_ID);
    }

    private boolean userCanEdit(String username) {
        return userCanEditIssue(username, "COW-1");
    }

    private boolean userCanEditIssue(String username, String issueKey) {
        String password = username;
        if (username.equals(BETTY2_USERNAME))
            password = BETTY_USERNAME;
        if (username.equals("cat2"))
            password = "cat";
        try {
            navigation.login(username, password);
            ViewIssuePage viewIssuePage = navigation.issue().viewIssue(issueKey);
            return viewIssuePage.containsEditButton();
        } finally {
            navigation.login("admin");
        }
    }
}
