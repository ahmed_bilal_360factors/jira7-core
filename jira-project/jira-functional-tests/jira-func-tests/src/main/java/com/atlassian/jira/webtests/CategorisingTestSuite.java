package com.atlassian.jira.webtests;

import com.atlassian.jira.functest.framework.TestSuiteBuilder;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.FunctionalCategoryComparator;
import com.atlassian.jira.functest.framework.suite.SystemPropertyBasedSuite;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestOut;
import com.atlassian.jira.webtests.util.TestClassUtils;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestResult;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Test suite that runs given set of tests based on provided test package name property and
 * included and excluded categories.
 * <p>
 * This basically mimics behaviour of {@link com.atlassian.jira.functest.framework.suite.SystemPropertyBasedSuite}, but
 * works with the old JUnit3-based test infrastructure.
 *
 * @since v4.3
 * @deprecated use {@link com.atlassian.jira.functest.framework.suite.SystemPropertyBasedSuite} instead
 */
public class CategorisingTestSuite implements Test {
    public static final CategorisingTestSuite SUITE = new CategorisingTestSuite();

    public static Test suite() {
        return SUITE.createTest();
    }

    private static final String NUMBER_OF_BATCHES = "atlassian.test.suite.numbatches";
    private static final String BATCH_NUMBER = "atlassian.test.suite.batch";

    private static TestSuiteBuilder createFuncTestBuilder() {
        final String numberOfBatches = System.getProperty(NUMBER_OF_BATCHES);
        final String batchNumber = System.getProperty(BATCH_NUMBER);
        if (numberOfBatches != null && batchNumber != null) {
            String batchInfo = "Batch " + batchNumber + " of " + numberOfBatches;
            try {
                int numBatches = Integer.parseInt(numberOfBatches);
                int batch = Integer.parseInt(batchNumber);
                if (batch > 0 && batch <= numBatches) {
                    FuncTestOut.out.println(batchInfo);
                    return new TestSuiteBuilder(batch, numBatches).log(true);
                } else {
                    FuncTestOut.out.println("Batch mode FAIL. Batch information looks wrong-arse: " + batchInfo);
                }
            } catch (NumberFormatException e) {
                FuncTestOut.err.println("Batch mode FAIL. Batch information cannot be properly interpreted: " + batchInfo);
                e.printStackTrace(FuncTestOut.err);
                // will fall back to unbatched mode
            }
        }
        return new TestSuiteBuilder().log(true);
    }

    private final SystemPropertyBasedSuite realSuite = new SystemPropertyBasedSuite();


    public int countTestCases() {
        return createTest().countTestCases();
    }

    public void run(final TestResult result) {
        createTest().run(result);
    }

    private Test createTest() {
        final Collection<Class<?>> tests = getTests();
        return createFuncTestBuilder().addTests(tests).build();
    }

    private Collection<Class<?>> getTests() {
        List<Class<? extends TestCase>> allTestsInPackage = TestClassUtils.getJUnit3TestClasses(realSuite.webTestPackage(), true);
        List<Class<?>> allJunit4TestsInPackage = TestClassUtils.getJUnit4TestClasses(realSuite.webTestPackage(), true);
        // this should really be done in TestSuiteBuilder to allow for per-method filtering
        // but given this is a workaround and we will hopefully move to JUnit4 and WebTestSuite
        // it should be good enough
        List<Class<?>> filtered = Lists.newArrayList(Iterables.filter(Iterables.concat(allTestsInPackage, allJunit4TestsInPackage),
                new CategoryMatchPredicate(realSuite.includes(), realSuite.excludes())));
        Collections.sort(filtered, FunctionalCategoryComparator.INSTANCE);
        return new LinkedHashSet<>(filtered);
    }


    public static class CategoryMatchPredicate implements Predicate<Class<?>> {
        private final Set<Category> included;
        private final Set<Category> excluded;

        public CategoryMatchPredicate(Set<Category> includedCategories, Set<Category> excludedCategories) {
            included = includedCategories.size() > 0 ? EnumSet.copyOf(includedCategories) : EnumSet.noneOf(Category.class);
            excluded = excludedCategories.size() > 0 ? EnumSet.copyOf(excludedCategories) : EnumSet.noneOf(Category.class);
        }

        @Override
        public boolean apply(@Nullable Class<?> input) {
            return hasCorrectCategoryAnnotation(input);
        }

        private boolean hasCorrectCategoryAnnotation(Class<?> testClass) {
            Set<Category> categories = categories(testClass);
            if (categories.isEmpty()) {
                return included == null;
            }
            for (Category each : categories) {
                if (excluded != null && excluded.contains(each)) {
                    return false;
                }
            }
            for (Category each : categories) {
                if (included == null || included.contains(each)) {
                    return true;
                }
            }
            return false;
        }

        private Set<Category> categories(Class<?> testClass) {
            WebTest annotation = testClass.getAnnotation(WebTest.class);
            if (annotation == null || annotation.value().length == 0) {
                return EnumSet.noneOf(Category.class);
            }
            return EnumSet.copyOf(Arrays.asList(annotation.value()));
        }
    }
}
