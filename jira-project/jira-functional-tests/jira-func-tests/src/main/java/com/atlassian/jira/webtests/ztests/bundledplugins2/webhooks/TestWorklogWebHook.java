package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.issue.fields.rest.json.beans.WorklogJsonBean;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Worklog;
import com.atlassian.jira.testkit.client.restclient.WorklogClient;
import com.google.gson.Gson;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static com.atlassian.jira.rest.Dates.fromTimeString;
import static com.atlassian.jira.testkit.client.util.JsonMatchers.hasField;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.WEBHOOKS, Category.REFERENCE_PLUGIN})
public class TestWorklogWebHook extends AbstractWebHookTest {
    protected static final String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final long DEFAULT_SCHEME_ID = 0L;
    protected WorklogClient worklogClient;

    @Before
    @Override
    public void setUpTest() {
        super.setUpTest();
        backdoor.restoreBlankInstance();
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_TIMETRACKING, true);
        worklogClient = new WorklogClient(environmentData);
        backdoor.permissionSchemes().addEveryonePermission(0L, ProjectPermissions.EDIT_OWN_WORKLOGS);
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_SCHEME_ID, ProjectPermissions.DELETE_OWN_WORKLOGS);
    }

    @Test
    public void testWorklogCreatedWebHook() throws Exception {
        final String webHookName = "worklog_created";
        registerWebHook(webHookName);

        IssueCreateResponse issue = createIssue();
        Worklog worklog = createWorklog(issue);

        checkResponseForWebHook(webHookName, worklog);
    }

    @Test
    public void testWorklogUpdatedWebHook() throws Exception {

        final String webHookName = "worklog_updated";
        registerWebHook(webHookName);

        IssueCreateResponse issue = createIssue();
        Worklog worklog = createWorklog(issue);
        Worklog updatedWorklog = updateWorklog(issue, worklog);

        checkResponseForWebHook(webHookName, updatedWorklog);
    }

    @Test
    public void testWorklogDeletedWebHook() throws Exception {
        final String webHookName = "worklog_deleted";
        registerWebHook(webHookName);

        IssueCreateResponse issue = createIssue();
        Worklog worklog = createWorklog(issue);
        deleteWorklog(issue, worklog);

        checkResponseForWebHook(webHookName, worklog);
    }

    @Test
    public void testWorklogWebHookWorksWhenRegisteredWithModuleDescriptor() throws Exception {
        final String webHookCreatedName = "worklog_created";
        final String webHookUpdatedName = "worklog_updated";
        final String webHookDeletedName = "worklog_deleted";

        IssueCreateResponse issue = createIssue();
        Worklog worklog = createWorklog(issue);
        checkWebHookNameRegisteredInModuleDescriptor(webHookCreatedName, worklog);

        Worklog updatedWorklog = updateWorklog(issue, worklog);
        checkWebHookNameRegisteredInModuleDescriptor(webHookUpdatedName, updatedWorklog);

        deleteWorklog(issue, worklog);
        checkWebHookNameRegisteredInModuleDescriptor(webHookDeletedName, updatedWorklog);
    }

    @Test
    public void testWorklogUpdatedInIssue() throws Exception {
        final String webHookName = "jira:worklog_updated";
        registerWebHook(webHookName);

        IssueCreateResponse issue = createIssue();
        Worklog worklog = createWorklog(issue);
        checkWebHookName(webHookName);

        updateWorklog(issue, worklog);
        checkWebHookName(webHookName);

        deleteWorklog(issue, worklog);
        checkWebHookName(webHookName);
    }

    @Test
    public void testWorklogDeletedWebHooksSentWhenIssueDeleted() throws Exception {
        final String webHookName = "worklog_deleted";
        registerWebHook(webHookName);

        IssueCreateResponse issue = createIssue();
        Worklog worklog = createWorklog(issue);

        backdoor.issues().deleteIssue(issue.key, true);

        checkResponseForWebHook(webHookName, worklog);
    }

    private IssueCreateResponse createIssue() {
        return backdoor.issues().createIssue("HSP", "Issue summary");
    }

    protected Worklog createWorklog(final IssueCreateResponse issue) {
        final Worklog worklog = new Worklog();
        worklog.started = new SimpleDateFormat(TIME_FORMAT).format(new Date());
        worklog.timeSpent = "1h";
        worklog.comment = "comment";
        return worklogClient.post(issue.key, worklog).body;
    }

    private Worklog updateWorklog(final IssueCreateResponse issue, final Worklog worklog) {
        worklog.comment = "new comment";
        worklog.timeSpentSeconds = null;
        return worklogClient.put(issue.key, worklog).body;
    }

    private void deleteWorklog(final IssueCreateResponse issue, final Worklog worklog) {
        worklogClient.delete(issue.key, worklog);
    }

    private void checkResponseForWebHook(String eventName, Worklog worklog) throws Exception {
        JSONObject event = getWebHookResponseDataFromDataBase();
        assertThat(event, hasField("webhookEvent").equalTo(eventName));
        WorklogJsonBean worklogFromWebHook = new Gson().fromJson(event.getJSONObject("worklog").toString(), WorklogJsonBean.class);
        assertThat(worklogFromWebHook, isWorklog(worklog));
    }

    private void checkWebHookNameRegisteredInModuleDescriptor(final String eventName, final Worklog worklog)
            throws JSONException {
        final JSONObject event = new JSONObject(getWebHookResponseFromTestServlet(eventName));
        assertThat(event, hasField("webhookEvent").equalTo(eventName));
        WorklogJsonBean worklogFromWebHook = new Gson().fromJson(event.getJSONObject("worklog").toString(), WorklogJsonBean.class);
        assertThat(worklogFromWebHook, isWorklog(worklog));
    }

    private void checkWebHookName(final String eventName) throws Exception {
        JSONObject event = getWebHookResponseDataFromDataBase();
        assertThat(event, hasField("webhookEvent").equalTo(eventName));
    }

    private JSONObject getWebHookResponseDataFromDataBase() throws Exception {
        final WebHookResponseData responseData = getWebHookResponse();
        assertThat(responseData, notNullValue());
        assertThat(responseTester.getResponseData(), nullValue());
        return new JSONObject(responseData.getJson());
    }

    public static Matcher<WorklogJsonBean> isWorklog(final Worklog worklog) {
        return new TypeSafeMatcher<WorklogJsonBean>() {
            @Override
            protected boolean matchesSafely(WorklogJsonBean item) {
                //noinspection unchecked
                return allOf(
                        hasProperty("id", equalTo(worklog.id)),
                        hasProperty("issueId", equalTo(worklog.issueId)),
                        hasProperty("comment", equalTo(worklog.comment)),
                        hasProperty("updated", equalAsLocalDate(fromTimeString(worklog.updated))),
                        hasProperty("created", equalAsLocalDate(fromTimeString(worklog.created))),
                        hasProperty("started", equalAsLocalDate(fromTimeString(worklog.started))),
                        hasProperty("timeSpent", equalTo(worklog.timeSpent))
                ).matches(item);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(String.format("(id=%s, author=%s, updateAuthor=%s, comment=%s, updated=%s, created=%s, started=%s, timeSpent=%s)",
                        worklog.id, worklog.author, worklog.updateAuthor, worklog.comment, worklog.updated, worklog.created, worklog.started, worklog.timeSpent));
            }

        };
    }

    private static Matcher<Date> equalAsLocalDate(Date date) {
        return new TypeSafeMatcher<Date>() {
            @Override
            protected boolean matchesSafely(Date item) {
                return toLocalState(item).equals(toLocalState(date));
            }

            @Override
            public void describeTo(Description description) {
                final DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                description.appendText(format.format(date));
            }
        };
    }

    private static LocalDate toLocalState(Date date) {
        Instant instant = Instant.ofEpochMilli(date.getTime());
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();

    }
}
