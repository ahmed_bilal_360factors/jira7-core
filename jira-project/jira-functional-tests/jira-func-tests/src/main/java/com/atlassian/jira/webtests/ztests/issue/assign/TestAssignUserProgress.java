package com.atlassian.jira.webtests.ztests.issue.assign;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_EMAIL;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;

@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
@LoginAs(user = ADMIN_USERNAME)
public class TestAssignUserProgress extends BaseJiraFuncTest {
    public static final String TEST_SUMMARY = "testing progress inconsistency";
    public static final String IN_PROGRESS = "In Progress";

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
        backdoor.darkFeatures().enableForSite("no.frother.assignee.field");
    }

    @After
    public void tearDownTest() {
        backdoor.darkFeatures().disableForSite("no.frother.assignee.field");
    }

    @Test
    public void testChangeProgressWithAssign() {
        backdoor.usersAndGroups().addUser(BOB_USERNAME, BOB_PASSWORD, BOB_FULLNAME, BOB_EMAIL);
        backdoor.usersAndGroups().addUserToGroup(BOB_USERNAME, "jira-developers");

        administration.project().addProject(PROJECT_HOMOSAP, PROJECT_HOMOSAP_KEY, ADMIN_USERNAME);
        final String key = navigation.issue().createIssue(PROJECT_HOMOSAP, ISSUE_TYPE_BUG, TEST_SUMMARY);

        navigation.issue().viewIssue(key);
        tester.clickLinkWithText("Start Progress");
        navigation.issue().assignIssue(key, TEST_SUMMARY, BOB_FULLNAME);

        textAssertions.assertTextPresent(locator.page(), IN_PROGRESS);
    }

    @Test
    public void testChangeProgressWithEdit() {
        backdoor.usersAndGroups().addUser(BOB_USERNAME, BOB_PASSWORD, BOB_FULLNAME, BOB_EMAIL);
        backdoor.usersAndGroups().addUserToGroup(BOB_USERNAME, "jira-developers");

        administration.project().addProject(PROJECT_HOMOSAP, PROJECT_HOMOSAP_KEY, ADMIN_USERNAME);
        final String key = navigation.issue().createIssue(PROJECT_HOMOSAP, ISSUE_TYPE_BUG, TEST_SUMMARY);

        navigation.issue().viewIssue(key);
        tester.clickLinkWithText("Start Progress");

        tester.clickLink("edit-issue");
        tester.selectOption("assignee", BOB_FULLNAME);
        tester.submit("Update");

        textAssertions.assertTextPresent(locator.page(), IN_PROGRESS);
    }
}
