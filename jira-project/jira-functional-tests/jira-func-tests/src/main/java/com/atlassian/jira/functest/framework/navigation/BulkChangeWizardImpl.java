package com.atlassian.jira.functest.framework.navigation;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.util.ProgressPageControl;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;

import java.util.EnumSet;

import static com.atlassian.jira.functest.framework.BulkOperations.SAME_FOR_ALL;
import static com.atlassian.jira.functest.framework.NavigationImpl.BUTTON_NEXT;

/**
 * Implementation of the Bulk Change Wizard for Functional Tests. Works with basic cases of Bulk Move and Bulk Edit, but
 * it needs improvement to work for other things!
 * <p/>
 * Stateful. Not injected into test classes.
 *
 * @since v4.2
 */
public class BulkChangeWizardImpl implements BulkChangeWizard {
    private final WebTester tester;
    private final IssueNavigation issueNavigation;

    public BulkChangeWizardImpl(WebTester tester, JIRAEnvironmentData environmentData) {
        this.tester = tester;
        this.issueNavigation = new IssueNavigationImpl(tester, environmentData);
    }

    protected static final String BULK_EDIT_KEY = "10000_1_";
    protected static final String TARGET_PROJECT_ID = "10000_1_pid";
    protected static final String ISSUE_TYPE_SELECT = "10000_1_issuetype";
    protected static final String TARGET_PROJECT_ID_TEMPLATE = "10000_%d_pid";

    private WizardState state = WizardState.SELECT_ISSUES;
    private BulkOperations operation = null;

    @Override
    public BulkChangeWizard selectAllIssues() {
        validateState(WizardState.SELECT_ISSUES);

        selectAllIssueCheckboxes();
        clickOnNext();

        state = WizardState.CHOOSE_OPERATION;

        return this;
    }

    @Override
    public BulkChangeWizard chooseOperation(final BulkOperations operation) {
        validateState(WizardState.CHOOSE_OPERATION);

        chooseOperationRadioButton(operation);
        clickOnNext();

        this.operation = operation;

        if (operation.getRadioValue().equals(BulkOperationsImpl.MOVE.getRadioValue())) {
            state = WizardState.CHOOSE_TARGET_CONTEXTS;
        } else if (operation.getRadioValue().equals(BulkOperationsImpl.EDIT.getRadioValue())) {
            state = WizardState.SET_FIELDS;
        } else if (operation.getRadioValue().equals(BulkOperationsImpl.DELETE.getRadioValue())) {
            state = WizardState.CONFIRMATION;
        }


        return this;
    }

    @Override
    public BulkChangeWizard chooseWorkflowTransition(BulkOperations workflowTransition) {
        validateState(WizardState.CHOOSE_OPERATION);

        chooseCustomRadioButton(FunctTestConstants.FIELD_WORKFLOW, workflowTransition.getRadioValue());
        clickOnNext();

        this.operation = workflowTransition;

        return this;
    }

    @Override
    public BulkChangeWizard chooseTargetContextForAll(final String projectName) {
        return chooseTargetContextForAll(projectName, null);
    }

    @Override
    public BulkChangeWizard chooseTargetContextForAll(String projectName, String issueType) {
        validateState(BulkOperationsImpl.MOVE, WizardState.CHOOSE_TARGET_CONTEXTS);

        // note that this only currently works when you are moving issues from Homosapien project, and when that is the
        // first project context offered on the page. Might need to fix it if the data is different!
        checkSameTargetForAllCheckbox();
        selectFirstTargetProject(projectName);
        if (issueType != null && !issueType.isEmpty()) {
            selectIssueType(issueType);
        }
        clickOnNext();

        state = WizardState.SET_FIELDS;

        return this;
    }

    @Override
    public BulkChangeWizard chooseTargetContextForEach(final int numContextsToSelect, final String projectName) {
        validateState(BulkOperationsImpl.MOVE, WizardState.CHOOSE_TARGET_CONTEXTS);

        // note that this only currently works when you are moving issues from Homosapien project, and when that is the
        // only project context offered on the page. Might need to fix it if the data is different!
        selectEachTargetProject(numContextsToSelect, projectName);
        clickOnNext();

        state = WizardState.SET_FIELDS;

        return this;
    }

    @Override
    public BulkChangeWizard setFieldValue(final String fieldName, final String value) {
        return setFieldValue(InputTypes.TEXT, fieldName, value);
    }

    @Override
    public BulkChangeWizard setFieldValue(final InputTypes inputType, final String fieldName, final String value) {
        validateState(WizardState.SET_FIELDS);

        switch (inputType) {
            case SELECT:
                setSelectElement(fieldName, value);
                break;
            default:
                setTextElement(fieldName, value);
        }

        return this;
    }

    @Override
    public BulkChangeWizard checkRetainForField(final String fieldName) {
        validateState(BulkOperationsImpl.MOVE, WizardState.SET_FIELDS);

        checkCheckbox("retain_" + fieldName);

        return this;
    }

    @Override
    public BulkChangeWizard checkActionForField(final String fieldName) {
        validateState(BulkOperationsImpl.EDIT, WizardState.SET_FIELDS);

        checkCheckbox("actions", fieldName);

        return this;
    }

    @Override
    public BulkChangeWizard finaliseFields() {
        validateState(WizardState.SET_FIELDS);

        clickOnNext();

        // check to see if we have any more fields to set
        if (operation == BulkOperationsImpl.MOVE || operation == BulkOperationsImpl.EDIT) {
            // this particular text appears at the top of the "Issue Fields" screen
            if (pageContainsText("Step 4 of 4: Confirmation")) {
                this.state = WizardState.CONFIRMATION;
            }
        }

        return this;
    }

    @Override
    public BulkChangeWizard complete() {
        validateState(WizardState.CONFIRMATION);

        if (operation == BulkOperationsImpl.MOVE) {
            clickOnNext();
        } else if (operation == BulkOperationsImpl.DELETE || operation == BulkOperationsImpl.EDIT) {
            clickOnConfirm();
        }

        this.state = WizardState.COMPLETE;

        return this;
    }

    @Override
    public BulkChangeWizard waitForBulkChangeCompletion() {
        validateState(WizardState.COMPLETE);

        tester.assertTextPresent("Bulk Operation Progress");
        ProgressPageControl.waitAndReload(tester, "bulkoperationprogressform", "Refresh", "Acknowledge");

        return this;
    }

    @Override
    public BulkChangeWizard revertTo(final WizardState state) {
        if (this.state.getStage() <= state.getStage()) {
            throw new IllegalStateException(String.format("Cannot revert from state: %s to state: %s: target state does"
                    + " not precede the current state.", this.state, state));
        }
        switch (state) {
            case CHOOSE_TARGET_CONTEXTS:
                // This state is native only to move operation.
                if (!operation.equals(BulkOperationsImpl.MOVE)) {
                    throw new IllegalStateException(illegalStateForOperation(state, this.operation));
                }
                break;
            case SET_FIELDS:
                if (EnumSet.of(BulkOperationsImpl.MOVE, BulkOperationsImpl.DELETE).contains(this.operation)) {
                    throw new IllegalStateException(illegalStateForOperation(state, this.operation));
                }
                break;
            case SELECT_ISSUES:
            case CHOOSE_OPERATION:
                this.operation = null;
                break;
            case COMPLETE:
            case CONFIRMATION:
                throw new IllegalStateException(String.format("Cannot revert to state: %s.", state));
        }
        clickOnLinkWithText(state.getLinkText());
        this.state = state;
        return this;
    }

    @Override
    public BulkChangeWizard cancel() {
        clickOnLinkId("cancel");
        this.state = WizardState.COMPLETE;
        return this;
    }

    private String illegalStateForOperation(final WizardState state, final BulkOperations operation) {
        return String.format("Cannot revert to: %s: Operation %s does not this state.", state, operation);
    }


    private void validateState(final WizardState expectedState) {
        if (this.state != expectedState) {
            throw new IllegalStateException("Wizard is in invalid state. Expected state: " + expectedState + "; actual state: " + state.toString());
        }
    }

    private void validateState(final BulkOperations expectedOperation, final WizardState expectedState) {
        if (this.operation != expectedOperation) {
            throw new IllegalStateException("Wizard is in invalid state. Expected operation: " + expectedOperation + "; actual operation: " + expectedOperation.toString());
        }

        validateState(expectedState);
    }

    public WizardState getState() {
        return state;
    }

    protected void clickOnNext() {
        tester.submit(BUTTON_NEXT);
    }

    protected void clickOnConfirm() {
        tester.submit("Confirm");
    }

    protected void selectAllIssueCheckboxes() {
        tester.setWorkingForm("bulkedit");
        final String[] paramNames = tester.getDialog().getForm().getParameterNames();
        for (String paramName : paramNames) {
            if (paramName.startsWith("bulkedit_")) {
                tester.checkCheckbox(paramName);
            }
        }
    }

    protected void chooseOperationRadioButton(final BulkOperations operation) {
        setTextElement(FunctTestConstants.FIELD_OPERATION, operation.getRadioValue());
    }

    protected void chooseCustomRadioButton(String radiogroupName, String radiobuttonValue) {
        setTextElement(radiogroupName, radiobuttonValue);
    }

    protected void selectFirstTargetProject(final String projectName) {
        issueNavigation.selectProject(projectName, TARGET_PROJECT_ID);
    }

    protected void selectIssueType(final String issueType) {
        issueNavigation.selectIssueType(issueType, ISSUE_TYPE_SELECT);
    }

    protected void selectEachTargetProject(final int numContextsToSelect, final String projectName) {
        for (int i = 1; i <= numContextsToSelect; i++) {
            issueNavigation.selectProject(projectName, String.format(TARGET_PROJECT_ID_TEMPLATE, i));
        }
    }

    protected void checkSameTargetForAllCheckbox() {
        tester.checkCheckbox(SAME_FOR_ALL, BULK_EDIT_KEY);
    }

    protected void setTextElement(final String fieldName, final String value) {
        tester.setFormElement(fieldName, value);
    }

    protected void setSelectElement(final String fieldName, final String value) {
        tester.setFormElement(fieldName, value);
    }

    protected void checkCheckbox(final String fieldName) {
        tester.checkCheckbox(fieldName);
    }

    protected void checkCheckbox(final String checkboxName, final String value) {
        tester.checkCheckbox(checkboxName, value);
    }

    protected boolean pageContainsText(final String text) {
        // regular expression here ensures that words from matched text can be separated by
        // any whitespace(s)
        return tester.getDialog().getResponseText().matches("(?s).*" + text.replace(" ", "\\s*") + ".*");
    }

    protected void clickOnLinkId(final String id) {
        tester.clickLink(id);
    }

    protected void clickOnLinkWithText(final String linkText) {
        tester.clickLinkWithText(linkText);
    }
}
