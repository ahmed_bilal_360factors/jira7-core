package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 *
 */
@WebTest({Category.FUNC_TEST, Category.BROWSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestReleaseNotes extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testHtmlEscaping() throws IOException {
        administration.restoreData("TestReleaseNotes.xml");

        tester.gotoPage("/ConfigureReleaseNote.jspa?projectId=10000");
        tester.selectOption("version", "New Version 4");
        tester.selectOption("styleName", "Html");
        tester.submit("Create");
        //text in the bottom text area should be doubly escaped so users can copy/paste straight from the web browser (JRA-12184)
        textAssertions.assertTextPresent("Release Notes - &amp;quot;homosapien&amp;quot; - Version New Version 4");
        textAssertions.assertTextPresent("Bugs &amp;amp; Things");

        String baseUrl = getEnvironmentData().getBaseUrl().toString();
        if (!baseUrl.endsWith("/")) {
            baseUrl += "/";
        }
        textAssertions.assertTextPresent("<li>[<a href='" + baseUrl + "browse/HSP-1'>HSP-1</a>] -         Chevrons - &amp;gt;&amp;gt;&amp;gt; &amp;lt;&amp;lt;&amp;lt;");
        textAssertions.assertTextPresent("<li>[<a href='" + baseUrl + "browse/HSP-2'>HSP-2</a>] -         Ampersands - &amp;amp; &amp;amp; &amp;amp; &amp;amp; &amp;amp;");
        textAssertions.assertTextPresent("<li>[<a href='" + baseUrl + "browse/HSP-3'>HSP-3</a>] -         Quotes - &amp;quot; &amp;quot; &amp;quot; &amp;#39; &amp;#39; &amp;#39;  &amp;quot; &amp;quot; &amp;quot;");
    }
}
