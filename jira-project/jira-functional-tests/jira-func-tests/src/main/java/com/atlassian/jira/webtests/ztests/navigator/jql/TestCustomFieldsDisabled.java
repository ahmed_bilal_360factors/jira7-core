package com.atlassian.jira.webtests.ztests.navigator.jql;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.After;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Verify the behaviour of custom fields in JQL queries when they have been disabled.
 *
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.JQL})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestCustomFieldsDisabled extends BaseJiraFuncTest {
    @Inject
    private IssueTableAssertions issueTableAssertions;
    @Inject
    private Administration administration;

    @After
    public void tearDownTest() {
        administration.plugins().enablePlugin("com.atlassian.jira.plugin.system.customfieldtypes");
    }

    @Test
    public void testDisable() throws Exception {
        administration.restoreData("TestCustomFieldOperators.xml");

        final String[] customFields = new String[]{
                "CSF",
                "DP",
                "DT",
                "FTF",
                "GP",
                "II",
                "MC",
                "MGP",
                "MS",
                "MUP",
                "NF",
                "PP",
                "RB",
                "ROTF",
                "SL",
                "SVP",
                "TF",
                "UP",
                "URL",
                "VP"
        };

        for (String cf : customFields) {
            navigation.issueNavigator().createSearch(String.format("%s is not empty", cf));
            assertions.getIssueNavigatorAssertions().assertNoJqlErrors();
        }

        navigation.gotoAdmin();
        administration.plugins().disablePlugin("com.atlassian.jira.plugin.system.customfieldtypes");

        for (String cf : customFields) {
            issueTableAssertions.assertSearchWithError(String.format("%s is not empty", cf), String.format("Field '%s' does not exist or you do not have permission to view it.", cf));
        }
    }
}
