package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

/**
 * Verify that going to the JiraLockedError page when JIRA is up and functional takes you to a useful page
 * instead of getting a useless 404 from the NoopServlet
 *
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST})
@RestoreBlankInstance
public class TestJiraLockedError extends BaseJiraFuncTest {

    // JRA-18822 show a better page when JIRA is actually functional
    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testJiraLockedError() throws Exception {
        navigation.gotoPage("/JiraLockedError");
        // check for the expected informative messages
        textAssertions.assertTextPresent(new XPathLocator(tester, "//h1"), "JIRA Startup Succeeded");
        textAssertions.assertTextPresent(locator.id("noerrors"), "No startup errors detected at present.");
        // make sure there is a link back to the dashboard
        tester.assertLinkPresent("context-path");
    }
}
