package com.atlassian.jira.webtests.ztests;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.LicenseKeys;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertTrue;

@WebTest({Category.FUNC_TEST, Category.IMPORT_EXPORT})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestXmlRestore extends BaseJiraFuncTest {

    // JRA-14662: when restoring data which does not include the currently logged in user, do not throw an exception
    @Inject
    private Administration administration;

    @Test
    public void testRestoreDataWhereCurrentUserDoesntExist() {
        administration.restoreBlankInstance();
        backdoor.usersAndGroups().addUser("idontexist");
        backdoor.usersAndGroups().addUserToGroup("idontexist", "jira-administrators");
        navigation.logout();
        navigation.login("idontexist");

        // restore data again
        getTester().gotoPage("secure/admin/XmlRestore!default.jspa");
        getTester().setWorkingForm("restore-xml-data-backup");

        String filePath = getEnvironmentData().getXMLDataLocation().getAbsolutePath() + "/blankprojects.xml";
        getTester().setFormElement("filename", filePath);
        getTester().setFormElement("license", LicenseKeys.COMMERCIAL.getLicenseString());
        getTester().submit();
        administration.waitForRestore();
        getTester().assertTextPresent("Your import has been successful");
        getTester().assertTextNotPresent("NullPointerException");

        navigation.disableWebSudo();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);

        administration.generalConfiguration().setBaseUrl(getBaseUrl());
    }

    @Test
    public void testRestoreWithCustomPathsErrors() {
        try {
            administration.restoreData("TestXMLRestore.xml", false);
        } catch (Throwable e) {
            assertTrue("custom path should not be created", e.getMessage().startsWith("Failed to restore JIRA data. Cause: specified in the backup file is not valid"));
        }
        tester.assertTextPresent("specified in the backup file is not valid");
    }

    private String getBaseUrl() {
        return getEnvironmentData().getBaseUrl().toExternalForm();
    }
}
