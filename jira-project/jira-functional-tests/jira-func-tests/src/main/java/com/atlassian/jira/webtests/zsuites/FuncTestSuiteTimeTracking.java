package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.atlassian.jira.webtests.ztests.admin.TestTimeTrackingAdmin;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkMoveTimeTracking;
import com.atlassian.jira.webtests.ztests.issue.move.TestDeleteHiddenFieldOnMove;
import com.atlassian.jira.webtests.ztests.misc.TestReplacedLocalVelocityMacros;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Responsible for holding the time tracking module's functional tests.
 *
 * @since v4.0
 */
public class FuncTestSuiteTimeTracking {
    private final static String TIME_TRACKING_TESTS_PACKAGE = "com.atlassian.jira.webtests.ztests.timetracking";

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestTimeTrackingAdmin.class)
                .add(TestDeleteHiddenFieldOnMove.class)
                .add(TestBulkMoveTimeTracking.class)
                .addAll(FuncTestSuite.getTestClasses(TIME_TRACKING_TESTS_PACKAGE, true))
                .add(TestReplacedLocalVelocityMacros.class)
                .build();
    }
}