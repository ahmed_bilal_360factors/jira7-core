package com.atlassian.jira.webtests.ztests.customfield;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperations;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY;

@WebTest({Category.FUNC_TEST, Category.CUSTOM_FIELDS, Category.FIELDS})
@LoginAs(user = ADMIN_USERNAME)
public class TestCascadingSelectCustomField extends BaseJiraFuncTest {
    private static final String CUSTOMFIELD_10000 = "customfield_10000";
    private static final String CUSTOMFIELD_10000_OPTION = "customfield_10000:1";
    private static final String CUSTOMFIELD_10000_1 = "customfield_10000:1";
    private static final String CASCADING_SELECT_NAME = "asdf";
    private static final String UPDATE_FIELD_TEXT = "Update the fields of the issue to relate to the new project.";
    private static final String ERROR_PRESENT_FOR_BAD_OPTION = "The option 'a1' is invalid for parent option 'test'";

    private static final String HOMO_PID_OPTION = "10001_1_pid";
    private static final String MONKEY_PID_OPTION = "10000_1_pid";
    private static final String MOVE_ISSUES_CONFIRMATION_TEXT = "Confirmation";

//    private static final String FAILURE_ERROR_FOR_OPTION_SELECT = "The option 'p1' is invalid for parent option 'stuff'";

    @Inject
    private BulkOperations bulkOperations;

    @Inject
    private TextAssertions textAssertions;

    @Test
    @Restore("TestMoveIssueWithCascadingSelectCustomFieldWithDifferentContexts.xml")
    public void testMoveIssueWithCascadingSelectCustomFieldWithDifferentContexts() {
        navigation.issue().gotoIssue("HSP-1");

        tester.clickLink("move-issue");
        tester.setFormElement("pid", "10001");
        tester.submit("Next >>");

        tester.assertTextPresent(UPDATE_FIELD_TEXT);

        tester.assertTextPresent(CASCADING_SELECT_NAME);
        tester.selectOption(CUSTOMFIELD_10000, "test");
        tester.selectOption(CUSTOMFIELD_10000_OPTION, "t1");

        tester.submit("Next >>");

        tester.assertTextPresent("stuff");
        tester.assertTextPresent("s1");
        tester.assertTextPresent("test");
        tester.assertTextPresent("t1");

        tester.submit("Move");
        tester.assertTextPresent("test");
        tester.assertTextPresent("t1");
    }

    @Test
    @Restore("TestMoveIssueWithCascadingSelectCustomFieldWithDifferentContexts.xml")
    public void testMoveIssueWithCascadingSelectCustomFieldWithBadValues() {
        navigation.issue().gotoIssue("HSP-1");

        tester.clickLink("move-issue");
        tester.setFormElement("pid", "10001");
        tester.submit("Next >>");

        tester.assertTextPresent(UPDATE_FIELD_TEXT);

        tester.assertTextPresent(CASCADING_SELECT_NAME);
        tester.selectOption(CUSTOMFIELD_10000, "test");
        tester.selectOption(CUSTOMFIELD_10000_OPTION, "a1");

        tester.submit("Next >>");
        assertions.getTextAssertions().assertTextPresentHtmlEncoded(ERROR_PRESENT_FOR_BAD_OPTION);
    }

    @Test
    @Restore("TestBulkMoveWithCascadingSelectCustomFieldWithDifferentContexts.xml")
    public void testBulkMoveWithCascadingSelectCustomFieldWithDifferentContexts() {
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        bulkOperations.bulkChangeChooseIssuesAll();
        bulkOperations.chooseOperationBulkMove();

        navigation.issue().selectProject(PROJECT_MONKEY, MONKEY_PID_OPTION);
        navigation.issue().selectProject(PROJECT_HOMOSAP, HOMO_PID_OPTION);
        tester.submit("Next");
        // We need to be able to select the values for the project we are going to
        tester.selectOption(CUSTOMFIELD_10000, "test");
        tester.selectOption(CUSTOMFIELD_10000_1, "t1");
        tester.submit("Next");
        // We need to be able to select the values for the project we are going to
        tester.selectOption(CUSTOMFIELD_10000, "stuff");
        tester.selectOption(CUSTOMFIELD_10000_1, "s1");
        tester.submit("Next");

        // If we get the confirmation screen then we were presented with the right values
        // and were able to select them
        tester.assertTextPresent(MOVE_ISSUES_CONFIRMATION_TEXT);
    }

    @Test
    @Restore("TestBulkMoveWithCascadingSelectCustomFieldWithDifferentContexts.xml")
    public void testEditIssueWithCascadingSelectField() {
        navigation.issue().gotoIssue("HSP-1");

        tester.clickLink("edit-issue");
        tester.selectOption(CUSTOMFIELD_10000, "cranky");
        tester.selectOption(CUSTOMFIELD_10000_1, "p1");
        tester.submit("Update");

        tester.assertTextPresent("asdf");
        tester.assertTextPresent("cranky");
        tester.assertTextPresent("p1");

        // Assert validation is thrown for cascading select
        tester.clickLink("edit-issue");
        tester.selectOption(CUSTOMFIELD_10000, "stuff");
        tester.selectOption(CUSTOMFIELD_10000_1, "p1");
        tester.submit("Update");

        assertions.getTextAssertions().assertTextPresentHtmlEncoded("The option 'p1' is invalid for parent option 'stuff'");
    }

    @Test
    @Restore("TestMoveIssueWithCascadingSelectCustomFieldWithDifferentContexts.xml")
    public void testCreateIssueWithCascadingSelect() {
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");

        tester.setFormElement("summary", "Test summary");

        // Do the negative test
        tester.selectOption(CUSTOMFIELD_10000, "cranky");
        tester.selectOption(CUSTOMFIELD_10000_OPTION, "s1");

        tester.submit("Create");

        assertions.getTextAssertions().assertTextPresentHtmlEncoded("The option 's1' is invalid for parent option 'cranky'");

        tester.selectOption(CUSTOMFIELD_10000, "stuff");
        tester.selectOption(CUSTOMFIELD_10000_OPTION, "s1");

        tester.submit("Create");

        tester.assertTextPresent("asdf");
        tester.assertTextPresent("stuff");
        tester.assertTextPresent("s1");
    }

    @Test
    @Restore("TestMoveIssueWithCascadingSelectCustomFieldWithDifferentContexts.xml")
    public void testCreateIssueWithCascadingSelectChoosingOptionsNoneForParent() {
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");

        tester.setFormElement("summary", "Test summary");

        // Test that we can add with a value of none for the parent
        tester.selectOption(CUSTOMFIELD_10000, "None");

        tester.submit("Create");

        // Since we chose 'None' make sure the field is not displayed on the view issue screen.
        tester.assertTextNotPresent("asdf:");
    }

    @Test
    @Restore("TestMoveIssueWithCascadingSelectCustomFieldWithDifferentContexts.xml")
    public void testCreateIssueWithCascadingSelectChoosingOptionsNoneForChild() {
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");

        tester.setFormElement("summary", "Test summary");

        // Test that we can add with a value of none for the parent
        tester.selectOption(CUSTOMFIELD_10000, "stuff");
        tester.selectOption(CUSTOMFIELD_10000_OPTION, "None");

        tester.submit("Create");

        // Only the parent should be displayed
        tester.assertTextPresent("stuff");
    }

    @Test
    @Restore("TestMoveIssueWithCascadingSelectCustomFieldWithDifferentContexts.xml")
    public void testSetDefaultNoneOptionsForCascadingSelect() {
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        tester.clickLink("config_" + CUSTOMFIELD_10000);
        tester.clickLinkWithText("Edit Default Value");
        tester.selectOption(CUSTOMFIELD_10000, "None");

        tester.submit();
    }

    @Test
    @Restore("TestMoveIssueWithCascadingSelectCustomFieldWithDifferentContexts.xml")
    public void testIssueNavigatorSearchCascadingSelectWithAllOption() {
        navigation.issueNavigator().createSearch("project=homosapien");
        tester.assertTextPresent("This is a test bug for the subversion plugin");
    }

    //JRA-20554: Removing cascading option from Postgres 8.3+ does not work.
    @Test
    @Restore("TestDeleteOptionFromCascadingSelectCustomField.xml")
    public void testDeleteOptionFromFieldAtFirstLevelOnTheIssue() {
        removeOptions("10000", "10000", "HSP-1");
        tester.assertTextPresent("Edit Options for Custom Field");
        tester.assertTextNotPresent("Option_A");

        navigation.issue().viewIssue("HSP-1");

        assertions.assertNodeByIdDoesNotExist("rowForcustomfield_10000");
    }

    //JRA-20554: Removing cascading option from Postgres 8.3+ does not work.
    @Test
    @Restore("TestDeleteOptionFromCascadingSelectCustomField.xml")
    public void testDeleteOptionFromFieldAtSecondLevelOnTheIssue() {
        removeSubOptions(10000L, "Option_B", "10003", "HSP-3");

        textAssertions.assertTextPresent(new XPathLocator(tester, "//*[@class='instructions']"), "with parent option Option_B");
        tester.assertTextPresent("There are currently no options available for this select list");

        navigation.issue().viewIssue("HSP-3");

        textAssertions.assertTextPresent(new IdLocator(tester, "customfield_10000-val"), "Option_B");
    }

    //JRA-20554: Removing cascading option from Postgres 8.3+ does not work.
    @Test
    @Restore("TestDeleteOptionFromCascadingSelectCustomField.xml")
    public void testDeleteOptionFromFieldAtFirstLevelWithSubOptions() {
        removeOptions("10000", "10001", "HSP-3");

        tester.assertTextPresent("Edit Options for Custom Field");
        tester.assertTextNotPresent("Option_B");

        navigation.issue().viewIssue("HSP-3");

        assertions.assertNodeByIdDoesNotExist("rowForcustomfield_10000");
    }

    //JRA-20554: Removing cascading option from Postgres 8.3+ does not work.
    @Test
    @Restore("TestDeleteOptionFromCascadingSelectCustomField.xml")
    public void testDeleteOptionFromFieldWithNoAssociatedIssues() {
        removeOptions("10000", "10002", null);

        tester.assertTextPresent("Edit Options for Custom Field");
        tester.assertTextNotPresent("Option_C");
    }

    private void removeOptions(final String numericCustomFieldId, final String options, String issueAffected) {
        tester.gotoPage("/secure/admin/ConfigureCustomField!default.jspa?customFieldId=" + numericCustomFieldId);
        tester.clickLinkWithText("Edit Options");
        tester.clickLink("del_" + options);

        assertRemoveOptionConfirmationScreen(issueAffected);

        tester.submit("Delete");
    }

    private void removeSubOptions(final Long customFieldId, final String parentOption, final String optionToRemove, final String issueAffected) {
        tester.gotoPage("/secure/admin/ConfigureCustomField!default.jspa?customFieldId=" + customFieldId);
        tester.clickLinkWithText("Edit Options");
        tester.clickLinkWithText(parentOption);
        tester.clickLink("del_" + optionToRemove);

        assertRemoveOptionConfirmationScreen(issueAffected);

        tester.submit("Delete");
    }

    private void assertRemoveOptionConfirmationScreen(final String issueAffected) {
        final String issueCount = issueAffected == null ? "0" : "1";
        final List<String> textSequence = new ArrayList<String>();
        textSequence.add("Issues with this value");
        textSequence.add(issueCount);
        if (issueAffected != null) {
            textSequence.add(issueAffected);
        }
        textAssertions.assertTextSequence(new WebPageLocator(tester), textSequence.toArray(new String[textSequence.size()]));
    }
}
