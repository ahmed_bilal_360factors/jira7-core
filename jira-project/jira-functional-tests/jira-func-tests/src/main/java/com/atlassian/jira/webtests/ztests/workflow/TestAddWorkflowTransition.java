package com.atlassian.jira.webtests.ztests.workflow;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.beans.WorkflowSchemeData;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.WORKFLOW_SCHEME;

@WebTest({Category.FUNC_TEST, Category.WORKFLOW})
@LoginAs(user = ADMIN_USERNAME)
public class TestAddWorkflowTransition extends BaseJiraFuncTest {
    private static final String WORKFLOW_NAME = "Test Workflow";
    private static final String STEP_NAME_OPEN = "Open";
    private static final String STEP_NAME_AND = "SpecialChar&";
    private static final String STEP_NAME_LESSTHAN = "SpecialChar<";
    private static final String STEP_NAME_QUOTE = "SpecialChar\"";
    private static final String TRANSITION_NAME_AND = "To&";
    private static final String TRANSITION_NAME_LESSTHAN = "To<";
    private static final String TRANSITION_NAME_QUOTE = "To\"";
    private static final String ISSUE_STATUS_VALUE_ID = "status-val";

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("TestAddWorkflowTransition.xml");
    }

    @Test
    public void testAddWorkflowTransitionSpecialCharacters() {
        administration.workflows().goTo().workflowSteps(WORKFLOW_NAME).
                add(STEP_NAME_AND, null).
                add(STEP_NAME_LESSTHAN, null).
                add(STEP_NAME_QUOTE, null).
                addTransition(STEP_NAME_OPEN, TRANSITION_NAME_AND, null, STEP_NAME_AND, null).
                addTransition(STEP_NAME_AND, TRANSITION_NAME_LESSTHAN, null, STEP_NAME_LESSTHAN, null).
                addTransition(STEP_NAME_LESSTHAN, TRANSITION_NAME_QUOTE, null, STEP_NAME_QUOTE, null);

        // Associate the project with the new workflow
        backdoor.workflowSchemes().createScheme(new WorkflowSchemeData().setName(WORKFLOW_SCHEME).setDescription("Test workflow scheme.").setMapping(ISSUE_BUG, WORKFLOW_NAME));

        administration.project().associateWorkflowScheme(PROJECT_HOMOSAP, WORKFLOW_SCHEME);

        navigation.issue().createIssue(PROJECT_HOMOSAP, ISSUE_TYPE_BUG, "Test Special Characters");
        transitionCurrentIssueTo(TRANSITION_NAME_AND);
        transitionCurrentIssueTo(TRANSITION_NAME_LESSTHAN);
        transitionCurrentIssueTo(TRANSITION_NAME_QUOTE);
        assertCurrentIssueIsResolved();
    }

    private void transitionCurrentIssueTo(String transitionName) {
        navigation.clickLinkWithExactText(transitionName);
    }

    private void assertCurrentIssueIsResolved() {
        textAssertions.assertTextPresent(locator.id(ISSUE_STATUS_VALUE_ID), "Resolved");
    }
}
