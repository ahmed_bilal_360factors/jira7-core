package com.atlassian.jira.webtests.ztests.bulk;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperations;
import com.atlassian.jira.functest.framework.HttpUnitConfiguration;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.meterware.httpunit.WebForm;
import com.meterware.httpunit.WebTable;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_OPERATION;
import static com.atlassian.jira.functest.framework.FunctTestConstants.RADIO_OPERATION_EDIT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.RADIO_OPERATION_WORKFLOW;
import static org.junit.Assert.assertTrue;

@WebTest({Category.FUNC_TEST, Category.BULK_OPERATIONS, Category.FIELDS})
@HttpUnitConfiguration(enableScripting = true)
@Restore("TestBulkOperationCustomField.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestBulkOperationCustomField extends BaseJiraFuncTest {
    private static final String TABLE_EDITFIELDS_ID = "screen-tab-1-editfields";
    private static final String CHANGE_CF_FOR_PROJECT_A = "Change CF for project A";
    private static final String CHANGE_CF_FOR_PROJECT_B = "Change CF for project B";
    private static final String FIELD_NOT_AVAILABLE = "The field is not available for all issues with the same configuration.";
    private static final String TABLE_UNAVAILABLE_ACTIONS_ID = "unavailableActionsTable";

    /**
     * Tests the availability of customfields with different contexts in bulk edit
     */
    @Inject
    private BulkOperations bulkOperations;

    @Test
    public void testBulkEdit() {
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        selectAllIssuesToEdit();
        tester.submit("Next");
        tester.checkCheckbox(FIELD_OPERATION, RADIO_OPERATION_EDIT);
        tester.assertRadioOptionSelected(FIELD_OPERATION, RADIO_OPERATION_EDIT);
        tester.submit("Next");

        tester.assertTextNotInElement(TABLE_UNAVAILABLE_ACTIONS_ID, CHANGE_CF_FOR_PROJECT_A);
        tester.assertTextNotInElement(TABLE_UNAVAILABLE_ACTIONS_ID, CHANGE_CF_FOR_PROJECT_B);
        tester.assertTextNotInElement(TABLE_UNAVAILABLE_ACTIONS_ID, FIELD_NOT_AVAILABLE);
    }

    private void selectAllIssuesToEdit() {
        tester.setWorkingForm("bulkedit");
        WebForm form = tester.getDialog().getForm();
        String[] parameterNames = form.getParameterNames();
        for (String name : parameterNames) {
            if (name.startsWith("bulkedit_")) {
                tester.checkCheckbox(name);
            }
        }
    }

    /**
     * Tests the availability of customfields with different contexts in bulk workflow transition
     */
    @Test
    public void testBulkTransition() {
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        selectAllIssuesToEdit();
        tester.submit("Next");
        tester.checkCheckbox(FIELD_OPERATION, RADIO_OPERATION_WORKFLOW);
        tester.assertRadioOptionSelected(FIELD_OPERATION, RADIO_OPERATION_WORKFLOW);
        tester.submit("Next");

        tester.checkCheckbox("wftransition", "classic default workflow_5_5"); //resolve issue
        tester.submit("Next");

        try {
            WebTable editFieldsTable = tester.getDialog().getResponse().getTableWithID(TABLE_EDITFIELDS_ID);
            tester.assertTextInTable(TABLE_EDITFIELDS_ID, CHANGE_CF_FOR_PROJECT_A);
            tester.assertTextInTable(TABLE_EDITFIELDS_ID, CHANGE_CF_FOR_PROJECT_B);
            tester.assertTextInTable(TABLE_EDITFIELDS_ID, FIELD_NOT_AVAILABLE);
            for (int i = 0; i < editFieldsTable.getRowCount(); i++) {
                //skip the first column (checkbox and 'N/A')
                String field = editFieldsTable.getCellAsText(i, 1).trim();
                if (field.equals(CHANGE_CF_FOR_PROJECT_A)) {
                    assertTrue(editFieldsTable.getCellAsText(i, 2).trim().contains(FIELD_NOT_AVAILABLE));
                } else if (field.equals(CHANGE_CF_FOR_PROJECT_B)) {
                    assertTrue(editFieldsTable.getCellAsText(i, 2).trim().contains(FIELD_NOT_AVAILABLE));
                }
            }
        } catch (SAXException e) {
            //ignore
        }
    }
}
