package com.atlassian.jira.functest.rule;

import com.atlassian.jira.functest.framework.FuncTestWebClientListener;
import com.atlassian.jira.functest.framework.dump.TestInformationKit;
import com.atlassian.jira.functest.framework.log.LogOnBothSides;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import javax.inject.Inject;
import java.util.function.Supplier;

/**
 * Rule which adds logging before and after test execution.
 *
 * @since v6.5
 */
public class LogTimeRule implements TestRule {
    private final Supplier<JIRAEnvironmentData> environmentDataSupplier;
    private final Supplier<FuncTestWebClientListener> webClientListenerSupplier;
    private final Supplier<Backdoor> testkitSupplier;

    public LogTimeRule(final Supplier<JIRAEnvironmentData> environmentDataSupplier, final Supplier<FuncTestWebClientListener> webClientListenerSupplier, final Supplier<Backdoor> testkitSupplier) {
        this.environmentDataSupplier = environmentDataSupplier;
        this.webClientListenerSupplier = webClientListenerSupplier;
        this.testkitSupplier = testkitSupplier;
    }

    @Inject
    public LogTimeRule(final JIRAEnvironmentData environmentDataSupplier, final Backdoor testkitSupplier) {
        this.environmentDataSupplier = () -> environmentDataSupplier;
        // FuncTestWebClientListener is not available normally - if it is then the other constructor should be used
        this.webClientListenerSupplier = () -> null;
        this.testkitSupplier = () -> testkitSupplier;
    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {

            @Override
            public void evaluate() throws Throwable {
                final long startTime = System.currentTimeMillis();
                //We need to get this through the method because an environment may not have been set.
                final JIRAEnvironmentData data = environmentDataSupplier.get();
                final Backdoor testkit = testkitSupplier.get();
                LogOnBothSides.log(testkit, TestInformationKit.getStartMsg(description, data.getTenant()));
                final FuncTestWebClientListener webClientListener = webClientListenerSupplier.get();
                try {
                    base.evaluate();
                    LogOnBothSides.log(testkit, TestInformationKit.getEndMsg(description, data.getTenant(), System.currentTimeMillis() - startTime, webClientListener));
                } catch (final Throwable throwable) {
                    LogOnBothSides.log(testkit, TestInformationKit.getEndMsg(description, data.getTenant(), System.currentTimeMillis() - startTime, webClientListener, throwable));
                    throw throwable;
                }
            }
        };
    }
}
