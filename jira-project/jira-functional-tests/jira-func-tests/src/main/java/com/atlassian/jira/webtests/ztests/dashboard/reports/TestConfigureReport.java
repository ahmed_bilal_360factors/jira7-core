package com.atlassian.jira.webtests.ztests.dashboard.reports;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import javax.inject.Inject;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.REPORTS})
@LoginAs(user = ADMIN_USERNAME)
public class TestConfigureReport extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
    }

    /**
     * JRA-13939
     */
    @Test
    public void testConfigureReportWithInvalidReportKey() {
        //first go to a valid configure report URL.
        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:singlelevelgroupby");
        tester.assertTextPresent("Report: Single Level Group By Report");
        //now lets go to an invalid URL as may be submitted by a shit crawler
        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&amp;reportKey=com.atlassian.jira.jira-core-reports-plugin:singlelevelgroupby");
        assertions.forms().assertFormErrMsg("No report was specified.  The 'reportKey' parameter was empty.");

        //lets try to execute a good report.
        tester.gotoPage("/secure/ConfigureReport.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:singlelevelgroupby");
        //fails because not filters are setup.
        textAssertions.assertTextSequence(new WebPageLocator(tester),
                new String[]{"Report:", "Single Level Group By Report",
                        "Filter is a required field"});

        //lets try to execute a report with a stuffed URL.
        tester.gotoPage("/secure/ConfigureReport.jspa?selectedProjectId=10000&amp;reportKey=com.atlassian.jira.jira-core-reports-plugin:singlelevelgroupby");
        assertions.forms().assertFormErrMsg("No report was specified.  The 'reportKey' parameter was empty.");
    }

    @Test
    public void testConfigureReportWithMissingReportKey() {
        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=plugin.key:module.key");
        assertions.forms().assertFormErrMsg("Found no available report with key 'plugin.key:module.key'");
    }

    @Test
    public void testSidebarIsPresentWhenThereIsProjectContext() {
        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:singlelevelgroupby");
        tester.assertTextPresent("Report: Single Level Group By Report");
        tester.assertLinkPresentWithText("Issues");
        tester.assertLinkPresentWithText("Reports");
        tester.assertLinkPresentWithText("Versions");
        tester.assertLinkPresentWithText("Components");
    }

    @Test
    public void testSidebarIsNotPresentWhenThereIsNoProjectContext() {
        navigation.logout();
        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:singlelevelgroupby");
        tester.assertTextPresent("Report: Single Level Group By Report");
        tester.assertLinkNotPresentWithText("Issues");
        tester.assertLinkNotPresentWithText("Reports");
        tester.assertLinkNotPresentWithText("Versions");
        tester.assertLinkNotPresentWithText("Components");
    }

    @Test
    public void testRedirectsOldReportsKeysToTheCoreReportsPlugin() {
        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.plugin.system.reports:singlelevelgroupby");

        assertions.getURLAssertions().assertCurrentURLEndsWith("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin%3Asinglelevelgroupby");
        tester.assertTextPresent("Report: Single Level Group By Report");
    }

    @Test
    public void testDisplaysErrorWhenReportKeyDoesNotExist() {
        tester.gotoPage("/secure/ConfigureReport!default.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:notfound");

        tester.assertTextPresent("Configure Report - Error");
    }
}
