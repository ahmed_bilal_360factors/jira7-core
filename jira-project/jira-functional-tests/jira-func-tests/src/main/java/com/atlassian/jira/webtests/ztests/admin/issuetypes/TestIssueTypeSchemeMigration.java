package com.atlassian.jira.webtests.ztests.admin.issuetypes;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperations;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.rule.IssueTypeUrls;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.meterware.httpunit.WebTable;
import org.junit.Rule;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY_KEY;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static org.junit.Assert.assertNotNull;

/**
 *
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.ISSUES, Category.SCHEMES})
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueTypeSchemeMigration extends BaseJiraFuncTest {
    @Rule
    public IssueTypeUrls urls = new IssueTypeUrls();

    @Inject
    private HtmlPage page;

    @Inject
    private BulkOperations bulkOperations;

    @Inject
    private Form form;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testI18n() {
        administration.restoreI18nData("TestIssueTypeSchemeMigrationGerman.xml");
        urls.invalidateIssueTypeIconsCache();
        final String baseUrl = getEnvironmentData().getBaseUrl().toString();
        logger.log("Setting baseurl to '" + baseUrl + "'");
        navigation.gotoAdminSection(Navigation.AdminSection.GENERAL_CONFIGURATION);
        assertions.assertNodeByIdHasText("edit-app-properties", "Einstellungen bearbeiten");
        tester.clickLink("edit-app-properties");
        tester.setFormElement("baseURL", baseUrl);
        tester.submit("Aktualisieren");

        Long projectId = backdoor.project().getProjectId(PROJECT_MONKEY_KEY);
        tester.gotoPage("secure/admin/SelectIssueTypeSchemeForProject!default.jspa?projectId=" + projectId);

        tester.checkCheckbox("createType", "chooseScheme");
        // Select 'monkey' from select box 'schemeId'.
        tester.selectOption("schemeId", "monkey");
        tester.submit(" OK ");

        tester.submit("nextBtn");
        assertCurrentStep(2);

        tester.submit("previousBtn");
        assertCurrentStep(1);

        tester.submit("nextBtn");
        tester.submit("nextBtn");
        tester.submit("nextBtn");
        assertCurrentStep(4);

        XPathLocator finishLocator = locator.xpath("//*[@id=\"nextButton\" and @value=\"Fertigstellen\"]");
        assertNotNull(finishLocator.getNode());

        tester.submit("nextBtn");

        administration.generalConfiguration().setJiraLocaleToSystemDefault();
    }

    private void assertCurrentStep(int step) {
        XPathLocator locator = new XPathLocator(tester, "//*[@id=\"currentStep\" and @value=" + step + " ]");
        assertNotNull(locator.getNode());
    }

    @Test
    public void testIssueTypeOptionsCorrectForIssueTypeMapping() {
        // NOTE: this data has a bunch of useless issue types so that we get id's of larger than 10
        // so that when they are ordered issue type with id 11 will come before issue type with id 2
        // this will exercise the problem area that we have fixed.
        administration.restoreData("TestIssueTypeSchemeMigration.xml");
        urls.invalidateIssueTypeIconsCache();

        // We want to migrate the homosapien project to the Issue Type Scheme to Move to scheme
        // this will force us to be prompted with a option group of possible top-level issue
        // types for where we are going and a option group of possible sub-task issue types
        // for where we are going. We want to verify that this does not get reordered.

        Long projectId = backdoor.project().getProjectId(PROJECT_HOMOSAP_KEY);
        tester.gotoPage("secure/admin/SelectIssueTypeSchemeForProject!default.jspa?projectId=" + projectId);

        tester.selectOption("schemeId", "Issue Type Scheme to Move to");
        tester.submit(" OK ");

        // Make sure the correct issue types are on the summary page
        tester.assertTextPresent("Issue Type Migration");

        textAssertions.assertTextPresent(locator.id("summary_table"), "New Feature");
        textAssertions.assertTextPresent(locator.id("summary_table"), "Sub-task ORIG");

        tester.submit("nextBtn");

        // We should be presented with the sub task issue type to map
        textAssertions.assertTextPresent(locator.css(".jiraform .instructions"), "Sub-task ORIG");

        // Make sure the correct options of subtask issue types for where we are going are available and that the
        // top-level issue types are not.
        assertions.getProjectFieldsAssertions().assertIssueTypesEquals(new String[]{"Another Subtask", "Sub-task DIFFERENT"});

        // Move on to the next mapping, the mapping of the top-level issue type
        tester.submit("nextBtn");
        tester.submit("nextBtn");

        textAssertions.assertTextPresent(locator.css(".jiraform .instructions"), "New Feature");
        assertions.getProjectFieldsAssertions().assertIssueTypesEquals(new String[]{"Improvement", "Bug"});
    }

    @Test
    public void testIssueTypeSchemeMigrationNoSubtasksMultiProjects() throws SAXException {
        administration.restoreData("TestIssueTypeSchemeMigrationNoSubtasks.xml");
        urls.invalidateIssueTypeIconsCache();
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        //check pre-conditions: This isn't a 100% but good enough for the purpose of this test.
        navigation.issueNavigator().displayAllIssues();
        WebTable issueTable = tester.getDialog().getResponse().getTableWithID("issuetable");
        assertions.getTableAssertions().assertTableCellHasImage(issueTable, 1, 0, urls.getIssueTypeUrl("sub-task"));
        assertions.getTableAssertions().assertTableCellHasImage(issueTable, 2, 0, urls.getIssueTypeUrl("new feature"));
        assertions.getTableAssertions().assertTableCellHasImage(issueTable, 3, 0, urls.getIssueTypeUrl("ignore3"));
        assertions.getTableAssertions().assertTableCellHasImage(issueTable, 4, 0, urls.getIssueTypeUrl("bug"));
        navigation.issue().gotoIssue("HSP-4");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Type:", "Another Subtask");
        navigation.issue().gotoIssue("HSP-3");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Type:", "New Feature");
        navigation.issue().gotoIssue("HSP-2");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Type:", "Sub-task ORIG");
        navigation.issue().gotoIssue("HSP-1");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Type:", "Bug");

        //first test that migrating to multiple projects does not work if a project has subtasks and the scheme
        //doesn't have subtask types.
        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_TYPE_SCHEMES);
        tester.clickLinkWithText("Issue Type Schemes");
        tester.clickLink("associate_10030");
        form.selectOptionsByValue("projects", new String[]{"10000", "10001"});
        tester.submit("Associate");
        //ensure we get an error!
        tester.assertTextPresent("There are 2 sub-tasks that will be affected by this change. You must have at least one valid sub-task issue type.");

        //then test the successfulMultiproject migration to a scheme with subtasks.
        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_TYPE_SCHEMES);
        tester.clickLinkWithText("Issue Type Schemes");
        tester.clickLink("associate_10020");
        form.selectOptionsByValue("projects", new String[]{"10000", "10001"});
        tester.submit("Associate");

        //check if the correct project/issue types to be modifed are shown
        textAssertions.assertTextSequence(page.getCollapsedResponseText(),
                "Issue Type Migration: Overview (Step 1 of 6)",
                "homosapien", "New Feature", "1",
                "homosapien", "Sub-task ORIG", "1");
        tester.submit("nextBtn");

        textAssertions.assertTextSequence(page.getCollapsedResponseText(),
                "Issue Type Migration: Select Issue Type (Step 2 of 6)",
                "Select a new issue type for issues with current issue type ",
                "Sub-task ORIG",
                " in project ",
                "homosapien");
        //lets select a different subtask issue type.
        navigation.issue().selectIssueType("Sub-task DIFFERENT");
        tester.submit("nextBtn");

        //no field should need to be mapped
        textAssertions.assertTextSequence(page.getCollapsedResponseText(),
                "Issue Type Migration: Update Fields (Step 3 of 6)",
                "Update fields for issues with current issue type ",
                "Sub-task ORIG",
                " in project ",
                "homosapien.");
        tester.assertTextPresent("All field values will be retained.");
        tester.submit("nextBtn");

        //no field should need to be mapped
        textAssertions.assertTextSequence(page.getCollapsedResponseText(),
                "Issue Type Migration: Select Issue Type (Step 4 of 6)",
                "Select a new issue type for issues with current issue type ",
                "New Feature",
                " in project ",
                "homosapien");
        //lets select a new issue type.
        navigation.issue().selectIssueType("Bug");
        tester.submit("nextBtn");

        //check if any of the fields for the new feature type should be mapped
        textAssertions.assertTextSequence(page.getCollapsedResponseText(),
                "Issue Type Migration: Update Fields (Step 5 of 6)",
                "Update fields for issues with current issue type ",
                "New Feature",
                " in project ",
                "homosapien.");
        tester.assertTextPresent("All field values will be retained.");
        tester.submit("nextBtn");

        //submit past the confirmation screen.
        tester.submit("nextBtn");

        if (page.asString().contains("Bulk Operation")) {
            bulkOperations.waitAndReloadBulkOperationProgressPage();
        }

        //check the correct issue types are shown in the issue navigator.
        navigation.issueNavigator().displayAllIssues();
        issueTable = tester.getDialog().getResponse().getTableWithID("issuetable");
        assertions.getTableAssertions().assertTableCellHasImage(issueTable, 1, 0, urls.getIssueTypeUrl("sub-task"));
        assertions.getTableAssertions().assertTableCellHasImage(issueTable, 2, 0, urls.getIssueTypeUrl("bug"));
        assertions.getTableAssertions().assertTableCellHasImage(issueTable, 3, 0, urls.getIssueTypeUrl("ignore3"));
        assertions.getTableAssertions().assertTableCellHasImage(issueTable, 4, 0, urls.getIssueTypeUrl("bug"));
        navigation.issue().gotoIssue("HSP-4");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Type:", "Another Subtask");
        navigation.issue().gotoIssue("HSP-3");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Type:", "Bug");
        navigation.issue().gotoIssue("HSP-2");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Type:", "Sub-task DIFFERENT");
        navigation.issue().gotoIssue("HSP-1");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Type:", "Bug");
    }

    @Test
    public void testIssueTypeSchemeMigrationMultiProjectsSecurityLevels() {
        administration.restoreData("TestIssueTypeSchemeMigrationSecurityLevel.xml");
        urls.invalidateIssueTypeIconsCache();
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
        assertPrecondition();

        //first test that migrating to multiple projects does not work if a project has subtasks and the scheme
        //doesn't have subtask types.
        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_TYPE_SCHEMES);
        tester.clickLinkWithText("Issue Type Schemes");
        tester.clickLink("associate_10060");
        form.selectOptionsByValue("projects", new String[]{"10000", "10020", "10021", "10022"});

        tester.submit("Associate");
        tester.assertTextPresent("There are 5 sub-tasks that will be affected by this change. You must have at least one valid sub-task issue type.");

        assertPrecondition();

        //now lets move to an issue type scheme with different issue types.
        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_TYPE_SCHEMES);
        tester.clickLinkWithText("Issue Type Schemes");
        tester.clickLink("associate_10061");
        form.selectOptionsByValue("projects", new String[]{"10000", "10020", "10021", "10022"});
        tester.submit("Associate");
        textAssertions.assertTextSequence(page.getCollapsedResponseText(),
                "Issue Type Migration: Overview (Step 1 of 6)",
                "Bovine", "Sub-task", "3",
                "Rattus", "Sub-task", "2");
        tester.submit("nextBtn");

        textAssertions.assertTextSequence(page.getCollapsedResponseText(),
                "Select a new issue type for issues with current issue type ",
                "Sub-task",
                "in project ",
                "Bovine");
        tester.submit("nextBtn");

        textAssertions.assertTextSequence(page.getCollapsedResponseText(),
                "Issue Type Migration: Update Fields (Step 3 of 6)",
                "All field values will be retained.");
        tester.submit("nextBtn");

        textAssertions.assertTextSequence(page.getCollapsedResponseText(),
                "Issue Type Migration: Select Issue Type (Step 4 of 6)",
                "Select a new issue type for issues with current issue type ",
                "Sub-task",
                "in project",
                "Rattus");
        tester.submit("nextBtn");

        textAssertions.assertTextSequence(page.getCollapsedResponseText(),
                "Issue Type Migration: Update Fields (Step 5 of 6)",
                "All field values will be retained.");
        tester.submit("nextBtn");
        tester.submit("nextBtn");

        //check issues to see they've still got the security level set.
        assertPrecondition();

        //now go to an issue and make sure it's got the right type and level.
        navigation.issue().gotoIssue("RAT-7");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Security", "Level Mouse");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Type", "DIFFERENT SUBTASK");

        navigation.issue().gotoIssue("COW-36");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Security", "MyFriendsOnly");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Type", "DIFFERENT SUBTASK");
    }

    // added to validate JRA-16052
    @Test
    public void testIssueTypeMigrationWithNumericCustomField() {
        administration.restoreData("TestIssueTypeMigrationWithNumericCustomField.xml");
        urls.invalidateIssueTypeIconsCache();

        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_TYPE_SCHEMES);
        tester.clickLinkWithText("Issue Type Schemes");

        tester.clickLink("associate_10001");
        tester.selectOption("projects", "homosapien");
        tester.submit("Associate");
        tester.submit("nextBtn");
        tester.submit("nextBtn");

        tester.setFormElement("customfield_10000", "LOREM_IPSUM");
        tester.submit("nextBtn");

        // verify that we are still on step 3 and have an error message in the form.
        tester.assertTextPresent("&#39;LOREM_IPSUM&#39; is an invalid number");
        tester.assertTextPresent("Issue Type Migration: Update Fields (Step 3 of 4)");

        // now make sure the next step works when we use a number.
        tester.setFormElement("customfield_10000", "857");
        tester.submit("nextBtn");

        tester.assertTextPresent("Issue Type Migration: Confirmation (Step 4 of 4)");
        tester.assertTextPresent("857");

    }

    // This is here to test the bug in JRA-10244, the problem that subtasks whose parents
    // issue type is obsolete in the destination context are removed from the list of issues
    // to be operated on.
    @Test
    public void testIssueTypeSchemeMigrationWithSubtasks() throws SAXException {
        administration.restoreData("TestIssueTypeSchemeMigrationWithSubtasks.xml");
        urls.invalidateIssueTypeIconsCache();

        Long projectId = backdoor.project().getProjectId(PROJECT_HOMOSAP_KEY);
        tester.gotoPage("secure/admin/SelectIssueTypeSchemeForProject!default.jspa?projectId=" + projectId);

        tester.selectOption("schemeId", "Issue Type Scheme to Move to");
        tester.submit(" OK ");

        // Make sure the correct issue types are on the summary page
        tester.assertTextPresent("Issue Type Migration");

        // Before we fixed the bug the sub task would not have been here
        WebTable summaryTable = tester.getDialog().getResponse().getTableWithID("summary_table");

        assertions.getTableAssertions().assertTableCellHasText(summaryTable, 1, 1, "Bug");
        assertions.getTableAssertions().assertTableCellHasText(summaryTable, 2, 1, "Sub-task ORIG");

        tester.submit("nextBtn");

        // We should be presented with the bug issue type map
        tester.assertTextPresent("Issue Type Migration");
        navigation.issue().selectIssueType("New Feature");
        tester.submit("nextBtn");
        tester.submit("nextBtn");

        // We should be presented with the sub task issue type to map
        tester.assertTextPresent("Issue Type Migration");
        navigation.issue().selectIssueType("Sub-task DIFFERENT");
        tester.submit("nextBtn");
        tester.submit("nextBtn");

        // Complete the operation
        tester.submit("nextBtn");

        // Browse to the issues and confirm that everything has been changed correctly
        navigation.issue().gotoIssue("HSP-1");
        tester.assertTextPresent("New Feature");
        navigation.issue().gotoIssue("HSP-2");
        tester.assertTextPresent("Sub-task DIFFERENT");
    }

    private void assertPrecondition() {
        //first lets check we have an issue with subtasks and they all have a particular security level set.
        navigation.issueNavigator().displayAllIssues();
        final WebTable issuetable = tester.getDialog().getWebTableBySummaryOrId("issuetable");

        // Rat Issues
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 1, 1, "RAT-7");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 1, 2, "RAT-5");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 1, 11, "Level Mouse");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 2, 1, "RAT-6");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 2, 2, "RAT-5");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 2, 11, "Level Mouse");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 3, 1, "RAT-5");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 3, 11, "Level Mouse");

        // Cow Issues
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 4, 1, "COW-37");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 4, 2, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 4, 2, "Lets get a third milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 4, 11, "MyFriendsOnly");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 5, 1, "COW-36");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 5, 2, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 5, 2, "Get another milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 5, 11, "MyFriendsOnly");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 6, 1, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 6, 2, "No more milk");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 6, 11, "MyFriendsOnly");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 7, 1, "COW-34");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 7, 2, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 7, 2, "Get new milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issuetable, 7, 11, "MyFriendsOnly");
    }
}
