package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.project.type.TestProjectTypeStorage;
import com.atlassian.jira.webtests.ztests.project.type.TestProjectTypeValidation;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * A suite of tests around JIRA project types
 *
 * @since 7.0
 */
public class FuncTestSuiteProjectTypes {
    public static List<Class<?>> suite() {
        return Lists.newArrayList(
                TestProjectTypeStorage.class,
                TestProjectTypeValidation.class
        );
    }
}
