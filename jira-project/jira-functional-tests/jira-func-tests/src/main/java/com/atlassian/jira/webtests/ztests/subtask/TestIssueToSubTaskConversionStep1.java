package com.atlassian.jira.webtests.ztests.subtask;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;

@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.SUB_TASKS})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestIssueToSubTaskConversion.xml")
public class TestIssueToSubTaskConversionStep1 extends BaseJiraFuncTest {
    private static final String ISSUE_WITHOUT_SUBTASK = "HSP-1";
    private static final String ISSUE_WITHOUT_SUBTASK_ID = "10000";
    private static final String ISSUE_WITH_SUBTASK = "HSP-2";
    private static final String ISSUE_WITH_SUBTASK_ID = "10001";
    private static final String SUBTASK = "HSP-3";
    private static final String SUBTASK_ID = "10002";
    private static final String INVALID_ISSUE_1 = "HSP-9";
    private static final String INVALID_ISSUE_ID_1 = "1)%20Please%20login%20again%20at%20https://attacker.com%20";
    private static final String INVALID_ISSUE_ID_2 = "10000a";
    private static final String ISSUE_FROM_OTHER_PROJECT = "MKY-1";

    private static final String ISSUE_TO_CONVERT_ID = "10020";
    private static final String ISSUE_TO_CONVERT_KEY = "MKY-2";
    private static final String PARENT_ISSUE = "MKY-3";
    private static final String SUBTASK_TYPE = "Sub-task";
    private static final String SUBTASK_TYPE_ID = "5";
    private static final String TASK_TYPE = "Task";
    private static final String TASK_TYPE_ID = "3";
    private static final String SUBTASK_TYPE_2 = "Sub-task 2";
    private static final String SUBTASK_TYPE_2_ID = "6";
    private static final String SUBTASK_TYPE_3 = "Sub-task 3";
    private static final String SUBTASK_TYPE_3_ID = "7";
    private static final String INVALID_TYPE_ID = "976";

    @Inject
    private SubTaskAssertions subTaskAssertions;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
    }

    /*
     * If you place historical parent key it should get translated to the current project key.
     */
    @Test
    public void testParentIssueKeyIsTranslatedToCurrentKey() {
        Long projectId = backdoor.project().getProjectId("HSP");
        backdoor.project().addProjectKey(projectId, "OLD");

        gotoConvertIssue(ISSUE_WITHOUT_SUBTASK_ID);
        tester.assertTextPresent("Select Parent Issue");
        tester.assertTextPresent("Begin typing to search for issues to link");
        tester.assertTextPresent("Only non-sub-task issues from the same project (HSP) can be selected.");

        // Cannot be null
        tester.setFormElement("parentIssueKey", "OLD-2");
        tester.submit();
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Parent Issue:", "HSP-2");
    }

    /*
     * Tests that when subtasks are not enabled, issues cannot be converted to subtasks.
     */
    @Test
    public void testIssueToSubTaskConversionWhenSubTaskNotEnabled() {
        navigation.issue().gotoIssue(ISSUE_WITHOUT_SUBTASK);
        tester.assertLinkPresent("issue-to-subtask");

        gotoConvertIssue(ISSUE_WITHOUT_SUBTASK_ID);
        tester.assertTextPresent("Step 1 of 4");

        navigation.issue().deleteIssue(SUBTASK);
        administration.subtasks().disable();

        navigation.issue().gotoIssue(ISSUE_WITHOUT_SUBTASK);
        tester.assertLinkNotPresent("issue-to-subtask");

        gotoConvertIssue(ISSUE_WITHOUT_SUBTASK_ID);
        tester.assertTextPresent("Errors");
        tester.assertTextPresent("Sub-tasks are disabled.");
        tester.assertTextNotPresent("Step 1 of 4");
    }

    /*
     * Tests that subtasks cannot be converted to subtasks.
     */
    @Test
    public void testIssueToSubTaskConversionOnSubtask() {
        navigation.issue().gotoIssue(ISSUE_WITHOUT_SUBTASK);
        tester.assertLinkPresent("issue-to-subtask");

        gotoConvertIssue(ISSUE_WITHOUT_SUBTASK_ID);
        tester.assertTextPresent("Step 1 of 4");

        navigation.issue().gotoIssue(SUBTASK);
        tester.assertLinkNotPresent("issue-to-subtask");

        gotoConvertIssue(SUBTASK_ID);
        tester.assertTextPresent("Errors");
        tester.assertTextPresent("Issue " + SUBTASK + " is already a sub-task.");
        tester.assertTextNotPresent("Step 1 of 4");
    }

    /*
     * Tests that issues which have subtasks cannot be converted to subtasks.
     */
    @Test
    public void testIssueToSubTaskConversionWhenIssueHasSubTasks() {
        navigation.issue().gotoIssue(ISSUE_WITHOUT_SUBTASK);
        tester.assertLinkPresent("issue-to-subtask");

        gotoConvertIssue(ISSUE_WITHOUT_SUBTASK_ID);
        tester.assertTextPresent("Step 1 of 4");

        navigation.issue().gotoIssue(ISSUE_WITH_SUBTASK);
        tester.assertLinkNotPresent("issue-to-subtask");

        gotoConvertIssue(ISSUE_WITH_SUBTASK_ID);
        tester.assertTextPresent("Errors");
        tester.assertTextPresent("Can not convert issue " + ISSUE_WITH_SUBTASK + " with sub-tasks to a sub-task.");
        tester.assertTextNotPresent("Step 1 of 4");

        // Check Wizard pane shows correct link
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Return to", ISSUE_WITH_SUBTASK);
        tester.assertLinkPresentWithText(ISSUE_WITH_SUBTASK);
    }

    /*
     * Tests that convert issue to subtasks is only available to users with Permissions.EDIT_ISSUE
     */
    @Test
    public void testIssueToSubTaskConversionEditPermission() {
        navigation.issue().gotoIssue(ISSUE_WITHOUT_SUBTASK);
        tester.assertLinkPresent("issue-to-subtask");

        gotoConvertIssue(ISSUE_WITHOUT_SUBTASK_ID);
        tester.assertTextPresent("Step 1 of 4");
        tester.assertTextPresent("Select Parent Issue");
        tester.assertTextPresent("Begin typing to search for issues to link");
        tester.assertTextPresent("Only non-sub-task issues from the same project (HSP) can be selected.");

        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(ISSUE_WITHOUT_SUBTASK);
        tester.assertLinkNotPresent("issue-to-subtask");

        gotoConvertIssue(ISSUE_WITHOUT_SUBTASK_ID);
        tester.assertTextPresent("Access Denied");
        tester.assertTextNotPresent("Step 1 of 4");
    }

    /*
     * Tests that convert issue to subtasks is not available when subtasks are not in the issue types scheme
     */
    @Test
    public void testIssueToSubTaskConversionWhenNotInIssueTypes() {
        navigation.issue().gotoIssue(ISSUE_WITHOUT_SUBTASK);
        tester.assertLinkPresent("issue-to-subtask");

        gotoConvertIssue(ISSUE_WITHOUT_SUBTASK_ID);
        tester.assertTextPresent("Step 1 of 4");

        // Delete all subtask issue types (and all subtask issues)
        navigation.issue().deleteIssue(SUBTASK);
        tester.gotoPage("secure/admin/DeleteIssueType!default.jspa?id=" + SUBTASK_TYPE_ID);
        tester.assertTextPresent("Delete Issue Type: " + SUBTASK_TYPE);
        tester.submit("Delete");
        tester.gotoPage("secure/admin/DeleteIssueType!default.jspa?id=" + SUBTASK_TYPE_2_ID);
        tester.assertTextPresent("Delete Issue Type: " + SUBTASK_TYPE_2);
        tester.submit("Delete");
        tester.gotoPage("secure/admin/DeleteIssueType!default.jspa?id=" + SUBTASK_TYPE_3_ID);
        tester.assertTextPresent("Delete Issue Type: " + SUBTASK_TYPE_3);
        tester.submit("Delete");

        navigation.issue().gotoIssue(ISSUE_WITHOUT_SUBTASK);
        tester.assertLinkNotPresent("issue-to-subtask");

        gotoConvertIssue(ISSUE_WITHOUT_SUBTASK_ID);
        tester.assertTextPresent("Errors");
        tester.assertTextPresent("Project HSP associated with issue does not have any sub-task issue types available.");
        tester.assertTextNotPresent("Step 1 of 4");
    }

    /*
     * Tests that an invalid issue id returns error
     */
    @Test
    public void testIssueToSubTaskConversionIsProtectAgainstContentSpoofingThroughAnInvalidIssueId() {
        gotoConvertIssue(ISSUE_WITHOUT_SUBTASK_ID);
        tester.assertTextPresent("Step 1 of 4");

        gotoConvertIssue(INVALID_ISSUE_ID_1);
        tester.assertTextPresent("Errors");
        tester.assertTextPresent("Invalid issue id");
        tester.assertTextNotPresent("attacker.com");
        tester.assertTextNotPresent("Step 1 of 4");

        gotoConvertIssue(INVALID_ISSUE_ID_2);
        tester.assertTextPresent("Errors");
        tester.assertTextNotPresent(INVALID_ISSUE_ID_2);
        tester.assertTextNotPresent("Step 1 of 4");

        // Check Wizard pane shows correct link
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Return to", "Dashboard");
        tester.assertLinkPresentWithText("Dashboard");
    }

    /*
     * Tests that the parent issue must be valid
     */
    @Test
    public void testIssueToSubTaskConversionInvalidParentIssue() {
        gotoConvertIssue(ISSUE_WITHOUT_SUBTASK_ID);
        tester.assertTextPresent("Select Parent Issue");
        tester.assertTextPresent("Begin typing to search for issues to link");
        tester.assertTextPresent("Only non-sub-task issues from the same project (HSP) can be selected.");

        // Cannot be null
        tester.setFormElement("parentIssueKey", "");
        tester.submit();
        tester.assertTextPresent("Parent issue key not specified.");

        // Must exist
        tester.setFormElement("parentIssueKey", INVALID_ISSUE_1);
        tester.submit();
        tester.assertTextPresent("Parent issue with key " + INVALID_ISSUE_1 + " not found.");

        // Cannot be from another project
        tester.setFormElement("parentIssueKey", ISSUE_FROM_OTHER_PROJECT);
        tester.submit();
        tester.assertTextPresent("Parent issue " + ISSUE_FROM_OTHER_PROJECT + " must be from the same project as issue " + ISSUE_WITHOUT_SUBTASK);

        // Cannot be itself
        tester.setFormElement("parentIssueKey", ISSUE_WITHOUT_SUBTASK);
        tester.submit();
        tester.assertTextPresent("Issue " + ISSUE_WITHOUT_SUBTASK + " can not be parent of itself.");

        // Cannot be sub-task
        tester.setFormElement("parentIssueKey", SUBTASK);
        tester.submit();
        tester.assertTextPresent("Parent issue " + SUBTASK + " can not be sub-task.");
    }

    /*
    * Tests that the new subtask type list contains only valid new subtask types
    */
    @Test
    public void testIssueToSubTaskConversionSubTaskType() {
        // Project where SUBTASK_TYPE_2 exists
        gotoConvertIssue(ISSUE_WITHOUT_SUBTASK_ID);
        tester.assertOptionsEqual("issuetype", new String[]{SUBTASK_TYPE, SUBTASK_TYPE_2, SUBTASK_TYPE_3});
        tester.assertRadioOptionValueNotPresent("issuetype", TASK_TYPE);

        // Project where SUBTASK_TYPE_2 doesn't exist
        gotoConvertIssue(ISSUE_TO_CONVERT_ID);
        tester.assertOptionsEqual("issuetype", new String[]{SUBTASK_TYPE, SUBTASK_TYPE_3});
        tester.assertRadioOptionValueNotPresent("issuetype", SUBTASK_TYPE_2);
    }

    /*
     * Tests that the new subtask type must be valid
     */
    @Test
    public void testIssueToSubTaskConversionInvalidSubTaskType() {
        // No such issue type
        gotoConvertIssueStep2(ISSUE_TO_CONVERT_ID, PARENT_ISSUE, INVALID_TYPE_ID);
        tester.assertTextPresent("Selected issue type not found.");

        // Not applicable for the project
        gotoConvertIssueStep2(ISSUE_TO_CONVERT_ID, PARENT_ISSUE, SUBTASK_TYPE_2_ID);

        tester.assertTextPresent("Issue type " + SUBTASK_TYPE_2 + " not applicable for this project");

        // Not a subtask type
        gotoConvertIssueStep2(ISSUE_TO_CONVERT_ID, PARENT_ISSUE, TASK_TYPE_ID);

        tester.assertTextPresent("Issue type " + TASK_TYPE + " is not a sub-task");
    }

    /*
     * Tests that the panel shows the correct steps and progress for step 1
     */
    @Test
    public void testIssueToSubTaskConversionPanelStep1() {
        gotoConvertIssue(ISSUE_TO_CONVERT_ID);
        tester.assertTextPresent("Select Parent Issue");
        tester.assertTextPresent("Begin typing to search for issues to link");
        tester.assertTextPresent("Only non-sub-task issues from the same project (MKY) can be selected.");
        subTaskAssertions.assertSubTaskConversionPanelSteps(ISSUE_TO_CONVERT_KEY, 1);

    }

    private void gotoConvertIssue(String issueId) {
        tester.gotoPage("/secure/ConvertIssue.jspa?id=" + issueId);
    }


    private void gotoConvertIssueStep2(String issueId, String parent, String issueType) {
        tester.gotoPage("/secure/ConvertIssueSetIssueType.jspa?id=" + issueId +
                "&parentIssueKey=" + parent + "&issuetype=" + issueType);
    }
}
