package com.atlassian.jira.functest.framework.admin;

import com.google.inject.ImplementedBy;

import java.util.Map;

/**
 * A Page Object useful for retrieving and setting advanced application properties
 *
 * @since v4.4.5
 */
@ImplementedBy(AdvancedApplicationPropertiesImpl.class)
public interface AdvancedApplicationProperties {
    Map<String, String> getApplicationProperties();

    void setApplicationProperty(String key, String value);

    String getApplicationProperty(String key);
}
