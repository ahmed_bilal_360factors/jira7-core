package com.atlassian.jira.webtests.ztests.admin.services;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.TableLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.webtests.util.TempDirectoryUtil;
import com.google.common.collect.ImmutableMap;
import com.meterware.httpunit.WebTable;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION})
@LoginAs(user = ADMIN_USERNAME)
public class TestBackupService extends BaseJiraFuncTest {
    @Inject
    private FuncTestLogger logger;

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testAddAndEditBackupService() throws Exception {
        administration.restoreData("TestBackupService.xml");
        // Goto Services
        navigation.gotoAdminSection(Navigation.AdminSection.SERVICES);
        tester.assertTextPresent("Add a new service by entering a name and class below. You can then edit it to set properties");
        // Add a Backup Service
        tester.setFormElement("name", "My Backup Service");
        tester.setFormElement("clazz", "com.atlassian.jira.service.services.export.ExportService");
        tester.checkRadioOption("service.schedule.dailyWeeklyMonthly", "advanced");
        tester.setFormElement("service.schedule.cronString", "0 0 2 1/2 * ?");
        tester.submit("Add Service");
        tester.assertTextPresent("Edit Service: My Backup Service");
        tester.assertFormElementEquals("service.schedule.cronString", "0 0 2 1/2 * ?");

        // Configure it
        // Change our mind on the delay and set to 2am every day
        tester.checkRadioOption("service.schedule.dailyWeeklyMonthly", "advanced");
        tester.setFormElement("service.schedule.cronString", "0 0 2 * * ?");
        tester.submit("Update");

        // Assert the Service is as expected:
        // Assert the cells in table 'tbl_services'.
        WebTable tbl_services = tester.getDialog().getWebTableBySummaryOrId("tbl_services");
        // Assert row 0: |Name / Class|Properties|Delay (mins)|Operations|
        Assert.assertEquals("Cell (0, 1) in table 'tbl_services' should be 'Name / Class'.", "Name / Class", tbl_services.getCellAsText(0, 1).trim());
        Assert.assertEquals("Cell (0, 2) in table 'tbl_services' should be 'Properties'.", "Properties", tbl_services.getCellAsText(0, 2).trim());
        Assert.assertEquals("Cell (0, 3) in table 'tbl_services' should be 'Schedule'.", "Schedule", tbl_services.getCellAsText(0, 3).trim());

        // Assert row: |My Backup Service com.atlassian.jira.service.services.export.ExportService|99|Edit Delete|
        TableLocator tableLocator = new TableLocator(tester, "tbl_services");
        textAssertions.assertTextSequence(tableLocator, "My Backup Service", "com.atlassian.jira.service.services.export.ExportService", "Daily at 2:00 am");
        textAssertions.assertTextNotPresent(tableLocator, "DIR_NAME");

        // Now we will edit this service
        administration.services().clickEdit("My Backup Service");
        tester.checkRadioOption("service.schedule.dailyWeeklyMonthly", "advanced");
        tester.setFormElement("service.schedule.cronString", "0 0 0/3 * * ?");
        tester.submit("Update");

        // Assert row: |My Backup Service com.atlassian.jira.service.services.export.ExportService|USE_DEFAULT_DIRECTORY:true |99|Edit Delete|
        tableLocator = new TableLocator(tester, "tbl_services");
        textAssertions.assertTextSequence(tableLocator, "My Backup Service", "com.atlassian.jira.service.services.export.ExportService", "Daily every 3 hours");
        textAssertions.assertTextNotPresent(tableLocator, "USE_DEFAULT_DIRECTORY");
    }

    /**
     * This tests that if they have custom paths in legacy data, then there are displayed and respected but cant be
     * edited
     * <p/>
     * The previous tests check that the paths cannot be set during creation
     */
    @Test
    public void testWhenDataHasLegacyPathInIt() throws IOException {
        File attachment = TempDirectoryUtil.createTempDirectory("tWDHLPII");
        File indexes = TempDirectoryUtil.createTempDirectory("tWDHLPII");
        File fileservice = TempDirectoryUtil.createTempDirectory("tWDHLPII");
        File backup = TempDirectoryUtil.createTempDirectory("tWDHLPII");

        administration.restoreDataWithReplacedTokens("TestCustomPathInLegacyData.xml",
                ImmutableMap.<String, String>of("@INDEX_DIR@", indexes.getAbsolutePath(),
                        "@ATTACHMENT_DIR@", attachment.getAbsolutePath(),
                        "@BACKUP_DIR@", backup.getAbsolutePath(),
                        "@FILESERVICE_DIR@", fileservice.getAbsolutePath()));
        navigation.gotoAdminSection(Navigation.AdminSection.SERVICES);

        TableLocator tableLocator = new TableLocator(tester, "tbl_services");

        /// it should reflect that they cant do stuff
        textAssertions.assertTextSequence(tableLocator, "File Service",
                "com.atlassian.jira.service.services.file.FileService", "directory", fileservice.getAbsolutePath(),
                "handler", "Add a comment from the non quoted email body");
        textAssertions.assertTextSequence(tableLocator, "Backup Service", "com.atlassian.jira.service.services.export.ExportService", "DIR_NAME", backup.getAbsolutePath());

        navigation.gotoPage(page.addXsrfToken("secure/admin/EditService!default.jspa?id=10021")); // edit file service
        textAssertions.assertTextPresent(fileservice.getAbsolutePath());
        tester.submit("Update");

        navigation.gotoPage(page.addXsrfToken("secure/admin/EditService!default.jspa?id=10001")); // edit backup service
        textAssertions.assertTextPresent(backup.getAbsolutePath());
        tester.submit("Update");

        // the paths should not have changed at all
        textAssertions.assertTextSequence(tableLocator, "File Service",
                "com.atlassian.jira.service.services.file.FileService", "directory", fileservice.getAbsolutePath(),
                "handler", "Add a comment from the non quoted email body");
        textAssertions.assertTextSequence(tableLocator, "Backup Service", "com.atlassian.jira.service.services.export.ExportService", "DIR_NAME", backup.getAbsolutePath());

        //Make sure we ain't using any of the directories before we kill them.
        administration.restoreBlankInstance();

        FileUtils.deleteDirectory(attachment);
        try {
            FileUtils.deleteDirectory(indexes);
        } catch (IOException ex) {
            // JRADEV-8944: This fails to remove index files sometimes in the MS-SQL TPM tests.
            // Cleanup is a courtesy and shouldn't fail the test
            logger.log("Failed to delete indexes directory. " + ex.getMessage());
            ex.printStackTrace();
        }
        FileUtils.deleteDirectory(fileservice);
        FileUtils.deleteDirectory(backup);
    }
}
