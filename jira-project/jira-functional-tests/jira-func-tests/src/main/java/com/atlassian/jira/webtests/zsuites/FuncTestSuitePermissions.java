package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.admin.TestAdminsWithoutAppRoles;
import com.atlassian.jira.webtests.ztests.admin.TestApplicationRoleBasedPermissions;
import com.atlassian.jira.webtests.ztests.admin.TestOldPermissionSchemes;
import com.atlassian.jira.webtests.ztests.admin.TestSystemAdminAndAdminPermissions;
import com.atlassian.jira.webtests.ztests.customfield.TestCustomFieldsNoSearcherPermissions;
import com.atlassian.jira.webtests.ztests.dashboard.TestManageDashboardPagePermissions;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestDeveloperWorkloadReportPermissions;
import com.atlassian.jira.webtests.ztests.issue.TestIssueOperationsWithLimitedPermissions;
import com.atlassian.jira.webtests.ztests.issue.comments.TestCommentPermissions;
import com.atlassian.jira.webtests.ztests.misc.TestAddPermission;
import com.atlassian.jira.webtests.ztests.subtask.TestCreateSubTasksContextPermission;
import com.atlassian.jira.webtests.ztests.timetracking.legacy.TestTimeTrackingReportPermissions;
import com.atlassian.jira.webtests.ztests.user.TestDeleteUserAndPermissions;
import com.atlassian.jira.webtests.ztests.user.TestGroupSelectorPermissions;
import com.atlassian.jira.webtests.ztests.workflow.TestWorkflowBasedPermissions;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of tests related to Permissions
 *
 * @since v4.0
 */
public class FuncTestSuitePermissions {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestDeveloperWorkloadReportPermissions.class)
                .add(TestTimeTrackingReportPermissions.class)
                .add(TestIssueOperationsWithLimitedPermissions.class)
                .add(TestOldPermissionSchemes.class)
                .add(TestCreateSubTasksContextPermission.class)
                .add(TestManageDashboardPagePermissions.class)
                .add(TestDeleteUserAndPermissions.class)
                .add(TestWorkflowBasedPermissions.class)
                .add(TestGroupSelectorPermissions.class)
                .add(TestCustomFieldsNoSearcherPermissions.class)
                .add(TestSystemAdminAndAdminPermissions.class)
                .add(TestAddPermission.class)
                .add(TestCommentPermissions.class)
                .add(TestAdminsWithoutAppRoles.class)
                .add(TestApplicationRoleBasedPermissions.class)
                .build();
    }
}