package com.atlassian.jira.webtests.ztests.attachment;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock;
import com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock.ViewMode;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

@WebTest({Category.FUNC_TEST, Category.ATTACHMENTS})
public class TestAttachmentViewMode extends AbstractTestAttachmentsBlockSortingOnViewIssue implements FunctTestConstants {

    @Before
    public void addBrowseProjectPermissions() {
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.BROWSE_PROJECTS);
    }

    @After
    public void revokeBrowseProjectPermissions() {
        backdoor.permissionSchemes().removeEveryonePermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.BROWSE_PROJECTS);
    }

    @Test
    public void testViewModeRetainedForSession() {
        AttachmentsBlock attachments = navigation.issue().attachments("HSP-1");
        assertEquals("Gallery is default view mode", ViewMode.GALLERY, attachments.getViewMode());

        attachments.setViewMode(AttachmentsBlock.ViewMode.LIST);
        attachments = navigation.issue().attachments("HSP-1");
        assertEquals("List view mode retained after reload", ViewMode.LIST, attachments.getViewMode());
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testViewModeRetainedForLoggedIn() {
        AttachmentsBlock attachments = navigation.issue().attachments("HSP-1");
        assertEquals("Gallery is default view mode", ViewMode.GALLERY, attachments.getViewMode());

        attachments.setViewMode(AttachmentsBlock.ViewMode.LIST);
        navigation.logout();

        navigation.login("admin", "admin");
        attachments = navigation.issue().attachments("HSP-1");
        assertEquals("List view mode restored after login", ViewMode.LIST, attachments.getViewMode());
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testViewModeResetAfterLogout() {
        AttachmentsBlock attachments = navigation.issue().attachments("HSP-1", ViewMode.LIST);
        assertEquals("Attachments are in list view mode", ViewMode.LIST, attachments.getViewMode());

        navigation.logout();
        attachments = navigation.issue().attachments("HSP-1");
        assertEquals("View mode was reset to gallery after logout", ViewMode.GALLERY, attachments.getViewMode());
    }
}
