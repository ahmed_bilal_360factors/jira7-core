package com.atlassian.jira.webtests.ztests.attachment;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.rule.Rules;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestRule;

import javax.inject.Inject;

/**
 * @since v6.1
 */
public abstract class AbstractTestAttachmentsBlockSortingOnViewIssue extends BaseJiraFuncTest {

    @Rule
    public TestRule copyAttachmentsRule = Rules.prepareAttachments(this::getEnvironmentData, this::getBackdoor, "TestAttachmentsBlockSortingOnViewIssue/attachments");

    @Inject
    private Administration administration;

    @Before
    public void ensureProperSortingOfAttachments() {
        administration.restoreData("TestAttachmentsBlockSortingOnViewIssue.xml");
        administration.generalConfiguration().setJiraLocale("English (Australia)");
    }
}
