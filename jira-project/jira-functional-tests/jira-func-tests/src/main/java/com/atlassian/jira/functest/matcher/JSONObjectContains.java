package com.atlassian.jira.functest.matcher;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.json.JSONObject;

public class JSONObjectContains extends TypeSafeMatcher<JSONObject> {

    private final Iterable<String> keys;

    public JSONObjectContains(final String... keys) {
        this.keys = Lists.newArrayList(keys);
    }

    @Override
    protected boolean matchesSafely(final JSONObject jsonObject) {
        return Iterables.all(keys, new Predicate<String>() {
            public boolean apply(final String key) {
                return jsonObject.has(key);
            }
        });
    }

    public void describeTo(final Description description) {
        description.appendText("Json object didn't contain the following keys " + keys);
    }
}
