package com.atlassian.jira.webtests.ztests.subtask;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_USERS_GROUP;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ISSUES;

@WebTest({Category.FUNC_TEST, Category.SUB_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestSubTaskQuickCreation extends BaseJiraFuncTest {
    public static final String JIRA_LOZENGE_DARK_FEATURE = "jira.issue.status.lozenge";
    private static final String ISSUE_PARENT = "HSP-6";
    private static final String SUB_TASKS_TABLE_ID = "issuetable";
    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("TestTimeTrackingAggregates.xml");
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    public void testSubTaskDisplayOptions() {
        subTaskDisplayOptions(false);
    }

    @Test
    public void testSubTaskDisplayOptionsWithStatusLozengesEnabled() throws Exception {
        final boolean isLozengeEnabled = backdoor.darkFeatures().isGlobalEnabled(JIRA_LOZENGE_DARK_FEATURE);
        try {
            backdoor.darkFeatures().enableForSite(JIRA_LOZENGE_DARK_FEATURE);
            subTaskDisplayOptions(true);
        } finally {
            if (!isLozengeEnabled) {
                backdoor.darkFeatures().disableForSite(JIRA_LOZENGE_DARK_FEATURE);
            }
        }
    }

    private void subTaskDisplayOptions(final boolean statusLozengesEnabled) {
        // HSP-7 and HSP-8 are children of HSP-6
        navigation.issue().resolveIssue("HSP-7", "Fixed", "");

        navigation.issue().gotoIssue(ISSUE_PARENT);

        // should be in "Show All" view
        if (statusLozengesEnabled) {
            textAssertions.assertTextSequence(locator.table(SUB_TASKS_TABLE_ID), "sub 1", "Resolved", "sub 2", "Open");
        } else {
            textAssertions.assertTextSequence(locator.table(SUB_TASKS_TABLE_ID).getHTML(), "sub 1", "<img", "Resolved", "sub 2", "<img", "Open");
        }

        // click "Show Open"
        tester.clickLink("subtasks-show-open");

        // now only open sub tasks are shown
        if (statusLozengesEnabled) {
            textAssertions.assertTextSequence(locator.table(SUB_TASKS_TABLE_ID), "sub 2", "Open");
            textAssertions.assertTextNotPresent(locator.table(SUB_TASKS_TABLE_ID), "sub 1");
            textAssertions.assertTextNotPresent(locator.table(SUB_TASKS_TABLE_ID), "Resolved");
        } else {
            textAssertions.assertTextSequence(locator.table(SUB_TASKS_TABLE_ID).getHTML(), "sub 2", "<img", "Open");
            textAssertions.assertTextNotPresent(locator.table(SUB_TASKS_TABLE_ID).getHTML(), "sub 1");
            textAssertions.assertTextNotPresent(locator.table(SUB_TASKS_TABLE_ID).getHTML(), "Resolved");
        }


        // click "Show All"
        tester.clickLink("subtasks-show-all");

        // all sub tasks are visible again
        if (statusLozengesEnabled) {
            textAssertions.assertTextSequence(locator.table(SUB_TASKS_TABLE_ID), "sub 1", "Resolved", "sub 2", "Open");
        } else {
            textAssertions.assertTextSequence(locator.table(SUB_TASKS_TABLE_ID).getHTML(), "sub 1", "<img", "Resolved", "sub 2", "<img", "Open");
        }
    }

    @Test
    public void testCreateSubTaskNotVisibleWithoutPermission() {
        navigation.issue().viewIssue(ISSUE_PARENT);

        tester.assertLinkPresent("stqc_show");

        // now change permissions so that the current user doesn't have permission to create sub-tasks.
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, CREATE_ISSUES, JIRA_USERS_GROUP);

        navigation.issue().viewIssue(ISSUE_PARENT);

        tester.assertLinkNotPresent("stqc_show");
    }
}
