package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.ISSUE_NAVIGATOR, Category.USERS_AND_GROUPS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUserVotes extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreData("TestUserWatches.xml");
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    public void testUnresolvedFilter() throws Exception {

        // should default to All issues being shown, meaning that Unresolved will be the link
        navigation.userProfile().gotoCurrentUserProfile();
        tester.clickLink("voted");

        assertions.getIssueNavigatorAssertions().assertExactIssuesInResults("HSP-3");

        // switch to Unresolved
        navigation.userProfile().gotoCurrentUserProfile();
        tester.clickLink("voted_open");
        tester.assertElementNotPresent("issuetable");


        // reopen & unvote that one issue
        navigation.issue().reopenIssue("HSP-3");
        navigation.issue().unvoteIssue("HSP-3");
    }

}
