package com.atlassian.jira.webtests.ztests.license;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.webtests.LicenseKeys.COMMERCIAL;
import static com.atlassian.jira.webtests.LicenseKeys.COMMUNITY;
import static com.atlassian.jira.webtests.LicenseKeys.DEMO;
import static com.atlassian.jira.webtests.LicenseKeys.DEVELOPER;
import static com.atlassian.jira.webtests.LicenseKeys.EVAL_EXPIRED;
import static com.atlassian.jira.webtests.LicenseKeys.OPEN_SOURCE;
import static com.atlassian.jira.webtests.LicenseKeys.PERSONAL;

@WebTest({Category.FUNC_TEST, Category.LICENSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestLicenseFooters extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testEnterpriseCommunityLicense() {
        administration.restoreBlankInstanceWithLicense(COMMUNITY);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Powered by a free Atlassian", "community license for Atlassian.");

        tester.assertTextNotPresent("site is for non-production use only.");
        tester.assertTextNotPresent("open source license for Atlassian.");
        tester.assertTextNotPresent("This JIRA site is for demonstration purposes only.");
    }

    @Test
    public void testEnterpriseCommunityLicenseLoggedOut() {
        try {
            administration.restoreBlankInstanceWithLicense(COMMUNITY);
            navigation.logout();
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Powered by a free Atlassian", "community license for Atlassian.");

            tester.assertTextNotPresent("site is for non-production use only.");
            tester.assertTextNotPresent("open source license for Atlassian.");
            tester.assertTextNotPresent("This JIRA site is for demonstration purposes only.");
        } finally {
            navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        }
    }

    @Test
    public void testEnterpriseDeveloperLicense() {
        administration.restoreBlankInstanceWithLicense(DEVELOPER);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "This", "site is for non-production use only.");

        tester.assertTextNotPresent("Powered by a free Atlassian");
        tester.assertTextNotPresent("community license for Atlassian.");
        tester.assertTextNotPresent("open source license for Atlassian.");
        tester.assertTextNotPresent("This JIRA site is for demonstration purposes only.");
    }

    @Test
    public void testEnterpriseDeveloperLicenseLoggedOut() {
        try {
            administration.restoreBlankInstanceWithLicense(DEVELOPER);
            navigation.logout();
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "This", "site is for non-production use only.");

            tester.assertTextNotPresent("Powered by a free Atlassian");
            tester.assertTextNotPresent("community license for Atlassian.");
            tester.assertTextNotPresent("open source license for Atlassian.");
            tester.assertTextNotPresent("This JIRA site is for demonstration purposes only.");
        } finally {
            navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        }
    }

    @Test
    public void testEnterprisePersonalLicense() {
        administration.restoreBlankInstanceWithLicense(PERSONAL);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"A", "free bug tracker", "for up to three users? Try", "JIRA Personal", "Edition."});

        tester.assertTextNotPresent("Powered by a free Atlassian");
        tester.assertTextNotPresent("community license for Atlassian.");
        tester.assertTextNotPresent("open source license for Atlassian.");
        tester.assertTextNotPresent("This JIRA site is for demonstration purposes only.");
        tester.assertTextNotPresent("site is for non-production use only.");
    }

    @Test
    public void testEnterprisePersonalLicenseLoggedOut() {
        try {
            administration.restoreBlankInstanceWithLicense(PERSONAL);
            navigation.logout();
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"A", "free bug tracker", "for up to three users? Try", "JIRA Personal", "Edition."});

            tester.assertTextNotPresent("Powered by a free Atlassian");
            tester.assertTextNotPresent("community license for Atlassian.");
            tester.assertTextNotPresent("open source license for Atlassian.");
            tester.assertTextNotPresent("This JIRA site is for demonstration purposes only.");
            tester.assertTextNotPresent("site is for non-production use only.");
        } finally {
            navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        }
    }

    @Test
    public void testEnterpriseOpenSourceLicense() {
        administration.restoreBlankInstanceWithLicense(OPEN_SOURCE);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Powered by a free Atlassian", "open source license for Atlassian.");

        tester.assertTextNotPresent("community license for Atlassian.");
        tester.assertTextNotPresent("site is for non-production use only.");
        tester.assertTextNotPresent("This JIRA site is for demonstration purposes only.");
    }

    @Test
    public void testEnterpriseOpenSourceLicenseLoggedOut() {
        try {
            administration.restoreBlankInstanceWithLicense(OPEN_SOURCE);
            navigation.logout();
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Powered by a free Atlassian", "open source license for Atlassian.");

            tester.assertTextNotPresent("community license for Atlassian.");
            tester.assertTextNotPresent("site is for non-production use only.");
            tester.assertTextNotPresent("This JIRA site is for demonstration purposes only.");
        } finally {
            navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        }

    }

    @Test
    public void testEnterpriseDemonstrationLicense() {
        administration.restoreBlankInstanceWithLicense(DEMO);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "This JIRA site is for demonstration purposes only.", "bug tracking software for your team.");

        tester.assertTextNotPresent("Powered by a free Atlassian");
        tester.assertTextNotPresent("community license for Atlassian.");
        tester.assertTextNotPresent("site is for non-production use only.");
        tester.assertTextNotPresent("open source license for Atlassian.");
    }

    @Test
    public void testEnterpriseDemonstrationLicenseLoggedOut() {
        try {
            administration.restoreBlankInstanceWithLicense(DEMO);
            navigation.logout();
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "This JIRA site is for demonstration purposes only.", "bug tracking software for your team.");

            tester.assertTextNotPresent("Powered by a free Atlassian");
            tester.assertTextNotPresent("community license for Atlassian.");
            tester.assertTextNotPresent("site is for non-production use only.");
            tester.assertTextNotPresent("open source license for Atlassian.");
        } finally {
            navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        }
    }

    @Test
    public void testEnterpriseLicense() {
        administration.restoreBlankInstanceWithLicense(COMMERCIAL);

        tester.assertTextNotPresent("Powered by a free Atlassian");
        tester.assertTextNotPresent("community license for Atlassian.");
        tester.assertTextNotPresent("site is for non-production use only.");
        tester.assertTextNotPresent("Powered by a free Atlassian");
        tester.assertTextNotPresent("open source license for Atlassian.");
        tester.assertTextNotPresent("This JIRA site is for demonstration purposes only.");
    }

    @Test
    public void testEnterpriseLicenseLoggedOut() {
        try {
            administration.restoreBlankInstanceWithLicense(COMMERCIAL);
            navigation.logout();
            tester.assertTextNotPresent("Powered by a free Atlassian");
            tester.assertTextNotPresent("community license for Atlassian.");
            tester.assertTextNotPresent("site is for non-production use only.");
            tester.assertTextNotPresent("open source license for Atlassian.");
            tester.assertTextNotPresent("This JIRA site is for demonstration purposes only.");
        } finally {
            navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        }
    }

    @Test
    public void testEnterpriseEvaluationLicense() {
        administration.restoreBlankInstanceWithLicense(EVAL_EXPIRED);

        navigation.gotoPage("secure/BrowseProjects.jspa");

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Powered by a free Atlassian", "JIRA evaluation license", "Please consider", "purchasing it", "today"});
        tester.assertTextNotPresent("community license for Atlassian.");
        tester.assertTextNotPresent("site is for non-production use only.");
        tester.assertTextNotPresent("open source license for Atlassian.");
        tester.assertTextNotPresent("This JIRA site is for demonstration purposes only.");
        tester.assertTextNotPresent("for up to three users? Try");
    }

    @Test
    public void testEnterpriseEvaluationLicenseLoggedOut() {
        try {
            administration.restoreBlankInstanceWithLicense(EVAL_EXPIRED);
            navigation.logout();
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Powered by a free Atlassian", "JIRA evaluation license", "Please consider", "purchasing it", "today"});
            tester.assertTextNotPresent("community license for Atlassian.");
            tester.assertTextNotPresent("site is for non-production use only.");
            tester.assertTextNotPresent("open source license for Atlassian.");
            tester.assertTextNotPresent("This JIRA site is for demonstration purposes only.");
            tester.assertTextNotPresent("for up to three users? Try");
        } finally {
            navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        }
    }
}
