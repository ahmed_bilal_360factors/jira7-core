package com.atlassian.jira.webtests.ztests.bulk;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BulkOperations;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.navigation.BulkChangeWizard;
import com.atlassian.jira.functest.framework.navigation.IssueNavigatorNavigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.EmailBaseFuncTestCase;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import javax.mail.internet.MimeMessage;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@WebTest({Category.FUNC_TEST, Category.BULK_OPERATIONS, Category.ISSUES})
@Restore("TestBulkMoveIssuesNotifications.xml")
@LoginAs(user = "test")
public class TestBulkMoveIssuesNotifications extends EmailBaseFuncTestCase {

    public static final String TEXT_ON_ISSUE_MOVED_MAIL = "moved";
    public static final String TEXT_ON_ISSUE_UPDATED_MAIL = "updated";
    @Inject
    private BulkOperations bulk;

    @Before
    public void setUp() {
        configureAndStartSmtpServer();
    }

    @Test
    public void testMovingIssuesSendsEmailWhenWeChangeProject() throws Exception {
        moveIssueToADifferentProject();

        expectEmailWitText(TEXT_ON_ISSUE_MOVED_MAIL);
    }

    @Test
    public void testMovingIssuesSendsEmailWhenWeDoNotChangeProject() throws Exception {
        moveTwoIssuesToTheSameProjectChangingTheIssueTypes();

        expectEmailWitText(TEXT_ON_ISSUE_UPDATED_MAIL);
    }

    private void moveIssueToADifferentProject() {
        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator()
                .bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES)
                .selectAllIssues()
                .chooseOperation(BulkChangeWizard.BulkOperationsImpl.MOVE)
                .chooseTargetContextForAll("project2")
                .finaliseFields()
                .complete();
        bulk.waitAndReloadBulkOperationProgressPage();
    }

    private void moveTwoIssuesToTheSameProjectChangingTheIssueTypes() {
        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator()
                .bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES)
                .selectAllIssues()
                .chooseOperation(BulkChangeWizard.BulkOperationsImpl.MOVE)
                .chooseTargetContextForAll("project1", "Task")
                .finaliseFields()
                .complete();
        bulk.waitAndReloadBulkOperationProgressPage();
    }

    private void expectEmailWitText(String text) throws Exception {
        flushMailQueueAndWait(1);
        List<MimeMessage> messagesForRecipient = getMessagesForRecipient("admin@example.com");
        assertThat(messagesForRecipient.size(), is(1));
        assertEmailBodyContains(messagesForRecipient.get(0), text);
    }
}
