package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.TableCellLocator;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.framework.util.env.EnvironmentUtils;
import com.meterware.httpunit.WebTable;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Check that the SystemInfo page is correct, added for the new ReleaseInfo integration.
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.BROWSING})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestSystemInfoPage extends BaseJiraFuncTest {
    /**
     * installation type comes from the release.info file
     */
    @Inject
    private HtmlPage page;
    @Inject
    private Administration administration;
    @Inject
    private TextAssertions textAssertions;
    @Inject
    private EnvironmentUtils environmentUtils;

    @After
    public void tearDownTest() {
        administration.generalConfiguration().setJiraLocaleToSystemDefault();
    }

    @Test
    public void testInstallationType() {
        String installationType = getEnvironmentData().getReleaseInfo();
        assertFalse("You must have the 'jira.release.info' property set in your localtest.properties", StringUtils.isBlank(installationType));

        administration.restoreBlankInstance();
        navigation.gotoAdminSection(Navigation.AdminSection.SYSTEM_INFO);
        textAssertions.assertTextSequence(new WebPageLocator(tester), new String[]{"Installation Type", installationType});
    }

    @Test
    public void testSupportRequestContainsMemoryAndInputArgsInfo() {
        administration.restoreBlankInstance();
        navigation.gotoAdminSection(Navigation.AdminSection.SYSTEM_INFO);

        if (!isSunJVM()) {
            if (isJvmWithPermGen()) {
                tester.assertTextNotPresent("PermGen Memory Graph");
            }
            tester.assertTextNotPresent("Unable to determine, this requires running JDK 1.5 and higher.");
        } else {
            tester.assertTextPresent("JVM Input Arguments");
            if (isJvmWithPermGen()) {
                tester.assertTextPresent("Used PermGen Memory");
                tester.assertTextPresent("Free PermGen Memory");
                tester.assertTextPresent("PermGen Memory Graph");
            }
            tester.assertTextNotPresent("Unable to determine, this requires running JDK 1.5 and higher.");
        }
    }

    @Test
    public void testSystemInfoContainsTimezoneInfo() throws Exception {
        administration.restoreBlankInstance();
        navigation.gotoAdminSection(Navigation.AdminSection.SYSTEM_INFO);

        final WebTable table = tester.getDialog().getWebTableBySummaryOrId("system_info_table");
        final String systemTime = table.getCellAsText(2, 1).trim();
        assertTrue("System time does not contain GMT offset '" + systemTime + "'", systemTime.matches(".*[-+]\\d{4}"));
        final String timezoneLabel = table.getCellAsText(12, 0);
        assertEquals("User Timezone", timezoneLabel.trim());
        final String timezoneValue = table.getCellAsText(12, 1);
        assertTrue("Timezone value not present in text '" + timezoneValue + "'", timezoneValue.trim().length() > 0);
    }

    @Test
    public void testApplicationPropertiesPresent() {
        administration.restoreBlankInstance();
        navigation.gotoAdminSection(Navigation.AdminSection.SYSTEM_INFO);

        tester.assertTextInTable("application_properties", "jira.baseurl");
        tester.assertTextInTable("application_properties", "jira.option.voting");
        tester.assertTextInTable("application_properties", "jira.option.watching");

        tester.assertTextNotInTable("application_properties", "License Hash 1");
        tester.assertTextNotInTable("application_properties", "License Hash 1 Text");
        tester.assertTextNotInTable("application_properties", "License Message");
        tester.assertTextNotInTable("application_properties", "License Message Text");
        tester.assertTextNotInTable("application_properties", "License20");
        tester.assertTextNotInTable("application_properties", "jira.sid.key");
        tester.assertTextNotInTable("application_properties", "org.apache.shindig.common.crypto.BlobCrypter:key");

    }

    @Test
    public void testSystemInfoContainsJiraHome() {
        administration.restoreBlankInstance();
        navigation.gotoAdminSection(Navigation.AdminSection.SYSTEM_INFO);

        tester.assertTextInTable("file_paths", "Location of JIRA Local Home");
    }

    /**
     * A user with no predefined language gets the language options in the system's default language
     */
    @Test
    public void testShowsLanguageListInDefaultLanguage() {
        administration.restoreData("TestUserProfileI18n.xml");

        administration.generalConfiguration().setJiraLocale("Deutsch (Deutschland)");

        navigation.gotoAdminSection(Navigation.AdminSection.SYSTEM_INFO);

        // assert that the page defaults to German
        final int lastRow = page.getHtmlTable("jirainfo").getRowCount() - 1;
        textAssertions.assertTextPresent(new TableCellLocator(tester, "jirainfo", lastRow, 1), "Deutsch (Deutschland)");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "jirainfo", lastRow - 1, 1), "Deutsch (Deutschland)");
    }

    /**
     * A user with a language preference that is different from the system's language gets the list of languages in his preferred language.
     */
    @Test
    public void testShowsLanguageListInTheUsersLanguage() {
        administration.restoreData("TestUserProfileI18n.xml");

        // set the system locale to something other than English just to be different
        administration.generalConfiguration().setJiraLocale("Deutsch (Deutschland)");

        navigation.login(FRED_USERNAME);

        navigation.gotoAdminSection(Navigation.AdminSection.SYSTEM_INFO);

        // assert that the page defaults to Spanish
        final int lastRow = page.getHtmlTable("jirainfo").getRowCount() - 1;
        textAssertions.assertTextPresent(new TableCellLocator(tester, "jirainfo", lastRow, 1), "alem\u00e1n (Alemania)");
        textAssertions.assertTextPresent(new TableCellLocator(tester, "jirainfo", lastRow - 1, 1), "espa\u00f1ol (Espa\u00f1a)");
    }

    private boolean isJvmWithPermGen() {
        return environmentUtils.isJvmWithPermGen();
    }

    private boolean isSunJVM() {
        return environmentUtils.isSunJVM();
    }
}
