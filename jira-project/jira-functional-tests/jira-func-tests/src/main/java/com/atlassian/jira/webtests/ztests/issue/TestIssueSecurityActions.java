package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.EditIssueFieldVisibility;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.admin.IssueSecurityLevel;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.testkit.client.restclient.Component;
import com.atlassian.jira.testkit.client.restclient.ComponentClient;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.atlassian.jira.testkit.client.restclient.ProjectClient;
import com.atlassian.jira.testkit.client.restclient.Version;
import com.atlassian.jira.testkit.client.restclient.VersionClient;
import com.atlassian.jira.webtests.Groups;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_EMAIL;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.COMPONENT_NAME_ONE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_NEO;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_NEO_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SECURITY_LEVEL_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SECURITY_LEVEL_ONE_DESC;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SECURITY_LEVEL_ONE_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SECURITY_LEVEL_THREE_DESC;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SECURITY_LEVEL_THREE_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SECURITY_LEVEL_TWO_DESC;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SECURITY_LEVEL_TWO_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SECURITY_SCHEME_DESC;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SECURITY_SCHEME_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SUB_TASK_DEFAULT_TYPE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SUB_TASK_SUMMARY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.VERSION_NAME_ONE;
import static com.atlassian.jira.functest.framework.Navigation.AdminSection.SECURITY_SCHEMES;
import static com.atlassian.jira.webtests.Groups.ADMINISTRATORS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueSecurityActions extends BaseJiraFuncTest {
    private static final String DEFAULT_FIELD_CONFIG = "Default Field Configuration";

    @Inject
    private FuncTestLogger logger;

    @Inject
    private EditIssueFieldVisibility editIssueFieldVisibility;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        if (administration.project().projectWithKeyExists(PROJECT_HOMOSAP_KEY)) {
            logger.log("Project 'homospaien' exists");
            if (!(componentExists(COMPONENT_NAME_ONE, PROJECT_HOMOSAP))) {
                addComponent(PROJECT_HOMOSAP, COMPONENT_NAME_ONE);
            }
            if (!(versionExists(VERSION_NAME_ONE, PROJECT_HOMOSAP))) {
                addVersion(PROJECT_HOMOSAP, VERSION_NAME_ONE, "Version 1");
            }
        } else {
            administration.project().addProject(PROJECT_HOMOSAP, PROJECT_HOMOSAP_KEY, ADMIN_USERNAME);
            addComponent(PROJECT_HOMOSAP, COMPONENT_NAME_ONE);
            addVersion(PROJECT_HOMOSAP, VERSION_NAME_ONE, "Version 1");
        }
        if (administration.project().projectWithKeyExists(PROJECT_NEO_KEY)) {
            logger.log("Project '" + PROJECT_NEO + "' exists");
        } else {
            administration.project().addProject(PROJECT_NEO, PROJECT_NEO_KEY, ADMIN_USERNAME);
        }

        if (securtiySchemeExists(SECURITY_SCHEME_NAME)) {
            administration.project().removeAssociationOfIssueLevelSecurityScheme(PROJECT_HOMOSAP);
            administration.project().removeAssociationOfIssueLevelSecurityScheme(PROJECT_NEO);
            deleteSecurityScheme(SECURITY_SCHEME_NAME);
        }

        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        try {
            tester.assertLinkPresentWithText(BOB_USERNAME);
        } catch (Throwable t) {
            administration.usersAndGroups().addUser(BOB_USERNAME, BOB_PASSWORD, BOB_FULLNAME, BOB_EMAIL);

        }

        editIssueFieldVisibility.resetFields();
    }

    @Test
    @RestoreBlankInstance
    public void testIssueSecurityActions() {

        String issueKeyWithoutSecurity = backdoor.issues().createIssue(PROJECT_NEO_KEY, "test without issue security", ADMIN_USERNAME, "Minor", "Bug").key();
        backdoor.issues().setIssueFields(issueKeyWithoutSecurity, new IssueFields()
                .environment("test environment 1")
                .description("test description without issue security"));


        String issueKeyNormal = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "test with components", ADMIN_USERNAME, "Minor", "Bug").key();
        backdoor.issues().setIssueFields(issueKeyNormal, new IssueFields()
                .environment("test environment 1")
                .description("test description with components"));

        issueSecurityCreateScheme();
        issueSecurityFieldSchemesAddDuplicateScheme();
        issueSecurityFieldSchemesAddInvalidScheme();
        issueSecurityAddSecurityLevel();
        issueSecurityAssociateSchemeToProject();

        issueSecurityAddGroupToSecurityLevel();

        String issueKeyWithSecurity = issueSecurityCreateIssueWithSecurity();
        issueSecurityCreateIssueWithoutIssueSecurity();
        issueSecurityCreateIssueWithSecurityRequired();

        issueSecurityEditIssueWithIssueSecurity(issueKeyWithSecurity);
        issueSecurityEditIssueWithoutIssueSecurity(issueKeyWithoutSecurity);
        issueSecurityEditIssueWithSecurityRequired(issueKeyNormal);
        issueSecuritySecurityViolation(issueKeyWithSecurity);

        issueSecurityMoveIssueAwayFromIssueSecurity(issueKeyWithSecurity);
        issueSecurityMoveIssueToIssueSecurity(issueKeyWithSecurity);
        issueSecurityMoveIssueWithDefaultSecurity(issueKeyWithoutSecurity);
        issueSecurityMoveIssueWithSameSecurity(issueKeyWithoutSecurity);

        issueSecurityCreateSubTaskWithSecurity(issueKeyWithSecurity);

        issueSecurityRemoveAssociationWithProject();
        issueSecurityRemoveGroupFromSecurityLevel();
        issueSecurityDeleteSecurityLevel();
        issueSecurityDeleteScheme();
    }

    @Test
    @Restore("TestIssueSecurityScheme.xml")
    public void testProjectRoleIssueSecurityType() {
        // Goto the issue security scheme
        navigation.gotoAdminSection(SECURITY_SCHEMES);
        tester.clickLinkWithText("Security Levels");
        tester.assertTextNotPresent("(Administrators)");

        // Goto level 1 security level
        tester.clickLink("add_level 1");

        // Select a project role security type
        tester.checkCheckbox("type", "projectrole");
        tester.selectOption("projectrole", "Administrators");
        tester.submit(" Add ");

        tester.assertTextPresent("(Administrators)");
    }

    public boolean securtiySchemeExists(String securityScheme) {
        navigation.gotoAdminSection(Navigation.AdminSection.SECURITY_SCHEMES);
        return tester.getDialog().isTextInResponse(securityScheme);
    }

    public String addVersion(String project, String name, String description) {
        logger.log("Adding version '" + name + "' to project '" + project + "'");

        VersionClient versionClient = new VersionClient(environmentData);
        final Version version = new Version();
        version.project(project).name(name).description(description);
        versionClient.create(version);
        return "" + version.id;
    }

    private boolean componentExists(String component, String project) {
        final Project projectByName = getProjectByName(project);

        final Component componentByname = getComponentByname(projectByName.key, component);
        return componentByname != null;

    }

    private boolean versionExists(String version, String project) {
        final Project projectByName = getProjectByName(project);
        final Version versionByName = getVersionByName(projectByName.key, version);

        return versionByName != null;
    }

    private Version getVersionByName(String projectKey, String versionName) {
        ProjectClient projectClient = new ProjectClient(environmentData);

        final List<Version> versions = projectClient.getVersions(projectKey);
        for (Version version : versions) {
            if (version.name.equals(versionName)) {
                return version;
            }
        }
        return null;

    }

    /**
     * Adds a component with the given name (and no lead) to the projectName with the given name.
     *
     * @param projectName the name of the project.
     * @param name        the name of the component.
     * @return the component id.
     */
    private String addComponent(String projectName, String name) {
        return addComponent(projectName, name, null);
    }

    /**
     * Adds a component with the given name and component lead to the projectName with the given name.
     *
     * @param projectName   the name of the project.
     * @param name          the name of the component.
     * @param componentLead the username of the lead for the component, may be null for none.
     * @return the component id.
     */
    private String addComponent(String projectName, String name, String componentLead) {
        final Project project = getProjectByName(projectName);
        final Component componentByname = getComponentByname(project.key, name);

        if (componentByname != null) {
            return "" + componentByname.id;
        }


        ComponentClient componentClient = new ComponentClient(environmentData);

        final Component component = componentClient.create(new Component().project(project.key).name(name).leadUserName(componentLead));

        return "" + component.id;

    }

    private Component getComponentByname(String projectKey, String componentName) {
        ProjectClient projectClient = new ProjectClient(environmentData);
        final List<Component> components = projectClient.getComponents(projectKey);
        for (Component component : components) {
            if (component.name.equals(componentName)) {
                return component;
            }
        }

        return null;

    }

    private Project getProjectByName(String projectName) {
        ProjectClient projectClient = new ProjectClient(environmentData);

        final List<Project> projects = projectClient.getProjects();
        for (Project project : projects) {
            if (project.name.equals(projectName)) {
                return project;
            }
        }
        return null;

    }

    public void issueSecurityCreateScheme() {
        logger.log("Issue Security: Creating a security scheme");
        createSecurityScheme(SECURITY_SCHEME_NAME, SECURITY_SCHEME_DESC);
        tester.assertTextPresent("Issue Security Schemes");
        tester.assertLinkPresentWithText(SECURITY_SCHEME_NAME);
    }

    public void issueSecurityDeleteScheme() {
        logger.log("Issue Security: Deleting a security scheme");
        deleteSecurityScheme(SECURITY_SCHEME_NAME);
        tester.assertLinkNotPresentWithText(SECURITY_SCHEME_NAME);
        tester.assertTextPresent("You do not currently have any issue security schemes configured.");
    }

    private void createSecurityScheme(String scheme_name, String scheme_description) {
        navigation.gotoAdminSection(SECURITY_SCHEMES);
        tester.clickLink("add_securityscheme");

        tester.assertTextPresent("Add issue security scheme");
        tester.setFormElement("name", scheme_name);
        tester.setFormElement("description", scheme_description);

        tester.submit("Add");
    }

    private void deleteSecurityScheme(String scheme_name) {
        navigation.gotoAdminSection(SECURITY_SCHEMES);
        tester.clickLink("del_" + scheme_name);

        tester.assertTextPresent("Delete Issue Security Scheme");
        tester.assertTextPresent(scheme_name);
        tester.submit("Delete");
    }

    public void issueSecurityAddSecurityLevel() {
        logger.log("Issue Security: Adding a security level to a security scheme");
        administration.issueSecuritySchemes().getScheme(SECURITY_SCHEME_NAME).
                newLevel(SECURITY_LEVEL_ONE_NAME, SECURITY_LEVEL_ONE_DESC);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{SECURITY_LEVEL_ONE_NAME, SECURITY_LEVEL_ONE_DESC});
        tester.assertTextNotPresent(SECURITY_LEVEL_TWO_NAME);
        tester.assertTextNotPresent(SECURITY_LEVEL_TWO_DESC);
        tester.assertTextNotPresent(SECURITY_LEVEL_THREE_NAME);
        tester.assertTextNotPresent(SECURITY_LEVEL_THREE_DESC);

        administration.issueSecuritySchemes().getScheme(SECURITY_SCHEME_NAME).
                newLevel(SECURITY_LEVEL_TWO_NAME, SECURITY_LEVEL_TWO_DESC);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{
                SECURITY_LEVEL_TWO_NAME, SECURITY_LEVEL_TWO_DESC,
                SECURITY_LEVEL_ONE_NAME, SECURITY_LEVEL_ONE_DESC
        });
        tester.assertTextNotPresent(SECURITY_LEVEL_THREE_NAME);
        tester.assertTextNotPresent(SECURITY_LEVEL_THREE_DESC);

        administration.issueSecuritySchemes().getScheme(SECURITY_SCHEME_NAME).
                newLevel(SECURITY_LEVEL_THREE_NAME, SECURITY_LEVEL_THREE_DESC);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{
                SECURITY_LEVEL_THREE_NAME, SECURITY_LEVEL_THREE_DESC,
                SECURITY_LEVEL_TWO_NAME, SECURITY_LEVEL_TWO_DESC,
                SECURITY_LEVEL_ONE_NAME, SECURITY_LEVEL_ONE_DESC
        });

        // Check it is added
        navigation.gotoAdminSection(SECURITY_SCHEMES);
        tester.clickLinkWithText(SECURITY_SCHEME_NAME);
        tester.assertLinkPresent("add_" + SECURITY_LEVEL_ONE_NAME);
        tester.assertLinkPresent("add_" + SECURITY_LEVEL_TWO_NAME);
        tester.assertLinkPresent("add_" + SECURITY_LEVEL_THREE_NAME);
    }

    public void issueSecurityDeleteSecurityLevel() {
        logger.log("Issue Security: Deleting a security level to a security scheme");
        deleteSecurityLevel(SECURITY_SCHEME_NAME, SECURITY_LEVEL_ONE_NAME);

        navigation.gotoAdminSection(SECURITY_SCHEMES);
        tester.clickLinkWithText(SECURITY_SCHEME_NAME);
        tester.assertLinkNotPresent("add_" + SECURITY_LEVEL_ONE_NAME);
        tester.assertLinkPresent("add_" + SECURITY_LEVEL_TWO_NAME);
        tester.assertLinkPresent("add_" + SECURITY_LEVEL_THREE_NAME);
    }

    private void deleteSecurityLevel(String scheme_name, String level_name) {
        navigation.gotoAdminSection(Navigation.AdminSection.SECURITY_SCHEMES);
        tester.clickLinkWithText(scheme_name);
        tester.clickLink("delLevel_" + level_name);

        tester.assertTextPresent("Delete Issue Security Level: " + level_name);
        tester.submit("Delete");
    }

    public void issueSecurityAssociateSchemeToProject() {
        logger.log("Issue Security: Associate a Project to a Scheme");

        administration.project().associateIssueLevelSecurityScheme(PROJECT_HOMOSAP, SECURITY_SCHEME_NAME);

        assertThat(backdoor.project().getSchemes(PROJECT_HOMOSAP_KEY).issueSecurityScheme.name, equalTo(SECURITY_SCHEME_NAME));
    }

    public void issueSecurityRemoveAssociationWithProject() {
        logger.log("Issue Security: Remove association between a Project and a Scheme");

        administration.project().removeAssociationOfIssueLevelSecurityScheme(PROJECT_HOMOSAP);

        assertThat(backdoor.project().getSchemes(PROJECT_HOMOSAP_KEY).issueSecurityScheme, nullValue());
    }

    public void issueSecurityAddGroupToSecurityLevel() {
        logger.log("Issue Security: Add groups to issue security level");

        administration.issueSecuritySchemes().getScheme(SECURITY_SCHEME_NAME).getLevel(SECURITY_LEVEL_ONE_NAME).
                addIssueSecurity(IssueSecurityLevel.IssueSecurity.GROUP, Groups.ADMINISTRATORS);
        administration.issueSecuritySchemes().getScheme(SECURITY_SCHEME_NAME).getLevel(SECURITY_LEVEL_TWO_NAME).
                addIssueSecurity(IssueSecurityLevel.IssueSecurity.GROUP, Groups.DEVELOPERS);
        administration.issueSecuritySchemes().getScheme(SECURITY_SCHEME_NAME).getLevel(SECURITY_LEVEL_THREE_NAME).
                addIssueSecurity(IssueSecurityLevel.IssueSecurity.GROUP, Groups.USERS);

        navigation.gotoAdminSection(SECURITY_SCHEMES);
        tester.clickLinkWithText(SECURITY_SCHEME_NAME);
        tester.assertLinkPresent("delGroup_" + Groups.ADMINISTRATORS + "_" + SECURITY_LEVEL_ONE_NAME);
        tester.assertLinkPresent("delGroup_" + Groups.DEVELOPERS + "_" + SECURITY_LEVEL_TWO_NAME);
        tester.assertLinkPresent("delGroup_" + Groups.USERS + "_" + SECURITY_LEVEL_THREE_NAME);
    }

    public void issueSecurityRemoveGroupFromSecurityLevel() {
        logger.log("Issue Security: Remove groups from issue security level");

        removeGroupFromSecurityLevel(SECURITY_SCHEME_NAME, SECURITY_LEVEL_ONE_NAME, Groups.ADMINISTRATORS);

        navigation.gotoAdminSection(SECURITY_SCHEMES);
        tester.clickLinkWithText(SECURITY_SCHEME_NAME);
        tester.assertLinkNotPresent("delGroup_" + Groups.ADMINISTRATORS + "_" + SECURITY_LEVEL_ONE_NAME);
        tester.assertLinkPresent("delGroup_" + Groups.DEVELOPERS + "_" + SECURITY_LEVEL_TWO_NAME);
        tester.assertLinkPresent("delGroup_" + Groups.USERS + "_" + SECURITY_LEVEL_THREE_NAME);
    }

    /**
     * Tests the error handling if a duplicate scheme is made
     */
    public void issueSecurityFieldSchemesAddDuplicateScheme() {
        logger.log("Issue Security: Creating a duplicate security scheme");
        createSecurityScheme(SECURITY_SCHEME_NAME, SECURITY_SCHEME_DESC);
        tester.assertTextPresent("A Scheme with this name already exists.");
    }

    /**
     * Tests the error handling if a scheme with an invalid nameis made
     */
    public void issueSecurityFieldSchemesAddInvalidScheme() {
        logger.log("Issue Security: Creating a duplicate security scheme");
        createSecurityScheme("", "");
        tester.assertTextPresent("Please specify a name for this Scheme.");
    }

    /**
     * Creating issue with issue security settings
     */
    public String issueSecurityCreateIssueWithSecurity() {
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, ADMINISTRATORS);

        String issueKey = navigation.issue().createIssue(PROJECT_HOMOSAP, "Bug", "test with issue security");
        navigation.issue().setPriority(issueKey, "Minor");
        navigation.issue().setSecurity(issueKey, "Red");
        navigation.issue().setAffectsVersions(issueKey, VERSION_NAME_ONE);
        navigation.issue().setFixVersions(issueKey, VERSION_NAME_ONE);
        navigation.issue().setComponents(issueKey, COMPONENT_NAME_ONE);
        navigation.issue().setEnvironment(issueKey, "test environment 9");
        navigation.issue().setDescription(issueKey, "test description 9");

        tester.assertTextPresent("test with issue security");
        tester.assertTextPresent("Minor");
        tester.assertTextPresent("Bug");
        tester.assertTextPresent("test environment 9");
        tester.assertTextPresent(VERSION_NAME_ONE);
        tester.assertTextPresent(COMPONENT_NAME_ONE);
        tester.assertTextPresent("Security Level:");
        tester.assertTextPresent("Red");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, ADMINISTRATORS);
        return issueKey;
    }

    /**
     * Tests if the 'Security Level' field is available with an project WITHOUT an associated 'Issue Security Scheme'
     */
    public void issueSecurityCreateIssueWithoutIssueSecurity() {
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, ADMINISTRATORS);
        logger.log("Create Issue: Tests the availability of the 'Security Level' field");
        navigation.issue().goToCreateIssueForm(PROJECT_NEO, "Bug");

        tester.assertTextPresent("CreateIssueDetails.jspa");

        tester.assertTextPresent(PROJECT_NEO);
        tester.assertFormElementNotPresent("security");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, ADMINISTRATORS);
    }

    /**
     * Tests for error handling with 'Security Level' required
     */
    public void issueSecurityCreateIssueWithSecurityRequired() {
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, ADMINISTRATORS);
        editIssueFieldVisibility.setSecurityLevelToRequried();

        logger.log("Create Issue: Test the ability to create an issue with 'Security Level' required");
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");

        tester.setFormElement("summary", "This is a summary");

        tester.submit("Create");

        tester.assertTextPresent("CreateIssueDetails.jspa");
        tester.assertTextPresent("Security Level is required.");

        editIssueFieldVisibility.resetFields();
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, ADMINISTRATORS);
    }

    /**
     * Tests if the user is able to alter the security level of an issue
     */
    public void issueSecurityEditIssueWithIssueSecurity(String issueKey) {
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, ADMINISTRATORS);
        logger.log("Edit Issue: Test ability to change Security Level");

        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("edit-issue");

        tester.selectOption("security", SECURITY_LEVEL_TWO_NAME);
        tester.submit();
        tester.assertTextPresent(issueKey);
        tester.assertTextPresent(SECURITY_LEVEL_TWO_NAME);
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, ADMINISTRATORS);
    }

    /**
     * Tests if the 'Security Level' Link is available with an project WITHOUT an associated 'Issue Security Scheme'
     */
    public void issueSecurityEditIssueWithoutIssueSecurity(String issueKey) {
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, ADMINISTRATORS);
        logger.log("Edit Issue: Test availability of 'Security Level' field");
        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("edit-issue");

        tester.assertFormElementNotPresent("security");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, ADMINISTRATORS);
    }

    /**
     * Tests for error handling with 'Security Level' required
     */
    public void issueSecurityEditIssueWithSecurityRequired(String issueKey) {
        editIssueFieldVisibility.setSecurityLevelToRequried();
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, ADMINISTRATORS);

        logger.log("Edit Issue: Test the ability to update an issue with 'Security Level' required");
        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("edit-issue");

        tester.submit("Update");

        tester.assertTextPresent("Edit Issue");
        tester.assertTextPresent("Security Level is required.");

        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, ADMINISTRATORS);
        editIssueFieldVisibility.resetFields();
    }

    /**
     * Tests if a user can breach Issue Security()
     */
    public void issueSecuritySecurityViolation(String issueKey) {
        logger.log("Edit Issue: Test the availabilty of an issue for which a user is not permitted to view.");
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, ADMINISTRATORS);
        navigation.logout();
        navigation.login(BOB_USERNAME, BOB_PASSWORD);
        navigation.issue().gotoIssue(issueKey);
        assertions.getViewIssueAssertions().assertIssueNotFound();
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, ADMINISTRATORS);
    }

    /**
     * Tests the availabilty of 'Issue Security' when moving an issue to a project WITHOUT issue security
     */
    public void issueSecurityMoveIssueAwayFromIssueSecurity(String issueKey) {
        logger.log("Move Operation: Moving an issue to a project without 'Issue Security");
        navigation.issue().gotoIssue(issueKey);

        tester.clickLink("move-issue");
        tester.assertTextPresent("Move Issue");

        navigation.issue().selectProject(PROJECT_NEO);
        tester.submit();

        tester.assertTextPresent("Step 3 of 4");
        tester.assertTextNotPresent("All fields will be updated automatically.");
        tester.getDialog().setWorkingForm("jiraform");
        tester.submit();

        tester.assertTextPresent("Step 4 of 4");
        tester.assertTextPresent(PROJECT_NEO);
        tester.assertTextPresent(PROJECT_HOMOSAP);
        tester.getDialog().setWorkingForm("jiraform");
        tester.submit("Move");

        tester.assertTextNotPresent("Security Level");
        tester.assertTextNotPresent("Red");
    }

    /**
     * Tests the availabilty of 'Issue Security' when moving an issue to a project WITH issue security
     */
    public void issueSecurityMoveIssueToIssueSecurity(String issueKey) {
        logger.log("Move Operation: Moving an issue to a project with 'Issue Security");
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, ADMINISTRATORS);

        editIssueFieldVisibility.setRequiredFieldsOnEnterprise(DEFAULT_FIELD_CONFIG, SECURITY_LEVEL_FIELD_ID);
        navigation.issue().gotoIssue(issueKey);

        tester.clickLink("move-issue");
        tester.assertTextPresent("Move Issue");

        navigation.issue().selectProject(PROJECT_HOMOSAP);
        tester.submit();

        tester.assertTextPresent("Step 3 of 4");
        tester.selectOption("security", SECURITY_LEVEL_TWO_NAME);

        tester.submit();
        tester.assertTextPresent("Step 4 of 4");
        tester.submit("Move");

        tester.assertTextPresent("Security Level:");
        tester.assertTextPresent("Orange");

        editIssueFieldVisibility.setOptionalFieldsOnEnterprise(DEFAULT_FIELD_CONFIG, SECURITY_LEVEL_FIELD_ID);
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, ADMINISTRATORS);
    }

    /**
     * Tests that default security level is used if user does not have permission to set security
     */
    public void issueSecurityMoveIssueWithDefaultSecurity(String issueKey) {
        logger.log("Move Operation: Moving an issue with default security levels");
        editIssueFieldVisibility.setRequiredFieldsOnEnterprise(DEFAULT_FIELD_CONFIG, SECURITY_LEVEL_FIELD_ID);
        navigation.issue().gotoIssue(issueKey);

        tester.clickLink("move-issue");
        tester.assertTextPresent("Move Issue");

        navigation.issue().selectProject(PROJECT_HOMOSAP);
        tester.submit();

        tester.assertTextPresent("Move Issue: Update Fields");
        tester.getDialog().setWorkingForm("jiraform");
        tester.submit();

        tester.assertTextPresent("Security Level: Security Level is required.");

        setDefaultSecurityLevel(SECURITY_SCHEME_NAME, SECURITY_LEVEL_ONE_NAME);
        navigation.issue().gotoIssue(issueKey);

        tester.clickLink("move-issue");
        tester.assertTextPresent("Move Issue");

        navigation.issue().selectProject(PROJECT_HOMOSAP);
        tester.submit();

        tester.assertTextPresent("Move Issue: Update Fields");
        tester.setWorkingForm("jiraform");
        tester.submit();

        tester.assertTextPresent("Move Issue: Confirm");
        tester.setWorkingForm("jiraform");
        tester.submit();

        tester.assertTextPresent("Security Level:");
        tester.assertTextPresent(SECURITY_LEVEL_ONE_NAME);
        setDefaultSecurityLevel(SECURITY_SCHEME_NAME, null);
        editIssueFieldVisibility.setOptionalFieldsOnEnterprise(DEFAULT_FIELD_CONFIG, SECURITY_LEVEL_FIELD_ID);
    }

    /**
     * Test that security level stays the same if moved between the same security scheme
     */
    public void issueSecurityMoveIssueWithSameSecurity(String issueKey) {
        logger.log("Move Operation: Move an issue to a project with the same issue security scheme.");
        administration.project().associateIssueLevelSecurityScheme(PROJECT_NEO, SECURITY_SCHEME_NAME);
        navigation.issue().gotoIssue(issueKey);

        tester.clickLink("move-issue");
        tester.assertTextPresent("Move Issue");

        navigation.issue().selectProject(PROJECT_NEO);
        tester.submit();

        tester.assertTextPresent("Move Issue: Update Fields");
        tester.setWorkingForm("jiraform");
        tester.submit();

        tester.assertTextPresent("Move Issue: Confirm");
        tester.setWorkingForm("jiraform");
        tester.submit();

        tester.assertTextPresent("Security Level:");
        tester.assertTextPresent(SECURITY_LEVEL_ONE_NAME);

        administration.project().removeAssociationOfIssueLevelSecurityScheme(PROJECT_NEO);
    }

    /**
     * Tests if a sub task has its security level automatically allocated
     */
    public void issueSecurityCreateSubTaskWithSecurity(String issueKey) {
        logger.log("Sub Task Create: Create a sub task from an issue with a security level");
        //        grantGroupPermission(SET_ISSUE_SECURITY, JIRA_ADMIN);
        navigation.issue().createSubTaskStep1(issueKey, SUB_TASK_DEFAULT_TYPE);
        tester.setFormElement("summary", SUB_TASK_SUMMARY);
        tester.submit();
        tester.assertTextPresent("test with issue security");
        tester.assertTextPresent(SUB_TASK_SUMMARY);
        tester.assertTextPresent(SECURITY_LEVEL_TWO_NAME);

        deleteCurrentIssue();

        administration.subtasks().disable();
//        removeGroupPermission(SET_ISSUE_SECURITY, JIRA_ADMIN);
    }

    private void setDefaultSecurityLevel(String scheme_name, String securityLevel) {
        navigation.gotoAdminSection(SECURITY_SCHEMES);
        tester.clickLinkWithText(scheme_name);
        if (securityLevel != null) {
            tester.clickLink("default_" + securityLevel);
        } else {
            tester.clickLinkWithText("Change default security level to \"None\"");
        }
    }

    public void removeGroupFromSecurityLevel(String scheme_name, String level_name, String groupName) {
        navigation.gotoAdminSection(SECURITY_SCHEMES);
        tester.clickLinkWithText(scheme_name);

        tester.clickLink("delGroup_" + groupName + "_" + level_name);
        tester.submit("Delete");

    }

    private void deleteCurrentIssue() {
        tester.clickLink("delete-issue");
        tester.setWorkingForm("delete-issue");
        tester.submit("Delete");
    }
}
