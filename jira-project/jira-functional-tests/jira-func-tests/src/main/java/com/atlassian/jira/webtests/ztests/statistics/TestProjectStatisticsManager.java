package com.atlassian.jira.webtests.ztests.statistics;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Collection;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.PROJECTS})
@Restore("blankprojects.xml")
public class TestProjectStatisticsManager extends BaseJiraRestTest {
    public static final String ISSUE_TYPE_BUG = "1";
    public static final String ISSUE_TYPE_FEATURE = "2";
    public static final String MAJOR_PRIORITY_ID = "3";
    
    @Inject
    private Backdoor backdoor;

    @Test
    public void testCorrectProjectsEmptyJQL() {
        final String jql = "";

        // null query
        Collection<String> result = backdoor.statisticsControl().getProjectsResultingFrom();
        assertThat(result, hasSize(0));

        // no issues
        result = backdoor.statisticsControl().getProjectsResultingFrom(jql);
        assertThat(result, hasSize(0));

        // issues in one project
        backdoor.issues().createIssue("HSP", "New issue for the HSP project");
        result = backdoor.statisticsControl().getProjectsResultingFrom(jql);
        assertThat(result, hasItem("HSP"));

        // issues in both projects
        backdoor.issues().createIssue("MKY", "New issue for the MKY project");
        result = backdoor.statisticsControl().getProjectsResultingFrom(jql);
        assertThat(result, hasItems("HSP", "MKY"));
    }

    @Test
    public void testCorrectProjectsWithAmbiguousJQL() {
        final String jql = "issuetype = Bug";

        // no issues
        Collection<String> result = backdoor.statisticsControl().getProjectsResultingFrom(jql);
        assertThat(result, hasSize(0));

        // issues in one project
        backdoor.issues().createIssue("HSP", "It's a bug", null, MAJOR_PRIORITY_ID, ISSUE_TYPE_BUG);
        result = backdoor.statisticsControl().getProjectsResultingFrom(jql);
        assertThat(result, hasItem("HSP"));

        // issues in both projects
        backdoor.issues().createIssue("MKY", "It's not a bug, it's a feature", null, MAJOR_PRIORITY_ID, ISSUE_TYPE_FEATURE);
        result = backdoor.statisticsControl().getProjectsResultingFrom(jql);
        assertThat(result, hasItem("HSP"));
    }

    @Test
    public void testCorrectProjectsWithExplicitProjectsJQL() {
        final String jql = "Project = MKY";

        // no issues
        Collection<String> result = backdoor.statisticsControl().getProjectsResultingFrom(jql);
        assertThat(result, hasSize(0));

        // issues in one project
        backdoor.issues().createIssue("HSP", "It's a bug", null, MAJOR_PRIORITY_ID, ISSUE_TYPE_BUG);
        result = backdoor.statisticsControl().getProjectsResultingFrom(jql);
        assertThat(result, hasSize(0));

        // issues in both projects
        backdoor.issues().createIssue("MKY", "It's not a bug, it's a feature", null, MAJOR_PRIORITY_ID, ISSUE_TYPE_FEATURE);
        result = backdoor.statisticsControl().getProjectsResultingFrom(jql);
        assertThat(result, hasItem("MKY"));
    }
}
