package com.atlassian.jira.functest.framework.email;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;

import javax.inject.Inject;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

import static com.jayway.awaitility.Awaitility.await;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.equalTo;

/**
 * Helper for dealing with MimeMessages
 *
 * @since v7.1
 */
public class OutgoingMailHelper {

    private final Backdoor backdoor;

    @Inject
    public OutgoingMailHelper(Backdoor backdoor) {
        this.backdoor = backdoor;
    }

    public List<MimeMessage> filterMessages(final Collection<MimeMessage> messages, final Predicate<MimeMessage> predicate) {
        return messages.stream().filter(predicate).collect(CollectorsUtil.toImmutableList());
    }

    public List<MimeMessage> filterMessagesForRecipient(final Collection<MimeMessage> messages, String recipient) {
        return filterMessages(messages, message -> {
            try {
                return asList(message.getHeader("To")).contains(recipient);
            } catch (MessagingException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public Collection<MimeMessage> flushMailQueueAndWait(final int emailCount) {
        return flushMailQueueAndWait(emailCount, 5000);
    }

    /**
     * Waits at most {@code waitPeriodMillis} milliseconds for {@code mailCount} mails to be present in outgoing mail
     * queue. If condition is not met within requested timeout then {@code ConditionTimeoutException} is thrown. A check
     * if smtp is configured is performed prior flushing queue.
     *
     * @param emailCount       Number of mails in queue to wait for.
     * @param waitPeriodMillis Amount of time to wait for mails to appear in queue.
     * @return Collected mime messages
     */
    public Collection<MimeMessage> flushMailQueueAndWait(final long emailCount, final int waitPeriodMillis) {
        if (!backdoor.getTestkit().mailServers().isSmtpConfigured()) {
            throw new RuntimeException("Smtp is not configured properly. Check the existence of @Before annotation in the test and presence of backdoor.restoreData().");
        }

        flushMailQueue();

        await().atMost(waitPeriodMillis, TimeUnit.MILLISECONDS)
                .until(() -> backdoor.outgoingMailControl().getMails().count(), equalTo(emailCount));

        return backdoor.outgoingMailControl().getMails().collect(CollectorsUtil.toImmutableList());
    }

    private void flushMailQueue() {
        //flush mail queue
        backdoor.mailServers().flushMailQueue();
    }

    public void clearOutgoingMailQueue() {
        backdoor.outgoingMailControl().clearMessages();
    }


    public String getPreviewUrl(MimeMessage message) {
        try {
            return backdoor.outgoingMailControl().getMessagePreviewURI(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    public MimeMessage getMessageForAddress(Collection<MimeMessage> messages, String toAddress)
            throws MessagingException {
        return messages.stream().filter(message ->
                        toAddress.equals(getRecipients(message, Message.RecipientType.TO)[0].toString())
        ).findFirst().orElse(null);
    }

    private Address[] getRecipients(MimeMessage message, Message.RecipientType recipientType) {
        try {
            return message.getRecipients(recipientType);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
