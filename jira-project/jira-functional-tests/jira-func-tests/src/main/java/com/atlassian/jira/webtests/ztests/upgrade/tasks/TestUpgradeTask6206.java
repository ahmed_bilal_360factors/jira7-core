package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.net.URISyntaxException;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.UPGRADE_TASKS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @since v6.2
 */
@WebTest({FUNC_TEST, UPGRADE_TASKS})
public class TestUpgradeTask6206 extends BaseJiraFuncTest {

    private JiraRestClient restClient;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Before
    public void setUpTest() {
        final AsynchronousJiraRestClientFactory restClientFactory = new AsynchronousJiraRestClientFactory();
        try {
            restClient = restClientFactory.createWithBasicHttpAuthentication(environmentData.getBaseUrl().toURI(), ADMIN_USERNAME, ADMIN_PASSWORD);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testStatusesAssignedAsExpected() {
        administration.restoreDataWithBuildNumber("JDEV-24290-TestStatusCategoryAssignment.xml", 6204);
        backdoor.darkFeatures().enableForSite("jira.issue.status.lozenge");
        navigation.gotoPage("/secure/admin/ViewStatuses.jspa");

        List<String> lozenges = getStatusNamesFromStatusLozengeClass(".jira-issue-status-lozenge");
        assertThat(lozenges.size(), equalTo(15));

        List<String> undefined = getStatusNamesFromStatusLozengeClass(".jira-issue-status-lozenge-undefined");
        assertThat(undefined.size(), equalTo(1));
        assertThat(undefined.get(0), equalTo("The loneliest state"));

        List<String> done = getStatusNamesFromStatusLozengeClass(".jira-issue-status-lozenge-done");
        assertThat(done.size(), equalTo(4));
        assertThat(done, Matchers.<String>hasItems(equalTo("Resolved"), equalTo("Closed"), equalTo("RCRT - Hired"), equalTo("RCRT - Rejected")));

        List<String> todo = getStatusNamesFromStatusLozengeClass(".jira-issue-status-lozenge-new");
        assertThat(todo.size(), equalTo(3));
        assertThat(todo, Matchers.<String>hasItems(equalTo("Open"), equalTo("Reopened"), equalTo("RCRT - Resume Check")));
    }

    private List<String> getStatusNamesFromStatusLozengeClass(final String selector) {
        Node[] nodes = locator.css(selector).getNodes();
        return Lists.transform(Lists.newArrayList(nodes), new Function<Node, String>() {
            @Override
            public String apply(@Nullable final Node item) {
                return (item == null) ? null : String.valueOf(item.getParentNode().getPreviousSibling().getFirstChild().getNodeValue());
            }
        });
    }
}
