package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.bulk.TestBulkMoveTimeTracking;
import com.atlassian.jira.webtests.ztests.screens.tabs.TestFieldScreenTabsOnCreateIssue;
import com.atlassian.jira.webtests.ztests.screens.tabs.TestFieldScreenTabsOnEditIssue;
import com.atlassian.jira.webtests.ztests.screens.tabs.TestFieldScreenTabsOnResolveIssue;
import com.atlassian.jira.webtests.ztests.timetracking.legacy.TestCreateWorklog;
import com.atlassian.jira.webtests.ztests.timetracking.legacy.TestDeleteWorklog;
import com.atlassian.jira.webtests.ztests.timetracking.legacy.TestUpdateWorklog;
import com.atlassian.jira.webtests.ztests.timetracking.legacy.TestWorkLogOperationVisibility;
import com.atlassian.jira.webtests.ztests.timetracking.legacy.TestWorkLogTabPanel;
import com.atlassian.jira.webtests.ztests.timetracking.legacy.TestWorkLogTabPanelVisibility;
import com.atlassian.jira.webtests.ztests.timetracking.modern.TestCreateWorklogOnCloseTransition;
import com.atlassian.jira.webtests.ztests.timetracking.modern.TestCreateWorklogOnCreateIssue;
import com.atlassian.jira.webtests.ztests.timetracking.modern.TestCreateWorklogOnEditIssue;
import com.atlassian.jira.webtests.ztests.timetracking.modern.TestCreateWorklogOnResolveTransition;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of tests around Worklogs
 *
 * @since v4.0
 */
public class FuncTestSuiteWorklogs {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestWorkLogOperationVisibility.class)
                .add(TestWorkLogTabPanel.class)
                .add(TestWorkLogTabPanelVisibility.class)
                .add(TestCreateWorklog.class)
                .add(TestUpdateWorklog.class)
                .add(TestDeleteWorklog.class)
                .add(TestCreateWorklogOnCloseTransition.class)
                .add(TestCreateWorklogOnResolveTransition.class)
                .add(TestCreateWorklogOnCreateIssue.class)
                .add(TestCreateWorklogOnEditIssue.class)
                .add(TestFieldScreenTabsOnCreateIssue.class)
                .add(TestFieldScreenTabsOnEditIssue.class)
                .add(TestFieldScreenTabsOnResolveIssue.class)
                .add(TestBulkMoveTimeTracking.class)
                .build();
    }
}