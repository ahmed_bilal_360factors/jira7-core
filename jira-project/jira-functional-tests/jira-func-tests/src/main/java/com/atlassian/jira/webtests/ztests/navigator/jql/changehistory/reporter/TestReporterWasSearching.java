package com.atlassian.jira.webtests.ztests.navigator.jql.changehistory.reporter;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.ChangeHistoryAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Set;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * @since v4.4
 */
@WebTest({Category.FUNC_TEST, Category.JQL, Category.CHANGE_HISTORY})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestChangeHistorySearch.xml")
public class TestReporterWasSearching extends BaseJiraFuncTest {
    private static final String FIELD_NAME = "reporter";
    private static final String[] ALL_ISSUES = {"HSP-9", "HSP-8", "HSP-7", "HSP-6", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1"};

    @Inject
    private ChangeHistoryAssertions changeHistoryAssertions;

    @Before
    public void setUpTest() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    public void testWasEmptySearch() {
        final String[] issueKeys = {"HSP-9"};
        changeHistoryAssertions.assertWasEmptySearchReturnsEmptyValuesUsingEmptyKeyword(FIELD_NAME, issueKeys);
    }

    @Test
    public void testWasNotEmptySearch() {
        final String[] issueKeys = {"HSP-8", "HSP-7", "HSP-6", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1"};
        changeHistoryAssertions.assertWasNotEmptySearchReturnsNotEmptyValuesWithEmptyKeyword(FIELD_NAME, issueKeys);
    }

    @Test
    public void testWasSearchUsingSingleValueOperandsReturnsExpectedValues() {
        changeHistoryAssertions.assertWasSearchReturnsExpectedValues(FIELD_NAME, "admin", ALL_ISSUES);
        changeHistoryAssertions.assertWasSearchReturnsExpectedValues(FIELD_NAME, "fred", "HSP-9");
        changeHistoryAssertions.assertWasSearchReturnsExpectedValues(FIELD_NAME, "bob");
    }

    @Test
    public void testWasSearchUsingListOperands() {
        Set users = Sets.newHashSet("fred", "admin");
        changeHistoryAssertions.assertWasInSearchReturnsExpectedValues(FIELD_NAME, users, ALL_ISSUES);
        users = Sets.newHashSet("fred", "bob");
        changeHistoryAssertions.assertWasInSearchReturnsExpectedValues(FIELD_NAME, users, "HSP-9");
    }

    @Test
    public void testWasNotInSearchUsingListOperands() {
        final String[] expected = {"HSP-8", "HSP-7", "HSP-6", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1"};
        final Set users = Sets.newHashSet("fred", "bob");
        changeHistoryAssertions.assertWasNotInSearchReturnsExpectedValues(FIELD_NAME, users, expected);
    }


    @Test
    public void testWasSearchUsingByPredicate() {
        String[] expected = {"HSP-9"};
        changeHistoryAssertions.assertWasBySearchReturnsExpectedValues(FIELD_NAME, "fred", "admin", expected);
        expected = new String[]{};
        changeHistoryAssertions.assertWasBySearchReturnsExpectedValues(FIELD_NAME, "admin", "fred", expected);
        changeHistoryAssertions.assertWasBySearchUsingListOperandsReturnsExpectedValues(FIELD_NAME, "empty", Sets.newHashSet("fred", "admin"), "HSP-9");
    }

    @Test
    public void testWasSearchUsingDuringPredicate() {
        changeHistoryAssertions.assertWasDuringSearchReturnsExpectedValues(FIELD_NAME, "admin", "'2011/05/01'", "'2011/05/31'", ALL_ISSUES);
        changeHistoryAssertions.assertWasDuringSearchReturnsExpectedValues(FIELD_NAME, "fred", "'2011/05/01'", "'2011/05/31'", "HSP-9");
    }

    @Test
    public void testWasSearchUsingBeforePredicate() {
        final String[] expected = {"HSP-4", "HSP-3", "HSP-2", "HSP-1"};
        changeHistoryAssertions.assertWasBeforeSearchReturnsExpectedValues(FIELD_NAME, "admin", "'2010/05/01'", expected);
        changeHistoryAssertions.assertWasBeforeSearchReturnsExpectedValues(FIELD_NAME, "fred", "'2011/05/21 10:55'", "HSP-9");
    }

    @Test
    public void testWasSearchUsingAfterPredicate() {
        changeHistoryAssertions.assertWasAfterSearchReturnsExpectedValues(FIELD_NAME, "admin", "'2011/05/01'", ALL_ISSUES);
        final String[] expected = new String[]{"HSP-8", "HSP-7", "HSP-6", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1"};
        changeHistoryAssertions.assertWasAfterSearchReturnsExpectedValues(FIELD_NAME, "admin", "'2011/06/03 10:55'", expected);
    }

    @Test
    public void testWasSearchUsingOnPredicate() {
        String[] expected = {"HSP-8", "HSP-7", "HSP-6", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1"};
        changeHistoryAssertions.assertWasOnSearchReturnsExpectedValues(FIELD_NAME, "admin", "'2011/05/01'", expected);
        expected = new String[]{"HSP-9"};
        changeHistoryAssertions.assertWasOnSearchReturnsExpectedValues(FIELD_NAME, "fred", "'2011/06/01'", expected);
    }

    @Test
    public void testWasSearchUsingLongOperandsIsInvalid() {
        // invalid user id
        final String expectedError = "A value with ID '1' does not exist for the field 'reporter'.";
        changeHistoryAssertions.assertInvalidSearchProducesError(FIELD_NAME, "1", "", expectedError);
    }

    @Test
    public void testWasSearchUsingUnclosedListIsInvalid() {
        // invalid list
        final String expectedError = "Error in the JQL Query: Expecting ')' before the end of the query.";
        changeHistoryAssertions.assertInvalidSearchProducesError(FIELD_NAME, "(fred, bob", "", expectedError);
    }

    @Test
    public void testWasSearchUsingIncorrectPredicateIsInvalid() {
        // invalid predicate
        final String expectedError = "Error in the JQL Query: Expecting either 'OR' or 'AND' but got 'at'. (line 1, character 26)";
        changeHistoryAssertions.assertInvalidSearchProducesError(FIELD_NAME, "(fred, bob)", "at '10:55'", expectedError);
    }

    @Test
    public void testWasSearchUsingIncorrectFunctionIsInvalid() {
        // invalid operand  type
        final String expectedError = "A value provided by the function 'currentLogin' is invalid for the field 'reporter'.";
        changeHistoryAssertions.assertInvalidSearchProducesError(FIELD_NAME, "currentLogin()", "", expectedError);
    }
}
