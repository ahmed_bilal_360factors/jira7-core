package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Attachment;
import com.atlassian.jira.testkit.client.restclient.AttachmentClient;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;

/**
 * Func test for the attachment resource.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@Restore("TestIssueResourceAttachments.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestAttachmentResource extends BaseJiraRestTest {
    private AttachmentClient attachmentClient;
    private IssueClient issueClient;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        issueClient = new IssueClient(environmentData);
        attachmentClient = new AttachmentClient(environmentData);
    }

    @Test
    public void testViewAttachment() throws Exception {
        // first try to open the issue that contains the attachments
        Issue mky1 = issueClient.get("MKY-1");
        assertThat(mky1.fields.attachment.size(), equalTo(3));

        // {
        //   self: http://localhost:8090/jira/rest/api/2/attachment/10000
        //   filename: attachment.txt
        //   author: {
        //     self: http://localhost:8090/jira/rest/api/2/user?username=admin
        //     name: admin
        //     displayName: Administrator
        //   }
        //   created: 2010-06-09T15:59:34.602+1000
        //   size: 19
        //   mimeType: text/plain
        //   properties: {
        //     magickey7: 1311805918487
        //     magickey6: "Picture of an big long elephant"
        //     magickey5: "Picture of an elephant"
        //     magickey4: 3.14
        //     magickey3: 1212121
        //     magickey2: 11
        //     magickey1: true
        //   }
        //   content: http://localhost:8090/jira/secure/attachment/10000/attachment.txt
        // }

        // now go and verify one of those attachments
        Attachment attachment1 = attachmentClient.get("10000");
        Assert.assertEquals(environmentData.getBaseUrl() + "/rest/api/2/attachment/10000", attachment1.self);
        Assert.assertEquals("attachment.txt", attachment1.filename);
        Assert.assertEquals(environmentData.getBaseUrl() + "/rest/api/2/user?username=admin", attachment1.author.self);
        Assert.assertEquals(ADMIN_USERNAME, attachment1.author.name);
        Assert.assertEquals(ADMIN_FULLNAME, attachment1.author.displayName);
        textAssertions.assertEqualDateStrings("2010-06-09T15:59:34.602+1000", attachment1.created);
        Assert.assertEquals(19L, attachment1.size);
        Assert.assertEquals("text/plain", attachment1.mimeType);
        Assert.assertEquals(environmentData.getBaseUrl() + "/secure/attachment/10000/attachment.txt", attachment1.content);
        Assert.assertTrue((Boolean) attachment1.properties.get("magickey1"));
        Assert.assertEquals(Integer.valueOf(11), attachment1.properties.get("magickey2"));
        Assert.assertEquals(Integer.valueOf(1212121), attachment1.properties.get("magickey3"));
        Assert.assertEquals(Double.valueOf(3.14), attachment1.properties.get("magickey4"));
        Assert.assertEquals("Picture of an elephant", attachment1.properties.get("magickey5"));
        Assert.assertEquals("Picture of an big long elephant", attachment1.properties.get("magickey6"));
        Assert.assertEquals("2011-07-28T08:31:58.487+1000", attachment1.properties.get("magickey7"));
    }

    @Test
    public void testViewAttachmentNotFound() throws Exception {
        // {"errorMessages":["The attachment with id '123' does not exist"],"errors":[]}
        Response response123 = attachmentClient.getResponse("123");
        Assert.assertEquals(404, response123.statusCode);
        Assert.assertEquals(1, response123.entity.errorMessages.size());
        Assert.assertTrue(response123.entity.errorMessages.contains("The attachment with id '123' does not exist"));

        // {"errorMessages":["The attachment with id 'abc' does not exist"],"errors":[]}
        Response responseAbc = attachmentClient.getResponse("abc");
        Assert.assertEquals(404, responseAbc.statusCode);
        Assert.assertEquals(1, responseAbc.entity.errorMessages.size());
        Assert.assertTrue(responseAbc.entity.errorMessages.contains("The attachment with id 'abc' does not exist"));
    }

    @Test
    public void testViewAttachmentNotAuthorised() throws Exception {
        Response response = attachmentClient.anonymous().getResponse("10000");
        Assert.assertEquals(403, response.statusCode);
        Assert.assertEquals(1, response.entity.errorMessages.size());
        Assert.assertTrue(response.entity.errorMessages.contains("You do not have permission to view attachment with id: 10000"));
    }

    @Test
    public void testRemoveAttachment() throws Exception {
        // first try to open the issue that contains the attachments
        Issue mky1 = issueClient.get("MKY-1");
        assertThat(mky1.fields.attachment.size(), equalTo(3));

        // Get the attachment first
        Attachment attachment1 = attachmentClient.get("10000");
        Assert.assertEquals("attachment.txt", attachment1.filename);
        // Delete it
        Response response = attachmentClient.deleteResponse("10000");
        Assert.assertEquals(204, response.statusCode);

        // Try and get the attachment again, should be 404
        response = attachmentClient.getResponse("10000");
        Assert.assertEquals(404, response.statusCode);
        mky1 = issueClient.get("MKY-1");
        assertThat(mky1.fields.attachment.size(), equalTo(2));
    }

    @Test
    public void testDeleteAttachmentNotFound() throws Exception {
        // {"errorMessages":["The attachment with id '123' does not exist"],"errors":[]}
        Response response123 = attachmentClient.deleteResponse("123");
        Assert.assertEquals(404, response123.statusCode);
        Assert.assertEquals(1, response123.entity.errorMessages.size());
        Assert.assertTrue(response123.entity.errorMessages.contains("The attachment with id '123' does not exist"));

        // {"errorMessages":["The attachment with id 'abc' does not exist"],"errors":[]}
        Response responseAbc = attachmentClient.deleteResponse("abc");
        Assert.assertEquals(404, responseAbc.statusCode);
        Assert.assertEquals(1, responseAbc.entity.errorMessages.size());
        Assert.assertTrue(responseAbc.entity.errorMessages.contains("The attachment with id 'abc' does not exist"));
    }

    @Test
    public void testGetMeta() throws Exception {
        Map responseMeta = attachmentClient.getMeta();
        Assert.assertEquals(true, responseMeta.get("enabled"));
        Assert.assertEquals(10485760, responseMeta.get("uploadLimit"));
    }

}
