package com.atlassian.jira.functest.framework.admin;

import com.atlassian.jira.functest.framework.Navigation;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;

/**
 * @since 7.2
 */
public class StatusesImpl implements Statuses {

    private final WebTester tester;
    private final Navigation navigation;

    @Inject
    public StatusesImpl(final WebTester tester, final Navigation navigation) {
        this.tester = tester;
        this.navigation = navigation;
    }

    public void addLinkedStatus(final String statusName, final String statusDesc) {
        navigation.gotoPage("/secure/admin/AddStatus!default.jspa");
        tester.setFormElement("name", statusName);
        tester.setFormElement("description", statusDesc);
        tester.submit("Add");
    }

    public void deleteLinkedStatus(final String statusId) {
        navigation.gotoPage("/secure/admin/ViewStatuses.jspa");
        tester.clickLink("del_" + statusId);
        tester.submit("Delete");
    }


}
