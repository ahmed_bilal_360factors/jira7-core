package com.atlassian.jira.webtests.ztests.admin.audit;

import com.atlassian.jira.auditing.AssociatedItem;
import com.atlassian.jira.auditing.AuditRecord;
import com.atlassian.jira.auditing.AuditingCategory;
import com.atlassian.jira.auditing.ChangedValue;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;

/**
 * @since 7.0.0
 */
class TestingAuditRecord implements AuditRecord {
    private String description = null;

    @Nonnull
    @Override
    public Long getId() {
        return 1L;
    }

    @Nonnull
    @Override
    public Date getCreated() {
        return new Date(1395254742967L);
    }

    @Nonnull
    @Override
    public AuditingCategory getCategory() {
        return AuditingCategory.AUDITING;
    }

    @Nonnull
    @Override
    public String getSummary() {
        return "AuditLoggerTest";
    }

    @Nonnull
    @Override
    public String getEventSource() {
        return "JIRA Func tests";
    }

    public void setDescription(String desc) {
        this.description = desc;
    }

    @Nullable
    @Override
    public String getDescription() {
        return description;
    }

    @Nullable
    @Override
    public String getRemoteAddr() {
        return null;
    }

    @Nullable
    @Override
    public String getAuthorKey() {
        return "example-user";
    }

    @Nonnull
    @Override
    public Iterable<AssociatedItem> getAssociatedItems() {
        return ImmutableList.<AssociatedItem>of();
    }

    @Nonnull
    @Override
    public Iterable<ChangedValue> getValues() {
        return ImmutableList.<ChangedValue>of();
    }

    @Nullable
    @Override
    public AssociatedItem getObjectItem() {
        return new AssociatedItem() {
            @Nonnull
            @Override
            public String getObjectName() {
                return "user";
            }

            @Nullable
            @Override
            public String getObjectId() {
                return "usr";
            }

            @Nullable
            @Override
            public String getParentName() {
                return "JIRA Internal Directory";
            }

            @Nullable
            @Override
            public String getParentId() {
                return "1";
            }

            @Nonnull
            @Override
            public Type getObjectType() {
                return Type.USER;
            }
        };
    }
}
