package com.atlassian.jira.webtests.ztests.navigator;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.AbstractTestIssueNavigatorColumnsView;
import com.meterware.httpunit.WebTable;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.util.Arrays;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Format.DAYS;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Format.HOURS;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

@WebTest({Category.FUNC_TEST, Category.ISSUE_NAVIGATOR, Category.ISSUES})
@Restore("TestSearchRequestViewsAndIssueViews.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueNavigatorPrintableView extends AbstractTestIssueNavigatorColumnsView {

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testTimeTrackingPrintableView() throws SAXException {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        administration.subtasks().enable();
        backdoor.issueTableClient().getIssueTable(""); // create session
        backdoor.columnControl().addLoggedInUserColumns(Arrays.asList(
                "aggregatetimeoriginalestimate", "aggregateprogress", "aggregatetimeestimate", "aggregatetimespent"));

        tester.gotoPage("/sr/jira.issueviews:searchrequest-printable/temp/SearchRequest.html?jqlQuery=&tempMax=1000");

        //Check that the issue table contains the correct fields for each issue.
        final WebTable issueTable = tester.getDialog().getResponse().getTableWithID("issuetable");

        assertions.getTableAssertions().assertTableCellHasText(issueTable, 0, 45, "\u03A3 Original Estimate");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 0, 46, "\u03A3 Progress");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 0, 47, "\u03A3 Remaining Estimate");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 0, 48, "\u03A3 Time Spent");

        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 46, "86%");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 47, "30 minutes");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 48, "3 hours, 20 minutes");

        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 45, "1 day");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 46, "0%");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 47, "1 day");
    }

    @Test
    public void testAllColumnsPrintableView() throws SAXException {
        logger.log("Issue Navigator: Test that the printable view shows all required items");
        tester.gotoPage("/sr/jira.issueviews:searchrequest-printable/temp/SearchRequest.html?jqlQuery=&tempMax=1000");
        //Check that the issue table contains the correct fields for each issue.
        final WebTable issueTable = tester.getDialog().getResponse().getTableWithID("issuetable");

        for (final Item item : items) {
            new ItemVerifier(this, item, issueTable, getEnvironmentData().getBaseUrl()).verify();
        }

    }

    //JRA-15984
    @Test
    public void testPrintableViewXSSBug() {
        final String filter = backdoor.filters().createFilter("", "<script>alert('evil');</script>");
        //now view the printable view and esure the name is encoded properly.
        tester.gotoPage("/sr/jira.issueviews:searchrequest-printable/" + filter + "/SearchRequest-" + filter + ".html?tempMax=1000");
        tester.assertTextPresent("&lt;script&gt;alert(&#39;evil&#39;);&lt;/script&gt; (Your Company JIRA)");
    }

    @Test
    public void testAllColumnsPrintableViewDaysFormat() throws SAXException {
        reconfigureTimetracking(DAYS);
        testAllColumnsPrintableView();
    }

    @Test
    public void testAllColumnsPrintableViewHoursFormat() throws SAXException {
        reconfigureTimetracking(HOURS);
        testAllColumnsPrintableView();
    }

    @Test
    public void testSearchRequestHeaderSummaryDisplaysCorrectTotals() {
        logger.log("Issue Navigator: Test that the printable view shows all required items");
        navigation.gotoPage("/sr/jira.issueviews:searchrequest-printable/temp/SearchRequest.html?sorter/field=issuekey&sorter/order=DESC&tempMax=2");

        tester.assertTextPresent("<b>1</b>&ndash;<b>2</b> of <b>3</b>");

        navigation.gotoPage("/sr/jira.issueviews:searchrequest-printable/temp/SearchRequest.html?sorter/field=issuekey&sorter/order=DESC");

        tester.assertTextPresent("<b>1</b>&ndash;<b>3</b> of <b>3</b>");
    }

    @Test
    public void testSearchRequestSummaryWithSimpleAndAdvancedQuery() throws Exception {
        tester.gotoPage("/sr/jira.issueviews:searchrequest-printable/temp/SearchRequest.html?jqlQuery=status%20=%20'In Progress'&tempMax=1000");

        // make our assertions about Simple query summary
        CssLocator summaryLocator = new CssLocator(tester, ".result-header");
        textAssertions.assertTextSequence(summaryLocator, "Status", "In Progress");
        textAssertions.assertTextNotPresent(summaryLocator, "JQL Query");
        textAssertions.assertTextNotPresent(summaryLocator, "status = 'In Progress'");


        tester.gotoPage("/sr/jira.issueviews:searchrequest-printable/temp/SearchRequest.html?jqlQuery=status%20!=%20'In Progress'&tempMax=1000");

        // make our assertions about Advanced query summary
        summaryLocator = new CssLocator(tester, ".result-header");
        textAssertions.assertTextSequence(summaryLocator, "JQL Query", "status != 'In Progress'");
        textAssertions.assertTextNotPresent(summaryLocator, "Status");
    }
}
