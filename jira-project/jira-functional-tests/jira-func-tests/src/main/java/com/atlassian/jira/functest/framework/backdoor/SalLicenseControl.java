package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.annotation.Nullable;
import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * @since v7.0
 */
public class SalLicenseControl extends BackdoorControl<SalLicenseControl> {
    public SalLicenseControl(final JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public boolean addProductLicense(String productKey, License licenseKey) {
        return addProductLicense(productKey, licenseKey.getLicenseString());
    }

    public boolean addProductLicense(String productKey, String licenseKey) {
        try {
            createSalLicenseResource()
                    .path("products")
                    .type(MediaType.APPLICATION_JSON_TYPE)
                    .post(new ProductLicenseTO(productKey, licenseKey));
            return true;
        } catch (Exception anyException) {
            return false;
        }
    }

    public Boolean deleteProductLicense(String productKey) {
        return createSalLicenseResource()
                .path("products").path(productKey)
                .delete(Boolean.class);
    }

    public List<String> getProductKeys() {
        return createSalLicenseResource()
                .path("products")
                .get(new GenericType<List<String>>() {
                });
    }

    public String getRawProductLicense(String productKey) {
        return createSalLicenseResource()
                .path("products").path(productKey)
                .get(String.class);
    }

    public String getSupportEntitlementNumber() {
        return createSalLicenseResource()
                .path("sen")
                .get(String.class);
    }

    public String getServerId() {
        return createSalLicenseResource()
                .path("serverid")
                .get(String.class);
    }

    public String ping() {
        return createSalLicenseResource()
                .path("ping")
                .get(String.class);
    }

    public Boolean hostAllowsCustomProducts() {
        return createSalLicenseResource()
                .path("host")
                .path("allowsCustomProducts")
                .get(Boolean.class);
    }

    public Boolean hostAllowsMultipleLicenses() {
        return createSalLicenseResource()
                .path("host")
                .path("allowsMultipleLicenses")
                .get(Boolean.class);
    }

    public SingleProductLicenseDetailsViewTO getProductLicenseDetails(String productKey) {
        return createSalLicenseResource().
                path("product").path("details")
                .path(productKey).get(new GenericType<SingleProductLicenseDetailsViewTO>() {
                });
    }

    public List<MultiProductLicenseDetailsTO> getAllProductLicenses() {
        return createSalLicenseResource().
                path("product").path("details")
                .get(new GenericType<List<MultiProductLicenseDetailsTO>>() {
                });
    }

    public MultiProductLicenseDetailsTO decodeLicenseDetails(String rawLicenseString) {
        return createSalLicenseResource()
                .path("product").path("details").path("decode")
                .type(MediaType.APPLICATION_JSON_TYPE)
                .post(MultiProductLicenseDetailsTO.class, new ProductLicenseTO(null, rawLicenseString));
    }

    public MultiProductLicenseDetailsTO decodeLicenseDetails(License license) {
        return decodeLicenseDetails(license.getLicenseString());
    }

    public ValidationResultTO validateLicenseString(String productKey, License license, Locale locale) {
        return validateLicenseString(productKey, license.getLicenseString(), locale);
    }

    public ValidationResultTO validateLicenseString(String productKey, String rawLicenseString, Locale locale) {
        return createSalLicenseResource()
                .path("product").path("validate")
                .type(MediaType.APPLICATION_JSON_TYPE)
                .post(ValidationResultTO.class, new ProductLicenseTO(productKey, rawLicenseString, locale));
    }

    private WebResource createSalLicenseResource() {
        return createResource().path("sal").path("license");
    }

    public static class MultiProductLicenseDetailsTO extends BaseLicenseDetailsTO {
        @JsonProperty
        public List<ProductLicenseTO> productLicenses;

        @Nullable
        public ProductLicenseTO getProductByKey(final String productKey) {
            for (ProductLicenseTO productLicense : productLicenses) {
                if (productLicense.productKey.equals(productKey)) {
                    return productLicense;
                }
            }
            return null;
        }
    }

    public static class ProductLicenseTO {
        @JsonProperty
        public String licenseKey;
        @JsonProperty
        public String productKey;
        @JsonProperty
        public boolean isUnlimitedNumberOfUsers;
        @JsonProperty
        public int numberOfUsers;
        @JsonProperty
        public String productDisplayName;
        @JsonProperty
        Locale locale;

        public ProductLicenseTO() {
        }

        public ProductLicenseTO(final String licenseKey) {
            this.licenseKey = licenseKey;
        }

        public ProductLicenseTO(final String productKey, final String licenseKey) {
            this.productKey = productKey;
            this.licenseKey = licenseKey;
        }

        public ProductLicenseTO(final String productKey, final String licenseKey, final Locale locale) {
            this.productKey = productKey;
            this.licenseKey = licenseKey;
            this.locale = locale;
        }
    }

    public static class BaseLicenseDetailsTO {
        @JsonProperty
        public boolean isEvaluationLicense;
        @JsonProperty
        public String licenseTypeName;
        @JsonProperty
        public String organisationName;
        @JsonProperty
        public String supportEntitlementNumber;
        @JsonProperty
        public String description;
        @JsonProperty
        public String serverId;
        @JsonProperty
        public boolean isPerpetualLicense;
        @JsonProperty
        public Date licenseExpiryDate;
        @JsonProperty
        public Date maintenanceExpiryDate;
        @JsonProperty
        public boolean isDataCenter;
        @JsonProperty
        public boolean isEnterpriseLicensingAgreement;
    }

    public static class ValidationResultTO {
        @JsonProperty
        protected Set<String> errorMessages;
        @JsonProperty
        protected Set<String> warningMessages;

        public ValidationResultTO() {
            this.errorMessages = Sets.newHashSet();
            this.warningMessages = Sets.newHashSet();
        }

        public ValidationResultTO(final Iterable<String> errorMessages, final Iterable<String> warningMessages) {
            this.errorMessages = ImmutableSet.copyOf(errorMessages);
            this.warningMessages = ImmutableSet.copyOf(warningMessages);
        }

        public boolean isValid() {
            return errorMessages.isEmpty();
        }

        public boolean hasErrors() {
            return !errorMessages.isEmpty();
        }

        public boolean hasWarnings() {
            return !warningMessages.isEmpty();
        }

        public Set<String> getErrorMessages() {
            return errorMessages;
        }

        public Set<String> getWarningMessages() {
            return warningMessages;
        }

        @Override
        public String toString() {
            return "ValidationResultTO{" +
                    "isValid=" + isValid() +
                    ", hasErrors=" + hasErrors() +
                    ", hasWarnings=" + hasWarnings() +
                    ", errorMessages=" + errorMessages +
                    ", warningMessages=" + warningMessages +
                    '}';
        }
    }
}