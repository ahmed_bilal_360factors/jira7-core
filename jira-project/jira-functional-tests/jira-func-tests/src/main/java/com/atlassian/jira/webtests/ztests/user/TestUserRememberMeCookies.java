package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.webtests.WebTesterFactory;
import net.sourceforge.jwebunit.WebTester;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;

/**
 *
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestUserRememberMeCookies extends BaseJiraFuncTest {
    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
        for (int i = 0; i < 5; i++) {
            loginAs(ADMIN_USERNAME);
        }
    }

    @Test
    public void testHasRememberMeCookies() {
        gotoRememberMeUserBrowserAdminSection();

        textAssertions.assertTextSequence(locator.xpath("//form[@id='rememberme_cookies_form']//h2"), "Remember My Login", ADMIN_FULLNAME);

        assertEquals("There should be 5 rows of cookies", 5, locator.xpath("//table[@id='rememberme_cookies_table']//tbody/tr").getNodes().length);

        tester.submit("Submit");

        assertHasNoRememberMeCookies();
    }

    @Test
    public void testUserProfileRememberMeClear() {
        navigation.gotoPage("secure/ViewProfile.jspa");
        tester.clickLink("view_clear_rememberme");

        textAssertions.assertTextPresent("Remember My Login");
        tester.submit("Clear");
        textAssertions.assertTextPresent("Your remember my login tokens have successfully been cleared");

        gotoRememberMeUserBrowserAdminSection();
        assertHasNoRememberMeCookies();
    }


    @Test
    public void testUserProfileRememberMeClear_NotLoggedIn() {
        navigation.logout();
        navigation.gotoPage("secure/ClearRememberMeCookies!default.jspa");
        textAssertions.assertTextPresent("You must be logged in to clear your remember my login tokens.");
    }

    @Test
    public void testAllUsersRememberMeClear() {
        // direct page goto
        navigation.gotoPage("secure/admin/user/AllUsersRememberMeCookies!default.jspa");
        assertWeAreOnTheClearAllPage(5);

        // via the plugin point
        navigation.gotoAdminSection(Navigation.AdminSection.REMEMBERME);
        assertWeAreOnTheClearAllPage(5);

        tester.submit("Submit");
        textAssertions.assertTextPresent("All the tokens have been cleared.");
        assertWeAreOnTheClearAllPage(0);

        gotoRememberMeUserBrowserAdminSection();
        assertHasNoRememberMeCookies();
    }

    private void assertWeAreOnTheClearAllPage(final Integer numberOfTokens) {
        textAssertions.assertTextPresent("Remember My Login for All Users");
        textAssertions.assertTextPresent("to clear all of these tokens from this JIRA site.");
        textAssertions.assertTextPresent(locator.xpath("//div[@class='form-body']/p/strong"), numberOfTokens.toString());
    }


    private void assertHasNoRememberMeCookies() {
        textAssertions.assertTextPresent(locator.xpath("//form[@id='rememberme_cookies_form']"), "No login tokens have been set for: " + ADMIN_FULLNAME);
    }

    private void gotoRememberMeUserBrowserAdminSection() {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.clickLink(ADMIN_USERNAME);
        tester.clickLink("rememberme_link");
    }

    private void loginAs(final String userName) {
        final WebTester tester = WebTesterFactory.createNewWebTester(environmentData);
        tester.beginAt("/login.jsp");
        tester.setFormElement("os_username", userName);
        tester.setFormElement("os_password", userName);
        tester.setFormElement("os_cookie", "true");
        tester.setWorkingForm("login-form");
        tester.submit();
        logger.log("Started session for " + userName);
    }
}
