package com.atlassian.jira.webtests.ztests.avatar;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.UPGRADE_TASKS;
import static org.junit.Assert.assertEquals;

@WebTest({FUNC_TEST, UPGRADE_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestAvatarSettingsMigration extends BaseJiraFuncTest {
    static final ImmutableList<String> USERS = ImmutableList.of(
            "user_with_avatar",
            "user_without_avatar"
    );

    static final Map<String, String> GRAVATAR_ON = ImmutableMap.of(
            "user_with_avatar", "http://www.gravatar.com/avatar/3b273c72f359240538caceb63b6384ef?d=mm&s=48",
            "user_without_avatar", "http://www.gravatar.com/avatar/13570ed7405a9e0a95744ea68e55017e?d=mm&s=48"
    );

    static final Map<String, String> GRAVATAR_OFF = ImmutableMap.of(
            "user_with_avatar", "/secure/useravatar?avatarId=10040",
            "user_without_avatar", "/secure/useravatar?avatarId=10062"
    );

    @Test
    @Restore("avatar/GravatarsOn.xml")
    public void testGravatarsRemainOnAfterUpgrade() throws Exception {
        for (String user : USERS) {
            assertEquals(user + " should keep their gravatar after upgrade", GRAVATAR_ON.get(user), backdoor.userProfile().getAvatarUrl(user));
        }
    }

    @Test
    @Restore("avatar/GravatarsOff.xml")
    public void testGravatarsRemainOffAfterUpgradeWhenExplicitlyOff() throws Exception {
        String base = getEnvironmentData().getBaseUrl().toString();
        for (String user : ImmutableList.of("user_with_avatar", "user_without_avatar")) {
            assertEquals(user + " should keep their internal avatar after upgrade", base + GRAVATAR_OFF.get(user), backdoor.userProfile().getAvatarUrl(user));
        }
    }

    @Test
    @Restore("avatar/GravatarsDefault.xml")
    public void testGravatarsRemainOffAfterUpgradeWhenDefaultedToOff() throws Exception {
        String base = getEnvironmentData().getBaseUrl().toString();
        for (String user : ImmutableList.of("user_with_avatar", "user_without_avatar")) {
            assertEquals(user + " should keep their internal avatar after upgrade", base + GRAVATAR_OFF.get(user), backdoor.userProfile().getAvatarUrl(user));
        }
    }
}
