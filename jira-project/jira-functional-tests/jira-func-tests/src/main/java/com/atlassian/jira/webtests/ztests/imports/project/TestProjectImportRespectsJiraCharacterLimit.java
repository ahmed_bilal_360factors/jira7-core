package com.atlassian.jira.webtests.ztests.imports.project;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;

import static org.hamcrest.Matchers.arrayWithSize;
import static org.junit.Assert.assertThat;

/**
 * Tests the validation errors for text exceeding jira character limit displayed the project import summary page.
 */
@WebTest({Category.FUNC_TEST, Category.PROJECT_IMPORT, Category.DATABASE})
public class TestProjectImportRespectsJiraCharacterLimit extends BaseJiraProjectImportFuncTest
        implements FunctTestConstants {

    @Inject
    private TextAssertions textAssertions;

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void givesValidationErrorsWhenIssueTextFieldsExceedLimit() throws Exception {
        try (AutoCloseable ignored = tempFile(doProjectImport("TestProjectImportRespectsJiraCharacterLimit.zip", "TestProjectImportStandardSimpleDataNoIssuesWith3kLimit.xml", "homosapien"))) {
            // Check that the import has been stopped
            tester.assertTextPresent("The data mappings have produced errors, you can not import this project until all errors have been resolved. See below for details.");
            // Assert we have no Submit Button.
            tester.assertSubmitButtonNotPresent("Import");

            // Test that there is only one error in system fields
            XPathLocator pathLocator = new XPathLocator(tester, "//div[@id='system-fields']//div[@class='description']");
            assertThat(pathLocator.getNodes(), arrayWithSize(1));
            textAssertions.assertTextPresent(pathLocator, "The largest text field with length of 4646 exceeds the allowed limit of 3000. Increase the limit in advanced settings to continue.");

            // Test that there is only one error in custom fields
            pathLocator = new XPathLocator(tester, "//div[@id='custom-fields']//div[@class='description']");
            assertThat(pathLocator.getNodes(), arrayWithSize(1));
            textAssertions.assertTextPresent(pathLocator, "The largest text field with length of 4661 exceeds the allowed limit of 3000. Increase the limit in advanced settings to continue.");

        }
    }

    private AutoCloseable tempFile(final File file) {
        return file::delete;
    }
}
