package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.testkit.beans.PermissionSchemeBean;
import com.atlassian.jira.testkit.client.restclient.GenericRestClient;
import com.atlassian.jira.testkit.client.restclient.PermissionSchemeRestClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response.Status;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.REST})
@RestoreBlankInstance
@LoginAs(user = ADMIN_USERNAME)
public final class TestProjectPermissionSchemeResource extends BaseJiraRestTest {
    public static final String SCHEME_NAME = "scheme";
    public static final String SCHEME_DESC = "desc";
    public static final String PROJECT_KEY = "PR";
    public static final String PROJECT_NAME = "project";
    private PermissionSchemeRestClient client;

    private Long schemeId;
    private Long projectId;
    private GenericRestClient genericRestClient;

    @Before
    public void setUpRestClients() {
        client = new PermissionSchemeRestClient(getEnvironmentData()).loginAs("admin", "admin");
        genericRestClient = new GenericRestClient();
        genericRestClient.loginAs("admin", "admin");
    }

    @Before
    public void setUpJiraData() {
        schemeId = backdoor.permissionSchemes().createScheme(SCHEME_NAME, SCHEME_DESC);
        projectId = backdoor.project().addProject(PROJECT_NAME, PROJECT_KEY, "admin");
        backdoor.project().setPermissionScheme(projectId, schemeId);
    }

    @Test
    public void testAdminCanGetAssignedPermissionScheme() {
        assumeUserCanGetAssignedScheme("admin");
    }

    @Test
    public void testProjectAdministratorCanGetAssignedPermissionScheme() {
        assumeFredAdministersProject();
        assumeUserCanGetAssignedScheme("fred");
    }

    @Test
    public void testNotProjectAdministratorGets403WhenTryingToGetPermissionScheme() {
        assumeEveryoneCanSeeTheProject();
        assertThat(client.loginAs("fred").getAssignedScheme(PROJECT_KEY).statusCode, equalTo(Status.FORBIDDEN.getStatusCode()));
    }

    @Test
    public void testAdminCanAssignPermissionScheme() {
        final Long schemeId = backdoor.permissionSchemes().createScheme("newScheme", "newDesc");
        for (final String projectKeyOrId : new String[]{PROJECT_KEY, projectId.toString()}) {
            final Response<PermissionSchemeBean> response = client.assignScheme(projectKeyOrId, schemeId);
            assertResponse(response, "newScheme", "newDesc");
            assumeUserCanGetAssignedScheme("admin", "newScheme", "newDesc");
        }
    }

    @Test
    public void testNonAdminGets403WhenTryingToUpdatePermissionScheme() {
        assumeEveryoneCanSeeTheProject();
        assertThat(client.loginAs("fred").assignScheme(PROJECT_KEY, schemeId).statusCode, equalTo(Status.FORBIDDEN.getStatusCode()));
    }

    @Test
    public void testProjectAdministratorGets403WhenTryingToUpdatePermissionScheme() {
        assumeFredAdministersProject();
        assertThat(client.loginAs("fred").assignScheme(PROJECT_KEY, schemeId).statusCode, equalTo(Status.FORBIDDEN.getStatusCode()));
    }

    @Test
    public void testUserThatDoesntHaveAccessToProjectGets404WhenTryingToViewOrAssignPermissionScheme() {
        assertThat(client.loginAs("fred").assignScheme(PROJECT_KEY, schemeId).statusCode, equalTo(Status.NOT_FOUND.getStatusCode()));
        assertThat(client.loginAs("fred").getAssignedScheme(PROJECT_KEY).statusCode, equalTo(Status.NOT_FOUND.getStatusCode()));
    }

    @Test
    public void testAnonymousGets401WhenTryingToGetOrAssignPermissionScheme() {
        assertThat(client.anonymous().assignScheme(PROJECT_KEY, schemeId).statusCode, equalTo(Status.UNAUTHORIZED.getStatusCode()));
        assertThat(client.anonymous().getAssignedScheme(PROJECT_KEY).statusCode, equalTo(Status.UNAUTHORIZED.getStatusCode()));
    }

    @Test
    public void testAssigningNonExistentPermissionSchemeReturns404() {
        assertThat(client.assignScheme(PROJECT_KEY, 4444L).statusCode, equalTo(Status.NOT_FOUND.getStatusCode()));
    }

    private void assumeUserCanGetAssignedScheme(final String username) {
        assumeUserCanGetAssignedScheme(username, SCHEME_NAME, SCHEME_DESC);
    }

    private void assumeUserCanGetAssignedScheme(final String username, final String schemeName, final String schemeDesc) {
        final Response<PermissionSchemeBean> responseByProjectKey = client.loginAs(username).getAssignedScheme(PROJECT_KEY);
        final Response<PermissionSchemeBean> responseByProjectId = client.loginAs(username).getAssignedScheme(projectId.toString());

        for (final Response<PermissionSchemeBean> response : ImmutableList.of(responseByProjectKey, responseByProjectId)) {
            assertResponse(response, schemeName, schemeDesc);
        }
    }

    private void assertResponse(final Response<PermissionSchemeBean> response, final String schemeName, final String schemeDesc) {
        assertResponseWithoutCheckingSelf(response, schemeName, schemeDesc);
        assertResponseWithoutCheckingSelf(genericRestClient.get(response.body.getSelf(), PermissionSchemeBean.class), schemeName, schemeDesc);
    }

    private void assertResponseWithoutCheckingSelf(final Response<PermissionSchemeBean> response, final String schemeName, final String schemeDesc) {
        assertThat(response.body.getName(), equalTo(schemeName));
        assertThat(response.body.getDescription(), equalTo(schemeDesc));
        assertThat(response.statusCode, equalTo(Status.OK.getStatusCode()));
    }

    private void assumeEveryoneCanSeeTheProject() {
        backdoor.permissionSchemes().addEveryonePermission(schemeId, ProjectPermissions.BROWSE_PROJECTS);
    }

    private void assumeFredAdministersProject() {
        backdoor.permissionSchemes().addUserPermission(schemeId, ProjectPermissions.ADMINISTER_PROJECTS, "fred");
    }
}
