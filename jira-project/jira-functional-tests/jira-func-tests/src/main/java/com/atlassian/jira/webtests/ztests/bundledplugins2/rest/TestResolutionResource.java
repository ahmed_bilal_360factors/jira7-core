package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Resolution;
import com.atlassian.jira.testkit.client.restclient.ResolutionClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Func test for the resolution resource.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestResolutionResource extends BaseJiraFuncTest {

    private ResolutionClient resolutionClient;

    @Before
    public void setUp() {
        resolutionClient = new ResolutionClient(getEnvironmentData());
    }

    @Test
    public void testAllResolutions() throws Exception {
        List<Resolution> resolutions = resolutionClient.get();

        assertResolutionsContain(resolutions, "1");
        assertResolutionsContain(resolutions, "2");
        assertResolutionsContain(resolutions, "3");
        assertResolutionsContain(resolutions, "4");
        assertResolutionsContain(resolutions, "5");
    }

    private void assertResolutionsContain(List<Resolution> resolutions, String id) {
        for (Resolution resolution : resolutions) {
            if (resolution.id.equals(id)) {
                return;
            }
        }
        fail("Resolution " + id + " not in list");
    }

    @Test
    public void testViewResolution() throws Exception {
        Resolution resolution = resolutionClient.get("2");
        assertEquals(environmentData.getBaseUrl() + "/rest/api/2/resolution/2", resolution.self);
        assertEquals("The problem described is an issue which will never be fixed.", resolution.description);
        assertEquals("2", resolution.id);
        assertEquals("Won't Fix", resolution.name);
    }

    @Test
    public void testViewResolutionNotFound() throws Exception {
        Response resp999 = resolutionClient.getResponse("999");
        assertEquals(404, resp999.statusCode);
        assertEquals(1, resp999.entity.errorMessages.size());
        assertTrue(resp999.entity.errorMessages.contains("The resolution with id '999' does not exist"));

        Response respBoom = resolutionClient.getResponse("boom");
        assertEquals(404, respBoom.statusCode);
        assertEquals(1, respBoom.entity.errorMessages.size());
        assertTrue(respBoom.entity.errorMessages.contains("The resolution with id 'boom' does not exist"));
    }
}
