package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.comment.TestAddCommentFooterLink;
import com.atlassian.jira.webtests.ztests.comment.TestAddCommentHeaderLink;
import com.atlassian.jira.webtests.ztests.comment.TestCommentDelete;
import com.atlassian.jira.webtests.ztests.comment.TestCommentNotifications;
import com.atlassian.jira.webtests.ztests.comment.TestCommentVisibility;
import com.atlassian.jira.webtests.ztests.comment.TestEditComment;
import com.atlassian.jira.webtests.ztests.issue.TestWikiRendererXSS;
import com.atlassian.jira.webtests.ztests.issue.comments.TestCommentPermissions;
import com.atlassian.jira.webtests.ztests.issue.move.TestDeleteHiddenFieldOnMove;
import com.atlassian.jira.webtests.ztests.misc.TestReplacedLocalVelocityMacros;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of test related to comments
 *
 * @since v4.0
 */
public class FuncTestSuiteComments {

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestDeleteHiddenFieldOnMove.class)
                .add(TestEditComment.class)
                .add(TestCommentDelete.class)
                .add(TestCommentNotifications.class)
                        // NOTE: This test has needs to have some tests commented in once we figure out why httpunit is not
                .add(TestAddCommentHeaderLink.class)
                .add(TestAddCommentFooterLink.class)
                .add(TestCommentVisibility.class)
                .add(TestCommentPermissions.class)
                .add(TestReplacedLocalVelocityMacros.class)
                .add(TestWikiRendererXSS.class)
                .build();
    }

}