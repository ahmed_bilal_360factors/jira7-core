package com.atlassian.jira.webtests.ztests.workflow;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.ProgressBar;
import com.atlassian.jira.functest.framework.WorkflowSchemes;
import com.atlassian.jira.functest.framework.admin.ViewWorkflows;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.WorkflowSchemesControl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ASSIGN_FIELD_SCREEN_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_ASSIGNEE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.STATUS_IN_PROGRESS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.STATUS_OPEN;
import static com.atlassian.jira.functest.framework.FunctTestConstants.TRANSIION_NAME_START_PROGRESS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.TRANSIION_NAME_STOP_PROGRESS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.WORKFLOW_COPIED;
import static com.atlassian.jira.functest.framework.FunctTestConstants.WORKFLOW_SCHEME;
import static com.google.common.collect.Iterables.getOnlyElement;


@WebTest({Category.FUNC_TEST, Category.WORKFLOW})
public class TestCustomWorkflow extends BaseJiraFuncTest {

    @Inject
    private ProgressBar progressBar;

    private WorkflowSchemes workflowSchemes;

    private WorkflowSchemesControl workflowSchemesControl;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        workflowSchemes = new WorkflowSchemes(tester, assertions, backdoor, progressBar, navigation);
        workflowSchemesControl = new WorkflowSchemesControl(environmentData);
        backdoor.userProfile().changeUserTimezone("admin", TimeZone.getDefault().getID());
    }

    /**
     * Test that the workflow conditions use the unmodified issue. This is because:
     * To figure out what workflow operations are available we call to OSWorkflow passing it an issue.
     * When the workflow action is executed, we gather the information from the user for issue update and then
     * execute the transition. When the transition is executed OSWorkflow checks teh conditions again. However, the
     * issue has been modified by this stage. If the modified issue causes a workflow condition to fail, the user gets
     * an error.
     * We need to ensure that we do the condition checks against an unmodified issue.
     */
    @Test
    public void testConditionsUseUnmodifiedIssue() {
        administration.restoreData("TestCustomWorkflow.xml");
        // Copy default JIRA workflow
        administration.workflows().goTo().copyWorkflow("jira_old", WORKFLOW_COPIED, "Workflow copied from JIRA default");
        Assert.assertTrue(administration.workflows().goTo().inactive().contains(WORKFLOW_COPIED));

        final ViewWorkflows.WorkflowItem copiedWorkflowItem = administration.workflows().goTo().inactive().get(0);


        Assert.assertTrue(copiedWorkflowItem.lastModified().contains(new SimpleDateFormat("dd/MMM/yy").format(new Date())));
        Assert.assertTrue(copiedWorkflowItem.lastModified().contains(ADMIN_FULLNAME));

        editTransitionScreen(WORKFLOW_COPIED, TRANSIION_NAME_START_PROGRESS, ASSIGN_FIELD_SCREEN_NAME);

        addWorkflowPostfunction(WORKFLOW_COPIED, STATUS_IN_PROGRESS, "Stop Progress", "com.atlassian.jira.plugin.system.workflow:assigntolead-function");

        enableWorkflow();

        // Create an issue for testing
        String key = navigation.issue().createIssue(PROJECT_HOMOSAP, "Bug", "Test Issue");

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field");
            navigation.issue().viewIssue(key);
            tester.clickLinkWithText(TRANSIION_NAME_START_PROGRESS);

            tester.selectOption(FIELD_ASSIGNEE, BOB_FULLNAME);
        } finally {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field");
        }

        tester.submit();

        // Ensure we are on the View Issue Screen
        Assert.assertNotNull(tester.getDialog().getResponse().getURL());
        Assert.assertTrue(tester.getDialog().getResponse().getURL().getPath().endsWith("browse/" + key));

        // Ensure the status got set
        textAssertions.assertTextPresent(new IdLocator(tester, "status-val"), STATUS_IN_PROGRESS);
        // Ensure the issue was assigned correctly
        textAssertions.assertTextPresent(new IdLocator(tester, "assignee-val"), BOB_FULLNAME);

        // Login as bob
        navigation.logout();
        navigation.login(BOB_USERNAME, BOB_PASSWORD);

        // Navigate to the issue
        navigation.issue().gotoIssue(key);

        // Now stop progress on the issue
        tester.clickLinkWithText(TRANSIION_NAME_STOP_PROGRESS);

        // Ensure the status got set
        textAssertions.assertTextPresent(new IdLocator(tester, "status-val"), STATUS_OPEN);
        // Ensure the issue was assigned correctly
        textAssertions.assertTextPresent(new IdLocator(tester, "assignee-val"), ADMIN_FULLNAME);

        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void testLastModifiedWithFunnyCharacters() {
        administration.restoreData("blankWithOldDefault.xml");

        administration.usersAndGroups().addUser("\"meta\"user", "meta", "\"meta\" user lastname", "meta@example.com");

        administration.usersAndGroups().addUserToGroup("\"meta\"user", "jira-administrators");

        navigation.logout();

        navigation.login("\"meta\"user", "meta");

        // Copy default JIRA workflow
        administration.workflows().goTo().copyWorkflow("jira", WORKFLOW_COPIED, "Workflow copied from JIRA default");
        Assert.assertTrue(administration.workflows().goTo().inactive().contains(WORKFLOW_COPIED));

        final ViewWorkflows.WorkflowItem copiedWorkflowItem = getOnlyElement(administration.workflows().goTo().inactive());
        Assert.assertTrue(copiedWorkflowItem.lastModified().contains(new SimpleDateFormat("dd/MMM/yy").format(new Date())));
        Assert.assertTrue(copiedWorkflowItem.lastModified().contains("\"meta\" user lastname"));
    }

    private void enableWorkflow() {
        // Associate the project with the new workflow
        workflowSchemes.addWorkFlowScheme(WORKFLOW_SCHEME, "Test workflow scheme.");
        workflowSchemesControl.assignScheme(10000L, "Bug", WORKFLOW_COPIED);
        administration.project().associateWorkflowScheme(PROJECT_HOMOSAP, WORKFLOW_SCHEME, null, true);
        workflowSchemes.waitForSuccessfulWorkflowSchemeMigration(PROJECT_HOMOSAP, WORKFLOW_SCHEME);
    }

    public void editTransitionScreen(String workflow_name, String transition_name, String transitionFieldScreen) {
        administration.workflows().goTo().workflowSteps(workflow_name);
        tester.clickLinkWithText(transition_name);
        tester.clickLink("edit_transition");
        tester.setFormElement("transitionName", transition_name);
        if (transitionFieldScreen != null) {
            tester.selectOption("view", transitionFieldScreen);
        }

        tester.submit();
    }

    private void selectMultiOptionByValue(String selectName, String value) {
        tester.checkCheckbox(selectName, value);
    }

    private void addWorkflowPostfunction(String workflowName, String stepName, String transitionName, String postFunctionName) {
        administration.workflows().goTo().workflowSteps(workflowName);
        tester.clickLinkWithText(transitionName);
        tester.clickLinkWithText("Post Functions");
        tester.clickLinkWithText("Add post function", 0);
        selectMultiOptionByValue("type", postFunctionName);

        tester.submit();
    }
}
