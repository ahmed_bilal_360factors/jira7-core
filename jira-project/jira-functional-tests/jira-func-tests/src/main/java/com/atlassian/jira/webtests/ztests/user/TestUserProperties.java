package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_EMAIL;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_USERNAME;

@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUserProperties extends BaseJiraFuncTest {

    protected static final String USER_BOB_BROWSER = "/secure/admin/user/EditUserProperties.jspa?name=bob";
    protected static final String DELETE_PROP = "delete_testprop";

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        administration.restoreBlankInstance();
        administration.usersAndGroups().addUser(BOB_USERNAME, BOB_PASSWORD, BOB_FULLNAME, BOB_EMAIL);

        goToBobPropertiesPage();
    }

    @Test
    public void testAddUserProperty() {
        addBobProperty("testprop", "testvalue");
        // Test that the property has been added
        goToBobPropertiesPage();
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "testprop", "testvalue");
    }

    @Test
    public void testDuplicateProperty() {
        addBobProperty("testprop", "testvalue1");
        addBobProperty("testprop", "testvalue2");
        assertions.assertNodeHasText(new CssLocator(tester, ".error[data-field=key]"), "This 'key' already exists for the user.");
    }

    @Test
    public void testDeleteUserProperty() {
        addBobProperty("testprop", "testvalue");
        tester.gotoPage(USER_BOB_BROWSER);
        tester.clickLink(DELETE_PROP);
        tester.assertTextPresent("Delete Property: testprop");
        tester.submit("Delete");
        tester.assertTextPresent("currently has no properties");

    }

    @Test
    public void testInvalidCharactersForProperty() {
        addBobProperty("testprop!", "testvalue");
        assertions.assertNodeHasText(new CssLocator(tester, ".error[data-field=key]"), "The 'key' can only contain alphanumeric characters");
    }

    @Test
    public void testKeyExceedsCharacterLengthCheck() {
        addBobProperty(StringUtils.repeat("x", 251), "testvalue");
        assertions.assertNodeHasText(new CssLocator(tester, ".error[data-field=key]"), "The 'key' length must be less than 200 characters");

    }

    @Test
    public void testValueExceedCharacterLengthCheck() {
        addBobProperty("testproperty", StringUtils.repeat("x", 251));
        assertions.assertNodeHasText(new CssLocator(tester, ".error[data-field=value]"), "The 'value' length must be less than 250 characters");
    }

    @Test
    public void testKeyIsEmptyCheck() {
        addBobProperty("", "testvalue");
        assertions.assertNodeHasText(new CssLocator(tester, ".error[data-field=key]"), "The 'key' cannot be empty");
    }

    @Test
    public void testValueIsEmptyCheck() {
        addBobProperty("testproperty", "");
        assertions.assertNodeHasText(new CssLocator(tester, ".error[data-field=value]"), "The 'value' cannot be empty");
    }

    @Test
    public void testValueIsHtmlEncoded() {
        addBobProperty("testproperty", "<blink>Annoying</blink>");
        tester.assertTextPresent("&lt;blink&gt;Annoying&lt;/blink&gt;");
        tester.assertTextNotPresent("<blink>Annoying</blink>");
    }

    private void addBobProperty(String key, String value) {
        goToBobPropertiesPage();
        tester.setFormElement("key", key);
        tester.setFormElement("value", value);
        tester.submit();
    }

    private void goToBobPropertiesPage() {
        tester.gotoPage(USER_BOB_BROWSER);
        tester.assertTextPresent(BOB_FULLNAME);
    }
}
