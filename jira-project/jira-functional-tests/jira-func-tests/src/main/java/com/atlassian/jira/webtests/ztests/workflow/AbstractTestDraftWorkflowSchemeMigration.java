package com.atlassian.jira.webtests.ztests.workflow;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.ProgressBar;
import com.atlassian.jira.testkit.beans.WorkflowSchemeData;
import com.atlassian.jira.util.collect.MapBuilder;

import javax.inject.Inject;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * @since v5.2
 */
public abstract class AbstractTestDraftWorkflowSchemeMigration extends AbstractTestWorkflowSchemeMigration {
    protected static final String TEST_PROJECT_NAME_II = "Test Project II";
    protected static final String TEST_PROJECT_KEY_II = "TSTII";

    protected static final long TEST_PROJECT_ID = 10000L;
    protected static final long TEST_PROJECT_ID_II = 10010L;

    protected static final String COPY_OF_SOURCE_WORKFLOW_SCHEME = "Copy of Source Workflow Scheme";
    protected static final String COPY_2_OF_SOURCE_WORKFLOW_SCHEME = "Copy 2 of Source Workflow Scheme";
    protected static final String SOURCE_SCHEME_DESCRIPTION = "The workflow scheme the project started on";
    protected static final String COPY_OF_SOURCE_WORKFLOW_SCHEME_DESCRIPTION = "The workflow scheme the project started on (This copy was automatically generated from a draft, as an intermediate scheme for migration)";

    protected static final long SOURCE_SCHEME_ID = 10000L;

    @Inject
    private Administration administration;

    @Inject
    private ProgressBar progressBar;

    protected static final Map<String, String> DRAFT_WORKFLOW_MAPPING = MapBuilder.<String, String>newBuilder()
            .add("Task", JIRA_DEFAULT_WORKFLOW).toMap();

    protected static final Map<String, String> SOURCE_SCHEME_WORKFLOW_MAPPING = MapBuilder.<String, String>newBuilder()
            .add("Bug", SOURCE_WORKFLOW_2).toMap();

    protected void publishDraft() {
        final Map<String, String> statusMapping = createTestWorkflowMigrationMapping();

        final Long schemeId = backdoor.workflowSchemes().getWorkflowSchemeDraftByProjectKey(TEST_PROJECT_KEY).getId();
        administration.project().publishWorkflowSchemeDraft(TEST_PROJECT_NAME, schemeId, statusMapping, true);
    }

    protected void assertSchemeAndNoDraft(final String projectName) {
        assertScheme(projectName);
        assertNoDraft(projectName);
    }

    protected void assertNoDraft(final String projectName) {
        assertNull(getBackdoor().workflowSchemes().getWorkflowSchemeDraftByProjectNameNullIfNotFound(projectName));
    }

    protected void assertDraft(final String projectName) {
        final WorkflowSchemeData draft = getBackdoor().workflowSchemes().getWorkflowSchemeDraftByProjectNameNullIfNotFound(projectName);

        assertNotNull(draft);
        assertEquals(SOURCE_WORKFLOW_SCHEME, draft.getName());
        assertEquals(DRAFT_WORKFLOW_MAPPING, draft.getMappings());
    }

    protected void assertScheme(final String projectName) {
        assertScheme(projectName, SOURCE_WORKFLOW_SCHEME, SOURCE_SCHEME_DESCRIPTION, DRAFT_WORKFLOW_MAPPING, DESTINATION_WORKFLOW);
    }

    protected void assertScheme(final String projectName, final String schemeName, final String schemeDescription, final Map<String, String> workflowMapping, final String defaultWorkflow) {
        final WorkflowSchemeData schemeData = getBackdoor().workflowSchemes().getWorkflowSchemeByProjectName(projectName);

        assertEquals(schemeName, schemeData.getName());
        assertEquals(schemeDescription, schemeData.getDescription());
        assertEquals(workflowMapping, schemeData.getMappings());
        assertEquals(defaultWorkflow, schemeData.getDefaultWorkflow());
    }

    protected void assertSchemeIdChanged(final String schemeName, final long id) {
        final WorkflowSchemeData schemeData = getBackdoor().workflowSchemes().getWorkflowSchemeByName(schemeName);

        assertFalse(schemeData.getId().equals(id));
    }

    protected void assertSchemeId(final String schemeName, final long id) {
        final WorkflowSchemeData schemeData = getBackdoor().workflowSchemes().getWorkflowSchemeByName(schemeName);

        assertEquals((long) schemeData.getId(), id);
    }

    protected void assertInactiveSchemeExists(final String schemeName, final String schemeDescription) {
        final WorkflowSchemeData schemeData = getBackdoor().workflowSchemes().getWorkflowSchemeByNameNullIfNotFound(schemeName);

        assertNotNull(schemeData);
        assertEquals(schemeDescription, schemeData.getDescription());
        assertFalse(schemeData.isActive());
    }

    protected void assertNoScheme(final String schemeName) {
        assertNull(getBackdoor().workflowSchemes().getWorkflowSchemeByNameNullIfNotFound(schemeName));
    }

    protected void assertMultiAdminTaskProgressFlow(final long projectId) {
        // ok find out what the task id is
        final long taskId = progressBar.getSubmittedTaskId();

        progressBar.waitForTaskAcknowledgement(taskId);
        tester.assertTextPresent("input type=\"submit\" name=\"Acknowledge\"");
        tester.assertTextNotPresent("input type=\"submit\" name=\"Done\"");
        progressBar.validateProgressBarUI(ACKNOWLEDGE);

        // ok connect as another user and have a look at the task
        navigation.logout();
        navigation.login("admin2", "admin2");
        navigation.gotoAdmin();
        tester.gotoPage("/secure/project/SelectProjectWorkflowSchemeStep3.jspa?projectId=" + projectId + "&taskId=" + taskId + "&draftMigration=true");

        progressBar.validateProgressBarUI(DONE);
        tester.assertTextNotPresent("input type=\"submit\" name=\"Acknowledge\"");
        tester.assertTextPresent("input type=\"submit\" name=\"Done\"");

        // ok go back and acknowledge as the task starter
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.gotoAdmin();
        tester.gotoPage("/secure/project/SelectProjectWorkflowSchemeStep3.jspa?projectId=" + projectId + "&taskId=" + taskId + "&draftMigration=true");

        tester.assertTextPresent("input type=\"submit\" name=\"Acknowledge\"");
        tester.assertTextNotPresent("input type=\"submit\" name=\"Done\"");
        progressBar.validateProgressBarUI(ACKNOWLEDGE);

        tester.submit(ACKNOWLEDGE);

        // now the task should be cleaned up
        tester.gotoPage("/secure/project/SelectProjectWorkflowSchemeStep3.jspa?projectId=" + projectId + "&taskId=" + taskId + "&draftMigration=true");
        tester.assertTextPresent("The task could not be found. Perhaps it has finished and has been acknowledged?");
        tester.assertTextPresent("input type=\"submit\" name=\"Done\"");
    }
}
