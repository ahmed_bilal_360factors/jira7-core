package com.atlassian.jira.webtests.ztests.projectconfig;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.RoleResourceClient;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.rest.beans.GroupResponse;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.rest.beans.RoleActorsAddResponse;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.rest.beans.RoleMembersResponse;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.rest.beans.RolesResponse;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.rest.beans.UserResponse;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_ADMIN_GROUP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_ADMIN_ROLE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_ADMIN_ROLE_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_DEV_GROUP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_DEV_ROLE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_USERS_GROUP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_USERS_ROLE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_USERS_ROLE_ID;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@WebTest({Category.FUNC_TEST, Category.REST})
@RestoreBlankInstance
@LoginAs(user = ADMIN_USERNAME)
public class TestRolesResource extends BaseJiraFuncTest {
    private final static String SCO_PROJECT_KEY = "SCO";
    private final static String SCO_PROJECT_NAME = "Scotts";

    @Before
    public void setUpProjects() {
        backdoor.project().addProject(SCO_PROJECT_NAME, SCO_PROJECT_KEY, ADMIN_USERNAME);
    }

    @Test
    public void testGetRolesActors() {
        String specialistDevGroup = "specialist-developers";
        String jsNinjaUser = "jsninja";

        backdoor.usersAndGroups().addUser(jsNinjaUser);
        backdoor.usersAndGroups().addGroup(specialistDevGroup);
        backdoor.usersAndGroups().addUserToGroup(jsNinjaUser, specialistDevGroup);

        assignMembersToRole(JIRA_DEV_ROLE, specialistDevGroup, FRED_USERNAME);
        assignMembersToRole(JIRA_USERS_ROLE, null, jsNinjaUser);

        RoleResourceClient client = new RoleResourceClient(environmentData);
        RolesResponse response = client.get(SCO_PROJECT_KEY);

        assertRoleHasNumberOfMembers(response, JIRA_ADMIN_ROLE, 1);
        assertRoleHasGroups(response, JIRA_ADMIN_ROLE, JIRA_ADMIN_GROUP);
        assertRoleHasUsers(response, JIRA_ADMIN_ROLE);

        assertRoleHasNumberOfMembers(response, JIRA_DEV_ROLE, 3);
        assertRoleHasGroups(response, JIRA_DEV_ROLE, JIRA_DEV_GROUP, specialistDevGroup);
        assertRoleHasUsers(response, JIRA_DEV_ROLE, FRED_USERNAME);

        assertRoleHasNumberOfMembers(response, JIRA_USERS_ROLE, 2);
        assertRoleHasGroups(response, JIRA_USERS_ROLE, JIRA_USERS_GROUP);
        assertRoleHasUsers(response, JIRA_USERS_ROLE, jsNinjaUser);
    }

    @Test
    public void testGetRolesActorsWithMaxResults() {
        setupMultipleUsers("sd", "user", 20);

        RoleResourceClient client = new RoleResourceClient(environmentData);
        RolesResponse response = client.get(SCO_PROJECT_KEY);

        assertRoleHasNumberOfMembers(response, JIRA_USERS_ROLE, 21);
        assertRoleHasNumberOfGroups(response, JIRA_USERS_ROLE, 1);
        assertRoleHasNumberOfUsers(response, JIRA_USERS_ROLE, 4);
    }

    @Test
    public void testSearchRolesActors() {
        setupMultipleUsers("user", "The User No.", 20);
        setupMultipleUsers("people", "The Person No.", 5);

        RoleResourceClient client = new RoleResourceClient(environmentData);
        RolesResponse response = client.search(SCO_PROJECT_KEY, "  use  ");

        assertNumberOfRolesReturned(response, 3);

        assertRoleHasNumberOfMembers(response, JIRA_ADMIN_ROLE, 0);

        assertRoleHasNumberOfMembers(response, JIRA_DEV_ROLE, 0);

        assertRoleHasNumberOfMembers(response, JIRA_USERS_ROLE, 21);
        assertRoleHasNumberOfGroups(response, JIRA_USERS_ROLE, 1);
        assertRoleHasNumberOfUsers(response, JIRA_USERS_ROLE, 4);
    }

    @Test
    public void testSearchSpecificRoleActors() {
        setupMultipleUsers("user", "The User No.", 20);
        setupMultipleUsers("people", "The Person No.", 5);

        RoleResourceClient client = new RoleResourceClient(environmentData);
        RolesResponse response = client.search(SCO_PROJECT_KEY, JIRA_USERS_ROLE_ID, "  use  ");

        assertNumberOfRolesReturned(response, 1);

        assertRoleHasNumberOfMembers(response, JIRA_USERS_ROLE, 21);
        assertRoleHasNumberOfGroups(response, JIRA_USERS_ROLE, 1);
        assertRoleHasNumberOfUsers(response, JIRA_USERS_ROLE, 4);
    }

    @Test
    public void testSearchSpecificRoleActorsWithPagination() {
        setupMultipleGroups("Group Pagination", 7);
        setupMultipleUsers("userpagination", "User Pagination", 10);

        searchAndVerifyPaginatedResults("  paginat  ", 1, 5, 17, 5, 0);
        searchAndVerifyPaginatedResults("  paginat  ", 2, 5, 17, 2, 3);
        searchAndVerifyPaginatedResults("  paginat  ", 3, 5, 17, 0, 5);
        searchAndVerifyPaginatedResults("  paginat  ", 4, 5, 17, 0, 2);
        searchAndVerifyPaginatedResults("  paginat  ", 5, 5, 17, 0, 0);
    }

    @Test
    public void testSearchRolesActorsWithErrors() {
        RoleResourceClient client = new RoleResourceClient(environmentData);

        // invalid project
        Response response = client.searchResponse("BLAH", 99999l);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);

        // invalid role
        response = client.searchResponse(SCO_PROJECT_KEY, 99999l);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);

        // zero page number
        response = client.searchResponse(SCO_PROJECT_KEY, JIRA_USERS_ROLE_ID, 0, 5);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);

        // negative page number
        response = client.searchResponse(SCO_PROJECT_KEY, JIRA_USERS_ROLE_ID, -1, 5);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);

        // zero page size
        response = client.searchResponse(SCO_PROJECT_KEY, JIRA_USERS_ROLE_ID, 1, 0);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);

        // negative page size
        response = client.searchResponse(SCO_PROJECT_KEY, JIRA_USERS_ROLE_ID, 1, -1);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);

        // anonymous
        response = client.anonymous().searchResponse(SCO_PROJECT_KEY, JIRA_USERS_ROLE_ID);
        assertEquals(UNAUTHORIZED.getStatusCode(), response.statusCode);
    }

    @Test
    public void testAddActorsToRole() {
        String testUser = "testuser";
        backdoor.usersAndGroups().addUser(testUser);
        assignMembersToRole(JIRA_ADMIN_ROLE, null, testUser);

        String[] groupsToAdd = new String[]{JIRA_ADMIN_GROUP, JIRA_DEV_GROUP, JIRA_USERS_GROUP};
        String[] usersToAdd = new String[]{ADMIN_USERNAME, testUser};

        RoleResourceClient client = new RoleResourceClient(environmentData);
        RoleActorsAddResponse addResponse = client.addActors(SCO_PROJECT_KEY, JIRA_ADMIN_ROLE_ID, groupsToAdd, usersToAdd);

        assertEquals(2, addResponse.getNumberOfGroupsAdded());
        assertEquals(1, addResponse.getNumberOfUsersAdded());

        RolesResponse searchResponse = client.search(SCO_PROJECT_KEY, JIRA_ADMIN_ROLE_ID);

        assertRoleHasGroups(searchResponse, JIRA_ADMIN_ROLE, JIRA_ADMIN_GROUP, JIRA_DEV_GROUP, JIRA_USERS_GROUP);
        assertRoleHasUsers(searchResponse, JIRA_ADMIN_ROLE, ADMIN_USERNAME, testUser);
    }

    @Test
    public void testAddActorsToRoleWithErrors() {
        RoleResourceClient client = new RoleResourceClient(environmentData);

        String[] groupsToAdd = new String[]{JIRA_ADMIN_GROUP};
        String[] usersToAdd = new String[]{ADMIN_USERNAME};

        // invalid project
        Response response = client.addActorsResponse("BLAH", 99999l, groupsToAdd, usersToAdd);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);

        // invalid role
        response = client.addActorsResponse(SCO_PROJECT_KEY, 99999l, groupsToAdd, usersToAdd);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);

        // empty actors
        response = client.addActorsResponse(SCO_PROJECT_KEY, 99999l, null, null);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);

        // anonymous
        response = client.anonymous().addActorsResponse(SCO_PROJECT_KEY, JIRA_USERS_ROLE_ID, groupsToAdd, usersToAdd);
        assertEquals(UNAUTHORIZED.getStatusCode(), response.statusCode);
    }

    @Test
    public void testEmailVisibilitySetting() {
        assignMembersToRole(JIRA_DEV_ROLE, null, ADMIN_USERNAME);

        RoleResourceClient client = new RoleResourceClient(environmentData);
        RolesResponse response = client.get(SCO_PROJECT_KEY);

        assertEquals(Lists.newArrayList("admin@example.com"), getUserActorEmailAddresses(response));

        backdoor.applicationProperties().setString(APKeys.JIRA_OPTION_EMAIL_VISIBLE, "hide");
        response = client.get(SCO_PROJECT_KEY);

        assertTrue(getUserActorEmailAddresses(response).isEmpty());
    }

    private List<String> getUserActorEmailAddresses(RolesResponse response) {
        return response.getRoles()
                .stream()
                .flatMap(r -> r.getUsers().stream().map(UserResponse::getEmailAddress))
                .filter(email -> email != null)
                .collect(Collectors.toList());
    }

    private void assignMembersToRole(String role, String group, String user) {
        String[] groupArray = null;
        if (group != null) {
            groupArray = new String[]{group};
        }

        String[] userArray = null;
        if (user != null) {
            userArray = new String[]{user};
        }

        backdoor.projectRole().addActors(SCO_PROJECT_KEY, role, groupArray, userArray);
    }

    private void setupMultipleGroups(String groupNamePrefix, int numberOfGroups) {
        IntStream.range(0, numberOfGroups)
                .forEach(i -> backdoor.usersAndGroups().addGroup(groupNamePrefix + i));

        final String[] groupNames = IntStream.range(0, numberOfGroups)
                .mapToObj(i -> groupNamePrefix + i)
                .toArray(String[]::new);

        backdoor.projectRole().addActors(SCO_PROJECT_KEY, JIRA_USERS_ROLE, groupNames, null);
    }

    private void setupMultipleUsers(String userNamePrefix, String displayNamePrefix, int numberOfUsers) {
        backdoor.usersAndGroups().addUsers(userNamePrefix, displayNamePrefix, numberOfUsers);

        final String[] userNames = IntStream.range(0, numberOfUsers)
                .mapToObj(i -> userNamePrefix + i)
                .toArray(String[]::new);

        backdoor.projectRole().addActors(SCO_PROJECT_KEY, JIRA_USERS_ROLE, null, userNames);
    }

    private void searchAndVerifyPaginatedResults(String query, int pageNumber, int pageSize,
                                                 int expectedTotal, int expectedNumOfGroup, int expectedNumOfUser) {
        RoleResourceClient client = new RoleResourceClient(environmentData);
        RolesResponse response = client.search(SCO_PROJECT_KEY, JIRA_USERS_ROLE_ID, query, pageNumber, pageSize);

        assertRoleHasNumberOfMembers(response, JIRA_USERS_ROLE, expectedTotal);
        assertRoleHasNumberOfGroups(response, JIRA_USERS_ROLE, expectedNumOfGroup);
        assertRoleHasNumberOfUsers(response, JIRA_USERS_ROLE, expectedNumOfUser);
    }

    private void assertNumberOfRolesReturned(RolesResponse response, int expectedNumber) {
        assertEquals(expectedNumber, response.getRoles().size());
    }

    private void assertRoleHasNumberOfMembers(RolesResponse response, String role, int expectedNumber) {
        assertEquals(expectedNumber, getTotalMembersForRole(response, role));
    }

    private void assertRoleHasNumberOfGroups(RolesResponse response, String role, int expectedNumber) {
        assertEquals(expectedNumber, getGroupsForRole(response, role).size());
    }

    private void assertRoleHasNumberOfUsers(RolesResponse response, String role, int expectedNumber) {
        assertEquals(expectedNumber, getUsersForRole(response, role).size());
    }

    private void assertRoleHasGroups(RolesResponse response, String role, String... expectedGroups) {
        assertEquals(Lists.newArrayList(expectedGroups), getGroupsForRole(response, role));
    }

    private void assertRoleHasUsers(RolesResponse response, String role, String... expectedUsers) {
        assertEquals(Lists.newArrayList(expectedUsers), getUsersForRole(response, role));
    }

    private long getTotalMembersForRole(RolesResponse response, String roleName) {
        return getRoleMembers(response, roleName)
                .map(RoleMembersResponse::getTotal)
                .get();
    }

    private List<String> getGroupsForRole(RolesResponse response, String roleName) {
        return getRoleMembers(response, roleName)
                .map(role -> role.getGroups().stream().map(GroupResponse::getName))
                .get()
                .collect(Collectors.toList());
    }

    private List<String> getUsersForRole(RolesResponse response, String roleName) {
        return getRoleMembers(response, roleName)
                .map(role -> role.getUsers().stream().map(UserResponse::getName))
                .get()
                .collect(Collectors.toList());
    }

    private Optional<RoleMembersResponse> getRoleMembers(RolesResponse response, String roleName) {
        return response.getRoles()
                .stream()
                .filter(r -> r.getName().equals(roleName))
                .findFirst();
    }
}
