package com.atlassian.jira.webtests.ztests.navigator;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.ResourceRef;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Arrays;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.ISSUE_NAVIGATOR, Category.ISSUES})
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueNavigatorCsvView extends BaseJiraFuncTest {
    private static final String EXPORT_USER = "export_user";
    private static final String EXPORT_USER_LOCALISED = "export_user_localised";

    private static final String EMPTY_PROJECT_KEY = "NEW";
    private static final Long EMPTY_PROJECT_ID = 10010L;

    private static final String JQL_ORDERING = "ORDER BY key ASC";

    private static final String ADMIN_USERNAME = "admin";

    @Inject
    private Administration administration;

    @Before
    public void setUp() {
        administration.backdoor().darkFeatures().enableForSite("jira.export.csv.enabled");
    }

    @Test
    @Restore("TestIssueNavigatorCsvView.xml")
    public void testIfAllFieldsExportIsTheSameAsModelFile() throws IOException {
        final String csvExport = getCsvExportOfAllFields(EXPORT_USER);
        final String expectedCsv = getExportModelContent("TestIssueNavigatorCsvViewAllFields.csv");

        assertCsvTheSame(expectedCsv, csvExport);
    }

    @Test
    @Restore("TestIssueNavigatorCsvView.xml")
    public void testIfColumnsAndValuesAreTranslated() throws IOException {
        final String csvExport = getCsvExportOfAllFields(EXPORT_USER_LOCALISED);
        final String expectedCsv = getExportModelContent("TestIssueNavigatorCsvViewAllFieldsGerman.csv");

        assertCsvTheSame(expectedCsv, csvExport);
    }

    @Test
    @Restore("TestIssueNavigatorCsvViewFeaturesDisabled.xml")
    public void testAllFieldsExportedWhenSomeFeaturesAreDisabled() throws IOException {
        // disabled features are voting, watching, time tracking, issue links and attachments;
        // only issue links and attachments should be exported

        final String csvExport = getCsvExportOfAllFields(EXPORT_USER);
        final String expectedCsv = getExportModelContent("TestIssueNavigatorCsvViewAllFieldsFeaturesDisabled.csv");

        assertCsvTheSame(expectedCsv, csvExport);
    }

    @Test
    @Restore("TestIssueNavigatorCsvViewAnonymousExportIssues.xml")
    public void testAnonymousUsersSeeIssueWithPermissionTo() throws IOException {
        final String csvExport = getCsvExportOfAllFields(null);
        final String expectedCsv = getExportModelContent("TestIssueNavigatorCsvViewAnonymousExportIssues.csv");

        assertCsvTheSame(expectedCsv, csvExport);
    }

    @Test
    @Restore("TestIssueNavigatorCsvView.xml")
    public void testAnonymousUseDoesNotSeeAnyIssuesWithoutProperPermission() throws IOException {
        final String csvExport = getCsvExportOfAllFields(null);

        assertThat(csvExport, equalTo("\n"));
    }

    @Test
    @Restore("TestIssueNavigatorCsvViewEmptyProject.xml")
    public void testIssueWithSecurityNotShown() {

        //Set up issue security scheme for empty project
        backdoor.permissionSchemes().addEveryonePermission(FunctTestConstants.DEFAULT_PERM_SCHEME_ID, ProjectPermissions.BROWSE_PROJECTS);
        backdoor.permissionSchemes().addEveryonePermission(FunctTestConstants.DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY);
        Long issueSchemeId = backdoor.issueSecuritySchemesControl().createScheme("Some scheme", "description");
        Long levelId = backdoor.issueSecuritySchemesControl().addSecurityLevel(issueSchemeId, "Internal", "Internal users only");
        backdoor.project().setIssueSecurityScheme(EMPTY_PROJECT_ID, issueSchemeId);

        IssueCreateResponse issueCreateResponse = backdoor.issues().createIssue(EMPTY_PROJECT_KEY, "This is a new issue");

        final String csvExport = getCsvExportOfAllFields(null);
        assertThat("Anonymous user should be able to see one issue exported", getNumberRows(csvExport), equalTo(1));

        //Make this issue hidden by an issue security scheme
        backdoor.issues().setIssueFields(issueCreateResponse.key, new IssueFields().securityLevel(ResourceRef.withId(String.valueOf(levelId))));

        final String newCsvExport = getCsvExportOfAllFields(null);

        assertThat(newCsvExport, equalTo("\n"));
    }

    /**
     * This test relies on the reference plugin being installed. If running locally use the -rp flag.
     */
    @Test
    @Restore("TestIssueNavigatorCsvViewWithCustomField.xml")
    public void testCustomFieldWithNoExportImplementedGetsDefaultOutput() throws IOException {
        final String csvExport = getCsvExportCurrentFields(ADMIN_USERNAME);
        final String expectedCsv = getExportModelContent("TestIssueNavigatorCsvViewWithCustomField.csv");

        assertCsvTheSame(expectedCsv, csvExport);
    }

    /**
     * Test the absence of a 1000 issue limit when exporting.
     */
    @Test
    @Restore("TestIssueNavigatorCsvViewWithMoreThan1000Issues.xml")
    public void testThatExportContainsMoreThan1000Issues() {
        String export = getCsvExportOfAllFields("admin");

        assertThat(export.split("\n").length, is(1006));
    }

    /**
     * Very simple implementation, doesn't work with issues with line breaks
     * @param csvExport string representing the export
     * @return number of data rows
     */
    private Integer getNumberRows(final String csvExport) {
        return csvExport.split("\n").length - 1;
    }


    private String getCsvExportOfAllFields(final String username) {
        final String jql = JQL_ORDERING;
        return getCsvExportAllFieldsForJql(username, jql);
    }

    private String getCsvExportAllFieldsForJql(final String username, final String jql) {
        return getExportRequestForJql(username, "searchrequest-csv-all-fields", jql);
    }

    private String getCsvExportCurrentFields(final String username) {
        final String jql = "";
        return getExportRequestForJql(username, "searchrequest-csv-current-fields", jql);

    }

    private String getExportRequestForJql(final String username, final String type, final String jql) {
        if (username != null) {
            navigation.login(username);
        } else {
            navigation.logout();
        }

        tester.gotoPage("/sr/jira.issueviews:" + type + "/temp/SearchRequest.csv?jqlQuery=" + URLEncoder.encode(jql).replace("+", "%20"));

        return replaceDates(tester.getDialog().getResponseText());
    }

    private String getExportModelContent(final String filename) throws IOException {
        final String filePath = environmentData.getXMLDataLocation().getPath()
                + System.getProperty("file.separator") + filename;
        final String content = FileUtils.readFileToString(new File(filePath));

        return replaceDates(content.replaceAll("http://localhost:8090/jira", environmentData.getBaseUrl().toString()));
    }

    private void assertCsvTheSame(final String expectedCsv, final String actualCsv) {
        //Comment out this line if you want to go into more in-depth checks for what is wrong.
        assertEquals("Expected csv export differs from actual (this test can break if a language pack was updated)",
                expectedCsv, actualCsv);

        String[] expectedLines = expectedCsv.split("\n");
        String[] actualLines = actualCsv.split("\n");
        assertThat("The number of new lines should be the same for both", actualLines.length, equalTo(expectedLines.length));

        String[] expectedFieldNames = expectedLines[0].split(",");
        String[] actualFieldNames = actualLines[0].split(",");

        assertThat("field columns should be the same", Arrays.asList(actualFieldNames), equalTo(Arrays.asList(expectedFieldNames)));

        for (int lineIndex = 0; lineIndex < expectedLines.length; ++lineIndex) {
            String[] expectedLineFields = expectedLines[lineIndex].split(",");
            String[] actualLineFields = actualLines[lineIndex].split(",");
            assertThat("Assert number of commas should be the same", actualLineFields.length, equalTo(expectedLineFields.length));

            for (int fieldIndex = 0; fieldIndex < expectedLineFields.length; ++fieldIndex) {
                String possibleFieldName = "unknown";
                if (fieldIndex < expectedFieldNames.length) {
                    possibleFieldName = expectedFieldNames[fieldIndex];
                }
                assertThat("Line " + (lineIndex + 1) + " field #" + fieldIndex + " should be the same, could be field '" + possibleFieldName + "' if no extra commas in data row",
                        actualLineFields[fieldIndex],
                        equalTo(expectedLineFields[fieldIndex]));
            }
        }
    }

    /**
     * Finds any text with the date format such as: 30/Mar/16 10:42 PM into xx/xx/xx xx:xx xx for diffing.
     * Not nice but makes running the test work with LastViewedField
     * @param text to convert
     * @return text with no dates
     */
    private String replaceDates(String text) {
        return text.replaceAll("\\d+/[^/]*/\\d+ \\d+:\\d+ (PM|AM)", "xx/Xxx/xx xx:xx XX");
    }
}
