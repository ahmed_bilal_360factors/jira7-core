package com.atlassian.jira.webtests.ztests.email;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraEmailTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.rule.MailTest;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Collection;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.matcher.MimeMessageMatchers.containsString;
import static com.atlassian.jira.functest.matcher.MimeMessageMatchers.withRecipientEqualTo;
import static com.atlassian.jira.functest.matcher.MimeMessageMatchers.withSubjectEqualTo;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.EMAIL, Category.FIELDS})
@LoginAs(user = ADMIN_USERNAME)
@MailTest
public class TestCustomFieldNotifications extends BaseJiraEmailTest {
    private static final String ISSUE_COMMENT = "If it bleeds we can kill it!";
    private static final String SUBJECT = "[JIRATEST] (HSP-1) Dude";

    @Test
    @Restore("TestCustomFieldNotifications.xml")
    public void testCFNotifications() throws InterruptedException, MessagingException, IOException {
        navigation.userProfile().changeNotifyMyChanges(true);

        //lets add a comment.  This should trigger 5 notifications.
        navigation.issue().addComment("HSP-1", ISSUE_COMMENT, null);

        //lets check all the right notifications were sent out.
        final Collection<MimeMessage> mails = mailHelper.flushMailQueueAndWait(6);

        assertThat(mails, hasItem(addressSubjectContentMatcher("user@example.com", SUBJECT, ISSUE_COMMENT)));
        assertThat(mails, hasItem(addressSubjectContentMatcher("multiuser@example.com", SUBJECT, ISSUE_COMMENT)));
        assertThat(mails, hasItem(addressSubjectContentMatcher("singlegroup@example.com", SUBJECT, ISSUE_COMMENT)));
        assertThat(mails, hasItem(addressSubjectContentMatcher("multigroup@example.com", SUBJECT, ISSUE_COMMENT)));
        assertThat(mails, hasItem(addressSubjectContentMatcher("admin@example.com", SUBJECT, ISSUE_COMMENT)));
        assertThat(mails, hasItem(addressSubjectContentMatcher("fred@example.com", SUBJECT, ISSUE_COMMENT)));
    }

    private Matcher<MimeMessage> addressSubjectContentMatcher(final String address, final String subject, final String content) {
        return Matchers.allOf(withRecipientEqualTo(address), withSubjectEqualTo(subject), containsString(content));
    }

    @Test
    @Restore("TestCustomFieldNotifications.xml")
    public void testCFNotificationsPublicIssue() throws InterruptedException, MessagingException, IOException {
        navigation.userProfile().changeNotifyMyChanges(true);

        //now give Anyone the permission to view HSP-1, which should add the single e-mail notification to the list.
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_PERM_SCHEME_ID, BROWSE_PROJECTS);

        navigation.issue().addComment("HSP-1", ISSUE_COMMENT, null);

        //lets check all the right notifications were sent out.
        final Collection<MimeMessage> mails = mailHelper.flushMailQueueAndWait(7);

        assertThat(mails, hasItem(addressSubjectContentMatcher("user@example.com", SUBJECT, ISSUE_COMMENT)));
        assertThat(mails, hasItem(addressSubjectContentMatcher("multiuser@example.com", SUBJECT, ISSUE_COMMENT)));
        assertThat(mails, hasItem(addressSubjectContentMatcher("singlegroup@example.com", SUBJECT, ISSUE_COMMENT)));
        assertThat(mails, hasItem(addressSubjectContentMatcher("multigroup@example.com", SUBJECT, ISSUE_COMMENT)));
        assertThat(mails, hasItem(addressSubjectContentMatcher("admin@example.com", SUBJECT, ISSUE_COMMENT)));
        assertThat(mails, hasItem(addressSubjectContentMatcher("fred@example.com", SUBJECT, ISSUE_COMMENT)));
        assertThat(mails, hasItem(addressSubjectContentMatcher("johnson@example.com", SUBJECT, ISSUE_COMMENT)));
    }

    @Test
    //this data contains a couple of invalid customfield notifications.
    @Restore("TestInvalidCustomFieldNotifications.xml")
    public void testDeletedCFDoesntCauseErrors() throws InterruptedException, IOException, MessagingException {
        navigation.userProfile().changeNotifyMyChanges(true);

        //give Anyone the permission to view HSP-1, which should add the single e-mail notification to the list.
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_PERM_SCHEME_ID, BROWSE_PROJECTS);

        navigation.issue().addComment("HSP-1", ISSUE_COMMENT, null);

        //lets check all the right notifications were sent out.
        final Collection<MimeMessage> mails = mailHelper.flushMailQueueAndWait(3);

        assertThat(mails, hasItem(addressSubjectContentMatcher("admin@example.com", SUBJECT, ISSUE_COMMENT)));
        assertThat(mails, hasItem(addressSubjectContentMatcher("fred@example.com", SUBJECT, ISSUE_COMMENT)));
        assertThat(mails, hasItem(addressSubjectContentMatcher("johnson@example.com", SUBJECT, ISSUE_COMMENT)));
    }
}