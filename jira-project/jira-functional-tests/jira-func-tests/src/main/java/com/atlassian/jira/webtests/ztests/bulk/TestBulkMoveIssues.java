package com.atlassian.jira.webtests.ztests.bulk;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperations;
import com.atlassian.jira.functest.framework.EditIssueFieldVisibility;
import com.atlassian.jira.functest.framework.Indexing;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.navigation.BulkChangeWizard;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.IssueUpdateRequest;
import com.atlassian.jira.rest.api.issue.ResourceRef;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.testkit.client.restclient.Component;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.jira.testkit.client.restclient.Version;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.AFFECTS_VERSIONS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.COMPONENTS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_COMPONENTS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_FIX_VERSIONS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIX_VERSIONS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_NEWFEATURE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.UNKNOWN_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.VERSION_NAME_ONE;
import static com.atlassian.jira.functest.framework.navigation.IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES;
import static com.atlassian.jira.permission.ProjectPermissions.MOVE_ISSUES;
import static com.atlassian.jira.webtests.Groups.DEVELOPERS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@WebTest({Category.FUNC_TEST, Category.BULK_OPERATIONS, Category.ISSUES})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestBulkMoveIssues extends BaseJiraFuncTest {

    private static final String ERROR_MOVE_PERMISSION = "You do not have the permission to move one or more of the selected issues";
    private static final String HSP1 = "HSP-1";
    private static final String HSP2 = "HSP-2";
    private static final String HSP3 = "HSP-3";
    private static final String HSP4 = "HSP-4";
    private static final String MKY1 = "MKY-1";
    @Inject
    private Indexing indexing;
    @Inject
    private EditIssueFieldVisibility editIssueFieldVisibility;
    private IssueClient issueClient = null;
    private Component hspComponentOne;
    private Component hspComponentTwo;
    private Component hspComponentThree;
    private Version hsp1dot0;
    private Version hsp2dot0;
    private Version hsp3dot0;
    private Version mky1dot0;
    private Version mky1dot1;
    private Version mky1dot2;
    private Component mkyWrench;
    private Component hspWrench;

    @Inject
    private BulkOperations bulkOperations;

    @Inject
    private FuncTestLogger logger;

    @Before
    public void setUp() {
        issueClient = new IssueClient(getEnvironmentData());

        hspComponentOne = createComponent("ONE", "component one", PROJECT_HOMOSAP_KEY);
        hspComponentTwo = createComponent("TWO", "component two", PROJECT_HOMOSAP_KEY);
        hspComponentThree = createComponent("THREE", "component three", PROJECT_HOMOSAP_KEY);

        hspWrench = createComponent("wrench", "lowercase", PROJECT_HOMOSAP_KEY);
        mkyWrench = createComponent("WRENCH", "uppercase", PROJECT_MONKEY_KEY);

        hsp1dot0 = createVersion("1.0", "one", PROJECT_HOMOSAP_KEY);
        hsp2dot0 = createVersion("2.0", "two", PROJECT_HOMOSAP_KEY);
        hsp3dot0 = createVersion("3.0", "three", PROJECT_HOMOSAP_KEY);

        mky1dot0 = createVersion("1.0", "first", PROJECT_MONKEY_KEY);
        mky1dot1 = createVersion("1.1", "second", PROJECT_MONKEY_KEY);
        mky1dot2 = createVersion("1.2", "third", PROJECT_MONKEY_KEY);
    }

    @After
    public void restoreDefaultUserPicker() {
        backdoor.darkFeatures().disableForSite("no.frother.assignee.field");
    }

    /*
     * test that error is shown when move is not permitted.
     * details unit tested in: com.atlassian.jira.bulkedit.operation.TestBulkMoveOperation
     */
    @Test
    public void testErrorVisibleWhenOperationNotPermitted() {
        logger.log("Bulk Move - move operation is not available without the move permission");

        //given:
        setupIssues(1);
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, MOVE_ISSUES, DEVELOPERS);

        //when:
        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator()
                .bulkChange(ALL_PAGES)
                .selectAllIssues();

        //then:
        tester.assertTextPresent(ERROR_MOVE_PERMISSION);
    }

    @Test
    public void testNavigatorCanShowMovedIssue() {
        logger.log("Bulk Move - navigator can follow changed issue key.");

        //given:
        setupIssues(1);

        //when:
        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator()
                .bulkChange(ALL_PAGES)
                .selectAllIssues()
                .chooseOperation(BulkChangeWizard.BulkOperationsImpl.MOVE)
                .chooseTargetContextForAll(PROJECT_MONKEY)
                .finaliseFields()
                .complete();

        bulkOperations.waitAndReloadBulkOperationProgressPage();

        navigation.issue().gotoIssue(HSP1);

        //then:
        assertions.getViewIssueAssertions().assertOnViewIssuePage(MKY1);
    }

    @Test
    public void testBulkMoveMatchesComponentsOrVersionsWithChangedCase() throws Exception {
        logger.log("Bulk Move - matching fields and remembering decisions for target context");

        // given:
        setupIssues(4);
        updateIssue(HSP1, hspWrench, hsp1dot0);
        updateIssue(HSP2, hspWrench, hsp2dot0);
        updateIssue(HSP3, hspWrench, hsp1dot0, ISSUE_TYPE_NEWFEATURE);
        updateIssue(HSP4, hspWrench, hsp3dot0, ISSUE_TYPE_NEWFEATURE);

        // when:
        navigation.issueNavigator().displayAllIssues();
        final BulkChangeWizard wizard = navigation.issueNavigator()
                .bulkChange(ALL_PAGES)
                .selectAllIssues()
                .chooseOperation(BulkChangeWizard.BulkOperationsImpl.MOVE)
                .chooseTargetContextForEach(2, PROJECT_MONKEY);

        // then:
        logger.log("Bulk Move - checking matched fields for bug type");
        // first issue type: bug:
        // component is not matched, version 2.0 isn't either, but version 1.0 is:
        tester.assertFormElementEquals(targetComponentFieldName(hspWrench), UNKNOWN_ID);
        tester.assertFormElementEquals(targetFixVersionName(hsp1dot0), mky1dot0.id.toString());
        tester.assertFormElementEquals(targetFixVersionName(hsp2dot0), UNKNOWN_ID);
        tester.assertFormElementNotPresent(targetFixVersionName(hsp3dot0));
        // ... but can be matched:
        wizard.setFieldValue(targetFixVersionName(hsp2dot0), mky1dot1.id.toString())
                .setFieldValue(targetComponentFieldName(hspWrench), mkyWrench.id.toString())
                .finaliseFields();

        logger.log("Bulk Move - checking matched fields for new feature type");
        // second issue type: new feature:
        // component should be remembered, version 1.0 also matched, but version 2.0 should not be:
        tester.assertFormElementEquals(targetComponentFieldName(hspWrench), mkyWrench.id.toString());
        tester.assertFormElementEquals(targetFixVersionName(hsp1dot0), mky1dot0.id.toString());
        tester.assertFormElementNotPresent(targetFixVersionName(hsp2dot0));
        tester.assertFormElementEquals(targetFixVersionName(hsp3dot0), UNKNOWN_ID);
        // ... but can be matched:
        wizard.setFieldValue(targetFixVersionName(hsp3dot0), mky1dot1.id.toString())
                .finaliseFields();

        Assert.assertThat("all selections should be accepted.", wizard.getState(), Matchers.is(BulkChangeWizard.WizardState.CONFIRMATION));
    }

    @Test
    public void testChangeAssigneeWithoutAssignablePermission() {
        backdoor.darkFeatures().enableForSite("no.frother.assignee.field");

        final String monkeyGroup = "monkey-users";
        backdoor.usersAndGroups().addGroup(monkeyGroup);

        final String monkeyUser = "monkey";
        backdoor.usersAndGroups().addUser(monkeyUser);
        backdoor.usersAndGroups().addUserToGroup(monkeyUser, monkeyGroup);
        backdoor.usersAndGroups().addUserToGroup(monkeyUser, "jira-developers");
        backdoor.usersAndGroups().addUserToGroup(FRED_USERNAME, "jira-developers");
        backdoor.usersAndGroups().addUserToGroup(ADMIN_USERNAME, monkeyGroup);

        final Long monkeyPermissionScheme = backdoor.permissionSchemes().copyDefaultScheme("monkey-scheme");
        backdoor.permissionSchemes().removeGroupPermission(monkeyPermissionScheme, ProjectPermissions.ASSIGNABLE_USER, "jira-developers");
        backdoor.permissionSchemes().addGroupPermission(monkeyPermissionScheme, ProjectPermissions.ASSIGNABLE_USER, monkeyGroup);
        backdoor.project().setPermissionScheme(backdoor.project().getProjectId(PROJECT_MONKEY_KEY), monkeyPermissionScheme);

        backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "retain the assignee", monkeyUser);
        backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "change the assignee", FRED_USERNAME);

        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator().bulkChange(ALL_PAGES)
                .selectAllIssues()
                .chooseOperation(BulkChangeWizard.BulkOperationsImpl.MOVE)
                .chooseTargetContextForAll(PROJECT_MONKEY)
                .checkRetainForField("assignee")
                .finaliseFields()
                .complete();

        bulkOperations.waitAndReloadBulkOperationProgressPage();

        String sameAssignee = backdoor.issueNavControl().getIssueKeyForSummary("retain the assignee");
        assertEquals(monkeyUser, backdoor.issues().getIssue(sameAssignee).fields.assignee.name);
        String changedAssignee = backdoor.issueNavControl().getIssueKeyForSummary("change the assignee");
        assertEquals(ADMIN_USERNAME, backdoor.issues().getIssue(changedAssignee).fields.assignee.name);
    }

    @Test
    public void testMoveIssueWithInactiveAssignee() {
        backdoor.darkFeatures().enableForSite("no.frother.assignee.field");

        backdoor.usersAndGroups().addUserToGroup(FRED_USERNAME, "jira-developers");
        backdoor.usersAndGroups().addUserToGroup(ADMIN_USERNAME, "jira-developers");
        backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "admin's issue", ADMIN_USERNAME);
        backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "fred's issue", FRED_USERNAME);
        backdoor.userManager().setActive(FRED_USERNAME, false);

        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator().bulkChange(ALL_PAGES)
                .selectAllIssues()
                .chooseOperation(BulkChangeWizard.BulkOperationsImpl.MOVE)
                .chooseTargetContextForAll(PROJECT_MONKEY)
                .finaliseFields()
                .complete();

        String adminsIssue = backdoor.issueNavControl().getIssueKeyForSummary("admin's issue");
        assertEquals(ADMIN_USERNAME, backdoor.issues().getIssue(adminsIssue).fields.assignee.name);
        String fredsIssue = backdoor.issueNavControl().getIssueKeyForSummary("fred's issue");
        assertEquals(FRED_USERNAME, backdoor.issues().getIssue(fredsIssue).fields.assignee.name);
    }

    @Test
    public void testMoveSTDComponentsAndVersionsRequiredFailure() {
        logger.log("Bulk Move - STD - components and versions required - failure");
        setupIssues(2);
        backdoor.versions().delete(mky1dot0.id.toString());
        backdoor.versions().delete(mky1dot1.id.toString());
        backdoor.versions().delete(mky1dot2.id.toString());
        backdoor.components().delete(mkyWrench.id.toString());

        editIssueFieldVisibility.setRequiredFields();
        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator()
                .bulkChange(ALL_PAGES)
                .selectAllIssues()
                .chooseOperation(BulkChangeWizard.BulkOperationsImpl.MOVE)
                .chooseTargetContextForAll(PROJECT_MONKEY)
                .finaliseFields();
        tester.assertTextPresent("&quot;" + COMPONENTS_FIELD_ID + "&quot; field is required and the project &quot;" + PROJECT_MONKEY + "&quot; does not have any " + "components");
        tester.assertTextPresent("&quot;" + FIX_VERSIONS_FIELD_ID + "&quot; field is required and the project &quot;" + PROJECT_MONKEY + "&quot; does not have any " + "versions");
        tester.assertTextPresent("&quot;" + AFFECTS_VERSIONS_FIELD_ID + "&quot; field is required and the project &quot;" + PROJECT_MONKEY + "&quot; does not have any " + "versions");
    }

    @Test
    public void testDontRetainRequiredComponentAndVersions() {
        logger.log("Bulk Move - STD - No retain, components and versions Required, Select new values");

        backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "issue that stays 1");
        backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "issue that stays 2");
        backdoor.issues().createIssue(PROJECT_MONKEY_KEY, "issue to move");
        updateIssue(HSP1, hspComponentOne, hsp2dot0);
        updateIssue(HSP2, hspComponentTwo, hsp3dot0);
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("key", HSP1, "component", hspComponentOne.name, "fixVersion", hsp2dot0.name), null, HSP1);
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("key", HSP2, "component", hspComponentTwo.name, "fixVersion", hsp3dot0.name), null, HSP2);

        editIssueFieldVisibility.setRequiredFields();

        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator()
                .bulkChange(ALL_PAGES)
                .selectAllIssues()
                .chooseOperation(BulkChangeWizard.BulkOperationsImpl.MOVE)
                .chooseTargetContextForAll(PROJECT_HOMOSAP)
                .setFieldValue(targetFieldName(FIELD_COMPONENTS, "-1"), hspComponentThree.id.toString())
                .setFieldValue(targetFieldName(FIELD_FIX_VERSIONS, "-1"), hsp1dot0.id.toString())
                .finaliseFields()
                .complete();

        bulkOperations.waitAndReloadBulkOperationProgressPage();

        navigation.issue().gotoIssue(HSP1);
        assertions.getViewIssueAssertions().assertComponents(hspComponentOne.name);
        assertions.getViewIssueAssertions().assertFixVersions(hsp2dot0.name);
        assertions.getViewIssueAssertions().assertAffectsVersions(VERSION_NAME_ONE);
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("key", HSP1, "component", hspComponentOne.name, "fixVersion", hsp2dot0.name, "version", VERSION_NAME_ONE), null, HSP1);


        navigation.issue().gotoIssue(HSP2);
        assertions.getViewIssueAssertions().assertComponents(hspComponentTwo.name);
        assertions.getViewIssueAssertions().assertFixVersions(hsp3dot0.name);
        assertions.getViewIssueAssertions().assertAffectsVersions(VERSION_NAME_ONE);
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("key", HSP2, "component", hspComponentTwo.name, "fixVersion", hsp3dot0.name, "version", VERSION_NAME_ONE), null, HSP2);

        //issue key changed...
        navigation.issue().gotoIssue(MKY1);
        tester.assertLinkPresentWithText(PROJECT_HOMOSAP);
        tester.assertLinkNotPresentWithText(PROJECT_MONKEY);
        assertions.getViewIssueAssertions().assertComponents(hspComponentThree.name);
        assertions.getViewIssueAssertions().assertFixVersions(hsp1dot0.name);
        assertions.getViewIssueAssertions().assertAffectsVersions(VERSION_NAME_ONE);
        //assert item was moved and the key has been updated in the index
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("key", HSP3, "component", hspComponentThree.name, "fixVersion", hsp1dot0.name, "version", VERSION_NAME_ONE), null, HSP3);

    }

    @Test
    public void testDontRetainNotRequiredComponentAndVersions() {
        logger.log("Bulk Move - STD - Dont Retain, components and versions Not Required, Select new values");

        backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "issue to move 1");
        backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "issue to move 2");
        backdoor.issues().createIssue(PROJECT_MONKEY_KEY, "issue that stays");
        updateIssue(HSP1, hspComponentOne, hsp2dot0);
        updateIssue(HSP2, hspComponentTwo, hsp3dot0);
        updateIssue(MKY1, mkyWrench, mky1dot1);

        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator()
                .bulkChange(ALL_PAGES)
                .selectAllIssues()
                .chooseOperation(BulkChangeWizard.BulkOperationsImpl.MOVE)
                .chooseTargetContextForAll(PROJECT_MONKEY)
                .setFieldValue(targetComponentFieldName(hspComponentOne), mkyWrench.id.toString())
                .setFieldValue(targetFixVersionName(hsp2dot0), mky1dot2.id.toString())
                .finaliseFields()
                .complete();

        bulkOperations.waitAndReloadBulkOperationProgressPage();

        // this issue was not touched by transformation
        navigation.issue().gotoIssue(MKY1);
        assertions.getViewIssueAssertions().assertComponents(mkyWrench.name);
        assertions.getViewIssueAssertions().assertFixVersions(mky1dot1.name);

        // this issue should have its versions/components changed:
        navigation.issue().gotoIssue(HSP1);
        tester.assertLinkNotPresentWithText(PROJECT_HOMOSAP);
        tester.assertLinkPresentWithText(PROJECT_MONKEY);
        assertions.getViewIssueAssertions().assertComponents(mkyWrench.name);
        assertions.getViewIssueAssertions().assertFixVersions(mky1dot2.name);

        // this issue should have its versions/components cleared:
        navigation.issue().gotoIssue(HSP2);
        tester.assertLinkNotPresentWithText(PROJECT_HOMOSAP);
        tester.assertLinkPresentWithText(PROJECT_MONKEY);
        assertions.getViewIssueAssertions().assertComponentsNone();
        assertions.getViewIssueAssertions().assertFixVersionsNone();
    }

    @Test
    public void testDontRetainNotRequiredNotSelectedComponentAndVersions() {
        logger.log("Bulk Move - STD - Dont Retain, components and versions Not Required, Dont Select new values");

        backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "issue to move 1");
        backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "issue to move 2");
        backdoor.issues().createIssue(PROJECT_MONKEY_KEY, "issue that stays");
        updateIssue(HSP1, hspComponentOne, hsp2dot0);
        updateIssue(HSP2, hspComponentTwo, hsp3dot0);

        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator()
                .bulkChange(ALL_PAGES)
                .selectAllIssues()
                .chooseOperation(BulkChangeWizard.BulkOperationsImpl.MOVE)
                .chooseTargetContextForAll(PROJECT_MONKEY)
                .finaliseFields()
                .complete();

        bulkOperations.waitAndReloadBulkOperationProgressPage();

        // this issue was not touched by transformation
        navigation.issue().gotoIssue(MKY1);
        assertions.getViewIssueAssertions().assertComponentsNone();
        assertions.getViewIssueAssertions().assertFixVersionsNone();

        // this issue should have its versions/components cleared:
        navigation.issue().gotoIssue(HSP1);
        tester.assertLinkNotPresentWithText(PROJECT_HOMOSAP);
        tester.assertLinkPresentWithText(PROJECT_MONKEY);
        assertions.getViewIssueAssertions().assertComponentsNone();
        assertions.getViewIssueAssertions().assertFixVersionsNone();

        // this issue should have its versions/components cleared:
        navigation.issue().gotoIssue(HSP2);
        tester.assertLinkNotPresentWithText(PROJECT_HOMOSAP);
        tester.assertLinkPresentWithText(PROJECT_MONKEY);
        assertions.getViewIssueAssertions().assertComponentsNone();
        assertions.getViewIssueAssertions().assertFixVersionsNone();
    }

    private void setupIssues(final int howMany) {
        for (int i = 0; i < howMany; i++) {
            final String summary = Integer.toBinaryString(i);
            assertNotNull(backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, summary).id);
        }
    }

    private String targetComponentFieldName(final Component forComponent) {
        return targetFieldName(FIELD_COMPONENTS, forComponent.id.toString());
    }

    private String targetFixVersionName(final Version forVersion) {
        return targetFieldName(FIELD_FIX_VERSIONS, forVersion.id.toString());
    }

    private String targetFieldName(final String fieldTypeName, final String id) {
        return StringUtils.join(new String[]{fieldTypeName, id}, "_");
    }


    private Version createVersion(final String name, final String description, final String project) {
        return backdoor.versions().create(new Version().name(name).description(description).project(project));
    }

    private Component createComponent(final String name, final String description, final String projectKey) {
        return backdoor.components().create(new Component().name(name).description(description).project(projectKey));
    }

    private void updateIssue(final String issueKey, final Component component, final Version version) {
        updateIssue(issueKey, component, version, null);
    }

    private void updateIssue(final String issueKey, final Component component, final Version version, final String issueType) {
        final IssueFields fields = new IssueFields()
                .fixVersions(ResourceRef.withId(version.id.toString()))
                .components(ResourceRef.withId(component.id.toString()));
        if (issueType != null) {
            fields.issueType(ResourceRef.withName(issueType));
        }
        issueClient.update(issueKey, new IssueUpdateRequest().fields(fields));
    }
}
