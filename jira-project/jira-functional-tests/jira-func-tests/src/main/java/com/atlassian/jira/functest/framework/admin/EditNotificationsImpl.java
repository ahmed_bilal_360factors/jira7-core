package com.atlassian.jira.functest.framework.admin;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LocatorFactoryImpl;
import com.atlassian.jira.functest.framework.util.form.FormParameterUtil;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;

/**
 * Default implementation of {@link com.atlassian.jira.functest.framework.admin.EditNotifications}.
 *
 * @since v4.4
 */
public class EditNotificationsImpl implements EditNotifications {
    private final WebTester tester;
    private final LocatorFactory locators;

    @Inject
    public EditNotificationsImpl(WebTester tester, JIRAEnvironmentData environmentData) {
        this.tester = tester;
        this.locators = new LocatorFactoryImpl(tester);
    }

    public EditNotificationsImpl(WebTester tester, JIRAEnvironmentData environmentData, int logIndentLevel) {
        this(tester, environmentData);
    }

    @Override
    public int notificationSchemeId() {
        return Integer.parseInt(locators.id("notification-scheme-id")
                .getNode().getAttributes().getNamedItem("value").getNodeValue());
    }

    @Override
    public EditNotifications addNotificationsForEvent(int eventId, NotificationType notificationType) {
        goAndSelectType(eventId, notificationType);
        submitAdd();
        return this;
    }

    @Override
    public EditNotifications addNotificationsForEvent(int eventId, NotificationType notificationType, String paramValue) {
        goAndSelectType(eventId, notificationType);
        if (notificationType == NotificationType.GROUP) {
            final FormParameterUtil formParameterUtil = new FormParameterUtil(tester, FunctTestConstants.JIRA_FORM_NAME, "Add");
            formParameterUtil.addOptionToHtmlSelect(notificationType.uiCode(), new String[]{paramValue});
            formParameterUtil.setFormElement(notificationType.uiCode(), paramValue);
            formParameterUtil.submitForm();
            //go back to scheme as would we after regular form submit
            tester.clickLinkWithText("Cancel");
        } else {
            tester.setFormElement(notificationType.uiCode(), paramValue);
            submitAdd();
        }
        return this;
    }

    private void goAndSelectType(int eventId, NotificationType notificationType) {
        tester.clickLink("add_" + eventId);
        tester.setWorkingForm(FunctTestConstants.JIRA_FORM_NAME);
        tester.checkRadioOption("type", notificationType.uiCode());
    }

    private void submitAdd() {
        tester.submit("Add");
    }
}
