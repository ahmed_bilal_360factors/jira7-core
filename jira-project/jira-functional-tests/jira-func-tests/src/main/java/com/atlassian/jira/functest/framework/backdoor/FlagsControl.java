package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

/**
 * Provides methods to control flags so they don't get in the way of webdriver tests. This does not necessarily use
 * backdoor methods, but it could
 *
 * If you're interested in disabling/clearing currently visible flags, take a look at AuiFlag#closeAll in jira-page-objects.
 *
 * @since v7.2
 */
public class FlagsControl extends BackdoorControl<FlagsControl> {
    public static final String AUI_FLAG_DISABLE = "com.atlassian.jira.dev.func-test-plugin:aui-flag-disable";

    private Backdoor backdoor;

    public FlagsControl(JIRAEnvironmentData environmentData, final Backdoor backdoor) {
        super(environmentData);
        this.backdoor = backdoor;
    }

    /**
     * Instructs JIRA to treat license reminder flags as if they have been dismissed for the current user. They may
     * come back again later!
     */
    public void clearFlags() {
        clearTimezonePrompt();
        clearBaseURLReminder();
        clearTipsSignupDialog(backdoor);
        backdoor.darkFeatures().enableForSite("never.display.maintenance.flag");
    }

    private void clearTipsSignupDialog(Backdoor backdoor) {
        backdoor.darkFeatures().enableForSite("never.display.newsletter.prompts");
    }

    public void enableFlags() {
        backdoor.plugins().disablePluginModule(AUI_FLAG_DISABLE);
    }

    public void disableFlags() {
        backdoor.plugins().enablePluginModule(AUI_FLAG_DISABLE);

    }

    private void clearTimezonePrompt() {
        resourceRoot(rootPath)
                .path("rest").path("flags").path("1.0")
                .path("flags").path("com.atlassian.jira.tzdetect.39600000%2C36000000").path("dismiss")
                .put();
    }

    private void clearBaseURLReminder() {
        resourceRoot(rootPath)
                .path("rest").path("flags").path("1.0")
                .path("flags").path("com.atlassian.jira.baseurl").path("dismiss")
                .put();
    }
}
