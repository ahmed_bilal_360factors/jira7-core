package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookRegistrationClient.Registration;
import com.atlassian.jira.functest.framework.navigation.BulkChangeWizard;
import com.atlassian.jira.functest.framework.navigation.IssueNavigatorNavigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.issue.fields.rest.json.beans.CommentJsonBean;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.Dates;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Comment;
import com.atlassian.jira.testkit.client.restclient.CommentClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.testkit.client.restclient.Visibility;
import com.atlassian.jira.util.lang.Pair;
import com.google.gson.Gson;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.HEAD;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.atlassian.jira.testkit.client.util.JsonMatchers.hasField;
import static com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks.TestIssueWebHook.ISSUE_UPDATED;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.WEBHOOKS, Category.REFERENCE_PLUGIN})
public class TestCommentWebHook extends AbstractWebHookTest {

    private static final long DEFAULT_SCHEME_ID = 0L;
    private static final String COMMENT_CREATED_WEBHOOK_ID = "comment_created";
    private static final String COMMENT_UPDATED_WEBHOOK_ID = "comment_updated";
    private static final String COMMENT_DELETED_WEBHOOK_ID = "comment_deleted";
    private static final int ONE_SECOND_IN_MILLIS = 1000;

    private CommentClient commentClient;

    @Before
    @Override
    public void setUpTest() {
        super.setUpTest();
        backdoor.restoreBlankInstance();
        commentClient = new CommentClient(environmentData);
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_SCHEME_ID, ProjectPermissions.ADD_COMMENTS);
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_SCHEME_ID, ProjectPermissions.EDIT_ALL_COMMENTS);
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_SCHEME_ID, ProjectPermissions.DELETE_ALL_COMMENTS);
    }

    @Test
    public void testCommentCreatedWebHook() throws JSONException {
        registerWebHook(COMMENT_CREATED_WEBHOOK_ID);

        Pair<Response<Comment>, IssueCreateResponse> issueWithComment = createIssueWithComment();

        WebHookResponseData webHookResponse = getWebHookResponse();

        JSONObject jsonObject = webHookResponse.asJsonObject();

        assertHasWebHookEvent(jsonObject, COMMENT_CREATED_WEBHOOK_ID);
        assertThat(webHookResponseAsCommentBean(webHookResponse), isComment(issueWithComment.first().body));
    }

    @Test
    public void testCommentCreatedWithRoleVisibility() throws JSONException {
        registerWebHook(COMMENT_CREATED_WEBHOOK_ID);

        final IssueCreateResponse issue = createTestIssue();

        Comment comment = new Comment();
        comment.body = "comment body";
        comment.visibility = new Visibility("group", "jira-administrators");
        Response<Comment> commentResponse = commentClient.post(issue.key, comment);

        WebHookResponseData webHookResponseData = getWebHookResponse();

        JSONObject jsonObject = webHookResponseData.asJsonObject();
        assertHasWebHookEvent(jsonObject, COMMENT_CREATED_WEBHOOK_ID);
        CommentJsonBean commentFromWebHook = webHookResponseAsCommentBean(webHookResponseData);

        assertThat(commentFromWebHook, isComment(commentResponse.body));
        assertThat(commentFromWebHook, both(
                hasProperty("visibility", hasProperty("type", hasToString(equalTo(comment.visibility.type))))).and(
                hasProperty("visibility", hasProperty("value", equalTo(comment.visibility.value)))
        ));
    }

    @Test
    public void testCommentCreatedWebHookAndIssueUpdatedAreBothTriggered() throws Exception {
        registerWebHook(COMMENT_CREATED_WEBHOOK_ID);
        HttpResponseTester issueUpdatedWebHookTester = createIssueUpdatedWebHookTester();

        Pair<Response<Comment>, IssueCreateResponse> issueWithComment = createIssueWithComment();

        WebHookResponseData commentWebHookResponse = getWebHookResponse();
        WebHookResponseData issueUpdatedWebHookResponse = issueUpdatedWebHookTester.getResponseData();

        assertHasWebHookEvent(commentWebHookResponse.asJsonObject(), COMMENT_CREATED_WEBHOOK_ID);
        assertHasWebHookEvent(issueUpdatedWebHookResponse.asJsonObject(), ISSUE_UPDATED);

        CommentJsonBean commentBean = webHookResponseAsCommentBean(commentWebHookResponse);
        assertThat(commentBean, isComment(issueWithComment.first().body));

        CommentJsonBean commentFromIssueUpdated = getCommentFromIssueUpdatedWebHook(issueUpdatedWebHookResponse);
        assertThat(commentFromIssueUpdated, isComment(issueWithComment.first().body));
    }

    @Test
    public void testUriVariableDuringIssueCreatedWebHook() throws Exception {
        Registration registration = new Registration();
        registration.name = "Comment Issue Web Hook";
        registration.events = new String[]{COMMENT_CREATED_WEBHOOK_ID};
        registration.path = "/issueKey=${issue.key}&commentId=${comment.id}";
        responseTester.registerWebhook(registration);

        IssueCreateResponse issue = createTestIssue();

        Comment comment = new Comment();
        comment.body = "This is a sample comment which should trigger webhook";
        Response<Comment> commentResponse = commentClient.post(issue.key, comment);

        WebHookResponseData webHookResponseData = responseTester.getResponseData();
        assertThat(webHookResponseData.getUri().toString(), both(containsString(commentResponse.body.id)).and(containsString(issue.key)));
    }

    @Test
    public void testCommentUpdatedWebHook() throws JSONException {
        registerWebHook(COMMENT_UPDATED_WEBHOOK_ID);

        Pair<Response<Comment>, IssueCreateResponse> issueWithComment = createIssueWithComment();
        Response<Comment> updatedComment = updateComment(issueWithComment.second(), issueWithComment.first().body);

        WebHookResponseData webHookResponse = getWebHookResponse();

        assertHasWebHookEvent(webHookResponse.asJsonObject(), COMMENT_UPDATED_WEBHOOK_ID);
        assertThat(webHookResponseAsCommentBean(webHookResponse), isComment(updatedComment.body));
    }

    @Test
    public void testCommentUpdatedWebHookIsTriggeredWithIssueUpdateWebHook() throws Exception {
        registerWebHook(COMMENT_UPDATED_WEBHOOK_ID);
        HttpResponseTester issueUpdatedWebHookTester = createIssueUpdatedWebHookTester();

        Pair<Response<Comment>, IssueCreateResponse> issueWithComment = createIssueWithComment();

        // first, jira:issue_updated webhook is triggered for comment create
        WebHookResponseData issueUpdateResponseWithCommentCreated = issueUpdatedWebHookTester.getResponseData();
        assertHasWebHookEvent(issueUpdateResponseWithCommentCreated.asJsonObject(), ISSUE_UPDATED);
        assertThat(getCommentFromIssueUpdatedWebHook(issueUpdateResponseWithCommentCreated), isComment(issueWithComment.first().body));

        Response<Comment> updatedComment = updateComment(issueWithComment.second(), issueWithComment.first().body);

        // jira:issue_updated webhook is triggered for comment update as well
        WebHookResponseData issueUpdatedWebHookWithCommentUpdated = issueUpdatedWebHookTester.getResponseData();
        assertHasWebHookEvent(issueUpdatedWebHookWithCommentUpdated.asJsonObject(), ISSUE_UPDATED);
        assertThat(getCommentFromIssueUpdatedWebHook(issueUpdatedWebHookWithCommentUpdated), isComment(updatedComment.body));

        WebHookResponseData webHookResponse = getWebHookResponse();
        assertHasWebHookEvent(webHookResponse.asJsonObject(), COMMENT_UPDATED_WEBHOOK_ID);
        assertThat(webHookResponseAsCommentBean(webHookResponse), isComment(updatedComment.body));
    }

    @Test
    @LoginAs(user = "admin")
    public void testCommentCreateWebHookTriggeredDuringBulkUpdate() throws Exception {
        HttpResponseTester bulkChangeTester = new HttpResponseTester(environmentData, 10);

        bulkChangeTester.start();

        Registration registration = new Registration();
        registration.name = "Comment Issue Web Hook";
        registration.events = new String[]{COMMENT_CREATED_WEBHOOK_ID};
        registration.path = "/issueKey=${issue.key}&commentId=${comment.id}";
        bulkChangeTester.registerWebhook(registration);

        Supplier<IntStream> testCount = () -> IntStream.range(0, 10);
        List<IssueCreateResponse> createdIssues =
                testCount.get().mapToObj(i -> createTestIssue()).collect(Collectors.toList());

        String commentBody = "Bulk comment";

        navigation.login("admin");
        navigation.issueNavigator().displayAllIssues();

        BulkChangeWizard wizard = navigation.issueNavigator().bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES)
                .selectAllIssues()
                .chooseOperation(BulkChangeWizard.BulkOperationsImpl.EDIT)
                .checkActionForField(FunctTestConstants.FIELD_COMMENT)
                .setFieldValue(FunctTestConstants.FIELD_COMMENT, commentBody)
                .finaliseFields()
                .complete()
                .waitForBulkChangeCompletion();

        assertEquals(wizard.getState(), BulkChangeWizard.WizardState.COMPLETE);

        List<WebHookResponseData> webHookResponsesForBulkEdit = testCount.get().mapToObj(i -> {
            try {
                return bulkChangeTester.getResponseData();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());

        // By default IDEA barfs on this if you inline the collector so it's split out here.
        final Collector<Matcher<? super WebHookResponseData>, ?, List<Matcher<? super WebHookResponseData>>> collector =
                Collectors.toList();
        Matcher<Iterable<? extends WebHookResponseData>> allResponsesNotNullMatcher =
                contains(testCount.get().mapToObj(e -> notNullValue()).collect(collector));
        assertThat(webHookResponsesForBulkEdit, allResponsesNotNullMatcher);

        List<String> webHookReceiveUrls = webHookResponsesForBulkEdit.stream()
                .map(response -> response.getUri().toString())
                .collect(Collectors.toList());

        List<CommentJsonBean> commentJsonBeans = webHookResponsesForBulkEdit.stream()
                .map(this::webHookResponseAsCommentBean)
                .collect(Collectors.toList());

        Matcher<Iterable<? super String>> urlMatchers = Matchers.hasItem(anyOf(createdIssues.stream()
                .map(issue -> containsString(issue.key))
                .collect(Collectors.toList())));

        assertThat(webHookReceiveUrls, urlMatchers);

        Matcher<Iterable<?>> allCommentJsonHaveProperBody =
                contains(testCount.get().mapToObj(e -> hasProperty("body", equalTo(commentBody))).collect(Collectors.toList()));
        assertThat(commentJsonBeans, allCommentJsonHaveProperBody);

        bulkChangeTester.stop();
    }

    @Test
    public void testCommentDeleted() throws JSONException {
        registerWebHook(COMMENT_DELETED_WEBHOOK_ID);

        Pair<Response<Comment>, IssueCreateResponse> issueWithComment = createIssueWithComment();

        commentClient.delete(issueWithComment.second().key, issueWithComment.first().body.id);

        WebHookResponseData webHookResponse = getWebHookResponse();

        assertHasWebHookEvent(webHookResponse.asJsonObject(), COMMENT_DELETED_WEBHOOK_ID);
        assertThat(webHookResponseAsCommentBean(webHookResponse), isComment(issueWithComment.first().body));
    }

    @Test
    public void testCommentDeletedTriggersIssueUpdateAndCommentDeleted() throws InterruptedException, JSONException {
        registerWebHook(COMMENT_DELETED_WEBHOOK_ID);
        HttpResponseTester issueUpdatedWebHookTester = createIssueUpdatedWebHookTester();

        Pair<Response<Comment>, IssueCreateResponse> issueWithComment = createIssueWithComment();

        // first, comment creation should have triggered issue_update
        WebHookResponseData commentCreateWebHookResponse = issueUpdatedWebHookTester.getResponseData();
        assertHasWebHookEvent(commentCreateWebHookResponse.asJsonObject(), ISSUE_UPDATED);
        assertThat(webHookResponseAsCommentBean(commentCreateWebHookResponse), isComment(issueWithComment.first().body));

        commentClient.delete(issueWithComment.second().key, issueWithComment.first().body.id);

        // comment deletion triggered issue_updated webhook
        WebHookResponseData issueUpdateByCommentDeletionResponse = issueUpdatedWebHookTester.getResponseData();
        assertHasWebHookEvent(issueUpdateByCommentDeletionResponse.asJsonObject(), ISSUE_UPDATED);

        // and comment_deleted webhook
        WebHookResponseData webHookResponse = getWebHookResponse();
        assertHasWebHookEvent(webHookResponse.asJsonObject(), COMMENT_DELETED_WEBHOOK_ID);
        assertThat(webHookResponseAsCommentBean(webHookResponse), isComment(issueWithComment.first().body));
    }

    @Test
    public void testCommentDeletedWebHooksTriggeredWhenIssueDeleted() throws JSONException, IOException, InterruptedException {
        HttpResponseTester deleteTester = new HttpResponseTester(environmentData, 2);
        deleteTester.start();

        Registration registration = new Registration();
        registration.name = "Comment Issue Web Hook";
        registration.events = new String[]{COMMENT_DELETED_WEBHOOK_ID};
        registration.path = "/issueKey=${issue.key}&commentId=${comment.id}";
        deleteTester.registerWebhook(registration);

        Pair<Response<Comment>, IssueCreateResponse> issueWithComment = createIssueWithComment();
        Comment secondComment = new Comment();
        secondComment.body = "Second comment";
        final Response<Comment> secondCommentResponse = commentClient.post(issueWithComment.second().key, secondComment);

        backdoor.issues().deleteIssue(issueWithComment.second().key, true);

        WebHookResponseData firstCommentDeletedWebHookResponse = deleteTester.getResponseData();
        WebHookResponseData secondCommentDeletedWebHookResponse = deleteTester.getResponseData();

        assertHasWebHookEvent(firstCommentDeletedWebHookResponse.asJsonObject(), COMMENT_DELETED_WEBHOOK_ID);
        assertHasWebHookEvent(secondCommentDeletedWebHookResponse.asJsonObject(), COMMENT_DELETED_WEBHOOK_ID);

        assertThat(webHookResponseAsCommentBean(firstCommentDeletedWebHookResponse), anyOf(isComment(issueWithComment.first().body), isComment(secondCommentResponse.body)));
        assertThat(webHookResponseAsCommentBean(secondCommentDeletedWebHookResponse), anyOf(isComment(issueWithComment.first().body), isComment(secondCommentResponse.body)));

        deleteTester.stop();
    }

    @Test
    public void testWebHookListenerRegisteredWithModuleDescriptorReceivesCommentCreated() throws JSONException {
        Pair<Response<Comment>, IssueCreateResponse> issueWithComment = createIssueWithComment();

        WebHookResponseData webHookResponse = getWebHookResponseFromReferencePlugin(COMMENT_CREATED_WEBHOOK_ID);

        assertHasWebHookEvent(webHookResponse.asJsonObject(), COMMENT_CREATED_WEBHOOK_ID);
        assertThat(webHookResponseAsCommentBean(webHookResponse), isComment(issueWithComment.first().body));
    }

    @Test
    public void testWebHookListenerRegisteredWithModuleDescriptorReceivesCommentUpdated() throws JSONException {
        Pair<Response<Comment>, IssueCreateResponse> issueWithComment = createIssueWithComment();
        Response<Comment> updatedComment = updateComment(issueWithComment.second(), issueWithComment.first().body);

        WebHookResponseData webHookResponse = getWebHookResponseFromReferencePlugin(COMMENT_UPDATED_WEBHOOK_ID);

        assertHasWebHookEvent(webHookResponse.asJsonObject(), COMMENT_UPDATED_WEBHOOK_ID);
        assertThat(webHookResponseAsCommentBean(webHookResponse), isComment(updatedComment.body));
    }

    @Test
    public void testWebHookListenerRegisteredWithModuleDescriptorReceivesCommentDelete() throws JSONException {
        Pair<Response<Comment>, IssueCreateResponse> issueWithComment = createIssueWithComment();
        commentClient.delete(issueWithComment.second().key, issueWithComment.first().body.id);

        WebHookResponseData webHookResponse = getWebHookResponseFromReferencePlugin(COMMENT_DELETED_WEBHOOK_ID);

        assertHasWebHookEvent(webHookResponse.asJsonObject(), COMMENT_DELETED_WEBHOOK_ID);
        assertThat(webHookResponseAsCommentBean(webHookResponse), isComment(issueWithComment.first().body));
    }

    private CommentJsonBean getCommentFromIssueUpdatedWebHook(final WebHookResponseData issueUpdatedWebHookResponse)
            throws JSONException {
        return new Gson().fromJson(issueUpdatedWebHookResponse.asJsonObject().get("comment").toString(), CommentJsonBean.class);
    }

    private CommentJsonBean webHookResponseAsCommentBean(final WebHookResponseData webHookResponse) {
        try {
            return webHookResponse.asBean(CommentJsonBean.class, "comment");
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private Pair<Response<Comment>, IssueCreateResponse> createIssueWithComment() {
        IssueCreateResponse issue = createTestIssue();

        Comment comment = new Comment();
        comment.body = "This is a sample comment which should trigger webhook";
        return Pair.of(commentClient.post(issue.key, comment), issue);
    }

    private Response<Comment> updateComment(final IssueCreateResponse issue, final Comment comment) {
        final Comment commentToUpdate = new Comment();
        commentToUpdate.body = "Updated comment body";
        commentToUpdate.visibility = comment.visibility;
        commentToUpdate.id = comment.id;
        return commentClient.put(issue.key, commentToUpdate);
    }

    private void assertHasWebHookEvent(final JSONObject jsonObject, final String webHookId) {
        assertThat(jsonObject, hasField("webhookEvent").equalTo(webHookId));
    }

    private HttpResponseTester createIssueUpdatedWebHookTester() {
        HttpResponseTester issueUpdatedTester = HttpResponseTester.createTester(getEnvironmentData());
        Registration registration = new Registration();
        registration.events = new String[]{ISSUE_UPDATED};
        registration.name = "Issue updated webhook";
        issueUpdatedTester.registerWebhook(registration);
        return issueUpdatedTester;
    }

    private IssueCreateResponse createTestIssue() {
        return backdoor.issues().createIssue("HSP", "Issue with comments");
    }

    private Matcher<? super CommentJsonBean> isComment(final Comment comment) {
        return allOf(
                hasProperty("id", equalTo(String.valueOf(comment.id))),
                hasProperty("body", equalTo(comment.body)),
                hasProperty("updated", equalWithOneSecondPrecision(Dates.fromTimeString(comment.updated))),
                hasProperty("created", equalWithOneSecondPrecision(Dates.fromTimeString(comment.created))),
                hasProperty("author", hasProperty("name", equalTo(comment.author.name)))
        );
    }

    // Different databases have different precision for datatime data type
    // For example MySQL - 1 second, MS SQL - 1/300 of second
    private static Matcher<Date> equalWithOneSecondPrecision(final Date date) {
        return new TypeSafeMatcher<Date>() {
            @Override
            protected boolean matchesSafely(final Date item) {
                return Math.abs(date.getTime() - item.getTime()) < ONE_SECOND_IN_MILLIS;
            }

            @Override
            public void describeTo(final Description description) {
                final DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                description.appendText(format.format(date));
            }
        };
    }
}
