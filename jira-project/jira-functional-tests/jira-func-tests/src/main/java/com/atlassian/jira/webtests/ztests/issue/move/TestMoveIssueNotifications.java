package com.atlassian.jira.webtests.ztests.issue.move;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.EmailBaseFuncTestCase;
import org.junit.Before;
import org.junit.Test;

import javax.mail.internet.MimeMessage;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.MOVE_ISSUE})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestMoveIssueNotifications.xml")
public class TestMoveIssueNotifications extends EmailBaseFuncTestCase {

    private static final String ISSUE_KEY = "PONE-1";
    private static final String TEXT_ON_ISSUE_MOVED_MAIL = "moved";
    private static final String TEXT_ON_ISSUE_UPDATED_MAIL = "updated";

    @Before
    public void setUp() {
        navigation.userProfile().changeNotifyMyChanges(true);
        configureAndStartSmtpServer();
    }

    @Test
    public void testMovingIssueWithoutSubtaskSendsEmailWhenWeChangeProject() throws Exception {
        moveIssueToDifferentProject(ISSUE_KEY);

        assertEmailContainingText(TEXT_ON_ISSUE_MOVED_MAIL);
    }

    @Test
    public void testMovingIssuesWithoutSubtaskSendsEmailWhenWeDoNotChangeProject() throws Exception {
        changeIssueTypeKeepingItOnTheSameProject(ISSUE_KEY);

        assertEmailContainingText(TEXT_ON_ISSUE_UPDATED_MAIL);
    }

    private void moveIssueToDifferentProject(final String issueKey) {
        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("move-issue");
        navigation.issue().selectProject("project2");
        tester.submit("Next >>");
        tester.submit("Next >>");
        tester.submit("Move");
    }

    private void changeIssueTypeKeepingItOnTheSameProject(final String issueKey) {
        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("move-issue");
        navigation.issue().selectIssueType("Task");
        tester.submit("Next >>");
        tester.submit("Next >>");
        tester.submit("Move");
    }

    private void assertEmailContainingText(String text) throws Exception {
        flushMailQueueAndWait(1);
        List<MimeMessage> messagesForRecipient = getMessagesForRecipient("admin@example.com");
        assertThat(messagesForRecipient.size(), is(1));
        assertEmailBodyContains(messagesForRecipient.get(0), text);
    }
}
