package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.admin.user.ViewUserPage;
import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.webtests.EmailBaseFuncTestCase;
import com.atlassian.jira.webtests.ztests.tpm.ldap.UserDirectoryTable;
import com.meterware.httpunit.WebLink;
import org.junit.Test;

import javax.inject.Inject;
import javax.mail.internet.MimeMessage;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
@RestoreBlankInstance
public class TestAddUser extends EmailBaseFuncTestCase {
    private static final String CREATE_USER_SUBMIT = "Create";

    private static final String TEST_USERNAME = "user1";
    private static final String TEST_FULL_NAME = "User1 Tested";
    private static final String TEST_EMAIL = "username1@email.com";
    private static final String TEST_PASSWORD = "evilwoman1";

    private static final String TEST_USERNAME_2 = "user2";
    private static final String TEST_FULL_NAME_2 = "User2 Tested";
    private static final String TEST_EMAIL_2 = "username2@email.com";

    private static final String CREATE_NEW_USER = "Create new user";
    @Inject
    protected TextAssertions textAssertions;
    @Inject
    private FuncTestLogger logger;
    @Inject
    private Assertions assertions;

    @Test
    @LoginAs(user = "admin")
    public void formDecoratesEmptyDataWithMessages() {
        buildUser("", "", "");
        tester.submit(CREATE_USER_SUBMIT);

        tester.assertTextPresent(CREATE_NEW_USER);
        tester.assertTextPresent("You must specify a username.");
        tester.assertTextPresent("You must specify a full name.");
        tester.assertTextPresent("You must specify an email address.");
    }

    @Test
    @LoginAs(user = "admin")
    public void systemRejectsDuplicateUserCreation() {
        checkSuccessUserCreate(false);

        buildUser(TEST_USERNAME, TEST_FULL_NAME, TEST_EMAIL);
        tester.submit(CREATE_USER_SUBMIT);
        tester.assertTextPresent("A user with that username already exists.");
    }

    @Test
    @LoginAs(user = "admin")
    public void createAnotherRedirToItselfWithCheckboxPrechecked() {
        checkSuccessUserCreate(false, true);

        tester.assertFormElementEquals("createAnother", "true");
        setUpUser(TEST_USERNAME_2, TEST_FULL_NAME_2, TEST_EMAIL_2);
        tester.uncheckCheckbox("createAnother");
        tester.submit(CREATE_USER_SUBMIT);

        assertRedirectToUserBrowserAfterUserCreation(TEST_USERNAME, TEST_USERNAME_2);
        assertSuccessUserCreate(TEST_USERNAME_2, TEST_FULL_NAME_2, TEST_EMAIL_2);
    }

    @Test
    @LoginAs(user = "admin")
    public void createUserSuccessPath() {
        checkSuccessUserCreate(false);
    }

    @Test
    @LoginAs(user = "admin")
    public void systemRejectsInvalidEmail() {
        buildUser(TEST_USERNAME, TEST_FULL_NAME, "username.email.com");
        tester.submit(CREATE_USER_SUBMIT);
        tester.assertTextPresent("You must specify a valid email address.");
    }

    @Test
    @LoginAs(user = "admin")
    public void testCreateUserPassword() {
        buildUser(TEST_USERNAME, TEST_FULL_NAME, TEST_EMAIL);

        tester.setFormElement("password", "password");
        tester.submit(CREATE_USER_SUBMIT);

        assertRedirectToUserBrowserAfterUserCreation(TEST_USERNAME);
        assertSuccessUserCreate(TEST_USERNAME, TEST_FULL_NAME, TEST_EMAIL);
    }

    @Test
    @LoginAs(user = "fred")
    public void testNoPermission() {
        tester.gotoPage("http://localhost:8090/jira/secure/admin/user/AddUser!default.jspa");

        tester.assertTextPresent("Welcome to jWebTest JIRA installation");
        tester.assertTextNotPresent("Project: newproject");
        tester.assertTextNotPresent("Add a new project");
    }

    // Removed testCreateUserExternalUserConfiguration ignored in JRADEV-8029

    @Test
    @LoginAs(user = "admin")
    public void createUserEmailIsSent() throws Exception {
        configureAndStartSmtpServer();

        checkSuccessUserCreate(true);

        // now see if we got an email with the right details
        flushMailQueueAndWait(1);

        final MimeMessage[] mimeMessages = mailService.getReceivedMessages();
        assertEquals(1, mimeMessages.length);

        final MimeMessage msg = mimeMessages[0];

        // should raise no exception:
        msg.getContent();

        assertEmailBodyContainsLine(msg, ".*" + TEST_USERNAME + ".*");
        assertEmailBodyContainsLine(msg, ".*" + TEST_FULL_NAME + ".*");
        assertEmailBodyContains(msg, TEST_EMAIL);

        // does it have the reset password part
        assertEmailBodyContains(msg, "Get started by setting your own password and logging in");
        assertEmailBodyContains(msg, "secure/ResetPassword!default.jspa?os_username=" + TEST_USERNAME + "&amp;token=");

        // and make sure it has NO password in there
        assertEmailBodyDoesntContain(msg, TEST_PASSWORD);
    }

    /**
     * Goes to the create user form and fills in values specified, but does not submit the form.
     *
     * @param username the username to give the user to be created.
     * @param fullName the full name to give the user to be created.
     * @param email    the email address to give the user to be created.
     */
    private void buildUser(final String username, final String fullName, final String email) {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.clickLink("create_user");

        setUpUser(username, fullName, email);
    }

    public void setUpUser(final String username, final String fullName, final String email) {
        tester.assertTextPresent(CREATE_NEW_USER);

        tester.setFormElement("username", username);
        tester.setFormElement("fullname", fullName);
        tester.setFormElement("email", email);
    }

    private void checkSuccessUserCreate(final boolean sendEmail) {
        checkSuccessUserCreate(sendEmail, false);
    }

    private void checkSuccessUserCreate(final boolean sendEmail, final boolean createAnother) {
        buildUser(TEST_USERNAME, TEST_FULL_NAME, TEST_EMAIL);
        tester.setFormElement("password", TEST_PASSWORD);
        if (sendEmail) {
            tester.checkCheckbox("sendEmail", "true");
        }
        if (createAnother) {
            tester.checkCheckbox("createAnother", "true");
        } else {
            tester.assertCheckboxNotSelected("createAnother");
        }
        tester.submit(CREATE_USER_SUBMIT);

        if (!createAnother) {
            tester.assertTextPresent(TEST_FULL_NAME);
            assertRedirectToUserBrowserAfterUserCreation(TEST_USERNAME);
            assertSuccessUserCreate(TEST_USERNAME, TEST_FULL_NAME, TEST_EMAIL);
        }
    }

    private void assertRedirectToUserBrowserAfterUserCreation(final String... createdUsers) {
        final String params = Arrays.stream(createdUsers).map(s -> String.format("createdUser=%s", s)).collect(Collectors.joining("&"));
        assertEquals(Navigation.AdminSection.USER_BROWSER.getUrl() + "?" + params + "#userCreatedFlag", navigation.getCurrentPage());
    }

    private void assertSuccessUserCreate(final String username, final String fullName, final String email) {
        navigation.gotoPageWithParams(ViewUserPage.class, ViewUserPage.generateViewUserQueryParameters(username));

        tester.assertTextPresent("User: " + fullName);

        final String[] userDetails = {"Username:", username,
                "Full name:", fullName,
                "Email:", email
        };
        textAssertions.assertTextSequence(new WebPageLocator(tester), userDetails);

    }

    //JRA-25554
    @Test
    @LoginAs(user = "admin")
    public void testNewUsersNotAddedToNestedGroups() {
        try {

            addEditNestedGroups();

            checkSuccessUserCreate(false);

            // check the members groups
            navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
            tester.clickLink(TEST_USERNAME);
            tester.assertTextPresent("jira-users");
            tester.assertTextNotPresent("accounts");
            tester.assertTextNotPresent("sales");
            tester.assertTextNotPresent("customer-service");
        } finally {
            // Reset the admin password otherwise the next test to run will be fail because some dick hacked the authenticator in most tests.
            resetAdminPassword();
        }
    }

    private void addEditNestedGroups() {
        toggleNestedGroups(true);

        addGroup("accounts");
        addGroup("sales");
        addGroup("customer-service");
        navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
        tester.clickLink("edit_nested_groups");
        tester.assertTextPresent("This page allows you to edit nested group memberships.");

        tester.setWorkingForm("jiraform");
        selectMultiOption("selectedGroupsStr", "jira-users");
        selectMultiOption("childrenToAssignStr", "accounts");
        selectMultiOption("childrenToAssignStr", "sales");
        selectMultiOption("childrenToAssignStr", "customer-service");

        tester.submit("assign");
    }

    public void selectMultiOption(final String selectName, final String option) {
        // A bit of a hack. The only way to really select multiple options at the moment is to treat it like a checkbox
        final String value = tester.getDialog().getValueForOption(selectName, option);
        tester.checkCheckbox(selectName, value);
    }

    private void addGroup(final String groupName) {
        navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
        tester.setFormElement("addName", groupName);
        tester.submit("add_group");
    }

    public void toggleNestedGroups(final boolean enable) {
        navigation.gotoAdmin();
        tester.gotoPage("/plugins/servlet/embedded-crowd/directories/list");

        final UserDirectoryTable userDirectoryTable = new UserDirectoryTable(this, logger, assertions);
        final WebLink link = userDirectoryTable.getTableCell(1, 4).getLinkWith("edit");
        navigation.clickLink(link);
        if (enable) {
            tester.checkCheckbox("nestedGroupsEnabled", "true");
        } else {
            tester.checkCheckbox("nestedGroupsEnabled", "false");
        }
        tester.submit("save");
    }

    public void resetAdminPassword() {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.clickLink("admin");
        tester.clickLinkWithText("Set Password");
        tester.setFormElement("password", "admin");
        tester.setFormElement("confirm", "admin");
        tester.submit("Update");
    }
}