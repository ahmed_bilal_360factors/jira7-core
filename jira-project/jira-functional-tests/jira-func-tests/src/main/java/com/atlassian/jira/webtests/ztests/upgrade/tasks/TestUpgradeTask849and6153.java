package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.matcher.IssueLinksTypeMatcher;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.IssuelinksType;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.net.URISyntaxException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.RENAME_USER;
import static com.atlassian.jira.functest.framework.suite.Category.UPGRADE_TASKS;
import static org.junit.Assert.assertThat;

/**
 * @since v6.0
 */
@WebTest({FUNC_TEST, RENAME_USER, UPGRADE_TASKS})
public class TestUpgradeTask849and6153 extends BaseJiraFuncTest {

    private JiraRestClient restClient;

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        final AsynchronousJiraRestClientFactory restClientFactory = new AsynchronousJiraRestClientFactory();
        try {
            restClient = restClientFactory.createWithBasicHttpAuthentication(environmentData.getBaseUrl().toURI(), ADMIN_USERNAME, ADMIN_PASSWORD);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testLegacyDirectionPropertyTrueIfClonersSwapped() {
        administration.restoreDataWithBuildNumber("ClonersSwapped_Pre5.2.6.xml", 848);
        final Iterable<IssuelinksType> issuelinksTypes = restClient.getMetadataClient().getIssueLinkTypes().claim();
        assertThat(issuelinksTypes, Matchers.contains(IssueLinksTypeMatcher.issueLinkType("Cloners", "is cloned by", "clones")));
    }

    @Test
    public void testLegacyDirectionPropertyTrueIfClonersChanged() {
        administration.restoreDataWithBuildNumber("ClonersModified_Pre5.2.6.xml", 848);
        final Iterable<IssuelinksType> issuelinksTypes = restClient.getMetadataClient().getIssueLinkTypes().claim();
        assertThat(issuelinksTypes, Matchers.contains(IssueLinksTypeMatcher.issueLinkType("Cloners", "is copied by", "copies")));
    }

    @Test
    public void testLegacyDirectionPropertyFalseIfClonersUnmodified() {
        administration.restoreDataWithBuildNumber("ClonersUnmodified_Pre5.2.6.xml", 848);
        final Iterable<IssuelinksType> issuelinksTypes = restClient.getMetadataClient().getIssueLinkTypes().claim();
        assertThat(issuelinksTypes, Matchers.contains(IssueLinksTypeMatcher.issueLinkType("Cloners", "is cloned by", "clones")));
    }
}