package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.framework.util.env.EnvironmentUtils;
import com.atlassian.jira.rest.v2.issue.GroupBean;
import com.atlassian.jira.testkit.client.UsersAndGroupsControl;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Test counts of users within groups.
 * <p>
 * This test compares whether the group counts we calculate (with a direct db query) match the number of users that
 * Crowd considers to be in the group.
 * This ensures our count query matches Crowd's rules around directory shadowing, nested groups, inactive users, etc.
 * </p>
 * <p>
 * The restored backup file contains multiple user directories with users and groups configured in various ways.
 * Directories are described here, in priority order:
 * <pre>
 *    Directory 1 (id = 1002, priority = 0)
 *    ===========
 *       Group                 | User
 *       ================================
 *       Koalas                | Anna
 *                             | Betty
 *
 *       Wombats               | Catherine
 *                             | John (inactive)
 *
 *       Wallabies (inactive)  | Catherine
 *
 *       Sealife               | Anna
 *
 *         -> Starfish         | Betty
 *
 *       Lions                 | Leopold
 *
 *    Directory 2 (id = 1001, priority = 1)
 *    ===========
 *       Group                 | User
 *       ================================
 *       Koalas                | Anna
 *                             | David
 *
 *       Wombats               | David
 *                             | John
 *
 *       Echidnas              | Ernie
 *
 *       Sealife               | David
 *
 *       Starfish              | Ernie
 *
 *    Directory 3 (INACTIVE) (id = 2001, priority = 2)
 *    ===========
 *       Group                 | User
 *       ================================
 *       Lions                 | Leonardo
 *
 *    Directory 4 (id = 3001, priority = 3)
 *    ===========
 *       Group                 | User
 *       ================================
 *       ground                | Bill
 *        -> hog               | Bill
 *           -> day            | Bill
 *                             | Andie
 *
 *       DiamondTop            | White
 *        -> DiamondLeft       | Pink
 *           -> DiamondBottom  | Yellow
 *         -> DiamondRight     | Blue
 *           -> (DiamondBottom)| (Yellow)
 *
 *       nest1                 | Owl
 *        -> nest2             | Robin
 *           -> nest3          | Frogmouth
 *              -> nest4       | Wren
 *                 -> nest5    | Lorikeet
 *
 *       Hall                  | Louis
 *        -> Mirrors           | Jules
 *           -> (Hall)         | (Louis)
 * </pre>
 * </p>
 */
@WebTest({Category.FUNC_TEST, Category.REST, Category.USERS_AND_GROUPS})
@Restore(TestGroupResourceCountUsers.GROUP_RESOURCE_COUNT_USERS_XML)
public class TestGroupResourceCountUsers extends BaseJiraFuncTest {
    public static final String GROUP_RESOURCE_COUNT_USERS_XML = "TestGroupResourceCountUsers.xml";
    private GroupClient groupClient;

    /**
     * query param to expand all users
     */
    private static String EXPAND_ALL_USERS = "users";

    private static final String KOALAS = "Koalas";
    private static final String WOMBATS = "Wombats";
    private static final String WALLABIES = "Wallabies";
    private static final String ECHIDNAS = "Echidnas";
    private static final String SEALIFE = "Sealife";
    private static final String LIONS = "Lions";

    private static final String DIAMOND_TOP = "DiamondTop";
    private static final String DIAMOND_LEFT = "DiamondLeft";
    private static final String DIAMOND_RIGHT = "DiamondRight";
    private static final String DIAMOND_BOTTOM = "DiamondBottom";

    private static final String NEST_1 = "nest1";
    private static final String NEST_2 = "nest2";
    private static final String NEST_3 = "nest3";
    private static final String NEST_4 = "nest4";
    private static final String NEST_5 = "nest5";

    private static final String HALL = "Hall";
    private static final String MIRRORS = "Mirrors";

    private static final String GROUND = "ground";

    @Inject
    private EnvironmentUtils environmentUtils;

    @Before
    public void setup() {
        groupClient = new GroupClient(environmentData);
    }

    @After
    public void tearDown() {
        groupClient.close();
    }

    @Test
    public void membersAreAmalgamatedAcrossDirectories() {
        // Directories 1 and 2 have their members of the group amalgamated
        // including Anna who is in both directories
        final GroupBean groupBean = getGroup(KOALAS);
        assertThat(groupBean.getUsers().getSize(), is(3));
        assertThat("Calculated size matches Crowd", groupBean.getUsers().getItems(), hasSize(3));
    }

    @Test
    public void nestedGroupsAreObservedInUsersCanonicalDirectory() {
        // Anna, Betty and David are members of Sealife.
        // But Ernie is not, as Starfish is not nested within Sealife in his canonical directory (2)
        final GroupBean groupBean = getGroup(SEALIFE);
        assertThat(groupBean.getUsers().getSize(), is(3));
        assertThat("Calculated size matches Crowd", groupBean.getUsers().getItems(), hasSize(3));
    }

    /**
     * A user made inactive by shadowing is not counted.
     * This test also ensures that shadowing is determined by directory priority, not by directory ID.
     * (Directory 1 has a higher priority, but a 'lower' ID)
     */
    @Test
    public void inactiveShadowedUsersAreIgnored() {
        // John is inactive by shadowing in directory 1, so is not in Wombats.
        final GroupBean groupBean = getGroup(WOMBATS);
        assertThat(groupBean.getUsers().getSize(), is(2));
        assertThat("Calculated size matches Crowd", groupBean.getUsers().getItems(), hasSize(2));
    }

    @Test
    public void inactiveDirectoriesAreIgnored() {
        // Leonardo not a member of Lions because his directory (3) is inactive
        final GroupBean groupBean = getGroup(LIONS);
        assertThat(groupBean.getUsers().getSize(), is(1));
        assertThat("Calculated size matches Crowd", groupBean.getUsers().getItems(), hasSize(1));
    }

    @Test
    public void inactiveGroupStillHasMembers() {
        // If we ask for the members of an inactive group, Crowd does count its members
        final GroupBean groupBean = getGroup(WALLABIES);
        assertThat(groupBean.getUsers().getSize(), is(1));
        assertThat("Calculated size matches Crowd", groupBean.getUsers().getItems(), hasSize(1));
    }

    @Test
    public void groupsOnlyInLowerPriorityDirectoriesAreFound() {
        // Echidnas group is only defined in directory 2
        final GroupBean groupBean = getGroup(ECHIDNAS);
        assertThat(groupBean.getUsers().getSize(), is(1));
        assertThat("Calculated size matches Crowd", groupBean.getUsers().getItems(), hasSize(1));
    }

    @Test
    public void groupNameIsCaseInsensitive() {
        final GroupBean groupBean = getGroup(KOALAS.toUpperCase());
        assertThat(groupBean.getUsers().getSize(), is(3));
        assertThat("Calculated size matches Crowd", groupBean.getUsers().getItems(), hasSize(3));
    }

    @Test
    public void memberOfMultipleNestedGroupsIsCountedOnce() {
        // Bill appears three times in the nested groups 'ground' -> 'hog' -> 'day'
        // Andie appears once
        final GroupBean groupBean = getGroup(GROUND);
        assertThat(groupBean.getUsers().getSize(), is(2));
        assertThat("Calculated size matches Crowd", groupBean.getUsers().getItems(), hasSize(2));
    }

    @Test
    public void deeplyNestedGroupsAreCountedCorrectly() {
        final GroupBean nest1 = getGroup(NEST_1);
        final GroupBean nest2 = getGroup(NEST_2);
        final GroupBean nest3 = getGroup(NEST_3);
        final GroupBean nest4 = getGroup(NEST_4);
        final GroupBean nest5 = getGroup(NEST_5);
        assertThat(nest5.getUsers().getSize(), is(1));
        assertThat("Size matches crowd, nest5", nest5.getUsers().getItems(), hasSize(1));
        assertThat(nest4.getUsers().getSize(), is(2));
        assertThat("Size matches crowd, nest4", nest4.getUsers().getItems(), hasSize(2));
        assertThat(nest3.getUsers().getSize(), is(3));
        assertThat("Size matches crowd, nest3", nest3.getUsers().getItems(), hasSize(3));
        assertThat(nest2.getUsers().getSize(), is(4));
        assertThat("Size matches crowd, nest2", nest2.getUsers().getItems(), hasSize(4));
        assertThat(nest1.getUsers().getSize(), is(5));
        assertThat("Size matches crowd, nest1", nest1.getUsers().getItems(), hasSize(5));
    }

    @Test
    public void diamonds_memberOfNestedGroupByTwoRoutesIsCountedOnce() {
        final GroupBean bottom = getGroup(DIAMOND_BOTTOM);
        final GroupBean left = getGroup(DIAMOND_LEFT);
        final GroupBean right = getGroup(DIAMOND_RIGHT);
        final GroupBean top = getGroup(DIAMOND_TOP);
        assertThat(bottom.getUsers().getSize(), is(1));
        assertThat("Size matches crowd, bottom", bottom.getUsers().getItems(), hasSize(1));
        assertThat(left.getUsers().getSize(), is(2));
        assertThat("Size matches crowd, left", left.getUsers().getItems(), hasSize(2));
        assertThat(right.getUsers().getSize(), is(2));
        assertThat("Size matches crowd, right", right.getUsers().getItems(), hasSize(2));
        assertThat("User 'Yellow' is a only counted once even though it is a member via two routes", top.getUsers().getSize(), is(4));
        assertThat("Size matches crowd, top", top.getUsers().getItems(), hasSize(4));
    }

    @Test
    public void cycles_cyclicalGroupMembershipsAreCountedCorrectly() {
        // Group Hall contains group Mirrors which contains group Hall
        final GroupBean hall = getGroup(HALL);
        final GroupBean mirrors = getGroup(MIRRORS);
        assertThat(mirrors.getUsers().getSize(), is(2));
        assertThat("Size matches crowd, mirrors", mirrors.getUsers().getItems(), hasSize(2));
        assertThat(hall.getUsers().getSize(), is(2));
        assertThat("Size matches crowd, hall", hall.getUsers().getItems(), hasSize(2));
    }

    /**
     * Test a group nesting containing 2001 nested groups can be successfully counted.
     * On Oracle, this involves using a temporary table.
     * On SQL Server, this involves using literals in place of SQL parameters.
     */
    @Test
    public void countUsersIn2001NestedGroups() {
        // Do not run this test against H2 or HSQL, it takes too long to create this many groups
        Assume.assumeThat("Not running on H2 or HSQL as they are too slow for this test", isH2OrHsqlDatabase(), is(false));

        // Given a parent group with 2000 nested groups
        final UsersAndGroupsControl usersAndGroups = backdoor.usersAndGroups();
        String parentGroupName = "lotsOfNestedChildren";
        usersAndGroups.addGroup(parentGroupName);
        usersAndGroups.addGroups("nestedgroup", 2000, parentGroupName, 1);

        // Expect to find 2000 users (one for each child group)
        final GroupBean groupBean = getGroup(parentGroupName);
        assertThat(groupBean.getUsers().getSize(), is(2000));

        // A maximum of 50 users were returned in GroupBean, so we need to page to get all users from crowd
        assertThat("Size matches crowd for 2001 nested groups", getAllUsersInGroup(parentGroupName), hasSize(2000));
    }

    @Test
    public void getCountOfJiraUsersMembers() {
        String parentGroupName = "jira-users";
        final GroupBean groupBean = getGroup(parentGroupName);
        assertThat(groupBean.getUsers().getSize(), is(6));

        assertThat("jira-users group is correctly shadowed", getAllUsersInGroup(parentGroupName), hasSize(6));
    }

    private GroupBean getGroup(String groupName) {
        return groupClient.getGroupBean(groupName, EXPAND_ALL_USERS);
    }

    /**
     * Get all the users in the group, as evaluated by Crowd.
     * This handles paging when the number of uses exceeds the GroupResource maximum of 50.
     *
     * @param parentGroupName the parent group
     * @return all the usernames
     */
    private Collection<String> getAllUsersInGroup(final String parentGroupName) {
        List<String> users = new ArrayList<>();
        UsersPageBean page = groupClient.getPaginatedUsersForGroup(parentGroupName, false, 0L, 50L);
        page.getValues().forEach(user -> users.add(user.name));

        while (!page.getIsLast()) {
            page = groupClient.getNextPage(page);
            page.getValues().forEach(user -> users.add(user.name));
        }

        return users;
    }

    /**
     * @return true if the database is H2 or HSQL
     */
    private boolean isH2OrHsqlDatabase() {
        navigation.login(FunctTestConstants.ADMIN_USERNAME, FunctTestConstants.ADMIN_PASSWORD);
        return environmentUtils.isH2() || environmentUtils.isHsql();
    }
}
