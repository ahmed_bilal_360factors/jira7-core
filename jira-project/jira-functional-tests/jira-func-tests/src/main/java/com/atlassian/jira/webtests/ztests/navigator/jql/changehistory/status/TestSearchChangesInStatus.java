package com.atlassian.jira.webtests.ztests.navigator.jql.changehistory.status;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * Responsible for verifying that a user is able to query all issues that have been in a specific status.
 * <p>
 * Story @ JRADEV-3734
 *
 * @since v4.0
 */
@Restore("TestWasSearch.xml")
@WebTest({Category.FUNC_TEST, Category.JQL, Category.CHANGE_HISTORY})
public class TestSearchChangesInStatus extends BaseJiraFuncTest {

    @Inject
    private IssueTableAssertions issueTableAssertions;

    @Before
    public void setUpTest() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    @LoginAs(user = BOB_USERNAME)
    public void ReturnsAllIssuesThatWereInASpecificStatus() {
        final String[] openIssues = {"HSP-4", "HSP-3", "HSP-2", "HSP-1"};
        issueTableAssertions.assertSearchWithResults("status was Open", openIssues);
    }

    @Test
    @LoginAs(user = BOB_USERNAME)
    public void ReturnsAllIssuesThatWereNotEverInASpecificStatus() throws Exception {
        final String[] openIssues = {"HSP-4", "HSP-3", "HSP-2", "HSP-1"};
        issueTableAssertions.assertSearchWithResults("Status was not 'In Progress'", openIssues);
    }

    @Test
    @LoginAs(user = BOB_USERNAME)
    public void ReturnsAllIssuesThatWereInASetOfSpecificStatuses() throws Exception {
        final String[] openIssues = {"HSP-4", "HSP-3", "HSP-2", "HSP-1"};
        issueTableAssertions.assertSearchWithResults("status was in (Open,'In Progress')", openIssues);
    }

    @Test
    @LoginAs(user = BOB_USERNAME)
    public void ReturnsAllIssuesThatWereNotEverInASetOfSpecificStatuses() throws Exception {
        final String[] openIssues = {"HSP-4", "HSP-3", "HSP-2", "HSP-1"};
        issueTableAssertions.assertSearchWithResults("status was not in ('In Progress',Reopened,Closed)", openIssues);
    }

    @Test
    @LoginAs(user = BOB_USERNAME)
    public void ReturnsZeroIssuesForAStatusThatHasNeverBeenSet() throws Exception {
        issueTableAssertions.assertSearchWithResults("status was Closed");
    }

    @Test
    @LoginAs(user = BOB_USERNAME)
    public void IgnoresTheCaseOfTheStatusValue() throws Exception {
        issueTableAssertions.assertSearchWithResults("status was Open", "HSP-4", "HSP-3", "HSP-2", "HSP-1");
        issueTableAssertions.assertSearchWithResults("Status was open", "HSP-4", "HSP-3", "HSP-2", "HSP-1");
        issueTableAssertions.assertSearchWithResults("Status was oPEn", "HSP-4", "HSP-3", "HSP-2", "HSP-1");
    }

    @Test
    @LoginAs(user = BOB_USERNAME)
    public void ReturnsAnErrorForANonExistingStatus() {
        issueTableAssertions.assertSearchWithError("status was dingbat", "The value 'dingbat' does not exist for the field 'status'.");
    }

    @Test
    @LoginAs(user = FRED_USERNAME)
    public void TakesIntoAccountUpdatesToTheStatusOfAnIssue() {
        final String issueToBeUpdated = "HSP-1";
        final String[] openIssues = {"HSP-4", "HSP-3", "HSP-2", issueToBeUpdated};

        navigation.issue().closeIssue(issueToBeUpdated, "Fixed", "Fixed");

        issueTableAssertions.assertSearchWithResults("status was Open", openIssues);

        issueTableAssertions.assertSearchWithResults("status was Closed", issueToBeUpdated);
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void AddingANewStatusAllowsSearchingForIssuesInThatStatus() {

        tester.gotoPage("secure/admin/AddStatus!default.jspa");
        tester.setFormElement("name", "myStatus");
        tester.submit("Add");

        // No matching issues
        issueTableAssertions.assertSearchWithResults("status was myStatus");
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void StatusFieldRename() {
        navigation.issue().closeIssue("HSP-1", "Fixed", "Fixed");

        issueTableAssertions.assertSearchWithResults("status was Closed", "HSP-1");

        tester.gotoPage("secure/admin/EditStatus!default.jspa?id=6");
        tester.setFormElement("name", "Shut");
        tester.submit("Update");

        issueTableAssertions.assertSearchWithResults("Status was Closed", "HSP-1");
        issueTableAssertions.assertSearchWithResults("Status was shut", "HSP-1");
    }
}