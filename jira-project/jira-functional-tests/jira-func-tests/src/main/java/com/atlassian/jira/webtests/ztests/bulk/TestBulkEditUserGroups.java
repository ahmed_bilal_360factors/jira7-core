package com.atlassian.jira.webtests.ztests.bulk;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.backdoor.GroupManagerControl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMINISTER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SYSTEM_ADMINISTER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SYS_ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SYS_ADMIN_USERNAME;
import static com.atlassian.jira.webtests.ztests.user.TestGroupResourceCountUsers.GROUP_RESOURCE_COUNT_USERS_XML;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.BULK_OPERATIONS, Category.USERS_AND_GROUPS})
@Restore("TestBulkEditGroupMembers.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestBulkEditUserGroups extends BaseJiraFuncTest {
    private static final String BULK_EDIT_GROUP_MEMBERS_ERRORS_CONTAINER_LOCATOR = ".aui-message.error";
    /**
     * initial counts on the number of users per group according to 'TestBulkEditGroupMembers.xml'
     * there are admin, dev and user as 3 distinct users.
     * And an additional 401 users for testing the limits of how many users to display per group
     */
    private static final String ERROR_LEAVING_ALL_ADMIN_GROUPS = "You are trying to leave all of the administration groups jira-administrators. You cannot delete your own administration permission";

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testBulkEditUserGroupsWithNoSysAdminPermRemoveLastAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
            tester.clickLink("edit_members_of_jira-administrators");

            tester.selectOption("usersToUnassign", ADMIN_USERNAME);
            tester.submit("unassign");

            tester.assertTextPresent(ERROR_LEAVING_ALL_ADMIN_GROUPS);
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testBulkEditUserGroupsWithNoSysAdminPermRemoveAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            backdoor.usersAndGroups().addGroup("admin-group2");
            administration.addGlobalPermission(ADMINISTER, "admin-group2");
            backdoor.usersAndGroups().addUserToGroup(ADMIN_USERNAME, "admin-group2");

            navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
            tester.clickLink("edit_members_of_jira-administrators");

            tester.selectOption("usersToUnassign", ADMIN_USERNAME);
            tester.submit("unassign");

            tester.clickLink("view_profile");

            tester.assertTextNotPresent("jira-administrators");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testBulkEditUserGroupsInvalidGroups() {
        tester.gotoPage(page.addXsrfToken("/secure/admin/user/BulkEditUserGroups.jspa?selectedGroupsStr=invalid&assign=true&usersToAssignStr=admin"));
        textAssertions.assertTextPresent(locator.css(BULK_EDIT_GROUP_MEMBERS_ERRORS_CONTAINER_LOCATOR), "The group 'invalid' is not a valid group.");
        tester.gotoPage(page.addXsrfToken("/secure/admin/user/BulkEditUserGroups.jspa?selectedGroupsStr=invalid&unassign=true&usersToUnassign=admin"));
        textAssertions.assertTextPresent(locator.css(BULK_EDIT_GROUP_MEMBERS_ERRORS_CONTAINER_LOCATOR), "The group 'invalid' is not a valid group.");
    }

    @Test
    public void testFredEditGroups() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.login(FRED_USERNAME, FRED_PASSWORD);
            // Try to hack the url to add a user to the group that is not present
            tester.gotoPage(page.addXsrfToken("/secure/admin/user/BulkEditUserGroups.jspa?selectedGroupsStr=jira-sys-admins&assign=true&usersToAssignStr=fred"));
            tester.assertTextPresent("my login on this computer");

            // Try to hack the url to remove a user from the group that is not present
            tester.gotoPage(page.addXsrfToken("/secure/admin/user/BulkEditUserGroups.jspa?selectedGroupsStr=jira-sys-admins&unassign=true&usersToUnassign=root"));
            tester.assertTextPresent("my login on this computer");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }

    }

    @Test
    public void testBulkEditUserGroupsHappyPathSysAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            backdoor.usersAndGroups().addGroup("sys-admin-group2");
            administration.addGlobalPermission(SYSTEM_ADMINISTER, "sys-admin-group2");
            backdoor.usersAndGroups().addUserToGroup(SYS_ADMIN_USERNAME, "sys-admin-group2");

            navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
            tester.clickLink("edit_members_of_jira-sys-admins");

            tester.selectOption("usersToUnassign", SYS_ADMIN_USERNAME);
            tester.submit("unassign");

            navigation.userProfile().gotoCurrentUserProfile();

            tester.assertTextNotPresent("jira-sys-admins");
            tester.assertTextPresent("sys-admin-group2");

            // Now go and rejoin the group just to prove it works
            navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
            tester.clickLink("edit_members_of_jira-sys-admins");

            tester.setFormElement("usersToAssignStr", SYS_ADMIN_USERNAME);
            tester.submit("assign");

            navigation.userProfile().gotoCurrentUserProfile();

            tester.assertTextPresent("jira-sys-admins");
            tester.assertTextPresent("sys-admin-group2");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }

    }

    @Test
    public void testBulkEditUserGroupsHappyPathAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            backdoor.usersAndGroups().addGroup("admin-group2");
            administration.addGlobalPermission(ADMINISTER, "admin-group2");
            backdoor.usersAndGroups().addUserToGroup(ADMIN_USERNAME, "admin-group2");

            navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
            tester.clickLink("edit_members_of_jira-administrators");

            tester.selectOption("usersToUnassign", ADMIN_USERNAME);
            tester.submit("unassign");

            navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
            tester.clickLink("editgroups_admin");

            // Should be able to leave admin-group2 and should not be able to leave jira-administrators
            // as we have just removed the user from that group
            tester.assertOptionsEqual("groupsToLeave", new String[]{"admin-group2", "jira-developers", "jira-users"});

            // Now go and rejoin the group just to prove it works
            navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
            tester.clickLink("edit_members_of_jira-administrators");

            tester.setFormElement("usersToAssignStr", ADMIN_USERNAME);
            tester.submit("assign");

            navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
            tester.clickLink("editgroups_admin");

            // Now you should be able to leave both jira-administrators and admin-group2
            tester.assertOptionsEqual("groupsToLeave", new String[]{"admin-group2", "jira-administrators", "jira-developers", "jira-users"});
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testGetMembersList() {
        runWithData(GROUP_RESOURCE_COUNT_USERS_XML, () ->
        {
            tester.gotoPage(page.addXsrfToken("/secure/admin/user/BulkEditUserGroups.jspa?selectedGroupsStr=jira-users"));
            tester.assertFormElementPresentWithLabel("6 Group member(s)");

            assertGroupMembers(asList("DiamondTop", "DiamondBottom"), "Yellow,DiamondBottom", "White,DiamondTop");
            assertGroupMembers(asList("DiamondBottom", "DiamondTop"), "Yellow,DiamondBottom", "White,DiamondTop");

            assertGroupMembers(singletonList("Wombats"), "Catherine,Wombats", "David,Wombats");
            assertGroupMembers(singletonList("Sealife"), "Anna,Sealife", "David,Sealife");
            assertGroupMembers(asList("Wombats", "Sealife"), "David", "Anna,Sealife", "Catherine,Wombats");
            assertGroupMembers(asList("Sealife", "Wombats"), "David", "Anna,Sealife", "Catherine,Wombats");
            assertGroupMembers(asList("Koalas", "Sealife", "Wombats"), "David", "Anna,Koalas", "Betty,Koalas", "Anna,Sealife", "Catherine,Wombats");
        });
    }

    private void assertGroupMembers(List<String> groupNames, String... members) {

        String url = "/secure/admin/user/BulkEditUserGroups.jspa?selectedGroupsStr=" + String.join("&selectedGroupsStr=", groupNames);
        tester.gotoPage(page.addXsrfToken(url));
        tester.assertOptionValuesEqual("usersToUnassign", members);
    }

    @Test
    public void testUserFiltering() {
        final GroupManagerControl groupManager = backdoor.groupManager();
        runWithData(GROUP_RESOURCE_COUNT_USERS_XML, () ->
        {
            assertThat(groupManager.filterUsersInAllGroupsDirect(asList("Bill", "Andie", "Troy"), asList("hog", "day")),
                    contains("Bill"));
            assertThat(groupManager.filterUsersInAllGroupsDirect(asList("Andie", "Troy"), asList("hog", "day")),
                    hasSize(0));
            assertThat(groupManager.filterUsersInAllGroupsDirect(asList("Bill", "Andie", "Troy"), singletonList("day")),
                    contains("Andie", "Bill"));
            assertThat(groupManager.filterUsersInAllGroupsDirect(asList("Bill", "Andie", "Troy"), asList("jira-users", "day")),
                    hasSize(0));
            assertThat(groupManager.filterUsersInAllGroupsDirect(emptyList(), asList("jira-users", "day")),
                    hasSize(0));
            assertThat(groupManager.filterUsersInAllGroupsDirect(asList("Bill", "Andie", "Troy"), emptyList()),
                    hasSize(0));
            assertThat(groupManager.filterUsersInAllGroupsDirect(asList("David", "Anna", "Betty", "Anna", "Catherine"), asList("Koalas", "Sealife", "Wombats")),
                    contains("David"));
        });
    }

    @Test
    public void testFindingMembers() {
        final GroupManagerControl groupManager = backdoor.groupManager();
        runWithData(GROUP_RESOURCE_COUNT_USERS_XML, () ->
        {
            assertThat(groupManager.getNamesOfDirectMembersOfGroups(emptyList(), 100),
                    hasSize(0));
            assertThat(groupManager.getNamesOfDirectMembersOfGroups(singletonList("DiamondTop"), 100),
                    contains("White"));
            assertThat(groupManager.getNamesOfDirectMembersOfGroups(singletonList("DiamondBottom"), 100),
                    contains("Yellow"));
            assertThat(groupManager.getNamesOfDirectMembersOfGroups(asList("DiamondTop", "DiamondBottom"), 100),
                    contains("White", "Yellow"));
            assertThat(groupManager.getNamesOfDirectMembersOfGroups(asList("Sealife", "Koalas"), 100),
                    contains("Anna", "Betty", "David"));
            assertThat(groupManager.getNamesOfDirectMembersOfGroups(singletonList("jira-users"), 1),
                    contains("admin"));
            assertThat(groupManager.getNamesOfDirectMembersOfGroups(singletonList("jira-users"), 4),
                    contains("Anna", "Betty", "Catherine", "admin"));
            assertThat(groupManager.getNamesOfDirectMembersOfGroups(singletonList("jira-users"), 100),
                    contains("Anna", "Betty", "Catherine", "Leopold", "admin", "fred"));
            assertThat(groupManager.getNamesOfDirectMembersOfGroups(asList("Koalas", "Sealife", "Wombats"), 100),
                    contains("Anna", "Betty", "Catherine", "David"));
        });
    }

    private void runWithData(String filename, Runnable runnable) {
        try {
            administration.restoreData(filename);
            runnable.run();
        } finally {
            administration.restoreBlankInstance();
        }
    }
}
