package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.vcache.PutPolicy;

public class VCacheControl extends BackdoorControl<VCacheControl> {
    public VCacheControl(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public boolean putValueToDirectCache(String cacheName, String key, PutPolicy putPolicy, String value) {
        final String result = createResource()
                .path("vcache/direct")
                .path(cacheName)
                .path(key)
                .queryParam("putPolicy", putPolicy.name())
                .post(String.class, value);
        return Boolean.parseBoolean(result);
    }

    public String getValueFromDirectCache(String cacheName, String key) {
        return createResource()
                .path("vcache/direct")
                .path(cacheName)
                .path(key)
                .get(String.class);
    }

    public String getValueFromRequestCache(String cacheName, String key) {
        return createResource()
                .path("vcache/request")
                .path(cacheName)
                .path(key)
                .get(String.class);
    }

    public void putValueToRequestCache(String cacheName, String key, String value) {
        createResource()
                .path("vcache/request")
                .path(cacheName)
                .path(key)
                .post(String.class, value);
    }

}
