package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.i18n.TestI18n500Page;
import com.atlassian.jira.webtests.ztests.i18n.TestResourceBundleCache;
import com.atlassian.jira.webtests.ztests.i18n.TestTranslateSubTasks;
import com.atlassian.jira.webtests.ztests.timetracking.legacy.TestTimeTrackingLocalization;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * I18n tests
 *
 * @since v4.0
 */
public class FuncTestSuiteI18n {


    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestI18n500Page.class)
                .add(TestTranslateSubTasks.class)
                .add(TestTimeTrackingLocalization.class)
                .add(TestResourceBundleCache.class)
                .build();
    }
}