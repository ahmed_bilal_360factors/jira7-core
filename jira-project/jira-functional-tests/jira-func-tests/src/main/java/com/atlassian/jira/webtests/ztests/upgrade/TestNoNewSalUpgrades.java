package com.atlassian.jira.webtests.ztests.upgrade;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.backdoor.upgrade.PluginUpgradeDetailsJsonBean;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.sun.jersey.api.client.UniformInterfaceException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Function;

import static java.lang.String.format;
import static java.util.Arrays.stream;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

@WebTest({Category.FUNC_TEST, Category.UPGRADE_TASKS})
public class TestNoNewSalUpgrades extends BaseJiraFuncTest {

    enum BuildNumber {
        AO(PluginUpgradeDetailsJsonBean::getAoBuildNumber),
        SAL(PluginUpgradeDetailsJsonBean::getSalBuildNumber);

        private final Function<PluginUpgradeDetailsJsonBean, Integer> buildNumberFunction;

        BuildNumber(Function<PluginUpgradeDetailsJsonBean, Integer> buildNumberFunction) {
            this.buildNumberFunction = buildNumberFunction;
        }

        String propertyName(String pluginKey) {
            return pluginKey + "." + name();
        }
    }

    /**
     * Prints plugin-build-numbers.properties representing the currently installed plugins to the console.
     */
    @Test
    public void generatePropertiesFile() throws IOException {
        backdoor.restoreBlankInstance();

        final List<String> installedPlugins = backdoor.plugins().listPlugins();

        System.out.println("========================================================");
        System.out.println("  Printing contents of plugin-build-numbers.properties");
        System.out.println("========================================================");
        System.out.println();

        final Properties properties = new Properties();
        for (String plugin : installedPlugins) {
            final Optional<PluginUpgradeDetailsJsonBean> maybeDetails = getPluginUpgradeDetails(plugin);
            maybeDetails.ifPresent(details ->
                stream(BuildNumber.values()).forEach(number -> {
                    final Integer buildNumber = number.buildNumberFunction.apply(details);
                    if (buildNumber > 0) {
                        properties.setProperty(number.propertyName(plugin), String.valueOf(buildNumber));
                    }
                })
            );
        }

        properties.store(System.out, "");
        System.out.println();
    }


    @Test
    public void verifyThatNoNewSalOrAoUpgradesArePresent() throws IOException {
        backdoor.restoreBlankInstance();

        final Properties properties = new Properties();
        properties.load(getClass().getResource("plugin-build-numbers.properties").openStream());

        final List<String> installedPlugins = backdoor.plugins().listPlugins();

        for (String plugin : installedPlugins) {
            final Optional<PluginUpgradeDetailsJsonBean> maybeDetails = getPluginUpgradeDetails(plugin);
            maybeDetails.ifPresent(details ->
                stream(BuildNumber.values()).forEach(number -> {
                    final String buildNumber = properties.getProperty(number.propertyName(plugin));
                    final Integer currentBuildNumber = number.buildNumberFunction.apply(details);
                    if (buildNumber == null) {
                        assertThat(format("Non-whitelisted plugin \"%1s\" has %2s upgrade tasks", plugin, number.name()), currentBuildNumber, is(0));
                    } else {
                        assertThat(format("Whitelisted plugin \"%1s\" added %2s upgrade tasks", plugin, number.name()), currentBuildNumber, lessThanOrEqualTo(Integer.valueOf(buildNumber)));
                    }
                })
            );
        }

    }

    private Optional<PluginUpgradeDetailsJsonBean> getPluginUpgradeDetails(final String installedPlugin) {
        try {
            return Optional.of(backdoor.upgradeControl().getPluginDetails(installedPlugin));
        } catch (UniformInterfaceException e) {
            final Document entity = Jsoup.parse(e.getResponse().getEntity(String.class), "", Parser.xmlParser());
            if (!entity.getElementsByTag("message").text().contains("must be an OsgiPlugin")) {
                throw new RuntimeException("Could not get plugin details", e);
            }
        }
        return Optional.empty();
    }
}
