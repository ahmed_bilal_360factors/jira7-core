package com.atlassian.jira.util;

import net.sourceforge.jwebunit.WebTester;

public class ProgressPageControl {
    /**
     * @deprecated since 7.2. Use {@link #wait(WebTester, String, String, String)} instead.
     */
    public static void wait(final WebTester tester, final String formName, final String reloadButton) {
        waitAndReload(tester, formName, reloadButton, null, "Acknowledge");
    }

    public static void wait(final WebTester tester, final String formName, final String reloadButton,
                            final String submitButton) {
        waitAndReload(tester, formName, reloadButton, null, submitButton);
    }

    public static void waitAndReload(final WebTester tester, final String formName, final String reloadButton,
                                     final String confirmButton) {
        waitAndReload(tester, formName, reloadButton, confirmButton, confirmButton);
    }

    private static void waitAndReload(final WebTester tester, final String formName, final String reloadButton,
                                     final String confirmButton, final String submitButton) {
        final int maxWaitTimeMs = 2 * 60 * 1000;
        final int snoozeMs = 100;
        int timeWaitedMs = 0;
        while (true) {
            if (formName != null) {
                tester.setWorkingForm(formName);
            }
            if (tester.getDialog().hasSubmitButton(submitButton)) {
                break;
            }
            if (timeWaitedMs > maxWaitTimeMs) {
                throw new RuntimeException("Waiting timed out.");
            }
            try {
                Thread.sleep(snoozeMs);
                timeWaitedMs += snoozeMs;
                tester.submit(reloadButton);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        if (confirmButton != null) {
            tester.submit(confirmButton);
        }
    }
}
