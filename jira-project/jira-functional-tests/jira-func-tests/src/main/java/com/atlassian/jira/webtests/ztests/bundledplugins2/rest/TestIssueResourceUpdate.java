package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.framework.util.ResponseMatchers;
import com.atlassian.jira.issue.changehistory.metadata.HistoryMetadata;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.api.issue.FieldOperation;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.IssueUpdateRequest;
import com.atlassian.jira.rest.api.issue.TimeTracking;
import com.atlassian.jira.testkit.client.restclient.EntityProperty;
import com.atlassian.jira.testkit.client.restclient.EntityPropertyClient;
import com.atlassian.jira.testkit.client.restclient.EntityPropertyKeys;
import com.atlassian.jira.testkit.client.restclient.Group;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_DEV_GROUP;
import static com.atlassian.jira.functest.framework.util.ResponseMatchers.hasStatusCode;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.atlassian.jira.rest.api.issue.ResourceRef.withId;
import static com.atlassian.jira.rest.api.issue.ResourceRef.withName;
import static com.atlassian.jira.rest.api.issue.ResourceRef.withRubbish;
import static com.google.common.collect.Sets.newHashSet;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isIn;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueResourceUpdate extends BaseJiraFuncTest {

    private IssueClient issueClient;

    @Inject
    private Administration administration;

    @Before
    public void setUp() {
        issueClient = new IssueClient(getEnvironmentData());
    }

    @Test
    @Restore("TestCreateIssueWithRequiredSystemFields.xml")
    public void testEditAllSystemFields() throws Exception {
        Issue original = issueClient.get("TST-1");

        IssueUpdateRequest updateSummaryRequest = new IssueUpdateRequest().fields(new IssueFields()
                .summary("issue that i'm about to edit")
        );

        // first edit only the summary, to make sure the REST api will reuse fields that aren't provided
        issueClient.update(original.id, updateSummaryRequest);
        assertThat(issueClient.get(original.key).fields.summary, equalTo(updateSummaryRequest.fields().summary()));

        // then try to edit everything else.
        IssueUpdateRequest updateRequest = new IssueUpdateRequest().fields(new IssueFields()
                .priority(withId("2"))
                .reporter(withName("fry"))
                .assignee(withName("farnsworth"))
                .labels(Arrays.asList("foo", "bar"))
                .timeTracking(new TimeTracking("20m", "20m"))
                .securityLevel(withId("10001"))
                .versions(withId("10001"))
                .environment("edited environment")
                .description("edited description")
                .dueDate("2012-03-01")
                .fixVersions(withId("10001"))
                .components(withId("10001"))
                .resolution(withId("2"))
        );
        issueClient.update(original.id, updateRequest);

        Issue updated = issueClient.get(original.key);
        assertThat(updated.self, equalTo(original.self));

        assertThat(updated.fields.priority.id(), not(equalTo(original.fields.priority.id())));
        assertThat(updated.fields.priority.id(), equalTo(updateRequest.fields().priority().id()));

        assertThat(updated.fields.reporter.name, equalTo(updateRequest.fields().reporter().name()));

        assertThat(updated.fields.assignee.name, equalTo(updateRequest.fields().assignee().name()));

        assertThat(newHashSet(updated.fields.labels), not(equalTo(newHashSet(original.fields.labels))));
        assertThat(newHashSet(updated.fields.labels), equalTo(newHashSet(updateRequest.fields().labels())));

        assertThat(updated.fields.timetracking, not(equalTo(original.fields.timetracking)));
        assertThat(updated.fields.timetracking.originalEstimate, equalTo(updateRequest.fields().timeTracking().originalEstimate));
        assertThat(updated.fields.timetracking.remainingEstimate, equalTo(updateRequest.fields().timeTracking().remainingEstimate));

        assertThat(updated.fields.security, not(equalTo(original.fields.security)));
        assertThat(updated.fields.security.name, equalTo("lvl2"));

        assertThat(updated.fields.versions, not(equalTo(original.fields.versions)));
        assertThat(updated.fields.versions.size(), equalTo(1));
        assertThat(updated.fields.versions.get(0).name, equalTo("v2"));

        assertThat(updated.fields.environment, not(equalTo(original.fields.environment)));
        assertThat(updated.fields.environment, equalTo(updateRequest.fields().environment()));

        assertThat(updated.fields.description, not(equalTo(original.fields.description)));
        assertThat(updated.fields.description, equalTo(updateRequest.fields().description()));

        assertThat(updated.fields.duedate, not(equalTo(original.fields.duedate)));
        assertThat(updated.fields.duedate, equalTo(updateRequest.fields().dueDate()));

        assertThat(updated.fields.fixVersions, not(equalTo(original.fields.fixVersions)));
        assertThat(updated.fields.fixVersions.size(), equalTo(1));
        assertThat(updated.fields.fixVersions.get(0).name, equalTo("v2"));

        assertThat(updated.fields.components, not(equalTo(original.fields.components)));
        assertThat(updated.fields.components.size(), equalTo(1));
        assertThat(updated.fields.components.get(0).name, equalTo("comp2"));

        assertThat(updated.fields.resolution.id, not(equalTo(original.fields.resolution.id)));
        assertThat(updated.fields.resolution.id, equalTo(updateRequest.fields().resolution().id()));
    }

    // TODO This needs to be updated as more fields support key/name inplace of IDs
    @Test
    @Restore("TestCreateIssueWithRequiredSystemFields.xml")
    public void testEditSystemFieldsByName() throws Exception {
        Issue original = issueClient.get("TST-1");

        IssueUpdateRequest updateSummaryRequest = new IssueUpdateRequest().fields(new IssueFields()
                .summary("issue that i'm about to edit")
        );

        // first edit only the summary, to make sure the REST api will reuse fields that aren't provided
        issueClient.update(original.id, updateSummaryRequest);
        assertThat(issueClient.get(original.key).fields.summary, equalTo(updateSummaryRequest.fields().summary()));

        // then try to edit everything else.
        IssueUpdateRequest updateRequest = new IssueUpdateRequest().fields(new IssueFields()
                .securityLevel(withName("lvl2"))
                .priority(withName("Critical"))
                .versions(withName("v2"))
                .fixVersions(withName("v2"))
                .components(withName("comp2"))
                .resolution(withName("Duplicate"))
        );
        issueClient.update(original.id, updateRequest);

        Issue updated = issueClient.get(original.key);
        assertThat(updated.self, equalTo(original.self));

        assertThat(updated.fields.security, not(equalTo(original.fields.security)));
        assertThat(updated.fields.security.name, equalTo("lvl2"));

        assertThat(updated.fields.priority.id(), not(equalTo(original.fields.priority.id())));
        assertThat(updated.fields.priority.id(), equalTo("2"));

        assertThat(updated.fields.versions, not(equalTo(original.fields.versions)));
        assertThat(updated.fields.versions.size(), equalTo(1));
        assertThat(updated.fields.versions.get(0).name, equalTo("v2"));

        assertThat(updated.fields.fixVersions, not(equalTo(original.fields.fixVersions)));
        assertThat(updated.fields.fixVersions.size(), equalTo(1));
        assertThat(updated.fields.fixVersions.get(0).name, equalTo("v2"));

        assertThat(updated.fields.components, not(equalTo(original.fields.components)));
        assertThat(updated.fields.components.size(), equalTo(1));
        assertThat(updated.fields.components.get(0).name, equalTo("comp2"));

        assertThat(updated.fields.resolution.id, not(equalTo(original.fields.resolution.id)));
        assertThat(updated.fields.resolution.id, equalTo("3"));
    }

    // TODO This needs to be updated as more fields support key/name inplace of IDs
    @Test
    @Restore("TestCreateIssueWithRequiredSystemFields.xml")
    public void testEditSystemFieldsByNameAndId() throws Exception {
        Issue original = issueClient.get("TST-1");

        IssueUpdateRequest updateSummaryRequest = new IssueUpdateRequest().fields(new IssueFields()
                .summary("issue that i'm about to edit")
        );

        // first edit only the summary, to make sure the REST api will reuse fields that aren't provided
        issueClient.update(original.id, updateSummaryRequest);
        assertThat(issueClient.get(original.key).fields.summary, equalTo(updateSummaryRequest.fields().summary()));

        // then try to edit everything else.
        IssueUpdateRequest updateRequest = new IssueUpdateRequest().fields(new IssueFields()
                .priority(withName("Critical"))
                .versions(withName("v1"), withName("v2"))
                .fixVersions(withId("10000"), withName("v2"))
                .components(withId("10000"), withName("comp2"))
        );
        issueClient.update(original.id, updateRequest);

        Issue updated = issueClient.get(original.key);
        assertThat(updated.self, equalTo(original.self));

        assertThat(updated.fields.priority.id(), not(equalTo(original.fields.priority.id())));
        assertThat(updated.fields.priority.id(), equalTo("2"));

        assertThat(updated.fields.versions, not(equalTo(original.fields.versions)));
        assertThat(updated.fields.versions.size(), equalTo(2));
        assertThat(updated.fields.versions.get(0).name, equalTo("v1"));
        assertThat(updated.fields.versions.get(1).name, equalTo("v2"));

        assertThat(updated.fields.fixVersions, not(equalTo(original.fields.fixVersions)));
        assertThat(updated.fields.fixVersions.size(), equalTo(2));
        assertThat(updated.fields.fixVersions.get(0).name, equalTo("v1"));
        assertThat(updated.fields.fixVersions.get(1).name, equalTo("v2"));

        assertThat(updated.fields.components, not(equalTo(original.fields.components)));
        assertThat(updated.fields.components.size(), equalTo(2));
        assertThat(updated.fields.components.get(0).name, equalTo("comp1"));
        assertThat(updated.fields.components.get(1).name, equalTo("comp2"));
    }

    // TODO This needs to be updated as more fields support key/name inplace of IDs
    @Test
    @Restore("TestCreateIssueWithRequiredSystemFields.xml")
    public void testEditSystemFieldsInvalidData() throws Exception {
        Issue original = issueClient.get("TST-1");

        IssueUpdateRequest updateSummaryRequest = new IssueUpdateRequest().fields(new IssueFields()
                .summary("issue that i'm about to edit")
        );

        // first edit only the summary, to make sure the REST api will reuse fields that aren't provided
        issueClient.update(original.id, updateSummaryRequest);
        assertThat(issueClient.get(original.key).fields.summary, equalTo(updateSummaryRequest.fields().summary()));

        // then try to edit everything else.
        IssueUpdateRequest updateRequest = new IssueUpdateRequest().fields(new IssueFields()
                .priority(withName("BadPriority"))
                .versions(withName("v1Bad"))
                .fixVersions(withName("v2Bad"))
                .components(withName("comp1Bad"))
                .resolution(withName("BadResolution"))
                .dueDate("2001-01-XV")
        );

        Response response = issueClient.updateResponse(original.id, updateRequest);

        // Expecting Bad Request error
        assertEquals(400, response.statusCode);
        assertEquals("Priority name 'BadPriority' is not valid", response.entity.errors.get("priority"));
        assertEquals("Resolution name 'BadResolution' is not valid", response.entity.errors.get("resolution"));
        assertEquals("Component name 'comp1Bad' is not valid", response.entity.errors.get("components"));
        assertEquals("Version name 'v1Bad' is not valid", response.entity.errors.get("versions"));
        assertEquals("Version name 'v2Bad' is not valid", response.entity.errors.get("fixVersions"));
        assertEquals("Error parsing date string: 2001-01-XV", response.entity.errors.get("duedate"));

        // Missing name & id.
        updateRequest = new IssueUpdateRequest().fields(new IssueFields()
                .priority(withRubbish("BadPriority"))
        );

        response = issueClient.updateResponse(original.id, updateRequest);

        // Expecting Bad Request error
        assertEquals(400, response.statusCode);
        assertEquals("Could not find valid 'id' or 'name' in priority object.", response.entity.errors.get("priority"));

        // Missing name & id.
        updateRequest = new IssueUpdateRequest().fields(new IssueFields()
                .resolution(withRubbish("badResolution"))
        );

        response = issueClient.updateResponse(original.id, updateRequest);

        // Expecting Bad Request error
        assertEquals(400, response.statusCode);
        assertEquals("Could not find valid 'id' or 'name' in resolution object.", response.entity.errors.get("resolution"));

        // Missing name & id.
        updateRequest = new IssueUpdateRequest().fields(new IssueFields()
                .versions(withRubbish("v1Bad"))
                .fixVersions(withRubbish("v2Bad"))
                .components(withRubbish("comp1Bad"))
        );

        response = issueClient.updateResponse(original.id, updateRequest);

        // Expecting Bad Request error
        assertEquals(400, response.statusCode);
        assertEquals("Component/s is required.", response.entity.errors.get("components"));
        assertEquals("Affects Version/s is required.", response.entity.errors.get("versions"));
        assertEquals("Fix Version/s is required.", response.entity.errors.get("fixVersions"));

        // can't accept Blank ID
        updateRequest = new IssueUpdateRequest().fields(new IssueFields()
                .resolution(withId(""))
        );

        response = issueClient.updateResponse(original.id, updateRequest);

        // Expecting Bad Request error
        assertEquals(400, response.statusCode);
        assertEquals("Could not find valid 'id' or 'name' in resolution object.", response.entity.errors.get("resolution"));
    }

    @Test
    @Restore("TestCreateIssueWithRequiredSystemFields.xml")
    public void testEditSystemFieldsSetNull() throws Exception {
        Issue original = issueClient.get("TST-1");

        IssueUpdateRequest updateSummaryRequest = new IssueUpdateRequest().fields(new IssueFields()
                .summary("issue that i'm about to edit")
        );

        // first edit only the summary, to make sure the REST api will reuse fields that aren't provided
        issueClient.update(original.id, updateSummaryRequest);
        assertThat(issueClient.get(original.key).fields.summary, equalTo(updateSummaryRequest.fields().summary()));

        // Our Client dosen't do nulls, so we just make a little map.
        Map<String, Map<String, String>> updateRequest = new HashMap<>();
        Map<String, String> fields = new HashMap<>();
        fields.put("security", null);
        fields.put("duedate", null);
        updateRequest.put("fields", fields);

        Response response = issueClient.update(original.id, updateRequest);
        assertEquals(400, response.statusCode);
        assertEquals("Security Level is required.", response.entity.errors.get("security"));
        assertEquals("Due Date is required.", response.entity.errors.get("duedate"));

        // Make fields optional then test again
        administration.fieldConfigurations().defaultFieldConfiguration().optionalField("Security Level");
        administration.fieldConfigurations().defaultFieldConfiguration().optionalField("Due Date");

        issueClient.update(original.id, updateRequest);

        Issue updated = issueClient.get(original.key);
        assertThat(updated.self, equalTo(original.self));

        assertThat(updated.fields.security, not(equalTo(original.fields.security)));
        assertThat(updated.fields.security, equalTo(null));
        assertThat(updated.fields.duedate, not(equalTo(original.fields.duedate)));
        assertThat(updated.fields.duedate, equalTo(null));
    }

    @Test
    @Restore("TestCreateIssueWithRequiredSystemFields.xml")
    public void testEditGroupCustomFields() throws Exception {
        final String TST_1 = "TST-1";
        final Group jira_developers = new Group().name("jira-developers").self(URI.create(environmentData.getBaseUrl() + "/rest/api/2/group?groupname=jira-developers"));
        final Group jira_users = new Group().name("jira-users").self(URI.create(environmentData.getBaseUrl() + "/rest/api/2/group?groupname=jira-users"));

        // test SET for group picker
        String picker = administration.customFields().addCustomField("com.atlassian.jira.plugin.system.customfieldtypes:grouppicker", "single group");
        {
            issueClient.edit(TST_1, new IssueUpdateRequest().update(picker, new FieldOperation("set", jira_developers)));

            Group group = issueClient.get(TST_1).fields.get(picker, Group.class);
            assertThat(group, equalTo(jira_developers));
        }

        // test SET for multi group picker
        String multiPicker = administration.customFields().addCustomField("com.atlassian.jira.plugin.system.customfieldtypes:multigrouppicker", "many groups");
        {
            issueClient.edit(TST_1, new IssueUpdateRequest().update(multiPicker, new FieldOperation("set", Arrays.asList(jira_users, jira_developers))));

            List<Group> groups = issueClient.get(TST_1).fields.get(multiPicker, new TypeReference<List<Group>>() {
            });
            assertThat(groups.size(), equalTo(2));
            assertThat(groups, hasItems(jira_users, jira_developers));
        }

        // test REMOVE for multi group picker
        {
            issueClient.edit(TST_1, new IssueUpdateRequest().update(multiPicker, new FieldOperation("remove", jira_users)));

            List<Group> groups = issueClient.get(TST_1).fields.get(multiPicker, new TypeReference<List<Group>>() {
            });
            assertThat(groups, equalTo(Collections.singletonList(jira_developers)));
        }

        // test ADD for multi group picker
        {
            issueClient.edit(TST_1, new IssueUpdateRequest().update(multiPicker, new FieldOperation("add", jira_users)));

            List<Group> groups = issueClient.get(TST_1).fields.get(multiPicker, new TypeReference<List<Group>>() {
            });
            assertThat(groups.size(), equalTo(2));
            assertThat(groups, hasItems(jira_users, jira_developers));
        }
    }

    @Test
    @RestoreBlankInstance
    public void testEditWithMetadata() throws Exception {
        // having
        backdoor.project().addProject("UPDATE", "UPDATE", ADMIN_USERNAME);
        final IssueCreateResponse issue = backdoor.issues().createIssue("UPDATE", "summary");

        // when
        issueClient.edit(issue.key(), new IssueUpdateRequest()
                .fields(new IssueFields().summary("newmmary"))
                .historyMetadata(
                        HistoryMetadata.builder("updateMetadataTest").build()
                ));
        final JsonNode metadata = backdoor.issueNavControl().getHistoryMetadata(issue.key).get(0);

        // then
        assertThat(metadata.get("type").asText(), equalTo("updateMetadataTest"));
    }

    @Test
    @Restore("TestCreateIssueWithRequiredSystemFields.xml")
    public void testEditAndSetIssueProperties() throws Exception {
        ImmutableMap<String, Object> obj1 = ImmutableMap.of("value", "value1");
        ImmutableMap<String, Object> obj2 = ImmutableMap.of("other", "value2");
        List<IssueUpdateRequest.IssueProperty> properties = ImmutableList.of(
                new IssueUpdateRequest.IssueProperty("ipObj1", new com.atlassian.jira.util.json.JSONObject(obj1).toString()),
                new IssueUpdateRequest.IssueProperty("ipObj2", new com.atlassian.jira.util.json.JSONObject(obj2).toString())
        );

        Issue original = issueClient.get("TST-1");

        IssueUpdateRequest updateRequest = new IssueUpdateRequest()
                .fields(new IssueFields().summary("issue that i'm about to edit"))
                .properties(properties);

        issueClient.update(original.id, updateRequest);

        //
        // use the entity properties API to check the properties got set
        //
        EntityPropertyClient propertyClient = new EntityPropertyClient(environmentData, "issue");

        EntityPropertyKeys propertyKeys = propertyClient.getKeys(original.key);
        Set<String> keyNames = propertyKeys.keys.stream().map(epk -> epk.key).collect(Collectors.toSet());

        assertThat(keyNames, is(ImmutableSet.of("ipObj1", "ipObj2")));

        final EntityProperty ip1 = propertyClient.get(original.key, "ipObj1");
        final EntityProperty ip2 = propertyClient.get(original.key, "ipObj2");

        assertThat(ip1.key, is("ipObj1"));
        assertThat(ip1.value.entrySet(), everyItem(isIn(obj1.entrySet())));

        assertThat(ip2.key, is("ipObj2"));
        assertThat(ip2.value.entrySet(), everyItem(isIn(obj2.entrySet())));
    }

    @Test
    @RestoreBlankInstance
    public void testEditWithoutNotificationDoesNotWorkForRegularUsers() throws Exception {
        backdoor.usersAndGroups().addUserToGroup(FRED_USERNAME, JIRA_DEV_GROUP);
        backdoor.project().addProject("Project", "PR", ADMIN_USERNAME);
        String issueKey = backdoor.issues().createIssue("PR", "summary").key;

        IssueUpdateRequest updateRequest = new IssueUpdateRequest().fields(new IssueFields().summary("new summary"));
        Response response = issueClient.loginAs(FRED_USERNAME).updateResponse(issueKey, updateRequest, false);

        assertThat(response,  hasStatusCode(FORBIDDEN));
        Issue issue = issueClient.get(issueKey);
        assertThat(issue.fields.summary, is("summary")); // check that issue was not changed
    }

    @Test
    @RestoreBlankInstance
    public void testEditWithoutNotificationWorksForAdminAndProjectAdmin() throws Exception {
        backdoor.permissionSchemes().addUserPermission(DEFAULT_PERM_SCHEME_ID, ADMINISTER_PROJECTS, FRED_USERNAME);
        backdoor.usersAndGroups().addUserToGroup(FRED_USERNAME, JIRA_DEV_GROUP);
        backdoor.project().addProject("Project", "PR", ADMIN_USERNAME);
        String issueKey = backdoor.issues().createIssue("PR", "summary").key;

        IssueUpdateRequest updateRequest = new IssueUpdateRequest().fields(new IssueFields().summary("new summary"));

        Response responseForAdmin = issueClient.loginAs(ADMIN_USERNAME).updateResponse(issueKey, updateRequest, false);
        assertThat(responseForAdmin,  hasStatusCode(NO_CONTENT));

        Response responseForProjectAdmin = issueClient.loginAs(FRED_USERNAME).updateResponse(issueKey, updateRequest, false);
        assertThat(responseForProjectAdmin,  hasStatusCode(NO_CONTENT));
    }
}
