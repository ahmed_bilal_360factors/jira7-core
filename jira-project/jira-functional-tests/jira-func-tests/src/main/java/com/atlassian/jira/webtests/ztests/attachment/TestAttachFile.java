package com.atlassian.jira.webtests.ztests.attachment;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.meterware.httpunit.HttpUnitOptions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

/**
 * Functional test for attaching files.
 */
@WebTest({Category.FUNC_TEST, Category.ATTACHMENTS})
@Restore("TestFullAnonymousPermissions.xml")
public class TestAttachFile extends BaseJiraFuncTest {
    private static final String LOGIN = "log in";
    private static final String SIGNUP = "sign up";

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        //add full anonymous permissions
        backdoor.attachments().enable();
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
    }

    @After
    public void tearDownTest() {
        //disable js in case tests fail without disabling it
        HttpUnitOptions.setScriptingEnabled(false);
    }

    /**
     * Tests that the attachment page is able to be accessed when logged in (using default permission scheme)
     */
    @Test
    public void testAttachFileLoggedIn() {
        disallowAnonymousAttachmentCreation();
        navigation.login("fred");
        navigation.issue().gotoIssue("MKY-2");
        tester.clickLink("attach-file");
        tester.assertElementPresent("attach-file-submit");
    }

    @Test
    public void testAttachNoFileError() {
        navigation.issue().gotoIssue("MKY-2");
        tester.clickLink("attach-file");
        textAssertions.assertTextNotPresent(locator.page(), "<h1>Errors</h1>");
        textAssertions.assertTextNotPresent(locator.page(), "Please indicate the file you wish to upload");
        tester.setWorkingForm("attach-file");
        tester.submit();
        textAssertions.assertTextPresent(locator.page(), "Please indicate the file you wish to upload");
    }

    /**
     * Tests that the attachment link is missing from the issue and the URL redirects to the security breach page if
     * anonymous access is disabled and the user is not logged in
     */
    @Test
    public void testAttachFileLoggedOutNoAnonymousPermission() {
        disallowAnonymousAttachmentCreation();
        navigation.logout();

        //attempt to create attachment via UI
        navigation.issue().gotoIssue("MKY-2");
        tester.assertLinkNotPresent("attach-file");

        //attempt to access URL directly (working id)
        tester.gotoPage("/secure/AttachFile!default.jspa?id=10011");
        textAssertions.assertTextPresent(locator.page(), "You do not have permission to create attachments for this issue.");
        textAssertions.assertTextPresent(locator.page(), "It seems that you have tried to perform an operation which you are not permitted to perform.");
        tester.assertLinkPresentWithText(LOGIN);
        tester.assertLinkPresentWithText(SIGNUP);
        tester.assertElementNotPresent("attach-file-submit");

        //attempt to access URL directly (non existent id)
        tester.gotoPage("/secure/AttachFile!default.jspa?id=99999");
        textAssertions.assertTextNotPresent(locator.page(), "You do not have permission to create attachments for this issue.");
        textAssertions.assertTextNotPresent(locator.page(), "It seems that you have tried to perform an operation which you are not permitted to perform.");
        tester.assertElementNotPresent("attach-file-submit");
    }

    /**
     * Tests that the user can attach files anonymously if the anonymous 'create attachment' permission is enabled
     */
    @Test
    public void testAttachFileLoggedOutWithAnonymousPermission() {
        navigation.logout();
        navigation.issue().gotoIssue("MKY-2");
        tester.clickLink("attach-file");
        tester.gotoPage("/secure/AttachFile!default.jspa?id=10011");
        tester.assertElementPresent("attach-file-submit");
        textAssertions.assertTextNotPresent(locator.page(), "You are not logged in, and do not have the permissions required to attach a file on the selected issue as a guest.");

        //attempt to access URL directly (non existant id)
        tester.gotoPage("/secure/AttachFile!default.jspa?id=99999");
        textAssertions.assertTextNotPresent(locator.page(), "You do not have permission to create attachments for this issue.");
        textAssertions.assertTextNotPresent(locator.page(), "You are not logged in, and do not have the permissions required to attach a file on the selected issue as a guest.");
        tester.assertElementNotPresent("attach-file-submit");
    }

    /**
     * Tests that a user who is logged in but does not have create attachment permission cannot attach files
     */
    @Test
    public void testAttachFileLoggedInWithoutPermission() {
        disallowAnyoneAttachmentCreation();

        //attempt to create attachment via UI
        navigation.issue().gotoIssue("MKY-2");
        tester.assertLinkNotPresent("attach-file");

        //attempt to access URL directly (working id)
        tester.gotoPage("/secure/AttachFile!default.jspa?id=10011");
        textAssertions.assertTextPresent(locator.page(), "You do not have permission to create attachments for this issue.");
        tester.assertElementNotPresent("attach-file-submit");

        //attempt to access URL directly (non existant id)
        tester.gotoPage("/secure/AttachFile!default.jspa?id=99999");
        textAssertions.assertTextNotPresent(locator.page(), "You do not have permission to create attachments for this issue.");
        textAssertions.assertTextNotPresent(locator.page(), "You are not logged in, and do not have the permissions required to attach a file on the selected issue as a guest.");
        tester.assertElementNotPresent("attach-file-submit");
    }

    /**
     * Remove anonymous permission for creating attachments
     */
    private void disallowAnonymousAttachmentCreation() {
        backdoor.getTestkit().permissionSchemes().removeEveryonePermission(0l, ProjectPermissions.CREATE_ATTACHMENTS);
    }

    private void disallowUsersAttachmentCreation() {
        backdoor.getTestkit().permissionSchemes().removeGroupPermission(0l, ProjectPermissions.CREATE_ATTACHMENTS, FunctTestConstants.JIRA_USERS_GROUP);
    }

    /**
     * Remove all permission for creating attachments
     */
    private void disallowAnyoneAttachmentCreation() {
        disallowAnonymousAttachmentCreation();
        disallowUsersAttachmentCreation();
    }
}
