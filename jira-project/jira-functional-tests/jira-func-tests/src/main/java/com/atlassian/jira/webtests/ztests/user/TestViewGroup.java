package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.admin.IssueSecurityLevel;
import com.atlassian.jira.functest.framework.admin.NotificationType;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;

/**
 * Test the view group page.
 *
 * @since v3.13
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS, Category.FILTERS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestViewGroup extends BaseJiraFuncTest {
    public static final int CREATE_SHARED_FILTER = 22;
    private static final String TEST_GROUP = "test_group";

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    /**
     * Ensure that shared group filters are reported.
     */
    @Test
    public void testGroupSharedFiltersReported() {
        gotoViewGroup("jira-users");

        //there should be no saved filters in this group now.
        assertions.assertNodeHasText(new CssLocator(tester, "table.aui td"), "There are no Saved Filters associated with this Group.");

        administration.addGlobalPermission(CREATE_SHARED_FILTER, "jira-users");
        long adminId = Long.parseLong(backdoor.filters().createFilter("", "AdministratorFilter", ADMIN_USERNAME, "jira-users"));
        long fredId = Long.parseLong(backdoor.filters().createFilter("", "FredFilter", FRED_USERNAME, "jira-users"));

        gotoViewGroup("jira-users");
        assertions.assertNodeHasText(new CssLocator(tester, "table.aui td"), "AdministratorFilter (Owner: " + ADMIN_FULLNAME + ")");
        assertions.assertNodeHasText(new CssLocator(tester, "table.aui td"), "FredFilter (Owner: " + FRED_FULLNAME + ")");

        navigation.manageFilters().deleteFilter((int) adminId);

        gotoViewGroup("jira-users");
        assertions.assertNodeDoesNotHaveText(new CssLocator(tester, "table.aui td"), "AdministratorFilter (Owner: " + ADMIN_FULLNAME + ")");
        assertions.assertNodeHasText(new CssLocator(tester, "table.aui td"), "FredFilter (Owner: " + FRED_FULLNAME + ")");

        navigation.logout();
        navigation.login(FRED_USERNAME);

        navigation.manageFilters().deleteFilter((int) fredId);

        navigation.logout();
        navigation.login(ADMIN_USERNAME);

        gotoViewGroup("jira-users");
        assertions.assertNodeDoesNotHaveText(new CssLocator(tester, "table.aui td"), "AdministratorFilter (Owner: " + ADMIN_FULLNAME + ")");
        assertions.assertNodeDoesNotHaveText(new CssLocator(tester, "table.aui td"), "FredFilter (Owner: " + FRED_FULLNAME + ")");

        assertions.assertNodeHasText(new CssLocator(tester, "table.aui td"), "There are no Saved Filters associated with this Group.");
    }

    /**
     * Test for JRA-15837
     */
    @Test
    public void testViewSchemes() {
        // add a test_group
        navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
        tester.setFormElement("addName", TEST_GROUP);
        tester.submit("add_group");
        tester.clickLinkWithText(TEST_GROUP);
        textAssertions.assertTextPresent(locator.page(), "There are no Permission Schemes associated with this Group.");
        textAssertions.assertTextPresent(locator.page(), "There are no Notification Schemes associated with this Group.");
        textAssertions.assertTextPresent(locator.page(), "There are no Saved Filters associated with this Group.");
        textAssertions.assertTextPresent(locator.page(), "There are no Issue Security Schemes associated with this Group.");

        // now add this group to various schemes and then check that its there

        // do permission schemes first
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ADMINISTER_PROJECTS, TEST_GROUP);

        // do notification schemes next
        administration.notificationSchemes().goTo().editNotifications(10000).addNotificationsForEvent(1, NotificationType.GROUP, TEST_GROUP);

        // do issue security next
        navigation.gotoAdminSection(Navigation.AdminSection.SECURITY_SCHEMES);
        // Click Link 'Add Issue Security Scheme' (id='add_securityscheme').
        administration.issueSecuritySchemes().newScheme("Test Issue Security Scheme", "").newLevel("Code Red", "").addIssueSecurity(IssueSecurityLevel.IssueSecurity.GROUP, TEST_GROUP);

        // now assert they are present
        gotoViewGroup(TEST_GROUP);
        textAssertions.assertTextPresent(locator.page(), "Default Permission Scheme");
        textAssertions.assertTextPresent(locator.page(), "Default Notification Scheme");
        textAssertions.assertTextPresent(locator.page(), "Test Issue Security Scheme");

        // now make sure its doesn't cache

        // Click Link 'Notification Schemes' (id='notification_schemes').
        navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);
        // Click Link 'Notifications' (id='10000_edit').
        tester.clickLink("10000_edit");
        // Click Link 'Delete' (id='del_10060').
        tester.clickLink("del_10160");
        tester.submit("Delete");

        // Click Link 'Permission Schemes' (id='permission_schemes').
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ADMINISTER_PROJECTS, TEST_GROUP);

        // issue security schemes next
        // Click Link 'Issue Security Schemes' (id='security_schemes').
        navigation.gotoAdminSection(Navigation.AdminSection.SECURITY_SCHEMES);
        // Click Link 'Delete' (id='del_Test Issue Security Scheme').
        tester.clickLink("del_Test Issue Security Scheme");
        tester.submit("Delete");

        // should now be removed and not cached
        gotoViewGroup(TEST_GROUP);
        textAssertions.assertTextPresent(locator.page(), "There are no Permission Schemes associated with this Group.");
        textAssertions.assertTextPresent(locator.page(), "There are no Notification Schemes associated with this Group.");
        textAssertions.assertTextPresent(locator.page(), "There are no Saved Filters associated with this Group.");
        textAssertions.assertTextPresent(locator.page(), "There are no Issue Security Schemes associated with this Group.");
    }

    private void gotoViewGroup(final String group) {
        tester.gotoPage("secure/admin/user/ViewGroup.jspa?name=" + group);
        assertions.assertNodeHasText(new CssLocator(tester, ".aui-page-header-main .aui-nav-breadcrumbs a"), "Groups");
        assertions.assertNodeHasText(new CssLocator(tester, ".aui-page-header-main h2"), group);
    }
}

