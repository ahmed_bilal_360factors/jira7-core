package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.testkit.client.BackdoorControl;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.restclient.Version;
import com.atlassian.jira.testkit.client.restclient.VersionClient;
import com.atlassian.jira.testkit.client.restclient.VersionMove;

import javax.annotation.Nullable;
import java.net.URI;

/**
 * VersionControl for testing creating, releasing and unreleasing version.
 */
public class VersionControl extends BackdoorControl<VersionControl> {
    private VersionClient versionClient;

    public VersionControl(JIRAEnvironmentData environmentData) {
        super(environmentData);
        versionClient = new VersionClient(environmentData);
    }

    public Version createVersion(String name, String description, String projectKey) {
        Version version = new Version().name(name).description(description).project(projectKey);
        return versionClient.create(version);
    }

    public Version releaseVersion(Long versionId) {
        Version version = versionClient.get(versionId.toString());
        Version updatedVersion = version.released(true);
        versionClient.putResponse(updatedVersion);
        return updatedVersion;
    }

    public Version unreleaseVersion(Long versionId) {
        Version version = versionClient.get(versionId.toString());
        Version updatedVersion = version.released(false);
        versionClient.putResponse(updatedVersion);
        return updatedVersion;
    }

    public void mergeVersion(Long fromVersionId, Long toVersionId2) {
        Version version2 = versionClient.get(toVersionId2.toString());
        URI mergeTo = uriOrNull(version2.self);
        versionClient.delete(fromVersionId.toString(), mergeTo, mergeTo);
    }

    public Version setDescription(Long versionId, String desc) {
        Version version = versionClient.get(versionId.toString());
        Version updatedVersion = version.description(desc);
        versionClient.putResponse(updatedVersion);
        return updatedVersion;
    }

    public void moveVersion(Long versionId) {
        VersionMove versionMove = new VersionMove();
        versionMove.position("Last");
        versionClient.move(versionId.toString(), versionMove);
    }

    public void deleteVersion(Long versionId) {
        versionClient.delete(versionId.toString());
    }

    public void delete(Long versionId, @Nullable String fixVersionSwapToSelf, @Nullable String affectsVersionSwapSelf) {
        versionClient.delete(String.valueOf(versionId), uriOrNull(fixVersionSwapToSelf), uriOrNull(affectsVersionSwapSelf));
    }

    private URI uriOrNull(final String fixVersionSwapToSelf) {
        return fixVersionSwapToSelf != null ? URI.create(fixVersionSwapToSelf) : null;
    }
}
