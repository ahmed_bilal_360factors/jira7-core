package com.atlassian.jira.functest.framework.admin;

import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.testkit.client.log.FuncTestLoggerImpl;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.meterware.httpunit.WebTable;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;

/**
 * @since v4.1
 */
public class AttachmentsImpl implements Attachments {
    private final WebTester tester;
    private final Navigation navigation;
    private final FuncTestLogger logger;

    @Inject
    public AttachmentsImpl(WebTester tester, JIRAEnvironmentData environmentData, final Navigation navigation) {
        this.tester = tester;
        this.logger = new FuncTestLoggerImpl(2);
        this.navigation = navigation;
    }

    public void enable() {
        logger.log("Enabling attachments with default path.");
        navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);
        tester.clickLinkWithText("Edit Settings");
        tester.checkCheckbox("attachmentPathOption", "DEFAULT");
        tester.submit("Update");
    }

    public void enable(final String maxAttachmentSize) {
        logger.log("Enabling attachments with max size of : " + maxAttachmentSize);
        navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);
        tester.clickLinkWithText("Edit Settings");
        tester.checkCheckbox("attachmentPathOption", "DEFAULT");
        tester.setFormElement("attachmentSize", maxAttachmentSize);
        tester.submit("Update");
    }


    public void disable() {
        logger.log("Disabling attachments.");
        navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);
        tester.clickLinkWithText("Edit Settings");
        tester.checkCheckbox("attachmentPathOption", "DISABLED");
        tester.checkCheckbox("thumbnailsEnabled", "false");
        tester.checkCheckbox("zipSupport", "false");
        tester.submit("Update");
    }

    public void enableZipSupport() {
        logger.log("Enabling ZIP support for downloading attachments.");
        navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);
        tester.clickLinkWithText("Edit Settings");
        tester.checkCheckbox("zipSupport", "true");
        tester.submit("Update");
    }

    public void disableZipSupport() {
        logger.log("Disabling ZIP support for downloading attachments.");
        navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);
        tester.clickLinkWithText("Edit Settings");
        tester.checkCheckbox("zipSupport", "false");
        tester.submit("Update");
    }

    public String getCurrentAttachmentPath() {
        navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);
        // Get the table 'attachmentSettings'.
        final WebTable attachmentSettings = tester.getDialog().getWebTableBySummaryOrId("attachmentSettings");
        // Check that  'Attachment Path' is in the third row where we expect it:
        if ("Attachment Path".equals(attachmentSettings.getCellAsText(2, 0).trim())) {
            String attachmentPath = attachmentSettings.getCellAsText(2, 1).trim();
            // Check if this is the "default" directory. Looks like "Default Directory [/home/mlassau/jira/jira_trunk/data/attachments]"
            if (attachmentPath.startsWith("Default Directory [")) {
                // Strip "Default Directory [" from the front, and the "]" from the end
                attachmentPath = attachmentPath.substring("Default Directory [".length(), attachmentPath.length() - 1);
            }
            return attachmentPath;
        } else {
            throw new RuntimeException("Error occured when trying to screen-scrape the attachment path. 'Attachment Path' not found where expected in the table.");
        }
    }

}
