package com.atlassian.jira.webtests.ztests.project.type;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.test.util.lic.ela.ElaLicenses;
import com.atlassian.jira.testkit.client.ProjectControl;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@WebTest({Category.FUNC_TEST, Category.API})
@LoginAs(user = ADMIN_USERNAME)
public class TestProjectTypeStorage extends BaseJiraFuncTest {
    private static final String PROJECT_NAME = "Test project";
    private static final String PROJECT_KEY = "TST";
    private static final String BUSINESS = "business";
    private static final String FIRST_TYPE = "first-type";
    private static final String ADMIN = "admin";
    private static final String TASK_MANAGEMENT_TEMPLATE = "com.atlassian.jira-core-project-templates:jira-core-task-management";

    private ProjectControl projectControl;

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstanceWithLicense(ElaLicenses.LICENSE_ELA_CORE_SW_SD_TEST_REF_OTHER);
        projectControl = backdoor.project();
    }

    @Test
    public void testProjectTypeIsCorrectlyStoredWhenAProjectIsCreated() throws Exception {
        long projectId = projectControl.addProject(PROJECT_NAME, PROJECT_KEY, ADMIN, BUSINESS);

        ProjectTypeKey projectType = projectControl.getProjectType(projectId);
        assertThat(projectType.getKey(), is(BUSINESS));
    }

    @Test
    public void testProjectTypeIsCorrectlyLookedUpFromTheProjectTemplateWhenAProjectIsCreated() throws Exception {
        long projectId = projectControl.addProjectWithTemplate(PROJECT_NAME, PROJECT_KEY, ADMIN, TASK_MANAGEMENT_TEMPLATE);
        ;

        ProjectTypeKey projectType = projectControl.getProjectType(projectId);
        assertThat(projectType.getKey(), is(BUSINESS));
    }

    @Test
    public void testProjectTypeCanBeUpdated() throws Exception {
        long projectId = projectControl.addProject(PROJECT_NAME, PROJECT_KEY, ADMIN, BUSINESS);


        projectControl.updateProjectType(projectId, new ProjectTypeKey(FIRST_TYPE));

        ProjectTypeKey projectType = projectControl.getProjectType(projectId);
        assertThat(projectType.getKey(), is(FIRST_TYPE));
    }
}
