package com.atlassian.jira.functest.rule;

import com.atlassian.jira.functest.framework.FuncTestWebClientListener;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.Session;
import com.atlassian.jira.functest.framework.SessionFactory;
import com.atlassian.jira.functest.framework.dump.DumpDataProvider;
import com.atlassian.jira.functest.framework.dump.TestCaseDumpKit;
import com.atlassian.jira.webtests.WebTesterFactory;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.inject.Binder;
import com.google.inject.Module;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sourceforge.jwebunit.WebTester;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.util.Date;
import java.util.function.Supplier;

/**
 * Rule to initialize HtmlUnit test for JIRA func tests.
 *
 * @since v6.5
 */
public class WebTesterRule implements TestRule, Module, Supplier<WebTester> {
    private final Supplier<JIRAEnvironmentData> environmentDataSupplier;
    private final Supplier<FuncTestWebClientListener> webClientListenerSupplier;
    private final SessionFactory sessionFactory = () -> new SessionImpl(createAndInitWebTester());
    private SessionImpl currentSession = null;

    // Responsibility #4 :(
    private class SessionImpl implements Session {
        private final WebTester tester;

        public SessionImpl(final WebTester tester) {
            this.tester = tester;
        }

        @Override
        public void withSession(final Runnable runnable) {
            final SessionImpl previousSession = currentSession;
            currentSession = this;
            try {
                runnable.run();
            } finally {
                currentSession = previousSession;
            }
        }
    }

    /**
     * Use this field to access the {@link com.atlassian.jira.functest.framework.LocatorFactory} in play
     */
    protected LocatorFactory locator;

    public WebTesterRule(final Supplier<JIRAEnvironmentData> environmentDataSupplier, final Supplier<FuncTestWebClientListener> webClientListenerSupplier) {
        this.environmentDataSupplier = environmentDataSupplier;
        this.webClientListenerSupplier = webClientListenerSupplier;

    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                // keep track of HTTP traffic
                evaluateLogException(base, description);
            }
        };
    }

    // Responsibility #1
    @Override
    public WebTester get() {
        // this is the only way testerProxy leaves this class
        // before testerProxy can be used initialize currentSession
        if (currentSession == null) {
            currentSession = (SessionImpl) sessionFactory.begin();
        }
        return testerProxy;
    }

    private final WebTester testerProxy = (WebTester) Enhancer.create(WebTester.class,
            // currentSession must be assigned before testerProxy is used
            (MethodInterceptor) (obj, method, args, proxy) -> proxy.invoke(currentSession.tester, args));

    private WebTester createAndInitWebTester() {
        final FuncTestWebClientListener webClientListener = webClientListenerSupplier.get();

        final WebTester tester = WebTesterFactory.createNewWebTester(environmentDataSupplier.get());
        tester.beginAt("/");

        if (webClientListener != null) {
            tester.getDialog().getWebClient().addClientListener(webClientListener);
        }

        return tester;
    }

    // Responsibility #2 :(
    @Override
    public void configure(final Binder binder) {
        binder.bind(WebTester.class).toProvider(this::get);
        binder.bind(SessionFactory.class).toInstance(sessionFactory);
    }

    // Responsibility #3 :(
    private void evaluateLogException(final Statement base, final Description description) throws Throwable {
        try {
            base.evaluate();
        } catch (final Throwable th) {
            dumpFailureInformationIfTesterExists(th, description);
            throw th;
        }
    }

    private void dumpFailureInformationIfTesterExists(final Throwable throwable, final Description description) {
        if (currentSession != null) {
            dumpFailureInformation(currentSession.tester, throwable, description);
        }
    }

    private void dumpFailureInformation(final WebTester tester, final Throwable throwable, final Description description) {
        try {
            TestCaseDumpKit.dumpTestInformation(getDataProvider(description.getDisplayName(), tester), new Date(), throwable, true);
        } catch (final RuntimeException ignored) {
            ignored.printStackTrace();
        }
    }

    private DumpDataProvider getDataProvider(final String testName, final WebTester tester) {
        return new DumpDataProvider() {
            @Override
            public JIRAEnvironmentData getEnvironmentData() {
                return environmentDataSupplier.get();
            }

            @Override
            public FuncTestWebClientListener getFuncTestWebClientListener() {
                return webClientListenerSupplier.get();
            }

            @Override
            public String getTestName() {
                return testName;
            }

            @Override
            public WebTester getWebTester() {
                return tester;
            }
        };
    }
}
