package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.HttpUnitConfiguration;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.SkipSetup;
import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.assertions.TableAssertions;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.setup.JiraSetupInstanceHelper;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.matcher.PermissionSchemesMatcher;
import com.atlassian.jira.permission.JiraPermissionHolderType;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.testkit.beans.PermissionSchemeBean;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.meterware.httpunit.WebTable;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_ADMIN_ROLE_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_DEV_ROLE_ID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * This functional test should run before all other functional tests. It's designed to check the initial setup of JIRA,
 * prior to any of the XML imports that occur in the other test classes.
 */
@WebTest({Category.FUNC_TEST, Category.BROWSING, Category.IMPORT_EXPORT, Category.SETUP, Category.ISSUE_TYPES, Category.REFERENCE_PLUGIN})
@SkipSetup
@HttpUnitConfiguration(enableScripting = false)
public class TestDefaultJiraDataFromInstall extends BaseJiraFuncTest {

    @Inject
    private Assertions assertions;
    @Inject
    private Administration administration;
    @Inject
    private FuncTestLogger logger;
    @Inject
    private HtmlPage htmlPage;

    private TableAssertions tableAssertions;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testDefaultJiraData() throws SAXException {

        final JiraSetupInstanceHelper jiraSetupInstanceHelper = new JiraSetupInstanceHelper(tester, environmentData);

        if (!jiraSetupInstanceHelper.isJiraSetup()) {
            jiraSetupInstanceHelper.ensureJIRAIsReadyToGo();

            tableAssertions = assertions.getTableAssertions();

            _testEnterpriseDefaultJiraPermissions();
            _testEnterpriseDefaultNotificationSchemes();
            _testEnterpriseDefaultScreens();
            _testEnterpriseDefaultScreenSchemes();
            _testEnterpriseDefaultIssueTypeScreenSchemes();
            _testEnterpriseDefaultEventTypes();
            _testEnterpriseDefaultFieldsVisibility();
            _testDefaultTimeTrackingOptions();
            _testDefaultSubTaskOptions();
            _testDefaultIssueTypes();
            _testWikiRendererAsDefaultForAllRenderableFields();

        } else {
            //if JIRA is setup, you probably need to promote this to the top of the list in AcceptanceTestHarness
            fail("TestDefaultJiraDataFromInstall needs to be prior to being setup");
        }
    }

    private void _testWikiRendererAsDefaultForAllRenderableFields() {
        final String[] renderableFields = {"Comment", "Description", "Environment", "Log Work"};
        for (final String fieldName : renderableFields) {
            assertEquals("Wiki Style Renderer", administration.fieldConfigurations().defaultFieldConfiguration().getRenderer(fieldName));
        }
    }

    private void _testDefaultSubTaskOptions() {
        // assert Sub-tasks are enabled
        navigation.gotoAdminSection(Navigation.AdminSection.SUBTASKS);

        textAssertions.assertTextSequence(new WebPageLocator(tester),
                "Sub-Tasks are currently turned", "ON",
                "You can manage your sub-tasks as part of standard issue types");

        assertions.getLinkAssertions().assertLinkPresentWithExactText("//td[@class='jiraformbody']", "Disable");
    }

    private void _testDefaultIssueTypes() {
        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_TYPES);

        final WebTable issueTypesTable = tester.getDialog().getWebTableBySummaryOrId("issue-types-table");
        // assert there are no issue types in the table (there is only the header row)
        assertEquals(issueTypesTable.getRowCount(), 1);
    }

    private void _testDefaultTimeTrackingOptions() {
        navigation.gotoAdminSection(Navigation.AdminSection.TIMETRACKING);

        // time tracking should be ON by default
        tester.assertElementPresent("deactivate_submit");

        // assert default options for Time Tracking
        textAssertions.assertTextSequence(new WebPageLocator(tester),
                "To change these values deactivate and then reactivate Time Tracking",
                "The number of working hours per day is", "8",
                "The number of working days per week is", "5",
                "Time estimates will be displayed in the following format:", "pretty (e.g. 4 days, 4 hours, 30 minutes)",
                "The current default unit for time tracking is", "minute",
                "Copying of comments to work description is currently", "enabled");

        textAssertions.assertTextNotPresent(new WebPageLocator(tester), "Legacy mode is currently");
    }

    public void _testEnterpriseDefaultFieldsVisibility() {
        // goto FieldConfigurationDefault
        navigation.gotoAdminSection(Navigation.AdminSection.FIELD_CONFIGURATION);
        tester.clickLink("configure-Default Field Configuration");
        tester.assertTextPresent("View Field Configuration");

        // Affects Version/s
        assertField("Affects Version/s", 0, false);
        // Assignee
        assertField("Assignee", 1, false);
        // Attachment
        assertField("Attachment", 2, false);
        // Component/s
        assertField("Component/s", 4, false);
        // Description
        assertField("Description", 5, false);
        // Due Date
        assertField("Due Date", 6, false);
        // Environment
        assertField("Environment", 7, false);
        // Fix Version/s
        assertField("Fix Version/s", 8, false);
        // Priority
        assertField("Priority", 10, false);
        // Reporter
        assertField("Reporter", 11, false);
        // Resolution
        assertField("Resolution", 12, false);
        // Security Level
        assertField("Security Level", 13, false);
    }

    private void assertField(final String field, final int id, final boolean hide) {
        tester.assertTextPresent(field);
        if (hide) {
            tester.assertLinkPresent("hide_" + id);
        }
    }

    public void _testEnterpriseDefaultJiraPermissions() throws SAXException {
        final PermissionSchemeBean permissions = backdoor.permissionSchemes().getAssignedPermissions(DEFAULT_PERM_SCHEME_ID);

        assertAdminRole(permissions, ProjectPermissions.ADMINISTER_PROJECTS);
        assertAnyoneRole(permissions, ProjectPermissions.BROWSE_PROJECTS);
        assertAnyoneRole(permissions, ProjectPermissions.VIEW_READONLY_WORKFLOW);

        logger.log("Checking issue permissions");
        assertAnyoneRole(permissions, ProjectPermissions.ASSIGNABLE_USER);
        assertAnyoneRole(permissions, ProjectPermissions.ASSIGN_ISSUES);
        assertAnyoneRole(permissions, ProjectPermissions.CLOSE_ISSUES);
        assertAnyoneRole(permissions, ProjectPermissions.CREATE_ISSUES);
        assertAdminRole(permissions, ProjectPermissions.DELETE_ISSUES);
        assertAnyoneRole(permissions, ProjectPermissions.EDIT_ISSUES);
        assertAnyoneRole(permissions, ProjectPermissions.LINK_ISSUES);
        assertAdminRole(permissions, ProjectPermissions.MODIFY_REPORTER);
        assertAnyoneRole(permissions, ProjectPermissions.MOVE_ISSUES);
        assertAnyoneRole(permissions, ProjectPermissions.RESOLVE_ISSUES);
        assertAnyoneRole(permissions, ProjectPermissions.SCHEDULE_ISSUES);
        assertNoRole(permissions, ProjectPermissions.SET_ISSUE_SECURITY);
        assertAnyoneRole(permissions, ProjectPermissions.TRANSITION_ISSUES);

        logger.log("Checking edit comments");
        assertAnyoneRole(permissions, ProjectPermissions.ADD_COMMENTS);
        assertAdminRole(permissions, ProjectPermissions.DELETE_ALL_COMMENTS);
        assertAnyoneRole(permissions, ProjectPermissions.DELETE_OWN_COMMENTS);
        assertAdminRole(permissions, ProjectPermissions.EDIT_ALL_COMMENTS);
        assertAnyoneRole(permissions, ProjectPermissions.EDIT_OWN_COMMENTS);

        logger.log("Checking time tracking permissions");
        assertAdminRole(permissions, ProjectPermissions.DELETE_ALL_WORKLOGS);
        assertAnyoneRole(permissions, ProjectPermissions.DELETE_OWN_WORKLOGS);
        assertAdminRole(permissions, ProjectPermissions.EDIT_ALL_WORKLOGS);
        assertAnyoneRole(permissions, ProjectPermissions.EDIT_OWN_WORKLOGS);
        assertAnyoneRole(permissions, ProjectPermissions.WORK_ON_ISSUES);

        logger.log("Checking attachments permissions");
        assertAnyoneRole(permissions, ProjectPermissions.CREATE_ATTACHMENTS);
        assertAdminRole(permissions, ProjectPermissions.DELETE_ALL_ATTACHMENTS);
        assertAnyoneRole(permissions, ProjectPermissions.DELETE_OWN_ATTACHMENTS);

        logger.log("Checking voters & watchers permissions");
        assertAdminRole(permissions, ProjectPermissions.MANAGE_WATCHERS);
        assertAnyoneRole(permissions, ProjectPermissions.VIEW_VOTERS_AND_WATCHERS);

        logger.log("Checking Global Permissions");
        navigation.gotoAdminSection(Navigation.AdminSection.GLOBAL_PERMISSIONS);
        final WebTable globalPermissionsTable = tester.getDialog().getResponse().getTableWithID("global_perms");
        tableAssertions.assertTableCellHasText(globalPermissionsTable, 1, 0, "JIRA System Administrators");
        tableAssertions.assertTableCellHasText(globalPermissionsTable, 1, 1, "jira-administrators");
        tableAssertions.assertTableCellHasText(globalPermissionsTable, 2, 0, "JIRA Administrators");
        tableAssertions.assertTableCellHasText(globalPermissionsTable, 2, 1, "jira-administrators");
        tableAssertions.assertTableCellHasText(globalPermissionsTable, 3, 0, "Browse Users");
        tableAssertions.assertTableCellHasText(globalPermissionsTable, 3, 1, "jira-software-users");
        tableAssertions.assertTableCellHasText(globalPermissionsTable, 4, 0, "Create Shared Objects");
        tableAssertions.assertTableCellHasText(globalPermissionsTable, 4, 1, "jira-software-users");
        tableAssertions.assertTableCellHasText(globalPermissionsTable, 5, 0, "Manage Group Filter Subscriptions");
        tableAssertions.assertTableCellHasText(globalPermissionsTable, 5, 1, "jira-software-users");
        tableAssertions.assertTableCellHasText(globalPermissionsTable, 6, 0, "Bulk Change");
        tableAssertions.assertTableCellHasText(globalPermissionsTable, 6, 1, "jira-software-users");

    }

    private void _testEnterpriseDefaultNotificationSchemes() {
        navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);

        final String atl_token = htmlPage.getXsrfToken();
        final XPathLocator nameLocator = new XPathLocator(tester, "//*[@href=\"EditNotifications!default.jspa?atl_token=" + atl_token + "&schemeId=10000\"]");
        textAssertions.assertTextPresent(nameLocator, "Default Notification Scheme");
    }

    private void _testEnterpriseDefaultScreens() {
        final String[] strings = {"Default Screen", "Allows to update all system fields.",
                "Resolve Issue Screen", "Allows to set resolution, change fix versions and assign an issue.",
                "Workflow Screen", "This screen is used in the workflow and enables you to assign issues"};

        navigation.gotoAdminSection(Navigation.AdminSection.FIELD_SCREENS);

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), strings);
        // Check assign issue screen was replaced by the workflow screen correctly
        textAssertions.assertTextNotPresent(tester.getDialog().getResponseText(), "Assign Issue Screen");
        textAssertions.assertTextNotPresent(tester.getDialog().getResponseText(), "Allows to assign an issue.");
    }

    private void _testEnterpriseDefaultScreenSchemes() {
        final String[] strings = {"Default Screen Scheme", "Default Screen Scheme"};

        navigation.gotoAdminSection(Navigation.AdminSection.FIELD_SCREEN_SCHEME);

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), strings);
    }

    private void _testEnterpriseDefaultIssueTypeScreenSchemes() {
        final String[] strings = {"Default Issue Type Screen Scheme", "The default issue type screen scheme"};

        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_TYPE_SCREEN_SCHEME);

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), strings);
    }

    private void _testEnterpriseDefaultEventTypes() {
        final String[] strings = {"Issue Created", "This is the &#39;issue created&#39; event.",
                "Issue Updated", "This is the &#39;issue updated&#39; event.",
                "Issue Assigned", "This is the &#39;issue assigned&#39; event.",
                "Issue Resolved", "This is the &#39;issue resolved&#39; event.",
                "Issue Closed", "This is the &#39;issue closed&#39; event.",
                "Issue Commented", "This is the &#39;issue commented&#39; event.",
                "Issue Comment Edited", "This is the &#39;issue comment edited&#39; event.",
                "Issue Reopened", "This is the &#39;issue reopened&#39; event.",
                "Issue Deleted", "This is the &#39;issue deleted&#39; event.",
                "Issue Moved", "This is the &#39;issue moved&#39; event.",
                "Work Logged On Issue", "This is the &#39;work logged on issue&#39; event.",
                "Work Started On Issue", "This is the &#39;work started on issue&#39; event.",
                "Work Stopped On Issue", "This is the &#39;work stopped on issue&#39; event.",
                "Issue Worklog Updated", "This is the &#39;issue worklog updated&#39; event.",
                "Issue Worklog Deleted", "This is the &#39;issue worklog deleted&#39; event.",
                "Generic Event", "This is the &#39;generic event&#39; event.",
        };

        navigation.gotoAdminSection(Navigation.AdminSection.EVENTTYPES);

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), strings);
    }

    private void assertAdminRole(final PermissionSchemeBean permissions, final ProjectPermissionKey permissionsKey) {
        assertHasPermission(permissions, permissionsKey, JiraPermissionHolderType.PROJECT_ROLE, Long.toString(JIRA_ADMIN_ROLE_ID));
    }

    private void assertAnyoneRole(final PermissionSchemeBean permissions, final ProjectPermissionKey permissionsKey) {
        assertHasPermission(permissions, permissionsKey, JiraPermissionHolderType.APPLICATION_ROLE, "");
    }

    private void assertNoRole(final PermissionSchemeBean permissions, final ProjectPermissionKey permissionsKey) {
        assertHasNoPermission(permissions, permissionsKey, JiraPermissionHolderType.PROJECT_ROLE, Long.toString(JIRA_ADMIN_ROLE_ID));
        assertHasNoPermission(permissions, permissionsKey, JiraPermissionHolderType.APPLICATION_ROLE, "");
        assertHasNoPermission(permissions, permissionsKey, JiraPermissionHolderType.PROJECT_ROLE, Long.toString(JIRA_DEV_ROLE_ID));
    }

    private void assertHasPermission(final PermissionSchemeBean permissions, final ProjectPermissionKey permissionsKey, final JiraPermissionHolderType type, final String parameter) {
        assertPermission(permissions, permissionsKey, type, parameter, true);
    }

    private void assertHasNoPermission(final PermissionSchemeBean permissions, final ProjectPermissionKey permissionsKey, final JiraPermissionHolderType type, final String parameter) {
        assertPermission(permissions, permissionsKey, type, parameter, false);
    }

    private void assertPermission(final PermissionSchemeBean permissions, final ProjectPermissionKey permissionsKey, final JiraPermissionHolderType type, final String parameter, final boolean hasPermission) {
        final String errorMessage = String.format("Does not have permission '%s' with type '%s' and value '%s'",
                permissionsKey.permissionKey(), type.getKey(), parameter);

        final boolean matches = PermissionSchemesMatcher.hasPermission(
                permissionsKey.permissionKey(), type.getKey(), parameter)
                .matches(permissions);

        assertEquals(errorMessage, matches, hasPermission);
    }

    @SuppressWarnings("unused") // As of JIRA 6.4, no default permission scheme values are assigned to Developers.
    private void assertDevRole(final WebTable permissionsTable, final int row) {
        tableAssertions.assertTableCellHasText(permissionsTable, row, 1, "(Developers)");
    }
}
