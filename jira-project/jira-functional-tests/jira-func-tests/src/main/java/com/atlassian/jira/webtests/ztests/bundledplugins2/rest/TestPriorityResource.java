package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Priority;
import com.atlassian.jira.testkit.client.restclient.PriorityClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Func tests for PriorityResource.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestPriorityResource extends BaseJiraRestTest {
    private static final String PRIORITY_ID = "1";

    private PriorityClient priorityClient;

    @Test
    public void testAllPriorities() throws Exception {
        List<Priority> priorities = priorityClient.get();

        assertprioritiesContain(priorities, "1");
        assertprioritiesContain(priorities, "2");
        assertprioritiesContain(priorities, "3");
        assertprioritiesContain(priorities, "4");
        assertprioritiesContain(priorities, "5");
    }

    private void assertprioritiesContain(List<Priority> priorities, String id) {
        for (Priority priority : priorities) {
            if (priority.id().equals(id)) {
                return;
            }
        }
        fail("Priority " + id + " not in list");
    }


    @Test
    public void testViewPriority() throws Exception {
        Priority priority = priorityClient.get(PRIORITY_ID);

        assertEquals(environmentData.getBaseUrl() + "/rest/api/2/priority/" + PRIORITY_ID, priority.self());
        assertEquals("#cc0000", priority.statusColor());
        assertEquals("Blocks development and/or testing work, production could not run.", priority.description());
        assertEquals(environmentData.getBaseUrl() + FunctTestConstants.PRIORITY_IMAGE_BLOCKER, priority.iconUrl());
        assertEquals("Blocker", priority.name());
        assertEquals(PRIORITY_ID, priority.id());
    }

    @Test
    public void testViewPriorityNotFound() throws Exception {
        // {"errorMessages":["The priority with id '123' does not exist"],"errors":[]}
        Response resp123 = priorityClient.getResponse("123");
        assertEquals(404, resp123.statusCode);
        assertEquals(1, resp123.entity.errorMessages.size());
        assertTrue(resp123.entity.errorMessages.contains("The priority with id '123' does not exist"));

        // {"errorMessages":["The priority with id 'foo' does not exist"],"errors":[]}
        Response respFoo = priorityClient.getResponse("foo");
        assertEquals(404, respFoo.statusCode);
        assertTrue(respFoo.entity.errorMessages.contains("The priority with id 'foo' does not exist"));
    }

    @Before
    public void setUp() {
        priorityClient = new PriorityClient(environmentData);
    }
}
