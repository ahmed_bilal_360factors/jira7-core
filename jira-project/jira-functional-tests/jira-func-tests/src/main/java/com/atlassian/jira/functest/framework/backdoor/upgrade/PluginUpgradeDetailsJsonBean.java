package com.atlassian.jira.functest.framework.backdoor.upgrade;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PluginUpgradeDetailsJsonBean {

    @JsonProperty
    private Integer buildNumber;

    @JsonProperty
    private Integer salBuildNumber;

    @JsonProperty
    private Integer aoBuildNumber;

    public Integer getBuildNumber() {
        return buildNumber;
    }

    public Integer getAoBuildNumber() {
        return aoBuildNumber;
    }

    public Integer getSalBuildNumber() {
        return salBuildNumber;
    }
}
