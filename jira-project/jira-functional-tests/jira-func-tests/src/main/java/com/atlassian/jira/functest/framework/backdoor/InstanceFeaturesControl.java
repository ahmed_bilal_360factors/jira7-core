package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

public class InstanceFeaturesControl extends BackdoorControl<InstanceFeaturesControl> {
    public InstanceFeaturesControl(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public boolean isEnabled(String featureKey) {
        final String result = createResource()
                .path("instanceFeatures")
                .path(featureKey)
                .get(String.class);
        return Boolean.parseBoolean(result);
    }

    public boolean enable(String featureKey) {
        final String result = createResource()
                .path("instanceFeatures")
                .path(featureKey)
                .path("set")
                .get(String.class);
        return Boolean.parseBoolean(result);
    }

    public boolean disable(String featureKey) {
        final String result = createResource()
                .path("instanceFeatures")
                .path(featureKey)
                .path("unset")
                .get(String.class);
        return Boolean.parseBoolean(result);
    }
}
