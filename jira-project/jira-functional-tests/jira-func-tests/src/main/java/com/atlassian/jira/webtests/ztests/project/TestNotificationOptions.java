package com.atlassian.jira.webtests.ztests.project;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.BROWSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestNotificationOptions extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testComponentLeadIsAnOptionForIssueCreatedNotifications() throws Exception {
        goToDefaultNotificationScheme();
        goToAddIssueCreatedNotificationPage();

        textAssertions.assertTextPresent(locator.page(), "Component Lead");
    }

    @Test
    public void testProjectRoleIsAnOptionForIssueCreatedNotifications() {
        administration.restoreData("TestSchemesProjectRoles.xml");
        goToDefaultNotificationScheme();
        goToAddIssueCreatedNotificationPage();

        textAssertions.assertTextPresent(locator.page(), "Choose a project role");

        tester.checkCheckbox("type", "Project_Role");
        tester.selectOption("Project_Role", "test role");
        tester.submit();

        textAssertions.assertTextPresent(locator.page(), "(test role)");
    }

    private void goToDefaultNotificationScheme() {
        navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);
        tester.clickLinkWithText("Notifications");
    }

    private void goToAddIssueCreatedNotificationPage() {
        tester.clickLink("add_1");
    }
}
