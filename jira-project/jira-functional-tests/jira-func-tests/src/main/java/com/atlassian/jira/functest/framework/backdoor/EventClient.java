package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.google.common.collect.ImmutableList;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * @since v7.0
 */
public class EventClient extends BackdoorControl<EventClient> {
    public EventClient(final JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public EventPoller createPoller() {
        final EventData data = getEventsAtPos(-1);
        return new EventPoller(data.position());
    }

    public void stopAllPollers() {
        createEventResource().delete();
    }

    private EventData getEventsAtPos(int position) {
        return createEventResource()
                .queryParam("position", String.valueOf(position))
                .get(EventData.class);
    }

    private WebResource createEventResource() {
        return createResource().path("event");
    }

    public class EventPoller {
        private int position;

        private EventPoller(final int position) {
            this.position = position;
        }

        public Iterable<String> events() {
            final EventData data = getEventsAtPos(position);

            if (data.position != position) {
                if (data.position > position) {
                    throw new IllegalStateException(String.format("%d events got dropped. Received event data: %s", data.position - position, data));
                } else {
                    throw new IllegalStateException("Asking for events that don't exist yet.");
                }
            } else {
                position = data.nextPosition();
                return data.events();
            }
        }
    }

    public static class EventData {
        private final List<String> events;
        private final int position;

        @JsonCreator
        public EventData(@JsonProperty("events") final List<String> events,
                         @JsonProperty("position") final int position) {
            this.events = ImmutableList.copyOf(events);
            this.position = position;
        }

        private List<String> events() {
            return events;
        }

        private int position() {
            return position;
        }

        private int nextPosition() {
            return position + events.size();
        }

        @Override
        public String toString() {
            return "EventData{" +
                    "events=" + events +
                    ", position=" + position +
                    '}';
        }
    }
}
