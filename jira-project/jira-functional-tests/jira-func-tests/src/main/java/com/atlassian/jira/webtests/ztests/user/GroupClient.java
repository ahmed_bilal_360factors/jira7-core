package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.rest.v2.issue.GroupBean;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import javax.annotation.Nullable;
import java.net.MalformedURLException;
import java.util.Set;

/**
 * Client for the REST group resource
 */
public class GroupClient extends RestApiClient<GroupClient> {
    public static final String GROUP_RESOURCE = "group";
    private static final String GROUP_ADD_USER_RESOURCE = "group/user";
    public static final String GROUP_NAME = "groupname";
    public static final String USER_NAME = "username";
    public static final String NAME = "name";

    private final Set<ClientResponse> responses = Sets.newHashSet();

    public GroupClient(final JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    /**
     * Unwrap the GroupBean body from the response
     *
     * @param response the response
     * @return the GroupBean
     */
    public GroupBean toGroupBean(ClientResponse response) {
        return toResponse(() -> response, GroupBean.class).body;
    }

    /**
     * Create a group
     *
     * @param groupName the group name
     * @return the raw response
     */
    public ClientResponse createGroup(final String groupName) {
        final ImmutableMap.Builder<Object, Object> builder = ImmutableMap.builder();
        builder.put("name", groupName);
        return doPost(groupResource(), builder);
    }

    private WebResource groupResource() {
        return createResource().path(GROUP_RESOURCE);
    }

    /**
     * Get a group
     *
     * @param group  name
     * @param expand the expand parameter, e.g. users[0:50]
     * @return the response
     */
    public ClientResponse getGroup(final String group, @Nullable final String expand) {
        WebResource webResource = groupResource().queryParam(GROUP_NAME, group);
        if (expand != null) {
            webResource = webResource.queryParam("expand", expand);
        }
        final ClientResponse clientResponse = webResource.get(ClientResponse.class);
        responses.add(clientResponse);
        return clientResponse;
    }

    /**
     * Get a group. No expansion of users.
     *
     * @param group the group
     * @return the response
     */
    public ClientResponse getGroup(final String group) {
        return getGroup(group, null);
    }

    /**
     * Get a group and unwrap the GroupBean. No expansion of users.
     *
     * @param group the group
     * @return the response
     */
    public GroupBean getGroupBean(final String group) {
        return toGroupBean(getGroup(group, null));
    }

    /**
     * Get a group and unwrap the GroupBean. No expansion of users.
     *
     * @param group  the group
     * @param expand the expand parameter
     * @return the response
     */
    public GroupBean getGroupBean(final String group, final String expand) {
        return toGroupBean(getGroup(group, expand));
    }

    public ClientResponse deleteGroup(final String group) {
        final WebResource webResource = groupResource()
                .queryParam(GROUP_NAME, group);
        final ImmutableMap.Builder<Object, Object> builder = ImmutableMap.builder();
        return doDelete(webResource, builder);

    }

    public ClientResponse addUserToGroup(final String group, final String user) {
        final WebResource webResource = createResource().path(GROUP_ADD_USER_RESOURCE).queryParam(GROUP_NAME, group);
        final ImmutableMap.Builder<Object, Object> builder = ImmutableMap.builder();
        builder.put(NAME, user);
        return doPost(webResource, builder);
    }

    public ClientResponse removeUserFromGroup(final String group, final String user) {
        final WebResource webResource = createResource().path(GROUP_ADD_USER_RESOURCE)
                .queryParam(GROUP_NAME, group)
                .queryParam(USER_NAME, user);
        final ImmutableMap.Builder<Object, Object> builder = ImmutableMap.builder();
        return doDelete(webResource, builder);
    }

    private ClientResponse doPost(final WebResource webResource, final ImmutableMap.Builder<Object, Object> builder) {
        final ClientResponse clientResponse = webResource
                .type("application/json")
                .post(ClientResponse.class, builder.build());

        responses.add(clientResponse);
        return clientResponse;
    }

    private ClientResponse doDelete(final WebResource webResource, final ImmutableMap.Builder<Object, Object> builder) {
        final ClientResponse clientResponse = webResource
                .type("application/json")
                .delete(ClientResponse.class, builder.build());

        responses.add(clientResponse);
        return clientResponse;
    }

    public UsersPageBean getPaginatedUsersForGroup(final String group,
                                                   final Boolean includeInactiveUsers,
                                                   final Long startAt,
                                                   final Long maxResults) {
        return groupResource()
                .path("member")
                .queryParam(GROUP_NAME, group)
                .queryParam("includeInactiveUsers", includeInactiveUsers.toString())
                .queryParam("startAt", startAt.toString())
                .queryParam("maxResults", maxResults.toString())
                .get(UsersPageBean.class);
    }

    public UsersPageBean getNextPage(UsersPageBean usersPageBean) {
        if (usersPageBean.getIsLast()) {
            throw new IllegalStateException("Cannot retrieve next page as this is already the last page");
        }

        try {
            return resourceRoot(usersPageBean.getNextPage().toURL().toExternalForm()).get(UsersPageBean.class);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public void close() {
        for (ClientResponse response : responses) {
            response.close();
        }
    }
}
