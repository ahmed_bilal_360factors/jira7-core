package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @since v5.2
 */
@WebTest({Category.FUNC_TEST, Category.UPGRADE_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUpgradeTask807 extends BaseJiraFuncTest {
    @Test
    public void testUpgradeTask() {
        backdoor.restoreDataFromResource("xml/TestUpgradeTask807.xml");
        try {
            backdoor.workflowSchemes().getWorkflowSchemeDraftByProjectKey("HSP");
            fail("The draft should have been removed. Expecting a 404.");
        } catch (UniformInterfaceException e) {
            final ClientResponse response = e.getResponse();
            assertEquals(ClientResponse.Status.NOT_FOUND.getStatusCode(), response.getStatus());
        }
    }
}
