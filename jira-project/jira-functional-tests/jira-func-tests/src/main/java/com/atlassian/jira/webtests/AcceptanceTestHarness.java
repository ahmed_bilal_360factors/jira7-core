package com.atlassian.jira.webtests;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteAdministration;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteAppLinks;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteAttachments;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteAvatars;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteBrowsing;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteBulkOperations;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteCharting;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteCloneIssue;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteComments;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteComponentsAndVersions;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteCustomFields;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteDashboards;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteDatabase;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteEmail;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteFields;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteFilters;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteForgotten;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteHelpUrls;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteI18n;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteImportExport;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteIssueNavigator;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteIssues;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteJql;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteLdap;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteLicencing;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteMoveIssue;
import com.atlassian.jira.webtests.zsuites.FuncTestSuitePermissions;
import com.atlassian.jira.webtests.zsuites.FuncTestSuitePlatform;
import com.atlassian.jira.webtests.zsuites.FuncTestSuitePlugins;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteProjectConfig;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteProjectImport;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteProjectTypes;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteProjects;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteREST;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteRandomTests;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteRemote;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteReports;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteRoles;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteSchemes;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteSecurity;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteSetup;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteSharedEntities;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteStreams;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteSubTasks;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteTimeTracking;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteTimeZone;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteUpgradeTasks;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteUsersAndGroups;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteWebhooks;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteWorkflow;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteWorklogs;
import com.atlassian.jira.webtests.ztests.TestInternalServerError;
import com.atlassian.jira.webtests.ztests.about.TestAboutPage;
import com.atlassian.jira.webtests.ztests.bundledplugins2.TestIssueTabPanels;
import com.atlassian.jira.webtests.ztests.bundledplugins2.gadget.TestAssignedToMeGadget;
import com.atlassian.jira.webtests.ztests.bundledplugins2.gadget.TestRoadMapGadget;
import com.atlassian.jira.webtests.ztests.dashboard.TestHead;
import com.atlassian.jira.webtests.ztests.plugin.FuncTestSuiteReferencePlugins;
import com.google.common.collect.ImmutableList;
import junit.framework.TestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.AllTests;

import java.util.List;

/**
 * This is the top level Test Suite for JIRA web functional tests.  In order for a functional test to be run by the
 * nightly / bamboo builds, it must be declared in this class or in a FuncTestSuite that is included via the class.
 */
@RunWith(AllTests.class)
public class AcceptanceTestHarness {
    public static TestSuite suite() {
        final FuncTestSuite testSuite = new FuncTestSuite();
        //NOTE: These "setup" tests must run first as they will fail otherwise.
        testSuite.addTests(FuncTestSuiteSetup.suite());

        testSuite.addTests(FuncTestSuiteMoveIssue.suite());
        testSuite.addTests(FuncTestSuiteAvatars.suite());
        testSuite.addTests(FuncTestSuiteBulkOperations.suite());
        testSuite.addTests(FuncTestSuiteComments.suite());
        testSuite.addTests(FuncTestSuiteComponentsAndVersions.suite());
        testSuite.addTests(FuncTestSuiteDashboards.suite());
        testSuite.addTests(FuncTestSuiteFields.suite());
        testSuite.addTests(FuncTestSuiteFilters.suite());
        testSuite.addTests(FuncTestSuiteIssueNavigator.suite());
        testSuite.addTests(FuncTestSuiteIssues.suite());
        testSuite.addTests(FuncTestSuiteProjectTypes.suite());
        testSuite.addTests(FuncTestSuiteLicencing.suite());
        testSuite.addTests(FuncTestSuitePermissions.suite());
        testSuite.addTests(FuncTestSuiteProjects.suite());
        testSuite.addTests(FuncTestSuiteProjectImport.suite());
        testSuite.addTests(FuncTestSuiteReports.suite());
        testSuite.addTests(FuncTestSuiteRoles.suite());
        testSuite.addTests(FuncTestSuiteSchemes.suite());
        testSuite.addTests(FuncTestSuiteSecurity.suite());
        testSuite.addTests(FuncTestSuiteI18n.suite());
        testSuite.addTests(FuncTestSuiteSharedEntities.suite());
        testSuite.addTests(FuncTestSuiteSubTasks.suite());
        testSuite.addTests(FuncTestSuiteCloneIssue.suite());
        testSuite.addTests(FuncTestSuiteUsersAndGroups.suite());
        testSuite.addTests(FuncTestSuiteWorkflow.suite());
        testSuite.addTests(FuncTestSuiteWorklogs.suite());
        testSuite.addTests(FuncTestSuiteBrowsing.suite());
        testSuite.addTests(FuncTestSuiteImportExport.suite());
        testSuite.addTests(FuncTestSuiteJql.suite());
        testSuite.addTests(FuncTestSuiteCharting.suite());
        testSuite.addTests(FuncTestSuiteTimeTracking.suite());
        testSuite.addTests(FuncTestSuiteAttachments.suite());
        testSuite.addTests(FuncTestSuiteCustomFields.suite());
        testSuite.addTests(FuncTestSuiteEmail.suite());
        testSuite.addTests(FuncTestSuiteUpgradeTasks.suite());
        testSuite.addTests(FuncTestSuiteAdministration.suite());
        testSuite.addTests(FuncTestSuiteRandomTests.suite());
        testSuite.addTests(FuncTestSuiteReferencePlugins.suite());
        testSuite.addTests(FuncTestSuiteAppLinks.suite());
        testSuite.addTests(FuncTestSuitePlatform.suite());
        testSuite.addTests(FuncTestSuiteDatabase.suite());
        testSuite.addTests(FuncTestSuiteStreams.suite());
        testSuite.addTests(FuncTestSuiteHelpUrls.suite());
        testSuite.addTests(FuncTestSuiteWebhooks.suite());
        testSuite.addTests(FuncTestSuiteProjectConfig.suite());

        // this suite holds classes that has not been added to other suites
        // it is temporary and will be removed together with classes that it will still contain
        // at the time of removal
        testSuite.addTests(FuncTestSuiteForgotten.suite());

        // this suite only runs with bundled plugins enabled
        testSuite.addBundledPluginsTests(FuncTestSuiteREST.bundledPluginsTests());
        testSuite.addBundledPluginsTests(FuncTestSuitePlugins.bundledPluginsTests());
        testSuite.addBundledPluginsTests(FuncTestSuiteRemote.bundledPluginsTests());
        testSuite.addBundledPluginsTests(getBundledPluginsIntegrationTests());

        //this suite should only run when
        testSuite.addBundledPluginsTest(TestAssignedToMeGadget.class);
        testSuite.addBundledPluginsTest(TestIssueTabPanels.class);
        testSuite.addBundledPluginsTest(TestRoadMapGadget.class);

        // this suite only runs on the LDAP TPM builds
        testSuite.addTests(FuncTestSuiteLdap.suite());
        testSuite.addTests(FuncTestSuiteTimeZone.suite());

        testSuite.addTest(TestAboutPage.class);
        testSuite.addTest(TestHead.class);
        testSuite.addTest(TestInternalServerError.class);

        return testSuite.createTest();
    }

    /**
     * JIRA's project config plugin (pcp) has moved back into the jira project after being a separate plugin.
     * The jira-admin-project-config-plugin test artifact's func and web driver tests gets extracted during un-packing
     * in jira-func-tests-runner this is so that pcp's func and web-driver test gets run with JIRA's tests.
     */
    private static List<Class<?>> getBundledPluginsIntegrationTests() {
        try {
            return FuncTestSuite.getTestClasses("it.com.atlassian.jira.projectconfig.func", true);
        } catch (IllegalStateException | IllegalArgumentException unableToLocateTests) {
            //Some builds don't build pcp's test artifact like "Unit Tests plus Functional Unit Tests"
            //because they skip building test artifacts -Dmaven.test.skip=true.
            //Those builds aren't meant to run func and web-driver test so when trying to retrieve pcp's test during
            //those build, these exception get thrown, test should continue running gracefully so returning no test found.
            return ImmutableList.of();
        }
    }
}
