package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.EditIssueFieldVisibility;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.Indexing;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Parser;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.admin.TimeTracking;
import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.parser.issue.ViewIssueDetails;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.ResourceRef;
import com.atlassian.jira.testkit.beans.Priority;
import com.atlassian.jira.testkit.client.IssueTypeControl;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.testkit.client.restclient.Component;
import com.atlassian.jira.webtests.Groups;
import com.atlassian.jira.webtests.ztests.workflow.ExpectedChangeHistoryItem;
import com.atlassian.jira.webtests.ztests.workflow.ExpectedChangeHistoryRecord;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.meterware.httpunit.WebForm;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.AFFECTS_VERSIONS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.COMPONENTS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.COMPONENT_NAME_ONE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.COMPONENT_NAME_THREE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.COMPONENT_NAME_TWO;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIX_VERSIONS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_IMPROVEMENT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_NEWFEATURE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_TASK;
import static com.atlassian.jira.functest.framework.FunctTestConstants.MODIFY_REPORTER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PRIORITY_BLOCKER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PRIORITY_CRITICAL;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PRIORITY_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PRIORITY_MINOR;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PRIORITY_TRIVIAL;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.REPORTER_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.TRANSIION_NAME_CLOSE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.VERSION_NAME_ONE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.VERSION_NAME_THREE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.VERSION_NAME_TWO;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Format.DAYS;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Format.HOURS;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Format.PRETTY;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Mode.LEGACY;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.permission.ProjectPermissions.ADD_COMMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.ASSIGN_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.EDIT_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.SCHEDULE_ISSUES;
import static com.atlassian.jira.webtests.Groups.ADMINISTRATORS;
import static com.atlassian.jira.webtests.Groups.DEVELOPERS;
import static com.atlassian.jira.webtests.Groups.USERS;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toMap;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

@WebTest({Category.FUNC_TEST, Category.ISSUES})
public class TestEditIssue extends BaseJiraFuncTest {

    private static final String COMMENT_1 = "This issue is resolved now.";
    private static final String COMMENT_2 = "Viewable by developers group.";
    private static final String COMMENT_3 = "Viewable by Developers role.";
    private static final String HSP_1 = "HSP-1";
    private static final String MKY_2 = "MKY-2";
    private static final String ADDED_COMPONENT = "oracle component";

    @Inject
    private IssueTableAssertions issueTableAssertions;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private TimeTracking timeTracking;

    @Inject
    private Indexing indexing;

    @Inject
    private Assertions assertions;

    @Inject
    private Parser parse;

    @Inject
    private Form form;

    @Inject
    private HtmlPage htmlPage;

    @Inject
    private EditIssueFieldVisibility editIssueFieldVisibility;
    private String issueKeyNormal;
    private String issueKeyWithNoComponents;
    private String issueKeyWithTimeTracking;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        backdoor.darkFeatures().enableForSite("jira.no.frother.reporter.field");
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
    }

    @After
    public void tearDown() {
        backdoor.darkFeatures().disableForSite("jira.no.frother.reporter.field");
    }

    /* fix for JRA-13921 */
    @Test
    @Restore("TestEditIssue.xml")
    @LoginAs(user = ADMIN_USERNAME)
    public void testUpdateIssueWithVersionCF() throws Exception {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        // add Version Picker custom field
        navigation.gotoAdmin();
        navigation.gotoCustomFields();
        tester.clickLink("add_custom_fields");
        tester.checkCheckbox("fieldType", "com.atlassian.jira.plugin.system.customfieldtypes:multiversion");
        tester.submit("nextBtn");
        tester.setFormElement("fieldName", "CF-A");
        tester.submit("nextBtn");
        tester.checkCheckbox("associatedScreens", "1");
        tester.checkCheckbox("associatedScreens", "3");
        tester.checkCheckbox("associatedScreens", "2");
        tester.submit("Update");

        // set the value of new custom field to 'New Version 1'
        navigation.issue().gotoIssue(HSP_1);
        tester.clickLink("edit-issue");
        tester.selectOption("customfield_10000", "New Version 1");
        tester.submit("Update");

        // verify in search
        issueTableAssertions.assertSearchWithResults("project=homosapien AND CF-A=\"New Version 1\"", "HSP-1");

        // unset the value of new custom field
        navigation.issue().gotoIssue(HSP_1);
        tester.clickLink("edit-issue");

        tester.getDialog().setFormParameter("customfield_10000", "-1");
        tester.submit("Update");

        // verify in search
        issueTableAssertions.assertSearchWithResults("project=homosapien AND CF-A=\"New Version 1\"");
    }

    /* fix for JRA-20146 */
    @Test
    @Restore("TestEditIssue.xml")
    @LoginAs(user = ADMIN_USERNAME)
    public void testEditIssueUnknownReporter() throws Exception {
        // set the value of new custom field to 'New Version 1'
        navigation.issue().gotoIssue(MKY_2);
        tester.clickLink("edit-issue");
        tester.submit("Update");

        // verify bad user
        tester.assertTextPresent("The reporter specified is not a user.");

        // set to a good user
        tester.setFormElement("assignee", ADMIN_USERNAME);
        tester.setFormElement("reporter", ADMIN_USERNAME);
        tester.submit("Update");
    }

    @Test
    @RestoreBlankInstance
    @LoginAs(user = ADMIN_USERNAME)
    public void editIssueWithoutSummary() {
        prepareIssuesForEditIssueTests();
        logger.log("Edit Issue: omitting summary");
        //check that the issue is initially indexed
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("summary", "test with components"), null, issueKeyNormal);
        navigation.issue().gotoIssue(issueKeyNormal);

        tester.clickLink("edit-issue");

        // Clear summary
        tester.setFormElement("summary", "");

        tester.submit();
        tester.assertTextPresent("You must specify a summary of the issue.");

        tester.setFormElement("summary", "test if index is updated");
        tester.submit();

        //assert that issue summary index was updated
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("summary", "test if index is updated"), ImmutableMap.of("summary", "test with components"), issueKeyNormal);
    }


    /**
     * Makes the fields Components,Affects Versions and Fixed Versions required.
     * Attempts to create an issue with required fields not filled out and with an invalid assignee
     */
    @Test
    @RestoreBlankInstance
    @LoginAs(user = ADMIN_USERNAME)
    public void editIssueWithRequiredFields() {
        prepareIssuesForEditIssueTests();
        // Set fields to be required
        editIssueFieldVisibility.setRequiredFields();

        logger.log("Edit Issue: Editing issue with required fields.");

        navigation.issue().gotoIssue(issueKeyWithNoComponents);
        tester.clickLink("edit-issue");
        tester.assertTextPresent("Edit Issue");
        tester.submit("Update");

        tester.assertTextPresent("Edit Issue");
        tester.assertTextPresent("Component/s is required");
        tester.assertTextPresent("Affects Version/s is required");
        tester.assertTextPresent("Fix Version/s is required");

        // Reset fields to be optional
        editIssueFieldVisibility.resetFields();
    }

    /**
     * Makes the fields Components,Affects Versions and Fixed Versions hidden.
     */
    @Test
    @RestoreBlankInstance
    @LoginAs(user = ADMIN_USERNAME)
    public void editIssueWithHiddenFields() {
        prepareIssuesForEditIssueTests();
        // Hide fields
        editIssueFieldVisibility.setHiddenFields(COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setHiddenFields(AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setHiddenFields(FIX_VERSIONS_FIELD_ID);

        logger.log("Edit Issue: Editing issue with Hidden fields.");

        navigation.issue().gotoIssue(issueKeyWithNoComponents);

        tester.clickLink("edit-issue");

        tester.assertTextPresent("Edit Issue");

        tester.assertLinkNotPresent("components");
        tester.assertLinkNotPresent("versions");
        tester.assertLinkNotPresent("fixVersions");

        // Reset fields to be optional
        editIssueFieldVisibility.resetFields();
    }

    @Test
    @RestoreBlankInstance
    @LoginAs(user = ADMIN_USERNAME)
    public void editIssueWithInvalidDueDate() {
        prepareIssuesForEditIssueTests();
        logger.log("Edit Issue: Editing issue with invalid due date");
        navigation.issue().gotoIssue(issueKeyWithNoComponents);
        tester.clickLink("edit-issue");
        tester.assertTextPresent("Edit Issue");
        tester.setFormElement("duedate", "stuff");
        tester.submit();
        tester.assertTextPresent("Edit Issue");
        tester.assertTextPresent("You did not enter a valid date. Please enter the date in the format &quot;d/MMM/yy&quot;");
    }

    /**
     * Tests if the 'Edit Issue' Link is available with the 'Edit Issue' permission removed.
     */
    @Test
    @RestoreBlankInstance
    @LoginAs(user = ADMIN_USERNAME)
    public void editIssueWithEditPermission() {
        prepareIssuesForEditIssueTests();
        logger.log("Edit Issue: Test availability of 'Edit Issue' link with 'Edit Issue' permission.");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES, DEVELOPERS);
        navigation.issue().gotoIssue(issueKeyNormal);
        tester.assertLinkNotPresent("edit-issue");

        // Grant 'Edit Issue' permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES, DEVELOPERS);
        navigation.issue().gotoIssue(issueKeyNormal);
        tester.assertLinkPresent("edit-issue");
    }

    /**
     * Tests if the 'Due Date' field is available with the 'Schedule Issue' permission removed
     */
    @Test
    @RestoreBlankInstance
    @LoginAs(user = ADMIN_USERNAME)
    public void editIssueWithSchedulePermission() {
        prepareIssuesForEditIssueTests();
        logger.log("Edit Issue: Test prescence of 'Due Date' field with 'Schedule Issue' permission.");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, SCHEDULE_ISSUES, DEVELOPERS);
        navigation.issue().gotoIssue(issueKeyNormal);
        tester.clickLink("edit-issue");
        tester.assertTextNotPresent("Due Date");

        // Grant Schedule Issue Permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, SCHEDULE_ISSUES, DEVELOPERS);
        navigation.issue().gotoIssue(issueKeyNormal);
        tester.clickLink("edit-issue");
        tester.assertTextPresent("Due Date");
    }

    /**
     * Tests if the user is able to specify an assignee with the 'Assign Issue' permission removed.
     */
    @Test
    @RestoreBlankInstance
    @LoginAs(user = ADMIN_USERNAME)
    public void editIssueWithAssignPermission() {
        prepareIssuesForEditIssueTests();
        logger.log("Edit Issue: Test ability to specify assignee with 'Assign Issue' permission.");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ASSIGN_ISSUES, DEVELOPERS);
        navigation.issue().gotoIssue(issueKeyNormal);
        tester.clickLink("edit-issue");
        tester.assertFormElementNotPresent("assignee");

        // Grant Assign Issue Permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ASSIGN_ISSUES, DEVELOPERS);
        navigation.issue().gotoIssue(issueKeyNormal);
        tester.clickLink("edit-issue");
        tester.assertFormElementPresent("assignee");
    }

    /**
     * Tests if the 'Reporter' field is available with the 'Assign Issue' permission removed.
     */
    @Test
    @RestoreBlankInstance
    @LoginAs(user = ADMIN_USERNAME)
    public void editIssueWithModifyReporterPermission() {
        prepareIssuesForEditIssueTests();
        logger.log("Edit Issue: Test availability of Reporter with 'Modify Reporter' permission.");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, MODIFY_REPORTER, ADMINISTRATORS);
        navigation.issue().gotoIssue(issueKeyNormal);
        tester.clickLink("edit-issue");
        tester.assertFormElementNotPresent("reporter");

        // Grant Modify Reporter Permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, MODIFY_REPORTER, ADMINISTRATORS);
        navigation.issue().gotoIssue(issueKeyNormal);
        tester.clickLink("edit-issue");
        tester.assertFormElementPresent("reporter");
    }

    /**
     * Tests if the 'Comment' field is available with the 'Add Comments' permission removed.
     */
    @Test
    @RestoreBlankInstance
    @LoginAs(user = ADMIN_USERNAME)
    public void editIssueWithAddCommentsPermission() {
        prepareIssuesForEditIssueTests();
        logger.log("Edit Issue: Test availability of Comment field with 'Add Comments' permission.");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ADD_COMMENTS, USERS);
        navigation.issue().gotoIssue(issueKeyNormal);
        tester.assertLinkNotPresent("comment-issue");
        tester.assertLinkNotPresent("footer-comment-button");
        tester.clickLink("edit-issue");

        // Grant Add Comments Permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ADD_COMMENTS, USERS);
        navigation.issue().gotoIssue(issueKeyNormal);
        tester.assertLinkPresent("comment-issue");
        tester.assertLinkPresent("footer-comment-button");
        tester.clickLink("edit-issue");
    }

    /**
     * Tests if the 'original estimate' field is available with time tracking activated and WITHOUT a previous original
     * estimate.
     * Tests if the 'original estimate' field is present and displays the correct information with time tracking
     * activated and WITH a previous original estimate,
     */
    @Test
    @RestoreBlankInstance
    @LoginAs(user = ADMIN_USERNAME)
    public void editIssueWithTimeTracking() {
        prepareIssuesForEditIssueTests();
        logger.log("Edit Issue: Test availability of time tracking related fields");

        // No original estimate specified
        timeTracking.enable(LEGACY);
        navigation.issue().gotoIssue(issueKeyWithTimeTracking);
        tester.clickLink("edit-issue");
        tester.assertFormElementPresent("timetracking");
        timeTracking.disable();

        // Original estimate specified - with work logged against issue
        timeTracking.enable(LEGACY);
        logWorkOnIssue(issueKeyWithTimeTracking, "1d");
        navigation.issue().gotoIssue(issueKeyWithTimeTracking);
        tester.clickLink("edit-issue");
        tester.assertTextPresent("Remaining Estimate");
        tester.assertFormElementPresent("timetracking");
        // default is "pretty"
        tester.assertFormElementEquals("timetracking", "6d");

        timeTracking.switchFormat(DAYS);
        navigation.issue().gotoIssue(issueKeyWithTimeTracking);
        tester.clickLink("edit-issue");
        tester.assertTextPresent("Remaining Estimate");
        tester.assertFormElementPresent("timetracking");
        tester.assertFormElementEquals("timetracking", "6d");

        timeTracking.switchFormat(HOURS);
        navigation.issue().gotoIssue(issueKeyWithTimeTracking);
        tester.clickLink("edit-issue");
        tester.assertTextPresent("Remaining Estimate");
        tester.assertFormElementPresent("timetracking");
        tester.assertFormElementEquals("timetracking", "144h");

        // now we log work in some crazy ways to make sure that the handling of decimal fractional days/hours is done properly
        // 11 minutes can't be displayed as a decimal fractional without losing information so we need to fallback appropriately
        logWorkOnIssue(issueKeyWithTimeTracking, "11m");

        timeTracking.switchFormat(PRETTY);
        navigation.issue().gotoIssue(issueKeyWithTimeTracking);
        tester.clickLink("edit-issue");
        tester.assertTextPresent("Remaining Estimate");
        tester.assertFormElementPresent("timetracking");
        tester.assertFormElementEquals("timetracking", "5d 23h 49m");

        timeTracking.switchFormat(DAYS);
        navigation.issue().gotoIssue(issueKeyWithTimeTracking);
        tester.clickLink("edit-issue");
        tester.assertTextPresent("Remaining Estimate");
        tester.assertFormElementPresent("timetracking");
        tester.assertFormElementEquals("timetracking", "5d 23h 49m");

        timeTracking.switchFormat(HOURS);
        navigation.issue().gotoIssue(issueKeyWithTimeTracking);
        tester.clickLink("edit-issue");
        tester.assertTextPresent("Remaining Estimate");
        tester.assertFormElementPresent("timetracking");
        tester.assertFormElementEquals("timetracking", "143h 49m");

        // after logging a full 30 minutes of work we have a number than can be safely represented as a decimal number of hours
        logWorkOnIssue(issueKeyWithTimeTracking, "19m");
        timeTracking.switchFormat(PRETTY);
        navigation.issue().gotoIssue(issueKeyWithTimeTracking);
        tester.clickLink("edit-issue");
        tester.assertTextPresent("Remaining Estimate");
        tester.assertFormElementPresent("timetracking");
        tester.assertFormElementEquals("timetracking", "5d 23h 30m");

        timeTracking.switchFormat(DAYS);
        navigation.issue().gotoIssue(issueKeyWithTimeTracking);
        tester.clickLink("edit-issue");
        tester.assertTextPresent("Remaining Estimate");
        tester.assertFormElementPresent("timetracking");
        tester.assertFormElementEquals("timetracking", "5d 23.5h");

        timeTracking.switchFormat(HOURS);
        navigation.issue().gotoIssue(issueKeyWithTimeTracking);
        tester.clickLink("edit-issue");
        tester.assertTextPresent("Remaining Estimate");
        tester.assertFormElementPresent("timetracking");
        tester.assertFormElementEquals("timetracking", "143.5h");

        // now we log work to get the days to display as a fraction
        logWorkOnIssue(issueKeyWithTimeTracking, "11h 48m");

        timeTracking.switchFormat(PRETTY);
        navigation.issue().gotoIssue(issueKeyWithTimeTracking);
        tester.clickLink("edit-issue");
        tester.assertTextPresent("Remaining Estimate");
        tester.assertFormElementPresent("timetracking");
        tester.assertFormElementEquals("timetracking", "5d 11h 42m");

        timeTracking.switchFormat(DAYS);
        navigation.issue().gotoIssue(issueKeyWithTimeTracking);
        tester.clickLink("edit-issue");
        tester.assertTextPresent("Remaining Estimate");
        tester.assertFormElementPresent("timetracking");
        tester.assertFormElementEquals("timetracking", "5d 11.7h");

        timeTracking.switchFormat(HOURS);
        navigation.issue().gotoIssue(issueKeyWithTimeTracking);
        tester.clickLink("edit-issue");
        tester.assertTextPresent("Remaining Estimate");
        tester.assertFormElementPresent("timetracking");
        tester.assertFormElementEquals("timetracking", "131.7h");

        timeTracking.disable();
    }

    /**
     * Tests that the appropriate error message is displayed when a user tries to edit a closed issue.
     * JRA-13553
     */
    @Test
    @Restore("TestEditIssue.xml")
    @LoginAs(user = ADMIN_USERNAME)
    public void testEditClosedIssue() {
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES, USERS);

        navigation.issue().gotoIssue(HSP_1);
        tester.assertLinkPresent("edit-issue");
        tester.clickLinkWithText(TRANSIION_NAME_CLOSE);
        tester.setWorkingForm("issue-workflow-transition");
        tester.submit("Transition");
        tester.assertLinkNotPresentWithText("edit-issue");
        navigation.gotoPage("/secure/EditIssue!default.jspa?id=10000");
        tester.assertTextPresent("You are not allowed to edit this issue due to its current status in the workflow.");
    }

    /**
     * Tests that the appropriate error message is displayed when a user tries to edit an issue without permission.
     * JRA-13553
     */
    @Test
    @Restore("TestEditIssue.xml")
    @LoginAs(user = ADMIN_USERNAME)
    public void testEditIssueWithEditPermissionManualNavigation() {
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES, DEVELOPERS);
        navigation.gotoPage("/secure/EditIssue!default.jspa?id=10000");
        tester.assertTextPresent("You do not have permission to edit issues in this project.");

        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES, USERS);
        navigation.gotoPage("/secure/EditIssue!default.jspa?id=10000");
        tester.assertTextNotPresent("You do not have permission to edit issues in this project.");
    }

    /**
     * Tests that the appropriate error message is displayed when a user tries to edit an issue when logged out.
     * JRA-13553
     */
    @Test
    @Restore("TestEditIssue.xml")
    public void testEditIssueWhileLoggedOut() {
        navigation.gotoPage("/secure/EditIssue!default.jspa?id=10000");
        tester.assertTextPresent("You are not logged in");
    }

    @Test
    @Restore("TestEditIssue.xml")
    public void testEditIssueWithPermissionWhileLoggedOut() {
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_PERM_SCHEME_ID, BROWSE_PROJECTS);
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_PERM_SCHEME_ID, EDIT_ISSUES);
        navigation.gotoPage("secure/Dashboard.jspa");
        navigation.issue().gotoIssue(HSP_1);
        tester.assertLinkPresent("edit-issue");
    }

    @Test
    @Restore("TestEditIssue.xml")
    @LoginAs(user = ADMIN_USERNAME)
    public void testEditWithCommentVisibility() {
        navigation.comment().enableCommentGroupVisibility(true);

        indexing.assertIndexedFieldCorrect("//item/comments", null, ImmutableMap.of("comment", COMMENT_1), HSP_1);
        // edit issue with comment visible to all users
        navigation.issue().gotoIssue(HSP_1);
        tester.clickLink("edit-issue");
        tester.setFormElement("comment", COMMENT_1);
        tester.submit("Update");
        indexing.assertIndexedFieldCorrect("//item/comments", ImmutableMap.of("comment", COMMENT_1), null, HSP_1);

        indexing.assertIndexedFieldCorrect("//item/comments", null, ImmutableMap.of("comment", COMMENT_2), HSP_1);
        // edit issue with comment visible only to jira-developers group
        navigation.issue().gotoIssue(HSP_1);
        tester.clickLink("edit-issue");
        tester.setFormElement("comment", COMMENT_2);
        tester.selectOption("commentLevel", Groups.DEVELOPERS);
        tester.submit("Update");
        indexing.assertIndexedFieldCorrect("//item/comments", ImmutableMap.of("comment", COMMENT_2), null, HSP_1);


        indexing.assertIndexedFieldCorrect("//item/comments", null, ImmutableMap.of("comment", COMMENT_3), HSP_1);
        // edit issue with comment visible only to Developers role
        navigation.issue().gotoIssue(HSP_1);
        tester.clickLink("edit-issue");
        tester.setFormElement("comment", COMMENT_3);
        tester.selectOption("commentLevel", "Developers");
        tester.submit("Update");
        indexing.assertIndexedFieldCorrect("//item/comments", ImmutableMap.of("comment", COMMENT_3), null, HSP_1);

        // verify that Fred can see general comment but not others as he is not in the visibility groups
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(HSP_1);
        tester.assertTextPresent(COMMENT_1);
        tester.assertTextNotPresent(COMMENT_2);
        tester.assertTextNotPresent(COMMENT_3);

        // verify that Admin can see all comments as he is not in all visibility groups
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.issue().gotoIssue(HSP_1);
        tester.assertTextPresent(COMMENT_1);
        tester.assertTextPresent(COMMENT_2);
        tester.assertTextPresent(COMMENT_3);
    }

    /**
     * Tests adding a new component, editing the issue with no changes and asserting
     * the issue has not changed. Initiated by JRA-12130
     */
    @Test
    @Restore("TestEditIssueWithNoChanges.xml")
    @LoginAs(user = ADMIN_USERNAME)
    public void testEditNewComponentAndIssueWithNoChanges() {

        backdoor.components().create(new Component().name(ADDED_COMPONENT).project(PROJECT_HOMOSAP_KEY));
        final String issueKey = backdoor.issues().createIssue(
                PROJECT_HOMOSAP_KEY,
                "edit issue test summary",
                ADMIN_USERNAME,
                getIdOfPriority(PRIORITY_BLOCKER),
                getIdOfIssueType(ISSUE_TYPE_NEWFEATURE)).key();
        backdoor.issues().setIssueFields(issueKey, new IssueFields()
                .components(ResourceRef.withName(ADDED_COMPONENT))
                .versions(ResourceRef.withName(VERSION_NAME_TWO))
                .fixVersions(ResourceRef.withName(VERSION_NAME_ONE), ResourceRef.withName(VERSION_NAME_THREE))
                .description("edit issue description")
                .environment("edit environment")
                .timeTracking(new com.atlassian.jira.rest.api.issue.TimeTracking("5d", null))
                .dueDate("2007-02-14"));

        //assert issues initial values are the same as the created values
        navigation.issue().gotoIssue(issueKey);
        assertNewComponentAndIssueViewPage();

        //check that the edit issue page has the correct values
        tester.clickLink("edit-issue");
        final WebForm form = this.form.switchTo("issue-edit");
        assertThat(form.getParameterValue("summary"), equalTo("edit issue test summary"));
        assertThat(form.getParameterValue("environment").trim(), equalTo("edit environment"));
        assertThat(form.getParameterValue("description").trim(), equalTo("edit issue description"));
        assertions.getProjectFieldsAssertions().assertIssueTypeIsSelected(ISSUE_TYPE_NEWFEATURE);
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("priority", PRIORITY_BLOCKER);
        assertThat(form.getParameterValue("duedate"), equalTo("14/Feb/07"));
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("components", ADDED_COMPONENT);
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("fixVersions", VERSION_NAME_ONE);
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("fixVersions", VERSION_NAME_THREE);
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("versions", VERSION_NAME_TWO);
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("assignee", ADMIN_FULLNAME);
        assertThat(form.getParameterValue("reporter"), equalTo(ADMIN_USERNAME));
        assertThat(form.getParameterValue("timetracking"), equalTo("5d"));

        //update without making any changes and assert that the values are the same
        //Needs to assert that the cached component was not lost JRA-12130
        tester.submit("Update");
        assertNewComponentAndIssueViewPage();
    }

    private void assertNewComponentAndIssueViewPage() {
        tester.assertLinkPresentWithText(PROJECT_HOMOSAP);
        textAssertions.assertTextSequence(htmlPage.asString(), "Type", ISSUE_TYPE_NEWFEATURE);
        tester.assertTextPresent("edit issue test summary");
        textAssertions.assertTextSequence(htmlPage.asString(), PRIORITY_FIELD_ID, PRIORITY_BLOCKER);
        textAssertions.assertTextSequence(htmlPage.asString(), "Due", "14/Feb/07");
        textAssertions.assertTextSequence(htmlPage.asString(), COMPONENTS_FIELD_ID, ADDED_COMPONENT);
        textAssertions.assertTextSequence(htmlPage.asString(), AFFECTS_VERSIONS_FIELD_ID, VERSION_NAME_TWO);
        textAssertions.assertTextSequence(htmlPage.asString(), FIX_VERSIONS_FIELD_ID, VERSION_NAME_ONE);
        textAssertions.assertTextSequence(htmlPage.asString(), FIX_VERSIONS_FIELD_ID, VERSION_NAME_THREE);
        textAssertions.assertTextSequence(htmlPage.asString(), "Assignee", ADMIN_FULLNAME);
        textAssertions.assertTextSequence(htmlPage.asString(), REPORTER_FIELD_ID, ADMIN_FULLNAME);
        textAssertions.assertTextSequence(htmlPage.asString(), "Environment", "edit environment");
        textAssertions.assertTextSequence(htmlPage.asString(), "Description", "edit issue description");
    }

    /**
     * Tests that edit issue does not make any changes if no values was actually changed.
     * ie. goto edit issue -> do nothing -> click update -> check nothing changed.
     */
    @Test
    @Restore("TestEditIssueWithNoChanges.xml")
    @LoginAs(user = ADMIN_USERNAME)
    public void testEditIssueWithCustomFieldsAndNoChanges() {
        //assert issues initial values
        navigation.issue().gotoIssue(HSP_1);
        assertInitialViewIssueFieldValues();

        //check that the edit issue page has the correct values
        tester.clickLink("edit-issue");
        assertInitialEditIssueFieldValues();

        //update without making any changes and assert that the values are the same
        tester.setWorkingForm("issue-edit");
        tester.submit();
        assertInitialViewIssueFieldValues();
    }

    /**
     * Tests that edit issue removes all non-required field values
     * ie. goto edit issue -> remove all values -> click update -> check no values set
     */
    @Test
    @Restore("TestEditIssueWithNoChanges.xml")
    @LoginAs(user = ADMIN_USERNAME)
    public void testEditIssueWithCustomFieldsAndRemoveValues() {
        //assert issues initial values
        navigation.issue().gotoIssue(HSP_1);
        assertInitialViewIssueFieldValues();
        assertions.assertLastChangeHistoryRecords(HSP_1, emptyList());

        //check that the edit issue page has the correct initial values
        tester.clickLink("edit-issue");
        assertInitialEditIssueFieldValues();

        //remove all the non-required values
        navigation.issue().selectIssueType(ISSUE_TYPE_TASK, "issuetype");
        tester.selectOption("priority", PRIORITY_CRITICAL);
        tester.selectOption("components", "Unknown");
        tester.selectOption("versions", "Unknown");
        tester.selectOption("fixVersions", "Unknown");
        tester.setFormElement("description", "");
        tester.setFormElement("environment", "");
        tester.setFormElement("reporter", ADMIN_USERNAME);
        tester.setFormElement("timetracking", "");
        tester.selectOption("customfield_10000", "None");
        tester.selectOption("customfield_10000:1", "None");
        tester.setFormElement("customfield_10001", "");
        tester.setFormElement("customfield_10002", "");
        tester.setFormElement("customfield_10003", "");
        tester.setFormElement("customfield_10004", "");
        tester.uncheckCheckbox("customfield_10006", "10008");
        tester.setFormElement("customfield_10007", "");
        tester.selectOption("customfield_10008", "None");
        tester.setFormElement("customfield_10009", "");
        tester.setFormElement("customfield_10010", "");
        tester.selectOption("customfield_10011", "None");
        tester.checkCheckbox("customfield_10012", "-1");
        tester.selectOption("customfield_10014", "None");
        tester.selectOption("customfield_10015", "Unknown");
        tester.setFormElement("customfield_10016", "");
        tester.setFormElement("customfield_10017", "");
        tester.setFormElement("customfield_10018", "");
        tester.selectOption("customfield_10019", "Unknown");
        //update the changes and assert that the values are removed
        tester.setWorkingForm("issue-edit");
        tester.submit("Update");

        assertions.assertLastChangeHistoryRecords(HSP_1, new ExpectedChangeHistoryRecord(
                new ExpectedChangeHistoryItem("Issue Type", ISSUE_TYPE_IMPROVEMENT, ISSUE_TYPE_TASK),
                new ExpectedChangeHistoryItem("Text Field", "text field", ""),
                new ExpectedChangeHistoryItem("Environment", "test environment 1", ""),
                new ExpectedChangeHistoryItem("Project Picker", PROJECT_MONKEY, ""),
                new ExpectedChangeHistoryItem("Group Picker", Groups.USERS, ""),
                //need to assert the values seperately to avoid dependency on database ordering
                new ExpectedChangeHistoryItem("Multi Select", ImmutableList.of("value 1", "value 2"), emptyList()),
                new ExpectedChangeHistoryItem("Number Field", "12345", ""),
                new ExpectedChangeHistoryItem(COMPONENTS_FIELD_ID, COMPONENT_NAME_ONE, ""),
                new ExpectedChangeHistoryItem(COMPONENTS_FIELD_ID, COMPONENT_NAME_THREE, ""),
                new ExpectedChangeHistoryItem(FIX_VERSIONS_FIELD_ID, VERSION_NAME_TWO, ""),
                new ExpectedChangeHistoryItem("Free Text Field", "this is a free text", ""),
                new ExpectedChangeHistoryItem("Multi User Picker", ADMIN_USERNAME, ""),
                new ExpectedChangeHistoryItem(AFFECTS_VERSIONS_FIELD_ID, VERSION_NAME_ONE, ""),
                new ExpectedChangeHistoryItem(AFFECTS_VERSIONS_FIELD_ID, VERSION_NAME_THREE, ""),
                new ExpectedChangeHistoryItem("User Picker", ADMIN_USERNAME, ""),
                new ExpectedChangeHistoryItem(PRIORITY_FIELD_ID, PRIORITY_TRIVIAL, PRIORITY_CRITICAL),
                new ExpectedChangeHistoryItem("URL Field", "http://www.atlassian.com", ""),
                new ExpectedChangeHistoryItem("Version Picker", ImmutableList.of(VERSION_NAME_ONE, VERSION_NAME_THREE), emptyList()),
                new ExpectedChangeHistoryItem("Single Version Picker", VERSION_NAME_THREE, ""),
                new ExpectedChangeHistoryItem("Date Picker", "13/Feb/07", ""),
                new ExpectedChangeHistoryItem("Radio Buttons", "value 3", ""),
                new ExpectedChangeHistoryItem("Multi Group Picker", ImmutableList.of(Groups.DEVELOPERS, Groups.USERS), emptyList()),
                new ExpectedChangeHistoryItem("Cascading Select", ImmutableList.of("Parent values: value 1", "Level 1 values: value 1.2"), emptyList()),
                new ExpectedChangeHistoryItem("Remaining Estimate", "3 days", ""),
                new ExpectedChangeHistoryItem("Select List", "value 3", ""),
                new ExpectedChangeHistoryItem("Description", "test editing issue without any changes", ""),
                new ExpectedChangeHistoryItem("Date Time", "12/Feb/07 11:26 AM", ""),
                new ExpectedChangeHistoryItem("Multi Checkboxes", "value 3", "")
        ));
    }

    /**
     * Tests that edit issue removes all non-required field values
     * ie. goto edit issue -> remove all values -> click update -> check no values set
     */
    @Test
    @Restore("TestEditIssueWithNoChanges.xml")
    @LoginAs(user = ADMIN_USERNAME)
    public void testEditIssueWithCustomFieldsAndChangeValues() {
        //assert issues initial values
        navigation.issue().gotoIssue(HSP_1);
        assertInitialViewIssueFieldValues();
        assertions.assertLastChangeHistoryRecords(HSP_1, emptyList());

        //check that the edit issue page has the correct values
        tester.clickLink("edit-issue");
        assertInitialEditIssueFieldValues();

        //modify values
        tester.setFormElement("summary", "new summary");
        tester.setFormElement("description", "new description");
        tester.setFormElement("environment", "new environment");
        navigation.issue().selectIssueType(ISSUE_TYPE_BUG, "issuetype");
        tester.selectOption("priority", PRIORITY_MINOR);
        tester.selectOption("components", COMPONENT_NAME_TWO);
        tester.selectOption("versions", VERSION_NAME_TWO);
        tester.selectOption("fixVersions", VERSION_NAME_ONE);
        tester.selectOption("fixVersions", VERSION_NAME_ONE);
        tester.setFormElement("timetracking", "1d");
        //cascading select
        tester.selectOption("customfield_10000", "value 2");
        tester.selectOption("customfield_10000:1", "value 2.1");
        //date picker
        tester.setFormElement("customfield_10001", "21/Feb/07");
        //date time
        tester.setFormElement("customfield_10002", "21/Feb/07 11:30 AM");
        //free text
        tester.setFormElement("customfield_10003", "new free text field");
        //group picker
        tester.setFormElement("customfield_10004", Groups.DEVELOPERS);
        //Multi checkboxes
        tester.checkCheckbox("customfield_10006", "10007");
        tester.setFormElement("customfield_10007", Groups.ADMINISTRATORS + ", " + Groups.USERS);
        //Multi select
        form.selectOptionsByDisplayName("customfield_10008", new String[]{"value 3", "value 2"});
        //Multi user picker
        tester.setFormElement("customfield_10009", FRED_USERNAME);
        //Number
        tester.setFormElement("customfield_10010", "54321");
        //Project picker
        tester.selectOption("customfield_10011", PROJECT_HOMOSAP);
        //Radio
        tester.checkCheckbox("customfield_10012", "10012");
        //Select List
        tester.selectOption("customfield_10014", "value 2");
        //Single version picker
        tester.selectOption("customfield_10015", VERSION_NAME_TWO);
        //text field
        tester.setFormElement("customfield_10016", "text field modified");
        //url
        tester.setFormElement("customfield_10017", "http://www.atlassian.com/software/jira");
        //user picker
        tester.setFormElement("customfield_10018", FRED_USERNAME);
        //version picker
        tester.selectOption("customfield_10019", VERSION_NAME_TWO);

        //update the changes and assert that the values are changed
        tester.submit("Update");
        assertions.assertLastChangeHistoryRecords(HSP_1, new ExpectedChangeHistoryRecord(
                new ExpectedChangeHistoryItem("Issue Type", ISSUE_TYPE_IMPROVEMENT, ISSUE_TYPE_BUG),
                new ExpectedChangeHistoryItem("Text Field", "text field", "text field modified"),
                new ExpectedChangeHistoryItem("Environment", "test environment 1", "new environment"),
                new ExpectedChangeHistoryItem("Project Picker", PROJECT_MONKEY, PROJECT_HOMOSAP),
                new ExpectedChangeHistoryItem("Group Picker", Groups.USERS, Groups.DEVELOPERS),
                new ExpectedChangeHistoryItem("Multi Select", ImmutableList.of("value 1", "value 2"), ImmutableList.of("value 2", "value 3")),
                new ExpectedChangeHistoryItem("Number Field", "12345", "54321"),
                new ExpectedChangeHistoryItem(COMPONENTS_FIELD_ID, COMPONENT_NAME_ONE, ""),
                new ExpectedChangeHistoryItem(COMPONENTS_FIELD_ID, COMPONENT_NAME_THREE, ""),
                new ExpectedChangeHistoryItem(COMPONENTS_FIELD_ID, "", COMPONENT_NAME_TWO),
                new ExpectedChangeHistoryItem(FIX_VERSIONS_FIELD_ID, VERSION_NAME_TWO, ""),
                new ExpectedChangeHistoryItem(FIX_VERSIONS_FIELD_ID, "", VERSION_NAME_ONE),
                new ExpectedChangeHistoryItem("Free Text Field", "this is a free text", "new free text field"),
                new ExpectedChangeHistoryItem("Multi User Picker", ADMIN_USERNAME, FRED_USERNAME),
                new ExpectedChangeHistoryItem(AFFECTS_VERSIONS_FIELD_ID, VERSION_NAME_ONE, ""),
                new ExpectedChangeHistoryItem(AFFECTS_VERSIONS_FIELD_ID, VERSION_NAME_THREE, ""),
                new ExpectedChangeHistoryItem(AFFECTS_VERSIONS_FIELD_ID, "", VERSION_NAME_TWO),
                new ExpectedChangeHistoryItem("User Picker", ADMIN_USERNAME, FRED_USERNAME),
                new ExpectedChangeHistoryItem(PRIORITY_FIELD_ID, PRIORITY_TRIVIAL, PRIORITY_MINOR),
                new ExpectedChangeHistoryItem("Summary", "test edit issue with no changes", "new summary"),
                new ExpectedChangeHistoryItem("URL Field", "http://www.atlassian.com", "http://www.atlassian.com/software/jira"),
                new ExpectedChangeHistoryItem("Date Picker", "13/Feb/07", "21/Feb/07"),
                new ExpectedChangeHistoryItem("Radio Buttons", "value 3", "value 1"),
                new ExpectedChangeHistoryItem("Multi Group Picker", ImmutableList.of(Groups.DEVELOPERS, Groups.USERS), ImmutableList.of(Groups.ADMINISTRATORS, Groups.USERS)),
                new ExpectedChangeHistoryItem("Cascading Select",
                        ImmutableList.of("Parent values: value 1", "Level 1 values: value 1.2"),
                        ImmutableList.of("Parent values: value 2", "Level 1 values: value 2.1")),
                new ExpectedChangeHistoryItem("Remaining Estimate", "3 days", "1 day"),
                new ExpectedChangeHistoryItem("Single Version Picker", VERSION_NAME_THREE, VERSION_NAME_TWO),
                new ExpectedChangeHistoryItem("Version Picker", ImmutableList.of(VERSION_NAME_ONE, VERSION_NAME_THREE), singletonList(VERSION_NAME_TWO)),
                new ExpectedChangeHistoryItem("Select List", "value 3", "value 2"),
                new ExpectedChangeHistoryItem("Description", "test editing issue without any changes", "new description"),
                new ExpectedChangeHistoryItem("Date Time", "12/Feb/07 11:26 AM", "21/Feb/07 11:30 AM"),
                new ExpectedChangeHistoryItem("Multi Checkboxes", "value 3", "value 2")
        ));
    }

    /**
     * On the view issue page, assert that the field values are set as in the import xml file TestEditIssueWithNoChanges.xml
     * (Note: need to navigate to the view issue page before calling this method)
     */
    private void assertInitialViewIssueFieldValues() {
        tester.assertLinkPresentWithText(PROJECT_HOMOSAP);
        textAssertions.assertTextSequence(htmlPage.asString(), "Type", ISSUE_TYPE_IMPROVEMENT);
        tester.assertTextPresent("test edit issue with no changes");
        textAssertions.assertTextSequence(htmlPage.asString(), PRIORITY_FIELD_ID, PRIORITY_TRIVIAL);
        textAssertions.assertTextSequence(htmlPage.asString(), "Due", "14/Feb/07");

        textAssertions.assertTextSequence(htmlPage.asString(), COMPONENTS_FIELD_ID, COMPONENT_NAME_ONE);
        textAssertions.assertTextSequence(htmlPage.asString(), COMPONENTS_FIELD_ID, COMPONENT_NAME_THREE);
        textAssertions.assertTextSequence(htmlPage.asString(), AFFECTS_VERSIONS_FIELD_ID, VERSION_NAME_ONE);
        textAssertions.assertTextSequence(htmlPage.asString(), AFFECTS_VERSIONS_FIELD_ID, VERSION_NAME_THREE);
        textAssertions.assertTextSequence(htmlPage.asString(), FIX_VERSIONS_FIELD_ID, VERSION_NAME_TWO);

        textAssertions.assertTextSequence(htmlPage.asString(), "Assignee", ADMIN_FULLNAME);
        textAssertions.assertTextSequence(htmlPage.asString(), REPORTER_FIELD_ID, ADMIN_FULLNAME);
        textAssertions.assertTextSequence(htmlPage.asString(), "Environment", "test environment 1");
        textAssertions.assertTextSequence(htmlPage.asString(), "Description", "test editing issue without any changes");

        final Map<String, Object> fieldValues = new TreeMap<String, Object>();
        fieldValues.put("Cascading Select", new String[]{"value 1", "value 1.2"});
        fieldValues.put("Date Picker", "13/Feb/07");
        fieldValues.put("Date Time", "12/Feb/07 11:26 AM");
        fieldValues.put("Free Text Field", "this is a free text");
        fieldValues.put("Group Picker", Groups.USERS);
        fieldValues.put("Multi Checkboxes", "value 3");
        fieldValues.put("Multi Group Picker", new String[]{Groups.DEVELOPERS, Groups.USERS});
        fieldValues.put("Multi Select", new String[]{"value 1", "value 2"});
        fieldValues.put("Multi User Picker", ADMIN_FULLNAME);
        fieldValues.put("Number Field", "12,345");
        fieldValues.put("Project Picker", PROJECT_MONKEY);
        fieldValues.put("Radio Buttons", "value 3");
        fieldValues.put("Select List", "value 3");
        fieldValues.put("Single Version Picker", VERSION_NAME_THREE);
        fieldValues.put("Text Field", "text field");
        fieldValues.put("URL Field", "http://www.atlassian.com");
        fieldValues.put("User Picker", ADMIN_FULLNAME);
        fieldValues.put("Version Picker", new String[]{VERSION_NAME_ONE, VERSION_NAME_THREE});

        final ViewIssueDetails details = parse.issue().parseViewIssuePage();
        for (final Map.Entry<String, Object> entry : fieldValues.entrySet()) {
            final String actualValue = details.getCustomFields().get(entry.getKey());
            if (actualValue == null) {
                fail("Unable to find a value for field '" + entry.getKey() + "'.");
            } else {
                if (entry.getValue() instanceof String[]) {
                    for (final String v : (String[]) entry.getValue()) {
                        textAssertions.assertTextPresent(actualValue, v);
                    }
                } else {
                    textAssertions.assertTextPresent(actualValue, entry.getValue().toString());
                }
            }
        }
    }

    /**
     * On the edit issue page, assert that the expected values of the issue are set in TestEditIssueWithNoChanges.xml
     * import file (Note: need to navigate to the edit issue page before calling this method)
     */
    private void assertInitialEditIssueFieldValues() {
        final WebForm form = this.form.switchTo("issue-edit");
        assertEquals("test edit issue with no changes", form.getParameterValue("summary"));
        assertions.getProjectFieldsAssertions().assertIssueTypeIsSelected(ISSUE_TYPE_IMPROVEMENT);
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("priority", PRIORITY_TRIVIAL);
        assertThat(form.getParameterValue("duedate"), equalTo("14/Feb/07"));
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("components", COMPONENT_NAME_ONE);
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("components", COMPONENT_NAME_THREE);
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("versions", VERSION_NAME_ONE);
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("versions", VERSION_NAME_THREE);
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("fixVersions", VERSION_NAME_TWO);
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("assignee", ADMIN_FULLNAME);
        assertThat(form.getParameterValue("reporter"), equalTo(ADMIN_USERNAME));
        assertThat(form.getParameterValue("environment").trim(), equalTo("test environment 1"));
        assertThat(form.getParameterValue("description").trim(), equalTo("test editing issue without any changes"));
        assertThat(form.getParameterValue("timetracking"), equalTo("3d"));
        //cascading select
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("customfield_10000", "value 1");
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("customfield_10000:1", "value 1.2");
        //date picker
        assertThat(form.getParameterValue("customfield_10001"), equalTo("13/Feb/07"));
        //date time
        assertThat(form.getParameterValue("customfield_10002"), equalTo("12/Feb/07 11:26 AM"));
        //free text
        assertThat(form.getParameterValue("customfield_10003").trim(), equalTo("this is a free text"));
        //group picker
        assertThat(form.getParameterValue("customfield_10004"), equalTo(Groups.USERS));
        //Multi checkboxes
        tester.checkCheckbox("customfield_10006", "10008");
        //Multi group picker
        assertThat(form.getParameterValue("customfield_10007"), equalTo(Groups.DEVELOPERS + ", " + Groups.USERS));
        //Multi select
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("customfield_10008", "value 1");
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("customfield_10008", "value 2");
        //Multi user picker
        assertThat(form.getParameterValue("customfield_10009").trim(), equalTo(ADMIN_USERNAME));
        //Number
        assertThat(form.getParameterValue("customfield_10010"), equalTo("12345"));
        //Project picker
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("customfield_10011", PROJECT_MONKEY);
        //Radio
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelectedById("customfield_10012", "10014");
        //Select List
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("customfield_10014", "value 3");
        //Single version picker
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("customfield_10015", VERSION_NAME_THREE);
        //text field
        assertThat(form.getParameterValue("customfield_10016"), equalTo("text field"));
        //url
        assertThat(form.getParameterValue("customfield_10017"), equalTo("http://www.atlassian.com"));
        //user picker
        assertThat(form.getParameterValue("customfield_10018"), equalTo(ADMIN_USERNAME));
        //version picker
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("customfield_10019", VERSION_NAME_ONE);
        assertions.getJiraFormAssertions().assertSelectElementByIdHasOptionSelected("customfield_10019", VERSION_NAME_THREE);
    }

    private void logWorkOnIssue(final String issueKey, final String timeLogged) {
        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("log-work");
        tester.setFormElement("timeLogged", timeLogged);
        tester.submit();
    }

    private void prepareIssuesForEditIssueTests() {

        final Map<String, Priority> priorityMap = getPriorityNameToObjectMap();
        final Map<String, IssueTypeControl.IssueType> issueTypeMap = getIssueTypeNameToObjectMap();

        issueKeyNormal = backdoor.issues().createIssue(
                PROJECT_HOMOSAP_KEY,
                "test with components",
                ADMIN_USERNAME,
                priorityMap.get(PRIORITY_MINOR).getId(),
                issueTypeMap.get(ISSUE_TYPE_BUG).getId()).key();
        backdoor.issues().setIssueFields(issueKeyNormal, new IssueFields()
                .components(ResourceRef.withName(COMPONENT_NAME_ONE))
                .versions(ResourceRef.withName(VERSION_NAME_ONE))
                .fixVersions(ResourceRef.withName(VERSION_NAME_ONE))
                .description("test description normal issue for editing")
                .environment("test environment 1"));

        issueKeyWithNoComponents = backdoor.issues().createIssue(
                PROJECT_HOMOSAP_KEY,
                "test without components or versions",
                ADMIN_USERNAME,
                priorityMap.get(PRIORITY_MINOR).getId(),
                issueTypeMap.get(ISSUE_TYPE_BUG).getId()).key();
        backdoor.issues().setIssueFields(issueKeyWithNoComponents, new IssueFields()
                .description("test description issue with components")
                .environment("test environment 2"));

        timeTracking.enable(LEGACY);
        issueKeyWithTimeTracking = backdoor.issues().createIssue(
                PROJECT_HOMOSAP_KEY,
                "test with time tracking",
                ADMIN_USERNAME,
                priorityMap.get(PRIORITY_CRITICAL).getId(),
                issueTypeMap.get(ISSUE_TYPE_NEWFEATURE).getId()).key();
        backdoor.issues().setIssueFields(issueKeyWithTimeTracking, new IssueFields()
                .description("test description issue with time tracking")
                .environment("test environment 3")
                .timeTracking(new com.atlassian.jira.rest.api.issue.TimeTracking("1w", null)));
        timeTracking.disable();
    }

    private Map<String, Priority> getPriorityNameToObjectMap() {
        return backdoor.getTestkit().priorities().getPriorities().stream()
                .collect(toMap(Priority::getName, Function.<Priority>identity()));
    }

    private String getIdOfPriority(final String priorityName) {
        return backdoor.getTestkit().priorities().getPriorities().stream()
                .filter(c -> c.getName().equals(priorityName))
                .map(Priority::getId)
                .findAny()
                .orElseThrow(AssertionError::new);
    }

    private Map<String, IssueTypeControl.IssueType> getIssueTypeNameToObjectMap() {
        return backdoor.getTestkit().issueType().getIssueTypesForProject(PROJECT_HOMOSAP_KEY).stream()
                .collect(toMap(IssueTypeControl.IssueType::getName, Function.<IssueTypeControl.IssueType>identity()));

    }

    private String getIdOfIssueType(final String issueTypeName) {
        return backdoor.getTestkit().issueType().getIssueTypesForProject(PROJECT_HOMOSAP_KEY).stream()
                .filter(t -> t.getName().equals(issueTypeName))
                .map(IssueTypeControl.IssueType::getId)
                .findAny()
                .orElseThrow(AssertionError::new);
    }
}
