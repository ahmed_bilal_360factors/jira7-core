package com.atlassian.jira.functest.rule;

import com.atlassian.jira.testkit.client.AttachmentsControl;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.io.File;
import java.util.function.Supplier;

/**
 * Cleans up attachment files before and after a test.
 *
 * @since v6.4
 */
class CleanAttachmentsRule implements TestRule {
    private final Supplier<AttachmentsControl> attachmentsControlSupplier;

    CleanAttachmentsRule(final Supplier<AttachmentsControl> attachmentsControlSupplier) {
        this.attachmentsControlSupplier = attachmentsControlSupplier;
    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                final String attachmentPath = attachmentsControlSupplier.get().getAttachmentPath();
                final File attachmentDirectory = new File(attachmentPath);
                final TestRule deleteDirectoryRule = Rules.cleanDirectory(attachmentDirectory);
                deleteDirectoryRule.apply(base, description).evaluate();
            }
        };
    }
}
