package com.atlassian.jira.functest.rule;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.util.function.Supplier;

/**
 * The rule which executes data restore before tests are executed basing on {@link
 * com.atlassian.integrationtesting.runner.restore.Restore} and {@link com.atlassian.jira.functest.framework.RestoreBlankInstance}
 * annotations.
 *
 * @since v6.5
 */
public class RestoreDataRule implements TestRule {
    private final Supplier<Backdoor> backdoorSupplier;

    public RestoreDataRule(final Supplier<Backdoor> backdoorSupplier) {
        this.backdoorSupplier = backdoorSupplier;
    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                tryInitializeData(description);
                base.evaluate();
            }
        };
    }

    private void tryInitializeData(final Description description) {
        if (!restoreByAnnotation(description.getAnnotation(RestoreBlankInstance.class),
                description.getAnnotation(Restore.class))) {
            //try restore by annotation on class level
            final Class<?> testClass = description.getTestClass();
            restoreByAnnotation(testClass.getAnnotation(RestoreBlankInstance.class),
                    testClass.getAnnotation(Restore.class));
        }
    }

    private boolean restoreByAnnotation(final RestoreBlankInstance restoreBlank, final Restore restore) {
        if (restoreBlank != null && restore != null) {
            throw new IllegalArgumentException("Use only one of Restore and RestoreBlankInstance");
        } else if (restoreBlank != null) {
            final Backdoor backdoor = backdoorSupplier.get();
            backdoor.restoreBlankInstance();
            backdoor.flags().clearFlags();
            return true;
        } else if (restore != null) {
            final Backdoor backdoor = backdoorSupplier.get();
            backdoor.restoreDataFromResource(restore.value());
            backdoor.flags().clearFlags();
            return true;
        }
        return false;
    }
}

