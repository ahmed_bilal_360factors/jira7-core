package com.atlassian.jira.webtests.ztests.bundledplugins2;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.ISSUES})
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueTabPanels extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("TestIssueTabPanels.xml");
    }

    /**
     * Check that the Activity Stream tab is not sortable (JRA-17973)
     */
    @Test
    public void testActivityStreamIsNotSortable() {
        tester.gotoPage("browse/MKY-1?page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel");
        // Assert that the Activity Stream is not Active
        tester.assertTextNotPresent("<li id=\"activity-stream-issue-tab\" class=\"active\"><strong>Activity</strong></li>");
        textAssertions.assertTextPresent(new WebPageLocator(tester), "Ascending order");

        tester.gotoPage("browse/MKY-1?page=com.atlassian.streams.streams-jira-plugin%3Aactivity-stream-issue-tab");
        // Assert that the Activity Stream is Active
        assertThat(locator.css("li#activity-stream-issue-tab.active").getText(), equalTo("Activity"));
        assertThat(locator.css("iframe#gadget-0").getNode(), not(equalTo(null)));
        textAssertions.assertTextNotPresent(new WebPageLocator(tester), "Ascending order");
    }
}
