package com.atlassian.jira.webtests.ztests.security;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Responsible for testing the retrieval of static web-resources does not expose protected resources.
 *
 * @since v5.2
 */
@WebTest({Category.FUNC_TEST, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
public class TestWebResourceRetrievalDoesNotExposeProtectedResources extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
    }

    @Test
    public void testWebInfResourceCannotBeRetrieved() throws Exception {
        tester.getDialog().getWebClient().setExceptionsThrownOnErrorStatus(false);

        tester.gotoPage("s/1519/3/1.0/_/WEB-INF/classes/seraph-config.xml");

        Assert.assertEquals(tester.getDialog().getResponse().getResponseCode(), 404);
        Assert.assertTrue(tester.getDialog().getResponse().getText().contains("dead link"));

        tester.getDialog().getWebClient().setExceptionsThrownOnErrorStatus(true);
    }

    @Test
    public void testWebInfResourceCannotBeRetrievedWithParentTransversal() throws Exception {
        tester.getDialog().getWebClient().setExceptionsThrownOnErrorStatus(false);

        tester.gotoPage("s/1519/3/1.0/_/WEB-INF/images/../classes/seraph-config.xml");

        Assert.assertEquals(tester.getDialog().getResponse().getResponseCode(), 404);
        Assert.assertTrue(tester.getDialog().getResponse().getText().contains("dead link"));

        tester.getDialog().getWebClient().setExceptionsThrownOnErrorStatus(true);
    }
}
