package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.httpclient.api.HttpStatus;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.IssueUpdateRequest;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Attachment;
import com.atlassian.jira.testkit.client.restclient.AttachmentClient;
import com.atlassian.jira.testkit.client.restclient.Comment;
import com.atlassian.jira.testkit.client.restclient.CommentClient;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.jira.testkit.client.restclient.IssueLink;
import com.atlassian.jira.testkit.client.restclient.LinkIssueClient;
import com.atlassian.jira.testkit.client.restclient.LinkRequest;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.atlassian.jira.testkit.client.restclient.ProjectClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.testkit.client.restclient.Vote;
import com.atlassian.jira.testkit.client.restclient.VotesClient;
import com.atlassian.jira.testkit.client.restclient.WatchersClient;
import com.atlassian.jira.testkit.client.restclient.Watches;
import com.atlassian.jira.testkit.client.restclient.Worklog;
import com.atlassian.jira.testkit.client.restclient.WorklogClient;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.rest.api.issue.ResourceRef.withId;
import static com.atlassian.jira.rest.api.issue.ResourceRef.withKey;
import static com.atlassian.jira.rest.api.issue.ResourceRef.withName;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

/**
 * This func tests verify that administrators that are not associated with an application role can not access Issues or
 * Projects
 * <p/>
 * In the data, JOHN_ADMIN is an administrator that has all the project permissions for project TWO but is not
 * associated with any application role by default.
 * <p/>
 * User ADMIN is the standard administrator for JIRA.
 * <p/>
 * Issue TWO-1 has been created by JOHN_ADMIN, watched by both JOHN_ADMIN and ADMIN, voted by none.
 *
 * @since v7.0
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.ISSUES})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestAdminsWithoutAppRoles extends BaseJiraFuncTest {
    private static final String JOHN_ADMIN = "john";

    private static final String JOHNS_ISSUE_KEY = "TWO-1";
    private static final String UNASSIGNED_ISSUE = "TWO-2";
    private static final String JOHNS_PROJECT = "TWO";
    private static final String OTHER_PROJECT = "ONE";

    private Comment adminsComment = new Comment();
    private Comment johnsComment = new Comment();

    private Worklog adminsWorklog = new Worklog();
    private Worklog johnsWorklog = new Worklog();

    private Attachment johnsAttachment = new Attachment();
    private Attachment adminsAttachment = new Attachment();

    private IssueClient client;
    private ProjectClient projectClient;
    private LinkIssueClient linkIssueClient;
    private WatchersClient watchersClient;
    private VotesClient votesClient;
    private CommentClient commentClient;
    private WorklogClient worklogClient;
    private AttachmentClient attachmentClient;
    private AttachFileClient attachFileClient;
    @Inject
    private Administration administration;
    @Inject
    private LocatorFactory locator;
    @Inject
    private TextAssertions textAssertions;

    {
        adminsWorklog.id = String.valueOf(10000);
        adminsWorklog.timeSpent = "2h";

        johnsWorklog.id = String.valueOf(10001);
        johnsWorklog.timeSpent = "4h";

        adminsComment.id = String.valueOf(10100);
        adminsComment.body = "Other Comment";

        johnsComment.id = String.valueOf(10000);
        johnsComment.body = "My Comment";

        johnsAttachment.id = String.valueOf(10000);
        johnsAttachment.filename = "Hello.txt";

        adminsAttachment.id = String.valueOf(10100);
        adminsAttachment.filename = "admin.txt";
    }

    private static <I, O> List<O> map(Collection<I> input, Function<? super I, ? extends O> func) {
        return input.stream().map(func).collect(CollectorsUtil.toImmutableList());
    }

    private static <T> Matcher<Iterable<? extends T>> containsInAnyOrder(Collection<T> items) {
        return Matchers.containsInAnyOrder(map(items, Matchers::equalTo));
    }

    @Before
    public void setUpTest() {
        administration.restoreData("TestAdminAccessNoAppRole.zip");

        client = new IssueClient(environmentData).loginAs(JOHN_ADMIN);
        projectClient = new ProjectClient(environmentData).loginAs(JOHN_ADMIN);
        linkIssueClient = new LinkIssueClient(environmentData).loginAs(JOHN_ADMIN);
        commentClient = new CommentClient(environmentData).loginAs(JOHN_ADMIN);
        worklogClient = new WorklogClient(environmentData).loginAs(JOHN_ADMIN);
        attachmentClient = new AttachmentClient(environmentData).loginAs(JOHN_ADMIN);
        votesClient = new VotesClient(environmentData).loginAs(JOHN_ADMIN);
        watchersClient = new WatchersClient(environmentData).loginAs(JOHN_ADMIN);
        attachFileClient = new AttachFileClient(environmentData).loginAs(JOHN_ADMIN);
    }

    @Test
    public void testAdminWithoutApplicationCantSeeWatchersAndVoters() {
        votesClient.loginAs(ADMIN_USERNAME);
        votesClient.postResponse(JOHNS_ISSUE_KEY);

        removeJohnFromSoftware();
        votesClient.loginAs(JOHN_ADMIN);

        //This call will fail because John can't see voters for issue.
        Response<?> watchResponce = watchersClient.getResponse(JOHNS_ISSUE_KEY);
        assertThat(watchResponce.statusCode, is(HttpStatus.FORBIDDEN.code));
        Response<?> voteResponce = votesClient.getResponse(JOHNS_ISSUE_KEY);
        assertThat(voteResponce.statusCode, is(HttpStatus.FORBIDDEN.code));

        //This should work because he has access to an application.
        addJohnToSoftware();
        Watches watches = watchersClient.get(JOHNS_ISSUE_KEY);
        assertThat(watches.watchCount, is(2L));
        Vote vote = votesClient.get(JOHNS_ISSUE_KEY);
        assertThat(vote.votes, equalTo(1));
        assertThat(vote.voters.stream().map(user -> user.name).collect(CollectorsUtil.toImmutableList()), hasItems(ADMIN_USERNAME));
    }

    @Test
    public void testAdminWithoutApplicationCantModifyWatchers() {
        removeJohnFromSoftware();

        //This call will fail because John can't see voters for issue.
        Response<?> modifyWatchResponce = watchersClient.deleteResponse(JOHNS_ISSUE_KEY, ADMIN_USERNAME);
        assertThat(modifyWatchResponce.statusCode, is(HttpStatus.FORBIDDEN.code));

        //This should work because he has access to an application.
        addJohnToSoftware();
        modifyWatchResponce = watchersClient.deleteResponse(JOHNS_ISSUE_KEY, ADMIN_USERNAME);
        assertThat(modifyWatchResponce.statusCode, is(HttpStatus.NO_CONTENT.code));
    }

    @Test
    public void testAdminWithoutApplicationCantModifyVoters() {
        addJohnToSoftware();
        votesClient.postResponse(UNASSIGNED_ISSUE);
        votesClient.get(UNASSIGNED_ISSUE);

        removeJohnFromSoftware();

        //This call will fail because John can't see voters for issue.
        votesClient.loginAs(JOHN_ADMIN);
        Response<?> modifyVoteResponce = votesClient.deleteResponse(UNASSIGNED_ISSUE);
        assertThat(modifyVoteResponce.statusCode, is(HttpStatus.FORBIDDEN.code));

        //This should work because he has access to an application.
        addJohnToSoftware();
        modifyVoteResponce = votesClient.deleteResponse(UNASSIGNED_ISSUE);
        assertThat(modifyVoteResponce.statusCode, is(HttpStatus.NO_CONTENT.code));
    }

    @Test
    public void testAdminWithoutApplicationCantVote() {
        removeJohnFromSoftware();

        //This call will fail because John can't see voters for issue.
        votesClient.loginAs(JOHN_ADMIN);
        Response<?> modifyVoteResponce = votesClient.postResponse(UNASSIGNED_ISSUE);
        assertThat(modifyVoteResponce.statusCode, is(HttpStatus.FORBIDDEN.code));

        //This should work because he has access to an application.
        addJohnToSoftware();
        modifyVoteResponce = votesClient.postResponse(UNASSIGNED_ISSUE);
        assertThat(modifyVoteResponce.statusCode, is(HttpStatus.NO_CONTENT.code));
    }

    @Test
    public void testAdminWithoutApplicationCantRemoveVote() {
        addJohnToSoftware();
        votesClient.postResponse(UNASSIGNED_ISSUE);
        votesClient.get(UNASSIGNED_ISSUE);

        removeJohnFromSoftware();

        //This call will fail because John can't see voters for issue.
        votesClient.loginAs(JOHN_ADMIN);
        Response<?> modifyVoteResponce = votesClient.deleteResponse(UNASSIGNED_ISSUE);
        assertThat(modifyVoteResponce.statusCode, is(HttpStatus.FORBIDDEN.code));

        //This should work because he has access to an application.
        addJohnToSoftware();
        modifyVoteResponce = votesClient.deleteResponse(UNASSIGNED_ISSUE);
        assertThat(modifyVoteResponce.statusCode, is(HttpStatus.NO_CONTENT.code));
    }

    @Test
    public void testAdminWithoutApplicationCantReadIssueThroughUI() {
        navigation.login(JOHN_ADMIN);
        navigation.issue().viewIssue(JOHNS_ISSUE_KEY);
        assertions.getViewIssueAssertions().assertIssueNotFound();
    }

    @Test
    public void testAdminWithoutApplicationCantReadIssue() {
        addJohnToSoftware();

        //This will fail because John isn't in an application.
        Issue issue = client.get(JOHNS_ISSUE_KEY);
        assertThat(issue.key, equalTo(JOHNS_ISSUE_KEY));

        removeJohnFromSoftware();

        //John should no longer have permission to view any issue.
        Response<?> response = client.getResponse(JOHNS_ISSUE_KEY);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));
    }

    @Test
    public void testAdminWithoutApplicationCantBeAssignedToIssue() {
        client.loginAs(ADMIN_USERNAME);

        removeJohnFromSoftware();

        IssueUpdateRequest updateSummaryRequest = new IssueUpdateRequest()
                .fields(new IssueFields().assignee(withName(JOHN_ADMIN)));
        //This will fail because John isn't in an application.
        Response<?> updateResponse = client.updateResponse(UNASSIGNED_ISSUE, updateSummaryRequest);
        assertThat(updateResponse.statusCode, is(HttpStatus.BAD_REQUEST.code));
        Issue issue = client.get(UNASSIGNED_ISSUE);
        assertThat(issue.fields.assignee, nullValue());

        addJohnToSoftware();
        //This should work because he has access to an application.
        updateResponse = client.updateResponse(UNASSIGNED_ISSUE, updateSummaryRequest);
        assertThat(updateResponse.statusCode, is(HttpStatus.NO_CONTENT.code));
        issue = client.get(UNASSIGNED_ISSUE);
        assertThat(issue.fields.assignee.name, equalTo(JOHN_ADMIN));
    }

    @Test
    public void testAdminWithoutApplicationCantAssignIssue() {
        removeJohnFromSoftware();

        IssueUpdateRequest updateSummaryRequest = new IssueUpdateRequest().fields(new IssueFields()
                .assignee(withName(ADMIN_USERNAME)));
        //This call will fail because John can't assign issues.
        Response<?> updateResponse = client.updateResponse(JOHNS_ISSUE_KEY, updateSummaryRequest);
        assertThat(updateResponse.statusCode, is(HttpStatus.FORBIDDEN.code));

        addJohnToSoftware();
        Issue issue = client.get(JOHNS_ISSUE_KEY);
        assertThat(issue.fields.assignee.name, equalTo(JOHN_ADMIN));

        //This should work because John is in software again.
        updateResponse = client.updateResponse(JOHNS_ISSUE_KEY, updateSummaryRequest);
        assertThat(updateResponse.statusCode, is(HttpStatus.NO_CONTENT.code));
        issue = client.get(JOHNS_ISSUE_KEY);
        assertThat(issue.fields.assignee.name, equalTo(ADMIN_USERNAME));
    }

    @Test
    public void testAdminWithoutApplicationCantAssignIssueUI() {
        navigation.login(JOHN_ADMIN);
        tester.gotoPage("/secure/AssignIssue!default.jspa?id=10100");
        assertThat(locator.id("assign-error").exists(), is(true));
    }

    @Test
    public void testAdminWithoutApplicationCantCreateIssue() {
        removeJohnFromSoftware();

        IssueUpdateRequest issueUpdateRequest = new IssueUpdateRequest();
        issueUpdateRequest.fields(new IssueFields()
                .issueType(withName("Task"))
                .project(withKey(JOHNS_PROJECT)));

        //This will fail because John isn't in an application.
        Response<?> createResponse = client.getResponse(issueUpdateRequest);
        assertThat(createResponse.statusCode, is(HttpStatus.FORBIDDEN.code));

        addJohnToSoftware();

        final String summary = "Create issue";
        final String priority = "Medium";
        issueUpdateRequest.fields().summary(summary).priority(withName(priority));
        //This should work because John is in software again.
        final IssueCreateResponse goodCreateResponse = client.create(issueUpdateRequest);

        Issue issue = client.get(goodCreateResponse.key);
        assertThat(issue.fields.summary, equalTo(summary));
        assertThat(issue.fields.priority.name(), equalTo(priority));
    }

    @Test
    public void testAdminWithoutApplicationCantDeleteIssue() {
        removeJohnFromSoftware();

        //This will fail because John isn't in an application.
        Response<?> response = client.delete(JOHNS_ISSUE_KEY, null);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        addJohnToSoftware();

        Issue issue = client.get(JOHNS_ISSUE_KEY);
        assertThat(issue.key, equalTo(JOHNS_ISSUE_KEY));

        //This should work because John is in software again.
        response = client.delete(JOHNS_ISSUE_KEY, null);
        assertThat(response.statusCode, is(HttpStatus.NO_CONTENT.code));

        response = client.getResponse(JOHNS_ISSUE_KEY);
        assertThat(response.statusCode, is(HttpStatus.NOT_FOUND.code));
    }

    @Test
    public void testAdminWithoutApplicationCantEditIssue() {
        final String oldSummary = "John's own task";
        final String newSummary = "This is the new description";

        removeJohnFromSoftware();
        IssueUpdateRequest request = new IssueUpdateRequest().fields(new IssueFields());
        request.fields().summary(newSummary);

        //This will fail because John isn't in an application.
        Response<?> response = client.updateResponse(JOHNS_ISSUE_KEY, request);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        addJohnToSoftware();

        Issue issue = client.get(JOHNS_ISSUE_KEY);
        assertThat(issue.fields.summary, equalTo(oldSummary));

        //This should work because John is in software again.
        response = client.updateResponse(JOHNS_ISSUE_KEY, request);
        assertThat(response.statusCode, is(HttpStatus.NO_CONTENT.code));

        issue = client.get(JOHNS_ISSUE_KEY);
        assertThat(issue.fields.summary, equalTo(newSummary));
    }

    @Test
    public void testAdminWithoutApplicationCantLinkIssue() {
        final String linkTypeName = "Blocks";
        final LinkRequest linkRequest = new LinkRequest()
                .inwardIssue(withKey(JOHNS_ISSUE_KEY))
                .outwardIssue(withKey(UNASSIGNED_ISSUE))
                .type(withName(linkTypeName));

        removeJohnFromSoftware();
        //This will fail because John isn't in an application.
        Response response = linkIssueClient.linkIssues(linkRequest);
        //This resource returns 404 when issue is not visible.
        assertThat(response.statusCode, is(HttpStatus.NOT_FOUND.code));

        addJohnToSoftware();

        Issue issue = client.get(JOHNS_ISSUE_KEY);
        assertThat(issue.fields.issuelinks.isEmpty(), equalTo(true));

        //This should work because John is in software again.
        response = linkIssueClient.linkIssues(linkRequest);
        assertThat(response.statusCode, is(HttpStatus.CREATED.code));

        issue = client.get(JOHNS_ISSUE_KEY);
        assertThat(issue.fields.issuelinks.size(), equalTo(1));
        final IssueLink issueLink = issue.fields.issuelinks.get(0);
        assertThat(issueLink.inwardIssue(), nullValue());
        assertThat(issueLink.outwardIssue().key(), equalTo(UNASSIGNED_ISSUE));
        assertThat(issueLink.type().name(), equalTo(linkTypeName));
    }

    @Test
    public void testAdminWithoutApplicationCantModifyReporters() {
        removeJohnFromSoftware();
        //This will fail because John isn't in an application.
        IssueUpdateRequest request = new IssueUpdateRequest().fields(new IssueFields());
        request.fields().reporter(withName(ADMIN_USERNAME));

        Response<?> response = client.updateResponse(JOHNS_ISSUE_KEY, request);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        addJohnToSoftware();

        Issue issue = client.get(JOHNS_ISSUE_KEY);
        assertThat(issue.fields.reporter.name, equalTo(JOHN_ADMIN));

        //This should work because John is in software again.
        response = client.updateResponse(JOHNS_ISSUE_KEY, request);
        assertThat(response.statusCode, is(HttpStatus.NO_CONTENT.code));

        issue = client.get(JOHNS_ISSUE_KEY);
        assertThat(issue.fields.reporter.name, equalTo(ADMIN_USERNAME));
    }

    @Test
    public void testAdminWithoutApplicationCantResolveIssue() {
        assertTransition(21, "To Do", "Done");
    }

    @Test
    public void testAdminWithoutApplicationCantScheduleIssue() {
        final String dueDate = "2030-01-01";

        removeJohnFromSoftware();

        IssueUpdateRequest request = new IssueUpdateRequest().fields(new IssueFields());
        request.fields().dueDate(dueDate);

        //This will fail because John isn't in an application.
        Response<?> response = client.updateResponse(JOHNS_ISSUE_KEY, request);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        addJohnToSoftware();

        Issue issue = client.get(JOHNS_ISSUE_KEY);
        assertThat(issue.fields.duedate, nullValue());

        //This should work because John is in software again.
        response = client.updateResponse(JOHNS_ISSUE_KEY, request);
        assertThat(response.statusCode, is(HttpStatus.NO_CONTENT.code));

        issue = client.get(JOHNS_ISSUE_KEY);
        assertThat(issue.fields.duedate, equalTo(dueDate));
    }

    @Test
    public void testAdminWithoutApplicationCantSetIssueSecurity() {
        final String securityLevel = "Secret";

        removeJohnFromSoftware();

        IssueUpdateRequest request = new IssueUpdateRequest().fields(new IssueFields());
        request.fields().securityLevel(withName(securityLevel));

        //This will fail because John isn't in an application.
        Response<?> response = client.updateResponse(JOHNS_ISSUE_KEY, request);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        addJohnToSoftware();

        Issue issue = client.get(JOHNS_ISSUE_KEY);
        assertThat(issue.fields.security, nullValue());

        //This should work because John is in software again.
        response = client.updateResponse(JOHNS_ISSUE_KEY, request);
        assertThat(response.statusCode, is(HttpStatus.NO_CONTENT.code));

        issue = client.get(JOHNS_ISSUE_KEY);
        assertThat(issue.fields.security.name, equalTo(securityLevel));
    }

    @Test
    public void testAdminWithoutApplicationCantTransitionIssue() {
        assertTransition(71, "To Do", "Other Status");
    }

    @Test
    public void testAdminWithoutApplicationCantCloseIssue() {
        assertTransition(81, "To Do", "Closed");
    }

    /**
     * The admin can view projects because he has permission to admin them.
     */
    @Test
    public void testViewProjectsAllowedForAdmin() {
        removeJohnFromSoftware();

        final List<String> collect = projectClient.getProjects()
                .stream().map(p -> p.key)
                .collect(CollectorsUtil.toImmutableList());

        assertThat(collect, Matchers.containsInAnyOrder(JOHNS_PROJECT, OTHER_PROJECT));

        Project project = projectClient.get(JOHNS_PROJECT);
        assertThat(project.key, equalTo(JOHNS_PROJECT));

        project = projectClient.get(OTHER_PROJECT);
        assertThat(project.key, equalTo(OTHER_PROJECT));
    }

    @Test
    public void testAdminWithoutApplicationCantComment() {
        removeJohnFromSoftware();

        final Comment newComment = new Comment();
        newComment.body = "Something";

        commentClient.post(JOHNS_ISSUE_KEY, newComment);

        //This will fail because John doesn't have an application.
        Response<Comment> response = commentClient.post(JOHNS_ISSUE_KEY, newComment);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        //This should work because John is in software again.
        addJohnToSoftware();

        assertComments(johnsComment, adminsComment);

        response = commentClient.post(JOHNS_ISSUE_KEY, newComment);
        assertThat(response.statusCode, is(HttpStatus.CREATED.code));

        assertComments(johnsComment, newComment, adminsComment);
    }

    @Test
    public void testAdminWithoutApplicationCantDeleteOwnComment() {
        removeJohnFromSoftware();

        //This will fail because John doesn't have an application.
        Response<?> response = commentClient.delete(JOHNS_ISSUE_KEY, johnsComment.id);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        //This should work because John is in software again.
        addJohnToSoftware();

        assertComments(johnsComment, adminsComment);

        response = commentClient.delete(JOHNS_ISSUE_KEY, johnsComment.id);
        assertThat(response.statusCode, is(HttpStatus.NO_CONTENT.code));

        assertComments(adminsComment);
    }

    @Test
    public void testAdminWithoutApplicationCantDeleteOtherComments() {
        removeJohnFromSoftware();

        //This will fail because John doesn't have an application.
        Response<?> response = commentClient.delete(JOHNS_ISSUE_KEY, adminsComment.id);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        //This should work because John is in software again.
        addJohnToSoftware();

        assertComments(johnsComment, adminsComment);

        response = commentClient.delete(JOHNS_ISSUE_KEY, adminsComment.id);
        assertThat(response.statusCode, is(HttpStatus.NO_CONTENT.code));

        assertComments(johnsComment);
    }

    @Test
    public void testAdminWithoutApplicationCantEditOwnComment() {
        removeJohnFromSoftware();

        final Comment editedComment = new Comment();
        editedComment.id = johnsComment.id;
        editedComment.body = "Something";

        //This will fail because John doesn't have an application.
        Response<Comment> response = commentClient.put(JOHNS_ISSUE_KEY, editedComment);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        //This should work because John is in software again.
        addJohnToSoftware();

        assertComments(johnsComment, adminsComment);

        response = commentClient.put(JOHNS_ISSUE_KEY, editedComment);
        assertThat(response.statusCode, is(HttpStatus.OK.code));
        assertThat(response.body.body, equalTo(editedComment.body));

        assertComments(editedComment, adminsComment);
    }

    @Test
    public void testAdminWithoutApplicationCantEditOtherComment() {
        final String newBody = "New Body";

        final Comment otherComment = new Comment();
        otherComment.body = newBody;
        otherComment.id = adminsComment.id;

        removeJohnFromSoftware();

        //This will fail because John doesn't have an application.
        Response<Comment> response = commentClient.put(JOHNS_ISSUE_KEY, otherComment);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        //This should work because John is in software again.
        addJohnToSoftware();

        assertComments(adminsComment, johnsComment);

        response = commentClient.put(JOHNS_ISSUE_KEY, otherComment);
        assertThat(response.statusCode, is(HttpStatus.OK.code));
        assertThat(response.body.body, equalTo(newBody));

        assertComments(johnsComment, otherComment);
    }

    @Test
    public void testAdminWithoutApplicationCantDeleteOwnWorklogs() {
        removeJohnFromSoftware();

        //This will fail because John doesn't have an application.
        Response<?> response = worklogClient.delete(JOHNS_ISSUE_KEY, johnsWorklog);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        //This should work because John is in software again.
        addJohnToSoftware();

        assertWorklogs(johnsWorklog, adminsWorklog);

        response = worklogClient.delete(JOHNS_ISSUE_KEY, johnsWorklog);
        assertThat(response.statusCode, is(HttpStatus.NO_CONTENT.code));

        assertWorklogs(adminsWorklog);
    }

    @Test
    public void testAdminWithoutApplicationCantDeleteOtherWorklogs() {
        removeJohnFromSoftware();

        //This will fail because John doesn't have an application.
        Response<?> response = worklogClient.delete(JOHNS_ISSUE_KEY, adminsWorklog);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        //This should work because John is in software again.
        addJohnToSoftware();

        assertWorklogs(johnsWorklog, adminsWorklog);

        response = worklogClient.delete(JOHNS_ISSUE_KEY, adminsWorklog);
        assertThat(response.statusCode, is(HttpStatus.NO_CONTENT.code));

        assertWorklogs(johnsWorklog);
    }

    @Test
    public void testAdminWithoutApplicationCantEditOwnWorklogs() {
        Worklog log = new Worklog();
        log.id = johnsWorklog.id;
        log.timeSpent = "1h";

        removeJohnFromSoftware();

        //This will fail because John doesn't have an application.
        Response<Worklog> response = worklogClient.put(JOHNS_ISSUE_KEY, johnsWorklog);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        //This should work because John is in software again.
        addJohnToSoftware();

        assertWorklogs(johnsWorklog, adminsWorklog);

        response = worklogClient.put(JOHNS_ISSUE_KEY, log);
        assertThat(response.statusCode, is(HttpStatus.OK.code));

        assertWorklogs(adminsWorklog, log);
    }

    @Test
    public void testAdminWithoutApplicationCantEditOtherWorklogs() {
        Worklog log = new Worklog();
        log.id = adminsWorklog.id;
        log.timeSpent = "1h";

        removeJohnFromSoftware();

        //This will fail because John doesn't have an application.
        Response<?> response = worklogClient.put(JOHNS_ISSUE_KEY, adminsWorklog);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        //This should work because John is in software again.
        addJohnToSoftware();

        assertWorklogs(johnsWorklog, adminsWorklog);

        response = worklogClient.put(JOHNS_ISSUE_KEY, log);
        assertThat(response.statusCode, is(HttpStatus.OK.code));

        assertWorklogs(johnsWorklog, log);
    }

    @Test
    public void testAdminWithoutApplicationCantLogWork() {
        Worklog log = new Worklog();
        log.timeSpent = "1h";

        removeJohnFromSoftware();

        //This will fail because John doesn't have an application.
        Response<?> response = worklogClient.post(JOHNS_ISSUE_KEY, adminsWorklog);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        //This should work because John is in software again.
        addJohnToSoftware();

        assertWorklogs(johnsWorklog, adminsWorklog);

        response = worklogClient.post(JOHNS_ISSUE_KEY, log);
        assertThat(response.statusCode, is(HttpStatus.CREATED.code));

        assertWorklogs(johnsWorklog, log, adminsWorklog);
    }

    @Test
    public void testAdminWithoutApplicationCantDeleteOwnAttachments() {
        removeJohnFromSoftware();

        //This will fail because John doesn't have permission.
        Response<?> response = attachmentClient.deleteResponse(johnsAttachment.id);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        addJohnToSoftware();

        assertAttachments(johnsAttachment, adminsAttachment);

        response = attachmentClient.deleteResponse(johnsAttachment.id);
        assertThat(response.statusCode, equalTo(HttpStatus.NO_CONTENT.code));

        assertAttachments(adminsAttachment);
    }

    @Test
    public void testAdminWithoutApplicationCantDeleteOtherAttachments() {
        removeJohnFromSoftware();

        //This will fail because John doesn't have permission.
        Response<?> response = attachmentClient.deleteResponse(adminsAttachment.id);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        addJohnToSoftware();

        assertAttachments(johnsAttachment, adminsAttachment);

        response = attachmentClient.deleteResponse(adminsAttachment.id);
        assertThat(response.statusCode, equalTo(HttpStatus.NO_CONTENT.code));

        assertAttachments(johnsAttachment);
    }

    @Test
    public void testAdminWithoutApplicationCantCreateAttachments() {
        Attachment attachment = new Attachment();
        attachment.filename = "hack.txt";

        removeJohnFromSoftware();

        //This will fail because John doesn't have permission.
        Response<?> response = attachFileClient.attachData(JOHNS_ISSUE_KEY, "hello hack", attachment.filename);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        addJohnToSoftware();

        assertAttachments(johnsAttachment, adminsAttachment);

        response = attachFileClient.attachData(JOHNS_ISSUE_KEY, "hello hack", attachment.filename);
        assertThat(response.statusCode, equalTo(HttpStatus.OK.code));

        assertAttachments(johnsAttachment, adminsAttachment, attachment);
    }

    @Test
    public void testAdminWithoutApplicationCantMoveIssuesUI() {
        navigation.login(JOHN_ADMIN);

        removeJohnFromSoftware();

        tester.gotoPage("/secure/MoveIssue!default.jspa?id=10100");
        assertThat(locator.id("move-error").exists(), is(true));

        addJohnToSoftware();

        tester.gotoPage("/secure/MoveIssue!default.jspa?id=10100");
        assertThat(locator.id("move-error").exists(), is(false));
    }

    private void assertTransition(int transitionId, String currentStatus, String targetStatus) {
        removeJohnFromSoftware();

        IssueUpdateRequest request = new IssueUpdateRequest().fields(new IssueFields());
        request.transition(withId(String.valueOf(transitionId)));

        //This will fail because John doesn't have permission.
        Response<?> response = client.transition(JOHNS_ISSUE_KEY, request);
        assertThat(response.statusCode, is(HttpStatus.FORBIDDEN.code));

        addJohnToSoftware();

        Issue issue = client.get(JOHNS_ISSUE_KEY);
        assertThat(issue.fields.status.name(), equalTo(currentStatus));

        //This should work because John is in software again.
        response = client.transition(JOHNS_ISSUE_KEY, request);
        assertThat(response.statusCode, is(HttpStatus.NO_CONTENT.code));

        issue = client.get(JOHNS_ISSUE_KEY);
        assertThat(issue.fields.status.name(), equalTo(targetStatus));
    }

    private void assertComments(Comment... bodies) {
        final List<String> actualBodies = map(commentClient.getComments(JOHNS_ISSUE_KEY).body.getComments(), c -> c.body);
        final List<String> expectedBodies = map(Arrays.asList(bodies), c -> c.body);
        assertThat(actualBodies, containsInAnyOrder(expectedBodies));
    }

    private void assertWorklogs(Worklog... logs) {
        final List<String> actualTimes = map(worklogClient.getAll(JOHNS_ISSUE_KEY).worklogs, c -> c.timeSpent);
        final List<String> expectedTimes = map(Arrays.asList(logs), c -> c.timeSpent);
        assertThat(actualTimes, containsInAnyOrder(expectedTimes));
    }

    private void assertAttachments(Attachment... attachments) {
        final List<String> actualNames = map(client.get(JOHNS_ISSUE_KEY).fields.attachment, c -> c.filename);
        final List<String> expectedNames = map(Arrays.asList(attachments), c -> c.filename);
        assertThat(actualNames, containsInAnyOrder(expectedNames));
    }

    private void addJohnToSoftware() {
        backdoor.applicationRoles().putRole("jira-software", "jira-administrators", "other-admin");
    }

    private void removeJohnFromSoftware() {
        backdoor.applicationRoles().putRole("jira-software", "jira-administrators");
    }

    private static class AttachFileClient extends RestApiClient<AttachFileClient> {
        private final static GenericType<List<Attachment>> ATTACHMENT_LIST = new GenericType<List<Attachment>>() {
        };

        private AttachFileClient(final JIRAEnvironmentData environmentData) {
            super(environmentData);
        }

        public Response<List<Attachment>> attachData(String issue, String data, String name) {
            return toResponse(() ->
            {
                final FormDataContentDisposition dispo = FormDataContentDisposition
                        .name("file")
                        .fileName(name)
                        .build();

                return createResource().path("issue").path(issue).path("attachments")
                        .type(MediaType.MULTIPART_FORM_DATA_TYPE)
                        .accept(MediaType.APPLICATION_JSON_TYPE)
                        .header("X-Atlassian-Token", "no-check")
                        .post(ClientResponse.class, new FormDataMultiPart().bodyPart(new FormDataBodyPart(dispo, data)));
            }, ATTACHMENT_LIST);
        }
    }
}
