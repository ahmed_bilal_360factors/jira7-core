package com.atlassian.jira.functest.framework.admin.workflows;

import com.atlassian.jira.functest.framework.admin.ViewWorkflows;
import com.atlassian.jira.functest.framework.admin.WorkflowSteps;
import com.atlassian.jira.functest.framework.admin.WorkflowStepsImpl;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;

/**
 * Represents the workflow designer page shown for a given workflow.
 *
 * @since v5.1
 */
public class WorkflowDesignerPage {
    private final WebTester tester;
    private final ViewWorkflows viewWorkflows;
    private final TextView textView;

    public WorkflowDesignerPage(WebTester tester, JIRAEnvironmentData environmentData, int logIndentLevel, ViewWorkflows viewWorkflows) {
        this(tester, environmentData, viewWorkflows);
    }

    @Inject
    public WorkflowDesignerPage(WebTester tester, JIRAEnvironmentData environmentData, ViewWorkflows viewWorkflows) {
        this.tester = tester;
        this.viewWorkflows = viewWorkflows;
        this.textView = new TextView(this, tester, environmentData, 2);
    }

    public RenameWorkflowPage rename() {
        tester.clickLink("edit-workflow-trigger");
        return new RenameWorkflowPage(tester, viewWorkflows);
    }

    public TextView textView() {
        return textView;
    }

    public static class TextView {
        private final WorkflowDesignerPage workflowDesignerPage;
        private WebTester tester;
        private WorkflowSteps steps;

        public TextView(final WorkflowDesignerPage workflowDesignerPage, final WebTester tester, final JIRAEnvironmentData jiraEnvironmentData, int logIndentLevel) {
            this.workflowDesignerPage = workflowDesignerPage;
            this.tester = tester;
            this.steps = new WorkflowStepsImpl(tester, jiraEnvironmentData, logIndentLevel);
        }

        public TextView goTo() {
            tester.clickLink("workflow-text");
            return this;
        }

        public WorkflowSteps steps() {
            return steps;
        }
    }
}
