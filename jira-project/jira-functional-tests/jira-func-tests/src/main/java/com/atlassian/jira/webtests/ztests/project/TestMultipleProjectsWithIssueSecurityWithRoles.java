package com.atlassian.jira.webtests.ztests.project;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * JRA-12739
 */
@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.PROJECTS, Category.ROLES, Category.SECURITY})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestMultipleProjectsWithIssueSecurityWithRoles extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("TestMultipleProjectsWithIssueSecurityWithRoles.xml");
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    public void testIssueWithSecurityLevelNotAccessibleInNavigator() {
        //first login as a user not in the developer role.
        //Should not be able to see Test Issue 2 in project 1.
        //Should be able to see My Bug which has security level set to reporter
        navigation.logout();
        navigation.login("nondeveloper", "nondeveloper");
        navigation.issueNavigator().gotoNavigator();
        textAssertions.assertTextPresent(locator.page(), "Test Issue 1 (TP2)");
        textAssertions.assertTextNotPresent(locator.page(), "Test Issue 2 (TP1)");
        textAssertions.assertTextPresent(locator.page(), "Test Issue 1 (TP1)");
        textAssertions.assertTextPresent(locator.page(), "My Bug");
        navigation.issue().gotoIssue("TPONE-2");
        assertions.getViewIssueAssertions().assertIssueNotFound();

        // login as user in developer role.  Should be able to see Test Issue 2(TP1) but
        // not My Bug which has reporter security level
        navigation.logout();
        navigation.login("developer", "developer");
        navigation.issueNavigator().gotoNavigator();
        textAssertions.assertTextPresent(locator.page(), "Test Issue 1 (TP2)");
        textAssertions.assertTextPresent(locator.page(), "Test Issue 2 (TP1)");
        textAssertions.assertTextPresent(locator.page(), "Test Issue 1 (TP1)");
        textAssertions.assertTextNotPresent(locator.page(), "My Bug");
    }
}
