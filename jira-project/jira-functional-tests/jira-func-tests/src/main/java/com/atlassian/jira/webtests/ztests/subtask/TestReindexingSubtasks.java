package com.atlassian.jira.webtests.ztests.subtask;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperations;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.meterware.httpunit.WebTable;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static org.junit.Assert.assertEquals;

@WebTest({Category.FUNC_TEST, Category.SUB_TASKS})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestReindexingSubtasks.xml")
public class TestReindexingSubtasks extends BaseJiraFuncTest {

    @Inject
    BulkOperations bulkOperations;

    @Inject
    private HtmlPage page;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
    }

    @Test
    public void testEditSingleIssue() throws SAXException {
        assertPrecondition();
        WebTable issueTable = getIssuesTable();
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 1, "RAT-7");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 11, "Level Mouse");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 1, "RAT-6");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 11, "Level Mouse");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 1, "RAT-5");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 11, "Level Mouse");
        // Go to issue "RAT-5"
        tester.clickLinkWithText("RAT-5");
        // Edit issue
        tester.clickLink("edit-issue");
        // Assert current Security Level is "Level Mouse"
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "Level Mouse", "Priority"});
        // Change security to "Level KingRat"
        tester.selectOption("security", "Level KingRat");
        tester.submit("Update");
        navigation.issue().returnToSearch();
        issueTable = getIssuesTable();
        // Assert that all issues are set to "Level KingRat"
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 1, "RAT-7");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 11, "Level KingRat");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 1, "RAT-6");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 11, "Level KingRat");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 1, "RAT-5");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 11, "Level KingRat");
        tester.assertTextNotPresent("Level Mouse");
    }

    @Test
    public void testMoveSingleIssue() throws SAXException {
        assertPrecondition();

        //now let's go to the parent issue and do a move asserting each screen along the way.
        navigation.issue().gotoIssue("COW-35");
        tester.clickLink("move-issue");
        tester.assertTextPresent("Move Issue");
        navigation.issue().selectProject("Porcine", "10000_4_pid");
        navigation.issue().selectIssueType("Bug", "10000_4_issuetype");
        tester.submit("Next");
        tester.submit("Next");
        tester.assertTextPresent("Update Fields");
        tester.submit("Next");
        tester.submit("Next");
        tester.assertTextPresent("Below is a summary of all issues that will be moved");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Project", "Porcine"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Type", "Bug"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "None"});
        tester.submit("Next");
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        navigation.issueNavigator().displayAllIssues();
        //check the security level is gone
        tester.assertTextNotPresent("MyFriendsOnly");
        WebTable issueTable = getIssuesTable();
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 1, "PIG-12");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 2, "PIG-9");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 2, "Lets get a third milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 1, "PIG-11");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 2, "PIG-9");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 2, "Get another milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 1, "PIG-10");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 2, "PIG-9");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 2, "Get new milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 1, "PIG-9");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 2, "No more milk");

        //now lets move the issue to yet another project with a security level this time..
        navigation.issue().gotoIssue("PIG-9");
        tester.clickLink("move-issue");
        tester.assertTextPresent("Move Issue");
        navigation.issue().selectProject("Rattus", "10021_1_pid");
        tester.submit("Next");
        tester.submit("Next");
        tester.assertTextPresent("Update Fields");
        //set the security level.
        tester.selectOption("security", "Level KingRat");
        tester.submit("Next");
        tester.submit("Next");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Project", "Rattus"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Type", "Bug"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "Level KingRat"});
        tester.submit("Next");
        bulkOperations.waitAndReloadBulkOperationProgressPage();
        tester.assertTextPresent("Level KingRat");

        navigation.issueNavigator().displayAllIssues();
        issueTable = getIssuesTable();
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 1, "RAT-11");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 2, "RAT-8");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 2, "Lets get a third milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 11, "Level KingRat");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 1, "RAT-10");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 2, "RAT-8");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 2, "Get another milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 11, "Level KingRat");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 1, "RAT-9");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 2, "RAT-8");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 2, "Get new milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 11, "Level KingRat");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 1, "RAT-8");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 2, "No more milk");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 11, "Level KingRat");
    }

    @Test
    public void testWorkflowTransitionSingleIssue() throws SAXException {
        assertPrecondition();
        // Open the "RAT-5" issue
        tester.clickLinkWithText("RAT-5");
        tester.assertTextPresent("Details");
        tester.assertTextPresent("RAT-5");
        // Click the "Close issue" link
        tester.clickLink("action_id_2");
        tester.setWorkingForm("issue-workflow-transition");
        tester.assertTextPresent("Close Issue");
        tester.assertTextPresent("Closing an issue indicates that there is no more work to be done on it, and that it has been verified as complete.");
        // Change the security level to "Level KingRat" on our Workflow transition screen
        tester.selectOption("security", "Level KingRat");
        tester.submit("Transition");

        // Search for all issues again
        navigation.issueNavigator().displayAllIssues();
        // Assert that all issues are set to "Level KingRat"
        tester.assertTextNotPresent("Level Mouse");
        WebTable issueTable = getIssuesTable();
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 1, "RAT-7");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 11, "Level KingRat");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 1, "RAT-6");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 11, "Level KingRat");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 1, "RAT-5");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 11, "Level KingRat");

        // Open the "RAT-5" issue again
        tester.clickLinkWithText("RAT-5");
        // Re-open the issue
        tester.clickLink("action_id_3");
        tester.setWorkingForm("issue-workflow-transition");
        // Now set security to None
        tester.selectOption("security", "None");
        tester.submit("Transition");
        navigation.issueNavigator().displayAllIssues();
        // Assert that all issues are set to None
        tester.assertTextNotPresent("Level KingRat");
        tester.assertTextNotPresent("Level Mouse");
        issueTable = getIssuesTable();
        assertEquals("RAT-7", issueTable.getCellAsText(1, 1).trim());
        assertEquals("", issueTable.getCellAsText(1, 11).trim());
        assertEquals("RAT-6", issueTable.getCellAsText(2, 1).trim());
        assertEquals("", issueTable.getCellAsText(2, 11).trim());
        assertEquals("RAT-5", issueTable.getCellAsText(3, 1).trim());
        assertEquals("", issueTable.getCellAsText(3, 11).trim());
    }

    @Test
    public void testBulkEditIssue() throws SAXException {
        assertPrecondition();

        //try bulk editing a single parent issue.
        bulkOperations.bulkChangeIncludeAllPages();
        tester.checkCheckbox("bulkedit_10033", "on");
        tester.assertTextPresent("Step 1 of 4: Choose Issues");
        tester.submit("Next");
        tester.assertTextPresent("Step 2 of 4: Choose Operation");
        tester.checkCheckbox("operation", "bulk.edit.operation.name");
        tester.submit("Next");
        tester.checkCheckbox("actions", "security");
        tester.selectOption("security", "A");
        tester.submit("Next");
        tester.assertTextPresent("Step 4 of 4: Confirmation");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "A"});
        tester.submit("Confirm");
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        //we should be on the issue navigator now.
        tester.assertTextPresent("Issue Navigator");
        tester.assertTextNotPresent("MyFriendsOnly");

        //now lets edit parent and one of the 4 subtasks
        bulkOperations.bulkChangeIncludeAllPages();
        tester.checkCheckbox("bulkedit_10033", "on");
        tester.checkCheckbox("bulkedit_10041", "on");
        tester.submit("Next");
        tester.checkCheckbox("operation", "bulk.edit.operation.name");
        tester.submit("Next");
        tester.checkCheckbox("actions", "security");
        tester.selectOption("security", "MyFriendsOnly");
        tester.submit("Next");
        //check both issues are present, as well as the field that's about to change.
        tester.assertTextPresent("No more milk");
        tester.assertTextPresent("Lets get a third milk bucket");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "MyFriendsOnly"});
        tester.submit("Confirm");
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        //now check that all the issues have the original security level again in the issue navigator.
        assertPrecondition();
    }

    @Test
    public void testBulkMoveIssue() throws SAXException {
        assertPrecondition();

        // Do a Bulk Operation
        bulkOperations.bulkChangeIncludeAllPages();

        // Assert we are at Step 1 of 4
        tester.assertTextPresent("Step 1 of 4: Choose Issues");
        tester.assertTextPresent("Step 1 of 4");
        // Select RAT-5 RAT-6 and COW-35
        tester.checkCheckbox("bulkedit_10030", "on");
        tester.checkCheckbox("bulkedit_10031", "on");
        tester.checkCheckbox("bulkedit_10033", "on");
        tester.submit("Next");

        // Assert we are at second step
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), new String[]{
                "Step 2 of 4: Choose Operation",
                "Choose the operation you wish to perform on the selected 3 issue(s)."
        });
        // Select "Bulk Move" as the operation.
        tester.checkCheckbox("operation", "bulk.move.operation.name");
        tester.submit("Next");

        // Assert step 3 - Parent Issues
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), new String[]{
                "Step 3 of 4",
                "Select Projects and Issue Types",
                "Please note that 1 sub-task issues were removed from the selection and do not appear in the table below. You are not allowed to bulk move sub-task issues together with their parent issue. In this case, you will only be asked to move the sub-task if you move the parent issue to a new project.",
                "The change will affect 1 issues with issue type(s) Improvement in project(s) Bovine.",
                "The change will affect 1 issues with issue type(s) Bug in project(s) Rattus."
        });
        // Change Bovine/Improvement to DOG/New Feature
        navigation.issue().selectProject("Canine", "10000_4_pid");
        navigation.issue().selectIssueType("New Feature", "10000_4_issuetype");
        // Change RAT to PIG
        navigation.issue().selectProject("Porcine", "10022_1_pid");
        tester.submit("Next");

        // Assert step 3 - Sub-Tasks
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), new String[]{
                "Step 3 of 4",
                "Select Projects and Issue Types for Sub-Tasks",
                "The table below lists all the sub-tasks that need to be moved to a new project. Please select the appropriate issue type for each of them",
                "The change will affect 3 issues with issue type(s) Sub-task in project(s) Bovine.",
                "The change will affect 2 issues with issue type(s) Sub-task in project(s) Rattus."
        });
        tester.submit("Next");

        // Assert Update Fields for various combinations:
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), new String[]{
                "Step 3 of 4",
                "Update Fields for Target Project 'Canine' - Issue Type 'New Feature'",
                "Security Level"
        });
        // Choose "Level Red" security level
        tester.selectOption("security", "Level Red");
        tester.submit("Next");
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), new String[]{
                "Step 3 of 4",
                "Update Fields for Target Project 'Canine' - Issue Type 'Sub-task'",
                "Security Level",
                "The security level of subtasks is inherited from parents."
        });
        tester.submit("Next");
        // Cannot choose security level as new Project (PIG) has no Security Level scheme.
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), new String[]{
                "Step 3 of 4",
                "Update Fields for Target Project 'Porcine' - Issue Type 'Bug'",
                "Security Level",
                "The value of this field must be changed to be valid in the target project, but you are not able to update this field in the target project. It will be set to the field's default value for the affected issues."
        });
        tester.submit("Next");
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), new String[]{
                "Step 3 of 4",
                "Update Fields for Target Project 'Porcine' - Issue Type 'Sub-task'",
                "Security Level",
                "The value of this field must be changed to be valid in the target project, but you are not able to update this field in the target project. It will be set to the field's default value for the affected issues."
        });
        tester.submit("Next");

        // Now we are finally on the summary screen.
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), new String[]{
                "Step 4 of 4",
                "Confirmation",
                "Below is a summary of all issues that will be moved. Please confirm that the correct changes have been entered. The bullet points below lists the different target projects and issue types the issues will be moved to. Click on a link below to go to that particular group of issues.",

                "Target Project",
                "Canine",
                "Target Issue Type",
                "New Feature",
                "Security Level",
                "Level Red",

                "Target Project",
                "Canine",
                "Target Issue Type",
                "Sub-task",
                "Security Level",
                "Level Red",

                "Target Project",
                "Porcine",
                "Target Issue Type",
                "Bug",
                "Security Level",
                "None",

                "Target Project",
                "Porcine",
                "Target Issue Type",
                "Sub-task",
                "Security Level",
                "None"
        });
        // Confirm change to get back to Issue Navigator
        tester.submit("Next");
        bulkOperations.waitAndReloadBulkOperationProgressPage();
        // Now test our Issue table.
        WebTable issueTable = getIssuesTable();
        assertEquals("PIG-11", issueTable.getCellAsText(1, 1).trim());
        assertEquals("", issueTable.getCellAsText(1, 11).trim());
        assertEquals("PIG-10", issueTable.getCellAsText(2, 1).trim());
        assertEquals("", issueTable.getCellAsText(2, 11).trim());
        assertEquals("PIG-9", issueTable.getCellAsText(3, 1).trim());
        assertEquals("", issueTable.getCellAsText(3, 11).trim());
        assertEquals("DOG-12", issueTable.getCellAsText(4, 1).trim());
        assertEquals("Level Red", issueTable.getCellAsText(4, 11).trim());
        assertEquals("DOG-11", issueTable.getCellAsText(5, 1).trim());
        assertEquals("Level Red", issueTable.getCellAsText(5, 11).trim());
        assertEquals("DOG-10", issueTable.getCellAsText(6, 1).trim());
        assertEquals("Level Red", issueTable.getCellAsText(6, 11).trim());
        assertEquals("DOG-9", issueTable.getCellAsText(7, 1).trim());
        assertEquals("Level Red", issueTable.getCellAsText(7, 11).trim());
    }

    /**
     * This test ensures that if a subtask as well as a parent (not the parent of the first subtask) and
     * subtask are selected, then the orphaned subtask will be able change project (if the
     * 'Use the above project and issue type pair for all other combinations.' checkbox is ticked)
     *
     * @throws SAXException SAXException
     */
    @Test
    public void testBulkMoveWithOrphanedSubtasks() throws SAXException {
        assertPrecondition();

        //create another parent and subtask issue in the COW project.
        navigation.issue().goToCreateIssueForm(null, null);
        tester.setFormElement("summary", "Another parent");
        tester.submit("Create");
        tester.clickLink("create-subtask");
        tester.assertTextPresent("Create Sub-Task");
        tester.submit("Create");
        tester.assertTextPresent("Create Sub-Task");
        tester.setFormElement("summary", "Orphan");
        tester.submit("Create");

        navigation.issueNavigator().displayAllIssues();
        // Do a Bulk Operation
        bulkOperations.bulkChangeIncludeAllPages();
        //so lets select the 'Orphan' subtask and another subtask and parent (not linked to the 'Orpan')
        tester.checkCheckbox("bulkedit_10051", "on");
        tester.checkCheckbox("bulkedit_10033", "on");
        tester.checkCheckbox("bulkedit_10040", "on");
        tester.submit("Next");
        tester.checkCheckbox("operation", "bulk.move.operation.name");
        tester.submit("Next");

        // Assert step 3 - The orphan subtask should have its own box.
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), new String[]{
                "Step 3 of 4",
                "Select Projects and Issue Types",
                "Please note that 1 sub-task issues were removed from the selection and do not appear in the table below. You are not "
                        + "allowed to bulk move sub-task issues together with their parent issue. In this case, you will only "
                        + "be asked to move the sub-task if you move the parent issue to a new project.",
                "The change will affect 1 issues with issue type(s) Improvement in project(s) Bovine.",
                //check that the orphaned subtask is shown and displayes the target project.
                "Move",
                "To",
                "Bovine",
                "The change will affect 1 issues with issue type(s) Sub-task in project(s) Bovine and parent issue(s) COW-38."
        });

        // check the 'Use the above project and issue type pair for all other combinations.' This should be ignored by
        // the 'Orphan' subtask.
        tester.checkCheckbox("sameAsBulkEditBean", "10000_4_");
        //lets move to RAT/New Feature
        navigation.issue().selectProject("Rattus", "10000_4_pid");
        navigation.issue().selectIssueType("New Feature", "10000_4_issuetype");
        navigation.issue().selectIssueType("Task", "10000_5_10000_10050_issuetype");
        tester.submit("Next");

        // Step 3 (still ;)) - Need to move all the subtasks attached to the bovine parent issue.
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), new String[]{
                "Step 3 of 4",
                "Select Projects and Issue Types for Sub-Tasks",
                "The table below lists all the sub-tasks that need to be moved to a new project. Please select the appropriate issue type for each of them",
                //check that the orphaned subtask is shown and displayes the target project.
                "The change will affect 3 issues with issue type(s) Sub-task in project(s) Bovine.",
                "Move",
                "To",
                "Rattus"
        });
        tester.submit("Next");

        // lets set the security level to kingrat
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), new String[]{
                "Step 3 of 4",
                "Update Fields for Target Project 'Rattus' - Issue Type 'New Feature'"
        });
        tester.selectOption("security", "Level KingRat");
        tester.submit("Next");

        //For subtasks, setting the security level shouldn't be possible.
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), new String[]{
                "Step 3 of 4",
                "Update Fields for Target Project 'Rattus' - Issue Type 'Sub-task'"
        });
        tester.assertTextPresent("The security level of subtasks is inherited from parents.");
        tester.submit("Next");

        //the orphaned subtask was already processed in the Rattus/New feature step because 'Use the above project and issue type pair for all other combinations.' was selected.

        //click next on the confirm screen.
        tester.submit("Next");
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        WebTable issueTable = getIssuesTable();
        assertEquals("RAT-12", issueTable.getCellAsText(1, 1).trim());
        assertEquals("Level KingRat", issueTable.getCellAsText(1, 11).trim());
        assertEquals("RAT-11", issueTable.getCellAsText(2, 1).trim());
        assertEquals("Level KingRat", issueTable.getCellAsText(2, 11).trim());
        assertEquals("RAT-10", issueTable.getCellAsText(3, 1).trim());
        assertEquals("Level KingRat", issueTable.getCellAsText(3, 11).trim());
        assertEquals("RAT-9", issueTable.getCellAsText(4, 1).trim());
        assertEquals("Level KingRat", issueTable.getCellAsText(4, 11).trim());
        assertEquals("RAT-8", issueTable.getCellAsText(5, 1).trim());
        assertEquals("Level KingRat", issueTable.getCellAsText(5, 11).trim());
        assertEquals("RAT-7", issueTable.getCellAsText(6, 1).trim());
        assertEquals("Level Mouse", issueTable.getCellAsText(6, 11).trim());
        assertEquals("RAT-6", issueTable.getCellAsText(7, 1).trim());
        assertEquals("Level Mouse", issueTable.getCellAsText(7, 11).trim());
        assertEquals("RAT-5", issueTable.getCellAsText(8, 1).trim());
        assertEquals("Level Mouse", issueTable.getCellAsText(8, 11).trim());
        assertEquals("COW-38", issueTable.getCellAsText(9, 1).trim());
        assertEquals("", issueTable.getCellAsText(9, 11).trim());
    }

    @Test
    public void testBulkWorkflowTransitionIssue() throws SAXException {
        assertPrecondition();

        //bulk transition a single parent issue, setting the security level to nonen
        bulkOperations.bulkChangeIncludeAllPages();
        tester.assertTextPresent("Step 1 of 4: Choose Issues");
        tester.checkCheckbox("bulkedit_10033", "on");
        tester.submit("Next");
        tester.assertTextPresent("Step 2 of 4: Choose Operation");
        tester.checkCheckbox("operation", "bulk.workflowtransition.operation.name");
        tester.submit("Next");
        tester.assertTextPresent("Step 3 of 4: Operation Details");
        tester.checkCheckbox("wftransition", "Copy of jira_2_6");
        tester.submit("Next");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Workflow", "Copy of jira"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Selected Transition", "Close Issue"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Status Transition", "Open", "Closed"});
        //set the resolution to fixed, and the security level to 'None'.
        tester.checkCheckbox("actions", "resolution");
        tester.selectOption("resolution", "Fixed");
        tester.checkCheckbox("actions", "security");
        tester.selectOption("security", "None");
        tester.submit("Next");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Workflow", "Copy of jira"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Selected Transition", "Close Issue"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Status Transition", "Open", "Closed"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"This change will affect", "1", "issues"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "None"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Resolution", "Fixed"});
        tester.assertTextPresent("COW-35");
        tester.submit("Next");
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        //check that we are on the issue navigator and that the security level is no longer present.
        tester.assertTextPresent("Issue Navigator");
        tester.assertTextNotPresent("MyFriendsOnly");
        WebTable issueTable = getIssuesTable();
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 1, "COW-37");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 2, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 2, "Lets get a third milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 6, "Open");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 1, "COW-36");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 2, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 2, "Get another milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 6, "Open");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 1, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 2, "No more milk");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 6, "Closed");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 1, "COW-34");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 2, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 2, "Get new milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 6, "Open");

        //now lets do another transition and change the security level back.  This time select parent and subtask.
        //in the end this should really only do the transition for the parent.
        bulkOperations.bulkChangeIncludeAllPages();
        tester.checkCheckbox("bulkedit_10033", "on");
        tester.checkCheckbox("bulkedit_10034", "on");
        tester.submit("Next");
        tester.checkCheckbox("operation", "bulk.workflowtransition.operation.name");
        tester.submit("Next");
        tester.assertTextPresent("Step 3 of 4: Operation Details");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Closed", "Reopened"});
        tester.submit("Next");
        tester.checkCheckbox("wftransition", "Copy of jira_3_4");
        tester.submit("Next");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Workflow", "Copy of jira"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Selected Transition", "Reopen Issue"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Status Transition", "Closed", "Reopened"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"This change will affect", "1", "issues"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "MyFriendsOnly"});
        tester.checkCheckbox("actions", "security");
        tester.selectOption("security", "MyFriendsOnly");
        tester.submit("Next");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Workflow", "Copy of jira"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Selected Transition", "Reopen Issue"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Status Transition", "Closed", "Reopened"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"This change will affect", "1", "issues"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"COW-35"});
        tester.submit("Next");
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        issueTable = getIssuesTable();
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 1, "COW-37");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 2, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 2, "Lets get a third milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 6, "Open");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 11, "MyFriendsOnly");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 1, "COW-36");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 2, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 2, "Get another milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 6, "Open");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 11, "MyFriendsOnly");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 1, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 2, "No more milk");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 6, "Reopened");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 11, "MyFriendsOnly");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 1, "COW-34");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 2, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 2, "Get new milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 6, "Open");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 11, "MyFriendsOnly");
    }

    @Test
    public void testConvertSingleIssueToSubtask() throws SAXException {
        assertPrecondition();

        //convert a subtask to a proper issue
        navigation.issue().gotoIssue("COW-34");
        tester.clickLink("subtask-to-issue");
        tester.assertTextPresent("Convert Sub-task to Issue: COW-34");
        navigation.issue().selectIssueType("New Feature", "issuetype");
        tester.submit("Next >>");
        tester.assertTextPresent("All fields will be updated automatically.");
        tester.submit("Next >>");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Type", "Sub-task", "New Feature"});
        tester.submit("Finish");
        tester.assertTextPresent("Get new milk bucket");

        tester.assertTextNotPresent("COW-35");
        tester.assertTextPresent("MyFriendsOnly");

        //let set the security leve to 'None'
        tester.clickLink("edit-issue");
        tester.selectOption("security", "None");
        tester.submit("Update");
        navigation.issue().returnToSearch();
        //ensure that COW-34 is now an issue with no security level.
        final WebTable issueTable = getIssuesTable();
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 1, "COW-37");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 2, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 2, "Lets get a third milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 11, "MyFriendsOnly");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 1, "COW-36");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 2, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 2, "Get another milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 11, "MyFriendsOnly");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 1, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 2, "No more milk");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 11, "MyFriendsOnly");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 1, "COW-34");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 2, "Get new milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 11, "");

        //lets convert it back to a subtask of COW-35 and make sure there's a security level.
        navigation.issue().gotoIssue("COW-34");
        tester.clickLink("issue-to-subtask");
        tester.setFormElement("parentIssueKey", "COW-35");
        tester.submit("Next >>");
        tester.assertTextPresent("Convert Issue to Sub-task: COW-34");
        tester.assertTextPresent("All fields will be updated automatically.");
        tester.submit("Next >>");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Type", "New Feature", "Sub-task"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "None", "MyFriendsOnly"});
        tester.submit("Finish");
        tester.assertTextPresent("COW-35");
        tester.assertTextPresent("MyFriendsOnly");
        navigation.issue().returnToSearch();

        assertPrecondition();
    }

    private void assertPrecondition() throws SAXException {
        //first lets check we have an issue with subtasks and they all have a particular security level set.
        navigation.issueNavigator().displayAllIssues();
        final WebTable issueTable = getIssuesTable();

        // Rat Issues
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 1, "RAT-7");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 2, "RAT-5");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 11, "Level Mouse");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 1, "RAT-6");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 2, "RAT-5");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 11, "Level Mouse");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 1, "RAT-5");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 11, "Level Mouse");

        // Cow Issues
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 1, "COW-37");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 2, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 2, "Lets get a third milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 11, "MyFriendsOnly");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 1, "COW-36");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 2, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 2, "Get another milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 11, "MyFriendsOnly");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 1, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 2, "No more milk");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 11, "MyFriendsOnly");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 1, "COW-34");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 2, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 2, "Get new milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 11, "MyFriendsOnly");
    }

    private WebTable getIssuesTable()
            throws SAXException {
        return tester.getDialog().getResponse().getTableWithID("issuetable");
    }

}
