package com.atlassian.jira.functest.rule;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.util.function.Supplier;

/**
 * Restore data on class level. Use {@link com.atlassian.integrationtesting.runner.restore.RestoreOnce} to restore data
 * before running all tests. This data will be used only once before all methods executed in this class.
 *
 * @since v7.1
 */
public class RestoreDataOnlyOnce implements TestRule {

    private final Supplier<Backdoor> backdoorSupplier;

    public RestoreDataOnlyOnce(final Supplier<Backdoor> backdoorSupplier) {
        this.backdoorSupplier = backdoorSupplier;
    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                tryInitializeData(description);
                base.evaluate();
            }
        };
    }

    private void tryInitializeData(final Description description) {
        final Class<?> testClass = description.getTestClass();
        final RestoreOnce restoreOnce = testClass.getAnnotation(RestoreOnce.class);
        if (restoreOnce != null) {
            backdoorSupplier.get().restoreDataFromResource(restoreOnce.value());
        }
    }
}
