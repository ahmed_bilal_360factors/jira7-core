package com.atlassian.jira.functest.matcher;

import com.google.common.collect.ImmutableList;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.opensymphony.util.TextUtils;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static org.junit.Assert.assertEquals;

/**
 * Matchers for MimeMessage
 *
 * @since v7.1
 */
public class MimeMessageMatchers {
    private static abstract class MimeMessageTypeSafeMatcher extends TypeSafeMatcher<MimeMessage> {
        @Override
        final protected void describeMismatchSafely(final MimeMessage item, final Description mismatchDescription) {
            try {
                mismatchDescription.appendText("was ").appendValue(getActualValue(item));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        abstract Object getActualValue(final MimeMessage item) throws Exception;

    }

    public static Matcher<MimeMessage> containsString(final String text) {
        notNull("text", text);
        return new MimeMessageTypeSafeMatcher() {
            @Override
            protected boolean matchesSafely(final MimeMessage message) {
                final String emailBody = GreenMailUtil.getBody(message);
                return emailBody.contains(text);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("MimeMessage that body contains: ").appendValue(text);
            }

            Object getActualValue(final MimeMessage item) {
                return GreenMailUtil.getBody(item);
            }
        };
    }

    public static Matcher<MimeMessage> hasNumberOfParts(final int expectedNumOfParts) {
        return new MimeMessageTypeSafeMatcher() {
            @Override
            protected boolean matchesSafely(final MimeMessage message) {
                try {
                    return expectedNumOfParts == getNumOfParts(message);
                } catch (IOException | MessagingException e) {
                    throw new RuntimeException(e);
                }
            }

            private int getNumOfParts(final MimeMessage message) throws IOException, MessagingException {
                final Object emailContent = message.getContent();
                if (emailContent instanceof Multipart) {
                    final Multipart multiPart = (Multipart) emailContent;
                    return multiPart.getCount();
                } else {
                    return -1;
                }
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("MimeMessage with number of parts=").appendValue(expectedNumOfParts);
            }

            Object getActualValue(final MimeMessage item) throws Exception {
                return getNumOfParts(item);
            }
        };
    }

    public static Matcher<MimeMessage> withRecipientEqualTo(final Collection<String> expectedToAddresses) {
        notNull("expectedToAddresses", expectedToAddresses);
        return new MimeMessageTypeSafeMatcher() {
            @Override
            protected boolean matchesSafely(final MimeMessage message) {
                try {
                    return expectedToAddresses.equals(getActualAddresses(message, "to"));
                } catch (MessagingException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("MimeMessage that to address is: ").appendValue(expectedToAddresses);
            }

            Object getActualValue(final MimeMessage item) throws Exception {
                return getActualAddresses(item, "to");
            }
        };
    }

    private static Collection<String> getActualAddresses(final MimeMessage message, final String field) throws MessagingException {
        final String[] addressHeader = message.getHeader(field);
        assertEquals(1, addressHeader.length);
        return parseEmailAddresses(addressHeader[0]);
    }

    public static Matcher<MimeMessage> withRecipientEqualTo(final String expectedTo) {
        return withRecipientEqualTo(ImmutableList.of(expectedTo));
    }


    public static Matcher<MimeMessage> withCcEqualTo(final Collection<String> expectedCcAddresses) {
        notNull("expectedCcAddresses", expectedCcAddresses);
        return new MimeMessageTypeSafeMatcher() {
            @Override
            protected boolean matchesSafely(final MimeMessage message) {
                try {
                    return expectedCcAddresses.equals(getActualAddresses(message, "cc"));
                } catch (MessagingException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("MimeMessage that cc address is: ").appendValue(expectedCcAddresses);
            }

            Object getActualValue(final MimeMessage item) throws Exception {
                return getActualAddresses(item, "cc");
            }
        };
    }

    public static Matcher<MimeMessage> withFromEqualTo(final String expectedFrom) {
        notNull("expectedFrom", expectedFrom);
        return new MimeMessageTypeSafeMatcher() {
            @Override
            protected boolean matchesSafely(final MimeMessage message) {
                try {
                    final String[] adresses = message.getHeader("from");
                    assertEquals(1, adresses.length);
                    final Collection<String> actualAddresses = parseEmailAddresses(adresses[0]);
                    return ImmutableList.of(expectedFrom).equals(actualAddresses);
                } catch (MessagingException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("MimeMessage that from address is: ").appendValue(expectedFrom);
            }

            Object getActualValue(final MimeMessage item) throws Exception {
                return getActualAddresses(item, "from");
            }
        };
    }

    public static Matcher<MimeMessage> withSubjectEqualTo(final String subject) {
        notNull("subject", subject);
        return new MimeMessageTypeSafeMatcher() {
            @Override
            protected boolean matchesSafely(final MimeMessage message) {
                try {
                    return subject.equals(message.getSubject());
                } catch (MessagingException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("MimeMessage that subject is: ").appendValue(subject);
            }

            Object getActualValue(final MimeMessage item) throws Exception {
                return item.getSubject();
            }
        };
    }

    public static Matcher<MimeMessage> containsContentType(final String contentType) {
        notNull("contentType", contentType);
        return new MimeMessageTypeSafeMatcher() {
            @Override
            protected boolean matchesSafely(final MimeMessage message) {
                try {
                    final Object content = message.getContent();
                    if (content instanceof Multipart) {
                        final Multipart multipart = (Multipart) content;
                        for (int i = 0; i < multipart.getCount(); i++) {
                            final BodyPart bodyPart = multipart.getBodyPart(i);
                            if (contentType.equals(bodyPart.getContentType())) {
                                return true;
                            }
                        }
                    }
                    return contentType.equals(message.getContentType());
                } catch (MessagingException | IOException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("MimeMessage that has content type: ").appendValue(contentType);
            }

            Object getActualValue(final MimeMessage item) throws Exception {
                return item.getContentType();
            }
        };
    }

    public static Matcher<MimeMessage> hasHtmlContentType() {
        return containsContentType("text/html; charset=UTF-8");
    }

    public static Matcher<MimeMessage> hasTextContentType() {
        return containsContentType("text/plain; charset=UTF-8");
    }

    public static Matcher<MimeMessage> withBodyMatching(final String regex) {
        notNull("regex", regex);
        return new MimeMessageTypeSafeMatcher() {
            @Override
            protected boolean matchesSafely(final MimeMessage message) {
                final String emailBody = GreenMailUtil.getBody(message);
                final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
                final java.util.regex.Matcher match = pattern.matcher(emailBody);

                return match.find();
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("MimeMessage that matches: ").appendValue(regex);
            }

            Object getActualValue(final MimeMessage item) throws Exception {
                return GreenMailUtil.getBody(item);
            }
        };
    }

    /**
     * Given a comma seperated list of email addresses, returns a collection of the email addresses.
     *
     * @param emails comma seperated list of email addresses
     * @return collection of individual email address
     */
    private static Collection<String> parseEmailAddresses(final String emails) {
        final StringTokenizer st = new StringTokenizer(emails, ",");
        final Collection<String> emailList = new ArrayList<String>();
        while (st.hasMoreTokens()) {
            final String email = st.nextToken().trim();
            if (TextUtils.stringSet(email)) {
                emailList.add(email.trim());
            }
        }
        return emailList;
    }
}
