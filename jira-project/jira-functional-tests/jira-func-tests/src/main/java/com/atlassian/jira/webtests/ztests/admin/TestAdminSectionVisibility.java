package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.Navigation.AdminSection.GENERAL_CONFIGURATION;
import static com.atlassian.jira.functest.framework.Navigation.AdminSection.ISSUE_TYPES;
import static com.atlassian.jira.security.Permissions.ADMINISTER;

/**
 * Tests the admin/sys-admin visibility of pages in the admin section.
 *
 * @since v3.12
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION})
@RestoreBlankInstance
public class TestAdminSectionVisibility extends BaseJiraFuncTest {

    private final static String NOTSYSADMIN_USERNAME = "notsysadmin";

    @Before
    public void setUp() {
        backdoor.getTestkit().usersAndGroups().addGroup("notsysadmins_group");
        backdoor.getTestkit().usersAndGroups().addUser(NOTSYSADMIN_USERNAME);
        backdoor.getTestkit().usersAndGroups().addUserToGroup(NOTSYSADMIN_USERNAME, "notsysadmins_group");
        backdoor.getTestkit().permissions().addGlobalPermission(ADMINISTER, "notsysadmins_group");
        backdoor.getTestkit().auditing().clearAllRecords();
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testSysAdminCanSeeBackupDataPage() {
        navigation.gotoAdminSection(GENERAL_CONFIGURATION);
        tester.clickLink("backup_data");
        tester.assertTextPresent("Backup JIRA data");
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testSysAdminCanSeeRestoreDataPage() {
        navigation.gotoAdminSection(GENERAL_CONFIGURATION);
        tester.clickLink("restore_data");
        tester.assertTextPresent("Restore JIRA data from Backup");
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testSysAdminCanSeeListenersPage() {
        navigation.gotoAdminSection(GENERAL_CONFIGURATION);
        tester.clickLink("listeners");
        tester.assertTextPresent("Listeners are used for performing certain actions");
        tester.clickLinkWithText("Built-in Listeners");
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testSysAdminCanSeeServicesPage() {
        navigation.gotoAdminSection(GENERAL_CONFIGURATION);
        tester.clickLink("services");
        tester.assertTextPresent("Add a new service");
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testSysAdminCanSeeOutgoingMailPage() {
        navigation.gotoAdminSection(GENERAL_CONFIGURATION);
        tester.clickLink("outgoing_mail");
        tester.assertTextPresent("Outgoing Mail");
        tester.assertTextPresent("SMTP Mail Server");
        tester.clickLinkWithText("Configure new SMTP mail server");
        tester.assertTextPresent("Use this page to add a new SMTP mail server");
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testSysAdminCanSeeIncomingMailPage() {
        navigation.gotoAdminSection(GENERAL_CONFIGURATION);
        tester.clickLink("incoming_mail");
        tester.assertTextPresent("Incoming Mail");
        tester.assertTextPresent("POP / IMAP Mail Servers");
        tester.clickLinkWithText("Add POP / IMAP mail server");
        tester.assertTextPresent("Use this page to add a new POP / IMAP server for JIRA to retrieve mail from.");
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testSysAdminCanSeeWorkflowsPage() {
        navigation.gotoAdminSection(ISSUE_TYPES);
        tester.clickLink("workflows");
        tester.assertLinkPresentWithText("Import Workflow");
        tester.assertLinkPresentWithText("Import XML");
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testSysAdminCanSeeAttachementsPage() {
        navigation.gotoAdminSection(GENERAL_CONFIGURATION);
        tester.clickLink("attachments");
        tester.assertTextPresent("To enable a user to attach files,");
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testSysAdminCanSeeEditConfigurationPage() {
        navigation.gotoAdminSection(GENERAL_CONFIGURATION);
        tester.clickLink("scheduler_details");
        tester.assertTextPresent("This page shows you the properties of the JIRA internal scheduler");
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testSysAdminCanSeeIntegrationCheckerPage() {
        navigation.gotoAdminSection(GENERAL_CONFIGURATION);
        tester.clickLink("integrity_checker");
        tester.assertTextPresent("Select one or more integrity checks from");
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testSysAdminCanSeeLoggingProfilingPage() {
        navigation.gotoAdminSection(GENERAL_CONFIGURATION);
        tester.clickLink("logging_profiling");
        tester.assertTextPresent("Mark Logs");
        tester.assertTextPresent("HTTP Access Logging");
        tester.assertTextPresent("SQL Logging");
        tester.assertTextPresent("Default Loggers");
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testSysAdminCanSeeViewProjectsPage() {
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_PROJECTS);
        tester.assertLinkPresent("view_projects");
    }

    @Test
    public void testAdminCantSeeProtectedPages() {
        navigation.login(NOTSYSADMIN_USERNAME);
        checkUrlForNoPerm("/secure/admin/XmlBackup!default.jspa", "a system administrator");
        checkUrlForNoPerm("/secure/admin/XmlRestore!default.jspa", "a system administrator");
        checkUrlForNoPerm("/secure/admin/jira/IntegrityChecker!default.jspa", "a system administrator");
        checkUrlForNoPerm("/secure/admin/jira/ViewListeners!default.jspa", "a system administrator");
        checkUrlForNoPerm("/secure/admin/jira/ViewLogging.jspa", "a system administrator");
        checkUrlForNoPerm("/secure/admin/jira/ConfigureLogging!default.jspa?loggerName=root", "a system administrator");
        checkUrlForNoPerm("/secure/admin/jira/SchedulerAdmin.jspa", "a system administrator");
        checkUrlForNoPerm("/secure/admin/workflows/ImportWorkflowFromXml!default.jspa", "a system administrator");
    }

    @Test
    @LoginAs(user = FRED_USERNAME)
    public void testNonAdminCantSeeProtectedPages() {
        checkUrlForNoPerm("/secure/admin/OutgoingMailServers.jspa", "an administrator");
        checkUrlForNoPerm("/secure/admin/IncomingMailServers.jspa", "an administrator");
        checkUrlForNoPerm("/secure/admin/AddSmtpMailServer!default.jspa", "an administrator");
        checkUrlForNoPerm("/secure/admin/XmlBackup!default.jspa", "a system administrator");
        checkUrlForNoPerm("/secure/admin/XmlRestore!default.jspa", "a system administrator");
        checkUrlForNoPerm("/secure/admin/jira/IntegrityChecker!default.jspa", "a system administrator");
        checkUrlForNoPerm("/secure/admin/jira/ViewListeners!default.jspa", "a system administrator");
        checkUrlForNoPerm("/secure/admin/jira/ViewLogging.jspa", "a system administrator");
        checkUrlForNoPerm("/secure/admin/jira/ConfigureLogging!default.jspa?loggerName=root", "a system administrator");
        checkUrlForNoPerm("/secure/admin/jira/SchedulerAdmin.jspa", "a system administrator");
        checkUrlForNoPerm("/secure/admin/jira/ViewServices!default.jspa", "an administrator");
        checkUrlForNoPerm("/secure/admin/workflows/ImportWorkflowFromXml!default.jspa", "a system administrator");
    }

    @Test
    public void testAdminCantSeeLinks() {
        navigation.login(NOTSYSADMIN_USERNAME);
        navigation.gotoAdminSection(GENERAL_CONFIGURATION);

        tester.assertLinkNotPresent("backup_data");
        tester.assertLinkNotPresent("restore_data");
        tester.assertLinkNotPresent("integrity_checker");
        tester.assertLinkNotPresent("license_details");
        tester.assertLinkNotPresent("listeners");
        tester.assertLinkNotPresent("logging_profiling");
        tester.assertLinkNotPresent("scheduler_details");

        navigation.gotoAdminSection(ISSUE_TYPES);
        tester.clickLink("workflows");
        tester.assertLinkNotPresentWithText("Import XML");
    }

    @Test
    @LoginAs(user = FRED_USERNAME)
    public void testNonAdminCantSeeLinks() {
        navigation.gotoAdmin();
        tester.assertTextPresent("You do not have the permissions to administer any projects, or there are none created.");
        tester.assertElementNotPresent("admin-nav-heading");
    }

    private void checkUrlForNoPerm(String url, String expectedPerm) {
        tester.gotoPage(url);
        tester.assertTextPresent("does not have permission to access this page.");
        tester.assertTextPresent("You must log in as " + expectedPerm + " to access this page.");
    }
}
