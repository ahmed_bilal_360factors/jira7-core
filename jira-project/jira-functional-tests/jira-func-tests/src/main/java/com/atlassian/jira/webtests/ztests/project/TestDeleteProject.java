package com.atlassian.jira.webtests.ztests.project;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.assertions.ViewIssueAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.beans.WorkflowSchemeData;
import com.atlassian.jira.util.ProgressPageControl;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SCHEMES, Category.PERMISSIONS,
        Category.PROJECTS, Category.ISSUE_TYPES, Category.SCREENS, Category.FIELDS, Category.CUSTOM_FIELDS,
        Category.SECURITY})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestDeleteProject extends BaseJiraFuncTest {
    public static final String DELETE_BUTTON_NAME = "delete";
    public static final String REFRESH_BUTTON_NAME = "refresh";
    public static final String ACKNOWLEDGE_BUTTON_NAME = "acknowledge";
    public static final String FORM_NAME = "project-delete-form";
    public static final String DELETE_PROJECT_HEADER = "Delete project";
    public static final String DELETE_PROJECT_HEADER_WITH_PROJECT_NAME = "Delete project: ";

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Inject
    private ViewIssueAssertions viewIssueAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("TestDeleteProject.xml");
    }

    @Test
    public void testPCPDeleteProjectRedirectsToViewProjects() {
        tester.gotoPage("secure/project/DeleteProject!default.jspa?pid=10000&pcp=true");
        tester.assertTextPresent("If you want to save the project data before you delete it, you can");
        tester.assertElementPresent("delete-project-confirm-submit");
        tester.submit(DELETE_BUTTON_NAME);

        ProgressPageControl.waitAndReload(tester, FORM_NAME, REFRESH_BUTTON_NAME, ACKNOWLEDGE_BUTTON_NAME);
        tester.assertElementPresent("view-projects-header");
        tester.assertTextNotPresent("homosapien");
    }

    @Test
    public void testPCPDeleteDeletedProjectAndShowError() {
        tester.gotoPage("secure/project/DeleteProject!default.jspa?pid=10000&pcp=true");

        try {
            backdoor.project().deleteProject("HSP");
        } finally {
            tester.submit(DELETE_BUTTON_NAME);
            assertions.getJiraFormAssertions().assertFormErrMsg("Project with id '10,000' does not exist. Perhaps it was deleted?");
        }
    }

    @Test
    public void testDeleteProjectNoPermission() {
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);

        tester.gotoPage("secure/project/DeleteProject.jspa?pid=10000&confirm=true&returnUrl=ViewProjects.jspa");
        tester.assertTextPresent("Welcome to jWebTest JIRA installation");

        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);

        //check the project still exists.
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_PROJECTS);
        tester.assertTextPresent("homosapien");
    }

    @Test
    public void testDeleteProject() {
        administration.restoreData("TestDeleteProjectEnterprise.xml");

        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_PROJECTS);
        tester.assertTextPresent("homosapien");

        //check the issues exist before deletion
        navigation.issue().viewIssue("HSP-1");
        tester.assertTextPresent("Test issue 1");
        navigation.issue().viewIssue("HSP-2");
        tester.assertTextPresent("JIRA needs to be more Web 2.0");

        WorkflowSchemeData draft = backdoor.workflowSchemes().getWorkflowSchemeDraftByProjectNameNullIfNotFound("homosapien");

        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_PROJECTS);
        tester.clickLink("delete_project_10000");
        tester.assertTextPresent(DELETE_PROJECT_HEADER_WITH_PROJECT_NAME);
        tester.submit(DELETE_BUTTON_NAME);

        tester.assertTextPresent(DELETE_PROJECT_HEADER);
        ProgressPageControl.waitAndReload(tester, FORM_NAME, REFRESH_BUTTON_NAME, ACKNOWLEDGE_BUTTON_NAME);
        tester.submit();

        //check the project has been deleted
        tester.assertTextNotPresent("homosapien");

        //now go through all the related stuff and check the projct is gone (permission schemes, etc)
        navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);
        tester.assertTextPresent("Notification Schemes");
        tester.assertTextPresent("The table below shows the notification schemes currently configured for this server");
        tester.assertTextNotPresent("homosapien");

        navigation.gotoAdminSection(Navigation.AdminSection.PERMISSION_SCHEMES);
        tester.assertTextPresent("Permission Schemes");
        tester.assertTextPresent("Permission Schemes allow you to create a set of permissions and apply this set of permissions to any project.");
        tester.assertTextNotPresent("homosapien");

        //custom fields
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        tester.assertTextPresent("custom fields");
        tester.assertTextNotPresent("homosapien");
        tester.assertTextPresent("Not configured for any context");

        //now lets check the issues have been deleted
        navigation.issue().viewIssue("HSP-1");
        viewIssueAssertions.assertIssueNotFound();
        navigation.issue().viewIssue("HSP-2");
        viewIssueAssertions.assertIssueNotFound();

        //issue sec schemes
        navigation.gotoAdminSection(Navigation.AdminSection.SECURITY_SCHEMES);
        tester.assertTextPresent("Issue Security Schemes allow you to control who can and cannot view issues.");
        tester.assertTextNotPresent("homosapien");

        //workflow schemes
        WorkflowSchemeData customerWorflowScheme = backdoor.workflowSchemes().getWorkflowSchemeByNameNullIfNotFound("Customer workflow");
        assertNotNull(customerWorflowScheme);
        assertFalse(customerWorflowScheme.isActive());

        WorkflowSchemeData copyOfCustomerWorkflowScheme = backdoor.workflowSchemes().getWorkflowSchemeByNameNullIfNotFound("Copy of Customer workflow");
        assertNotNull(copyOfCustomerWorkflowScheme);
        assertFalse(copyOfCustomerWorkflowScheme.isActive());
        assertEquals("(This copy was automatically generated from a draft, when workflow scheme 'Customer workflow' was made inactive.)", copyOfCustomerWorkflowScheme.getDescription());
        assertEquals(draft.getMappings(), copyOfCustomerWorkflowScheme.getMappings());

        assertNoDraft("homosapien");

        //field configuration schemes
        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_FIELDS);
        tester.assertTextPresent("The table below shows the current Field Configuration Schemes and the projects they are associated with.");
        tester.assertTextNotPresent("homosapien");

        //issue type screen schemes
        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_TYPE_SCREEN_SCHEME);
        textAssertions.assertTextPresent(locator.css("h2"), "Issue Type Screen Schemes");
        tester.assertTextNotPresent("homosapien");
    }

    @Test
    public void testDeleteProjectNotExists() {
        navigation.gotoAdmin();
        tester.gotoPage("secure/project/DeleteProject.jspa?pid=20000&confirm=true&returnUrl=ViewProjects.jspa&atl_token=" + page.getXsrfToken());
        tester.assertTextPresent(DELETE_PROJECT_HEADER);
        assertions.getJiraFormAssertions().assertFormErrMsg("Project with id '20,000' does not exist. Perhaps it was deleted?");
    }

    @Test
    public void testDeleteProjectWithoutWorkflowSchemeDraft() {
        administration.restoreData("TestDeleteProjectEnterprise.xml");

        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_PROJECTS);
        tester.clickLink("delete_project_10010");
        tester.assertTextPresent(DELETE_PROJECT_HEADER_WITH_PROJECT_NAME + "without draft WF scheme");
        tester.submit(DELETE_BUTTON_NAME);

        tester.assertTextPresent(DELETE_PROJECT_HEADER);
        ProgressPageControl.waitAndReload(tester, FORM_NAME, REFRESH_BUTTON_NAME, ACKNOWLEDGE_BUTTON_NAME);

        //workflow schemes
        navigation.gotoAdminSection(Navigation.AdminSection.WORKFLOW_SCHEMES);
        tester.assertTextPresent("Workflow Schemes allow you to define which workflows apply to given issue types and projects.");
        tester.assertTextNotPresent("without draft WF scheme");
        tester.assertTextNotPresent("Copy of ");

        WorkflowSchemeData customerWorflowScheme = backdoor.workflowSchemes().getWorkflowSchemeByNameNullIfNotFound("without draft WF scheme");
        assertNull(customerWorflowScheme);

        WorkflowSchemeData copyOfCustomerWorkflowScheme = backdoor.workflowSchemes().getWorkflowSchemeByNameNullIfNotFound("Copy of without draft WF scheme");
        assertNull(copyOfCustomerWorkflowScheme);

        assertNoDraft("without draft WF scheme");
    }

    @Test
    public void testDeleteProjectWithWorkflowSchemeDraftWithTheSameNameAsExistingWorkflowScheme() {
        administration.restoreData("TestDeleteProjectEnterprise.xml");

        testDraftCopyRenaming("Copy of Customer workflow", "Customer workflow", "Copy 2 of Customer workflow");
    }

    @Test
    public void testDeleteProjectWithWorkflowSchemeDraftWithTheSameNameAsExistingWorkflowSchemeAbbreviating() {
        administration.restoreData("TestDeleteProjectEnterprise.xml");

        String existingSchemeName = "Copy of " + StringUtils.repeat("1", 244) + "...";
        String originalSchemeName = StringUtils.repeat("1", 255);
        String expectedCopyName = "Copy 2 of " + StringUtils.repeat("1", 242) + "...";

        backdoor.workflowSchemes().createScheme(new WorkflowSchemeData().setName(originalSchemeName));
        administration.project().associateWorkflowScheme("homosapien", originalSchemeName);
        administration.project().createWorkflowSchemeDraft("HSP");

        testDraftCopyRenaming(existingSchemeName, originalSchemeName, expectedCopyName);
    }

    private void testDraftCopyRenaming(String existingSchemeName, String originalSchemeName, String expectedCopyName) {
        // Create a workflow scheme that has the same name as expected draft workflow scheme copy.
        backdoor.workflowSchemes().createScheme(new WorkflowSchemeData().setName(existingSchemeName));

        WorkflowSchemeData draft = backdoor.workflowSchemes().getWorkflowSchemeDraftByProjectNameNullIfNotFound("homosapien");

        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_PROJECTS);
        tester.clickLink("delete_project_10000");
        tester.assertTextPresent(DELETE_PROJECT_HEADER_WITH_PROJECT_NAME);
        tester.submit(DELETE_BUTTON_NAME);

        tester.assertTextPresent(DELETE_PROJECT_HEADER);
        ProgressPageControl.waitAndReload(tester, FORM_NAME, REFRESH_BUTTON_NAME, ACKNOWLEDGE_BUTTON_NAME);

        //workflow schemes
        WorkflowSchemeData originalScheme = backdoor.workflowSchemes().getWorkflowSchemeByNameNullIfNotFound(originalSchemeName);
        assertNotNull(originalScheme);
        assertFalse(originalScheme.isActive());

        WorkflowSchemeData existingScheme = backdoor.workflowSchemes().getWorkflowSchemeByNameNullIfNotFound(existingSchemeName);
        assertNotNull(existingScheme);
        assertFalse(existingScheme.isActive());

        WorkflowSchemeData copiedScheme = backdoor.workflowSchemes().getWorkflowSchemeByNameNullIfNotFound(expectedCopyName);
        assertNotNull(copiedScheme);
        assertFalse(copiedScheme.isActive());
        assertEquals("(This copy was automatically generated from a draft, when workflow scheme '" + originalSchemeName + "' was made inactive.)", copiedScheme.getDescription());
        assertEquals(draft.getMappings(), copiedScheme.getMappings());

        assertNoDraft("homosapien");
    }

    @Test
    public void testDeleteProjectWithWorkflowSchemeDraftUsedByAnotherProject() {
        administration.restoreData("TestDeleteProjectEnterprise.xml");

        administration.project().associateWorkflowScheme("monkey", "Customer workflow");

        WorkflowSchemeData draftBeforeDelete = backdoor.workflowSchemes().getWorkflowSchemeDraftByProjectNameNullIfNotFound("homosapien");

        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_PROJECTS);
        tester.clickLink("delete_project_10000");
        tester.assertTextPresent(DELETE_PROJECT_HEADER_WITH_PROJECT_NAME + "homosapien");
        tester.submit(DELETE_BUTTON_NAME);

        tester.assertTextPresent(DELETE_PROJECT_HEADER);
        ProgressPageControl.waitAndReload(tester, FORM_NAME, REFRESH_BUTTON_NAME, ACKNOWLEDGE_BUTTON_NAME);

        WorkflowSchemeData copyOfCustomerWorkflowScheme = backdoor.workflowSchemes().getWorkflowSchemeByNameNullIfNotFound("Copy of Customer workflow");
        assertNull(copyOfCustomerWorkflowScheme);

        WorkflowSchemeData draftAfterDelete = backdoor.workflowSchemes().getWorkflowSchemeDraftByProjectNameNullIfNotFound("monkey");
        assertNotNull(draftAfterDelete);
        assertEquals(draftBeforeDelete.getId(), draftAfterDelete.getId());
        assertEquals(draftBeforeDelete.getName(), draftAfterDelete.getName());
        assertEquals(draftBeforeDelete.getDescription(), draftAfterDelete.getDescription());
        assertEquals(draftBeforeDelete.getMappings(), draftAfterDelete.getMappings());
    }

    private void assertNoDraft(String projectName) {
        WorkflowSchemeData draft = backdoor.workflowSchemes().getWorkflowSchemeDraftByProjectNameNullIfNotFound(projectName);
        assertNull(draft);
    }
}
