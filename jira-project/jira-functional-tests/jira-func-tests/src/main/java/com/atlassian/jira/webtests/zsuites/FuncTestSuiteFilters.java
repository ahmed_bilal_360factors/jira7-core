package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.filter.TestDanglingGroups;
import com.atlassian.jira.webtests.ztests.filter.TestEditFilterInvalidShares;
import com.atlassian.jira.webtests.ztests.filter.TestFavouriteAndMyFilters;
import com.atlassian.jira.webtests.ztests.filter.TestFilterActions;
import com.atlassian.jira.webtests.ztests.filter.TestFilterHoldsItsSorting;
import com.atlassian.jira.webtests.ztests.filter.TestFilterPicker;
import com.atlassian.jira.webtests.ztests.filter.TestFilterRelatedEntitiesDelete;
import com.atlassian.jira.webtests.ztests.filter.TestFilterSortUserRename;
import com.atlassian.jira.webtests.ztests.filter.TestFilterSubscription;
import com.atlassian.jira.webtests.ztests.filter.TestFilterSubscriptionXss;
import com.atlassian.jira.webtests.ztests.filter.TestFilterWarnings;
import com.atlassian.jira.webtests.ztests.filter.TestPopularFilters;
import com.atlassian.jira.webtests.ztests.filter.TestSearchFilters;
import com.atlassian.jira.webtests.ztests.filter.TestSearchFiltersShareType;
import com.atlassian.jira.webtests.ztests.filter.management.TestChangeSharedFilterOwnerByAdmins;
import com.atlassian.jira.webtests.ztests.filter.management.TestDeleteSharedFilterByAdmins;
import com.atlassian.jira.webtests.ztests.filter.management.TestSharedFilterSearchingByAdmins;
import com.atlassian.jira.webtests.ztests.navigator.TestSearchRequestViewSecurity;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A func test suite for Filters
 *
 * @since v4.0
 */
public class FuncTestSuiteFilters {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestFilterSortUserRename.class)
                .add(TestDanglingGroups.class)
                .add(TestFilterRelatedEntitiesDelete.class)
                .add(TestFavouriteAndMyFilters.class)
                .add(TestPopularFilters.class)
                .add(TestSearchFilters.class)
                .add(TestSearchFiltersShareType.class)
                .add(TestFilterActions.class)
                .add(TestFilterSubscription.class)
                .add(TestFilterSubscriptionXss.class)
                .add(TestEditFilterInvalidShares.class)
                .add(TestSearchRequestViewSecurity.class)
                .add(TestFilterHoldsItsSorting.class)
                .add(TestFilterPicker.class)
                .add(TestFilterWarnings.class)
                .add(TestSharedFilterSearchingByAdmins.class)
                .add(TestDeleteSharedFilterByAdmins.class)
                .add(TestChangeSharedFilterOwnerByAdmins.class)
                .build();
    }
}
