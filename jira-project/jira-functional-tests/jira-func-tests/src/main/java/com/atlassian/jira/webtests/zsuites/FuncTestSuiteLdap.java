package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.tpm.ldap.TestBrowseUserDirectories;
import com.atlassian.jira.webtests.ztests.tpm.ldap.TestCrowdDirectoryMaintenance;
import com.atlassian.jira.webtests.ztests.tpm.ldap.TestDelegatingLdapDirectoryMaintenance;
import com.atlassian.jira.webtests.ztests.tpm.ldap.TestLdapDirectoryMaintenance;
import com.atlassian.jira.webtests.ztests.tpm.ldap.TestTpmDelegatingLdap;
import com.atlassian.jira.webtests.ztests.tpm.ldap.TestTpmLdap;
import com.atlassian.jira.webtests.ztests.tpm.ldap.TestTpmLdapAdvanced;
import com.atlassian.jira.webtests.ztests.tpm.ldap.TestTpmLdapRename;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * @since v4.3
 */
public class FuncTestSuiteLdap {
    public static List<Class<?>> suite() {
        // TestTpmLdapSetup must be run FIRST
        return ImmutableList.<Class<?>>builder()
                .add(TestBrowseUserDirectories.class)
                .add(TestCrowdDirectoryMaintenance.class)
                .add(TestDelegatingLdapDirectoryMaintenance.class)
                .add(TestLdapDirectoryMaintenance.class)
                .add(TestTpmDelegatingLdap.class)
                .add(TestTpmLdap.class)
                .add(TestTpmLdapAdvanced.class)
                .add(TestTpmLdapRename.class)
                .build();
    }
}