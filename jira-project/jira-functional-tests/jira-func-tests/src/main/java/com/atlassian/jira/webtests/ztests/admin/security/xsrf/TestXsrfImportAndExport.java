package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;
import org.w3c.dom.Element;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
public class TestXsrfImportAndExport extends BaseJiraFuncTest {
    @Inject
    protected Administration administration;
    @Inject
    private Form form;

    @Test
    @RestoreBlankInstance
    public void testProjectImportExport() throws Exception {
        final String tempFilePath = getTempFilePath(".xml");
        new XsrfTestSuite(
                new XsrfCheck("ExportXmlData", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.BACKUP_DATA);
                        tester.setFormElement("filename", tempFilePath);
                    }
                }, new BackupFormSubmission()),
                new XsrfCheck("ImportXmlData", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.RESTORE_DATA);
                        tester.setFormElement("filename", tempFilePath);
                    }
                }, new XsrfCheck.FormSubmission("Restore"))
        ).run(getTester(), navigation, form);
    }


    @Test
    public void testProjectImport() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck("ProjectImport", new XsrfCheck.Setup() {
                    public void setup() {
                        administration.projectImport().doImportToSummary("TestProjectImportStandardSimpleData.xml", "TestProjectImportStandardSimpleDataEmptyMKYProject.xml", null);
                    }
                }, new XsrfCheck.FormSubmission("Import"))
        ).run(getTester(), navigation, form);
    }

    private String getTempFilePath(final String suffix) {
        final File tempFile;
        try {
            tempFile = File.createTempFile("TestXsrfImportAndExport_Export", suffix);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return tempFile.getAbsolutePath();
    }

    /**
     * Submitting the XmlBackup action might prompt us with "File already exists - replace?". So we need to do an additional
     * submit in that case.
     */
    private class BackupFormSubmission extends XsrfCheck.FormSubmission {
        public BackupFormSubmission() {
            super("Backup");
        }

        @Override
        public void submitRequest() {
            super.submitRequest();
            final Element replaceSubmit = tester.getDialog().getElement("Replace File");
            if (replaceSubmit != null) {
                tester.submit("Replace File");
            }
        }
    }
}
