package com.atlassian.jira.functest.rule;

import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.TestAnnotationsExtractor;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.util.function.Supplier;

/**
 * The rule will log in user based on  {@link
 * com.atlassian.jira.functest.framework.LoginAs} annotation.
 *
 * @since v6.5
 */
public class LoginAsRule implements TestRule {
    private final Supplier<Navigation> navigationSupplier;
    private boolean loggedIn = false;

    public LoginAsRule(final Supplier<Navigation> navigationSupplier) {
        this.navigationSupplier = navigationSupplier;
    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                tryLogIn(description);
                base.evaluate();
                tryLogOut();
            }
        };
    }

    public void tryLogIn(final Description description) {
        new TestAnnotationsExtractor(description)
                .findAnnotationFor(LoginAs.class)
                .ifPresent(this::loginAsByAnnotation);
    }

    private void loginAsByAnnotation(final LoginAs loginAs) {
        if (loginAs != null) {
            if (loginAs.user() == null) {
                throw new IllegalStateException("You must specify user");
            } else {
                navigationSupplier.get().login(loginAs.user());
                loggedIn = true;
            }
        }
    }

    private void tryLogOut() {
        if (loggedIn) {
            navigationSupplier.get().logout();
        }
    }
}

