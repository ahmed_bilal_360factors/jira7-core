package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.httpclient.api.HttpStatus;
import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.AssertRedirect;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.EditIssueFieldVisibility;
import com.atlassian.jira.functest.framework.Indexing;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.admin.TimeTracking;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.testkit.beans.Priority;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.testkit.client.restclient.Component;
import com.atlassian.jira.testkit.client.restclient.Version;
import com.atlassian.jira.webtests.Groups;
import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Map;
import java.util.stream.Collectors;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.AFFECTS_VERSIONS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.COMPONENTS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.COMPONENT_NAME_ONE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_ASSIGNEE_ERROR_MESSAGE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_ASSIGNEE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_COMPONENTS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_FIX_VERSIONS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_PRIORITY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_VERSIONS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIX_VERSIONS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.VERSION_NAME_ONE;
import static com.atlassian.jira.permission.ProjectPermissions.ASSIGN_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.SCHEDULE_ISSUES;
import static java.util.function.Function.identity;
import static org.junit.Assert.fail;

@WebTest({Category.FUNC_TEST, Category.ISSUES})
@RestoreBlankInstance
@LoginAs(user = ADMIN_USERNAME)
public class TestCreateIssue extends BaseJiraFuncTest {
    private static final String PROJECT_MONKEY_ID = "10001";
    private static final String PROJECT_HOMOSAPIEN_ID = "10000";
    private static final String LOGIN = "log in";

    @Inject
    private AssertRedirect assertRedirect;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Indexing indexing;

    @Inject
    private EditIssueFieldVisibility editIssueFieldVisibility;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        backdoor.darkFeatures().enableForSite("jira.no.frother.reporter.field");
    }

    @After
    public void tearDown() {
        backdoor.darkFeatures().disableForSite("jira.no.frother.reporter.field");
    }

    @Test
    public void testCreateIssueInJiraWithMultipleProjectsAndManyIssueTypes() {
        tester.clickLink("create_link");
        tester.assertTextPresent("Create Issue"); // step 1 present as there are two projects
        tester.assertTextNotPresent("CreateIssueDetails.jspa");
        tester.assertFormElementPresent("pid");
        tester.assertFormElementPresent("issuetype");
        tester.assertFormElementNotPresent("summary");
    }

    @Test
    @Restore("TestOneProjectWithOneIssueType.xml")
    public void testCreateIssueInJiraWithSingleProjectAndSingleIssueType() {
        // TestOneProjectWithOneIssueType.xml is set up with a single project that uses "Bugs Only" issue type scheme,
        // which has only a single "Bug" issue type.

        tester.clickLink("create_link");
        tester.assertTextPresent("CreateIssueDetails.jspa");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Project", "homosapien", "Issue Type", "Bug"});
        tester.assertFormElementPresent("summary");

        final Long projectId = backdoor.project().getProjectId(PROJECT_HOMOSAP_KEY);
        // Additionally, we have a "Bugs & Sub-tasks" issue type scheme
        tester.gotoPage("/secure/admin/SelectIssueTypeSchemeForProject!default.jspa?projectId=" + projectId);

        tester.checkCheckbox("createType", "chooseScheme");
        // Select 'Bugs & Sub-tasks' from select box 'schemeId'.
        tester.selectOption("schemeId", "Bugs & Sub-tasks");
        tester.submit();

        //make sure we're no longer in the admin section (where the create issue link is no longer displayed).
        if (tester.getDialog().isLinkPresent("leave_admin")) {
            tester.clickLink("leave_admin");
        }
        tester.clickLink("create_link");
        tester.assertTextPresent("CreateIssueDetails.jspa");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Project", "homosapien", "Issue Type", "Bug"});
        tester.assertFormElementPresent("summary");
    }

    @Test
    public void testCreateIssue() {
        editIssueFieldVisibility.resetFields();

        final String issueKey1 = createIssue();
        final String issueKey2 = createIssueWithoutComponents();
        final String issueKey3 = createIssueWithTimeTrackingDetails();
        final String issueKey4 = createIssueWithoutAssignee();

        createIssueWithNoSummary();
        createIssueWithRequiredFields();
        createIssueWithHiddenFields();
        createIssueWithInvalidDueDate();
        createIssueWithCreatePermission();
        createIssueWithSchedulePermission();
        createIssueWithAssignPermission();
        createIssueWithModifyReporterPermission();
        createIssueWithTimeTracking();
        createIssueWithUnassignableUser();

        deleteCreatedIssue(issueKey1);
        deleteCreatedIssue(issueKey2);
        deleteCreatedIssue(issueKey3);
        deleteCreatedIssue(issueKey4);
    }

    /**
     * Checks that step 1 (choose project and issue type) of create issue is skipped if there is nothing for the user to choose on the form.
     * If there is only one project in which the user has create issue permission or the project id is preselected
     * and there is only one issue type available, then expect to be redirected to the create issue details page.
     */
    @Test
    public void testCreateIssueSkipStep1OnlyOneProjectAndOneIssueType() {
        final String[] data = new String[]
                {
                        "TestCreateIssueOneProjectOneIssueType.xml",           // one project
                        "TestCreateIssueOneIssueCreateProjectOneIssueType.xml" // two projects but only one has create issue perm
                };
        for (final String dataBackup : data) {
            navigation.gotoDashboard();
            administration.restoreData(dataBackup);
            assertRedirect.assertRedirectAndFollow("/secure/CreateIssue!default.jspa", ".*CreateIssue\\.jspa\\?pid=" + PROJECT_MONKEY_ID + "&issuetype=3$");
            textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Create Issue", "CreateIssueDetails.jspa", "Project", "monkey", "Issue Type", "Task", "Summary"});
        }
    }

    @Test
    @Restore("TestCreateMonkeyHasOneIssueType.xml")
    public void testCreateIssueSkipStep1IssueTypeSchemeInfersOneProjectAndIssueType() {
        // check the preselection of the project in the form to "current project"
        navigation.browseProject(PROJECT_MONKEY_KEY);
        tester.clickLink("create_link");
        tester.assertRadioOptionSelected("pid", PROJECT_MONKEY_ID);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Create Issue"});

        // check we redirect to step 2 if we pass a pid url param for a project which has one issue type
        assertRedirect.assertRedirectAndFollow("/secure/CreateIssue!default.jspa?pid=" + PROJECT_MONKEY_ID, ".*CreateIssue\\.jspa\\?pid=" + PROJECT_MONKEY_ID + "&issuetype=1$");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Create Issue", "CreateIssueDetails.jspa", "Project", "monkey", "Issue Type", "Bug", "Summary"});

        // check that we do not redirect for homosapien since there are multiple issue types to choose from
        tester.gotoPage("/secure/CreateIssue!default.jspa?pid=" + PROJECT_HOMOSAPIEN_ID);
        tester.assertRadioOptionSelected("pid", PROJECT_HOMOSAPIEN_ID);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Create Issue"});

        // logout and then rerequest the url
        navigation.logout();
        tester.gotoPage("/secure/CreateIssue!default.jspa?pid=" + PROJECT_MONKEY_ID);
        tester.assertTextPresent("You are not logged in, and do not have the permissions required to create an issue in this project as a guest.");
        tester.clickLinkWithText(LOGIN);
        tester.setFormElement("os_username", ADMIN_USERNAME);
        tester.setFormElement("os_password", ADMIN_USERNAME);
        tester.setWorkingForm("login-form");
        tester.submit();
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Create Issue", "CreateIssueDetails.jspa", "Project", "monkey", "Issue Type", "Bug", "Summary"});
    }

    @Test
    @Restore("TestCreateIssueOneProjectOneIssueType.xml")
    @LoginAs(user = FRED_USERNAME)
    public void testCreateIssueUserHasNoCreateIssuePermission() {
        // check the preselection of the project in the form to "current project"
        tester.assertLinkNotPresent("create_link");
        navigation.browseProject(PROJECT_MONKEY_KEY);
        tester.assertLinkNotPresentWithText("Create a new issue in project monkey");

        // now go direct to these URLs
        // if they choose the project on the URL we should forbid them in project permission terms
        tester.gotoPage("/secure/CreateIssue!default.jspa?pid=" + PROJECT_MONKEY_ID);
        tester.assertTextPresent("Error");
        tester.assertTextPresent("You do not have permission to create issues in this project.");

        tester.gotoPage("/secure/CreateIssue!default.jspa");
        tester.assertTitleEquals("Error - jWebTest JIRA installation");
        textAssertions.assertTextPresent(new CssLocator(tester, "#content .error"), "You have not selected a valid project to create an issue in.");
    }

    @Test
    @Restore("TestCreateIssueOneProjectThreeIssueTypes.xml")
    public void testCreateIssueWithNoPidInUrlOneProjectAvailableIssueTypeInUrl() {
        tester.clickLink("create_link");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Create Issue"});

        final String monkeyTask = ".*CreateIssue\\.jspa\\?pid=" + PROJECT_MONKEY_ID + "&issuetype=3$";
        assertRedirect.assertRedirectAndFollow("/secure/CreateIssue!default.jspa?issuetype=3", monkeyTask);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Create Issue", "CreateIssueDetails.jspa", "Project", "monkey", "Issue Type", "Task", "Summary"});
    }

    private void deleteCreatedIssue(final String issueKey) {
        navigation.issue().deleteIssue(issueKey);
        assertPageDoesNotExist("The issue has not been removed from the index.", "/si/jira.issueviews:issue-xml/" + issueKey + "/" + issueKey + ".xml?jira.issue.searchlocation=index");
    }

    /**
     * Test 1: Creating issue with summary, priority, components, fix versions, affects versions, environment and description
     */
    public String createIssue() {
        // Creating issue with summary, priority, components, fix versions, affects versions, environment and description
        final String versionOneId = getVersionMap().get(VERSION_NAME_ONE).id.toString();
        final String issueKey = navigation.issue().createIssue(PROJECT_HOMOSAP, "Bug", "test 1", ImmutableMap.of(
                FIELD_COMPONENTS, new String[]{getComponentMap().get(COMPONENT_NAME_ONE).id.toString()},
                FIELD_VERSIONS, new String[]{versionOneId},
                FIELD_FIX_VERSIONS, new String[]{versionOneId},
                FIELD_PRIORITY, new String[]{getPriorityMap().get("Minor").getId()},
                FIELD_ASSIGNEE, new String[]{ADMIN_USERNAME}
        ));
        navigation.issue().gotoIssue(issueKey);

        tester.assertTextPresent("test 1");
        tester.assertTextPresent("Bug");
        tester.assertTextPresent("Minor");

        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("summary", "test 1"), null, issueKey);
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("key", issueKey), null, issueKey);

        return issueKey;
    }

    /**
     * Test 2: Creating issue WITHOUT components, fix versions and affects versions
     */
    public String createIssueWithoutComponents() {
        // Creating issue WITHOUT components, fix versions and affects versions
        final String issueKey = navigation.issue().createIssue(PROJECT_HOMOSAP, "Improvement", "test 2", ImmutableMap.of(
                FIELD_PRIORITY, new String[]{getPriorityMap().get("Major").getId()},
                FIELD_ASSIGNEE, new String[]{ADMIN_USERNAME}
        ));
        navigation.issue().gotoIssue(issueKey);

        tester.assertTextPresent("test 2");
        tester.assertTextPresent("Improvement");
        tester.assertTextPresent("Major");

        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("type", "Improvement"), null, issueKey);
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("summary", "test 2"), null, issueKey);

        return issueKey;
    }

    /**
     * Test 3: Creating issue WITH time tracking details and WITHOUT components, fix versions and affects versions
     */
    public String createIssueWithTimeTrackingDetails() {
        // Creating issue WITH time tracking details
        administration.timeTracking().enable(TimeTracking.Mode.LEGACY);
        final String issueKey = navigation.issue().createIssue(PROJECT_HOMOSAP, "New Feature", "test 3", ImmutableMap.of(
                FIELD_PRIORITY, new String[]{getPriorityMap().get("Critical").getId()},
                FIELD_ASSIGNEE, new String[]{ADMIN_USERNAME},
                "timetracking", new String[]{"1w"}
        ));
        navigation.issue().gotoIssue(issueKey);

        tester.assertTextPresent("test 3");
        tester.assertTextPresent("New Feature");
        tester.assertTextPresent("Critical");
        tester.assertTextPresent("Original Estimate");
        tester.assertTextPresent("1 week");
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("priority", "Critical"), null, issueKey);
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("type", "New Feature"), null, issueKey);
        administration.timeTracking().disable();
        return issueKey;
    }

    /**
     * Test 4: Creating an issue WITHOUT an assignee and WITHOUT fix versions and affects version
     */
    public String createIssueWithoutAssignee() {
        // Creating an issue without an assignee
        administration.generalConfiguration().setAllowUnassignedIssues(true);
        final String issueKey = navigation.issue().createIssue(PROJECT_HOMOSAP, "Task", "test 4", ImmutableMap.of(
                FIELD_COMPONENTS, new String[]{getComponentMap().get(COMPONENT_NAME_ONE).id.toString()},
                FIELD_PRIORITY, new String[]{getPriorityMap().get("Blocker").getId()},
                FIELD_ASSIGNEE, new String[]{null},
                "environment", new String[]{"test environment 4"}
        ));
        navigation.issue().gotoIssue(issueKey);

        tester.assertTextPresent("test 4");
        tester.assertTextPresent("Task");
        tester.assertTextPresent("Blocker");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Assignee:", "Unassigned");
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("priority", "Blocker"), null, issueKey);
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("environment", "test environment 4"), null, issueKey);
        navigation.issue().assignIssue(issueKey, "Assigning issue to ADMIN", ADMIN_FULLNAME);
        administration.generalConfiguration().setAllowUnassignedIssues(false);
        return issueKey;
    }

    public void createIssueWithNoSummary() {
        logger.log("Create Issue: Adding issue without summary");
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");

        // Not setting summary

        // Need to set priority as this is automatically set
        tester.selectOption("priority", "Minor");

        tester.submit();
        tester.assertTextPresent("CreateIssueDetails.jspa");
        tester.assertTextPresent("You must specify a summary of the issue.");
    }

    /**
     * Makes the fields Components,Affects Versions and Fixed Versions required.
     * Attempts to create an issue with required fields not filled out and with an invalid assignee
     */
    public void createIssueWithRequiredFields() {
        // Set fields to be required
        editIssueFieldVisibility.setRequiredFields();

        logger.log("Create Issue: Test the creation of an issue using required fields");
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");

        tester.setFormElement("summary", "This is a new summary");
        tester.setFormElement("reporter", "");

        tester.submit("Create");

        tester.assertTextPresent("CreateIssueDetails.jspa");
        tester.assertTextPresent("Component/s is required");
        tester.assertTextPresent("Affects Version/s is required");
        tester.assertTextPresent("Fix Version/s is required");

        // Reset fields to be optional
        editIssueFieldVisibility.resetFields();
    }

    /**
     * Makes the fields Components,Affects Versions and Fixed Versions hidden.
     */
    public void createIssueWithHiddenFields() {
        // Hide fields
        editIssueFieldVisibility.setHiddenFields(COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setHiddenFields(AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setHiddenFields(FIX_VERSIONS_FIELD_ID);

        logger.log("Create Issue: Test the creation of am issue using hidden fields");
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");

        tester.assertFormElementNotPresent("components");
        tester.assertFormElementNotPresent("versions");
        tester.assertFormElementNotPresent("fixVersions");

        // Reset fields to be optional
        editIssueFieldVisibility.resetFields();
    }

    public void createIssueWithInvalidDueDate() {
        logger.log("Create Issue: Adding issue with invalid due date");
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");

        tester.assertTextPresent("CreateIssueDetails.jspa");

        tester.setFormElement("summary", "stuff");
        tester.setFormElement("duedate", "stuff");

        tester.submit("Create");

        tester.assertTextPresent("CreateIssueDetails.jspa");

        tester.assertTextPresent("You did not enter a valid date. Please enter the date in the format &quot;d/MMM/yy&quot;");
    }

    /**
     * Tests if the 'Create New Issue' Link is available with the 'Create Issue' permission removed
     */
    public void createIssueWithCreatePermission() {
        logger.log("Create Issue: Test availability of 'Create Issue' link with 'Create Issue' permission.");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, CREATE_ISSUES, Groups.USERS);
        navigation.gotoDashboard();
        tester.assertLinkNotPresent("create_link");

        // Grant 'Create Issue' permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, CREATE_ISSUES, Groups.USERS);
        navigation.gotoDashboard();
        tester.assertLinkPresent("create_link");
    }

    /**
     * Tests if the Due Date' field is available with the 'Schedule Issue' permission removed
     */
    public void createIssueWithSchedulePermission() {
        logger.log("Create Issue: Test prescence of 'Due Date' field with 'Schedule Issue' permission.");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, SCHEDULE_ISSUES, Groups.DEVELOPERS);
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");
        tester.assertTextNotPresent("Due Date");

        // Grant Schedule Issue Permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, SCHEDULE_ISSUES, Groups.DEVELOPERS);
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");
        tester.assertTextPresent("Due Date");
    }

    /**
     * Tests if the user is able to assign an issue with the 'Assign Issue' permission removed
     */
    public void createIssueWithAssignPermission() {
        logger.log("Create Issue: Test ability to specify assignee with 'Assign Issue' permission.");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ASSIGN_ISSUES, Groups.DEVELOPERS);
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");
        tester.assertFormElementNotPresent("assignee");

        // Grant Assign Issue Permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ASSIGN_ISSUES, Groups.DEVELOPERS);
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");
        tester.assertFormElementPresent("assignee");
    }

    /**
     * Tests if the 'Reporter' Option is available with the 'Modify Reporter' permission removed
     */
    public void createIssueWithModifyReporterPermission() {
        logger.log("Create Issue: Test availability of Reporter with 'Modify Reporter' permission.");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.MODIFY_REPORTER, Groups.ADMINISTRATORS);
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");
        tester.assertFormElementNotPresent("reporter");

        // Grant Modify Reporter Permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.MODIFY_REPORTER, Groups.ADMINISTRATORS);
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");
        tester.assertFormElementPresent("reporter");
    }

    /**
     * Tests if the 'Orignial Estimate' Link is available with Time Tracking activated
     */
    public void createIssueWithTimeTracking() {
        logger.log("Create Issue: Test availability of time tracking ...");
        administration.timeTracking().disable();
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");
        tester.assertFormElementNotPresent("timetracking");

        administration.timeTracking().enable(TimeTracking.Mode.LEGACY);
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");
        tester.assertFormElementPresent("timetracking");
        administration.timeTracking().disable();
    }

    /**
     * Tests if the user is able to assign an issue with the 'Assignable User' permission removed
     */
    public void createIssueWithUnassignableUser() {
        logger.log("Create Issue: Attempt to set the assignee to be an unassignable user ...");

        // Remove assignable permission
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGNABLE_USER, Groups.DEVELOPERS);
        navigation.issue().goToCreateIssueForm(PROJECT_HOMOSAP, "Bug");
        tester.submit("Create");
        tester.setFormElement("summary", "Test summary");

        tester.assertTextPresent(DEFAULT_ASSIGNEE_ERROR_MESSAGE);

        //Restore permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGNABLE_USER, Groups.DEVELOPERS);
    }

    @Test
    public void testEscapeProjectNameOnFirstScreenOfCreateIssue() {
        backdoor.project().addProject("Xss PRoject &trade;", "XP", ADMIN_USERNAME);

        //make sure we're no longer in the admin section (where the create issue link is no longer displayed).
        if (tester.getDialog().isLinkPresent("leave_admin")) {
            tester.clickLink("leave_admin");
        }
        tester.clickLink("create_link");
        tester.assertTextNotPresent("&trade;");
        tester.assertTextPresent("&amp;trade;");
    }


    // JRA-16369
    @Test
    public void testXssInDueDate() {
        final String value = "\"><script>";
        final String valueEncoded = "&quot;&gt;&lt;script&gt;";
        final String expectedFormElement = "<input class=\"text medium-field\" id=\"duedate\" name=\"duedate\" type=\"text\" value=\"";
        final String notExpected = expectedFormElement + value + "\"";
        final String expected = expectedFormElement + valueEncoded + "\"";

        tester.clickLink("create_link");
        tester.submit("Next");
        tester.setFormElement("duedate", value);
        tester.submit("Create");
        textAssertions.assertTextPresent(locator.page().getHTML(), expected);
        textAssertions.assertTextNotPresent(locator.page().getHTML(), notExpected);
    }

    @Test
    public void testCreateButtonEncoding() {
        try {
            navigation.userProfile().changeUserLanguage("fran\u00e7ais (France)");
            navigation.issue().goToCreateIssueForm("homosapien", "Bogue");
            assertions.assertNodeHasText(locator.xpath("//input[@id='issue-create-submit']/@value"), "Cr\u00E9er"); // UNICODE e-acute=00E9
        } finally {
            navigation.userProfile().changeUserLanguageToJiraDefault();
        }
    }

    /**
     * This will try and restrive the given URL and assert that it fails to be retrieved.
     *
     * @param assertionMessage the assertion message
     * @param url              the url to the page we DONT want to exist
     */
    private void assertPageDoesNotExist(final String assertionMessage, final String url) {
        logger.log("asserting that the page does not exist. [" + url + "]");
        tester.gotoPage(url);
        if (tester.getDialog().getResponse().getResponseCode() == HttpStatus.OK.code) {
            fail(assertionMessage);
        }
    }

    private Map<String, Priority> getPriorityMap() {
        return backdoor.getTestkit().priorities().getPriorities().stream().collect(Collectors.toMap(Priority::getName, identity()));
    }

    private Map<String, Version> getVersionMap() {
        return backdoor.project().getVersionsForProject(PROJECT_HOMOSAP_KEY).stream().collect(Collectors.toMap(v -> v.name, identity()));
    }

    private Map<String, Component> getComponentMap() {
        return backdoor.project().getComponentsForProject(PROJECT_HOMOSAP_KEY).stream().collect(Collectors.toMap(v -> v.name, identity()));
    }
}
