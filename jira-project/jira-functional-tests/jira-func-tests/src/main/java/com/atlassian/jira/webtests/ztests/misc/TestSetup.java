package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.core.util.StringUtils;
import com.atlassian.httpclient.api.HttpStatus;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.Navigation.AdminSection;
import com.atlassian.jira.functest.framework.admin.TimeTracking;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.backdoor.ComponentManagerControl;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.rule.SkipCacheCheck;
import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.testkit.client.restclient.User;
import com.atlassian.jira.testkit.client.restclient.UserClient;
import com.atlassian.jira.webtests.LicenseKeys;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebResponse;
import com.meterware.httpunit.WebTable;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Set;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

@WebTest({Category.FUNC_TEST, Category.BROWSING, Category.LICENSING, Category.DATABASE})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
@SkipCacheCheck
public class TestSetup extends BaseJiraFuncTest {
    private static final String CONFIGURE_APP_PROPERTIES_TITLE = "Set up application properties";
    private static final String SET_UP_LICENSE = "Specify your license key";
    private static final String SET_UP_ADMIN_ACCOUNT_TITLE = "Set up administrator account";
    private static final String URL_LICENSE_VALIDATION = "/secure/SetupLicense!validateLicense.jspa";

    /**
     * Try to navigate to all Setup URLs to ensure Setup cannot be run again once it has already been run
     */
    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testSetupCannotBeRunTwice() {
        administration.restoreBlankInstance();
        String[] actions = new String[]{
                "SetupDatabase.jspa", "SetupApplicationProperties.jspa", "SetupApplicationProperties!default.jspa",
                "SetupAdminAccount.jspa", "SetupAdminAccount!default.jspa",
                "SetupMailNotifications.jspa", "SetupMailNotifications!default.jspa",
                "SetupComplete.jspa", "SetupComplete!default.jspa",
                "SetupImport.jspa", "SetupImport!default.jspa"};

        String[] views = new String[]{
                "setup-application-properties.jsp", "setup-admin-account.jsp",
                "setup-mail-notifications.jsp", "setup-import.jsp"};

        for (String action : actions) {
            tester.gotoPage("/secure/" + action);
            assertSetupAlreadyLong();
        }

        for (String view : views) {
            tester.gotoPage("/views/" + view);
            assertSetupAlreadyShort();
        }
    }

    @Test
    public void testMissingTitle() throws Exception {
        // Revert to not set up state
        gotoSetUpApplicationProperties();
        tester.submit();
        // We should not be allowed to continue
        tester.assertTextPresent(CONFIGURE_APP_PROPERTIES_TITLE);
        tester.assertTextPresent("You must specify a title.");
    }

    /**
     * Invalid base URLs should be rejected.
     */
    @Test
    public void testInvalidBaseUrls() throws Exception {
        String[] invalidBaseUrls = {
                "",
                "http",
                "http://",
                "http://*&^%$#@",
                "http://example url.com:8090",
                "ldap://example.url.com:8090",
                "http://example.url.com:not_a_port",
                "http://example.url.com:8090/invalid path"
        };

        gotoSetUpApplicationProperties();
        for (String invalidBaseUrl : invalidBaseUrls) {
            tester.setFormElement("baseURL", invalidBaseUrl);
            tester.submit();
            tester.assertTextPresent("The URL specified is not valid.");
        }
    }

    @Test
    public void testMissingLicense() throws Exception {
        // Revert to not set up state
        doConfigureApplicationProperties();

        tester.setWorkingForm("setupLicenseForm");
        tester.submit();
        // We should not be allowed to continue
        tester.assertTextPresent(SET_UP_LICENSE);
    }

    @Test
    public void testInvalidLicense() throws Exception {
        doConfigureApplicationProperties();

        tester.setWorkingForm("setupLicenseForm");
        tester.setFormElement("setupLicenseKey", "blah");
        tester.submit();
        tester.assertTextPresent(SET_UP_LICENSE);
    }

    @Test
    public void testInvalidSmtpPorts() throws Exception {
        doConfigureApplicationProperties();
        doSetupLicense();
        doSetUpAdminAccount();

        try {
            //Lets try an invalid ports and make sure they don't work.
            tester.checkRadioOption("noemail", "false");
            tester.setFormElement("serverName", "localhost");
            tester.setFormElement("port", "-1");
            tester.submit("finish");
            tester.assertTextPresent("SMTP port must be a number between 0 and 65535");

            tester.setFormElement("serverName", "localhost");
            tester.setFormElement("port", String.valueOf(0xFFFF + 1));
            tester.submit("finish");
            tester.assertTextPresent("SMTP port must be a number between 0 and 65535");
        } finally {
            tester.setFormElement("serverName", "localhost");
            tester.setFormElement("port", "23000");
            tester.submit("finish");
        }
    }

    @Test
    public void testSetupWithDefaulExctDirectories() throws IOException {
        // Revert to not set up state
        gotoSetUpApplicationProperties();
        // Fill in mandatory fields
        tester.setWorkingForm("jira-setupwizard");
        tester.setFormElement("title", "My JIRA");
        // Submit Step 'Configure Application Properties' with Default paths.
        tester.submit();
        // Configure the license
        doSetupLicense();
        // Finish setup with standard values
        doSetUpAdminAccountAndEmailNotifications();

        // Now assert JIRA is setup as expected:

        // Attachments
        navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);
        assertions.assertNodeHasText(new CssLocator(tester, "h2"), "Attachments");
        // Assert the cells in table 'AttachmentSettings'.
        assertNotNull(tester.getDialog().getWebTableBySummaryOrId("table-AttachmentSettings"));
        // Assert row 1: |Allow Attachments|ON|
        assertions.assertNodeHasText(new XPathLocator(tester, tableAttachmentSettingsRowHeading(1)), "Allow Attachments");
        assertions.assertNodeHasText(new XPathLocator(tester, tableAttachmentSettingsRowValue(1)), "ON");
        // Assert row 2: |Attachment Path|Default Directory [/home/mlassau/jira_homes/jira_trunk/data/attachments]|
        assertions.assertNodeHasText(new XPathLocator(tester, tableAttachmentSettingsRowHeading(2)), "Attachment Path");
        assertions.assertNodeHasText(new XPathLocator(tester, tableAttachmentSettingsRowValue(2)), "Default Directory [");

        // Indexes
        navigation.gotoAdminSection(Navigation.AdminSection.INDEXING);
        tester.assertTextPresent("Re-Indexing");

        // Automated Backups
        administration.services().goTo();
        tester.assertTextPresent("Backup Service");
        tester.assertTextPresent("<strong>USE_DEFAULT_DIRECTORY:</strong> true");
        tester.assertTextNotPresent("DIR_NAME:");

        assertTimeTrackingActivationAndDefaultValues();
        assertIssueLinking();
    }

    private String tableAttachmentSettingsRowHeading(int rowNumber) {
        return "//table[@id='table-AttachmentSettings']/tbody/tr[" + rowNumber + "]/td/strong";
    }


    private String tableAttachmentSettingsRowValue(int rowNumber) {
        return "//table[@id='table-AttachmentSettings']/tbody/tr[" + rowNumber + "]/td";
    }

    @Test
    public void testSetupImportMissingFilename() throws IOException {
        restoreEmptyInstance();
        // Go to SetupImport
        tester.gotoPage("secure/SetupImport!default.jspa");
        tester.assertTextPresent("Import existing data");

        // Use custom path
        tester.setFormElement("filename", "");
        tester.submit();

        // We should not be allowed to continue
        tester.assertTextPresent("Import existing data");
        tester.assertTextPresent("You must enter the location of an XML file.");
        tester.assertTextNotPresent("You must specify a location for index files");
    }

    @Test
    public void testSetupImportInvalidLicense() throws IOException {
        restoreEmptyInstance();
        // Go to SetupImport
        tester.gotoPage("secure/SetupImport!default.jspa");
        tester.assertTextPresent("Import existing data");

        // Use custom path
        tester.setFormElement("filename", File.createTempFile("import", ".xml").getAbsolutePath());
        tester.setFormElement("license", "wrong");
        tester.submit();
        administration.waitForRestore();

        // We should not be allowed to continue
        tester.assertTextPresent("Import existing data");
        tester.assertTextPresent("This license is invalid.");
        tester.assertTextNotPresent("You must enter the location of an XML file.");
    }

    @Test
    public void testSetupImportWithDodgyIndexPath() throws IOException {
        restoreEmptyInstance();

        //By creating a file for the index path, we'll force the failure of the index path directory creation
        File indexPath = File.createTempFile("testXmlImportWithInvalidIndexDirectory", null);
        indexPath.deleteOnExit();

        File dataFile = administration.replaceTokensInFile("TestSetupInvalidIndexPath.xml", ImmutableMap.of("@@INDEX_PATH@@", indexPath.getAbsolutePath()));

        // Go to SetupImport
        tester.gotoPage("secure/SetupImport!default.jspa");
        tester.assertTextPresent("Import existing data");

        // Use default index path
        // We import an XML file with an old license (please do not update license in this file).
        // Now we prove that the optional license is actually used.
        tester.setFormElement("filename", dataFile.getAbsolutePath());
        tester.submit();
        administration.waitForRestore();

        textAssertions.assertTextPresent(new WebPageLocator(tester),
                "Cannot write to index directory. Check that the application server and JIRA have permissions to "
                        + "write to: " + indexPath.getAbsolutePath());
    }

    @Test
    public void testSetupImportWithDodgyAttachmentPath() throws IOException {
        restoreEmptyInstance();

        //By creating a file for the index path, we'll force the failure of the index path directory creation
        File attachmentPath = File.createTempFile("testXmlImportWithInvalidAttachmentDirectory", null);
        attachmentPath.deleteOnExit();

        File dataFile = administration.replaceTokensInFile("TestSetupInvalidAttachmentPath.xml",
                ImmutableMap.of("@@ATTACHMENT_PATH@@", attachmentPath.getAbsolutePath()));

        // Go to SetupImport
        tester.gotoPage("secure/SetupImport!default.jspa");
        tester.assertTextPresent("Import existing data");

        // Use default index path
        // We import an XML file with an old license (please do not update license in this file).
        // Now we prove that the optional license is actually used.
        tester.setFormElement("filename", dataFile.getAbsolutePath());
        tester.submit();
        administration.waitForRestore();

        textAssertions.assertTextPresent(new WebPageLocator(tester), "Cannot write to attachment directory. Check that the "
                + "application server and JIRA have permissions to write to: " + attachmentPath.getAbsolutePath());
    }

    @Test
    public void testSetupImportShouldDisplayAnErrorWhenAttemptingToDowngradeFromAnAllowedVersion() throws Exception {
        restoreEmptyInstance();
        File dataFile = administration.replaceTokensInFile("TestSetupDowngrade.xml", ImmutableMap.of("@@VERSION@@", "4.0"));

        // try to import the XML backup, which has a higher buildnumber
        tester.gotoPage("secure/SetupImport!default.jspa");
        tester.setFormElement("filename", dataFile.getAbsolutePath());
        tester.submit();
        administration.waitForRestore();

        // test that the attempted downgrade error message is shown and the user is given the option of ignoring the
        // error and downgrading anyway.
        textAssertions.assertTextPresent(locator.css("div.error"), "You are attempting to import data from JIRA X");
        textAssertions.assertTextPresent(locator.css("div.error"), "Click here to acknowledge this error and proceed anyway.");
        assertions.assertNodeExists(locator.css("a#acknowledgeDowngradeError"));
    }

    @Test
    public void testSetupImportShouldAllowDowngradeOnceDowngradeErrorHasBeenAcknowledged() throws Exception {
        restoreEmptyInstance();
        File dataFile = administration.replaceTokensInFile("TestSetupDowngrade.xml", ImmutableMap.of("@@VERSION@@", "4.0"));

        // try to import the XML backup, which has a higher buildnumber
        tester.gotoPage("secure/SetupImport!default.jspa?downgradeAnyway=true"); // force downgradeAnyway=true
        tester.setFormElement("filename", dataFile.getAbsolutePath());
        tester.submit();
        administration.waitForRestore();

        // make sure import worked.
        tester.assertTextPresent("You have finished importing your existing data, JIRA is ready to use.  Please log in and get started.");
    }

    @Test
    public void testSetupImportDefaultIndexDirectory() throws IOException {
        restoreEmptyInstance();
        // Go to SetupImport
        tester.gotoPage("secure/SetupImport!default.jspa");
        tester.assertTextPresent("Import existing data");

        // Use default index path
        // We import an XML file with an old license (please do not update license in this file).
        // Now we prove that the optional license is actually used.
        tester.setFormElement("filename", new File(environmentData.getXMLDataLocation(), "oldlicense.xml").getAbsolutePath());
        // need to set a new license, or we won't be allowed to log in.
        tester.setFormElement("license", LicenseKeys.COMMERCIAL.getLicenseString());
        tester.submit();
        administration.waitForRestore();
        tester.assertTextPresent("You have finished importing your existing data, JIRA is ready to use.  Please log in and get started.");

        // Now assert JIRA is setup with the Default Index directory
        navigation.disableWebSudo();
        navigation.login(ADMIN_USERNAME);
        navigation.gotoAdminSection(Navigation.AdminSection.INDEXING);
        tester.assertTextPresent("Re-Indexing");
    }

    @Test
    public void testSetupImportDefaultsForSetupComplete() throws IOException {
        restoreEmptyInstance();
        // Go to SetupImport
        tester.gotoPage("secure/SetupImport!default.jspa");
        tester.assertTextPresent("Import existing data");

        // Use default index path
        // We import an XML file with an old license (please do not update license in this file).
        // Now we prove that the optional license is actually used.
        tester.setFormElement("filename", new File(environmentData.getXMLDataLocation(), "oldlicense.xml").getAbsolutePath());
        // need to set a new license, or we won't be allowed to log in.
        tester.setFormElement("license", LicenseKeys.COMMERCIAL.getLicenseString());
        tester.submit();
        administration.waitForRestore();
        tester.assertTextPresent("You have finished importing your existing data, JIRA is ready to use.  Please log in and get started.");

        // Now assert JIRA is setup with sub tasks disabled
        navigation.disableWebSudo();
        navigation.login(ADMIN_USERNAME);

        assertSubTasksDisabled();

        assertDefaultTextRendererIsSetForAllRenderableFields();
    }

    private void assertSubTasksDisabled() {
        assertFalse("Sub-tasks were enabled when they shouldn't have been", administration.subtasks().isEnabled());
    }

    private void assertDefaultTextRendererIsSetForAllRenderableFields() {
        final String[] renderableFields = {"Comment", "Description", "Environment"};
        for (String fieldName : renderableFields) {
            assertEquals("Default Text Renderer", administration.fieldConfigurations().defaultFieldConfiguration().getRenderer(fieldName));
        }
    }

    private void restoreEmptyInstance() {
        administration.restoreNotSetupInstance();
    }

    private void assertTimeTrackingActivationAndDefaultValues() {
        administration.timeTracking().enable(TimeTracking.Mode.LEGACY);
        tester.assertTextPresent("The number of working hours per day is <b>8");
        tester.assertTextPresent("The number of working days per week is <b>5");
    }

    private void assertIssueLinking() {
        tester.gotoPage("secure/admin/ViewLinkTypes!default.jspa");
        WebPageLocator page = new WebPageLocator(tester);
        textAssertions.assertTextPresent(page, "Issue linking is currently ON.");
        textAssertions.assertTextSequence(page, new String[]{"Blocks", "blocks", "is blocked by"});
        textAssertions.assertTextSequence(page, new String[]{"Cloners", "clones", "is cloned by"});
        textAssertions.assertTextSequence(page, new String[]{"Duplicate", "duplicates", "is duplicated by"});
        textAssertions.assertTextSequence(page, new String[]{"Relates", "relates to", "relates to"});
    }

    private void assertSetupAlreadyLong() {
        tester.assertTextPresent("JIRA setup has already completed");
        tester.assertTextPresent("It seems that you have tried to set up JIRA when this process has already been done.");
    }

    private void assertSetupAlreadyShort() {
        tester.assertTextPresent("JIRA has already been set up.");
    }

    private void doConfigureApplicationProperties() {
        gotoSetUpApplicationProperties();

        // Fill in mandatory fields
        tester.setWorkingForm("jira-setupwizard");
        tester.setFormElement("title", "TestSetup JIRA");
        // Submit Step 'Set Up Application Properties' with Default paths.
        tester.submit();
    }

    private void doSetUpAdminAccountAndEmailNotifications() {
        doSetUpAdminAccount();
        doSetUpEmailNotifications();
    }

    private void doSetUpEmailNotifications() {
        logger.log("Noemail");
        tester.submit("finish");
        logger.log("Noemail");
        // During SetupComplete, the user is automatically logged in
        // Assert that the user is logged in by checking if the profile link is present
        tester.assertLinkPresent("header-details-user-fullname");
        navigation.disableWebSudo();
    }

    private void doSetUpAdminAccount() {
        tester.assertTextPresent(SET_UP_ADMIN_ACCOUNT_TITLE);
        tester.setFormElement("username", ADMIN_USERNAME);
        tester.setFormElement("password", ADMIN_USERNAME);
        tester.setFormElement("confirm", ADMIN_USERNAME);
        tester.setFormElement("fullname", "Mary Magdelene");
        tester.setFormElement("email", "admin@example.com");
        tester.submit();
    }

    private void gotoSetUpApplicationProperties() {
        restoreEmptyInstance();
        tester.gotoPage("secure/SetupApplicationProperties.jspa");
        tester.assertTextPresent(CONFIGURE_APP_PROPERTIES_TITLE);
    }

    private void gotoSetUpApplicationProperties(License license) {
        administration.restoreNotSetupInstance(license.getLicenseString());
        tester.gotoPage("secure/SetupApplicationProperties.jspa");
        tester.assertTextPresent(CONFIGURE_APP_PROPERTIES_TITLE);
    }


    private void doSetupLicense(final License license) {
        tester.assertTextPresent(SET_UP_LICENSE);
        tester.setWorkingForm("setupLicenseForm");
        tester.setFormElement("setupLicenseKey", license.getLicenseString());
        tester.submit();
        tester.assertTextPresent(SET_UP_ADMIN_ACCOUNT_TITLE);
    }

    private void doSetupLicense() {
        doSetupLicense(LicenseKeys.COMMERCIAL);
    }

    @Test
    public void testShouldGiveAdminApplicationAccess() {
        doConfigureApplicationProperties();
        doSetupLicense(LicenseKeys.CORE_ROLE);
        doSetUpAdminAccount();
        doSetUpEmailNotifications();
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_USERNAME);
        navigation.gotoPage(Navigation.AdminSection.SYSTEM_INFO.getUrl());
    }

    @Test
    public void testShouldGiveAdminMultiApplicationAccess() {
        doConfigureApplicationProperties();
        doSetupLicense(LicenseKeys.MULTI_ROLE);
        doSetUpAdminAccount();
        doSetUpEmailNotifications();
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_USERNAME);
        navigation.gotoPage(Navigation.AdminSection.SYSTEM_INFO.getUrl());

        final Set<String> expectedApplications = ImmutableSet.of("jira-servicedesk", "jira-software", "jira-reference", "jira-func-test", "jira-core");

        UserClient userClient = new UserClient(environmentData);
        Set<String> actualApplications = userClient.get(ADMIN_USERNAME, User.Expand.applicationRoles)
                .applicationRoles.items.stream()
                .map(role -> role.key)
                .collect(CollectorsUtil.toImmutableSet());

        assertEquals("admin has access to all licensed apps", expectedApplications, actualApplications);
    }

    @Test
    public void testThatSetupConfiguresApplicationDefaults() throws SAXException {
        gotoSetUpApplicationProperties(LicenseKeys.TEST_ROLE);
        // Fill in mandatory fields
        tester.setWorkingForm("jira-setupwizard");
        tester.setFormElement("title", "TestSetup JIRA");
        // Submit Step 'Set Up Application Properties' with Default paths.
        tester.submit();

        doSetupLicense(LicenseKeys.TEST_ROLE);
        doSetUpAdminAccount();
        doSetUpEmailNotifications();

        navigation.gotoAdminSection(AdminSection.GROUP_BROWSER);
        tester.assertTextPresent("jira-users");
        tester.clickLink("del_jira-users");
        tester.submit("Delete");
        tester.assertTextNotPresent("jira-users");
        tester.assertTextPresent("jira-developers");
        tester.clickLink("del_jira-developers");
        tester.submit("Delete");
        tester.assertTextNotPresent("jira-developers");

        navigation.gotoAdminSection(AdminSection.GLOBAL_PERMISSIONS);

        final String TEST_DEFAULT_GROUP_NAME = "jira-test-group";

        assertAllGlobalPermissionsPresent();
        assertAllGlobalPermissionsGrantedOnAdminGroup();
        assertAllGlobalPermissionsGrantedOnDefaultGroup(TEST_DEFAULT_GROUP_NAME);

        navigation.gotoAdminSection(AdminSection.USER_BROWSER);
        tester.setFormElement("userSearchFilter", "admin");
        tester.submit("");
        tester.assertTextPresent("admin");
        tester.assertTextPresent(TEST_DEFAULT_GROUP_NAME);
    }

    @Test
    public void testPluginManagerIsFullyStartedAfterInitialImport() {
        // Perform a data import the old slow way (the new fast way avoids the pico restart, which is what we're testing)
        administration.restoreDataSlowOldWay("blankprojects.xml");
        assertThat(getBackdoor().componentManager().state().getPluginSystemState(), is(ComponentManagerControl.PluginSystemState.LATE_STARTED));
    }

    private void assertAllGlobalPermissionsGrantedOnDefaultGroup(final String defaultGroupName) {
        Arrays.stream(GlobalPermissionTableRow.values())
                .filter(globalPermission -> GlobalPermissionTableRow.ADMIN != globalPermission && GlobalPermissionTableRow.SYSTEM_ADMIN != globalPermission)
                .forEach(globalPermission -> assertGlobalPermissionGrantedOnGroup(globalPermission, defaultGroupName));
    }

    private void assertAllGlobalPermissionsGrantedOnAdminGroup() {
        Arrays.stream(GlobalPermissionTableRow.values())
                .forEach(globalPermission -> assertGlobalPermissionGrantedOnGroup(globalPermission, "jira-administrators"));
    }

    private void assertGlobalPermissionGrantedOnGroup(GlobalPermissionTableRow globalPermission, String group) {
        assertGlobalPermissionTableCellHasText(globalPermission.row, 1, group);
    }

    private void assertAllGlobalPermissionsPresent() {
        Arrays.stream(GlobalPermissionTableRow.values())
                .forEach(globalPermission -> assertGlobalPermissionTableCellHasText(globalPermission.row, 0, globalPermission.globalPermissionName));
    }

    private void assertGlobalPermissionTableCellHasText(int row, int column, String expectedText) {
        try {
            WebTable globalPermissionsTable = getTester().getDialog().getResponse().getTableWithID("global_perms");
            assertions.getTableAssertions().assertTableCellHasText(globalPermissionsTable, row, column, expectedText);
        } catch (SAXException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testShouldPassLicenseValidationForValidLicenses() throws SAXException, IOException {
        doConfigureApplicationProperties();
        assertLicense(true, LicenseKeys.COMMERCIAL);
        assertLicenseRaw(true, LicenseKeys.NON_EXISTING_ROLE);
        assertLicense(true, LicenseKeys.MULTI_ROLE);
    }

    @Test
    public void testShouldFailLicenseValidationForInvalidLicenses() throws SAXException, IOException {
        doConfigureApplicationProperties();
        assertLicenseRaw(false, LicenseKeys.INVALID);
        assertLicense(false, LicenseKeys.V1_ENTERPRISE);
    }

    @Test
    public void testLicenseValidationEndpointReturnsJson() throws IOException, SAXException {
        final String license = "AAA<script>alert('test');</script>";
        final WebResponse webResponse = tester.getDialog().getWebClient().sendRequest(licenseValidationRequest(license));
        assertEquals("application/json", webResponse.getContentType());
    }

    private void assertLicense(boolean valid, License license) throws SAXException, IOException {
        assertLicenseRaw(valid, license.getLicenseString());
    }

    private void assertLicenseRaw(boolean valid, String license) throws SAXException, IOException {
        final WebResponse webResponse = tester.getDialog().getWebClient().sendRequest(licenseValidationRequest(license));
        if (valid) {
            assertThat(webResponse.getResponseCode(), is(HttpStatus.OK.code));
        } else {
            assertThat(webResponse.getResponseCode(), is(HttpStatus.FORBIDDEN.code));
        }
    }

    private PostMethodWebRequest licenseValidationRequest(String license) {
        final String licenseValidationUrl = environmentData.getBaseUrl().toExternalForm() + URL_LICENSE_VALIDATION;
        PostMethodWebRequest request = new PostMethodWebRequest(licenseValidationUrl);
        final String licString = StringUtils.replaceAll(license, "\n", "");
        request.setParameter("licenseToValidate", licString);
        return request;
    }

    private enum GlobalPermissionTableRow {
        SYSTEM_ADMIN(1, "JIRA System Administrators"),
        ADMIN(2, "JIRA Administrators"),
        BROWSE_USERS(3, "Browse Users"),
        CREATE_SHARED_OBJECTS(4, "Create Shared Objects"),
        MANAGE_GROUP_FILTER_SUBSCRIPTIONS(5, "Manage Group Filter Subscriptions"),
        BULK_CHANGE(6, "Bulk Change");

        private final int row;
        private final String globalPermissionName;

        GlobalPermissionTableRow(final int row, final String globalPermissionName) {
            this.row = row;
            this.globalPermissionName = globalPermissionName;
        }
    }

}
