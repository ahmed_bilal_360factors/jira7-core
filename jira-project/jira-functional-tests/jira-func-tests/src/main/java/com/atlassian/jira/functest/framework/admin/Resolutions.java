package com.atlassian.jira.functest.framework.admin;

import com.google.inject.ImplementedBy;

/**
 * Framework for manipulating resolutions
 *
 * @since v4.0
 */
@ImplementedBy(ResolutionsImpl.class)
public interface Resolutions {
    /**
     * @param name the name of the resolution to add
     * @return the id of the added resolution
     */
    String addResolution(String name);
}
