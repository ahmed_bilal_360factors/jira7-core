package com.atlassian.jira.webtests.ztests.security;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.page.Error404;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Holds the tests that verify the resolution of web actions when they are specified by an alias in a URL and that
 * the appropriate role checks are applied.
 *
 * @since v5.0.7
 */
@WebTest({Category.FUNC_TEST, Category.SECURITY})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestWebActionResolution extends BaseJiraFuncTest {
    @Inject
    private HtmlPage page;
    @Inject
    private Administration administration;
    @Inject
    private LocatorFactory locator;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
    }

    @Test
    public void testFullyQualifiedClassNameInUrlCanNotBeResolvedToAnAction() throws Exception {
        tester.getDialog().getWebClient().setExceptionsThrownOnErrorStatus(false);

        tester.gotoPage("com.atlassian.jira.web.action.user.ViewUserIssueColumns.jspa");

        assertFalse(tester.getDialog().getResponse().getText().contains(ViewUserIssueColumnsAction.Views.SECURITY_BREACH));
        assertThat(new Error404(tester), Error404.isOn404Page());

        tester.getDialog().getWebClient().setExceptionsThrownOnErrorStatus(true);
    }

    @Test
    public void testCanResolveARootActionGivenThatNoExplicitAliasHasBeenDefinedForIt() throws Exception {
        tester.gotoPage("Dashboard.jspa");

        assertTrue(locator.id("dashboard").exists());
    }

    @Test
    public void testCanResolveACommandActionGivenAnAliasHasBeenDefinedForIt() throws Exception {
        navigation.login(ADMIN_USERNAME);
        tester.gotoPage("AddNewWorkflow.jspa");

        assertTrue(locator.id("add-workflow").exists());
    }

    @Test
    public void testCanNotResolveARootActionGivenThatTheUserDoesNotHaveTheRolesRequiredForIt() throws Exception {
        navigation.login(FRED_USERNAME);
        tester.gotoPage("ImportWorkflowFromXml!default.jspa");

        assertTrue(locator.id("login-form").exists());
    }

    @Test
    public void testCanNotResolveACommandActionGivenThatTheUserDoesNotHaveTheRolesRequiredForItByInheritance() throws Exception {
        navigation.login(FRED_USERNAME);
        tester.gotoPage("IssueLinkingActivate.jspa?" + XsrfCheck.ATL_TOKEN + "=" + page.getFreshXsrfToken());

        assertTrue(locator.id("login-form").exists());
    }

    private static class ViewUserIssueColumnsAction {
        private static class Views {
            public static final String SECURITY_BREACH = "securitybreach";
        }
    }
}
