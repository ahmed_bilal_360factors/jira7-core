package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.issue.fields.rest.json.beans.IssueRefJsonBean;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client.IssueSubTaskClient;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.sun.jersey.api.client.ClientResponse.Status.BAD_REQUEST;
import static com.sun.jersey.api.client.ClientResponse.Status.NOT_FOUND;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Func tests for IssueSubTaskResource.
 *
 * @since v7.2
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestIssueSubTaskResource extends BaseJiraFuncTest {

    private IssueSubTaskClient issueClient;
    private Long projectId;
    private IssueCreateResponse parentIssue;

    @Before
    public void createIssueClient() {
        issueClient = new IssueSubTaskClient(environmentData);
        backdoor.restoreBlankInstance();

        backdoor.subtask().enable();

        projectId = backdoor.project().getProjectId(PROJECT_HOMOSAP_KEY);
        parentIssue = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "summary");
    }

    @Test
    public void getWorks() throws Exception {
        IssueCreateResponse expected = createSubtask("subtask summary");

        List<IssueRefJsonBean> result = getSubtasks();

        assertThat(result, contains(subtask(expected.id())));
    }

    @Test
    public void testGetNonExistingIssue() throws Exception {
        Response response = issueClient.getResponse("NTEXST-1");

        assertThat(response.statusCode, is(404));
    }

    @Test
    public void reorder() throws Exception {
        IssueCreateResponse sub1 = createSubtask("sub1");
        IssueCreateResponse sub2 = createSubtask("sub2");
        IssueCreateResponse sub3 = createSubtask("sub3");

        assertThat(getSubtasks(), contains(subtask(sub1), subtask(sub2), subtask(sub3)));

        issueClient.reorder(parentIssue.key, 0L, 1L);

        assertThat(getSubtasks(), contains(subtask(sub2), subtask(sub1), subtask(sub3)));
    }

    @Test
    public void testOutOfBounds() throws Exception {
        createSubtask("sub1");
        createSubtask("sub2");
        createSubtask("sub3");

        assertThat(issueClient.reorder(parentIssue.key, -1L, 1L).statusCode, is(BAD_REQUEST.getStatusCode()));
        assertThat(issueClient.reorder(parentIssue.key, 0L, -1L).statusCode, is(BAD_REQUEST.getStatusCode()));

        assertThat(issueClient.reorder(parentIssue.key, 0L, 3L).statusCode, is(BAD_REQUEST.getStatusCode()));
        assertThat(issueClient.reorder(parentIssue.key, 3L, 0L).statusCode, is(BAD_REQUEST.getStatusCode()));
    }

    @Test
    public void testNotExistingIssueWhenReordering() throws Exception {
        createSubtask("sub1");
        createSubtask("sub2");

        Response response = issueClient.reorder("i-dont-exist", 0L, 1L);

        assertThat(response.statusCode, is(NOT_FOUND.getStatusCode()));
    }

    private List<IssueRefJsonBean> getSubtasks() {
        return issueClient.get(parentIssue.key());
    }


    private IssueCreateResponse createSubtask(final String summary) {
        return backdoor.issues().createSubtask(projectId.toString(), parentIssue.key(), summary);
    }

    private Matcher<IssueRefJsonBean> subtask(String id) {
        return new TypeSafeMatcher<IssueRefJsonBean>() {
            @Override
            protected boolean matchesSafely(IssueRefJsonBean item) {
                return item.id().equals(id);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("subtask with id ").appendValue(id);
            }
        };
    }

    private Matcher<IssueRefJsonBean> subtask(IssueCreateResponse issue) {
        return subtask(issue.id);
    }
}
