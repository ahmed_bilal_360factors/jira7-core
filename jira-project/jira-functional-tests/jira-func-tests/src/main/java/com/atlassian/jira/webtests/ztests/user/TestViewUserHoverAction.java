package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.FuncTestRuleChain;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.w3c.dom.Element;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * These tests are for the ViewUserHover action, which provides the content for the
 * pop-up when you hover on a user's full name.
 *
 * @since v7.0.1
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
public class TestViewUserHoverAction {
    @Rule
    public TestRule initData = FuncTestRuleChain.forTest(this);

    @Inject
    private Navigation navigation;
    @Inject
    private LocatorFactory locator;

    //    KEY       USERNAME    NAME
    //    bb        betty       Betty Boop
    //    ID10001   bb          Bob Belcher
    //    cc        cat         Crazy Cat
    //    ID10101   cc          Candy Chaos

    @Test
    @Restore("user_rename.xml")
    @LoginAs(user = "admin")
    public void userHoverResultsCorrectWhenLoggedInAsAdmin() {
        assertUserHoverDetailsForMissingUser("doesnotexist");
        assertUserHoverDetails("admin", "Adam Ant", "mlassau@atlassian.com");
        assertUserHoverDetails("bb", "Bob Belcher", "bob@example.com");
        assertUserHoverDetails("betty", "Betty Boop", "betty@example.com");
        assertUserHoverDetails("cat", "Crazy Cat", "cat@example.com");
        assertUserHoverDetails("cc", "Candy Chaos", "candy@example.com");
    }

    @Test
    @Restore("user_rename.xml")
    public void usernameEnumerationNotPossibleWhenAnonymous() {
        assertUserHoverDetailsForAnonymousUser("doesnotexist");
        assertUserHoverDetailsForAnonymousUser("admin");
        assertUserHoverDetailsForAnonymousUser("bb");
        assertUserHoverDetailsForAnonymousUser("betty");
        assertUserHoverDetailsForAnonymousUser("cat");
        assertUserHoverDetailsForAnonymousUser("cc");
    }

    private void gotoUserHover(String username) {
        navigation.gotoPage("/secure/ViewUserHover!default.jspa?decorator=none&username=" + username);
    }

    private void assertUserHoverDetailsForMissingUser(String username) {
        gotoUserHover(username);
        assertFalse(locator.id("avatar-full-name-link").exists());
        assertEquals("User does not exist: " + username, locator.css("div.user-hover-details").getText());
        assertEquals("", locator.id("user-hover-email").getText());
    }

    private void assertUserHoverDetailsForAnonymousUser(String username) {
        gotoUserHover(username);
        assertFalse(locator.id("avatar-full-name-link").exists());
        assertEquals("Your session has timed out", locator.css(" div.aui-message").getText());
        assertEquals("", locator.id("user-hover-email").getText());
    }

    private void assertUserHoverDetails(String username, String fullName, String emailAddress) {
        gotoUserHover(username);
        final Element link = (Element) locator.id("avatar-full-name-link").getNode();
        assertEquals(username, link.getAttribute("title"));
        assertEquals(fullName, link.getTextContent().trim());
        assertEquals(emailAddress, locator.id("user-hover-email").getText());
    }
}
