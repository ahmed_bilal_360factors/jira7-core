package com.atlassian.jira.webtests.ztests.remote;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.backdoor.TestRunnerControl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.fail;

@WebTest({Category.FUNC_TEST, Category.INDEXING})
@LoginAs(user = ADMIN_USERNAME)
public class TestManagerFactory extends BaseJiraFuncTest {

    private static final String BACK_END_TEST_NAME = "com.atlassian.jira.dev.functest.TestManagerFactoryBackEnd";

    @Test
    public void testBackEnd() throws Exception {
        final TestRunnerControl.TestResult response = backdoor.testRunner().getRunTests(BACK_END_TEST_NAME);
        if (!response.passed) {
            fail(response.message);
        }
    }

}
