package com.atlassian.jira.webtests.ztests.subtask;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;

@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.SECURITY, Category.SUB_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueToSubTaskConversionSecurityLevel extends BaseJiraFuncTest {
    private static final String NO_SECURITY_ISSUE = "HSP-1";
    private static final String DEV_SECURITY_ISSUE = "HSP-2";
    private static final String ADMIN_SECURITY_ISSUE = "HSP-3";
    private static final String DEV_SECURITY_ISSUE_2 = "HSP-4";

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        administration.restoreData("TestIssueToSubTaskConversionSecurityLevel.xml");
    }

    /*
     * Tests that when the issue to convert has no security level and the parent issue does
     * the subtask takes the parent security level
     */
    @Test
    public void testIssueToSubTaskConversionSecurityLevelIntroduced() {
        // Check original security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(NO_SECURITY_ISSUE);
        tester.assertTextPresent(NO_SECURITY_ISSUE);

        // Convert to subtask (and change security level)
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.issue().gotoIssue(NO_SECURITY_ISSUE);
        tester.assertTextNotPresent("Security Level:");

        tester.clickLink("issue-to-subtask");
        tester.setFormElement("parentIssueKey", DEV_SECURITY_ISSUE);
        navigation.issue().selectIssueType("Sub-task", "issuetype");
        tester.submit("Next >>");
        tester.assertTextPresent("All fields will be updated automatically.");
        tester.submit("Next >>");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Security", "Developers");
        tester.submit("Finish");

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Security Level:", "Developers");

        // Check new security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(NO_SECURITY_ISSUE);
        assertions.getViewIssueAssertions().assertIssueNotFound();
    }

    /*
     * Tests that when the issue to convert has a security level and the parent issue doesn't
     * the subtask takes the parent security level ie. none
     */
    @Test
    public void testIssueToSubTaskConversionSecurityLevelRemoved() {
        // Check original security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(DEV_SECURITY_ISSUE);
        assertions.getViewIssueAssertions().assertIssueNotFound();

        // Convert to subtask (and change security level)
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.issue().gotoIssue(DEV_SECURITY_ISSUE);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Security Level:", "Developers");

        tester.clickLink("issue-to-subtask");
        tester.setFormElement("parentIssueKey", NO_SECURITY_ISSUE);
        navigation.issue().selectIssueType("Sub-task", "issuetype");
        tester.submit("Next >>");
        tester.assertTextPresent("All fields will be updated automatically.");
        tester.submit("Next >>");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Security", "Developers");
        tester.submit("Finish");

        tester.assertTextNotPresent("Security Level:");

        // Check new security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(DEV_SECURITY_ISSUE);
        tester.assertTextPresent(DEV_SECURITY_ISSUE);
    }

    /*
     * Tests that when the issue to convert has a security level and the parent issue has
     * a different security level the subtask takes the parent security level
     */
    @Test
    public void testIssueToSubTaskConversionSecurityLevelChanged() {
        // Check original security level
        navigation.logout();
        navigation.login("jane", "jane");
        navigation.issue().gotoIssue(DEV_SECURITY_ISSUE);
        tester.assertTextPresent(DEV_SECURITY_ISSUE);

        // Convert to subtask (and change security level)
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.issue().gotoIssue(DEV_SECURITY_ISSUE);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Security Level:", "Developers");

        tester.clickLink("issue-to-subtask");
        tester.setFormElement("parentIssueKey", ADMIN_SECURITY_ISSUE);
        navigation.issue().selectIssueType("Sub-task", "issuetype");
        tester.submit("Next >>");
        tester.assertTextPresent("All fields will be updated automatically.");
        tester.submit("Next >>");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Security", "Developers");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Developers", "Administrators");
        tester.submit("Finish");

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Security Level:", "Administrators");

        // Check new security level
        navigation.logout();
        navigation.login("jane", "jane");
        navigation.issue().gotoIssue(DEV_SECURITY_ISSUE);
        assertions.getViewIssueAssertions().assertIssueNotFound();
    }

    /*
     * Tests that when the issue to convert has a security level and the parent issue has
     * the same security level there is no change and no confirmation
     */
    @Test
    public void testIssueToSubTaskConversionSecurityLevelSame() {
        // Check original security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(DEV_SECURITY_ISSUE);
        assertions.getViewIssueAssertions().assertIssueNotFound();

        // Convert to subtask (and change security level)
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.issue().gotoIssue(DEV_SECURITY_ISSUE);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Security Level:", "Developers");

        tester.clickLink("issue-to-subtask");
        tester.setFormElement("parentIssueKey", DEV_SECURITY_ISSUE_2);
        navigation.issue().selectIssueType("Sub-task", "issuetype");
        tester.submit("Next >>");
        tester.assertTextPresent("All fields will be updated automatically.");
        tester.submit("Next >>");
        tester.assertTextNotPresent("Developers");
        tester.submit("Finish");

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Security Level:", "Developers");

        // Check new security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(DEV_SECURITY_ISSUE);
        assertions.getViewIssueAssertions().assertIssueNotFound();
    }
}
