package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.dashboard.reports.TestConfigureReport;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestConfigureReportCheckboxField;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestConfigureReportDateField;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestConfigureReportFilterField;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestConfigureReportFilterProjectField;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestConfigureReportHiddenField;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestConfigureReportLongField;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestConfigureReportMultiSelectField;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestConfigureReportSelectField;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestConfigureReportStringField;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestConfigureReportTextField;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestConfigureReportUserField;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestDeveloperWorkloadReport;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestDeveloperWorkloadReportPermissions;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestPieChartReport;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestSingleLevelGroupByReport;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestSingleLevelGroupByReportByLabels;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestSingleLevelGroupByReportIrrelevantIssues;
import com.atlassian.jira.webtests.ztests.dashboard.reports.security.xss.TestXssInConfigureReport;
import com.atlassian.jira.webtests.ztests.timetracking.legacy.TestTimeTrackingExcelReport;
import com.atlassian.jira.webtests.ztests.timetracking.legacy.TestTimeTrackingReport;
import com.atlassian.jira.webtests.ztests.timetracking.legacy.TestTimeTrackingReportPermissions;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * a suite of tests related to Reports
 *
 * @since v4.0
 */
public class FuncTestSuiteReports {

    public FuncTestSuiteReports() {
    }

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestDeveloperWorkloadReport.class)
                .add(TestDeveloperWorkloadReportPermissions.class)
                .add(TestTimeTrackingReport.class)
                .add(TestTimeTrackingReportPermissions.class)
                .add(TestTimeTrackingExcelReport.class)
                .add(TestSingleLevelGroupByReport.class)
                .add(TestSingleLevelGroupByReportIrrelevantIssues.class)
                .add(TestConfigureReport.class)
                .add(TestConfigureReportStringField.class)
                .add(TestConfigureReportLongField.class)
                .add(TestConfigureReportMultiSelectField.class)
                .add(TestConfigureReportSelectField.class)
                .add(TestConfigureReportTextField.class)
                .add(TestConfigureReportCheckboxField.class)
                .add(TestConfigureReportDateField.class)
                .add(TestConfigureReportHiddenField.class)
                .add(TestConfigureReportUserField.class)
                .add(TestConfigureReportFilterField.class)
                .add(TestConfigureReportFilterProjectField.class)
                .add(TestPieChartReport.class)
                .add(TestSingleLevelGroupByReportByLabels.class)
                .add(TestXssInConfigureReport.class)
                .build();
    }
}