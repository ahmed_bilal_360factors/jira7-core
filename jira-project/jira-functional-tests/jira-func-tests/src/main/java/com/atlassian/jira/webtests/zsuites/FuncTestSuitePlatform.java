package com.atlassian.jira.webtests.zsuites;

import com.google.common.collect.ImmutableList;

import java.util.List;

public class FuncTestSuitePlatform {

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
//                .add(TestPlatformCompatibility.class)
                .build();
    }
}
