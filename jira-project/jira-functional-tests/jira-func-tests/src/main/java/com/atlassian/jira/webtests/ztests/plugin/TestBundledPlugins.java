package com.atlassian.jira.webtests.ztests.plugin;

import com.atlassian.jira.functest.framework.FuncTestRuleChain;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.admin.plugins.Plugins;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.collect.ImmutableMap;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.emptyIterable;
import static org.junit.Assert.assertThat;

/**
 * @since v7.1.6
 */
@WebTest(Category.FUNC_TEST)
public class TestBundledPlugins {

    @Rule
    public RuleChain funcTestRuleChain = FuncTestRuleChain.forTest(this);

    public static final Map<String, String> PLUGINS = ImmutableMap.<String, String>builder()
            .put("com.atlassian.activeobjects.activeobjects-plugin", "Active Objects")
            .put("com.atlassian.gadgets.dashboard", "Gadgets Dashboard")
            .put("com.atlassian.gadgets.directory", "Gadgets Directory")
            .put("com.atlassian.gadgets.embedded", "Gadgets Embedded")
            .put("com.atlassian.gadgets.oauth.serviceprovider", "Gadgets Service Provider")
            .put("com.atlassian.gadgets.opensocial", "Gadgets Opensocial")
            .put("com.atlassian.gadgets.publisher", "Gadgets Publisher")
            .put("com.atlassian.administration.atlassian-admin-quicksearch-jira", "Admin Quicksearch")
            .put("com.atlassian.jira.extra.jira-ical-feed", "iCalendar")
            .put("com.atlassian.labs.atlassian-bot-killer", "Bot Killer")
            .build();

    @Inject
    private Plugins plugins;

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testIfPluginsAreInstalledAndEnabled() throws Exception {
        final List<String> notInstalledPlugins = PLUGINS.keySet().stream()
                .filter(key -> !plugins.isPluginInstalled(key))
                .map(PLUGINS::get)
                .collect(toList());
        assertThat("The following plugins are not installed:", notInstalledPlugins, emptyIterable());

        final List<String> notEnabledPlugins = PLUGINS.keySet().stream()
                .filter(key -> !plugins.isPluginEnabled(key))
                .map(PLUGINS::get)
                .collect(toList());
        assertThat("The following plugins are not enabled:", notEnabledPlugins, emptyIterable());

    }
}
