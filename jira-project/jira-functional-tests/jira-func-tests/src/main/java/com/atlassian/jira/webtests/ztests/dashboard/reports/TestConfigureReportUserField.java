package com.atlassian.jira.webtests.ztests.dashboard.reports;

import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;
import org.w3c.dom.Element;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.REPORTS})
@LoginAs(user = ADMIN_USERNAME)
public class TestConfigureReportUserField extends AbstractConfigureReportFieldTestCase {

    private Element fieldContainer() {
        return (Element)new XPathLocator(tester, "//fieldset[@class = 'group'][descendant::input[@id = 'aUser']]").getNode();
    }

    @Test
    public void fieldExists() {
        tester.assertFormElementPresent("aUser");
    }

    @Test
    public void fieldLabelRendered() {
        textAssertions.assertTextPresent(new XPathLocator(fieldContainer(), "legend"), "A user");
    }

    @Test
    public void fieldDescriptionRendered() {
        textAssertions.assertTextPresent(new XPathLocator(fieldContainer(), "descendant::label[@for='aUser']"), "This is a user field");
    }

    @Test
    public void validationErrorsRendered() {
        tester.gotoPage("/secure/ConfigureReport.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.dev.func-test-plugin:fieldtest-report&aString=fail");
        textAssertions.assertTextPresent(new XPathLocator(fieldContainer(), "*[@class='errMsg']"), "This user field has an error");
    }

    @Test
    public void valueIsSubmittedToReportBean() {
        tester.setFormElement("aUser", "admin");
        tester.submit();
        textAssertions.assertTextPresent(new XPathLocator(tester, "//tr[th = 'aUser']/td"), "admin");
    }
}
