package com.atlassian.jira.functest.framework.admin;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.NavigationImpl;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;

/**
 * Default implementation of {@link com.atlassian.jira.functest.framework.admin.NotificationSchemes}.
 *
 * @since v4.4
 */
public class NotificationSchemesImpl implements NotificationSchemes {
    private static final String ADD_NOTIFICATION_SCHEME_LINK_ID = "add-notification-scheme";

    private final WebTester tester;
    private final JIRAEnvironmentData environmentData;
    private final EditNotifications editNotifications;
    private Navigation navigation;

    @Inject
    public NotificationSchemesImpl(WebTester tester, JIRAEnvironmentData environmentData) {
        this.tester = tester;
        this.environmentData = environmentData;
        this.editNotifications = new EditNotificationsImpl(tester, environmentData);
    }

    public NotificationSchemesImpl(WebTester tester, JIRAEnvironmentData environmentData, int logIndentLevel) {
        this(tester, environmentData);
    }

    @Override
    public NotificationSchemes goTo() {
        getNavigation().gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);
        return this;
    }

    private Navigation getNavigation() {
        if (navigation == null) {
            navigation = new NavigationImpl(tester, environmentData);
        }
        return navigation;
    }

    @Override
    public EditNotifications addNotificationScheme(String name, String description) {
        tester.clickLink(ADD_NOTIFICATION_SCHEME_LINK_ID);
        tester.setWorkingForm(FunctTestConstants.JIRA_FORM_NAME);
        tester.setFormElement("name", name);
        tester.setFormElement("description", description);
        tester.submit("Add");
        return editNotifications;
    }

    @Override
    public EditNotifications editNotifications(int id) {
        tester.clickLink(id + "_edit");
        return editNotifications;
    }
}
