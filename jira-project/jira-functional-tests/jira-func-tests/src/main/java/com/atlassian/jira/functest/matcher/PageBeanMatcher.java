package com.atlassian.jira.functest.matcher;

import com.atlassian.jira.testkit.client.restclient.PageBean;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import static org.hamcrest.Matchers.hasSize;

/**
 * Matcher for {@link com.atlassian.jira.rest.api.pagination.PageBean}.
 *
 * @since v6.5
 */
public class PageBeanMatcher {
    private PageBeanMatcher() {
    }

    public static <T> Matcher<PageBean<T>> matcher(final Matcher<Long> startMatcher,
                                                   final Matcher<Long> totalMatcher,
                                                   final Matcher<Integer> expectedSizeMatcher,
                                                   final Matcher<Iterable<? extends T>> valuesMatcher) {
        return Matchers.allOf(Matchers.hasProperty("startAt", startMatcher),
                Matchers.hasProperty("total", totalMatcher),
                Matchers.hasProperty("values", hasSize(expectedSizeMatcher)),
                Matchers.hasProperty("values", valuesMatcher));
    }
}
