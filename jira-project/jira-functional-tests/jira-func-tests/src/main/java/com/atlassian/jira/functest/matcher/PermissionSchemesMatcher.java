package com.atlassian.jira.functest.matcher;

import com.atlassian.jira.testkit.beans.PermissionSchemeBean;
import com.atlassian.jira.util.ObjectUtils;
import com.google.common.base.Strings;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class PermissionSchemesMatcher {

    private static class HasPermission extends TypeSafeMatcher<PermissionSchemeBean> {

        final String permissionKey;
        final String type;
        final String parameter;

        public HasPermission(String permissionKey, String type, String parameter) {
            this.permissionKey = permissionKey;
            this.type = type;
            this.parameter = parameter;
        }

        @Override
        protected boolean matchesSafely(PermissionSchemeBean permissionSchemeBean) {
            if (permissionSchemeBean.getPermissions() == null) {
                return false;
            }

            return permissionSchemeBean.getPermissions().stream().anyMatch(permissionGrantBean ->
                    permissionGrantBean.getPermission().equals(permissionKey) &&
                    //In Oracle, empty string is returned as null so these are treated as equivalent
                    ObjectUtils.equalsNullSafe(Strings.nullToEmpty(permissionGrantBean.getHolder().getType()), Strings.nullToEmpty(type)) &&
                    ObjectUtils.equalsNullSafe(Strings.nullToEmpty(permissionGrantBean.getHolder().getParameter()), Strings.nullToEmpty(parameter)));
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("permission ").appendValue(permissionKey)
                    .appendText(" with type ").appendValue(type)
                    .appendText(" and value: ").appendValue(parameter);
        }
    }

    private static class HasPermissionCount extends TypeSafeMatcher<PermissionSchemeBean> {
        final String type;
        final String parameter;
        final Integer expectedMatchingPermissions;

        public HasPermissionCount(String type, String parameter, Integer expectedMatchingPermissions) {
            this.type = type;
            this.parameter = parameter;
            this.expectedMatchingPermissions = expectedMatchingPermissions;
        }

        @Override
        protected boolean matchesSafely(PermissionSchemeBean permissionSchemeBean) {
            if (permissionSchemeBean.getPermissions() == null) {
                return 0 == expectedMatchingPermissions;
            }

            long matchCount = permissionSchemeBean.getPermissions().stream().filter(permissionGrantBean ->
                    ObjectUtils.equalsNullSafe(permissionGrantBean.getHolder().getType(), type) &&
                            ObjectUtils.equalsNullSafe(permissionGrantBean.getHolder().getParameter(), parameter)).count();
            return matchCount == expectedMatchingPermissions;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("expected ").appendValue(expectedMatchingPermissions)
                    .appendText(" permission matches of type ").appendValue(type)
                    .appendText(" and value: ").appendValue(parameter);
        }
    }
    /**
     * Matches if there is a given permission in the permission scheme with the provided values,
     * for example, ADMINISTER_PROJECTS, USER, "fred"
     * @param permissionKey to permission assignment
     * @param type of permission assignment, e.g. "USER"
     * @param parameter of the value, e.g. "fred"
     * @return matcher to check this
     */
    public static Matcher<PermissionSchemeBean> hasPermission(String permissionKey, String type, String parameter) {
        return new HasPermission(permissionKey, type, parameter);
    }


    /**
     * Counts the number of permissions a given type has and matches if it is the correct amount.
     * E.g. USER, "fred", 0 is true if there is no permissions with fred as a user assignment.
     * @param type of permission assignment, e.g. "USER"
     * @param parameter of the value, e.g. "fred"
     * @param expectedMatchingPermissions number of expected matches in permission scheme
     * @return matcher to check this
     */
    public static Matcher<PermissionSchemeBean> hasPermissionCount(String type, String parameter, Integer expectedMatchingPermissions) {
        return new HasPermissionCount(type, parameter, expectedMatchingPermissions);
    }
}
