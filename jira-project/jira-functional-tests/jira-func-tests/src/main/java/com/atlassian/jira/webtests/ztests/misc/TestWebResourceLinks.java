package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.framework.util.dom.SneakyDomExtractor;
import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static com.atlassian.collectors.CollectorsUtil.toImmutableSet;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@WebTest({Category.FUNC_TEST, Category.I18N})
public class TestWebResourceLinks extends BaseJiraFuncTest {

    @Test
    public void shouldBeStableAcrossLanguagesForCss() {
        backdoor.userProfile().changeUserLanguage(ADMIN_USERNAME, "en_UK");
        navigation.gotoDashboard();
        final Set<String> cssLinksEnglish = getCssLinks();

        backdoor.userProfile().changeUserLanguage(ADMIN_USERNAME, "de_DE");
        navigation.gotoDashboard();
        final Set<String> cssLinksGerman = getCssLinks();
        assertThat("Loaded different set of css", cssLinksEnglish, equalTo(cssLinksGerman));
    }

    @Test
    public void shouldBeStableAcrossLanguagesForJavaScript() {
        backdoor.userProfile().changeUserLanguage(ADMIN_USERNAME, "en_UK");
        navigation.gotoDashboard();
        final Set<String> jsLinksEnglish = getJavaScriptLinks();

        backdoor.userProfile().changeUserLanguage(ADMIN_USERNAME, "de_DE");
        navigation.gotoDashboard();
        final Set<String> jsLinksGerman = getJavaScriptLinks();
        assertThat("Loaded different set of javaScript", jsLinksEnglish, equalTo(jsLinksGerman));
    }

    private Set<String> getCssLinks() {
        return getUrls("link", "href",
                attr -> "stylesheet".equals(attr.getNamedItem("rel").getTextContent()));
    }

    private Set<String> getJavaScriptLinks() {
        return getUrls("script", "src", (ignore) -> true);
    }

    private Set<String> getUrls(final String tagName, final String urlAttribute, Predicate<NamedNodeMap> filter) {
        final Document dom = SneakyDomExtractor.getDOM(tester);
        final Stream<Node> links = getElementsByTagNameStream(dom, tagName);
        return links
                .map(Node::getAttributes)
                .filter(filter)
                .filter(attr -> attr.getNamedItem(urlAttribute) != null)
                .map(attr -> attr.getNamedItem(urlAttribute).getTextContent())
                .collect(toImmutableSet());
    }

    private Stream<Node> getElementsByTagNameStream(final Document dom, final String tagName) {
        final NodeList elementsByTagName = dom.getElementsByTagName(tagName);
        final int linksLength = elementsByTagName.getLength();
        final ImmutableList.Builder<Node> builder = ImmutableList.builder();
        for (int i = 0; i < linksLength; i++) {
            builder.add(elementsByTagName.item(i));
        }
        return builder.build().stream();
    }
}
