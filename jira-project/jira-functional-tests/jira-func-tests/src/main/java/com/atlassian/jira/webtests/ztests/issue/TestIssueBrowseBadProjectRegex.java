package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.httpclient.api.HttpStatus;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.assertions.ViewIssueAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.ISSUES})
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueBrowseBadProjectRegex extends BaseJiraFuncTest {
    /**
     * Test browsing an Issue that does exist with a bad project REGEX
     */
    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Inject
    private ViewIssueAssertions viewIssueAssertions;

    @Test
    public void testViewIssuePageWithInvalidAssigneeAndReporters() {
        //import pro/ent data (with subtasks)
        administration.restoreData("TestIssueBrowseBadProjectRegex.xml");

        //Test browsing an Issue that does exist with a bad project REGEX
        tester.gotoPage("/browse/M02-1");
        textAssertions.assertTextPresent(locator.page(), "Monkey bug 1");

        //Test browsing an Issue that masks a project key.
        //This should return issue.
        tester.gotoPage("/browse/H01-1");
        textAssertions.assertTextPresent(locator.page(), "adgvadgvad");

        //Test browsing an Issue that does exist with a bad project REGEX
        tester.gotoPage("/browse/H01-1-1");
        textAssertions.assertTextPresent(locator.page(), "Awful regex bug");

        // Test browsing a link that is not found that matches a valid issue key
        tester.gotoPage("/browse/HSP-1");
        viewIssueAssertions.assertIssueNotFound();

        // Test browsing a link that is not found that DOES NOT match a valid issue key (a bad project REGEX)
        tester.gotoPage("/browse/!@#$%^&*-1");
        textAssertions.assertTextNotPresent(locator.page(), "Issue does not exist");
        assertions.assertHttpStatusCode(HttpStatus.NOT_FOUND);
    }
}
