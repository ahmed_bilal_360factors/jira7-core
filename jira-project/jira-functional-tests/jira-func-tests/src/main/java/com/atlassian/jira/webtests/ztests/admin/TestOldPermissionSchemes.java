package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.EditIssueFieldVisibility;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.admin.GeneralConfiguration;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.sun.jersey.api.client.UniformInterfaceException;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_ASSIGNEE_ERROR_MESSAGE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PERM_SCHEME_DESC;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PERM_SCHEME_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PRIORITY_MINOR;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_NEO;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_NEO_KEY;
import static com.atlassian.jira.functest.framework.Navigation.AdminSection.PERMISSION_SCHEMES;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.MOVE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.SCHEDULE_ISSUES;
import static com.atlassian.jira.webtests.Groups.DEVELOPERS;
import static com.atlassian.jira.webtests.Groups.USERS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.PERMISSIONS, Category.SCHEMES})
@LoginAs(user = ADMIN_USERNAME)
public class TestOldPermissionSchemes extends BaseJiraFuncTest {

    private static final String PERMISSION_SCHEMES_SINGLE_PAGE_DISABLED = "com.atlassian.jira.permission-schemes.single-page-ui.disabled";

    @Inject
    private EditIssueFieldVisibility editIssueFieldVisibility;

    @Inject
    private GeneralConfiguration generalConfiguration;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Test
    @RestoreBlankInstance
    public void testPermissionSchemes() {
        administration.restoreBlankInstance();
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        administration.project().addProject(PROJECT_NEO, PROJECT_NEO_KEY, ADMIN_USERNAME);

        String issueKeyNormal = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "test 1", ADMIN_USERNAME, PRIORITY_MINOR, ISSUE_TYPE_BUG).key();
        backdoor.issues().setIssueFields(issueKeyNormal, new IssueFields()
                .description("test description for permission schemes")
                .environment("test environment 1"));

        permissionSchemesCreateScheme();
        permissionSchemeAssociateScheme();
        permissionSchemeAddDuplicateScheme();
        permissionSchemeAddInvalidScheme();
        permissionSchemesMoveIssueToProjectWithAssignablePermission(issueKeyNormal);
        permissionSchemesMoveIssueWithSchedulePermission(issueKeyNormal);
        permissionSchemesMoveIssueToProjectWithCreatePermission(issueKeyNormal);

        permissionSchemeDeleteScheme();

        navigation.issue().deleteIssue(issueKeyNormal);
    }

    @Test
    @Restore("TestSchemesProjectRoles.xml")
    public void testProjectRolePermissionScheme() {
        logger.log("Test to check that project role permission scheme works");
        backdoor.darkFeatures().enableForSite(PERMISSION_SCHEMES_SINGLE_PAGE_DISABLED);
        navigation.gotoAdminSection(PERMISSION_SCHEMES);
        tester.clickLinkWithText(DEFAULT_PERM_SCHEME);
        tester.assertTextPresent("Edit Permissions &mdash; " + DEFAULT_PERM_SCHEME);

        tester.clickLink("add_perm_" + MOVE_ISSUES.permissionKey());

        tester.assertTextPresent("Choose a project role");

        tester.checkCheckbox("type", "projectrole");
        tester.selectOption("projectrole", "test role");
        tester.submit();
        tester.assertTextPresent("(test role)");
    }

    public void permissionSchemesCreateScheme() {
        logger.log("Permission Schemes: Create a new permission scheme");
        createPermissionScheme(PERM_SCHEME_NAME, PERM_SCHEME_DESC);
        tester.assertLinkPresentWithText(PERM_SCHEME_NAME);
        tester.assertTextPresent(PERM_SCHEME_DESC);
    }

    public void permissionSchemeDeleteScheme() {
        logger.log("Permission Schemes:Delete a permission scheme");
        deletePermissionScheme(PERM_SCHEME_NAME);
        tester.assertLinkNotPresentWithText(PERM_SCHEME_NAME);
    }

    public void permissionSchemeAssociateScheme() {
        logger.log("Permission Schemes: Associate a permission scheme with a project");
        associatePermSchemeToProject(PROJECT_NEO, PERM_SCHEME_NAME);

        assertThat(backdoor.project().getSchemes(PROJECT_NEO_KEY).permissionScheme.name, equalTo(PERM_SCHEME_NAME));

        associatePermSchemeToProject(PROJECT_NEO, DEFAULT_PERM_SCHEME);
    }

    /**
     * Create a scheme with a duplicate name
     */
    public void permissionSchemeAddDuplicateScheme() {
        logger.log("Permission Schemes: Attempt to create a scheme with a duplicate name");
        createPermissionScheme(PERM_SCHEME_NAME, "");
        tester.assertTextPresent("Add permission scheme");
        tester.assertTextPresent("A Scheme with this name already exists.");
    }

    /**
     * Create a scheme with an invalid name
     */
    public void permissionSchemeAddInvalidScheme() {
        logger.log("Permission Schemes: Attempt to create a scheme with an invalid name");
        createPermissionScheme("", "");
        tester.assertTextPresent("Add permission scheme");
        tester.assertTextPresent("Please specify a name for this Scheme.");
    }

    /**
     * Tests the ability to move an issue to a project WITHOUT the 'Assignable User' Permission
     */
    private void permissionSchemesMoveIssueToProjectWithAssignablePermission(String issueKey) {
        logger.log("Move Operation: Moving issue to a project with 'Assign Issue' Permission.");
        associatePermSchemeToProject(PROJECT_NEO, PERM_SCHEME_NAME);
        generalConfiguration.setAllowUnassignedIssues(true);
        // Give jira-users 'Create' Permission
        backdoor.permissionSchemes().addGroupPermission(PERM_SCHEME_ID, CREATE_ISSUES, USERS);
        // give jira-developers 'Assignable Users' Permission
        backdoor.permissionSchemes().addGroupPermission(PERM_SCHEME_ID, ProjectPermissions.ASSIGNABLE_USER, DEVELOPERS);

        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("move-issue");
        navigation.issue().selectProject(PROJECT_NEO);
        tester.submit();
        tester.assertTextPresent("Step 3 of 4");

        tester.assertTextNotPresent(DEFAULT_ASSIGNEE_ERROR_MESSAGE);

        generalConfiguration.setAllowUnassignedIssues(false);
        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("move-issue");
        navigation.issue().selectProject(PROJECT_NEO);
        tester.submit();
        tester.assertTextPresent("Step 3 of 4");
        tester.assertTextNotPresent(DEFAULT_ASSIGNEE_ERROR_MESSAGE);

        backdoor.permissionSchemes().removeGroupPermission(PERM_SCHEME_ID, CREATE_ISSUES, USERS);
        associatePermSchemeToProject(PROJECT_NEO, DEFAULT_PERM_SCHEME);
    }

    /**
     * Test the abilty to move an issue with 'Schedule Issues' Permission and 'Due Date' Required
     */
    public void permissionSchemesMoveIssueWithSchedulePermission(String issueKey) {
        logger.log("Move Operation: Moving issue to a project with 'Schedule Issue' Permission.");
        associatePermSchemeToProject(PROJECT_NEO, PERM_SCHEME_NAME);
        try {
            //Try to remove this permission.
            backdoor.permissionSchemes().removeGroupPermission(PERM_SCHEME_ID, SCHEDULE_ISSUES, DEVELOPERS);
        } catch (UniformInterfaceException e) {
            //It's fine if it fails, because the permission entity may not have existed in the first place.
        }
        backdoor.permissionSchemes().addGroupPermission(PERM_SCHEME_ID, CREATE_ISSUES, USERS);
        editIssueFieldVisibility.setDueDateToRequried();

        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("move-issue");
        navigation.issue().selectProject(PROJECT_NEO);
        tester.submit();

        tester.assertTextPresent("Step 3 of 4");

        tester.setWorkingForm("jiraform");
        tester.submit();
        tester.assertTextPresent("&quot;Due Date&quot; field is required and you do not have permission to Schedule Issues for project &quot;" + PROJECT_NEO + "&quot;.");

        // restore settings
        editIssueFieldVisibility.resetFields();
        backdoor.permissionSchemes().addGroupPermission(PERM_SCHEME_ID, SCHEDULE_ISSUES, DEVELOPERS);
        backdoor.permissionSchemes().removeGroupPermission(PERM_SCHEME_ID, CREATE_ISSUES, USERS);
        associatePermSchemeToProject(PROJECT_NEO, DEFAULT_PERM_SCHEME);
    }

    /**
     * Tests the ability to move an issue to a project WITHOUT the 'Create Issue' Permission
     */
    public void permissionSchemesMoveIssueToProjectWithCreatePermission(String issueKey) {
        logger.log("Move Operation: Moving issue to a project with 'Create Issue' Permission.");
        associatePermSchemeToProject(PROJECT_NEO, PERM_SCHEME_NAME);

        backdoor.permissionSchemes().addGroupPermission(PERM_SCHEME_ID, CREATE_ISSUES, USERS);
        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("move-issue");
        assertions.getProjectFieldsAssertions().assertProjectsEquals(new String[]{PROJECT_HOMOSAP, PROJECT_NEO, PROJECT_HOMOSAP, PROJECT_MONKEY, PROJECT_NEO});
        backdoor.permissionSchemes().removeGroupPermission(PERM_SCHEME_ID, CREATE_ISSUES, USERS);
        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("move-issue");
        assertions.getProjectFieldsAssertions().assertProjectsEquals(new String[]{PROJECT_HOMOSAP, PROJECT_HOMOSAP, PROJECT_MONKEY});

        associatePermSchemeToProject(PROJECT_NEO, DEFAULT_PERM_SCHEME);
    }

    public void createPermissionScheme(String permission_name, String permission_desc) {
        navigation.gotoAdminSection(PERMISSION_SCHEMES);
        tester.clickLinkWithText("Add Permission Scheme");
        tester.setFormElement("name", permission_name);
        tester.setFormElement("description", permission_desc);
        tester.submit("Add");
    }

    public void deletePermissionScheme(String permission_name) {
        navigation.gotoAdminSection(PERMISSION_SCHEMES);
        tester.clickLink("del_" + permission_name);
        tester.submit("Delete");
    }

    public void associatePermSchemeToProject(String project, String permission_name) {
        String projectId = backdoor.project().getProjects().stream()
                .filter(p -> project.equals(p.name))
                .findFirst()
                .orElseThrow(AssertionError::new)
                .id;

        navigation.gotoPage("/secure/project/SelectProjectPermissionScheme!default.jspa?projectId=" + projectId);
        tester.selectOption("schemeIds", permission_name);
        tester.submit("Associate");
    }
}

