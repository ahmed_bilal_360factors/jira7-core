package com.atlassian.jira.webtests.ztests.timezone;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.TIME_ZONES;

/**
 * @since v4.4
 */
@WebTest({FUNC_TEST, TIME_ZONES})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestTimeZoneManager extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreData("TestTimeZoneManager.xml");
        administration.generalConfiguration().setDefaultUserTimeZone("Australia/Sydney");
    }

    @Test
    public void testTimeZoneManager() throws Exception {
        navigation.login("fred");
        tester.gotoPage("/plugins/servlet/functest-timezone-servlet");
        tester.assertTextPresent("DefaultTimeZone=Australia/Sydney");
        tester.assertTextPresent("UserTimeZone=Europe/Berlin");
    }
}
