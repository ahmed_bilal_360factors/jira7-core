package com.atlassian.jira.functest.framework;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This rule logs in user at the beginning of test and then tries to log user out.  This assumes the standard JIRA test
 * behaviour of having the user name the same value as the password.
 *
 * @since v6.5
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface LoginAs {
    String user();
}