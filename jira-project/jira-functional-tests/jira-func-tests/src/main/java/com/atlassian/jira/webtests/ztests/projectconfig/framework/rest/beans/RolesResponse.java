package com.atlassian.jira.webtests.ztests.projectconfig.framework.rest.beans;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

@JsonAutoDetect
public class RolesResponse {

    @JsonProperty
    private List<RoleMembersResponse> roles;

    @JsonProperty
    private boolean showApplicationRole;

    private RolesResponse() {
    }

    public RolesResponse(List<RoleMembersResponse> roles, boolean showApplicationRole) {
        this.roles = roles;
        this.showApplicationRole = showApplicationRole;
    }

    public List<RoleMembersResponse> getRoles() {
        return roles;
    }

    public boolean isShowApplicationRole() {
        return showApplicationRole;
    }

}
