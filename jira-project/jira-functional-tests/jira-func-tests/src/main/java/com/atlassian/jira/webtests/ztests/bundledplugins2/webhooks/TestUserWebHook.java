package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.UserBean;
import com.atlassian.jira.testkit.client.restclient.UserClient;
import com.google.common.base.Supplier;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.testkit.client.util.JsonMatchers.hasField;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@WebTest({Category.FUNC_TEST, Category.WEBHOOKS, Category.REFERENCE_PLUGIN})
public class TestUserWebHook extends AbstractWebHookTest {

    public static final String USER_NAME = "user";
    public static final String USER_PASSWORD = "pass";
    public static final String DISPLAY_NAME = "User";
    public static final String EMAIL = "user@atlassian.com";

    public static final UserBean INITIAL_USER = UserBean.builder()
            .setName(USER_NAME)
            .setKey(USER_NAME)
            .setDisplayName(DISPLAY_NAME)
            .setEmailAddress(EMAIL)
            .setPassword(USER_PASSWORD)
            .build();

    private UserClient userClient;

    @Before
    @Override
    public void setUpTest() {
        super.setUpTest();
        backdoor.restoreBlankInstance();
        userClient = new UserClient(getEnvironmentData());
        deleteUser(); // delete default user created in tests
    }

    @Test
    public void testWebhookIsSentForCreatedUserWhenListenerIsPersisted() throws Exception {
        registerWebHook("user_created");
        createUser();
        assertUserCreatedWebHookReceived(receivedByPersistentListener);
    }

    @Test
    public void testWebhookIsSentForCreatedUserWhenListenerComesFromModuleDescriptor() throws Exception {
        createUser();
        assertUserCreatedWebHookReceived(receivedByModuleDescriptorListener("user_created"));
    }

    @Test
    public void testWebhookIsSentForDeletedUserWhenListenerIsPersisted() throws Exception {
        registerWebHook("user_deleted");
        createUser();
        assertWebHookNotReceived(receivedByPersistentListener);
        deleteUser();
        assertUserDeletedWebHookReceived(receivedByPersistentListener);
    }

    @Test
    public void testUriVariablesAreCorrectlyProvidedForUserCreatedWebHook() throws Exception {
        createUser();
        JSONObject parameters = getUrlParametersOfReceivedWebHook("user_created");
        assertThat(parameters, hasField("created_user_name").equalTo("user"));
        assertThat(parameters, hasField("created_user_key").equalTo("user"));
    }

    @Test
    public void testUriVariablesAreCorrectlyProvidedForUserDeletedWebHook() throws Exception {
        createUser();
        deleteUser();
        JSONObject parameters = getUrlParametersOfReceivedWebHook("user_deleted");
        assertThat(parameters, hasField("deleted_user_name").equalTo("user"));
        assertThat(parameters, hasField("deleted_user_key").equalTo("user"));
    }

    @Test
    public void testUserUpdatedWebhookIsSentWhenEmailIsChanged() throws Exception {
        createDefaultUserAndRegisterPersistentListenerForUserUpdatedWebhook();
        userClient.updateEmail(USER_NAME, "new@email.com");
        assertWebhookUpdatedReceived(INITIAL_USER.but().setEmailAddress("new@email.com"));
    }

    @Test
    public void testUserUpdatedWebhookIsSentWhenDisplayNameIsChanged() throws Exception {
        createDefaultUserAndRegisterPersistentListenerForUserUpdatedWebhook();
        userClient.updateDisplayName(USER_NAME, "newDisplayName");
        assertWebhookUpdatedReceived(INITIAL_USER.but().setDisplayName("newDisplayName"));
    }

    @Test
    public void testUserUpdatedWebhookIsSentWhenNameIsChanged() throws Exception {
        createDefaultUserAndRegisterPersistentListenerForUserUpdatedWebhook();
        userClient.updateName(USER_NAME, "newName");
        assertWebhookUpdatedReceived(INITIAL_USER.but().setName("newName"));
    }

    private void createDefaultUserAndRegisterPersistentListenerForUserUpdatedWebhook() {
        createUser();
        registerWebHook("user_updated");
    }

    @Test
    public void testCorrectParametersAreSentForUserUpdatedWebhook() throws Exception {
        createUser();
        userClient.updateName(USER_NAME, "newName");

        JSONObject updateParameters = getUrlParametersOfReceivedWebHook("user_updated");
        assertThat(updateParameters, hasField("updated_user_key").equalTo(USER_NAME));
        assertThat(updateParameters, hasField("updated_user_name").equalTo("newName"));
        deleteUser();
    }

    private void createUser() {
        backdoor.usersAndGroups().addUser(USER_NAME, USER_PASSWORD, DISPLAY_NAME, EMAIL);
    }

    private void deleteUser() {
        backdoor.usersAndGroups().deleteUser(USER_NAME);
    }

    private void assertUserCreatedWebHookReceived(Supplier<String> receivedWebhook) throws JSONException {
        assertUserWebhookReceived(receivedWebhook, "user_created", INITIAL_USER);
    }

    private void assertWebhookUpdatedReceived(final UserBean.Builder expectedUser) throws JSONException {
        assertUserWebhookReceived(receivedByModuleDescriptorListener("user_updated"), "user_updated", expectedUser.build());
        assertUserWebhookReceived(receivedByPersistentListener, "user_updated", expectedUser.build());
    }

    private void assertUserWebhookReceived(Supplier<String> receivedWebhook, String webhookName, UserBean expectedPayload)
            throws JSONException {
        JSONObject json = new JSONObject(receivedWebhook.get());

        assertThat(json, hasField("timestamp"));
        assertThat(json, hasField("webhookEvent").equalTo(webhookName));
        assertThat(json, hasField("user.name").equalTo(expectedPayload.name));
        assertThat(json, hasField("user.key").equalTo(expectedPayload.key));
        assertThat(json, hasField("user.displayName").equalTo(expectedPayload.displayName));
        assertThat(json, hasField("user.emailAddress").equalTo(expectedPayload.emailAddress));
    }

    private void assertUserDeletedWebHookReceived(Supplier<String> receivedWebhook) throws JSONException {
        JSONObject json = new JSONObject(receivedWebhook.get());

        assertThat(json, hasField("timestamp"));
        assertThat(json, hasField("webhookEvent").equalTo("user_deleted"));
        assertThat(json, hasField("user.name").equalTo(USER_NAME));
        assertThat(json, hasField("user.key").equalTo(USER_NAME));
    }

    private void assertWebHookNotReceived(Supplier<String> receivedWebhook) throws JSONException {
        final String webhook;
        try {
            webhook = receivedWebhook.get();
        } catch (NullPointerException npe) {
            return;
        }
        assertTrue("Didn't expect to receive this webhook: " + webhook, false);
    }

}
