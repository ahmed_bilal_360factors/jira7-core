package com.atlassian.jira.webtests.ztests.subtask;

import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;

/**
 * @since v7.1
 */
public class SubTaskAssertions {

    private final TextAssertions textAssertions;
    private final WebTester tester;

    @Inject
    public SubTaskAssertions(final TextAssertions textAssertions, final WebTester tester) {
        this.textAssertions = textAssertions;
        this.tester = tester;
    }

    /**
     * Tests that the panel shows the correct steps
     */
    protected void assertSubTaskConversionPanelSteps(final String key, final int currentStep) {
        final String responseText = tester.getDialog().getResponseText();
        textAssertions.assertTextSequence(responseText, new String[]{
                "Convert Issue to Sub-task: " + key,
                "Select Parent and Sub-task Type",
                "Select New Status",
                "Update Fields",
                "Confirmation"
        });

        final String[] icons = new String[4];
        for (int i = 1; i <= 4; i++) {
            if (i < currentStep) {
                icons[i - 1] = "<li class=\"done\">";
            } else if (i > currentStep) {
                icons[i - 1] = "<li class=\"todo\">";
            } else {
                icons[i - 1] = "<li class=\"current\">";
            }
        }

        textAssertions.assertTextSequence(responseText, icons);
    }
}
