package com.atlassian.jira.webtests.ztests.subtask;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_IMPROVEMENT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_NEWFEATURE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_SUB_TASK;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_TASK;

@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.SUB_TASKS})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestSubTaskToIssueConversion.xml")
public class TestSubTaskToIssueConversionStep1 extends BaseJiraFuncTest {
    private static final String BUG_KEY = "HSP-1";
    private static final String BUG_ID = "10000";
    private static final String SUBTASK_KEY = "HSP-3";
    private static final String SUBTASK_ID = "10002";
    private static final String SUBTASK_KEY_NO_FEATURES_IN_SCHEME = "MKY-2";
    private static final String SUBTASK_ID_NO_FEATURES_IN_SCHEME = "10021";
    private static final String FEATURE_KEY = "HSP-4";
    private static final String FEATURE_ID = "10010";
    private static final String TASK_KEY = "HSP-5";
    private static final String IMPROVEMENT_KEY = "HSP-6";
    private static final String INVALID_ISSUE_ID = "9999";


    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
    }

    /*
     * Tests that convert subtask to issue is only available to users with Permissions.EDIT_ISSUE
     */
    @Test
    public void testSubTaskToIssueConversionEditPermission() {
        navigation.issue().gotoIssue(SUBTASK_KEY);
        tester.assertLinkPresent("subtask-to-issue");
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(SUBTASK_KEY);
        tester.assertLinkNotPresent("subtask-to-issue");

        gotoConvertSubTask(SUBTASK_ID);
        tester.assertTextPresent("Access Denied");
        tester.assertTextNotPresent("Step 1 of 4");

        navigation.logout();
        gotoConvertSubTask(SUBTASK_ID);
        tester.assertTextPresent("You must log in to access this page.");
    }

    /*
     * Tests that only subtasks can be converted to issues
     */
    @Test
    public void testSubTaskToIssueConversionCheckIssueType() {
        navigation.issue().gotoIssue(SUBTASK_KEY);
        tester.assertLinkPresent("subtask-to-issue");
        navigation.issue().gotoIssue(BUG_KEY);
        tester.assertLinkNotPresent("subtask-to-issue");
        navigation.issue().gotoIssue(FEATURE_KEY);
        tester.assertLinkNotPresent("subtask-to-issue");
        navigation.issue().gotoIssue(TASK_KEY);
        tester.assertLinkNotPresent("subtask-to-issue");
        navigation.issue().gotoIssue(IMPROVEMENT_KEY);
        tester.assertLinkNotPresent("subtask-to-issue");

        gotoConvertSubTask(FEATURE_ID);
        tester.assertTextPresent("Issue HSP-4 is not a sub-task");
        tester.assertTextNotPresent("Step 1 of 4");

        // Check Wizard pane shows correct link
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Return to", FEATURE_KEY);
        tester.assertLinkPresentWithText(FEATURE_KEY);

        gotoConvertSubTask(SUBTASK_ID);
        tester.assertTextPresent("Step 1 of 4");


    }

    /*
     * Tests that an invalid issue id returns error
     */
    @Test
    public void testIssueToSubTaskConversionInvalidIssue() {
        gotoConvertSubTask(SUBTASK_ID);
        tester.assertTextPresent("Step 1 of 4");

        gotoConvertSubTask(INVALID_ISSUE_ID);
        tester.assertTextPresent("Errors");
        tester.assertTextPresent("Issue not found");
        tester.assertTextNotPresent("Step 1 of 4");

        // Check Wizard pane shows correct link
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Return to", "Dashboard");
        tester.assertLinkPresentWithText("Dashboard");
    }

    /*
    * Tests that the new issue type list only contains valid issue types
    */
    @Test
    public void testIssueToSubTaskConversionIssueType() {

        gotoConvertSubTask(SUBTASK_ID);
        tester.assertOptionsEqual("issuetype", new String[]{ISSUE_TYPE_BUG, ISSUE_TYPE_NEWFEATURE, ISSUE_TYPE_TASK, ISSUE_TYPE_IMPROVEMENT});
        tester.assertRadioOptionValueNotPresent("issuetype", ISSUE_TYPE_SUB_TASK);

        // Project where SUBTASK_TYPE_2 doesn't exist
        gotoConvertSubTask(SUBTASK_ID_NO_FEATURES_IN_SCHEME);
        tester.assertOptionsEqual("issuetype", new String[]{ISSUE_TYPE_BUG, ISSUE_TYPE_IMPROVEMENT, ISSUE_TYPE_TASK});
        tester.assertRadioOptionValueNotPresent("issuetype", ISSUE_TYPE_NEWFEATURE);

        // Test issue type not in project
        gotoConvertSubTaskStep2(SUBTASK_ID_NO_FEATURES_IN_SCHEME, "2");
        tester.assertTextPresent("Step 1 of 4");
        tester.assertTextPresent("Issue type New Feature not applicable for this project");

        //Test invalid issue type
        gotoConvertSubTaskStep2(SUBTASK_ID_NO_FEATURES_IN_SCHEME, "9999");
        tester.assertTextPresent("Step 1 of 4");
        tester.assertTextPresent("Selected issue type not found.");

        //Test no issue type passed
        gotoConvertSubTaskStep2(SUBTASK_ID_NO_FEATURES_IN_SCHEME, "");
        tester.assertTextPresent("Step 1 of 4");
        tester.assertTextPresent("Issue type not specified");

        //Test subtask issue type
        gotoConvertSubTaskStep2(SUBTASK_ID_NO_FEATURES_IN_SCHEME, "5");
        tester.assertTextPresent("Step 1 of 4");
        tester.assertTextPresent("Issue type Sub-task is a sub-task");


    }

    private void gotoConvertSubTask(String issueId) {
        tester.gotoPage("/secure/ConvertSubTask.jspa?id=" + issueId);
    }

    private void gotoConvertSubTaskStep2(String issueId, String issueType) {
        tester.gotoPage("/secure/ConvertSubTaskSetIssueType.jspa?id=" + issueId + "&issuetype=" + issueType);
    }


}
