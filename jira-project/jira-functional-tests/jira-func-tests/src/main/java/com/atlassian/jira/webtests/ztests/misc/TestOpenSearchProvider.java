package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.API})
@LoginAs(user = ADMIN_USERNAME)
public class TestOpenSearchProvider extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
    }

    @Test
    public void testOpenSearch() throws IOException {
        navigation.gotoPage("/osd.jsp");
        String responseText = tester.getDialog().getResponse().getText();
        Assert.assertTrue(responseText.contains("<ShortName>jWebTest JIRA installation</ShortName>"));
        Assert.assertTrue(responseText.contains("<Description>Atlassian JIRA Search Provider</Description>"));
        Assert.assertTrue(responseText.contains("/secure/QuickSearch.jspa?searchString={searchTerms}"));
    }
}
