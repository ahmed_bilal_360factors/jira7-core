package com.atlassian.jira.webtests.ztests.license;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.webtests.LicenseKeys;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;


/**
 * Uses the following xml files:
 * <p/>
 * TestLicenseMessagesNewBuildOldLicenseFull.xml
 * <p/>
 * Most of other scenarios unit tested in: com.atlassian.jira.license.TestDefaultLicenseDetails
 *
 * @since v3.13
 */
@WebTest({Category.FUNC_TEST, Category.LICENSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestLicenseMessages extends BaseJiraFuncTest {
    private static final String URL_WWW_ATLASSIAN_COM_EXPIRED_EVAL = "http://www.atlassian.com/software/jira/expiredevaluation.jsp";

    @Inject
    private FuncTestLogger logger;
    @Inject
    private Administration administration;
    @Inject
    private TextAssertions textAssertions;

    private static LicenseInfo createSupportedLicense(final License forLicense) {
        return new LicenseInfo(forLicense,
                new String[]{
                        "(Support and updates available until "
                },
                new String[]{
                        "JIRA support and updates for this license ended on ",
                        "JIRA support and updates created after ",
                        "are not valid for this license."
                },
                new LicenseInfoUrl[]{}
        );
    }

    private static LicenseInfo createUnsupportedLicense(final License forLicense) {
        return new LicenseInfo(forLicense,
                new String[]{
                        "(Updates available until "
                },
                new String[]{
                },
                new LicenseInfoUrl[]{}
        );
    }

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
    }

    @Test
    public void testSwitchableLicenses() throws Exception {
        final List<LicenseInfo> switchableLicenses = ImmutableList.of(
                new LicenseInfo(LicenseKeys.EVAL_EXPIRED,
                        new String[]{
                                "(Your evaluation has expired.)",
                                "Your JIRA evaluation period expired on ",
                                "You are not able to create new issues in JIRA.",
                                "To reactivate JIRA, please "
                        },
                        new String[]{},
                        new LicenseInfoUrl[]{
                                new LicenseInfoUrl("purchase JIRA", URL_WWW_ATLASSIAN_COM_EXPIRED_EVAL)
                        }),
                new LicenseInfo(LicenseKeys.COMMERCIAL,
                        new String[]{
                                "(Support and updates available until "
                        },
                        new String[]{
                        },
                        new LicenseInfoUrl[]{}),
                createSupportedLicense(LicenseKeys.COMMUNITY),
                createSupportedLicense(LicenseKeys.DEVELOPER),
                createUnsupportedLicense(LicenseKeys.PERSONAL),
                createSupportedLicense(LicenseKeys.OPEN_SOURCE),
                createUnsupportedLicense(LicenseKeys.DEMO));

        for (final LicenseInfo licenseInfo : switchableLicenses) {
            logger.log("Testing license for " + licenseInfo.description);
            administration.switchToLicense(licenseInfo.license, licenseInfo.description);

            if (licenseInfo.license.equals(LicenseKeys.EVAL_EXPIRED.getLicenseString())) {
                // make sure we CANT create issues
                tester.gotoPage("secure/CreateIssue!default.jspa");
                assertCantCreateIssues();

                // make sure we CANT create issues even if they jump to the second step
                tester.gotoPage("secure/CreateIssue.jspa?pid=10000&issuetype=1");
                assertCantCreateIssues();
            }
        }
    }

    private void assertCantCreateIssues() {
        final CssLocator locator = new CssLocator(tester, ".aui-message.error");
        textAssertions.assertTextPresent(locator, "You will not be able to create new issues because your JIRA evaluation period has expired, please contact your JIRA administrators.");
    }

    private static class LicenseInfo {
        final String license;
        final String description;
        final String[] messages;
        final String[] notMessages;
        final LicenseInfoUrl[] urls;

        private LicenseInfo(final License forLicense, final String[] messages, final String[] notMessages, final LicenseInfoUrl[] urls) {
            this.license = forLicense.getLicenseString();
            this.description = forLicense.getDescription();
            this.messages = messages;
            this.notMessages = notMessages;
            this.urls = urls;
        }
    }

    private static class LicenseInfoUrl {
        String text;
        String url;

        private LicenseInfoUrl(final String text, final String url) {
            this.text = text;
            this.url = url;
        }
    }
}
