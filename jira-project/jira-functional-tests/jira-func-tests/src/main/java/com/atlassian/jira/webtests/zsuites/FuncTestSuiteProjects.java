package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.project.TestComponentValidation;
import com.atlassian.jira.webtests.ztests.project.TestDeleteProject;
import com.atlassian.jira.webtests.ztests.project.TestEditProject;
import com.atlassian.jira.webtests.ztests.project.TestEditProjectLeadAndDefaultAssignee;
import com.atlassian.jira.webtests.ztests.project.TestEditProjectPCounter;
import com.atlassian.jira.webtests.ztests.project.TestMultipleProjectsWithIssueSecurityWithRoles;
import com.atlassian.jira.webtests.ztests.project.TestProjectCategory;
import com.atlassian.jira.webtests.ztests.project.TestProjectComponentQuickSearch;
import com.atlassian.jira.webtests.ztests.project.TestProjectCreateBasedOnExisting;
import com.atlassian.jira.webtests.ztests.project.TestProjectKeyEditOnEntityLinks;
import com.atlassian.jira.webtests.ztests.project.TestProjectKeyEditOnSearch;
import com.atlassian.jira.webtests.ztests.project.TestProjectRoles;
import com.atlassian.jira.webtests.ztests.project.TestVersionValidation;
import com.atlassian.jira.webtests.ztests.project.TestViewProjectRoleUsage;
import com.atlassian.jira.webtests.ztests.user.TestEditUserProjectRoles;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of tests around JIRA projects
 *
 * @since v4.0
 */
public class FuncTestSuiteProjects {

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestComponentValidation.class)
                .add(TestVersionValidation.class)
                .add(TestDeleteProject.class)
                .add(TestEditProject.class)
                .add(TestEditProjectLeadAndDefaultAssignee.class)
                .add(TestEditProjectPCounter.class)
                .add(TestEditUserProjectRoles.class)
                .add(TestViewProjectRoleUsage.class)
                .add(TestProjectKeyEditOnEntityLinks.class)
                .add(TestProjectKeyEditOnSearch.class)
                .add(TestProjectRoles.class)
                .add(TestProjectComponentQuickSearch.class)
                .add(TestProjectCategory.class)
                .add(TestMultipleProjectsWithIssueSecurityWithRoles.class)
                .add(TestProjectCreateBasedOnExisting.class)
                .build();
    }
}