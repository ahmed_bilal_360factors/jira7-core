package com.atlassian.jira.webtests.ztests.imports.project;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import java.io.File;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;

/**
 * Func tests the project import results screen and the errors leading up to that screen.
 *
 * @since v6.5
 */
@WebTest({Category.FUNC_TEST, Category.PROJECT_IMPORT, Category.DATABASE})
public class TestProjectImportPluginData extends BaseJiraProjectImportFuncTest implements FunctTestConstants {

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void aoDataImported() throws Exception {
        try (AutoCloseable tmp = tempFile(doProjectImport("TestAoProjectImport.zip", "TestProjectImportStandardSimpleDataEmptyMKYProject.xml"))) {
            // Make sure we are ready to import
            tester.assertTextPresent("The results of automatic mapping are displayed below. You will not be able to continue if any validation errors were raised");
            tester.assertSubmitButtonPresent("Import");

            tester.submit("Import");
            advanceThroughWaitingPage();

            // We should end up on the results page
            final XPathLocator xPathLocator = new XPathLocator(tester, "//div[@id='systemfields']/table");
            final String projectDetailsResults = xPathLocator.getText();
            assertThat(projectDetailsResults, containsString("Key: MKY"));
            // Check refentity count
            assertThat(projectDetailsResults, containsString("Reference entities: 3"));

            // Check that group TestPluginGroup was added & delete it.
            administration.usersAndGroups().deleteGroup("TestPluginGroup");

            // Go to the ref plugin entity list and assert our entries are there
            navigation.gotoPage("/plugins/servlet/refentity/list");
            tester.assertTextPresent("TestEntity1");
            tester.assertTextPresent("TestEntity2");
            tester.assertTextPresent("TestEntity3");

            // Test all the plugin points we expected got called
            assertThat(backdoor.applicationProperties().getOption("ReferenceAoPreImport.args.set"), is(true));
            assertThat(backdoor.applicationProperties().getOption("ReferenceAoPostImport.args.set"), is(true));
            assertThat(backdoor.applicationProperties().getOption("ReferenceOfBizPreImport.args.set"), is(true));
            assertThat(backdoor.applicationProperties().getOption("ReferenceOfBizPostImport.args.set"), is(true));

            assertThat(backdoor.applicationProperties().getOption("ReferencePreImportPluginRun"), is(true));
            assertThat(backdoor.applicationProperties().getOption("ReferencePreImportPluginRun.args.set"), is(true));
            assertThat(backdoor.applicationProperties().getOption("ReferencePostImportPluginRun"), is(true));
            assertThat(backdoor.applicationProperties().getOption("ReferencePostImportPluginRun.args.set"), is(true));

            navigation.gotoDashboard();
        }

        this.navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void refPluginValidationError() throws Exception {
        try (AutoCloseable tmp = tempFile(doProjectImport("TestAoProjectImport.zip", "TestProjectImportStandardSimpleDataNoIssues.xml", "homosapien"))) {
            // Make sure we are ready to import
            tester.assertTextPresent("The results of automatic mapping are displayed below. You will not be able to continue if any validation errors were raised");
            tester.assertTextPresent("There is no ref entity for this project. Please add &#39;HSP&#39;.");
            tester.assertSubmitButtonNotPresent("Import");
            tester.assertSubmitButtonPresent("refreshValidationButton");
        }

        this.navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    private AutoCloseable tempFile(final File file) {
        return file::delete;
    }
}
