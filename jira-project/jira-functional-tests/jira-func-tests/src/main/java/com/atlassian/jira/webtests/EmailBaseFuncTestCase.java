package com.atlassian.jira.webtests;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.webtests.util.mail.MailService;
import com.google.common.collect.Sets;
import com.icegreen.greenmail.store.FolderException;
import com.icegreen.greenmail.store.MailFolder;
import com.icegreen.greenmail.store.SimpleStoredMessage;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.opensymphony.util.TextUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;

import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.BindException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import static com.atlassian.jira.matchers.RegexMatchers.regexMatches;
import static com.atlassian.jira.matchers.RegexMatchers.regexMatchesNot;
import static com.atlassian.jira.webtests.JIRAServerSetup.IMAP;
import static com.atlassian.jira.webtests.JIRAServerSetup.POP3;
import static com.atlassian.jira.webtests.JIRAServerSetup.SMTP;
import static com.google.common.collect.Sets.newHashSetWithExpectedSize;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * This class extends FuncTestCase by adding methods to test emails being sent from JIRA.
 */
public class EmailBaseFuncTestCase extends BaseJiraFuncTest implements FunctTestConstants {
    public static final String DEFAULT_FROM_ADDRESS = "jiratest@atlassian.com";
    public static final String DEFAULT_SUBJECT_PREFIX = "[JIRATEST]";
    public static final String newline = "\r\n";
    public static final String HTML_FORMAT_REGEX = "<body class=\"jira\" style=\".*\">\\s*<table id=\"background-table\"";
    protected MailService mailService;
    @Inject
    private FuncTestLogger logger;
    @Inject
    private TextAssertions textAssertions;

    @Before
    public void createMailService() {
        mailService = new MailService(logger);
    }

    @After
    public void stopMailService() {
        flushMailQueue();
        mailService.stop();
    }

    /**
     * Use this method to start a {@link com.icegreen.greenmail.smtp.SmtpServer}.
     * <p>
     * This will also configure JIRA to use this SMTP server in the admin section. You should call this after your data
     * import. This will override any existing mail servers setup already.
     * </p>
     * <p>
     * A simple SMTP server proxy is started by first attempting to start on a default port number. If this port is
     * already used we try that port number plus one and so on for 10 attempts. this allows for multiple tests running
     * in Bamboo concurrently, and also for a particular test machine maybe using that port already.
     * </p>
     * <p>
     * The tearDown() method will close the TCP socket.
     * </p>
     */
    protected void configureAndStartSmtpServer() {
        configureAndStartMailServers(DEFAULT_FROM_ADDRESS, DEFAULT_SUBJECT_PREFIX, SMTP);
    }

    protected void configureAndStartSmtpServer(final String from, final String prefix) {
        configureAndStartMailServers(from, prefix, SMTP);
    }

    protected void configureAndStartMailServers(final String from, final String prefix, final JIRAServerSetup... jiraServerSetups) {
        assertSendingMailIsEnabled();

        startMailService(jiraServerSetups);

        final List<JIRAServerSetup> serverSetupList = asList(jiraServerSetups);

        if (serverSetupList.contains(IMAP)) {
            final int imapPort = mailService.getImapPort();
            logger.log("Setting IMAP server to 'localhost:" + imapPort + "'");
            backdoor.mailServers().addPopServer("Local Test Imap Server", "Imap Server for test purposes", "imap",
                    "localhost", imapPort, ADMIN_USERNAME, ADMIN_PASSWORD);
        }
        if (serverSetupList.contains(POP3)) {
            final int popPort = mailService.getPop3Port();
            logger.log("Setting POP3 server to 'localhost:" + popPort + "'");
            backdoor.mailServers().addPopServer("Local Test Pop Server", "Pop Server for test purposes", "pop3",
                    "localhost", popPort, ADMIN_USERNAME, ADMIN_PASSWORD);
        }
        if (serverSetupList.contains(SMTP)) {
            final int smtpPort = mailService.getSmtpPort();
            logger.log("Setting SMTP server to 'localhost:" + smtpPort + "'");
            backdoor.mailServers().addSmtpServer(from, prefix, smtpPort);
        }
    }

    protected void configureAndStartSmtpServerWithNotify() {
        configureAndStartSmtpServer();
        // Chances are you if you're testing mail you want this setting enabled from previous default
        navigation.userProfile().changeNotifyMyChanges(true);
    }

    protected void startMailService(final JIRAServerSetup... jiraServerSetups) {
        try {
            mailService.configureAndStartGreenMail(jiraServerSetups);
        } catch (final BindException e) {
            fail("Error: Could not start green mail server. See log for details.");
        }
    }

    /**
     * Given a comma seperated list of email addresses, returns a collection of the email addresses.
     *
     * @param emails comma seperated list of email addresses
     * @return collection of individual email address
     */
    protected Collection<String> parseEmailAddresses(final String emails) {
        final StringTokenizer st = new StringTokenizer(emails, ",");
        final Collection<String> emailList = new ArrayList<String>();
        while (st.hasMoreTokens()) {
            final String email = st.nextToken().trim();
            if (TextUtils.stringSet(email)) {
                emailList.add(email.trim());
            }
        }
        return emailList;
    }

    protected void assertRecipientsHaveMessages(final Collection<String> recipients) throws MessagingException {
        for (final String recipient : recipients) {
            assertThat(getMessagesForRecipient(recipient), not(empty()));
        }
    }

    protected List<MimeMessage> getMessagesForRecipient(final String recipient) throws MessagingException {
        final MimeMessage[] messages = mailService.getReceivedMessages();
        final List<MimeMessage> ret = new ArrayList<MimeMessage>();

        for (final MimeMessage message : messages) {
            if (asList(message.getHeader("To")).contains(recipient)) {
                ret.add(message);
            }
        }

        return ret;
    }

    protected void assertSendingMailIsEnabled() {
        navigation.gotoAdminSection(Navigation.AdminSection.MAIL_QUEUE);

        try {
            final String responseText = tester.getDialog().getResponse().getText();
            if (responseText.contains("Sending mail is disabled")) {
                fail("Mail sending is disabled. Please restart your server without -Datlassian.mail.senddisabled=true.");
            }
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void setupJiraImapPopServer() {
        navigation.gotoAdminSection(Navigation.AdminSection.INCOMING_MAIL);
        tester.clickLinkWithText("Add POP / IMAP mail server");
        tester.setFormElement("name", "Local Test Pop/Imap Server");
        tester.setFormElement("serverName", "localhost");
        tester.setFormElement("username", ADMIN_USERNAME);
        tester.setFormElement("password", ADMIN_USERNAME);
        tester.submit("Add");
    }

    protected void setupPopService() {
        setupPopService("project=MKY, issue=1, createusers=true, issuetype=1");
    }

    protected void setupPopService(final String handlerParameters) {
        navigation.gotoAdminSection(Navigation.AdminSection.SERVICES);
        tester.setFormElement("name", "pop");
        tester.setFormElement("clazz", "com.atlassian.jira.service.services.mail.MailFetcherService");
        tester.setFormElement("service.schedule.cronString", "0 0 * * * ?");
        tester.submit("Add Service");
        tester.setFormElement("handler.params", handlerParameters);
        tester.setFormElement("service.schedule.cronString", "0 0 * * * ?");
        tester.submit("Update");

    }

    protected void setupImapService() {
        setupImapService("project=MKY, issue=1, createusers=true, issuetype=1");
    }

    protected void setupImapService(final String handlerParameters) {
        navigation.gotoAdminSection(Navigation.AdminSection.SERVICES);
        tester.setFormElement("name", "imap");
        tester.setFormElement("clazz", "com.atlassian.jira.service.services.mail.MailFetcherService");
        tester.setFormElement("service.schedule.cronString", "0 0 * * * ?");
        tester.submit("Add Service");
        tester.setFormElement("handler.params", handlerParameters);
        tester.setFormElement("service.schedule.cronString", "0 0 * * * ?");
        tester.submit("Update");
    }

    /**
     * This is useful for writing func tests that test that the correct notifications are being sent. It goest to the
     * admin section mail-queue and flushes the queue and waits till it recieves emailCount number of emails before
     * timeout. If the timeout is reached before the expected number of emails arrives will fail.
     *
     * @param emailCount number of expected emails to wait to receive
     * @throws InterruptedException if interrupted
     */
    protected void flushMailQueueAndWait(final int emailCount) throws InterruptedException {
        // JDEV-29756: We are seeing flakey time outs in production ... better to bump this timeout up to something large.
        // When the test passes it will still be just as fast, this can only slow down genuine failures and make the test
        // less flakey.
        flushMailQueueAndWait(emailCount, 5000);
    }

    /**
     * Does the same as {@link #flushMailQueueAndWait(int)} but allows the user to specify the wait period in case a lot
     * of e-mails are being sent.
     *
     * @param emailCount       number of expected emails to wait to receive
     * @param waitPeriodMillis The amount of time to wait in millis until the e-mails should have arrived.
     * @throws InterruptedException if interrupted
     */
    protected void flushMailQueueAndWait(final int emailCount, final int waitPeriodMillis) throws InterruptedException {
        flushMailQueue();
        logger.log("Flushed mail queue. Waiting for '" + waitPeriodMillis + "' ms...");
        // Sleep for a small while - just to be sure the mail is received.
        final boolean receivedAllMail = mailService.waitForIncomingMessage(waitPeriodMillis, emailCount);

        if (!receivedAllMail) {
            failNotReceivedAllMail(emailCount);
        }
    }

    /**
     * Flushes the mail queue and waits for emails to arrive for specified recipients instead of a set number of emails.
     *
     * @param waitPeriodMillis   The amount of time to wait in millis until the e-mails should have arrived.
     * @param recipientAddresses the addresses to wait for or fail
     * @throws InterruptedException if interrupted
     * @throws MessagingException   if there is a problem extracting the sent messages
     */
    protected void flushMailQueueAndWaitForRecipients(final int waitPeriodMillis, final String... recipientAddresses)
            throws InterruptedException, MessagingException {
        flushMailQueue();

        long start = System.currentTimeMillis();
        logger.log("Flushed mail queue. Waiting for '" + waitPeriodMillis + "' ms...");
        // Sleep for a small while - just to be sure the mail is received.
        final boolean receivedAllMail = mailService.waitForIncomingMessage(waitPeriodMillis, recipientAddresses.length);
        long waited = System.currentTimeMillis() - start;
        long remainder = waitPeriodMillis - waited;

        if (!receivedAllMail) {
            failNotReceivedAllMail(recipientAddresses.length);
        }

        final Set<String> remainingRecipients = newHashSetWithExpectedSize(recipientAddresses.length);
        remainingRecipients.addAll(asList(recipientAddresses));

        while (remainder > 0 && !remainingRecipients.isEmpty()) {
            start = System.currentTimeMillis();
            final MimeMessage[] receivedMessages = mailService.getReceivedMessages();
            for (final MimeMessage message : receivedMessages) {
                final String[] tos = message.getHeader("To");
                remainingRecipients.removeAll(asList(tos));
            }

            if (remainingRecipients.isEmpty()) return;

            mailService.waitForIncomingMessage(remainder, receivedMessages.length + 1);
            waited = System.currentTimeMillis() - start;
            remainder = remainder - waited;
        }

        String msg = "Did not receive emails for all recipients (" + StringUtils.join(recipientAddresses, ", ") +
                ") within the timeout.";
        final MimeMessage[] receivedMessages = mailService.getReceivedMessages();
        if (receivedMessages != null) {
            if (receivedMessages.length > 0) {
                msg += "\n  Recipients: " + display(receivedMessages);
            }
        } else {
            msg += " Received zero messages.";
        }
        fail(msg);
    }

    private void failNotReceivedAllMail(final int emailCount) {
        String msg = "Did not receive all expected emails (" + emailCount + ") within the timeout.";
        final MimeMessage[] receivedMessages = mailService.getReceivedMessages();
        if (receivedMessages != null) {
            msg += " Only received " + receivedMessages.length + " message(s).";
            if (receivedMessages.length > 0) {
                msg += "\n  Recipients: " + display(receivedMessages);
            }
        } else {
            msg += " Received zero messages.";
        }
        fail(msg);
    }

    protected void flushMailQueue() {
        //flush mail queue
        backdoor.mailServers().flushMailQueue();
    }

    private String display(final MimeMessage[] receivedMessages) {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < receivedMessages.length; i++) {
            if (i > 0)
                sb.append(", ");
            final MimeMessage receivedMessage = receivedMessages[i];
            try {
                sb.append(receivedMessage.getRecipients(Message.RecipientType.TO)[0]);
            } catch (final MessagingException e) {
                sb.append("???");
            }
        }
        return sb.toString();
    }

    protected void waitForMail(final int emailCount) throws InterruptedException {
        final int waitPeriodMillis = 500;
        assertTrue("Did not recieve all expected emails within the timeout", mailService.waitForIncomingMessage(waitPeriodMillis, emailCount));
    }

    /**
     * Asserts that the given email's body contains the bodySubString using indexOf.
     *
     * @param email         email to extract the content body from
     * @param bodySubString expected substring of the email body
     * @throws MessagingException Message error
     * @throws IOException        IO error
     * @see GreenMailUtil#getBody(javax.mail.Part)
     */
    protected void assertEmailBodyContains(final MimeMessage email, final String bodySubString)
            throws MessagingException, IOException {
        final String emailBody = GreenMailUtil.getBody(email);
        assertTrue("The string '" + bodySubString + "' was not found in the e-mail body [" + emailBody + "]",
                emailBody.contains(bodySubString));
    }

    /**
     * Asserts that the given email's body contains a line which matches the given string or pattern.
     * If multiple lines are specified, they must appear in the given order.
     *
     * @param email       email to extract the content body from
     * @param linePattern expected line or line pattern
     * @throws MessagingException Message error
     * @throws IOException        IO error
     * @see GreenMailUtil#getBody(javax.mail.Part)
     */
    protected void assertEmailBodyContainsLine(final MimeMessage email, final String... linePattern)
            throws MessagingException, IOException {
        // temporary fix for JRADEV-23803
        final String emailBody = decodeQuotedPrintableBody(email);

        final String[] lines = emailBody.split("\\n");
        int linePatternIdx = 0;
        for (final String line : lines) {
            if (line.trim().matches(linePattern[linePatternIdx])) {
                linePatternIdx++;
            }

            if (linePatternIdx >= linePattern.length) {
                return;     // All lines found - all good!
            }
        }

        fail("The line '" + linePattern[linePatternIdx] + "' was not found in the e-mail body [" + emailBody + "]");
    }

    /**
     * Asserts that the given email's body does not contain the bodySubString using indexOf.
     *
     * @param email         email to extract the content body from
     * @param bodySubString string to not occur in body
     * @throws MessagingException Message error
     * @throws IOException        IO error
     * @see GreenMailUtil#getBody(javax.mail.Part)
     */
    protected void assertEmailBodyDoesntContain(final MimeMessage email, final String bodySubString)
            throws MessagingException, IOException {
        final String emailBody = GreenMailUtil.getBody(email);
        assertTrue("The string '" + bodySubString + "' was found (shouldn't exist) in the e-mail body [" + emailBody + "]",
                !emailBody.contains(bodySubString));
    }

    /**
     * Assert that the String emailBody contains bodySubString
     *
     * @param emailBody     body
     * @param bodySubString expected substring
     * @throws MessagingException message error
     * @throws IOException        IO error
     */
    protected void assertEmailBodyContains(final String emailBody, final String bodySubString)
            throws MessagingException, IOException {
        assertTrue("Expected '" + bodySubString + "' to be present in email body '" + emailBody + "'", emailBody.contains(bodySubString));
    }

    protected void assertEmailHasNumberOfParts(final MimeMessage email, final int expectedNumOfParts)
            throws MessagingException, IOException {
        final Object emailContent = email.getContent();
        if (emailContent instanceof Multipart) {
            final Multipart multiPart = (Multipart) emailContent;
            assertEquals(expectedNumOfParts, multiPart.getCount());
        } else {
            fail("Cannot assert number of parts for email. Email is not a multipart type.");
        }
    }

    /**
     * Assert that the email was addressed to the expectedTo
     *
     * @param email      email to assert the value of the to header
     * @param expectedTo the single or comma seperated list of expected email addresses
     * @throws MessagingException message error
     * @see #assertEmailToEquals(javax.mail.internet.MimeMessage, java.util.Collection)
     */
    protected void assertEmailToEquals(final MimeMessage email, final String expectedTo) throws MessagingException {
        assertEmailToEquals(email, parseEmailAddresses(expectedTo));
    }

    /**
     * Assert that the email was addressed to each and everyone of the expectedAddresses
     *
     * @param email               email to assert the value of the to header
     * @param expectedToAddresses collection of expected email addresses
     * @throws MessagingException meesage error
     */
    protected void assertEmailToEquals(final MimeMessage email, final Collection<?> expectedToAddresses) throws MessagingException {
        final String[] toHeader = email.getHeader("to");
        assertThat(toHeader.length, equalTo(1));
        final Collection actualAddresses = parseEmailAddresses(toHeader[0]);
        assertEmailsEquals(expectedToAddresses, actualAddresses);
    }

    protected void assertEmailCcEquals(final MimeMessage email, final Collection<?> expectedCcAddresses) throws MessagingException {
        final String[] ccHeader = email.getHeader("cc");
        if (ccHeader != null) {
            assertThat(ccHeader.length, equalTo(1));
            final Collection actualAddresses = parseEmailAddresses(ccHeader[0]);
            assertEmailsEquals(expectedCcAddresses, actualAddresses);
        } else {
            //if there is no Cc header, assert that we were not expecting any emails.
            assertThat(expectedCcAddresses, empty());
        }
    }

    private void assertEmailsEquals(final Collection expectedAddresses, final Collection actualAddresses) {
        assertEquals(expectedAddresses, actualAddresses);
    }

    protected void assertEmailFromEquals(final MimeMessage email, final String expectedTo) throws MessagingException {
        final String[] addresses = email.getHeader("from");
        assertThat(asList(addresses), contains(expectedTo));
    }

    protected void assertEmailSubjectEquals(final MimeMessage email, final String subject) throws MessagingException {
        assertEquals(subject, email.getSubject());
    }

    protected void assertEmailSent(final String recipient, final String subject, final String issueComment)
            throws MessagingException, IOException {
        final List emails = getMessagesForRecipient(recipient);
        assertEquals("Incorrect number of e-mails received for '" + recipient + "'", 1, emails.size());
        final MimeMessage emailMessage = (MimeMessage) emails.get(0);
        assertEmailBodyContains(emailMessage, issueComment);
        assertEmailSubjectEquals(emailMessage, subject);
    }

    protected void assertCorrectNumberEmailsSent(final int numOfMessages)
            throws MessagingException {
        final MimeMessage[] messages = mailService.getReceivedMessages();
        if (messages.length != numOfMessages) {
            for (final MimeMessage message : messages) {
                logger.log("Mail sent to '" + message.getHeader("to")[0] + "' with SUBJECT '" + message.getSubject() + "'");
            }
            fail("Invalid number of e-mails received.  Was " + messages.length + " but should have been " + numOfMessages + ".");
        }
    }

    protected final MailBox getMailBox(final String email) throws FolderException {
        return new MailBox(mailService.getUserInbox(email), email);
    }

    /**
     * Temporary helper method for JRADEV-23803
     *
     * @param message
     * @return
     */
    private String decodeQuotedPrintableBody(final MimeMessage message) {
        String body = GreenMailUtil.getBody(message);

        try {
            final String[] headersArray = message.getHeader("Content-Transfer-Encoding");
            final Set<String> headers = Sets.newHashSet(headersArray == null ? new String[0] : headersArray);
            if (headers.contains("quoted-printable")) {
                final InputStream inputStream = MimeUtility.decode(new ByteArrayInputStream(body.getBytes()), "quoted-printable");
                final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                body = IOUtils.toString(reader);
            }
        } catch (final MessagingException e) {
            throw new RuntimeException(e);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }

        return body;
    }

    protected final void assertMessageAndType(final MimeMessage message, final String expectedComment, final boolean html) {
        // temporary fix for JRADEV-23803
        final String body = decodeQuotedPrintableBody(message);

        if (html) {
            assertThat(body, regexMatches(HTML_FORMAT_REGEX));
        } else {
            assertThat(body, regexMatchesNot(HTML_FORMAT_REGEX));
        }

        textAssertions.assertTextSequence(body, expectedComment);
    }

    protected final void assertNotMessageAndType(final MimeMessage message, final String expectedComment, final boolean html) {
        final String body = GreenMailUtil.getBody(message);

        if (html) {
            assertThat(body, regexMatches(HTML_FORMAT_REGEX));
        } else {
            assertThat(body, regexMatchesNot(HTML_FORMAT_REGEX));
        }

        textAssertions.assertTextNotPresent(body, expectedComment);
    }

    public static TypeSafeMatcher<MimeMessage> hasBody(Matcher<String> bodyMatcher) {
        return new TypeSafeMatcher<MimeMessage>() {
            @Override
            protected boolean matchesSafely(MimeMessage email) {
                final String emailBody = GreenMailUtil.getBody(email);
                return bodyMatcher.matches(emailBody);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Email body that matches ");
                bodyMatcher.describeTo(description);
            }
        };
    }

    protected final static class MailBox {
        private final MailFolder folder;
        private final String userEmail;
        private int pos;

        public MailBox(final MailFolder folder, final String userEmail) {
            this.folder = folder;
            this.userEmail = userEmail;
        }

        public MimeMessage nextMessage() {
            final List<SimpleStoredMessage> messages = getMessages();
            if (pos >= messages.size()) {
                return null;
            } else {
                return messages.get(pos++).getMimeMessage();
            }
        }

        private List<SimpleStoredMessage> getMessages() {
            return this.folder.getMessages();
        }

        public int size() {
            return this.folder.getMessageCount();
        }

        public void clear() {
            folder.deleteAllMessages();
            pos = 0;
        }

        public MimeMessage awaitMessage() {
            return awaitMessage(1000);
        }

        public MimeMessage awaitMessage(final long timeout) {
            MimeMessage message = nextMessage();
            final long startTime = System.currentTimeMillis();
            if (message == null) {
                final long timeoutTime = timeout + startTime;
                long currentTime = startTime;
                while (currentTime < timeoutTime && message == null) {
                    //I hate this, but greenmail does not have any way to wait for a particular message. Arrrh.
                    try {
                        Thread.sleep(100);
                    } catch (final InterruptedException e) {
                        break;
                    }

                    message = nextMessage();
                    currentTime = System.currentTimeMillis();
                }
            }

            if (message == null) {
                fail("Waited '" + (System.currentTimeMillis() - startTime) + "' ms for e-mail to '" + userEmail + "' but got nothing.");
            }
            return message;
        }
    }
}

