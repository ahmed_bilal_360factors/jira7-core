package com.atlassian.jira.webtests.ztests.navigator.jql.changehistory.assignee;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.ChangeHistoryAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.beans.UserDTO;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Set;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * @since v4.4
 */
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestChangeHistorySearch.xml")
@WebTest({Category.FUNC_TEST, Category.JQL, Category.CHANGE_HISTORY})
public class TestAssigneeWasSearching extends BaseJiraFuncTest {

    @Inject
    private ChangeHistoryAssertions changeHistoryAssertions;

    private static final String FIELD_NAME = "assignee";
    private static final String[] ALL_ISSUES = {"HSP-9", "HSP-8", "HSP-7", "HSP-6", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1"};

    @Before
    public void setPreferredSearchLayout() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    public void testWasEmptySearch() {
        String[] issueKeys = {"HSP-5", "HSP-2", "HSP-1"};
        changeHistoryAssertions.assertWasEmptySearchReturnsEmptyValuesUsingEmptyKeyword(FIELD_NAME, issueKeys);
    }

    @Test
    public void testWasNotEmptySearch() {
        String[] issueKeys = {"HSP-9", "HSP-8", "HSP-7", "HSP-6", "HSP-4", "HSP-3"};
        changeHistoryAssertions.assertWasNotEmptySearchReturnsNotEmptyValuesWithEmptyKeyword(FIELD_NAME, issueKeys);
    }

    @Test
    public void testWasSearchUsingSingleValueOperandsReturnsExpectedValues() {
        changeHistoryAssertions.assertWasSearchReturnsExpectedValues(FIELD_NAME, "admin", ALL_ISSUES);
        changeHistoryAssertions.assertWasSearchReturnsExpectedValues(FIELD_NAME, "fred", "HSP-9", "HSP-5", "HSP-4");
        changeHistoryAssertions.assertWasSearchReturnsExpectedValues(FIELD_NAME, "bob", "HSP-2");
    }

    @Test
    public void testWasSearchUsingListOperands() {
        Set<String> users = Sets.newHashSet("fred", "admin");
        changeHistoryAssertions.assertWasInSearchReturnsExpectedValues(FIELD_NAME, users, ALL_ISSUES);
        users = Sets.newHashSet("fred", "bob");
        changeHistoryAssertions.assertWasInSearchReturnsExpectedValues(FIELD_NAME, users, "HSP-9", "HSP-5", "HSP-4", "HSP-2");
    }

    @Test
    public void testWasNotInSearchUsingListOperands() {
        Set<String> users = Sets.newHashSet("fred", "bob");
        String[] expected = {"HSP-8", "HSP-7", "HSP-6", "HSP-3", "HSP-1"};
        changeHistoryAssertions.assertWasNotInSearchReturnsExpectedValues(FIELD_NAME, users, expected);
    }


    @Test
    public void testWasSearchUsingByPredicate() {
        String[] expected = {"HSP-9", "HSP-5", "HSP-4"};
        changeHistoryAssertions.assertWasBySearchReturnsExpectedValues(FIELD_NAME, "fred", "admin", expected);
        expected = new String[]{"HSP-9"};
        changeHistoryAssertions.assertWasBySearchReturnsExpectedValues(FIELD_NAME, "admin", "fred", expected);
        changeHistoryAssertions.assertWasBySearchUsingListOperandsReturnsExpectedValues(FIELD_NAME, "empty", Sets.newHashSet("fred", "bob"), "HSP-2", "HSP-1");
    }

    @Test
    public void testWasSearchUsingDuringPredicate() {
        String[] expected = {"HSP-9", "HSP-8", "HSP-7", "HSP-6", "HSP-5", "HSP-3", "HSP-2", "HSP-1"};
        changeHistoryAssertions.assertWasDuringSearchReturnsExpectedValues(FIELD_NAME, "admin", "'2011/05/01'", "'2011/05/31'", expected);
    }

    @Test
    public void testWasSearchUsingBeforePredicate() {
        String[] expected = {"HSP-4", "HSP-3", "HSP-2", "HSP-1"};
        changeHistoryAssertions.assertWasBeforeSearchReturnsExpectedValues(FIELD_NAME, "admin", "'2011/05/01'", expected);
        expected = new String[]{"HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1"};
        changeHistoryAssertions.assertWasBeforeSearchReturnsExpectedValues(FIELD_NAME, "admin", "'2011/05/01 10:55'", expected);
    }

    @Test
    public void testWasSearchUsingAfterPredicate() {
        String[] expected = {"HSP-9", "HSP-8", "HSP-7", "HSP-6", "HSP-5", "HSP-3", "HSP-2", "HSP-1"};
        changeHistoryAssertions.assertWasAfterSearchReturnsExpectedValues(FIELD_NAME, "admin", "'2011/05/01'", expected);
        expected = new String[]{"HSP-9", "HSP-8", "HSP-7", "HSP-6", "HSP-3", "HSP-2", "HSP-1"};
        changeHistoryAssertions.assertWasAfterSearchReturnsExpectedValues(FIELD_NAME, "admin", "'2011/06/01 10:55'", expected);
    }

    @Test
    public void testWasSearchUsingOnPredicate() {
        String[] expected = {"HSP-9", "HSP-8", "HSP-7", "HSP-6", "HSP-5", "HSP-3", "HSP-2", "HSP-1"};
        changeHistoryAssertions.assertWasOnSearchReturnsExpectedValues(FIELD_NAME, "admin", "'2011/05/01'", expected);
        expected = new String[]{"HSP-9", "HSP-8", "HSP-7", "HSP-6", "HSP-3", "HSP-2", "HSP-1"};
        changeHistoryAssertions.assertWasOnSearchReturnsExpectedValues(FIELD_NAME, "admin", "'2011/06/01'", expected);
    }

    @Test
    public void testWasSearchUsingLongOperandsIsInvalid() {
        // invalid operand  type
        String expectedError = "A value with ID '1' does not exist for the field 'assignee'.";
        changeHistoryAssertions.assertInvalidSearchProducesError(FIELD_NAME, "1", "", expectedError);
    }

    @Test
    public void testWasSearchUsingUnclosedListIsInvalid() {
        // invalid list
        String expectedError = "Error in the JQL Query: Expecting ')' before the end of the query.";
        changeHistoryAssertions.assertInvalidSearchProducesError(FIELD_NAME, "(fred, bob", "", expectedError);
    }

    @Test
    public void testWasSearchUsingIncorrectPredicateIsInvalid() {
        // invalid predicate
        String expectedError = "Error in the JQL Query: Expecting either 'OR' or 'AND' but got 'at'. (line 1, character 26)";
        changeHistoryAssertions.assertInvalidSearchProducesError(FIELD_NAME, "(fred, bob)", "at '10:55'", expectedError);
    }

    @Test
    public void testWasSearchUsingIncorrectFunctionIsInvalid() {
        // invalid function
        String expectedError = "A value provided by the function 'currentLogin' is invalid for the field 'assignee'.";
        changeHistoryAssertions.assertInvalidSearchProducesError(FIELD_NAME, "currentLogin()", "", expectedError);
    }

    @Test
    public void testWasInAcceptsListFunctions() {
        // JRADEV-6413 membersOf causes stack trace
        changeHistoryAssertions.assertWasInListFunctionReturnsExpectedValues(FIELD_NAME, "membersOf('jira-users')", ALL_ISSUES);
        changeHistoryAssertions.assertWasInListFunctionReturnsExpectedValues(FIELD_NAME, "membersOf('jira-administrators')", ALL_ISSUES);
    }

    @Test
    // JDEV-34987 Disabled users were messing this up.
    public void testWasInAcceptsListFunctionWithDisabledUser() {
        // Rename "fred" to something else and disable him.
        final UserDTO fred = backdoor.usersAndGroups().getUserByName("fred");
        final UserDTO newfred = new UserDTO(false, fred.getDirectoryId(), fred.getDisplayName(), fred.getEmail(),
                fred.getKey(), "newfred", "newfred", null);
        backdoor.usersAndGroups().updateUser(newfred);

        changeHistoryAssertions.assertWasInListFunctionReturnsExpectedValues(FIELD_NAME, "membersOf('jira-users')", ALL_ISSUES);
    }
}