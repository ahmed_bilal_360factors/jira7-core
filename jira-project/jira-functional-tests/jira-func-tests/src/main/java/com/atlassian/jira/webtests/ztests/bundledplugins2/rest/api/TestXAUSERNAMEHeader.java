package com.atlassian.jira.webtests.ztests.bundledplugins2.rest.api;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.sun.jersey.api.client.ClientResponse;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertThat;

/**
 * This is API backwards compatibility test. X-AUSERNAME is a header which we return in all
 * REST responses. The header contain information about username of the logged in user or
 * 'anonymous' if the user was logged out.
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@RestoreBlankInstance
public class TestXAUSERNAMEHeader extends BaseJiraRestTest {
    public static final String ADMIN = "admin";
    public static final String ANONYMOUS = "anonymous";

    private Client restApiClient;

    @Before
    public void setUp() throws Exception {
        this.restApiClient = new Client(environmentData);
    }

    @Test
    public void testGettingIssueWithLoggedInUser() {
        IssueCreateResponse issue = backdoor.issues().createIssue("MKY", "Issue summary");
        ClientResponse clientResponse = restApiClient.loginAs(ADMIN, ADMIN).issue(issue.key);

        readEntity(clientResponse);

        assertThat(clientResponse.getHeaders(), Matchers.hasKey("X-AUSERNAME"));
        assertThat(clientResponse.getHeaders().getFirst("X-AUSERNAME"), Matchers.is(ADMIN));
    }

    @Test
    public void testGettingIssueWithAnonymousUser() {
        IssueCreateResponse issue = backdoor.issues().createIssue("MKY", "Issue summary");
        ClientResponse clientResponse = restApiClient.anonymous().issue(issue.key);

        readEntity(clientResponse);

        assertThat(clientResponse.getHeaders(), Matchers.hasKey("X-AUSERNAME"));
        assertThat(clientResponse.getHeaders().getFirst("X-AUSERNAME"), Matchers.is(ANONYMOUS));
    }

    private void readEntity(final ClientResponse clientResponse) {
        try {
            clientResponse.getEntityInputStream().close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static class Client extends RestApiClient<Client> {
        public Client(final JIRAEnvironmentData environmentData) {
            super(environmentData);
        }

        public ClientResponse issue(String issueKey) {
            return createResource()
                    .path("issue")
                    .path(issueKey)
                    .get(ClientResponse.class);
        }
    }
}
