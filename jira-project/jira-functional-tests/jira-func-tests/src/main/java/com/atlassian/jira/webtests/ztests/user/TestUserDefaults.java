package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Test the UpdateUserDefaults page
 *
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUserDefaults extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
    }

    @Test
    public void testIssuesPerPageValidation() throws Exception {
        _testIssuesPerPageValidation("0", false);
        _testIssuesPerPageValidation("-100", false);
        _testIssuesPerPageValidation("1001", false);

        _testIssuesPerPageValidation("1", true);
        _testIssuesPerPageValidation("1000", true);
        _testIssuesPerPageValidation("500", true);
    }

    private void _testIssuesPerPageValidation(final String setting, final boolean valid) {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_DEFAULTS);
        tester.clickLink("user-defaults-edit");
        tester.setWorkingForm("edit_user_defaults");
        tester.setFormElement("numIssues", setting);
        tester.submit("Update");
        if (valid) {
            assertions.getTextAssertions().assertTextSequence(new CssLocator(tester, "#view_user_defaults"), new String[]{"Number of Issues displayed per Issue Navigator page", setting});
        } else {
            assertions.assertNodeHasText(new CssLocator(tester, ".error"), "Issues per page must be a number between 1 and 1000");
        }
    }
}
