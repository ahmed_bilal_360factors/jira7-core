package com.atlassian.jira.webtests.ztests.navigator.jql;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.meterware.httpunit.WebResponse;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;

/**
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.JQL})
@LoginAs(user = ADMIN_USERNAME)
public class TestQueryTooComplex extends BaseJiraFuncTest {
    private static final int CLAUSES = 70000;

    private String reallyBloodyLongQuery;
    private String statusjQlQuery;

    @Inject
    private IssueTableAssertions issueTableAssertions;

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        reallyBloodyLongQuery = createLongQuery();
        statusjQlQuery = "status was Open";
    }

    private String createLongQuery() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("comment ~ monkey");
        for (int i = 0; i < CLAUSES; ++i) {
            stringBuilder.append(" and comment ~ ").append(i);
        }
        return stringBuilder.toString();
    }

    @Test
    public void testSearchRequestView() throws Exception {
        administration.restoreData("TestQueryTooComplex.xml");
        viewSearchRequestViewForSearchFilter(10000, 400, "One of the clauses in your search matches too many results. If a clause returns too many results, the entire search will fail. Please try refining or removing the clauses in your search and run it again.");
    }

    private void viewSearchRequestViewForSearchFilter(int filterId, int responseCode, String errorMessage) {
        try {
            tester.getDialog().getWebClient().setExceptionsThrownOnErrorStatus(false);
            tester.gotoPage("/sr/jira.issueviews:searchrequest-xml/" + filterId + "/SearchRequest-" + filterId + ".xml?tempMax=1000");
            final WebResponse response = tester.getDialog().getResponse();
            assertEquals(responseCode, response.getResponseCode());
            tester.assertTextPresent(errorMessage);

        } finally {
            tester.getDialog().getWebClient().setExceptionsThrownOnErrorStatus(true);
        }
    }

    @Test
    public void testChangeHistoryJqlTooComplex() throws Exception {
        administration.restoreBlankInstance();
        issueTableAssertions.assertSearchTooComplex(statusjQlQuery);
    }


}
