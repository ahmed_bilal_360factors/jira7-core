package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.admin.MailServerAdministration;
import com.atlassian.jira.functest.framework.admin.ViewServices;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.beans.WorkflowSchemeData;
import com.atlassian.jira.webtests.LicenseKeys;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_ADMIN_GROUP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SYSTEM_ADMINISTER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SYS_ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SYS_ADMIN_USERNAME;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests permissions of sysadmins and admins.
 *
 * @since v3.12
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.PERMISSIONS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestSystemAdminAndAdminPermissions extends BaseJiraFuncTest {
    private static final String MAIL_SERVER_ADMINISTRATION_URL = "/secure/admin/OutgoingMailServers.jspa";
    private static final String SETUP_MAIL_SERVER_WARNING_LINK_TEXT = "mail server";
    private static final String NO_MAIL_SERVER_SETUP_WARNING_CONTAINER_LOCATOR = "//*[@id='no-mail-server-setup-warning']";
    private static final String SETUP_MAIL_SERVER_WARNING_LINK_LOCATOR = NO_MAIL_SERVER_SETUP_WARNING_CONTAINER_LOCATOR + "/a";
    private static final String CONFIGURE_MAIL_SERVER_LINK_CONTAINER_LOCATOR = "//*[@id='configure_mail_server']/../..[@class='desc-wrap']";
    private static final String CONFIGURE_MAIL_SERVER_LINK_TEXT = "configure";
    private static final String VIEW_SERVICES_PAGE_FORM_TITLES_LOCATOR = ".formtitle";

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("TestSystemAdminAndAdminPermissions.xml");
    }

    /**
     * Removing admin (but not sysadmin) from the jira-users group should not stop admin from logging in.
     */
    @Test
    public void testRemoveAdminFromJiraUsers() {
        try {
            administration.usersAndGroups().removeUserFromGroup("nonsystemadmin", "jira-users");
            navigation.login("nonsystemadmin", "nonsystemadmin");

            tester.assertLinkPresent("log_out"); // thus we are logged in
        } finally {
            navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        }
    }

    /**
     * Removing sysadmin from the jira-users group should not stop sysadmin from logging in.
     */
    @Test
    public void testRemoveSysAdminFromJiraUsers() {
        administration.usersAndGroups().removeUserFromGroup(ADMIN_USERNAME, "jira-users");
        navigation.logout();
        navigation.login(ADMIN_USERNAME);

        tester.assertLinkPresent("log_out");
    }

    /**
     * Removing sysadmin from the jira-users and jira-administrators group should not stop sysadmin from logging in.
     */
    @Test
    public void testRemoveSysAdminFromAdminsAndJiraUsers() {
        administration.usersAndGroups().addGroup("systemadmins");
        administration.addGlobalPermission(SYSTEM_ADMINISTER, "systemadmins");
        administration.usersAndGroups().addUserToGroup(ADMIN_USERNAME, "systemadmins");

        administration.usersAndGroups().removeUserFromGroup(ADMIN_USERNAME, "jira-users");
        administration.usersAndGroups().removeUserFromGroup(ADMIN_USERNAME, "jira-administrators");

        navigation.logout();
        navigation.login(ADMIN_USERNAME);

        tester.assertLinkPresent("log_out");
    }

    @Test
    public void testNotificationSchemeMailServerWarningAsAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);

            assertions.getLinkAssertions().assertLinkNotPresentWithExactText(NO_MAIL_SERVER_SETUP_WARNING_CONTAINER_LOCATOR,
                    SETUP_MAIL_SERVER_WARNING_LINK_TEXT);
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testNotificationSchemeMailServerWarningAsSysAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);

            assertions.getLinkAssertions().assertLinkPresentWithExactText(NO_MAIL_SERVER_SETUP_WARNING_CONTAINER_LOCATOR,
                    SETUP_MAIL_SERVER_WARNING_LINK_TEXT);
            assertions.getLinkAssertions().assertLinkAtNodeContains(SETUP_MAIL_SERVER_WARNING_LINK_LOCATOR,
                    MAIL_SERVER_ADMINISTRATION_URL);

            tester.assertLinkNotPresentWithText("contact a System Administrator");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testEditNotificationSchemeMailServerWarningAsAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);
            tester.clickLinkWithText("Default Notification Scheme");

            assertions.getLinkAssertions().assertLinkNotPresentWithExactText(NO_MAIL_SERVER_SETUP_WARNING_CONTAINER_LOCATOR,
                    SETUP_MAIL_SERVER_WARNING_LINK_TEXT);
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }

    }

    @Test
    public void testEditNotificationSchemeMailServerWarningAsSysAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);
            tester.clickLinkWithText("Default Notification Scheme");

            tester.assertLinkNotPresentWithText("contact a System Administrator");

            assertions.getLinkAssertions().assertLinkPresentWithExactText(NO_MAIL_SERVER_SETUP_WARNING_CONTAINER_LOCATOR,
                    SETUP_MAIL_SERVER_WARNING_LINK_TEXT);
            assertions.getLinkAssertions().assertLinkAtNodeContains(SETUP_MAIL_SERVER_WARNING_LINK_LOCATOR,
                    MAIL_SERVER_ADMINISTRATION_URL);

        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }

    }

    @Test
    public void testSendBulkMailMailServerWarningAsAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.gotoAdminSection(Navigation.AdminSection.SEND_EMAIL);

            assertions.getLinkAssertions().assertLinkNotPresentWithExactText(CONFIGURE_MAIL_SERVER_LINK_CONTAINER_LOCATOR,
                    CONFIGURE_MAIL_SERVER_LINK_TEXT);
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testSendBulkMailMailServerWarningAsSysAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            navigation.gotoAdminSection(Navigation.AdminSection.SEND_EMAIL);

            assertions.getLinkAssertions().assertLinkPresentWithExactText(CONFIGURE_MAIL_SERVER_LINK_CONTAINER_LOCATOR,
                    CONFIGURE_MAIL_SERVER_LINK_TEXT);
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testMailQueueMailServerWarningAsAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.gotoAdminSection(Navigation.AdminSection.MAIL_QUEUE);

            assertions.getLinkAssertions().assertLinkNotPresentWithExactText
                    ("//*[@class='jiraformheader']//*[@class='warning']/../..", SETUP_MAIL_SERVER_WARNING_LINK_TEXT);
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testMailQueueMailServerWarningAsSysAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            navigation.gotoAdminSection(Navigation.AdminSection.MAIL_QUEUE);

            assertions.getLinkAssertions().assertLinkPresentWithExactText
                    ("//*[@class='jiraformheader']//*[@class='warning']/../..", SETUP_MAIL_SERVER_WARNING_LINK_TEXT);
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testDeleteProjectBackupWarningAsAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.gotoAdminSection(Navigation.AdminSection.VIEW_PROJECTS);
            tester.clickLinkWithText("Delete");

            tester.assertLinkNotPresentWithText("back it up first");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testDeleteProjectBackupWarningAsSysAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            navigation.gotoAdminSection(Navigation.AdminSection.VIEW_PROJECTS);
            tester.clickLinkWithText("Delete");

            tester.assertLinkNotPresentWithText("contact a System Administrator");
            tester.assertLinkPresentWithText("create a back up");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testActivateWorkflowBackupWarningAsAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            createNewWorkflowSchemeAndGotoAssociateProject();

            assertions.getLinkAssertions().assertLinkNotPresentWithExactText("", "backup");

            // Now test the second screen
            tester.selectOption("schemeId", "Test");
            tester.submit("Associate");
            tester.assertTextPresent("2 of 3");
            assertions.getLinkAssertions().assertLinkNotPresentWithExactText("", "backup");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testActivateWorkflowBackupWarningAsSysAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            createNewWorkflowSchemeAndGotoAssociateProject();

            tester.assertLinkNotPresentWithText("contact a System Administrator");
            tester.assertLinkPresentWithText("backup");

            // Now test the second screen
            tester.selectOption("schemeId", "Test");
            tester.submit("Associate");
            tester.assertTextPresent("2 of 3");
            // TODO Not sure why this is no longer present.
            // tester.assertLinkPresentWithText("backup");
            tester.assertLinkNotPresentWithText("contact a System Administrator");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testDeleteWorkflowBackupWarningAsAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            administration.workflows().goTo().copyWorkflow("jira", "Copy of jira");
            administration.workflows().goTo();
            tester.clickLink("del_Copy of jira");

            tester.assertLinkNotPresentWithText("do a full backup");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testDeleteWorkflowBackupWarningAsSysAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            administration.workflows().goTo().copyWorkflow("jira", "Copy of jira");
            administration.workflows().goTo();
            tester.clickLink("del_Copy of jira");

            tester.assertLinkNotPresentWithText("contact a System Administrator");
            tester.assertLinkPresentWithText("do a full backup");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testViewGroupEditLinkNotPresentAsAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
            tester.clickLinkWithText("jira-sys-admins");

            tester.assertLinkNotPresent("edit_members_of_jira-sys-admins");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testViewGroupEditLinkPresentAsSysAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
            tester.clickLinkWithText("jira-sys-admins");

            tester.assertLinkPresent("edit_members_of_jira-sys-admins");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testUserBrowserEditAndDeleteLinkNotPresentAsAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);

            tester.assertLinkNotPresent("edituser_link_root");
            tester.assertLinkNotPresent("deleteuser_link_root");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testUserBrowserEditAndDeleteLinkPresentAsSysAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);

            tester.assertLinkPresent("edituser_link_root");
            tester.assertLinkPresent("deleteuser_link_root");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testAttachmentEditLinkNotPresentAsAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);

            tester.assertLinkNotPresentWithText("Edit Settings");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testAttachmentEditAsAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            // Fudge the url and make sure we get an error
            tester.gotoPage(page.addXsrfToken("/secure/admin/jira/EditAttachmentSettings.jspa?thumbnailsEnabled=true"));

            tester.assertTextPresent("Attachments must be enabled to enable thumbnails.");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testAttachmentEditLinkPresentAsSysAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);

            tester.assertLinkPresentWithText("Edit Settings");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testAdvancedSettingsWorksForSysAdminWhoIsAlsoAdmin() {
        areAdvancedSettingsWorkingForUser(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
    }

    private void areAdvancedSettingsWorkingForUser(String userName, String userPassword) {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.logout();
            navigation.login(userName, userPassword);

            navigation.gotoAdminSection(Navigation.AdminSection.GENERAL_CONFIGURATION);
            tester.assertLinkPresentWithText("Advanced Settings");

            navigation.gotoAdminSection(Navigation.AdminSection.ADVANCED_SETTINGS);
            tester.assertTextPresent("Advanced Settings");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testAdvancedSettingsWorksForSysAdminWhoIsNotAdmin() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.usersAndGroups().removeUserFromGroup(SYS_ADMIN_USERNAME, JIRA_ADMIN_GROUP);

            navigation.gotoAdminSection(Navigation.AdminSection.GENERAL_CONFIGURATION);
            tester.assertLinkPresentWithText("Advanced Settings");

            navigation.gotoAdminSection(Navigation.AdminSection.ADVANCED_SETTINGS);
            tester.assertTextPresent("Advanced Settings");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testAdvancedSettingsWorksForAdmin() {
        areAdvancedSettingsWorkingForUser(ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void testAdvancedSettingsDoesNotWorkForNormalUser() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            navigation.logout();
            navigation.login(FRED_USERNAME, FRED_PASSWORD);

            navigation.gotoAdminSection(Navigation.AdminSection.GENERAL_CONFIGURATION);
            tester.assertTextPresent("does not have permission to access this page");

            navigation.gotoAdminSection(Navigation.AdminSection.ADVANCED_SETTINGS);
            tester.assertTextPresent("does not have permission to access this page");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testAdminCanNotElevateHisPrivilegesBySettingSysAdminPassword() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            // I am admin right now so I shouldn't be able to set a sys admins password
            tester.gotoPage("/secure/admin/user/ViewUser.jspa?name=root");
            textAssertions.assertTextNotPresent(locator.page(), "Set Password");
            textAssertions.assertTextPresent(locator.page(), "This user is a System Administrator. Your permission to modify the user is restricted because you do not have System Administrator permissions.");

            // as admin I should not be able to set the sys admins password even if I URL hack
            tester.gotoPage("/secure/admin/user/SetPassword!default.jspa?name=root");
            tester.setFormElement("password", "newpassword");
            tester.setFormElement("confirm", "newpassword");
            tester.clickButton("user-edit-password-submit");
            textAssertions.assertTextNotPresent(locator.page(), "has successfully been set ");
            assertions.getJiraFormAssertions().assertFormErrMsg("Must be a System Administrator to reset a System Administrator's password");

            // but I should be able to set mine
            tester.gotoPage("/secure/admin/user/ViewUser.jspa?name=admin");
            textAssertions.assertTextPresent(locator.page(), "Set Password");
            tester.gotoPage("secure/admin/user/SetPassword!default.jspa?name=admin");
            tester.setFormElement("password", "newpassword");
            tester.setFormElement("confirm", "newpassword");
            tester.clickButton("user-edit-password-submit");
            assertions.getJiraFormAssertions().assertFormSuccessMsg("has successfully been set");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testAdminUsersCanAccessTheServicesAdministrationPage() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            administration.services().goTo();
            assertThatTheCurrentPageIsViewServices();
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testSysAdminUsersCanAccessTheServicesAdministrationPage() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            administration.services().goTo();
            assertThatTheCurrentPageIsViewServices();
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testSysAdminUsersCanViewAllServices() throws Exception {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            Set<ViewServices.Service> expectedListOfServices =
                    ImmutableSet.of
                            (
                                    new ViewServices.Service("An IMAP Service", "com.atlassian.jira.service.services.mail.MailFetcherService"),
                                    new ViewServices.Service("A Debugging Service", "com.atlassian.jira.service.services.DebugService"),
                                    new ViewServices.Service("Backup JIRA", "com.atlassian.jira.service.services.export.ExportService"),
                                    new ViewServices.Service("A Pop Service", "com.atlassian.jira.service.services.mail.MailFetcherService"),
                                    new ViewServices.Service("Mail Queue Service", "com.atlassian.jira.service.services.mail.MailQueueService")
                            );

            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            Set<ViewServices.Service> actualListOfServices = administration.services().goTo().list();
            assertTrue(actualListOfServices.containsAll(expectedListOfServices));
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testAdminUsersCanOnlySeePopAndImapServices() throws Exception {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            Set<ViewServices.Service> expectedListOfServices =
                    ImmutableSet.of
                            (
                                    new ViewServices.Service("An IMAP Service", "com.atlassian.jira.service.services.mail.MailFetcherService"),
                                    new ViewServices.Service("A Pop Service", "com.atlassian.jira.service.services.mail.MailFetcherService")
                            );

            Set<ViewServices.Service> actualListOfServices = administration.services().goTo().list();
            assertTrue(expectedListOfServices.equals(actualListOfServices));
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testSysAdminUsersCanAddABuiltInServiceThatIsNeitherAnImapNorPop() throws Exception {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            ViewServices.Service serviceToAdd = new ViewServices.Service("Another Backup JIRA", "com.atlassian.jira.service.services.export.ExportService");

            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            administration.services().goTo().add(serviceToAdd, "10");

            Set<ViewServices.Service> existingServices = administration.services().goTo().list();
            assertTrue(existingServices.contains(serviceToAdd));
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testAdminUsersCanOnlyAddPopAndImapServices() throws Exception {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            administration.services().goTo();

            // JRADEV-8623: admins do not see the form
            tester.assertFormNotPresent(FunctTestConstants.JIRA_FORM_NAME);
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testAdminUsersCanOnlySeePopServers() throws Exception {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            MailServerAdministration.MailServerConfiguration expectedPopConfiguration =
                    new MailServerAdministration.
                            MailServerConfiguration("Dummy POP Server", "dummy", "dummy");

            navigation.gotoAdmin();
            assertFalse("Admin should not be able to see list of smtp servers", administration.mailServers().Smtp().goTo().isPresent());

            assertTrue(administration.mailServers().Pop().goTo().isPresent());

            List<MailServerAdministration.MailServerConfiguration>
                    actualListOfConfiguredPopServers = administration.mailServers().Pop().goTo().list();

            assertEquals(actualListOfConfiguredPopServers, ImmutableList.of(expectedPopConfiguration));
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testSysAdminUsersCanSeeSmtpAndPopServers() throws Exception {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            MailServerAdministration.MailServerConfiguration expectedPopConfiguration =
                    new MailServerAdministration.
                            MailServerConfiguration("Dummy POP Server", "dummy", "dummy");

            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            assertTrue(administration.mailServers().Smtp().goTo().isPresent());
            assertFalse(administration.mailServers().Smtp().goTo().isConfigured());

            assertTrue(administration.mailServers().Pop().goTo().isPresent());

            List<MailServerAdministration.MailServerConfiguration>
                    actualListOfConfiguredPopServers = administration.mailServers().Pop().goTo().list();

            assertEquals(actualListOfConfiguredPopServers, ImmutableList.of(expectedPopConfiguration));
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testAdminUsersCanAddAPopServer() throws Exception {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            MailServerAdministration.MailServerConfiguration mailServerConfigurationToBeAdded =
                    new MailServerAdministration.MailServerConfiguration("New Pop Server", "dummy", "dummy");

            administration.
                    mailServers().Pop().goTo().add
                    (
                            mailServerConfigurationToBeAdded.getName(),
                            mailServerConfigurationToBeAdded.getHostName(),
                            mailServerConfigurationToBeAdded.getUserName(),
                            "dummy"
                    );

            List<MailServerAdministration.MailServerConfiguration>
                    actualListOfConfiguredPopServers = administration.mailServers().Pop().goTo().list();

            assertTrue(actualListOfConfiguredPopServers.contains(mailServerConfigurationToBeAdded));
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testAdminUsersCanEditAPopServer() throws Exception {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            final MailServerAdministration.MailServerConfiguration expectedUpdatedMailServerConfiguration =
                    new MailServerAdministration.MailServerConfiguration("Dummy POP Server", "PIRATES.AYE.AYE.COM", "Barbarossa");

            administration.mailServers().Pop().goTo().
                    edit("Dummy POP Server").
                    setHostName("PIRATES.AYE.AYE.COM").
                    setUserName("Barbarossa").
                    setPassword("secure-password").
                    update();

            final List<MailServerAdministration.MailServerConfiguration>
                    actualListOfConfiguredPopServers = administration.mailServers().Pop().goTo().list();

            assertTrue(Iterables.contains(actualListOfConfiguredPopServers, expectedUpdatedMailServerConfiguration));
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testAdminUsersCanDeleteAPopServer() throws Exception {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            administration.mailServers().Pop().goTo().delete("Dummy POP Server");

            final List<MailServerAdministration.MailServerConfiguration>
                    actualListOfConfiguredPopServers = administration.mailServers().Pop().goTo().list();

            // There is only one pop server in the data so the list of pop servers should be empty now.
            assertTrue(Iterables.isEmpty(actualListOfConfiguredPopServers));
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testAdminCanNotAccessSendTestMailByUrlHacking() throws Exception {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            tester.gotoPage("secure/admin/SendTestMail!default.jspa");
            textAssertions.assertTextPresent(locator.css(".aui-message.warning"), "'Administrator' does not have permission to access this page");
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testImportWorkflowFromXmlButtonShouldNotBeDisplayedForAdministrators() throws Exception {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");

            assertFalse(administration.workflows().goTo().isImportWorkflowFromXmlButtonPresent());
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testImportWorkflowFromXmlButtonShouldBeDisplayedForSystemAdministrators() throws Exception {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            assertTrue(administration.workflows().goTo().isImportWorkflowFromXmlButtonPresent());
        } finally {
            navigation.logout();
            // go back to sysadmin user
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            administration.restoreBlankInstance();
        }
    }

    @Test
    public void testThatAdminCanNotLoginWithoutAppRole() {
        try {
            administration.restoreData("TestWithSystemAdmin.xml");
            navigation.logout();
            navigation.login(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);

            navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
            tester.assertLinkPresent("edituser_link_root");
            tester.assertLinkPresent("deleteuser_link_root");

            backdoor.usersAndGroups().addGroup("no-users");
            backdoor.usersAndGroups().addGroup("single-admin");
            backdoor.usersAndGroups().addUser("newadmin");
            backdoor.usersAndGroups().addUserToGroup("newadmin", "single-admin");
            backdoor.usersAndGroups().addUserToGroup("newadmin", "jira-sys-admins");
            backdoor.applicationRoles().putRoleWithDefaults("jira-software", newHashSet("no-users", "single-admin"), Sets.<String>newHashSet());

            navigation.logout();
            navigation.loginAttempt(SYS_ADMIN_USERNAME, SYS_ADMIN_PASSWORD);
            tester.assertTextNotPresent("Sorry, your username and password are incorrect - please try again.");
        } finally {
            navigation.logout();
            navigation.login("newadmin", "newadmin");
            administration.restoreBlankInstance();
        }
    }

    /**
     * Move admin from Core to Software role.
     */
    @Test
    public void testAdminCanLoginWhenMovedToAnotherRole() {
        try {
            //Given
            backdoor.restoreBlankInstance(LicenseKeys.MULTI_ROLE);
            navigation.logout();
            //Admin has CORE role and should be able to login
            navigation.login(ADMIN_USERNAME, ADMIN_USERNAME);

            final String noUsers = "no-users";
            backdoor.usersAndGroups().addGroup(noUsers);

            assertTrue(backdoor.applicationRoles().getRole("jira-core").getGroups().contains(JIRA_ADMIN_GROUP));
            assertTrue(backdoor.applicationRoles().getRole("jira-software").getGroups().contains(JIRA_ADMIN_GROUP));
            assertFalse(backdoor.applicationRoles().getRole("jira-servicedesk").getGroups().contains(JIRA_ADMIN_GROUP));

            //when -- admin groups moves over to another application.
            backdoor.applicationRoles().putRole("jira-servicedesk", JIRA_ADMIN_GROUP);
            backdoor.applicationRoles().putRole("jira-core", noUsers);
            backdoor.applicationRoles().putRole("jira-software", noUsers);

            //then -- make sure the admin can still login.
            navigation.logout();
            navigation.login(ADMIN_USERNAME, ADMIN_USERNAME);
        } finally {
            navigation.logout();
            navigation.login(ADMIN_USERNAME, ADMIN_USERNAME);
            administration.restoreBlankInstance();
        }
    }

    private void assertThatTheCurrentPageIsViewServices() {
        textAssertions.assertTextSequence(locator.css(VIEW_SERVICES_PAGE_FORM_TITLES_LOCATOR), "Services", "Add Service");
    }

    private void createNewWorkflowSchemeAndGotoAssociateProject() {
        navigation.gotoAdmin();
        administration.workflows().goTo().copyWorkflow("jira", "Copy of jira");
        backdoor.workflowSchemes().createScheme(new WorkflowSchemeData().setName("Test").setDefaultWorkflow("Copy of jira"));

        // Goto activate the workflow
        tester.gotoPage("/secure/project/SelectProjectWorkflowScheme!default.jspa?projectId=10000");
    }
}
