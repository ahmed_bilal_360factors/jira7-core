package com.atlassian.jira.webtests.ztests.subtask;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperationProgress;
import com.atlassian.jira.functest.framework.BulkOperations;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.ProjectClient;
import com.atlassian.jira.util.ProgressPageControl;
import com.meterware.httpunit.WebTable;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Performs functional tests on Security Level field of subtasks.
 *
 * @since v3.13
 */
@WebTest({Category.FUNC_TEST, Category.SECURITY, Category.SUB_TASKS})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestSecurityLevelOfSubtasks.xml")
public class TestSecurityLevelOfSubtasks extends BaseJiraFuncTest {

    @Inject
    private BulkOperations bulkOperations;

    @Inject
    private BulkOperationProgress bulkOperationProgress;

    @Inject
    private HtmlPage page;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {

        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
    }

    @Test
    public void testBulkEditWithSubtasksOnly() {
        // Go to issue navigator.
        showAllIssues();
        // Choose Bulk Operation
        bulkOperations.bulkChangeIncludeAllPages();

        // Choose to operate on two subtasks - RAT-7 and RAT-9
        tester.checkCheckbox("bulkedit_10051", "on");
        tester.checkCheckbox("bulkedit_10032", "on");
        tester.submit("Next");

        // Choose "Bulk Edit"
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), new String[]{
                "Step 2 of 4: Choose Operation",
                "Choose the operation you wish to perform on the selected 2 issue(s)."
        });
        tester.checkCheckbox("operation", "bulk.edit.operation.name");
        tester.submit("Next");

        // Operation Details
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), new String[]{
                "Step 3 of 4: Operation Details",
                "Choose the bulk action(s) you wish to perform on the selected 2 issue(s)."
        });
        // Check the table with editable fields:
        WebTable webTable = getWebTableWithID("availableActionsTable");
        assertions.getTableAssertions().assertTableCellHasText(webTable, 1, 1, "Change Security Level");
        // Assert we cannot set security level: read-only message.
        assertEquals("The security level of subtasks is inherited from parents.", webTable.getCellAsText(1, 2).trim());
        // Change the priority
        tester.checkCheckbox("actions", "priority");
        tester.selectOption("priority", "Minor");
        // Change the assignee
        tester.checkCheckbox("actions", "assignee");
        tester.selectOption("assignee", "Henry Ford");
        tester.submit("Next");

        // Confirmation Screen
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), new String[]{
                "Step 4 of 4: Confirmation",
                "Updated Fields",
                "Priority",
                "Minor",
                "Assignee",
                "Henry Ford",
                "The above table summarises the changes you are about to make"
        });
        // Check the issue table
        webTable = getWebTableWithID("issuetable");
        // Issues appear to be returned in a random order - so find the correct order.
        int rat9Row;
        int rat7Row;
        if (webTable.getCellAsText(1, 1).trim().equals("RAT-9")) {
            rat9Row = 1;
            rat7Row = 2;
        } else {
            rat9Row = 2;
            rat7Row = 1;
        }
        assertEquals("RAT-9", webTable.getCellAsText(rat9Row, 1).trim());
        // Show the old value for assignee
        assertEquals("Mark", webTable.getCellAsText(rat9Row, 3).trim());
        assertEquals("Level KingRat", webTable.getCellAsText(rat9Row, 11).trim());
        assertEquals("RAT-7", webTable.getCellAsText(rat7Row, 1).trim());
        assertEquals("Henry Ford", webTable.getCellAsText(rat7Row, 3).trim());
        assertEquals("Level Mouse", webTable.getCellAsText(rat7Row, 11).trim());
        tester.submit("Confirm");
        bulkOperationProgress.waitAndReloadBulkOperationProgressPage();

        // Back to navigator
        // Check the issue table
        webTable = getWebTableWithID("issuetable");
        assertEquals("RAT-9", webTable.getCellAsText(1, 1).trim());
        // New Assignee
        assertEquals("Henry Ford", webTable.getCellAsText(1, 3).trim());
        assertEquals("Level KingRat", webTable.getCellAsText(1, 11).trim());
        assertEquals("RAT-7", webTable.getCellAsText(3, 1).trim());
        // New Assignee
        assertEquals("Henry Ford", webTable.getCellAsText(3, 3).trim());
        assertEquals("Level Mouse", webTable.getCellAsText(3, 11).trim());
    }

    /**
     * This test will bulk edit a "standard" issue as well as a subtask.
     * The parent of the subtask is not included in the bulk edit.
     */
    @Test
    public void testBulkEditWithStandardIssueAndSubTask() {
        // Go to issue navigator
        showAllIssues();

        // Do Bulk Operation
        bulkOperations.bulkChangeIncludeAllPages();

        // Select Issues
        tester.checkCheckbox("bulkedit_10032", "on");
        tester.checkCheckbox("bulkedit_10050", "on");
        tester.submit("Next");

        // Choose "Bulk Edit" operation.
        textAssertions.assertTextSequence(page.getCollapsedResponseText(), new String[]{
                "Step 2 of 4: Choose Operation",
                "Choose the operation you wish to perform on the selected 2 issue(s)."
        });
        tester.checkCheckbox("operation", "bulk.edit.operation.name");
        tester.submit("Next");

        // On the Edit field screen
        tester.assertTextPresent("Change Security Level");
        // We should now have an editable drop-down for security level. This should affect the standard Issue and its
        // subtasks, but be ignored by the "orphan" subtask.
        tester.checkCheckbox("actions", "security");
        tester.selectOption("security", "None");
        tester.submit("Next");

        // Confirmation Screen.
        tester.assertTextPresent("Step 4 of 4: Confirmation");
        tester.assertTextPresent("Updated Fields");
        tester.assertTextPresent("None");
        tester.assertTextPresent("The above table summarises the changes you are about to make to the following <strong>2</strong> issues. Do you wish to continue?");
        tester.submit("Confirm");
        bulkOperationProgress.waitAndReloadBulkOperationProgressPage();

        // navigator      
        // Re-assert the Rat Issues
        // RAT-8 and its subtask will be changed to No security level.
        // Rat-7 which was selected in the Bulk Edit operation, should nevertheless still have a security level of
        // "Mouse", agreeing with its parent.
        WebTable issueTable = getWebTableWithID("issuetable");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 1, "RAT-9");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 2, "RAT-8");
        assertEquals("", issueTable.getCellAsText(1, 11).trim());
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 1, "RAT-8");
        assertEquals("", issueTable.getCellAsText(2, 11).trim());
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 1, "RAT-7");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 2, "RAT-5");
        assertEquals("Level Mouse", issueTable.getCellAsText(3, 11).trim());
    }

    @Test
    public void testEditParentIssue() {
        // Go to issue navigator
        showAllIssues();
        // go to issue RAT-8
        tester.clickLinkWithText("RAT-8");
        tester.assertTextPresent("A top level task.");
        // Edit the issue
        tester.clickLink("edit-issue");
        tester.assertTextPresent("Edit Issue");

        // Set security to none
        tester.selectOption("security", "None");
        tester.submit("Update");

        // View navigator
        navigation.issue().returnToSearch();
        // RAT-8 and its subtask will be changed to No security level.
        WebTable issueTable = getWebTableWithID("issuetable");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 1, "RAT-9");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 2, "RAT-8");
        assertEquals("", issueTable.getCellAsText(1, 11).trim());
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 1, "RAT-8");
        assertEquals("", issueTable.getCellAsText(2, 11).trim());

        // -- Now move it to level mouse. ----------------------------
        // go to issue RAT-8
        tester.clickLinkWithText("RAT-8");
        tester.assertTextPresent("A top level task.");
        // Edit the issue
        tester.clickLink("edit-issue");
        tester.assertTextPresent("Edit Issue");

        // Set security to Level KingRat
        tester.selectOption("security", "Level KingRat");
        tester.submit("Update");

        // View navigator
        navigation.issue().returnToSearch();
        // RAT-8 and its subtask will be changed to KingRat security level.
        issueTable = getWebTableWithID("issuetable");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 1, "RAT-9");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 2, "RAT-8");
        assertEquals("Level KingRat", issueTable.getCellAsText(1, 11).trim());
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 1, "RAT-8");
        assertEquals("Level KingRat", issueTable.getCellAsText(2, 11).trim());
    }

    @Test
    public void testWorkflowTransitionParentIssue() {
        // Go to issue navigator
        showAllIssues();
        // Go to RAT-8
        tester.clickLinkWithText("RAT-8");
        // Workflow transition "Resolve Issue"
        tester.clickLink("action_id_5");
        tester.setWorkingForm("issue-workflow-transition");
        tester.assertTextPresent("Resolve Issue");
        tester.assertTextPresent("Resolving an issue indicates that the developers are satisfied the issue is finished.");
        // Set Security level field to "none" as part of the workflow.
        tester.selectOption("security", "None");
        tester.submit("Transition");

        // We are on the issue view screen. Because the security level is none, the field is not even shown:
        tester.assertTextNotPresent("Security Level");

        // return to navigator
        navigation.issue().returnToSearch();
        // Check security level of parent and subtask:
        WebTable issueTable = getWebTableWithID("issuetable");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 1, "RAT-9");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 2, "RAT-8");
        assertEquals("", issueTable.getCellAsText(1, 11).trim());
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 1, "RAT-8");
        assertEquals("", issueTable.getCellAsText(2, 11).trim());
    }

    @Test
    public void testWorkflowTransitionSubtask() {
        // Go to issue navigator
        showAllIssues();
        // Go to RAT-9
        tester.clickLinkWithText("RAT-9");
        // assert that the security level is KingRat
        tester.assertTextPresent("Security Level:");
        tester.assertTextPresent("Level KingRat");
        // Workflow transition "Resolve Issue"
        tester.clickLink("action_id_5");
        tester.setWorkingForm("issue-workflow-transition");
        tester.assertTextPresent("Resolve Issue");
        tester.assertTextPresent("Resolving an issue indicates that the developers are satisfied the issue is finished.");
        // Security Level field is inherited from parents for subtasks, so check that we have a read-only message.
        tester.assertFormElementNotPresent("security");
        tester.assertTextPresent("The security level of subtasks is inherited from parents.");
        tester.submit("Transition");

        // We are on the issue view screen. Because the security level is none, the field is not even shown:
        // assert that the security level is still KingRat
        tester.assertTextPresent("Security Level:");
        tester.assertTextPresent("Level KingRat");

        // return to navigator
        navigation.issue().returnToSearch();
        // Check security level of parent and subtask:
        WebTable issueTable = getWebTableWithID("issuetable");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 1, "RAT-9");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 2, "RAT-8");
        assertEquals("Level KingRat", issueTable.getCellAsText(1, 11).trim());
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 1, "RAT-8");
        assertEquals("Level KingRat", issueTable.getCellAsText(2, 11).trim());
    }

    @Test
    public void testChangeProjectsSecurityLevelScheme() {
        // Assert Precondition.
        assertPrecondition();

        Long projectId = backdoor.project().getProjectId("RAT");
        Long schemeId = backdoor.project().getSchemes(projectId).issueSecurityScheme.id;
        tester.gotoPage("/secure/project/SelectProjectIssueSecurityScheme!default.jspa?projectId=" + projectId + "&schemeId=" + schemeId);

        // Change Security Scheme to DogSecurityScheme
        tester.selectOption("newSchemeId", "DogSecurityScheme");
        tester.submit("Next >>");
        tester.assertTextPresent("Associate Issue Security Scheme to Project");
        // Change Kingrat to red
        tester.selectOption("level_10025", "Level Red");
        // Change mouse to green
        tester.selectOption("level_10026", "Level Green");
        tester.submit("Associate");
        ProgressPageControl.waitAndReload(tester, "assignissuesecurityprogressform", "Refresh", "Acknowledge");

        assertThat(new ProjectClient(environmentData).get("RAT").name, equalTo("Rattus"));

        // go to navigator
        navigation.issueNavigator().displayAllIssues();
        // Assert the security levels have moved as expected.
        final WebTable issueTable = tester.getDialog().getWebTableBySummaryOrId("issuetable");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 1, "RAT-9");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 2, "RAT-8");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 11, "Level Red");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 1, "RAT-8");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 11, "Level Red");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 1, "RAT-7");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 2, "RAT-5");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 11, "Level Green");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 1, "RAT-6");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 2, "RAT-5");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 11, "Level Green");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 1, "RAT-5");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 11, "Level Green");
    }

    private void assertPrecondition() {
        showAllIssues();
    }

    private void showAllIssues() {
        // Go to issue navigator
        navigation.issueNavigator().displayAllIssues();

        // Assert Issue Set up
        // Rat Issues
        final WebTable issueTable = tester.getDialog().getWebTableBySummaryOrId("issuetable");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 1, "RAT-9");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 2, "RAT-8");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 1, 11, "Level KingRat");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 1, "RAT-8");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 2, 11, "Level KingRat");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 1, "RAT-7");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 2, "RAT-5");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 3, 11, "Level Mouse");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 1, "RAT-6");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 2, "RAT-5");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 4, 11, "Level Mouse");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 1, "RAT-5");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 5, 11, "Level Mouse");

        // Cow Issues
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 1, "COW-37");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 2, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 6, 2, "Lets get a third milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 11, "MyFriendsOnly");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 1, "COW-36");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 2, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 2, "Get another milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 7, 11, "MyFriendsOnly");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 8, 1, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 8, 2, "No more milk");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 8, 11, "MyFriendsOnly");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 9, 1, "COW-34");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 9, 2, "COW-35");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 9, 2, "Get new milk bucket");
        assertions.getTableAssertions().assertTableCellHasText(issueTable, 9, 11, "MyFriendsOnly");
    }

    private WebTable getWebTableWithID(final String id) {
        return tester.getDialog().getWebTableBySummaryOrId(id);
    }
}
