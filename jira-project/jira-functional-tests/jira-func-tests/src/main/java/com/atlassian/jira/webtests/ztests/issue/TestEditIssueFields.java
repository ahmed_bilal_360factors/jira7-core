package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Test system & custom fields on the edit issue page.
 *
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.ISSUES})
@Restore("TestEditIssueVersion.xml")
public class TestEditIssueFields extends BaseJiraFuncTest {

    @Inject
    Assertions assertions;

    @Before
    public void setUpTest() {
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    public void testNoneDisplayedGivenNoAvailableVersions() {
        navigation.issue().gotoEditIssue("MK-1");
        assertions.assertNodeHasText(versionPickerLocatorFor("versions"), "None");
        assertions.assertNodeDoesNotHaveText(versionPickerLocatorFor("versions"), "Unknown");
        assertions.assertNodeHasText(versionPickerLocatorFor("fixVersions"), "None");
        assertions.assertNodeDoesNotHaveText(versionPickerLocatorFor("fixVersions"), "Unknown");
    }

    @Test
    public void testNoneDisplayedGivenNoAvailableComponents() {
        navigation.issue().gotoEditIssue("MK-1");
        assertions.assertNodeHasText(componentPickerLocator(), "None");
        assertions.assertNodeDoesNotHaveText(componentPickerLocator(), "Unknown");
    }

    private String versionPickerLocatorFor(final String versionPickerFieldId) {
        return String.format("//div[contains(@class, 'field-group') and contains(@class, 'aui-field-versionspicker')]/label[@for='%s']/..", versionPickerFieldId);
    }

    private String componentPickerLocator() {
        return "//div[contains(@class, 'field-group') and contains(@class, 'aui-field-componentspicker')]";
    }
}
