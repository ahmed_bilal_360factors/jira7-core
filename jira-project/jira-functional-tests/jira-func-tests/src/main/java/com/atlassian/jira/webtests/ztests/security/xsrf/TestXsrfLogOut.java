package com.atlassian.jira.webtests.ztests.security.xsrf;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import net.sourceforge.jwebunit.WebTester;
import org.junit.Test;

import javax.inject.Inject;
import java.net.URL;
import java.util.regex.Pattern;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Responsible for verifying XSRF protection of "log-out" functionality. Takes into account remember-me cookies and the
 * different entry points for logging out of jira (Seraph, direct invocation of the Logout Action command methods).
 *
 * @since v4.1.1
 */
@WebTest({Category.FUNC_TEST, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestXsrfLogOut extends BaseJiraFuncTest {
    private static final String LOG_OUT_LINK_ID = "log_out";

    @Inject
    private Form form;

    @Inject
    private HtmlPage page;

    @Test
    public void testXsrfLogoutFromSeraph() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck("Log Out XSRF Protection Test - Seraph", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoDashboard();
                    }
                }, new XsrfCheck.LinkWithIdSubmission(LOG_OUT_LINK_ID) {
                    @Override
                    public String getLink() throws Exception {
                        // HACK: Remove the context path from the link obtained from the super-class.
                        // This is necessary because the goToPage() method on WebTester already appends this
                        // and if we don't remove it the link will contain the context path twice.
                        final String baseLink = super.getLink();
                        final String finalLink = baseLink.replaceFirst(getEnvironmentData().getContext(), "");
                        return finalLink;
                    }
                })
        ).run(tester, navigation, form, "Confirm logout");
    }

    @Test
    public void testXsrfLogoutFromSeraphWithAnExpiredSession() throws Exception {
        navigation.gotoDashboard();

        // We clear the browser's cookies to simulate an expired session / token
        tester.getDialog().getWebClient().clearCookies();

        tester.clickLink(LOG_OUT_LINK_ID);
        tester.getDialog().getResponse();
        tester.assertTextPresent("You have already been logged out of JIRA");
    }

    @Test
    public void testXsrfLogOutFromSeraphConfirmsLogOutWhenRememberMeIsOn() throws Exception {
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD, true);

        new XsrfTestSuite(
                new XsrfCheck("Log Out XSRF Protection Test - Seraph", new XsrfCheck.Setup() {
                    public void setup() {
                        // We are relying on the fact that the test infrastructure tries to submit a dodgy token first.
                        // Therefore, there is no need to log you back in here.
                        navigation.gotoDashboard();
                    }
                }, new XsrfCheck.LinkWithIdSubmission(LOG_OUT_LINK_ID) {
                    @Override
                    public String getLink() throws Exception {
                        // HACK: Remove the context path from the link obtained from the super-class.
                        // This is necessary because the goToPage() method on WebTester already appends this
                        // and if we don't remove it the link will contain the context path twice.
                        final String baseLink = super.getLink();
                        final String finalLink = baseLink.replaceFirst(getEnvironmentData().getContext(), "");
                        return finalLink;
                    }
                })
        ).run(tester, navigation, form, "Confirm logout");
    }

    @Test
    public void testXsrfLogoutFromActionViaDefaultCommand() throws Exception {
        final String tokenValue = page.getXsrfToken();

        new XsrfTestSuite(
                new XsrfCheck("Log Out XSRF Protection Test - Default Action", new XsrfCheck.Setup.None()
                        , new UrlSubmission(new URL(getEnvironmentData().getBaseUrl() + "/Logout!default.jspa?" + XsrfCheck.ATL_TOKEN + "=" + tokenValue)))
        ).run(tester, navigation, form, "Confirm logout");
    }

    @Test
    public void testXsrfLogoutFromActionViaDefaultCommandWithAnExpiredSession() throws Exception {
        navigation.gotoDashboard();

        // We clear the browser's cookies to simulate an expired session / token
        tester.getDialog().getWebClient().clearCookies();

        final String tokenValue = page.getXsrfToken();

        tester.gotoPage("/Logout!default.jspa?" + XsrfCheck.ATL_TOKEN + "=" + tokenValue);
        tester.getDialog().getResponse();
        tester.assertTextPresent("You have already been logged out of JIRA");
    }

    @Test
    public void testXsrfLogoutFromActionViaDefaultCommandWhenRememberMeIsOn() throws Exception {
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD, true);

        final String tokenValue = page.getXsrfToken();

        new XsrfTestSuite(
                new XsrfCheck("Log Out XSRF Protection Test - Default Action",
                        // We are relying on the fact that the test infrastructure tries to submit a dodgy token first.
                        // Therefore, there is no need to log you back in here.
                        new XsrfCheck.Setup.None()
                        , new UrlSubmission(new URL(getEnvironmentData().getBaseUrl() + "/Logout!default.jspa?" + XsrfCheck.ATL_TOKEN + "=" + tokenValue)))
        ).run(tester, navigation, form, "Confirm logout");
    }

    @Test
    public void testXsrfLogoutFromActionViaExecuteCommand() throws Exception {
        final String tokenValue = page.getXsrfToken();

        new XsrfTestSuite(
                new XsrfCheck("Log Out XSRF Protection Test - Action", new XsrfCheck.Setup.None()
                        , new UrlSubmission(new URL(getEnvironmentData().getBaseUrl() + "/Logout.jspa?" + XsrfCheck.ATL_TOKEN + "=" + tokenValue)))
        ).run(tester, navigation, form, "Confirm logout");
    }

    @Test
    public void testXsrfLogoutFromActionViaExecuteCommandWithAnExpiredSession() throws Exception {
        navigation.gotoDashboard();

        // We clear the browser's cookies to simulate an expired session / token
        tester.getDialog().getWebClient().clearCookies();

        final String tokenValue = page.getXsrfToken();

        tester.gotoPage("/Logout.jspa?" + XsrfCheck.ATL_TOKEN + "=" + tokenValue);
        tester.getDialog().getResponse();
        tester.assertTextPresent("You have already been logged out of JIRA");
    }

    @Test
    public void testXsrfLogoutFromActionViaExecuteCommandWhenRememberMeIsOn() throws Exception {
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD, true);

        final String tokenValue = page.getXsrfToken();

        new XsrfTestSuite(
                new XsrfCheck("Log Out XSRF Protection Test - Action",
                        // We are relying on the fact that the test infrastructure tries to submit a dodgy token first.
                        // Therefore, there is no need to log you back in here.
                        new XsrfCheck.Setup.None()
                        , new UrlSubmission(new URL(getEnvironmentData().getBaseUrl() + "/Logout.jspa?" + XsrfCheck.ATL_TOKEN + "=" + tokenValue)))
        ).run(tester, navigation, form, "Confirm logout");
    }

    /**
     * <p>Defines a submission based on a caller supplied URL.</p>
     * <p>The URL must contain a XSRF token parameter.</p>
     */
    private final class UrlSubmission implements XsrfCheck.Submission {

        private final URL originalUrl;
        private WebTester tester;
        private URL urlToSubmit;

        /**
         * Creates a new URLSubmission instance.
         *
         * @param originalUrl The original originalUrl containing a valid XSRF token.
         * @throws IllegalArgumentException if the originalUrl does not contain an XSRF token.
         */
        private UrlSubmission(final URL originalUrl) {
            final String queryString = originalUrl.getQuery();
            final String xsrfTokenParameterRegex = ".*?" + XsrfCheck.ATL_TOKEN + "=" + ".*?";

            if (!Pattern.matches(xsrfTokenParameterRegex, queryString)) {
                throw new IllegalArgumentException("The URL must contain a XSRF Token parameter");
            }
            this.originalUrl = originalUrl;
            this.urlToSubmit = originalUrl;
        }

        @Override
        public void init(final WebTester tester, final Navigation navigation, final Form form) {
            this.tester = tester;
        }

        /**
         * Changes the urlToSubmit so that it contains an invalid XSRF token.
         *
         * @throws Exception
         */
        public void removeToken() throws Exception {
            final String invalidTokenInUrlString = XsrfCheck.invalidTokenInUrl(originalUrl.toString());
            urlToSubmit = new URL(invalidTokenInUrlString);
        }

        /**
         * Goes to the page according to the urlToSubmit and resets the urlToSubmit to the originalUrl.
         *
         * @throws Exception
         */
        public void submitRequest() throws Exception {
            tester.gotoPage(urlToSubmit.toString());
            resetUrlToSubmit();
        }

        private void resetUrlToSubmit() {
            urlToSubmit = originalUrl;
        }

    }
}