package com.atlassian.jira.webtests.ztests.servermetrics;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.backdoor.EventClient;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.matchers.RegexMatchers;
import org.hamcrest.Matchers;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.FILTERS})
@LoginAs(user = ADMIN_USERNAME)
public class TestServerMetricsEvents extends BaseJiraFuncTest {

    private static final String CORRELATION_ID_VARIABLE = "jira.request.correlation-id";

    @Test
    public void shouldPublishServerMetricsEvents() {
        final EventClient.EventPoller events = backdoor.events().createPoller();
        navigation.gotoDashboard();

        final Iterable<String> allEvents = events.events();
        assertThat(allEvents,
                hasItem("com.atlassian.jira.servermetrics.RequestMetricsEvent"));
    }

    @Test
    public void correlationIdIsTransferredToClient() {
        tester.gotoPage("/secure/Dashboard.jspa");
        String response = tester.getDialog().getResponseText();

        final String requestCorrelationIdSetup = "WRM._unparsedData\\[\"" + CORRELATION_ID_VARIABLE + "\"]=\"\\\\\"[a-z0-9_]+\\\\\"\";";
        assertThat(response, RegexMatchers.regexMatches(requestCorrelationIdSetup));
    }
}
