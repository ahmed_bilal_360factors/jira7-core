package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.admin.TestAdministerUserLink;
import com.atlassian.jira.webtests.ztests.admin.TestGroupBrowser;
import com.atlassian.jira.webtests.ztests.bulk.TestBulkEditUserGroups;
import com.atlassian.jira.webtests.ztests.bulk.TestEditNestedGroups;
import com.atlassian.jira.webtests.ztests.crowd.embedded.TestConcurrentAttributeUpdates;
import com.atlassian.jira.webtests.ztests.crowd.embedded.TestCrowdServerConfiguration;
import com.atlassian.jira.webtests.ztests.dashboard.reports.TestSingleLevelGroupByReport;
import com.atlassian.jira.webtests.ztests.issue.TestIssueSecurityWithGroupsAndRoles;
import com.atlassian.jira.webtests.ztests.issue.assign.TestAssignToCurrentUserFunction;
import com.atlassian.jira.webtests.ztests.issue.assign.TestAssignUserProgress;
import com.atlassian.jira.webtests.ztests.issue.move.TestPromptUserForSecurityLevelOnMove;
import com.atlassian.jira.webtests.ztests.misc.TestForgotLoginDetails;
import com.atlassian.jira.webtests.ztests.misc.TestXSSInFullName;
import com.atlassian.jira.webtests.ztests.user.TestAddUser;
import com.atlassian.jira.webtests.ztests.user.TestAutoWatches;
import com.atlassian.jira.webtests.ztests.user.TestDeleteGroup;
import com.atlassian.jira.webtests.ztests.user.TestDeleteUserAndPermissions;
import com.atlassian.jira.webtests.ztests.user.TestDeleteUserSharedEntities;
import com.atlassian.jira.webtests.ztests.user.TestEditUserDetails;
import com.atlassian.jira.webtests.ztests.user.TestEditUserGroups;
import com.atlassian.jira.webtests.ztests.user.TestEditUserProjectRoles;
import com.atlassian.jira.webtests.ztests.user.TestGlobalUserPreferences;
import com.atlassian.jira.webtests.ztests.user.TestGroupResourceCountUsers;
import com.atlassian.jira.webtests.ztests.user.TestGroupResourceFunc;
import com.atlassian.jira.webtests.ztests.user.TestGroupSelectorPermissions;
import com.atlassian.jira.webtests.ztests.user.TestNonExistentUsers;
import com.atlassian.jira.webtests.ztests.user.TestProcessUsers;
import com.atlassian.jira.webtests.ztests.user.TestShareUserDefault;
import com.atlassian.jira.webtests.ztests.user.TestUserDefaults;
import com.atlassian.jira.webtests.ztests.user.TestUserFormat;
import com.atlassian.jira.webtests.ztests.user.TestUserGroupPicker;
import com.atlassian.jira.webtests.ztests.user.TestUserHistory;
import com.atlassian.jira.webtests.ztests.user.TestUserHover;
import com.atlassian.jira.webtests.ztests.user.TestUserManagement;
import com.atlassian.jira.webtests.ztests.user.TestUserNameIsEncoded;
import com.atlassian.jira.webtests.ztests.user.TestUserNavigationBarWebFragment;
import com.atlassian.jira.webtests.ztests.user.TestUserProfile;
import com.atlassian.jira.webtests.ztests.user.TestUserProperties;
import com.atlassian.jira.webtests.ztests.user.TestUserRememberMeCookies;
import com.atlassian.jira.webtests.ztests.user.TestUserResolver;
import com.atlassian.jira.webtests.ztests.user.TestUserSessions;
import com.atlassian.jira.webtests.ztests.user.TestUserVotes;
import com.atlassian.jira.webtests.ztests.user.TestUserWatches;
import com.atlassian.jira.webtests.ztests.user.TestViewGroup;
import com.atlassian.jira.webtests.ztests.user.rename.TestUserRenameOnChangeHistory;
import com.atlassian.jira.webtests.ztests.user.rename.TestUserRenameOnComments;
import com.atlassian.jira.webtests.ztests.user.rename.TestUserRenameOnComponent;
import com.atlassian.jira.webtests.ztests.user.rename.TestUserRenameOnDashboards;
import com.atlassian.jira.webtests.ztests.user.rename.TestUserRenameOnFilters;
import com.atlassian.jira.webtests.ztests.user.rename.TestUserRenameOnIssuePrintableView;
import com.atlassian.jira.webtests.ztests.user.rename.TestUserRenameOnIssueXmlView;
import com.atlassian.jira.webtests.ztests.user.rename.TestUserRenameOnIssues;
import com.atlassian.jira.webtests.ztests.user.rename.TestUserRenameOnPermissions;
import com.atlassian.jira.webtests.ztests.user.rename.TestUserRenameOnProfiles;
import com.atlassian.jira.webtests.ztests.user.rename.TestUserRenameOnProject;
import com.atlassian.jira.webtests.ztests.user.rename.TestUserRenameOnProjectRoles;
import com.atlassian.jira.webtests.ztests.user.rename.TestUserRenameOnSearch;
import com.atlassian.jira.webtests.ztests.user.rename.TestUserRenameOnWorkflow;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of test related to Users and Groups
 *
 * @since v4.0
 */
public class FuncTestSuiteUsersAndGroups {

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestUserNavigationBarWebFragment.class)
                .add(TestEditUserProjectRoles.class)
                .add(TestBulkEditUserGroups.class)
                .add(TestUserManagement.class)
                .add(TestUserProfile.class)
                .add(TestUserHover.class)
                .add(TestUserWatches.class)
                .add(TestAutoWatches.class)
                .add(TestUserVotes.class)
                .add(TestAssignUserProgress.class)
                .add(TestAdministerUserLink.class)
                .add(TestDeleteUserSharedEntities.class)
                .add(TestUserGroupPicker.class)
                .add(TestDeleteUserAndPermissions.class)
                .add(TestGlobalUserPreferences.class)
                .add(TestEditUserDetails.class)
                .add(TestPromptUserForSecurityLevelOnMove.class)
                .add(TestAssignToCurrentUserFunction.class)
                .add(TestShareUserDefault.class)
                .add(TestUserFormat.class)
                .add(TestNonExistentUsers.class)
                .add(TestUserRenameOnIssues.class)
                .add(TestUserRenameOnWorkflow.class)
                .add(TestUserRenameOnComments.class)
                .add(TestUserRenameOnChangeHistory.class)
                .add(TestUserRenameOnIssuePrintableView.class)
                .add(TestUserRenameOnIssueXmlView.class)
                .add(TestUserRenameOnPermissions.class)
                .add(TestUserRenameOnProfiles.class)
                .add(TestUserRenameOnProjectRoles.class)
                .add(TestUserRenameOnProject.class)
                .add(TestUserRenameOnDashboards.class)
                .add(TestUserRenameOnFilters.class)
                .add(TestUserRenameOnSearch.class)
                .add(TestUserRenameOnComponent.class)
                .add(TestDeleteGroup.class)
                .add(TestSingleLevelGroupByReport.class)
                .add(TestEditNestedGroups.class)
                .add(TestIssueSecurityWithGroupsAndRoles.class)
                .add(TestGroupSelectorPermissions.class)
                .add(TestXSSInFullName.class)
                .add(TestAddUser.class)
                .add(TestUserProperties.class)
                .add(TestUserNameIsEncoded.class)
                .add(TestEditUserGroups.class)
                .add(TestGroupBrowser.class)
                .add(TestViewGroup.class)
                .add(TestForgotLoginDetails.class)
                .add(TestUserDefaults.class)
                .add(TestUserSessions.class)
                .add(TestUserRememberMeCookies.class)
                .add(TestUserHistory.class)
                .add(TestUserResolver.class)
                .add(TestGroupResourceCountUsers.class)
                .add(TestGroupResourceFunc.class)

                .add(TestConcurrentAttributeUpdates.class)
                .add(TestCrowdServerConfiguration.class)
                .add(TestProcessUsers.class)
                .build();
    }
}
