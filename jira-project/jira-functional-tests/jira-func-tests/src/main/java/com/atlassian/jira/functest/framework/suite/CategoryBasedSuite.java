package com.atlassian.jira.functest.framework.suite;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.atlassian.jira.util.Predicate;
import com.atlassian.jira.webtests.util.ClassLocator;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;

import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Test suite based on categories and narrowed to provided package.
 * @since v7.2
 */
public class CategoryBasedSuite implements WebTestSuite, Supplier<FuncTestSuite> {

    private final String testPackage;
    private final Set<Category> includes;
    private final Set<Category> excludes;

    public CategoryBasedSuite(String testPackage, final Set<Category> includes, final Set<Category> excludes) {
        Preconditions.checkNotNull(testPackage);
        this.testPackage = testPackage;
        this.includes = (includes == null) ? ImmutableSet.of() : includes;
        this.excludes = (excludes == null) ? ImmutableSet.of() : excludes;
    }

    @Override
    public String webTestPackage() {
        return testPackage;
    }

    @Override
    public Set<Category> includes() {
        return includes;
    }

    @Override
    public Set<Category> excludes() {
        return excludes;
    }

    @Override
    public FuncTestSuite get() {
        final FuncTestSuite testSuite = new FuncTestSuite();
        getClassesFromPackages().stream().forEach(testSuite::addTest);
        return testSuite;
    }

    private Set<Class<?>> getClassesFromPackages() {
        final List<String> packages = Arrays.asList(testPackage.split(WebTestSuiteRunner.PACKAGE_SEPARATOR));
        return packages.stream()
                .map(p ->
                        new ClassLocator<>(Object.class)
                                .setAllowInner(true)
                                .setPredicate(new TestPredicate())
                                .setPackage(p)
                                .findClasses())
                .flatMap(List::stream)
                .filter(this::hasCorrectCategoryAnnotation)
                .collect(Collectors.toSet());
    }

    private class TestPredicate implements Predicate<Class<?>> {
        @Override
        public boolean evaluate(Class<?> input) {
            return !(!Modifier.isPublic(input.getModifiers()) || Modifier.isAbstract(input.getModifiers()));
        }
    }

    @VisibleForTesting
    protected boolean hasCorrectCategoryAnnotation(Class<?> clazz) {
        final Set<Category> categories = getDirectCategories(clazz);
        return categories.containsAll(includes) && !categories.stream().filter(excludes::contains).findAny().isPresent();
    }

    private Set<Category> getDirectCategories(Class<?> clazz) {
        final WebTest annotation = clazz.getAnnotation(WebTest.class);
        if (annotation == null || annotation.value().length == 0) {
            return EnumSet.noneOf(Category.class);
        }
        return EnumSet.copyOf(Arrays.asList(annotation.value()));
    }
}
