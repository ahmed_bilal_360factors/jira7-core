package com.atlassian.jira.webtests.ztests.projectconfig;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.BaseTestWorkflowSchemeResource;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.EditDraftWorkflowSchemeResource;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.SimpleIssueType;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.SimpleWorkflow;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.SimpleWorkflowScheme;
import com.atlassian.jira.webtests.ztests.projectconfig.framework.WorkflowSchemeResource;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_BUG;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


/**
 * @since v5.2
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@RestoreOnce("TestProjectWorkflowSchemeResource.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestProjectDraftWorkflowSchemeResource extends BaseTestWorkflowSchemeResource {

    @Test
    public void testEditNoWorkflowName() {
        EditDraftWorkflowSchemeResource client = new EditDraftWorkflowSchemeResource(environmentData);
        Response response = client.editResponse(HSP.getKey(), "", ISSUE_BUG);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);
    }

    @Test
    public void testEditNoIssueTypes() {
        EditDraftWorkflowSchemeResource client = new EditDraftWorkflowSchemeResource(environmentData);
        Response response = client.editResponse(HSP.getKey(), MONKEY_WORKFLOW);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);
    }

    @Test
    public void testEditNoProject() {
        EditDraftWorkflowSchemeResource client = new EditDraftWorkflowSchemeResource(environmentData);
        Response response = client.editResponse("No such project", MONKEY_WORKFLOW, ISSUE_BUG);
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);
    }

    @Test
    public void testEditNoPermissionAnonymous() {
        EditDraftWorkflowSchemeResource client = new EditDraftWorkflowSchemeResource(environmentData);
        Response response = client.anonymous().editResponse(HSP.getKey(), MONKEY_WORKFLOW, ISSUE_BUG);
        assertEquals(UNAUTHORIZED.getStatusCode(), response.statusCode);
    }

    @Test
    public void testEditNoAdminPermission() {
        EditDraftWorkflowSchemeResource client = new EditDraftWorkflowSchemeResource(environmentData);
        Response response = client.loginAs(PROJECT_ADMIN_USERNAME).editResponse(HSP.getKey(), MONKEY_WORKFLOW, ISSUE_BUG);
        assertEquals(FORBIDDEN.getStatusCode(), response.statusCode);
        assertTrue(response.entity.errorMessages.contains("You do not have permission to administer the workflow scheme."));
    }

    @Test
    public void testEditNoWorkflow() {
        EditDraftWorkflowSchemeResource client = new EditDraftWorkflowSchemeResource(environmentData);
        Response response = client.editResponse(HSP.getKey(), "No such WF", ISSUE_BUG);
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);
    }

    @Test
    public void testEditNoIssueType() {
        final String projectKey = "ENIT";
        backdoor.project().addProject("testEditNoIssueType", projectKey, "admin");

        final WorkflowSchemeResource workflowSchemeResource = new WorkflowSchemeResource(environmentData);
        workflowSchemeResource.createDraftWorkflowScheme(projectKey);

        // Single invalid issue type
        EditDraftWorkflowSchemeResource client = new EditDraftWorkflowSchemeResource(environmentData);
        Response response = client.editResponse(projectKey, MONKEY_WORKFLOW, "No such issue type");
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);

        assertTrue(response.entity.errorMessages.contains("The specified issue type does not exist in the project."));

        // One invalid and one valid issue type
        response = client.editResponse(projectKey, MONKEY_WORKFLOW, "No such issue type", ISSUE_BUG);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);
        assertTrue(response.entity.errorMessages.contains("The specified issue type does not exist in the project."));
    }

    @Test
    public void testEditNoDraftScheme() {
        EditDraftWorkflowSchemeResource client = new EditDraftWorkflowSchemeResource(environmentData);
        Response response = client.editResponse(HSP.getKey(), MONKEY_WORKFLOW, ISSUE_BUG);
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);
        assertTrue(response.entity.errorMessages.contains("The workflow scheme does not have a draft."));
    }

    @Test
    public void testEditMakeNewDefault() {
        final SimpleIssueType bug = bug();
        bug.setDefaultIssueType(true);
        final SimpleIssueType improvement = improvement();
        final SimpleIssueType monkey = monkey();
        final SimpleIssueType newFeature = newFeature();
        final SimpleIssueType task = task();
        final SimpleIssueType ignored = ignored();

        EditDraftWorkflowSchemeResource client = new EditDraftWorkflowSchemeResource(environmentData);
        SimpleWorkflowScheme pwdws = client.edit(PROJECT_WITH_DRAFT_WORKFLOW_SCHEME.getKey(), MONKEY_WORKFLOW,
                bug.getId(), newFeature.getId(), task.getId(), improvement.getId(), ignored.getId(), monkey.getId());

        List<SimpleIssueType> issueTypes = asList(bug, ignored, improvement, monkey, newFeature, task);

        assertEquals(issueTypes, pwdws.getIssueTypes());
        assertEquals("monkey workflow scheme 4", pwdws.getName());
        assertNull(pwdws.getDescription());
        assertTrue(pwdws.isAdmin());
        assertFalse(pwdws.isDefaultScheme());
        assertEquals(emptyList(), pwdws.getShared().getSharedWithProjects());
        assertTrue(pwdws.isDraftScheme());
        assertEquals(ADMIN_USER, pwdws.getLastModifiedUser());
        assertEquals(ADMIN_USERNAME, pwdws.getCurrentUser());

        SimpleWorkflow monkeyWorkflow = new SimpleWorkflow(MONKEY_WORKFLOW, null, true, false,
                asList(bug.getId(), ignored.getId(), improvement.getId(), monkey.getId(), newFeature.getId(), task.getId()));
        assertEquals(asList(monkeyWorkflow), pwdws.getMappings());
    }

    @Test
    public void testEditDraftWithMultipleShares() {
        EditDraftWorkflowSchemeResource client = new EditDraftWorkflowSchemeResource(environmentData);
        Response response = client.editResponse(PROJECT_WITH_SHARED_DRAFT_SCHEME.getKey(), DEFAULT_WORKFLOW, ISSUE_BUG);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);
        assertTrue(response.entity.errorMessages.contains("You can only edit a workflow scheme if it is associated uniquely to your project."));
    }

    @Test
    public void testRemoveWorkflowFromDraftScheme() {
        EditDraftWorkflowSchemeResource client = new EditDraftWorkflowSchemeResource(environmentData);
        SimpleWorkflowScheme scheme = client.remove(PROJECT_WITH_WORKFLOW_TO_DELETE.getKey(), DEFAULT_WORKFLOW);

        final SimpleIssueType bug = bug();
        final SimpleIssueType improvement = improvement();
        final SimpleIssueType monkey = monkey();
        final SimpleIssueType newFeature = newFeature();
        final SimpleIssueType task = task();
        final SimpleIssueType ignored = ignored();

        SimpleWorkflow workflow = new SimpleWorkflow(MONKEY_WORKFLOW, null, true, false,
                asList(bug.getId(), ignored.getId(), improvement.getId(), monkey.getId(), newFeature.getId(), task.getId()));

        assertEquals(Collections.singletonList(workflow), scheme.getMappings());
    }

    @Test
    public void testRemoveWorkflowFromDraftSchemeBad() {
        EditDraftWorkflowSchemeResource client = new EditDraftWorkflowSchemeResource(environmentData);

        //Null workflowName
        Response response = client.removeResponse(PROJECT_WITH_WORKFLOW_TO_DELETE.getKey(), "");
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);

        //Anonymous
        response = client.anonymous().removeResponse(PROJECT_WITH_WORKFLOW_TO_DELETE.getKey(), "skdaskdla");
        assertEquals(UNAUTHORIZED.getStatusCode(), response.statusCode);

        //Login as someone with project admin but without gloabl admin.
        response = client.loginAs(FRED_USERNAME).removeResponse(PROJECT_WITH_WORKFLOW_TO_DELETE.getKey(), "skdaskdla");
        assertEquals(FORBIDDEN.getStatusCode(), response.statusCode);

        //Project does not exist.
        response = client.loginAs(ADMIN_USERNAME).removeResponse("Key Which Does not Exist", "sjdskjasd");
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);

        //Workflow does not exist.
        response = client.removeResponse(PROJECT_WITH_WORKFLOW_TO_DELETE.getKey(), "I also Don't Exist");
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);

        //Draft does not exist.
        response = client.removeResponse(MKY.getKey(), DEFAULT_WORKFLOW);
        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);

        //Delete when shared.
        response = client.removeResponse(PROJECT_WITH_SHARED_DRAFT_SCHEME.getKey(), DEFAULT_WORKFLOW);
        assertEquals(BAD_REQUEST.getStatusCode(), response.statusCode);
    }
}
