package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.FuncTestRuleChain;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.w3c.dom.Node;

import javax.inject.Inject;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import static com.atlassian.collectors.CollectorsUtil.toImmutableListWithCapacity;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.matchers.HasItemsInOrderMatcher.hasItemsInOrder;
import static com.atlassian.jira.matchers.HasSubsequenceMatcher.hasSubsequenceOf;
import static org.junit.Assert.assertThat;

/**
 * @since v7.0
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION})
@RestoreBlankInstance
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueFeaturesAdminItemsLayout {
    @Rule
    public TestRule initData = FuncTestRuleChain.forTest(this);
    @Inject
    Navigation navigation;

    @Inject
    LocatorFactory locatorFactory;

    @Test
    public void shouldAddIssueFeaturesToIssuesPage() {
        // when
        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_TYPES);
        final List<String> headersTexts = getAdminMenuHeadings();
        final List<String> allMenuItemsTexts = getAllAdminMenuItems();

        // then
        // sections sequence
        assertThat(headersTexts, hasItemsInOrder("Fields", "Issue Features"));
        // items in correct section
        assertThat(allMenuItemsTexts, hasSubsequenceOf("Issue Features", "Time tracking", "Issue linking"));
    }

    private List<String> getAdminMenuHeadings() {
        final Node[] headers = locatorFactory.css(".admin-menu-links .aui-nav-heading").getNodes();

        return extractNodeValues(headers);
    }

    private List<String> getAllAdminMenuItems() {
        final Node[] allMenuLabels = locatorFactory.css(".admin-menu-links :matchesOwn([A-Za-z])").getNodes();

        return extractNodeValues(allMenuLabels);
    }


    private static List<String> extractNodeValues(final Node[] nodes) {
        final Function<Node, String> trimNodeValue = ((Function<Node, String>) Node::getNodeValue).andThen(String::trim);

        return Stream.of(nodes)
                .map(trimNodeValue)
                .collect(toImmutableListWithCapacity(nodes.length));
    }
}
