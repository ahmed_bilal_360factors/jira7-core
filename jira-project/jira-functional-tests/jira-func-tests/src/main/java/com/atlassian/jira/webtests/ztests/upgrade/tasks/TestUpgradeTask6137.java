package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.UPGRADE_TASKS;

/**
 * Responsible for verifying that references to deleted users that have been left in
 * assignee and reporter fields are assigned a new username in the user key store so
 * that they can be searched for.
 *
 * @since v6.0.6 (as TestUpgradeTask6104)
 */
@WebTest({FUNC_TEST, UPGRADE_TASKS})
@Restore("TestUpgradeTask6137.xml")
public class TestUpgradeTask6137 extends BaseJiraFuncTest {

    @Inject
    private IssueTableAssertions issueTableAssertions;

    @Test
    public void testSearchForDeletedUsers() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        issueTableAssertions.assertSearchWithResults("assignee = assignee", "HSP-1");
        issueTableAssertions.assertSearchWithResults("reporter = reporter", "HSP-1");
        issueTableAssertions.assertSearchWithResults("assignee = newassignee", "HSP-2");
        issueTableAssertions.assertSearchWithResults("reporter = \"newreporter#1\"", "HSP-2");

        // History searches that work
        issueTableAssertions.assertSearchWithResults("assignee was assignee", "HSP-1");
        issueTableAssertions.assertSearchWithResults("reporter was reporter", "HSP-1");

        // Current value searches that are accepted even though the value is never used
        issueTableAssertions.assertSearchWithResults("reporter = newreporter");
        issueTableAssertions.assertSearchWithResults("assignee = changer");
        issueTableAssertions.assertSearchWithResults("assignee = \"changer#1\"");
        issueTableAssertions.assertSearchWithWarning("assignee = \"changer#2\"", "The value 'changer#2' does not exist for the field 'assignee'.");
        issueTableAssertions.assertSearchWithWarning("assignee = \"changer#3\"", "The value 'changer#3' does not exist for the field 'assignee'.");

        // Searches that did not work when this upgrade task was created but were later fixed by JDEV-34987
        issueTableAssertions.assertSearchWithResults("assignee was newassignee", "HSP-2");
        issueTableAssertions.assertSearchWithResults("reporter was reporter", "HSP-1");
        issueTableAssertions.assertSearchWithResults("reporter was \"newreporter#1\"", "HSP-2");

        // Does not match either an existing user or the history, so fails.
        issueTableAssertions.assertSearchWithError("reporter was newreporter", "The value 'newreporter' does not exist for the field 'reporter'.");

        // History searches that probably should work, but currently do not
        issueTableAssertions.assertSearchWithError("assignee changed by changer", "The user 'changer' does not exist and cannot be used in the 'by' predicate.");
    }
}

