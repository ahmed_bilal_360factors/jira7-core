package com.atlassian.jira.functest.framework.admin;

import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Default implementation of {@link ViewFieldScreens}.
 *
 * @since v4.2
 */
public class ViewFieldScreensImpl implements ViewFieldScreens {
    private static final String CONFIGURE_LINK_PREFIX = "configure_fieldscreen_";

    private final Navigation navigation;

    private final ConfigureScreen configureScreen = new ConfigureScreenImpl();
    private final WebTester tester;

    @Inject
    public ViewFieldScreensImpl(final WebTester tester,
                                final JIRAEnvironmentData environmentData,
                                final Navigation navigation) {
        this.tester = tester;
        this.navigation = notNull("navigation", navigation);
    }

    public ViewFieldScreens goTo() {
        navigation.gotoAdminSection(Navigation.AdminSection.FIELD_SCREENS);
        return this;
    }

    @Override
    public ConfigureScreen configureScreen(String screenName) {
        tester.clickLink(CONFIGURE_LINK_PREFIX + screenName);
        return configureScreen;
    }
}
