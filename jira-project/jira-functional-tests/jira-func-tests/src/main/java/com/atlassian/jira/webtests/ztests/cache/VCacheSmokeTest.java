package com.atlassian.jira.webtests.ztests.cache;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.vcache.PutPolicy;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertTrue;

/**
 * @since v7.1
 */
@WebTest({Category.FUNC_TEST, Category.REST})
public class VCacheSmokeTest extends BaseJiraFuncTest {
    private static final String CACHE_NAME = "funcTestCache";

    @Test
    public void canAddAndRetrieveValuesFromExternalDirectCache() {
        final boolean result = backdoor.vCacheControl().putValueToDirectCache(CACHE_NAME, "key1",
                PutPolicy.ADD_ONLY, "someValue");
        final String value = backdoor.vCacheControl().getValueFromDirectCache(CACHE_NAME, "key1");

        assertTrue("value for 'key1' hasn't been added", result);
        assertThat(value, is("someValue"));
    }

    @Test
    public void valuesInRequestCacheAreClearedAfterEachRequest() {
        backdoor.vCacheControl().putValueToRequestCache(CACHE_NAME, "key1", "someValue");
        final String value = backdoor.vCacheControl().getValueFromRequestCache(CACHE_NAME, "key1");

        assertThat(value, Matchers.isEmptyOrNullString());
    }
}
