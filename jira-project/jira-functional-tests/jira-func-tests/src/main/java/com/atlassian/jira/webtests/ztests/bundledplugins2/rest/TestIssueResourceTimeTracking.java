package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.admin.TimeTracking;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the time tracking field.
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestIssueResourceTimeTracking.xml")
public class TestIssueResourceTimeTracking extends BaseJiraFuncTest {
    private IssueClient issueClient;

    @Inject
    private TimeTracking timeTracking;

    @Before
    public void setUpTest() {
        issueClient = new IssueClient(environmentData);
    }

    @Test
    public void testTimeTrackingDisabled() throws Exception {
        timeTracking.disable();

        Issue issue = issueClient.get("FUNC-3");
        assertNull("Time tracking shouldn't be in response when time tracking is disabled", issue.fields.timetracking);
    }

    @Test
    public void testIssueWithNoTimeTracking() throws Exception {
        Issue issue = issueClient.get("FUNC-1");
        assertNull(issue.fields.timetracking.originalEstimate);
        assertNull(issue.fields.timetracking.remainingEstimate);
        assertNull(issue.fields.timetracking.timeSpent);

        assertNotNull(issue.fields.progress);
        assertEquals(Long.valueOf(0), issue.fields.progress.total());
        assertEquals(Long.valueOf(0), issue.fields.progress.progress());
        assertNull(issue.fields.progress.percent());

        assertNotNull(issue.fields.aggregateprogress);
        assertEquals(Long.valueOf(0), issue.fields.aggregateprogress.total());
        assertEquals(Long.valueOf(0), issue.fields.aggregateprogress.progress());
        assertNull(issue.fields.aggregateprogress.percent());
    }

    @Test
    public void testIssueWithOriginalEstimate() throws Exception {
        Issue issue = issueClient.get("FUNC-3");
        assertNotNull(issue.fields.timetracking);
        assertEquals("3d", issue.fields.timetracking.originalEstimate);
    }

    @Test
    public void testIssueWithTimeSpent() throws Exception {
        Issue issue = issueClient.get("FUNC-3");
        assertNotNull(issue.fields.timetracking);
        assertEquals("1d", issue.fields.timetracking.timeSpent);
    }

    @Test
    public void testIssueWithTimeRemaining() throws Exception {
        Issue issue = issueClient.get("FUNC-3");
        assertNotNull(issue.fields.timetracking);
        assertEquals("2d", issue.fields.timetracking.remainingEstimate);
    }

    @Test
    public void testProgressWithSubtasks() throws Exception {
        Issue issue = issueClient.get("FUNC-4");
        assertNotNull(issue.fields.progress);
        assertEquals(Long.valueOf(36000), issue.fields.progress.total());
        assertEquals(Long.valueOf(7200), issue.fields.progress.progress());
        assertEquals(Long.valueOf(20), issue.fields.progress.percent());

        assertNotNull(issue.fields.aggregateprogress);
        assertEquals(Long.valueOf(43200), issue.fields.aggregateprogress.total());
        assertEquals(Long.valueOf(14400), issue.fields.aggregateprogress.progress());
        assertEquals(Long.valueOf(33), issue.fields.aggregateprogress.percent());

        issue = issueClient.get("FUNC-5");
        assertNotNull(issue.fields.progress);
        assertEquals(Long.valueOf(7200), issue.fields.progress.total());
        assertEquals(Long.valueOf(7200), issue.fields.progress.progress());
        assertEquals(Long.valueOf(100), issue.fields.progress.percent());

        assertNotNull(issue.fields.aggregateprogress);
        assertEquals(Long.valueOf(7200), issue.fields.aggregateprogress.total());
        assertEquals(Long.valueOf(7200), issue.fields.aggregateprogress.progress());
        assertEquals(Long.valueOf(100), issue.fields.aggregateprogress.percent());
    }

}
