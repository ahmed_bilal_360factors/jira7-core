package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.ATTACHMENTS, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestXsrfAttachments extends BaseJiraFuncTest {

    @Inject
    private Form form;

    @Test
    public void testAttachmentAdministration() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck("EditAttachmentSettings", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);
                        tester.clickLinkWithText("Edit Settings");
                        tester.checkCheckbox("attachmentPathOption", "DEFAULT");
                    }
                }, new XsrfCheck.FormSubmission("Update"))
        ).run(getTester(), navigation, form);
    }
}
