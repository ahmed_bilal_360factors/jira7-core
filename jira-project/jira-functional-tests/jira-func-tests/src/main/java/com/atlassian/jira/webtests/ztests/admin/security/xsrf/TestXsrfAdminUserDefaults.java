package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestXsrfAdminUserDefaults extends BaseJiraFuncTest {
    @Inject
    private Form form;

    @Test
    public void testAdminUserDefaults() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck("Edit User Defaults", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.USER_DEFAULTS);
                        tester.clickLinkWithText("Edit default values");
                    }
                }, new XsrfCheck.FormSubmission("Update"))
                ,
                new XsrfCheck("Edit User Defaults Apply", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.USER_DEFAULTS);
                        tester.clickLinkWithText("Apply");
                    }
                }, new XsrfCheck.FormSubmission("Update"))

        ).run(tester, navigation, form);
    }

}