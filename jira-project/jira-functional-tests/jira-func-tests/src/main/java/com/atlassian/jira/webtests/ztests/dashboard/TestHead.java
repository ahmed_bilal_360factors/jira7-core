package com.atlassian.jira.webtests.ztests.dashboard;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertTrue;


@WebTest({Category.FUNC_TEST, Category.BROWSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestHead extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreData("blankprojects.xml");
    }

    @Test
    public void testDashboard() throws Exception {
        tester.gotoPage("secure/Dashboard.jspa");
        String pageText = tester.getTestContext().getWebClient().getCurrentPage().getText();
        int uaIndex = pageText.indexOf("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\"/>");
        int scriptIndex = pageText.indexOf("<script");

        boolean condition = uaIndex > -1;
        assertTrue("Could not find X-UA-Compatible meta tag", condition);
        if (scriptIndex > -1)
            assertTrue("At least one script tag appears before the first X-UA-Compatible meta tag", uaIndex < scriptIndex);
    }
}
