package com.atlassian.jira.functest.framework.admin;

import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LocatorFactoryImpl;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.NavigationImpl;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.assertions.TextAssertionsImpl;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.testkit.client.log.FuncTestLoggerImpl;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;
import java.util.Locale;

/**
 * Time tracking configuration
 *
 * @since v4.0
 */
public class TimeTrackingImpl implements TimeTracking {

    private final WebTester tester;
    private final JIRAEnvironmentData environmentData;
    private final FuncTestLogger logger;
    private final TextAssertions textAssertions;
    private final LocatorFactory locators;
    private Navigation navigation;

    @Inject
    public TimeTrackingImpl(final WebTester tester,
                            final JIRAEnvironmentData environmentData) {
        this.tester = tester;
        this.environmentData = environmentData;
        this.logger = new FuncTestLoggerImpl(2);
        this.textAssertions = new TextAssertionsImpl(tester);
        this.locators = new LocatorFactoryImpl(tester);
    }

    public void enable(final Mode mode) {
        logger.log("Activating time tracking");
        tester.gotoPage(TIME_TRACKING_ADMIN_PAGE);

        if (tester.getDialog().hasSubmitButton("Activate")) {
            if (mode.equals(Mode.LEGACY)) {
                tester.checkCheckbox("legacyMode", "true");
            } else {
                tester.uncheckCheckbox("legacyMode");
            }
            tester.submit("Activate");
        } else {
            logger.log("Time tracking already activated.");
        }
    }

    public void switchMode(final Mode mode) {
        disable();
        enable(mode);
    }

    public void enable(final Format format) {
        logger.log("Activating time tracking");
        tester.gotoPage(TIME_TRACKING_ADMIN_PAGE);

        if (tester.getDialog().hasSubmitButton("Activate")) {
            tester.checkCheckbox("timeTrackingFormat", lowerName(format));
            tester.submit("Activate");
        } else {
            logger.log("Time tracking already activated.");
        }
    }

    public void enable(final String hoursPerDay, final String daysPerWeek, final String format, final String defaultUnit, final Mode mode) {
        logger.log("Activating time tracking");
        tester.gotoPage(TIME_TRACKING_ADMIN_PAGE);

        if (tester.getDialog().hasSubmitButton("Activate")) {
            tester.setFormElement("hoursPerDay", hoursPerDay);
            tester.setFormElement("daysPerWeek", daysPerWeek);
            tester.checkCheckbox("timeTrackingFormat", format);
            tester.selectOption("defaultUnit", defaultUnit);
            if (mode.equals(Mode.LEGACY)) {
                tester.checkCheckbox("legacyMode", "true");
            } else {
                tester.uncheckCheckbox("legacyMode");
            }
            tester.submit("Activate");
            tester.assertTextPresent(String.format("The current default unit for time tracking is <b>%s</b>.", defaultUnit));
        } else {
            logger.log("Time tracking already activated.");
        }
    }

    @Override
    public void enable(String hoursPerDay, String daysPerWeek, Format format, Unit defaultUnit, Mode mode) {
        enable(hoursPerDay, daysPerWeek, lowerName(format), lowerName(defaultUnit), Mode.MODERN);
    }

    private static String lowerName(Enum<?> e) {
        return e.name().toLowerCase(Locale.ENGLISH);
    }

    private void enable(final boolean copyCommentEnabled) {
        logger.log("Activating time tracking");
        tester.gotoPage(TIME_TRACKING_ADMIN_PAGE);

        if (tester.getDialog().hasSubmitButton("Activate")) {
            if (copyCommentEnabled) {
                tester.checkCheckbox("copyComment", "true");
            } else {
                tester.uncheckCheckbox("copyComment");
            }
            tester.submit("Activate");
        } else {
            logger.log("Time tracking already activated.");
        }
    }

    public void disable() {
        logger.log("Deactivating time tracking.");
        tester.gotoPage(TIME_TRACKING_ADMIN_PAGE);
        submitAtPage(TIME_TRACKING_ADMIN_PAGE, "Deactivate", "time tracking already deactivated");
    }

    /**
     * Goes to the given URL, submits the given button or logs the given message if the given button doesn't exist.
     *
     * @param url       url to go to to submit the button
     * @param button    label on the button to submit at url
     * @param logOnFail null or a message to log if button isn't found
     */
    protected void submitAtPage(String url, String button, String logOnFail) {
        tester.gotoPage(url);
        if (tester.getDialog().hasSubmitButton(button)) {
            tester.submit(button);
        } else if (logOnFail != null) {
            logger.log(logOnFail);
        }
    }

    public boolean isIn(final Mode mode) {
        tester.gotoPage(TIME_TRACKING_ADMIN_PAGE);

        if (isEnabled()) {
            // we are in Modern Mode if the text is not present
            // we are in Legacy Mode if the text is present
            return (Mode.MODERN == mode ^ locators.id("legacy-on").exists());
        } else {
            return false;
        }
    }

    public void disableCopyingOfComments() {
        if (isEnabled()) {
            disable();
        }
        enable(false);

        // assert that we actually disabled the Copying of Comments
        textAssertions.assertTextSequence(new WebPageLocator(tester),
                "Copying of comments to work description is currently", "disabled", "For the users you wish");
    }

    @Override
    public void switchFormat(Format format) {
        disable();
        enable(format);
    }

    private boolean isEnabled() {
        if (navigation == null) {
            navigation = new NavigationImpl(tester, environmentData);
        }

        if (!navigation.getCurrentPage().equals(TIME_TRACKING_ADMIN_PAGE)) {
            tester.gotoPage(TIME_TRACKING_ADMIN_PAGE);
        }
        return tester.getDialog().hasSubmitButton("Deactivate");
    }
}