package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.TestXmlRestore;
import com.atlassian.jira.webtests.ztests.admin.TestDataExport;
import com.atlassian.jira.webtests.ztests.admin.TestImportExport;
import com.atlassian.jira.webtests.ztests.imports.properties.TestImportExportExcludedEntities;
import com.atlassian.jira.webtests.ztests.imports.properties.TestImportWithEntityProperties;
import com.atlassian.jira.webtests.ztests.misc.TestDefaultJiraDataFromInstall;
import com.atlassian.jira.webtests.ztests.misc.TestEmptyStringDataRestore;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Test for project import/export functionality.
 *
 * @since v3.13
 */
public class FuncTestSuiteImportExport {

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestDataExport.class)
                .add(TestDefaultJiraDataFromInstall.class)
                .add(TestEmptyStringDataRestore.class)
                .add(TestImportExport.class)
                .add(TestXmlRestore.class)
                .add(TestImportWithEntityProperties.class)
                .add(TestImportExportExcludedEntities.class)
                .build();
    }
}