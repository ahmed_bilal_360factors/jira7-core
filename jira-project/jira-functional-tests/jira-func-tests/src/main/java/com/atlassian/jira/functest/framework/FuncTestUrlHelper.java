package com.atlassian.jira.functest.framework;

import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * Util class for base and rest URL manipulation
 *
 * @since v7.0
 */
public class FuncTestUrlHelper {

    private final String baseUrl;

    @Inject
    public FuncTestUrlHelper(final JIRAEnvironmentData environmentData) {
        baseUrl = environmentData.getBaseUrl().toExternalForm();
    }

    public String getBaseUrl() {
        return baseUrl;
    }


    public String getBaseUrlPlus(final String... paths) {
        return getBaseUrlPlus(Arrays.asList(paths));
    }


    public String getBaseUrlPlus(final Iterable<String> paths) {
        final Iterable<String> pathsNoLeadingSlashes = Iterables.transform(paths, new Function<String, String>() {
            @Override
            public String apply(final String path) {
                // remove leading slashes so we don't end up with duplicates
                return path.startsWith("/") ? path.substring(1) : path;
            }
        });

        final String path = pathsNoLeadingSlashes != null ? StringUtils.join(Lists.newArrayList(pathsNoLeadingSlashes), '/') : "";
        return String.format("%s/%s", getBaseUrl(), path);
    }


    public URI getBaseUriPlus(final Iterable<String> paths) {
        try {
            return new URI(getBaseUrlPlus(paths));
        } catch (final URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }


    public URI getBaseUriPlus(final String... paths) {
        return getBaseUriPlus(Arrays.asList(paths));
    }

    public String getRestApiUrl(final String... paths) {
        final List<String> list = CollectionBuilder.<String>newBuilder("rest", "api", "2").addAll(paths).asList();
        return getBaseUrlPlus(list);
    }

    public URI getRestApiUri(final String... paths) {
        return getRestApiUri(asList(paths));
    }

    public URI getRestApiUri(final Iterable<String> paths) {
        final List<String> all = Lists.newArrayList("rest", "api", "2");
        all.addAll(Lists.newArrayList(paths));
        return getBaseUriPlus(all);
    }

}
