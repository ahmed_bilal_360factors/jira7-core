package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Component;
import com.atlassian.jira.webtests.Groups;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_EMAIL;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_COMPONENTS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.testkit.client.restclient.Component.AssigneeType.COMPONENT_LEAD;
import static com.atlassian.jira.testkit.client.restclient.Component.AssigneeType.PROJECT_LEAD;
import static com.atlassian.jira.testkit.client.restclient.Component.AssigneeType.UNASSIGNED;

@WebTest({Category.FUNC_TEST, Category.ISSUES})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestCreateIssueForEnterprise extends BaseJiraFuncTest {
    public static final String THE_OTHER_COMPONENT = "The other component";

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        backdoor.darkFeatures().enableForSite("no.frother.assignee.field");
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
        backdoor.usersAndGroups().addUser(BOB_USERNAME, BOB_PASSWORD, BOB_FULLNAME, BOB_EMAIL);
    }

    /**
     * Creating issue with default component assignee set at 'Component Lead'
     */
    @Test
    public void createIssueWithComponentLeadAssignee() {
        // Ensure user has the 'Assignable User' permission
        backdoor.usersAndGroups().addUserToGroup(BOB_USERNAME, Groups.DEVELOPERS);
        final Component component = backdoor.components().create(new Component()
                .project(PROJECT_HOMOSAP_KEY)
                .name(THE_OTHER_COMPONENT)
                .leadUserName(BOB_USERNAME)
                .assigneeType(COMPONENT_LEAD));
        final String issue = navigation.issue().createIssue(PROJECT_HOMOSAP, "Bug", "test component lead",
                ImmutableMap.of(FIELD_COMPONENTS, new String[]{component.id.toString()}));
        navigation.issue().gotoIssue(issue);
        tester.assertTextPresent("test component lead");
        tester.assertTextPresent("Bug");
        tester.assertTextPresent(BOB_FULLNAME);
    }

    /**
     * Creating issue with default component assignee set 'Project Lead'
     */
    @Test
    public void createIssueWithProjectLeadAssignee() {
        // Ensure user has the 'Assignable User' permission
        administration.usersAndGroups().addUserToGroup(BOB_USERNAME, Groups.DEVELOPERS);
        administration.project().setProjectLead(PROJECT_HOMOSAP, BOB_USERNAME);
        final Component component = backdoor.components().create(new Component()
                .project(PROJECT_HOMOSAP_KEY)
                .name(THE_OTHER_COMPONENT)
                .assigneeType(PROJECT_LEAD));
        final String issue = navigation.issue().createIssue(PROJECT_HOMOSAP, "Bug", "test project lead",
                ImmutableMap.of(FIELD_COMPONENTS, new String[]{component.id.toString()}));
        navigation.issue().gotoIssue(issue);
        tester.assertTextPresent("test project lead");
        tester.assertTextPresent("Bug");
        tester.assertTextPresent(BOB_FULLNAME);
    }

    /**
     * Creating issue with default component assignee set 'Unassigned'
     */
    @Test
    public void createIssueWithUnassignedComponentLeadAssignee() {
        // Creating an issue with 'Unassigned' as component lead assignee
        administration.generalConfiguration().setAllowUnassignedIssues(true);
        final Component component = backdoor.components().create(new Component()
                .project(PROJECT_HOMOSAP_KEY)
                .name(THE_OTHER_COMPONENT)
                .assigneeType(UNASSIGNED));
        final String issue = navigation.issue().createIssue(PROJECT_HOMOSAP, "Bug", "test unassigned",
                ImmutableMap.of(FIELD_COMPONENTS, new String[]{component.id.toString()}));
        navigation.issue().gotoIssue(issue);
        tester.assertTextPresent("test unassigned");
        tester.assertTextPresent("Bug");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Assignee:", "Unassigned");
    }
}
