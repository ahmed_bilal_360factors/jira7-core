package com.atlassian.jira.webtests.ztests.admin.issuessecurity;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.admin.IssueSecuritySchemes;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.apache.commons.lang3.StringUtils.repeat;

@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestEditIssueSecurityLevel extends BaseJiraFuncTest {

    private static final String TEST_SCHEME_NAME = "test-scheme";
    private static final String SECURITY_LEVEL_A = "Level A";
    private static final String SECURITY_LEVEL_B = "Level B";

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        IssueSecuritySchemes.IssueSecurityScheme scheme = administration.issueSecuritySchemes().newScheme(TEST_SCHEME_NAME, "blah");
        scheme.newLevel(SECURITY_LEVEL_A, "blah");
        scheme.newLevel(SECURITY_LEVEL_B, "blah");
    }

    @Test
    public void whenTryingToSaveWithEmptyNameShouldDisplayErrorMessage() {
        shouldDisplayErrorForName("", "Please specify a name for this security level.");
    }

    @Test
    public void whenTryingToSaveWithTooLongNameSHouldDisplayErrorMessage() {
        String newName = repeat('a', 256);
        shouldDisplayErrorForName(newName, "Name is too long. Please enter a name shorter than 256 characters.");
    }

    @Test
    public void whenTryingToSaveWithNameThatIsAlreadyTakenShouldDisplayErrorMessage() {
        shouldDisplayErrorForName(SECURITY_LEVEL_B, "A Security Level with this name already exists.");
    }

    private void shouldDisplayErrorForName(String newName, String expectedMessage) {
        goToEditPage();
        tester.setFormElement("name", newName);
        tester.submit();
        assertions.getJiraFormAssertions().assertFieldErrMsg(expectedMessage);
    }

    @Test
    public void whenTryingToSaveWithDifferentDescriptionShouldSucceed() {
        goToEditPage();
        tester.setFormElement("description", "some awesome new description");
        tester.submit();
        assertions.getJiraFormAssertions().assertNoErrorsPresent();
    }

    @Test
    public void whenTryingToSaveWithNoDescriptionShouldSucceed() {
        goToEditPage();
        tester.setFormElement("description", "");
        tester.submit();
        assertions.getJiraFormAssertions().assertNoErrorsPresent();
    }

    @Test
    public void whenClickingOnCancelShouldGoBackToSchemeView() {
        goToEditPage();
        tester.clickLink("cancelButton");
        assertions.getURLAssertions().assertCurrentURLPathEndsWith("EditIssueSecurities!default.jspa");
    }

    private void goToEditPage() {
        navigation.gotoPage("secure/admin/ViewIssueSecuritySchemes.jspa");
        navigation.clickLinkWithExactText("Security Levels");
        tester.clickLink(String.format("editLevel_%s", SECURITY_LEVEL_A));
    }

}
