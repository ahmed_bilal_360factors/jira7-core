package com.atlassian.jira.webtests.ztests.issue.move;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperations;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.WorkflowSchemes;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.meterware.httpunit.WebTable;
import org.junit.After;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ASSIGN_FIELD_SCREEN;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_NEO;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_NEO_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.STATUS_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.STEP_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.TRANSIION_NAME_APPROVE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.TRANSIION_NAME_CLOSE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.TRANSIION_NAME_REOPEN;
import static com.atlassian.jira.functest.framework.FunctTestConstants.WORKFLOW_ADDED;
import static com.atlassian.jira.functest.framework.FunctTestConstants.WORKFLOW_COPIED;
import static com.atlassian.jira.functest.framework.FunctTestConstants.WORKFLOW_SCHEME;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.MOVE_ISSUE})
@Restore("TestMoveIssueWithIssueSecurityLevel.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestMoveIssueForEnterprise extends BaseJiraFuncTest {
    private static final String MOVE_CONFIRM_TABLE = "move_confirm_table";
    private static final int CHANGE_TYPE_INDEX = 0;
    private static final int PREVIOUS_VALUE_INDEX = 1;
    private static final int NEW_VALUE_INDEX = 2;
    private static final String FIELD_STATUS = "Status:";
    private static final String SPAN_TAG_END = "</span>";

    @Inject
    private WorkflowSchemes workflowSchemes;

    @Inject
    private FuncTestLogger logger;
    @Inject
    private BulkOperations bulkOperations;
    @Inject
    private Administration administration;
    @Inject
    private TextAssertions textAssertions;

    @After
    public void tearDown() {
        removeWorkflow();
        administration.restoreBlankInstance();
    }

    @Test
    public void testMoveIssueWithSubtaskFromIssueSecuritySchemeToNoScheme() {
        // Move issue with a subtask to a project that does not have an issue security scheme
        navigation.issue().gotoIssue("HSP-8");
        tester.clickLink("move-issue");
        navigation.issue().selectProject("test", "10000_1_pid");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        // Verify that the issue no longer has a security level set
        tester.assertTextNotPresent("Level 1");

        // Verify that the issues subtask is still visible
        tester.assertTextPresent("subtask 1");
        tester.clickLinkWithText("subtask 1");
        // Verify that the subtask also does not have any security level set
        tester.assertTextNotPresent("Level 1");
    }

    @Test
    public void testMoveIssueWithSubtaskFromIssueSecuritySchemeToSameScheme() {
        // Move issue with a subtask to a project that does not have an issue security scheme
        navigation.issue().gotoIssue("HSP-8");
        tester.clickLink("move-issue");
        navigation.issue().selectProject("SameSchemeProject", "10000_1_pid");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        // Verify that the issue still has the same issue security level
        tester.assertTextPresent("Level 1");

        // Verify that the issues subtask is still visible
        tester.assertTextPresent("subtask 1");
        tester.clickLinkWithText("subtask 1");
        // Verify that the subtask also has the same issue security level.
        tester.assertTextPresent("Level 1");
    }

    @Test
    public void testMoveIssueWithSubtaskFromIssueSecuritySchemeToDifferentScheme() {
        // Move issue with a subtask to a project that does not have an issue security scheme
        navigation.issue().gotoIssue("HSP-8");
        tester.clickLink("move-issue");
        navigation.issue().selectProject(PROJECT_MONKEY, "10000_1_pid");
        tester.submit("Next");
        tester.submit("Next");
        tester.selectOption("security", "Level 2");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        // Verify that the issue has the new issue security level
        tester.assertTextPresent("Level 2");

        // Verify that the issues subtask is still visible
        tester.assertTextPresent("subtask 1");
        tester.clickLinkWithText("subtask 1");
        // Verify that the subtask also has the new issue security level.
        tester.assertTextPresent("Level 2");
    }

    @Test
    public void testMoveIssueWithSubtaskFromNoSchemeToIssueSecurityScheme() {
        // Move issue with a subtask to a project that does not have an issue security scheme
        navigation.issue().gotoIssue("TST-1");
        tester.clickLink("move-issue");
        navigation.issue().selectProject(PROJECT_MONKEY, "10010_1_pid");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        bulkOperations.waitAndReloadBulkOperationProgressPage();
        // Verify that the issue still does not have any issue security level set
        tester.assertTextNotPresent("Level 2");

        // Verify that the issues subtask is still visible
        tester.assertTextPresent("subtask 2");
        tester.clickLinkWithText("subtask 2");
        // Verify that the subtask also does not have any issue security level set
        tester.assertTextNotPresent("Level 2");
    }

    @Test
    public void testMoveIssueWithSubtaskFromNoSchemeToIssueSecuritySchemeRequired() {
        // Move issue with a subtask to a project that does not have an issue security scheme
        navigation.issue().gotoIssue("TST-1");
        tester.clickLink("move-issue");
        navigation.issue().selectProject(PROJECT_HOMOSAP, "10010_1_pid");
        tester.submit("Next");
        tester.submit("Next");
        tester.selectOption("security", "Level 1");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        // Verify that the issue now has a issue security level set
        tester.assertTextPresent("Level 1");

        // Verify that the issues subtask is still visible
        tester.assertTextPresent("subtask 2");
        tester.clickLinkWithText("subtask 2");
        // Verify that the subtask also now has a issue security level set
        tester.assertTextPresent("Level 1");
    }

    @Test
    @Restore("blankWithOldDefault.xml")
    public void testMoveIssueForEnterprise() {
        createProjectIfAbsent(PROJECT_HOMOSAP, PROJECT_HOMOSAP_KEY, ADMIN_USERNAME);
        createProjectIfAbsent(PROJECT_NEO, PROJECT_NEO_KEY, ADMIN_USERNAME);

        moveOperationToSameProjectAndType();
        moveIssueWithWorkflow();
        moveIssueWithMultipleWorkflowInScheme();
    }

    public void moveIssueWithMultipleWorkflowInScheme() {
        logger.log("Move Operation: Moving issue to a different issueType with a different workflow that has different status.");

        createWorkflowWithDifferentWorkflows();

        // Create an issue that uses a issue type in the WORKFLOW_COPIED
        final String issueKey = backdoor.issues().createIssue(PROJECT_NEO_KEY, "test moving issue between workflows", ADMIN_USERNAME, "Minor", "Bug").key();
        navigation.issue().gotoIssue(issueKey);

        //change the status to a status thats not in WORKFLOW_ADDED
        tester.clickLinkWithText("Resolve Issue");
        tester.setWorkingForm("issue-workflow-transition");
        tester.submit("Transition");
        tester.assertTextPresent("Resolved");

        //start moving
        tester.clickLink("move-issue");
        navigation.issue().selectIssueType("Improvement", "issuetype"); //set it to an issuetype thats in WORKFLOW_ADDED to not skip step 2
        tester.submit();

        tester.assertTextPresent("Move Issue: Select Status");
        //checks its got the correct statuses by choosing the status that was added in WORKFLOW_ADDED
        tester.selectOption("beanTargetStatusId", STEP_NAME);
        tester.submit();

        tester.assertTextPresent("Move Issue: Update Fields");
        tester.getDialog().setWorkingForm("jiraform");
        tester.submit();

        //check correct changes are going to be made
        tester.assertTextPresent("Move Issue: Confirm");
        tester.getDialog().setWorkingForm("jiraform");
        confirmChangesToBeMade("Bug", "Improvement");
        confirmStatusChangesToBeMade("Resolved", STEP_NAME);
        confirmWorkflowMovement(WORKFLOW_COPIED, WORKFLOW_ADDED);
        tester.submit("Move");

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), FIELD_STATUS, STEP_NAME);
        tester.assertLinkPresentWithText(PROJECT_NEO);

        //reopen the issue to check if the WORKFLOW_ADDED operations are available
        tester.clickLinkWithText(TRANSIION_NAME_REOPEN);
        tester.setWorkingForm("issue-workflow-transition");
        tester.submit("Transition");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), FIELD_STATUS, "Open");

        //check that the workflow operation 'Approve Issue' is there
        tester.assertLinkPresentWithText(TRANSIION_NAME_APPROVE);
        tester.clickLinkWithText(TRANSIION_NAME_APPROVE);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), FIELD_STATUS, STEP_NAME);

        navigation.issue().deleteIssue(issueKey);
    }

    private void confirmWorkflowMovement(final String oldWorkflow, final String newWorkFlow) {
        tester.assertTextPresent("Move Issue: Confirm");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "(" + oldWorkflow + ")" + SPAN_TAG_END, "(" + newWorkFlow + ")" + SPAN_TAG_END);
    }

    private void confirmChangesToBeMade(final String oldValue, final String newValue) {
        tester.assertTextPresent("Move Issue: Confirm");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), oldValue + SPAN_TAG_END, newValue + SPAN_TAG_END);
    }

    private void confirmStatusChangesToBeMade(final String oldValue, final String newValue) {
        tester.assertTextPresent("Move Issue: Confirm");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), oldValue, newValue);
    }

    private void createWorkflowWithDifferentWorkflows() {
        logger.log("Creating workflow");

        workflowSchemes.addWorkFlowScheme(WORKFLOW_SCHEME, "Workflow scheme to test move issue");
        administration.workflows().goTo().copyWorkflow("jira", WORKFLOW_COPIED);
        administration.workflows().goTo().addWorkflow(WORKFLOW_ADDED, "this workflow has one status Open only");
        administration.workflows().goTo();

        administration.statuses().addLinkedStatus(STATUS_NAME, "The resolution of this issue has been approved");
        administration.workflows().goTo().workflowSteps(WORKFLOW_ADDED).add(STEP_NAME, STATUS_NAME);
        administration.workflows().goTo().workflowSteps(WORKFLOW_ADDED).addTransition("Open", TRANSIION_NAME_APPROVE, "", STEP_NAME, null);
        administration.workflows().goTo().workflowSteps(WORKFLOW_ADDED).addTransition(STEP_NAME, TRANSIION_NAME_REOPEN, "", "Open", ASSIGN_FIELD_SCREEN);

        backdoor.getTestkit().workflowSchemes().assignScheme(10001L, "Bug", WORKFLOW_COPIED);
        backdoor.getTestkit().workflowSchemes().assignScheme(10001L, "New Feature", WORKFLOW_COPIED);
        backdoor.getTestkit().workflowSchemes().assignScheme(10001L, "Task", WORKFLOW_COPIED);
        backdoor.getTestkit().workflowSchemes().assignScheme(10001L, "Improvement", WORKFLOW_ADDED); //this issue type has a different workflow with different statuses

        administration.project().associateWorkflowScheme(PROJECT_NEO, WORKFLOW_SCHEME);
        workflowSchemes.waitForSuccessfulWorkflowSchemeMigration(PROJECT_NEO, WORKFLOW_SCHEME);
    }


    /**
     * Tests the error handling if the user attempts to move an issue to the same position
     */
    public void moveOperationToSameProjectAndType() {
        logger.log("Move Operation: Test the error checking for moving an issue to the same project and issue type");
        final String issueKey = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "test move issue", ADMIN_USERNAME, "Trivial", "Bug").key();

        navigation.issue().gotoIssue(issueKey);

        tester.clickLink("move-issue");
        tester.assertTextPresent("Move Issue");
        tester.setFormElement("issuetype", "1");

        tester.submit();
        tester.assertTextPresent("Step 1 of 4");
        tester.assertTextPresent("You must select a different project or issue type to complete a move operation.");
    }

    public void moveIssueWithWorkflow() {
        logger.log("Move Operation: Moving issue to project without the same status.");

        createWorkflow();

        performMoveIssue();

        removeWorkflow();

    }

    private void createProjectIfAbsent(final String name, final String key, final String lead) {
        administration.project().addProject(name, key, lead);
    }

    private void removeWorkflow() {
        logger.log("Removing workflow");

        createProjectIfAbsent(PROJECT_NEO, PROJECT_NEO_KEY, ADMIN_USERNAME);
        administration.project().associateWorkflowScheme(PROJECT_NEO, "Default");

        workflowSchemes.deleteWorkFlowScheme("10000");
        administration.workflows().goTo().delete(WORKFLOW_COPIED);
        administration.workflows().goTo().delete(WORKFLOW_ADDED);
    }

    private void createWorkflow() {
        logger.log("Creating workflow");
        workflowSchemes.addWorkFlowScheme(WORKFLOW_SCHEME, "Workflow scheme to test move issue");

        administration.workflows().goTo().copyWorkflow("jira", WORKFLOW_COPIED);
        administration.statuses().addLinkedStatus(STATUS_NAME, "The resolution of this issue has been approved");
        administration.workflows().goTo().workflowSteps(WORKFLOW_COPIED).add(STEP_NAME, STATUS_NAME);
        administration.workflows().goTo().workflowSteps(WORKFLOW_COPIED).addTransition("Resolved", TRANSIION_NAME_APPROVE, "", STEP_NAME, null);
        administration.workflows().goTo().workflowSteps(WORKFLOW_COPIED).addTransition(STEP_NAME, TRANSIION_NAME_REOPEN, "", "Open", ASSIGN_FIELD_SCREEN);
        administration.workflows().goTo().workflowSteps(WORKFLOW_COPIED).addTransition(STEP_NAME, TRANSIION_NAME_CLOSE, "", "Closed", null);

        backdoor.getTestkit().workflowSchemes().assignScheme(10000L, "Bug", WORKFLOW_COPIED);
        administration.project().associateWorkflowScheme(PROJECT_NEO, WORKFLOW_SCHEME);
        workflowSchemes.waitForSuccessfulWorkflowSchemeMigration(PROJECT_NEO, WORKFLOW_SCHEME);
    }

    private void performMoveIssue() {
        final String issueKey = backdoor.issues().createIssue(PROJECT_NEO_KEY, "test moving issue between workflows", ADMIN_USERNAME, "Minor", "Bug").key();
        navigation.issue().gotoIssue(issueKey);

        tester.clickLinkWithText("Resolve Issue");
        tester.setWorkingForm("issue-workflow-transition");
        tester.submit("Transition");
        tester.assertTextPresent("Resolved");

        tester.clickLinkWithText(TRANSIION_NAME_APPROVE);

        tester.clickLink("move-issue");
        navigation.issue().selectProject(PROJECT_HOMOSAP);
        tester.submit();

        tester.assertTextPresent("Move Issue: Select Status");
        tester.selectOption("beanTargetStatusId", "Open");
        tester.submit();

        tester.assertTextPresent("Move Issue: Update Fields");
        tester.getDialog().setWorkingForm("jiraform");
        tester.submit();

        checkMoveConfirmTable();

        tester.getDialog().setWorkingForm("jiraform");
        tester.submit("Move");

        tester.assertTextPresent("Open");
        tester.assertLinkPresentWithText(PROJECT_HOMOSAP);

        navigation.issue().deleteIssue(issueKey);
    }

    private void checkMoveConfirmTable() {
        tester.assertTextPresent("Move Issue: Confirm");
        try {
            final WebTable fieldTable = tester.getDialog().getResponse().getTableWithID(MOVE_CONFIRM_TABLE);
            // First row is a headings row so skip it
            for (int i = 1; i < fieldTable.getRowCount(); i++) {
                final String field = fieldTable.getCellAsText(i, CHANGE_TYPE_INDEX);
                if (field.contains("Status")) {
                    final String previousCell = fieldTable.getCellAsText(i, PREVIOUS_VALUE_INDEX);
                    final String newCell = fieldTable.getCellAsText(i, NEW_VALUE_INDEX);
                    assertTrue(previousCell.contains("Approved"));
                    assertTrue(newCell.contains("Open"));
                    return;
                }
            }

            fail("Cannot find field with id '" + "Status" + "'.");
        } catch (final SAXException e) {
            fail("Cannot find table with id '" + MOVE_CONFIRM_TABLE + "'.");
            e.printStackTrace();
        }
    }
}
