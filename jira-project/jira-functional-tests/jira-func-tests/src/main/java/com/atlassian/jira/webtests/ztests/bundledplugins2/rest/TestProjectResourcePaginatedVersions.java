package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.GenericRestClient;
import com.atlassian.jira.testkit.client.restclient.PageBean;
import com.atlassian.jira.testkit.client.restclient.ProjectClient;
import com.atlassian.jira.testkit.client.restclient.Version;
import com.google.common.collect.ImmutableList;
import com.sun.jersey.api.client.GenericType;
import org.junit.Before;
import org.junit.Test;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.REST})
public class TestProjectResourcePaginatedVersions extends BaseJiraRestTest {
    private static final GenericType<PageBean<Version>> PAGE_TYPE = new GenericType<PageBean<Version>>() {
    };

    public static final String ATL_PROJECT_KEY = "ATL";

    private Version version5;
    private Version version4;
    private Version version3;
    private Version version2;
    private Version version1;
    private List<Version> atlVersions;

    private ProjectClient projectClient;
    private GenericRestClient genericClient;

    @Before
    public void setUpTest() {
        projectClient = new ProjectClient(environmentData);
        genericClient = new GenericRestClient();

        backdoor.restoreDataFromResource("TestProjectResource.xml");

        version1 = new Version().self(createVersionUri(10010)).archived(false)
                .released(false).name("One").releaseDate("01/Mar/11").overdue(true).id(10010L).projectId(10010l);

        version2 = new Version().self(createVersionUri(10011)).archived(false)
                .released(false).name("Two").description("Description").id(10011L).projectId(10010l);

        version3 = new Version().self(createVersionUri(10012)).archived(false)
                .released(true).name("Three")
                .releaseDate("09/Mar/11").id(10012L).projectId(10010l);

        version4 = new Version().self(createVersionUri(10013)).archived(true)
                .released(true).name("Four").description("Four")
                .releaseDate("09/Mar/11").id(10013L).projectId(10010l);

        version5 = new Version().self(createVersionUri(10014)).archived(true)
                .released(false).name("Five").description("Five").id(10014L).projectId(10010l);

        atlVersions = ImmutableList.of(version5, version4, version3, version2, version1);
    }

    @Test
    public void testGettingPagedVersionsWithoutAnyParameters() {
        PageBean<Version> versionsPaged = projectClient.getVersionsPaged(ATL_PROJECT_KEY, null, null, null);
        assertThat(versionsPaged.getValues(), equalTo(atlVersions));
    }

    @Test
    public void whenOrderingIsNotSpecifiedVersionsAreReturnedInAscendingSequenceOrder() {
        assertPagedVersionsAreReturnedInExpectedOrderForSpecifiedOrdering(null, atlVersions);
    }

    @Test
    public void versionsCanBeOrderedBySequenceAscendingExplicitly() {

        assertPagedVersionsAreReturnedInExpectedOrderForSpecifiedOrdering("sequence", atlVersions);
        assertPagedVersionsAreReturnedInExpectedOrderForSpecifiedOrdering("+sequence", atlVersions);
    }

    @Test
    public void versionsCanBeOrderedBySequenceDescending() {
        assertPagedVersionsAreReturnedInExpectedOrderForSpecifiedOrdering("-sequence", version1, version2, version3, version4, version5);
    }

    @Test
    public void versionsCanBeOrderedByNameDescending() {
        assertPagedVersionsAreReturnedInExpectedOrderForSpecifiedOrdering("-name", version2, version3, version1, version4, version5);
    }

    @Test
    public void versionsCanBeOrderedByNameAscending() {
        assertPagedVersionsAreReturnedInExpectedOrderForSpecifiedOrdering("name", version5, version4, version1, version3, version2);
        assertPagedVersionsAreReturnedInExpectedOrderForSpecifiedOrdering("+name", version5, version4, version1, version3, version2);
    }

    @Test
    public void nullValuesInOrderingAreReturnedLastAndOrderedBySequence() {
        assertPagedVersionsAreReturnedInExpectedOrderForSpecifiedOrdering("releaseDate", version1, version4, version3, version5, version2);
    }

    @Test
    public void emptyPageIsReturnedWhenPageRequestIsOutOfRange() {
        PageBean<Version> versions = projectClient.getVersionsPaged(ATL_PROJECT_KEY, 10L, 4, "name");
        assertThat(versions.getIsLast(), equalTo(Boolean.TRUE));
        assertThat(versions.getValues(), hasSize(0));
    }

    private void assertPagedVersionsAreReturnedInExpectedOrderForSpecifiedOrdering(String ordering, Version... expectedOrder) {
        assertPagedVersionsAreReturnedInExpectedOrderForSpecifiedOrdering(ordering, Arrays.asList(expectedOrder));
    }

    private void assertPagedVersionsAreReturnedInExpectedOrderForSpecifiedOrdering(String ordering, List<Version> expectedOrder) {
        PageBean<Version> firstPage = projectClient.getVersionsPaged(ATL_PROJECT_KEY, 0L, 3, ordering);
        PageBean<Version> secondPage = projectClient.getVersionsPaged(ATL_PROJECT_KEY, 3L, 3, ordering);

        assertThat(firstPage.getValues(), contains(expectedOrder.subList(0, 3).toArray(new Version[3])));
        assertThat(secondPage.getValues(), contains(expectedOrder.subList(3, 5).toArray(new Version[2])));

        assertThat(firstPage.getIsLast(), equalTo(false));
        assertThat(secondPage.getIsLast(), equalTo(true));

        assertThat(secondPage.getMaxResults(), equalTo(3));

        assertThat(genericClient.getNextPage(firstPage, PAGE_TYPE).body, equalTo(secondPage));
    }

    private URI createVersionUri(long id) {
        return URI.create(environmentData.getBaseUrl() + "/rest/api/2/version/" + id);
    }
}
