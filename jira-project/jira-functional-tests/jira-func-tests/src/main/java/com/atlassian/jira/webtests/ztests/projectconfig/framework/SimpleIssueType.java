package com.atlassian.jira.webtests.ztests.projectconfig.framework;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @since v5.2
 */
public class SimpleIssueType implements Comparable<SimpleIssueType> {
    @JsonProperty
    private String iconUrl;

    @JsonProperty
    private String name;

    @JsonProperty
    private String description;

    @JsonProperty
    private String id;

    @JsonProperty
    private boolean subTask;

    @JsonProperty
    private boolean defaultIssueType;

    public SimpleIssueType() {
    }

    public SimpleIssueType(SimpleIssueType issueType) {
        this(issueType.iconUrl, issueType.name, issueType.description, issueType.id, issueType.subTask, issueType.defaultIssueType);
    }

    public SimpleIssueType(String iconUrl, String name, String description, String id, boolean subTask, boolean defaultIssueType) {
        this.iconUrl = iconUrl;
        this.name = name;
        this.description = description;
        this.id = id;
        this.subTask = subTask;
        this.defaultIssueType = defaultIssueType;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getId() {
        return id;
    }

    public boolean isSubTask() {
        return subTask;
    }

    public boolean isDefaultIssueType() {
        return defaultIssueType;
    }

    public void setDefaultIssueType(final boolean defaultIssueType) {
        this.defaultIssueType = defaultIssueType;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SimpleIssueType that = (SimpleIssueType) o;

        if (defaultIssueType != that.defaultIssueType) {
            return false;
        }
        if (subTask != that.subTask) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (iconUrl != null ? !iconUrl.equals(that.iconUrl) : that.iconUrl != null) {
            return false;
        }
        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = iconUrl != null ? iconUrl.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (subTask ? 1 : 0);
        result = 31 * result + (defaultIssueType ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public int compareTo(SimpleIssueType o) {
        if (defaultIssueType != o.defaultIssueType) {
            return defaultIssueType ? -1 : 1;
        } else {
            return name.compareTo(o.name);
        }
    }
}
