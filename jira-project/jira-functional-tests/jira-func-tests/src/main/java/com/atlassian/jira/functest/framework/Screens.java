package com.atlassian.jira.functest.framework;

import com.google.inject.ImplementedBy;

/**
 * An interface for working with Screens. It mimics user interaction to perform actions connected with Field Screens.
 *
 * @since v7.2
 */
@ImplementedBy(ScreensImpl.class)
public interface Screens {
    void addScreen(String screenName, String screenDescription);

    void deleteScreen(String screenName);

    void addFieldScreenScheme(String schemeName, String schemeDescription);

    void copyFieldScreenScheme(String copiedSchemeName, String schemeName, String schemeDescription);

    void deleteFieldScreenScheme(String schemeName);

    void removeAllRemainingScreenAssociationsFromDefault();

    void removeAllRemainingFieldScreens();

    void addIssueTypeFieldScreenScheme(String schemeName, String schemeDescription, String defaultScreenScheme);

    void deleteIssueTypeFieldScreenScheme(String schemeId);

    void copyIssueTypeFieldScreenSchemeName(String copiedSchemeId, String schemeName, String schemeDescription);

    void addIssueOperationToScreenAssociation(String schemeName, String issueOperation, String screenName);

    void deleteIssueOperationFromScreenAssociation(String schemeName, String issueOperation);

    void deleteTabFromScreen(String screenName, String tabName);

    void removeFieldFromFieldScreenTab(String screenName, String tabName, String[] fieldNames);
}
