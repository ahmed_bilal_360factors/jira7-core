package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookListener;
import com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookRegistrationClient;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.atlassian.jira.functest.matcher.WebHookMatchers.containsWebHook;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.WEBHOOKS, Category.REFERENCE_PLUGIN})
@Restore("TestVersionWebHooks.zip")
public class TestWebHookUpgradeTaskForVersion extends AbstractWebHookTest {
    private final static String[] allEvents = new String[]{
            "jira:issue_created",
            "jira:issue_updated",
            "jira:issue_deleted",
            "jira:worklog_updated",

            "user_created",
            "user_deleted",

            "version_created",
            "version_released",
            "version_unreleased",
            "version_moved",
            "version_updated",
            "version_merged",
            "version_deleted",

            "project_created",
            "project_deleted",
            "project_updated",

            "option_attachments_changed",
            "option_issuelinks_changed",
            "option_timetracking_changed",
            "option_subtasks_changed",
            "option_unassigned_issues_changed",
            "option_watching_changed",
            "option_voting_changed"
    };

    private final static String[] versionEvents = new String[]{
            "version_created",
            "version_deleted",
            "version_merged",
            "version_updated",
            "version_moved",
            "version_released",
            "version_unreleased"
    };

    @Before
    public void setUp() throws Exception {
        super.setUpTest();
    }

    @Test
    public void testUpgradingVersionWebHooks() {
        WebHookRegistrationClient client = new WebHookRegistrationClient(environmentData);
        final List<WebHookRegistrationClient.RegistrationResponse> allWebHooks = client.getAllWebHooks();

        assertEquals("All WebHooks should have been restored", 3, allWebHooks.size());

        assertThat(allWebHooks, containsWebHook(
                new WebHookListener("Empty Webhook", "http://example.com/rest/webhooks/webhook1", new String[]{}, "", false, 1)));

        assertThat(allWebHooks, containsWebHook(
                new WebHookListener("Only Version Webhook", "http://example.com/rest/webhooks/webhook1", versionEvents, "", false, 3)));

        assertThat(allWebHooks, containsWebHook(
                new WebHookListener("All Events Webhook", "http://example.com/rest/webhooks/webhook1", allEvents, "", false, 2)));
    }
}
