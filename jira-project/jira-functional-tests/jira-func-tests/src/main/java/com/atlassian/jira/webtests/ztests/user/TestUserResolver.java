package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;

@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUserResolver extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
    }

    @Test
    public void testFindByFullNameExists() {
        List<String> results = backdoor.userManager().userKeysByFullName("Administrator");
        assertEquals(results, Arrays.asList("admin"));
    }

    @Test
    public void testFindByFullNameCaseInsensitive() {
        List<String> results = backdoor.userManager().userKeysByFullName("ADMINISTRATOR");
        assertEquals(results, Arrays.asList("admin"));
    }

    @Test
    public void testFindByFullNameNotFound() {
        List<String> results = backdoor.userManager().userKeysByFullName("IDoNotExist");
        assertEquals(results, Collections.emptyList());
    }

    @Test
    public void testFindByEmailExists() {
        List<String> results = backdoor.userManager().userKeysByEmail("admin@example.com");
        assertEquals(results, Arrays.asList("admin"));
    }

    @Test
    public void testFindByEmailCaseInsensitive() {
        List<String> results = backdoor.userManager().userKeysByEmail("ADMIN@EXAMPLE.COM");
        assertEquals(results, Arrays.asList("admin"));
    }

    @Test
    public void testFindByEmailNotFound() {
        List<String> results = backdoor.userManager().userKeysByEmail("iDoNotExist@nowhere.com");
        assertEquals(results, Collections.emptyList());
    }
}
