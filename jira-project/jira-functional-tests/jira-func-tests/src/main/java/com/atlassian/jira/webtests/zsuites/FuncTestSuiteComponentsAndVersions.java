package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.customfield.TestVersionCustomField;
import com.atlassian.jira.webtests.ztests.project.TestComponentValidation;
import com.atlassian.jira.webtests.ztests.project.TestProjectComponentQuickSearch;
import com.atlassian.jira.webtests.ztests.project.TestVersionValidation;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of tests related to Components and Versions
 *
 * @since v4.0
 */
public class FuncTestSuiteComponentsAndVersions {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestComponentValidation.class)
                .add(TestVersionValidation.class)
                .add(TestVersionCustomField.class)
                .add(TestProjectComponentQuickSearch.class)
                .build();
    }
}