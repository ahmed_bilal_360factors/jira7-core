package com.atlassian.jira.functest.framework.page;

import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.locator.Locator;
import com.atlassian.jira.webtests.table.HtmlTable;
import com.meterware.httpunit.WebTable;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * @since v6.0
 */
public class IssueSearchPage {

    private final WebTester tester;
    private final LocatorFactory locators;

    @Inject
    public IssueSearchPage(final WebTester tester, final LocatorFactory locators) {
        this.tester = tester;
        this.locators = locators;
    }


    public boolean hasResultsTable() {
        return locators.id("issuetable").exists();
    }

    public HtmlTable getResultsTable() {
        final WebTable webTable = tester.getDialog().getWebTableBySummaryOrId("issuetable");
        return new HtmlTable(webTable);
    }

    public List<String> getResultsIssueKeys() {
        final List<String> issueKeys = new ArrayList<String>();
        for (HtmlTable.Row row : getResultsTable().getRows()) {
            if (row.getRowIndex() > 0) {
                issueKeys.add(row.getCellForHeading("Key"));
            }
        }
        return issueKeys;
    }

    public String getWarning() {
        final Locator warning = locators.css("aui-message.warning");
        return warning.exists() ? warning.getText() : null;
    }
}
