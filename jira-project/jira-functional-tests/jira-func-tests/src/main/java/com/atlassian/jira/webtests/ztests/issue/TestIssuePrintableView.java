package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.ISSUES})
@Restore("TestTimeTrackingAggregates.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestIssuePrintableView extends BaseJiraFuncTest {

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
    }

    @Test
    public void testPrintableView() {
        // this is a sub-task
        navigation.issue().gotoIssue("HSP-10");
        tester.clickLinkWithText("Printable");

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"parent 2", "HSP-9", "HSP-10", "sub 3"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Status:", "Open");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Project:", "homosapien");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Component/s:", "None");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Affects Version/s:", "None");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Fix Version/s:", "None");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Type:", "Sub-task", "Priority:", "Major"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Reporter:", ADMIN_FULLNAME, "Assignee:", ADMIN_FULLNAME});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Resolution:", "Unresolved", "Votes:", "0"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Remaining Estimate:", "1 day", "Time Spent:", "1 day", "Original Estimate:", "1 day"});
        tester.assertTableNotPresent(" Remaining Estimate:");
        tester.assertTableNotPresent(" Time Spent:");
        tester.assertTableNotPresent(" Original Estimate:");
        tester.assertTableNotPresent("Sub-Tasks:");

        // this is a parent issue
        tester.gotoPage("/browse/HSP-9");
        tester.clickLinkWithText("Printable");

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "HSP-9", "parent 2");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Status:", "Open");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Project:", "homosapien");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Component/s:", "None");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Affects Version/s:", "None");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Fix Version/s:", "None");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Type:", "Bug", "Priority:", "Major"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Reporter:", ADMIN_FULLNAME, "Assignee:", ADMIN_FULLNAME});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Resolution:", "Unresolved", "Votes:", "0"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{
                " Remaining Estimate:", "3 days", "Remaining Estimate:", "Not Specified",
                " Time Spent:", "1 day", "Time Spent:", "Not Specified",
                " Original Estimate:", "3 days", "Original Estimate:", "Not Specified"
        });
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Sub-Tasks:", "HSP-10", "sub 3", "HSP-11", "sub 4"});
    }

    /**
     * Tests for some xss hacks JSP-145410
     */
    @Test
    public void testProperHtmlEscaping() {
        // make admin's full name contain &trade;
        navigation.userProfile().gotoCurrentUserProfile();
        tester.clickLink("edit_profile_lnk");
        tester.setFormElement("fullName", ADMIN_FULLNAME + " &trade;");
        tester.submit();

        putTradeMarkIntoIssue("HSP-10");
        putTradeMarkIntoIssue("HSP-9");

        // set up issue links to test issue link lists
        backdoor.issueLinking().createIssueLinkType("related", "is related to", "relates to");
        navigation.issue().linkIssueWithComment("HSP-9", "relates to", "HSP-10", null, null);

        // single issue printable
        tester.clickLinkWithText("Printable");
        assertTradeMarkIsEscaped();
        tester.gotoPage("/"); // get off the printable view

        // issue navigator printable
        navigation.issueNavigator().displayPrintableAllIssues();
        assertTradeMarkIsEscaped();
        tester.gotoPage("/"); // get off the printable view

        // issue navigator full content
        navigation.issueNavigator().displayFullContentAllIssues();
        assertTradeMarkIsEscaped();
        tester.gotoPage("/");
    }

    private void putTradeMarkIntoIssue(String issueKey) {
        navigation.issue().gotoIssue(issueKey);
        // edit issue and add trademark symbol &trade; entity to the summary and other non-validated text fields
        tester.clickLink("edit-issue");
        tester.setFormElement("summary", "Issue " + issueKey + " &trade;");
        tester.setFormElement("environment", "&trade;");
        tester.setFormElement("description", "&trade;");
        tester.setFormElement("comment", "&trade;");
        tester.submit("Update");
    }

    private void assertTradeMarkIsEscaped() {
        tester.assertTextNotPresent("&trade;"); // should not be unescaped
        tester.assertTextPresent("&amp;trade;"); // this is proper entity escaping
    }
}
