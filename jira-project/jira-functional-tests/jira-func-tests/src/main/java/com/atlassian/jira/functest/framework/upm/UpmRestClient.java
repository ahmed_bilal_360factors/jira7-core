package com.atlassian.jira.functest.framework.upm;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.httpclient.apache.httpcomponents.DefaultHttpClientFactory;
import com.atlassian.httpclient.api.DefaultResponseTransformation;
import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.httpclient.api.Request;
import com.atlassian.httpclient.api.Response;
import com.atlassian.httpclient.api.ResponsePromise;
import com.atlassian.httpclient.api.factory.HttpClientOptions;
import com.atlassian.integrationtesting.ApplicationPropertiesImpl;
import com.atlassian.sal.api.executor.ThreadLocalContextManager;
import com.atlassian.util.concurrent.Promise;
import com.google.common.base.Function;
import org.apache.http.Header;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.auth.BasicScheme;
import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Nullable;
import java.net.URI;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @since 6.3
 */
public class UpmRestClient {
    public static final String CONTENT_TYPE_PLUGIN_JSON = "application/vnd.atl.plugins.plugin+json";

    private final String baseUrl;
    private final UsernamePasswordCredentials credentials;
    private final DefaultHttpClientFactory httpClientFactory;
    private final HttpClient httpClient;

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UpmRestClient.class);

    public UpmRestClient(final String baseUrl, final UsernamePasswordCredentials credentials) {
        this.baseUrl = checkNotNull(baseUrl, "baseurl");
        this.credentials = checkNotNull(credentials, "credentials");
        final HttpClientOptions httpClientOptions = new HttpClientOptions();
        final int timeout = 120;
        httpClientOptions.setSocketTimeout(timeout, TimeUnit.SECONDS);
        httpClientOptions.setConnectionTimeout(timeout, TimeUnit.SECONDS);
        httpClientOptions.setRequestTimeout(timeout, TimeUnit.SECONDS);
        httpClientFactory = new DefaultHttpClientFactory(new VoidEventPublisher(), new ApplicationPropertiesImpl(baseUrl),
                new ThreadLocalContextManager() {
                    @Override
                    public Object getThreadLocalContext() {
                        return null;
                    }

                    @Override
                    public void setThreadLocalContext(Object context) {}

                    @Override
                    public void clearThreadLocalContext() {}
                });

        httpClient = httpClientFactory.create(httpClientOptions);
    }

    public static UpmRestClient withDefaultAdminCredentials(final String baseUrl) {
        return new UpmRestClient(baseUrl, DefaultCredentials.getDefaultAdminCredentials());
    }

    public boolean isPluginEnabled(final String pluginKey) {
        return getPluginStatus(pluginKey).isEnabled();
    }

    public boolean isPluginUserInstalled(final String pluginKey) {
        return getPluginStatus(pluginKey).isUserInstalled();
    }

    public PluginStatus getPluginStatus(final String pluginKey) {
        try {
            return requestPluginStatus(pluginKey).get();
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Promise<PluginStatus> requestPluginStatus(final String pluginKey) {
        checkNotNull(pluginKey, "pluginKey");

        final Request.Builder builder = newRequest(upmPluginInformation(pluginKey));
        final Request request = builder.build();
        return builder.get().transform(DefaultResponseTransformation.<PluginStatus>builder()
                .ok(response -> {
                    final String entity = response.getEntity();
                    try {
                        final JSONObject jsonObject = new JSONObject(entity);
                        final boolean enabled = jsonObject.getBoolean("enabled");
                        final boolean userInstalled = jsonObject.getBoolean("userInstalled");

                        return new PluginStatus(pluginKey, jsonObject, enabled, userInstalled);
                    } catch (final JSONException e) {
                        throw new RuntimeException("Requesting details of plugin with key \"" + pluginKey +
                                "\" returned " + response.getStatusCode() + ": " + entity, e);
                    }
                }).notFound(response -> {
                    throw new RuntimeException("Could not get details on plugin with key \"" + pluginKey + "\": plugin not installed (404).");
                }).serverError(new Function<Response, PluginStatus>() {
                    @Nullable
                    @Override
                    public PluginStatus apply(@Nullable Response response) {
                        throw httpRuntimeError(response, pluginKey, request);
                    }
                }).build());
    }

    public Promise<Option<PluginStatus>> requestPluginStatusOption(final String pluginKey) {
        checkNotNull(pluginKey, "pluginKey");

        final Request.Builder builder = newRequest(upmPluginInformation(pluginKey));
        final Request request = builder.build();
        return builder.get().transform(DefaultResponseTransformation.<Option<PluginStatus>>builder()
                .ok(response -> {
                    final String entity = response.getEntity();
                    try {
                        final JSONObject jsonObject = new JSONObject(entity);
                        final boolean enabled = jsonObject.getBoolean("enabled");
                        final boolean userInstalled = jsonObject.getBoolean("userInstalled");

                        return Option.some(new PluginStatus(pluginKey, jsonObject, enabled, userInstalled));
                    } catch (final JSONException e) {
                        log.error("Got response " + response.getStatusCode() + ": " + entity);
                        throw new RuntimeException(e);
                    }
                }).notFound(response -> Option.none()).serverError(new Function<Response, Option<PluginStatus>>() {
                    @Nullable
                    @Override
                    public Option<PluginStatus> apply(Response response) {
                        throw httpRuntimeError(response, pluginKey, request);
                    }
                }).build());
    }


    public ResponsePromise updatePlugin(final String pluginKey, final String json) {
        checkNotNull(pluginKey, "pluginKey");

        try {
            return newRequest(upmPluginInformation(pluginKey)).setContentType(CONTENT_TYPE_PLUGIN_JSON).setEntity(json)
                    .put();
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Request.Builder newRequest(final URI uri) {
        final Header authenticateHeader = BasicScheme.authenticate(credentials, "UTF-8", false);
        return httpClient.newRequest(uri).setHeader(authenticateHeader.getName(), authenticateHeader.getValue());
    }

    private URI upmPluginInformation(final String pluginKey) {
        return URI.create(baseUrl + "/rest/plugins/1.0/" + pluginKey + "-key");
    }

    public void destroy() throws Exception {
        httpClientFactory.dispose(httpClient);
    }

    class VoidEventPublisher implements EventPublisher {
        @Override
        public void publish(final Object o) {
        }

        @Override
        public void register(final Object o) {
        }

        @Override
        public void unregister(final Object o) {
        }

        @Override
        public void unregisterAll() {
        }
    }

    private static RuntimeException httpRuntimeError(@Nullable Response response, String pluginKey, @Nullable Request request) {
        final String message = String.format("Unable to get status for plugin [%s]:%s\trequest: [%s]%s\tresponse: [%s]",
                pluginKey,
                System.lineSeparator(),
                print(request),
                System.lineSeparator(),
                print(response));
        return new RuntimeException(message);
    }

    private static String print(@Nullable Request req) {
        if (req == null) {
            return "null";
        }
        return String.format("request { method: %s, uri: %s, contentType: %s, accept: %s , entity : %s }",
                req.getMethod(), req.getUri(), req.getContentType(), req.getAccept(), req.getEntity());
    }

    private static String print(@Nullable Response res) {
        if (res == null) {
            return "null";
        }
        return String.format("response { statusCode: %s, entity: %s, contentType: %s, text: %s }",
                res.getStatusCode(), res.getEntity(), res.getContentType(), res.getStatusText());
    }
}