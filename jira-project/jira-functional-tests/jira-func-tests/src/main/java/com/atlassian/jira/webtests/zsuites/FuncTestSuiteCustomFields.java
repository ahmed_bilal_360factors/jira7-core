package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.atlassian.jira.webtests.ztests.admin.index.TestReindexMessages;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Some tests for custom fields.
 *
 * @since v4.0
 */
public class FuncTestSuiteCustomFields {

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.customfield", true))
                .add(TestReindexMessages.class)
                .build();
    }
}