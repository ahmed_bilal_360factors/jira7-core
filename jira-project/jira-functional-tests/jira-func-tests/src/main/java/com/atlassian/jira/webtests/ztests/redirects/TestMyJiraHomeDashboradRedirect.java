package com.atlassian.jira.webtests.ztests.redirects;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;

@WebTest({Category.FUNC_TEST, Category.DASHBOARDS})
public class TestMyJiraHomeDashboradRedirect extends BaseJiraFuncTest {

    @Test
    public void myJiraHomeShoudForwardToDashboard() {
        // when
        navigation.gotoPage("/secure/MyJiraHome.jspa");

        // then
        // if redirect is transformed to forward - func test will see same URL in navigation
        // (if redirect will happen URL will be changed)
        Assert.assertThat(
                navigation.getCurrentPage(),
                equalTo("/secure/MyJiraHome.jspa"));
        // but there will be dashboard page elements
        tester.assertElementPresent("dashboard");
    }
}
