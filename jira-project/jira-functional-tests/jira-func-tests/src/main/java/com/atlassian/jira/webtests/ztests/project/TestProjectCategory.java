package com.atlassian.jira.webtests.ztests.project;

import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.PROJECTS})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestProjectCategory extends BaseJiraFuncTest {
    private static final String CATEGORY_NAME = "New Project Category For Testing";
    private static final String CATEGORY_DESCRIPTION = "Testing for project category";


    @Inject
    private FuncTestLogger logger;

    @Test
    public void testAddProjectCategory() {
        logger.log("Project Category: Add project category");

        createProjectCategory(CATEGORY_NAME, CATEGORY_DESCRIPTION);
        tester.assertTextPresent(CATEGORY_NAME);
    }

    @Test
    public void testDeleteProjectCategory() {
        logger.log("Project Category: Delete project category");
        createProjectCategory(CATEGORY_NAME, CATEGORY_DESCRIPTION);

        deleteProjectCategory(CATEGORY_NAME);
        tester.assertTextNotPresent(CATEGORY_NAME);

    }

    @Test
    public void testPlaceProjectInProjectCategory() {
        logger.log("Project Category: Place a project in a project category");
        //create project
        final String projectKey = "CAT";
        final long projectId = backdoor.project().addProject("Project with category", projectKey, ADMIN_USERNAME);

        //assign to category
        createProjectCategory(CATEGORY_NAME, CATEGORY_DESCRIPTION);
        navigation.gotoPage("/secure/project/SelectProjectCategory!default.jspa?pid=" + projectId);
        tester.selectOption(ProjectService.PROJECT_CATEGORY_ID, CATEGORY_NAME);
        tester.submit("Select");

        assertThat(backdoor.project().getProjectCategoryName(projectKey), equalTo(CATEGORY_NAME));
    }

    @Test
    public void testAddDuplicateCategory() {
        logger.log("Project Category: Attempt to create a project category with a duplicate name");
        createProjectCategory(CATEGORY_NAME, CATEGORY_DESCRIPTION);

        //try to create duplicate
        createProjectCategory(CATEGORY_NAME, CATEGORY_DESCRIPTION);

        assertions.getJiraFormAssertions().assertFieldErrMsg("The project category '" + CATEGORY_NAME + "' already exists");
    }

    @Test
    public void testAddInvalidCategory() {
        logger.log("Project Category: Attempt to create a project category with an invalid name");
        createProjectCategory("", "");

        assertions.getJiraFormAssertions().assertFieldErrMsg("Please specify a name");
    }

    private void createProjectCategory(final String categoryName, final String categoryDescription) {
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CATEGORIES);
        tester.setFormElement("name", categoryName);
        tester.setFormElement("description", categoryDescription);
        tester.submit("Add");
    }

    private void deleteProjectCategory(final String categoryName) {
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CATEGORIES);
        tester.clickLink("del_" + categoryName);
        tester.submit("Delete");
    }

}
