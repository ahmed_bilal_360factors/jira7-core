package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.CommentClient;
import com.atlassian.jira.testkit.client.restclient.ConfigurationBean;
import com.atlassian.jira.testkit.client.restclient.ConfigurationClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.util.ResponseMatchers.hasStatusCode;
import static com.atlassian.jira.testkit.client.restclient.ConfigurationBean.TimeTrackingConfigurationBean.TimeFormat;
import static com.atlassian.jira.testkit.client.restclient.ConfigurationBean.TimeTrackingConfigurationBean.TimeTrackingUnit;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Test for the resource which returns JIRA configuration.
 *
 * @since v6.4
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@RestoreBlankInstance
public class TestConfigurationResource extends BaseJiraRestTest {
    private ConfigurationClient configurationClient;

    @Before
    public void setUpTest() {
        this.configurationClient = new ConfigurationClient(environmentData);
    }

    @Test
    public void noAccessByAnonymous() throws Exception {
        Response<ConfigurationBean> response = configurationClient.anonymous().getConfigurationResponse();

        assertThat(response, hasStatusCode(UNAUTHORIZED));
    }

    @Test
    public void accessibleWhenNormalUser() throws Exception {
        Response<ConfigurationBean> response = configurationClient.loginAs("fred").getConfigurationResponse();

        assertThat(response, hasStatusCode(OK));
    }

    @Test
    public void gettingJIRAConfiguration() {
        backdoor.attachments().disable();
        backdoor.issueLinking().disable();

        backdoor.subtask().enable();
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_TIMETRACKING, true);
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED, true);

        final ConfigurationBean configuration = configurationClient.getConfiguration();

        assertThat(configuration.attachmentsEnabled, is(equalTo(false)));
        assertThat(configuration.issueLinkingEnabled, is(equalTo(false)));

        assertThat(configuration.subTasksEnabled, is(equalTo(true)));
        assertThat(configuration.timeTrackingEnabled, is(equalTo(true)));
        assertThat(configuration.unassignedIssuesAllowed, is(equalTo(true)));
        assertThat(configuration.votingEnabled, is(equalTo(true)));
        assertThat(configuration.watchingEnabled, is(equalTo(true)));

        assertThat(configuration.timeTrackingConfiguration.defaultUnit, is(equalTo(TimeTrackingUnit.minute)));
        assertThat(configuration.timeTrackingConfiguration.timeFormat, is(equalTo(TimeFormat.pretty)));
        assertEquals(configuration.timeTrackingConfiguration.workingDaysPerWeek, 7.0d, Math.pow(10, -5));
        assertEquals(configuration.timeTrackingConfiguration.workingHoursPerDay, 24.0d, Math.pow(10, -5)); // :)
    }

    @Test
    public void timeTrackingDisabled() {
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_TIMETRACKING, false);

        final ConfigurationBean configuration = configurationClient.getConfiguration();

        assertThat(configuration.timeTrackingEnabled, is(equalTo(false)));
        assertThat(configuration.timeTrackingConfiguration, is(nullValue()));
    }
}
