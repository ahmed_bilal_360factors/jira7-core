package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.WebResponse;
import org.junit.Test;

import javax.inject.Inject;
import java.io.InputStream;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.matcher.ConditionClassMatcher.usesConditionClass;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.UPGRADE_TASKS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestUpgradeTask701 extends BaseJiraFuncTest {
    static final String XML_BACKUP = "TestUpgradeTask701.xml";

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testUpgradeTaskRemovesReferencesToOSUserGroupCondition() throws Exception {
        administration.restoreDataWithBuildNumber(XML_BACKUP, 700);

        WebResponse upgraded = tester.getDialog().getWebClient().getResponse(new GetMethodWebRequest(getEnvironmentData().getBaseUrl() + "/secure/admin/workflows/ViewWorkflowXml.jspa?workflowMode=live&workflowName=Workflow+that+uses+OSUserGroupCondition"));

        // make sure the condition class has been upgraded by looking at the workflow XML
        InputStream workflow = upgraded.getInputStream();
        assertThat(workflow, not(usesConditionClass("com.opensymphony.workflow.util.OSUserGroupCondition")));
        workflow.reset();
        assertThat(workflow, usesConditionClass("com.atlassian.jira.workflow.condition.UserInGroupCondition"));

        // now actually exercise the workflow. admin should be able to close the issue, fred shouldn't.
        String key = "HSP-1";
        navigation.issue().viewIssue(key);
        textAssertions.assertTextPresent(locator.css(".issueaction-workflow-transition"), "Close Issue");

        navigation.login("fred");
        textAssertions.assertTextNotPresent(locator.css(".issueaction-workflow-transition"), "Close Issue");
    }
}
