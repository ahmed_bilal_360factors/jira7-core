package com.atlassian.jira.functest.framework.assertions;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.functest.framework.util.SearchRendererValueResults;
import com.atlassian.jira.functest.framework.util.SearchResults;
import com.atlassian.jira.testkit.client.restclient.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Set of assertions related JQL
 *
 * @since v7.1
 */
public class JqlAssertions {

    private final Backdoor backdoor;

    @Inject
    public JqlAssertions(final Backdoor backdoor) {
        this.backdoor = backdoor;
    }

    public void assertFitsFilterForm(final String jqlQuery, final IssueNavigatorAssertions.FilterFormParam... formParams) {
        Response searchersResponse = backdoor.searchersClient().getSearchersResponse(jqlQuery);
        assertEquals(200, searchersResponse.statusCode);
    }

    public void assertTooComplex(final String jqlQuery) {
        Response searchersResponse = backdoor.searchersClient().getSearchersResponse(jqlQuery);
        assertEquals(400, searchersResponse.statusCode);
        assertEquals("jqlTooComplex", searchersResponse.entity.errorMessages.get(0));
    }

    public void assertInvalidContext(final String jqlQuery) {
        SearchResults searchers = backdoor.searchersClient().getSearchers(jqlQuery);
        SearchRendererValueResults values = searchers.values;
        boolean invalid = false;
        for (String s : values.keySet()) {
            if (!values.get(s).validSearcher) {
                invalid = true;
            }
        }
        assertTrue("Expected invalid searcher", invalid);
    }

    public void assertInvalidValue(final String jqlQuery) {
        SearchResults searchers = backdoor.searchersClient().getSearchers(jqlQuery);
        SearchRendererValueResults values = searchers.values;
        boolean invalid = false;
        for (String s : values.keySet()) {
            Document editHtml = Jsoup.parse(values.get(s).editHtml);
            if (editHtml.getElementsByClass("invalid_sel").size() > 0) {
                invalid = true;
                break;
            }
        }
        assertTrue("Expected invalid value", invalid);
    }
}
