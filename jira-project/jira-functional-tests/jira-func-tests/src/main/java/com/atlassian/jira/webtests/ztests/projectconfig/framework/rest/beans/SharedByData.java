package com.atlassian.jira.webtests.ztests.projectconfig.framework.rest.beans;

import com.atlassian.jira.webtests.ztests.projectconfig.framework.SimpleProject;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.List;
import java.util.Objects;

import static java.util.Collections.emptyList;

/**
 * A class used by beans that want to pass around {@link SimpleProject project} and
 * {@link String issue type} sharing information.
 *
 * @since v6.2
 */
@JsonAutoDetect
public class SharedByData {
    private List<SimpleProject> sharedWithProjects;

    private List<String> sharedWithIssueTypes;

    private long totalProjectsCount;

    public SharedByData() {
    }

    public SharedByData(final List<SimpleProject> allowedProjects, final List<String> issueTypes,
                        final long totalProjectsCount) {
        this.sharedWithProjects = allowedProjects;
        this.sharedWithIssueTypes = issueTypes;
        this.totalProjectsCount = totalProjectsCount;
    }

    public SharedByData(final List<SimpleProject> allowedProjects, final List<String> issueTypes) {
        this(allowedProjects, issueTypes, allowedProjects.size());
    }

    public SharedByData(final List<SimpleProject> projects, final long totalProjectsCount) {
        this(projects, emptyList(), totalProjectsCount);
    }

    public SharedByData(final List<SimpleProject> projects) {
        this(projects, projects.size());
    }

    public List<SimpleProject> getSharedWithProjects() {
        return sharedWithProjects;
    }

    public List<String> getSharedWithIssueTypes() {
        return sharedWithIssueTypes;
    }

    public long getHiddenProjectsCount() {
        return totalProjectsCount - sharedWithProjects.size();
    }

    public long getTotalProjectsCount() {
        return totalProjectsCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SharedByData that = (SharedByData) o;
        return totalProjectsCount == that.totalProjectsCount &&
                Objects.equals(sharedWithProjects, that.sharedWithProjects) &&
                Objects.equals(sharedWithIssueTypes, that.sharedWithIssueTypes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sharedWithProjects, sharedWithIssueTypes, totalProjectsCount);
    }
}
