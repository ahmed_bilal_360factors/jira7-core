package com.atlassian.jira.webtests.ztests.email;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BulkOperationProgress;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.navigation.IssueNavigatorNavigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.EmailBaseFuncTestCase;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.BULK_OPERATIONS, Category.EMAIL, Category.ISSUES})
@LoginAs(user = FunctTestConstants.ADMIN_USERNAME)
public class TestBulkDeleteIssuesNotifications extends EmailBaseFuncTestCase {

    @Inject
    protected TextAssertions textAssertions;
    @Inject
    private Administration administration;
    @Inject
    private BulkOperationProgress bulkOperationProgress;
    @Inject
    private LocatorFactory locator;

    @Test
    public void testBulkDeleteNoNoEmatifications() throws InterruptedException {
        administration.restoreData("TestBulkDeleteIssuesNotifications.xml");
        configureAndStartSmtpServer();
        bulkDeleteAllIssues(false, false);

        flushMailQueueAndWait(0);

        final MimeMessage[] mimeMessages = mailService.getReceivedMessages();
        assertEquals(0, mimeMessages.length);
    }

    @Test
    public void testBulkDeleteNotifications() throws InterruptedException, MessagingException {
        administration.restoreData("TestBulkDeleteIssuesNotifications.xml");
        configureAndStartSmtpServerWithNotify();
        bulkDeleteAllIssues(true, false);

        flushMailQueueAndWait(2);

        final MimeMessage[] mimeMessages = mailService.getReceivedMessages();
        assertEquals(2, mimeMessages.length);
        assertRecipientsHaveMessages(ImmutableList.of("admin@example.com", "fred@example.com"));
    }


    @Test
    public void testBulkDeleteSubtaskNoNotifications() throws InterruptedException {
        administration.restoreData("TestBulkDeleteIssuesNotificationsWithSubtasks.xml");
        configureAndStartSmtpServer();

        bulkDeleteAllIssues(false, true);

        flushMailQueueAndWait(0);

        final MimeMessage[] mimeMessages = mailService.getReceivedMessages();
        assertEquals(0, mimeMessages.length);
    }

    @Test
    public void testBulkDeleteSubtaskNotifications() throws InterruptedException, MessagingException {
        administration.restoreData("TestBulkDeleteIssuesNotificationsWithSubtasks.xml");
        configureAndStartSmtpServerWithNotify();

        bulkDeleteAllIssues(true, true);

        flushMailQueueAndWait(3);

        final MimeMessage[] mimeMessages = mailService.getReceivedMessages();
        assertEquals(3, mimeMessages.length);
        assertRecipientsHaveMessages(ImmutableList.of("admin@example.com", "fred@example.com"));

        final List<String> subjects = new ArrayList<>();
        final List<MimeMessage> adminMessages = getMessagesForRecipient("admin@example.com");
        for (final MimeMessage adminMessage : adminMessages) {
            subjects.add(adminMessage.getSubject());
        }

        assertThat(subjects, containsInAnyOrder("[JIRATEST] (HSP-2) This is my bug", "[JIRATEST] (HSP-4) Subtask1"));

        final List<MimeMessage> fredMessages = getMessagesForRecipient("fred@example.com");
        assertEmailSubjectEquals(fredMessages.get(0), "[JIRATEST] (HSP-3) This is fred's bug");
    }

    private void bulkDeleteAllIssues(final boolean sendMail, final boolean subtaskPresent) {
        //first lets try deleting issues without e-mail notifications
        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator().bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES);
        tester.checkCheckbox("bulkedit_10002", "on");
        tester.checkCheckbox("bulkedit_10001", "on");
        tester.submit("Next");
        tester.setFormElement("operation", "bulk.delete.operation.name");
        tester.submit("Next");
        //checkbox is checked by default.
        if (!sendMail) {
            tester.uncheckCheckbox("sendBulkNotification");
        }
        tester.submit("Next");
        if (!sendMail) {
            textAssertions.assertTextSequence(locator.page(), "Email notifications will", "NOT", "be sent for this update.");
        } else {
            tester.assertTextPresent("Email notifications will be sent for this update.");
        }
        if (subtaskPresent) {
            tester.assertTextPresent("Subtask1");
        } else {
            tester.assertTextNotPresent("Subtask1");
        }
        tester.submit("Confirm");
        bulkOperationProgress.waitAndReloadBulkOperationProgressPage();
        tester.assertElementNotPresent("issuetable");
    }

}