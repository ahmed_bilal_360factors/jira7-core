package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Dashboard;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.admin.plugins.ReferencePlugin;
import com.atlassian.jira.functest.framework.admin.user.DeleteUserPage;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.sharing.SharedEntityInfo;
import com.atlassian.jira.functest.framework.sharing.TestSharingPermissionUtils;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.regex.Pattern;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestDeleteUserSharedEntities extends BaseJiraFuncTest {
    private static final SharedEntityInfo SYSTEM_PAGE = new SharedEntityInfo(10000L, "System Dashboard", null, true, TestSharingPermissionUtils.createPublicPermissions());
    private static final SharedEntityInfo DASHBOARD_2 = new SharedEntityInfo(10011L, "Dashboard 2", null, true, TestSharingPermissionUtils.createPublicPermissions());
    private static final SharedEntityInfo DASHBOARD_FOR_ADMIN = new SharedEntityInfo(10010L, "Dashboard for " + ADMIN_FULLNAME, "Copy of 'System Dashboard'", true, TestSharingPermissionUtils.createPublicPermissions());

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("TestDeleteUserForSharedEntity.xml");
    }


    @Test
    public void testDeleteUser() {
        DeleteUserPage deleteUserPage = navigation.gotoPageWithParams(DeleteUserPage.class, DeleteUserPage.generateDeleteQueryParameters(FRED_USERNAME));
        assertThat(deleteUserPage.getNumberFromWarningFieldNamed(DeleteUserPage.SHARED_FILTERS), equalTo("3"));
        assertThat(deleteUserPage.getNumberFromWarningFieldNamedNoLink(DeleteUserPage.FAVORITED_FILTERS), equalTo("2"));
        assertThat(deleteUserPage.getNumberFromWarningFieldNamed(DeleteUserPage.SHARED_DASHBOARDS), equalTo("3"));
        assertThat(deleteUserPage.getNumberFromWarningFieldNamedNoLink(DeleteUserPage.FAVORITED_DASHBOARDS), equalTo("3"));
        deleteUserPage.clickDeleteUser();

        navigation.dashboard().navigateToPopular();

        SYSTEM_PAGE.setFavCount(1);
        DASHBOARD_2.setFavCount(1);
        DASHBOARD_FOR_ADMIN.setFavCount(1);

        assertions.getDashboardAssertions().assertDashboardPages(Arrays.asList(DASHBOARD_2, DASHBOARD_FOR_ADMIN, SYSTEM_PAGE), Dashboard.Table.POPULAR);

        navigation.manageFilters().popularFilters();

        textAssertions.assertTextPresent(new WebPageLocator(tester), "There are no filters in the system that you can view.");
    }

    @Test
    public void testPreDeleteUserErrorMessages() {
        ReferencePlugin referencePlugin = administration.plugins().referencePlugin();
        if (referencePlugin.isInstalled() && referencePlugin.isEnabled()) {
            String refUserName = "predeleteuser";
            navigation.login(ADMIN_USERNAME);
            administration.usersAndGroups().addUser(refUserName);
            DeleteUserPage deleteUserPage = navigation.gotoPageWithParams(DeleteUserPage.class, DeleteUserPage.generateDeleteQueryParameters(refUserName));
            assertThat(deleteUserPage.getUserDeletionError(), equalTo(deleteUserPage.getUserCannotBeDeleteMessage(refUserName)));
            assertThat(deleteUserPage.getNumberForPluginErrorNamed(Pattern.compile("(.*)Entity:(.*)")), equalTo("17"));
            navigation.logout();
        }
    }
}
