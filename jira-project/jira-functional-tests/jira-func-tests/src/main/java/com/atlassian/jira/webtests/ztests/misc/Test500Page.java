package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.framework.util.env.EnvironmentUtils;
import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.WebClient;
import com.meterware.httpunit.WebResponse;
import net.sourceforge.jwebunit.TestContext;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_USERNAME;
import static org.junit.Assert.assertFalse;

@WebTest({Category.FUNC_TEST, Category.BROWSING})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class Test500Page extends BaseJiraFuncTest {
    private boolean isBeforeJdk = false;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Inject
    private EnvironmentUtils environmentUtils;

    @Before
    public void setUpTest() {
        administration.restoreData("Test500Page.xml");
        // isBeforeJdk moves the current page to the system info page
        isBeforeJdk = environmentUtils.isJavaBeforeJdk15();
        navigation.gotoDashboard();
    }

    @Test
    public void test500PageServiceParamVisibility() {
        //check admins can see the service params
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        tester.gotoPage("/internal-error");

        //check listeners
        textAssertions.assertTextSequence(locator.page(), "ParamListeners", "com.atlassian.jira.event.listeners.DebugParamListener");
        textAssertions.assertTextSequence(locator.page(), "Param1", "paramKey");
        textAssertions.assertTextSequence(locator.page(), "Param2", "paramValue");

        //check services
        assertServiceHeaderPresent("Pop Service", "com.atlassian.jira.service.services.mail.MailFetcherService", "123");
        textAssertions.assertTextSequence(locator.page(), "popserver:", "fake server");
        textAssertions.assertTextSequence(locator.page(), "handler.params:", "project=hsp, issuetype=1, catchemail=sam@atlassian.com");
        textAssertions.assertTextSequence(locator.page(), "forwardEmail:", "fake@example.com");
        textAssertions.assertTextSequence(locator.page(), "handler:", "Create a new issue or add a comment to an existing issue");

        //check that non-logged in users cannot see the service params
        navigation.logout();
        setDevMode("false");
        tester.gotoPage("/internal-error");

        //check listeners
        textAssertions.assertTextNotPresent(locator.page(), "ParamListeners");
        textAssertions.assertTextNotPresent(locator.page(), "Param1:");
        textAssertions.assertTextNotPresent(locator.page(), "paramKey");
        textAssertions.assertTextNotPresent(locator.page(), "Param2:");
        textAssertions.assertTextNotPresent(locator.page(), "paramValue");

        //check services
        assertServiceHeaderNotPresent("Pop Service", "com.atlassian.jira.service.services.mail.MailFetcherService");
        assertServiceParamsNotVisible();

        //check that users with no global permission cannot see the service params
        navigation.login(BOB_USERNAME, BOB_PASSWORD);
        tester.gotoPage("/internal-error");

        //check listeners
        textAssertions.assertTextNotPresent(locator.page(), "ParamListeners");
        textAssertions.assertTextNotPresent(locator.page(), "Param1:");
        textAssertions.assertTextNotPresent(locator.page(), "paramKey");
        textAssertions.assertTextNotPresent(locator.page(), "Param2:");
        textAssertions.assertTextNotPresent(locator.page(), "paramValue");

        //check services
        assertServiceHeaderNotPresent("Pop Service", "com.atlassian.jira.service.services.mail.MailFetcherService");
        assertServiceParamsNotVisible();

        setDevMode("true");
    }

    private void setDevMode(String value) {
        backdoor.systemProperties().setProperty("jira.dev.mode", value);
        backdoor.systemProperties().setProperty("atlassian.dev.mode", value);
    }

    @Test
    public void test500PageIsShownToRegularUserInDevMode() {
        setDevMode("true");
        navigation.login(BOB_USERNAME, BOB_PASSWORD);
        tester.gotoPage("/internal-error");

        textAssertions.assertTextPresent(locator.page(), "Server ID");
        textAssertions.assertTextPresent(locator.page(), "File Paths");
        textAssertions.assertTextPresent(locator.page(), "JVM Input Arguments");
        textAssertions.assertTextPresent(locator.page(), "-Xmx");

    }

    @Test
    public void test500PageContainsMemoryAndInputArgsInfo() {
        tester.gotoPage("/internal-error");

        if (environmentUtils.isJvmWithPermGen()) {
            textAssertions.assertTextPresent(locator.page(), "Used PermGen Memory");
            textAssertions.assertTextPresent(locator.page(), "Free PermGen Memory");
        }
        textAssertions.assertTextPresent(locator.page(), "JVM Input Arguments");

        if (isBeforeJdk) {
            // Make sure the warning message are present
            textAssertions.assertTextPresent(locator.page(), "Unable to determine, this requires running JDK 1.5 and higher.");
        } else {
            // Make sure the warning message are not present
            textAssertions.assertTextNotPresent(locator.page(), "Unable to determine, this requires running JDK 1.5 and higher.");
        }
    }

    @Test
    public void testExternalUserManagement() {
        tester.gotoPage("/internal-error");
        textAssertions.assertTextPresent(locator.page(), "External user management");
    }

    /*
     * JRA-14105 inserting some escaped HTML in the command name of an action will show up on the 500 page in the
     * stack trace - need to ensure this is escaped to prevent XSS attacks
     */
    @Test
    public void testHtmlEscapingOfErrors() throws Exception {
        String badHtml = "<body onload=alert('XSSATTACK')>";
        String badHtmlEscaped = "%3Cbody%20onload=alert('XSSATTACK')%3E";

        GetMethodWebRequest request = new GetMethodWebRequest(getEnvironmentData().getBaseUrl() + "/secure/Dashboard!default" + badHtmlEscaped + ".jspa");
        final TestContext testContext = tester.getTestContext();
        final WebClient webClient = testContext.getWebClient();

        // set this flag so that test doesn't blow up when we get a 500 error
        // note: no need to reset this flag as it is automatically reset for the next test
        webClient.setExceptionsThrownOnErrorStatus(false);

        final WebResponse response = webClient.sendRequest(request);

        String responseText = response.getText();
        assertFalse("Found bad HTML in the response", responseText.indexOf(badHtml) >= 0);
    }

    @Test
    public void testAdministratorDoesntSeeContactYourAdmin() {
        // as admin
        tester.gotoPage("/internal-error");
        textAssertions.assertTextPresent(locator.page(), "Raise an issue for the Support team");
        textAssertions.assertTextNotPresent(locator.page(), "JIRA Administrator contact form");
    }

    @Test
    public void testNonAdministratorSeesContactYourAdmin() {
        navigation.login(BOB_USERNAME, BOB_PASSWORD);
        tester.gotoPage("/internal-error");
        textAssertions.assertTextPresent(locator.page(), "to your JIRA Administrator");
        textAssertions.assertTextNotPresent(locator.page(), "Raise an issue for the Support team");
    }

    @Test
    public void testSystemAdministratorCanSeeSysAdminOnlyProperties() {
        tester.gotoPage("/internal-error");
        textAssertions.assertTextNotPresent(locator.page(), "Contact your System Administrator to discover file path information.");
        textAssertions.assertTextNotPresent(locator.page(), "Contact your System Administrator to discover this property value.");
        textAssertions.assertTextSequence(locator.page(), "Server ID", "ABN9-RZYJ-WI2T-37UF");

        // assert that they can see the step 3 file path
        textAssertions.assertTextSequence(locator.page(), "server logs", "Support ZIP", " technical details");

        textAssertions.assertTextSequence(locator.page(), "File Paths", "atlassian-jira.log", "entityengine.xml");
        textAssertions.assertTextPresent(locator.page(), "JVM Input Arguments");
        if (!isBeforeJdk) {
            textAssertions.assertTextPresent(locator.page(), "-D");
        }
        textAssertions.assertTextPresent(locator.page(), "Current Working Directory");
    }

    @Test
    public void testNonSystemAdministratorDoesntSeeFilePaths() {
        setDevMode("false");
        try {
            navigation.login(BOB_USERNAME, BOB_PASSWORD);
            tester.gotoPage("/internal-error");

            // assert that they CANT see the step 3 file path
            textAssertions.assertTextNotPresent(locator.page(), "attach the application server log file");
            textAssertions.assertTextNotPresent(locator.page(), "atlassian-jira.log");
            textAssertions.assertTextNotPresent(locator.page(), "Server ID");
            textAssertions.assertTextNotPresent(locator.page(), "File Paths");
            textAssertions.assertTextNotPresent(locator.page(), "Current Working Directory");
            textAssertions.assertTextNotPresent(locator.page(), "JVM Input Arguments");


            textAssertions.assertTextNotPresent(locator.page(), "-Xmx"); // this shouldn't be present during tests for non sysadmin user
            navigation.login("admin_non_sysadmin", "admin_non_sysadmin");
            tester.gotoPage("/internal-error");
            textAssertions.assertTextNotPresent("File Paths");

            textAssertions.assertTextSequence(locator.page(),
                    "Server ID",
                    "ABN9-RZYJ-WI2T-37UF", // admins can see server ids
                    "Current Working Directory",
                    "Contact your System Administrator to discover this property value.",
                    "JVM Input Arguments",
                    "Contact your System Administrator to discover this property value.");
            textAssertions.assertTextNotPresent(locator.page(), "-Xmx"); // this shouldn't be present during tests for non sysadmin user
        } finally {
            setDevMode("true");
        }

    }

    private void assertServiceHeaderPresent(String serviceName, String serviceClass, String delay) {
        textAssertions.assertTextSequence(locator.page(), "Services", serviceName, serviceClass, "0 0 0/2 * * ?");
    }

    private void assertServiceHeaderNotPresent(String serviceName, String serviceClass) {
        textAssertions.assertTextNotPresent(serviceName);
        textAssertions.assertTextNotPresent(serviceClass);
    }

    private void assertServiceParamsNotVisible() {
        textAssertions.assertTextNotPresent(locator.page(), "usessl:");
        textAssertions.assertTextNotPresent(locator.page(), "No SSL");
        textAssertions.assertTextNotPresent(locator.page(), "popserver:");
        textAssertions.assertTextNotPresent(locator.page(), "fake server");
        textAssertions.assertTextNotPresent(locator.page(), "handler.params:");
        textAssertions.assertTextNotPresent(locator.page(), "project=hsp, issuetype=1, catchemail=sam@atlassian.com");
        textAssertions.assertTextNotPresent(locator.page(), "forwardEmail:");
        textAssertions.assertTextNotPresent(locator.page(), "fake@example.com");
        textAssertions.assertTextNotPresent(locator.page(), "handler:");
        textAssertions.assertTextNotPresent(locator.page(), "Create a new issue or add a comment to an existing issue");
    }
}
