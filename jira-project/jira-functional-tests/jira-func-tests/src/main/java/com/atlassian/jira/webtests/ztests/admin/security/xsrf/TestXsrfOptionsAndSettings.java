package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.EmailBaseFuncTestCase;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY, Category.SLOW_IMPORT})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestXsrfOptionsAndSettings extends EmailBaseFuncTestCase {
    @Inject
    private Form form;

    @Test
    public void testSendEmail() throws Exception {
        configureAndStartSmtpServer();

        new XsrfTestSuite(
                new XsrfCheck("SendBulkMail", new XsrfCheck.Setup() {
                    public void setup() {
                        navigation.gotoAdminSection(Navigation.AdminSection.SEND_EMAIL);
                        tester.checkCheckbox("sendToRoles", "false");
                        tester.selectOption("groups", "jira-users");
                        tester.setFormElement("subject", "I'm sending an email");
                        tester.setFormElement("message", "This is the message.");
                    }
                }, new XsrfCheck.FormSubmission("Send"))
        ).run(getTester(), navigation, form);

        flushMailQueueAndWait(2);
    }
}
