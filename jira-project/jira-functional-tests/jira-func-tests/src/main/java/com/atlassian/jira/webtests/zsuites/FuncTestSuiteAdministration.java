package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Test for the administration of the JIRA instance. This is a kind of catch all for those configuration tests that
 * don't need their own suite. Consider adding the tests to a more specific test suite if possible.
 *
 * @since v4.0
 */
public class FuncTestSuiteAdministration {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.admin", true))
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.admin.audit", true))
                .build();
    }
}