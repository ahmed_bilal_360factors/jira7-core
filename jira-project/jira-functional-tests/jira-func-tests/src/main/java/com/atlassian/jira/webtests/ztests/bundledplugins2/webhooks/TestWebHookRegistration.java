package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.assertions.WebHookAssert;
import com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookRegistrationClient;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.UserClient;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimaps;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import static com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookRegistrationClient.Registration;
import static com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookRegistrationClient.RegistrationResponse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@WebTest({Category.FUNC_TEST, Category.WEBHOOKS, Category.REFERENCE_PLUGIN})
public class TestWebHookRegistration extends BaseJiraFuncTest {
    private WebHookRegistrationClient client;

    @Before
    public void setUpTest() {
        this.client = new WebHookRegistrationClient(environmentData);
        backdoor.restoreBlankInstance();
    }

    @Test
    public void testCreateReadDelete() throws Exception {
        WebHookRegistrationClient client = new WebHookRegistrationClient(environmentData);

        Registration registration = new Registration();
        registration.name = "New Web Hook";
        registration.url = "http://localhost:12343/hook/" + Math.random();
        registration.events = new String[]{"jira:issue_updated"};
        registration.setFilterForIssueSection("type = Bug");

        final RegistrationResponse expectedResponse = new RegistrationResponse(registration);
        expectedResponse.lastUpdatedUser = "admin";
        expectedResponse.excludeBody = false;
        final UserClient uc = new UserClient(environmentData);
        expectedResponse.lastUpdatedDisplayName = uc.get(expectedResponse.lastUpdatedUser).displayName;
        expectedResponse.lastUpdated = System.currentTimeMillis();

        final RegistrationResponse registeredResponse = client.register(registration);
        final String id = new File(registeredResponse.self).getName();
        expectedResponse.self = environmentData.getBaseUrl().toString() + "/rest/webhooks/1.0/webhook/" + id;
        assertThat(registeredResponse.self, Matchers.startsWith(environmentData.getBaseUrl().toString()));
        assertEquals(expectedResponse, registeredResponse);

        final WebResource resource = client.createResource(registeredResponse.self);

        final List<RegistrationResponse> allWebHooks = client.getAllWebHooks();
        final ImmutableListMultimap<String, RegistrationResponse> index = Multimaps.index(allWebHooks, ID_FUNCTION);

        assertTrue(index.containsKey(id));
        final Registration fromAll = Iterables.getOnlyElement(index.get(id));
        assertEquals(expectedResponse, fromAll);

        final Registration webHook = client.getWebHook(id);
        assertEquals(expectedResponse, webHook);

        final Registration webHook2 = resource.get(RegistrationResponse.class);
        assertEquals(expectedResponse, webHook2);

        resource.delete();

        final ClientResponse noWebHook = resource.get(ClientResponse.class);
        assertEquals(HttpServletResponse.SC_NOT_FOUND, noWebHook.getStatus());
        noWebHook.close();

        final List<RegistrationResponse> allWebHooksAfterDelete = client.getAllWebHooks();
        final ImmutableListMultimap<String, RegistrationResponse> indexAfter = Multimaps.index(allWebHooksAfterDelete, ID_FUNCTION);

        assertFalse(indexAfter.containsKey(id));
    }

    @Test
    public void testRegisterVersionWebHook() throws Exception {
        WebHookRegistrationClient client = new WebHookRegistrationClient(environmentData);

        Registration registration = new Registration();
        registration.name = "New Web Hook";
        registration.url = "http://localhost:12343/hook/" + Math.random();
        registration.events = new String[]{"jira:version_released"};
        registration.setFilterForIssueSection("project = HSP");

        final RegistrationResponse expectedResponse = new RegistrationResponse(registration);
        expectedResponse.lastUpdatedUser = "admin";
        final UserClient uc = new UserClient(environmentData);
        expectedResponse.lastUpdatedDisplayName = uc.get(expectedResponse.lastUpdatedUser).displayName;
        expectedResponse.lastUpdated = System.currentTimeMillis();
        expectedResponse.excludeBody = false;

        final RegistrationResponse registeredResponse = client.register(registration);
        final String id = new File(registeredResponse.self).getName();
        expectedResponse.self = environmentData.getBaseUrl().toString() + "/rest/webhooks/1.0/webhook/" + id;
        assertThat(registeredResponse.self, Matchers.startsWith(environmentData.getBaseUrl().toString()));
        assertEquals(expectedResponse, registeredResponse);

        final WebResource resource = client.createResource(registeredResponse.self);

        final List<RegistrationResponse> allWebHooks = client.getAllWebHooks();
        final ImmutableListMultimap<String, RegistrationResponse> index = Multimaps.index(allWebHooks, ID_FUNCTION);

        assertTrue(index.containsKey(id));
        final Registration fromAll = Iterables.getOnlyElement(index.get(id));
        assertEquals(expectedResponse, fromAll);

        final Registration webHook = client.getWebHook(id);
        assertEquals(expectedResponse, webHook);

        final Registration webHook2 = resource.get(RegistrationResponse.class);
        assertEquals(expectedResponse, webHook2);

        resource.delete();

        final ClientResponse noWebHook = resource.get(ClientResponse.class);
        assertEquals(HttpServletResponse.SC_NOT_FOUND, noWebHook.getStatus());
        noWebHook.close();

        final List<RegistrationResponse> allWebHooksAfterDelete = client.getAllWebHooks();
        final ImmutableListMultimap<String, RegistrationResponse> indexAfter = Multimaps.index(allWebHooksAfterDelete, ID_FUNCTION);

        assertFalse(indexAfter.containsKey(id));
    }

    @Test
    public void testUpdate() throws InvocationTargetException, IllegalAccessException {
        backdoor.usersAndGroups().addUserEvenIfUserExists("new_admin", "new_admin", "New Admin", "newadmin@example.com");
        backdoor.usersAndGroups().addUserToGroup("new_admin", "jira-administrators");

        Registration registration = new Registration();
        registration.name = "Test Web Hook";
        registration.url = "http://localhost:12343/hook/" + Math.random();
        registration.events = new String[]{"jira:issue_updated"};
        registration.setFilterForIssueSection("type = Bug");

        final RegistrationResponse registeredResponse = client.register(registration);
        final String id = new File(registeredResponse.self).getName();

        // update with the same attributes
        client.update(id, registration);
        RegistrationResponse response = client.getWebHook(id);
        assertEquals(expectedResponse(registration, id), response);

        registration.name = "New Name Web Hook";
        final RegistrationResponse updateResponse = client.loginAs("new_admin").update(id, registration);
        assertEquals(expectedResponse(registration, id, "new_admin"), updateResponse);
        response = client.getWebHook(id);
        assertEquals(expectedResponse(registration, id, "new_admin"), response);

        // update with the Response object - makes UI simpler
        response.name = "Changed with reused object";
        final ClientResponse put = client.createResource(registeredResponse.self).type(MediaType.APPLICATION_JSON_TYPE).put(ClientResponse.class, new Registration(response));
        assertEquals(Response.Status.Family.SUCCESSFUL, put.getClientResponseStatus().getFamily());
        put.close();
        response = client.getWebHook(id);
        assertEquals("Changed with reused object", response.name);

        final String genericTestHookUrl = "http://localhost:12343/genericTestHook" + Math.random();
        registration.url = genericTestHookUrl;
        client.loginAs("admin").update(id, registration);
        response = client.getWebHook(id);
        assertEquals(expectedResponse(registration, id), response);

        registration.events = new String[]{"jira:issue_updated"};
        client.update(id, registration);
        response = client.getWebHook(id);
        assertEquals(expectedResponse(registration, id), response);

        client.update(id, registration);
        response = client.getWebHook(id);
        assertEquals(expectedResponse(registration, id), response);

        final Registration invalidRegistration = new Registration();
        invalidRegistration.name = "Test Web Hook 2";
        final String hookUpdateUrl = "http://localhost:12343/hookUpdate/" + Math.random();
        invalidRegistration.url = hookUpdateUrl;
        invalidRegistration.events = new String[]{"jira:issue_updated"};

        final RegistrationResponse registeredOtherResponse = client.register(invalidRegistration);
        final String otherId = new File(registeredOtherResponse.self).getName();

        // registration update matches existing webhook now
        invalidRegistration.url = genericTestHookUrl;
        invalidRegistration.events = new String[]{"jira:issue_updated"};
        invalidRegistration.filters = updateResponse.filters; // we need to send the same parameters to get 409
        invalidRegistration.excludeBody = updateResponse.excludeBody;

        WebHookAssert.assertUniformInterfaceException(Response.Status.CONFLICT, () -> {
            client.updateStatus(otherId, invalidRegistration);
            return null;
        });

        // check whether webhook can be updated with an event used in other webhook
        invalidRegistration.url = hookUpdateUrl;
        invalidRegistration.events = new String[]{"jira:issue_updated"};
        final RegistrationResponse registrationResponse = client.update(otherId, invalidRegistration);
        assertEquals(expectedResponse(invalidRegistration, otherId), registrationResponse);
    }

    @Test
    public void testDeleteWebHookWithAssociatedTransition() {
        backdoor.restoreDataFromResource("TestWebHookPostFunction.zip");

        assertNotNull(client.getWebHook("1"));
        assertNotNull(client.getWebHook("2"));

        WebHookAssert.assertUniformInterfaceException(Response.Status.CONFLICT, () -> {
            client.delete("1");
            return null;
        });

        WebHookAssert.assertUniformInterfaceException(Response.Status.CONFLICT, () -> {
            assertNotNull(client.getWebHook("2"));
            client.delete("2");
            return null;
        });
    }

    private RegistrationResponse expectedResponse(Registration registration, String id)
            throws InvocationTargetException, IllegalAccessException {
        return expectedResponse(registration, id, "admin");
    }

    private RegistrationResponse expectedResponse(Registration registration, String id, String username)
            throws InvocationTargetException, IllegalAccessException {
        final RegistrationResponse expectedResponse = new RegistrationResponse(registration);
        expectedResponse.lastUpdatedUser = username;

        final UserClient uc = new UserClient(environmentData);
        expectedResponse.lastUpdatedDisplayName = uc.get(expectedResponse.lastUpdatedUser).displayName;
        expectedResponse.lastUpdated = System.currentTimeMillis();
        expectedResponse.self = environmentData.getBaseUrl().toString() + "/rest/webhooks/1.0/webhook/" + id;
        expectedResponse.excludeBody = false;
        return expectedResponse;
    }

    private static final Function<RegistrationResponse, String> ID_FUNCTION = input -> new File(input.self).getName();
}
