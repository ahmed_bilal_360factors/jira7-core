package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.REST})
public class TestProjectCategoryResource extends BaseJiraRestTest {
    private static final String SELF = "self";
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String DESCRIPTION = "description";

    public static final String CATEGORY_NAME = "category_one";
    public static final String CATEGORY_DESCRIPTION = "description of category one";

    public static final String CATEGORY_NAME_2 = "category_two";
    public static final String CATEGORY_DESCRIPTION_2 = "description of category two";
    public static final String BUSINESS = "business";

    private ProjectCategoryClient preferencesClient;

    @Before
    public void setUpTest() {
        preferencesClient = new ProjectCategoryClient(environmentData);
        backdoor.restoreBlankInstance();
    }

    @After
    public void tearDownTest() {
        preferencesClient.cleanUp();
    }

    @Test
    public void emptyAtFirstList() throws Exception {
        //there are no project categories at first
        final ClientResponse response = preferencesClient.getAllCategories();

        assertThat(response, hasStatus(Status.OK));
        assertThat(getEntityCollection(response), empty());
    }

    @Test
    public void retrievingNonExistingCategoryFails() {
        //retrieving not existing category will fail
        final ClientResponse response = preferencesClient.getCategory("0");
        assertThat(response, hasStatus(Status.NOT_FOUND));
    }

    @Test
    public void createCategoryWorks() throws Exception {
        //create new category
        final ClientResponse response = preferencesClient.createCategory(CATEGORY_NAME, CATEGORY_DESCRIPTION);
        final Map<String, String> entity = getEntity(response);

        assertThat(response, hasStatus(Status.CREATED));
        assertThat(entity, hasKey(SELF));
        assertThat(entity, hasKey(ID));
        assertThat(entity, hasEntry(NAME, CATEGORY_NAME));
        assertThat(entity, hasEntry(DESCRIPTION, CATEGORY_DESCRIPTION));
    }

    @Test
    public void getCategoryWorks() throws Exception {
        String categoryId = withCreatedCategory(CATEGORY_NAME, CATEGORY_DESCRIPTION);

        //retrieving one of created categories by id
        final ClientResponse response = preferencesClient.getCategory(categoryId);
        final Map<String, String> entity = getEntity(response);

        assertThat(response, hasStatus(Status.OK));
        assertThat(entity, hasKey(SELF));
        assertThat(entity, hasEntry(ID, categoryId));
        assertThat(entity, hasEntry(NAME, CATEGORY_NAME));
        assertThat(entity, hasEntry(DESCRIPTION, CATEGORY_DESCRIPTION));
    }

    @Test
    public void deletingWorks() throws Exception {
        final ClientResponse response = preferencesClient.createCategory(CATEGORY_NAME, CATEGORY_DESCRIPTION);
        final Map<String, String> entity = getEntity(response);

        //deleting second category
        final ClientResponse response2 = preferencesClient.deleteCategory(entity.get(ID));
        assertThat(response2, hasStatus(Status.NO_CONTENT));
    }

    @Test
    public void deletingNonExistentFails() throws Exception {
        //deleting second category again should fail
        final ClientResponse response = preferencesClient.deleteCategory(ID);
        assertThat(response, hasStatus(Status.NOT_FOUND));
    }

    @Test
    public void listCategoriesWorks() throws Exception {
        String categoryId = withCreatedCategory(CATEGORY_NAME, CATEGORY_DESCRIPTION);
        withCreatedCategory(CATEGORY_NAME_2, CATEGORY_DESCRIPTION_2);

        //now retrieving all categories will return all of them
        final ClientResponse listResponse = preferencesClient.getAllCategories();
        assertThat(listResponse, hasStatus(Status.OK));

        final Collection<Object> listEntity = getEntityCollection(listResponse);
        assertThat(listEntity, hasSize(2));

        assertDeleted(categoryId);

        //now retrieving all categories will return one less
        final ClientResponse listResponse2 = preferencesClient.getAllCategories();
        assertThat(listResponse2, hasStatus(Status.OK));

        final Collection<Object> listEntity2 = getEntityCollection(listResponse2);
        assertThat(listEntity2, hasSize(1));
    }

    @Test
    public void testCannotCreateTwoCategoriesWithTheSameName() throws Exception {
        withCreatedCategory(CATEGORY_NAME, CATEGORY_DESCRIPTION);
        final ClientResponse createResponse = preferencesClient.createCategory(CATEGORY_NAME, CATEGORY_DESCRIPTION);

        assertThat(createResponse.getStatus(), equalTo(Status.CONFLICT.getStatusCode()));
    }

    @SuppressWarnings("unchecked")
    private Map<String, String> getEntity(final ClientResponse response) {
        return response.getEntity(Map.class);
    }

    @SuppressWarnings("unchecked")
    private Collection<Object> getEntityCollection(final ClientResponse response) {
        return response.getEntity(Collection.class);
    }

    private String withCreatedCategory(final String name, final String description) {
        final ClientResponse response = preferencesClient.createCategory(name, description);
        final Map<String, String> entity = getEntity(response);

        assertThat(response, hasStatus(Status.CREATED));
        return entity.get(ID);
    }

    private void assertDeleted(String categoryId) {
        final ClientResponse response = preferencesClient.deleteCategory(categoryId);
        assertThat(response, hasStatus(Status.NO_CONTENT));
    }

    private Matcher<ClientResponse> hasStatus(Status status) {
        return new TypeSafeMatcher<ClientResponse>() {
            @Override
            public void describeTo(final Description description) {
                description.appendValue(status);
            }

            @Override
            protected boolean matchesSafely(final ClientResponse response) {
                return response.getStatus() == status.getStatusCode();
            }
        };
    }
}
