package com.atlassian.jira.webtests.ztests.navigator.jql.changehistory.priority;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.ChangeHistoryAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Set;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * @since v4.4
 */
@WebTest({Category.FUNC_TEST, Category.JQL, Category.CHANGE_HISTORY})
@Restore("TestChangeHistorySearch.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestPriorityWasSearching extends BaseJiraFuncTest {
    private static final String FIELD_NAME = "priority";
    private static final String[] ALL_ISSUES = {"HSP-9", "HSP-8", "HSP-7", "HSP-6", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1"};

    @Before
    public void setUpSearchLayout() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Inject
    private ChangeHistoryAssertions changeHistoryAssertions;

    @Test
    public void testWasEmptySearch() {
        //priority can never be empty
        changeHistoryAssertions.assertWasEmptySearchReturnsEmptyValuesUsingEmptyKeyword(FIELD_NAME);
    }

    @Test
    public void testWasNotEmptySearch() {
        changeHistoryAssertions.assertWasNotEmptySearchReturnsNotEmptyValuesWithEmptyKeyword(FIELD_NAME, ALL_ISSUES);
    }

    @Test
    public void testWasSearchUsingSingleValueOperandsReturnsExpectedValues() {
        changeHistoryAssertions.assertWasSearchReturnsExpectedValues(FIELD_NAME, "major", ALL_ISSUES);
        changeHistoryAssertions.assertWasSearchReturnsExpectedValues(FIELD_NAME, "minor", "HSP-7", "HSP-2");
        changeHistoryAssertions.assertWasSearchReturnsExpectedValues(FIELD_NAME, "catastrophic", "HSP-7", "HSP-1");
    }

    @Test
    public void testWasSearchUsingListOperands() {
        Set priorities = Sets.newHashSet("major", "minor");
        changeHistoryAssertions.assertWasInSearchReturnsExpectedValues(FIELD_NAME, priorities, ALL_ISSUES);
        priorities = Sets.newHashSet("minor", "catastrophic");
        changeHistoryAssertions.assertWasInSearchReturnsExpectedValues(FIELD_NAME, priorities, "HSP-7", "HSP-2", "HSP-1");
    }

    @Test
    public void testWasNotInSearchUsingListOperands() {
        final Set priorities = Sets.newHashSet("minor", "catastrophic");
        final String[] expected = {"HSP-9", "HSP-8", "HSP-6", "HSP-5", "HSP-4", "HSP-3"};
        changeHistoryAssertions.assertWasNotInSearchReturnsExpectedValues(FIELD_NAME, priorities, expected);
    }

    @Test
    public void testWasSearchForRenamedConstant() {
        changeHistoryAssertions.assertWasSearchReturnsExpectedValues(FIELD_NAME, "critical", "HSP-7");
        changeHistoryAssertions.assertWasSearchReturnsExpectedValues(FIELD_NAME, "catastrophic", "HSP-7", "HSP-1");
    }


    @Test
    public void testWasSearchUsingByPredicate() {
        String[] expected = {"HSP-8", "HSP-7", "HSP-6", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1"};
        changeHistoryAssertions.assertWasBySearchReturnsExpectedValues(FIELD_NAME, "major", "admin", expected);
        expected = new String[]{"HSP-1"};
        changeHistoryAssertions.assertWasBySearchReturnsExpectedValues(FIELD_NAME, "catastrophic", "fred", expected);
        changeHistoryAssertions.assertWasBySearchUsingListOperandsReturnsExpectedValues(FIELD_NAME, "catastrophic", Sets.newHashSet("fred", "bob"), "HSP-1");
    }

    @Test
    public void testWasSearchUsingDuringPredicate() {
        final String[] expected = {"HSP-9", "HSP-8", "HSP-6", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1"};
        changeHistoryAssertions.assertWasDuringSearchReturnsExpectedValues(FIELD_NAME, "major", "'2011/05/30'", "'2011/06/02'", expected);
    }

    @Test
    public void testWasSearchUsingBeforePredicate() {
        changeHistoryAssertions.assertWasBeforeSearchReturnsExpectedValues(FIELD_NAME, "major", "'2011/06/01'", ALL_ISSUES);
        final String[] expected = {"HSP-4", "HSP-3", "HSP-2", "HSP-1"};
        changeHistoryAssertions.assertWasBeforeSearchReturnsExpectedValues(FIELD_NAME, "major", "'2010/06/02'", expected);
    }

    @Test
    public void testWasSearchUsingAfterPredicate() {
        changeHistoryAssertions.assertWasAfterSearchReturnsExpectedValues(FIELD_NAME, "major", "'2011/05/01'", ALL_ISSUES);
        final String[] expected = {"HSP-9", "HSP-8", "HSP-6", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1"};
        changeHistoryAssertions.assertWasAfterSearchReturnsExpectedValues(FIELD_NAME, "major", "'2011/06/01 10:55'", expected);
    }

    @Test
    public void testWasSearchUsingOnPredicate() {
        changeHistoryAssertions.assertWasOnSearchReturnsExpectedValues(FIELD_NAME, "major", "'2011/05/01'", ALL_ISSUES);
        final String[] expected = new String[]{"HSP-9", "HSP-8", "HSP-6", "HSP-5", "HSP-4", "HSP-3", "HSP-2", "HSP-1"};
        changeHistoryAssertions.assertWasOnSearchReturnsExpectedValues(FIELD_NAME, "major", "'2011/06/01'", expected);
    }

    @Test
    public void testWasSearchUsingLongOperandsIsInvalid() {
        // invalid id
        final String expectedError = "A value with ID '7' does not exist for the field 'priority'.";
        changeHistoryAssertions.assertInvalidSearchProducesError(FIELD_NAME, "7", "", expectedError);
    }

    @Test
    public void testWasSearchUsingLongOperandsIsValid() {
        // valid id
        changeHistoryAssertions.assertWasSearchReturnsExpectedValues(FIELD_NAME, "4", "HSP-7", "HSP-2");
    }

    @Test
    public void testWasSearchUsingUnclosedListIsInvalid() {
        // invalid list
        final String expectedError = "Error in the JQL Query: Expecting ')' before the end of the query.";
        changeHistoryAssertions.assertInvalidSearchProducesError(FIELD_NAME, "(major, minor", "", expectedError);
    }

    @Test
    public void testWasSearchUsingIcorrectPriorityIsInvalid() {
        // invalid list
        final String expectedError = "The value 'urgent' does not exist for the field 'priority'.";
        changeHistoryAssertions.assertInvalidSearchProducesError(FIELD_NAME, "urgent", "", expectedError);
    }

    @Test
    public void testWasSearchUsingIncorrectPredicateIsInvalid() {
        // invalid predicate
        final String expectedError = "Error in the JQL Query: Expecting either 'OR' or 'AND' but got 'at'. (line 1, character 29)";
        changeHistoryAssertions.assertInvalidSearchProducesError(FIELD_NAME, "(major, minor)", "at '10:55'", expectedError);
    }

    @Test
    public void testWasSearchUsingIncorrectFunctionIsInvalid() {
        // invalid operand  type
        final String expectedError = "A value provided by the function 'currentLogin' is invalid for the field 'priority'.";
        changeHistoryAssertions.assertInvalidSearchProducesError(FIELD_NAME, "currentLogin()", "", expectedError);
    }
}