package com.atlassian.jira.webtests.ztests.attachment;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FS;

/**
 * @since v3.12
 */
@WebTest({Category.FUNC_TEST, Category.ATTACHMENTS, Category.BROWSING})
@Restore("TestEditAttachmentSettings.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestEditAttachmentSettings extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testEditAttachmentSettingsValidation() {
        navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);
        tester.clickLinkWithText("Edit Settings");

        tester.checkCheckbox("attachmentPathOption", "DEFAULT");
        tester.setFormElement("attachmentSize", "");
        tester.setFormElement("thumbnailsEnabled", "true");
        tester.submit("Update");
        //
        tester.assertTextPresent("Please specify the attachment size.");

        tester.checkCheckbox("attachmentPathOption", "DEFAULT");
        tester.setFormElement("attachmentSize", "-1");
        tester.setFormElement("thumbnailsEnabled", "true");
        tester.submit("Update");
        tester.assertTextPresent("Attachment size must be a positive number.");

        tester.checkCheckbox("attachmentPathOption", "DEFAULT");
        tester.setFormElement("attachmentSize", "0");
        tester.setFormElement("thumbnailsEnabled", "true");
        tester.submit("Update");
        tester.assertTextPresent("Attachment size must be a positive number.");

        tester.checkCheckbox("attachmentPathOption", "DEFAULT");
        tester.setFormElement("attachmentSize", "2147483648"); //Integer.MAX_VALUE + 1
        tester.setFormElement("thumbnailsEnabled", "true");
        tester.submit("Update");
        tester.assertTextPresent("Attachment size must be a number between 1 to 2147483647");

        tester.checkCheckbox("attachmentPathOption", "DEFAULT");
        tester.setFormElement("attachmentSize", "9999999999");
        tester.setFormElement("thumbnailsEnabled", "true");
        tester.submit("Update");
        tester.assertTextPresent("Attachment size must be a number between 1 to 2147483647");

        tester.checkCheckbox("attachmentPathOption", "DISABLED");
        tester.setFormElement("attachmentSize", "1");
        tester.setFormElement("thumbnailsEnabled", "true");
        tester.submit("Update");
        tester.assertTextPresent("Attachments must be enabled to enable thumbnails.");

        tester.checkCheckbox("attachmentPathOption", "DISABLED");
        tester.setFormElement("attachmentSize", "1");
        tester.setFormElement("thumbnailsEnabled", "false");
        tester.setFormElement("zipSupport", "true");
        tester.submit("Update");
        tester.assertTextPresent("Attachments must be enabled to enable ZIP support.");
    }


    @Test
    public void testSizeIsNotUpdatedIfAttachmentsDisabled() throws SAXException {
        navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);
        tester.clickLinkWithText("Edit Settings");

        //set the attachment size to a known size
        tester.checkCheckbox("attachmentPathOption", "DEFAULT");
        tester.setFormElement("attachmentSize", "12345");
        tester.submit("Update");
        tester.assertTextPresent("12 kB");

        tester.clickLinkWithText("Edit Settings");

        //disable attachments and change the attachment size
        tester.checkCheckbox("attachmentPathOption", "DISABLED");
        tester.setFormElement("attachmentSize", "54321");
        tester.setFormElement("thumbnailsEnabled", "false");
        tester.setFormElement("zipSupport", "false");
        tester.submit("Update");
        tester.assertTextPresent("12 kB"); //the attachment size should not have been updated

        //go back to the edit page and assert the size value has not changed
        tester.clickLinkWithText("Edit Settings");
        tester.assertTextPresent("12345");
        tester.assertTextNotPresent("54321");
    }

    @Test
    public void testEditAttachmentSettingsJiraHome() {
        navigation.logout();
        navigation.login("sysadmin", "sysadmin");
        // this uses the default path (i.e. jira home)
        administration.attachments().enable("2940");
        navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);
        tester.clickLinkWithText("Edit Settings");
        tester.setFormElement("thumbnailsEnabled", "true");
        tester.submit("Update");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Allow Attachments", "ON", "Attachment Path", "Default Directory", "Attachment Size", "3 kB", "Enable Thumbnails", "ON"});
    }

    @Test
    public void testEditAttachmentSettingsWithGlobalAdmin() {
        String attachmentPath = getEnvironmentData().getWorkingDirectory().getAbsolutePath() + FS + "attachments";

        //disable attachments so that the global admin does not have a link to the edit page
        navigation.logout();
        navigation.login("sysadmin", "sysadmin");
        navigation.gotoAdmin();
        administration.attachments().enable("8765");//enable to set the attachment size (so size that value of size is known)
        navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);
        tester.clickLinkWithText("Edit Settings");
        tester.checkCheckbox("attachmentPathOption", "DISABLED");
        tester.setFormElement("thumbnailsEnabled", "false");
        tester.setFormElement("zipSupport", "false");
        tester.submit("Update");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Allow Attachments", "OFF", "Attachment Path", "Attachment Size", "9 kB", "Enable Thumbnails", "OFF"});

        //check global admin cannot see the edit config link
        navigation.logout();
        navigation.login("globaladmin", "globaladmin");
        navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);
        tester.assertTextPresent("You can not enable thumbnails unless attachments are enabled.");
        tester.assertLinkNotPresentWithText("Edit Settings");

        //goto the edit page directly as the global admin and verify cannot change system admin options
        tester.gotoPage("/secure/admin/jira/EditAttachmentSettings!default.jspa");
        tester.assertFormElementNotPresent("attachmentPathOption"); //should be hidden as only system admin can update this field
        tester.assertFormElementNotPresent("attachmentPath"); //should be hidden as only system admin can update this field
        tester.setFormElement("attachmentSize", "5678");
        tester.setFormElement("thumbnailsEnabled", "true"); //try to enable thumbnails
        tester.submit("Update");
        tester.assertTextPresent("Attachments must be enabled to enable thumbnails."); //verify thumbnails cannot be enabled
        tester.setFormElement("thumbnailsEnabled", "false"); //set to false to check attachment size is not updated
        tester.submit("Update");
        //check its still off and that size hasnt changed
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Allow Attachments", "OFF", "Attachment Path", "Attachment Size", "9 kB", "Enable Thumbnails", "OFF"});

        //enable attachments so that global admins can modify the size and enable thumbnails
        navigation.logout();
        navigation.login("sysadmin", "sysadmin");
        navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);
        tester.clickLinkWithText("Edit Settings");
        tester.checkCheckbox("attachmentPathOption", "DEFAULT");
        tester.setFormElement("attachmentSize", "1234");
        tester.setFormElement("thumbnailsEnabled", "false");
        tester.submit("Update");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Allow Attachments", "ON", "Attachment Size", "1 kB", "Enable Thumbnails", "OFF"});

        //check global admins can now edit part of the attachment settings
        navigation.logout();
        navigation.login("globaladmin", "globaladmin");
        navigation.gotoAdminSection(Navigation.AdminSection.ATTACHMENTS);
        tester.assertLinkPresentWithText("Edit Settings");
        tester.clickLinkWithText("Edit Settings");
        tester.assertFormElementNotPresent("attachmentPathOption"); //should be hidden as only system admin can update this field
        tester.assertFormElementNotPresent("attachmentPath"); //should be hidden as only system admin can update this field
        tester.setFormElement("attachmentSize", "3456");
        tester.setFormElement("thumbnailsEnabled", "true");
        tester.submit("Update");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Allow Attachments", "ON", "Attachment Size", "3 kB", "Enable Thumbnails", "ON"});
    }

    @Test
    public void testEditAttachmentSize() {
        assertAttachmentSize(1, "0.0 kB");
        assertAttachmentSize(1024, "1.0 kB");
        assertAttachmentSize(1024 * 2, "2 kB");
        assertAttachmentSize(1024 * 1024, "1024 kB");
        assertAttachmentSize(1024 * 1024 + 1, "1.00 MB");
        assertAttachmentSize(1024 * 1024 * 155 / 100, "1.55 MB");
        assertAttachmentSize(1024 * 1024 * 10, "10.00 MB");
        assertAttachmentSize(Integer.MAX_VALUE, "2,048.00 MB");
    }

    private void assertAttachmentSize(int maxSizeInBytes, String maxSizePrettyFormat) {
        administration.attachments().enable(String.valueOf(maxSizeInBytes));
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Allow Attachments", "ON", "Attachment Path", "Attachment Size", maxSizePrettyFormat, "Enable Thumbnails"});

        navigation.issue().goToCreateIssueForm(null, null);
        tester.assertTextPresent("The maximum file upload size is " + maxSizePrettyFormat + ".");

        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("attach-file");
        tester.assertTextPresent("The maximum file upload size is " + maxSizePrettyFormat + ".");
    }
}
