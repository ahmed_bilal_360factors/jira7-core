package com.atlassian.jira.functest.framework;

import com.google.common.collect.ImmutableList;
import com.meterware.httpunit.Button;
import com.meterware.httpunit.WebForm;
import net.sourceforge.jwebunit.UnableToSetFormException;
import net.sourceforge.jwebunit.WebTester;
import net.sourceforge.jwebunit.util.ExceptionUtility;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class FormImpl implements Form {
    private WebTester webTester;

    @Inject
    public FormImpl(final WebTester webTester) {
        this.webTester = webTester;
    }

    public void selectOption(final String selectName, final String option) {
        webTester.assertFormElementPresent(selectName);
        webTester.selectOption(selectName, option);
    }

    @Override
    public void selectOptionsByDisplayName(final String selectName, final String option, final String... moreOptions) {
        selectOptionsByDisplayName(selectName, join(option, moreOptions));
    }

    public void selectOptionsByDisplayName(final String selectName, final String[] options) {
        webTester.assertFormElementPresent(selectName);

        checkFormStateWithParameter(selectName);

        if (options.length > 0) {
            webTester.getDialog().getForm().setParameter(selectName, getValuesForOptions(selectName, options));
        } else {
            webTester.getDialog().getForm().removeParameter(selectName);
        }
    }

    @Override
    public void selectOptionsByValue(final String selectName, final String option, final String... moreOptions) {
        selectOptionsByValue(selectName, join(option, moreOptions));
    }

    public void selectOptionsByValue(final String selectName, final String[] options) {
        webTester.assertFormElementPresent(selectName);

        checkFormStateWithParameter(selectName);

        if (options.length > 0) {
            webTester.getDialog().getForm().setParameter(selectName, options);
        } else {
            webTester.getDialog().getForm().removeParameter(selectName);
        }
    }

    /**
     * Converts a list of select box option labels to the underlying values.
     *
     * @param selectName name of the select box.
     * @param options    labels of the options.
     * @return the list of values for these labels.
     */
    private String[] getValuesForOptions(final String selectName, final String[] options) {
        final String[] values = new String[options.length];
        for (int i = 0; i < options.length; i++) {
            final String option = options[i];
            // Get the value for the label of the option of the select box.
            values[i] = webTester.getDialog().getValueForOption(selectName, option);
        }
        return values;
    }

    private void checkFormStateWithParameter(final String paramName) {
        if (webTester.getDialog().getForm() == null) {
            try {
                webTester.getDialog().setWorkingForm(getFormWithParameter(paramName).getID());
            } catch (final UnableToSetFormException e) {
                throw new UnableToSetFormException("Unable to set form based on parameter [" + paramName + "].");
            }
        }
    }

    private WebForm getFormWithParameter(final String paramName) {
        for (int i = 0; i < getForms().length; i++) {
            final WebForm webForm = getForms()[i];
            final String[] names = webForm.getParameterNames();
            for (final String name : names) {
                if (name.equals(paramName)) {
                    return webForm;
                }
            }
        }
        return null;
    }

    public WebForm[] getForms() {
        try {
            return webTester.getDialog().getResponse().getForms();
        } catch (final SAXException ex) {
            throw new RuntimeException("Unable to get forms from the WebResponse.", ex);
        }
    }

    @Override
    public WebForm switchTo(final String formId) {
        webTester.setWorkingForm(formId);
        return webTester.getDialog().getForm();
    }

    private String[] join(final String option, final String[] moreOptions) {
        final ImmutableList<String> list = ImmutableList.<String>builder().add(option).add(moreOptions).build();
        return list.toArray(new String[list.size()]);
    }

    /**
     * Click only a single button with the specific value.
     * If more than one button found with that value it will barf.
     *
     * @param value the value attribute of the of the button element to click.
     */
    @Override
    public void clickButtonWithValue(final String value) {
        assertButtonPresentWithValue(value);
        Button buttonToClick = null;
        final Button[] buttons = webTester.getDialog().getForm().getButtons();
        for (final Button button : buttons) {
            if (value.equals(button.getValue())) {
                buttonToClick = button;
                break; // we've already tested for multiple buttons in assert above
            }
        }

        clickButton(buttonToClick);
    }

    /**
     * Click the first button you find with the given value. Ignores duplicate values.
     *
     * @param value the value attribute of the of the button element to click.
     */
    @Override
    public void clickAnyButtonWithValue(final String value) {
        assertAnyButtonPresentWithValue(value);
        Button buttonToClick = null;
        final Button[] buttons = webTester.getDialog().getForm().getButtons();
        for (final Button button : buttons) {
            if (value.equals(button.getValue())) {
                buttonToClick = button;
                break; // we've already tested for multiple buttons in assert above
            }
        }

        clickButton(buttonToClick);
    }

    private void clickButton(final Button buttonToClick) {
        try {
            // Stops IDEA's NPE complier warning, although assertAnyButtonPresentWithValue() would already cover this.
            assertNotNull(buttonToClick);
            // ideally we would just click the button now, but we can't do that through jwebunit yet!
            if (buttonToClick.getID() != null) {
                webTester.clickButton(buttonToClick.getID());
            } else {
                if (buttonToClick.getName() != null) {
                    webTester.setFormElement(buttonToClick.getName(), buttonToClick.getValue());
                }

                webTester.submit();
            }
        } catch (final Exception e) {
            throw new RuntimeException(ExceptionUtility.stackTraceToString(e));
        }
    }

    /**
     * Assert a single button exists with the given value. Barfs if 0 or > 1 buttons found.
     *
     * @param value the value attribute of the of the button element to click.
     */
    private void assertButtonPresentWithValue(final String value) {
        Button buttonToAssert = null;
        final Button[] buttons = webTester.getDialog().getForm().getButtons();
        for (final Button button : buttons) {
            if (value.equals(button.getValue())) {
                if (buttonToAssert != null) {
                    fail("Found multiple buttons with value: " + value + "\n" + foundButtons());
                }

                buttonToAssert = button;
            }
        }

        if (buttonToAssert == null) {
            fail("Did not find button with value: " + value + "\n" + foundButtons());
        }
    }

    /**
     * Assert any buttons are found with this value. Barfs if 0 buttons found.
     *
     * @param value the value attribute of the of the button element to click.
     */
    private void assertAnyButtonPresentWithValue(final String value) {
        final Button[] buttons = webTester.getDialog().getForm().getButtons();
        for (final Button button : buttons) {
            if (value.equals(button.getValue())) {
                return;
            }
        }

        fail("Did not find button with value: " + value);
    }

    private String foundButtons() {
        final StringBuilder result = new StringBuilder();
        final Button[] buttons = webTester.getDialog().getForm().getButtons();
        for (int i = 0; i < buttons.length; i++) {
            final Button button = buttons[i];
            result.append("Button[");
            if (button.getID() != null) {
                result.append("id:").append(button.getID()).append(" ");
            }

            result.append(button.getName());
            result.append("=");
            result.append(button.getValue());
            result.append("]");

            if (i != buttons.length - 1) {
                result.append(", ");
            }
        }

        return result.toString();
    }

}
