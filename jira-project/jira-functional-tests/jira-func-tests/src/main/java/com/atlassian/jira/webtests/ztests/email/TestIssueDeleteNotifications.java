package com.atlassian.jira.webtests.ztests.email;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.admin.DefaultIssueEvents;
import com.atlassian.jira.functest.framework.admin.NotificationType;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.EmailBaseFuncTestCase;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import javax.inject.Inject;
import javax.mail.internet.MimeMessage;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test that issue notifications are being sent on issue delete to particular subscribers.
 * <p/>
 * See:<br/>
 * http://jira.atlassian.com/browse/JRA-21646<br/>
 * http://jira.atlassian.com/browse/JRA-24331
 *
 * @since 4.4
 */
@WebTest({Category.FUNC_TEST, Category.EMAIL, Category.ISSUES})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestIssueNotificationsWithCustomFields.xml")
public class TestIssueDeleteNotifications extends EmailBaseFuncTestCase {

    private static final String HENRY_FORD_EMAIL = "Henry.Ford@example.com";
    private static final String HENRY_FORD_USERNAME = "henry.ford";

    private static final String USER_CF_ID = "customfield_10000";
    private static final String MULTI_USER_CF_ID = "customfield_10001";
    private static final String GROUP_CF_ID = "customfield_10002";
    private static final String MULTI_GROUP_CF_ID = "customfield_10003";

    private static final String TEST_GROUP_ONE = "to-notify-1";
    private static final String TEST_GROUP_TWO = "to-notify-2";

    @Inject
    private Administration administration;

    @Test
    public void testAllWatchersNotifiedOfIssueDeletion() throws Exception {
        configureAndStartSmtpServerWithNotify();

        administration.notificationSchemes().goTo().addNotificationScheme("Test scheme", "Test description")
                .addNotificationsForEvent(DefaultIssueEvents.ISSUE_DELETED.eventId(), NotificationType.ALL_WATCHERS);

        administration.project().associateNotificationScheme("COW", "Test scheme");
        final String issueKey = navigation.issue().createIssue("COW", "Bug", "Notification test");
        navigation.issue().addWatchers(issueKey, ADMIN_USERNAME, HENRY_FORD_USERNAME);
        navigation.issue().deleteIssue(issueKey);

        flushMailQueueAndWait(2);
        assertGotDeleteIssueMessages(issueKey, ADMIN_EMAIL, HENRY_FORD_EMAIL);
    }

    @Test
    public void testUserFromUserCustomFieldNotifiedOfIssueDeletion() throws Exception {
        configureAndStartSmtpServer();

        administration.notificationSchemes().goTo().addNotificationScheme("Test scheme", "Test description")
                .addNotificationsForEvent(DefaultIssueEvents.ISSUE_DELETED.eventId(),
                        NotificationType.USER_CUSTOM_FIELD_VALUE, USER_CF_ID);
        administration.project().associateNotificationScheme("COW", "Test scheme");

        final String issueKey = navigation.issue().createIssue("COW", "Bug", "Notification test",
                ImmutableMap.<String, String[]>of(USER_CF_ID, new String[]{BOB_USERNAME}));
        navigation.issue().deleteIssue(issueKey);

        flushMailQueueAndWait(1);
        assertGotDeleteIssueMessages(issueKey, BOB_EMAIL);
    }

    @Test
    public void testUsersFromMultiUserCustomFieldNotifiedOfIssueDeletion() throws Exception {
        configureAndStartSmtpServer();

        administration.notificationSchemes().goTo().addNotificationScheme("Test scheme", "Test description")
                .addNotificationsForEvent(DefaultIssueEvents.ISSUE_DELETED.eventId(),
                        NotificationType.USER_CUSTOM_FIELD_VALUE, MULTI_USER_CF_ID);
        administration.project().associateNotificationScheme("COW", "Test scheme");

        final String issueKey = navigation.issue().createIssue("COW", "Bug", "Notification test",
                ImmutableMap.<String, String[]>of(MULTI_USER_CF_ID, new String[]{BOB_USERNAME + "," + FRED_USERNAME}));
        navigation.issue().deleteIssue(issueKey);

        flushMailQueueAndWait(2);
        assertGotDeleteIssueMessages(issueKey, BOB_EMAIL, FRED_EMAIL);
    }

    @Test
    public void testUsersFromGroupCustomFieldNotifiedOfIssueDeletion() throws Exception {
        configureAndStartSmtpServer();

        administration.notificationSchemes().goTo().addNotificationScheme("Test scheme", "Test description")
                .addNotificationsForEvent(DefaultIssueEvents.ISSUE_DELETED.eventId(),
                        NotificationType.GROUP_CUSTOM_FIELD_VALUE, GROUP_CF_ID);
        administration.project().associateNotificationScheme("COW", "Test scheme");
        setUpGroups();

        final String issueKey = navigation.issue().createIssue("COW", "Bug", "Notification test",
                ImmutableMap.<String, String[]>of(GROUP_CF_ID, new String[]{TEST_GROUP_ONE}));
        navigation.issue().deleteIssue(issueKey);

        flushMailQueueAndWait(2);
        assertGotDeleteIssueMessages(issueKey, BOB_EMAIL, FRED_EMAIL);
    }

    @Test
    public void testUsersFromMultiGroupCustomFieldNotifiedOfIssueDeletion() throws Exception {
        configureAndStartSmtpServer();

        administration.notificationSchemes().goTo().addNotificationScheme("Test scheme", "Test description")
                .addNotificationsForEvent(DefaultIssueEvents.ISSUE_DELETED.eventId(),
                        NotificationType.GROUP_CUSTOM_FIELD_VALUE, MULTI_GROUP_CF_ID);
        administration.project().associateNotificationScheme("COW", "Test scheme");
        setUpGroups();

        final String issueKey = navigation.issue().createIssue("COW", "Bug", "Notification test",
                ImmutableMap.<String, String[]>of(MULTI_GROUP_CF_ID, new String[]{TEST_GROUP_ONE, TEST_GROUP_TWO}));
        navigation.issue().deleteIssue(issueKey);

        flushMailQueueAndWait(3);
        assertGotDeleteIssueMessages(issueKey, BOB_EMAIL, FRED_EMAIL, HENRY_FORD_EMAIL);
    }

    private void setUpGroups() {
        backdoor.usersAndGroups().addGroup(TEST_GROUP_ONE);
        backdoor.usersAndGroups().addUserToGroup(BOB_USERNAME, TEST_GROUP_ONE);
        backdoor.usersAndGroups().addUserToGroup(FRED_USERNAME, TEST_GROUP_ONE);
        backdoor.usersAndGroups().addGroup(TEST_GROUP_TWO);
        backdoor.usersAndGroups().addUserToGroup(BOB_USERNAME, TEST_GROUP_TWO);
        backdoor.usersAndGroups().addUserToGroup(HENRY_FORD_USERNAME, TEST_GROUP_TWO);
    }

    private void assertGotDeleteIssueMessages(String deletedIssueKey, String... emails) throws Exception {
        assertEquals(emails.length, mailService.getReceivedMessages().length);
        for (String username : emails) {
            final List<MimeMessage> received = getMessagesForRecipient(username);
            assertEquals("Unexpected number of messages for " + username, 1, received.size());
            final MimeMessage message = received.get(0);
            final String subject = message.getSubject();
            final String expectedPhrase = String.format("(%s)", deletedIssueKey);
            assertTrue("Subject '" + subject + "' does not contain expected expected phrase: " + expectedPhrase, subject.contains(expectedPhrase));

            assertEmailBodyContains(message, "deleted");
        }
    }


}
