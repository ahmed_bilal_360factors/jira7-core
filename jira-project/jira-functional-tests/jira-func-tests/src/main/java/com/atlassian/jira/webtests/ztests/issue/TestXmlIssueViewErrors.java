package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HttpUnitConfiguration;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.page.Error404;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.meterware.httpunit.WebResponse;
import org.junit.Test;

import java.io.IOException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * This class tests the various error conditions encountered when trying to view an issue using the IssueViewURLHandler.
 *
 * @since v3.13.3
 */
@WebTest({Category.FUNC_TEST, Category.ISSUES})
@LoginAs(user = ADMIN_USERNAME)
@HttpUnitConfiguration(throwOnErrorStatus = false)
public class TestXmlIssueViewErrors extends BaseJiraFuncTest {
    private static final String SAMPLE_PATH_MESSAGE_1 = "Invalid path format.";
    private static final String SAMPLE_PATH_MESSAGE_2 = "Path should be of format";
    private static final String SAMPLE_PATH_MESSAGE_3 = "/si/jira.issueviews:xml/JRA-10/JRA-10.xml";

    @Test
    public void testMalformedUrls() throws Exception {
        assert400SamplePathMessage("/si");
        assert400SamplePathMessage("/si/");
        assert400SamplePathMessage("/si/unknownplugin");
        assert400SamplePathMessage("/si/unknownplugin/");
        assert400SamplePathMessage("/si/unknownplugin/blah");
        assert400BadPlugin("/si/unknownplugin/blah/");
        assert404BadKey("/si/jira.issueviews:issue-xml/badkey/");

        // TODO: add more cases here
    }

    private void assert404BadKey(final String url) throws IOException {
        tester.beginAt(url);
        assertThat(new Error404(tester), Error404.isOn404Page());
    }

    private void assert400SamplePathMessage(final String url) {
        assertResponseCode(url, 400);
        tester.assertTextPresent(SAMPLE_PATH_MESSAGE_1 + " " + SAMPLE_PATH_MESSAGE_2 + " " + SAMPLE_PATH_MESSAGE_3);
    }

    private void assert400BadPlugin(final String url) {
        assertResponseCode(url, 400);
        tester.assertTextPresent("Could not find any enabled plugin with key");
    }

    private void assertResponseCode(final String url, final int code) {
        tester.beginAt(url);
        final WebResponse webResponse = tester.getDialog().getResponse();
        assertEquals(code, webResponse.getResponseCode());
    }
}