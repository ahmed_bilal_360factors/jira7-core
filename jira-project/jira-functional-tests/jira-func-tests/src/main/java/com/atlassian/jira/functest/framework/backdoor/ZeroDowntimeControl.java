package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.google.common.base.MoreObjects;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.ws.rs.core.MediaType;

/**
 * Retrieve and alter zero downtime upgrade state.
 *
 * @since v7.3
 */
public class ZeroDowntimeControl extends BackdoorControl<ZeroDowntimeControl> {

    public ZeroDowntimeControl(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    private WebResource zduResource() {
        return resourceRoot(rootPath).path("rest").path(API_REST_PATH).path(API_REST_VERSION).path("cluster").path("zdu");
    }

    public void start() {
        zduResource().path("start").post();
    }

    public void cancel() {
        zduResource().path("cancel").post();
    }

    public void approve() {
        zduResource().path("approve").post();
    }

    public ClusterState currentState() {
        return zduResource().path("state")
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get(ClusterState.class);
    }

    public static class ClusterState {
        private final UpgradeState state;
        private final NodeBuildInfo buildInfo;

        @JsonCreator
        public ClusterState(@JsonProperty("state") UpgradeState state, @JsonProperty("build") NodeBuildInfo buildInfo) {
            this.state = state;
            this.buildInfo = buildInfo;
        }

        @JsonProperty("state")
        public UpgradeState getState() {
            return state;
        }

        @JsonProperty("build")
        public NodeBuildInfo getBuildInfo() {
            return buildInfo;
        }
    }

    public enum UpgradeState {
        STABLE,
        READY_TO_UPGRADE,
        MIXED,
        READY_TO_RUN_UPGRADE_TASKS,
        RUNNING_UPGRADE_TASKS
    }

    public static class NodeBuildInfo {
        private final long buildNumber;
        private final String version;

        @JsonCreator
        public NodeBuildInfo(@JsonProperty("buildNumber") long buildNumber, @JsonProperty("version") String version) {
            this.buildNumber = buildNumber;
            this.version = version;
        }

        @JsonProperty("buildNumber")
        public long getBuildNumber() {
            return buildNumber;
        }

        @JsonProperty("version")
        public String getVersion() {
            return version;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("buildNumber", buildNumber)
                    .add("version", version)
                    .toString();
        }
    }
}
