package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.WorklogControl;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.testkit.client.restclient.Visibility;
import com.atlassian.jira.testkit.client.restclient.Worklog;
import com.atlassian.jira.testkit.client.restclient.WorklogChangeBean;
import com.atlassian.jira.testkit.client.restclient.WorklogChangedSinceBean;
import com.atlassian.jira.testkit.client.restclient.WorklogClient;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nullable;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.atlassian.jira.issue.worklog.WorklogManager.WORKLOG_UPDATE_DATA_PAGE_SIZE;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


@WebTest({Category.FUNC_TEST, Category.REST, Category.WORKLOGS})
@RestoreBlankInstance
public class TestWorklogResource extends BaseJiraRestTest {
    public static final String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final DateTime YEAR_AGO = DateTime.now().minusYears(1);

    private WorklogClient worklogClient;
    private WorklogControl worklogControl;

    @Before
    public void setUpTest() {
        worklogClient = new WorklogClient(environmentData);
        worklogControl = new WorklogControl(environmentData);
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_TIMETRACKING, true);
        backdoor.permissionSchemes().addEveryonePermission(0L, ProjectPermissions.DELETE_ALL_WORKLOGS);
    }

    @Test
    public void noWorklogsReturnEmptyPage() {
        Response<WorklogChangedSinceBean> updatedWorklogsSince = worklogClient.getUpdatedWorklogsSince(0L);
        WorklogChangedSinceBean body = updatedWorklogsSince.body;

        assertThat(body.values, hasSize(0));
        assertTrue(body.lastPage);
        assertNull(body.nextPage);
        assertEquals(body.since, body.until);
        assertEquals(0L, body.since.longValue());
    }

    @Test
    public void gettingSingleElementsInPage() {
        final IssueCreateResponse issue = backdoor.issues().createIssue(FunctTestConstants.PROJECT_HOMOSAP_KEY, "asd");
        final Long worklog = createWorklog(issue);

        final Response<WorklogChangedSinceBean> updatedWorklogsSince = worklogClient.getUpdatedWorklogsSince(0L);
        final WorklogChangedSinceBean body = updatedWorklogsSince.body;

        assertThat(body.values, contains(changeBeanMatcher(worklog)));
    }

    @Test
    public void userWontSeeWorklogsRestrictedToRoleHeDoesntHave() {
        final String groupname = "group-without-members";
        backdoor.usersAndGroups().addGroup(groupname);
        backdoor.usersAndGroups().addUserToGroup("admin", groupname);

        final IssueCreateResponse issue = backdoor.issues().createIssue(FunctTestConstants.PROJECT_HOMOSAP_KEY, "asd");

        Worklog worklog = newWorklogBean();
        worklog.visibility = new Visibility("group", groupname);
        createWorklog(issue, worklog);

        backdoor.usersAndGroups().removeUserFromGroup("admin", groupname);
        Response<WorklogChangedSinceBean> updatedWorklogsSince = worklogClient.getUpdatedWorklogsSince(0L);
        WorklogChangedSinceBean body = updatedWorklogsSince.body;

        assertThat(body.values, Matchers.empty());
    }

    @Test
    public void anonymousResultsIn401() {
        Response<WorklogChangedSinceBean> updatedWorklogsSince = worklogClient.anonymous().getUpdatedWorklogsSince(0L);

        assertEquals(updatedWorklogsSince.statusCode, UNAUTHORIZED.getStatusCode());
    }

    @Test
    public void responseIsPaginated() {
        IssueCreateResponse issue = backdoor.issues().createIssue(FunctTestConstants.PROJECT_HOMOSAP_KEY, "asd");

        final List<Long> createdWorklogIds = IntStream.range(0, WORKLOG_UPDATE_DATA_PAGE_SIZE * 2 + 1)
                .mapToObj(n -> createWorklog(issue, n))
                .collect(Collectors.toList());

        WorklogChangedSinceBean firstPage = worklogClient.getUpdatedWorklogsSince(0L).body;

        assertEquals(0L, firstPage.since.longValue());
        assertThat(firstPage.values,
                contains(createdWorklogIds.stream().limit(WORKLOG_UPDATE_DATA_PAGE_SIZE).map(this::changeBeanMatcher).collect(Collectors.toList())));

        assertNotNull(firstPage.until);

        WorklogChangedSinceBean secondPage = worklogClient.getUpdatedWorklogsSince(firstPage.until).body;
        assertEquals(firstPage.until, secondPage.since);
        // start with a different since
        assertThat(secondPage.values, contains(createdWorklogIds.stream()
                .skip(firstPage.values.size() - 1)
                .limit(WORKLOG_UPDATE_DATA_PAGE_SIZE)
                .map(this::changeBeanMatcher).collect(Collectors.toList())));

        WorklogChangedSinceBean thirdPage = worklogClient.getUpdatedWorklogsSince(secondPage.until).body;
        assertEquals(secondPage.until, thirdPage.since);
        assertThat(thirdPage.values,
                contains(createdWorklogIds.stream()
                        .skip(WORKLOG_UPDATE_DATA_PAGE_SIZE + secondPage.values.size() - 2)
                        .limit(WORKLOG_UPDATE_DATA_PAGE_SIZE)
                        .map(this::changeBeanMatcher).collect(Collectors.toList())));

        for (WorklogChangeBean bean : firstPage.values) {
            removeWorklog(issue, bean);
        }

        WorklogChangedSinceBean deletedWorklgosPage = worklogClient.getDeletedWorklogsSince(firstPage.since).body;

        // assert deleted worklogs contains all items from first page
        assertThat(deletedWorklgosPage.values, containsInAnyOrder(firstPage.values.stream()
                        .map(WorklogChangeBean::getWorklogId)
                        .map(i -> hasProperty("worklogId", equalTo(i)))
                        .collect(Collectors.toList()))
        );
    }

    @Test
    public void getDeletedWorklogsReturnsEmptyListWhenNoWorklogsDeleted() {
        final WorklogChangedSinceBean body = worklogClient.getDeletedWorklogsSince(0L).body;

        assertThat(body.values, Matchers.empty());
    }

    @Test
    public void deletedWorklogNotInUpdatedWorklogs() {
        IssueCreateResponse issue = backdoor.issues().createIssue(FunctTestConstants.PROJECT_HOMOSAP_KEY, "asd");
        Long createdWorklogId = createWorklog(issue);

        final List<WorklogChangeBean> updatedSince = worklogClient.getUpdatedWorklogsSince(0L).body.values;
        assertThat(updatedSince, contains(changeBeanMatcher(createdWorklogId)));

        removeWorklog(issue, updatedSince.get(0));

        assertThat(worklogClient.getUpdatedWorklogsSince(0L).body.values, empty());
        assertThat(worklogClient.getDeletedWorklogsSince(0L).body.values, contains(changeBeanMatcher(createdWorklogId)));
    }

    @Test
    public void getModifiedWorklogsByIds() {
        IssueCreateResponse issue = backdoor.issues().createIssue(FunctTestConstants.PROJECT_HOMOSAP_KEY, "asd");

        Set<Long> createdWorklogIds = IntStream.range(0, 1)
                .mapToObj(n -> createWorklog(issue))
                .collect(Collectors.toSet());

        Collection<Worklog> worklogs = worklogClient.getWorklogs(createdWorklogIds).body;

        assertThat(worklogs, containsInAnyOrder(createdWorklogIds.stream().map(this::worklogIdMatcher).collect(Collectors.toSet())));
    }

    @Test
    public void returnedWorklogShouldBeSameAsReturnedByJira() {
        IssueCreateResponse issue = backdoor.issues().createIssue(FunctTestConstants.PROJECT_HOMOSAP_KEY, "asd");

        Long worklogId = createWorklog(issue);

        Worklog worklog = worklogClient.get(issue.key, Long.toString(worklogId));
        Collection<Worklog> worklogsById = worklogClient.getWorklogs(ImmutableSet.of(worklogId)).body;

        assertThat(worklogsById, contains(worklog));
    }

    @Test
    public void canNotGetMoreWorklogsThanPageLimitIs() {
        final Set<Long> ids = IntStream
                .range(0, WORKLOG_UPDATE_DATA_PAGE_SIZE + 1)
                .mapToObj(value -> (long) value)
                .collect(Collectors.toSet());

        final Response<List<Worklog>> worklogs = worklogClient.getWorklogs(ids);

        assertThat(worklogs.statusCode, is(400));
    }

    @Test
    public void cantGetWorklogsWithoutPermissions() {
        final String groupname = "group-without-members";
        backdoor.usersAndGroups().addGroup(groupname);
        backdoor.usersAndGroups().addUserToGroup("admin", groupname);

        final IssueCreateResponse issue = backdoor.issues().createIssue(FunctTestConstants.PROJECT_HOMOSAP_KEY, "asd");

        Worklog worklog = newWorklogBean();
        worklog.visibility = new Visibility("group", groupname);
        final Long worklogWithoutPermissionsId = createWorklog(issue, worklog);

        backdoor.usersAndGroups().removeUserFromGroup("admin", groupname);

        final Long worklogWithPermissionsId = createWorklog(issue);

        final Response<List<Worklog>> worklogs = worklogClient.getWorklogs(Lists.newArrayList(worklogWithoutPermissionsId, worklogWithPermissionsId));

        assertThat(worklogs.statusCode, is(200));
        assertThat(worklogs.body, Matchers.contains(worklogIdMatcher(worklogWithPermissionsId)));
    }

    @Test
    public void willNotGetNotExistingWorklogs() {
        final Response<List<Worklog>> worklogs = worklogClient.getWorklogs(Lists.newArrayList(100002L));

        assertThat(worklogs.statusCode, is(200));
        assertThat(worklogs.body, Matchers.empty());
    }

    @Test
    public void worklogUpdatedWithinLastMinuteIsNotReturned() {
        final IssueCreateResponse issue = backdoor.issues().createIssue(FunctTestConstants.PROJECT_HOMOSAP_KEY, "asd");
        worklogClient.post(issue.key, newWorklogBean());

        assertThat(worklogClient.getUpdatedWorklogsSince(0L).body.values, Matchers.empty());
    }

    @Test
    public void worklogRemovedWithinLastMinuteIsNotReturned() {
        final IssueCreateResponse issue = backdoor.issues().createIssue(FunctTestConstants.PROJECT_HOMOSAP_KEY, "asd");
        final Response<Worklog> worklogResponse = worklogClient.post(issue.key, newWorklogBean());

        worklogClient.delete(issue.key, worklogResponse.body);

        assertThat(worklogClient.getDeletedWorklogsSince(0L).body.values, Matchers.empty());
    }

    private Matcher<Worklog> worklogIdMatcher(final Long id) {
        return new TypeSafeMatcher<Worklog>() {
            @Override
            protected boolean matchesSafely(final Worklog item) {
                return Long.parseLong(item.id) == id;
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("Expected worklog with id ").appendValue(id);
            }
        };
    }

    private void removeWorklog(final IssueCreateResponse issue, final WorklogChangeBean changeBean) {
        final Worklog worklog = new Worklog();
        worklog.id = changeBean.getWorklogId().toString();
        worklogClient.delete(issue.key, worklog);
        worklogControl.setWorklogDeletedTimestamp(Long.parseLong(worklog.id), yearAgo().getMillis());
    }

    private Long createWorklog(final IssueCreateResponse issue) {
        return createWorklog(issue, newWorklogBean());
    }

    private Long createWorklog(final IssueCreateResponse issue, int n) {
        return createWorklog(issue, newWorklogBean(), n);
    }

    private Long createWorklog(final IssueCreateResponse issue, Worklog worklog, int n) {
        return createWorklog(issue, worklog, YEAR_AGO.plusSeconds(n));
    }

    private Long createWorklog(final IssueCreateResponse issue, Worklog worklog) {
        return createWorklog(issue, worklog, yearAgo());
    }

    private Long createWorklog(final IssueCreateResponse issue, Worklog worklog, DateTime updateDate) {
        final Response<Worklog> worklogResponse = worklogClient.post(issue.key, worklog);

        Worklog createdWorklog = worklogResponse.body;
        worklogControl.setWorklogUpdatedTimestamp(Long.parseLong(createdWorklog.id), updateDate.getMillis());

        // only id is returned instead of full worklog object because
        // `createdWorklog` doesn't equals to the that is stored in JIRA as it doesn't have updated `updateDate` filed.
        return Long.parseLong(createdWorklog.id);
    }

    private DateTime yearAgo() {
        return new DateTime(System.currentTimeMillis()).minusYears(1);
    }

    private Worklog newWorklogBean() {
        Date now = new Date();
        Worklog worklog = new Worklog();
        worklog.timeSpent = "1h";
        worklog.started = asTimeString(now);
        worklog.comment = "This is my comment";
        return worklog;
    }

    private Matcher<WorklogChangeBean> changeBeanMatcher(Long worklogId) {
        return hasProperty("worklogId", equalTo(worklogId));
    }

    private static String asTimeString(@Nullable Date date) {
        return date != null ? new SimpleDateFormat(TIME_FORMAT).format(date) : null;
    }
}
