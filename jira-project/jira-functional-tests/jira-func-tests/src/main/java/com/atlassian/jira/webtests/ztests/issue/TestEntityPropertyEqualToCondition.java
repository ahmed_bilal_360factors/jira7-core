package com.atlassian.jira.webtests.ztests.issue;

import java.io.IOException;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.locator.Locator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.EntityPropertyControl;
import com.atlassian.jira.testkit.client.IssueTypeControl;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.client.UserPropertyClient;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.JsonNodeFactory;
import org.codehaus.jackson.node.ObjectNode;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;

@WebTest({Category.FUNC_TEST, Category.ENTITY_PROPERTIES, Category.REFERENCE_PLUGIN})
@RestoreBlankInstance
public class TestEntityPropertyEqualToCondition extends BaseJiraFuncTest {
    public static final String CONDITIONAL_WEBPANEL_ID_PREFIX = "conditional-webpanel-";

    @Inject
    private Assertions assertions;

    private EntityPropertyControl entityPropertyControl;
    private IssueCreateResponse issue;

    @Before
    public void setUp() throws Exception {
        backdoor.restoreBlankInstance();
        navigation.login(ADMIN_USERNAME);
        entityPropertyControl = backdoor.getTestkit().getEntityPropertyControl();
        issue = backdoor.issues().createIssue(FunctTestConstants.PROJECT_HOMOSAP_KEY, "sum");
    }

    @Test
    public void testPanelInIssueViewIsDisplayedWhenIssuePropertyConditionEvalTrue() {
        testWebPanelInIssueViewRespectsIssueProperty("0", assertions::assertNodeByIdExists);
    }

    @Test
    public void testPanelInIssueViewIsNotDisplayedWhenIssuePropertyConditionEvalFalse() {
        testWebPanelInIssueViewRespectsIssueProperty("1", assertions::assertNodeByIdDoesNotExist);
    }

    @Test
    public void testPanelInIssueViewIsDisplayedWhenNestedIssuePropertyConditionEvalTrue() throws IOException {
        final ObjectNode root = JsonNodeFactory.instance.objectNode();
        final ObjectNode one = root.putObject("one");
        one.put("unimportant", "data");
        final ObjectNode two = one.putObject("two");
        two.put("three", true);

        testWebPanelInIssueViewRespectsNestedIssueProperty(root, assertions::assertNodeByIdExists);
    }

    @Test
    public void testPanelInIssueViewIsNotDisplayedWhenNestedIssuePropertyConditionEvalFalse() throws IOException {
        final ObjectNode root = JsonNodeFactory.instance.objectNode();
        final ObjectNode one = root.putObject("one");
        one.put("unimportant", "test data");
        final ObjectNode two = one.putObject("two");
        two.put("three", false);

        testWebPanelInIssueViewRespectsNestedIssueProperty(root, assertions::assertNodeByIdDoesNotExist);
    }

    @Test
    public void testPanelInIssueViewIsDisplayedWhenIssueTypePropertyConditionEvalTrue() {
        testWebPanelInIssueViewRespectsIssueTypeProperty("true", assertions::assertNodeByIdExists);
    }

    @Test
    public void testPanelInIssueViewIsNotDisplayedWhenIssueTypePropertyConditionEvalFalse() {
        testWebPanelInIssueViewRespectsIssueTypeProperty("false", assertions::assertNodeByIdDoesNotExist);
    }

    @Test
    public void testPanelInIsueViewIsDisplayedWhenProjectPropertyConditionEvalTrue() {
        testWebPanelInIssueViewRespectsProjectProperty("\"big-product-type\"", assertions::assertNodeByIdExists);
    }

    @Test
    public void testPanelInIssueViewIsNotDisplayedWhenProjetPropertyConditionEvalFalse() {
        testWebPanelInIssueViewRespectsProjectProperty("\"big-product-type-2\"", assertions::assertNodeByIdDoesNotExist);
    }

    @Test
    public void testPanelInIssueViewIsDisplayedWhenUserPropertyConditionEvalTrue() throws JSONException {
        testWebPanelInIssueViewRespectsUserProperty("\"admin\"", assertions::assertNodeByIdExists);
    }

    @Test
    public void testPanelInIssueViewIsNotDisplayedWhenUserPropertyConditionEvalFalse() throws JSONException {
        testWebPanelInIssueViewRespectsUserProperty("\"notAdmin\"", assertions::assertNodeByIdDoesNotExist);
    }

    @Test
    public void testWebItemInProjectViewIsDisplayedWhenUserPropertyConditionEvalTrue() throws JSONException {
        testWebItemInProjectViewRespectsUserProperty("\"admin\"", assertions::assertNodeExists);
    }

    @Test
    public void testWebItemInProjectViewIsNotDisplayedWhenUserPropertyConditionEvalFalse() throws JSONException {
        testWebItemInProjectViewRespectsUserProperty("\"notAdmin\"", assertions::assertNodeDoesNotExist);
    }

    @Test
    public void testWebItemInProjectViewIsDisplayedWhenProjectPropertyConditionEvalTrue() throws JSONException {
        testWebItemInProjectViewRespectsProjectProperty("\"big-product-type\"", assertions::assertNodeExists);
    }

    @Test
    public void testWebItemInProjectViewIsNotDisplayedWhenProjectPropertyConditionEvalFalse() throws JSONException {
        testWebItemInProjectViewRespectsProjectProperty("\"small-product-type\"", assertions::assertNodeDoesNotExist);
    }

    private void testWebItemInProjectViewRespectsUserProperty(String propertyValue, Consumer<Locator> assertion) throws JSONException {
        UserPropertyClient userPropertyClient = new UserPropertyClient(environmentData);
        userPropertyClient.put(FunctTestConstants.ADMIN_USERNAME, "type", propertyValue);

        navigation.browseProject(FunctTestConstants.PROJECT_HOMOSAP_KEY);

        assertion.accept(new CssLocator(tester, "a[data-link-id=\"com.atlassian.jira.dev.reference-plugin:referenceUserConditionProjectSidebarWebItem\"]"));
    }

    private void testWebPanelInIssueViewRespectsUserProperty(String propertyValue, Consumer<String> assertion) throws JSONException {
        UserPropertyClient userPropertyClient = new UserPropertyClient(environmentData);
        userPropertyClient.put(FunctTestConstants.ADMIN_USERNAME, "type", propertyValue);

        IssueCreateResponse issue = backdoor.issues().createIssue(FunctTestConstants.PROJECT_HOMOSAP_KEY, "Some summary");

        navigation.issue().viewIssue(issue.key);
        assertion.accept(CONDITIONAL_WEBPANEL_ID_PREFIX + "user");
    }

    private void testWebItemInProjectViewRespectsProjectProperty(String propertyValue, Consumer<Locator> assertion) {
        final String projectKey = "PWP";
        String projectId = String.valueOf(backdoor.project().addProject("Project with property", projectKey, FunctTestConstants.ADMIN_USERNAME));
        entityPropertyControl.putProperty(EntityPropertyType.PROJECT_PROPERTY, projectId, "type", propertyValue);

        assertEquals(propertyValue, entityPropertyControl.getProperty(EntityPropertyType.PROJECT_PROPERTY, projectId, "type"));

        navigation.browseProject(projectKey);

        assertion.accept(new CssLocator(tester, "a[data-link-id=\"com.atlassian.jira.dev.reference-plugin:referenceProjectConditionProjectSidebarWebItem\"]"));
    }

    private void testWebPanelInIssueViewRespectsProjectProperty(String propertyValue, Consumer<String> assertion) {
        String projectId = String.valueOf(backdoor.project().addProject("Project with property", "PWP", FunctTestConstants.ADMIN_USERNAME));
        entityPropertyControl.putProperty(EntityPropertyType.PROJECT_PROPERTY, projectId, "type", propertyValue);

        assertEquals(propertyValue, entityPropertyControl.getProperty(EntityPropertyType.PROJECT_PROPERTY, projectId, "type"));

        IssueCreateResponse issue = backdoor.issues().createIssue("PWP", "Issue in project with properties");

        navigation.issue().viewIssue(issue.key);
        assertion.accept(CONDITIONAL_WEBPANEL_ID_PREFIX + "project");
    }

    private void testWebPanelInIssueViewRespectsIssueTypeProperty(String propertyValue, Consumer<String> assertion) {
        IssueTypeControl.IssueType issueType = backdoor.issueType().createIssueType("new-issue-type-with-properties");
        entityPropertyControl.putProperty(EntityPropertyType.ISSUE_TYPE_PROPERTY, issueType.getId(), "com.some.addon.vendor", propertyValue);

        assertEquals(propertyValue, entityPropertyControl.getProperty(EntityPropertyType.ISSUE_TYPE_PROPERTY, issueType.getId(), "com.some.addon.vendor"));

        IssueCreateResponse issue =
                backdoor.issues().createIssue(FunctTestConstants.PROJECT_HOMOSAP_KEY, "summary", null, "1", issueType.getId());

        navigation.issue().viewIssue(issue.key);
        assertion.accept(CONDITIONAL_WEBPANEL_ID_PREFIX + "issueType");
    }

    private void testWebPanelInIssueViewRespectsIssueProperty(String propertyValue, Consumer<String> assertion) {
        entityPropertyControl.putProperty(EntityPropertyType.ISSUE_PROPERTY, issue.id, "likes", propertyValue);

        String property = entityPropertyControl.getProperty(EntityPropertyType.ISSUE_PROPERTY, issue.id, "likes");
        assertEquals(property, propertyValue);

        navigation.issue().viewIssue(issue.key);
        assertion.accept(CONDITIONAL_WEBPANEL_ID_PREFIX + "issue");
    }

    private void testWebPanelInIssueViewRespectsNestedIssueProperty(JsonNode providedPropertyValue, Consumer<String> assertion) throws IOException {
        final String propertyKey = "nested-values";
        entityPropertyControl.putProperty(EntityPropertyType.ISSUE_PROPERTY, issue.id, propertyKey, providedPropertyValue.toString());

        final ObjectMapper objectMapper = new ObjectMapper();
        String readProperty = entityPropertyControl.getProperty(EntityPropertyType.ISSUE_PROPERTY, issue.id, propertyKey);
        final JsonNode readJsonProperty = objectMapper.readTree(readProperty);
        assertEquals(readJsonProperty, providedPropertyValue);

        navigation.issue().viewIssue(issue.key);
        assertion.accept(CONDITIONAL_WEBPANEL_ID_PREFIX + "issue");
    }
}
