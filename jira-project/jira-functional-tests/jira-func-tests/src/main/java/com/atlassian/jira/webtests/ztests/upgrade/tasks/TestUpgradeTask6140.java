package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Issue;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.io.IOException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertNotNull;

/**
 * Responsible for testing project roles.
 * <p/>
 * From TestUpgradeTask6108
 */
@WebTest({Category.FUNC_TEST, Category.UPGRADE_TASKS, Category.PROJECTS, Category.ROLES})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestUpgradeTask6140 extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Test
    public void testProjectRolesCorrectForMixedCaseUsersAndGroups() throws IOException, SAXException {
        administration.restoreData("TestUpgradeTask6140.xml");

        navigation.login("fred_renamed", "fred");

        Issue issue = backdoor.issues().getIssue("HSP-1");
        assertNotNull(issue);
    }
}


