package com.atlassian.jira.webtests.ztests.project;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.SessionFactory;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.COMPONENTS_AND_VERSIONS, Category.PROJECTS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestVersionValidation extends BaseJiraFuncTest {

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Inject
    private SessionFactory sessionFactory;

    @Before
    public void setUpTest() {
        administration.restoreData("TestBrowseProjectRoadmapAndChangeLogTab.xml");
    }

    @Test
    public void testVersionValidationSwitchingProjectsUnderneath() {
        navigation.issue().viewIssue("LOTS-1");
        tester.clickLink("edit-issue");

        sessionFactory.begin().withSession(()->{
            navigation.login(ADMIN_USERNAME);
            navigation.issue().viewIssue("LOTS-1");
            tester.clickLink("move-issue");
            tester.setFormElement("pid", "10051");
            tester.submit("Next >>");
            tester.submit("Next >>");
            tester.submit("Move");
            assertions.assertNodeExists("//a[@title='Version 1 ']");
        });

        tester.submit("Update");

        assertions.assertNodeHasText("//*[@class='error']", "Versions Version 1(10040), Version 2(10041), Version A(10042), Version <b>B</b>(10043), Version 3(10044), Version Version(10045), Version 6(10046), Version 8(10047), Version Nick(10048), This is getting silly(10060), V2(10059), Lets throw in a Date(10058), Still going(10057), Version Justus(10054), Version Brenden(10053) are not valid for project 'All Released'.");
        assertions.assertNodeHasText("//*[@id='key-val']", "RELEASED-1");
    }

    @Test
    public void testVersionValidationNonExistantVersion() {
        tester.gotoPage(page.addXsrfToken("/secure/EditIssue.jspa?id=10000&summary=LOTS-1&components=99&fixVersions=999&assignee=admin&reporter=admin&issuetype=1"));

        assertions.assertNodeHasText("//*[@class='error']", "Version with id '999' does not exist.");
    }
}
