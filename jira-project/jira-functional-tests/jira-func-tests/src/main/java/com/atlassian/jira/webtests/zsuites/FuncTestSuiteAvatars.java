package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.avatar.TestAvatarSettingsMigration;
import com.atlassian.jira.webtests.ztests.avatar.TestGettingIconsAsPNGs;
import com.atlassian.jira.webtests.ztests.avatar.TestGravatarSupport;
import com.google.common.collect.Lists;

import java.util.List;

public class FuncTestSuiteAvatars {
    public static List<Class<?>> suite() {
        return Lists.newArrayList(
                TestAvatarSettingsMigration.class,
                TestGravatarSupport.class,
                TestGettingIconsAsPNGs.class);
    }
}
