package com.atlassian.jira.functest.rule;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.webtests.JIRAServerSetup;
import com.atlassian.jira.webtests.util.mail.MailService;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.net.BindException;
import java.util.function.Supplier;

/**
 * Rule to initialize SMTPBackdoor for listening for emails.
 * Starts dummy smtp server (greenmail) and configures jira to use it.
 * Previous step (dummy smtp server and configuration of jira) is omitted on ondemand instances.
 * Also proper configuration of smtp server is checked (if default smtp server is present, and outgoing emails are not disabled -Datlassian.mail.senddisabled=true)
 *
 * @since v7.1
 */
public class OutgoingMailConfigureSmtpRule extends TestWatcher {
    private final String from;
    private final String prefix;
    private final JIRAServerSetup[] jiraServerSetups;
    private final Supplier<Backdoor> backdoorSupplier;
    private final MailService mailService;

    OutgoingMailConfigureSmtpRule(final String from, final String prefix, final Supplier<Backdoor> backdoorSupplier, final MailService mailService, final JIRAServerSetup... jiraServerSetups) {
        this.from = from;
        this.prefix = prefix;
        this.jiraServerSetups = jiraServerSetups;
        this.backdoorSupplier = backdoorSupplier;
        this.mailService = mailService;
    }

    @Override
    protected void starting(final Description description) {
        if (shouldFire(description)) {
            Backdoor backdoor = backdoorSupplier.get();
            try {
                if (!backdoor.getTestkit().getFeatureManagerControl().isOnDemand()) {
                    mailService.configureAndStartGreenMail(jiraServerSetups);
                    int smtpPort = mailService.getSmtpPort();
                    try {
                        backdoor.mailServers().addSmtpServer(from, prefix, smtpPort);
                    } catch (Exception e) {
                        throw new RuntimeException("Adding smtp server failed. Check that no -Datlassian.mail.senddisabled=true parameter is present.", e);
                    }
                }
            } catch (BindException e) {
                throw new RuntimeException(e);
            }

            //check if mail sending is enabled (is smtp server, no disable flag)
            if (!backdoor.getTestkit().mailServers().isSmtpConfigured()) {
                throw new RuntimeException("Smtp is not configured properly. Check if smtp server was added, and no -Datlassian.mail.senddisabled=true parameter is present.");
            }
        }
    }

    @Override
    protected void finished(final Description description) {
        if (shouldFire(description)) {
            mailService.stop();
        }
    }

    private boolean shouldFire(final Description description) {
        return description.getAnnotation(MailTest.class) != null || description.getTestClass().getAnnotation(MailTest.class) != null;
    }
}
