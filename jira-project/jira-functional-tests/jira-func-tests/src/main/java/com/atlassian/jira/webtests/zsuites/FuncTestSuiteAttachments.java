package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.atlassian.jira.webtests.ztests.admin.security.xsrf.TestXsrfAttachments;
import com.atlassian.jira.webtests.ztests.issue.clone.TestCloneIssueAttachments;
import com.atlassian.jira.webtests.ztests.issue.move.TestMoveIssueAttachment;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Tests for attachments.
 *
 * @since v4.0
 */
public class FuncTestSuiteAttachments {

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestMoveIssueAttachment.class)
                .add(TestXsrfAttachments.class)
                .add(TestCloneIssueAttachments.class)
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.attachment", true))
                .build();
    }
}