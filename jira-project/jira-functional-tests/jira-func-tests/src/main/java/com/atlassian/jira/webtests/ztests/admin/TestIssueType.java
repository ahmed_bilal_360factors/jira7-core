package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Tests for the administration of issue types.
 *
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION})
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueType extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Before
    public void init(){
        administration.restoreBlankInstance();
        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_TYPES);
    }

    @Test
    public void testSameName() throws Exception {
        tester.clickLink("add-issue-type");

        //Check to see that we can't add an issue type of the same name.
        addIssueType("Bug");
        assertDuplicateIssueTypeErrorAui();

        addIssueType("bUG");
        assertDuplicateIssueTypeErrorAui();

        //Check to see that we can't change an issue type name to an already existing name.
        tester.gotoPage("secure/admin/EditIssueType!default.jspa?id=4");

        //Check to see that we can't edit a issue type such that becomes a duplicate.
        tester.setFormElement("name", "Bug");
        tester.submit();
        assertDuplicateIssueTypeErrorAui();

        tester.setFormElement("name", "BUG");
        tester.submit();
        assertDuplicateIssueTypeErrorAui();
    }

    @Test
    public void testIssueTypeOrderedByStyleAndName(){
        backdoor.subtask().enable();

        addTaskIssueType("Heroic mission on Mars");
        addTaskIssueType("another Heroic mission on Mars");
        addTaskIssueType("Heroic mission on Bruce Willis Asteroid");
        addTaskIssueType("Tell interesting story, Mark");
        addTaskIssueType("Kill all humans task");

        addSubtaskIssueType("And other lifeforms");
        addSubtaskIssueType("a little but important subtask");
        addSubtaskIssueType("Also kill all mutants");

        assertions.text().assertTextSequence( new IdLocator(tester, "issue-types-table"),
                "Heroic mission on Bruce Willis Asteroid",
                "Heroic mission on Mars",
                "Kill all humans task",
                "Tell interesting story, Mark",
                "another Heroic mission on Mars",
                "Also kill all mutants",
                "And other lifeforms",
                "a little but important subtask"
                );
    }

    @Test
    public void testCanNotAddIssueTypeWithEmptyName(){
        tester.clickLink("add-issue-type");
        tester.setFormElement("name", "");
        tester.submit();
        assertions.getJiraFormAssertions().assertAuiFieldErrMsg("You must specify a name.");
    }

    private void assertDuplicateIssueTypeError() {
        assertions.getJiraFormAssertions().assertFieldErrMsg("An issue type with this name already exists.");
    }

    private void assertDuplicateIssueTypeErrorAui() {
        assertions.getJiraFormAssertions().assertAuiFieldErrMsg("An issue type with this name already exists.");
    }

    private void addIssueType(String name) {
        tester.setFormElement("name", name);
        tester.submit();
    }

    private void addSubtaskIssueType(String name) {
        tester.clickLink("add-issue-type");
        tester.setFormElement("name", name);
        tester.checkRadioOption("style", "jira_subtask");
        tester.submit();
    }

    private void addTaskIssueType(String name) {
        tester.clickLink("add-issue-type");
        tester.setFormElement("name", name);
        tester.checkRadioOption("style", "");
        tester.submit();
    }
}
