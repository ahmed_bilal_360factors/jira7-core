package com.atlassian.jira.functest.rule;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.functest.framework.backdoor.InstanceFeaturesControl;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Supplier;

import static com.atlassian.jira.functest.framework.FunctTestConstants.BIGPIPE_KILLSWITCH_FLAG;
import static java.util.Optional.of;

/**
 * @since 7.2
 */
public class DisableBigPipeRule implements TestRule {
    private static final Logger log = LoggerFactory.getLogger(DisableBigPipeRule.class);

    private final Supplier<Backdoor> backdoor;

    public DisableBigPipeRule(final Supplier<Backdoor> backdoorModule) {
        backdoor = backdoorModule;
    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try {
                    if (!isBigpipeEnabled()) {
                        base.evaluate();
                    } else {
                        setBigpipeEnabled(false);
                        base.evaluate();
                        setBigpipeEnabled(true);
                    }
                } catch (Exception e) {
                    // This is ugly, until we attack JSEV-261 we have to do this
                    log.error("Could not disable BigPipe", e);
                    base.evaluate();
                }
            }
        };
    }

    private boolean isBigpipeEnabled() {
        InstanceFeaturesControl instanceFeaturesControl = getInstanceFeaturesControl();
        return !instanceFeaturesControl.isEnabled(BIGPIPE_KILLSWITCH_FLAG);
    }

    private void setBigpipeEnabled(boolean flag) {
        InstanceFeaturesControl instanceFeaturesControl = getInstanceFeaturesControl();
        of(BIGPIPE_KILLSWITCH_FLAG).ifPresent(flag ? instanceFeaturesControl::disable : instanceFeaturesControl::enable);
    }

    private InstanceFeaturesControl getInstanceFeaturesControl() {
        return backdoor.get().instanceFeaturesControl();
    }
}
