package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;


import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookListener;
import com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookRegistrationClient;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.functest.matcher.WebHookMatchers.containsWebHook;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.WEBHOOKS, Category.REFERENCE_PLUGIN})
@Restore("TestWebHooks.zip")
public class TestWebHookUpgradeTask extends AbstractWebHookTest {

    @Before
    public void setUp() throws Exception {
        super.setUpTest();
    }

    @Test
    public void testUpgradingWebHooks() {
        WebHookRegistrationClient client = new WebHookRegistrationClient(environmentData);
        final List<WebHookRegistrationClient.RegistrationResponse> allWebHooks = client.getAllWebHooks();

        final String[] allEvents = new String[]{"jira:issue_created", "jira:issue_deleted", "jira:issue_updated", "jira:worklog_updated"};
        assertEquals("All WebHooks should have been upgraded", 7, allWebHooks.size());

        assertThat(allWebHooks, containsWebHook(
                new WebHookListener("WebHooks without details", "http://localhost:500/${issue.key}/no-details", new String[]{"jira:issue_created"}, "", true, 6)));

        assertThat(allWebHooks, containsWebHook(
                new WebHookListener("WebHook with simple JQL", "http://localhost:100", new String[]{"jira:issue_created"}, "project = DEMO AND issuetype = Bug", false, 5)));

        assertThat(allWebHooks, containsWebHook(
                new WebHookListener("WebHook without details and with filter", "https://localhost:600", allEvents, "issuetype = Improvement", true, 7)));

        assertThat(allWebHooks, containsWebHook(
                new WebHookListener("WebHook with transitions", "http://localhost:700", allEvents, "", true, 8)));

        assertThat(allWebHooks, containsWebHook(
                new WebHookListener("WebHook no events", "http://localhost:400/no-events/${issue.key}", allEvents, "", false, 2)));

        assertThat(allWebHooks, containsWebHook(
                new WebHookListener("WebHook with exclude and transition", "http://localhost:200", allEvents, "", true, 1)));

        assertThat(allWebHooks, containsWebHook(
                new WebHookListener("WebHook some events only", "http://localhost:300/${issue.key}", new String[]{"jira:issue_deleted", "jira:issue_updated"}, "project = Demonstration", false, 3)));

        // can add WebHook after upgrade
        WebHookRegistrationClient.Registration newRegistration = new WebHookRegistrationClient.Registration();
        newRegistration.events = allEvents;
        newRegistration.setFilterForIssueSection("project = DEMO");
        newRegistration.url = "http://localhost:900";
        newRegistration.name = "WebHook added after import";
        client.register(newRegistration);

        final WebHookRegistrationClient.RegistrationResponse webHook = client.getWebHook("9");
        assertThat(Collections.singletonList(webHook), containsWebHook(
                new WebHookListener("WebHook added after import", "http://localhost:900", allEvents, "project = DEMO", false, 9)));
    }
}
