package com.atlassian.jira.webtests.ztests.comment;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.admin.TimeTracking;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.webtests.Groups;
import org.junit.Test;

import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.permission.ProjectPermissions.ADD_COMMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.google.common.collect.Iterables.concat;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public abstract class TestAddComment extends BaseJiraFuncTest {

    private final String commentLinkid;
    @Inject
    private HtmlPage page;
    @Inject
    private Administration administration;
    @Inject
    private LocatorFactory locator;
    @Inject
    private TextAssertions textAssertions;

    TestAddComment(String commentLinkid) {
        this.commentLinkid = commentLinkid;
    }

    @Test
    @RestoreBlankInstance
    public void testCannotAddCommentWithoutIssue() {
        navigation.gotoPage(page.addXsrfToken("/secure/AddComment.jspa"));
        tester.assertTitleEquals("Error - jWebTest JIRA installation");
        textAssertions.assertTextPresent(new CssLocator(tester, "#content .error"), "The issue no longer exists.");
    }

    @Test
    @Restore("TestAddComment.xml")
    public void testCommentVisiblityOrdering() {
        final String FIRST_GROUP_NAME = "a group";
        final String LAST_GROUP_NAME = "z group";
        administration.usersAndGroups().addGroup(FIRST_GROUP_NAME);
        administration.usersAndGroups().addGroup(LAST_GROUP_NAME);
        administration.usersAndGroups().addUserToGroup(ADMIN_USERNAME, FIRST_GROUP_NAME);
        administration.usersAndGroups().addUserToGroup(ADMIN_USERNAME, LAST_GROUP_NAME);
        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink(commentLinkid);
        tester.setWorkingForm("comment-add");
        String[] commentLevels = tester.getDialog().getOptionLabelsFromSelectList("commentLevel");
        assertEquals(9, commentLevels.length);
        assertEquals("All Users", commentLevels[0]);
        assertEquals("Administrators", commentLevels[1]);
        assertEquals("Developers", commentLevels[2]);
        assertEquals("Users", commentLevels[3]);
        assertEquals(FIRST_GROUP_NAME, commentLevels[4]);
        assertEquals("jira-administrators", commentLevels[5]);
        assertEquals("jira-developers", commentLevels[6]);
        assertEquals("jira-users", commentLevels[7]);
        assertEquals(LAST_GROUP_NAME, commentLevels[8]);
    }

    @Test
    @Restore("TestAddComment.xml")
    public void testAddInvalidComment() {
        navigation.issue().gotoIssue("HSP-1");

        // empty is not ok for AddComment action
        addComment("All Users", "");
        tester.assertTextPresent("Comment body can not be empty!");

        // all spaces on the other hand is not considered kosher!
        navigation.issue().gotoIssue("HSP-1");
        addComment("All Users", "     ");
        tester.assertTextPresent("Comment body can not be empty!");
    }

    @Test
    @Restore("TestBlankInstancePlusAFewUsers.xml")
    public void testAddCommentWithVisibility() {
        String allUsersComment = "This is a comment assigned to all users";
        String jiraUsersGroupComment = "this comment visible to jira-users group";
        String jiraUsersRoleComment = "this is a comment visible to Users role"; //role Users
        String jiraDevelopersGroupComment = "this is a comment visible to jira-developers group";
        String jiraDevelopersRoleComment = "this is a comment visible to Developers role"; //role Developers
        String jiraAdminsGroupComment = "this is a comment visible to jira-admin group";
        String jiraAdminsRoleComment = "this is a comment visible to Administrators role"; //Administrators role

        navigation.issue().gotoIssue("HSP-5");

        //create comment visible to all users
        addComment("All Users", allUsersComment);

        //create comments visible to all jira users
        addComment("jira-users", jiraUsersGroupComment);
        addComment("Users", jiraUsersRoleComment);

        //create comments visible to jira developers
        addComment("jira-developers", jiraDevelopersGroupComment);
        addComment("Developers", jiraDevelopersRoleComment);

        //create comments visible to all admins
        addComment("jira-administrators", jiraAdminsGroupComment);
        addComment("Administrators", jiraAdminsRoleComment);

        List<String> userComments = asList(allUsersComment, jiraUsersRoleComment, jiraUsersGroupComment);
        List<String> devComments = asList(jiraDevelopersGroupComment, jiraDevelopersRoleComment);
        List<String> adminComments = asList(jiraAdminsGroupComment, jiraAdminsRoleComment);

        // verify that Fred can see general comment but not others as he is not in the visibility groups
        checkCommentVisibility(FRED_USERNAME, "HSP-5", userComments, concat(devComments, adminComments));

        // verify that Admin can see all comments as he is not in all visibility groups
        //list userComments now becomes all comments as we have added two lists together
        checkCommentVisibility(ADMIN_USERNAME, "HSP-5", concat(concat(userComments, devComments), adminComments), null);

        //verify that developer only user can only view developer comments
        checkCommentVisibility("devman", "HSP-5", concat(devComments, userComments), adminComments);

        //veryify that onlyadmin guy can only see admin stuff
        checkCommentVisibility("onlyadmin", "HSP-5", concat(adminComments, userComments), devComments);
    }

    private void checkCommentVisibility(String username, String issueKey, Iterable<String> expectedVisible, Iterable<String> expectedNotVisible) {
        if (expectedVisible != null) {
            assertions.comments(expectedVisible).areVisibleTo(username, issueKey);
        }
        if (expectedNotVisible != null) {
            assertions.comments(expectedNotVisible).areNotVisibleTo(username, issueKey);
        }
    }

    @Test
    @Restore("TestAddComment.xml")
    public void testAddCommentErrorWhenLoggedOut() {
        navigation.logout();
        String theComment = "comment with html <input type=\"input\" id=\"invalid\"/>";
        page.getFreshXsrfToken();
        navigation.gotoPage(page.addXsrfToken("/secure/AddComment.jspa?id=10000&comment=" + theComment));
        tester.assertTextPresent(
                "It seems that you have tried to perform an operation which you are not permitted to perform.");
    }

    @Test
    @Restore("TestAddComment.xml")
    public void testAddCommentErrorWhenNoPermission() {
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ADD_COMMENTS, Groups.USERS);
        String theComment = "comment with html <input type=\"input\" id=\"invalid\"/>";
        navigation.gotoPage(page.addXsrfToken("/secure/AddComment.jspa?id=10000&comment=" + theComment));
        tester.assertTextPresent(ADMIN_FULLNAME + ", you do not have the permission to comment on this issue.");
    }

    @Test
    @Restore("TestAddComment.xml")
    public void testAddCommentErrorWhenIssueDoesNotExist() {
        navigation.issue().deleteIssue("HSP-1");
        String theComment = "comment with html <input type=\"input\" id=\"invalid\"/>";
        navigation.gotoPage(page.addXsrfToken("/secure/AddComment.jspa?id=10000&comment=" + theComment));
        tester.assertTextPresent("The issue no longer exists.");
        tester.assertTextNotPresent(theComment); //should have been escaped
        tester.assertTextPresent("The issue no longer exists.");
    }

    private void addComment(String visibleTo, String comment) {
        tester.clickLink(commentLinkid);
        tester.selectOption("commentLevel", visibleTo);
        tester.setFormElement("comment", comment);
        tester.submit();
    }

    @Test
    @RestoreBlankInstance
    public void testAddCommentWithGroupButNotLoggedIn() throws Exception {
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
        String key = navigation.issue().createIssue(PROJECT_HOMOSAP, "Bug", "Test Issue");
        navigation.issue().gotoIssue(key);
        tester.assertTextPresent("Test Issue");

        final String id = navigation.issue().getId(key);

        navigation.gotoPage(page.addXsrfToken("/secure/AddComment.jspa?id=" + id + "&comment=Hello"));
        tester.assertTextPresent("Test Issue");
        tester.assertTextPresent("Hello");
        tester.assertTextNotPresent("Ahoj");

        navigation.gotoPage(page.addXsrfToken(
                "/secure/AddComment.jspa?id=" + id + "&comment=Ahoj&commentLevel=group%3Ajira-users"));
        tester.assertTextPresent("Test Issue");
        tester.assertTextPresent("Hello");
        tester.assertTextPresent("Ahoj");

        /// Make HSP project visible to anonymous users
        addBrowseProjectPermissionToAnonymous();
        addCreateCommentPermissionToAnonymous();
        administration.timeTracking().enable(TimeTracking.Mode.MODERN);
        navigation.logout();

        page.getFreshXsrfToken();
        navigation.gotoPage(page.addXsrfToken(
                "/secure/AddComment.jspa?id=" + id + "&comment=Hola&commentLevel=group%3Ajira-users"));
        tester.assertTextNotPresent("NullPointerException");
        tester.assertTextPresent(
                "You cannot add a comment for specific groups or roles, as your session has expired. Please log in and try again.");

        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    @Restore("TestAddComment.xml")
    public void testAddCommentCheckUpdatedDate() throws ParseException {
        navigation.issue().gotoIssue("HSP-1");
        textAssertions.assertTextPresent(new IdLocator(tester, "created-val"), "14/Aug/06 4:26 PM");
        textAssertions.assertTextPresent(new IdLocator(tester, "updated-val"), "14/Aug/06 4:26 PM");
        final String commentText = "This is my first test comment!";
        tester.assertTextNotPresent(commentText);

        tester.clickLink(commentLinkid);
        tester.setFormElement("comment", commentText);
        tester.submit();

        tester.assertTextPresent(commentText);

        //now check the updated time is the same as the one of the comment (kinda, since the updated time is pretty formatted while the comments is not, but we can assert the time)
        String dateString = locator.css("span.date").getNodes()[0].getNodeValue();
        SimpleDateFormat format = new SimpleDateFormat("dd/MMM/yy h:mm a");
        final Date date = format.parse(dateString);

        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
        String timeString = timeFormat.format(date);
        textAssertions.assertTextPresent(new IdLocator(tester, "created-val"), "14/Aug/06 4:26 PM");
        textAssertions.assertTextPresent(new IdLocator(tester, "updated-val"), timeString);
    }

    @Test
    @Restore("TestAddComment.xml")
    public void testAddTooLongComment() {
        backdoor.advancedSettings().setTextFieldCharacterLengthLimit(10);
        navigation.issue().gotoIssue("HSP-1");

        addComment("All Users", "This is too long comment");
        tester.assertTextPresent("The entered text is too long. It exceeds the allowed limit of 10 characters.");
    }

    @Test
    @Restore("TestAddComment.xml")
    public void testAddCommentWithTextLengthLimitOn() {
        backdoor.advancedSettings().setTextFieldCharacterLengthLimit(10);
        navigation.issue().gotoIssue("HSP-1");

        final String correctCommentBody = "AllGood";
        addComment("All Users", correctCommentBody);
        assertions.comments(Collections.singletonList(correctCommentBody)).areVisibleTo("admin", "HSP-1");
    }

    private void addBrowseProjectPermissionToAnonymous() {
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_PERM_SCHEME_ID, BROWSE_PROJECTS);
    }

    private void addCreateCommentPermissionToAnonymous() {
        backdoor.permissionSchemes().addEveryonePermission(DEFAULT_PERM_SCHEME_ID, ADD_COMMENTS);
    }
}
