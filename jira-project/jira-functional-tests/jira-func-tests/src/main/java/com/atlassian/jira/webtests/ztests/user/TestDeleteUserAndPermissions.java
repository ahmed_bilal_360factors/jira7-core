package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.admin.user.DeleteUserPage;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.matcher.PermissionSchemesMatcher;
import com.atlassian.jira.permission.JiraPermissionHolderType;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.testkit.beans.PermissionSchemeBean;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.webtests.Groups;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMINISTER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BULK_CHANGE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.CLOSE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ATTACHMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.DELETE_ALL_ATTACHMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.DELETE_ALL_COMMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.DELETE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.EDIT_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.LINK_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.MANAGE_WATCHERS;
import static com.atlassian.jira.permission.ProjectPermissions.MOVE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.RESOLVE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.SCHEDULE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.WORK_ON_ISSUES;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertTrue;

@WebTest({Category.FUNC_TEST, Category.PERMISSIONS, Category.USERS_AND_GROUPS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestDeleteUserAndPermissions extends BaseJiraFuncTest {
    /**
     * This file import contains two projects. One project has a specific permission scheme that only allows user fred
     * to perform any instructions. Both fred and admin have public filters set up with each subscribing to each other's
     * filters. There are two issues created by fred, one in each of the two projects
     */
    private static final String TWO_PROJECTS_WITH_SUBSCRIPTIONS = "TestDeleteUserAndPermissions.xml";
    private static final Long FREDS_PERMISSION_SCHEME_ID = 10000L;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreData(TWO_PROJECTS_WITH_SUBSCRIPTIONS);
        administration.addGlobalPermission(BULK_CHANGE, Groups.USERS);
        backdoor.darkFeatures().enableForSite("jira.no.frother.reporter.field");
    }

    @After
    public void tearDownTest() {
        backdoor.darkFeatures().disableForSite("jira.no.frother.reporter.field");
        administration.removeGlobalPermission(BULK_CHANGE, Groups.USERS);
    }

    //------------------------------------------------------------------------------------------------------------ Tests

    @Test
    public void testDeleteUserNotPossibleWithAssignedIssue() {
        logger.log("Test Delete User Not Possible With Assigned Issue");

        logger.log("Making sure that you can't see the other issue");
        navigation.issueNavigator().displayAllIssues();
        tester.assertTextPresent("Seen issue");
        tester.assertTextNotPresent("Unseen issue");

        logger.log("Ensuring that you're unable to delete fred and that the correct number of issues are shown");
        DeleteUserPage deleteUserPage = navigation.gotoPageWithParams(DeleteUserPage.class, DeleteUserPage.generateDeleteQueryParameters(FRED_USERNAME));
        assertThat(deleteUserPage.getUserDeletionError(), equalTo(deleteUserPage.getUserCannotBeDeleteMessage(FRED_USERNAME)));
        assertThat(deleteUserPage.getNumberFromErrorFieldNamed(DeleteUserPage.ASSIGNED_ISSUES), equalTo("1"));
        assertThat(deleteUserPage.getNumberFromErrorFieldNamed(DeleteUserPage.REPORTED_ISSUES), equalTo("2"));
        assertThat(deleteUserPage.getNumberFromWarningFieldNamed(DeleteUserPage.SHARED_FILTERS), equalTo("1"));
    }

    @Test
    public void testDeleteUserRemoveFromPermissionAndNotificationSchemes() {
        logger.log("Test delete user with shared filters");

        assertFredHasPermissionsAssigned();

        // assert Fred is in issue security scheme
        navigation.gotoAdminSection(Navigation.AdminSection.SECURITY_SCHEMES);
        tester.clickLinkWithText("Test Issue Security Scheme");
        tester.assertTextPresent(FRED_USERNAME);

        // assert Fred is in Default Notification Scheme
        navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);
        tester.clickLinkWithText("Default Notification Scheme");
        tester.assertTextPresent(FRED_USERNAME);
        tester.assertLinkPresent("del_10050");

        // add permissions to admin so we can edit MKY-1
        navigation.gotoAdminSection(Navigation.AdminSection.PERMISSION_SCHEMES);
        backdoor.permissionSchemes().addUserPermission(FREDS_PERMISSION_SCHEME_ID, EDIT_ISSUES, ADMIN_USERNAME);
        backdoor.permissionSchemes().addUserPermission(FREDS_PERMISSION_SCHEME_ID, BROWSE_PROJECTS, ADMIN_USERNAME);
        backdoor.permissionSchemes().addUserPermission(FREDS_PERMISSION_SCHEME_ID, ProjectPermissions.MODIFY_REPORTER, ADMIN_USERNAME);

        // remove fred from issue
        navigation.issue().gotoIssue("HSP-1");
        tester.clickLink("edit-issue");
        tester.setFormElement("reporter", ADMIN_USERNAME);
        tester.submit("Update");
        navigation.issue().gotoIssue("MKY-1");
        tester.clickLink("edit-issue");
        tester.setFormElement("reporter", ADMIN_USERNAME);
        tester.selectOption("assignee", ADMIN_FULLNAME);
        tester.submit("Update");

        logger.log("Deleting Fred");
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.clickLink(FRED_USERNAME);
        tester.clickLink("deleteuser_link");
        tester.assertTextPresent("Delete User");
        tester.submit("Delete");
        assertions.assertNodeByIdExists("user_browser_table");
        tester.assertTextNotPresent(FRED_USERNAME);

        assertFredHasNoPermissionsAssigned();

        // assert Fred isn't in issue security scheme
        navigation.gotoAdminSection(Navigation.AdminSection.SECURITY_SCHEMES);
        tester.clickLinkWithText("Test Issue Security Scheme");
        tester.assertTextNotPresent(FRED_USERNAME);

        // assert Fred isn't in Default Notification Scheme
        navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);
        tester.clickLinkWithText("Default Notification Scheme");
        tester.assertTextNotPresent(FRED_USERNAME);
        tester.assertLinkNotPresent("del_10050");
    }

    @Test
    public void testCannotSelfDestruct() {
        administration.restoreBlankInstance();
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);

        DeleteUserPage deleteUserPage = navigation.gotoPageWithParams(DeleteUserPage.class, DeleteUserPage.generateDeleteQueryParametersWithXsrf("admin", page));
        assertThat(deleteUserPage.getUserDeleteSelfError(), equalTo(DeleteUserPage.USER_CANNOT_DELETE_SELF));
        deleteUserPage.confirmNoDeleteButtonPresent();
    }

    @Test
    public void testAdminCannotDeleteSysadmin() {
        administration.restoreBlankInstance();
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
        try {
            // create a sysadmin to attempt to delete (also not a project lead like "admin")
            administration.usersAndGroups().addUser("sysadmin2");
            administration.usersAndGroups().addUserToGroup("sysadmin2", "jira-administrators"); // should have system administrator permission now

            // create a normal admin (non system admin)
            administration.usersAndGroups().addUser("nonsystemadmin");
            administration.usersAndGroups().addGroup("nonsystemadmins");
            administration.usersAndGroups().addUserToGroup("nonsystemadmin", "nonsystemadmins");
            administration.addGlobalPermission(ADMINISTER, "nonsystemadmins");

            navigation.login("nonsystemadmin");

            navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
            tester.assertLinkNotPresent("deleteuser_link_sysadmin2");
            // Hack the url
            DeleteUserPage deleteUserPage = navigation.gotoPageWithParams(DeleteUserPage.class, DeleteUserPage.generateDeleteQueryParametersWithXsrf("sysadmin2", page));
            assertThat(deleteUserPage.getNonAdminDeletingSysAdminErrorMessage(), equalTo(DeleteUserPage.USER_CANNOT_DELETE_SYSADMIN));
            deleteUserPage.confirmNoDeleteButtonPresent();

            // try to hack the URL for getting to the form to delete the user
            deleteUserPage = navigation.gotoPageWithParams(DeleteUserPage.class, DeleteUserPage.generateDeleteQueryParametersWithXsrf("sysadmin2", page) + "&confirm=true");
            assertThat(deleteUserPage.getNonAdminDeletingSysAdminErrorMessage(), equalTo(DeleteUserPage.USER_CANNOT_DELETE_SYSADMIN));

            navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
            tester.assertLinkPresent("sysadmin2"); // check user is still in user list

        } finally {
            navigation.login(ADMIN_USERNAME);
        }
    }

    private void assertFredHasPermissionsAssigned() {
        assertFredHasNoPermissionsAssignedInPermissionScheme(DEFAULT_PERM_SCHEME_ID);

        PermissionSchemeBean permissionScheme = backdoor.permissionSchemes().getAssignedPermissions(FREDS_PERMISSION_SCHEME_ID);

        assertHasPermission(permissionScheme, ADMINISTER_PROJECTS);
        assertHasPermission(permissionScheme, BROWSE_PROJECTS);
        assertHasPermission(permissionScheme, CREATE_ISSUES);
        assertHasPermission(permissionScheme, EDIT_ISSUES);
        assertHasPermission(permissionScheme, SCHEDULE_ISSUES);
        assertHasPermission(permissionScheme, MOVE_ISSUES);
        assertHasPermission(permissionScheme, ProjectPermissions.ASSIGNABLE_USER);
        assertHasPermission(permissionScheme, RESOLVE_ISSUES);
        assertHasPermission(permissionScheme, CLOSE_ISSUES);
        assertHasPermission(permissionScheme, ProjectPermissions.MODIFY_REPORTER);
        assertHasPermission(permissionScheme, DELETE_ALL_COMMENTS);
        assertHasPermission(permissionScheme, DELETE_ISSUES);
        assertHasPermission(permissionScheme, WORK_ON_ISSUES);
        assertHasPermission(permissionScheme, LINK_ISSUES);
        assertHasPermission(permissionScheme, CREATE_ATTACHMENTS);
        assertHasPermission(permissionScheme, DELETE_ALL_ATTACHMENTS);
        assertHasPermission(permissionScheme, ProjectPermissions.VIEW_VOTERS_AND_WATCHERS);
        assertHasPermission(permissionScheme, MANAGE_WATCHERS);
        assertHasPermission(permissionScheme, ProjectPermissions.SET_ISSUE_SECURITY);
    }

    private void assertHasPermission(PermissionSchemeBean permissionScheme, ProjectPermissionKey permission) {
        assertTrue("Fred should have " + permission.permissionKey() + " permission", PermissionSchemesMatcher.hasPermission(permission.permissionKey(), JiraPermissionHolderType.USER.getKey(), "fred").matches(permissionScheme));
    }

    private void assertFredHasNoPermissionsAssigned() {
        assertFredHasNoPermissionsAssignedInPermissionScheme(DEFAULT_PERM_SCHEME_ID);
        assertFredHasNoPermissionsAssignedInPermissionScheme(FREDS_PERMISSION_SCHEME_ID);
    }

    private void assertFredHasNoPermissionsAssignedInPermissionScheme(Long schemeId) {
        PermissionSchemeBean permissionScheme = backdoor.permissionSchemes().getAssignedPermissions(schemeId);

        assertTrue("Fred should have no permissions in the " + permissionScheme.getName() + " permission scheme", PermissionSchemesMatcher.hasPermissionCount(JiraPermissionHolderType.USER.getKey(), "fred", 0).matches(permissionScheme));
    }
}
