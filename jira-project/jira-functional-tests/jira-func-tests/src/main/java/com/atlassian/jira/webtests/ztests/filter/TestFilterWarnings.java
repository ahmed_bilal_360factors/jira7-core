package com.atlassian.jira.webtests.ztests.filter;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.FILTERS})
@LoginAs(user = ADMIN_USERNAME)
public class TestFilterWarnings extends BaseJiraFuncTest {
    private static final String NO_MAIL_SERVER_IS_CURRENTLY_CONFIGURED =
            "No mail server is currently configured. Notifications will not be sent.";

    /**
     * Set up a simple instance with a filter and a subscription to it.
     */
    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
        String filter = backdoor.filters().createFilter("", "Test Filter");
        navigation.manageFilters().addSubscription(Integer.parseInt(filter, 10));
    }

    private void addSubscription() {

        tester.checkCheckbox("filter.subscription.prefix.dailyWeeklyMonthly", "daysOfMonth");
        tester.submit("Subscribe");
    }

    @Test
    public void testMailNotConfiguredWarning() {
        addSubscription();

        navigation.manageFilters().goToDefault();
        navigation.manageFilters().manageSubscriptions(10000);
        textAssertions.assertTextPresent(locator.page(), NO_MAIL_SERVER_IS_CURRENTLY_CONFIGURED);
        textAssertions.assertTextNotPresent(locator.page(), "Run now");
    }

    @Test
    public void testMailConfiguredNoWarning() {
        addSubscription();

        navigation.gotoDashboard();
        administration.mailServers().Smtp().goTo().add("testserver", "admin@example.com", "JIRA", "mail.example.com");

        navigation.manageFilters().goToDefault();
        navigation.manageFilters().manageSubscriptions(10000);
        textAssertions.assertTextNotPresent(locator.page(), NO_MAIL_SERVER_IS_CURRENTLY_CONFIGURED);
        textAssertions.assertTextPresent(locator.page(), "Run now");
    }

    @Test
    public void testNoMailServerNoSubscriptions() {
        navigation.manageFilters().goToDefault();
        textAssertions.assertTextNotPresent(locator.page(), NO_MAIL_SERVER_IS_CURRENTLY_CONFIGURED);
    }
}
