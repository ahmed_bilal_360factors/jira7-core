package com.atlassian.jira.webtests.ztests.bulk;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.navigation.IssueNavigatorNavigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.0
 */
@WebTest({Category.FUNC_TEST, Category.BULK_OPERATIONS})
@LoginAs(user = ADMIN_USERNAME)
public class TestBulkTransition extends BaseJiraFuncTest {
    /**
     * Tests for regression of http://jira.atlassian.com/browse/JRA-18359
     */
    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testBulkTransitionDuplicateWorkflows() {
        administration.restoreData("TestBulkTransitionDuplicateWorkflows.xml");
        // Click Link 'Issues' (id='find_link').
        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator().bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES);
        tester.checkCheckbox("bulkedit_10000", "on");
        tester.checkCheckbox("bulkedit_10001", "on");
        tester.checkCheckbox("bulkedit_10002", "on");
        tester.submit("Next");
        tester.checkCheckbox("operation", "bulk.workflowtransition.operation.name");
        tester.checkCheckbox("operation", "bulk.workflowtransition.operation.name");
        tester.checkCheckbox("operation", "bulk.workflowtransition.operation.name");
        tester.assertTextPresent("Step 2 of 4: Choose Operation");
        tester.submit("Next");
        tester.assertTextPresent("Step 3 of 4: Operation Details");
        tester.assertTextPresent("Select the workflow transition to execute on the associated issues.");
        // Assert that we don't have multiple copies of Workflow
        textAssertions.assertTextPresentNumOccurences("Workflow: classic default workflow", 1);
    }
}
