package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.meterware.httpunit.WebForm;
import org.junit.After;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.BUTTON_NAME_NEXT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static org.junit.Assert.assertEquals;

/**
 * Test some basic operations in JIRA in German.
 */
@WebTest({Category.FUNC_TEST, Category.BROWSING})
@RestoreBlankInstance
@LoginAs(user = FunctTestConstants.ADMIN_USERNAME)
public class TestBasic extends BaseJiraFuncTest {
    private static final String NEW_FEATURE_I18N_KEY = "jira.translation.issuetype.newfeature.name";
    private static final String BUG_I18N_KEY = "jira.translation.issuetype.bug.name";
    private static final String GERMAN_LOCALE = "de_DE";
    private static final String IMPROVEMENT_I18N_KEY = "jira.translation.issuetype.improvement.name";
    private static final String TASK_I18N_KEY = "jira.translation.issuetype.task.name";
    @Inject
    private LocatorFactory locator;
    @Inject
    private TextAssertions textAssertions;

    @After
    public void revertLocaleChanges() {
        navigation.userProfile().changeUserLanguageToJiraDefault();
    }

    @Test
    public void testI18NDates() throws IOException {
        setLocaleTo("Deutsch (Deutschland)");

        // create an issue with a valid due date
        createIssueInGermanWithDueDate("25/Dez/05");
        tester.assertTextPresent("Es liegen noch keine Kommentare zu diesem Vorgang vor.");

        // create an issue with an invalid due date
        createIssueInGermanWithDueDate("25/Dec/05");
        tester.assertTextPresent("Datum eingegeben. Geben Sie das Datum im Format");
    }

    @Test
    public void testIssueConstantTranslations() {
        setLocaleTo("Deutsch (Deutschland)");
        // reset the translation to blank
        updateBugTranslationWith("", "");

        // browse to the admin section and make sure that we see the issue constants as translated
        browseToCustomFieldAdd();

        tester.assertTextPresent("Alle Vorgangstypen");
        tester.assertTextPresent("Bug");
        tester.assertTextPresent(getBackdoor().i18n().getText(IMPROVEMENT_I18N_KEY, GERMAN_LOCALE));
        tester.assertTextPresent(getBackdoor().i18n().getText(NEW_FEATURE_I18N_KEY, GERMAN_LOCALE));
        tester.assertTextPresent(getBackdoor().i18n().getText(TASK_I18N_KEY, GERMAN_LOCALE));

        // add a translation via the GUI, confirm present in place of the default properties
        updateBugTranslationWith("bugenzee", "bugenzee desc");

        browseToCustomFieldAdd();
        textAssertions.assertTextPresent(locator.css(".jiraform"), "bugenzee");
        textAssertions.assertTextNotPresent(locator.css(".jiraform"), "Fehler");
    }

    private void updateBugTranslationWith(final String name, final String desc) {
        navigation.gotoAdminSection(Navigation.AdminSection.ISSUE_TYPES);
        tester.clickLink("translate_link");
        tester.setWorkingForm("update");
        tester.setFormElement("jira.translation.Vorgangstyp.1.name", name);
        tester.setFormElement("jira.translation.Vorgangstyp.1.desc", desc);
        tester.submit();
    }

    private void browseToCustomFieldAdd() {
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
        tester.clickLink("add_custom_fields");
        tester.checkCheckbox("fieldType", "com.atlassian.jira.plugin.system.customfieldtypes:textarea");
        tester.submit(BUTTON_NAME_NEXT);
    }

    private void createIssueInGermanWithDueDate(final String dueDate) {
        navigation.issue().
                goToCreateIssueForm(PROJECT_HOMOSAP, getBackdoor().i18n().getText(BUG_I18N_KEY, GERMAN_LOCALE));

        tester.setWorkingForm("issue-create");
        final WebForm form = tester.getDialog().getForm();
        form.setParameter("duedate", dueDate);
        form.setParameter("summary", "test issue");
        assertEquals("Erstellen", form.getButtonWithID("issue-create-submit").getValue());
        tester.submit("Create");
    }

    private void setLocaleTo(final String localeName) {
        navigation.userProfile().changeUserLanguage(localeName);
    }
}