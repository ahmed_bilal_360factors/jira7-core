package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.Locator;
import com.atlassian.jira.functest.framework.locator.TableCellLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestShareUserDefault extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("TestShareUserDefaults.xml");
    }

    @After
    public void tearDownTest() {
        administration.restoreBlankInstance();
    }

    @Test
    public void testDefaults() {
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);

        // Check the initial state for the defaults
        navigation.gotoAdminSection(Navigation.AdminSection.USER_DEFAULTS);
        Locator locator = new TableCellLocator(tester, "view_user_defaults", 3, 0);
        textAssertions.assertTextPresent(locator, "Default sharing for filters and dashboards");
        locator = new TableCellLocator(tester, "view_user_defaults", 3, 1);
        textAssertions.assertTextPresent(locator, "Private");

        // Check the initial state for the user
        tester.gotoPage("secure/ViewProfile.jspa");
        tester.clickLink("edit_prefs_lnk");
        tester.assertRadioOptionPresent("shareDefault", "true");
        tester.assertRadioOptionPresent("shareDefault", "false");
        tester.assertRadioOptionSelected("shareDefault", "true");

        // Change the default for sharing
        navigation.gotoAdminSection(Navigation.AdminSection.USER_DEFAULTS);
        tester.clickLinkWithText("Edit default values");
        tester.setFormElement("sharePublic", "false");
        tester.submit("Update");

        // Ensure the default has changed
        locator = new TableCellLocator(tester, "view_user_defaults", 3, 0);
        textAssertions.assertTextPresent(locator, "Default sharing for filters and dashboards");
        locator = new TableCellLocator(tester, "view_user_defaults", 3, 1);
        textAssertions.assertTextPresent(locator, "Public");

        // Ensure the user's setting hasn't changed
        tester.gotoPage("secure/ViewProfile.jspa");
        tester.clickLink("edit_prefs_lnk");
        tester.assertRadioOptionPresent("shareDefault", "true");
        tester.assertRadioOptionPresent("shareDefault", "false");
        tester.assertRadioOptionSelected("shareDefault", "false");
        // Change their share option
        tester.checkCheckbox("shareDefault", "true");
        tester.submit();

        // Ensure their share option has changed
        tester.gotoPage("secure/ViewProfile.jspa");
        tester.clickLink("edit_prefs_lnk");
        tester.assertRadioOptionPresent("shareDefault", "true");
        tester.assertRadioOptionPresent("shareDefault", "false");
        tester.assertRadioOptionSelected("shareDefault", "true");

        // Ensure changing their settings hasn't affected the global default
        navigation.gotoAdminSection(Navigation.AdminSection.USER_DEFAULTS);
        locator = new TableCellLocator(tester, "view_user_defaults", 3, 0);
        textAssertions.assertTextPresent(locator, "Default sharing for filters and dashboards");
        locator = new TableCellLocator(tester, "view_user_defaults", 3, 1);
        textAssertions.assertTextPresent(locator, "Public");
    }
}
