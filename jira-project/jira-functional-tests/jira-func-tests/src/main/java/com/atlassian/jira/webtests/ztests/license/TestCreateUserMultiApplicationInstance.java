package com.atlassian.jira.webtests.ztests.license;

import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.CORE_KEY;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.SERVICE_DESK_KEY;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.SOFTWARE_KEY;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.TEST_KEY;
import static com.atlassian.jira.webtests.LicenseKeys.MULTI_ROLE;
import static com.google.common.collect.Sets.newHashSet;

/**
 * In a "multi application instance" multiple JIRA Applications (products) has been installed.<br>
 * During User Creation the applications should be selectable.
 * <p> When one of the Applications has an error:
 * <ul>
 * <li>Application license limit exceeded.</li>
 * <li>No default group configured.</li>
 * </ul>
 * Then the application is not selectable, a generic error message is displayed on top of user creation dialog and
 * a detailed message is displayed next to application's disabled checkbox.
 */
@WebTest({Category.FUNC_TEST, Category.LICENSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestCreateUserMultiApplicationInstance extends TestCreateUserHelper {
    private static final String PLATFORM_GROUP = "platform-users";
    private static final String TEST_GROUP1 = "test-users";
    private static final String TEST_GROUP2 = "test-users2";
    private static final String ADMIN_GROUP = "jira-administrators";

    @Before
    public void setUp() {
        backdoor.restoreBlankInstance(MULTI_ROLE);

        backdoor.applicationRoles().putRole(TEST_KEY);
        backdoor.applicationRoles().putRole(CORE_KEY);
        backdoor.applicationRoles().putRole(SERVICE_DESK_KEY);
        backdoor.applicationRoles().putRole(SOFTWARE_KEY);
        backdoor.applicationRoles().putRole("jira-reference");
    }

    @Test
    public void testShouldCreateUserAndAddToGroupsOfMultipleApplications() {
        backdoor.usersAndGroups().addGroup(PLATFORM_GROUP);
        //Adding admin group to ensure that admin can still login
        backdoor.applicationRoles().putRoleWithDefaults(CORE_KEY, newHashSet(PLATFORM_GROUP, ADMIN_GROUP), newHashSet(PLATFORM_GROUP));
        backdoor.usersAndGroups().addGroup(TEST_GROUP1);
        backdoor.usersAndGroups().addGroup(TEST_GROUP2);
        backdoor.applicationRoles().putRoleWithDefaults(TEST_KEY, newHashSet(TEST_GROUP1, TEST_GROUP2),
                newHashSet(TEST_GROUP1, TEST_GROUP2));

        populateCreateUserForm("user");
        tester.checkCheckbox("selectedApplications", CORE_KEY);
        tester.checkCheckbox("selectedApplications", TEST_KEY);
        tester.submit("Create");

        assertUserInGroups("user", PLATFORM_GROUP, TEST_GROUP1, TEST_GROUP2);
    }

    @Test
    public void testShouldCreateUserAndOnlyAddToDefaultGroupOfSelectedApplication() {
        backdoor.usersAndGroups().addGroup(PLATFORM_GROUP);
        //Adding admin group to ensure that admin can still login
        backdoor.applicationRoles().putRoleWithDefaults(CORE_KEY, newHashSet(PLATFORM_GROUP, ADMIN_GROUP), newHashSet(PLATFORM_GROUP));
        backdoor.usersAndGroups().addGroup(TEST_GROUP1);
        backdoor.applicationRoles().putRoleAndSetDefault(TEST_KEY, TEST_GROUP1);

        populateCreateUserForm("user");
        //Make sure Test checkbox is selectable
        assertHasCheckbox(TEST_KEY);

        tester.checkCheckbox("selectedApplications", CORE_KEY);
        tester.submit("Create");

        assertUserInGroups("user", PLATFORM_GROUP);
    }

    @Test
    public void testShouldCreateUserWhenAdminHasNotSelectedAnyApplications() {
        backdoor.usersAndGroups().addGroup(PLATFORM_GROUP);
        backdoor.applicationRoles().putRoleWithDefaults(CORE_KEY, newHashSet(PLATFORM_GROUP), newHashSet(PLATFORM_GROUP));
        backdoor.usersAndGroups().addGroup(TEST_GROUP1);
        backdoor.applicationRoles().putRoleAndSetDefault(TEST_KEY, TEST_GROUP1);

        populateCreateUserForm("user");

        assertHasCheckbox(TEST_KEY);
        assertHasCheckbox(CORE_KEY);
        tester.uncheckCheckbox("selectedApplications");

        tester.submit("Create");

        assertUserNotInAnyGroups("user");
    }

    @Test
    public void testShouldWarnUserWhenOneOfAppsHasReachedLicenseLimitAndAllowCreationUsingOtherApp() {
        backdoor.usersAndGroups().addGroup(PLATFORM_GROUP);
        //Adding admin group to ensure that admin can still login
        backdoor.applicationRoles().putRoleWithDefaults(CORE_KEY, newHashSet(PLATFORM_GROUP, ADMIN_GROUP), newHashSet(PLATFORM_GROUP));
        backdoor.usersAndGroups().addGroup(TEST_GROUP1);
        backdoor.applicationRoles().putRoleAndSetDefault(TEST_KEY, TEST_GROUP1);

        //Use up PLATFORM license
        final int LIC_USER_LIMIT = 3;
        // Admin already occupies one seat as the admin group gets added to JIRA Core
        for (int i = 1; i < LIC_USER_LIMIT; i++) {
            populateCreateUserForm("user" + i);
            tester.checkCheckbox("selectedApplications", CORE_KEY);
            tester.submit("Create");
            assertUserInGroups("user" + i, PLATFORM_GROUP);
        }

        populateCreateUserForm("noGroupUser");

        //Warn user with generic message
        //TODO: HIROL-333-Warning messages when creating users with multiple roles

        //PLATFORM should display message next to checkbox
        assertNoCheckboxButWarningButton(CORE_KEY);

        //Should be able to select TEST
        tester.checkCheckbox("selectedApplications", TEST_KEY);

        tester.submit("Create");
        assertUserInGroups("noGroupUser", TEST_GROUP1);
    }

    @Test
    public void testShouldWarnUserWhenApplicationHasNoDefaultGroup() {
        populateCreateUserForm("user");

        //Warn generic message
        //TODO: HIROL-333-Warning messages when creating users with multiple roles

        assertNoCheckboxButWarningButton(TEST_KEY);

        tester.submit("Create");

        assertUserNotInAnyGroups("user");
    }

    /**
     * Tests {@link com.atlassian.jira.application.ApplicationRoleManager#isAnyRoleLimitExceeded()}
     * with a number of users equal to the #users granted by license.
     */
    @Test
    public void testRoleUserLimitsWhenAllRolesUnderLimit() {
        // license grants 4 func test, and 4 software users
        setupRoleWithUsers(TEST_KEY, 4, TEST_GROUP1);
        setupRoleWithUsers(SOFTWARE_KEY, 4, "software-users");

        navigation.gotoAdmin();

        // literal text from i18n key "admin.globalpermissions.user.limit.warning"
        tester.assertTextNotPresent("You have exceeded the number of users allowed to use JIRA");
    }

    /**
     * Tests {@link com.atlassian.jira.application.ApplicationRoleManager#isAnyRoleLimitExceeded()}
     * where #users exceeds licensed limit in 1 of the 2 roles.
     */
    @Test
    public void testRoleUserLimitsWhenOneRoleOverLimit() {
        // license grants 4 func test, and 4 software users
        setupRoleWithUsers(TEST_KEY, 4, TEST_GROUP1);
        setupRoleWithUsers(SOFTWARE_KEY, 5, "software-users");

        navigation.gotoAdmin();

        // literal text from i18n key "admin.globalpermissions.user.limit.single.error"
        tester.assertTextPresent("license's user limit is exceeded.");
    }

    /**
     * Tests {@link com.atlassian.jira.application.ApplicationRoleManager#isAnyRoleLimitExceeded()}
     * where 2 roles are exceeded.
     */
    @Test
    public void testRoleUserLimitsWhenAllRolesOverLimit() {
        // license grants 4 func test, and 4 software users
        setupRoleWithUsers(TEST_KEY, 5, TEST_GROUP1);
        setupRoleWithUsers(SOFTWARE_KEY, 5, "software-users");

        navigation.gotoAdmin();

        // literal text from i18n key "admin.globalpermissions.user.limit.multiple.error"
        tester.assertTextPresent("Users may not be able to create issues");
    }

    /**
     * Create role {@code roleName} with {@code numUsers} users in group {@code groupName}.
     */
    void setupRoleWithUsers(String roleName, int numUsers, String groupName) {
        backdoor.usersAndGroups().addGroup(groupName);
        backdoor.applicationRoles().putRoleAndSetDefault(roleName, groupName);

        String userNamePrefix = "user-of-" + groupName;
        backdoor.usersAndGroups().addUsersWithGroup(userNamePrefix, userNamePrefix, numUsers, groupName);
    }
}
