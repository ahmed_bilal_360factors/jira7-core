package com.atlassian.jira.webtests.ztests.projectconfig.framework;

import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.ws.rs.core.MediaType;
import java.util.List;

import static java.util.Arrays.asList;
import static org.apache.commons.lang.StringUtils.stripToNull;

/**
 * @since v5.2
 */
public class EditDraftWorkflowSchemeResource extends RestApiClient<EditDraftWorkflowSchemeResource> {
    private final String root;

    public EditDraftWorkflowSchemeResource(JIRAEnvironmentData environmentData) {
        super(environmentData);
        root = environmentData.getBaseUrl().toExternalForm();
    }

    @Override
    protected WebResource createResource() {
        return resourceRoot(root).path("rest").path("projectconfig").path("latest").path("draftworkflowscheme");
    }

    private WebResource doCreateResource(String projectKey) {
        return createResource().path(projectKey);
    }

    public SimpleWorkflowScheme edit(String projectKey, String workflowName, String... issueTypeIds) {
        return doCreateResource(projectKey).type(MediaType.APPLICATION_JSON_TYPE).put(SimpleWorkflowScheme.class,
                new AssignIssueTypesRequest(workflowName, issueTypeIds));
    }

    public SimpleWorkflowScheme remove(String projectKey, String workflowName) {
        return doCreateResource(projectKey).type(MediaType.APPLICATION_JSON_TYPE).delete(SimpleWorkflowScheme.class, workflowName);
    }

    public Response editResponse(final String projectKey, final String workflowName, final String... issueTypeIds) {
        return toResponse(new Method() {
            @Override
            public ClientResponse call() {
                return doCreateResource(projectKey).type(MediaType.APPLICATION_JSON_TYPE).put(ClientResponse.class,
                        new AssignIssueTypesRequest(workflowName, issueTypeIds));
            }
        });
    }

    public Response removeResponse(final String projectKey, final String workflowName) {
        return toResponse(new Method() {
            @Override
            public ClientResponse call() {
                return doCreateResource(projectKey).type(MediaType.APPLICATION_JSON_TYPE).delete(ClientResponse.class, workflowName);
            }
        });
    }

    public static class AssignIssueTypesRequest {
        @JsonProperty
        private final String name;
        @JsonProperty
        private final List<String> issueTypes;

        public AssignIssueTypesRequest(String name,
                                       String... issueTypes) {
            this.issueTypes = asList(issueTypes);
            this.name = stripToNull(name);
        }
    }
}