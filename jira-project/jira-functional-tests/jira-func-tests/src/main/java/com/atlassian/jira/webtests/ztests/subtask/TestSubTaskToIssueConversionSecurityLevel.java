package com.atlassian.jira.webtests.ztests.subtask;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CHANGE_HISTORY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_IMPROVEMENT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_NEWFEATURE;

@WebTest({Category.FUNC_TEST, Category.ISSUES, Category.SECURITY, Category.SUB_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestSubTaskToIssueConversionSecurityLevel extends BaseJiraFuncTest {
    private static final String SUB_SL_REQ_NO_VAL = "HSP-8"; // sub task where security level required but parent has none
    private static final String PARENT_NO_VAL = "HSP-7";
    private static final String SUB_SL_REQ_HAS_VAL = "HSP-6"; // sub task where security level required and set to parent
    private static final String SUB_SL_HIDDEN = "HSP-9";

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUp() {
        administration.restoreData("TestSubTaskToIssueConversionSecurityLevel.xml");
    }

    /*
     * Tests that when the issue to convert has no security level and the target type has it as optional
     * the user is not prompted and it is not on the confirm screen, and checks the issue has not been updated
     */
    @Test
    public void testSubTaskToIssueConversionSecurityLevelNoValToOptional() {
        // Check original security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_REQ_NO_VAL);
        tester.assertTextPresent(SUB_SL_REQ_NO_VAL);

        // Convert
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_REQ_NO_VAL);
        tester.assertTextNotPresent("Security Level:");

        tester.clickLink("subtask-to-issue");
        navigation.issue().selectIssueType(ISSUE_TYPE_BUG, "issuetype");
        tester.submit("Next >>");
        tester.assertTextPresent("All fields will be updated automatically.");
        tester.submit("Next >>");
        tester.assertTextNotPresent("Security Level");
        tester.submit("Finish");


        tester.assertTextNotPresent("Security Level");

        // Ensure Parent Issue link is removed
        tester.assertTextNotPresent(PARENT_NO_VAL);
        tester.clickLinkWithText(CHANGE_HISTORY);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{CHANGE_HISTORY, PARENT_NO_VAL});

        navigation.issue().gotoIssue(PARENT_NO_VAL);
        tester.assertTableNotPresent(SUB_SL_REQ_NO_VAL);

        // Check new security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_REQ_NO_VAL);
        tester.assertTextPresent(SUB_SL_REQ_NO_VAL);
    }

    /*
     * Tests that when the issue to convert has no security level and the target type has it as hidden
     * the user is not prompted and it is not on the confirm screen, and checks the issue has not been updated
     */
    @Test
    public void testSubTaskToIssueConversionSecurityLevelNoValToHidden() {
        // Check original security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_REQ_NO_VAL);
        tester.assertTextPresent(SUB_SL_REQ_NO_VAL);

        // Convert
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_REQ_NO_VAL);
        tester.assertTextNotPresent("Security Level:");

        tester.clickLink("subtask-to-issue");
        navigation.issue().selectIssueType(ISSUE_TYPE_NEWFEATURE, "issuetype");
        tester.submit("Next >>");
        tester.assertTextPresent("All fields will be updated automatically.");
        tester.submit("Next >>");
        tester.assertTextNotPresent("Security Level");
        tester.submit("Finish");

        tester.assertTextNotPresent("Security Level");

        // Check new security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_REQ_NO_VAL);
        tester.assertTextPresent(SUB_SL_REQ_NO_VAL);
    }

    /*
     * Tests that when the issue to convert has no security level and the target type has it as required
     * the user is prompted and it is on the confirm screen, and checks the issue has been updated
     */
    @Test
    public void testSubTaskToIssueConversionSecurityLevelNoValToRequired() {
        // Check original security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_REQ_NO_VAL);
        tester.assertTextPresent(SUB_SL_REQ_NO_VAL);

        // Convert
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_REQ_NO_VAL);
        tester.assertTextNotPresent("Security Level:");

        tester.clickLink("subtask-to-issue");
        navigation.issue().selectIssueType(ISSUE_TYPE_IMPROVEMENT, "issuetype");
        tester.submit("Next >>");
        tester.assertTextPresent("Security Level");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "Developers"});
        tester.selectOption("security", "Developers");
        tester.submit("Next >>");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "None", "Developers"});

        tester.submit("Finish");

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "Developers"});


        // Check new security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_REQ_NO_VAL);
        assertions.getViewIssueAssertions().assertIssueNotFound();
    }

    /*
     * Tests that when the issue to convert has security level and the target type has it as optional/required
     * the user is not prompted and it is not on the confirm screen, and checks the issue has not been updated
     */
    @Test
    public void testSubTaskToIssueConversionSecurityLevelValToOptional() {
        // Check original security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_REQ_HAS_VAL);
        assertions.getViewIssueAssertions().assertIssueNotFound();

        // Convert
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_REQ_HAS_VAL);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "Developers"});

        tester.clickLink("subtask-to-issue");
        navigation.issue().selectIssueType(ISSUE_TYPE_IMPROVEMENT, "issuetype");
        tester.submit("Next >>");
        tester.assertTextNotPresent("Security Level");
        tester.assertTextPresent("All fields will be updated automatically.");
        tester.submit("Next >>");
        tester.assertTextNotPresent("Security Level");
        tester.submit("Finish");


        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "Developers"});

        // Check new security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_REQ_HAS_VAL);
        assertions.getViewIssueAssertions().assertIssueNotFound();
    }

    /*
     * Tests that when the issue to convert has security level and the target type has it as hidden
     * the user is not prompted and it is on the confirm screen, and checks the issue has been updated
     */
    @Test
    public void testSubTaskToIssueConversionSecurityLevelValToHidden() {
        // Check original security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_REQ_HAS_VAL);
        assertions.getViewIssueAssertions().assertIssueNotFound();

        // Convert
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_REQ_HAS_VAL);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "Developers"});

        tester.clickLink("subtask-to-issue");
        navigation.issue().selectIssueType(ISSUE_TYPE_NEWFEATURE, "issuetype");
        tester.submit("Next >>");
        tester.assertTextNotPresent("Security Level");
        tester.assertTextPresent("All fields will be updated automatically.");
        tester.submit("Next >>");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "Developers"});
        tester.submit("Finish");


        tester.assertTextNotPresent("Security Level");

        // Check new security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_REQ_HAS_VAL);
        tester.assertTextPresent(SUB_SL_REQ_HAS_VAL);
    }

    /*
     * Tests that when the issue to convert has security level hidden and the target type has it as optional
     * the user is not prompted and it is not on the confirm screen, and checks the issue has not been updated
     */
    @Test
    public void testSubTaskToIssueConversionSecurityLevelHiddenToOptional() {
        // Check original security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_HIDDEN);
        tester.assertTextPresent(SUB_SL_HIDDEN);

        // Convert
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_HIDDEN);
        tester.assertTextNotPresent("Security Level");

        tester.clickLink("subtask-to-issue");
        navigation.issue().selectIssueType(ISSUE_TYPE_BUG, "issuetype");
        tester.submit("Next >>");
        tester.assertTextNotPresent("Security Level");
        tester.assertTextPresent("All fields will be updated automatically.");
        tester.submit("Next >>");
        tester.assertTextNotPresent("Security Level");
        tester.submit("Finish");

        tester.assertTextNotPresent("Security Level");

        // Check new security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_HIDDEN);
        tester.assertTextPresent(SUB_SL_HIDDEN);
    }

    /*
     * Tests that when the issue to convert has security level hidden and the target type has it as hidden
     * the user is not prompted and it is not on the confirm screen, and checks the issue has not been updated
     */
    @Test
    public void testSubTaskToIssueConversionSecurityLevelHiddenToHidden() {
        // Check original security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_HIDDEN);
        tester.assertTextPresent(SUB_SL_HIDDEN);

        // Convert
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_HIDDEN);
        tester.assertTextNotPresent("Security Level");

        tester.clickLink("subtask-to-issue");
        navigation.issue().selectIssueType(ISSUE_TYPE_NEWFEATURE, "issuetype");
        tester.submit("Next >>");
        tester.assertTextNotPresent("Security Level");
        tester.assertTextPresent("All fields will be updated automatically.");
        tester.submit("Next >>");
        tester.assertTextNotPresent("Security Level");
        tester.submit("Finish");

        tester.assertTextNotPresent("Security Level");

        // Check new security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_HIDDEN);
        tester.assertTextPresent(SUB_SL_HIDDEN);
    }

    /*
     * Tests that when the issue to convert has security level hidden and the target type has it as required
     * the user is prompted and it is on the confirm screen, and checks the issue has been updated
     */
    @Test
    public void testSubTaskToIssueConversionSecurityLevelHiddenToRequired() {
        // Check original security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_HIDDEN);
        tester.assertTextPresent(SUB_SL_HIDDEN);

        // Convert
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_HIDDEN);
        tester.assertTextNotPresent("Security Level");

        tester.clickLink("subtask-to-issue");
        navigation.issue().selectIssueType(ISSUE_TYPE_IMPROVEMENT, "issuetype");
        tester.submit("Next >>");
        tester.assertTextPresent("Security Level");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "Developers"});
        tester.selectOption("security", "Developers");
        tester.submit("Next >>");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "None", "Developers"});

        tester.submit("Finish");

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "Developers"});


        // Check new security level
        navigation.logout();
        navigation.login(FRED_USERNAME, FRED_PASSWORD);
        navigation.issue().gotoIssue(SUB_SL_HIDDEN);
        assertions.getViewIssueAssertions().assertIssueNotFound();
    }

}
