package com.atlassian.jira.webtests.ztests.avatar;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.collect.Ordering;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.webtests.ztests.avatar.ImageClient.Image.Type.PNG;
import static com.atlassian.jira.webtests.ztests.avatar.ImageClient.Image.Type.SVG;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

@WebTest({FUNC_TEST})
@RestoreOnce("blankprojects.xml")
public class TestGettingIconsAsPNGs extends BaseJiraFuncTest {
    private final static String ISSUE_TYPE = "/secure/viewavatar?avatarId=10243&avatarType=issuetype";
    private final static String PROJECT_AVATAR = "/secure/projectavatar?pid=10000&avatarId=10001";
    private final static String PRIORITY = FunctTestConstants.PRIORITY_IMAGE_MAJOR;

    private final ImageClient imageClient = new ImageClient();

    @Test
    public void svgIsReturnedWhenNoQueryParamIsAdded() {
        testSvgIsReturned(ISSUE_TYPE);
        testSvgIsReturned(PROJECT_AVATAR);
        testSvgIsReturned(PRIORITY);
    }

    private void testSvgIsReturned(final String relativeUrl) {
        ImageClient.Image image = imageClient.get(prependBaseUrl(relativeUrl));
        assertThat(image.getType(), equalTo(SVG));
        assertThat(image.getBytesList(), hasSize(greaterThan(0)));
    }

    @Test
    public void avatarIsReturnedAsPngIfFormatIsSetToPng() {
        String url = prependBaseUrl(PROJECT_AVATAR) + "&format=PNG";
        ImageClient.Image image = imageClient.get(url);
        assertThat(image.getType(), equalTo(PNG));
        assertThat(image.getBytesList(), hasSize(greaterThan(0)));
    }

    @Test
    public void avatarIsReturnedAsPngIfFormatIsSetToPngInLowerCase() {
        String url = prependBaseUrl(PROJECT_AVATAR) + "&format=png";
        ImageClient.Image image = imageClient.get(url);
        assertThat(image.getType(), equalTo(PNG));
        assertThat(image.getBytesList(), hasSize(greaterThan(0)));
    }

    @Test
    public void avatarIsReturnedAsPngIfFormatIsSetToPngInDifferentCase() {
        String url = prependBaseUrl(PROJECT_AVATAR) + "&format=PnG";
        ImageClient.Image image = imageClient.get(url);
        assertThat(image.getType(), equalTo(PNG));
        assertThat(image.getBytesList(), hasSize(greaterThan(0)));
    }

    @Test
    public void issueTypeIsReturnedAsPngIfFormatIsSetToPng() {
        String url = prependBaseUrl(ISSUE_TYPE) + "&format=PNG";
        ImageClient.Image image = imageClient.get(url);
        assertThat(image.getType(), equalTo(PNG));
        assertThat(image.getBytesList(), hasSize(greaterThan(0)));
    }

    @Test
    public void priorityIsReturnedAsPngIfFormatIsSetToPng() {
        String url = prependBaseUrl(PRIORITY) + "?format=PNG";
        ImageClient.Image image = imageClient.get(url);
        assertThat(image.getType(), equalTo(PNG));
        assertThat(image.getBytesList(), hasSize(greaterThan(0)));
    }

    @Test
    public void pngImagesCanBeReturnedTwice() {
        // we have different paths for first and subsequent conversions (we save the result to a file) so it's worth testing
        testReturningTwice(ISSUE_TYPE + "&format=PNG");
        testReturningTwice(PROJECT_AVATAR + "&format=PNG");
        testReturningTwice(PRIORITY + "?format=PNG");
    }

    @Test
    public void pngImagesCanBeReturnedInDifferentSizes() {
        testDifferentSizedOfPngImages(ISSUE_TYPE + "&format=PNG");
        testDifferentSizedOfPngImages(PROJECT_AVATAR + "&format=PNG");
        testDifferentSizedOfPngImages(PRIORITY + "?format=PNG");
    }


    private void testReturningTwice(final String url) {
        ImageClient.Image first = imageClient.get(prependBaseUrl(url));
        assertThat(first.getType(), equalTo(PNG));
        assertThat(first.getBytesList(), hasSize(greaterThan(0)));
        ImageClient.Image second = imageClient.get(prependBaseUrl(url));
        assertThat(second.getType(), equalTo(PNG));
        assertThat(second.getBytesList(), hasSize(greaterThan(0)));
    }

    private void testDifferentSizedOfPngImages(final String urlToPngImage) {
        int previousLength = 0;
        for (Avatar.Size size : Ordering.natural().onResultOf(Avatar.Size::getPixels).sortedCopy(asList(Avatar.Size.values()))) {
            ImageClient.Image image = imageClient.get(prependBaseUrl(urlToPngImage) + "&size=" + size.getParam());
            assertThat(image.getBytesList(), hasSize(greaterThan(previousLength)));
            previousLength = image.getBytes().length;
        }
    }

    private String prependBaseUrl(String relativeUrl) {
        return environmentData.getBaseUrl().toString() + relativeUrl;
    }
}
