package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestXsrfBulkOperations.xml")
public class TestXsrfBulkOperations extends BaseJiraFuncTest {

    @Inject
    private Form form;

    @Test
    public void testBulkOperations() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck("Bulk Delete Operation", new XsrfCheck.Setup() {
                    public void setup() {
                        tester.gotoPage("/issues/?jql=");
                        tester.gotoPage("/views/bulkedit/BulkEdit1!default.jspa?reset=true&tempMax=10000");
                        tester.checkCheckbox("bulkedit_10021", "on");
                        tester.submit("Next");
                        tester.checkCheckbox("operation", "bulk.delete.operation.name");
                        tester.submit("Next");

                    }
                }, new XsrfCheck.FormSubmission("Confirm"))
        ).run(getTester(), navigation, form);
    }
}