package com.atlassian.jira.webtests.ztests.navigator.jql;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Parser;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.webtests.ztests.tpm.ldap.UserDirectoryTable;
import com.meterware.httpunit.WebLink;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.io.IOException;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static org.junit.Assert.assertEquals;

/**
 * This test class test for User queries being case insensitive.
 * There are situations with multiple directories (particularly when using LDAP) where customers
 * can have the same user in multiple directories but with the user name varying by case. In this case (and always)
 * queries should find issues where the user is stored with either an upper, lower or mixed case name.
 *
 * @since v5.0
 */
@WebTest({Category.FUNC_TEST, Category.JQL, Category.SLOW_IMPORT})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestUserQueriesCaseInsensitive.xml")
public class TestUserQueriesCaseInsensitive extends BaseJiraFuncTest {

    @Inject
    private IssueTableAssertions jql;

    @Inject
    private Parser parse;

    @Inject
    private FuncTestLogger logger;

    @Before
    public void setUp() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    @Test
    public void testUserQueries() throws Exception {
        assertEquals("Administrator from LDAP", parse.header().getFullUserName());
        logger.log("Running queries with (pseudo) LDAP directory first");
        runUserQueries();
        swapDirectories();
        assertEquals("Administrator in the Shadows", parse.header().getFullUserName());
        logger.log("Running queries with internal directory first");
        runUserQueries();
    }

    private void runUserQueries() {
        logger.log("Running assignee queries");
        jql.assertSearchWithResults("assignee = admin", "NO-1", "MKY-1", "JRA-2", "HSP-1", "CONF-1", "ABC-4");
        jql.assertSearchWithResults("assignee = Admin", "NO-1", "MKY-1", "JRA-2", "HSP-1", "CONF-1", "ABC-4");
        jql.assertSearchWithResults("assignee = fred");
        jql.assertSearchWithResults("assignee = Fred");
        logger.log("Running reporter queries");
        jql.assertSearchWithResults("reporter = admin", "NO-1", "JRA-2", "JRA-1", "CONF-1", "ABC-3", "ABC-2");
        jql.assertSearchWithResults("reporter = Admin", "NO-1", "JRA-2", "JRA-1", "CONF-1", "ABC-3", "ABC-2");
        jql.assertSearchWithResults("reporter = currentUser()", "NO-1", "JRA-2", "JRA-1", "CONF-1", "ABC-3", "ABC-2");
        logger.log("Running voter queries");
        jql.assertSearchWithResults("voter = admin", "MKY-1");
        jql.assertSearchWithResults("voter = Admin", "MKY-1");
        jql.assertSearchWithResults("voter = fred", "MKY-1", "JRA-2");
        jql.assertSearchWithResults("voter = Fred", "MKY-1", "JRA-2");
        logger.log("Running watcher queries");
        jql.assertSearchWithResults("watcher = admin", "JRA-2");
        jql.assertSearchWithResults("watcher = Admin", "JRA-2");
        logger.log("Running custom field queries");
        jql.assertSearchWithResults("\"QA done by\" = John", "CONF-1", "ABC-4", "ABC-3", "ABC-2");
        jql.assertSearchWithResults("\"QA done by\" = john", "CONF-1", "ABC-4", "ABC-3", "ABC-2");
        jql.assertSearchWithResults("\"Interested parties\" = admin", "JRA-1", "HSP-1");
        jql.assertSearchWithResults("\"Interested parties\" = Admin", "JRA-1", "HSP-1");
        jql.assertSearchWithResults("\"Interested parties\" = John", "JRA-2", "HSP-1", "CONF-1");
        jql.assertSearchWithResults("\"Interested parties\" = john", "JRA-2", "HSP-1", "CONF-1");

        logger.log("Running change history queries");
        jql.assertSearchWithResults("reporter was admin before \"2009-07-24\"", "NO-1", "JRA-2", "JRA-1", "CONF-1", "ABC-2");
        jql.assertSearchWithResults("reporter was Admin before \"2009-07-24\"", "NO-1", "JRA-2", "JRA-1", "CONF-1", "ABC-2");
        jql.assertSearchWithResults("reporter was not admin after \"2009-07-24\"", "MKY-1", "HSP-1", "ABC-4");
        jql.assertSearchWithResults("reporter was not Admin after \"2009-07-24\"", "MKY-1", "HSP-1", "ABC-4");
    }

    private void swapDirectories() throws IOException, SAXException {
        logger.log("Swap directory order");
        navigation.gotoPage("/plugins/servlet/embedded-crowd/directories/list");
        tester.setWorkingForm("");

        UserDirectoryTable userDirectoryTable = new UserDirectoryTable(this, logger, assertions);
        WebLink link = userDirectoryTable.getTableCell(2, 3).getLinkWith("up");
        navigation.clickLink(link);
    }
}
