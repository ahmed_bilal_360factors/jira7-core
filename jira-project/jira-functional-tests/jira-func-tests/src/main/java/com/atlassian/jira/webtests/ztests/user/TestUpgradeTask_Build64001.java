package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;

/**
 * @since v6.3.3
 */
@WebTest({Category.FUNC_TEST, Category.ISSUE_LINKS, Category.UPGRADE_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestUpgradeTask_Build64001 extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Test
    public void testFixingIncorrectWatchCountForClonedIssues() {
        administration.restoreDataWithBuildNumber("TestUpgradeTask_Build64001.xml", 6330);

        assertEquals(Long.valueOf(1L), getWatches(10000));
        assertEquals(Long.valueOf(1L), getWatches(10001)); // before fix it is 2 and should be fixed to 1
        assertEquals(Long.valueOf(1L), getWatches(10002)); // before fix it is 3 and should be fixed to 1
        assertEquals(Long.valueOf(0L), getWatches(10004)); // before fix it is 1 and should be fixed to 0
    }

    private Long getWatches(final long id) {
        final List<Map<String, Object>> issues = backdoor.entityEngine().findByAnd("Issue", ImmutableMap.of("id", id));
        assertEquals("Issue with id: " + id + " must exists!", 1, issues.size());
        return Long.parseLong(String.valueOf(issues.get(0).get("watches")));
    }

}
