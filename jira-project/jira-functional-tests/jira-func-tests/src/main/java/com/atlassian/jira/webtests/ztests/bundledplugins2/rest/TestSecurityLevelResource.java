package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.testkit.client.IssueSecuritySchemesControl;
import com.atlassian.jira.testkit.client.restclient.IssueSecurityLevels;
import com.atlassian.jira.testkit.client.restclient.ProjectSecurityLevelClient;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.util.PropertyAssertions;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.concurrent.Callable;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestSecurityLevelResource extends BaseJiraRestTest {
    private final String USER_NAME = "security_level_test_user";
    private String USER_KEY;
    private ProjectSecurityLevelClient projectSecurityLevelClient;
    private IssueSecuritySchemesControl control;

    @Before
    public void setUpTest() {
        projectSecurityLevelClient = new ProjectSecurityLevelClient(environmentData);
        control = backdoor.getTestkit().issueSecuritySchemesControl();

        backdoor.usersAndGroups().addUser(USER_NAME);
        USER_KEY = backdoor.usersAndGroups().getUserByName(USER_NAME).getKey();
        backdoor.project().addProject("TEST", "TEST", USER_KEY);
    }

    @Test
    public void testGetSecurityLevelsForNonExistingProject() {
        PropertyAssertions.assertUniformInterfaceException(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                projectSecurityLevelClient.get("NON_EXISTING");
                return null;
            }
        }, Response.Status.NOT_FOUND);
    }

    @Test
    public void testGetSecurityLevelsForExistingProjectWithoutPermissionsForProject() {
        backdoor.project().addProject("HIDDEN", "HIDDEN", "admin");

        PropertyAssertions.assertUniformInterfaceException(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                projectSecurityLevelClient.anonymous().get("HIDDEN");
                return null;
            }
        }, Response.Status.NOT_FOUND);
    }

    @Test
    public void testGetNoSecurityLevelsWhenDoesNotHaveSetIssueSecurityPermission() {
        Long schemeId = control.createScheme("TestIssueSecurityLevelScheme", "Test");
        Long levelId = control.addSecurityLevel(schemeId, "TestLevel", "desc");

        backdoor.project().setIssueSecurityScheme(backdoor.project().getProjectId("TEST"), schemeId);
        control.addUserToSecurityLevel(schemeId, levelId, USER_KEY);

        projectSecurityLevelClient.loginAs(USER_NAME);

        IssueSecurityLevels levels = projectSecurityLevelClient.get("TEST");
        assertThat(levels.getLevels(), hasSize(0));
    }

    @Test
    public void testGetNoSecurityLevelsWhenNotInSecurityLevelGroup() {
        Long schemeId = control.createScheme("TestIssueSecurityLevelScheme", "Test");
        control.addSecurityLevel(schemeId, "TestLevel", "desc");

        backdoor.project().setIssueSecurityScheme(backdoor.project().getProjectId("TEST"), schemeId);
        backdoor.permissionSchemes().addUserPermission(0, ProjectPermissions.SET_ISSUE_SECURITY, USER_NAME);

        projectSecurityLevelClient.loginAs(USER_NAME);

        IssueSecurityLevels levels = projectSecurityLevelClient.get("TEST");
        assertThat(levels.getLevels(), hasSize(0));
    }

    @Test
    public void testGetSecurityLevelWhenInSecurityLevelGroup() {
        Long schemeId = control.createScheme("TestIssueSecurityLevelScheme", "Test");
        Long levelId = control.addSecurityLevel(schemeId, "TestLevel", "desc");

        control.addUserToSecurityLevel(schemeId, levelId, USER_KEY);

        backdoor.project().setIssueSecurityScheme(backdoor.project().getProjectId("TEST"), schemeId);
        backdoor.permissionSchemes().addUserPermission(0, ProjectPermissions.SET_ISSUE_SECURITY, USER_NAME);

        projectSecurityLevelClient.loginAs(USER_NAME);

        IssueSecurityLevels levels = projectSecurityLevelClient.get("TEST");
        assertThat(levels.getLevels(), hasSize(1));
    }

    @Test
    public void testGetSecurityLevelWhenInTwoSecurityLevelGroups() {
        Long schemeId = control.createScheme("TestIssueSecurityLevelScheme", "Test");

        Long levelId = control.addSecurityLevel(schemeId, "TestLevel", "desc");
        control.addUserToSecurityLevel(schemeId, levelId, USER_KEY);

        Long levelId2 = control.addSecurityLevel(schemeId, "TestLevel2", "desc2");
        control.addUserToSecurityLevel(schemeId, levelId2, USER_KEY);

        backdoor.project().setIssueSecurityScheme(backdoor.project().getProjectId("TEST"), schemeId);
        backdoor.permissionSchemes().addUserPermission(0, ProjectPermissions.SET_ISSUE_SECURITY, USER_NAME);

        projectSecurityLevelClient.loginAs(USER_NAME);

        IssueSecurityLevels levels = projectSecurityLevelClient.get("TEST");
        assertThat(levels.getLevels(), hasSize(2));
    }
}
