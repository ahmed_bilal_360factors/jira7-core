package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.admin.plugins.ReferencePlugin;
import com.atlassian.jira.functest.framework.backdoor.JqlAutoCompleteControl;
import com.atlassian.jira.functest.framework.backdoor.PluginIndexConfigurationControl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.internal.jql.FieldAndPredicateAutoCompleteResultGenerator;
import com.atlassian.jira.testkit.client.restclient.EntityPropertyClient;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operator.Operator;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.hamcrest.Description;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsCollectionContaining;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.jira.functest.framework.backdoor.JqlAutoCompleteControl.AutoCompleteResult;
import static com.atlassian.jira.functest.framework.util.ReferencePluginReloadHelper.updatePluginIndexConfiguration;
import static com.atlassian.jira.util.SearchResultMatcher.issues;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.ENTITY_PROPERTIES, Category.REST, Category.REFERENCE_PLUGIN})
public class TestIssuePropertyAliasing extends BaseJiraRestTest {
    private static final Label TEST_LABEL = new Label("propertyAliasing", "description of the label", "2014-10-30", 10l);

    private EntityPropertyClient client;
    private PluginIndexConfigurationControl configurationControl;
    private JqlAutoCompleteControl autoCompleteControl;

    @Before
    public void setUpTest() {
        this.client = new EntityPropertyClient(environmentData, "issue");
        this.configurationControl = new PluginIndexConfigurationControl(environmentData);
        this.autoCompleteControl = new JqlAutoCompleteControl(environmentData);
        backdoor.restoreBlankInstance();
    }

    @Test
    public void testJqlAliasSearchingForStrings() throws IOException {
        setLabelDocumentConfiguration();

        IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "Issue with aliases issue property");

        setLabelProperty(issue, TEST_LABEL);

        assertSearchResultsForAlias("label", Operator.EQUALS, TEST_LABEL.getValue(), issue);
        assertSearchResultsForAlias("label", Operator.IN, "('" + TEST_LABEL.getValue() + "')", issue);
    }

    @Test
    public void testJqlAliasingForRelativeDateQueries() throws Exception {
        setLabelDocumentConfiguration();

        IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "Issue with aliases issue property");

        setLabelProperty(issue, TEST_LABEL);

        assertSearchResultsForAlias("labelDate", Operator.GREATER_THAN_EQUALS, "'2014-10-29'", issue);
        assertSearchResultsForAlias("labelDate", Operator.LESS_THAN, "'2014-11-01'", issue);
        assertSearchResultsForAlias("labelDate", Operator.LESS_THAN, "'2014-11-01'", issue);
    }

    @Test
    public void testJqlAliasingForJqlFunctionDateQueries() throws Exception {
        setLabelDocumentConfiguration();

        IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "Issue with aliases issue property");

        String date = ISODateTimeFormat.dateTimeNoMillis().print(System.currentTimeMillis());
        Label label = new Label("val", "description", date, 1l);

        setLabelProperty(issue, label);

        assertSearchResultsForAlias("labelDate", Operator.GREATER_THAN_EQUALS, "'-2d'", issue);
        assertSearchResultsForAlias("labelDate", Operator.GREATER_THAN, "startOfDay()", issue);
        assertSearchResultsForAlias("labelDate", Operator.LESS_THAN, "endOfDay()", issue);
        assertSearchResultsForAlias("labelDate", Operator.LESS_THAN_EQUALS, "'2d'", issue);
    }

    @Test
    public void testJqlAliasingForTextValues() throws Exception {
        setLabelDocumentConfiguration();

        IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "Issue with aliases issue property");

        setLabelProperty(issue, TEST_LABEL);

        assertSearchResultsForAlias("labelDescription", Operator.LIKE, "description", issue);
        assertSearchResultsForAlias("labelDescription", Operator.LIKE, "label", issue);
    }

    @Test
    public void testJqlAliasSearchingForMultipleIssues() throws Exception {
        setLabelDocumentConfiguration();

        IssueCreateResponse firstIssue = backdoor.issues().createIssue("HSP", "First issue");
        IssueCreateResponse secondIssue = backdoor.issues().createIssue("HSP", "Second issue");
        IssueCreateResponse thirdIssue = backdoor.issues().createIssue("HSP", "Third issue");

        String date = ISODateTimeFormat.dateTimeNoMillis().print(System.currentTimeMillis());

        Label firstLabel = new Label("first value", "first description", date, 1l);
        Label secondLabel = new Label("second value", "second description", date, 1l);
        Label thirdLabel = new Label("third value", "third description", date, 1l);

        setLabelProperty(firstIssue, firstLabel);
        setLabelProperty(secondIssue, secondLabel);
        setLabelProperty(thirdIssue, thirdLabel);

        assertSearchResultsForAlias("label", Operator.EQUALS, "'" + firstLabel.value + "'", firstIssue);
        assertSearchResultsForAlias("labelDescription", Operator.LIKE, "description", firstIssue, secondIssue, thirdIssue);

        assertSearchResult(new SearchRequest().jql("label = 'second value' AND labelDescription ~ 'description'"), secondIssue);
        assertSearchResult(new SearchRequest().jql("labelDate > '-1d' AND labelDescription ~ 'description'"), firstIssue, secondIssue, thirdIssue);
        assertSearchResult(new SearchRequest().jql("labelId = 1"), firstIssue, secondIssue, thirdIssue);
    }

    @Test
    public void testConflictingJqlAliases() throws Exception {
        updatePluginIndexConfiguration(backdoor, "first plugin", "conflict_indexconf1.xml", configurationControl);
        updatePluginIndexConfiguration(backdoor, "second plugin", "conflict_indexconf2.xml", configurationControl);

        IssueCreateResponse firstIssue = backdoor.issues().createIssue("HSP", "Some issue");
        IssueCreateResponse secondIssue = backdoor.issues().createIssue("HSP", "Some issue");
        String date = ISODateTimeFormat.dateTimeNoMillis().print(System.currentTimeMillis());

        client.put(firstIssue.key, "label", new Label("label", "description", date, 1l).toJson());
        client.put(secondIssue.key, "conflicting_alias", new Label("conflict", "description", date, 1l).toJson());

        assertSearchResultsForAlias("conflict", Operator.EQUALS, "'label'", firstIssue);
        assertSearchResultsForAlias("conflict", Operator.EQUALS, "'conflict'", secondIssue);

        assertSearchResultsForAlias("issue.property[conflicting_alias].value", Operator.EQUALS, "'conflict'", secondIssue);
        assertSearchResultsForAlias("issue.property[label].value", Operator.EQUALS, "'label'", firstIssue);
    }

    @Test
    public void testAliasAutoCompletionWithoutConflict() throws Exception {
        setLabelDocumentConfiguration();

        String date = ISODateTimeFormat.dateTimeNoMillis().print(System.currentTimeMillis());

        String[] values = new String[]{"value1", "value2", "value3", "value4", "value5", "value6"};

        for (String value : values) {
            IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "new issue with label value " + value);
            setLabelProperty(issue, new Label(value, "some description", date, 1l));
        }

        assertAutoCompletionResult("label", "val", values);
        assertAutoCompletionResult("label", "value", values);
        assertAutoCompletionResult("label", "value1", "value1");
        assertAutoCompletionResult("label", "value2", "value2");
    }

    @Test
    public void testAliasWithMoreThen15Matches() throws Exception {
        setLabelDocumentConfiguration();

        String date = ISODateTimeFormat.dateTimeNoMillis().print(System.currentTimeMillis());

        for (int i = 0; i < 20; i++) {
            IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "new issue " + i);
            setLabelProperty(issue, new Label("value" + i, "some description", date, 1l));
        }

        JqlAutoCompleteControl.Results autoCompleteResults = autoCompleteControl.getAutoCompleteResults("label", "val");
        assertThat(autoCompleteResults.getResults(), hasSize(lessThanOrEqualTo(FieldAndPredicateAutoCompleteResultGenerator.MAX_RESULTS)));
    }

    @Test
    public void testAutoCompletionWorksForConflictingProperties() throws Exception {
        updatePluginIndexConfiguration(backdoor, "first plugin", "conflict_indexconf1.xml", configurationControl);
        updatePluginIndexConfiguration(backdoor, "second plugin", "conflict_indexconf2.xml", configurationControl);

        IssueCreateResponse firstIssue = backdoor.issues().createIssue("HSP", "Some issue");
        IssueCreateResponse secondIssue = backdoor.issues().createIssue("HSP", "Some issue");
        String date = ISODateTimeFormat.dateTimeNoMillis().print(System.currentTimeMillis());

        client.put(firstIssue.key, "label", new Label("some label value", "description", date, 1l).toJson());
        client.put(secondIssue.key, "conflicting_alias", new Label("conflict", "description", date, 1l).toJson());

        assertAutoCompletionResult("issue.property[label].value", "som", "\"some label value\"");
        assertAutoCompletionResult("issue.property[conflicting_alias].value", "conf", "conflict");

        assertThat(autoCompleteControl.getAutoCompleteResults("label", "lab").getResults(), Matchers.<AutoCompleteResult>emptyIterable());
    }

    @Test
    public void testAutoCompletionReturnsOnlyDistrinctResults() {
        setLabelDocumentConfiguration();

        String date = ISODateTimeFormat.dateTimeNoMillis().print(System.currentTimeMillis());

        // sets the same property 20 times
        for (int i = 0; i < 20; i++) {
            IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "new issue " + i);
            setLabelProperty(issue, new Label("value", "some description", date, 1l));
        }

        // assert only one result
        assertThat(autoCompleteControl.getAutoCompleteResults("label", "val").getResults(), hasSize(equalTo(1)));
    }

    @Test
    public void orderByAliasedIdSortsDescendingByDefault() {
        setLabelDocumentConfiguration();
        Label[] labels = new Label[]{withId(1l), withId(2l), withId(3l), withId(4l), withId(5l), withId(6l)};

        List<IssueCreateResponse> expected = Lists.reverse(createIssuesWithLabel(labels));

        assertSearchResultsWithOrderBy("labelId", expected);
        assertSearchResultsWithOrderBy("labelId DESC", expected);
        assertSearchResultsWithOrderBy("labelId ASC", Lists.reverse(expected));
    }

    @Test
    public void orderBySystemFieldIdSortsAscendingByDefault() {
        setLabelDocumentConfiguration();
        Label[] labels = new Label[]{withId(1l), withId(1l), withId(1l), withId(1l), withId(1l), withId(1l)};

        List<IssueCreateResponse> expected = createIssuesWithLabel(labels);
        assertSearchResultsWithOrderBy("id", expected);
        assertSearchResultsWithOrderBy("id ASC", expected);
        assertSearchResultsWithOrderBy("id DESC", Lists.reverse(expected));
    }

    @Test
    public void orderByStringAliasSortsDescendingByDefault() throws Exception {
        setLabelDocumentConfiguration();
        Label[] labels = new Label[]{withValue("a"), withValue("b"), withValue("c"), withValue("d"), withValue("e"), withValue("f")};

        List<IssueCreateResponse> expected = Lists.reverse(createIssuesWithLabel(labels));
        assertSearchResultsWithOrderBy("label", expected);
        assertSearchResultsWithOrderBy("label DESC", expected);
        assertSearchResultsWithOrderBy("label ASC", Lists.reverse(expected));
    }

    @Test
    public void orderByStringFQNSortsDescendingByDefault() throws Exception {
        setLabelDocumentConfiguration();
        Label[] labels = new Label[]{withValue("a"), withValue("b"), withValue("c"), withValue("d"), withValue("e"), withValue("f")};

        List<IssueCreateResponse> expected = Lists.reverse(createIssuesWithLabel(labels));
        assertSearchResultsWithOrderBy("issue.property[label].value", expected);
        assertSearchResultsWithOrderBy("issue.property[label].value DESC", expected);
        assertSearchResultsWithOrderBy("issue.property[label].value ASC", Lists.reverse(expected));
    }

    @Test
    public void orderByIssueWithoutLabelWhenOrderingByLabelIsLast() {
        setLabelDocumentConfiguration();
        Label[] labels = new Label[]{withId(1l), withId(2l), withId(3l), withId(4l), withId(5l), withId(6l)};

        List<IssueCreateResponse> expected = Lists.reverse(createIssuesWithLabel(labels));

        IssueCreateResponse issueWithoutLabel = backdoor.issues().createIssue("HSP", "new issue with no label value.");
        expected.add(issueWithoutLabel);

        assertSearchResultsWithOrderBy("labelId", expected);
        assertSearchResultsWithOrderBy("labelId DESC", expected);
        assertSearchResultsWithOrderBy("labelId ASC", Lists.reverse(expected));
    }

    @Test
    public void orderByAliasDateDescendingByDefault() {
        testOrderByDateIsDescendingByDefault("labelDate");
    }

    @Test
    public void orderByFQNDateDescendingByDefault() {
        testOrderByDateIsDescendingByDefault("issue.property[label].date");
    }

    @Test
    public void orderByCreatedWorks() throws InterruptedException {
        IssueCreateResponse first = backdoor.issues().createIssue("HSP", "first");
        Thread.sleep(2000);
        IssueCreateResponse second = backdoor.issues().createIssue("HSP", "second");
        Thread.sleep(2000);
        IssueCreateResponse third = backdoor.issues().createIssue("HSP", "third");

        assertSearchResultsWithOrderBy("created", asList(third, second, first));
        assertSearchResultsWithOrderBy("created DESC", asList(third, second, first));
        assertSearchResultsWithOrderBy("created ASC", asList(first, second, third));
    }

    @Test
    public void testConflictingAliasWithSystemFieldSortsBySystemField() throws Exception {
        updatePluginIndexConfiguration(backdoor, "first plugin", "conflict_with_system_field.xml", configurationControl);

        IssueCreateResponse firstIssue = backdoor.issues().createIssue("HSP", "Some issue");
        IssueCreateResponse secondIssue = backdoor.issues().createIssue("HSP", "Some issue2");

        addLabelToIssue(firstIssue, withValue("2"));
        addLabelToIssue(secondIssue, withValue("1"));

        assertSearchResultsWithOrderBy("summary", Arrays.asList(firstIssue, secondIssue));

        addLabelToIssue(firstIssue, withValue("1"));
        addLabelToIssue(secondIssue, withValue("2"));

        assertSearchResultsWithOrderBy("summary", Arrays.asList(firstIssue, secondIssue));
    }

    @Test
    public void testConflictingAliasWithSystemFieldSortsByAliasSecond() throws Exception {
        // all properties are sorted descending by default
        // label.value is aliased as summary
        updatePluginIndexConfiguration(backdoor, "first plugin", "conflict_with_system_field.xml", configurationControl);

        IssueCreateResponse firstIssue = backdoor.issues().createIssue("HSP", "Some issue");
        IssueCreateResponse secondIssue = backdoor.issues().createIssue("HSP", "Some issue");

        addLabelToIssue(firstIssue, withValue("a"));
        addLabelToIssue(secondIssue, withValue("b"));

        assertSearchResultsWithOrderBy("summary", Arrays.asList(secondIssue, firstIssue));

        addLabelToIssue(firstIssue, withValue("b"));
        addLabelToIssue(secondIssue, withValue("a"));

        assertSearchResultsWithOrderBy("summary", Arrays.asList(firstIssue, secondIssue));
    }

    @Test
    public void testMissingPropertyValueIsHandledCorrectlyByOperators() throws IOException {
        setLabelDocumentConfiguration();

        IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "Issue with aliases issue property");
        IssueCreateResponse issueWithoutProperty = backdoor.issues().createIssue("HSP", "Issue without property");

        setLabelProperty(issue, TEST_LABEL);

        assertSearchResultsForAlias("label", Operator.EQUALS, TEST_LABEL.getValue(), issue);
        assertSearchResultsForAlias("label", Operator.IN, "('" + TEST_LABEL.getValue() + "')", issue);

        assertSearchResultsForAlias("label", Operator.IS, EmptyOperand.EMPTY.getName(), issueWithoutProperty);
        assertSearchResultsForAlias("label", Operator.IS_NOT, EmptyOperand.EMPTY.getName(), issue);

        // check it works without aliases as well
        assertSearchResultsForAlias("issue.property[\"label\"].value", Operator.IS_NOT, EmptyOperand.EMPTY.getName(), issue);
    }

    @Test
    public void testNumericOperatorsDontReturnIssuesWithMissingValues() throws IOException {
        setLabelDocumentConfiguration();

        IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "Issue with aliases issue property");
        IssueCreateResponse issueWithoutProperty = backdoor.issues().createIssue("HSP", "Issue without property");

        setLabelProperty(issue, TEST_LABEL);

        assertSearchResultsForAlias("labelId", Operator.LESS_THAN, String.valueOf(TEST_LABEL.getId() + 1L), issue);
        assertSearchResultsForAlias("labelId", Operator.GREATER_THAN, String.valueOf(TEST_LABEL.getId() - 1L), issue);
        assertSearchResultsForAlias("labelId", Operator.LESS_THAN_EQUALS, String.valueOf(TEST_LABEL.getId()), issue);
        assertSearchResultsForAlias("labelId", Operator.GREATER_THAN_EQUALS, String.valueOf(TEST_LABEL.getId()), issue);
    }

    @Test
    public void testEmptyOrNonEmptyQueryReturnsAll() throws IOException {
        setLabelDocumentConfiguration();

        IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "Issue with aliases issue property");
        IssueCreateResponse issueWithoutProperty = backdoor.issues().createIssue("HSP", "Issue without property");

        setLabelProperty(issue, TEST_LABEL);

        final SearchRequest searchRequest = new SearchRequest().jql(String.format("%s is empty OR %s is not empty", "label", "label"));
        assertSearchResult(searchRequest, issue, issueWithoutProperty);
    }

    @Test
    public void testJsonValueIsIgnored() throws IOException, JSONException {
        setLabelDocumentConfiguration();

        IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "Issue with aliases issue property");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("value", ImmutableMap.of("jsonthing", "jsonthingvalue"));
        client.put(issue.key, "label", jsonObject);

        assertSearchResultsForAlias("label", Operator.IS, EmptyOperand.EMPTY.getName(), issue);
    }

    @Test
    public void testNullValueIsIgnored() throws IOException, JSONException {
        setLabelDocumentConfiguration();

        IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "Issue with aliases issue property");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("value", (Object) null);
        client.put(issue.key, "label", jsonObject);

        assertSearchResultsForAlias("label", Operator.IS, EmptyOperand.EMPTY.getName(), issue);
    }

    private void addLabelToIssue(final IssueCreateResponse issue, final Label label) {
        client.put(issue.key, "label", label.toJson());
    }

    private void testOrderByDateIsDescendingByDefault(final String property) {
        setLabelDocumentConfiguration();

        Label[] labels = new Label[]{withDate("2014-10-30"), withDate("2014-10-31"), withDate("2014-11-1")};

        List<IssueCreateResponse> responses = createIssuesWithLabel(labels);

        List<IssueCreateResponse> expected = Lists.reverse(responses);
        assertSearchResultsWithOrderBy(property, expected);
        assertSearchResultsWithOrderBy(property + " DESC", expected);
        assertSearchResultsWithOrderBy(property + " ASC", Lists.reverse(expected));
    }

    private Label withId(Long id) {
        return Label.from(TEST_LABEL).id(id).build();
    }

    private Label withValue(String value) {
        return Label.from(TEST_LABEL).value(value).build();
    }

    private Label withDate(String date) {
        return Label.from(TEST_LABEL).date(date).build();
    }

    private List<IssueCreateResponse> createIssuesWithLabel(final Label[] values) {
        List<IssueCreateResponse> responses = new ArrayList<>();
        for (final Label value : values) {
            IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "new issue with label value " + value.value);
            setLabelProperty(issue, value);
            responses.add(issue);
        }
        return responses;
    }

    private void assertAutoCompletionResult(final String fieldName, final String fieldValue, final String... values) {
        final JqlAutoCompleteControl.Results results = autoCompleteControl.getAutoCompleteResults(fieldName, fieldValue);
        assertThat(results.getResults(), new TypeSafeMatcher<List<AutoCompleteResult>>() {
            @Override
            protected boolean matchesSafely(final List<AutoCompleteResult> autoCompleteResults) {
                return IsCollectionContaining.hasItems(values).matches(Lists.transform(autoCompleteResults, new Function<AutoCompleteResult, String>() {
                    @Override
                    public String apply(final AutoCompleteResult result) {
                        return result.getValue();
                    }
                }));
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("Auto complete result does not contain").appendValue(values);
            }
        });
    }

    private void assertSearchResultsForAlias(final String alias, final Operator operator, final String value,
                                             final IssueCreateResponse... issues) {
        final SearchRequest searchRequest = new SearchRequest().jql(String.format("%s %s %s", alias, operator.getDisplayString(), value));
        assertSearchResult(searchRequest, issues);
    }

    private void assertSearchResultsWithOrderBy(final String orderByClause,
                                                final List<IssueCreateResponse> issues) {
        final SearchRequest searchRequest = new SearchRequest().jql(String.format("%s order by %s", "project = HSP", orderByClause));

        SearchResult search = backdoor.search().getSearch(searchRequest);

        List<String> resultIssueIds = search.issues.stream().map((issue) -> issue.id).collect(toList());
        List<String> expected = issues.stream().map((issue) -> issue.id).collect(toList());

        assertThat(resultIssueIds, Matchers.contains(expected.toArray()));
    }

    private void assertSearchResult(final SearchRequest searchRequest, final IssueCreateResponse... issues) {
        SearchResult searchResult = backdoor.search().getSearch(searchRequest);
        assertThat(searchResult, issues(Lists.transform(Lists.newArrayList(issues), new Function<IssueCreateResponse, String>() {
            @Override
            public String apply(final IssueCreateResponse issueCreateResponse) {
                return issueCreateResponse.key();
            }
        })));
    }

    private void setLabelDocumentConfiguration() {
        updatePluginIndexConfiguration(backdoor, ReferencePlugin.KEY, "pluginindexconfiguration1.xml", configurationControl);
    }

    private void setLabelProperty(final IssueCreateResponse issue, final Label label) {
        client.put(issue.key, "label", label.toJson());
    }

    private static class Label {
        private final String value;
        private final String description;
        private final String date;
        private final Long id;

        private Label(final String value, final String description, final String date, final Long id) {
            this.value = value;
            this.description = description;
            this.date = date;
            this.id = id;
        }

        public JSONObject toJson() {
            return new JSONObject(ImmutableMap.<String, Object>of("value", value,
                    "description", description,
                    "date", date,
                    "id", id));
        }

        public String getValue() {
            return value;
        }

        public String getDescription() {
            return description;
        }

        public String getDate() {
            return date;
        }

        public Long getId() {
            return id;
        }

        public static Builder from(Label label) {
            return new Builder(label);
        }

        private static class Builder {
            private String value;
            private String description;
            private String date;
            private Long id;

            public Builder(Label label) {
                this.value = label.value;
                this.description = label.description;
                this.date = label.date;
                this.id = label.id;
            }

            public Builder value(String value) {
                this.value = value;
                return this;
            }

            public Builder description(String description) {
                this.description = description;
                return this;
            }

            public Builder date(String date) {
                this.date = date;
                return this;
            }

            public Builder id(Long id) {
                this.id = id;
                return this;
            }

            public Label build() {
                return new Label(value, description, date, id);
            }
        }
    }
}
