package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.common.collect.ImmutableMap;
import com.sun.jersey.api.client.GenericType;

import com.atlassian.jira.testkit.client.BackdoorControl;

import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class AnalyticsEventsControl extends BackdoorControl<AnalyticsEventsControl> {

    public AnalyticsEventsControl(JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    @Override
    protected String getRestModulePath() {
        return "analytics";
    }

    public AnalyticsEventsControl clear() {
        createResource().path("report").delete();
        return this;
    }

    public AnalyticsEventsControl enable() {
        return setEnabled(true);
    }

    public AnalyticsEventsControl disable() {
        return setEnabled(false);
    }

    public AnalyticsEventsControl setEnabled(boolean enabled) {
        if (enabled) {
            createResource().path("config/acknowledge").type(MediaType.APPLICATION_JSON_TYPE).put(ImmutableMap.of());
            // unacknowledge end point is currently disabled:
            // https://bitbucket.org/atlassian/atlassian-analytics/src/0bc9ccb58d212c4de00ef9ad4ef514b6cd0b62f4/analytics-client/src/main/java/com/atlassian/analytics/client/configuration/AnalyticsConfigResource.java?at=master&fileviewer=file-view-default#AnalyticsConfigResource.java-78
        }
        createResource().path("config/enable").type(MediaType.APPLICATION_JSON_TYPE).put(ImmutableMap.of("analyticsEnabled", enabled));
        createResource().path("report").type(MediaType.APPLICATION_JSON_TYPE).put(ImmutableMap.of("capturing", Boolean.toString(enabled)));
        return this;
    }

    public List<Map> getEvents() {
        return (List<Map>) createResource().path("report").queryParam("mode", "btf_processed").get(new GenericType<Map>() { }).get("events");
    }

    public List<Map> matchEvents(String name) {
        // removed:false = it is in whitelist
        return this.getEvents()
            .stream().filter(event -> event.get("removed").equals(false) && event.get("name").equals(name))
            .collect(CollectorsUtil.toImmutableList());
    }

    public boolean isEnabled() {
        String response = this.resourceRoot(this.rootPath).path("plugins/servlet/analytics/configuration").get(String.class);
        String configLine = Arrays.stream(response.split("\n")).filter(line -> line.contains("id=\"enable-analytics\"")).findAny().orElse("");
        return configLine.contains(" checked");
    }
}
