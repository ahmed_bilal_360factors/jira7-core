package com.atlassian.jira.util;

import com.atlassian.jira.functest.framework.backdoor.PluginIndexConfigurationControl;
import com.google.common.io.CharStreams;

import java.io.IOException;
import java.io.InputStreamReader;

public final class IndexDocumentConfigurationControlUtil {
    private IndexDocumentConfigurationControlUtil() {
    }

    public static void loadPropertyFromFile(final String pluginKey,
                                            final String filename,
                                            final PluginIndexConfigurationControl pluginIndexConfigurationControl) {
        pluginIndexConfigurationControl.putDocumentConfiguration(pluginKey, "conf1", getPluginIndexConfiguration(filename));
    }

    private static String getPluginIndexConfiguration(final String filename) {
        try {
            final InputStreamReader r = new InputStreamReader(IndexDocumentConfigurationControlUtil.class.getResourceAsStream("/resources/indexconfiguration/" + filename));
            try {
                return CharStreams.toString(r);
            } finally {
                r.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
