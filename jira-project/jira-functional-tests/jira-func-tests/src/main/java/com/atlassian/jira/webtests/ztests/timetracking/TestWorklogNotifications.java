package com.atlassian.jira.webtests.ztests.timetracking;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraEmailTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.rule.MailTest;
import org.junit.Test;

import javax.mail.internet.MimeMessage;
import java.util.Collection;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.matcher.MimeMessageMatchers.containsString;
import static com.atlassian.jira.functest.matcher.MimeMessageMatchers.withRecipientEqualTo;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.TIME_TRACKING})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestWorklogNotificationsIncludingUserThatListensToIssueUpdated.xml")
@MailTest
public class TestWorklogNotifications extends BaseJiraEmailTest {
    public static final String EMAIL_ADDRESS = "admin@example.com";
    public static final String EMAIL_ADDRESS_USER_LISTENING_TO_ISSUE_UPDATED = "fred@example.com";

    public static final String TEXT_ON_WORKLOG_ADDED_MAIL = "added a worklog";
    public static final String TEXT_ON_WORKLOG_UPDATED_MAIL = "updated a worklog";
    public static final String TEXT_ON_WORKLOG_DELETED_MAIL = "deleted a worklog";

    @Test
    public void testAddingNewWorkLogSendsEmail() throws Exception {
        logSomeWork("TEST-1");

        assertOneEmailWasSent(EMAIL_ADDRESS, TEXT_ON_WORKLOG_ADDED_MAIL);
    }

    @Test
    public void testUpdatingWorkLogSendsEmail() throws Exception {
        updateWork("TEST-1");

        assertOneEmailWasSent("admin@example.com", TEXT_ON_WORKLOG_UPDATED_MAIL);
    }

    @Test
    public void testDeletingWorklogSendsEmail() throws Exception {
        deleteWork("TEST-1");

        assertOneEmailWasSent("admin@example.com", TEXT_ON_WORKLOG_DELETED_MAIL);
    }

    @Test
    public void testUsersWhoOnlyListenToIssueUpdatedEventsDoNotReceiveAnyEmailsWhenLoggingWork() throws Exception {
        logSomeWork("TEST-1");

        assertNoEmailsWereSentTo(EMAIL_ADDRESS_USER_LISTENING_TO_ISSUE_UPDATED);
    }

    @Test
    public void testUsersWhoOnlyListenToIssueUpdatedEventsDoNotReceiveAnyEmailsWhenUpdatingWork() throws Exception {
        updateWork("TEST-1");

        assertNoEmailsWereSentTo(EMAIL_ADDRESS_USER_LISTENING_TO_ISSUE_UPDATED);
    }

    @Test
    public void testUsersWhoOnlyListenToIssueUpdatedEventsDoNotReceiveAnyEmailsWhenDeletingWork() throws Exception {
        deleteWork("TEST-1");

        assertNoEmailsWereSentTo(EMAIL_ADDRESS_USER_LISTENING_TO_ISSUE_UPDATED);
    }

    private void logSomeWork(String issueKey) {
        navigation.issue().logWork(issueKey, "1h");
    }

    private void updateWork(String issueKey) {
        tester.beginAt("/browse/" + issueKey + "?page=com.atlassian.jira.plugin.system.issuetabpanels:worklog-tabpanel");
        tester.clickLink("edit_worklog_10000");
        tester.submit("Log");
    }

    private void deleteWork(String issueKey) {
        tester.beginAt("/browse/" + issueKey + "?page=com.atlassian.jira.plugin.system.issuetabpanels:worklog-tabpanel");
        tester.clickLink("delete_worklog_10000");
        tester.submit("Delete");
    }

    private void assertOneEmailWasSent(String emailAddress, String expectedText) throws Exception {
        final Collection<MimeMessage> emails = mailHelper.flushMailQueueAndWait(1);
        final MimeMessage email = emails.iterator().next();
        assertThat(email, allOf(withRecipientEqualTo(emailAddress), containsString(expectedText)));
    }

    private void assertNoEmailsWereSentTo(String emailAddress) throws Exception {
        final Collection<MimeMessage> emails = mailHelper.flushMailQueueAndWait(1);
        assertThat(emails, not(hasItem(withRecipientEqualTo(emailAddress))));
    }
}
