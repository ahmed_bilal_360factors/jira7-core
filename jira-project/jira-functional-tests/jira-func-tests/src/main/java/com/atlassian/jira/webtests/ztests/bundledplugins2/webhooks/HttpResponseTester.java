package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import com.atlassian.jira.functest.framework.backdoor.webhooks.WebHookRegistrationClient;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

import com.sun.net.httpserver.HttpServer;

/**
 * Helper class used for testing Webhook response over HTTP
 */
public class HttpResponseTester implements AutoCloseable {

    private final JIRAEnvironmentData environmentData;
    private final BlockingQueue<WebHookResponseData> responseQueue;

    private HttpServer server;
    private String path;
    private int port;

    public HttpResponseTester(final JIRAEnvironmentData environmentData) {
        this(environmentData, 1);
    }

    public HttpResponseTester(final JIRAEnvironmentData environmentData, int queueCapacity) {
        this.environmentData = environmentData;
        this.responseQueue = new ArrayBlockingQueue<>(queueCapacity);
    }

    public static HttpResponseTester createTester(final JIRAEnvironmentData environmentData) {
        final HttpResponseTester tester = new HttpResponseTester(environmentData);
        try {
            tester.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tester;
    }

    public void start() throws IOException {
        server = HttpServer.create(new InetSocketAddress(0), 0);
        SimpleHttpHandler handler = new SimpleHttpHandler(responseQueue);
        path = "/hook" + Math.random();
        port = server.getAddress().getPort();
        server.createContext(path, handler);
        server.setExecutor(null);
        server.start();
    }


    public void start(final String path, final int port) throws IOException {
        server = HttpServer.create(new InetSocketAddress(port), 0);
        SimpleHttpHandler handler = new SimpleHttpHandler(responseQueue);
        server.createContext(path, handler);
        server.setExecutor(null);
        server.start();
    }

    /**
     * Registers webhook.
     *
     * @param registration webhook registration WITHOUT url
     * @return registration response
     */
    public WebHookRegistrationClient.RegistrationResponse registerWebhook(WebHookRegistrationClient.Registration registration) {
        WebHookRegistrationClient client = new WebHookRegistrationClient(environmentData);
        final int serverPort = server.getAddress().getPort();
        registration.url = "http://localhost:" + serverPort + path + (registration.path != null ? registration.path : "");
        return client.register(registration);
    }

    public WebHookResponseData getResponseData() throws InterruptedException {
        return responseQueue.poll(5, TimeUnit.SECONDS);
    }

    public void stop() {
        server.stop(0);
    }

    public String getPath() {
        return "http://localhost:" + port + path;
    }

    @Override
    public void close() throws Exception {
        stop();
    }
}
