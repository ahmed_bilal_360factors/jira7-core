package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.issue.move.TestDeleteHiddenFieldOnMove;
import com.atlassian.jira.webtests.ztests.issue.move.TestMoveIssue;
import com.atlassian.jira.webtests.ztests.issue.move.TestMoveIssueAndRemoveFields;
import com.atlassian.jira.webtests.ztests.issue.move.TestMoveIssueAttachment;
import com.atlassian.jira.webtests.ztests.issue.move.TestMoveIssueForEnterprise;
import com.atlassian.jira.webtests.ztests.issue.move.TestPromptUserForSecurityLevelOnBulkMove;
import com.atlassian.jira.webtests.ztests.issue.move.TestPromptUserForSecurityLevelOnMove;
import com.atlassian.jira.webtests.ztests.issue.move.TestRedirectToMovedIssues;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Tests for moving issues. For bulk operations see {@link FuncTestSuiteBulkOperations}.
 *
 * @since v4.0
 */
public class FuncTestSuiteMoveIssue {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestDeleteHiddenFieldOnMove.class)
                .add(TestMoveIssue.class)
                .add(TestMoveIssueAndRemoveFields.class)
                .add(TestMoveIssueAttachment.class)
                .add(TestMoveIssueForEnterprise.class)
                .add(TestPromptUserForSecurityLevelOnBulkMove.class)
                .add(TestPromptUserForSecurityLevelOnMove.class)
                .add(TestRedirectToMovedIssues.class)
                .build();
    }
}