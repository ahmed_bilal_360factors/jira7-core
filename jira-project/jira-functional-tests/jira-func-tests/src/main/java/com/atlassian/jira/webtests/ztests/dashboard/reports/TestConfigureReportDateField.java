package com.atlassian.jira.webtests.ztests.dashboard.reports;

import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;
import org.w3c.dom.Element;
import org.w3c.dom.html.HTMLInputElement;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.REPORTS})
@LoginAs(user = ADMIN_USERNAME)
public class TestConfigureReportDateField extends AbstractConfigureReportFieldTestCase {

    @Test
    public void fieldExists() {
        tester.assertFormElementPresent("aDate");
    }

    @Test
    public void defaultValueIsEmpty() {
        HTMLInputElement element = (HTMLInputElement) tester.getDialog().getElement("date_aDate");

        assertThat(element.getValue(), is(""));
    }

    @Test
    public void fieldLabelRendered() {
        textAssertions.assertTextPresent("A date");
    }

    @Test
    public void fieldDescriptionRendered() {
        textAssertions.assertTextPresent("This is a date field");
    }

    @Test
    public void validationErrorsRendered() {
        tester.gotoPage("/secure/ConfigureReport.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.dev.func-test-plugin:fieldtest-report&aString=fail");

        textAssertions.assertTextPresent(new XPathLocator(fieldContainer(), "*[@class='errMsg']"), "This date field has an error");
    }

    @Test
    public void valueIsSubmittedToReportBean() {
        tester.setFormElement("aDate", "1977-01-09");
        tester.submit();
        textAssertions.assertTextPresent(new XPathLocator(tester, "//tr[th = 'aDate']/td"), "1977-01-09");
    }

    private Element fieldContainer() {
        return (Element)new XPathLocator(tester, "//fieldset[@class = 'group'][descendant::input[@name = 'aDate']]").getNode();
    }
}
