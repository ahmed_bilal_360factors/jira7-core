package com.atlassian.jira.webtests.ztests.subtask;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.collect.Lists;
import com.meterware.httpunit.TableCell;
import com.meterware.httpunit.WebTable;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.w3c.dom.Node;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SUB_TASK_DEFAULT_TYPE;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.SUB_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestSubTaskProgressBar extends BaseJiraFuncTest {
    public static final int SUBTASK_COUNT = 5;

    /**
     * Verify that subtasks progress bar advances with each closed subtask.
     */
    @Inject
    private Administration administration;

    @Test
    public void testSubTaskProgressIsNotStuck() {
        administration.restoreBlankInstance();
        administration.timeTracking().disable();
        administration.subtasks().enable();

        final String parentIssueKey = navigation.issue().createIssue("homosapien", "Bug", "Parent Summary");

        final List<String> childIssueKeys = Lists.newArrayListWithCapacity(SUBTASK_COUNT);
        for (int i = 0; i < SUBTASK_COUNT; i++) {
            childIssueKeys.add(navigation.issue().
                    createSubTask(parentIssueKey, SUB_TASK_DEFAULT_TYPE, "Child summary " + i, "Sub Task Desc"));
        }
        for (int i = 0; i < SUBTASK_COUNT; i++) {
            navigation.issue().resolveIssue(childIssueKeys.get(i), "Fixed", "Resolving the Issue as Fixed. This should increase the progress in parent issue.");
            navigation.issue().gotoIssue(parentIssueKey);
            assertSubtaskProgressTableStyleCorrect(i);
        }
    }

    private void assertSubtaskProgressTableStyleCorrect(final int i) {
        final WebTable subtasksResolutionPercentage = tester.getDialog().getWebTableBySummaryOrId("subtasks_resolution_percentage");
        final TableCell tableCell = subtasksResolutionPercentage.getTableCell(0, 0);
        final Node attribute = tableCell.getDOM().getAttributes().getNamedItem("style");
        assertThat(StringUtils.deleteWhitespace(attribute.getNodeValue()), Matchers.is(getStyleForSubtask(i)));
    }

    private String getStyleForSubtask(final int i) {
        return String.format("width:%d%%;background-color:#51A825", (i + 1) * (100 / SUBTASK_COUNT));
    }

}
