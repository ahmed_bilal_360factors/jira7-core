package com.atlassian.jira.webtests.ztests.upgrade;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.locator.Locator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.LicenseKeys;
import org.junit.After;
import org.junit.Test;

import javax.inject.Inject;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringEndsWith.endsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

@WebTest({Category.FUNC_TEST, Category.UPGRADE_TASKS})
public class TestRenaissanceMigrationSummary extends BaseJiraFuncTest implements FunctTestConstants {
    private static final String SD_GROUP_CSSPATH = "#notMigratedGroups > li";
    private static final String MIGRATION_SUMMARY_ID = "migrationSummary";
    private static final String NOT_DEFINED_ROLES_ID = "notDefinedRoles";

    private static final String URL_VERSIONS_LICENSES = "/plugins/servlet/applications/versions-licenses";

    private static final String MIGRATION_SUMMARY_URL = "/secure/MigrationSummary.jspa";
    private static final String HOMEPAGE_URL = "/secure/MyJiraHome.jspa";
    private static final String BACKUP_BLANK_PROJECT_PRE_RENAISSANCE = "TestRenaissanceMigrationSummary/blankproject-6004.xml";
    private static final String BACKUP_AGENTS_NO_USE_PRE_RENAISSANCE = "TestRenaissanceMigrationSummary/agentsWithNoUse.xml";

    @Inject
    private TextAssertions textAssertions;

    @After
    public void tearDown() {
        this.backdoor.darkFeatures().enableForSite(DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG);
    }

    @Test
    public void testMigrationSummaryPageIsDisplayedOnceAfterRenaissanceMigration() {
        backdoor.restoreDataFromResource(BACKUP_BLANK_PROJECT_PRE_RENAISSANCE);
        this.backdoor.darkFeatures().disableForSite(DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG);

        //Login, should see migration page, click continue, should see dashboard. Login again should see dashboard only.
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        assertThat(navigation.getCurrentPage(), endsWith(MIGRATION_SUMMARY_URL));
        tester.clickLink("continueButton");
        assertThat(navigation.getCurrentPage(), endsWith(HOMEPAGE_URL));

        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        assertThat(navigation.getCurrentPage(), endsWith("/"));
    }

    @Test
    public void testDisplaysWarningAboutServiceDeskGroupsNotMigratedAndThereAreNotDefinedApplications() {
        backdoor.restoreDataFromResource(BACKUP_AGENTS_NO_USE_PRE_RENAISSANCE);
        backdoor.darkFeatures().disableForSite(DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG);

        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        assertThat(navigation.getCurrentPage(), endsWith(MIGRATION_SUMMARY_URL));
        //We should see warning about not migrated service desk groups
        textAssertions.assertTextPresent(new CssLocator(tester, SD_GROUP_CSSPATH), "service-desk-agents");

        Locator migrationSummaryLocator = new IdLocator(tester, MIGRATION_SUMMARY_ID);
        String txt = getI18nText("renaissance.migration.summary.install.undefined.applications.with.errors", "", "", "JIRA Service Desk, JIRA Software");
        textAssertions.assertTextPresent(migrationSummaryLocator.getText(), txt);
        txt = getI18nText("renaissance.migration.summary.update.almost");
        textAssertions.assertTextPresent(migrationSummaryLocator.getText(), txt);
    }

    @Test
    public void testDisplaysWarningAboutServiceDeskGroupsNotMigratedAndThereAreNotDefinedApplication() {
        backdoor.restoreDataFromResource(BACKUP_AGENTS_NO_USE_PRE_RENAISSANCE);
        backdoor.darkFeatures().disableForSite(DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG);
        backdoor.license().replace(LicenseKeys.CORE_SERVICEDESK);
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        assertThat(navigation.getCurrentPage(), endsWith(MIGRATION_SUMMARY_URL));
        //We should see warning about not migrated service desk groups
        textAssertions.assertTextPresent(new CssLocator(tester, SD_GROUP_CSSPATH), "service-desk-agents");

        Locator migrationSummaryLocator = new IdLocator(tester, MIGRATION_SUMMARY_ID);
        String txt = getI18nText("renaissance.migration.summary.install.undefined.application.with.errors", "", "", "JIRA Service Desk");
        textAssertions.assertTextPresent(migrationSummaryLocator.getText(), txt);
        txt = getI18nText("renaissance.migration.summary.update.almost");
        textAssertions.assertTextPresent(migrationSummaryLocator.getText(), txt);
    }

    @Test
    public void testDisplaysWarningAboutServiceDeskGroupsNotMigrated() {
        backdoor.restoreDataFromResource(BACKUP_AGENTS_NO_USE_PRE_RENAISSANCE);
        backdoor.darkFeatures().disableForSite(DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG);

        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        assertThat(navigation.getCurrentPage(), endsWith(MIGRATION_SUMMARY_URL));
        //We should see warning about not migrated service desk groups
        textAssertions.assertTextPresent(new CssLocator(tester, SD_GROUP_CSSPATH), "service-desk-agents");
    }

    //Ignoring this test as checking undefined applications is flaky cause there is no guarantee on
    //the order of which they will be displayed
    @Test
    public void testMigrationSummaryPageContainsLinkToVersionsAndLicencesWhenThereAreNotDefinedApplications() {
        backdoor.restoreDataFromResource(BACKUP_BLANK_PROJECT_PRE_RENAISSANCE,
                LicenseKeys.MULTI_ROLE.getLicenseString());

        this.backdoor.darkFeatures().disableForSite(DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG);

        //Login, should see migration page, click continue, should see dashboard. Login again should see dashboard only.
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        assertThat(navigation.getCurrentPage(), endsWith(MIGRATION_SUMMARY_URL));
        Locator migrationSummaryLocator = new IdLocator(tester, MIGRATION_SUMMARY_ID);
        textAssertions.assertTextPresentNumOccurences(migrationSummaryLocator.getHTML(), URL_VERSIONS_LICENSES, 1);

        final Stream<String> strings = getUndefinedApplicationsTestStrings();
        assertOnlyOneTextPresent(migrationSummaryLocator.getText(), strings);
    }

    private Stream<String> getUndefinedApplicationsTestStrings() {
        /*
         * the {@code LicenseKeys.MULTI_ROLE} license includes JIRA Reference application but by default bamboo test include jira-reference-plugin
         * which satisfies JIRA Reference application so JIRA Reference is installed and we should not check for its presence here.
         * The application names are hardcoded now because there is a little chance that the trademark names will change - if it happens application names should be updated here.
         */
        return Stream.of(
                getI18nText("renaissance.migration.summary.install.undefined.applications", "", "", "JIRA Service Desk, JIRA Software"),
                getI18nText("renaissance.migration.summary.install.undefined.applications", "", "", "JIRA Software, JIRA Service Desk")
        );
    }

    private void assertOnlyOneTextPresent(final String srcString, final Stream<String> strings) {
        assertNotNull(srcString);

        final long occurred = strings.filter(srcString::contains).count();
        final long expected = 1l;
        assertEquals("Wrong number of text appearing", expected, occurred);
    }

    @Test
    public void testMigrationSummaryPageContainsLinkToVersionsAndLicencesWhenThereIsNotDefinedApplication() {
        backdoor.restoreDataFromResource(BACKUP_BLANK_PROJECT_PRE_RENAISSANCE,
                LicenseKeys.CORE_SERVICEDESK.getLicenseString());

        this.backdoor.darkFeatures().disableForSite(DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG);

        //Login, should see migration page, click continue, should see dashboard. Login again should see dashboard only.
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        assertThat(navigation.getCurrentPage(), endsWith(MIGRATION_SUMMARY_URL));
        Locator migrationSummaryLocator = new IdLocator(tester, MIGRATION_SUMMARY_ID);
        textAssertions.assertTextPresentNumOccurences(migrationSummaryLocator.getHTML(), URL_VERSIONS_LICENSES, 1);

        String txt = getI18nText("renaissance.migration.summary.install.undefined.application", "", "", "JIRA Service Desk");
        textAssertions.assertTextPresent(migrationSummaryLocator.getText(), txt);
    }

    @Test
    public void testMigrationSummaryPageDoesNotContainLinkToVersionsAndLicencesWhenThereAreNoNotDefinedApplications() {
        backdoor.restoreDataFromResource(BACKUP_BLANK_PROJECT_PRE_RENAISSANCE,
                LicenseKeys.CORE_ROLE.getLicenseString());

        this.backdoor.darkFeatures().disableForSite(DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG);

        //Login, should see migration page, click continue, should see dashboard. Login again should see dashboard only.
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        assertThat(navigation.getCurrentPage(), endsWith(MIGRATION_SUMMARY_URL));

        this.getTester().assertElementNotPresent(NOT_DEFINED_ROLES_ID);
        Locator migrationSummaryLocator = new IdLocator(tester, MIGRATION_SUMMARY_ID);
        textAssertions.assertTextNotPresent(migrationSummaryLocator.getHTML(), URL_VERSIONS_LICENSES);
        textAssertions.assertTextPresent(migrationSummaryLocator.getText(), getI18nText("renaissance.migration.summary.update.allset"));
        textAssertions.assertTextNotPresent(migrationSummaryLocator.getText(), getI18nText("renaissance.migration.summary.update.almost"));
    }

    private String getLocale() {
        return "en_AU";
    }

    private String getI18nText(String key, String... params) {
        String txt = backdoor.i18n().getText(key, getLocale());
        for (int i = 0; i < params.length; i++) {
            txt = txt.replaceAll("\\{" + i + "\\}", params[i]);
        }
        return txt;
    }
}
