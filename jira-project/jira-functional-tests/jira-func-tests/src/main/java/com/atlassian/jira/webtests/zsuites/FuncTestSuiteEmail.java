package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Tests that check various e-mail JIRA related capabilities.
 *
 * @since v4.0
 */
public class FuncTestSuiteEmail {

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.email", true))
                .build();
    }
}