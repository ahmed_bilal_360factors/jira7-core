package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import com.atlassian.jira.functest.framework.backdoor.VersionControl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.ResourceRef;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.Version;
import com.google.common.collect.ImmutableList;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.net.URI;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;

import static com.atlassian.jira.testkit.client.util.JsonMatchers.hasField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.WEBHOOKS, Category.REFERENCE_PLUGIN})
public class TestVersionWebHook extends AbstractWebHookTest {
    private static final String VERSION_DESC = "Description";
    private static final String PROJECT_KEY = "MKY";
    private static final Random rand = new Random();
    public static final String JIRA_VERSION_DELETED_WEBHOOK_ID = "jira:version_deleted";

    private VersionControl versionControl;

    public final List<VersionTest> versionTests = ImmutableList.of(
            new VersionTest("jira:version_created", () -> {
                registerWebHook("jira:version_created");
                Version version = createVersion();
                return versionBeanFromVersion(version);
            }),
            new VersionTest("jira:version_released", () -> {
                registerWebHook("jira:version_released");
                Version version = createVersion();
                Version updatedVersion = versionControl.releaseVersion(version.id);
                return versionBeanFromVersion(updatedVersion);
            }),
            new VersionTest("jira:version_unreleased", () -> {
                registerWebHook("jira:version_unreleased");
                Version version = createVersion();
                versionControl.releaseVersion(version.id);
                versionControl.unreleaseVersion(version.id);
                return versionBeanFromVersion(version.released(false));
            }),
            new VersionTest("jira:version_updated", () -> {
                registerWebHook("jira:version_updated");
                Version version = createVersion();
                Version updatedVersion = versionControl.setDescription(version.id, "desc");
                return versionBeanFromVersion(updatedVersion);
            }),
            new VersionTest("jira:version_moved", () -> {
                registerWebHook("jira:version_moved");
                Version version = createVersion();
                versionControl.moveVersion(version.id);
                return versionBeanFromVersion(version);
            }),
            new VersionTest("jira:version_deleted", () -> {
                registerWebHook("jira:version_deleted");
                Version version = createVersion();
                versionControl.deleteVersion(version.id);
                return versionBeanFromVersion(version);
            }));

    @Before
    @Override
    public void setUpTest() {
        super.setUpTest();
        versionControl = new VersionControl(environmentData);
        backdoor.restoreBlankInstance();
    }

    @Test
    public void testWebHookIsSentForVersionEvent() throws Exception {
        for (VersionTest versionTest : versionTests) {
            backdoor.restoreBlankInstance();
            final VersionBean expectedValue = versionTest.performActionAndGetExpectedValue();
            WebHookEventVersionResult result = webHookEventVersionResult();
            assertEquals("webhook id different for " + versionTest.getWebHookId(), versionTest.getWebHookId(), result.getWebhookEvent());
            assertEquals("result different for " + versionTest.getWebHookId(), expectedValue, result.getVersion());
        }
    }

    @Test
    public void testUriVariablesAreCorrectlyProvidedForVersionWebHook() throws Exception {
        for (VersionTest versionTest : versionTests) {
            backdoor.restoreBlankInstance();
            final VersionBean expectedValue = versionTest.performActionAndGetExpectedValue();
            JSONObject parameters = getUrlParametersOfReceivedWebHook(versionTest.getWebHookId().replace("jira:", ""));
            assertThat(parameters, hasField("version_id").equalTo(expectedValue.getId()));
            assertThat(parameters, hasField("project_key").equalTo(PROJECT_KEY));
        }
    }

    @Test
    public void testWebHookIsSentForVersionMergeEvent() throws Exception {
        final String webHookId = "jira:version_merged";

        registerWebHook(webHookId);

        Version fromVersion = createVersion();
        createIssueAndUpdateWithVersion(fromVersion, PROJECT_KEY);

        Version versionNew = createVersion();
        versionControl.mergeVersion(fromVersion.id, versionNew.id);

        VersionBean expectedValue = versionBeanFromVersion(versionNew);
        VersionBean expectedValueMerged = versionBeanFromVersion(fromVersion);

        WebHookEventVersionMergedResult result = receivedByPersistentListener.get(WebHookEventVersionMergedResult.class);

        assertEquals(webHookId, result.getWebhookEvent());
        assertEquals(expectedValue, result.getVersion());
        assertEquals(expectedValueMerged, result.getMergedVersion());
    }

    @Test
    public void testDeleteEventWithMergedInformationIsSentWhenPerformingMerge() throws Exception {
        registerWebHook(JIRA_VERSION_DELETED_WEBHOOK_ID);

        Version fromVersion = createVersion();
        createIssueAndUpdateWithVersion(fromVersion, PROJECT_KEY);

        Version versionNew = createVersion();
        versionControl.mergeVersion(fromVersion.id, versionNew.id);

        VersionBean expectedValue = versionBeanFromVersion(fromVersion);
        VersionBean expectedValueMergedTo = versionBeanFromVersion(versionNew);

        WebHookEventVersionDeletedResult result = receivedByPersistentListener.get(WebHookEventVersionDeletedResult.class);

        assertEquals(JIRA_VERSION_DELETED_WEBHOOK_ID, result.getWebhookEvent());
        assertEquals(expectedValue, result.getVersion());
        assertEquals(expectedValueMergedTo, result.getAffectsVersionSwappedTo());
        assertEquals(expectedValueMergedTo, result.getFixVersionSwappedTo());
    }

    @Test
    public void testUriVariablesAreCorrecttlyProvidedForVersionMergeEventWebHook() throws Exception {
        Version fromVersion = createVersion();
        createIssueAndUpdateWithVersion(fromVersion, PROJECT_KEY);

        Version versionNew = createVersion();

        versionControl.mergeVersion(fromVersion.id, versionNew.id);

        VersionBean expectedValue = versionBeanFromVersion(versionNew);
        VersionBean expectedValueMerged = versionBeanFromVersion(fromVersion);

        JSONObject parameters = getUrlParametersOfReceivedWebHook("version_merged");
        assertThat(parameters, hasField("project_key").equalTo(PROJECT_KEY));
        assertThat(parameters, hasField("mergedVersion_id").equalTo(expectedValueMerged.getId()));
        assertThat(parameters, hasField("version_id").equalTo(expectedValue.getId()));
    }

    @Test
    public void testFixVersionSetWhenVersionDeleted() throws Exception {
        registerWebHook(JIRA_VERSION_DELETED_WEBHOOK_ID);

        final Version versionToDelete = createVersion();
        final Version fixVersionSwap = createVersion();
        final Version affectsVersionSwap = createVersion();

        final IssueCreateResponse issue = backdoor.issues().createIssue("MKY", "Test issue");

        backdoor.issues().setIssueFields(issue.key, new IssueFields().fixVersions(ResourceRef.withId(versionToDelete.id.toString())));

        versionControl.delete(versionToDelete.id, fixVersionSwap.self, affectsVersionSwap.self);

        WebHookEventVersionDeletedResult result = receivedByPersistentListener.get(WebHookEventVersionDeletedResult.class);

        assertEquals(JIRA_VERSION_DELETED_WEBHOOK_ID, result.getWebhookEvent());
        assertEquals(versionBeanFromVersion(versionToDelete), result.getVersion());
        assertEquals(versionBeanFromVersion(fixVersionSwap), result.getFixVersionSwappedTo());
        assertEquals(versionBeanFromVersion(affectsVersionSwap), result.getAffectsVersionSwappedTo());
    }

    private Version createVersion() {
        return versionControl.createVersion(String.valueOf(rand.nextLong()), VERSION_DESC, PROJECT_KEY);
    }

    private static class VersionTest {
        private final String webHookId;
        private final Callable<VersionBean> expectedValueCallable;

        private VersionTest(String webHookId, Callable<VersionBean> expectedValueCallable) {
            this.webHookId = webHookId;
            this.expectedValueCallable = expectedValueCallable;
        }

        public String getWebHookId() {
            return webHookId;
        }

        public VersionBean performActionAndGetExpectedValue() throws Exception {
            return expectedValueCallable.call();
        }
    }

    private class WebHookEventVersionResult {
        long timestamp;
        String webhookEvent;
        VersionBean version;

        public long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }

        //the getter has to have Webhook written with a lowercase h since returned json has webhookEvent field
        public String getWebhookEvent() {
            return webhookEvent;
        }

        public void setWebhookEvent(String webhookEvent) {
            this.webhookEvent = webhookEvent;
        }

        public VersionBean getVersion() {
            return version;
        }

        public void setVersion(VersionBean version) {
            this.version = version;
        }
    }

    private class WebHookEventVersionMergedResult extends WebHookEventVersionResult {
        VersionBean mergedVersion;

        public VersionBean getMergedVersion() {
            return mergedVersion;
        }

        public void setMergedVersion(VersionBean mergedVersion) {
            this.mergedVersion = mergedVersion;
        }
    }

    private class WebHookEventVersionDeletedResult extends WebHookEventVersionResult {
        VersionBean mergedTo;
        VersionBean affectsVersionSwappedTo;
        VersionBean fixVersionSwappedTo;

        public VersionBean getMergedTo() {
            return mergedTo;
        }

        public void setMergedTo(final VersionBean mergedTo) {
            this.mergedTo = mergedTo;
        }

        public VersionBean getAffectsVersionSwappedTo() {
            return affectsVersionSwappedTo;
        }

        public void setAffectsVersionSwappedTo(final VersionBean affectsVersionSwappedTo) {
            this.affectsVersionSwappedTo = affectsVersionSwappedTo;
        }

        public VersionBean getFixVersionSwappedTo() {
            return fixVersionSwappedTo;
        }

        public void setFixVersionSwappedTo(final VersionBean fixVersionSwappedTo) {
            this.fixVersionSwappedTo = fixVersionSwappedTo;
        }
    }

    private WebHookEventVersionResult webHookEventVersionResult() throws Exception {
        return receivedByPersistentListener.get(WebHookEventVersionResult.class);
    }

    private Issue createIssueAndUpdateWithVersion(Version version, String projectKey) {
        String issueKey = backdoor.issues().createIssue(projectKey, "test summary").key();
        backdoor.issues().setIssueFields(issueKey, new IssueFields().versions(new ResourceRef().name(version.name)));
        return backdoor.issues().getIssue(issueKey);
    }

    private static VersionBean versionBeanFromVersion(Version version) {
        return new VersionBean.Builder()
                .setName(version.name)
                .setDescription(version.description)
                .setProjectId(version.projectId)
                .setId(version.id)
                .setSelf(URI.create(version.self))
                .setReleased(version.released)
                .setOverdue(version.overdue == null ? false : version.overdue)
                .build();
    }
}
