package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.webtests.ztests.imports.project.TestProjectImportPluginData;
import com.atlassian.jira.webtests.ztests.imports.project.TestProjectImportResults;
import com.atlassian.jira.webtests.ztests.imports.project.TestProjectImportSelectBackup;
import com.atlassian.jira.webtests.ztests.imports.project.TestProjectImportSelectProject;
import com.atlassian.jira.webtests.ztests.imports.project.TestProjectImportSummary;
import com.atlassian.jira.webtests.ztests.imports.project.TestProjectImportUsersDoNotExistPage;
import com.atlassian.jira.webtests.ztests.imports.project.TestProjectImportWithProjectKeyRename;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of tests around JIRA project import
 *
 * @since v4.0
 */
public class FuncTestSuiteProjectImport {

    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestProjectImportSelectBackup.class)
                .add(TestProjectImportUsersDoNotExistPage.class)
                .add(TestProjectImportSummary.class)
                .add(TestProjectImportResults.class)
                .add(TestProjectImportSelectProject.class)
                .add(TestProjectImportWithProjectKeyRename.class)
                .add(TestProjectImportPluginData.class)
                .build();
    }
}