package com.atlassian.jira.webtests.ztests.admin.audit;

import com.atlassian.jira.auditing.AuditingCategory;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.ztests.admin.AuditingClient;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

import static com.atlassian.jira.webtests.ztests.admin.AuditingClient.ViewResponse;
import static org.junit.Assert.assertEquals;

/**
 * This should always pass in basic func tests; the main value is from TPM builds
 *
 * @since 7.0.0
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION})
public class AuditLoggerTest extends BaseJiraFuncTest {
    @Test
    @LoginAs(user = "admin")
    @RestoreBlankInstance
    public void testAuditLogDescription2000IsOKInThisDb() throws JSONException, IOException {
        MyAuditingClient auditingClient = new MyAuditingClient();

        TestingAuditRecord auditRecord = new TestingAuditRecord();
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < 2000; i++) {
            s.append('i');
        }
        String desc = s.toString();
        auditRecord.setDescription(desc);

        MockAuditRecordBean bean = new MockAuditRecordBean(auditRecord, AuditingCategory.AUDITING.name());
        bean.setDescription(desc);
        auditingClient.postBean(bean);

        final List<ViewResponse.RecordResponse> records = auditingClient.getViewResponse().getRecords();

        assertEquals(desc, records.get(0).getDescription());
    }


    private class MyAuditingClient extends AuditingClient {
        public MyAuditingClient() {
            super(AuditLoggerTest.this.getEnvironmentData());
        }

        @Override
        protected WebResource createResource() {
            return resourceRoot(environmentData.getBaseUrl().toExternalForm())
                    .path("rest").path("api").path("latest").path("auditing").path("record");
        }

        @Override
        public ViewResponse getViewResponse() {
            return super.getViewResponse();
        }

        public void postBean(Object bean) throws JSONException, IOException {
            final WebResource resource = createResource();

            final ObjectMapper objectMapper = new ObjectMapper();

            final String requestEntity = objectMapper.writeValueAsString(bean);
            resource.type(MediaType.APPLICATION_JSON_TYPE).post(requestEntity);
        }
    }
}