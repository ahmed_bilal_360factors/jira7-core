package com.atlassian.jira.functest.rule;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.lang.annotation.Annotation;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Supplier;

import static org.junit.Assume.assumeTrue;

abstract public class AbstractBuildNumberRule<T extends Annotation> implements TestRule {

    private final Class<T> annotation;
    private final BiPredicate<T, Integer> skipTestPredicate;
    private final Function<T, Integer> annotationBuildNumberFunction;
    private final Supplier<Backdoor> backdoor;


    AbstractBuildNumberRule(Supplier<Backdoor> backdoor, Class<T> annotation, BiPredicate<T, Integer> skipTestPredicate, Function<T, Integer> annotationBuildNumberFunction) {
        this.backdoor = backdoor;
        this.annotation = annotation;
        this.skipTestPredicate = skipTestPredicate;
        this.annotationBuildNumberFunction = annotationBuildNumberFunction;
    }

    @Override
    public Statement apply(Statement statement, Description description) {
        final T annotation = description.getAnnotation(this.annotation);
        if (annotation != null) {
            final int dbBuildNumber = backdoor.get().upgradeControl().getHostDetails().getBuildNumber();

            if (skipTestPredicate.test(annotation, dbBuildNumber)) {
                return new Statement() {
                    @Override
                    public void evaluate() throws Throwable {
                        assumeTrue(describeUnsatisfiedRule(description.getDisplayName(), annotationBuildNumberFunction.apply(annotation), dbBuildNumber), false);
                    }
                };
            }
        }

        return statement;
    }

    abstract String describeUnsatisfiedRule(String testName, int annotationBuildNumber, int dbBuildNumber);
}
