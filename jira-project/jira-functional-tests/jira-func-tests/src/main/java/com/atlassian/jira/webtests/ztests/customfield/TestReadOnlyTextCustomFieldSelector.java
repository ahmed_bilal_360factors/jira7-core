package com.atlassian.jira.webtests.ztests.customfield;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.admin.CustomFields;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_READONLYTEXT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_FIELD_SCREEN_NAME;

@WebTest({Category.FUNC_TEST, Category.BROWSING, Category.CUSTOM_FIELDS})
@Restore("blankprojects.xml")
@LoginAs(user = FunctTestConstants.ADMIN_USERNAME)
public class TestReadOnlyTextCustomFieldSelector extends BaseJiraFuncTest {
    public static final String CUSTOM_FIELD_NAME = "myreadonlytextfield";
    public static final String ISSUE_SUMMARY = "This is my summary";

    @Inject
    private CustomFields customFields;

    @Test
    public void testReadOnlyTextNoXXS() throws SAXException {
        String customFieldId = customFields.addCustomField(CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_READONLYTEXT), CUSTOM_FIELD_NAME);
        backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, CUSTOM_FIELD_NAME);


        IssueCreateResponse issue = backdoor.issues().createIssue("HSP", ISSUE_SUMMARY);

        IssueFields fields = new IssueFields();

        Long fieldId = Long.valueOf(CustomFields.numericCfId(customFieldId));
        fields.customField(fieldId, "<script>alert(0);</script>");
        backdoor.issues().setIssueFields(issue.key(), fields);

        navigation.issue().viewIssue(issue.key());

        tester.assertTextPresent("&lt;script&gt;alert(0);&lt;/script&gt;");
        tester.assertTextNotPresent("<script>alert(0);</script>");
    }
}
