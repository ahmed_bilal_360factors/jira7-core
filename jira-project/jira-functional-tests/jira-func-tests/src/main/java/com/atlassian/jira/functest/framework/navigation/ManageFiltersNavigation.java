package com.atlassian.jira.functest.framework.navigation;

import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.navigator.NavigatorCondition;
import com.atlassian.jira.functest.framework.navigator.NavigatorSearch;
import com.atlassian.jira.functest.framework.parser.filter.FilterItem;
import com.atlassian.jira.functest.framework.sharing.SharedEntityInfo;
import com.atlassian.jira.functest.framework.sharing.TestSharingPermission;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The FilterNavigation representing the ManageFilters views, this implementation should be complete and
 * definitive of the full functionality of the view.
 */
public class ManageFiltersNavigation implements FilterNavigation {
    private final WebTester tester;
    private final JIRAEnvironmentData environmentData;
    private final HtmlPage page;
    private IssueNavigatorNavigation issueNavigatorNavigation;

    @Inject
    public ManageFiltersNavigation(WebTester tester, JIRAEnvironmentData environmentData) {
        this.tester = tester;
        this.environmentData = environmentData;
        this.page = new HtmlPage(tester);
        this.issueNavigatorNavigation = null;
    }

    public void addFavourite(final int id) {
        tester.gotoPage(page.addXsrfToken("secure/AddFavourite.jspa?entityId=" + id + "&entityType=SearchRequest"));
    }

    public void removeFavourite(final int id) {
        tester.gotoPage(page.addXsrfToken("secure/RemoveFavourite.jspa?entityId=" + id + "&entityType=SearchRequest"));
    }

    public void goToDefault() {
        favouriteFilters();
    }

    public void manageSubscriptions(final int filterId) {
        tester.gotoPage("secure/ViewSubscriptions.jspa?filterId=" + filterId);
    }

    public void addSubscription(final int filterId) {
        tester.gotoPage("/secure/EditSubscription!default.jspa?filterId=" + filterId);
    }

    public void favouriteFilters() {
        tester.gotoPage("secure/ManageFilters.jspa?filterView=favourites");
    }

    public void myFilters() {
        tester.gotoPage("secure/ManageFilters.jspa?filterView=my");
    }

    public void allFilters() {
        tester.gotoPage("secure/ManageFilters.jspa?filterView=search&pressedSearchButton=true&searchName=&searchOwner=&Search=");
    }

    public void popularFilters() {
        tester.gotoPage("secure/ManageFilters.jspa?filterView=popular");
    }

    public void searchFilters() {
        tester.gotoPage("secure/ManageFilters.jspa?filterView=search");
    }

    public long createFilter(final String filterName, final String filterDesc) {
        return createFilter(filterName, filterDesc, null);
    }

    @Override
    public long createFilter(String filterName, String filterDesc, Set<TestSharingPermission> sharingPermissions) {
        final SharedEntityInfo info = new SharedEntityInfo(filterName, filterDesc, true, sharingPermissions);
        final NavigatorSearch search = new NavigatorSearch(Collections.<NavigatorCondition>emptyList());
        return getIssueNavigatorNavigation().createNewAndSaveAsFilter(info, search);
    }

    private IssueNavigatorNavigation getIssueNavigatorNavigation() {
        if (issueNavigatorNavigation == null) {
            issueNavigatorNavigation = new IssueNavigatorNavigationImpl(tester, environmentData);
        }
        return issueNavigatorNavigation;
    }

    public void deleteFilter(final int id) {
        tester.gotoPage(page.addXsrfToken("secure/DeleteFilter.jspa?filterId=" + id + "&returnUrl=ManageFilters.jspa"));
    }

    public void findFilters(final String filterName) {
        tester.getDialog().setFormParameter("searchName", filterName);
        tester.submit("Search");
    }

    public List<FilterItem> sanitiseSearchFilterItems(final List<FilterItem> expectedItems) {
        return expectedItems;
    }

    public List<FilterItem> sanitiseFavouriteFilterItems(final List<FilterItem> expectedItems) {
        return expectedItems;
    }

    public String getActionBaseUrl() {
        return "ManageFilters.jspa";
    }

    public FilterNavigation projects() {
        throw new UnsupportedOperationException("The projects view is unique to the filterpickerpopup implementation");
    }
}
