package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.FuncTestRuleChain;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.w3c.dom.Node;

import javax.inject.Inject;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import static com.atlassian.collectors.CollectorsUtil.toImmutableListWithCapacity;
import static com.atlassian.jira.matchers.HasItemsInOrderMatcher.hasItemsInOrder;
import static com.atlassian.jira.matchers.HasSubsequenceMatcher.hasSubsequenceOf;
import static org.junit.Assert.assertThat;

/**
 * @since v7.0
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION})
@RestoreBlankInstance
@LoginAs(user = "admin")
public class TestSystemAdminItemsLayout {
    @Rule
    public TestRule initData = FuncTestRuleChain.forTest(this);
    @Inject
    private Navigation navigation;
    @Inject
    private LocatorFactory locatorFactory;
    private List<String> headersTexts;
    private List<String> allMenuItemsTexts;
    private List<String> topAdministrationTabs;

    @Before
    public void setUp() {
        navigation.gotoAdminSection(Navigation.AdminSection.GENERAL_CONFIGURATION);
        headersTexts = getAdminMenuHeadings();
        allMenuItemsTexts = getAllAdminMenuItems();
        topAdministrationTabs = getTopAdministrationTabs();
    }

    @Test
    public void shouldAddTopAdministrationTabsInCorrectOrder() {
        assertThat(topAdministrationTabs, hasSubsequenceOf("Applications", "Projects", "Issues", "Add-ons", "User management", "System"));
    }

    @Test
    public void shouldAddIssueCollectorsToSystemPage() {
        // sections sequence
        assertThat(headersTexts, hasItemsInOrder("Security", "User Interface"));  // Issue collectors section is in between but has no title
        // items in correct section
        assertThat(allMenuItemsTexts, hasItemsInOrder("Security", "Issue collectors"));
    }

    @Test
    public void shouldAddProjectRolesToSecuritySection() {
        // items in correct section
        assertThat(allMenuItemsTexts, hasSubsequenceOf("Security", "Project roles"));
    }

    @Test
    public void shouldAddAtlassianSupportToolsToTroubleshootinAdnSupportSection() {
        // no "Atlassian support tools" section in between these two
        assertThat(headersTexts, hasSubsequenceOf("Troubleshooting and Support", "Security"));
        // items in correct section
        assertThat(allMenuItemsTexts, hasItemsInOrder("Troubleshooting and Support", "Support Tools"));
    }

    @Test
    public void shouldAddDatabaseMonitoringToTroubleshootinAdnSupportSection() {
        // items in correct section
        assertThat(allMenuItemsTexts, hasItemsInOrder("Troubleshooting and Support", "Database monitoring"));
        assertThat(allMenuItemsTexts, hasSubsequenceOf("Instrumentation", "Database monitoring", "Integrity checker"));
    }

    @Test
    public void orderOfItemsInTroubleshootingAndSupportSectionShouldBeFixed() {
        // assert on section order:
        assertThat(headersTexts, hasSubsequenceOf("Troubleshooting and Support", "Security"));
        // items in correct section
        assertThat(allMenuItemsTexts, hasItemsInOrder(
                // section header:
                "Troubleshooting and Support",
                // elements that should have fixed order:
                "System info", "Instrumentation", "Integrity checker", "Logging and profiling", "Scheduler details",
                // and next section:
                "Security"));
    }

    @Test
    public void shouldAddAdminHelperItemsToSystemPage() {
        // then
        // sections sequence
        assertThat(headersTexts, hasItemsInOrder("Mail", "Admin helper", "Shared Items"));
        // items in correct section
        assertThat(allMenuItemsTexts, hasSubsequenceOf("Admin helper", "Permission helper", "Notification helper"));
    }

    private List<String> getAdminMenuHeadings() {
        final Node[] headers = locatorFactory.css(".admin-menu-links .aui-nav-heading").getNodes();

        return extractNodeValues(headers);
    }

    private List<String> getAllAdminMenuItems() {
        final Node[] allMenuLabels = locatorFactory.css(".admin-menu-links :matchesOwn([A-Za-z])").getNodes();

        return extractNodeValues(allMenuLabels);
    }

    private List<String> getTopAdministrationTabs() {
        final Node[] administrationTabs = locatorFactory.css("#admin-nav-heading .aui-nav li span").getNodes();
        return extractNodeValues(administrationTabs);
    }

    private static List<String> extractNodeValues(final Node[] nodes) {
        final Function<Node, String> trimNodeValue = ((Function<Node, String>) Node::getNodeValue).andThen(String::trim);

        return Stream.of(nodes)
                .map(trimNodeValue)
                .collect(toImmutableListWithCapacity(nodes.length));
    }
}
