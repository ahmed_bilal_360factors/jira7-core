package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Responsible for holding tests which verify that the User Administration actions are not susceptible to XSRF attacks.
 *
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@Restore("TestEditCustomFieldDescription.xml")
public class TestXsrfAdminFieldConfigurations extends BaseJiraFuncTest {
    private static final String FIELD_NAME = "config-name";

    @Inject
    private Form form;

    @Test
    public void testFieldConfigurationOperations() throws Exception {
        new XsrfTestSuite(
                new XsrfCheck(
                        "Add Field Configuration",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                navigation.gotoAdminSection(Navigation.AdminSection.FIELD_CONFIGURATION);
                                tester.clickLink("add-field-configuration");
                                tester.setFormElement("fieldLayoutName", FIELD_NAME);
                            }
                        },
                        new XsrfCheck.FormSubmission("Add")),
                new XsrfCheck(
                        "Copy Field Configuration",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                addFieldConfiguration();
                                tester.clickLink("copy-" + FIELD_NAME);
                            }
                        },
                        new XsrfCheck.FormSubmission("Copy")),
                new XsrfCheck(
                        "Edit Custom Field",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                addFieldConfiguration();
                                tester.clickLink("edit-" + FIELD_NAME);
                                tester.setFormElement("fieldLayoutName", FIELD_NAME + "-i-edit-you");
                            }
                        },
                        new XsrfCheck.FormSubmission("Update")),
                new XsrfCheck(
                        "Delete Custom Field",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                addFieldConfiguration();
                                tester.clickLink("delete-" + FIELD_NAME);
                            }
                        },
                        new XsrfCheck.FormSubmission("Delete"))

        ).run(tester, navigation, form);
    }

    @Test
    public void testFieldConfigurationConfigureOperations() throws Exception {
        addFieldConfiguration();

        new XsrfTestSuite(
                new XsrfCheck(
                        "Configure Field Configuration Edit",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                viewConfiguration();
                                tester.clickLink("edit_0");
                                tester.setFormElement("description", "OMG description ^_^");
                            }
                        },
                        new XsrfCheck.FormSubmission("Update")),
                new XsrfCheck(
                        "Configure Field Configuration Hide",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                viewConfiguration();
                            }
                        },
                        new XsrfCheck.LinkWithIdSubmission("hide_0")),
                new XsrfCheck(
                        "Configure Field Configuration Show",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                viewConfiguration();
                            }
                        },
                        new XsrfCheck.LinkWithIdSubmission("show_0")),
                new XsrfCheck(
                        "Configure Field Configuration Required",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                viewConfiguration();
                            }
                        },
                        new XsrfCheck.LinkWithIdSubmission("require_0")),
                new XsrfCheck(
                        "Configure Field Configuration Optional",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                viewConfiguration();
                            }
                        },
                        new XsrfCheck.LinkWithIdSubmission("require_0")),
                new XsrfCheck(
                        "Configure Field Configuration Renderers",
                        new XsrfCheck.Setup() {
                            public void setup() {
                                viewConfiguration();
                                tester.clickLink("renderer_versions");
                                tester.setWorkingForm("jiraform");
                                tester.clickButton("update_submit"); // takes us to confirm screen
                            }
                        },
                        new XsrfCheck.FormSubmission("Update"))

        ).run(tester, navigation, form);

    }

    private void addFieldConfiguration() {
        navigation.gotoAdminSection(Navigation.AdminSection.FIELD_CONFIGURATION);
        tester.clickLink("add-field-configuration");
        tester.setFormElement("fieldLayoutName", FIELD_NAME);
        tester.clickButton("add-field-configuration-submit");
        // Hack: we go to the field configuration list so that if we already have created the field configuration we can
        // continue on with the test and do not get stuck in the add field configuration form.
        navigation.gotoAdminSection(Navigation.AdminSection.FIELD_CONFIGURATION);
    }

    private void viewConfiguration() {
        navigation.gotoAdminSection(Navigation.AdminSection.FIELD_CONFIGURATION);
        tester.clickLink("configure-" + FIELD_NAME);
    }
}
