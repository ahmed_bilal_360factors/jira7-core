package com.atlassian.jira.webtests.ztests.upgrade;

import com.atlassian.jira.functest.config.CheckOptions;
import com.atlassian.jira.functest.config.CheckOptionsUtils;
import com.atlassian.jira.functest.config.ConfigFile;
import com.atlassian.jira.functest.config.ConfigFileWalker;
import com.atlassian.jira.functest.config.ConfigurationDefaults;
import com.atlassian.jira.functest.config.JiraConfig;
import com.atlassian.jira.functest.config.xml.Checks;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.dom4j.Document;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.fail;

/**
 * You can run this test to upgrade the JIRA's test XML. It basically goes through all of the XML (ZIP) files in your
 * func test (selenium test) XML directories as identified from your localtest.properties and upgrades them. It does
 * this by importing the data into JIRA and then exports it.
 * <p/>
 * To upgrade your data simply setup JIRA for a regular func test run (i.e start the server and configure your
 * localtest.properties) and then run this test. If you run it from the  jira-func-test module then it will upgrade the
 * func test xml. If you run it from the jira-selenium-test module then it will upgrade the selenium test xml.
 * <p/>
 * You can add 'suppresscheck: upgrade' to the top level comment in an XML to stop it from being upgraded. This
 * is useful for XML data that is meant test upgrade tasks. It is also useful for upgrade tasks that have replacement
 * tokens and can only be imported using their associated tests.
 *
 * @since v4.3
 */
@WebTest({Category.FUNC_TEST, Category.INFRASTRUCTURE, Category.SLOW_IMPORT})
@LoginAs(user = ADMIN_USERNAME)
public class TestUpgradeXmlData extends BaseJiraFuncTest implements ConfigFileWalker.ConfigVisitor {
    private static final Logger log = LoggerFactory.getLogger(TestUpgradeXmlData.class);

    private static final String JIRA_XML_UPGRADE_DATA_LOCATION = "jira.xml.upgrade.dataLocation";
    private static final String JIRA_XML_UPGRADE_ADD_SUPPRESS_OPTIONS = "jira.xml.upgrade.add.suppress.options";

    private List<File> brokenFiles = new LinkedList<>();
    private long currentBuildNumber;
    private File dataLocation;

    @Inject
    private Administration administration;

    private static String getRelativeFile(final File base, final File file) {
        final String basePath = base.getPath();
        final String filePath = file.getPath();
        if (filePath != null && filePath.startsWith(basePath)) {
            String s = filePath.substring(basePath.length());
            if (s.startsWith(File.separator)) {
                s = s.substring(File.separator.length());
            }
            return s;
        } else {
            return file.getName();
        }
    }

    private static File normalizeFile(final File importFile) {
        try {
            return importFile.getCanonicalFile();
        } catch (IOException e) {
            return importFile.getAbsoluteFile();
        }
    }

    @Test
    public void testUpgradeTestData() throws Exception {
        dataLocation = getDataLocation();
        log.debug(String.format("Using the following data location %s ", dataLocation.getAbsolutePath()));
        currentBuildNumber = administration.getBuildNumber();

        final ConfigFileWalker fileWalker = new ConfigFileWalker(dataLocation, this);
        fileWalker.setExcludes(ConfigurationDefaults.getDefaultExcludedFilters());
        fileWalker.walk();

        if (!brokenFiles.isEmpty()) {
            final StringWriter writer = new StringWriter();
            PrintWriter builder = new PrintWriter(writer);
            builder.println("Unable to update all the XML.");
            for (File brokenFile : brokenFiles) {
                builder.format("\t%s%n", brokenFile.getAbsolutePath());
            }

            builder.close();
            fail(writer.toString());
        }
        dataLocation = null;
        brokenFiles = null;
    }

    @Override
    public void visitConfig(final ConfigFile entitiesFile) {
        // We consider broken files the ones that
        // - Are not xmls
        // - Cannot be safely restored

        final File importFile = entitiesFile.getFile();
        final Document entitiesDocument = entitiesFile.readConfig();
        final CheckOptions checkOptions = CheckOptionsUtils.parseOptions(entitiesDocument);
        if (!checkOptions.checkEnabled(Checks.UPGRADE)) {
            log.info(String.format("Not upgrading '%s' as it contains the '%s' suppresscheck flag.",
                    importFile, Checks.UPGRADE));
            return;
        }
        if (!JiraConfig.isJiraXml(entitiesDocument)) {
            brokenFiles.add(importFile);
            log.error(String.format("Not upgrading '%s' as it does not appear to be a JIRA backup.", importFile));
            return;
        }

        final JiraConfig config = new JiraConfig(entitiesDocument, importFile);
        long dataBuildNumber = config.getBuildNumber();
        if (dataBuildNumber >= currentBuildNumber) {
            log.warn(String.format("Not upgrading as data build number (%s) is greater than instance build number (%s).",
                    dataBuildNumber, currentBuildNumber));
            return;
        }

        final Set<String> sysAdmins = config.getSystemAdmins();
        if (!restoreDataSafely(importFile, sysAdmins)) {
            brokenFiles.add(importFile);
            return;
        }

        File newFile = administration.exportDataToFile(importFile.getName());

        // It was originally an XML file, so get the XML out of the zip and continue to use that
        if ("xml".equals(FilenameUtils.getExtension(importFile.getName()))) {
            try {
                final InputStream inputStream = new ZipFile(newFile).getInputStream(new ZipEntry(ConfigFile.ENTITIES_XML));
                final File extractedXml = new File(newFile.getParentFile(), importFile.getName());
                try {
                    FileUtils.forceDelete(extractedXml);
                } catch (FileNotFoundException ignored) {
                }

                final FileWriter fileWriter = new FileWriter(extractedXml);
                IOUtils.copy(inputStream, fileWriter);
                fileWriter.close();
                newFile = extractedXml;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        final ConfigFile newConfigFile = ConfigFile.create(newFile);

        //Make sure the document has the correct suppress checks.
        final Document newDocument = newConfigFile.readConfig();
        CheckOptionsUtils.writeOptions(newDocument, CheckOptionsUtils.disableIn(checkOptions, extraDisabledOptions()));
        newConfigFile.writeFile(newDocument);

        try {
            Files.move(newFile.toPath(), importFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String[] extraDisabledOptions() {
        /*
         * This option is designed to be enabled on CI when this test runs after TestXmlBackups test.
         * Its purpose is to mark all the upgraded xmls as passing existing checks, thus speeding up
         * TextXmlBackups significantly.
         */
        if (Boolean.getBoolean(JIRA_XML_UPGRADE_ADD_SUPPRESS_OPTIONS)) {
            List<String> keyList = ConfigurationDefaults.getListOfDefaultConfigurationChecksSuppressKeys();
            return keyList.toArray(new String[keyList.size()]);
        } else {
            return new String[0];
        }

    }

    public void visitConfigError(final File file, final ConfigFile.ConfigFileException e) {
        throw e;
    }

    private File getDataLocation() {
        final String fromSystem = System.getProperty(JIRA_XML_UPGRADE_DATA_LOCATION);
        if (fromSystem != null) {
            log.info("+++ Using property JIRA_XML_UPGRADE_DATA_LOCATION value: " + fromSystem);
            return normalizeFile(new File(fromSystem));
        } else {
            File xmlDataLocation = ConfigurationDefaults.getDefaultXmlDataLocation();
            log.info("+++ Using default value: " + xmlDataLocation.getAbsolutePath());
            return normalizeFile(xmlDataLocation);
        }
    }

    private boolean restoreDataSafely(final File file, final Set<String> admins) {
        try {
            copyFileToDataLocation(file);

            if (admins.isEmpty()) {
                administration.restoreDataSlowOldWay(getRelativeFile(dataLocation, file));
            } else {
                administration.restoreDataSlowOldWayAndLogin(getRelativeFile(dataLocation, file), admins.iterator().next());
            }
            return true;
        } catch (Throwable e) {
            log.error("Unable to restore '" + file.getAbsolutePath() + "'.", e);
            return false;
        }
    }

    private void copyFileToDataLocation(File file) throws IOException {
        File destination = getDestinationFile(file);
        if (file.getParent().equals(destination.getPath())) {
            log.warn(String.format("Not copying %s to %s as source and destination is the same",
                    file.getName(), destination.getPath()));
        } else {
            FileUtils.copyFileToDirectory(file, destination);
        }
    }

    private File getDestinationFile(final File file) {
        final File subdirectoryWithinSource = new File(getRelativeFile(dataLocation, file)).getParentFile();
        File destination;
        if (subdirectoryWithinSource != null) {
            destination = getSubdirectoryWithinTarget(subdirectoryWithinSource);
        } else {
            destination = environmentData.getXMLDataLocation();
        }
        return destination;
    }

    private File getSubdirectoryWithinTarget(File subdirectoryWithinDataLocation) {
        return new File(environmentData.getXMLDataLocation().getPath() + File.separator + subdirectoryWithinDataLocation.getPath());
    }
}
