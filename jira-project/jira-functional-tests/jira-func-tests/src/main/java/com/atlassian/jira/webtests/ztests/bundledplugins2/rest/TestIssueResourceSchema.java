package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Set;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.REST;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@WebTest({FUNC_TEST, REST})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueResourceSchema extends BaseJiraFuncTest {
    private IssueClient issueClient;

    @Inject
    private Administration administration;

    @Test
    public void testIssueRepresentationShouldContainExpandableSchemaField() throws Exception {
        administration.restoreData("TestIssueResourceCustomFields.xml");

        Issue minimal = issueClient.get("HSP-1");
        assertThat("names should not be expanded by default", minimal.schema, equalTo(null));
        assertThat(minimal.expand, containsString(Issue.Expand.schema.name()));

        Issue hsp1_expanded = issueClient.get("HSP-1", Issue.Expand.schema);
        Set<String> fields = hsp1_expanded.fields.idSet();
        Set<String> schema = hsp1_expanded.schema.keySet();
        assertTrue("Found in 'schema' but not in 'fields'" + Sets.difference(schema, fields), Sets.difference(schema, fields).isEmpty());
    }

    @Before
    public void setUpTest() {
        issueClient = new IssueClient(getEnvironmentData());
    }
}
