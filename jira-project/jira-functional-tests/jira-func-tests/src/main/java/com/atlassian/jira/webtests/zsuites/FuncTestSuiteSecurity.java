package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.atlassian.jira.webtests.ztests.issue.TestIssueSecurityActions;
import com.atlassian.jira.webtests.ztests.issue.TestIssueSecurityWithCustomFields;
import com.atlassian.jira.webtests.ztests.issue.TestIssueSecurityWithGroupsAndRoles;
import com.atlassian.jira.webtests.ztests.issue.TestIssueSecurityWithRoles;
import com.atlassian.jira.webtests.ztests.issue.move.TestPromptUserForSecurityLevelOnBulkMove;
import com.atlassian.jira.webtests.ztests.issue.move.TestPromptUserForSecurityLevelOnMove;
import com.atlassian.jira.webtests.ztests.misc.TestForgotLoginDetails;
import com.atlassian.jira.webtests.ztests.navigator.TestSearchRequestViewSecurity;
import com.atlassian.jira.webtests.ztests.project.TestMultipleProjectsWithIssueSecurityWithRoles;
import com.atlassian.jira.webtests.ztests.security.TestBackendActionResolution;
import com.atlassian.jira.webtests.ztests.security.TestRedirectAfterLogin;
import com.atlassian.jira.webtests.ztests.security.TestSignupWithExternalUserManagement;
import com.atlassian.jira.webtests.ztests.security.TestWebActionResolution;
import com.atlassian.jira.webtests.ztests.security.TestWebResourceRetrievalDoesNotExposeProtectedResources;
import com.atlassian.jira.webtests.ztests.subtask.TestIssueToSubTaskConversionSecurityLevel;
import com.atlassian.jira.webtests.ztests.subtask.TestSecurityLevelOfSubtasks;
import com.atlassian.jira.webtests.ztests.subtask.TestSubTaskToIssueConversionSecurityLevel;
import com.atlassian.jira.webtests.ztests.subtask.TestSubtaskSecurity;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A suite of test related to Security
 *
 * @since v4.0
 */
public class FuncTestSuiteSecurity {
    public static List<Class<?>> suite() {
        return ImmutableList.<Class<?>>builder()
                .add(TestIssueSecurityActions.class)
                .add(TestIssueSecurityWithGroupsAndRoles.class)
                .add(TestIssueSecurityWithCustomFields.class)
                .add(TestIssueSecurityWithRoles.class)
                .add(TestIssueToSubTaskConversionSecurityLevel.class)
                .add(TestSubTaskToIssueConversionSecurityLevel.class)
                .add(TestSearchRequestViewSecurity.class)
                .add(TestMultipleProjectsWithIssueSecurityWithRoles.class)
                .add(TestSecurityLevelOfSubtasks.class)
                .add(TestPromptUserForSecurityLevelOnMove.class)
                .add(TestPromptUserForSecurityLevelOnBulkMove.class)
                .add(TestSignupWithExternalUserManagement.class)
                .add(TestRedirectAfterLogin.class)
                .add(TestForgotLoginDetails.class)
                .add(TestSubtaskSecurity.class)
                .add(TestBackendActionResolution.class)
                .add(TestWebActionResolution.class)
                .add(TestWebResourceRetrievalDoesNotExposeProtectedResources.class)
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.security", true))
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.admin.security.xsrf", true))
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.issue.security.xsrf", true))
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.user.security.xsrf", true))
                .addAll(FuncTestSuite.getTestClasses("com.atlassian.jira.webtests.ztests.project.security.xss", true))

                .build();
    }
}