package com.atlassian.jira.webtests.ztests.timetracking.legacy;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.RequestAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.meterware.httpunit.WebResponseUtil;
import com.meterware.httpunit.WebTable;
import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Mode.MODERN;

@WebTest({Category.FUNC_TEST, Category.REPORTS, Category.TIME_TRACKING})
@LoginAs(user = ADMIN_USERNAME)
public class TestTimeTrackingExcelReport extends BaseJiraFuncTest {

    private static String EXCEL_REPORT_URL = "/secure/ConfigureReport!excelView.jspa?selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin:time-tracking&Next=Next&versionId=10001&sortingOrder=least&completedFilter=all";


    @Inject
    private FuncTestLogger logger;

    @Inject
    private RequestAssertions requestAssertions;

    @Inject
    private Administration administration;

    @Test
    @Restore("TestTimeTrackingReport.xml")
    public void testTimeTrackingExcelReport() throws SAXException {
        //same values as used by TestTimeTrackingReport
        tester.gotoPage("/secure/ConfigureReport!excelView.jspa?Next=Next&versionId=-1&reportKey=com.atlassian.jira.jira-core-reports-plugin:time-tracking&sortingOrder=least&selectedProjectId=10000&completedFilter=all");

        if (!WebResponseUtil.replaceResponseContentType(tester.getDialog().getResponse(), "text/html")) {
            Assert.fail("Failed to replace response content type with 'text/html'");
        }

        WebTable reportTable = tester.getDialog().getResponse().getTableWithID("time_tracking_report_table");
        assertions.getTableAssertions().assertTableRowEquals(reportTable, 3, new Object[]{"Bug", "HSP-1", "Open", "Major", "massive bug", "14400", "8370", "6030", "0"});
        assertions.getTableAssertions().assertTableRowEquals(reportTable, 4, new Object[]{"Bug", "HSP-2", "Open", "Major", "bug2", "1440", "1230", "210", "0"});
        assertions.getTableAssertions().assertTableRowEquals(reportTable, 5, new Object[]{"", "", "", "", "Total", "15840", "9600", "6240", "0", "0"});
    }

    @Test
    @Restore("TestTimeTrackingExcelReport.xml")
    public void testInternationalizationOfExcelReport() {
        administration.timeTracking().enable(MODERN);

        try {
            //need to change the content type to "text/html" so that HTTPUnit understands the response.
            //This can only be done from within the com.meterware.httpunit package and we therefore use the
            //WebResponseUtil class.
            if (!WebResponseUtil.replaceResponseContentType(tester.getDialog().getResponse(), "text/html")) {
                logger.log("Failed to replace response content type with 'text/html'");
                Assert.fail();
            } else {
                tester.gotoPage(EXCEL_REPORT_URL);

                // English statuses
                tester.assertTextPresent("In Progress");
                tester.assertTextPresent("Open");
            }
        } catch (Exception e) {
            logger.log("Failed to parse the printable view");
            logger.log(e);
            Assert.fail();
        }

        // change locale to German
        tester.gotoPage("/secure/Dashboard.jspa");
        setLocaleTo("Deutsch (Deutschland)");

        try {
            //need to change the content type to "text/html" so that HTTPUnit understands the response.
            //This can only be done from within the com.meterware.httpunit package and we therefore use the
            //WebResponseUtil class.
            if (!WebResponseUtil.replaceResponseContentType(tester.getDialog().getResponse(), "text/html")) {
                logger.log("Failed to replace response content type with 'text/html'");
                Assert.fail();
            } else {
                tester.gotoPage(EXCEL_REPORT_URL);

                // German statuses
                tester.assertTextPresent(backdoor.i18n().getText("jira.translation.status.inprogress.name", "de_DE"));
                tester.assertTextPresent(backdoor.i18n().getText("jira.translation.status.open.name", "de_DE"));
            }
        } catch (Exception e) {
            logger.log("Failed to parse the printable view");
            logger.log(e);
            Assert.fail();
        } finally {
            tester.gotoPage("/secure/Dashboard.jspa");
            navigation.userProfile().changeUserLanguageToJiraDefault();
        }
    }

    @Test
    @Restore("TestTimeTrackingReport.xml")
    public void testExcelReportResponseCanBeCached() throws SAXException {
        //same values as used by TestTimeTrackingReport
        tester.gotoPage("/secure/ConfigureReport!excelView.jspa?Next=Next&versionId=-1&reportKey=com.atlassian.jira.jira-core-reports-plugin:time-tracking&sortingOrder=least&selectedProjectId=10000&completedFilter=all");

        if (!WebResponseUtil.replaceResponseContentType(tester.getDialog().getResponse(), "text/html")) {
            Assert.fail("Failed to replace response content type with 'text/html'");
        }

        // JRA-14030: Make sure the Excel report response does not set Cache-control: no-cache headers
        requestAssertions.assertResponseCanBeCached();
    }

    private void setLocaleTo(String localeName) {
        navigation.userProfile().changeUserLanguage(localeName);
    }
}
