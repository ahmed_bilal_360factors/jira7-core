/**
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

package com.atlassian.jira.webtests.ztests.navigator;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.admin.TimeTracking;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.AbstractTestIssueNavigatorView;
import com.meterware.httpunit.WebLink;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Format.DAYS;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Format.HOURS;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@WebTest({Category.FUNC_TEST, Category.ISSUE_NAVIGATOR, Category.ISSUES})
@Restore("TestSearchRequestViewsAndIssueViews.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueNavigatorFullContentView extends AbstractTestIssueNavigatorView {

    private URL baseUrl;

    @Before
    public void setUpTest() {
        baseUrl = getEnvironmentData().getBaseUrl();
    }

    @Inject
    private TimeTracking timeTracking;

    @Test
    public void testFullContentViewItem1() {
        goToView("status=Open");

        final Item item = item1;
        verifyCommons(item);

        final String responseText = tester.getDialog().getResponseText();
        assertions.getTextAssertions().assertTextSequence(responseText, "Affects Version/s", "None");
        assertions.getTextAssertions().assertTextSequence(responseText, "Fix Version/s", "New Version 5");
        assertions.getTextAssertions().assertTextSequence(responseText, "duplicates", "HSP-10");
        assertions.getTextAssertions().assertTextSequence(responseText, "HSP-10", "Big 01");
        assertions.getTextAssertions().assertTextSequence(responseText, "Big 01", "Resolved");
        assertions.getTextAssertions().assertTextSequence(responseText, "CascadingSelectField", "value 01");
        assertions.getTextAssertions().assertTextSequence(responseText, "CascadingSelectField", "value 013");

        // links
        assertions.getTextAssertions();
        tester.assertLinkPresentWithText("New Version 5");
        assertLinkPresent(new String[]{
                baseUrl + "/issues/",
                "jql=project%3D10000%20AND%20%22fixVersion%22%3D10000%20ORDER%20BY%20priority%20ASC"});

        assertUserProfileLink(item.getAttribute(ATT_ASSIGNEE), "dev");

        tester.assertLinkPresentWithText("HSP-10");
        assertLinkPresentWithUrl(baseUrl + "/browse/HSP-10", true);
        tester.assertLinkNotPresentWithText("HSP-11");
        tester.assertLinkNotPresentWithText("HSP-12");

        assertSingleVersionPicker("New Version 5", "New+Version+5");
        assertVersionPicker("New Version 2", "New+Version+2");
        assertVersionPicker("New Version 4", "New+Version+4");
    }

    @Test
    public void testFullContentViewItem1DaysTimeFormat() {
        reconfigureTimetracking(DAYS);
        testFullContentViewItem1();
    }

    @Test
    public void testFullContentViewItem1HoursTimeFormat() {
        reconfigureTimetracking(HOURS);
        testFullContentViewItem1();
    }


    protected void goToView(final String jql) {
        try {
            tester.gotoPage("/sr/jira.issueviews:searchrequest-fullcontent/temp/SearchRequest.html?jqlQuery=" + URLEncoder.encode(jql, "UTF-8") + "&tempMax=1000");
        } catch (final UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testFullContentViewItem2() {
        goToView("status=\"In Progress\"");

        final Item item = item2;
        verifyCommons(item);

        final String responseText = tester.getDialog().getResponseText();
        assertions.getTextAssertions().assertTextSequence(responseText, "Affects Version/s", "New Version 2");
        assertions.getTextAssertions().assertTextSequence(responseText, "Fix Version/s", "New Version 4");
        assertions.getTextAssertions().assertTextSequence(responseText, "duplicates", "HSP-10");
        assertions.getTextAssertions().assertTextSequence(responseText, "HSP-10", "Big 01");
        assertions.getTextAssertions().assertTextSequence(responseText, "Big 01", "Resolved");
        assertions.getTextAssertions().assertTextSequence(responseText, "CascadingSelectField", "value 05");
        assertions.getTextAssertions().assertTextSequence(responseText, "ProjectPickerField", "homosapien");
        assertLinkPresent(new String[]{baseUrl + "/secure/BrowseProject.jspa", "id=10000"});

        // links
        tester.assertLinkPresentWithText("New Version 4");
        assertLinkPresent(new String[]{
                baseUrl + "/issues/",
                "jql=project%3D10000%20AND%20%22fixVersion%22%3D10001%20ORDER%20BY%20priority%20ASC"});

        assertUserProfileLink(item.getAttribute(ATT_ASSIGNEE), ADMIN_USERNAME);

        tester.assertLinkPresentWithText("HSP-10");
        assertLinkPresentWithUrl(baseUrl + "/browse/HSP-10", true);
        tester.assertLinkNotPresentWithText("HSP-11");
        tester.assertLinkNotPresentWithText("HSP-12");

        assertSingleVersionPicker("New Version 5", "New+Version+5");
        assertVersionPicker("New Version 5", "New+Version+5");
    }

    @Test
    public void testFullContentViewItem2DaysTimeFormat() {
        reconfigureTimetracking(DAYS);
        testFullContentViewItem2();
    }

    @Test
    public void testFullContentViewItem2HoursTimeFormat() {
        reconfigureTimetracking(HOURS);
        testFullContentViewItem2();
    }

    @Test
    public void testFullContentViewItem3() {
        goToView("status=\"Resolved\"");

        final Item item = item3;
        verifyCommons(item);

        final String responseText = tester.getDialog().getResponseText();
        assertions.getTextAssertions().assertTextSequence(responseText, "Affects Version/s", "New Version 4");
        assertions.getTextAssertions().assertTextSequence(responseText, "Fix Version/s", "New Version 5");
        assertions.getTextAssertions().assertTextSequence(responseText, "duplicates", "HSP-11");
        assertions.getTextAssertions().assertTextSequence(responseText, "HSP-11", "Minor Bug 01");
        assertions.getTextAssertions().assertTextSequence(responseText, "Minor Bug 01", "In Progress");
        assertions.getTextAssertions().assertTextSequence(responseText, "is duplicated by", "HSP-11");
        assertions.getTextAssertions().assertTextSequence(responseText, "HSP-11", "Minor Bug 01");
        assertions.getTextAssertions().assertTextSequence(responseText, "Minor Bug 01", "In Progress");
        assertions.getTextAssertions().assertTextSequence(responseText, "is duplicated by", "HSP-12");
        assertions.getTextAssertions().assertTextSequence(responseText, "HSP-12", "Feature 00");
        assertions.getTextAssertions().assertTextSequence(responseText, "Feature 00", "Open");
        assertions.getTextAssertions().assertTextSequence(responseText, "CascadingSelectField", "value 01");
        assertions.getTextAssertions().assertTextSequence(responseText, "value 01", "value 013");
        assertions.getTextAssertions().assertTextSequence(responseText, "GroupPickerField", "jira-developers");

        // links
        tester.assertLinkPresentWithText("New Version 4");
        assertLinkPresent(new String[]{
                baseUrl + "/issues/",
                "jql=project%3D10000%20AND%20%22affectedVersion%22%3D10001%20ORDER%20BY%20priority%20ASC"});
        tester.assertLinkPresentWithText("New Version 5");
        assertLinkPresent(new String[]{
                baseUrl + "/issues/",
                "jql=project%3D10000%20AND%20%22fixVersion%22%3D10000%20ORDER%20BY%20priority%20ASC"});
        assertUserProfileLink(item.getAttribute(ATT_ASSIGNEE), "dev");

        tester.assertLinkNotPresentWithText("HSP-10");
        tester.assertLinkPresentWithText("HSP-11");
        assertLinkPresentWithUrl(baseUrl + "/browse/HSP-11", true);
        tester.assertLinkPresentWithText("HSP-12");
        assertLinkPresentWithUrl(baseUrl + "/browse/HSP-12", true);

        assertSingleVersionPicker("New Version 5", "New+Version+5");
        assertVersionPicker("New Version 2", "New+Version+2");
        assertVersionPicker("New Version 4", "New+Version+4");
    }

    @Test
    public void testFullContentViewItem3DaysFormat() {
        reconfigureTimetracking(DAYS);
        testFullContentViewItem3();
    }

    @Test
    public void testFullContentViewItem3HoursFormat() {
        reconfigureTimetracking(HOURS);
        testFullContentViewItem3();
    }

    //special test for JRA-11613
    @Test
    @Restore("TestSearchRequestViewsAndIssueViewsWithPriorityNull.xml")
    public void testFullContentViewItem1NullPriority() {
        goToView("status=\"Resolved\"");

        assertions.getTextAssertions().assertTextNotPresent("Priority");
    }

    private void assertCustomField(final Item item, final String title, final String cfName) {
        final CustomField customField = item.getCustomFieldByName(cfName);
        if (customField == null) {
            assertions.getTextAssertions().assertTextNotPresent(title);
        } else {
            assertions.getTextAssertions().assertTextPresent(title);
        }
    }

    private void assertAttribute(final Item item, final String title, final String attName) {
        final String attribute = item.getAttribute(attName);
        if (attribute == null || attribute.length() == 0) {
            assertions.getTextAssertions().assertTextNotPresent(title);
        } else {
            assertions.getTextAssertions().assertTextPresent(title);
        }
    }

    private void assertProjectPickerField(final Item item) {
        final String title = "ProjectPickerField";
        final CustomField customField = item.getCustomFieldByName(CF_PROJECT_PICKER_FIELD);
        if (customField == null) {
            assertions.getTextAssertions().assertTextNotPresent(title);
        } else {
            assertions.getTextAssertions().assertTextSequence(tester.getDialog().getResponseText(), title, "homosapien");
            assertLinkPresent(new String[]{baseUrl + "/secure/BrowseProject.jspa", "id=10000"});
        }
    }

    private void assertCustomFieldValues(final Item item, final String title, final String cfName) {
        final CustomField customField = item.getCustomFieldByName(cfName);
        if (customField == null) {
            assertions.getTextAssertions().assertTextNotPresent(title);
        } else {
            final List<CustomField.Value> values = customField.getValues();
            if (values != null) {
                for (final CustomField.Value value : values) {
                    assertions.getTextAssertions().assertTextSequence(tester.getDialog().getResponseText(), title, value.getValue());
                }
            }
        }
    }

    private void verifyCommons(final Item item) {
        assertions.getTextAssertions().assertTextPresent(item.getAttribute(ATT_KEY));
        tester.assertLinkPresentWithText(item.getAttribute(ATT_SUMMARY));
        assertLinkPresentWithUrl(baseUrl + "/browse/" + item.getAttribute(ATT_KEY), false);

        final String responseText = tester.getDialog().getResponseText();
        assertions.getTextAssertions().assertTextSequence(responseText, "Created", "Updated");
        assertions.getTextAssertions().assertTextSequence(responseText, "Status", item.getAttribute(ATT_STATUS));
        assertions.getTextAssertions().assertTextSequence(responseText, "Project", "homosapien");
        assertions.getTextAssertions().assertTextSequence(responseText, "Type", item.getAttribute(ATT_TYPE));
        assertions.getTextAssertions().assertTextSequence(responseText, "Priority", item.getAttribute(ATT_PRIORITY));
        assertions.getTextAssertions().assertTextSequence(responseText, "Reporter", item.getAttribute(ATT_REPORTER));
        assertions.getTextAssertions().assertTextSequence(responseText, "Assignee", item.getAttribute(ATT_ASSIGNEE));
        assertions.getTextAssertions().assertTextSequence(responseText, "Resolution", item.getAttribute(ATT_RESOLUTION));

        tester.assertLinkPresentWithText("homosapien");
        assertLinkPresent(new String[]{baseUrl + "/secure/BrowseProject.jspa", "id=10000"});

        assertAttribute(item, "Description", ATT_DESCRIPTION);
        assertUserProfileLink(item.getAttribute(ATT_REPORTER), ADMIN_USERNAME);

        if (timeFormat == DAYS) {
            assertTimeUnknownIfNotSet("Remaining Estimate", item.getAttribute(ATT_REMAINING_ESTIMATE_DAYS));
            assertTimeUnknownIfNotSet("Time Spent", item.getAttribute(ATT_TIMESPENT_DAYS));
            assertTimeUnknownIfNotSet("Original Estimate", item.getAttribute(ATT_TIMEORIGINALESTIMATE_DAYS));
        } else if (timeFormat == HOURS) {
            assertTimeUnknownIfNotSet("Remaining Estimate", item.getAttribute(ATT_REMAINING_ESTIMATE_HOURS));
            assertTimeUnknownIfNotSet("Time Spent", item.getAttribute(ATT_TIMESPENT_HOURS));
            assertTimeUnknownIfNotSet("Original Estimate", item.getAttribute(ATT_TIMEORIGINALESTIMATE_HOURS));
        } else {
            assertTimeUnknownIfNotSet("Remaining Estimate", item.getAttribute(ATT_REMAINING_ESTIMATE));
            assertTimeUnknownIfNotSet("Time Spent", item.getAttribute(ATT_TIMESPENT));
            assertTimeUnknownIfNotSet("Original Estimate", item.getAttribute(ATT_TIMEORIGINALESTIMATE));
        }

        // components
        assertComponents(item);

        // comments
        assertComments(item);

        // attachments
        assertAttachments(item);

        // custom fields
        assertCustomField(item, "DateTimeField", CF_DATE_TIME_FIELD);
        assertCustomFieldValues(item, "MultiCheckboxesField", CF_MULTI_CHECKBOXES_FIELD);
        assertCustomFieldValues(item, "MultiSelectField", CF_MULTI_SELECT_FIELD);
//        assertCustomFieldValues(item, "NumberField", CF_NUMBER_FIELD);
        assertCustomFieldValues(item, "RadioButtonsField", CF_RADIO_BUTTONS_FIELD);
        assertCustomFieldValues(item, "SelectList", CF_SELECT_LIST);
        assertCustomFieldValues(item, "TextField255", CF_TEXT_FIELD255);
        assertCustomField(item, "DatePickerField", CF_DATE_PICKER_FIELD);
        assertCustomFieldValues(item, "FreeTextField", CF_FREE_TEXT_FIELD);

        assertProjectPickerField(item);

    }

    private void assertAttachments(final Item item) {
        final List<String> attachments = item.getAttachments();
        if (attachments.isEmpty()) {
            assertions.getTextAssertions().assertTextNotPresent("Attachments");
        } else {
            for (final String attachment : attachments) {
                assertions.getTextAssertions().assertTextSequence(tester.getDialog().getResponseText(), "Attachments", attachment);
            }
        }
    }

    private void assertComments(final Item item) {
        final List<Comment> comments = item.getComments();
        if (comments.isEmpty()) {
            assertions.getTextAssertions().assertTextNotPresent("Comments");
        } else {
            for (final Comment comment : comments) {
                assertions.getTextAssertions().assertTextSequence(tester.getDialog().getResponseText(), "Comments", comment.getValue());
            }
        }
    }

    private void assertComponents(final Item item) {
        final List<String> components = item.getComponents();
        if (components.isEmpty()) {
            assertions.getTextAssertions().assertTextSequence(tester.getDialog().getResponseText(), "Component/s", "None");
        } else {
            for (final String component : components) {
                assertions.getTextAssertions().assertTextSequence(tester.getDialog().getResponseText(), "Component/s", component);
            }
        }
    }

    private void assertTimeUnknownIfNotSet(final String title, final String attribute) {
        if (attribute == null) {
            assertions.getTextAssertions().assertTextSequence(tester.getDialog().getResponseText(), title, "Not Specified");
        } else {
            assertions.getTextAssertions().assertTextSequence(tester.getDialog().getResponseText(), title, attribute);
        }
    }

    private void assertUserProfileLink(final String title, final String user) {
        tester.assertLinkPresentWithText(title);
        assertLinkPresent(new String[]{baseUrl + "/secure/ViewProfile.jspa", "name=" + user});
    }

    private void assertSingleVersionPicker(final String title, final String value) {
        assertCustomFieldLink(title, value, "SingleVersionPickerField");
    }

    private void assertVersionPicker(final String title, final String value) {
        assertCustomFieldLink(title, value, "VersionPickerField");
    }

    private void assertCustomFieldLink(final String title, final String value, final String customFieldName) {
        tester.assertLinkPresentWithText(title);

        assertLinkPresent(new String[]{
                baseUrl + "/issues/",
                "jql=project%3D%22HSP%22",
                "%22" + customFieldName + "%22%3D%22" + value + "%22",
                "ORDER%20BY%20priority%20ASC"});
    }

    private void assertLinkPresentWithUrl(final String url, final boolean exactMatch) {
        if (url != null) {
            try {
                final WebLink[] links = tester.getDialog().getResponse().getLinks();
                for (final WebLink link : links) {
                    final String urlString = link.getURLString();
                    if (exactMatch) {
                        if (url.equals(urlString)) {
                            return;
                        }
                    } else {
                        if (urlString != null && urlString.indexOf(url) >= 0) {
                            return;
                        }
                    }
                }
                fail("Link '" + url + "' not found in response");
            } catch (final SAXException e) {
                e.printStackTrace();
            }
        }
    }

    private void assertLinkPresent(final String[] urlParts) {
        if (urlParts != null) {
            try {
                final WebLink[] links = tester.getDialog().getResponse().getLinks();
                assertTrue(
                        "Link with '" + Arrays.toString(urlParts) + "' not found in response. Existing: " +
                                Arrays.toString(Arrays.stream(tester.getDialog().getResponse().getLinks()).map(i -> i.getURLString()).toArray()),
                        anyLinkContainsAllParts(links, urlParts));
            } catch (final SAXException e) {
                // ignored
            }
        }
    }

    private boolean anyLinkContainsAllParts(final WebLink[] links, final String[] urlParts) {
        final boolean result = false;
        for (final WebLink link : links) {
            if (linkContainsAllParts(link.getURLString(), urlParts)) {
                return true;
            }
        }
        return result;
    }

    private boolean linkContainsAllParts(final String link, final String[] urlParts) {
        boolean result = true;
        if (link != null) {
            for (final String urlPart : urlParts) {
                if (link.indexOf(urlPart) < 0) {
                    result = false;
                }
            }
        }
        return result;
    }

    /**
     * Overriden to do nothing
     */
    protected void initFieldColumnMap() {
    }

    @Test
    public void testTimeTracking() throws SAXException {
        administration.subtasks().enable();
        subTaskify("HSP-12", "HSP-10");
        subTaskify("HSP-11", "HSP-10");

        navigation.issueNavigator().gotoNavigator();
        goToView("");

        assertions.getTextAssertions().assertTextSequence(tester.getDialog().getResponseText(), new String[]{
                " Remaining Estimate:", "1 day, 30 minutes", "Remaining Estimate:", "1 day",
                " Time Spent:", "3 hours, 20 minutes", "Time Spent:", "Not Specified",
                " Original Estimate:", "1 day", "Original Estimate:", "1 day"
        });
    }

}
