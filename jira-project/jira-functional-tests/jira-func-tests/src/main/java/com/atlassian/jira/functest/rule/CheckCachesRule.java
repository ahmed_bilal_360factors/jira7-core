package com.atlassian.jira.functest.rule;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.util.function.Supplier;

/**
 * Executes after tests to validate that no cache serialization errors occurred.
 *
 * @since v7.2.0
 */
public class CheckCachesRule implements TestRule {
    private final Supplier<Backdoor> backdoorSupplier;

    public CheckCachesRule(final Supplier<Backdoor> backdoorSupplier) {
        this.backdoorSupplier = backdoorSupplier;
    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                base.evaluate();

                boolean skip = false;
                if (description.getAnnotation(SkipCacheCheck.class) != null) {
                    skip = true;
                } else if (description.isTest() && description.getTestClass() != null && description.getTestClass().isAnnotationPresent(SkipCacheCheck.class)) {
                    skip = true;
                }

                if (!skip) {
                    checkCaches();
                }
            }
        };
    }

    private void checkCaches() {
        backdoorSupplier.get().cacheCheckControl().checkSerialization();
    }
}

