package com.atlassian.jira.webtests.ztests.projectconfig.framework;

import com.atlassian.jira.webtests.ztests.projectconfig.framework.rest.beans.SharedByData;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * @since v5.2
 */
public class SimpleWorkflowScheme {
    @JsonProperty
    private String name;

    @JsonProperty
    private String description;

    @JsonProperty
    private Long id;

    @JsonProperty
    private List<SimpleWorkflow> mappings;

    @JsonProperty
    private List<SimpleWorkflow> originalMappings;

    @JsonProperty
    private List<SimpleIssueType> issueTypes;

    @JsonProperty
    private SharedByData shared;

    @JsonProperty
    private boolean defaultScheme;

    @JsonProperty
    private boolean admin;

    @JsonProperty
    private boolean sysAdmin;

    @JsonProperty
    private int totalWorkflows;

    @JsonProperty
    private boolean draftScheme;

    @JsonProperty
    private String lastModifiedDate;

    @JsonProperty
    private SimpleUser lastModifiedUser;

    @JsonProperty
    private String currentUser;

    @JsonProperty
    private Long parentId;

    public SimpleWorkflowScheme() {
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public List<SimpleWorkflow> getMappings() {
        return mappings;
    }

    public void setMappings(final List<SimpleWorkflow> mappings) {
        this.mappings = mappings;
    }

    public List<SimpleWorkflow> getOriginalMappings() {
        return originalMappings;
    }

    public void setOriginalMappings(final List<SimpleWorkflow> originalMappings) {
        this.originalMappings = originalMappings;
    }

    public List<SimpleIssueType> getIssueTypes() {
        return issueTypes;
    }

    public void setIssueTypes(final List<SimpleIssueType> issueTypes) {
        this.issueTypes = issueTypes;
    }

    public SharedByData getShared() {
        return shared;
    }

    public void setShared(final SharedByData shared) {
        this.shared = shared;
    }

    public boolean isDefaultScheme() {
        return defaultScheme;
    }

    public void setDefaultScheme(final boolean defaultScheme) {
        this.defaultScheme = defaultScheme;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(final boolean admin) {
        this.admin = admin;
    }

    public boolean isSysAdmin() {
        return sysAdmin;
    }

    public void setSysAdmin(final boolean sysAdmin) {
        this.sysAdmin = sysAdmin;
    }

    public int getTotalWorkflows() {
        return totalWorkflows;
    }

    public void setTotalWorkflows(final int totalWorkflows) {
        this.totalWorkflows = totalWorkflows;
    }

    public boolean isDraftScheme() {
        return draftScheme;
    }

    public void setDraftScheme(final boolean draftScheme) {
        this.draftScheme = draftScheme;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(final String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public SimpleUser getLastModifiedUser() {
        return lastModifiedUser;
    }

    public void setLastModifiedUser(final SimpleUser lastModifiedUser) {
        this.lastModifiedUser = lastModifiedUser;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(final String currentUser) {
        this.currentUser = currentUser;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(final Long parentId) {
        this.parentId = parentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SimpleWorkflowScheme that = (SimpleWorkflowScheme) o;

        if (admin != that.admin) {
            return false;
        }
        if (sysAdmin != that.sysAdmin) {
            return false;
        }
        if (defaultScheme != that.defaultScheme) {
            return false;
        }
        if (draftScheme != that.draftScheme) {
            return false;
        }
        if (totalWorkflows != that.totalWorkflows) {
            return false;
        }
        if (currentUser != null ? !currentUser.equals(that.currentUser) : that.currentUser != null) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (issueTypes != null ? !issueTypes.equals(that.issueTypes) : that.issueTypes != null) {
            return false;
        }
        if (lastModifiedDate != null ? !lastModifiedDate.equals(that.lastModifiedDate) : that.lastModifiedDate != null) {
            return false;
        }
        if (lastModifiedUser != null ? !lastModifiedUser.equals(that.lastModifiedUser) : that.lastModifiedUser != null) {
            return false;
        }
        if (mappings != null ? !mappings.equals(that.mappings) : that.mappings != null) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (originalMappings != null ? !originalMappings.equals(that.originalMappings) : that.originalMappings != null) {
            return false;
        }
        if (shared != null ? !shared.equals(that.shared) : that.shared != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (mappings != null ? mappings.hashCode() : 0);
        result = 31 * result + (originalMappings != null ? originalMappings.hashCode() : 0);
        result = 31 * result + (issueTypes != null ? issueTypes.hashCode() : 0);
        result = 31 * result + (shared != null ? shared.hashCode() : 0);
        result = 31 * result + (defaultScheme ? 1 : 0);
        result = 31 * result + (admin ? 1 : 0);
        result = 31 * result + (sysAdmin ? 1 : 0);
        result = 31 * result + totalWorkflows;
        result = 31 * result + (draftScheme ? 1 : 0);
        result = 31 * result + (lastModifiedDate != null ? lastModifiedDate.hashCode() : 0);
        result = 31 * result + (lastModifiedUser != null ? lastModifiedUser.hashCode() : 0);
        result = 31 * result + (currentUser != null ? currentUser.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
