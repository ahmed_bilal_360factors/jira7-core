package com.atlassian.jira.webtests.ztests.tpm.ldap;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.meterware.httpunit.WebLink;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * @since v4.2
 */
@WebTest({Category.FUNC_TEST, Category.LDAP, Category.TPM})
@RestoreBlankInstance
@LoginAs(user = ADMIN_USERNAME)
public class TestDelegatingLdapDirectoryMaintenance extends BaseJiraFuncTest {
    @Inject
    protected Administration administration;
    @Inject
    protected FuncTestLogger logger;
    @Inject
    protected Assertions assertions;
    @Inject
    protected LdapUtil ldapUtil;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testAddDelegatingLdapDirectory() {
        navigation.gotoAdmin();
        addDirectory();

        // assert we are back on the "View Directories" page.
        textAssertions.assertTextPresent("The table below shows the user directories currently configured for JIRA.");
        // assert the new directory is added at the end
        UserDirectoryTable userDirectoryTable = new UserDirectoryTable(this, logger, assertions);
        userDirectoryTable.assertRow(1).contains("1", "JIRA Internal Directory", "Internal").hasMoveUp(false).hasMoveDown(true).hasOnlyEditOperation();
        userDirectoryTable.assertRow(2).contains("10000", "First Delegating Ldap", ldapUtil.getTypeDisplayName()).hasMoveUp(true).hasMoveDown(false).hasDisableEditOperations();
    }

    private void addDirectory() {
        // We go directly ot the add page, because we need JavaScript to drive the normal add UI.
        navigation.gotoPage("/plugins/servlet/embedded-crowd/configure/delegatingldap/");
        textAssertions.assertTextPresent(new IdLocator(tester, "embcwd"), "Server Settings");

        // Test for all mandatory fields
        tester.setWorkingForm("configure-delegating-ldap-form");
        tester.setFormElement("name", "");
        tester.setFormElement("port", "");
        tester.submit("test");

        // Check for the errors
        textAssertions.assertTextPresent("Name is a required field.");
        textAssertions.assertTextPresent("Hostname is a required field.");
        textAssertions.assertTextPresent("Port is a required field.");
// JRA-23819
//        text.assertTextPresent("Username is a required field.");
//        text.assertTextPresent("Password is a required field.");
        textAssertions.assertTextPresent("User name attribute is a required field");

        tester.setWorkingForm("configure-delegating-ldap-form");
        tester.setFormElement("name", "First Delegating Ldap");
        tester.selectOption("type", ldapUtil.getTypeDisplayName());
        tester.setFormElement("hostname", ldapUtil.getLdapServer());
        // For this test we can use AD read-only (no SSL)
        tester.setFormElement("port", "389");
        tester.setFormElement("ldapUserdn", ldapUtil.getUserDn());
        tester.setFormElement("ldapPassword", ldapUtil.getPassword());
        tester.setFormElement("ldapBasedn", ldapUtil.getBaseDn());
        tester.setFormElement("ldapUserUsername", "cn");
        tester.submit("test");

        tester.submit("save");
    }

    @Test
    public void testEditDelegatingLdapDirectory() {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_DIRECTORIES);
        // assert we are back on the "View Directories" page.
        textAssertions.assertTextPresent("The table below shows the user directories currently configured for JIRA.");

        addDirectory();

        UserDirectoryTable userDirectoryTable = new UserDirectoryTable(this, logger, assertions);
        WebLink link = userDirectoryTable.getTableCell(2, 4).getLinkWith("edit");
        navigation.clickLink(link);

        textAssertions.assertTextPresent(new IdLocator(tester, "embcwd"), "Server Settings");

        // Change the contents and save
        tester.setFormElement("name", "First Delegating Ldap X");
        tester.setFormElement("hostname", ldapUtil.getLdapServer());
        tester.setFormElement("ldapPassword", ldapUtil.getPassword());
        tester.submit("test");

        tester.submit("save");

        // assert we are back on the "View Directories" page.
        textAssertions.assertTextPresent("The table below shows the user directories currently configured for JIRA.");
        // assert the new directory is added at the end
        userDirectoryTable = new UserDirectoryTable(this, logger, assertions);
        userDirectoryTable.assertRow(1).contains("1", "JIRA Internal Directory", "Internal").hasMoveUp(false).hasMoveDown(true).hasOnlyEditOperation();
        userDirectoryTable.assertRow(2).contains("10000", "First Delegating Ldap X", ldapUtil.getTypeDisplayName()).hasMoveUp(true).hasMoveDown(false).hasDisableEditOperations();

        // Check for mandatory fields
        link = userDirectoryTable.getTableCell(2, 4).getLinkWith("edit");
        navigation.clickLink(link);

        textAssertions.assertTextPresent(new IdLocator(tester, "embcwd"), "Server Settings");

        // Change the contents and save
        tester.setFormElement("name", "");
        tester.setFormElement("hostname", "");
        tester.setFormElement("port", "");
        tester.setFormElement("ldapUserdn", "");
        tester.setFormElement("ldapPassword", "");
        tester.setFormElement("ldapBasedn", "");
        tester.setFormElement("ldapUserUsername", "");
        tester.submit("test");

        // Check for the errors
        textAssertions.assertTextPresent("Name is a required field.");
        textAssertions.assertTextPresent("Hostname is a required field.");
        textAssertions.assertTextPresent("Port is a required field.");
// JRA-23819
//        text.assertTextPresent("Username is a required field.");
//        text.assertTextPresent("Password is a required field.");
        textAssertions.assertTextPresent("Base DN is a required field for this directory type.");
        textAssertions.assertTextPresent("User name attribute is a required field");

        tester.clickLink("configure-delegating-ldap-form-cancel");

        // assert we are back on the "View Directories" page.
        textAssertions.assertTextPresent("The table below shows the user directories currently configured for JIRA.");
        // assert the directory is unchanged
        userDirectoryTable = new UserDirectoryTable(this, logger, assertions);
        userDirectoryTable.assertRow(1).contains("1", "JIRA Internal Directory", "Internal").hasMoveUp(false).hasMoveDown(true).hasOnlyEditOperation();
        userDirectoryTable.assertRow(2).contains("10000", "First Delegating Ldap X", ldapUtil.getTypeDisplayName()).hasMoveUp(true).hasMoveDown(false).hasDisableEditOperations();
    }

    @Test
    public void testDeleteDelegatingLdapDirectory() {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_DIRECTORIES);
        textAssertions.assertTextPresent("The table below shows the user directories currently configured for JIRA.");

        addDirectory();

        // Disable the second directory
        UserDirectoryTable userDirectoryTable = new UserDirectoryTable(this, logger, assertions);
        WebLink link = userDirectoryTable.getTableCell(2, 4).getLinkWith("Disable");
        navigation.clickLink(link);

        userDirectoryTable = new UserDirectoryTable(this, logger, assertions);
        userDirectoryTable.assertRow(1).contains("1", "JIRA Internal Directory", "Internal").hasMoveUp(false).hasMoveDown(true).hasOnlyEditOperation();
        userDirectoryTable.assertRow(2).contains("10000", "First Delegating Ldap (inactive)", ldapUtil.getTypeDisplayName()).hasMoveUp(true).hasMoveDown(false).hasEnableEditRemoveOperations();

        // Now Delete the directory
        link = userDirectoryTable.getTableCell(2, 4).getLinkWith("Remove");
        navigation.clickLink(link);

        // assert we are back on the "View Directories" page.
        textAssertions.assertTextPresent("The table below shows the user directories currently configured for JIRA.");
        // assert the new directory is added at the end
        userDirectoryTable = new UserDirectoryTable(this, logger, assertions);
        userDirectoryTable.assertRow(1).contains("1", "JIRA Internal Directory", "Internal").hasMoveUp(false).hasMoveDown(false).hasOnlyEditOperation();
        textAssertions.assertTextNotPresent(new IdLocator(tester, "embcwd"), "First Delegating Ldap");

    }

}