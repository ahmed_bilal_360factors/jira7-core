package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FuncTestRestClient;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.inject.Inject;
import com.meterware.httpunit.WebResponse;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Testing for the a 400 response for bad Json string
 *
 * @since v5.0
 */
@WebTest({Category.FUNC_TEST, Category.REST})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestBadJson extends BaseJiraFuncTest {

    @Inject
    FuncTestRestClient client;

    private String badJson = "{ fields: { \"project\" : { \"key\" : \"TST\" } }}";

    @Test
    public void testBadJsonGives400() throws Exception {
        WebResponse response = client.POST("/rest/api/2/issue", badJson);
        assertEquals(400, response.getResponseCode());
        assertTrue(response.getText().contains("Unexpected character ('f'"));
    }
}
