package com.atlassian.jira.webtests.ztests.about;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.BROWSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestAboutPage extends BaseJiraFuncTest {

    @Test
    public void test500PageServiceParamVisibility() {
        tester.gotoPage("/secure/AboutPage.jspa");

        tester.assertElementPresent("test-about-introduction");
        tester.assertElementPresent("test-about-conclusion");
        tester.assertTextPresent("Atlassian JIRA - Plugins - DevMode - Func Test Plugin");
        tester.assertTextPresent("test-library-ignore");
        tester.assertLinkPresentWithText("thisIsAnExampleIgnoreIt");
        tester.assertTextNotPresent("another-test-library-ignore");
    }
}
