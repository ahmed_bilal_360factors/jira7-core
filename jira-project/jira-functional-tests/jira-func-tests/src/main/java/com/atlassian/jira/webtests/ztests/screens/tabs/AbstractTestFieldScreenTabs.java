package com.atlassian.jira.webtests.ztests.screens.tabs;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.admin.TimeTracking;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Node;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.fields.EditFieldConstants.DUEDATE;
import static com.atlassian.jira.functest.framework.fields.EditFieldConstants.REPORTER;
import static com.atlassian.jira.functest.framework.fields.EditFieldConstants.TIMETRACKING;
import static com.atlassian.jira.functest.framework.fields.EditFieldConstants.TIMETRACKING_ORIGINALESTIMATE;
import static com.atlassian.jira.functest.framework.fields.EditFieldConstants.TIMETRACKING_REMAININGESTIMATE;
import static com.atlassian.jira.functest.framework.fields.EditFieldConstants.WORKLOG_ACTIVATE;
import static com.atlassian.jira.functest.framework.fields.EditFieldConstants.WORKLOG_TIMELOGGED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the functionality of Screen Tabs on an arbitrary "editing" screen.
 *
 * @since v4.2
 */
@Ignore("This is an abstract class and should not be instantiated.")
public abstract class AbstractTestFieldScreenTabs extends BaseJiraFuncTest {
    private static final String CLASS_HAS_ERRORS = "has-errors";

    private static final String ORIGINAL_ESTIMATE_ERROR_MESSAGE = "The original estimate specified is not valid.";
    private static final String REMAINING_ESTIMATE_ERROR_MESSAGE = "The remaining estimate specified is not valid.";
    private static final String TIME_LOGGED_ERROR_MESSAGE = "Invalid time duration entered.";

    @Inject
    private TimeTracking timeTracking;
    @Inject
    private HtmlPage page;
    @Inject
    private TextAssertions textAssertions;

    /**
     * Navigate to the screen where editing of fields can take place for an issue with no work logged on it yet.
     */
    protected abstract void gotoTabScreenForIssueWithNoWork();

    /**
     * Navigate to the screen where editing of fields can take place for an issue with some work logged on it.
     *
     * @throws UnsupportedOperationException if {@link #canShowScreenForIssueWithWork()} returns <code>false</code>.
     */
    protected abstract void gotoTabScreenForIssueWithWorkStarted();

    /**
     * @return true if it makes sense to get to a screen where we are editing fields for an issue with some work logged
     * on it i.e. any time except Creating an issue.
     */
    protected boolean canShowScreenForIssueWithWork() {
        return true;
    }

    /**
     * @return the names of the fields to be displayed in the first tab on the screen.
     */
    protected String[] getFieldsInFirstTab() {
        return new String[]{"Summary", "Issue Type", "Priority"};
    }

    @Before
    public void setUpDataAndDisableJsReporterField() {
        backdoor.darkFeatures().enableForSite("jira.no.frother.reporter.field");
    }

    @After
    public void restoreJsReporterFieldSetting() {
        backdoor.darkFeatures().disableForSite("jira.no.frother.reporter.field");
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testErrorsForTimeTrackingAndWorklog() throws Exception {
        navigation.gotoDashboard();
        // worklog and timetracking errors - work not started
        testErrorsForTimeTrackingAndWorklog_LegacyModeWorkNotStarted();

        // worklog and timetracking errors - work started
        // only makes sense if we can get to this screen for an issue with work started
        if (canShowScreenForIssueWithWork()) {
            testErrorsForTimeTrackingAndWorklog_LegacyModeWorkStarted();
        }

        // tests in modern mode
        testErrorsForTimeTrackingAndWorklog_ModernMode();
    }

    private void testErrorsForTimeTrackingAndWorklog_LegacyModeWorkNotStarted() {
        gotoTabScreenForIssueWithNoWork();
        tester.setFormElement(TIMETRACKING, "xxx");
        tester.setFormElement(WORKLOG_TIMELOGGED, "5m");
        tester.submit();
        assertErrorAppearsOnTimeTrackingTab(ORIGINAL_ESTIMATE_ERROR_MESSAGE);

        tester.setFormElement(TIMETRACKING, "5h");
        tester.setFormElement(WORKLOG_TIMELOGGED, "yyy");
        tester.submit();
        assertErrorAppearsOnWorkLogTab(TIME_LOGGED_ERROR_MESSAGE);
    }

    private void testErrorsForTimeTrackingAndWorklog_LegacyModeWorkStarted() {
        gotoTabScreenForIssueWithWorkStarted();
        tester.setFormElement(TIMETRACKING, "xxx");
        tester.setFormElement(WORKLOG_TIMELOGGED, "5m");
        tester.submit();
        assertErrorAppearsOnWorkLogTab(REMAINING_ESTIMATE_ERROR_MESSAGE);

        tester.setFormElement(TIMETRACKING, "5h");
        tester.setFormElement(WORKLOG_TIMELOGGED, "yyy");
        tester.checkCheckbox(WORKLOG_ACTIVATE, "true");
        tester.submit();
        assertErrorAppearsOnWorkLogTab(TIME_LOGGED_ERROR_MESSAGE);
    }

    private void testErrorsForTimeTrackingAndWorklog_ModernMode() {
        enableModernMode();
        gotoTabScreenForIssueWithNoWork();
        tester.setFormElement(TIMETRACKING_ORIGINALESTIMATE, "xxx");
        tester.setFormElement(TIMETRACKING_REMAININGESTIMATE, "4m");
        tester.setFormElement(WORKLOG_TIMELOGGED, "5m");
        tester.submit();
        assertErrorAppearsOnTimeTrackingTab(ORIGINAL_ESTIMATE_ERROR_MESSAGE);

        tester.setFormElement(TIMETRACKING_ORIGINALESTIMATE, "5h");
        tester.setFormElement(TIMETRACKING_REMAININGESTIMATE, "zzz");
        tester.setFormElement(WORKLOG_TIMELOGGED, "2h");
        tester.submit();
        assertErrorAppearsOnWorkLogTab(REMAINING_ESTIMATE_ERROR_MESSAGE);

        tester.checkCheckbox(WORKLOG_ACTIVATE, "true");
        tester.setFormElement(TIMETRACKING_ORIGINALESTIMATE, "5h");
        tester.setFormElement(TIMETRACKING_REMAININGESTIMATE, "15m");
        tester.setFormElement(WORKLOG_TIMELOGGED, "xxx");
        tester.submit();
        assertErrorAppearsOnWorkLogTab(TIME_LOGGED_ERROR_MESSAGE);
    }

    private void assertErrorAppearsOnWorkLogTab(final String errorMessage) {    // create errors for time tracking in legacy mode
        assertTabIsSelected("Tab 2");
        assertTabsHaveErrors("Tab 2");
        textAssertions.assertTextPresent(new IdLocator(tester, "tab2"), errorMessage);
    }

    private void assertErrorAppearsOnTimeTrackingTab(final String errorMessage) {
        // create errors for time tracking in legacy mode
        assertTabIsSelected("Tab 4");
        assertTabsHaveErrors("Tab 4");
        textAssertions.assertTextPresent(new IdLocator(tester, "tab4"), errorMessage);
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testErrorsOnMultipleTabs() throws Exception {
        navigation.gotoDashboard();
        // create errors for Tabs 2, 3 and 4 and submit
        gotoTabScreenForIssueWithNoWork();
        tester.setFormElement(DUEDATE, "xxx");
        tester.setFormElement(REPORTER, "yyy");
        tester.setFormElement(TIMETRACKING, "zzz");
        tester.submit();

        // assert Tabs 2, 3 and 4 indicate errors
        assertTabsDontHaveErrors("Field Tab");
        assertTabsHaveErrors("Tab 2", "Tab 3", "Tab 4");

        // assert Tab 2 is the selected tab as it is the first tab with errors
        assertTabIsSelected("Tab 2");

        // remove error from Tab 2
        tester.setFormElement(DUEDATE, "");
        tester.submit();

        // now Tabs 3 and 4 indicate errors and Tab 3 is selected
        assertTabsDontHaveErrors("Field Tab", "Tab 2");
        assertTabsHaveErrors("Tab 3", "Tab 4");
        assertTabIsSelected("Tab 3");
    }

    @Test
    @LoginAs(user = ADMIN_USERNAME)
    public void testPresentationOfTabs() throws Exception {
        navigation.gotoDashboard();
        // time tracking is in legacy mode - therefore Original and Remaining Estimate don't ever appear together
        gotoTabScreenForIssueWithNoWork();
        assertFieldsInTabsForLegacyModeWorkNotStarted();

        // enable modern mode for time tracking - therefore Original and Remaining Estimate can appear together
        enableModernMode();
        gotoTabScreenForIssueWithNoWork();
        assertFieldsInTabsForModernMode();

        if (canShowScreenForIssueWithWork()) {
            enableLegacyMode();
            gotoTabScreenForIssueWithWorkStarted();
            assertFieldsInTabsForLegacyModeWorkStarted();

            enableModernMode();
            gotoTabScreenForIssueWithWorkStarted();
            assertFieldsInTabsForModernMode();
        }
    }

    private void assertFieldsInTabsForLegacyModeWorkNotStarted() {
        assertFieldsInTab("tab1", getFieldsInFirstTab());
        assertFieldsInTab("tab2", "Time Spent", "Date Started", "Remaining Estimate", "Affects Version/s", "Component/s", "Due Date");
        assertFieldsInTab("tab3", "Environment", "Reporter", "Assignee", "Fix Version/s");
        assertFieldsInTab("tab4", "Labels", "Original Estimate", "Description");
    }

    private void assertFieldsInTabsForLegacyModeWorkStarted() {
        assertFieldsInTab("tab1", getFieldsInFirstTab());
        assertFieldsInTab("tab2", "Log work", "Remaining Estimate", "Affects Version/s", "Component/s", "Due Date");
        assertFieldsInTab("tab3", "Environment", "Reporter", "Assignee", "Fix Version/s");
        assertFieldsInTab("tab4", "Labels", "Description");
    }

    private void assertFieldsInTabsForModernMode() {
        assertFieldsInTab("tab1", getFieldsInFirstTab());
        assertFieldsInTab("tab2", "Log work", "Remaining Estimate", "Affects Version/s", "Component/s", "Due Date");
        assertFieldsInTab("tab3", "Environment", "Reporter", "Assignee", "Fix Version/s");
        assertFieldsInTab("tab4", "Labels", "Original Estimate", "Description");
    }

    private void enableModernMode() {
        timeTracking.disable();
        timeTracking.enable(TimeTracking.Mode.MODERN);
    }

    private void enableLegacyMode() {
        timeTracking.disable();
        timeTracking.enable(TimeTracking.Mode.LEGACY);
    }

    private void assertTabsDontHaveErrors(final String... tabNames) {
        for (final String tabName : tabNames) {
            assertFalse("Tab with name '" + tabName + "' had error class", page.getLinksWithExactText(tabName)[0].getClassName().contains(CLASS_HAS_ERRORS));
        }
    }

    private void assertTabsHaveErrors(final String... tabNames) {
        for (final String tabName : tabNames) {
            assertEquals("Tab with name '" + tabName + "' did not have error class", CLASS_HAS_ERRORS, page.getLinksWithExactText(tabName)[0].getClassName());
        }
    }

    private void assertTabIsSelected(final String tabName) {
        final XPathLocator locator = new XPathLocator(tester, String.format("//li[@class='menu-item active-tab']/a/strong[contains(text(), '%s')]", tabName));
        final Node[] nodes = locator.getNodes();
        assertTrue("Could not find tab with name '" + tabName + "' that was selected.", nodes != null && nodes.length == 1);
    }

    private void assertFieldsInTab(final String tabId, final String... fields) {
        final IdLocator tabContainerLocator = new IdLocator(tester, tabId);
        textAssertions.assertTextSequence(tabContainerLocator, fields);
    }
}
