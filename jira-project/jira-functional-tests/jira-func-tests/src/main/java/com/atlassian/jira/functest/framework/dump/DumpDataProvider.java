package com.atlassian.jira.functest.framework.dump;

import com.atlassian.jira.functest.framework.FuncTestWebClientListener;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;

/**
 * Provide all the data/objects that is required to perform dump of html in func tests.
 *
 * @since v6.5
 */
public interface DumpDataProvider {
    JIRAEnvironmentData getEnvironmentData();

    FuncTestWebClientListener getFuncTestWebClientListener();

    String getTestName();

    WebTester getWebTester();
}
