package com.atlassian.jira.webtests.ztests.dashboard.reports;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.TableLocator;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.REPORTS})
@LoginAs(user = ADMIN_USERNAME)
public class TestPieChartReport extends BaseJiraFuncTest {
    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreData("TestPieChartWithCustomField.xml");
    }

    @Test
    public void testPieChartXSS() throws Exception {
        administration.backdoor().usersAndGroups().addUser("testuser", "testuser", "\"><img src=x onerror=alert(1)>", "testuser@example.com");
        navigation.gotoPage("secure/ConfigureReport.jspa?projectOrFilterId=project-10000&statistictype=assignees&selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin%3Apie-report&Next=Next");
    }

    @Test
    public void testPieChartReportWithGroupCustomField() {
        navigation.gotoPage("/secure/ConfigureReport.jspa?projectOrFilterId=project-10000&statistictype=customfield_10010&selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin%3Apie-report&Next=Next");

        textAssertions.assertTextPresent(new WebPageLocator(tester), "Pie Chart Report");
        textAssertions.assertTextSequence(new TableLocator(tester, "singlefieldpie-report-datatable"), "None", "4", "100%");
    }

    @Test
    public void testPieChartReportWithMultiSelectCustomField() {
        navigation.gotoPage("/secure/ConfigureReport.jspa?projectOrFilterId=project-10000&statistictype=customfield_10011&selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin%3Apie-report&Next=Next");

        textAssertions.assertTextPresent(new WebPageLocator(tester), "Pie Chart Report");
        textAssertions.assertTextSequence(new TableLocator(tester, "singlefieldpie-report-datatable"), "None", "4", "100%");
    }

    //JRA-19121
    @Test
    public void testPieChartReportWithProjectCustomField() {
        navigation.gotoPage("/secure/ConfigureReport.jspa?projectOrFilterId=project-10000&statistictype=customfield_10012&selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin%3Apie-report&Next=Next");

        textAssertions.assertTextPresent(new WebPageLocator(tester), "Pie Chart Report");
        textAssertions.assertTextSequence(new TableLocator(tester, "singlefieldpie-report-datatable"), "None", "4", "100%");
    }

    @Test
    public void testPieChartReportWithSelectCustomField() {
        navigation.gotoPage("/secure/ConfigureReport.jspa?projectOrFilterId=project-10000&statistictype=customfield_10013&selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin%3Apie-report&Next=Next");

        textAssertions.assertTextPresent(new WebPageLocator(tester), "Pie Chart Report");
        textAssertions.assertTextSequence(new TableLocator(tester, "singlefieldpie-report-datatable"), "None", "4", "100%");
    }

    @Test
    public void testPieChartReportWithUserCustomField() {
        navigation.gotoPage("/secure/ConfigureReport.jspa?projectOrFilterId=project-10000&statistictype=customfield_10014&selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin%3Apie-report&Next=Next");

        textAssertions.assertTextPresent(new WebPageLocator(tester), "Pie Chart Report");
        textAssertions.assertTextSequence(new TableLocator(tester, "singlefieldpie-report-datatable"), "None", "4", "100%");
    }

    //JRA-19118
    @Test
    public void testPieChartReportWithVersionCustomField() {
        navigation.gotoPage("/secure/ConfigureReport.jspa?projectOrFilterId=project-10000&statistictype=customfield_10000&selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin%3Apie-report&Next=Next");

        textAssertions.assertTextPresent(new WebPageLocator(tester), "Pie Chart Report");
        textAssertions.assertTextSequence(new TableLocator(tester, "singlefieldpie-report-datatable"), "None", "4", "100%");
    }

    @Test
    public void testSidebarIsPresentWhenThereIsProjectContext() {
        navigation.gotoPage("/secure/ConfigureReport.jspa?projectOrFilterId=project-10000&statistictype=customfield_10000&selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin%3Apie-report&Next=Next");
        textAssertions.assertTextPresent(new WebPageLocator(tester), "Pie Chart Report");
        tester.assertLinkPresentWithText("Issues");
        tester.assertLinkPresentWithText("Reports");
        tester.assertLinkPresentWithText("Versions");
        tester.assertLinkPresentWithText("Components");
    }

    @Test
    public void testSidebarIsNotPresentWhenThereIsNoProjectContext() {
        navigation.logout();
        navigation.gotoPage("/secure/ConfigureReport.jspa?projectOrFilterId=project-10000&statistictype=customfield_10000&selectedProjectId=10000&reportKey=com.atlassian.jira.jira-core-reports-plugin%3Apie-report&Next=Next");
        textAssertions.assertTextPresent(new WebPageLocator(tester), "Pie Chart Report");
        tester.assertLinkNotPresentWithText("Issues");
        tester.assertLinkNotPresentWithText("Reports");
        tester.assertLinkNotPresentWithText("Versions");
        tester.assertLinkNotPresentWithText("Components");
    }
}
