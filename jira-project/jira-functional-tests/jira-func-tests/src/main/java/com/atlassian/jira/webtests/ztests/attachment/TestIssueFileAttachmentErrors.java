package com.atlassian.jira.webtests.ztests.attachment;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HttpUnitConfiguration;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.page.Error404;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * @since v3.13.5
 */
@WebTest({Category.FUNC_TEST, Category.ATTACHMENTS})
@HttpUnitConfiguration(throwOnErrorStatus = false, enableScripting = false)
@Restore("TestDeleteAttachments.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestIssueFileAttachmentErrors extends BaseJiraFuncTest {

    @Inject
    private Administration administration;
    @Inject
    private Assertions assertions;

    @Before
    public void setUpTest() {
        administration.attachments().enable();
    }

    @Test
    public void testViewAttachmentWithNonNumericId() {
        assertThat(new Error404(tester).visit("/secure/attachment/idontparse/DummyAttachment"), Error404.isOn404Page());
    }

    @Test
    public void testViewAttachmentWithNoPath() {
        assertErrorResponse("/secure/attachment/", 400, "Invalid attachment path");
    }

    @Test
    public void testTryDownloadAsZipWhenZipDisabled() throws Exception {
        administration.attachments().disableZipSupport();
        assertThat(new Error404(tester).visit("/secure/attachmentzip/10000.zip"), Error404.isOn404Page());
    }

    private void assertErrorResponse(final String url, final int errorCode, final String message) {
        tester.beginAt(url);
        assertEquals(errorCode, tester.getDialog().getResponse().getResponseCode());
        assertions.html().assertResponseContains(tester, message);
    }
}
