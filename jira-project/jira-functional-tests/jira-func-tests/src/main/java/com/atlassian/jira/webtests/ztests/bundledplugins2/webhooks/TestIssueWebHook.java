package com.atlassian.jira.webtests.ztests.bundledplugins2.webhooks;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.IssueUpdateRequest;
import com.atlassian.jira.testkit.client.restclient.Comment;
import com.atlassian.jira.testkit.client.restclient.CommentClient;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.testkit.client.util.JsonMatchers.hasField;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.WEBHOOKS, Category.REFERENCE_PLUGIN})
public class TestIssueWebHook extends AbstractWebHookTest {
    public static final String ISSUE_UPDATED = "jira:issue_updated";

    private String issueKey;
    private CommentClient commentClient;

    @Before
    @Override
    public void setUpTest() {
        super.setUpTest();
        backdoor.restoreBlankInstance();
        commentClient = new CommentClient(environmentData);
        issueKey = backdoor.issues().createIssue("HSP", "summary").key;
    }

    @Test
    public void testIssueUpdatedContainsIssueEventTypeWhenUpdatingIssue() throws Exception {
        registerWebHook(ISSUE_UPDATED);

        backdoor.issues().addLabel(issueKey, "label");

        final WebHookResponseData responseData = getWebHookResponse();
        assertThat(responseData, notNullValue());

        assertThat(responseData.asJsonObject().getString("issue_event_type_name"), equalTo("issue_updated"));
    }

    @Test
    public void testIssueUpdatedFiresOnCommentCreate() throws Exception {
        registerWebHook(ISSUE_UPDATED);

        String expected = "my-comment";
        backdoor.issues().commentIssue(issueKey, expected);

        checkResponseForWebHook(ISSUE_UPDATED, expected);
    }

    @Test
    public void testIssueUpdatedFiresOnCommentEdit() throws Exception {
        String expected = "my-comment";
        Response<Comment> commentResponse = backdoor.issues().commentIssue(issueKey, expected);

        registerWebHook(ISSUE_UPDATED);

        commentClient.post(issueKey, commentResponse.body);

        checkResponseForWebHook(ISSUE_UPDATED, expected);
    }

    @Test
    public void testIssueUpdatedFiresOnCommentDelete() throws Exception {
        Response<Comment> commentResponse = backdoor.issues().commentIssue(issueKey, "my-comment");

        registerWebHook(ISSUE_UPDATED);

        commentClient.delete(issueKey, commentResponse.body);

        checkResponseForWebHook(ISSUE_UPDATED, null);
    }

    @Test
    public void testIssueUpdatedFiresWhenUpdateIsWithoutNotifyingUsers() throws Exception {
        registerWebHook(ISSUE_UPDATED);

        IssueUpdateRequest update = new IssueUpdateRequest().fields(new IssueFields().summary("new summary"));
        new IssueClient(getEnvironmentData()).updateResponse(issueKey, update, false);

        WebHookResponseData responseData = getWebHookResponse();

        assertThat(responseData, notNullValue());
        assertThat(responseData.asJsonObject().getString("issue_event_type_name"), equalTo("issue_updated"));
    }


    private void checkResponseForWebHook(final String eventName, final String comment) throws Exception {
        final WebHookResponseData responseData = getWebHookResponse();
        assertThat(responseTester.getResponseData(), nullValue());
        assertThat(responseData, notNullValue());

        checkReturnedJson(eventName, comment, new JSONObject(responseData.getJson()));
    }

    private void checkReturnedJson(final String eventName, final String commentString, final JSONObject event)
            throws JSONException {
        assertThat(event, hasField("webhookEvent").equalTo(eventName));

        JSONObject comment = event.optJSONObject("comment");
        if (commentString != null) {
            assertThat(comment.getString("body"), equalTo(commentString));
        } else {
            assertNull(comment);
        }
    }
}
