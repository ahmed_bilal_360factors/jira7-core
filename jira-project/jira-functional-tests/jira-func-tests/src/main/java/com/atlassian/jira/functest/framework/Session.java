package com.atlassian.jira.functest.framework;

/**
 *
 * @since v7.2
 */
public interface Session {
    void withSession(Runnable runnable);
}
