package com.atlassian.jira.functest.framework.sharing;

import com.atlassian.jira.functest.framework.util.json.TestJSONException;
import com.atlassian.jira.functest.framework.util.json.TestJSONObject;

/**
 * Represents an authenticated user share in the Func tests.
 *
 */
public class AuthenticatedTestSharingPermission implements TestSharingPermission {
    public static final AuthenticatedTestSharingPermission AUTHENTICATED_USER_PERMISSION = new AuthenticatedTestSharingPermission();
    public static final String TYPE = "loggedin";

    public String toString() {
        return "Authenticated User Share";
    }

    public TestJSONObject toJson() {
        try {
            TestJSONObject object = new TestJSONObject();
            object.put(JSONConstants.TYPE_KEY, TYPE);
            return object;
        } catch (TestJSONException e) {
            throw new RuntimeException(e);
        }
    }

    public static AuthenticatedTestSharingPermission parseJson(TestJSONObject json) throws TestJSONException {
        if (json.has(JSONConstants.TYPE_KEY)) {
            String type = (String) json.get(JSONConstants.TYPE_KEY);
            if (TYPE.equals(type)) {
                return AUTHENTICATED_USER_PERMISSION;
            }
        }
        return null;
    }

    public String toDisplayFormat() {
        return "Shared with logged-in users";
    }
}
