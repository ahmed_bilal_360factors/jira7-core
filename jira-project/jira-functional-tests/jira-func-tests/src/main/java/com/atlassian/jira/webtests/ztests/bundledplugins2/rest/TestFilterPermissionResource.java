package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Filter;
import com.atlassian.jira.testkit.client.restclient.Filter.FilterPermission;
import com.atlassian.jira.testkit.client.restclient.FilterClient;
import com.atlassian.jira.testkit.client.restclient.FilterClient.FilterPermissionInputBean;
import com.atlassian.jira.testkit.client.restclient.ProjectRole;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.google.common.base.Objects;
import org.apache.commons.lang.RandomStringUtils;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.REST, Category.FILTERS})
@RestoreBlankInstance
public class TestFilterPermissionResource extends BaseJiraRestTest {
    private FilterClient client;

    private  static final String NON_EXISTENT_FILTER_ID = "123132";
    private  static final FilterPermissionInputBean GROUP_PERMISSION = new FilterPermissionInputBean("group", null, null, "jira-administrators");

    @Before
    public void setUp() throws Exception {
        client = new FilterClient(environmentData);
    }

    @Test
    public void noPermissionsReturnsEmptyList() {
        String filterId = createFilter("test");

        List<FilterPermission> result = client.getFilterPermissions(filterId).body;

        assertThat(result, hasSize(0));
    }

    @Test
    public void addGroupPermissionWorksAndResultsInPermissionBeingAdded() {
        String filterId = createFilter("test");

        List<FilterPermission> putResult = assertCreated(postPermission(filterId, GROUP_PERMISSION));
        assertThat(putResult, hasSize(1));

        List<FilterPermission> result = client.getFilterPermissions(filterId).body;

        assertThat(result, hasSize(1));
    }

    @Test
    public void addGlobalPermissionWorksAndResultsInPermissionBeingAdded() {
        String filterId = createFilter("test");

        FilterPermissionInputBean inputPermission = globalPermission();

        List<FilterPermission> putResult = assertCreated(postPermission(filterId, inputPermission));
        assertThat(putResult, hasSize(1));

        List<FilterPermission> result = client.getFilterPermissions(filterId).body;

        assertThat(result, Matchers.contains(filterPermission(inputPermission)));
    }

    @Test
    public void addGlobalPermissionOverwritesPreviousPermissions() {
        String filterId = createFilter("test");

        withFilterContainingPermission(filterId, GROUP_PERMISSION);

        FilterPermissionInputBean inputPermission = globalPermission();

        List<FilterPermission> result = assertCreated(postPermission(filterId, inputPermission));

        assertThat(result, Matchers.contains(filterPermission(inputPermission)));
    }

    @Test
    public void addProjectPermissionWorksAndResultsInPermissionBeingAdded() {
        String filterId = createFilter("test");

        long projectId = backdoor.project().addProject(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(3).toUpperCase(), "admin");

        List<FilterPermission> putResult = assertCreated(postPermission(filterId, projectPermission(projectId)));
        assertThat(putResult, hasSize(1));

        List<FilterPermission> result = client.getFilterPermissions(filterId).body;

        assertThat(result, hasSize(1));
    }

    @Test
    public void addProjectRolePermissionWorksAndResultsInPermissionBeingAdded() {
        String filterId = createFilter("test");

        String projectKey = RandomStringUtils.randomAlphabetic(3).toUpperCase();
        long projectId = backdoor.project().addProject(RandomStringUtils.randomAlphabetic(5), projectKey, "admin");
        backdoor.projectRole().addActors(projectKey, "Developers", null, new String[]{"admin"});

        ProjectRole projectRole = backdoor.projectRole().get(projectKey, "Developers");

        List<FilterPermission> putResult = assertCreated(postPermission(filterId, projectRolePermission(projectId, projectRole)));
        assertThat(putResult, hasSize(1));

        List<FilterPermission> result = client.getFilterPermissions(filterId).body;

        assertThat(result, hasSize(1));
    }

    @Test
    public void getSinglePermissionWorks() {
        String filterId = createFilter("test");

        List<FilterPermission> putResult = assertCreated(postPermission(filterId, GROUP_PERMISSION));
        assertThat(putResult, hasSize(1));

        FilterPermission filterPermission = putResult.get(0);

        FilterPermission result = client.getFilterPermission(filterId, filterPermission.id.toString()).body;

        assertThat(result, equalTo(filterPermission));
    }

    @Test
    public void getPermissionsReturns404ForNonExistingFilter() {
        assertNotFoundResponse(client.getFilterPermissions(NON_EXISTENT_FILTER_ID));
    }

    @Test
    public void getPermissionReturns404ForNonExistingFilter() {
        assertNotFoundResponse(client.getFilterPermission(NON_EXISTENT_FILTER_ID, "1"));
    }

    @Test
    public void putPermissionReturns404ForNonExistingFilter() {
        assertNotFoundResponse(postPermission(NON_EXISTENT_FILTER_ID, GROUP_PERMISSION));
    }

    @Test
    public void deletePermissionReturns404ForNonExistingFilter() {
        assertNotFoundResponse(client.deleteFilterPermission(NON_EXISTENT_FILTER_ID, "1"));
    }

    @Test
    public void getPermissionReturns404ForNonExistingPermission() {
        String filterId = createFilter("asdf");
        assertNotFoundResponse(client.getFilterPermission(filterId, NON_EXISTENT_FILTER_ID));
    }

    @Test
    public void deletePermissionWorksAndResultsInPermissionBeingAdded() {
        String filterId = createFilter("test");
        FilterPermission filterPermission = withFilterContainingPermission(filterId, GROUP_PERMISSION);

        Response response = client.deleteFilterPermission(filterId, filterPermission.id.toString());

        assertEquals(response.statusCode, 204);

        List<FilterPermission> result = client.getFilterPermissions(filterId).body;
        assertThat(result, hasSize(0));
    }

    @Test
    public void cannotShareFilterWithoutShareObjectsGlobalPermission() {
        client.loginAs("fred"); //fred is in the jira-users-group which does not have the CREATE_SHARED_OBJECTS permission
        String filterId = createFilter("test");

        Response<List<FilterPermission>> putResponse = postPermission(filterId, new FilterPermissionInputBean("group", null, null, "jira-users"));
        assertEquals(putResponse.statusCode, 400);

        List<FilterPermission> result = client.getFilterPermissions(filterId).body;

        assertThat(result, hasSize(0));
    }

    @Test
    public void invalidAddPermissionBeanResultsIn400() {

        String filterId = createFilter("test");

        assertBadRequestForPutFilterPermission(filterId, new FilterPermissionInputBean("group", null, null, null));
        assertBadRequestForPutFilterPermission(filterId, new FilterPermissionInputBean("project", null, null, null));
        assertBadRequestForPutFilterPermission(filterId, new FilterPermissionInputBean("projectRole", null, null, null));
        assertBadRequestForPutFilterPermission(filterId, new FilterPermissionInputBean("invalid", null, null, null));
        assertBadRequestForPutFilterPermission(filterId, new FilterPermissionInputBean("project-unknown", null, null, null));
    }

    private FilterPermissionInputBean projectRolePermission(long projectId, ProjectRole projectRole) {
        return new FilterPermissionInputBean("projectRole", String.valueOf(projectId), projectRole.id.toString(), null);
    }

    private FilterPermissionInputBean projectPermission(long l) {
        return new FilterPermissionInputBean("project", String.valueOf(l), null, null);
    }

    private FilterPermissionInputBean globalPermission() {
        return new FilterPermissionInputBean("global", null, null, null);
    }

    private Response<List<FilterPermission>> postPermission(String filter, FilterPermissionInputBean inputBean) {
        return client.postFilterPermission(filter, inputBean);
    }

    private <T> T assertCreated(Response<T> response) {
        assertEquals(response.statusCode, 201);
        return response.body;
    }

    private void assertBadRequestForPutFilterPermission(String filterId, FilterPermissionInputBean inputBean) {
        Response<List<FilterPermission>> putResponse = postPermission(filterId, inputBean);
        assertEquals(putResponse.statusCode, 400);
    }

    private void assertNotFoundResponse(Response response) {
        assertEquals(response.statusCode, 404);
    }

    private FilterPermission withFilterContainingPermission(String filterId, FilterPermissionInputBean permissionInputBean) {
        List<FilterPermission> putResult = assertCreated(postPermission(filterId, permissionInputBean));
        assertThat(putResult, hasSize(1));

        return putResult.get(0);
    }

    private Matcher<? super FilterPermission> filterPermission(FilterPermissionInputBean permission) {
        return new TypeSafeMatcher<FilterPermission>() {
            @Override
            protected boolean matchesSafely(FilterPermission item) {
                return Objects.equal(permission.groupname, item.group == null ? null : item.group.getName())
                        && Objects.equal(permission.projectId, item.project == null? null: item.project.id)
                        && Objects.equal(permission.projectRoleId, item.role == null? null : item.role.id.toString())
                        && Objects.equal(permission.type, item.type);
            }

            @Override
            public void describeTo(Description description) {
                description.appendValue(permission);
            }
        };
    }


    private String createFilter(String name) {
        //testing a basic filter
        Filter filter1 = new Filter();
        filter1.name = name;
        filter1.jql = "project=homosapien";
        filter1.description = "test description";
        filter1.favourite = true;
        Response<Filter> response = client.postFilterResponse(filter1);
        return response.body.id;
    }
}
