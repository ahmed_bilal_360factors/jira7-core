package com.atlassian.jira.functest.framework.assertions;

import javax.inject.Inject;
import java.util.Iterator;
import java.util.Set;


/**
 * Set of assertions related change history
 *
 * @since v7.1
 */
public class ChangeHistoryAssertions {

    private IssueTableAssertions issueTableAssertions;

    @Inject
    public ChangeHistoryAssertions(final IssueTableAssertions issueTableAssertions) {
        this.issueTableAssertions = issueTableAssertions;
    }

    public void assertWasEmptySearchReturnsEmptyValuesUsingEmptyKeyword(String fieldName, String... issueKeys) {
        String jqlQuery = String.format("%s was EMPTY", fieldName);
        issueTableAssertions.assertSearchWithResults(jqlQuery, issueKeys);
    }

    public void assertWasNotEmptySearchReturnsNotEmptyValuesWithEmptyKeyword(String fieldName, String... issueKeys) {
        String jqlQuery = String.format("%s was not EMPTY", fieldName);
        issueTableAssertions.assertSearchWithResults(jqlQuery, issueKeys);
    }

    public void assertWasSearchReturnsExpectedValues(String fieldName, String value, String... issueKeys) {
        String jqlQuery = String.format("%s was %s", fieldName, value);
        issueTableAssertions.assertSearchWithResults(jqlQuery, issueKeys);
    }

    public void assertWasInSearchReturnsExpectedValues(String fieldName, Set<String> listValues, String... issueKeys) {
        String list = buildListString(listValues);
        String jqlQuery = String.format("%s was in %s", fieldName, list);
        issueTableAssertions.assertSearchWithResults(jqlQuery, issueKeys);
    }

    public void assertWasNotInSearchReturnsExpectedValues(String fieldName, Set<String> listValues, String... issueKeys) {
        String list = buildListString(listValues);
        String jqlQuery = String.format("%s was not in %s", fieldName, list);
        issueTableAssertions.assertSearchWithResults(jqlQuery, issueKeys);
    }

    public void assertWasBySearchReturnsExpectedValues(String fieldName, String value, String actioner, String... issueKeys) {
        String jqlQuery = String.format("%s was %s by %s", fieldName, value, actioner);
        issueTableAssertions.assertSearchWithResults(jqlQuery, issueKeys);
    }

    public void assertWasBySearchUsingListOperandsReturnsExpectedValues(String fieldName, String value, Set<String> actioners, String... issueKeys) {
        String list = buildListString(actioners);
        String jqlQuery = String.format("%s was %s by %s", fieldName, value, list);
        issueTableAssertions.assertSearchWithResults(jqlQuery, issueKeys);
    }

    public void assertWasDuringSearchReturnsExpectedValues(String fieldName, String value, String from, String to, String... issueKeys) {
        String list = String.format("(%s,%s)", from, to);
        String jqlQuery = String.format("%s was %s during %s", fieldName, value, list);
        issueTableAssertions.assertSearchWithResults(jqlQuery, issueKeys);
    }

    public void assertWasBeforeSearchReturnsExpectedValues(String fieldName, String value, String date, String... issueKeys) {
        String jqlQuery = String.format("%s was %s before %s", fieldName, value, date);
        issueTableAssertions.assertSearchWithResults(jqlQuery, issueKeys);
    }

    public void assertWasAfterSearchReturnsExpectedValues(String fieldName, String value, String date, String... issueKeys) {
        String jqlQuery = String.format("%s was %s after %s", fieldName, value, date);
        issueTableAssertions.assertSearchWithResults(jqlQuery, issueKeys);
    }

    public void assertWasOnSearchReturnsExpectedValues(String fieldName, String value, String date, String... issueKeys) {
        String jqlQuery = String.format("%s was %s on %s", fieldName, value, date);
        issueTableAssertions.assertSearchWithResults(jqlQuery, issueKeys);
    }

    public void assertWasInListFunctionReturnsExpectedValues(String fieldName, String function, String... issueKeys) {
        String jqlQuery = String.format("%s was in %s", fieldName, function);
        issueTableAssertions.assertSearchWithResults(jqlQuery, issueKeys);
    }

    public ChangeHistoryAssertions() {
        super();
    }

    public void assertInvalidSearchProducesError(String fieldName, String value, String whereClause, String error) {
        String jqlQuery = String.format("%s was %s %s", fieldName, value, whereClause);
        issueTableAssertions.assertSearchWithError(jqlQuery, error);
    }

    private String buildListString(Set<String> listValues) {
        final StringBuilder list = new StringBuilder();
        if (listValues != null) {
            list.append('(');
            Iterator<String> iter = listValues.iterator();
            while (iter.hasNext()) {
                list.append(iter.next());
                if (iter.hasNext()) {
                    list.append(',');
                }
            }
            list.append(')');

        }
        return list.toString();
    }
}
