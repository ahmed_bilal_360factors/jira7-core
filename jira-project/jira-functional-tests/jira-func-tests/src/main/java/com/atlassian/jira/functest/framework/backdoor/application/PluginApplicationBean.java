package com.atlassian.jira.functest.framework.backdoor.application;

import com.google.common.collect.Lists;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * @since 7.0
 */
public class PluginApplicationBean extends ApplicationBean {
    public enum PluginType {
        PRIMARY, APPLICATION, UTILITY
    }

    @JsonProperty
    private String definitionModuleKey;

    @JsonProperty
    private List<PluginBean> plugins = Lists.newArrayList();

    public String getDefinitionModuleKey() {
        return definitionModuleKey;
    }

    public List<PluginBean> getPlugins() {
        return plugins;
    }

    public static class PluginBean {
        @JsonProperty
        private String key;

        @JsonProperty
        private PluginType type;

        public String getKey() {
            return key;
        }

        public PluginType getType() {
            return type;
        }
    }
}
