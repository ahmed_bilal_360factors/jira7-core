package com.atlassian.jira.webtests.ztests.user.rename;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.assertions.IssueTableAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * @since v6.0
 */
@WebTest({Category.FUNC_TEST, Category.USERS_AND_GROUPS, Category.RENAME_USER, Category.JQL, Category.CHANGE_HISTORY})
@Restore("user_rename_search.xml")
public class TestUserRenameOnSearch extends BaseJiraFuncTest {
    @Inject
    private IssueTableAssertions issueTableAssertions;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Before
    public void setUpTest() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, "betty");
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, "cc");
        issueTableAssertions.assertSearchWithResultsForUser("admin", "", "COW-4", "COW-3", "COW-2", "COW-1");
    }

    //    KEY       USERNAME    NAME
    //    bb        betty       Betty Boop
    //    ID10001   bb          Bob Belcher
    //    cc        cat         Crazy Cat
    //    ID10101   cc          Candy Chaos

    @Test
    public void testCurrentAssigneeAndReporter() {
        issueTableAssertions.assertSearchWithResults("assignee = currentUser()", "COW-4");
        issueTableAssertions.assertSearchWithResults("assignee = admin", "COW-4");
        issueTableAssertions.assertSearchWithResults("assignee = bb", "COW-3");
        issueTableAssertions.assertSearchWithResults("assignee = betty", "COW-1");
        assertBadValueWarning("assignee = candy", "assignee", "candy");
        issueTableAssertions.assertSearchWithResults("assignee = cat", "COW-2");
        issueTableAssertions.assertSearchWithResults("assignee = cc");
        issueTableAssertions.assertSearchWithResults("assignee in (betty,bb)", "COW-3", "COW-1");
        issueTableAssertions.assertSearchWithResults("assignee in (admin,bb)", "COW-4", "COW-3");
        issueTableAssertions.assertSearchWithResults("assignee in (currentUser(),bb)", "COW-4", "COW-3");

        issueTableAssertions.assertSearchWithResults("reporter = currentUser()", "COW-4");
        issueTableAssertions.assertSearchWithResults("reporter = admin", "COW-4");
        issueTableAssertions.assertSearchWithResults("reporter = bb");
        issueTableAssertions.assertSearchWithResults("reporter = betty");
        assertBadValueWarning("reporter = candy", "reporter", "candy");
        issueTableAssertions.assertSearchWithResults("reporter = cat", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("reporter = cc", "COW-3");
        issueTableAssertions.assertSearchWithResults("reporter in (cc,cat)", "COW-3", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("reporter in (admin,cat)", "COW-4", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("reporter in (currentUser(),cat)", "COW-4", "COW-2", "COW-1");
    }

    @Test
    public void testCurrentCustomFieldValue() {
        issueTableAssertions.assertSearchWithResults("tester = currentUser()");
        issueTableAssertions.assertSearchWithResults("tester = admin");
        issueTableAssertions.assertSearchWithResults("tester = bb");
        issueTableAssertions.assertSearchWithResults("tester = betty");
        assertBadValueWarning("tester = candy", "tester", "candy");
        issueTableAssertions.assertSearchWithResults("tester = cat", "COW-3", "COW-2");
        issueTableAssertions.assertSearchWithResults("tester = cc", "COW-1");
        issueTableAssertions.assertSearchWithResults("tester in (cc,cat)", "COW-3", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("tester in (admin,cat)", "COW-3", "COW-2");
        issueTableAssertions.assertSearchWithResults("tester in (currentUser(),cat)", "COW-3", "COW-2");

        issueTableAssertions.assertSearchWithResults("cc = currentUser()", "COW-3", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("cc = admin", "COW-3", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("cc = bb", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("cc = betty", "COW-3");
        assertBadValueWarning("cc = candy", "cc", "candy");
        issueTableAssertions.assertSearchWithResults("cc = cat");
        issueTableAssertions.assertSearchWithResults("cc = cc");
        issueTableAssertions.assertSearchWithResults("cc in (cc,cat)");
        issueTableAssertions.assertSearchWithResults("cc in (admin,cat)", "COW-3", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("cc in (currentUser(),cat)", "COW-3", "COW-2", "COW-1");
    }

    @Test
    public void testVotersAndWatchers() {
        initVotersAndWatchers("betty", "bb");

        issueTableAssertions.assertSearchWithResults("voter = currentUser()");
        issueTableAssertions.assertSearchWithResults("voter = admin");
        issueTableAssertions.assertSearchWithResults("voter = bb", "COW-3");
        issueTableAssertions.assertSearchWithResults("voter = betty", "COW-1");
        assertBadValueWarning("voter = candy", "voter", "candy");
        issueTableAssertions.assertSearchWithResults("voter = cat");
        issueTableAssertions.assertSearchWithResults("voter = cc");
        issueTableAssertions.assertSearchWithResults("voter in (bb,betty)", "COW-3", "COW-1");
        issueTableAssertions.assertSearchWithResults("voter in (admin,bb)", "COW-3");
        issueTableAssertions.assertSearchWithResults("voter in (currentUser(),bb)", "COW-3");

        issueTableAssertions.assertSearchWithResults("watcher = currentUser()", "COW-4", "COW-3", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("watcher = admin", "COW-4", "COW-3", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("watcher = bb", "COW-4");
        issueTableAssertions.assertSearchWithResults("watcher = betty", "COW-2");
        assertBadValueWarning("watcher = candy", "watcher", "candy");
        issueTableAssertions.assertSearchWithResults("watcher = cat");
        issueTableAssertions.assertSearchWithResults("watcher = cc");
        issueTableAssertions.assertSearchWithResults("watcher in (bb,betty)", "COW-4", "COW-2");
        issueTableAssertions.assertSearchWithResults("watcher in (admin,bb)", "COW-4", "COW-3", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("watcher in (currentUser(),bb)", "COW-4", "COW-3", "COW-2", "COW-1");
    }

    /*
     * History items...
     *
     * SELECT i.pkey,cg.author,ci.*
     * FROM jiraissue i
     * INNER JOIN changegroup cg ON cg.issueid = i.id
     * INNER JOIN changeitem ci ON ci.groupid = cg.id
     * WHERE ci.field in ('assignee','reporter')
     * ORDER BY ci.field, i.pkey DESC;
     *
     *  pkey  | author  |  id   | groupid | fieldtype |  field   | oldvalue |  oldstring  | newvalue |  newstring
     * -------+---------+-------+---------+-----------+----------+----------+-------------+----------+-------------
     *  COW-3 | admin   | 10500 |   10500 | jira      | assignee | admin    | Adam Ant    | ID10001  | The new BB
     *  COW-2 | ID10001 | 10600 |   10600 | jira      | assignee | admin    | Adam Ant    | cc       | Crazy Cat
     *  COW-1 | admin   | 10101 |   10101 | jira      | assignee | admin    | Adam Ant    | kiran    | Kiran
     *  COW-1 | bb      | 10400 |   10400 | jira      | assignee | kiran    | Kiran       | bb       | Betty Boop
     *  COW-3 | ID10001 | 10706 |   10704 | jira      | reporter | bb       | Betty Boop  | ID10101  | Candy Chaos
     *  COW-3 | admin   | 10502 |   10500 | jira      | reporter | admin    | Adam Ant    | ID10001  | The new BB
     *  COW-3 | cc      | 10704 |   10703 | jira      | reporter | ID10001  | Bob Belcher | bb       | Betty Boop
     *  COW-2 | ID10001 | 10602 |   10600 | jira      | reporter | admin    | Adam Ant    | cc       | Crazy Cat
     *  COW-1 | bb      | 10402 |   10400 | jira      | reporter | admin    | Adam Ant    | bb       | Betty Boop
     *  COW-1 | admin   | 10700 |   10700 | jira      | reporter | bb       | Betty Boop  | cc       | Crazy Cat
     *
     *  New changes that get added to verify that change history is created with keys vs. usernames:
     */


    @Test
    public void testChangedBy() {
        issueTableAssertions.assertSearchWithResults("reporter changed by admin", "COW-3", "COW-1");
        issueTableAssertions.assertSearchWithResults("reporter changed by bb", "COW-3", "COW-2");
        issueTableAssertions.assertSearchWithResults("reporter changed by cat", "COW-3");
        issueTableAssertions.assertSearchWithResults("reporter changed by cc");

        issueTableAssertions.assertSearchWithResults("assignee changed by admin", "COW-3", "COW-1");
        issueTableAssertions.assertSearchWithResults("assignee changed by bb", "COW-2");
        issueTableAssertions.assertSearchWithResults("assignee changed by cat");
        issueTableAssertions.assertSearchWithResults("assignee changed by cc");

        assertByClauseError("reporter changed by asdfasdf", "asdfasdf");
        assertByClauseError("assignee changed by candy", "candy");
    }

    @Test
    public void testWasClauseForAssigneeAndReporter() {
        issueTableAssertions.assertSearchWithResults("assignee was currentUser()", "COW-4", "COW-3", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("assignee was admin", "COW-4", "COW-3", "COW-2", "COW-1");
        assertBadValueError("assignee was asdfasdf", "assignee", "asdfasdf");
        issueTableAssertions.assertSearchWithResults("assignee was bb", "COW-3");
        assertBadValueError("assignee was candy", "assignee", "candy");
        issueTableAssertions.assertSearchWithResults("assignee was cat", "COW-2");
        assertBadValueError("assignee was in (candy, cat)", "assignee", "candy");
        issueTableAssertions.assertSearchWithResults("assignee was cc");

        issueTableAssertions.assertSearchWithResults("reporter was currentUser()", "COW-4", "COW-3", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("reporter was admin", "COW-4", "COW-3", "COW-2", "COW-1");
        assertBadValueError("reporter was asdfasdf", "reporter", "asdfasdf");
        issueTableAssertions.assertSearchWithResults("reporter was bb", "COW-3");
        assertBadValueError("reporter was candy", "reporter", "candy");
        issueTableAssertions.assertSearchWithResults("reporter was cat", "COW-2", "COW-1");
        assertBadValueError("reporter was in (candy, cat)", "reporter", "candy");
        issueTableAssertions.assertSearchWithResults("reporter was cc", "COW-3");
    }

    private void checkBettyAndCandy(final String goodBetty, final String badBetty, final String goodCandy, final String badCandy) {
        issueTableAssertions.assertSearchWithResults("assignee = " + goodCandy);
        issueTableAssertions.assertSearchWithResults("assignee changed by " + goodBetty + "", "COW-1");
        issueTableAssertions.assertSearchWithResults("assignee was " + goodBetty + "", "COW-1");
        issueTableAssertions.assertSearchWithResults("assignee was in (" + goodBetty + ", cat)", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("assignee was in (" + goodCandy + ", " + goodBetty + ")", "COW-1");
        issueTableAssertions.assertSearchWithResults("assignee changed from " + goodBetty + "");
        issueTableAssertions.assertSearchWithResults("assignee changed to " + goodBetty + "", "COW-1");

        issueTableAssertions.assertSearchWithResults("reporter = " + goodCandy, "COW-3");
        issueTableAssertions.assertSearchWithResults("reporter in (" + goodCandy + ", cat)", "COW-3", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("reporter was " + goodBetty + "", "COW-3", "COW-1");
        issueTableAssertions.assertSearchWithResults("reporter was in (" + goodBetty + ", cat)", "COW-3", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("reporter was in (" + goodCandy + ", " + goodBetty + ")", "COW-3", "COW-1");
        issueTableAssertions.assertSearchWithResults("reporter changed by " + goodBetty + "", "COW-1");
        issueTableAssertions.assertSearchWithResults("reporter changed from " + goodBetty + "", "COW-3", "COW-1");
        issueTableAssertions.assertSearchWithResults("reporter changed to " + goodBetty + "", "COW-3", "COW-1");
        issueTableAssertions.assertSearchWithResults("reporter changed from bb to " + goodBetty + "", "COW-3");
        issueTableAssertions.assertSearchWithResults("reporter changed from " + goodBetty + " to bb");
        assertBadValueError("reporter changed from " + goodBetty + " to " + badCandy, "reporter", badCandy);
        issueTableAssertions.assertSearchWithResults("reporter changed from " + goodBetty + " to cat", "COW-1");
        issueTableAssertions.assertSearchWithResults("reporter changed from " + goodBetty + " to " + goodCandy, "COW-3");

        issueTableAssertions.assertSearchWithResults("tester = " + goodCandy, "COW-1");
        issueTableAssertions.assertSearchWithResults("cc = " + goodCandy);
        issueTableAssertions.assertSearchWithResults("voter = " + goodCandy, "COW-1");
        issueTableAssertions.assertSearchWithResults("watcher = " + goodCandy, "COW-2");
        issueTableAssertions.assertSearchWithResults("voter = " + goodBetty, "COW-3");
        issueTableAssertions.assertSearchWithResults("watcher = " + goodBetty, "COW-4");

        assertBadValueWarning("assignee = " + badCandy, "assignee", badCandy);
        assertByClauseError("assignee changed by " + badBetty, badBetty);
        assertBadValueError("assignee changed to " + badBetty, "assignee", badBetty);
        assertBadValueWarning("reporter = " + badCandy, "reporter", badCandy);
        assertByClauseError("reporter changed by " + badBetty, badBetty);
        assertBadValueError("reporter changed from " + badBetty, "reporter", badBetty);
        assertBadValueWarning("tester = " + badCandy, "tester", badCandy);
        assertBadValueWarning("cc = " + badCandy, "cc", badCandy);
        assertBadValueWarning("voter = " + badCandy, "voter", badCandy);
        assertBadValueWarning("watcher = " + badCandy, "watcher", badCandy);

        try {
            backdoor.issueTableClient().loginAs(goodBetty, "betty");
            assertCurrentUserQueryResultsForBetty();
            backdoor.issueTableClient().loginAs(goodCandy, "cc");
            assertCurrentUserQueryResultsForCandy();
        } finally {
            backdoor.issueTableClient().loginAs(ADMIN_USERNAME);
        }
    }

    private void assertCurrentUserQueryResultsForBetty() {
        issueTableAssertions.assertSearchWithResults("assignee = currentUser()", "COW-1");
        issueTableAssertions.assertSearchWithResults("reporter = currentUser()");
        issueTableAssertions.assertSearchWithResults("reporter in (currentUser(), cat)", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("tester = currentUser()");
        issueTableAssertions.assertSearchWithResults("cc = currentUser()", "COW-3");
        issueTableAssertions.assertSearchWithResults("voter = currentUser()", "COW-3");
        issueTableAssertions.assertSearchWithResults("watcher = currentUser()", "COW-4");
    }

    private void assertCurrentUserQueryResultsForCandy() {
        issueTableAssertions.assertSearchWithResults("assignee = currentUser()");
        issueTableAssertions.assertSearchWithResults("reporter = currentUser()", "COW-3");
        issueTableAssertions.assertSearchWithResults("reporter in (currentUser(), cat)", "COW-3", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("tester = currentUser()", "COW-1");
        issueTableAssertions.assertSearchWithResults("cc = currentUser()");
        issueTableAssertions.assertSearchWithResults("voter = currentUser()", "COW-1");
        issueTableAssertions.assertSearchWithResults("watcher = currentUser()", "COW-2");
    }

    @Test
    public void testChangedFromAndToForAssigneeAndReporter() {
        issueTableAssertions.assertSearchWithResults("assignee changed from admin", "COW-3", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("assignee changed from bb");
        assertBadValueError("assignee changed from candy", "assignee", "candy");
        issueTableAssertions.assertSearchWithResults("assignee changed from cat");
        issueTableAssertions.assertSearchWithResults("assignee changed from cc");

        issueTableAssertions.assertSearchWithResults("assignee changed to admin");
        issueTableAssertions.assertSearchWithResults("assignee changed to bb", "COW-3");
        assertBadValueError("assignee changed to candy", "assignee", "candy");
        issueTableAssertions.assertSearchWithResults("assignee changed to cat", "COW-2");
        issueTableAssertions.assertSearchWithResults("assignee changed to cc");

        issueTableAssertions.assertSearchWithResults("assignee changed from admin to cat", "COW-2");
        issueTableAssertions.assertSearchWithResults("assignee changed from admin to cc");
        assertBadValueError("assignee changed from admin to candy", "assignee", "candy");
        issueTableAssertions.assertSearchWithResults("assignee changed from cat to admin");
        issueTableAssertions.assertSearchWithResults("assignee changed from cc to admin");

        issueTableAssertions.assertSearchWithResults("reporter changed from admin", "COW-3", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("reporter changed from bb", "COW-3");
        assertBadValueError("reporter changed from candy", "reporter", "candy");
        issueTableAssertions.assertSearchWithResults("reporter changed from cat");
        issueTableAssertions.assertSearchWithResults("reporter changed from cc");

        issueTableAssertions.assertSearchWithResults("reporter changed to admin");
        issueTableAssertions.assertSearchWithResults("reporter changed to bb", "COW-3");
        assertBadValueError("reporter changed to candy", "reporter", "candy");
        issueTableAssertions.assertSearchWithResults("reporter changed to cat", "COW-2", "COW-1");
        issueTableAssertions.assertSearchWithResults("reporter changed to cc", "COW-3");

        issueTableAssertions.assertSearchWithResults("reporter changed from admin to cat", "COW-2");
        assertBadValueError("reporter changed from admin to candy", "reporter", "candy");
    }

    private void initVotersAndWatchers(final String user1, final String user2) {
        try {
            navigation.logout();
            navigation.login(user1);
            navigation.issue().viewIssue("COW-1").toggleVote();
            navigation.issue().viewIssue("COW-2").toggleWatch();

            navigation.logout();
            navigation.login(user2);
            navigation.issue().viewIssue("COW-3").toggleVote();
            navigation.issue().viewIssue("COW-4").toggleWatch();
        } finally {
            navigation.logout();
            navigation.login(ADMIN_USERNAME);
        }
    }

    @Test
    public void testRenameUser() {
        initVotersAndWatchers("cc", "betty");
        checkBettyAndCandy("betty", "boop", "cc", "candy");

        renameUser("cc", "candy");
        renameUser("betty", "boop");

        checkBettyAndCandy("boop", "betty", "candy", "cc");
    }

    private void renameUser(final String oldUsername, final String newUsername) {
        administration.usersAndGroups().gotoEditUser(oldUsername).setUsername(newUsername).submitUpdate();
        // Now we are on View User
        Assert.assertEquals("/secure/admin/user/ViewUser.jspa?name=" + newUsername, navigation.getCurrentPage());
        Assert.assertEquals(newUsername, locator.id("username").getText());

    }

    private void assertByClauseError(String jqlQuery, String badUser) {
        final String expectedMessage = "The user '" + badUser + "' does not exist and cannot be used in the 'by' predicate.";
        issueTableAssertions.assertSearchWithError(jqlQuery, expectedMessage);
    }

    private void assertBadValueError(String jqlQuery, String badField, String badValue) {
        final String expectedMessage = "The value '" + badValue + "' does not exist for the field '" + badField + "'.";
        issueTableAssertions.assertSearchWithError(jqlQuery, expectedMessage);
    }

    private void assertBadValueWarning(String jqlQuery, String badField, String badValue) {
        final String expectedMessage = "The value '" + badValue + "' does not exist for the field '" + badField + "'.";
        issueTableAssertions.assertSearchWithWarning(jqlQuery, expectedMessage);
    }
}

