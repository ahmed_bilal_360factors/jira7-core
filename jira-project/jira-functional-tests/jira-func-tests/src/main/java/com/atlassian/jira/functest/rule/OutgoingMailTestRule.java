package com.atlassian.jira.functest.rule;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.testkit.client.log.FuncTestLoggerImpl;
import com.atlassian.jira.webtests.JIRAServerSetup;
import com.atlassian.jira.webtests.util.mail.MailService;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.util.function.Supplier;

/**
 * Adds support for email testing by using it as a rule in conjunction with {@link MailTest} method annotation. Basicly
 * it starts dummy smtp server and configures smtp server in jira by using {@link OutgoingMailConfigureSmtpRule}
 * (omitted on onDemand), starts outgoing email listener and clears the queue by using {@link
 * OutgoingMailEventQueueRule}.
 *
 * @since v7.1
 */
public class OutgoingMailTestRule implements TestRule {
    public static final String DEFAULT_FROM_ADDRESS = "jiratest@atlassian.com";
    public static final String DEFAULT_SUBJECT_PREFIX = "[JIRATEST]";

    private final OutgoingMailConfigureSmtpRule mailEventConfigureSmtpRule;
    private final OutgoingMailEventQueueRule mailEventQueueRule;
    private RuleChain innerChain;

    public OutgoingMailTestRule(final Supplier<Backdoor> backdoorSupplier, final String mailFrom, final String mailSubjectPrefix) {
        this.mailEventQueueRule = new OutgoingMailEventQueueRule(() -> backdoorSupplier.get().outgoingMailControl());
        final MailService mailService = new MailService(new FuncTestLoggerImpl(3));
        this.mailEventConfigureSmtpRule = new OutgoingMailConfigureSmtpRule(mailFrom,
                mailSubjectPrefix,
                backdoorSupplier,
                mailService,
                JIRAServerSetup.SMTP
        );

        this.innerChain = RuleChain.emptyRuleChain()
                .around(mailEventConfigureSmtpRule)
                .around(this.mailEventQueueRule);
    }

    public OutgoingMailTestRule(final Supplier<Backdoor> backdoorSupplier) {
        this(backdoorSupplier, DEFAULT_FROM_ADDRESS, DEFAULT_SUBJECT_PREFIX);
    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return innerChain.apply(base, description);
    }

    public OutgoingMailEventQueueRule getMailEventQueueRule() {
        return mailEventQueueRule;
    }

    public RuleChain getInnerChain() {
        return innerChain;
    }
}
