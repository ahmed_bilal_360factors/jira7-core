package com.atlassian.jira.functest.rule;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.atlassian.jira.util.Supplier;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import org.apache.commons.io.IOUtils;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static java.util.Optional.of;

public class JiraRestClientSupplier implements TestRule, Supplier<JiraRestClient> {

    private final Supplier<URI> serverUriSupplier;
    private Optional<JiraRestClient> jiraRestClient = Optional.empty();

    private final String username;
    private final String password;

    public JiraRestClientSupplier(final Supplier<JIRAEnvironmentData> environmentDataSupplier) {
        serverUriSupplier = () -> getUri(environmentDataSupplier);
        this.username = ADMIN_USERNAME;
        this.password = ADMIN_PASSWORD;
    }

    public JiraRestClientSupplier(final Supplier<JIRAEnvironmentData> environmentDataSupplier, final String username, final String password) {
        serverUriSupplier = () -> getUri(environmentDataSupplier);
        this.username = username;
        this.password = password;
    }


    private URI getUri(final Supplier<JIRAEnvironmentData> environmentDataSupplier) {
        try {
            return environmentDataSupplier.get().getBaseUrl().toURI();
        } catch (final URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public JiraRestClient get() {
        jiraRestClient = of(jiraRestClient.orElseGet(this::createClient));
        return jiraRestClient.get();
    }

    private JiraRestClient createClient() {
        return new AsynchronousJiraRestClientFactory().createWithBasicHttpAuthentication(serverUriSupplier.get(), username, password);
    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try {
                    base.evaluate();
                } finally {
                    jiraRestClient.ifPresent(IOUtils::closeQuietly);
                }
            }
        };
    }
}
