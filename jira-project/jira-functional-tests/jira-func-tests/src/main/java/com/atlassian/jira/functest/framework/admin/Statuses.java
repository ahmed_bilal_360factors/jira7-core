package com.atlassian.jira.functest.framework.admin;


import com.google.inject.ImplementedBy;

/**
 * Operations on the administrator pages dealing with global issue statuses.
 */
@ImplementedBy(StatusesImpl.class)
public interface Statuses {

    void addLinkedStatus(String statusName, String statusDesc);

    void deleteLinkedStatus(String statusId);

}
