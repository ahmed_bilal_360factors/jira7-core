package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Tests the AddPermission action.
 * <p/>
 * Note that in order for this test to run, you must add the non-standard permission types "assigneeassignable"
 * and "reportercreate". Currently you edit permission-types.xml and run the test manually.
 *
 * @since v3.12
 */
@Ignore("Can only be run manually because it requires a non-standard permission-types.xml config")
@WebTest({Category.FUNC_TEST, Category.PERMISSIONS})
@LoginAs(user = ADMIN_USERNAME)
public class TestAddPermissionNonStandardPermissionTypes extends BaseJiraFuncTest {

    @Inject
    private Administration administration;

    @Before
    public void setUp() {
        administration.restoreBlankInstance();
    }

    @Test
    public void testAssignableUser() {
        navigation.gotoAdminSection(Navigation.AdminSection.PERMISSION_SCHEMES);
        tester.clickLink("0_edit");
        tester.clickLinkWithText("Grant permission");
        tester.checkCheckbox("type", "assignee");
        tester.selectOption("permissions", "Assignable User");
        tester.submit(" Add ");
        tester.assertTextPresent("Errors");
        tester.assertTextPresent("Selected Permission 'Assignable User' is not valid for 'Assignee (show only projects with assignable permission)'.");
    }

    @Test
    public void testReporterCreate_Create() {
        navigation.gotoAdminSection(Navigation.AdminSection.PERMISSION_SCHEMES);
        tester.clickLink("0_edit");
        tester.clickLinkWithText("Grant permission");
        tester.checkCheckbox("type", "reporter");
        tester.selectOption("permissions", "Create Issues");
        tester.submit(" Add ");
        tester.assertTextPresent("Errors");
        tester.assertTextPresent("Selected Permission 'Create Issues' is not valid for 'Reporter (show only projects with create permission)'.");
    }

    @Test
    public void testGroupCreate() {
        navigation.gotoAdminSection(Navigation.AdminSection.PERMISSION_SCHEMES);
        tester.clickLink("0_edit");
        tester.clickLinkWithText("Grant permission");
        tester.checkCheckbox("type", "group");
        tester.selectOption("permissions", "Create Issues");
        tester.submit(" Add ");
        tester.assertTextPresent("Edit Permissions &mdash; Default Permission Scheme");
        tester.assertTextNotPresent("Errors");
    }

}
