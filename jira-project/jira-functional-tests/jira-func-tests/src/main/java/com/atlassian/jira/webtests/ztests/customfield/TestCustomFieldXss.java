package com.atlassian.jira.webtests.ztests.customfield;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.RestoreBlankInstance;
import com.atlassian.jira.functest.framework.admin.CustomFields;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.navigation.BulkChangeWizard;
import com.atlassian.jira.functest.framework.navigation.IssueNavigatorNavigation;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_CASCADING_SELECT_SEARCHER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_DATE_RANGE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_EXACT_NUMBER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_EXACT_TEXT_SEARCHER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_GROUP_PICKER_SEARCHER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_LABEL_SEARCHER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_MULTI_SELECT_SEARCHER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_NUMBER_RANGE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_PROJECT_SEARCHER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TEXT_SEARCHER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_CASCADINGSELECT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_CHECKBOX;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_DATEPICKER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_DATETIME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_FLOAT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_GROUPPICKER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_LABELS;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_MULTICHECKBOXES;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_MULTIGROUPPICKER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_MULTISELECT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_MULTIUSERPICKER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_PROJECT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_RADIO;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_SELECT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_TEXTFIELD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_URL;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_USERPICKER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_VERSION;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_USER_PICKER_GROUP_SEARCHER;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_VERSION_SEARCHER;
import static com.atlassian.jira.functest.framework.suite.Category.CUSTOM_FIELDS;
import static com.atlassian.jira.functest.framework.suite.Category.FIELDS;
import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.SECURITY;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


@WebTest({FUNC_TEST, CUSTOM_FIELDS, FIELDS, SECURITY})
@LoginAs(user = ADMIN_USERNAME)
@RestoreBlankInstance
public class TestCustomFieldXss extends BaseJiraFuncTest {

    private static final String RAW_DESC_TEMPLATE = "description *wiki* markup <div>%s</div>";
    private static final String HTML_DESC_TEMPLATE = "description *wiki* markup <div>%s</div>";
    private static final String WIKI_DESC_TEMPLATE = "<p>description <b>wiki</b> markup &lt;div&gt;%s&lt;/div&gt;</p>";

    private static final String CUSTOM_FIELD_TITLE = "<div>xsstest</div>";
    private static final String CUSTOM_FIELD_WIKI = "&lt;div&gt;xsstest&lt;/div&gt;";

    private static final String CUSTOM_DESCRIPTION_HTML = "<div>xssdescription</div>";

    private static final Iterable<String> CUSTOM_FIELD_TYPES = ImmutableList.of(
            CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_SELECT),
            CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_RADIO),
            CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_CHECKBOX),
            CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_TEXTFIELD),
            CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_MULTISELECT),
            CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_USERPICKER),
            CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_MULTIUSERPICKER),
            CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_DATEPICKER),
            CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_DATETIME),
            CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_GROUPPICKER),
            CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_MULTIGROUPPICKER)
    );

    private static final Map<String, List<String>> SEARCHERS = ImmutableMap.<String, List<String>>builder()
            .put(CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TEXT_SEARCHER), toBuiltInCustomFieldKeys(CUSTOM_FIELD_TYPE_TEXTFIELD))
            .put(CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_EXACT_TEXT_SEARCHER), toBuiltInCustomFieldKeys(CUSTOM_FIELD_TYPE_URL))
            .put(CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_DATE_RANGE), toBuiltInCustomFieldKeys(CUSTOM_FIELD_TYPE_DATEPICKER))
            .put(CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_EXACT_NUMBER), toBuiltInCustomFieldKeys(CUSTOM_FIELD_TYPE_FLOAT))
            .put(CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_NUMBER_RANGE), toBuiltInCustomFieldKeys(CUSTOM_FIELD_TYPE_FLOAT))
            .put(CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_PROJECT_SEARCHER), toBuiltInCustomFieldKeys(CUSTOM_FIELD_TYPE_PROJECT))
            .put(CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_GROUP_PICKER_SEARCHER), toBuiltInCustomFieldKeys(CUSTOM_FIELD_TYPE_MULTIGROUPPICKER))
            .put(CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_MULTI_SELECT_SEARCHER), toBuiltInCustomFieldKeys(CUSTOM_FIELD_TYPE_SELECT, CUSTOM_FIELD_TYPE_RADIO, CUSTOM_FIELD_TYPE_MULTICHECKBOXES))
            .put(CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_CASCADING_SELECT_SEARCHER), toBuiltInCustomFieldKeys(CUSTOM_FIELD_TYPE_CASCADINGSELECT))
            .put(CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_LABEL_SEARCHER), toBuiltInCustomFieldKeys(CUSTOM_FIELD_TYPE_LABELS))
            .build();

    private static final ImmutableMap<String, String> SEARCHERS_NON_RENDERING = ImmutableMap.of(
            CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_VERSION_SEARCHER), CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_VERSION),
            CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_USER_PICKER_GROUP_SEARCHER), CustomFields.builtInCustomFieldKey(CUSTOM_FIELD_TYPE_USERPICKER)
    );
    @Inject
    private Administration administration;
    @Inject
    private LocatorFactory locator;

    private static List<String> toBuiltInCustomFieldKeys(String... keys) {
        List<String> customFieldKeys = Lists.newArrayListWithCapacity(keys.length);

        for (String key : keys) {
            customFieldKeys.add(CustomFields.builtInCustomFieldKey(key));
        }

        return customFieldKeys;
    }

    private static String fieldDescription(String fieldId) {
        return String.format(RAW_DESC_TEMPLATE, fieldId);
    }

    private static String fieldDescriptionHtml(String fieldId) {
        return String.format(HTML_DESC_TEMPLATE, fieldId);
    }

    private static String fieldDescriptionWikiFormat(String fieldId) {
        return String.format(WIKI_DESC_TEMPLATE, fieldId);
    }

    @Before
    public void setUp() throws Exception {
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_HTML_IN_CUSTOM_FIELDS_ENABLED, true);
    }

    @Test
    public void testCustomFieldDescriptionsCanBeRenderedAsRawHtmlOrWikiMarkup() throws Exception {
        for (String type : CUSTOM_FIELD_TYPES) {
            testSingleCustomFieldDescriptionOnCustomFieldsScreen(type);
        }
    }

    @Test
    public void testCustomFieldDescriptionsCanBeRenderedAsRawHtmlOrWikiMarkUpInIssueNavigator() throws Exception {
        for (Map.Entry<String, List<String>> entry : SEARCHERS.entrySet()) {
            testSingleCustomFieldDescriptionOnIssueNavigatorScreen(entry.getValue(), entry.getKey());
        }
    }

    @Test
    public void testNonRenderingCustomFieldSearchersInIssueNavigator() throws Exception {
        for (Map.Entry<String, String> entry : SEARCHERS_NON_RENDERING.entrySet()) {
            testSingleCustomFieldOnIssueNavigatorScreen(entry.getValue(), entry.getKey());
        }
    }

    /**
     * Just for this test, we are restoring from an XML backup because currently there's no way to put
     * custom fields into screens.
     *
     * @throws Exception
     */
    @Test
    public void testCustomFieldDescriptionsInBulkChangeIssue() throws Exception {
        restoreDataForTest();

        backdoor.project().addProject("TestProject", "TEST", "admin");
        backdoor.issues().createIssue("TEST", "This is just a test");

        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_HTML_IN_CUSTOM_FIELDS_ENABLED, false);
        checkBulkIssueTransition(false);

        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_HTML_IN_CUSTOM_FIELDS_ENABLED, true);
        checkBulkIssueTransition(true);
    }

    private void testSingleCustomFieldOnIssueNavigatorScreen(String customFieldType, String customFieldSearcher) {
        final String fieldId = backdoor.customFields().createCustomField(customFieldType + "-name", fieldDescription(customFieldType), customFieldType, customFieldSearcher);

        // now test that the description is does not contain raw HTML
        navigation.issueNavigator().displayAllIssues();
        assertFalse("Description should be rendered as wiki markup for " + customFieldSearcher, getPageSource().contains("<div>" + customFieldType + "</div>"));

        backdoor.customFields().deleteCustomField(fieldId);
    }

    private void testSingleCustomFieldDescriptionOnIssueNavigatorScreen(List<String> customFieldTypes, String customFieldSearcher) {
        for (String customFieldType : customFieldTypes) {
            testSingleCustomFieldDescriptionOnIssueNavigatorScreen(customFieldType, customFieldSearcher);
        }
    }

    private void testSingleCustomFieldDescriptionOnIssueNavigatorScreen(String customFieldType, String customFieldSearcher) {
        final String fieldId = backdoor.customFields().createCustomField(customFieldType + "-name", fieldDescription(customFieldType), customFieldType, customFieldSearcher);

        // test that the description is rendered as raw HTML
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_HTML_IN_CUSTOM_FIELDS_ENABLED, true);
        tester.gotoPage("secure/QueryComponentRendererEdit!Default.jspa?fieldId=" + fieldId + "&decorator=none&jqlContext=");
        assertTrue("HTML in Custom Fields is enabled so the description should be rendered as raw HTML for " + customFieldSearcher, getPageSource().contains(fieldDescriptionHtml(customFieldType)));

        // now test that the description is rendered as Wiki markup
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_HTML_IN_CUSTOM_FIELDS_ENABLED, false);
        tester.gotoPage("secure/QueryComponentRendererEdit!Default.jspa?fieldId=" + fieldId + "&decorator=none&jqlContext=");
        assertTrue("HTML in Custom Fields is disabled so the description should be rendered as wiki markup for " + customFieldSearcher, getPageSource().contains(fieldDescriptionWikiFormat(customFieldType)));
        assertFalse("HTML in Custom Fields is disabled so the description should be rendered as wiki markup for " + customFieldSearcher, getPageSource().contains("<div>" + customFieldType + "</div>"));

        backdoor.customFields().deleteCustomField(fieldId);
    }

    private void testSingleCustomFieldDescriptionOnCustomFieldsScreen(String customFieldType) {
        final String fieldId = backdoor.customFields().createCustomField(customFieldType + "-name", fieldDescription(customFieldType), customFieldType, null);

        // test that the description is rendered as raw HTML
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_HTML_IN_CUSTOM_FIELDS_ENABLED, true);
        goToCustomFields();
        assertTrue("HTML in Custom Fields is enabled so the description should be rendered as raw HTML for " + customFieldType, getPageSource().contains(fieldDescriptionHtml(customFieldType)));

        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_HTML_IN_CUSTOM_FIELDS_ENABLED, false);
        goToCustomFields();
        assertTrue("HTML in Custom Fields is disabled so the description should be rendered as wiki markup for " + customFieldType, getPageSource().contains(fieldDescriptionWikiFormat(customFieldType)));
        assertFalse("HTML in Custom Fields is dsabled so the description should be rendered as wiki markup for " + customFieldType, getPageSource().contains("<div>" + customFieldType + "</div>"));

        backdoor.customFields().deleteCustomField(fieldId);
    }

    private void checkBulkIssueTransition(final boolean shouldHtmlBeEnabled) {
        navigation.issueNavigator().displayAllIssues();

        final String closeIssue = "jira_2_6";

        navigation.issueNavigator()
                .bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES)
                .selectAllIssues()
                .chooseOperation(BulkChangeWizard.BulkOperationsImpl.TRANSITION)
                .chooseWorkflowTransition(new BulkChangeWizard.BulkOperationsCustom(closeIssue));

        assertFalse("Custom field names should not be rendered as html in this page", getPageSource().contains(CUSTOM_FIELD_TITLE));
        assertTrue("Custom field names should be rendered as wiki markup in this page", getPageSource().contains(CUSTOM_FIELD_WIKI));

        assertThat(getAssertionMessage(shouldHtmlBeEnabled), getPageSource().contains(CUSTOM_DESCRIPTION_HTML), Matchers.is(shouldHtmlBeEnabled));
    }

    private String getAssertionMessage(final boolean shouldHtmlBeEnabled) {
        return "HTML in custom fields is " + shouldHtmlBeEnabled + ": custom field descriptions should " + (shouldHtmlBeEnabled ? "" : "not ") + "be rendered as html";
    }

    private void restoreDataForTest() {
        administration.restoreData("TestCustomFieldTitle.xml");

    }

    private void goToCustomFields() {
        navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
    }

    private String getPageSource() {
        return tester.getDialog().getResponseText();
    }

    private CssLocator locatorForDescription(String fieldId) {
        return locator.css(String.format("#custom-fields-%s-name div.description", fieldId));
    }
}
