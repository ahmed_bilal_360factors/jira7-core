package com.atlassian.jira.webtests.ztests.workflow;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.opensymphony.util.TextUtils;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.WORKFLOW_COPIED;

@WebTest({Category.FUNC_TEST, Category.WORKFLOW})
@LoginAs(user = ADMIN_USERNAME)
public class TestWorkflowTransitionView extends BaseJiraFuncTest {

    private static final String TRANSITION_NAME_1 = "My Transition 1";
    private static final String TRANSITION_NAME_2 = "My Transition 2";
    private static final String TRANSITION_DESC = "This is a test transition";
    private static final String TRANSITION_FIELD_SCREEN = "No view for transition";

    @Inject
    private Administration administration;

    @Before
    public void setUp() {
        administration.restoreBlankInstance();
    }

    private void copyWorkflowAndAddTransitions() {
        administration.workflows().goTo()
                .copyWorkflow("jira", WORKFLOW_COPIED, "Workflow copied from JIRA default");
        tester.assertTextPresent(WORKFLOW_COPIED);
        administration.workflows().goTo().workflowSteps(WORKFLOW_COPIED)
                .addTransition("Open", TRANSITION_NAME_1, TRANSITION_DESC, "Closed", TRANSITION_FIELD_SCREEN)
                .addTransition("Open", TRANSITION_NAME_2, TRANSITION_DESC, "Closed", TRANSITION_FIELD_SCREEN);
    }

    /**
     * Tests that the default value in the Workflow Transision stays the same. Fix for JRA-9550
     */
    @Test
    public void testTransitionView1() {
        copyWorkflowAndAddTransitions();
        administration.workflows().goTo().workflowSteps("Copied Workflow");
        tester.clickLinkWithText(TRANSITION_NAME_1);
        tester.assertTextPresent("None - it will happen instantly");

        tester.clickLink("edit_transition");
        tester.assertFormElementEquals("view", "");
        tester.submit("Update");
        tester.assertTextPresent("None - it will happen instantly");

        tester.clickLink("edit_transition");
        tester.assertFormElementEquals("view", "");
        tester.selectOption("view", "Default Screen");
        tester.assertFormElementEquals("view", "1");
        tester.submit("Update");
        tester.assertTextPresent("Default Screen");

        tester.clickLink("edit_transition");
        tester.assertFormElementEquals("view", "1");
        tester.submit("Update");
        tester.assertTextPresent("Default Screen");

        tester.clickLink("edit_transition");
        tester.assertFormElementEquals("view", "1");
        tester.selectOption("view", "Workflow Screen");
        tester.assertFormElementEquals("view", "2");
        tester.submit("Update");
        tester.assertTextPresent("Workflow Screen");

        tester.clickLink("edit_transition");
        tester.assertFormElementEquals("view", "2");
        tester.submit("Update");
        tester.assertTextPresent("Workflow Screen");

        tester.clickLink("edit_transition");
        tester.assertFormElementEquals("view", "2");
        tester.selectOption("view", "No view for transition");
        tester.assertFormElementEquals("view", "");
        tester.submit("Update");
        tester.assertTextPresent("None - it will happen instantly");

        tester.clickLink("edit_transition");
        tester.assertFormElementEquals("view", "");
        tester.submit("Update");
        tester.assertTextPresent("None - it will happen instantly");
    }

    /**
     * Tests that the default value in the Workflow Transision stays the same. Fix for JRA-9550
     */
    @Test
    public void testTransitionView2() {
        copyWorkflowAndAddTransitions();
        administration.workflows().goTo().workflowSteps("Copied Workflow");
        tester.clickLinkWithText(TRANSITION_NAME_2);
        tester.assertTextPresent("None - it will happen instantly");

        tester.clickLink("edit_transition");
        tester.assertFormElementEquals("view", "");
        tester.selectOption("destinationStep", "Closed");
        tester.assertFormElementEquals("view", "");
        tester.submit("Update");
        tester.assertTextPresent("None - it will happen instantly");

        tester.clickLink("edit_transition");
        tester.assertFormElementEquals("view", "");
        tester.selectOption("view", "Resolve Issue Screen");
        tester.assertFormElementEquals("view", "3");
        tester.submit("Update");
        tester.assertTextPresent("Resolve Issue Screen");

        tester.clickLink("edit_transition");
        tester.assertFormElementEquals("view", "3");
        tester.selectOption("destinationStep", "Resolved");
        tester.assertFormElementEquals("view", "3");
        tester.submit("Update");
        tester.assertTextPresent("Resolve Issue Screen");

        tester.clickLink("edit_transition");
        tester.assertFormElementEquals("view", "3");
        tester.selectOption("view", "No view for transition");
        tester.assertFormElementEquals("view", "");
        tester.submit("Update");
        tester.assertTextPresent("None - it will happen instantly");
    }

    @Test
    public void shouldEscapeUsernameWhenNullIsAllowed() {
        shouldEscapeUsername("true");
    }

    private void shouldEscapeUsername(String nullAllowed) {
        int stepId = 1;
        int transitionId = 4;
        String username = "<script>alert(1)</script>";
        String permissionKey = "ADMINISTER_PROJECTS";

        addUserPermissionValidator(stepId, transitionId, username, permissionKey, nullAllowed);
        administration
                .workflows()
                .goTo()
                .workflowSteps(WORKFLOW_COPIED)
                .editTransition(stepId, transitionId);

        tester.assertTextNotPresent(username);
        tester.assertTextPresent(TextUtils.htmlEncode(username));
    }

    private void addUserPermissionValidator(
            int stepId,
            int transitionId,
            String username,
            String permissionKey,
            String nullAllowed
    ) {
        String workflowValidatorKey = "com.atlassian.jira.plugin.system.workflow:user-permission-validator";
        Map<String, String> configFormParams = new HashMap<>();
        configFormParams.put("vars.key", username);
        configFormParams.put("permissionKey", permissionKey);
        configFormParams.put("nullallowed", nullAllowed);

        copyWorkflowAndAddTransitions();
        administration
                .workflows()
                .goTo()
                .workflowSteps(WORKFLOW_COPIED)
                .editTransition(stepId, transitionId)
                .addWorkflowValidator(workflowValidatorKey, configFormParams);
    }

    @Test
    public void shouldEscapeUsernameWhenNullIsForbidden() {
        shouldEscapeUsername("false");
    }
}
