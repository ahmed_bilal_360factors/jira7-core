package com.atlassian.jira.functest.framework.navigation;

import com.google.inject.ImplementedBy;

/**
 * Navigation for comments
 *
 * @since v6.5
 */
@ImplementedBy(CommentNavigationImpl.class)
public interface CommentNavigation {
    void enableCommentGroupVisibility(final Boolean enable);
}
