package com.atlassian.jira.webtests.ztests.bulk;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperations;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.GlobalPermissionKey;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_SUB_TASK;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.STATUS_OPEN;
import static com.atlassian.jira.webtests.Groups.USERS;

@WebTest({Category.FUNC_TEST, Category.BULK_OPERATIONS})
@Restore("IssuesWithSubTasksWorkflowScheme.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestBulkMoveWithMultiContexts extends BaseJiraFuncTest {

    @Inject
    private BulkOperations bulkOperations;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    public void testBulkMoveWithMultiContexts() throws Exception {
        administration.addGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        bulkOperations.bulkChangeChooseIssuesAll();
        navigation.clickOnNext();

        tester.checkCheckbox("operation", "bulk.move.operation.name");
        navigation.clickOnNext();

        tester.assertTextPresent("Select Projects and Issue Types");
        navigation.issue().selectProject(PROJECT_MONKEY, "10000_1_pid");
        navigation.clickOnNext();

        tester.assertTextPresent("Select Projects and Issue Types for Sub-Tasks");
        tester.assertTextPresent("Super Sub Task");
        tester.assertTextPresent("Mega Sub Task");
        navigation.issue().selectIssueType(ISSUE_TYPE_SUB_TASK, "10000_6_10001_10000_issuetype");
        navigation.issue().selectIssueType(ISSUE_TYPE_SUB_TASK, "10000_7_10001_10000_issuetype");
        navigation.clickOnNext();

        tester.assertTextPresent("Map Status for Target Project 'monkey' - Issue Type 'Bug'");
        tester.selectOption("10000", STATUS_OPEN);
        navigation.clickOnNext();

        tester.assertTextPresent("All field values will be retained");
        navigation.clickOnNext();

        tester.assertTextPresent("Map Status for Target Project 'monkey' - Issue Type 'Sub-task'");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Current Status", "Mega Open");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Current Status", "Super Open");
        navigation.clickOnNext();

        tester.assertTextPresent("All field values will be retained");
        navigation.clickOnNext();

        tester.assertTextPresent("Confirmation");
        navigation.clickOnNext();

        bulkOperations.waitAndReloadBulkOperationProgressPage();

        // Check that everything has moved correctly
        tester.assertTextPresent("Issue Navigator");
        tester.assertTextPresent("MKY-1");
        tester.clickLinkWithText("MKY-1");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Status", STATUS_OPEN);
        tester.clickLinkWithText("Super Sub Task Issue");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Type", ISSUE_TYPE_SUB_TASK);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), "Status", STATUS_OPEN);
        tester.assertTextPresent(PROJECT_MONKEY);
        administration.removeGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
    }

    @Test
    public void testBulkMoveIssueWithoutVersionPermission() {
        administration.restoreData("TestMoveIssueWithoutVersionPermission.xml");
        navigation.issue().gotoIssue("HSP-1");
        tester.assertTextPresent("Test issue 1");
        tester.assertTextPresent("New Version 1");
        tester.assertTextPresent("New Version 4");

        navigation.issueNavigator().displayAllIssues();

        //do a bulk move of HSP-1 to the monkey project.
        bulkOperations.bulkChangeIncludeAllPages();
        tester.checkCheckbox("bulkedit_10000", "on");
        tester.submit("Next");
        tester.checkCheckbox("operation", "bulk.move.operation.name");
        tester.submit("Next");
        navigation.issue().selectProject("monkey", "10000_1_pid");
        tester.submit("Next");

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Fix Version/s", "The value of this field must be changed to be valid in the target project, but you are not able to update this field in the target project. It will be set to the field's default value for the affected issues."});
        tester.submit("Next");
        tester.submit("Next");

        bulkOperations.waitAndReloadBulkOperationProgressPage();

        tester.clickLinkWithText("MKY-1");
        tester.assertTextPresent("Test issue 1");
        tester.assertTextNotPresent("New Version 1");
        tester.assertTextNotPresent("New Version 4");
    }
}
