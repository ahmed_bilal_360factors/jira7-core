package com.atlassian.jira.webtests.ztests.bulk;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.net.URL;
import java.net.URLEncoder;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.BULK_OPERATIONS, Category.ISSUES})
@LoginAs(user = ADMIN_USERNAME)
public class TestBulkOperationRedirections extends BaseJiraFuncTest {
    private static final int HSP_2_ID = 10020;
    private String returnUrl;

    @Inject
    private Administration administration;

    @Before
    public void setUpTest() {
        administration.restoreData("TestBulkEditIssues.xml");
        returnUrl = "/secure/AboutPage.jspa";
    }

    @Test
    public void testFlowForBulkMigrateWithRedirect() throws Exception {
        gotoPageWithReturnUrl("/views/bulkedit/BulkMigrateDetails.jspa?singleIssueId=" + HSP_2_ID);
        getTester().assertSubmitButtonPresent("Next");
        getTester().assertLinkPresent("cancel");

        getTester().clickLink("cancel");
        assertAtPage(returnUrl);

        gotoPageWithReturnUrl("/views/bulkedit/BulkMigrateDetails.jspa?singleIssueId=" + HSP_2_ID);

        getTester().submit("Next");
        assertNotAtPage(returnUrl); // Since at this point, the redirection URL might have been awkwardly used immediately.

        assertAtPage("BulkMigrateSetFields!default.jspa");
        getTester().assertSubmitButtonPresent("Next");
        getTester().assertLinkPresent("cancel");

        getTester().clickLink("cancel");
        assertAtPage(returnUrl); // The redirection URL was properly stored and recalled.
    }

    private void gotoPageWithReturnUrl(final String url) throws Exception {
        getTester().gotoPage(url + "&returnUrl=" + URLEncoder.encode(returnUrl, "UTF-8"));
    }

    private void assertAtPage(final String page) {
        getAssertions().getTextAssertions().assertTextPresent(getUrl().getPath(), page);
    }

    private void assertNotAtPage(final String page) {
        getAssertions().getTextAssertions().assertTextNotPresent(getUrl().getPath(), page);
    }

    private URL getUrl() {
        return getTester().getDialog().getResponse().getURL();
    }
}
