package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LocatorFactory;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.Groups;
import com.meterware.httpunit.HttpUnitOptions;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

@WebTest({Category.FUNC_TEST, Category.BROWSING})
@LoginAs(user = ADMIN_USERNAME)
public class TestGroupSelector extends BaseJiraFuncTest {
    public static final String GROUP_PICKER_CF_NAME = "mypickerofgroups";
    public static final String INVALID_GROUP_NAME = "invalid_group_name";
    public static final String ISSUE_SUMMARY = "This is my summary";

    private String groupPickerId = null;

    @Inject
    private Administration administration;

    @Inject
    private LocatorFactory locator;

    @Inject
    private TextAssertions textAssertions;

    @Before
    public void setUpTest() {
        administration.restoreBlankInstance();
        HttpUnitOptions.setScriptingEnabled(true);

        groupPickerId = administration.customFields().
                addCustomField("com.atlassian.jira.plugin.system.customfieldtypes:grouppicker", GROUP_PICKER_CF_NAME);
    }

    @Test
    public void testCreateIssueWithGroupPicker() {
        // Start the create issue operation
        navigation.issue().goToCreateIssueForm(null, null);
        tester.setFormElement("summary", ISSUE_SUMMARY);

        // Assert that the group picker is available
        tester.assertLinkPresent(groupPickerId + "-trigger");

        // Attempt to add invalid group name
        tester.setFormElement(groupPickerId, INVALID_GROUP_NAME);
        tester.submit("Create");
        textAssertions.assertTextPresentHtmlEncoded("Could not find group with name '" + INVALID_GROUP_NAME + "'");

        tester.setFormElement(groupPickerId, Groups.USERS);
        tester.submit("Create");

        textAssertions.assertTextPresent(locator.page(), ISSUE_SUMMARY);
        textAssertions.assertTextPresent(locator.page(), GROUP_PICKER_CF_NAME);
        tester.assertTextPresent(Groups.USERS);
    }
}
