package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.HtmlPage;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.webtests.Groups;
import com.google.common.base.Joiner;
import com.meterware.httpunit.HttpUnitOptions;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_EMAIL;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_FULLNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_PASSWORD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BOB_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.webtests.Groups.ADMINISTRATORS;
import static com.atlassian.jira.webtests.Groups.DEVELOPERS;
import static com.atlassian.jira.webtests.Groups.USERS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@WebTest({Category.FUNC_TEST, Category.BROWSING})
@LoginAs(user = ADMIN_USERNAME)

public class TestWatchers extends BaseJiraFuncTest {
    private static final Issue ISSUE = new Issue("HSP-1", 10000L);
    private static final String MANAGE_WATCHERS = "Watchers";

    private static final String ID_WATCH_LINK = "watching-toggle";
    private static final String ID_WATCH_SPAN = "watch-label";
    private static final String ID_VIEW_WATCHERS_LINK = "view-watcher-list";
    private static final String ID_WATCH_DATA = "watcher-data";
    private static final String ID_WATCH_ACTION = "toggle-watch-issue";
    private static final String ID_MANAGE_WATCHERS = "manage-watchers";

    private static final String XPATH_WATCHERS_SPAN_TITLE = String.format("//span[@id='%s']/@title", ID_WATCH_SPAN);

    private static final String MSG_MUST_BE_LOGGED_IN = "You have to be logged in to watch an issue.";
    private static final String MSG_GUEST_PERM = "You must log in to access this page";

    private static final String TITLE_WATCHING = "Stop watching this issue";
    private static final String TITLE_WATCH = "Start watching this issue";


    @Inject
    private FuncTestLogger logger;

    @Inject
    private HtmlPage page;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    @Restore("TestWatchers.xml")
    @LoginAs(user = ADMIN_USERNAME)
    public void testManageWatchers() {
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");

        HttpUnitOptions.setScriptingEnabled(true);
        try {
            watcherOperationRemove();
            watcherOperationDeleteWatcher();
            administration.usersAndGroups().addUser(BOB_USERNAME, BOB_PASSWORD, BOB_FULLNAME, BOB_EMAIL);
            administration.usersAndGroups().addUserToGroup(BOB_USERNAME, Groups.DEVELOPERS);

            watcherOperationViewWatchingWithWatchPermission();
            watcherOperationWithManageandViewWatchingPermission();
            watcherOperationManageWatchingWithWatchPermission();
        } finally {
            HttpUnitOptions.setScriptingEnabled(false);
        }
    }

    @Test
    @Restore("TestWatchers.xml")
    public void testAnonymousUserCannotWatch() {
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
        navigation.logout();
        gotoIssue(ISSUE);
        //Should not be able to watch.
        tester.assertLinkNotPresent(ID_WATCH_LINK);
        tester.assertLinkNotPresent(ID_WATCH_ACTION);
        //Should see this text when we can't watch.
        assertTextPresentInElement(ID_WATCH_SPAN, TITLE_WATCH);
        //Should see this title when we can't watch.
        assertEquals(MSG_MUST_BE_LOGGED_IN, getXpathText(XPATH_WATCHERS_SPAN_TITLE));

        //Make sure the watch count is correct
        assertEquals(0, getWatchCount());

        //Make sure we can't hit the action directly to hack our watch.
        watchIssueDirectly(ISSUE);
        assertions.getTextAssertions().assertTextPresent(new WebPageLocator(tester), MSG_GUEST_PERM);

        //Go to the issue page again
        gotoIssue(ISSUE);

        //Make sure the watch count is still correct.
        assertEquals(0, getWatchCount());
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Test
    @Restore("TestWatchers.xml")
    public void testUserCanWatch() throws Exception {
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");
        gotoIssue(ISSUE);

        assertNotWatching(0);

        //Lets watch the issue and make sure everything is correct.
        tester.clickLink(ID_WATCH_LINK);
        assertWatching(1);

        //Lets try to watch the issue again and make sure this does not work.
        watchIssueDirectly(ISSUE);
        assertWatching(1);

        tester.clickLink(ID_WATCH_LINK);
        assertNotWatching(0);

        unwatchIssueDirectly(ISSUE);
        assertNotWatching(0);
    }

    // JRA-19814
    @Test
    @Restore("TestWatchersCannotAddUserWithNoPerms.xml")
    public void testCannotAddWatchersWhoDontHavePermissionToViewIssue() throws Exception {
        // HSP excludes fred by Browse permission
        _testCannotAddWatchersWhoDontHavePermissionToViewIssue("HSP-1");
        // MKY-1 excludes fred by Security Level
        _testCannotAddWatchersWhoDontHavePermissionToViewIssue("MKY-1");
    }

    @Test
    @Restore("TestWatchersCannotAddUserWithNoPerms.xml")
    public void testAddBadNames() throws Exception {
        navigation.issue().viewIssue("HSP-1");
        tester.clickLink(ID_MANAGE_WATCHERS);

        tester.setWorkingForm("startform");
        // no names in box - should display error
        tester.submit("add");
        textAssertions.assertTextPresent(new WebPageLocator(tester), "You must select a user.");

        tester.setWorkingForm("startform");
        tester.setFormElement("userNames", "nonexistentuser");
        tester.submit("add");
        textAssertions.assertTextPresent(new WebPageLocator(tester), "The user \"nonexistentuser\" could not be found. This user will not be added to the watch list.");

        // even with multiple bad names - any good names should still be added
        tester.setWorkingForm("startform");
        tester.setFormElement("userNames", "a, b, admin, c");
        tester.submit("add");
        textAssertions.assertTextPresent(new WebPageLocator(tester), "The user \"a\" could not be found. This user will not be added to the watch list.");
        textAssertions.assertTextPresent(new WebPageLocator(tester), "The user \"b\" could not be found. This user will not be added to the watch list.");
        textAssertions.assertTextPresent(new WebPageLocator(tester), "The user \"c\" could not be found. This user will not be added to the watch list.");

        // assert that admin is now watching the issue
        tester.assertLinkNotPresentWithText("Watch Issue");
        tester.assertLinkPresentWithText("Stop Watching");
    }

    @Test
    @Restore("TestWatchers.xml")
    public void testBackToIssueLink() {

        gotoIssue(ISSUE);
        tester.clickLink(ID_VIEW_WATCHERS_LINK);

        assertions.assertNodeByIdExists("back-lnk");
        tester.clickLink("back-lnk");

        assertions.assertNodeByIdExists(ID_VIEW_WATCHERS_LINK);
    }

    private void _testCannotAddWatchersWhoDontHavePermissionToViewIssue(final String issueKey) {
        navigation.issue().viewIssue(issueKey);
        tester.clickLink(ID_MANAGE_WATCHERS);

        // assert that admin is not yet watching the issue
        tester.assertLinkPresentWithText("Watch Issue");
        tester.assertLinkNotPresentWithText("Stop Watching");

        tester.setWorkingForm("startform");

        // user 'fred' doesn't have permission to browse project HSP, but admin does so he will be added
        tester.setFormElement("userNames", "fred, admin");
        tester.submit("add");

        // assert error for fred
        textAssertions.assertTextPresent(new WebPageLocator(tester), "The user \"fred\" does not have permission to view this issue. This user will not be added to the watch list.");

        // assert that admin is now watching the issue
        tester.assertLinkNotPresentWithText("Watch Issue");
        tester.assertLinkPresentWithText("Stop Watching");
    }

    /**
     * Tests the ability to view and remove watchers from the watcher list
     */
    private void watcherOperationRemove() {
        logger.log("Watcher Operation: Test the ability to manage watchers");

        // Test if it is possible to watch
        gotoIssue(ISSUE);
        startWatchingAnIssue();
        tester.clickLink(ID_MANAGE_WATCHERS);
        tester.assertTextPresent(MANAGE_WATCHERS);
        tester.assertLinkPresent("watcher_link_" + ADMIN_USERNAME);
        tester.checkCheckbox("all");
        tester.assertCheckboxSelected("all");
        tester.getDialog().setWorkingForm("stopform");
        tester.submit();
        tester.assertTextPresent("There are no watchers.");
    }

    /**
     * Test that a user's watcher association is removed when the user is deleted
     */
    private void watcherOperationDeleteWatcher() {
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        logger.log("Watcher Operation: Test that a user's watcher association is removed when the user is deleted");
        gotoIssue(ISSUE);
        startWatchingAnIssue(BOB_USERNAME, ADMIN_USERNAME);
        backdoor.usersAndGroups().deleteUser(BOB_USERNAME);
        gotoIssue(ISSUE);
        tester.clickLink(ID_VIEW_WATCHERS_LINK);
        tester.assertLinkNotPresent("watcher_link_" + BOB_USERNAME);
        tester.assertLinkPresent("watcher_link_" + ADMIN_USERNAME);
        gotoIssue(ISSUE);
        stopWatchingAnIssue();
    }

    /**
     * Test the availabilty of the 'View Watchers' Link with 'View Voters and Watchers' Permission"
     */
    private void watcherOperationViewWatchingWithWatchPermission() {
        logger.log("Watcher Operation: Test the availabilty of the 'View Watchers' Link with 'View Voters and Watchers' Permission");
        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        gotoIssue(ISSUE);
        assertManageLinksPresent();

        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.VIEW_VOTERS_AND_WATCHERS, DEVELOPERS);
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.MANAGE_WATCHERS, ADMINISTRATORS);
        gotoIssue(ISSUE);
        assertManageLinksNotPresent();

        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.VIEW_VOTERS_AND_WATCHERS, DEVELOPERS);
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.MANAGE_WATCHERS, ADMINISTRATORS);
    }

    private void assertManageLinksPresent() {
        tester.assertLinkPresent(ID_VIEW_WATCHERS_LINK);
        tester.assertLinkPresent(ID_MANAGE_WATCHERS);
    }

    private void assertManageLinksNotPresent() {
        tester.assertLinkNotPresent(ID_MANAGE_WATCHERS);
        tester.assertLinkNotPresent(ID_VIEW_WATCHERS_LINK);
    }

    /**
     * Test the availabilty of the 'Manage Watchers' link with the 'Manage Watchers' Permission
     */
    private void watcherOperationManageWatchingWithWatchPermission() {
        logger.log("Watcher Operation: Test the availabilty of the 'Manage Watchers'link with the 'Manage Watchers' Permission");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.VIEW_VOTERS_AND_WATCHERS, DEVELOPERS);

//        startWatchingAnIssue(ISSUE.getKey(), new String[] {FRED_USERNAME});

        // Check if another user cannot view the watchers
        navigation.logout();
        navigation.login(BOB_USERNAME, BOB_PASSWORD);
        gotoIssue(ISSUE);
        assertManageLinksNotPresent();
        navigation.logout();

        // check the proper user CAN view and edit the watchers
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        gotoIssue(ISSUE);
        tester.clickLink(ID_MANAGE_WATCHERS);
        tester.assertTextPresent(MANAGE_WATCHERS);

        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.VIEW_VOTERS_AND_WATCHERS, DEVELOPERS);

        navigation.logout();
        navigation.login(BOB_USERNAME, BOB_PASSWORD);
        gotoIssue(ISSUE);
        tester.clickLink(ID_MANAGE_WATCHERS);
        tester.assertTextNotPresent("Add Watchers");
        navigation.logout();

        // JRA-10308 grant the Manage permission to the reporter and check that not everyone gets it accidentally
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        backdoor.permissionSchemes().addReporterPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.MANAGE_WATCHERS);
        navigation.logout();
        navigation.login(BOB_USERNAME, BOB_PASSWORD);
        gotoIssue(ISSUE);
        tester.clickLink(ID_MANAGE_WATCHERS);
        tester.assertTextNotPresent("Add Watchers");
    }

    /**
     * Ensure that a user can view but not manage a watchers list
     */
    private void watcherOperationWithManageandViewWatchingPermission() {
        logger.log("Watcher Operation: Test the difference between viewing and managing watcher lists");
        gotoIssue(ISSUE);
        startWatchingAnIssue();

        // Check if the administrator can manage watch lists

        tester.clickLink(ID_MANAGE_WATCHERS);
        tester.assertFormElementPresent("stopwatch_admin");
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.VIEW_VOTERS_AND_WATCHERS, USERS);

        // Check if the developer (bob) can manage but view watch lists
        navigation.logout();
        navigation.login(BOB_USERNAME, BOB_PASSWORD);
        gotoIssue(ISSUE);
        tester.clickLink(ID_MANAGE_WATCHERS);
        tester.assertFormElementNotPresent("stopwatch_admin");

        navigation.logout();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        gotoIssue(ISSUE);
        stopWatchingAnIssue();
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.VIEW_VOTERS_AND_WATCHERS, USERS);
    }

    private void assertTextPresentInElement(final String elementId, final String expectedText) {
        assertions.getTextAssertions().assertTextPresent(new IdLocator(tester, elementId), expectedText);
    }

    private void assertWatching(final int watcherCount) {
        tester.assertLinkPresent(ID_WATCH_LINK);
        tester.assertLinkPresent(ID_WATCH_ACTION);
        tester.assertTextInElement(ID_WATCH_ACTION, "Stop watching");

        assertTextPresentInElement(ID_WATCH_LINK, TITLE_WATCHING);
        assertEquals(watcherCount, getWatchCount());

        tester.assertTextInElement(ID_WATCH_ACTION, "Stop watching");
    }

    private void assertNotWatching(final int watcherCount) {
        tester.assertTextPresent(ID_WATCH_LINK);
        tester.assertLinkPresent(ID_WATCH_ACTION);
        tester.assertTextInElement(ID_WATCH_ACTION, "Watch");

        //Should see this text when we can watch.
        assertTextPresentInElement(ID_WATCH_LINK, TITLE_WATCH);
        //Make sure the watch count is correct.
        assertEquals(watcherCount, getWatchCount());

        tester.assertTextInElement(ID_WATCH_ACTION, "Watch");
    }

    private void gotoIssue(final Issue issue) {
        navigation.issue().gotoIssue(issue.getKey());
    }

    private String getXpathText(final String xpath) {
        return StringUtils.trimToNull(new XPathLocator(tester, xpath).getText());
    }

    private int getWatchCount() {
        final String s = StringUtils.trimToNull(new IdLocator(tester, ID_WATCH_DATA).getText());
        if (s != null) {
            return Integer.parseInt(s);
        } else {
            fail("Unable to find watch count.");
            return Integer.MIN_VALUE;
        }
    }

    private void watchIssueDirectly(final Issue issue) {
        navigation.gotoPage(page.addXsrfToken(String.format("/secure/VoteOrWatchIssue.jspa?id=%d&watch=watch", issue.getId())));
    }

    private void unwatchIssueDirectly(final Issue issue) {
        navigation.gotoPage(page.addXsrfToken(String.format("/secure/VoteOrWatchIssue.jspa?id=%d&watch=unwatch", issue.getId())));
    }

    public void startWatchingAnIssue() {
        if (tester.getDialog().getResponseText().contains("watch-state-off")) {
            tester.clickLink("toggle-watch-issue");
        }
        assertTrue(tester.getDialog().getResponseText().contains("watch-state-on"));
        tester.assertLinkPresent("toggle-watch-issue");
    }

    private void stopWatchingAnIssue() {
        if (tester.getDialog().getResponseText().contains("watch-state-on")) {
            tester.clickLink("toggle-watch-issue");
        }
        assertTrue(tester.getDialog().getResponseText().contains("watch-state-off"));
    }

    private void startWatchingAnIssue(final String... watchers) {
        tester.clickLink("manage-watchers");

        tester.assertTextPresent("Watchers");

        tester.setFormElement("userNames", Joiner.on(',').join(watchers));
        tester.submit();
    }

    private static class Issue {
        private final String key;
        private final long id;

        private Issue(final String key, final long id) {
            this.key = key;
            this.id = id;
        }

        public String getKey() {
            return key;
        }

        public long getId() {
            return id;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }
}
