package com.atlassian.jira.webtests.ztests.i18n;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.After;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FRED_USERNAME;

/**
 * @since v4.1.1
 */
@WebTest({Category.FUNC_TEST, Category.I18N})
//todo annotation @LoginAs was added automatically - consider removing it if not needed by this test (otherwise remove this comment)
@LoginAs(user = ADMIN_USERNAME)
public class TestTranslateSubTasks extends BaseJiraFuncTest {

    /**
     * A user with no predefined language gets the language options in the system's default language
     */
    @Inject
    private Administration administration;
    @Inject
    private TextAssertions textAssertions;

    @After
    public void tearDownTest() {
        administration.generalConfiguration().setJiraLocaleToSystemDefault();
    }

    @Test
    public void testShowsLanguageListInDefaultLanguage() {
        administration.restoreData("TestTranslateSubTasks.xml");

        administration.subtasks().enable();

        administration.generalConfiguration().setJiraLocale("Deutsch (Deutschland)");

        navigation.gotoAdminSection(Navigation.AdminSection.SUBTASKS);
        tester.clickLink("translate_sub_tasks");

        // assert that the page defaults to German
        textAssertions.assertTextPresent("Sprache: Deutsch (Deutschland)");

        // assert that the list of languages has German selected by default
        assertions.getJiraFormAssertions().assertSelectElementHasOptionSelected("selectedLocale", "Deutsch (Deutschland)");
    }

    /**
     * A user with a language preference that is different from the system's language gets the list of languages in his preferred language.
     */
    @Test
    public void testShowsLanguageListInTheUsersLanguage() {
        administration.restoreData("TestTranslateSubTasks.xml");

        administration.subtasks().enable();

        // set the system locale to something other than English just to be different
        administration.generalConfiguration().setJiraLocale("Deutsch (Deutschland)");

        navigation.login(FRED_USERNAME);

        navigation.gotoAdminSection(Navigation.AdminSection.SUBTASKS);
        tester.clickLink("translate_sub_tasks");

        // assert that the page defaults to Spanish
        textAssertions.assertTextPresent("Lenguaje: espa\u00f1ol (Espa\u00f1a)");

        // assert that the list of languages has Spanish selected by default
        // note that there is a difference with how HttpUnit handles unicode for getResponseText (above) and getSelectedOption()!
        assertions.getJiraFormAssertions().assertSelectElementHasOptionSelected("selectedLocale", "espa\u00f1ol (Espa\u00f1a)");
    }
}
