package com.atlassian.jira.webtests.ztests.bulk;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperations;
import com.atlassian.jira.functest.framework.EditIssueFieldVisibility;
import com.atlassian.jira.functest.framework.Indexing;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.atlassian.jira.webtests.ztests.workflow.ExpectedChangeHistoryItem;
import com.atlassian.jira.webtests.ztests.workflow.ExpectedChangeHistoryRecord;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.BulkOperations.SAME_FOR_ALL;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.BUTTON_NEXT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_IMPROVEMENT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_BUG;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_IMPROVEMENT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_NEWFEATURE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ISSUE_TYPE_SUB_TASK;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PRIORITY_MAJOR;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_MONKEY_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_NEO;
import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.webtests.Groups.USERS;
import static java.util.Collections.emptyList;

@WebTest({Category.FUNC_TEST, Category.BULK_OPERATIONS, Category.ISSUES})
@LoginAs(user = ADMIN_USERNAME)
public class TestBulkMoveIssuesForEnterprise extends BaseJiraFuncTest {

    private static final String CONFIRMATION_TEXT = "Please confirm that the correct changes have been entered.";
    private static final String BULK_EDIT_KEY = "10000_1_";
    private static final String TARGET_PROJECT_ID = BULK_EDIT_KEY + "pid";
    private static final String TARGET_ISSUE_TYPE_ID = BULK_EDIT_KEY + "issuetype";
    private static final String STD_ISSUE_SELECTION = "Select Projects and Issue Types";
    private static final String ENT_UPDATE_FIELDS_PROJECT = "Update Fields for Target Project '";
    private static final String ENT_UPDATE_FIELDS_ISSUE_TYPE = "' - Issue Type '";
    private static final String ENT_UPDATE_FIELDS_END = "'";
    private static final String FIELDS_UPDATE_AUTO = "All field values will be retained";
    @Inject
    private Indexing indexing;
    @Inject
    private EditIssueFieldVisibility editIssueFieldVisibility;
    // JRA-17011: Move a task within the same project. Make sure we don't get any option to change the assignee.
    @Inject
    private BulkOperations bulkOperations;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Inject
    private TextAssertions textAssertions;

    @Test
    @Restore("TestBulkMoveAssigneeChange.xml")
    public void testBulkMoveChangeAssignee() throws Exception {
        //Select the issue to move.
        startBulkMoveForIssue(10000);

        //Move the issue into the same project.
        setTargetProject("Source");

        //Make sure that the assignee drop down is not there.
        assertRetainAssignees();
        tester.submit(BUTTON_NEXT);

        //Check the confirmation screen.
        assertConfirmationScreen();
        tester.submit(BUTTON_NEXT);

        bulkOperations.waitAndReloadBulkOperationProgressPage();

        //Make sure the issue has the right assignee.
        navigation.issue().viewIssue("SRC-5");
        assertions.getViewIssueAssertions().assertAssignee("Source User");
    }

    // This test illustrates JRA-13937 "Bulk Move does not update the Security Level of subtasks".
    @Test
    @Restore("TestBulkMoveSubTasks.xml")
    public void testBulkMoveSubTasks() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        //find all issues
        navigation.issueNavigator().displayAllIssues();
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"RAT-4", "No more milk", "Level Mouse"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"RAT-3", "Get new milk bucket", "Level Mouse"});

        //bulk edit and move RAT-4 to project Bovine.
        bulkOperations.bulkChangeIncludeAllPages();
        tester.checkCheckbox("bulkedit_10033", "on");
        tester.submit(BUTTON_NEXT);
        tester.checkCheckbox("operation", "bulk.move.operation.name");
        tester.submit(BUTTON_NEXT);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Improvement", "Rattus"});
        navigation.issue().selectProject("Bovine", "10022_4_pid");
        tester.submit(BUTTON_NEXT);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Sub-task", "Rattus"});
        tester.submit(BUTTON_NEXT);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Improvement", "Rattus"});
        tester.assertTextPresent("Security Level");
        tester.assertFormElementPresent("security");
        tester.submit(BUTTON_NEXT);
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Sub-task", "Rattus"});
        // The security level of subtasks is inherited from parents.
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Security Level", "The security level of subtasks is inherited from parents."});
        tester.submit(BUTTON_NEXT);
        tester.assertTextPresent("Step 4 of 4: Confirmation");
        tester.submit(BUTTON_NEXT);

        bulkOperations.waitAndReloadBulkOperationProgressPage();

        //check that the security level has been removed, and that the original issues have become
        //COW issues.
        tester.assertTextPresent("Issue Navigator");
        tester.assertTextNotPresent("RAT-4");
        tester.assertTextNotPresent("RAT-3");
        tester.assertTextNotPresent("Level Mouse");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"COW-35", "No more milk"});
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"COW-34", "Get new milk bucket"});
    }

    @Test
    @Restore("TestBulkMoveIssuesForEnterprise.xml")
    public void testBulkMoveIssuesForEnterprise() {
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        logger.log("Bulk Move - Tests for Enterprise Version");
        _testBulkMoveParentIssuesProjectAndIssueType();
        _testBulkMoveSubTaskAndItsParentSelectParents();
        _testMoveSessionTimeouts();
        // uses the blankprojects.xml backup
        _testMoveIssueTypeWithAndWithoutSubtasks();
    }

    private void _testMoveSessionTimeouts() {
        logger.log("Bulk Move - Test that you get redirected to the session timeout page when jumping into the wizard");

        tester.beginAt("secure/views/bulkedit/BulkMigrateDetails.jspa");
        verifyAtSessionTimeoutPage();
        tester.beginAt("secure/views/bulkedit/BulkMigrateChooseSubTaskContext!default.jspa?subTaskPhase=false");
        verifyAtSessionTimeoutPage();
        tester.beginAt("secure/views/bulkedit/BulkMigrateSetFields!default.jspa?subTaskPhase=false");
        verifyAtSessionTimeoutPage();
        tester.beginAt("secure/views/bulkedit/BulkMigrateSetFields!default.jspa?subTaskPhase=true");
        verifyAtSessionTimeoutPage();
        tester.beginAt("secure/views/bulkedit/BulkMigrateSetFields.jspa");
        verifyAtSessionTimeoutPage();
        tester.beginAt("secure/views/bulkedit/BulkMigratePerform.jspa");
        verifyAtSessionTimeoutPage();
    }

    private void verifyAtSessionTimeoutPage() {
        tester.assertTextPresent("Your session timed out while performing bulk operation on issues.");
    }

    // See JRA-9067
    private void _testMoveIssueTypeWithAndWithoutSubtasks() {
        logger.log("Bulk Move - Move multiple issue types where at least one (but not all) issue types do not have sub tasks");
        administration.restoreBlankInstance();
        administration.addGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
        final String issueKey1 = backdoor.issues().createIssue(PROJECT_MONKEY_KEY, "issueKey1", null, PRIORITY_MAJOR, ISSUE_BUG).key();
        final String issueKey2 = backdoor.issues().createIssue(PROJECT_MONKEY_KEY, "issueKey2", null, PRIORITY_MAJOR, ISSUE_IMPROVEMENT).key();
        final String subTaskKey1 = navigation.issue().createSubTask(issueKey1, ISSUE_TYPE_SUB_TASK, "subtask1", "");

        editIssueFieldVisibility.resetFields();
        navigation.issueNavigator().displayAllIssues();

        bulkOperations.bulkChangeIncludeAllPages();
        bulkOperations.bulkChangeChooseIssuesAll();
        bulkOperations.chooseOperationBulkMove();
        tester.assertTextPresent(STD_ISSUE_SELECTION);

        navigation.issue().selectProject(PROJECT_HOMOSAP, "10001_1_pid");
        navigation.issue().selectIssueType(ISSUE_TYPE_BUG, "10001_1_issuetype");

        navigation.issue().selectProject(PROJECT_HOMOSAP, "10001_4_pid");
        navigation.issue().selectIssueType(ISSUE_TYPE_IMPROVEMENT, "10001_4_issuetype");

        tester.submit(BUTTON_NEXT);
        tester.submit(BUTTON_NEXT);

        //retain all values
        tester.assertTextPresent(ENT_UPDATE_FIELDS_PROJECT + PROJECT_HOMOSAP + ENT_UPDATE_FIELDS_ISSUE_TYPE + ISSUE_TYPE_BUG + ENT_UPDATE_FIELDS_END);
        tester.submit(BUTTON_NEXT);
        tester.assertTextPresent(ENT_UPDATE_FIELDS_PROJECT + PROJECT_HOMOSAP + ENT_UPDATE_FIELDS_ISSUE_TYPE + ISSUE_TYPE_SUB_TASK + ENT_UPDATE_FIELDS_END);
        tester.submit(BUTTON_NEXT);
        tester.assertTextPresent(ENT_UPDATE_FIELDS_PROJECT + PROJECT_HOMOSAP + ENT_UPDATE_FIELDS_ISSUE_TYPE + ISSUE_TYPE_IMPROVEMENT + ENT_UPDATE_FIELDS_END);
        tester.submit(BUTTON_NEXT);

        bulkOperations.isStepConfirmation();
        tester.submit(BUTTON_NEXT);
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        navigation.issue().gotoIssue(issueKey1);
        tester.assertLinkPresentWithText(PROJECT_HOMOSAP);
        tester.assertLinkNotPresentWithText(PROJECT_MONKEY);
        navigation.issue().gotoIssue(issueKey2);
        tester.assertLinkPresentWithText(PROJECT_HOMOSAP);
        tester.assertLinkNotPresentWithText(PROJECT_MONKEY);
        navigation.issue().gotoIssue(subTaskKey1);
        tester.assertLinkPresentWithText(PROJECT_HOMOSAP);
        tester.assertLinkNotPresentWithText(PROJECT_MONKEY);
        administration.removeGlobalPermission(GlobalPermissionKey.BULK_CHANGE, USERS);
    }

    private void _testBulkMoveParentIssuesProjectAndIssueType() {
        logger.log("Bulk Move - Test bulk move Parent issues - Move the parents Project and Issue type");

        final String issueKey1 = "NDT-1";
        final String issueId1 = "10000";

        final String issueKey2 = "HSP-1";
        final String issueId2 = "10001";

        final String issueKey3 = "HSP-2";
        final String issueId3 = "10002";

        final String issueKey4 = "NDT-2";
        final String issueId4 = "10003";

        final String issueKey5 = "MKY-1";
        final String issueId5 = "10004";

        backdoor.subtask().enable();
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        bulkOperations.isStepChooseIssues();
        tester.checkCheckbox("bulkedit_" + issueId1);
        tester.checkCheckbox("bulkedit_" + issueId2);
        tester.assertCheckboxNotSelected("bulkedit_" + issueId3);
        tester.assertCheckboxNotSelected("bulkedit_" + issueId4);
        tester.checkCheckbox("bulkedit_" + issueId5);
        tester.submit(BUTTON_NEXT);
        bulkOperations.chooseOperationBulkMove();
        tester.assertTextPresent("Target Project");
        tester.checkCheckbox(SAME_FOR_ALL, BULK_EDIT_KEY);
        navigation.issue().selectProject(PROJECT_HOMOSAP, TARGET_PROJECT_ID);
        navigation.issue().selectIssueType(ISSUE_TYPE_IMPROVEMENT, TARGET_ISSUE_TYPE_ID);
        tester.submit(BUTTON_NEXT);
        tester.assertTextPresent(FIELDS_UPDATE_AUTO);
        tester.submit(BUTTON_NEXT);

        tester.submit(BUTTON_NEXT);
        bulkOperations.waitAndReloadBulkOperationProgressPage();

        //check that the included issues with different projects moved
        assertions.assertLastChangeHistoryRecords(issueKey1, new ExpectedChangeHistoryRecord(
                new ExpectedChangeHistoryItem("Project", PROJECT_NEO, PROJECT_HOMOSAP),
                new ExpectedChangeHistoryItem("Issue Type", ISSUE_TYPE_NEWFEATURE, ISSUE_TYPE_IMPROVEMENT),
                new ExpectedChangeHistoryItem("Key", issueKey1, PROJECT_HOMOSAP_KEY)));
        assertions.assertLastChangeHistoryRecords(issueKey5, new ExpectedChangeHistoryRecord(
                new ExpectedChangeHistoryItem("Project", PROJECT_MONKEY, PROJECT_HOMOSAP),
                new ExpectedChangeHistoryItem("Key", issueKey5, PROJECT_HOMOSAP_KEY)));
        //check that the parents selected has its issue type changed to Improvement
        assertions.assertLastChangeHistoryRecords(issueKey2, new ExpectedChangeHistoryRecord(
                new ExpectedChangeHistoryItem("Issue Type", ISSUE_TYPE_BUG, ISSUE_TYPE_IMPROVEMENT)));
        //check excluded parents and all subtasks remain unchanged
        assertions.assertLastChangeHistoryRecords(issueKey3, emptyList());
        assertions.assertLastChangeHistoryRecords(issueKey4, emptyList());
    }

    /**
     * This tests bulk moving issues with sub-tasks. This verfies that even if the sub-tasks were selected bulk move will
     * unselect them
     */
    private void _testBulkMoveSubTaskAndItsParentSelectParents() {
        logger.log("Bulk Move - Test bulk move subtask and its parent - Move the parents Project and Issue type");

        final String issueKey1 = "NDT-3";
        final String issueId1 = "10010";

        final String subTaskKey1 = "NDT-4";
        final String subTaskId1 = "10011";

        final String subTaskKey2 = "NDT-5";
        final String subTaskId2 = "10012";

        final String issueKey2 = "HSP-3";
        final String issueId2 = "10013";

        final String issueKey3 = "HSP-4";
        final String issueId3 = "10014";

        final String subTaskKey3 = "HSP-5";
        final String subTaskId3 = "10015";

        final String issueKey4 = "NDT-6";
        final String issueId4 = "10016";

        backdoor.subtask().enable();
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();

        bulkOperations.isStepChooseIssues();
        tester.checkCheckbox("bulkedit_" + issueId1);
        tester.checkCheckbox("bulkedit_" + subTaskId1);
        tester.checkCheckbox("bulkedit_" + subTaskId2);
        tester.checkCheckbox("bulkedit_" + issueId2);
        tester.assertCheckboxNotSelected("bulkedit_" + issueId3);
        tester.assertCheckboxNotSelected("bulkedit_" + subTaskId3);
        tester.assertCheckboxNotSelected("bulkedit_" + issueId4);
        tester.submit(BUTTON_NEXT);

        bulkOperations.chooseOperationBulkMove();

        // Editing the parent
        tester.assertTextPresent("Target Project");
        tester.checkCheckbox(SAME_FOR_ALL, BULK_EDIT_KEY);
        navigation.issue().selectProject(PROJECT_HOMOSAP, TARGET_PROJECT_ID);
        navigation.issue().selectIssueType(ISSUE_TYPE_IMPROVEMENT, TARGET_ISSUE_TYPE_ID);
        tester.submit(BUTTON_NEXT);

        // Selecting context for sub-tasks
        tester.assertTextPresent("Select Projects and Issue Types for Sub-Tasks");
        navigation.issue().selectIssueType(ISSUE_TYPE_SUB_TASK, "10001_5_10000_10010_issuetype");
        tester.submit(BUTTON_NEXT);

        // No fields change required for parents
        tester.assertTextPresent(FIELDS_UPDATE_AUTO);
        tester.submit(BUTTON_NEXT);

        // No fields change required for sub-tasks
        tester.assertTextPresent(FIELDS_UPDATE_AUTO);
        tester.submit(BUTTON_NEXT);

        tester.assertTextPresent(CONFIRMATION_TEXT);
        tester.submit(BUTTON_NEXT);

        bulkOperations.waitAndReloadBulkOperationProgressPage();

        //check that the included issues with different projects moved
        assertions.assertLastChangeHistoryRecords(issueKey1, new ExpectedChangeHistoryRecord(
                new ExpectedChangeHistoryItem("Project", PROJECT_NEO, PROJECT_HOMOSAP),
                new ExpectedChangeHistoryItem("Issue Type", ISSUE_TYPE_NEWFEATURE, ISSUE_TYPE_IMPROVEMENT),
                new ExpectedChangeHistoryItem("Key", issueKey1, PROJECT_HOMOSAP_KEY)));

        assertions.assertLastChangeHistoryRecords(subTaskKey1, new ExpectedChangeHistoryRecord(
                new ExpectedChangeHistoryItem("Project", PROJECT_NEO, PROJECT_HOMOSAP),
                new ExpectedChangeHistoryItem("Key", subTaskKey1, PROJECT_HOMOSAP_KEY)));
        assertions.assertLastChangeHistoryRecords(subTaskKey2, new ExpectedChangeHistoryRecord(
                new ExpectedChangeHistoryItem("Project", PROJECT_NEO, PROJECT_HOMOSAP),
                new ExpectedChangeHistoryItem("Key", subTaskKey2, PROJECT_HOMOSAP_KEY)));
        //check that the parents selected has its issue type changed to Improvement
        assertions.assertLastChangeHistoryRecords(issueKey2, new ExpectedChangeHistoryRecord(
                new ExpectedChangeHistoryItem("Issue Type", ISSUE_TYPE_BUG, ISSUE_TYPE_IMPROVEMENT)));
        //check excluded parents and all subtasks remain unchanged
        assertions.assertLastChangeHistoryRecords(issueKey3, emptyList());
        assertions.assertLastChangeHistoryRecords(subTaskKey3, emptyList());
        assertions.assertLastChangeHistoryRecords(issueKey4, emptyList());
        backdoor.subtask().disable();
    }

    @Test
    @Restore("TestBulkMoveIssuesForEnterpriseIssueLevelSecurity.xml")
    public void testBulkMoveIssuesWithSecurityLevels() {
        final String issueKey1 = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "Summary for Bug", null, PRIORITY_MAJOR, ISSUE_TYPE_BUG).key();
        final String issueKey2 = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "Summary for New Feature", null, PRIORITY_MAJOR, ISSUE_TYPE_NEWFEATURE).key();

        navigation.issueNavigator().displayAllIssues();

        bulkOperations.bulkChangeIncludeAllPages();
        bulkOperations.bulkChangeChooseIssuesAll();
        bulkOperations.chooseOperationBulkMove();

        tester.assertTextPresent("Select Projects and Issue Types");
        navigation.issue().selectProject("monkey", "10000_1_pid");
        navigation.issue().selectProject("monkey", "10000_2_pid");
        tester.submit(BUTTON_NEXT);

        // Set the security Levels for the Issues
        tester.selectOption("security", "Medium");
        tester.submit(BUTTON_NEXT);
        tester.selectOption("security", "High");
        tester.submit(BUTTON_NEXT);

        // Assert that both the issues have their security level set on the confirmation page
        tester.assertTextPresent("Medium");
        tester.assertTextPresent("High");
        tester.submit(BUTTON_NEXT);

        bulkOperations.waitAndReloadBulkOperationProgressPage();

        // Check that the issues have the correct Security Level Set
        navigation.issue().gotoIssue(issueKey1);
        tester.assertTextPresent("Medium");
        navigation.issue().gotoIssue(issueKey2);
        tester.assertTextPresent("High");

        //assert the index reflects the bulk move (to project monkey) and retained the security information
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("key", "MKY-6", "security", "Medium"), null, "MKY-6");
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("key", "MKY-7", "security", "High"), null, "MKY-7");

    }

    @Test
    @Restore("TestBulkMoveIssuesForEnterpriseCustomFieldContextBehaviour.xml")
    public void testBulkMoveWithCustomFieldContextBehaviour() {
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();

        // Select two Monkey issues to move.
        tester.checkCheckbox("bulkedit_10011", "on");
        tester.checkCheckbox("bulkedit_10000", "on");
        tester.submit(BUTTON_NEXT);

        // Choose the 'Bulk Move' operation
        tester.checkCheckbox("operation", "bulk.move.operation.name");
        tester.submit(BUTTON_NEXT);

        // Choose The target project 'homosapien'
        navigation.issue().selectProject("homosapien", "10001_1_pid");
        tester.submit(BUTTON_NEXT);

        // Select the target issue type
        navigation.issue().selectIssueType("Sub-task", "10001_5_10000_10000_issuetype");
        tester.submit(BUTTON_NEXT);

        // Choose the default status
        tester.submit(BUTTON_NEXT);

        // Select a value for the Man - Bug Custom field
        tester.assertTextPresent("Man Bug Custom Field");
        tester.selectOption("customfield_10002", "Yes");
        tester.submit(BUTTON_NEXT);

        // Assert that all other fields will remain the same
        tester.assertTextPresent("All field values will be retained");
        tester.submit(BUTTON_NEXT);

        // Click next on the confirmation screen
        tester.submit(BUTTON_NEXT);

        bulkOperations.waitAndReloadBulkOperationProgressPage();

        // go to the issue and confirm the fields have been updated
        navigation.issueNavigator().displayAllIssues();
        tester.clickLinkWithText("Another Monkey Bug!");

        textAssertions.assertTextPresent(new IdLocator(tester, "rowForcustomfield_10001"), "Monkey Option");
        textAssertions.assertTextPresent(new IdLocator(tester, "rowForcustomfield_10002"), "Yes");
    }

    @Test
    @Restore("TestBulkMoveRetainWarningTest.xml")
    public void testBulkMoveRetainWarning() {
        navigation.issueNavigator().displayAllIssues();
        bulkOperations.bulkChangeIncludeAllPages();
        bulkOperations.bulkChangeChooseIssuesAll();
        bulkOperations.chooseOperationBulkMove();
        tester.assertTextPresent(STD_ISSUE_SELECTION);
        navigation.issue().selectProject(PROJECT_MONKEY, TARGET_PROJECT_ID);
        tester.submit(BUTTON_NEXT);

        // JRA-8248: retain checkbox is no longer enabled for Component and Version fields; however there is no way to
        // check if a form control is disabled using JWebUnit, so just assert that the message we used to display is
        // no longer there
        tester.assertTextNotPresent("Issues not in the project <strong>" + PROJECT_MONKEY + "</strong> will not retain values for <strong>Fix Version/s</strong>");
        tester.assertTextNotPresent("Issues not in the project <strong>" + PROJECT_MONKEY + "</strong> will not retain values for <strong>Affects Version/s</strong>");
        tester.assertTextNotPresent("Issues not in the project <strong>" + PROJECT_MONKEY + "</strong> will not retain values for <strong>Component/s</strong>");

        tester.assertTextNotPresent("Issues not in the project <strong>" + PROJECT_MONKEY + "</strong> will not retain values for <strong>Description</strong>");

    }

    //test bulk moving issues and bulk changing the statuses to map to the new project
    @Test
    @Restore("TestBulkMoveMapWorkflows.xml")
    public void testBulkMoveAndChangeStatus() {
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("key", "HSP-13", "type", "Bug", "status", "Totally Open", "summary", "bugs3"), ImmutableMap.of("key", "MKY-13"), "HSP-13");

        //let's move all homosapien issues to monkey and map "Totally Open" to "Open"
        navigation.issueNavigator().createSearch("project=homosapien");
        bulkOperations.bulkChangeIncludeAllPages();
        checkAllIssuesOnPage();
        moveIssuesToMonkey();

        assertStatusMapped("MKY-11"); //make sure that "totally open" is now "open"

        final String movedIssueKey = backdoor.issueNavControl().getIssueKeyForSummary("bugs3");
        //it should now just be "open" in the index
        indexing.assertIndexedFieldCorrect("//item", ImmutableMap.of("key", movedIssueKey, "type", "Bug", "status", "Open", "summary", "bugs3"), ImmutableMap.of("key", "HSP-14"), movedIssueKey);
    }

    private void assertStatusMapped(final String issue) {
        navigation.issue().gotoIssue(issue);
        tester.assertTextPresent("Open");
        tester.assertTextNotPresent("Totally Open");
    }

    private void moveIssuesToMonkey() {
        tester.submit(BUTTON_NEXT);
        tester.checkCheckbox("operation", "bulk.move.operation.name");
        tester.submit(BUTTON_NEXT);
        navigation.issue().selectProject("monkey", "10000_1_pid");

        //accept all the default mappings (including the suggested "totally open" to "open"
        tester.submit(BUTTON_NEXT);
        tester.submit(BUTTON_NEXT);
        tester.submit(BUTTON_NEXT);
        tester.submit(BUTTON_NEXT);
        bulkOperations.waitAndReloadBulkOperationProgressPage();
    }


    private void checkAllIssuesOnPage() {
        tester.checkCheckbox("bulkedit_10022", "on");
        tester.checkCheckbox("bulkedit_10023", "on");
        tester.checkCheckbox("bulkedit_10024", "on");
        tester.checkCheckbox("bulkedit_10025", "on");
    }

    private void startBulkMoveForIssue(final long id) {
        navigation.issueNavigator().displayAllIssues();

        //Goto bulk edit.
        bulkOperations.bulkChangeIncludeAllPages();

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Bulk Operation", "Choose Issues"});
        //select the issue that we want.
        tester.checkCheckbox("bulkedit_" + id, "on");
        tester.submit(BUTTON_NEXT);

        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Bulk Operation", "Choose Operation"});
        tester.checkCheckbox("operation", "bulk.move.operation.name");
        tester.submit(BUTTON_NEXT);
    }

    private void setTargetProject(final String option) {
        assertSelectProjectScreen();
        navigation.issue().selectProject(option, "10010_1_pid");
        tester.submit(BUTTON_NEXT);
    }

    private void assertConfirmationScreen() {
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Move Issues", "Confirmation"});
    }

    private void assertSelectProjectScreen() {
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Move Issues", "Select Projects and Issue Types"});
    }

    private void assertRetainAssignees() {
        tester.assertTextPresent("All field values will be retained");
    }
}
