package com.atlassian.jira.webtests.ztests.help;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.backdoor.application.ApplicationControl;
import com.atlassian.jira.functest.framework.backdoor.application.PlatformApplicationBean;
import com.atlassian.jira.functest.framework.backdoor.application.PluginApplicationBean;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.LicenseKeys;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.TEST_KEY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

@WebTest({Category.FUNC_TEST, Category.LICENSING})
public class TestCoreHelpLinks extends BaseJiraRestTest {
    private static final String TESTUSER = "klimero";
    private final static String TEST_DEFAULT_GROUP_NAME = "jira-test-group";

    private static final String USER_LINK = "jira101";
    private static final String ADMIN_LINK = "embedded.crowd.directory.configure";

    private String testApplicationHelpServerSpaceURI;
    private String coreHelpServerSpaceURI;

    @Test
    public void helpUrlsForUserWithAccessToSingleApplication() {
        assertThat("Anonymous user gets url with core help space",
                getHelpLinkForAnonymous(USER_LINK), containsString(coreHelpServerSpaceURI));

        assertThat("Application user with access to only one application gets url with application help space",
                getHelpLinkForUser(USER_LINK), containsString(testApplicationHelpServerSpaceURI));

        assertThat("Admin help links have the same url (core) for user and anonymous",
                getHelpLinkForAnonymous(ADMIN_LINK), equalTo(getHelpLinkForUser(ADMIN_LINK)));
    }

    @Before
    public void setUp() {
        backdoor.restoreBlankInstance(LicenseKeys.TEST_ROLE);

        final ApplicationControl applicationControl = backdoor.getApplicationControl();
        PluginApplicationBean testApp = applicationControl.getPlugin(TEST_KEY);
        testApplicationHelpServerSpaceURI = testApp.getProductHelpServerSpaceURI();

        PlatformApplicationBean core = applicationControl.getCore();
        coreHelpServerSpaceURI = core.getProductHelpServerSpaceURI();

        backdoor.usersAndGroups().addUser(TESTUSER);
        backdoor.usersAndGroups().addUserToGroup(TESTUSER, TEST_DEFAULT_GROUP_NAME);
    }

    @After
    public void tearDown() {
        backdoor.usersAndGroups().deleteUser(TESTUSER);
    }

    private String getHelpLinkForUser(final String key) {
        return backdoor.helpUrls().loginAs(TESTUSER).getHelpUrl(key);
    }

    private String getHelpLinkForAnonymous(final String key) {
        return backdoor.helpUrls().anonymous().getHelpUrl(key);
    }

}
