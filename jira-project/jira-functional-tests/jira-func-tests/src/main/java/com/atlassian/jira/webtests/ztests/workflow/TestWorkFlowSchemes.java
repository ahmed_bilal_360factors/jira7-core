package com.atlassian.jira.webtests.ztests.workflow;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.Screens;
import com.atlassian.jira.functest.framework.WorkflowSchemes;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.ASSIGN_FIELD_SCREEN;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_PREFIX;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_FIELD_TYPE_TEXTFIELD;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.STATUS_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.STEP_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.TEST_FIELD_SCREEN;
import static com.atlassian.jira.functest.framework.FunctTestConstants.TRANSIION_NAME_APPROVE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.TRANSIION_NAME_CLOSE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.TRANSIION_NAME_REOPEN;
import static com.atlassian.jira.functest.framework.FunctTestConstants.WORKFLOW_ADDED;
import static com.atlassian.jira.functest.framework.FunctTestConstants.WORKFLOW_COPIED;
import static com.atlassian.jira.functest.framework.FunctTestConstants.WORKFLOW_SCHEME;
import static com.atlassian.jira.functest.framework.admin.CustomFields.builtInCustomFieldKey;
import static com.atlassian.jira.functest.framework.admin.CustomFields.numericCfId;

@WebTest({Category.FUNC_TEST, Category.SCHEMES, Category.WORKFLOW})
@Restore("blankWithOldDefault.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestWorkFlowSchemes extends BaseJiraFuncTest {
    private static final String TAB_NAME = "Tab for Testing";
    private static final String CUSTOM_FIELD_NAME = "Approval Rating";
    private static final String CUSTOM_FIELD_NAME_TWO = "Animal";
    private static final String UNSHOWN_STATUS = "Unshown Status";
    String customFieldId;
    String customFieldId2;
    @Inject
    private Screens screens;

    @Inject
    private WorkflowSchemes workflowSchemes;

    @Inject
    private FuncTestLogger logger;

    @Inject
    private Administration administration;

    @Test
    public void testWorkFlowSchemes() {
        customFieldId = numericCfId(administration.customFields().addCustomField(builtInCustomFieldKey(CUSTOM_FIELD_TYPE_TEXTFIELD), CUSTOM_FIELD_NAME));
        customFieldId2 = numericCfId(administration.customFields().addCustomField(builtInCustomFieldKey(CUSTOM_FIELD_TYPE_TEXTFIELD), CUSTOM_FIELD_NAME_TWO));

        final String issueKey = backdoor.issues().createIssue(PROJECT_HOMOSAP_KEY, "test 1", ADMIN_USERNAME, "Minor", "Bug").key();

        screens.addScreen(TEST_FIELD_SCREEN, "");
        backdoor.screens().addFieldToScreen(TEST_FIELD_SCREEN, CUSTOM_FIELD_NAME);

        backdoor.screens().addTabToScreen(TEST_FIELD_SCREEN, TAB_NAME);
        backdoor.screens().addFieldToScreenTab(TEST_FIELD_SCREEN, TAB_NAME, CUSTOM_FIELD_NAME_TWO, "");

        workflowAddScheme();
        workflowAddDuplicateScheme();
        workflowAddInvalidScheme();

        workflowAddWorkflow();
        workflowCopyWorkflow();
        workflowAddDuplicateWorkflow();
        workflowAddInvalidWorkflow();
        workflowAddLinkedStatus();
        workflowAddDuplicateLinkedStatus();
        workflowAddInvalidLinkedStatus();
        workflowAddStep();
        workflowAddTransition();
        workflowAddDuplicateTransition();
        workflowAddInvalidTransition();

        workflowAssignWorkflowSchemeToIssueType();
        workflowAssociateWorkflowSchemeToProject();

        workflowPerformAction(issueKey);

        _testNoActiveStatus();

        administration.project().associateWorkflowScheme(PROJECT_HOMOSAP, "Default");
        workflowSchemes.waitForSuccessfulWorkflowSchemeMigration(PROJECT_HOMOSAP, "Default");
        workflowDeleteScheme();

        workflowDeleteTransition();
        workflowDeleteStep();
        workflowDeleteLinkedStatus();

        workflowDeleteWorkflow();

        navigation.issue().deleteIssue(issueKey);
        screens.removeAllRemainingFieldScreens();
        administration.customFields().removeAllCustomFields();
    }

    @Test
    public void testJRADEV7692() {
        administration.restoreBlankInstance();

        workflowAddScheme();
        workflowAddWorkflow();
        workflowCopyWorkflow();
        workflowAssignWorkflowSchemeToIssueType();

        workflowSchemes.addWorkFlowScheme("Scheme7692", "Another tests workflow scheme.");
    }

    private void _testNoActiveStatus() {
        logger.log("Workflow Schemes: Non-active status should not be displayed if it's not active");

        administration.statuses().addLinkedStatus(UNSHOWN_STATUS, "This status should not be shown in the issue navigator");
        tester.assertTextPresent(UNSHOWN_STATUS);

        navigation.issueNavigator().displayAllIssues();

        tester.assertTextPresent(STATUS_NAME);
        tester.assertTextNotPresent(UNSHOWN_STATUS);

        administration.statuses().deleteLinkedStatus("10001");
        tester.assertTextNotPresent(UNSHOWN_STATUS);
    }

    public void workflowAddScheme() {
        logger.log("Workflow Schemes: Create a workflow scheme");
        workflowSchemes.addWorkFlowScheme(WORKFLOW_SCHEME, "New workflow scheme for testing");
        tester.assertTextPresent("Workflow schemes");
    }

    public void workflowDeleteScheme() {
        logger.log("Workflow Schemes: Delete a workflow scheme");
        workflowSchemes.deleteWorkFlowScheme("10000");
        tester.assertTextPresent("Workflow schemes");
        tester.assertTextNotPresent(WORKFLOW_SCHEME);
    }

    public void workflowAddDuplicateScheme() {
        logger.log("Workflow Schemes: Add a workflow scheme with a duplicate name");
        workflowSchemes.addWorkFlowScheme(WORKFLOW_SCHEME, "");
        tester.assertTextPresent("Add Workflow Scheme");
        tester.assertTextPresent("A Scheme with this name already exists.");
    }

    public void workflowAddInvalidScheme() {
        logger.log("Workflow Schemes: Add a workflow scheme with a invalid name");
        workflowSchemes.addWorkFlowScheme("", "");
        tester.assertTextPresent("Add Workflow Scheme");
        tester.assertTextPresent("Please specify a name for this Scheme.");
    }

    public void workflowAddWorkflow() {
        logger.log("Workflow Schemes: Create a workflow");
        administration.workflows().goTo().addWorkflow(WORKFLOW_ADDED, "New workflow for testing").goTo();

        tester.assertTextPresent(WORKFLOW_ADDED);
    }


    public void workflowDeleteWorkflow() {
        logger.log("Workflow Schemes: Delete a workflow");
        administration.workflows().goTo().delete(WORKFLOW_ADDED);
        tester.assertTextNotPresent(WORKFLOW_ADDED);
        administration.workflows().goTo().delete(WORKFLOW_COPIED);
        tester.assertTextNotPresent(WORKFLOW_COPIED);
    }


    public void workflowCopyWorkflow() {
        logger.log("Workflow Schemes: Copy a workflow");
        administration.workflows().goTo().copyWorkflow("jira", WORKFLOW_COPIED, "Workflow copied from JIRA default");
        tester.assertTextPresent(WORKFLOW_COPIED);
    }

    public void workflowAddDuplicateWorkflow() {
        logger.log("Workflow Schemes: Add a workflow with a duplicate name");
        administration.workflows().goTo().addWorkflow(WORKFLOW_COPIED, "");
        tester.assertTextPresent("A workflow with this name already exists.");
    }

    public void workflowAddInvalidWorkflow() {
        logger.log("Workflow Schemes: Add a workflow with an invalid name");
        administration.workflows().goTo().addWorkflow("", "");
        tester.assertTextPresent("You must specify a workflow name.");
    }

    public void workflowAddLinkedStatus() {
        logger.log("Workflow Schemes: Add a linked status");
        administration.statuses().addLinkedStatus(STATUS_NAME, "The resolution of this issue has been approved");
        tester.assertTextPresent(STATUS_NAME);
    }

    public void workflowDeleteLinkedStatus() {
        logger.log("Workflow Schemes: Delete a linked status");
        administration.statuses().deleteLinkedStatus("10000");
        tester.assertTextNotPresent(STATUS_NAME);
    }

    public void workflowAddDuplicateLinkedStatus() {
        logger.log("Workflow Scheme: Add a linked status with a duplicate name");
        administration.statuses().addLinkedStatus(STATUS_NAME, "");
        tester.assertTextPresent("A status with that name already exists, please enter a different name.");
    }

    public void workflowAddInvalidLinkedStatus() {
        logger.log("Workflow Scheme: Add a linked status with a invalid name");
        administration.statuses().addLinkedStatus("", "");
        tester.assertTextPresent("You must specify a name.");
    }

    public void workflowAddStep() {
        logger.log("Workflow Scheme: Add a step");
        administration.workflows().goTo().workflowSteps(WORKFLOW_COPIED).add(STEP_NAME, STATUS_NAME);
        tester.assertLinkPresentWithText(STEP_NAME);
        tester.assertFormElementNotPresent("stepName");
        tester.assertFormElementNotPresent("stepStatus");
    }

    public void workflowDeleteStep() {
        logger.log("Workflow Scheme: delete a step");
        administration.workflows().goTo().workflowSteps(WORKFLOW_COPIED).deleteStep(STEP_NAME);
        tester.assertLinkNotPresent(STEP_NAME);
        tester.assertFormElementPresent("stepName");
        tester.assertFormElementPresent("stepStatus");
    }

    public void workflowAddTransition() {
        logger.log("Workflow Scheme: Add a transition to a step");
        administration.workflows().goTo().workflowSteps(WORKFLOW_COPIED).addTransition("Resolved", TRANSIION_NAME_APPROVE, "", STEP_NAME, TEST_FIELD_SCREEN);
        tester.assertLinkPresentWithText(TRANSIION_NAME_APPROVE);
        administration.workflows().goTo().workflowSteps(WORKFLOW_COPIED).addTransition(STEP_NAME, TRANSIION_NAME_REOPEN, "", "Open", ASSIGN_FIELD_SCREEN);
        administration.workflows().goTo().workflowSteps(WORKFLOW_COPIED).addTransition(STEP_NAME, TRANSIION_NAME_CLOSE, "", "Closed", null);
    }

    public void workflowDeleteTransition() {
        logger.log("Workflow Scheme: Delete a transition from a Step");
        administration.workflows().goTo().workflowSteps(WORKFLOW_COPIED).deleteTransition("Resolved", TRANSIION_NAME_APPROVE);
        tester.assertLinkNotPresentWithText(TRANSIION_NAME_APPROVE);
        administration.workflows().goTo().workflowSteps(WORKFLOW_COPIED).deleteTransition(STEP_NAME, TRANSIION_NAME_REOPEN);
        administration.workflows().goTo().workflowSteps(WORKFLOW_COPIED).deleteTransition(STEP_NAME, TRANSIION_NAME_CLOSE);
    }

    public void workflowAddDuplicateTransition() {
        logger.log("Workflow Scheme: Add a transition with a duplicate nane");
        administration.workflows().goTo().workflowSteps(WORKFLOW_COPIED).addTransition(STEP_NAME, TRANSIION_NAME_REOPEN, "", "Open", null);
        tester.assertTextPresent("Add Workflow Transition");
        tester.assertTextPresent("Transition with this name already exists for Approved step.");
    }

    public void workflowAddInvalidTransition() {
        logger.log("Workflow Scheme: Add a transition with an invalid name");
        administration.workflows().goTo().workflowSteps(WORKFLOW_COPIED).addTransition(STEP_NAME, "", "", "Open", null);
        tester.assertTextPresent("Add Workflow Transition");
        tester.assertTextPresent("You must enter a valid name.");
    }

    public void workflowAssignWorkflowSchemeToIssueType() {
        logger.log("Workflow Scheme: Assign a workflow scheme to an issue type");
        backdoor.workflowSchemes().assignScheme(10000L, "Bug", WORKFLOW_COPIED);
    }

    public void workflowAssociateWorkflowSchemeToProject() {
        logger.log("Workflow Scheme; Associate a workflow scheme with a project");
        administration.project().associateWorkflowScheme(PROJECT_HOMOSAP, WORKFLOW_SCHEME);
        workflowSchemes.waitForSuccessfulWorkflowSchemeMigration(PROJECT_HOMOSAP, WORKFLOW_SCHEME);
    }

    /* Perform workflow actions using the customised workflow/workflow scheme */
    public void workflowPerformAction(final String issueKey) {
        logger.log("Perform workflow actions using the customised workflow/workflow scheme");
        navigation.issue().viewIssue(issueKey);

        tester.clickLinkWithText("Resolve Issue");
        tester.setWorkingForm("issue-workflow-transition");
        tester.submit("Transition");
        tester.assertTextPresent("Resolved");

        tester.clickLinkWithText(TRANSIION_NAME_APPROVE);
        tester.setWorkingForm("issue-workflow-transition");
        tester.setFormElement(CUSTOM_FIELD_PREFIX + customFieldId, "High");
        tester.clickLinkWithText(TAB_NAME);
        tester.setFormElement(CUSTOM_FIELD_PREFIX + customFieldId2, "Whale");
        tester.submit("Transition");
        tester.assertTextPresent("Approved");

        tester.clickLinkWithText(TRANSIION_NAME_REOPEN);
        tester.setWorkingForm("issue-workflow-transition");
        tester.submit("Transition");
        tester.assertTextPresent("Open");
    }
}
