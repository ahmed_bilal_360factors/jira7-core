package com.atlassian.jira.functest.framework.admin.user;

import com.atlassian.jira.functest.framework.page.AbstractWebTestPage;

/**
 * @since v7.0
 */
public class ViewUserPage extends AbstractWebTestPage {
    @Override
    public String baseUrl() {
        return "/secure/admin/user/ViewUser.jspa";
    }

    public static String generateViewUserQueryParameters(final String username) {
        return String.format("name=%s", username);
    }
}
