package com.atlassian.jira.webtests.ztests.subtask;

import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.AssertRedirect;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.BulkOperationProgress;
import com.atlassian.jira.functest.framework.EditIssueFieldVisibility;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.testkit.client.log.FuncTestLogger;
import com.sun.jersey.api.client.UniformInterfaceException;
import org.junit.After;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;
import java.util.stream.Stream;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.AFFECTS_VERSIONS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.COMPONENTS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_SUB_TASK_SUMMARY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_SUB_TASK_TYPE_DESCRIPTION;
import static com.atlassian.jira.functest.framework.FunctTestConstants.CUSTOM_SUB_TASK_TYPE_NAME;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_ASSIGNEE_ERROR_MESSAGE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_PERM_SCHEME_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIX_VERSIONS_FIELD_ID;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_HOMOSAP;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_NEO;
import static com.atlassian.jira.functest.framework.FunctTestConstants.PROJECT_NEO_KEY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SUB_TASK_DEFAULT_TYPE;
import static com.atlassian.jira.functest.framework.FunctTestConstants.SUB_TASK_SUMMARY;
import static com.atlassian.jira.functest.framework.FunctTestConstants.minorPriority;
import static com.atlassian.jira.functest.framework.admin.TimeTracking.Mode.LEGACY;
import static com.atlassian.jira.permission.ProjectPermissions.ASSIGN_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.MOVE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.SCHEDULE_ISSUES;
import static com.atlassian.jira.webtests.Groups.ADMINISTRATORS;
import static com.atlassian.jira.webtests.Groups.DEVELOPERS;
import static com.atlassian.jira.webtests.Groups.USERS;
import static org.junit.Assert.fail;

@WebTest({Category.FUNC_TEST, Category.SUB_TASKS})
@LoginAs(user = ADMIN_USERNAME)
public class TestCreateSubTasks extends BaseJiraFuncTest {
    private static final String PROJECT_MONKEY_ID = "10001";


    @Inject
    private AssertRedirect assertRedirect;

    @Inject
    private EditIssueFieldVisibility editIssueFieldVisibility;

    @Inject
    private FuncTestLogger logger;
    @Inject
    private Administration administration;
    @Inject
    private TextAssertions textAssertions;
    @Inject
    private BulkOperationProgress bulkOperationProgress;

    @After
    public void tearDown() {
        backdoor.darkFeatures().disableForSite("jira.no.frother.reporter.field");
    }

    @Test
    public void testCreateSubTaskInJiraWithSingleSubTaskType() {
        // TestOneProjectWithOneIssueType.xml is set up with a single project that uses "Bugs Only" issue type scheme,
        // which has only a single "Bug" issue type.
        administration.restoreData("TestOneProjectWithOneIssueType.xml");

        // the number of projects present should not matter, so add a project too
        administration.project().addProject("neanderthal", "NEA", ADMIN_USERNAME);

        // Associate "Bugs & Subtasks" with homosapien project
        Long projectId = backdoor.project().getProjectId("HSP");
        tester.gotoPage("/secure/admin/SelectIssueTypeSchemeForProject!default.jspa?projectId=" + projectId);

        tester.checkCheckbox("createType", "chooseScheme");
        // Select 'Bugs & Sub-tasks' from select box 'schemeId'.
        tester.selectOption("schemeId", "Bugs & Sub-tasks");
        tester.submit();

        navigation.issue().createIssue("homosapien", "Bug", "First issue");
        navigation.issue().viewIssue("HSP-1");

        tester.clickLink("create-subtask");
        tester.assertTextNotPresent("Choose the project and issue type"); // step 1 skipped
        tester.assertTextPresent("Create Sub-Task");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Project", "homosapien", "Issue Type", "Sub-task"});
        tester.assertFormElementPresent("summary");
    }

    @Test
    public void testCreateSubTasks() {
        administration.restoreBlankInstance();
        backdoor.darkFeatures().enableForSite("no.frother.assignee.field");
        backdoor.darkFeatures().enableForSite("jira.no.frother.reporter.field");
        backdoor.darkFeatures().enableForSite("ka.NO_GLOBAL_SHORTCUT_LINKS");

        administration.project().addProject(PROJECT_NEO, PROJECT_NEO_KEY, ADMIN_USERNAME);
        administration.subtasks().addSubTaskType(CUSTOM_SUB_TASK_TYPE_NAME, CUSTOM_SUB_TASK_TYPE_DESCRIPTION);

        editIssueFieldVisibility.resetFields();
        String issueKeyNormal = navigation.issue().createIssue(PROJECT_HOMOSAP, "Bug", "test for sub tasks");

        subTasksWithSubTasksEnabled(issueKeyNormal);
        subTasksCreateSubTaskWithCustonType(issueKeyNormal);
        subTasksWithCreatePermission(issueKeyNormal);
        subTaskWithNoSummary(issueKeyNormal);
        subTaskWithRequiredFields(issueKeyNormal);
        subTaskWithHiddenFields(issueKeyNormal);
        subTaskWithInvalidDueDate(issueKeyNormal);
        subTaskWithSchedulePermission(issueKeyNormal);
        subTaskWithAssignPermission(issueKeyNormal);
        subTaskWithModifyReporterPermission(issueKeyNormal);
        subTaskWithTimeTracking(issueKeyNormal);
        subTaskWithUnassignableUser(issueKeyNormal);
        subTaskMoveIssueWithSubTask(issueKeyNormal);
//        subTaskMoveSubTask(issueKeyNormal);
////            subTaskWithFieldSchemeRequired();
////            subTaskWithFieldSchemeHidden();
//            subTaskCreateSubTaskWithSecurity();

        navigation.issue().deleteIssue(issueKeyNormal);

    }

    @Test
    public void testCreateSubtaskSkipStep1OnlyOneProjectAndOneIssueType() {
        administration.restoreData("TestCreateSubtaskOneProjectOneSubtaskType.xml");
        assertRedirect.assertRedirectAndFollow("/secure/CreateSubTaskIssue!default.jspa?parentIssueId=10000",
                ".*CreateSubTaskIssue\\.jspa\\?parentIssueId=10000&pid=" + PROJECT_MONKEY_ID + "&issuetype=4$");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Create Sub-Task", "Project", "monkey", "Issue Type", "Sub-task", "Summary"});
    }

    @Test
    public void testCreateSubtaskStep1ShowsCorrectIssueTypes() {
        administration.restoreData("IssuesWithSubTasksWorkflowScheme.xml");
        tester.gotoPage("/secure/CreateSubTaskIssue!default.jspa?parentIssueId=10000");

        Stream.of("issuetype-options", "Sub Task", "Super Sub Task", "Mega Sub Task").forEach(textAssertions::assertTextPresent);
    }

    @Test
    public void testCreateIssueSkipStep1IssueTypeSchemeInfersOneProjectAndIssueType() {
        // check the preselection of the project in the form to "current project"
        administration.restoreData("TestCreateMonkeyHasOneIssueType.xml");

        // check we redirect to step 2 if we pass a pid url param for a project which has one issue type
        assertRedirect.assertRedirectAndFollow("/secure/CreateSubTaskIssue!default.jspa?parentIssueId=10000",
                ".*CreateSubTaskIssue\\.jspa\\?parentIssueId=10000&pid=" + PROJECT_MONKEY_ID + "&issuetype=5$");
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Create Sub-Task", "Project", "monkey", "Issue Type", "Sub-task", "Summary"});

        // check that we do not redirect for homosapien issue since there are multiple issue types to choose from
        tester.gotoPage("/secure/CreateSubTaskIssue!default.jspa?parentIssueId=10001"); // HSP-1
        tester.assertFormPresent("subtask-create-start");

        // logout and then rerequest the url
        navigation.logout();
        tester.gotoPage("/secure/CreateSubTaskIssue!default.jspa?parentIssueId=10000"); // MKY-1
        tester.assertTextPresent("You are not logged in");
        tester.clickLinkWithText("Log In");
        tester.setFormElement("os_username", ADMIN_USERNAME);
        tester.setFormElement("os_password", ADMIN_USERNAME);
        tester.setWorkingForm("login-form");
        tester.submit();
        textAssertions.assertTextSequence(tester.getDialog().getResponseText(), new String[]{"Create Sub-Task", "Project", "monkey", "Issue Type", "Sub-task", "Summary"});
    }

    /**
     * Tests if the 'Sub Task' link is available with 'Sub Tasks' enabled
     */
    private void subTasksWithSubTasksEnabled(String issueKey) {
        administration.subtasks().enable();
        logger.log("Sub Task Create: Tests the availability of the 'Sub Task' Link with 'Sub Tasks' enabled");
        navigation.issue().gotoIssue(issueKey);
        tester.assertLinkPresent("create-subtask");

        administration.subtasks().disable();
        navigation.issue().gotoIssue(issueKey);
        tester.assertLinkNotPresent("create-subtask");
    }

    /**
     * Tests the ability to create a sub task using a custom-made sub task type
     */
    private void subTasksCreateSubTaskWithCustonType(String issueKey) {
        logger.log("Sub Task Create: Tests the ability to create a sub task using a custom-made sub task type");
        navigation.issue().createSubTaskStep1(issueKey, CUSTOM_SUB_TASK_TYPE_NAME);

        tester.setFormElement("summary", CUSTOM_SUB_TASK_SUMMARY);
        tester.submit();
        tester.assertTextPresent(CUSTOM_SUB_TASK_SUMMARY);
        tester.assertTextPresent("test for sub tasks");

        // All sub-tasks must be deleted before Sub-Tasks can be deactivated
        deleteCurrentIssue();
        administration.subtasks().disable();
        tester.assertTextPresent("Enable");
    }

    /**
     * Tests if the 'Create Sub Task' Link is available with the 'Create Issue' permission removed
     */
    private void subTasksWithCreatePermission(String issueKey) {
        logger.log("Sub Task Create: Test availability of 'Create Sub Task' link with 'Create Issue' permission.");
        administration.subtasks().enable();
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, CREATE_ISSUES, USERS);
        navigation.issue().gotoIssue(issueKey);
        tester.assertLinkNotPresent("create-subtask");

        // Grant 'Create Issue' permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, CREATE_ISSUES, USERS);
        navigation.issue().gotoIssue(issueKey);
        tester.assertLinkPresent("create-subtask");
        administration.subtasks().disable();
    }


    private void subTaskWithNoSummary(String issueKey) {
        logger.log("Sub Task Create: Adding sub task without summary");
        navigation.issue().createSubTaskStep1(issueKey, SUB_TASK_DEFAULT_TYPE);

        // Not setting summary

        // Need to set priority as this is automatically set
        tester.selectOption("priority", minorPriority);

        tester.submit();
        tester.assertTextPresent("Create Sub-Task");
        tester.assertTextPresent("You must specify a summary of the issue.");
        administration.subtasks().disable();
    }

    /**
     * Makes the fields Components,Affects Versions and Fixed Versions required.
     * Attempts to create a sub task with required fields not filled out and with an invalid assignee
     */
    private void subTaskWithRequiredFields(String issueKey) {
        // Set fields to be required
        editIssueFieldVisibility.setRequiredFields();

        logger.log("Sub Task Create: Test the creation of a sub task using required fields");
        navigation.issue().createSubTaskStep1(issueKey, SUB_TASK_DEFAULT_TYPE);

        tester.setFormElement("summary", "This is a new summary");
        tester.setFormElement("reporter", "");

        tester.submit("Create");

        tester.assertTextPresent("Create Sub-Task");
        tester.assertTextPresent("Component/s is required");
        tester.assertTextPresent("Affects Version/s is required");
        tester.assertTextPresent("Fix Version/s is required");

        // Reset fields to be optional
        editIssueFieldVisibility.resetFields();
        administration.subtasks().disable();
    }

    /**
     * Makes the fields Components,Affects Versions and Fixed Versions hidden.
     */
    private void subTaskWithHiddenFields(String issueKey) {
        // Hide fields
        editIssueFieldVisibility.setHiddenFields(COMPONENTS_FIELD_ID);
        editIssueFieldVisibility.setHiddenFields(AFFECTS_VERSIONS_FIELD_ID);
        editIssueFieldVisibility.setHiddenFields(FIX_VERSIONS_FIELD_ID);

        logger.log("Sub Task Create: Test the creation of a sub task using hidden fields");
        navigation.issue().createSubTaskStep1(issueKey, SUB_TASK_DEFAULT_TYPE);

        tester.assertTextPresent("Create Sub-Task");

        tester.assertLinkNotPresent("components");
        tester.assertLinkNotPresent("versions");
        tester.assertLinkNotPresent("fixVersions");

        // Reset fields to be optional
        editIssueFieldVisibility.resetFields();
        administration.subtasks().disable();
    }


    private void subTaskWithInvalidDueDate(String issueKey) {
        logger.log("Sub Task Create: Creating sub task with invalid due date");
        navigation.issue().createSubTaskStep1(issueKey, SUB_TASK_DEFAULT_TYPE);

        tester.setFormElement("summary", "stuff");
        tester.setFormElement("duedate", "stuff");

        tester.submit("Create");

        tester.assertTextPresent("Create Sub-Task");

        tester.assertTextPresent("You did not enter a valid date. Please enter the date in the format &quot;d/MMM/yy&quot;");
        administration.subtasks().disable();
    }

    /**
     * Tests if the Due Date' field is available with the 'Schedule Issue' permission removed
     */
    private void subTaskWithSchedulePermission(String issueKey) {
        logger.log("Sub Task Create: Test prescence of 'Due Date' field with 'Schedule Issue' permission.");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, SCHEDULE_ISSUES, DEVELOPERS);
        navigation.issue().createSubTaskStep1(issueKey, SUB_TASK_DEFAULT_TYPE);
        tester.assertTextNotPresent("Due Date");
        administration.subtasks().disable();

        // Grant Schedule Issue Permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, SCHEDULE_ISSUES, DEVELOPERS);
        navigation.issue().createSubTaskStep1(issueKey, SUB_TASK_DEFAULT_TYPE);
        tester.assertTextPresent("Due Date");
        administration.subtasks().disable();
    }

    /**
     * Tests if the user is able to assign an issue with the 'Assign Issue' permission removed
     */
    private void subTaskWithAssignPermission(String issueKey) {
        logger.log("Sub Task Create: Test ability to specify assignee with 'Assign Issue' permission.");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGN_ISSUES, DEVELOPERS);
        navigation.issue().createSubTaskStep1(issueKey, SUB_TASK_DEFAULT_TYPE);
        tester.assertFormElementNotPresent("assignee");
        administration.subtasks().disable();

        // Grant Assign Issue Permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ASSIGN_ISSUES, DEVELOPERS);
        navigation.issue().createSubTaskStep1(issueKey, SUB_TASK_DEFAULT_TYPE);
        tester.assertFormElementPresent("assignee");
        administration.subtasks().disable();
    }

    /**
     * Tests if the 'Reporter' Option is available with the 'Modify Reporter' permission removed
     */
    private void subTaskWithModifyReporterPermission(String issueKey) {
        logger.log("Sub Task Create: Test availability of Reporter with 'Modify Reporter' permission.");
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.MODIFY_REPORTER, ADMINISTRATORS);
        navigation.issue().createSubTaskStep1(issueKey, SUB_TASK_DEFAULT_TYPE);
        tester.assertFormElementNotPresent("reporter");
        administration.subtasks().disable();

        // Grant Modify Reporter Permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.MODIFY_REPORTER, ADMINISTRATORS);
        navigation.issue().createSubTaskStep1(issueKey, SUB_TASK_DEFAULT_TYPE);
        tester.assertFormElementPresent("reporter");
        administration.subtasks().disable();
    }

    /**
     * Tests if the 'Orignial Estimate' Link is available with Time Tracking activated
     */
    private void subTaskWithTimeTracking(String issueKey) {
        logger.log("Sub task Create: Test availability of the original esitmate field with time tracking activated");
        navigation.issue().createSubTaskStep1(issueKey, SUB_TASK_DEFAULT_TYPE);
        tester.assertFormElementNotPresent("timetracking");
        administration.subtasks().disable();

        administration.timeTracking().enable(LEGACY);
        navigation.issue().createSubTaskStep1(issueKey, SUB_TASK_DEFAULT_TYPE);
        tester.assertFormElementPresent("timetracking");
        administration.timeTracking().disable();
        administration.subtasks().disable();
    }

    /**
     * Tests if the user is able to assign an issue with the 'Assignable User' permission removed
     */
    private void subTaskWithUnassignableUser(String issueKey) {
        logger.log("Sub Task Create: Attempt to set the assignee to be an unassignable user ...");

        // Remove assignable permission
        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGNABLE_USER, DEVELOPERS);
        navigation.issue().createSubTaskStep1(issueKey, SUB_TASK_DEFAULT_TYPE);
        tester.submit("Create");

        tester.assertTextPresent(DEFAULT_ASSIGNEE_ERROR_MESSAGE);

        //Restore permission
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.ASSIGNABLE_USER, DEVELOPERS);
        administration.subtasks().disable();
    }

    /**
     * Tests if the sub test is moved with the issue to a different project
     */
    private void subTaskMoveIssueWithSubTask(String issueKey) {
        logger.log("Sub Task Move: Move issue with a sub task");
        navigation.issue().createSubTaskStep1(issueKey, SUB_TASK_DEFAULT_TYPE);
        tester.setFormElement("summary", SUB_TASK_SUMMARY);
        tester.submit();
        tester.assertTextPresent(SUB_TASK_SUMMARY);
        tester.assertTextPresent("test for sub tasks");

        // Move parent issue
        try {
            //Try adding permission.
            backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, MOVE_ISSUES, DEVELOPERS);
        } catch (UniformInterfaceException e) {
            //Continue even if it fails, permission entity may have already been added.
        }

        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("move-issue");
        navigation.issue().selectProject(PROJECT_NEO, "10000_1_pid");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        tester.submit("Next");
        bulkOperationProgress.waitAndReloadBulkOperationProgressPage();

        // Check the result
        navigation.issue().gotoIssue(issueKey);
        tester.clickLinkWithText(SUB_TASK_SUMMARY);
        tester.assertTextPresent(PROJECT_NEO);

        // restore to orignal settings
        deleteCurrentIssue();
        administration.subtasks().disable();

    }

    /**
     * Tests if a user can move a sub task to a different sub task type
     */
    private void subTaskMoveSubTask(String issueKey) {
        String subTaskKey;
        logger.log("Sub Task Move; Move a sub task to a different sub task type.");
        navigation.issue().createSubTaskStep1(issueKey, SUB_TASK_DEFAULT_TYPE);
        tester.setFormElement("summary", SUB_TASK_SUMMARY);
        tester.submit();
        tester.assertTextPresent(SUB_TASK_SUMMARY);
        tester.assertTextPresent("test for sub tasks");

        String text;

        try {
            text = tester.getDialog().getResponse().getText();
            int projectIdLocation = text.indexOf("[" + PROJECT_NEO_KEY) + 1;
            int endOfIssueKey = text.indexOf("]", projectIdLocation);
            subTaskKey = text.substring(projectIdLocation, endOfIssueKey);
        } catch (IOException t) {
            fail("Unable to obtain sub-task key");
            return;
        }

        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERM_SCHEME_ID, MOVE_ISSUES, DEVELOPERS);
        navigation.issue().gotoIssue(subTaskKey);
        tester.assertLinkNotPresent("move-issue");

        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, MOVE_ISSUES, DEVELOPERS);
        navigation.issue().gotoIssue(subTaskKey);
        tester.clickLink("move-issue");
        navigation.issue().selectIssueType(CUSTOM_SUB_TASK_TYPE_NAME, "issuetype");
        tester.getDialog().setWorkingForm("jiraform");
        tester.submit();

        tester.assertTextPresent("Step 2 of 3");
        tester.getDialog().setWorkingForm("jiraform");
        tester.submit();

        tester.assertTextPresent("Step 4 of 4");
        tester.submit("Move");

        tester.assertTextPresent(CUSTOM_SUB_TASK_TYPE_NAME);
        tester.assertTextPresent("Details");

        // restore settings
        navigation.issue().gotoIssue(subTaskKey);
        deleteCurrentIssue();
        administration.subtasks().disable();
    }

    private void deleteCurrentIssue() {
        tester.clickLink("delete-issue");
        tester.setWorkingForm("delete-issue");
        tester.submit("Delete");
    }
}
