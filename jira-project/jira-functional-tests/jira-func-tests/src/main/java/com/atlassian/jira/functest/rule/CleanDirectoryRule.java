package com.atlassian.jira.functest.rule;

import com.google.common.base.Supplier;
import org.apache.commons.io.FileUtils;
import org.junit.rules.ExternalResource;

import java.io.File;
import java.io.IOException;

import static com.atlassian.fugue.Suppliers.ofInstance;
import static com.google.common.base.Suppliers.memoize;

/**
 * Cleans up a directory before and after a test.
 *
 * @since v6.4
 */
class CleanDirectoryRule extends ExternalResource {
    private Supplier<File> directory;

    CleanDirectoryRule(final File directory) {
        this.directory = ofInstance(directory);
    }

    CleanDirectoryRule(final java.util.function.Supplier<File> directory) {
        this.directory = memoize(directory::get);
    }

    @Override
    protected void before() throws Throwable {
        super.before();
        cleanDirectory();
    }

    private void cleanDirectory() {
        File directory = this.directory.get();
        try {
            if (directory.exists() && directory.isDirectory()) {
                FileUtils.cleanDirectory(directory);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void after() {
        super.after();
        cleanDirectory();
    }

}
