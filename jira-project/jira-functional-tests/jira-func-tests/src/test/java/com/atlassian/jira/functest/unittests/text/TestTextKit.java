package com.atlassian.jira.functest.unittests.text;

import com.atlassian.jira.functest.framework.util.text.TextKit;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


/**
 * Test for TestKit.
 *
 * @since v3.13
 */
public class TestTextKit {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void testCollapseWhitespace() {
        assertNull(TextKit.collapseWhitespace(null));
        assertEquals("", TextKit.collapseWhitespace(""));
        assertEquals("", TextKit.collapseWhitespace("     \n  \t "));
        assertEquals("Hello World.", TextKit.collapseWhitespace("     \n  \t Hello\n\n\nWorld.  "));
    }

    @Test
    public void testEqualsCollapseWhiteSpace() {
        // Equal
        assertTrue(TextKit.equalsCollapseWhiteSpace(null, null));
        assertTrue(TextKit.equalsCollapseWhiteSpace("", ""));
        assertTrue(TextKit.equalsCollapseWhiteSpace(" hello", "hello  "));
        assertTrue(TextKit.equalsCollapseWhiteSpace("hello", " hello   "));
        assertTrue(TextKit.equalsCollapseWhiteSpace("  hello world\n", "hello\t\tworld"));
        // Not Equal
        assertFalse(TextKit.equalsCollapseWhiteSpace(null, ""));
        assertFalse(TextKit.equalsCollapseWhiteSpace("", null));
        assertFalse(TextKit.equalsCollapseWhiteSpace("helloworld", "hello world"));
    }

    @Test
    public void getNumOccurencesNullSubstring() throws Exception {
        exception.expect(IllegalArgumentException.class);
        TextKit.getNumOccurences("A", null);
    }

    @Test
    public void getNumOccurencesNullString() throws Exception {
        exception.expect(IllegalArgumentException.class);
        TextKit.getNumOccurences(null, "A");
    }

    @Test
    public void getNumOccurencesEmptySubstring() throws Exception {
        exception.expect(IllegalArgumentException.class);
        TextKit.getNumOccurences("A", "");
    }

    @Test
    public void getNumOccurences() throws Exception {
        assertEquals(0, TextKit.getNumOccurences("", "A"));
        assertEquals(0, TextKit.getNumOccurences(" asdf asd", "A"));
        assertEquals(1, TextKit.getNumOccurences("Apple", "Apple"));
        assertEquals(2, TextKit.getNumOccurences("AppleAppleasdf", "Apple"));
        assertEquals(2, TextKit.getNumOccurences("BananaAppleApple", "Apple"));
        assertEquals(2, TextKit.getNumOccurences("fresh Apple pie fresh Apple pie", "Apple"));
        // Check for overlaps
        assertEquals(2, TextKit.getNumOccurences("babababa", "baba"));
    }

    @Test
    public void assertContainsTextSequenceNullText() {
        exception.expect(IllegalArgumentException.class);
        TextKit.assertContainsTextSequence(null, new String[]{"a"});
    }

    @Test
    public void assertContainsTextSequenceNullSequence() {
        exception.expect(IllegalArgumentException.class);
        TextKit.assertContainsTextSequence("asdf", null);
    }

    @Test
    public void assertContainsTextSequenceEmptySequence() {
        // Trivially, an empty sequence should always pass
        TextKit.assertContainsTextSequence("", new String[]{});
        TextKit.assertContainsTextSequence("X", new String[]{});
    }

    @Test
    public void assertContainsTextSequenceEmptyText() {
        // Empty String should fail for any non-empty sequence
        exception.expect(AssertionError.class);
        exception.expectMessage("Sequence assertion failed on the 1st member of the expectedTextSequence: 'a'.");
        TextKit.assertContainsTextSequence("", new String[]{"a"});
    }

    @Test
    public void assertContainsTextSequence() {
        TextKit.assertContainsTextSequence("a", new String[]{"a"});
        TextKit.assertContainsTextSequence("aa", new String[]{"a", "a"});
        TextKit.assertContainsTextSequence("cathat", new String[]{"cat", "hat"});
        TextKit.assertContainsTextSequence("cat hat", new String[]{"cat", "hat"});
        TextKit.assertContainsTextSequence("The cat that ate the mouse.", new String[]{"cat", "hat"});
        TextKit.assertContainsTextSequence("The cat that ate the mouse.", new String[]{"The", "hat", "mouse"});
        TextKit.assertContainsTextSequence("AABBCC", new String[]{"AA", "BB", "CC"});
    }

    @Test
    public void assertContainsTextSequenceTextTooShort1() {
        exception.expect(AssertionError.class);
        exception.expectMessage("Sequence assertion failed on the 2nd member of the expectedTextSequence: 'a'.");
        TextKit.assertContainsTextSequence("a", new String[]{"a", "a"});
    }

    @Test
    public void assertContainsTextSequenceTextTooShort2() {
        exception.expect(AssertionError.class);
        exception.expectMessage("Sequence assertion failed on the 3rd member of the expectedTextSequence: 'a'.");
        TextKit.assertContainsTextSequence("aa", new String[]{"a", "a", "a"});
    }

    @Test
    public void assertContainsTextSequenceUnordered() {
        exception.expect(AssertionError.class);
        exception.expectMessage("Sequence assertion failed on the 3rd member of the expectedTextSequence: 'hat'.");
        TextKit.assertContainsTextSequence("The cat that ate the mouse.", new String[]{"The", "mouse", "hat"});
    }

    @Test
    public void assertContainsTextSequenceOverlaps() {
        exception.expect(AssertionError.class);
        exception.expectMessage("Sequence assertion failed on the 2nd member of the expectedTextSequence: 'that'.");
        TextKit.assertContainsTextSequence("cathat", new String[]{"cat", "that"});
    }

    @Test
    public void assertContainsTextSequence3Way() {
        exception.expect(AssertionError.class);
        exception.expectMessage("Sequence assertion failed on the 3rd member of the expectedTextSequence: 'CC'.");
        TextKit.assertContainsTextSequence("BBCCAABB", new String[]{"AA", "BB", "CC"});
    }

    @Test
    public void assertContainsTextSequence4ways() {
        exception.expect(AssertionError.class);
        exception.expectMessage("Sequence assertion failed on the 4th member of the expectedTextSequence: 'AA'.");
        TextKit.assertContainsTextSequence("AA BB CC", new String[]{"AA", "BB", "CC", "AA"});
    }

    @Test
    public void assertContainsTextSequence11ways() {
        exception.expect(AssertionError.class);
        exception.expectMessage("Sequence assertion failed on the 11th member of the expectedTextSequence: 'again'.");
        TextKit.assertContainsTextSequence("the quick brown fox jumped over the lazy dog backwards",
                    new String[]{"the", "quick", "brown", "fox", "jumped", "over", "the", "lazy", "dog", "backwards", "again"});
    }

    @Test
    public void testContainsTextSequenceNullText() throws Exception {
        exception.expect(IllegalArgumentException.class);
        TextKit.containsTextSequence(null, new String[]{"a"});
    }

    @Test
    public void testContainsTextSequenceNullSequence() throws Exception {
        exception.expect(IllegalArgumentException.class);
        TextKit.containsTextSequence("asdf", null);
    }

    @Test
    public void testContainsTextSequence() throws Exception {

        // Trivially, an empty sequence should always pass
        assertTrue(TextKit.containsTextSequence("", new String[]{}));
        assertTrue(TextKit.containsTextSequence("X", new String[]{}));

        assertFalse(TextKit.containsTextSequence("", new String[]{"a"}));

        assertTrue(TextKit.containsTextSequence("a", new String[]{"a"}));

        assertFalse(TextKit.containsTextSequence("a", new String[]{"a", "a"}));

        assertTrue(TextKit.containsTextSequence("aa", new String[]{"a", "a"}));

        assertFalse(TextKit.containsTextSequence("aa", new String[]{"a", "a", "a"}));

        assertTrue(TextKit.containsTextSequence("cathat", new String[]{"cat", "hat"}));

        assertTrue(TextKit.containsTextSequence("cat hat", new String[]{"cat", "hat"}));
        assertTrue(TextKit.containsTextSequence("The cat that ate the mouse.", new String[]{"cat", "hat"}));
        assertTrue(TextKit.containsTextSequence("The cat that ate the mouse.", new String[]{"The", "hat", "mouse"}));

        assertFalse(TextKit.containsTextSequence("The cat that ate the mouse.", new String[]{"The", "mouse", "hat"}));
        assertFalse(TextKit.containsTextSequence("cathat", new String[]{"cat", "that"}));

        // 3 way
        assertTrue(TextKit.containsTextSequence("AABBCC", new String[]{"AA", "BB", "CC"}));

        assertFalse(TextKit.containsTextSequence("BBCCAABB", new String[]{"AA", "BB", "CC"}));
        assertFalse(TextKit.containsTextSequence("AA BB CC", new String[]{"AA", "BB", "CC", "AA"}));

        // Exercise use of "11th"
        assertFalse(TextKit.containsTextSequence("the quick brown fox jumped over the lazy dog backwards",
                new String[]{"the", "quick", "brown", "fox", "jumped", "over", "the", "lazy", "dog", "backwards",
                        "again"}));
    }
}
