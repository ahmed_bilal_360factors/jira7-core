package com.atlassian.jira.functest.unittests.text;

import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.assertions.TextAssertionsImpl;
import org.junit.Test;

import static org.junit.Assert.fail;

/**
 * A test for TextAssertions
 * <p/>
 * TODO add more tests since this deserves special attention.
 * Mark L are you listening?
 * Yes Brad, I am listening.
 *
 * @since v3.13
 */
public class TestTextAssertions {
    static final String QBF = "the quick brown fox jumped over the lazy dog";

    @Test
    public void testAssertTextPresentNumOccurences() throws Exception {
        final TextAssertions textAssertions = new TextAssertionsImpl(null);
        textAssertions.assertTextPresentNumOccurences(QBF, "brown", 1);
        textAssertions.assertTextPresentNumOccurences(QBF, "the", 2);
        ensureAssertionFailed(() -> textAssertions.assertTextPresentNumOccurences(QBF, "the", 3));
        ensureAssertionFailed(() -> textAssertions.assertTextPresentNumOccurences(QBF, "the", 1));
        ensureAssertionFailed(() -> textAssertions.assertTextPresentNumOccurences(QBF, "brown", 2));
    }

    @Test
    public void testBasicStringOperations() throws Exception {
        final TextAssertions textAssertions = new TextAssertionsImpl(null);

        ensureNoAssertionFailed(() -> textAssertions.assertTextPresent(QBF, "lazy"));
        ensureAssertionFailed(() -> textAssertions.assertTextPresent(QBF, "lazi"));
        ensureAssertionFailed(() -> textAssertions.assertTextPresent(QBF, "doggy"));
        ensureAssertionFailed(() -> textAssertions.assertTextNotPresent(QBF, "dog"));
        ensureAssertionFailed(() -> textAssertions.assertTextSequence(QBF, "quick", "brown", "FAX"));
        ensureNoAssertionFailed(() -> textAssertions.assertTextSequence(QBF, "quick", "brown", "fox jumped o", "ver the"));
    }

    @Test
    public void testBasicRegexOperations() throws Exception {
        final TextAssertions textAssertions = new TextAssertionsImpl(null);

        ensureAssertionFailed(() -> textAssertions.assertRegexMatch(QBF, "ly"));
        ensureNoAssertionFailed(() -> textAssertions.assertRegexMatch(QBF, "l.*y"));
        ensureAssertionFailed(() -> textAssertions.assertRegexMatch(QBF, "lazi"));

        ensureAssertionFailed(() -> textAssertions.assertRegexNoMatch(QBF, "lazy"));
        ensureNoAssertionFailed(() -> textAssertions.assertRegexNoMatch(QBF, "tim"));
    }

    private void ensureAssertionFailed(final Runnable assertionRunnable) {
        try {
            assertionRunnable.run();
            // If we get here, then we need to fail, but not let the AssertionFailedError get caught by our catch block.
        } catch (final AssertionError e) {
            // Expected
            return;
        }
        // Wait until we are out of the try..catch block before we fail.
        fail("Did not throw AssertionFailedError as expected");
    }

    private void ensureNoAssertionFailed(final Runnable assertionRunnable) {
        try {
            assertionRunnable.run();
        } catch (final AssertionError e) {
            fail("Unexpected AssertionFailedError");
        }
    }
}
