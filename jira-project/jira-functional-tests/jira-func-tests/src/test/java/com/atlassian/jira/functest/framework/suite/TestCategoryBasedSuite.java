package com.atlassian.jira.functest.framework.suite;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.atlassian.jira.webtests.ztests.misc.TestDatabaseSetup;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class TestCategoryBasedSuite {

    /**
     * Two methods :
     * - testedPackageContainsClassWithSetupCategory
     * - excludeClassesWithCategorySetupAnnotationFromPackage
     *  use real package and classes as its data.
     *  Overtime this data may change and the test will need to be adjusted.
     */
    public static final String TEST_PACKAGE = "com.atlassian.jira.webtests.ztests.misc";

    @Test
    public void testedPackageContainsClassWithSetupCategory() {
        CategoryBasedSuite categoryBasedSuite = new CategoryBasedSuite(TEST_PACKAGE,
                ImmutableSet.of(Category.FUNC_TEST),
                ImmutableSet.of());

        FuncTestSuite testSuite = categoryBasedSuite.get();
        assertThat(testSuite.getFuncTests().stream().filter(cl -> cl.getName().equals(TestDatabaseSetup.class.getName())).findAny().isPresent(), Matchers.is(true));
    }

    @Test
    public void excludeClassesWithCategorySetupAnnotationFromPackage() {
        CategoryBasedSuite categoryBasedSuite = new CategoryBasedSuite(TEST_PACKAGE,
                ImmutableSet.of(Category.FUNC_TEST),
                ImmutableSet.of(Category.SETUP));

        FuncTestSuite testSuite = categoryBasedSuite.get();
        assertThat(testSuite.getFuncTests().stream().filter(cl -> cl.getName().equals(TestDatabaseSetup.class.getName())).findAny().isPresent(), Matchers.is(false));
        assertThatNonOfSelectedTestsHasExcludedCategory(testSuite.getFuncTests(), Category.SETUP);
        assertThatAllTestsHaveCategoryFuncTestAnnotation(testSuite.getFuncTests(), Category.FUNC_TEST);
    }

    @Test
    public void shouldFilterExcludedClasses() {
        CategoryBasedSuite categoryBasedSuite = new CategoryBasedSuite("this.should.be.ignored.in.this.test",
                ImmutableSet.of(Category.FUNC_TEST),
                ImmutableSet.of(Category.SETUP));

        assertThat(categoryBasedSuite.hasCorrectCategoryAnnotation(ClassToExclude.class),Matchers.is(false));
        assertThat(categoryBasedSuite.hasCorrectCategoryAnnotation(ClassToInclude.class),Matchers.is(true));

    }

    @Test
    public void shouldFilterNotIncludedClasses() {
        CategoryBasedSuite categoryBasedSuite = new CategoryBasedSuite("this.should.be.ignored.in.this.test",
                ImmutableSet.of(Category.FUNC_TEST),
                ImmutableSet.of(Category.SETUP));

        assertThat(categoryBasedSuite.hasCorrectCategoryAnnotation(ClassToInclude.class),Matchers.is(true));
        assertThat(categoryBasedSuite.hasCorrectCategoryAnnotation(ClassToNotInclude.class),Matchers.is(false));
    }

    @WebTest({Category.FUNC_TEST, Category.SETUP})
    private class ClassToExclude {}

    @WebTest({Category.FUNC_TEST, Category.ADMINISTRATION})
    private class ClassToInclude {}

    @WebTest({Category.ISSUES, Category.ADMINISTRATION})
    private class ClassToNotInclude {}

    private void assertThatNonOfSelectedTestsHasExcludedCategory(Set<Class<?>> testClasses, Category category)  {
         final Optional<Class<?>> optional = testClasses.stream()
                .filter(cl ->
                        Sets.newHashSet(cl.getAnnotation(WebTest.class).value()).contains(category))
                .findAny();
        if (optional.isPresent()) {
            fail(optional.get().getName() + " is present in suite and contains " + category + " annotation");
        }
    }

    private void assertThatAllTestsHaveCategoryFuncTestAnnotation(Set<Class<?>> testClasses, Category category)  {
        final Optional<Class<?>> optional = testClasses.stream()
                .filter(cl ->
                        !Sets.newHashSet(cl.getAnnotation(WebTest.class).value()).contains(category))
                .findAny();
        if (optional.isPresent()) {
            fail(optional.get().getName() + " is present in suite and does not contain " + category + " annotation");
        }
    }




}