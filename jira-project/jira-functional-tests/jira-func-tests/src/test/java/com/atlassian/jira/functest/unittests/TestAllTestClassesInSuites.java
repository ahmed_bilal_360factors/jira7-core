package com.atlassian.jira.functest.unittests;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.webtests.AcceptanceTestHarness;
import com.atlassian.jira.webtests.zsuites.FuncTestSuiteLdap;
import com.atlassian.jira.webtests.ztests.TestUpgradeExport;
import com.atlassian.jira.webtests.ztests.misc.TestDatabaseSetup;
import com.atlassian.jira.webtests.ztests.misc.TestDefaultJiraDataFromInstall;
import com.atlassian.jira.webtests.ztests.misc.TestJohnsonFiltersWhileNotSetup;
import com.atlassian.jira.webtests.ztests.misc.TestPlatformCompatibility;
import com.atlassian.jira.webtests.ztests.tpm.ldap.TestTpmLdapSyncPerformance10k;
import com.atlassian.jira.webtests.ztests.upgrade.TestUpgradeTasksReindexSuppression;
import com.atlassian.jira.webtests.ztests.upgrade.TestUpgradeXmlData;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.junit.After;
import org.junit.Test;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Set;

import static com.atlassian.collectors.CollectorsUtil.toImmutableSet;
import static com.atlassian.jira.functest.config.FuncSuiteAssertions.collectCaseClasses;
import static com.google.common.collect.Sets.difference;
import static com.google.common.collect.Sets.intersection;
import static com.google.common.collect.Sets.union;
import static java.util.stream.Collectors.toSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;

/**
 * Let's try to make sure we did not forgot to run any test
 *
 * @since v7.2
 */
public class TestAllTestClassesInSuites {

    public static final String ZTESTS_PACKAGE = "com.atlassian.jira.webtests.ztests";

    @After
    public void clearSystemProperties() throws Exception {
        System.clearProperty("jira.edition");
        System.clearProperty("atlassian.test.suite.package");
        System.clearProperty("atlassian.test.suite.includes");
        System.clearProperty("atlassian.test.suite.excludes");
    }

    @Test
    public void testAllJUnit4TestsArePresentInASuite() {
        System.setProperty("jira.edition", "all");
        System.setProperty("atlassian.test.suite.package", ZTESTS_PACKAGE);
        System.setProperty("atlassian.test.suite.includes", Category.FUNC_TEST.toString());
        System.setProperty("atlassian.test.suite.excludes", "TPM");

        final Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage(ZTESTS_PACKAGE))
                .setScanners(new MethodAnnotationsScanner(), new SubTypesScanner()));

        final Set<Class> foundOnPath = reflections
                .getMethodsAnnotatedWith(Test.class)
                .stream()
                .map(Method::getDeclaringClass)
                .map(c -> reflections.getSubTypesOf(c).isEmpty() ? ImmutableSet.of(c) : reflections.getSubTypesOf(c))
                .flatMap(Collection::stream)
                .collect(toSet());


        final ImmutableSet<Class> acceptanceHarness = collectCaseClasses(AcceptanceTestHarness.suite()).stream().collect(toImmutableSet());
        final Sets.SetView<Class> expected = intersection(foundOnPath, union(acceptanceHarness, excludedSuites()));

        assertThat("There are new tests not assigned to AcceptanceTestHarness " +
                "- these will not be run as part of FuncTests build.\n" +
                "To fix this, add the following tests to their corresponding FuncTestSuite.", difference(foundOnPath, expected), empty());
    }

    private Set<Class> excludedSuites() {
        return ImmutableSet.<Class>builder()
                .addAll(FuncTestSuiteLdap.suite())
                .add(TestUpgradeExport.class)
                .add(TestPlatformCompatibility.class)
                .add(TestTpmLdapSyncPerformance10k.class)
                .add(TestUpgradeXmlData.class)
                .add(TestUpgradeTasksReindexSuppression.class)
                .add(TestDefaultJiraDataFromInstall.class)
                .add(TestDatabaseSetup.class)
                .add(TestJohnsonFiltersWhileNotSetup.class)
                .build();
    }
}
