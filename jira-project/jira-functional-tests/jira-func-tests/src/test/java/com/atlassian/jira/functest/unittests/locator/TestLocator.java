package com.atlassian.jira.functest.unittests.locator;

import com.atlassian.jira.functest.framework.locator.AggregateLocator;
import com.atlassian.jira.functest.framework.locator.Locator;
import com.atlassian.jira.functest.framework.locator.TableLocator;
import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.util.text.TextKit;
import com.atlassian.jira.functest.unittests.mocks.MockWebServer;
import com.atlassian.jira.functest.unittests.mocks.MockWebTester;
import com.atlassian.jira.functest.unittests.mocks.XmlParserHelper;
import com.meterware.httpunit.WebTable;
import net.sourceforge.jwebunit.WebTester;
import org.junit.AfterClass;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * A Test for Locators
 *
 * @since v3.13
 */
public class TestLocator {
    private static final String HTML_START = "<html><body>";
    private static final String HTML_END = "</body></html>";
    private static final String HTML_TEXT_SIMPLE = HTML_START + "<span id=\"s1\">some text</span><div>Some text <b> with bold tags</b>   and spaces </div>" + HTML_END;

    @AfterClass
    public static void stopMockWebServer() throws Exception {
        MockWebServer.getInstance().stop();
    }

    @Test
    public void testXpathLocator() throws Exception {
        XPathLocator xpathLocator;
        Node[] nodes;
        String text;

        xpathLocator = newXPathLocator(HTML_TEXT_SIMPLE, "//span");
        nodes = xpathLocator.getNodes();
        assertNotNull(nodes);
        assertEquals(1, nodes.length);
        assertTrue(xpathLocator.hasNodes());
        assertEquals("span", ((Element) nodes[0]).getTagName());

        text = xpathLocator.getHTML();
        assertNotNull(text);
        assertEquals("<span id=\"s1\">some text</span>", text);

        text = xpathLocator.getText();
        assertNotNull(text);
        assertEquals("some text", text);


        xpathLocator = newXPathLocator(HTML_TEXT_SIMPLE, "//div");
        nodes = xpathLocator.getNodes();
        assertNotNull(nodes);
        assertEquals(1, nodes.length);
        assertTrue(xpathLocator.hasNodes());
        assertEquals("div", ((Element) nodes[0]).getTagName());

        text = xpathLocator.getHTML();
        assertNotNull(text);
        assertEquals("<div>Some text <b> with bold tags</b>   and spaces </div>", text);

        text = xpathLocator.getHTML(nodes[0]);
        assertNotNull(text);
        assertEquals("<div>Some text <b> with bold tags</b>   and spaces </div>", text);

        text = xpathLocator.getText();
        assertNotNull(text);
        assertEquals("Some text with bold tags and spaces", text);

        text = xpathLocator.getText(nodes[0]);
        assertNotNull(text);
        assertEquals("Some text with bold tags and spaces", text);

        text = xpathLocator.getRawText();
        assertNotNull(text);
        assertEquals("Some text  with bold tags   and spaces ", text);

        text = xpathLocator.getRawText(nodes[0]);
        assertNotNull(text);
        assertEquals("Some text  with bold tags   and spaces ", text);
    }

    @Test
    public void testWebPageLocator() throws Exception {
        final Locator locator;
        final Node[] nodes;
        String text;

        final MockWebServer webServer = MockWebServer.getInstance();
        webServer.addPage("/", HTML_TEXT_SIMPLE);

        final WebTester webTester = new MockWebTester(webServer);

        // test the collapser wrapping this xpath locator
        locator = new WebPageLocator(webTester);
        nodes = locator.getNodes();
        assertNotNull(nodes);
        assertEquals(1, nodes.length);
        assertTrue(locator.hasNodes());
        assertEquals("html", ((Element) nodes[0]).getTagName());
        text = locator.getHTML();
        assertNotNull(text);
        assertTrue(TextKit.containsTextSequence(text, new String[]{"<span id=\"s1\">some text</span><div>Some text <b> with bold tags</b>   and spaces </div>"}));


        text = locator.getText();
        assertNotNull(text);
        assertEquals(text, "some textSome text with bold tags and spaces");
    }

    @Test
    public void testAggregate() throws Exception {
        final XPathLocator xpathLocator1 = newXPathLocator(HTML_TEXT_SIMPLE, "//span");
        final XPathLocator xpathLocator2 = newXPathLocator(HTML_TEXT_SIMPLE, "//div");

        final AggregateLocator aggregateLocator = new AggregateLocator(xpathLocator1, xpathLocator2);
        final Node[] nodes = aggregateLocator.getNodes();
        assertNotNull(nodes);
        assertNotNull(aggregateLocator.getNode());
        assertEquals(2, nodes.length);
        assertTrue(aggregateLocator.hasNodes());

        final String text = aggregateLocator.getText();
        assertNotNull(text);
        assertThat(text, containsString("some text"));
        assertThat(text, containsString("bold tags"));

        final Node[] nodes2 = aggregateLocator.getNodes();
        assertNotNull(nodes2);
        assertNotNull(aggregateLocator.getNode());
        assertEquals(2, nodes2.length);
        assertTrue(aggregateLocator.hasNodes());
        assertSame(nodes[0], nodes2[0]);
        assertSame(nodes[1], nodes2[1]);
    }

    @Test
    public void testTableLocator() throws Exception {
        final String TABLE_HTML = "<html><body><table id=\"t1\"><tr><td>r0c1</td><td>r0c2</td></tr><tr><td>r1c1</td><td>r1c2</td></tr><tr><td>r2c1</td><td>r2c2</td></tr></table></body></html>";

        String text;
        final MockWebServer webServer = MockWebServer.getInstance();
        webServer.addPage("/table", TABLE_HTML);

        final WebTester webTester = new MockWebTester(webServer);
        webTester.gotoPage("/table");

        final TableLocator tableLocator = new TableLocator(webTester, "t1");
        assertNotNull(tableLocator.getNodes());
        assertEquals(1, tableLocator.getNodes().length);
        assertTrue(tableLocator.hasNodes());
        assertEquals("table", ((Element) tableLocator.getNodes()[0]).getTagName());

        text = tableLocator.getText();
        assertNotNull(text);

        final WebTable webTable = tableLocator.getTable();
        assertNotNull(webTable);
        assertEquals("r0c1", webTable.getCellAsText(0, 0));
        assertEquals("r0c2", webTable.getCellAsText(0, 1));

        text = tableLocator.getText();
        assertNotNull(text);
    }

    @Test
    public void testXpathAttributeSelectSupport() throws Exception {
        final String someHTML = "<html><body><div id=\"1 2\t 3 \t\n4\">div1</div><div id=\"456\" lang=\"es\">div2</div></body></html>";
        XPathLocator xpathLocator = newXPathLocator(someHTML, "//div/@id");
        assertTrue(xpathLocator.hasNodes());
        assertEquals(2, xpathLocator.getNodes().length);

        xpathLocator = newXPathLocator(someHTML, "//div[1]/@id");
        assertTrue(xpathLocator.hasNodes());
        assertEquals(1, xpathLocator.getNodes().length);

        String actualText;
        actualText = xpathLocator.getText();
        assertEquals("1 2  3   4", actualText);

        actualText = xpathLocator.getRawText();
        assertEquals("1 2  3   4", actualText);

        actualText = xpathLocator.getHTML();
        assertEquals("id=\"1 2  3   4\"", actualText);

        xpathLocator = newXPathLocator(someHTML, "//div/@lang");
        assertTrue(xpathLocator.hasNodes());
        assertEquals(1, xpathLocator.getNodes().length);

        xpathLocator = newXPathLocator(someHTML, "//div/@doesntexist");
        assertFalse(xpathLocator.hasNodes());
        assertEquals(0, xpathLocator.getNodes().length);

    }

    private XPathLocator newXPathLocator(final String xmlText, final String xpathExpression) {
        return new XPathLocator(getDOM(xmlText), xpathExpression);
    }

    private Document getDOM(final String xmlText) {
        try {
            return XmlParserHelper.parseXml(xmlText);
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }
}
