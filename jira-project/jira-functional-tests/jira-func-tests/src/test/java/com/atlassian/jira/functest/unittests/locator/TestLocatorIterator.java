package com.atlassian.jira.functest.unittests.locator;

import com.atlassian.jira.functest.framework.locator.Locator;
import com.atlassian.jira.functest.framework.locator.LocatorEntry;
import com.atlassian.jira.functest.framework.locator.LocatorIterator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TestLocatorIterator {

    static String[] TEST_STRS = new String[]{"Alpha", "Beta", "Delta", "Gamma"};
    static Node[] TEST_NODES = createNodes(TEST_STRS);
    static Locator LOCATOR_EMPTY = createLocator(new Node[0]);
    static Locator LOCATOR = createLocator(TEST_NODES);

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void testHasNextOnEmptyLocator() {

        final LocatorIterator iterator = new LocatorIterator(LOCATOR_EMPTY);
        assertFalse(iterator.hasNext());

        exception.expect(NoSuchElementException.class);
        iterator.next();
    }

    @Test
    public void testHasNext() {

        final LocatorIterator iterator = new LocatorIterator(LOCATOR);
        assertTrue(iterator.hasNext());
        for (final Node ignored : TEST_NODES) {
            assertTrue(iterator.hasNext());
            iterator.next();
        }
        assertFalse(iterator.hasNext());

        exception.expect(NoSuchElementException.class);
        iterator.next();
    }

    @Test
    public final void testNext() {
        final LocatorIterator iterator = new LocatorIterator(LOCATOR);
        assertTrue(iterator.hasNext());

        int count = 0;
        for (; iterator.hasNext(); ) {
            final LocatorEntry info = (LocatorEntry) iterator.next();
            assertNotNull(info);
            assertNotNull(info.getNode());
            assertNotNull(info.getText());
            assertEquals(count, info.getIndex());
            count++;
            final int i = info.getIndex();
            assertSame(TEST_NODES[i], info.getNode());
            assertEquals(TEST_STRS[i], info.getText());
        }
        assertFalse(iterator.hasNext());
    }

    @SuppressWarnings("EmptyCatchBlock")
    @Test
    public final void testRemove() {
        final LocatorIterator iterator = new LocatorIterator(LOCATOR);
        do {
            try {
                iterator.remove();
                fail("expecting UnsupportedOperationException");
            } catch (final UnsupportedOperationException e) {
            }
        } while (advance(iterator));
    }

    private boolean advance(LocatorIterator iterator) {
        final boolean hasNext = iterator.hasNext();
        if (hasNext) {
            iterator.next();
        }
        return hasNext;
    }

    private static Locator createLocator(final Node[] nodes) {
        return new Locator() {
            @Override
            public boolean exists() {
                return nodes != null && nodes.length > 0;
            }

            public Node[] getNodes() {
                return nodes;
            }

            public Node getNode() {
                return null;
            }

            public String getText() {
                return null;
            }

            public String getText(final Node node) {
                return node.getNodeValue();
            }

            public Iterator<LocatorEntry> iterator() {
                return null;
            }

            @Override
            public Iterable<LocatorEntry> allMatches() {
                return null;
            }

            public String getHTML() {
                return null;
            }

            public String getRawText() {
                return null;
            }

            public String getRawText(final Node node) {
                return null;
            }

            public String getHTML(final Node node) {
                return null;
            }

            public boolean hasNodes() {
                return getNodes().length == 0;
            }
        };
    }

    private static Text[] createNodes(final String[] textArr) {
        final Text[] text = new Text[textArr.length];
        for (int i = 0; i < textArr.length; i++) {
            final TextHandler handler = new TextHandler(textArr[i]);
            text[i] = (Text) Proxy.newProxyInstance(Text.class.getClassLoader(), new Class[]{Text.class}, handler);
        }
        return text;
    }

    static class TextHandler implements InvocationHandler {
        private final String text;

        public TextHandler(final String text) {
            this.text = text;
        }

        public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
            if ("getNodeValue".equals(method.getName())) {
                return text;
            }
            return null;
        }
    }
}
