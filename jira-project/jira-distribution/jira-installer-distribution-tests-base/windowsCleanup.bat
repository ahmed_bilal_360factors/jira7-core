mkdir buildlogs
copy "C:\Program Files\Atlassian\JIRA-JWI\*.log" buildlogs
copy "C:\Program Files\Atlassian\Application Data\JIRA\.install4j\installation.log" buildlogs
copy "C:\Program Files\Atlassian\Application Data\JIRA\.install4j\files.log" buildlogs
copy "C:\Program Files\Atlassian\Application Data\JIRA\logs\*.log" buildlogs
copy "C:\Program Files\Atlassian\JIRA-JWI\dbconfig.xml" buildlogs
copy "C:\Program Files\Atlassian\JIRA-JWI\log\*.log" buildlogs
"C:\Program Files\Atlassian\Application Data\JIRA\uninstall.exe" -q
rmdir /s/q "C:\Program Files\Atlassian\JIRA-JWI"
rmdir /s/q "C:\Program Files\Atlassian\Application Data\JIRA\"

EXIT /B 0
