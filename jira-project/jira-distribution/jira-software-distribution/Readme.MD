# JIRA Software Distribution

Build JIRA Software WAC installers and distribution using an already released JIRA and JIRA Software OBR.

```
Do not change the versions manually! Use the scripts provided to change versions across this distribution project
```

## How to build a standalone distribution?

 Assuming you want to use JIRA version ```7.0.0-SNAPSHOT```, and JIRA Software version ```7.0.0-int-0005```, and your installers
 go to maven as version ```7.0.0```:

```
 ./set-jira-version.sh 7.0.0-SNAPSHOT
 ./set-application-version.sh 7.0.0-int-0005
 ./set-product-version.sh 7.0.0
 mvn clean package -pl jira-software-standalone-dist -am
 ls jira-software-standalone-dist/target/
```

## Set version scripts

### set-jira-version.sh

This script changes the version of JIRA you pull into your distribution.

This file uses <!-- #-jira-version-# --> marker in pom.xml files.
Do not remove it and mark every <version> where JIRA version is used with it.

### set-application-version.sh

This script changes the version of Software you pull into your distribution.

### set-product-version.sh

This script changes the resulting version of your distribution. Do not use any of the JIRA or Software
version here, because that will make you not able to deploy another release bumping only JIRA or only
the Software application. Use something different, like consecutive numbers.

This file uses <!-- #-product-version-# --> marker in pom.xml files.
Do not remove it and mark every <version> where product version used with it.
