package it.com.atlassian.jira.plugin.software;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.upm.PluginStatus;
import com.atlassian.jira.functest.framework.upm.UpmRestClient;
import com.google.common.collect.ImmutableList;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class PluginIsActiveTest extends BaseJiraRestTest {

    private static final FeatureMatcher<PluginStatus, Boolean> IS_INSTALLED_BY_USER = new FeatureMatcher<PluginStatus, Boolean>(Matchers.is(true), "plugin status shows that plugin is installed by user", "plugin status with installed by user") {
        @Override
        protected Boolean featureValueOf(final PluginStatus actual) {
            return actual.isUserInstalled();
        }
    };
    private static final FeatureMatcher<PluginStatus, Boolean> IS_ENABLED = new FeatureMatcher<PluginStatus, Boolean>(Matchers.is(true), "plugin status shows that plugin is enabled", "plugin status with enabled") {
        @Override
        protected Boolean featureValueOf(final PluginStatus actual) {
            return actual.isEnabled();
        }
    };

    private static UpmRestClient restClient;

    @Before
    public void setUpRestClient() {
        restClient = UpmRestClient.withDefaultAdminCredentials(environmentData.getBaseUrl().toExternalForm());
    }

    @After
    public void destroyRestClient() {
        try {
            restClient.destroy();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static class PluginKeyAndName {
        final String name;
        final String key;

        private PluginKeyAndName(String key, String name) {
            this.name = name;
            this.key = key;
        }
    }

    private static final List<PluginKeyAndName> PLUGINS = new ImmutableList.Builder<PluginKeyAndName>()
            .add(new PluginKeyAndName("com.atlassian.jira.jira-software-application", "JIRA Software Application"))
            .add(new PluginKeyAndName("com.atlassian.jira.plugin.ext.bamboo", "Jira Bamboo"))
            .add(new PluginKeyAndName("com.atlassian.jira.plugins.jira-software-plugin", "Jira software"))
            .add(new PluginKeyAndName("com.atlassian.jirafisheyeplugin", "Jira Fisheye"))
            .add(new PluginKeyAndName("com.pyxis.greenhopper.jira", "Jira Agile"))
            .add(new PluginKeyAndName("com.atlassian.jira.plugins.jira-development-integration-plugin", "JIRA Development Integration"))
            .add(new PluginKeyAndName("com.atlassian.jira.plugins.jira-bitbucket-connector-plugin", "Jira DVCS Connector"))
            .add(new PluginKeyAndName("com.atlassian.jira.plugins.jira-transition-triggers-plugin", "Jira Transitions Triggers"))
            .build();

    @Test
    public void testIfPluginsExists() {
        for (PluginKeyAndName pluginWithName : PLUGINS) {
            PluginStatus pluginStatus = restClient.getPluginStatus(pluginWithName.key);

            Assert.assertThat(pluginWithName.name + " plugin enabled", pluginStatus, IS_ENABLED);
            Assert.assertThat(pluginWithName.name + " installed as user", pluginStatus, IS_INSTALLED_BY_USER);
        }
    }
}

