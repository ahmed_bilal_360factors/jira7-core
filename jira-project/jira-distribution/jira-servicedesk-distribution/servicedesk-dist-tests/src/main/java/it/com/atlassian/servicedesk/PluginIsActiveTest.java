package it.com.atlassian.servicedesk;

import com.atlassian.jira.functest.framework.BaseJiraRestTest;
import com.atlassian.jira.functest.framework.upm.PluginStatus;
import com.atlassian.jira.functest.framework.upm.UpmRestClient;
import com.google.common.collect.ImmutableList;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class PluginIsActiveTest extends BaseJiraRestTest {

    private static final FeatureMatcher<PluginStatus, Boolean> IS_INSTALLED_BY_USER = new FeatureMatcher<PluginStatus, Boolean>(Matchers.is(true), "plugin status shows that plugin is installed by user", "plugin status with installed by user") {
        @Override
        protected Boolean featureValueOf(final PluginStatus actual) {
            return actual.isUserInstalled();
        }
    };
    private static final FeatureMatcher<PluginStatus, Boolean> IS_ENABLED = new FeatureMatcher<PluginStatus, Boolean>(Matchers.is(true), "plugin status shows that plugin is enabled", "plugin status with enabled") {
        @Override
        protected Boolean featureValueOf(final PluginStatus actual) {
            return actual.isEnabled();
        }
    };

    private static UpmRestClient restClient;

    @Before
    public void setUpRestClient() {
        restClient = UpmRestClient.withDefaultAdminCredentials(environmentData.getBaseUrl().toExternalForm());
    }

    @After
    public void destroyRestClient() {
        try {
            restClient.destroy();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static class PluginKeyAndName {
        final String name;
        final String key;

        private PluginKeyAndName(String key, String name) {
            this.name = name;
            this.key = key;
        }
    }

    private static final List<PluginKeyAndName> PLUGINS = new ImmutableList.Builder<PluginKeyAndName>()
            // application
            .add(new PluginKeyAndName("com.atlassian.servicedesk.application", "Servicedesk Application"))
            // product plugins
            .add(new PluginKeyAndName("com.atlassian.servicedesk", "Servicedesk plugin"))
            .add(new PluginKeyAndName("com.atlassian.servicedesk.project-ui", "Servicedesk Project UI Plugin"))
            .add(new PluginKeyAndName("com.atlassian.jira.plugins.workinghours", "Servicedesk Workinghours Plugin"))
            .add(new PluginKeyAndName("com.atlassian.plugin.timedpromise.atlassian-timed-promise-plugin", "Timed Promise Plugin"))
            .add(new PluginKeyAndName("com.atlassian.servicedesk.plugins.automation.servicedesk-automation-plugin", "Servicedesk Automation Plugin"))
            .add(new PluginKeyAndName("com.atlassian.servicedesk.plugins.automation.servicedesk-automation-modules-plugin", "Servicedesk Automation modules Plugin"))
            // utility plugins
            .add(new PluginKeyAndName("com.atlassian.plugins.atlassian-client-resource", "Atlassian Client Resource Plugin"))
            .add(new PluginKeyAndName("com.atlassian.plugins.atlassian-chaperone", "Atlassian Chaperone"))
            .add(new PluginKeyAndName("com.atlassian.jira.jira-email-processor-plugin", "JIRA Email Processor Plugin"))
            .add(new PluginKeyAndName("com.atlassian.pocketknife.api.commons.plugin", "Pocketknife Plugin"))
            .add(new PluginKeyAndName("com.atlassian.servicedesk.approvals-plugin", "Service Desk Approvals Plugin"))
            .add(new PluginKeyAndName("com.atlassian.servicedesk.plugins.automation.servicedesk-automation-then-webhook-plugin", "Service Desk Automation Then-Webhook Plugin"))
            .add(new PluginKeyAndName("com.atlassian.servicedesk.core-ui", "Service Desk Core UI Plugin"))
            .add(new PluginKeyAndName("com.atlassian.servicedesk.servicedesk-knowledge-base-plugin", "Service Desk Knowledge Base Plugin"))
            .add(new PluginKeyAndName("com.atlassian.servicedesk.servicedesk-lingo-plugin", "Service Desk Lingo Plugin"))
            .add(new PluginKeyAndName("com.atlassian.servicedesk.servicedesk-notifications-plugin", "Service Desk Notifications Plugin"))
            .add(new PluginKeyAndName("com.atlassian.servicedesk.servicedesk-reports-plugin", "Service Desk Reports Plugin"))
            .add(new PluginKeyAndName("com.atlassian.servicedesk.plugins.servicedesk-search-plugin", "Service Desk Search Plugin"))
            .add(new PluginKeyAndName("com.atlassian.servicedesk.public-rest-api", "JIRA Service Desk Public REST API Plugin"))
            .add(new PluginKeyAndName("com.atlassian.querydsl.plugins.querydsl-4.0.7-provider-plugin", "querydsl-4.0.7-provider-plugin"))
            .add(new PluginKeyAndName("com.atlassian.psmq", "psmq-plugin"))
            .add(new PluginKeyAndName("com.atlassian.jwt.jwt-plugin", "Atlassian JWT Plugin"))
            .build();

    @Test
    public void testIfPluginsExists() {
        for (PluginKeyAndName pluginWithName : PLUGINS) {
            PluginStatus pluginStatus = restClient.getPluginStatus(pluginWithName.key);

            Assert.assertThat(pluginWithName.name + " plugin enabled", pluginStatus, IS_ENABLED);
            Assert.assertThat(pluginWithName.name + " installed as user", pluginStatus, IS_INSTALLED_BY_USER);
        }
    }
}
