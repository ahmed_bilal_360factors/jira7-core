<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.atlassian.jira</groupId>
        <artifactId>jira-distribution</artifactId>
        <version>7.3.8</version>
        <!-- An empty relative path suppresses the warning when Maven builds the effective model
             http://maven.apache.org/ref/3.0.3/maven-model/maven.html#class_parent -->
        <relativePath />
    </parent>

    <artifactId>jira-core-distribution</artifactId>
    <version>7.3.8</version>
    <packaging>pom</packaging>
    <name>Atlassian JIRA - JIRA Core Distribution</name>

    <licenses>
        <license>
            <name>Atlassian 3.0 End User License Agreement</name>
            <url>http://www.atlassian.com/end-user-agreement/</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <properties>
        <nexus.staging.autoRelease>true</nexus.staging.autoRelease>
    </properties>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>sandbox-maven-plugin</artifactId>
                <version>2.1-beta15</version>
                <executions>
                    <execution>
                        <id>enable-sandbox</id>
                        <phase>validate</phase>
                        <goals>
                            <goal>sandbox</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
        <pluginManagement>
            <plugins>
                <plugin>
                    <artifactId>maven-release-plugin</artifactId>
                    <version>2.5</version>
                    <configuration>
                        <preparationGoals>clean</preparationGoals>
                        <suppressCommitBeforeTag>true</suppressCommitBeforeTag>
                        <remoteTagging>false</remoteTagging>
                        <tagBase>https://svn.atlassian.com/svn/private/atlassian/jira/tags/distribution-tags</tagBase>
                    </configuration>

                    <dependencies>
                        <dependency>
                            <groupId>org.apache.maven.scm</groupId>
                            <artifactId>maven-scm-provider-gitexe</artifactId>
                            <version>1.9</version>
                        </dependency>
                    </dependencies>
                </plugin>
                <plugin>
                    <artifactId>maven-assembly-plugin</artifactId>
                    <version>2.2</version>
                    <configuration>
                        <finalName>${project.build.finalName}-${project.version}</finalName>
                        <tarLongFileMode>gnu</tarLongFileMode>
                        <archive>
                            <compress>true</compress>
                        </archive>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.cargo</groupId>
                    <artifactId>cargo-maven2-plugin</artifactId>
                    <version>${cargo.plugin.version}</version>
                </plugin>
                <plugin>
                    <artifactId>maven-enforcer-plugin</artifactId>
                    <version>1.2</version>
                </plugin>
                <plugin>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>2.5</version>
                    <configuration>
                        <escapeWindowsPaths>true</escapeWindowsPaths>
                    </configuration>
                </plugin>
                <plugin>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>2.5</version>
                </plugin>
            </plugins>
        </pluginManagement>
        <extensions>
            <extension>
                <groupId>org.apache.maven.wagon</groupId>
                <artifactId>wagon-webdav-jackrabbit</artifactId>
                <version>1.0</version>
            </extension>
        </extensions>
    </build>

    <profiles>
        <profile>
            <id>final-distros</id>
            <activation>
                <property>
                    <name>!skip.final.distributions</name>
                </property>
            </activation>
            <modules>
                <module>jira-core-standalone-distribution</module>
            </modules>
        </profile>
        <profile>
            <id>distribution-tests</id>
            <activation>
                <property>
                    <name>jira.core.distribution.tests</name>
                </property>
            </activation>
            <modules>
                <module>jira-core-standalone-tests</module>
                <module>jira-core-installer-tests</module>
            </modules>
        </profile>
        <profile>
            <id>installer</id>
            <activation>
                <property>
                    <name>install4j.home</name>
                </property>
            </activation>
            <modules>
                <module>jira-core-installer-distribution</module>
            </modules>
        </profile>
        <profile>
            <id>nexus-staging</id>
            <activation>
                <property>
                    <name>!sandbox.key</name>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <!--TODO FIXME: remove this on upgrade to parent pom 3.0.94 or later -->
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <version>1.6.5-atlassian-2</version>
                        <extensions>true</extensions>
                        <configuration>
                            <skipStaging>false</skipStaging>
                            <serverId>atlassian-private</serverId>
                            <nexusUrl>https://maven.atlassian.com/staging/atlassian-private/</nexusUrl>
                            <stagingProfileId>atlassian-private-staging</stagingProfileId>
                            <autoReleaseAfterClose>${nexus.staging.autoRelease}</autoReleaseAfterClose>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <!--
        When upgrading to a parent-pom (closedsource-pom) that is greater than version 4.0.10 this can be removed
        but you will also need to change the SOX plan templates to being to use the profile defined in those poms
        at the time of writing that would be swaping -Partifact-signing for -Psox
        This change also needs to be made in the following files
        - pom.xml
        - jira-distribution/pom.xml
        - jira-core-distribution/pom.xml
        -->
        <profile>
            <id>artifact-signing</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>1.6</version>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
