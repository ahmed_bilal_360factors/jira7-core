package com.atlassian.jira.hallelujah;

import com.atlassian.buildeng.hallelujah.HallelujahServer;
import com.atlassian.buildeng.hallelujah.core.JUnitUtils;
import com.atlassian.buildeng.hallelujah.jms.JMSConnectionFactory.DeliveryMode;
import com.atlassian.buildeng.hallelujah.jms.JMSHallelujahServer;
import com.atlassian.jira.functest.framework.suite.SystemPropertyBasedSuite;
import org.junit.runners.model.InitializationError;

import javax.jms.JMSException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class JIRAHallelujahServer {
    public static void main(String[] args) throws IOException, InitializationError {
        System.out.println("JIRA Hallelujah Server starting...");

        System.setProperty("jira.edition", "all");
        final String junitFilename = "TEST-Hallelujah.xml";
        final String suiteName = "AcceptanceTestHarness";

        final SystemPropertyBasedSuite systemPropertyBasedSuite = new SystemPropertyBasedSuite();

        final HallelujahServer hallelujahServer;
        try {
            hallelujahServer = new JMSHallelujahServer.Builder()
                    .setJmsConfig(JIRAHallelujahConfig.getConfiguration())
                    .setSuiteTests(JUnitUtils.flatten(systemPropertyBasedSuite.get().createTest()))
                    .setTestResultFileName(junitFilename)
                    .setSuiteName(suiteName)
                    .setDeliveryMode(DeliveryMode.PERSISTENT)
                    .setMessageExpiry(TimeUnit.HOURS.toMillis(2))
                    .build()
                    .registerListeners(new FailSlowTestsListener(5, TimeUnit.MINUTES));
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }

        final boolean success = hallelujahServer.call();

        System.out.println("JIRA Hallelujah Server finished. " + success + " was the execution result");

        if (!success) {
            System.exit(1);
        }
    }
}


