// $ANTLR 3.5.2 com/atlassian/jira/jql/parser/antlr/Jql.g 2018-02-28 15:34:08

package com.atlassian.jira.jql.parser.antlr;

import org.apache.log4j.Logger;
import com.atlassian.jira.jql.util.JqlStringSupportImpl;
import com.atlassian.jira.jql.parser.JqlParseErrorMessage;
import com.atlassian.jira.jql.parser.JqlParseErrorMessages;
import java.util.List;
import java.util.LinkedList;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class JqlLexer extends Lexer {
	public static final int EOF=-1;
	public static final int AFTER=4;
	public static final int AMPER=5;
	public static final int AMPER_AMPER=6;
	public static final int AND=7;
	public static final int ASC=8;
	public static final int BANG=9;
	public static final int BEFORE=10;
	public static final int BSLASH=11;
	public static final int BY=12;
	public static final int CHANGED=13;
	public static final int COMMA=14;
	public static final int CONTROLCHARS=15;
	public static final int CR=16;
	public static final int CUSTOMFIELD=17;
	public static final int DESC=18;
	public static final int DIGIT=19;
	public static final int DURING=20;
	public static final int EMPTY=21;
	public static final int EQUALS=22;
	public static final int ERRORCHAR=23;
	public static final int ERROR_RESERVED=24;
	public static final int ESCAPE=25;
	public static final int FROM=26;
	public static final int GT=27;
	public static final int GTEQ=28;
	public static final int HEXDIGIT=29;
	public static final int IN=30;
	public static final int IS=31;
	public static final int LBRACKET=32;
	public static final int LIKE=33;
	public static final int LPAREN=34;
	public static final int LT=35;
	public static final int LTEQ=36;
	public static final int MATCHWS=37;
	public static final int MINUS=38;
	public static final int NEGNUMBER=39;
	public static final int NEWLINE=40;
	public static final int NL=41;
	public static final int NOT=42;
	public static final int NOT_EQUALS=43;
	public static final int NOT_LIKE=44;
	public static final int ON=45;
	public static final int OR=46;
	public static final int ORDER=47;
	public static final int PIPE=48;
	public static final int PIPE_PIPE=49;
	public static final int POSNUMBER=50;
	public static final int QUOTE=51;
	public static final int QUOTE_STRING=52;
	public static final int RBRACKET=53;
	public static final int RESERVED_CHARS=54;
	public static final int RPAREN=55;
	public static final int SPACE=56;
	public static final int SQUOTE=57;
	public static final int SQUOTE_STRING=58;
	public static final int STRING=59;
	public static final int STRINGSTOP=60;
	public static final int TO=61;
	public static final int WAS=62;
	public static final int WS=63;

	private static final Logger log = Logger.getLogger(JqlLexer.class);

	private final List<AntlrPosition> stack = new LinkedList<AntlrPosition>();

	private void stripAndSet()
	{
		String text = getText();
		text = text.substring(1, text.length() - 1);
		text = JqlStringSupportImpl.decode(text);
		setText(text);
	}

	private void checkAndSet()
	{
	    final String text = JqlStringSupportImpl.decode(getText());
	    if (JqlStringSupportImpl.isReservedString(text))
	    {
	        JqlParseErrorMessage message = JqlParseErrorMessages.reservedWord(text, state.tokenStartLine, state.tokenStartCharPositionInLine);
	        throw new RuntimeRecognitionException(message);
	    }
	    setText(text);
	}

	@Override
	/** Override this method to change where error messages go */
	public void emitErrorMessage(String msg) 
	{
		log.debug(msg);
	}

	/* START HACK 
	 * We need to get the "lexer" to fail when it detects errors. At the moment ANTLR just drops the input
	 * up until the error and tries again. This can leave antlr actually parsing JQL strings that are not
	 * valid. For example, the string "priority = \kbjb" will actually be parsed as "priority = bjb". 
	 * To stop this we throw a RuntimeRecognitionRuntimeException which the DefaultJqlParser is careful to catch. 
	 * Throwing the RecognitionException  will not work as JqlLexer.nextToken catches this exception and tries 
	 * again (which will cause an infinite loop).
	 *
	 * Antlr (check up to 3.1.3) does not seem to be able to handle the "catch" clause on lexer rules. It throws a RuntimeException
	 * when trying to process the grammar. To get around this we have hacked the error reporting using a "stack" to push
	 * on the rule we currently have an error in. We can use this information to produce a pretty good error message when
	 * the lexer tries to recover though is does make for some pretty strange logic.
	 */

	@Override
	public void recover(RecognitionException re)
	{
	    LexerErrorHelper handler = new LexerErrorHelper(input, peekPosition());
	    handler.handleError(re);
	}

	private void recover()
	{
	    MismatchedSetException e = new MismatchedSetException(null, input);
	    recover(e);
	}

	private void pushPosition(int tokenType)
	{
	    stack.add(0, new AntlrPosition(tokenType, input));
	}

	private AntlrPosition popPosition()
	{
	    return stack.isEmpty() ? null : stack.remove(0);
	}

	private AntlrPosition peekPosition()
	{
	    return stack.isEmpty() ? null : stack.get(0);
	}

	/* END HACK */


	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public JqlLexer() {} 
	public JqlLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public JqlLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "com/atlassian/jira/jql/parser/antlr/Jql.g"; }

	// $ANTLR start "LPAREN"
	public final void mLPAREN() throws RecognitionException {
		try {
			int _type = LPAREN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:924:13: ( '(' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:924:16: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LPAREN"

	// $ANTLR start "RPAREN"
	public final void mRPAREN() throws RecognitionException {
		try {
			int _type = RPAREN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:922:9: ( ')' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:922:11: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RPAREN"

	// $ANTLR start "COMMA"
	public final void mCOMMA() throws RecognitionException {
		try {
			int _type = COMMA;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:923:8: ( ',' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:923:11: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMA"

	// $ANTLR start "LBRACKET"
	public final void mLBRACKET() throws RecognitionException {
		try {
			int _type = LBRACKET;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:924:10: ( '[' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:924:12: '['
			{
			match('['); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LBRACKET"

	// $ANTLR start "RBRACKET"
	public final void mRBRACKET() throws RecognitionException {
		try {
			int _type = RBRACKET;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:925:11: ( ']' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:925:14: ']'
			{
			match(']'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RBRACKET"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:927:15: ( '-' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:927:18: '-'
			{
			match('-'); 
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS"

	// $ANTLR start "BANG"
	public final void mBANG() throws RecognitionException {
		try {
			int _type = BANG;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:937:7: ( '!' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:937:9: '!'
			{
			match('!'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BANG"

	// $ANTLR start "LT"
	public final void mLT() throws RecognitionException {
		try {
			int _type = LT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:934:5: ( '<' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:934:7: '<'
			{
			match('<'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LT"

	// $ANTLR start "GT"
	public final void mGT() throws RecognitionException {
		try {
			int _type = GT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:935:5: ( '>' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:935:7: '>'
			{
			match('>'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GT"

	// $ANTLR start "GTEQ"
	public final void mGTEQ() throws RecognitionException {
		try {
			int _type = GTEQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:936:7: ( '>=' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:936:9: '>='
			{
			match(">="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GTEQ"

	// $ANTLR start "LTEQ"
	public final void mLTEQ() throws RecognitionException {
		try {
			int _type = LTEQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:937:8: ( '<=' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:937:10: '<='
			{
			match("<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LTEQ"

	// $ANTLR start "EQUALS"
	public final void mEQUALS() throws RecognitionException {
		try {
			int _type = EQUALS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:938:9: ( '=' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:938:11: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EQUALS"

	// $ANTLR start "NOT_EQUALS"
	public final void mNOT_EQUALS() throws RecognitionException {
		try {
			int _type = NOT_EQUALS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:939:12: ( '!=' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:939:14: '!='
			{
			match("!="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NOT_EQUALS"

	// $ANTLR start "LIKE"
	public final void mLIKE() throws RecognitionException {
		try {
			int _type = LIKE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:940:7: ( '~' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:940:9: '~'
			{
			match('~'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LIKE"

	// $ANTLR start "NOT_LIKE"
	public final void mNOT_LIKE() throws RecognitionException {
		try {
			int _type = NOT_LIKE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:941:10: ( '!~' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:941:12: '!~'
			{
			match("!~"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NOT_LIKE"

	// $ANTLR start "IN"
	public final void mIN() throws RecognitionException {
		try {
			int _type = IN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:942:5: ( ( 'I' | 'i' ) ( 'N' | 'n' ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:942:7: ( 'I' | 'i' ) ( 'N' | 'n' )
			{
			if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IN"

	// $ANTLR start "IS"
	public final void mIS() throws RecognitionException {
		try {
			int _type = IS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:943:5: ( ( 'I' | 'i' ) ( 'S' | 's' ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:943:7: ( 'I' | 'i' ) ( 'S' | 's' )
			{
			if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IS"

	// $ANTLR start "AND"
	public final void mAND() throws RecognitionException {
		try {
			int _type = AND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:944:7: ( ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'D' | 'd' ) | AMPER | AMPER_AMPER )
			int alt1=3;
			int LA1_0 = input.LA(1);
			if ( (LA1_0=='A'||LA1_0=='a') ) {
				alt1=1;
			}
			else if ( (LA1_0=='&') ) {
				int LA1_2 = input.LA(2);
				if ( (LA1_2=='&') ) {
					alt1=3;
				}

				else {
					alt1=2;
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:944:9: ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'D' | 'd' )
					{
					if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 2 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:944:39: AMPER
					{
					mAMPER(); 

					}
					break;
				case 3 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:944:47: AMPER_AMPER
					{
					mAMPER_AMPER(); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AND"

	// $ANTLR start "OR"
	public final void mOR() throws RecognitionException {
		try {
			int _type = OR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:945:5: ( ( 'O' | 'o' ) ( 'R' | 'r' ) | PIPE | PIPE_PIPE )
			int alt2=3;
			int LA2_0 = input.LA(1);
			if ( (LA2_0=='O'||LA2_0=='o') ) {
				alt2=1;
			}
			else if ( (LA2_0=='|') ) {
				int LA2_2 = input.LA(2);
				if ( (LA2_2=='|') ) {
					alt2=3;
				}

				else {
					alt2=2;
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}

			switch (alt2) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:945:7: ( 'O' | 'o' ) ( 'R' | 'r' )
					{
					if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 2 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:945:28: PIPE
					{
					mPIPE(); 

					}
					break;
				case 3 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:945:35: PIPE_PIPE
					{
					mPIPE_PIPE(); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OR"

	// $ANTLR start "NOT"
	public final void mNOT() throws RecognitionException {
		try {
			int _type = NOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:946:6: ( ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:946:8: ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' )
			{
			if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NOT"

	// $ANTLR start "EMPTY"
	public final void mEMPTY() throws RecognitionException {
		try {
			int _type = EMPTY;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:947:8: ( ( 'E' | 'e' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'T' | 't' ) ( 'Y' | 'y' ) | ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' ) )
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0=='E'||LA3_0=='e') ) {
				alt3=1;
			}
			else if ( (LA3_0=='N'||LA3_0=='n') ) {
				alt3=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}

			switch (alt3) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:947:10: ( 'E' | 'e' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'T' | 't' ) ( 'Y' | 'y' )
					{
					if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 2 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:947:58: ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' )
					{
					if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EMPTY"

	// $ANTLR start "WAS"
	public final void mWAS() throws RecognitionException {
		try {
			int _type = WAS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:949:6: ( ( 'W' | 'w' ) ( 'A' | 'a' ) ( 'S' | 's' ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:949:8: ( 'W' | 'w' ) ( 'A' | 'a' ) ( 'S' | 's' )
			{
			if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WAS"

	// $ANTLR start "CHANGED"
	public final void mCHANGED() throws RecognitionException {
		try {
			int _type = CHANGED;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:950:10: ( ( 'C' | 'c' ) ( 'H' | 'h' ) ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'D' | 'd' ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:950:12: ( 'C' | 'c' ) ( 'H' | 'h' ) ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'D' | 'd' )
			{
			if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CHANGED"

	// $ANTLR start "BEFORE"
	public final void mBEFORE() throws RecognitionException {
		try {
			int _type = BEFORE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:952:9: ( ( 'B' | 'b' ) ( 'E' | 'e' ) ( 'F' | 'f' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'E' | 'e' ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:952:11: ( 'B' | 'b' ) ( 'E' | 'e' ) ( 'F' | 'f' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'E' | 'e' )
			{
			if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BEFORE"

	// $ANTLR start "AFTER"
	public final void mAFTER() throws RecognitionException {
		try {
			int _type = AFTER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:953:8: ( ( 'A' | 'a' ) ( 'F' | 'f' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:953:10: ( 'A' | 'a' ) ( 'F' | 'f' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' )
			{
			if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AFTER"

	// $ANTLR start "FROM"
	public final void mFROM() throws RecognitionException {
		try {
			int _type = FROM;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:954:7: ( ( 'F' | 'f' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'M' | 'm' ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:954:9: ( 'F' | 'f' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'M' | 'm' )
			{
			if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FROM"

	// $ANTLR start "TO"
	public final void mTO() throws RecognitionException {
		try {
			int _type = TO;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:955:5: ( ( 'T' | 't' ) ( 'O' | 'o' ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:955:7: ( 'T' | 't' ) ( 'O' | 'o' )
			{
			if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TO"

	// $ANTLR start "ON"
	public final void mON() throws RecognitionException {
		try {
			int _type = ON;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:958:5: ( ( 'O' | 'o' ) ( 'N' | 'n' ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:958:7: ( 'O' | 'o' ) ( 'N' | 'n' )
			{
			if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ON"

	// $ANTLR start "DURING"
	public final void mDURING() throws RecognitionException {
		try {
			int _type = DURING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:959:9: ( ( 'D' | 'd' ) ( 'U' | 'u' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:959:11: ( 'D' | 'd' ) ( 'U' | 'u' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' )
			{
			if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DURING"

	// $ANTLR start "ORDER"
	public final void mORDER() throws RecognitionException {
		try {
			int _type = ORDER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:968:7: ( ( 'o' | 'O' ) ( 'r' | 'R' ) ( 'd' | 'D' ) ( 'e' | 'E' ) ( 'r' | 'R' ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:968:9: ( 'o' | 'O' ) ( 'r' | 'R' ) ( 'd' | 'D' ) ( 'e' | 'E' ) ( 'r' | 'R' )
			{
			if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ORDER"

	// $ANTLR start "BY"
	public final void mBY() throws RecognitionException {
		try {
			int _type = BY;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:966:4: ( ( 'b' | 'B' ) ( 'y' | 'Y' ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:966:6: ( 'b' | 'B' ) ( 'y' | 'Y' )
			{
			if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BY"

	// $ANTLR start "ASC"
	public final void mASC() throws RecognitionException {
		try {
			int _type = ASC;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:967:5: ( ( 'a' | 'A' ) ( 's' | 'S' ) ( 'c' | 'C' ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:967:7: ( 'a' | 'A' ) ( 's' | 'S' ) ( 'c' | 'C' )
			{
			if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ASC"

	// $ANTLR start "DESC"
	public final void mDESC() throws RecognitionException {
		try {
			int _type = DESC;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:968:5: ( ( 'd' | 'D' ) ( 'e' | 'E' ) ( 's' | 'S' ) ( 'c' | 'C' ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:968:7: ( 'd' | 'D' ) ( 'e' | 'E' ) ( 's' | 'S' ) ( 'c' | 'C' )
			{
			if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DESC"

	// $ANTLR start "POSNUMBER"
	public final void mPOSNUMBER() throws RecognitionException {
		try {
			int _type = POSNUMBER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:974:2: ( ( DIGIT )+ )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:974:4: ( DIGIT )+
			{
			// com/atlassian/jira/jql/parser/antlr/Jql.g:974:4: ( DIGIT )+
			int cnt4=0;
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( ((LA4_0 >= '0' && LA4_0 <= '9')) ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt4 >= 1 ) break loop4;
					EarlyExitException eee = new EarlyExitException(4, input);
					throw eee;
				}
				cnt4++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "POSNUMBER"

	// $ANTLR start "NEGNUMBER"
	public final void mNEGNUMBER() throws RecognitionException {
		try {
			int _type = NEGNUMBER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:977:2: ( MINUS ( DIGIT )+ )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:977:4: MINUS ( DIGIT )+
			{
			mMINUS(); 

			// com/atlassian/jira/jql/parser/antlr/Jql.g:977:10: ( DIGIT )+
			int cnt5=0;
			loop5:
			while (true) {
				int alt5=2;
				int LA5_0 = input.LA(1);
				if ( ((LA5_0 >= '0' && LA5_0 <= '9')) ) {
					alt5=1;
				}

				switch (alt5) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt5 >= 1 ) break loop5;
					EarlyExitException eee = new EarlyExitException(5, input);
					throw eee;
				}
				cnt5++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NEGNUMBER"

	// $ANTLR start "CUSTOMFIELD"
	public final void mCUSTOMFIELD() throws RecognitionException {
		try {
			int _type = CUSTOMFIELD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:986:2: ( ( 'c' | 'C' ) ( 'f' | 'F' ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:986:4: ( 'c' | 'C' ) ( 'f' | 'F' )
			{
			if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CUSTOMFIELD"

	// $ANTLR start "STRING"
	public final void mSTRING() throws RecognitionException {
		try {
			int _type = STRING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			 pushPosition(STRING); 
			// com/atlassian/jira/jql/parser/antlr/Jql.g:996:2: ( ( ESCAPE |~ ( BSLASH | WS | STRINGSTOP ) )+ )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:996:4: ( ESCAPE |~ ( BSLASH | WS | STRINGSTOP ) )+
			{
			// com/atlassian/jira/jql/parser/antlr/Jql.g:996:4: ( ESCAPE |~ ( BSLASH | WS | STRINGSTOP ) )+
			int cnt6=0;
			loop6:
			while (true) {
				int alt6=3;
				int LA6_0 = input.LA(1);
				if ( (LA6_0=='\\') ) {
					alt6=1;
				}
				else if ( ((LA6_0 >= '-' && LA6_0 <= '.')||(LA6_0 >= '0' && LA6_0 <= ':')||(LA6_0 >= 'A' && LA6_0 <= 'Z')||(LA6_0 >= '_' && LA6_0 <= 'z')||(LA6_0 >= '\u00A0' && LA6_0 <= '\uFDCF')||(LA6_0 >= '\uFDF0' && LA6_0 <= '\uFFFD')) ) {
					alt6=2;
				}

				switch (alt6) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:996:5: ESCAPE
					{
					mESCAPE(); 

					}
					break;
				case 2 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:996:14: ~ ( BSLASH | WS | STRINGSTOP )
					{
					if ( (input.LA(1) >= '-' && input.LA(1) <= '.')||(input.LA(1) >= '0' && input.LA(1) <= ':')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= '_' && input.LA(1) <= 'z')||(input.LA(1) >= '\u00A0' && input.LA(1) <= '\uFDCF')||(input.LA(1) >= '\uFDF0' && input.LA(1) <= '\uFFFD') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt6 >= 1 ) break loop6;
					EarlyExitException eee = new EarlyExitException(6, input);
					throw eee;
				}
				cnt6++;
			}


					//Once this method is called, the text of the current token is fixed. This means that this Lexical rule
					//should not be called from other lexical rules.
					checkAndSet();
				
			}

			state.type = _type;
			state.channel = _channel;
			 popPosition(); 
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "STRING"

	// $ANTLR start "QUOTE_STRING"
	public final void mQUOTE_STRING() throws RecognitionException {
		try {
			int _type = QUOTE_STRING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			 pushPosition(QUOTE_STRING); 
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1003:2: ( ( QUOTE ( ESCAPE |~ ( BSLASH | QUOTE | CONTROLCHARS ) )* QUOTE ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1003:4: ( QUOTE ( ESCAPE |~ ( BSLASH | QUOTE | CONTROLCHARS ) )* QUOTE )
			{
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1003:4: ( QUOTE ( ESCAPE |~ ( BSLASH | QUOTE | CONTROLCHARS ) )* QUOTE )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1003:5: QUOTE ( ESCAPE |~ ( BSLASH | QUOTE | CONTROLCHARS ) )* QUOTE
			{
			mQUOTE(); 

			// com/atlassian/jira/jql/parser/antlr/Jql.g:1003:11: ( ESCAPE |~ ( BSLASH | QUOTE | CONTROLCHARS ) )*
			loop7:
			while (true) {
				int alt7=3;
				int LA7_0 = input.LA(1);
				if ( (LA7_0=='\\') ) {
					alt7=1;
				}
				else if ( (LA7_0=='\n'||LA7_0=='\r'||(LA7_0 >= ' ' && LA7_0 <= '!')||(LA7_0 >= '#' && LA7_0 <= '[')||(LA7_0 >= ']' && LA7_0 <= '~')||(LA7_0 >= '\u00A0' && LA7_0 <= '\uFDCF')||(LA7_0 >= '\uFDF0' && LA7_0 <= '\uFFFD')) ) {
					alt7=2;
				}

				switch (alt7) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:1003:12: ESCAPE
					{
					mESCAPE(); 

					}
					break;
				case 2 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:1003:21: ~ ( BSLASH | QUOTE | CONTROLCHARS )
					{
					if ( input.LA(1)=='\n'||input.LA(1)=='\r'||(input.LA(1) >= ' ' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '~')||(input.LA(1) >= '\u00A0' && input.LA(1) <= '\uFDCF')||(input.LA(1) >= '\uFDF0' && input.LA(1) <= '\uFFFD') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop7;
				}
			}

			mQUOTE(); 

			}


					//Once this method is called, the text of the current token is fixed. This means that this Lexical rule
					//should not be called from other lexical rules.
					stripAndSet();
				
			}

			state.type = _type;
			state.channel = _channel;
			 popPosition(); 
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "QUOTE_STRING"

	// $ANTLR start "SQUOTE_STRING"
	public final void mSQUOTE_STRING() throws RecognitionException {
		try {
			int _type = SQUOTE_STRING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			 pushPosition(SQUOTE_STRING); 
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1013:2: ( ( SQUOTE ( ESCAPE |~ ( BSLASH | SQUOTE | CONTROLCHARS ) )* SQUOTE ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1013:4: ( SQUOTE ( ESCAPE |~ ( BSLASH | SQUOTE | CONTROLCHARS ) )* SQUOTE )
			{
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1013:4: ( SQUOTE ( ESCAPE |~ ( BSLASH | SQUOTE | CONTROLCHARS ) )* SQUOTE )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1013:5: SQUOTE ( ESCAPE |~ ( BSLASH | SQUOTE | CONTROLCHARS ) )* SQUOTE
			{
			mSQUOTE(); 

			// com/atlassian/jira/jql/parser/antlr/Jql.g:1013:12: ( ESCAPE |~ ( BSLASH | SQUOTE | CONTROLCHARS ) )*
			loop8:
			while (true) {
				int alt8=3;
				int LA8_0 = input.LA(1);
				if ( (LA8_0=='\\') ) {
					alt8=1;
				}
				else if ( (LA8_0=='\n'||LA8_0=='\r'||(LA8_0 >= ' ' && LA8_0 <= '&')||(LA8_0 >= '(' && LA8_0 <= '[')||(LA8_0 >= ']' && LA8_0 <= '~')||(LA8_0 >= '\u00A0' && LA8_0 <= '\uFDCF')||(LA8_0 >= '\uFDF0' && LA8_0 <= '\uFFFD')) ) {
					alt8=2;
				}

				switch (alt8) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:1013:13: ESCAPE
					{
					mESCAPE(); 

					}
					break;
				case 2 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:1013:22: ~ ( BSLASH | SQUOTE | CONTROLCHARS )
					{
					if ( input.LA(1)=='\n'||input.LA(1)=='\r'||(input.LA(1) >= ' ' && input.LA(1) <= '&')||(input.LA(1) >= '(' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '~')||(input.LA(1) >= '\u00A0' && input.LA(1) <= '\uFDCF')||(input.LA(1) >= '\uFDF0' && input.LA(1) <= '\uFFFD') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop8;
				}
			}

			mSQUOTE(); 

			}


					//Once this method is called, the text of the current token is fixed. This means that this Lexical rule
					//should not be called from other lexical rules.
					stripAndSet();
				
			}

			state.type = _type;
			state.channel = _channel;
			 popPosition(); 
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SQUOTE_STRING"

	// $ANTLR start "MATCHWS"
	public final void mMATCHWS() throws RecognitionException {
		try {
			int _type = MATCHWS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1026:12: ( ( WS )+ )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1026:16: ( WS )+
			{
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1026:16: ( WS )+
			int cnt9=0;
			loop9:
			while (true) {
				int alt9=2;
				int LA9_0 = input.LA(1);
				if ( ((LA9_0 >= '\t' && LA9_0 <= '\n')||LA9_0=='\r'||LA9_0==' ') ) {
					alt9=1;
				}

				switch (alt9) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:
					{
					if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt9 >= 1 ) break loop9;
					EarlyExitException eee = new EarlyExitException(9, input);
					throw eee;
				}
				cnt9++;
			}

			 _channel = HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MATCHWS"

	// $ANTLR start "RESERVED_CHARS"
	public final void mRESERVED_CHARS() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1034:2: ( '{' | '}' | '*' | '/' | '%' | '+' | '^' | '$' | '#' | '@' | '?' | ';' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:
			{
			if ( (input.LA(1) >= '#' && input.LA(1) <= '%')||(input.LA(1) >= '*' && input.LA(1) <= '+')||input.LA(1)=='/'||input.LA(1)==';'||(input.LA(1) >= '?' && input.LA(1) <= '@')||input.LA(1)=='^'||input.LA(1)=='{'||input.LA(1)=='}' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RESERVED_CHARS"

	// $ANTLR start "ERROR_RESERVED"
	public final void mERROR_RESERVED() throws RecognitionException {
		try {
			int _type = ERROR_RESERVED;
			int _channel = DEFAULT_TOKEN_CHANNEL;

			        pushPosition(ERROR_RESERVED);
			        recover();
			    
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1051:5: ( RESERVED_CHARS )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:
			{
			if ( (input.LA(1) >= '#' && input.LA(1) <= '%')||(input.LA(1) >= '*' && input.LA(1) <= '+')||input.LA(1)=='/'||input.LA(1)==';'||(input.LA(1) >= '?' && input.LA(1) <= '@')||input.LA(1)=='^'||input.LA(1)=='{'||input.LA(1)=='}' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
			 popPosition(); 
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ERROR_RESERVED"

	// $ANTLR start "ERRORCHAR"
	public final void mERRORCHAR() throws RecognitionException {
		try {
			int _type = ERRORCHAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;

			        pushPosition(ERRORCHAR);
			        recover();
			    
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1066:5: ( . )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1066:7: .
			{
			matchAny(); 
			}

			state.type = _type;
			state.channel = _channel;
			 popPosition(); 
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ERRORCHAR"

	// $ANTLR start "QUOTE"
	public final void mQUOTE() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1065:17: ( '\"' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1065:19: '\"'
			{
			match('\"'); 
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "QUOTE"

	// $ANTLR start "SQUOTE"
	public final void mSQUOTE() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1066:18: ( '\\'' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1066:20: '\\''
			{
			match('\''); 
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SQUOTE"

	// $ANTLR start "BSLASH"
	public final void mBSLASH() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1067:18: ( '\\\\' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1067:20: '\\\\'
			{
			match('\\'); 
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BSLASH"

	// $ANTLR start "NL"
	public final void mNL() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1068:14: ( '\\r' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1068:16: '\\r'
			{
			match('\r'); 
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NL"

	// $ANTLR start "CR"
	public final void mCR() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1069:14: ( '\\n' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1069:16: '\\n'
			{
			match('\n'); 
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CR"

	// $ANTLR start "SPACE"
	public final void mSPACE() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1070:17: ( ' ' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1070:19: ' '
			{
			match(' '); 
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SPACE"

	// $ANTLR start "AMPER"
	public final void mAMPER() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1071:16: ( '&' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1071:18: '&'
			{
			match('&'); 
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AMPER"

	// $ANTLR start "AMPER_AMPER"
	public final void mAMPER_AMPER() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1072:21: ( '&&' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1072:24: '&&'
			{
			match("&&"); 

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AMPER_AMPER"

	// $ANTLR start "PIPE"
	public final void mPIPE() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1073:15: ( '|' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1073:17: '|'
			{
			match('|'); 
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PIPE"

	// $ANTLR start "PIPE_PIPE"
	public final void mPIPE_PIPE() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1074:20: ( '||' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1074:22: '||'
			{
			match("||"); 

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PIPE_PIPE"

	// $ANTLR start "ESCAPE"
	public final void mESCAPE() throws RecognitionException {
		try {
			 pushPosition(ESCAPE); 
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1082:2: ( BSLASH ( 't' | 'n' | 'r' | QUOTE | SQUOTE | BSLASH | SPACE | 'u' HEXDIGIT HEXDIGIT HEXDIGIT HEXDIGIT ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1082:6: BSLASH ( 't' | 'n' | 'r' | QUOTE | SQUOTE | BSLASH | SPACE | 'u' HEXDIGIT HEXDIGIT HEXDIGIT HEXDIGIT )
			{
			mBSLASH(); 

			// com/atlassian/jira/jql/parser/antlr/Jql.g:1083:2: ( 't' | 'n' | 'r' | QUOTE | SQUOTE | BSLASH | SPACE | 'u' HEXDIGIT HEXDIGIT HEXDIGIT HEXDIGIT )
			int alt10=8;
			switch ( input.LA(1) ) {
			case 't':
				{
				alt10=1;
				}
				break;
			case 'n':
				{
				alt10=2;
				}
				break;
			case 'r':
				{
				alt10=3;
				}
				break;
			case '\"':
				{
				alt10=4;
				}
				break;
			case '\'':
				{
				alt10=5;
				}
				break;
			case '\\':
				{
				alt10=6;
				}
				break;
			case ' ':
				{
				alt10=7;
				}
				break;
			case 'u':
				{
				alt10=8;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 10, 0, input);
				throw nvae;
			}
			switch (alt10) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:1084:15: 't'
					{
					match('t'); 
					}
					break;
				case 2 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:1085:17: 'n'
					{
					match('n'); 
					}
					break;
				case 3 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:1086:17: 'r'
					{
					match('r'); 
					}
					break;
				case 4 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:1087:17: QUOTE
					{
					mQUOTE(); 

					}
					break;
				case 5 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:1088:17: SQUOTE
					{
					mSQUOTE(); 

					}
					break;
				case 6 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:1089:17: BSLASH
					{
					mBSLASH(); 

					}
					break;
				case 7 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:1090:17: SPACE
					{
					mSPACE(); 

					}
					break;
				case 8 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:1091:16: 'u' HEXDIGIT HEXDIGIT HEXDIGIT HEXDIGIT
					{
					match('u'); 
					mHEXDIGIT(); 

					mHEXDIGIT(); 

					mHEXDIGIT(); 

					mHEXDIGIT(); 

					}
					break;

			}

			 popPosition(); 
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ESCAPE"

	// $ANTLR start "STRINGSTOP"
	public final void mSTRINGSTOP() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1102:2: ( CONTROLCHARS | QUOTE | SQUOTE | EQUALS | BANG | LT | GT | LPAREN | RPAREN | LIKE | COMMA | LBRACKET | RBRACKET | PIPE | AMPER | RESERVED_CHARS | NEWLINE )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:
			{
			if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\u001F')||(input.LA(1) >= '!' && input.LA(1) <= ',')||input.LA(1)=='/'||(input.LA(1) >= ';' && input.LA(1) <= '@')||input.LA(1)=='['||(input.LA(1) >= ']' && input.LA(1) <= '^')||(input.LA(1) >= '{' && input.LA(1) <= '\u009F')||(input.LA(1) >= '\uFDD0' && input.LA(1) <= '\uFDEF')||(input.LA(1) >= '\uFFFE' && input.LA(1) <= '\uFFFF') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "STRINGSTOP"

	// $ANTLR start "CONTROLCHARS"
	public final void mCONTROLCHARS() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1120:2: ( '\\u0000' .. '\\u0009' | '\\u000b' .. '\\u000c' | '\\u000e' .. '\\u001f' | '\\u007f' .. '\\u009f' | '\\ufdd0' .. '\\ufdef' | '\\ufffe' .. '\\uffff' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:
			{
			if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\u001F')||(input.LA(1) >= '\u007F' && input.LA(1) <= '\u009F')||(input.LA(1) >= '\uFDD0' && input.LA(1) <= '\uFDEF')||(input.LA(1) >= '\uFFFE' && input.LA(1) <= '\uFFFF') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CONTROLCHARS"

	// $ANTLR start "NEWLINE"
	public final void mNEWLINE() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1135:5: ( NL | CR )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:
			{
			if ( input.LA(1)=='\n'||input.LA(1)=='\r' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NEWLINE"

	// $ANTLR start "HEXDIGIT"
	public final void mHEXDIGIT() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1138:2: ( DIGIT | ( 'A' | 'a' ) | ( 'B' | 'b' ) | ( 'C' | 'c' ) | ( 'D' | 'd' ) | ( 'E' | 'e' ) | ( 'F' | 'f' ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'F')||(input.LA(1) >= 'a' && input.LA(1) <= 'f') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "HEXDIGIT"

	// $ANTLR start "DIGIT"
	public final void mDIGIT() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1142:2: ( '0' .. '9' )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIGIT"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:1146:2: ( ( SPACE | '\\t' | NEWLINE ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:
			{
			if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	@Override
	public void mTokens() throws RecognitionException {
		// com/atlassian/jira/jql/parser/antlr/Jql.g:1:8: ( LPAREN | RPAREN | COMMA | LBRACKET | RBRACKET | BANG | LT | GT | GTEQ | LTEQ | EQUALS | NOT_EQUALS | LIKE | NOT_LIKE | IN | IS | AND | OR | NOT | EMPTY | WAS | CHANGED | BEFORE | AFTER | FROM | TO | ON | DURING | ORDER | BY | ASC | DESC | POSNUMBER | NEGNUMBER | CUSTOMFIELD | STRING | QUOTE_STRING | SQUOTE_STRING | MATCHWS | ERROR_RESERVED | ERRORCHAR )
		int alt11=41;
		alt11 = dfa11.predict(input);
		switch (alt11) {
			case 1 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:10: LPAREN
				{
				mLPAREN(); 

				}
				break;
			case 2 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:17: RPAREN
				{
				mRPAREN(); 

				}
				break;
			case 3 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:24: COMMA
				{
				mCOMMA(); 

				}
				break;
			case 4 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:30: LBRACKET
				{
				mLBRACKET(); 

				}
				break;
			case 5 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:39: RBRACKET
				{
				mRBRACKET(); 

				}
				break;
			case 6 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:48: BANG
				{
				mBANG(); 

				}
				break;
			case 7 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:53: LT
				{
				mLT(); 

				}
				break;
			case 8 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:56: GT
				{
				mGT(); 

				}
				break;
			case 9 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:59: GTEQ
				{
				mGTEQ(); 

				}
				break;
			case 10 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:64: LTEQ
				{
				mLTEQ(); 

				}
				break;
			case 11 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:69: EQUALS
				{
				mEQUALS(); 

				}
				break;
			case 12 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:76: NOT_EQUALS
				{
				mNOT_EQUALS(); 

				}
				break;
			case 13 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:87: LIKE
				{
				mLIKE(); 

				}
				break;
			case 14 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:92: NOT_LIKE
				{
				mNOT_LIKE(); 

				}
				break;
			case 15 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:101: IN
				{
				mIN(); 

				}
				break;
			case 16 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:104: IS
				{
				mIS(); 

				}
				break;
			case 17 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:107: AND
				{
				mAND(); 

				}
				break;
			case 18 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:111: OR
				{
				mOR(); 

				}
				break;
			case 19 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:114: NOT
				{
				mNOT(); 

				}
				break;
			case 20 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:118: EMPTY
				{
				mEMPTY(); 

				}
				break;
			case 21 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:124: WAS
				{
				mWAS(); 

				}
				break;
			case 22 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:128: CHANGED
				{
				mCHANGED(); 

				}
				break;
			case 23 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:136: BEFORE
				{
				mBEFORE(); 

				}
				break;
			case 24 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:143: AFTER
				{
				mAFTER(); 

				}
				break;
			case 25 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:149: FROM
				{
				mFROM(); 

				}
				break;
			case 26 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:154: TO
				{
				mTO(); 

				}
				break;
			case 27 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:157: ON
				{
				mON(); 

				}
				break;
			case 28 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:160: DURING
				{
				mDURING(); 

				}
				break;
			case 29 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:167: ORDER
				{
				mORDER(); 

				}
				break;
			case 30 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:173: BY
				{
				mBY(); 

				}
				break;
			case 31 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:176: ASC
				{
				mASC(); 

				}
				break;
			case 32 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:180: DESC
				{
				mDESC(); 

				}
				break;
			case 33 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:185: POSNUMBER
				{
				mPOSNUMBER(); 

				}
				break;
			case 34 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:195: NEGNUMBER
				{
				mNEGNUMBER(); 

				}
				break;
			case 35 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:205: CUSTOMFIELD
				{
				mCUSTOMFIELD(); 

				}
				break;
			case 36 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:217: STRING
				{
				mSTRING(); 

				}
				break;
			case 37 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:224: QUOTE_STRING
				{
				mQUOTE_STRING(); 

				}
				break;
			case 38 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:237: SQUOTE_STRING
				{
				mSQUOTE_STRING(); 

				}
				break;
			case 39 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:251: MATCHWS
				{
				mMATCHWS(); 

				}
				break;
			case 40 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:259: ERROR_RESERVED
				{
				mERROR_RESERVED(); 

				}
				break;
			case 41 :
				// com/atlassian/jira/jql/parser/antlr/Jql.g:1:274: ERRORCHAR
				{
				mERRORCHAR(); 

				}
				break;

		}
	}


	protected DFA11 dfa11 = new DFA11(this);
	static final String DFA11_eotS =
		"\6\uffff\1\50\1\52\1\54\2\uffff\2\61\1\uffff\1\61\1\uffff\10\61\1\105"+
		"\1\61\1\40\1\uffff\2\40\21\uffff\1\114\1\115\1\uffff\3\61\1\uffff\1\70"+
		"\1\122\1\uffff\5\61\1\130\1\61\1\132\1\61\1\134\2\61\1\uffff\1\105\1\137"+
		"\6\uffff\1\65\1\61\1\141\1\61\1\uffff\1\143\2\61\1\146\1\61\1\uffff\1"+
		"\61\1\uffff\1\61\1\uffff\2\61\1\uffff\1\61\1\uffff\1\61\1\uffff\1\156"+
		"\1\61\1\uffff\2\61\1\162\1\61\1\164\1\165\1\166\1\uffff\1\156\2\61\1\uffff"+
		"\1\61\3\uffff\1\61\1\173\1\174\1\175\3\uffff";
	static final String DFA11_eofS =
		"\176\uffff";
	static final String DFA11_minS =
		"\1\0\5\uffff\3\75\2\uffff\1\116\1\106\1\uffff\1\116\1\uffff\1\117\1\115"+
		"\1\101\1\106\1\105\1\122\1\117\1\105\1\55\1\60\1\40\1\uffff\2\12\21\uffff"+
		"\2\55\1\uffff\1\104\1\124\1\103\1\uffff\2\55\1\uffff\1\124\1\114\1\120"+
		"\1\123\1\101\1\55\1\106\1\55\1\117\1\55\1\122\1\123\1\uffff\2\55\6\uffff"+
		"\1\55\1\105\1\55\1\105\1\uffff\1\55\1\114\1\124\1\55\1\116\1\uffff\1\117"+
		"\1\uffff\1\115\1\uffff\1\111\1\103\1\uffff\1\122\1\uffff\1\122\1\uffff"+
		"\1\55\1\131\1\uffff\1\107\1\122\1\55\1\116\3\55\1\uffff\1\55\2\105\1\uffff"+
		"\1\107\3\uffff\1\104\3\55\3\uffff";
	static final String DFA11_maxS =
		"\1\uffff\5\uffff\1\176\2\75\2\uffff\2\163\1\uffff\1\162\1\uffff\1\165"+
		"\1\155\1\141\1\150\1\171\1\162\1\157\1\165\1\ufffd\1\71\1\165\1\uffff"+
		"\2\ufffd\21\uffff\2\ufffd\1\uffff\1\144\1\164\1\143\1\uffff\2\ufffd\1"+
		"\uffff\1\164\1\154\1\160\1\163\1\141\1\ufffd\1\146\1\ufffd\1\157\1\ufffd"+
		"\1\162\1\163\1\uffff\2\ufffd\6\uffff\1\ufffd\1\145\1\ufffd\1\145\1\uffff"+
		"\1\ufffd\1\154\1\164\1\ufffd\1\156\1\uffff\1\157\1\uffff\1\155\1\uffff"+
		"\1\151\1\143\1\uffff\1\162\1\uffff\1\162\1\uffff\1\ufffd\1\171\1\uffff"+
		"\1\147\1\162\1\ufffd\1\156\3\ufffd\1\uffff\1\ufffd\2\145\1\uffff\1\147"+
		"\3\uffff\1\144\3\ufffd\3\uffff";
	static final String DFA11_acceptS =
		"\1\uffff\1\1\1\2\1\3\1\4\1\5\3\uffff\1\13\1\15\2\uffff\1\21\1\uffff\1"+
		"\22\13\uffff\1\44\2\uffff\1\47\1\50\1\51\1\1\1\2\1\3\1\4\1\5\1\14\1\16"+
		"\1\6\1\12\1\7\1\11\1\10\1\13\1\15\2\uffff\1\44\3\uffff\1\21\2\uffff\1"+
		"\22\14\uffff\1\41\2\uffff\1\45\1\46\1\47\1\50\1\17\1\20\4\uffff\1\33\5"+
		"\uffff\1\43\1\uffff\1\36\1\uffff\1\32\2\uffff\1\42\1\uffff\1\37\1\uffff"+
		"\1\23\2\uffff\1\25\7\uffff\1\24\3\uffff\1\31\1\uffff\1\40\1\30\1\35\4"+
		"\uffff\1\27\1\34\1\26";
	static final String DFA11_specialS =
		"\1\0\175\uffff}>";
	static final String[] DFA11_transitionS = {
			"\11\40\2\36\2\40\1\36\22\40\1\36\1\6\1\34\3\37\1\15\1\35\1\1\1\2\2\37"+
			"\1\3\1\31\1\33\1\37\12\30\1\33\1\37\1\7\1\11\1\10\2\37\1\14\1\24\1\23"+
			"\1\27\1\21\1\25\2\33\1\13\4\33\1\20\1\16\4\33\1\26\2\33\1\22\3\33\1\4"+
			"\1\32\1\5\1\37\2\33\1\14\1\24\1\23\1\27\1\21\1\25\2\33\1\13\4\33\1\20"+
			"\1\16\4\33\1\26\2\33\1\22\3\33\1\37\1\17\1\37\1\12\41\40\ufd30\33\40"+
			"\40\u020e\33\2\40",
			"",
			"",
			"",
			"",
			"",
			"\1\46\100\uffff\1\47",
			"\1\51",
			"\1\53",
			"",
			"",
			"\1\57\4\uffff\1\60\32\uffff\1\57\4\uffff\1\60",
			"\1\63\7\uffff\1\62\4\uffff\1\64\22\uffff\1\63\7\uffff\1\62\4\uffff\1"+
			"\64",
			"",
			"\1\67\3\uffff\1\66\33\uffff\1\67\3\uffff\1\66",
			"",
			"\1\71\5\uffff\1\72\31\uffff\1\71\5\uffff\1\72",
			"\1\73\37\uffff\1\73",
			"\1\74\37\uffff\1\74",
			"\1\76\1\uffff\1\75\35\uffff\1\76\1\uffff\1\75",
			"\1\77\23\uffff\1\100\13\uffff\1\77\23\uffff\1\100",
			"\1\101\37\uffff\1\101",
			"\1\102\37\uffff\1\102",
			"\1\104\17\uffff\1\103\17\uffff\1\104\17\uffff\1\103",
			"\2\61\1\uffff\12\106\1\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45"+
			"\uffff\ufd30\61\40\uffff\u020e\61",
			"\12\107",
			"\1\61\1\uffff\1\61\4\uffff\1\61\64\uffff\1\61\21\uffff\1\61\3\uffff"+
			"\1\61\1\uffff\2\61",
			"",
			"\1\110\2\uffff\1\110\22\uffff\137\110\41\uffff\ufd30\110\40\uffff\u020e"+
			"\110",
			"\1\111\2\uffff\1\111\22\uffff\137\111\41\uffff\ufd30\111\40\uffff\u020e"+
			"\111",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"",
			"\1\116\37\uffff\1\116",
			"\1\117\37\uffff\1\117",
			"\1\120\37\uffff\1\120",
			"",
			"\2\61\1\uffff\13\61\6\uffff\3\61\1\121\26\61\1\uffff\1\61\2\uffff\5"+
			"\61\1\121\26\61\45\uffff\ufd30\61\40\uffff\u020e\61",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"",
			"\1\123\37\uffff\1\123",
			"\1\124\37\uffff\1\124",
			"\1\125\37\uffff\1\125",
			"\1\126\37\uffff\1\126",
			"\1\127\37\uffff\1\127",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"\1\131\37\uffff\1\131",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"\1\133\37\uffff\1\133",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"\1\135\37\uffff\1\135",
			"\1\136\37\uffff\1\136",
			"",
			"\2\61\1\uffff\12\106\1\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45"+
			"\uffff\ufd30\61\40\uffff\u020e\61",
			"\2\61\1\uffff\12\107\1\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45"+
			"\uffff\ufd30\61\40\uffff\u020e\61",
			"",
			"",
			"",
			"",
			"",
			"",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"\1\140\37\uffff\1\140",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"\1\142\37\uffff\1\142",
			"",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"\1\144\37\uffff\1\144",
			"\1\145\37\uffff\1\145",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"\1\147\37\uffff\1\147",
			"",
			"\1\150\37\uffff\1\150",
			"",
			"\1\151\37\uffff\1\151",
			"",
			"\1\152\37\uffff\1\152",
			"\1\153\37\uffff\1\153",
			"",
			"\1\154\37\uffff\1\154",
			"",
			"\1\155\37\uffff\1\155",
			"",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"\1\157\37\uffff\1\157",
			"",
			"\1\160\37\uffff\1\160",
			"\1\161\37\uffff\1\161",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"\1\163\37\uffff\1\163",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"\1\167\37\uffff\1\167",
			"\1\170\37\uffff\1\170",
			"",
			"\1\171\37\uffff\1\171",
			"",
			"",
			"",
			"\1\172\37\uffff\1\172",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"\2\61\1\uffff\13\61\6\uffff\32\61\1\uffff\1\61\2\uffff\34\61\45\uffff"+
			"\ufd30\61\40\uffff\u020e\61",
			"",
			"",
			""
	};

	static final short[] DFA11_eot = DFA.unpackEncodedString(DFA11_eotS);
	static final short[] DFA11_eof = DFA.unpackEncodedString(DFA11_eofS);
	static final char[] DFA11_min = DFA.unpackEncodedStringToUnsignedChars(DFA11_minS);
	static final char[] DFA11_max = DFA.unpackEncodedStringToUnsignedChars(DFA11_maxS);
	static final short[] DFA11_accept = DFA.unpackEncodedString(DFA11_acceptS);
	static final short[] DFA11_special = DFA.unpackEncodedString(DFA11_specialS);
	static final short[][] DFA11_transition;

	static {
		int numStates = DFA11_transitionS.length;
		DFA11_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA11_transition[i] = DFA.unpackEncodedString(DFA11_transitionS[i]);
		}
	}

	protected class DFA11 extends DFA {

		public DFA11(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 11;
			this.eot = DFA11_eot;
			this.eof = DFA11_eof;
			this.min = DFA11_min;
			this.max = DFA11_max;
			this.accept = DFA11_accept;
			this.special = DFA11_special;
			this.transition = DFA11_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( LPAREN | RPAREN | COMMA | LBRACKET | RBRACKET | BANG | LT | GT | GTEQ | LTEQ | EQUALS | NOT_EQUALS | LIKE | NOT_LIKE | IN | IS | AND | OR | NOT | EMPTY | WAS | CHANGED | BEFORE | AFTER | FROM | TO | ON | DURING | ORDER | BY | ASC | DESC | POSNUMBER | NEGNUMBER | CUSTOMFIELD | STRING | QUOTE_STRING | SQUOTE_STRING | MATCHWS | ERROR_RESERVED | ERRORCHAR );";
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			IntStream input = _input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA11_0 = input.LA(1);
						s = -1;
						if ( (LA11_0=='(') ) {s = 1;}
						else if ( (LA11_0==')') ) {s = 2;}
						else if ( (LA11_0==',') ) {s = 3;}
						else if ( (LA11_0=='[') ) {s = 4;}
						else if ( (LA11_0==']') ) {s = 5;}
						else if ( (LA11_0=='!') ) {s = 6;}
						else if ( (LA11_0=='<') ) {s = 7;}
						else if ( (LA11_0=='>') ) {s = 8;}
						else if ( (LA11_0=='=') ) {s = 9;}
						else if ( (LA11_0=='~') ) {s = 10;}
						else if ( (LA11_0=='I'||LA11_0=='i') ) {s = 11;}
						else if ( (LA11_0=='A'||LA11_0=='a') ) {s = 12;}
						else if ( (LA11_0=='&') ) {s = 13;}
						else if ( (LA11_0=='O'||LA11_0=='o') ) {s = 14;}
						else if ( (LA11_0=='|') ) {s = 15;}
						else if ( (LA11_0=='N'||LA11_0=='n') ) {s = 16;}
						else if ( (LA11_0=='E'||LA11_0=='e') ) {s = 17;}
						else if ( (LA11_0=='W'||LA11_0=='w') ) {s = 18;}
						else if ( (LA11_0=='C'||LA11_0=='c') ) {s = 19;}
						else if ( (LA11_0=='B'||LA11_0=='b') ) {s = 20;}
						else if ( (LA11_0=='F'||LA11_0=='f') ) {s = 21;}
						else if ( (LA11_0=='T'||LA11_0=='t') ) {s = 22;}
						else if ( (LA11_0=='D'||LA11_0=='d') ) {s = 23;}
						else if ( ((LA11_0 >= '0' && LA11_0 <= '9')) ) {s = 24;}
						else if ( (LA11_0=='-') ) {s = 25;}
						else if ( (LA11_0=='\\') ) {s = 26;}
						else if ( (LA11_0=='.'||LA11_0==':'||(LA11_0 >= 'G' && LA11_0 <= 'H')||(LA11_0 >= 'J' && LA11_0 <= 'M')||(LA11_0 >= 'P' && LA11_0 <= 'S')||(LA11_0 >= 'U' && LA11_0 <= 'V')||(LA11_0 >= 'X' && LA11_0 <= 'Z')||(LA11_0 >= '_' && LA11_0 <= '`')||(LA11_0 >= 'g' && LA11_0 <= 'h')||(LA11_0 >= 'j' && LA11_0 <= 'm')||(LA11_0 >= 'p' && LA11_0 <= 's')||(LA11_0 >= 'u' && LA11_0 <= 'v')||(LA11_0 >= 'x' && LA11_0 <= 'z')||(LA11_0 >= '\u00A0' && LA11_0 <= '\uFDCF')||(LA11_0 >= '\uFDF0' && LA11_0 <= '\uFFFD')) ) {s = 27;}
						else if ( (LA11_0=='\"') ) {s = 28;}
						else if ( (LA11_0=='\'') ) {s = 29;}
						else if ( ((LA11_0 >= '\t' && LA11_0 <= '\n')||LA11_0=='\r'||LA11_0==' ') ) {s = 30;}
						else if ( ((LA11_0 >= '#' && LA11_0 <= '%')||(LA11_0 >= '*' && LA11_0 <= '+')||LA11_0=='/'||LA11_0==';'||(LA11_0 >= '?' && LA11_0 <= '@')||LA11_0=='^'||LA11_0=='{'||LA11_0=='}') ) {s = 31;}
						else if ( ((LA11_0 >= '\u0000' && LA11_0 <= '\b')||(LA11_0 >= '\u000B' && LA11_0 <= '\f')||(LA11_0 >= '\u000E' && LA11_0 <= '\u001F')||(LA11_0 >= '\u007F' && LA11_0 <= '\u009F')||(LA11_0 >= '\uFDD0' && LA11_0 <= '\uFDEF')||(LA11_0 >= '\uFFFE' && LA11_0 <= '\uFFFF')) ) {s = 32;}
						if ( s>=0 ) return s;
						break;
			}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 11, _s, input);
			error(nvae);
			throw nvae;
		}
	}

}
