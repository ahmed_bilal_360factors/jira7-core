// $ANTLR 3.5.2 com/atlassian/jira/jql/parser/antlr/Jql.g 2018-02-28 15:34:07

package com.atlassian.jira.jql.parser.antlr;

import java.util.Collections;
import com.atlassian.query.operand.*;
import com.atlassian.query.operator.*;
import com.atlassian.query.clause.*;
import com.atlassian.query.order.*;
import com.atlassian.query.history.*;
import com.atlassian.jira.jql.util.FieldReference;
import com.atlassian.jira.jql.util.JqlCustomFieldId;
import com.atlassian.jira.jql.parser.JqlParseErrorMessage;
import com.atlassian.jira.jql.parser.JqlParseErrorMessages;
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.jql.validator.EntityPropertyClauseValidator;
import com.atlassian.fugue.Option;
import org.apache.commons.lang.StringUtils;

import org.apache.log4j.Logger;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

/**
 * This is the ANTLRv3 grammar for JQL. The lexer (JqlLexer) and parser (JqlParser) can be generated from
 * this file by running mvn generate-sources
 *
 * This grammar uses JqlStringSupportImpl.isReservedString to determine whether or not a string is reserved.
 * We tried doing this in the grammar by listing all the reserved words as tokens but ANTLR did not 
 * react very well to this (it generated a very large and slow Lexer). Thus changes to JqlStringSupportImpl.isReservedString
 * will change what strings this grammar will parse.
 * 
 * NOTE: Making changes to the grammar is likely to affect JqlStringSupportImpl which makes assumptions
 * about the structure of this file.
 */
@SuppressWarnings("all")
public class JqlParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AFTER", "AMPER", "AMPER_AMPER", 
		"AND", "ASC", "BANG", "BEFORE", "BSLASH", "BY", "CHANGED", "COMMA", "CONTROLCHARS", 
		"CR", "CUSTOMFIELD", "DESC", "DIGIT", "DURING", "EMPTY", "EQUALS", "ERRORCHAR", 
		"ERROR_RESERVED", "ESCAPE", "FROM", "GT", "GTEQ", "HEXDIGIT", "IN", "IS", 
		"LBRACKET", "LIKE", "LPAREN", "LT", "LTEQ", "MATCHWS", "MINUS", "NEGNUMBER", 
		"NEWLINE", "NL", "NOT", "NOT_EQUALS", "NOT_LIKE", "ON", "OR", "ORDER", 
		"PIPE", "PIPE_PIPE", "POSNUMBER", "QUOTE", "QUOTE_STRING", "RBRACKET", 
		"RESERVED_CHARS", "RPAREN", "SPACE", "SQUOTE", "SQUOTE_STRING", "STRING", 
		"STRINGSTOP", "TO", "WAS", "WS"
	};
	public static final int EOF=-1;
	public static final int AFTER=4;
	public static final int AMPER=5;
	public static final int AMPER_AMPER=6;
	public static final int AND=7;
	public static final int ASC=8;
	public static final int BANG=9;
	public static final int BEFORE=10;
	public static final int BSLASH=11;
	public static final int BY=12;
	public static final int CHANGED=13;
	public static final int COMMA=14;
	public static final int CONTROLCHARS=15;
	public static final int CR=16;
	public static final int CUSTOMFIELD=17;
	public static final int DESC=18;
	public static final int DIGIT=19;
	public static final int DURING=20;
	public static final int EMPTY=21;
	public static final int EQUALS=22;
	public static final int ERRORCHAR=23;
	public static final int ERROR_RESERVED=24;
	public static final int ESCAPE=25;
	public static final int FROM=26;
	public static final int GT=27;
	public static final int GTEQ=28;
	public static final int HEXDIGIT=29;
	public static final int IN=30;
	public static final int IS=31;
	public static final int LBRACKET=32;
	public static final int LIKE=33;
	public static final int LPAREN=34;
	public static final int LT=35;
	public static final int LTEQ=36;
	public static final int MATCHWS=37;
	public static final int MINUS=38;
	public static final int NEGNUMBER=39;
	public static final int NEWLINE=40;
	public static final int NL=41;
	public static final int NOT=42;
	public static final int NOT_EQUALS=43;
	public static final int NOT_LIKE=44;
	public static final int ON=45;
	public static final int OR=46;
	public static final int ORDER=47;
	public static final int PIPE=48;
	public static final int PIPE_PIPE=49;
	public static final int POSNUMBER=50;
	public static final int QUOTE=51;
	public static final int QUOTE_STRING=52;
	public static final int RBRACKET=53;
	public static final int RESERVED_CHARS=54;
	public static final int RPAREN=55;
	public static final int SPACE=56;
	public static final int SQUOTE=57;
	public static final int SQUOTE_STRING=58;
	public static final int STRING=59;
	public static final int STRINGSTOP=60;
	public static final int TO=61;
	public static final int WAS=62;
	public static final int WS=63;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public JqlParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public JqlParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return JqlParser.tokenNames; }
	@Override public String getGrammarFileName() { return "com/atlassian/jira/jql/parser/antlr/Jql.g"; }


	private static final Logger log = Logger.getLogger(JqlParser.class);

	private int operandLevel = 0;
	private boolean supportsHistoryPredicate = false;

	@Override
	protected Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException
	{
		throw new MismatchedTokenException(ttype, input);
	} 

	@Override
	/** This method does not appear to be used, but better safe the sorry. **/
	public Object recoverFromMismatchedSet(IntStream input, RecognitionException e, BitSet follow) throws RecognitionException 
	{ 
		throw e;
	}

	@Override
	/** Override this method to change where error messages go */
	public void emitErrorMessage(String msg) 
	{
		log.warn(msg);
	}

	/**
	 * Make sure that the passed token can be turned into a long. In ANTLR there
	 * does not appear to be an easy way to limit numbers to a valid Long range, so
	 * lets do so in Java.
	 *
	 * @param token the token to turn into a long.
	 * @return the valid long.
	 */
	private long parseLong(Token token)
	{
	    final String text = token.getText();
	    try
	    {
	        return Long.parseLong(text);
	    }
	    catch (NumberFormatException e)
	    {
	        JqlParseErrorMessage message = JqlParseErrorMessages.illegalNumber(text, token.getLine(), token.getCharPositionInLine());
	        throw new RuntimeRecognitionException(message, e);
	    }
	}

	private String checkFieldName(Token token)
	{
	    final String text = token.getText();
	    if (StringUtils.isBlank(text))
	    {
	        reportError(JqlParseErrorMessages.emptyFieldName(token.getLine(), token.getCharPositionInLine()), null);
	    }
	    return text;
	}

	private String checkFunctionName(Token token)
	{
	    final String text = token.getText();
	    if (StringUtils.isBlank(text))
	    {
	        reportError(JqlParseErrorMessages.emptyFunctionName(token.getLine(), token.getCharPositionInLine()), null);
	    }
	    return text;
	}

	private void reportError(JqlParseErrorMessage message, Throwable th) throws RuntimeRecognitionException
	{
	    throw new RuntimeRecognitionException(message, th);
	}


	public static class query_return extends ParserRuleReturnScope {
		public Clause clause;
		public OrderBy order;
	};


	// $ANTLR start "query"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:213:1: query returns [Clause clause, OrderBy order] : (where= clause )? (sort= orderBy )? EOF ;
	public final JqlParser.query_return query() throws RecognitionException {
		JqlParser.query_return retval = new JqlParser.query_return();
		retval.start = input.LT(1);

		Clause where =null;
		OrderBy sort =null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:214:2: ( (where= clause )? (sort= orderBy )? EOF )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:214:4: (where= clause )? (sort= orderBy )? EOF
			{
			// com/atlassian/jira/jql/parser/antlr/Jql.g:214:4: (where= clause )?
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==BANG||LA1_0==CUSTOMFIELD||LA1_0==LPAREN||LA1_0==NEGNUMBER||LA1_0==NOT||LA1_0==POSNUMBER||LA1_0==QUOTE_STRING||(LA1_0 >= SQUOTE_STRING && LA1_0 <= STRING)) ) {
				alt1=1;
			}
			switch (alt1) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:214:5: where= clause
					{
					pushFollow(FOLLOW_clause_in_query72);
					where=clause();
					state._fsp--;

					}
					break;

			}

			// com/atlassian/jira/jql/parser/antlr/Jql.g:214:23: (sort= orderBy )?
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( (LA2_0==ORDER) ) {
				alt2=1;
			}
			switch (alt2) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:214:24: sort= orderBy
					{
					pushFollow(FOLLOW_orderBy_in_query82);
					sort=orderBy();
					state._fsp--;

					}
					break;

			}

			match(input,EOF,FOLLOW_EOF_in_query86); 

					retval.clause = where;
					retval.order = sort;
				
			}

			retval.stop = input.LT(-1);

		}
		catch (MismatchedTokenException e) {

				    if (e.expecting == EOF)
			        {
			            if (sort != null)
			            {
			                //If the sort has trailing tokens then "," must be the next token as this is the only way to
			                //continue a sort.
			                reportError(JqlParseErrorMessages.expectedText(e.token, ","), e);
			            }
			            else if (where == null)
			            {
			                //If there is no where clause, then we were not able to find a field name. If we found a field
			                //name we would have found a clause and got an error there.
			                reportError(JqlParseErrorMessages.badFieldName(e.token), e);
			            }
			            else
			            {
			                //If we get a clause but have other stuff after, then the next token must be an "AND" or "OR" as this
			                //is the only valid way to combine two clauses.
			                reportError(JqlParseErrorMessages.needLogicalOperator(e.token), e);
			            }
			        }
				    reportError(JqlParseErrorMessages.genericParseError(e.token), e);
				
		}
		catch (RecognitionException e) {

				    reportError(JqlParseErrorMessages.genericParseError(e.token), e);
				
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "query"



	// $ANTLR start "clause"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:253:1: clause returns [Clause clause] : orClause ;
	public final Clause clause() throws RecognitionException {
		Clause clause = null;


		Clause orClause1 =null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:254:2: ( orClause )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:254:4: orClause
			{
			pushFollow(FOLLOW_orClause_in_clause122);
			orClause1=orClause();
			state._fsp--;

			 clause = orClause1; 
			}

		}

		catch (RecognitionException e) 
		{
			throw e;
		}

		finally {
			// do for sure before leaving
		}
		return clause;
	}
	// $ANTLR end "clause"



	// $ANTLR start "orClause"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:260:1: orClause returns [Clause clause] : cl= andClause ( OR cl= andClause )* ;
	public final Clause orClause() throws RecognitionException {
		Clause clause = null;


		Clause cl =null;


				final List<Clause> clauses = new ArrayList<Clause>();
			
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:265:2: (cl= andClause ( OR cl= andClause )* )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:265:4: cl= andClause ( OR cl= andClause )*
			{
			pushFollow(FOLLOW_andClause_in_orClause153);
			cl=andClause();
			state._fsp--;

			 clauses.add(cl); 
			// com/atlassian/jira/jql/parser/antlr/Jql.g:265:48: ( OR cl= andClause )*
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( (LA3_0==OR) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:265:49: OR cl= andClause
					{
					match(input,OR,FOLLOW_OR_in_orClause158); 
					pushFollow(FOLLOW_andClause_in_orClause164);
					cl=andClause();
					state._fsp--;

					 clauses.add(cl); 
					}
					break;

				default :
					break loop3;
				}
			}

			clause = clauses.size() == 1? clauses.get(0) : new OrClause(clauses); 
			}

		}

		catch (RecognitionException e) 
		{
			throw e;
		}

		finally {
			// do for sure before leaving
		}
		return clause;
	}
	// $ANTLR end "orClause"



	// $ANTLR start "andClause"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:271:1: andClause returns [Clause clause] : cl= notClause ( AND cl= notClause )* ;
	public final Clause andClause() throws RecognitionException {
		Clause clause = null;


		Clause cl =null;


				final List<Clause> clauses = new ArrayList<Clause>();
			
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:275:2: (cl= notClause ( AND cl= notClause )* )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:275:4: cl= notClause ( AND cl= notClause )*
			{
			pushFollow(FOLLOW_notClause_in_andClause197);
			cl=notClause();
			state._fsp--;

			 clauses.add(cl); 
			// com/atlassian/jira/jql/parser/antlr/Jql.g:275:48: ( AND cl= notClause )*
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( (LA4_0==AND) ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:275:49: AND cl= notClause
					{
					match(input,AND,FOLLOW_AND_in_andClause202); 
					pushFollow(FOLLOW_notClause_in_andClause208);
					cl=notClause();
					state._fsp--;

					 clauses.add(cl); 
					}
					break;

				default :
					break loop4;
				}
			}

			clause = clauses.size() == 1? clauses.get(0) : new AndClause(clauses); 
			}

		}

		catch (RecognitionException e) 
		{
			throw e;
		}

		finally {
			// do for sure before leaving
		}
		return clause;
	}
	// $ANTLR end "andClause"



	// $ANTLR start "notClause"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:281:1: notClause returns [Clause clause] : ( ( NOT | BANG ) nc= notClause | subClause | terminalClause );
	public final Clause notClause() throws RecognitionException {
		Clause clause = null;


		Clause nc =null;
		Clause subClause2 =null;
		Clause terminalClause3 =null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:282:2: ( ( NOT | BANG ) nc= notClause | subClause | terminalClause )
			int alt5=3;
			switch ( input.LA(1) ) {
			case BANG:
			case NOT:
				{
				alt5=1;
				}
				break;
			case LPAREN:
				{
				alt5=2;
				}
				break;
			case CUSTOMFIELD:
			case NEGNUMBER:
			case POSNUMBER:
			case QUOTE_STRING:
			case SQUOTE_STRING:
			case STRING:
				{
				alt5=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 5, 0, input);
				throw nvae;
			}
			switch (alt5) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:282:4: ( NOT | BANG ) nc= notClause
					{
					if ( input.LA(1)==BANG||input.LA(1)==NOT ) {
						input.consume();
						state.errorRecovery=false;
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					pushFollow(FOLLOW_notClause_in_notClause243);
					nc=notClause();
					state._fsp--;

					 clause = new NotClause(nc); 
					}
					break;
				case 2 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:283:4: subClause
					{
					pushFollow(FOLLOW_subClause_in_notClause250);
					subClause2=subClause();
					state._fsp--;

					 clause = subClause2; 
					}
					break;
				case 3 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:284:25: terminalClause
					{
					pushFollow(FOLLOW_terminalClause_in_notClause278);
					terminalClause3=terminalClause();
					state._fsp--;

					 clause = terminalClause3; 
					}
					break;

			}
		}
		catch (NoViableAltException e) {

				    //If there is no option here then we assume that the user meant a field expression.
				    reportError(JqlParseErrorMessages.badFieldName(e.token), e);
				
		}

		finally {
			// do for sure before leaving
		}
		return clause;
	}
	// $ANTLR end "notClause"



	// $ANTLR start "subClause"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:296:1: subClause returns [Clause clause] : LPAREN orClause RPAREN ;
	public final Clause subClause() throws RecognitionException {
		Clause clause = null;


		Clause orClause4 =null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:297:5: ( LPAREN orClause RPAREN )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:297:7: LPAREN orClause RPAREN
			{
			match(input,LPAREN,FOLLOW_LPAREN_in_subClause310); 
			pushFollow(FOLLOW_orClause_in_subClause312);
			orClause4=orClause();
			state._fsp--;

			match(input,RPAREN,FOLLOW_RPAREN_in_subClause314); 
			 clause = orClause4; 
			}

		}
		catch (MismatchedTokenException e) {

			        if (e.expecting == RPAREN)
			        {
			            reportError(JqlParseErrorMessages.expectedText(e.token, ")"), e);
			        }
			        else
			        {
			            throw e;
			        }
			    
		}

		finally {
			// do for sure before leaving
		}
		return clause;
	}
	// $ANTLR end "subClause"



	// $ANTLR start "terminalClause"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:314:1: terminalClause returns [Clause clause] : f= field op= operator ( (opand= operand (pred= historyPredicate )? ) | (chPred= historyPredicate )? ) ;
	public final Clause terminalClause() throws RecognitionException {
		Clause clause = null;


		ParserRuleReturnScope f =null;
		Operator op =null;
		Operand opand =null;
		HistoryPredicate pred =null;
		HistoryPredicate chPred =null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:315:2: (f= field op= operator ( (opand= operand (pred= historyPredicate )? ) | (chPred= historyPredicate )? ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:315:4: f= field op= operator ( (opand= operand (pred= historyPredicate )? ) | (chPred= historyPredicate )? )
			{
			pushFollow(FOLLOW_field_in_terminalClause355);
			f=field();
			state._fsp--;

			pushFollow(FOLLOW_operator_in_terminalClause361);
			op=operator();
			state._fsp--;

			// com/atlassian/jira/jql/parser/antlr/Jql.g:315:29: ( (opand= operand (pred= historyPredicate )? ) | (chPred= historyPredicate )? )
			int alt8=2;
			int LA8_0 = input.LA(1);
			if ( (LA8_0==EMPTY||LA8_0==LPAREN||LA8_0==NEGNUMBER||LA8_0==POSNUMBER||LA8_0==QUOTE_STRING||(LA8_0 >= SQUOTE_STRING && LA8_0 <= STRING)) ) {
				alt8=1;
			}
			else if ( (LA8_0==EOF||LA8_0==AFTER||LA8_0==AND||LA8_0==BEFORE||LA8_0==BY||LA8_0==DURING||LA8_0==FROM||(LA8_0 >= ON && LA8_0 <= ORDER)||LA8_0==RPAREN||LA8_0==TO) ) {
				alt8=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 8, 0, input);
				throw nvae;
			}

			switch (alt8) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:315:31: (opand= operand (pred= historyPredicate )? )
					{
					// com/atlassian/jira/jql/parser/antlr/Jql.g:315:31: (opand= operand (pred= historyPredicate )? )
					// com/atlassian/jira/jql/parser/antlr/Jql.g:315:32: opand= operand (pred= historyPredicate )?
					{
					pushFollow(FOLLOW_operand_in_terminalClause371);
					opand=operand();
					state._fsp--;

					// com/atlassian/jira/jql/parser/antlr/Jql.g:315:52: (pred= historyPredicate )?
					int alt6=2;
					int LA6_0 = input.LA(1);
					if ( (LA6_0==AFTER||LA6_0==BEFORE||LA6_0==BY||LA6_0==DURING||LA6_0==FROM||LA6_0==ON||LA6_0==TO) ) {
						alt6=1;
					}
					switch (alt6) {
						case 1 :
							// com/atlassian/jira/jql/parser/antlr/Jql.g:315:52: pred= historyPredicate
							{
							pushFollow(FOLLOW_historyPredicate_in_terminalClause376);
							pred=historyPredicate();
							state._fsp--;

							}
							break;

					}

					}

					}
					break;
				case 2 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:315:75: (chPred= historyPredicate )?
					{
					// com/atlassian/jira/jql/parser/antlr/Jql.g:315:81: (chPred= historyPredicate )?
					int alt7=2;
					int LA7_0 = input.LA(1);
					if ( (LA7_0==AFTER||LA7_0==BEFORE||LA7_0==BY||LA7_0==DURING||LA7_0==FROM||LA7_0==ON||LA7_0==TO) ) {
						alt7=1;
					}
					switch (alt7) {
						case 1 :
							// com/atlassian/jira/jql/parser/antlr/Jql.g:315:81: chPred= historyPredicate
							{
							pushFollow(FOLLOW_historyPredicate_in_terminalClause384);
							chPred=historyPredicate();
							state._fsp--;

							}
							break;

					}

					}
					break;

			}


			        if (f != null && (f!=null?((JqlParser.field_return)f).field:null).isEntityProperty())
			        {
			            if (opand == null)
			            {
			                final RecognitionException e = new RecognitionException(input);
			                reportError(JqlParseErrorMessages.badOperand(e.token), e);
			            }
			            clause = new TerminalClauseImpl((f!=null?((JqlParser.field_return)f).field:null).getName(), op, opand, Option.some((f!=null?((JqlParser.field_return)f).field:null).getProperty()));
			        }
				    else if (op  == Operator.CHANGED)
				    {
			            if (opand != null)
			            {
			                final RecognitionException e = new RecognitionException(input);
			                reportError(JqlParseErrorMessages.unsupportedOperand(op.toString(), opand.getDisplayString()),e);
			            }
			            clause = new ChangedClauseImpl((f!=null?((JqlParser.field_return)f).field:null).getName(), op, chPred);
				    }
				    else
				    {
				       if (op == Operator.WAS || op == Operator.WAS_NOT || op == Operator.WAS_IN || op == Operator.WAS_NOT_IN )
					   {
					        if (opand == null)
			                {
			                    final RecognitionException e = new RecognitionException(input);
			                    reportError(JqlParseErrorMessages.badOperand(e.token), e);
			                }
			                clause = new WasClauseImpl((f!=null?((JqlParser.field_return)f).field:null).getName(), op, opand, pred);
			                supportsHistoryPredicate=true;
			           }
			           else
			           {
					        if (opand == null)
			                {
			                    final RecognitionException e = new RecognitionException(input);
			                    reportError(JqlParseErrorMessages.badOperand(e.token), e);
			                }
			                clause = new TerminalClauseImpl((f!=null?((JqlParser.field_return)f).field:null).getName(), op, opand);
			                supportsHistoryPredicate=false;
			                if (pred != null)
			                {
			                    final JqlParseErrorMessage errorMessage = JqlParseErrorMessages.unsupportedPredicate(pred.getDisplayString(), op.toString());
			                    JqlParseException exception = new JqlParseException(errorMessage);
			                    reportError(errorMessage, exception);
			                }
			           }
			        }
				
			}

		}
		catch (RecognitionException e) {

				    //Because of the ANTLR lookahead, these ain't actually called ;-)
				    if (f == null)
				    {
				        reportError(JqlParseErrorMessages.badFieldName(e.token), e);
				    }
				    else if (op == null)
				    {
				        reportError(JqlParseErrorMessages.badOperator(e.token), e);
				    }
				    else if (opand == null)
				    {
				        reportError(JqlParseErrorMessages.badOperand(e.token), e);
				    }
				    else
				    {
				        reportError(JqlParseErrorMessages.genericParseError(e.token), e);
				    }
				
		}

		finally {
			// do for sure before leaving
		}
		return clause;
	}
	// $ANTLR end "terminalClause"



	// $ANTLR start "historyPredicate"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:388:1: historyPredicate returns [HistoryPredicate predicate] : (p= terminalHistoryPredicate )+ ;
	public final HistoryPredicate historyPredicate() throws RecognitionException {
		HistoryPredicate predicate = null;


		HistoryPredicate p =null;


				final List<HistoryPredicate> predicates = new ArrayList<HistoryPredicate>();
			
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:392:2: ( (p= terminalHistoryPredicate )+ )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:392:4: (p= terminalHistoryPredicate )+
			{
			// com/atlassian/jira/jql/parser/antlr/Jql.g:392:4: (p= terminalHistoryPredicate )+
			int cnt9=0;
			loop9:
			while (true) {
				int alt9=2;
				int LA9_0 = input.LA(1);
				if ( (LA9_0==AFTER||LA9_0==BEFORE||LA9_0==BY||LA9_0==DURING||LA9_0==FROM||LA9_0==ON||LA9_0==TO) ) {
					alt9=1;
				}

				switch (alt9) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:392:5: p= terminalHistoryPredicate
					{
					pushFollow(FOLLOW_terminalHistoryPredicate_in_historyPredicate424);
					p=terminalHistoryPredicate();
					state._fsp--;

					 predicates.add(p);
					}
					break;

				default :
					if ( cnt9 >= 1 ) break loop9;
					EarlyExitException eee = new EarlyExitException(9, input);
					throw eee;
				}
				cnt9++;
			}

			predicate = predicates.size() == 1? predicates.get(0) : new AndHistoryPredicate(predicates); 
			}

		}

		catch (RecognitionException e) 
		{
			throw e;
		}

		finally {
			// do for sure before leaving
		}
		return predicate;
	}
	// $ANTLR end "historyPredicate"



	// $ANTLR start "terminalHistoryPredicate"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:456:1: terminalHistoryPredicate returns [HistoryPredicate predicate] : historyPredicateOperator operand ;
	public final HistoryPredicate terminalHistoryPredicate() throws RecognitionException {
		HistoryPredicate predicate = null;


		Operator historyPredicateOperator5 =null;
		Operand operand6 =null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:457:2: ( historyPredicateOperator operand )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:457:4: historyPredicateOperator operand
			{
			pushFollow(FOLLOW_historyPredicateOperator_in_terminalHistoryPredicate448);
			historyPredicateOperator5=historyPredicateOperator();
			state._fsp--;

			pushFollow(FOLLOW_operand_in_terminalHistoryPredicate450);
			operand6=operand();
			state._fsp--;


					predicate = new TerminalHistoryPredicate(historyPredicateOperator5, operand6);
				
			}

		}

		catch (RecognitionException e) 
		{
			throw e;
		}

		finally {
			// do for sure before leaving
		}
		return predicate;
	}
	// $ANTLR end "terminalHistoryPredicate"



	// $ANTLR start "historyPredicateOperator"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:464:1: historyPredicateOperator returns [Operator operator] : ( FROM | TO | BY | BEFORE | AFTER | ON | DURING );
	public final Operator historyPredicateOperator() throws RecognitionException {
		Operator operator = null;


		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:465:2: ( FROM | TO | BY | BEFORE | AFTER | ON | DURING )
			int alt10=7;
			switch ( input.LA(1) ) {
			case FROM:
				{
				alt10=1;
				}
				break;
			case TO:
				{
				alt10=2;
				}
				break;
			case BY:
				{
				alt10=3;
				}
				break;
			case BEFORE:
				{
				alt10=4;
				}
				break;
			case AFTER:
				{
				alt10=5;
				}
				break;
			case ON:
				{
				alt10=6;
				}
				break;
			case DURING:
				{
				alt10=7;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 10, 0, input);
				throw nvae;
			}
			switch (alt10) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:465:4: FROM
					{
					match(input,FROM,FOLLOW_FROM_in_historyPredicateOperator469); 
					 operator = Operator.FROM; 
					}
					break;
				case 2 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:466:4: TO
					{
					match(input,TO,FOLLOW_TO_in_historyPredicateOperator476); 
					operator = Operator.TO; 
					}
					break;
				case 3 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:467:4: BY
					{
					match(input,BY,FOLLOW_BY_in_historyPredicateOperator483); 
					operator = Operator.BY; 
					}
					break;
				case 4 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:468:4: BEFORE
					{
					match(input,BEFORE,FOLLOW_BEFORE_in_historyPredicateOperator490); 
					operator = Operator.BEFORE; 
					}
					break;
				case 5 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:469:4: AFTER
					{
					match(input,AFTER,FOLLOW_AFTER_in_historyPredicateOperator497); 
					operator = Operator.AFTER; 
					}
					break;
				case 6 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:470:4: ON
					{
					match(input,ON,FOLLOW_ON_in_historyPredicateOperator504); 
					operator = Operator.ON; 
					}
					break;
				case 7 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:471:4: DURING
					{
					match(input,DURING,FOLLOW_DURING_in_historyPredicateOperator512); 
					operator = Operator.DURING;
					}
					break;

			}
		}

		catch (RecognitionException e) 
		{
			throw e;
		}

		finally {
			// do for sure before leaving
		}
		return operator;
	}
	// $ANTLR end "historyPredicateOperator"



	// $ANTLR start "operator"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:478:1: operator returns [Operator operator] : ( EQUALS | NOT_EQUALS | LIKE | NOT_LIKE | LT | GT | LTEQ | GTEQ | IN | IS NOT | IS | NOT IN | WAS | WAS NOT | WAS IN | WAS NOT IN | CHANGED );
	public final Operator operator() throws RecognitionException {
		Operator operator = null;


		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:479:2: ( EQUALS | NOT_EQUALS | LIKE | NOT_LIKE | LT | GT | LTEQ | GTEQ | IN | IS NOT | IS | NOT IN | WAS | WAS NOT | WAS IN | WAS NOT IN | CHANGED )
			int alt11=17;
			switch ( input.LA(1) ) {
			case EQUALS:
				{
				alt11=1;
				}
				break;
			case NOT_EQUALS:
				{
				alt11=2;
				}
				break;
			case LIKE:
				{
				alt11=3;
				}
				break;
			case NOT_LIKE:
				{
				alt11=4;
				}
				break;
			case LT:
				{
				alt11=5;
				}
				break;
			case GT:
				{
				alt11=6;
				}
				break;
			case LTEQ:
				{
				alt11=7;
				}
				break;
			case GTEQ:
				{
				alt11=8;
				}
				break;
			case IN:
				{
				alt11=9;
				}
				break;
			case IS:
				{
				int LA11_10 = input.LA(2);
				if ( (LA11_10==NOT) ) {
					alt11=10;
				}
				else if ( (LA11_10==EOF||LA11_10==AFTER||LA11_10==AND||LA11_10==BEFORE||LA11_10==BY||(LA11_10 >= DURING && LA11_10 <= EMPTY)||LA11_10==FROM||LA11_10==LPAREN||LA11_10==NEGNUMBER||(LA11_10 >= ON && LA11_10 <= ORDER)||LA11_10==POSNUMBER||LA11_10==QUOTE_STRING||LA11_10==RPAREN||(LA11_10 >= SQUOTE_STRING && LA11_10 <= STRING)||LA11_10==TO) ) {
					alt11=11;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 11, 10, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case NOT:
				{
				alt11=12;
				}
				break;
			case WAS:
				{
				switch ( input.LA(2) ) {
				case NOT:
					{
					int LA11_16 = input.LA(3);
					if ( (LA11_16==IN) ) {
						alt11=16;
					}
					else if ( (LA11_16==EOF||LA11_16==AFTER||LA11_16==AND||LA11_16==BEFORE||LA11_16==BY||(LA11_16 >= DURING && LA11_16 <= EMPTY)||LA11_16==FROM||LA11_16==LPAREN||LA11_16==NEGNUMBER||(LA11_16 >= ON && LA11_16 <= ORDER)||LA11_16==POSNUMBER||LA11_16==QUOTE_STRING||LA11_16==RPAREN||(LA11_16 >= SQUOTE_STRING && LA11_16 <= STRING)||LA11_16==TO) ) {
						alt11=14;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 11, 16, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

					}
					break;
				case IN:
					{
					alt11=15;
					}
					break;
				case EOF:
				case AFTER:
				case AND:
				case BEFORE:
				case BY:
				case DURING:
				case EMPTY:
				case FROM:
				case LPAREN:
				case NEGNUMBER:
				case ON:
				case OR:
				case ORDER:
				case POSNUMBER:
				case QUOTE_STRING:
				case RPAREN:
				case SQUOTE_STRING:
				case STRING:
				case TO:
					{
					alt11=13;
					}
					break;
				default:
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 11, 12, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}
				}
				break;
			case CHANGED:
				{
				alt11=17;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 11, 0, input);
				throw nvae;
			}
			switch (alt11) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:479:4: EQUALS
					{
					match(input,EQUALS,FOLLOW_EQUALS_in_operator533); 
					 operator = Operator.EQUALS; 
					}
					break;
				case 2 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:480:4: NOT_EQUALS
					{
					match(input,NOT_EQUALS,FOLLOW_NOT_EQUALS_in_operator540); 
					 operator = Operator.NOT_EQUALS; 
					}
					break;
				case 3 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:481:4: LIKE
					{
					match(input,LIKE,FOLLOW_LIKE_in_operator547); 
					 operator = Operator.LIKE; 
					}
					break;
				case 4 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:482:4: NOT_LIKE
					{
					match(input,NOT_LIKE,FOLLOW_NOT_LIKE_in_operator554); 
					 operator = Operator.NOT_LIKE; 
					}
					break;
				case 5 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:483:4: LT
					{
					match(input,LT,FOLLOW_LT_in_operator562); 
					 operator = Operator.LESS_THAN; 
					}
					break;
				case 6 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:484:4: GT
					{
					match(input,GT,FOLLOW_GT_in_operator569); 
					 operator = Operator.GREATER_THAN; 
					}
					break;
				case 7 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:485:4: LTEQ
					{
					match(input,LTEQ,FOLLOW_LTEQ_in_operator576); 
					 operator = Operator.LESS_THAN_EQUALS; 
					}
					break;
				case 8 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:486:4: GTEQ
					{
					match(input,GTEQ,FOLLOW_GTEQ_in_operator583); 
					 operator = Operator.GREATER_THAN_EQUALS; 
					}
					break;
				case 9 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:487:4: IN
					{
					match(input,IN,FOLLOW_IN_in_operator590); 
					 operator = Operator.IN; 
					}
					break;
				case 10 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:488:4: IS NOT
					{
					match(input,IS,FOLLOW_IS_in_operator597); 
					match(input,NOT,FOLLOW_NOT_in_operator599); 
					 operator = Operator.IS_NOT; 
					}
					break;
				case 11 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:489:4: IS
					{
					match(input,IS,FOLLOW_IS_in_operator606); 
					 operator = Operator.IS; 
					}
					break;
				case 12 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:490:4: NOT IN
					{
					match(input,NOT,FOLLOW_NOT_in_operator613); 
					match(input,IN,FOLLOW_IN_in_operator615); 
					 operator = Operator.NOT_IN; 
					}
					break;
				case 13 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:491:4: WAS
					{
					match(input,WAS,FOLLOW_WAS_in_operator622); 
					 operator = Operator.WAS; 
					}
					break;
				case 14 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:492:4: WAS NOT
					{
					match(input,WAS,FOLLOW_WAS_in_operator629); 
					match(input,NOT,FOLLOW_NOT_in_operator631); 
					 operator = Operator.WAS_NOT; 
					}
					break;
				case 15 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:493:9: WAS IN
					{
					match(input,WAS,FOLLOW_WAS_in_operator643); 
					match(input,IN,FOLLOW_IN_in_operator645); 
					 operator = Operator.WAS_IN; 
					}
					break;
				case 16 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:494:9: WAS NOT IN
					{
					match(input,WAS,FOLLOW_WAS_in_operator657); 
					match(input,NOT,FOLLOW_NOT_in_operator659); 
					match(input,IN,FOLLOW_IN_in_operator661); 
					 operator = Operator.WAS_NOT_IN; 
					}
					break;
				case 17 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:495:9: CHANGED
					{
					match(input,CHANGED,FOLLOW_CHANGED_in_operator673); 
					 operator = Operator.CHANGED; 
					}
					break;

			}
		}
		catch (MismatchedTokenException e) {

				    //This will only get thrown when we read in "IS" or "NOT" not followed by its correct string.
			        if (e.expecting == NOT)
			        {
			            reportError(JqlParseErrorMessages.expectedText(e.token, "NOT"), e);
			        }
			        else if (e.expecting == IN)
			        {
			            reportError(JqlParseErrorMessages.expectedText(e.token, "IN"), e);
			        }
			        else
			        {
			            //We will do this just in case.
			            reportError(JqlParseErrorMessages.badOperator(e.token), e);
			        }
				
		}
		catch (RecognitionException e) {

			        Token currentToken = input.LT(1);
			        if (currentToken.getType() == IS)
			        {
			            //This happens when we get an IS is not followed by a NOT.
			            reportError(JqlParseErrorMessages.expectedText(e.token, "NOT"), e);
			        }
			        else if (currentToken.getType() == NOT)
			        {
			            //This happens when we get a NOT is not followed by a IN.
			            reportError(JqlParseErrorMessages.expectedText(e.token, "IN"), e);
			        }
			        else if (currentToken.getType() == WAS)
			        {
			            reportError(JqlParseErrorMessages.badOperand(e.token),e);
			        }
			        else
			        {
			            reportError(JqlParseErrorMessages.badOperator(e.token), e);
			        }
			    
		}

		finally {
			// do for sure before leaving
		}
		return operator;
	}
	// $ANTLR end "operator"


	public static class field_return extends ParserRuleReturnScope {
		public FieldReference field;
	};


	// $ANTLR start "field"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:540:1: field returns [FieldReference field] : (num= numberString | ( (str= string |cf= customField ) ( ( LBRACKET (ref= argument ) RBRACKET ) (ref= propertyArgument )* )* ) );
	public final JqlParser.field_return field() throws RecognitionException {
		JqlParser.field_return retval = new JqlParser.field_return();
		retval.start = input.LT(1);

		ParserRuleReturnScope num =null;
		ParserRuleReturnScope str =null;
		String cf =null;
		String ref =null;


		        ArrayList names = new ArrayList();
		        ArrayList arrays = new ArrayList();
		        ArrayList propertyRefs = new ArrayList();
		    
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:549:2: (num= numberString | ( (str= string |cf= customField ) ( ( LBRACKET (ref= argument ) RBRACKET ) (ref= propertyArgument )* )* ) )
			int alt15=2;
			int LA15_0 = input.LA(1);
			if ( (LA15_0==NEGNUMBER||LA15_0==POSNUMBER) ) {
				alt15=1;
			}
			else if ( (LA15_0==CUSTOMFIELD||LA15_0==QUOTE_STRING||(LA15_0 >= SQUOTE_STRING && LA15_0 <= STRING)) ) {
				alt15=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 15, 0, input);
				throw nvae;
			}

			switch (alt15) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:550:5: num= numberString
					{
					pushFollow(FOLLOW_numberString_in_field739);
					num=numberString();
					state._fsp--;

					 names.add((num!=null?((JqlParser.numberString_return)num).string:null)); 
					}
					break;
				case 2 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:552:5: ( (str= string |cf= customField ) ( ( LBRACKET (ref= argument ) RBRACKET ) (ref= propertyArgument )* )* )
					{
					// com/atlassian/jira/jql/parser/antlr/Jql.g:552:5: ( (str= string |cf= customField ) ( ( LBRACKET (ref= argument ) RBRACKET ) (ref= propertyArgument )* )* )
					// com/atlassian/jira/jql/parser/antlr/Jql.g:553:9: (str= string |cf= customField ) ( ( LBRACKET (ref= argument ) RBRACKET ) (ref= propertyArgument )* )*
					{
					// com/atlassian/jira/jql/parser/antlr/Jql.g:553:9: (str= string |cf= customField )
					int alt12=2;
					int LA12_0 = input.LA(1);
					if ( (LA12_0==QUOTE_STRING||(LA12_0 >= SQUOTE_STRING && LA12_0 <= STRING)) ) {
						alt12=1;
					}
					else if ( (LA12_0==CUSTOMFIELD) ) {
						alt12=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 12, 0, input);
						throw nvae;
					}

					switch (alt12) {
						case 1 :
							// com/atlassian/jira/jql/parser/antlr/Jql.g:554:13: str= string
							{
							pushFollow(FOLLOW_string_in_field781);
							str=string();
							state._fsp--;

							 names.add(checkFieldName((str!=null?(str.start):null))); 
							}
							break;
						case 2 :
							// com/atlassian/jira/jql/parser/antlr/Jql.g:555:15: cf= customField
							{
							pushFollow(FOLLOW_customField_in_field803);
							cf=customField();
							state._fsp--;

							 names.add(cf); 
							}
							break;

					}

					// com/atlassian/jira/jql/parser/antlr/Jql.g:557:9: ( ( LBRACKET (ref= argument ) RBRACKET ) (ref= propertyArgument )* )*
					loop14:
					while (true) {
						int alt14=2;
						int LA14_0 = input.LA(1);
						if ( (LA14_0==LBRACKET) ) {
							alt14=1;
						}

						switch (alt14) {
						case 1 :
							// com/atlassian/jira/jql/parser/antlr/Jql.g:558:10: ( LBRACKET (ref= argument ) RBRACKET ) (ref= propertyArgument )*
							{
							// com/atlassian/jira/jql/parser/antlr/Jql.g:558:10: ( LBRACKET (ref= argument ) RBRACKET )
							// com/atlassian/jira/jql/parser/antlr/Jql.g:559:14: LBRACKET (ref= argument ) RBRACKET
							{
							match(input,LBRACKET,FOLLOW_LBRACKET_in_field851); 
							// com/atlassian/jira/jql/parser/antlr/Jql.g:560:14: (ref= argument )
							// com/atlassian/jira/jql/parser/antlr/Jql.g:561:18: ref= argument
							{
							pushFollow(FOLLOW_argument_in_field889);
							ref=argument();
							state._fsp--;

							arrays.add(ref);
							}

							match(input,RBRACKET,FOLLOW_RBRACKET_in_field921); 
							}

							// com/atlassian/jira/jql/parser/antlr/Jql.g:565:10: (ref= propertyArgument )*
							loop13:
							while (true) {
								int alt13=2;
								int LA13_0 = input.LA(1);
								if ( (LA13_0==NEGNUMBER||LA13_0==POSNUMBER||LA13_0==QUOTE_STRING||(LA13_0 >= SQUOTE_STRING && LA13_0 <= STRING)) ) {
									alt13=1;
								}

								switch (alt13) {
								case 1 :
									// com/atlassian/jira/jql/parser/antlr/Jql.g:566:14: ref= propertyArgument
									{
									pushFollow(FOLLOW_propertyArgument_in_field962);
									ref=propertyArgument();
									state._fsp--;

									propertyRefs.add(ref);
									}
									break;

								default :
									break loop13;
								}
							}

							}
							break;

						default :
							break loop14;
						}
					}

					}

					}
					break;

			}
			retval.stop = input.LT(-1);


			        retval.field = new FieldReference(names, arrays, propertyRefs);
			    
		}
		catch (MismatchedTokenException e) {

				    switch (e.expecting)
				    {
				        case LBRACKET:
			                reportError(JqlParseErrorMessages.expectedText(e.token, "["), e);
			                break;
			            case RBRACKET:
			                reportError(JqlParseErrorMessages.expectedText(e.token, "]"), e);
			                break;
				    }
				
		}
		catch (EarlyExitException e) {

			        reportError(JqlParseErrorMessages.badPropertyArgument(e.token), e);
				
		}
		catch (RecognitionException e) {

				    if (e.token.getType() == LBRACKET)
			        {
			            //We probably have some sort of custom field id that does not start with cf. Lets tell the user all about it.
			            reportError(JqlParseErrorMessages.expectedText(e.token, "cf"), e);
			        }
			        else
			        {
			            reportError(JqlParseErrorMessages.badFieldName(e.token), e);
			        }
				
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "field"



	// $ANTLR start "customField"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:600:1: customField returns [String field] : CUSTOMFIELD LBRACKET posnum= POSNUMBER RBRACKET ;
	public final String customField() throws RecognitionException {
		String field = null;


		Token posnum=null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:601:5: ( CUSTOMFIELD LBRACKET posnum= POSNUMBER RBRACKET )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:601:7: CUSTOMFIELD LBRACKET posnum= POSNUMBER RBRACKET
			{
			match(input,CUSTOMFIELD,FOLLOW_CUSTOMFIELD_in_customField1032); 
			match(input,LBRACKET,FOLLOW_LBRACKET_in_customField1034); 
			posnum=(Token)match(input,POSNUMBER,FOLLOW_POSNUMBER_in_customField1040); 
			match(input,RBRACKET,FOLLOW_RBRACKET_in_customField1042); 
			 field = JqlCustomFieldId.toString(parseLong(posnum)); 
			}

		}
		catch (MismatchedTokenException e) {

			        switch(e.expecting)
			        {
			            case CUSTOMFIELD:
			                reportError(JqlParseErrorMessages.expectedText(e.token, "cf"), e);
			                break;
			            case LBRACKET:
			                reportError(JqlParseErrorMessages.expectedText(e.token, "["), e);
			                break;
			            case POSNUMBER:
			                reportError(JqlParseErrorMessages.badCustomFieldId(e.token), e);
			                break;
			            case RBRACKET:
			                reportError(JqlParseErrorMessages.expectedText(e.token, "]"), e);
			                break;
			            default:
			                throw e;
			        }
			    
		}

		finally {
			// do for sure before leaving
		}
		return field;
	}
	// $ANTLR end "customField"



	// $ANTLR start "fieldCheck"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:627:1: fieldCheck returns [FieldReference field] : f= field EOF ;
	public final FieldReference fieldCheck() throws RecognitionException {
		FieldReference field = null;


		ParserRuleReturnScope f =null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:628:2: (f= field EOF )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:628:4: f= field EOF
			{
			pushFollow(FOLLOW_field_in_fieldCheck1082);
			f=field();
			state._fsp--;

			 field = (f!=null?((JqlParser.field_return)f).field:null); 
			match(input,EOF,FOLLOW_EOF_in_fieldCheck1086); 
			}

		}

		catch (RecognitionException e) 
		{
			throw e;
		}

		finally {
			// do for sure before leaving
		}
		return field;
	}
	// $ANTLR end "fieldCheck"



	// $ANTLR start "operand"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:634:1: operand returns [Operand operand] : ( EMPTY |str= string |number= numberString |fn= func |l= list );
	public final Operand operand() throws RecognitionException {
		Operand operand = null;


		ParserRuleReturnScope str =null;
		ParserRuleReturnScope number =null;
		FunctionOperand fn =null;
		Operand l =null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:635:2: ( EMPTY |str= string |number= numberString |fn= func |l= list )
			int alt16=5;
			switch ( input.LA(1) ) {
			case EMPTY:
				{
				alt16=1;
				}
				break;
			case STRING:
				{
				int LA16_2 = input.LA(2);
				if ( (LA16_2==EOF||LA16_2==AFTER||LA16_2==AND||LA16_2==BEFORE||LA16_2==BY||LA16_2==COMMA||LA16_2==DURING||LA16_2==FROM||(LA16_2 >= ON && LA16_2 <= ORDER)||LA16_2==RPAREN||LA16_2==TO) ) {
					alt16=2;
				}
				else if ( (LA16_2==LPAREN) ) {
					alt16=4;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 16, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case QUOTE_STRING:
				{
				int LA16_3 = input.LA(2);
				if ( (LA16_3==EOF||LA16_3==AFTER||LA16_3==AND||LA16_3==BEFORE||LA16_3==BY||LA16_3==COMMA||LA16_3==DURING||LA16_3==FROM||(LA16_3 >= ON && LA16_3 <= ORDER)||LA16_3==RPAREN||LA16_3==TO) ) {
					alt16=2;
				}
				else if ( (LA16_3==LPAREN) ) {
					alt16=4;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 16, 3, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case SQUOTE_STRING:
				{
				int LA16_4 = input.LA(2);
				if ( (LA16_4==EOF||LA16_4==AFTER||LA16_4==AND||LA16_4==BEFORE||LA16_4==BY||LA16_4==COMMA||LA16_4==DURING||LA16_4==FROM||(LA16_4 >= ON && LA16_4 <= ORDER)||LA16_4==RPAREN||LA16_4==TO) ) {
					alt16=2;
				}
				else if ( (LA16_4==LPAREN) ) {
					alt16=4;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 16, 4, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case NEGNUMBER:
			case POSNUMBER:
				{
				int LA16_5 = input.LA(2);
				if ( (LA16_5==EOF||LA16_5==AFTER||LA16_5==AND||LA16_5==BEFORE||LA16_5==BY||LA16_5==COMMA||LA16_5==DURING||LA16_5==FROM||(LA16_5 >= ON && LA16_5 <= ORDER)||LA16_5==RPAREN||LA16_5==TO) ) {
					alt16=3;
				}
				else if ( (LA16_5==LPAREN) ) {
					alt16=4;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 16, 5, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case LPAREN:
				{
				alt16=5;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 16, 0, input);
				throw nvae;
			}
			switch (alt16) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:635:4: EMPTY
					{
					match(input,EMPTY,FOLLOW_EMPTY_in_operand1103); 
					 operand = new EmptyOperand(); 
					}
					break;
				case 2 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:636:4: str= string
					{
					pushFollow(FOLLOW_string_in_operand1114);
					str=string();
					state._fsp--;

					 operand = new SingleValueOperand((str!=null?((JqlParser.string_return)str).string:null)); 
					}
					break;
				case 3 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:637:4: number= numberString
					{
					pushFollow(FOLLOW_numberString_in_operand1125);
					number=numberString();
					state._fsp--;

					 operand = new SingleValueOperand(parseLong((number!=null?(number.start):null))); 
					}
					break;
				case 4 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:638:4: fn= func
					{
					pushFollow(FOLLOW_func_in_operand1136);
					fn=func();
					state._fsp--;

					operand = fn;
					}
					break;
				case 5 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:639:4: l= list
					{
					pushFollow(FOLLOW_list_in_operand1147);
					l=list();
					state._fsp--;

					operand = l;
					}
					break;

			}
		}
		catch (NoViableAltException e) {

			        final Token currentToken = input.LT(1);
			        final Token errorToken = e.token;

			        //HACKETY, HACKETY HACK. ANTLR uses a DFA to decide which type of operand we are going to handle. This DFA
			        //will only find "string" if it is followed by one of {EOF, OR, AND, RPAREN, COMMA, ORDER}. This means that
			        //a query like "a=b c=d" will actually fail here and appear to be an illegal operand. So we use a simple
			        //heuristic here. If the currentToken != errorToken then it means we have a valid value its just that we
			        //have not seen one of the expected trailing tokens.

			        if (currentToken.getTokenIndex() < errorToken.getTokenIndex())
			        {
			            if (operandLevel <= 0)
			            {
			                //If not in a MutliValueOperand, we probably mean "AND" and "OR"
			                reportError(JqlParseErrorMessages.needLogicalOperator(errorToken), e);
			            }
			            else
			            {
			                //If in a MutliValueOperand, we probably mean "," or ")".
			                reportError(JqlParseErrorMessages.expectedText(errorToken, ",", ")"), e);
			            }
			        }
			        else
			        {
			            reportError(JqlParseErrorMessages.badOperand(errorToken), e);
			        }
				
		}
		catch (RecognitionException e) {

				    reportError(JqlParseErrorMessages.badOperand(e.token), e);
				
		}

		finally {
			// do for sure before leaving
		}
		return operand;
	}
	// $ANTLR end "operand"


	public static class string_return extends ParserRuleReturnScope {
		public String string;
	};


	// $ANTLR start "string"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:678:1: string returns [String string] : (str= STRING |str= QUOTE_STRING |str= SQUOTE_STRING );
	public final JqlParser.string_return string() throws RecognitionException {
		JqlParser.string_return retval = new JqlParser.string_return();
		retval.start = input.LT(1);

		Token str=null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:679:2: (str= STRING |str= QUOTE_STRING |str= SQUOTE_STRING )
			int alt17=3;
			switch ( input.LA(1) ) {
			case STRING:
				{
				alt17=1;
				}
				break;
			case QUOTE_STRING:
				{
				alt17=2;
				}
				break;
			case SQUOTE_STRING:
				{
				alt17=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 17, 0, input);
				throw nvae;
			}
			switch (alt17) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:679:4: str= STRING
					{
					str=(Token)match(input,STRING,FOLLOW_STRING_in_string1187); 
					 retval.string = (str!=null?str.getText():null); 
					}
					break;
				case 2 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:680:4: str= QUOTE_STRING
					{
					str=(Token)match(input,QUOTE_STRING,FOLLOW_QUOTE_STRING_in_string1198); 
					 retval.string = (str!=null?str.getText():null); 
					}
					break;
				case 3 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:681:4: str= SQUOTE_STRING
					{
					str=(Token)match(input,SQUOTE_STRING,FOLLOW_SQUOTE_STRING_in_string1209); 
					 retval.string = (str!=null?str.getText():null); 
					}
					break;

			}
			retval.stop = input.LT(-1);

		}

		catch (RecognitionException e) 
		{
			throw e;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "string"


	public static class numberString_return extends ParserRuleReturnScope {
		public String string;
	};


	// $ANTLR start "numberString"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:684:1: numberString returns [String string] : num= ( POSNUMBER | NEGNUMBER ) ;
	public final JqlParser.numberString_return numberString() throws RecognitionException {
		JqlParser.numberString_return retval = new JqlParser.numberString_return();
		retval.start = input.LT(1);

		Token num=null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:685:2: (num= ( POSNUMBER | NEGNUMBER ) )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:685:4: num= ( POSNUMBER | NEGNUMBER )
			{
			num=input.LT(1);
			if ( input.LA(1)==NEGNUMBER||input.LA(1)==POSNUMBER ) {
				input.consume();
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			 retval.string = (num!=null?num.getText():null); 
			}

			retval.stop = input.LT(-1);

		}

		catch (RecognitionException e) 
		{
			throw e;
		}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "numberString"



	// $ANTLR start "stringValueCheck"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:691:1: stringValueCheck returns [String string] : str= string EOF ;
	public final String stringValueCheck() throws RecognitionException {
		String string = null;


		ParserRuleReturnScope str =null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:692:2: (str= string EOF )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:692:4: str= string EOF
			{
			pushFollow(FOLLOW_string_in_stringValueCheck1259);
			str=string();
			state._fsp--;

			 string = (str!=null?((JqlParser.string_return)str).string:null); 
			match(input,EOF,FOLLOW_EOF_in_stringValueCheck1263); 
			}

		}

		catch (RecognitionException e) 
		{
			throw e;
		}

		finally {
			// do for sure before leaving
		}
		return string;
	}
	// $ANTLR end "stringValueCheck"



	// $ANTLR start "list"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:698:1: list returns [Operand list] : LPAREN opnd= operand ( COMMA opnd= operand )* RPAREN ;
	public final Operand list() throws RecognitionException {
		Operand list = null;


		Operand opnd =null;


				final List <Operand> args = new ArrayList<Operand>();
				operandLevel++;
			
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:706:2: ( LPAREN opnd= operand ( COMMA opnd= operand )* RPAREN )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:706:4: LPAREN opnd= operand ( COMMA opnd= operand )* RPAREN
			{
			match(input,LPAREN,FOLLOW_LPAREN_in_list1293); 
			pushFollow(FOLLOW_operand_in_list1299);
			opnd=operand();
			state._fsp--;

			args.add(opnd);
			// com/atlassian/jira/jql/parser/antlr/Jql.g:707:3: ( COMMA opnd= operand )*
			loop18:
			while (true) {
				int alt18=2;
				int LA18_0 = input.LA(1);
				if ( (LA18_0==COMMA) ) {
					alt18=1;
				}

				switch (alt18) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:707:4: COMMA opnd= operand
					{
					match(input,COMMA,FOLLOW_COMMA_in_list1307); 
					pushFollow(FOLLOW_operand_in_list1313);
					opnd=operand();
					state._fsp--;

					args.add(opnd);
					}
					break;

				default :
					break loop18;
				}
			}

			match(input,RPAREN,FOLLOW_RPAREN_in_list1319); 
			list = new MultiValueOperand(args);
			}


				    operandLevel--;
				
		}
		catch (MismatchedTokenException e) {

			        if (e.expecting == RPAREN)
			        {
			            reportError(JqlParseErrorMessages.expectedText(e.token, ")"), e);
			        }
			        else
			        {
			            throw e;
			        }
			    
		}

		finally {
			// do for sure before leaving
		}
		return list;
	}
	// $ANTLR end "list"



	// $ANTLR start "func"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:724:1: func returns [FunctionOperand func] : fname= funcName LPAREN ( arglist )? RPAREN ;
	public final FunctionOperand func() throws RecognitionException {
		FunctionOperand func = null;


		String fname =null;
		List<String> arglist7 =null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:725:2: (fname= funcName LPAREN ( arglist )? RPAREN )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:725:4: fname= funcName LPAREN ( arglist )? RPAREN
			{
			pushFollow(FOLLOW_funcName_in_func1357);
			fname=funcName();
			state._fsp--;

			match(input,LPAREN,FOLLOW_LPAREN_in_func1359); 
			// com/atlassian/jira/jql/parser/antlr/Jql.g:725:28: ( arglist )?
			int alt19=2;
			int LA19_0 = input.LA(1);
			if ( (LA19_0==NEGNUMBER||LA19_0==POSNUMBER||LA19_0==QUOTE_STRING||(LA19_0 >= SQUOTE_STRING && LA19_0 <= STRING)) ) {
				alt19=1;
			}
			switch (alt19) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:725:28: arglist
					{
					pushFollow(FOLLOW_arglist_in_func1361);
					arglist7=arglist();
					state._fsp--;

					}
					break;

			}

			match(input,RPAREN,FOLLOW_RPAREN_in_func1364); 

				    final List<String> args = arglist7 == null ? Collections.<String>emptyList() : arglist7;
			        func = new FunctionOperand(fname, args);
			    
			}

		}
		catch (MismatchedTokenException e) {

				    //We should only get here when the query is trying to match ')'.
			        if (e.expecting == RPAREN)
			        {
			            if (e.token.getType() == EOF || e.token.getType() == COMMA)
			            {
			                reportError(JqlParseErrorMessages.expectedText(e.token, ")"), e);
			            }
			            else
			            {
			                //There is some argument that is not a string or number.
			                reportError(JqlParseErrorMessages.badFunctionArgument(e.token), e);
			            }
			        }
			        else
			        {
			            reportError(JqlParseErrorMessages.genericParseError(e.token), e);
			        }
			    
		}
		catch (RecognitionException e) {

			        reportError(JqlParseErrorMessages.genericParseError(e.token), e);
				
		}

		finally {
			// do for sure before leaving
		}
		return func;
	}
	// $ANTLR end "func"



	// $ANTLR start "funcName"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:759:1: funcName returns [String name] : ( string |num= numberString );
	public final String funcName() throws RecognitionException {
		String name = null;


		ParserRuleReturnScope num =null;
		ParserRuleReturnScope string8 =null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:760:2: ( string |num= numberString )
			int alt20=2;
			int LA20_0 = input.LA(1);
			if ( (LA20_0==QUOTE_STRING||(LA20_0 >= SQUOTE_STRING && LA20_0 <= STRING)) ) {
				alt20=1;
			}
			else if ( (LA20_0==NEGNUMBER||LA20_0==POSNUMBER) ) {
				alt20=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 20, 0, input);
				throw nvae;
			}

			switch (alt20) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:760:4: string
					{
					pushFollow(FOLLOW_string_in_funcName1401);
					string8=string();
					state._fsp--;

					 name = checkFunctionName((string8!=null?(string8.start):null)); 
					}
					break;
				case 2 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:761:4: num= numberString
					{
					pushFollow(FOLLOW_numberString_in_funcName1412);
					num=numberString();
					state._fsp--;

					 name = (num!=null?((JqlParser.numberString_return)num).string:null); 
					}
					break;

			}
		}

		catch (RecognitionException e) 
		{
			throw e;
		}

		finally {
			// do for sure before leaving
		}
		return name;
	}
	// $ANTLR end "funcName"



	// $ANTLR start "funcNameCheck"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:767:1: funcNameCheck returns [String name] : fname= funcName EOF ;
	public final String funcNameCheck() throws RecognitionException {
		String name = null;


		String fname =null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:768:2: (fname= funcName EOF )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:768:4: fname= funcName EOF
			{
			pushFollow(FOLLOW_funcName_in_funcNameCheck1436);
			fname=funcName();
			state._fsp--;

			 name = fname; 
			match(input,EOF,FOLLOW_EOF_in_funcNameCheck1440); 
			}

		}

		catch (RecognitionException e) 
		{
			throw e;
		}

		finally {
			// do for sure before leaving
		}
		return name;
	}
	// $ANTLR end "funcNameCheck"



	// $ANTLR start "arglist"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:774:1: arglist returns [List<String> args] : str= argument ( COMMA str= argument )* ;
	public final List<String> arglist() throws RecognitionException {
		List<String> args = null;


		String str =null;


				args = new ArrayList<String>();
			
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:788:2: (str= argument ( COMMA str= argument )* )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:788:4: str= argument ( COMMA str= argument )*
			{
			pushFollow(FOLLOW_argument_in_arglist1473);
			str=argument();
			state._fsp--;

			 args.add(str); 
			// com/atlassian/jira/jql/parser/antlr/Jql.g:789:2: ( COMMA str= argument )*
			loop21:
			while (true) {
				int alt21=2;
				int LA21_0 = input.LA(1);
				if ( (LA21_0==COMMA) ) {
					alt21=1;
				}

				switch (alt21) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:789:3: COMMA str= argument
					{
					match(input,COMMA,FOLLOW_COMMA_in_arglist1479); 
					pushFollow(FOLLOW_argument_in_arglist1485);
					str=argument();
					state._fsp--;

					 args.add(str); 
					}
					break;

				default :
					break loop21;
				}
			}

			}


				    //ANTLR will exit the arg list after the first token that is NOT a comma. If we exit and we are not
				    //at the end of the argument list, then we have an error.

				    final Token currentToken = input.LT(1);
				    if (currentToken.getType() != EOF && currentToken.getType() != RPAREN)
			        {
			            reportError(JqlParseErrorMessages.expectedText(currentToken, ")", ","), null);
			        }
				
		}

		catch (RecognitionException e) 
		{
			throw e;
		}

		finally {
			// do for sure before leaving
		}
		return args;
	}
	// $ANTLR end "arglist"



	// $ANTLR start "propertyArgument"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:792:1: propertyArgument returns [String arg] : a= argument ;
	public final String propertyArgument() throws RecognitionException {
		String arg = null;


		String a =null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:793:2: (a= argument )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:793:4: a= argument
			{
			pushFollow(FOLLOW_argument_in_propertyArgument1509);
			a=argument();
			state._fsp--;


				    //property argument should at least contain one dot and property reference
				        if (a.length()<2 || a.charAt(0) != '.' || a.charAt(1) == '.')
			            {

			                Token currentToken = input.LT(-1);
			                if(input.get(input.LT(-1).getTokenIndex()-1).getType() == MATCHWS )
			                {
			                    reportError(JqlParseErrorMessages.badOperator(currentToken), null);
			                }
			                else
			                {
			                    reportError(JqlParseErrorMessages.badPropertyArgument(currentToken), null);
			                }
			            }
			            //remove leading dot as this is unnecessary
			            arg = a.substring(1);

			    
			}

		}

		catch (RecognitionException e) 
		{
			throw e;
		}

		finally {
			// do for sure before leaving
		}
		return arg;
	}
	// $ANTLR end "propertyArgument"



	// $ANTLR start "argument"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:817:1: argument returns [String arg] : (str= string |number= numberString );
	public final String argument() throws RecognitionException {
		String arg = null;


		ParserRuleReturnScope str =null;
		ParserRuleReturnScope number =null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:818:2: (str= string |number= numberString )
			int alt22=2;
			int LA22_0 = input.LA(1);
			if ( (LA22_0==QUOTE_STRING||(LA22_0 >= SQUOTE_STRING && LA22_0 <= STRING)) ) {
				alt22=1;
			}
			else if ( (LA22_0==NEGNUMBER||LA22_0==POSNUMBER) ) {
				alt22=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 22, 0, input);
				throw nvae;
			}

			switch (alt22) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:818:4: str= string
					{
					pushFollow(FOLLOW_string_in_argument1531);
					str=string();
					state._fsp--;

					 arg = (str!=null?((JqlParser.string_return)str).string:null); 
					}
					break;
				case 2 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:819:4: number= numberString
					{
					pushFollow(FOLLOW_numberString_in_argument1543);
					number=numberString();
					state._fsp--;

					 arg = (number!=null?((JqlParser.numberString_return)number).string:null); 
					}
					break;

			}
		}
		catch (RecognitionException e) {

				    switch (e.token.getType())
				    {
				        case COMMA:
				        case RPAREN:
				            reportError(JqlParseErrorMessages.emptyFunctionArgument(e.token), e);
			                break;
			            case RBRACKET:
			                reportError(JqlParseErrorMessages.badPropertyArgument(e.token), e);
			                break;
			            default:
			                reportError(JqlParseErrorMessages.badFunctionArgument(e.token), e);
			                break;
				    }
				
		}

		finally {
			// do for sure before leaving
		}
		return arg;
	}
	// $ANTLR end "argument"



	// $ANTLR start "argumentCheck"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:841:1: argumentCheck returns [String arg] : a= argument EOF ;
	public final String argumentCheck() throws RecognitionException {
		String arg = null;


		String a =null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:842:2: (a= argument EOF )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:842:4: a= argument EOF
			{
			pushFollow(FOLLOW_argument_in_argumentCheck1574);
			a=argument();
			state._fsp--;

			 arg = a; 
			match(input,EOF,FOLLOW_EOF_in_argumentCheck1578); 
			}

		}

		catch (RecognitionException e) 
		{
			throw e;
		}

		finally {
			// do for sure before leaving
		}
		return arg;
	}
	// $ANTLR end "argumentCheck"



	// $ANTLR start "orderBy"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:848:1: orderBy returns [OrderBy order] : ORDER BY f= searchSort ( COMMA f= searchSort )* ;
	public final OrderBy orderBy() throws RecognitionException {
		OrderBy order = null;


		SearchSort f =null;


				final List <SearchSort> args = new ArrayList<SearchSort>();
			
		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:852:2: ( ORDER BY f= searchSort ( COMMA f= searchSort )* )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:852:4: ORDER BY f= searchSort ( COMMA f= searchSort )*
			{
			match(input,ORDER,FOLLOW_ORDER_in_orderBy1602); 
			match(input,BY,FOLLOW_BY_in_orderBy1604); 
			pushFollow(FOLLOW_searchSort_in_orderBy1610);
			f=searchSort();
			state._fsp--;

			 args.add(f); 
			// com/atlassian/jira/jql/parser/antlr/Jql.g:853:3: ( COMMA f= searchSort )*
			loop23:
			while (true) {
				int alt23=2;
				int LA23_0 = input.LA(1);
				if ( (LA23_0==COMMA) ) {
					alt23=1;
				}

				switch (alt23) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:853:4: COMMA f= searchSort
					{
					match(input,COMMA,FOLLOW_COMMA_in_orderBy1617); 
					pushFollow(FOLLOW_searchSort_in_orderBy1623);
					f=searchSort();
					state._fsp--;

					 args.add(f); 
					}
					break;

				default :
					break loop23;
				}
			}

			 order = new OrderByImpl(args); 
			}

		}
		catch (MismatchedTokenException e) {

			        if (e.expecting == BY)
			        {
			            reportError(JqlParseErrorMessages.expectedText(e.token, "by"), e);
			        }
			        else if (e.expecting == ORDER)
			        {
			            //This should not happen since the lookahead ensures that ORDER has been matched.
			            reportError(JqlParseErrorMessages.expectedText(e.token, "order"), e);
			        }
			        else
			        {
			            reportError(JqlParseErrorMessages.genericParseError(e.token), e);
			        }
				
		}
		catch (RecognitionException e) {

				    reportError(JqlParseErrorMessages.genericParseError(e.token), e);
				
		}

		finally {
			// do for sure before leaving
		}
		return order;
	}
	// $ANTLR end "orderBy"



	// $ANTLR start "searchSort"
	// com/atlassian/jira/jql/parser/antlr/Jql.g:879:1: searchSort returns [SearchSort sort] : f= field (o= DESC |o= ASC )? ;
	public final SearchSort searchSort() throws RecognitionException {
		SearchSort sort = null;


		Token o=null;
		ParserRuleReturnScope f =null;

		try {
			// com/atlassian/jira/jql/parser/antlr/Jql.g:880:2: (f= field (o= DESC |o= ASC )? )
			// com/atlassian/jira/jql/parser/antlr/Jql.g:880:4: f= field (o= DESC |o= ASC )?
			{
			pushFollow(FOLLOW_field_in_searchSort1666);
			f=field();
			state._fsp--;

			// com/atlassian/jira/jql/parser/antlr/Jql.g:880:14: (o= DESC |o= ASC )?
			int alt24=3;
			int LA24_0 = input.LA(1);
			if ( (LA24_0==DESC) ) {
				alt24=1;
			}
			else if ( (LA24_0==ASC) ) {
				alt24=2;
			}
			switch (alt24) {
				case 1 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:880:15: o= DESC
					{
					o=(Token)match(input,DESC,FOLLOW_DESC_in_searchSort1673); 
					}
					break;
				case 2 :
					// com/atlassian/jira/jql/parser/antlr/Jql.g:880:26: o= ASC
					{
					o=(Token)match(input,ASC,FOLLOW_ASC_in_searchSort1681); 
					}
					break;

			}


				    if (o == null)
				    {
				        Token token = input.LT(1);
				        //ANTLR is not very strict here. If ANTLR sees an illegal sort order, then it will simply leave
				        //the order by clause. We want to be a little stricter and find illegal SORT ORDERS.
				        if (token.getType() != EOF && token.getType() != COMMA)
				        {
				            reportError(JqlParseErrorMessages.badSortOrder(token), null);
				        }
				    }

				    SortOrder order = (o == null) ? null : SortOrder.parseString((o!=null?o.getText():null));
				    if (f != null && (f!=null?((JqlParser.field_return)f).field:null).isEntityProperty())
				    {
				        sort = new SearchSort((f!=null?((JqlParser.field_return)f).field:null).getName(), Option.some((f!=null?((JqlParser.field_return)f).field:null).getProperty()), order);
				    }
				    else
				    {
					    sort = new SearchSort((f!=null?input.toString(f.start,f.stop):null), order);
					}
				
			}

		}
		catch (RecognitionException e) {

				    if (f == null)
				    {
			            //We could not find a field.
			            reportError(JqlParseErrorMessages.badFieldName(e.token), e);
				    }
				    else
				    {
				        //We could not find a correct order.
				        reportError(JqlParseErrorMessages.badSortOrder(e.token), e);
				    }
				
		}

		finally {
			// do for sure before leaving
		}
		return sort;
	}
	// $ANTLR end "searchSort"

	// Delegated rules



	public static final BitSet FOLLOW_clause_in_query72 = new BitSet(new long[]{0x0000800000000000L});
	public static final BitSet FOLLOW_orderBy_in_query82 = new BitSet(new long[]{0x0000000000000000L});
	public static final BitSet FOLLOW_EOF_in_query86 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_orClause_in_clause122 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_andClause_in_orClause153 = new BitSet(new long[]{0x0000400000000002L});
	public static final BitSet FOLLOW_OR_in_orClause158 = new BitSet(new long[]{0x0C14048400020200L});
	public static final BitSet FOLLOW_andClause_in_orClause164 = new BitSet(new long[]{0x0000400000000002L});
	public static final BitSet FOLLOW_notClause_in_andClause197 = new BitSet(new long[]{0x0000000000000082L});
	public static final BitSet FOLLOW_AND_in_andClause202 = new BitSet(new long[]{0x0C14048400020200L});
	public static final BitSet FOLLOW_notClause_in_andClause208 = new BitSet(new long[]{0x0000000000000082L});
	public static final BitSet FOLLOW_set_in_notClause231 = new BitSet(new long[]{0x0C14048400020200L});
	public static final BitSet FOLLOW_notClause_in_notClause243 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_subClause_in_notClause250 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_terminalClause_in_notClause278 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAREN_in_subClause310 = new BitSet(new long[]{0x0C14048400020200L});
	public static final BitSet FOLLOW_orClause_in_subClause312 = new BitSet(new long[]{0x0080000000000000L});
	public static final BitSet FOLLOW_RPAREN_in_subClause314 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_field_in_terminalClause355 = new BitSet(new long[]{0x40001C1AD8402000L});
	public static final BitSet FOLLOW_operator_in_terminalClause361 = new BitSet(new long[]{0x2C14208404301412L});
	public static final BitSet FOLLOW_operand_in_terminalClause371 = new BitSet(new long[]{0x2000200004101412L});
	public static final BitSet FOLLOW_historyPredicate_in_terminalClause376 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_historyPredicate_in_terminalClause384 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_terminalHistoryPredicate_in_historyPredicate424 = new BitSet(new long[]{0x2000200004101412L});
	public static final BitSet FOLLOW_historyPredicateOperator_in_terminalHistoryPredicate448 = new BitSet(new long[]{0x0C14008400200000L});
	public static final BitSet FOLLOW_operand_in_terminalHistoryPredicate450 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FROM_in_historyPredicateOperator469 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TO_in_historyPredicateOperator476 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BY_in_historyPredicateOperator483 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BEFORE_in_historyPredicateOperator490 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_AFTER_in_historyPredicateOperator497 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ON_in_historyPredicateOperator504 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DURING_in_historyPredicateOperator512 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_EQUALS_in_operator533 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOT_EQUALS_in_operator540 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LIKE_in_operator547 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOT_LIKE_in_operator554 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LT_in_operator562 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_GT_in_operator569 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LTEQ_in_operator576 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_GTEQ_in_operator583 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IN_in_operator590 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IS_in_operator597 = new BitSet(new long[]{0x0000040000000000L});
	public static final BitSet FOLLOW_NOT_in_operator599 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IS_in_operator606 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOT_in_operator613 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_IN_in_operator615 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WAS_in_operator622 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WAS_in_operator629 = new BitSet(new long[]{0x0000040000000000L});
	public static final BitSet FOLLOW_NOT_in_operator631 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WAS_in_operator643 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_IN_in_operator645 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WAS_in_operator657 = new BitSet(new long[]{0x0000040000000000L});
	public static final BitSet FOLLOW_NOT_in_operator659 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_IN_in_operator661 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_CHANGED_in_operator673 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_numberString_in_field739 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_string_in_field781 = new BitSet(new long[]{0x0000000100000002L});
	public static final BitSet FOLLOW_customField_in_field803 = new BitSet(new long[]{0x0000000100000002L});
	public static final BitSet FOLLOW_LBRACKET_in_field851 = new BitSet(new long[]{0x0C14008000000000L});
	public static final BitSet FOLLOW_argument_in_field889 = new BitSet(new long[]{0x0020000000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_field921 = new BitSet(new long[]{0x0C14008100000002L});
	public static final BitSet FOLLOW_propertyArgument_in_field962 = new BitSet(new long[]{0x0C14008100000002L});
	public static final BitSet FOLLOW_CUSTOMFIELD_in_customField1032 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_LBRACKET_in_customField1034 = new BitSet(new long[]{0x0004000000000000L});
	public static final BitSet FOLLOW_POSNUMBER_in_customField1040 = new BitSet(new long[]{0x0020000000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_customField1042 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_field_in_fieldCheck1082 = new BitSet(new long[]{0x0000000000000000L});
	public static final BitSet FOLLOW_EOF_in_fieldCheck1086 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_EMPTY_in_operand1103 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_string_in_operand1114 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_numberString_in_operand1125 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_func_in_operand1136 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_list_in_operand1147 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_STRING_in_string1187 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_QUOTE_STRING_in_string1198 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SQUOTE_STRING_in_string1209 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_set_in_numberString1230 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_string_in_stringValueCheck1259 = new BitSet(new long[]{0x0000000000000000L});
	public static final BitSet FOLLOW_EOF_in_stringValueCheck1263 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAREN_in_list1293 = new BitSet(new long[]{0x0C14008400200000L});
	public static final BitSet FOLLOW_operand_in_list1299 = new BitSet(new long[]{0x0080000000004000L});
	public static final BitSet FOLLOW_COMMA_in_list1307 = new BitSet(new long[]{0x0C14008400200000L});
	public static final BitSet FOLLOW_operand_in_list1313 = new BitSet(new long[]{0x0080000000004000L});
	public static final BitSet FOLLOW_RPAREN_in_list1319 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_funcName_in_func1357 = new BitSet(new long[]{0x0000000400000000L});
	public static final BitSet FOLLOW_LPAREN_in_func1359 = new BitSet(new long[]{0x0C94008000000000L});
	public static final BitSet FOLLOW_arglist_in_func1361 = new BitSet(new long[]{0x0080000000000000L});
	public static final BitSet FOLLOW_RPAREN_in_func1364 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_string_in_funcName1401 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_numberString_in_funcName1412 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_funcName_in_funcNameCheck1436 = new BitSet(new long[]{0x0000000000000000L});
	public static final BitSet FOLLOW_EOF_in_funcNameCheck1440 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_argument_in_arglist1473 = new BitSet(new long[]{0x0000000000004002L});
	public static final BitSet FOLLOW_COMMA_in_arglist1479 = new BitSet(new long[]{0x0C14008000000000L});
	public static final BitSet FOLLOW_argument_in_arglist1485 = new BitSet(new long[]{0x0000000000004002L});
	public static final BitSet FOLLOW_argument_in_propertyArgument1509 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_string_in_argument1531 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_numberString_in_argument1543 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_argument_in_argumentCheck1574 = new BitSet(new long[]{0x0000000000000000L});
	public static final BitSet FOLLOW_EOF_in_argumentCheck1578 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ORDER_in_orderBy1602 = new BitSet(new long[]{0x0000000000001000L});
	public static final BitSet FOLLOW_BY_in_orderBy1604 = new BitSet(new long[]{0x0C14008000020000L});
	public static final BitSet FOLLOW_searchSort_in_orderBy1610 = new BitSet(new long[]{0x0000000000004002L});
	public static final BitSet FOLLOW_COMMA_in_orderBy1617 = new BitSet(new long[]{0x0C14008000020000L});
	public static final BitSet FOLLOW_searchSort_in_orderBy1623 = new BitSet(new long[]{0x0000000000004002L});
	public static final BitSet FOLLOW_field_in_searchSort1666 = new BitSet(new long[]{0x0000000000040102L});
	public static final BitSet FOLLOW_DESC_in_searchSort1673 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ASC_in_searchSort1681 = new BitSet(new long[]{0x0000000000000002L});
}
