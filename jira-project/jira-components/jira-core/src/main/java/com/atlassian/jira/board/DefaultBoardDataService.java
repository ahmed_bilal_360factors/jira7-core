package com.atlassian.jira.board;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.board.model.BoardColumn;
import com.atlassian.jira.board.model.BoardData;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.action.issue.IssueSearchLimits;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.query.Query;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static com.atlassian.jira.bc.ServiceOutcomeImpl.error;
import static com.atlassian.jira.bc.ServiceOutcomeImpl.from;
import static com.atlassian.jira.bc.ServiceOutcomeImpl.ok;
import static com.atlassian.jira.util.ErrorCollection.Reason.PRECONDITION_FAILED;
import static com.atlassian.jira.util.ErrorCollection.Reason.SERVER_ERROR;

public class DefaultBoardDataService implements BoardDataService {
    static final String SEARCH_ERROR = "core.board.data.service.search.error";
    static final String INVALID_BOARD_ERROR = "core.board.data.service.board.invalid";

    private final BoardWorkflowService boardWorkflowService;
    private final BoardQueryService boardQueryService;
    private final I18nHelper.BeanFactory i18nFactory;
    private final IssueSearchLimits issueSearchLimits;
    private final SearchService searchService;

    public DefaultBoardDataService(
            BoardWorkflowService boardWorkflowService,
            BoardQueryService boardQueryService,
            I18nHelper.BeanFactory i18nFactory,
            IssueSearchLimits issueSearchLimits,
            SearchService searchService
    ) {
        this.boardWorkflowService = boardWorkflowService;
        this.boardQueryService = boardQueryService;
        this.i18nFactory = i18nFactory;
        this.issueSearchLimits = issueSearchLimits;
        this.searchService = searchService;
    }

    @Override
    public ServiceOutcome<BoardData> getDataForBoard(ApplicationUser user, Board board) {
        ServiceOutcome<Query> queryOutcome = boardQueryService.getBaseQueryForBoard(user, board);

        if (queryOutcome.isValid()) {
            Query baseQuery = queryOutcome.get();
            return boardDataOrError(board, user, baseQuery, boardQueryService.getAugmentedQueryForDoneIssues(baseQuery));
        }
        return from(queryOutcome.getErrorCollection());
    }

    private ServiceOutcome<BoardData> boardDataOrError(Board board, ApplicationUser user, Query baseQuery, Query augmentedQuery) {
        try {
            if (!shouldShowBoard(user, augmentedQuery)) {
                return error(translatedTextForUser(INVALID_BOARD_ERROR, user), PRECONDITION_FAILED);
            }

            return ok(getBoardDataGivenQuery(board, user, baseQuery, augmentedQuery));
        } catch (SearchException searchException) {
            return error(translatedTextForUser(SEARCH_ERROR, user), SERVER_ERROR);
        }
    }

    private boolean shouldShowBoard(ApplicationUser user, Query query) {
        Collection<JiraWorkflow> jiraWorkflows = boardWorkflowService.getWorkflowsForQuery(user, query);

        if (jiraWorkflows.size() != 1) {
            return false;
        }

        JiraWorkflow workflow = jiraWorkflows.iterator().next();

        Collection<ActionDescriptor> actionDescriptors = workflow.getAllActions();
        for (ActionDescriptor actionDescriptor : actionDescriptors) {
            Collection<StepDescriptor> stepsForTransition = workflow.getStepsForTransition(actionDescriptor);
            for (StepDescriptor stepDescriptor : stepsForTransition) {
                if (hasMultipleTransitionsBetweenSameStates(stepDescriptor)) {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean hasMultipleTransitionsBetweenSameStates(StepDescriptor stepDescriptor) {
        List<ActionDescriptor> actions = stepDescriptor.getActions();

        List<Integer> transitions = actions
                .stream()
                .map(action -> action.getUnconditionalResult().getStep())
                .collect(toImmutableList());

        return transitions.stream().distinct().count() != transitions.size();
    }

    private BoardData getBoardDataGivenQuery(Board board, ApplicationUser user, Query baseQuery, Query augmentedQuery) throws SearchException {
        SearchResults issuesToBeDisplayed = doSearchGivenQuery(user, augmentedQuery);
        List<Issue> issues = getIssuesFromSearchResults(issuesToBeDisplayed);
        List<ApplicationUser> assignees = getAssigneesFromSearchResults(issuesToBeDisplayed);
        List<BoardColumn> columns = getColumns(user, augmentedQuery);

        long allIssuesInBoard = searchService.searchCount(user, baseQuery);

        return new BoardData.Builder()
                .board(board)
                .jql(board.getJql())
                .id(board.getId())
                .issues(issues)
                .people(assignees)
                .columns(columns)
                .maxResults(issues.size())
                .total(issuesToBeDisplayed.getTotal())
                .hasFilteredDoneIssues(issuesToBeDisplayed.getTotal() < allIssuesInBoard)
                .build();
    }

    private SearchResults doSearchGivenQuery(ApplicationUser user, Query query) throws SearchException {
        return searchService.search(user, query, new PagerFilter(issueSearchLimits.getMaxResults()));
    }

    private List<Issue> getIssuesFromSearchResults(SearchResults results) {
        return results.getIssues();
    }

    private List<ApplicationUser> getAssigneesFromSearchResults(SearchResults results) {
        return getIssuesFromSearchResults(results)
                .stream()
                .map(Issue::getAssignee)
                .distinct()
                .filter(Objects::nonNull)
                .collect(toImmutableList());
    }

    private List<Status> getStatusesForQuery(ApplicationUser user, Query query) {
        return boardWorkflowService.getAccessibleStatuses(user, query)
                .stream()
                .collect(toImmutableList());
    }

    private List<BoardColumn> getColumns(ApplicationUser user, Query query) {
        Set<Status> initialStatuses = boardWorkflowService.getInitialStatusesForQuery(user, query);

        List<Status> allStatusesSorted = Ordering.from(byInitialStatus(initialStatuses))
                .compound(byStatusCategory())
                .compound(byStatusId())
                .immutableSortedCopy(getStatusesForQuery(user, query));

        return allStatusesSorted
                .stream()
                .map(status -> new BoardColumn(status, ImmutableList.of(status)))
                .collect(toImmutableList());
    }

    private Comparator<Status> byInitialStatus(Set<Status> initialStatuses) {
        return (thisStatus, otherStatus) -> {
            boolean bothInitial = initialStatuses.contains(thisStatus) && initialStatuses.contains(otherStatus);
            boolean neitherInitial = !initialStatuses.contains(thisStatus) && !initialStatuses.contains(otherStatus);

            if (bothInitial || neitherInitial) {
                return 0;
            } else if (initialStatuses.contains(thisStatus)) {
                return -1;
            }
            return 1;
        };
    }

    private Comparator<Status> byStatusCategory() {
        return (thisStatus, otherStatus) -> thisStatus.getStatusCategory().compareTo(otherStatus.getStatusCategory());
    }

    private Comparator<Status> byStatusId() {
        return (thisStatus, otherStatus) -> Long.valueOf(thisStatus.getId()).compareTo(Long.valueOf(otherStatus.getId
                ()));
    }

    private String translatedTextForUser(String i18nKey, ApplicationUser user) {
        return i18nFactory.getInstance(user).getText(i18nKey);
    }
}
