package com.atlassian.jira.web.servlet;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.plugin.icon.IconTypeDefinitionFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.NoSuchElementException;

public class ViewUniversalAvatarServletImpl {
    public static final String AVATAR_ID_PARAM = "avatarId";
    public static final String AVATAR_TYPE_PARAM = "avatarType";
    public static final String AVATAR_SIZE_PARAM = "size";

    private final JiraAuthenticationContext authenticationContext;
    private final AvatarToStream avatarToStream;
    private final IconTypeDefinitionFactory iconTypeFactory;
    private final AvatarManager avatarManager;

    public ViewUniversalAvatarServletImpl(
            final JiraAuthenticationContext authenticationContext,
            final AvatarToStream avatarToStream,
            final IconTypeDefinitionFactory iconTypeFactory,
            final AvatarManager avatarManager) {
        this.authenticationContext = authenticationContext;
        this.avatarToStream = avatarToStream;
        this.iconTypeFactory = iconTypeFactory;
        this.avatarManager = avatarManager;
    }

    public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        try {
            final Avatar avatar = getAvatar(request);
            final Avatar.Size size = getValidAvatarSize(request);

            avatarToStream.sendAvatar(avatar, size, request, response);
        } catch (IllegalArgumentException iae) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, iae.getMessage());
        }
    }

    /**
     * @throws java.lang.IllegalArgumentException
     */
    @Nonnull
    private Avatar getAvatar(final HttpServletRequest request) {
        final ApplicationUser user = authenticationContext.getUser();

        final long avatarId = getValidAvatarId(request);
        final IconType iconType = getValidAvatarType(request);

        Avatar avatar = avatarManager.getById(avatarId);
        if (avatar != null) {
            if (!avatar.getIconType().equals(iconType)) {
                throw new IllegalArgumentException("No icon with ID=" + avatarId + " for type=" + iconType);
            }
            boolean userCanViewAvatar = avatarManager.userCanView(user, avatar);
            if (!userCanViewAvatar) {
                avatar = null;
            }
        }

        if (null == avatar) {
            avatar = avatarManager.getDefaultAvatar(iconType);
        }

        return avatar;
    }

    private long getValidAvatarId(final HttpServletRequest request) {
        String avatarIdSpec = request.getParameter(AVATAR_ID_PARAM);
        if (null == avatarIdSpec) {
            throw new IllegalArgumentException(AVATAR_ID_PARAM);
        }

        try {
            return Long.valueOf(avatarIdSpec);
        } catch (NumberFormatException x) {
            throw new IllegalArgumentException(AVATAR_ID_PARAM, x);
        }
    }

    private IconType getValidAvatarType(final HttpServletRequest request) {
        String avatarTypeSpec = request.getParameter(AVATAR_TYPE_PARAM);
        final IconType iconType = IconType.of(avatarTypeSpec);
        if (null == iconType) {
            throw new IllegalArgumentException(AVATAR_TYPE_PARAM);
        }

        return iconType;
    }

    private Avatar.Size getValidAvatarSize(final HttpServletRequest request) {
        String avatarSizeSpec = request.getParameter(AVATAR_SIZE_PARAM);
        if (null == avatarSizeSpec) {
            return Avatar.Size.defaultSize();
        }

        try {
            return Avatar.Size.getSizeFromParam(avatarSizeSpec);
        } catch (NoSuchElementException x) {
            throw new IllegalArgumentException(AVATAR_SIZE_PARAM, x);
        }
    }
}
