package com.atlassian.jira.index.ha;

import com.atlassian.jira.entity.WithId;
import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.index.IssueIndexingParams;
import com.atlassian.jira.issue.util.IssuesIterable;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.sharing.SharedEntity;

import java.util.Collection;
import java.util.Set;

/**
 * Writes ReplicatedIndexOperations to the database - as this class is called from the finally in the indexManagers
 * you must not throw any exceptions.  Log them and return
 *
 * @since v6.1
 */
public interface ReplicatedIndexManager {
    void reindexIssues(IssuesIterable issuesIterable, IssueIndexingParams indexingParams);

    void reindexComments(Collection<Comment> comments);

    void reindexWorklogs(Collection<Worklog> worklogs);

    <T extends WithId> void reindexEntity(final Collection<T> entities, AffectedIndex index);

    void deIndexIssues(Set<Issue> issuesToDelete);

    void indexSharedEntity(SharedEntity entity);

    void deIndexSharedEntity(SharedEntity entity);

    void reindexProject(Project project);
}
