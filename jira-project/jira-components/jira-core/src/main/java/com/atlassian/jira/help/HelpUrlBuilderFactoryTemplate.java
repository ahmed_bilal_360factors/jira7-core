package com.atlassian.jira.help;

import com.atlassian.fugue.Option;
import com.atlassian.jira.util.BuildUtilsInfo;

import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.trimToNull;

/**
 * A partial implementation of the {@link HelpUrlBuilder.Factory} that substitutes variables into the prefix
 * argument of the {@link #get(String, String)}. Implementers must extend this class and provide an implementation
 * to the abstract {@link #newUrlBuilder(String, String)}.
 *
 * @since v6.2.4
 */
abstract class HelpUrlBuilderFactoryTemplate implements HelpUrlBuilder.Factory {
    /**
     * Matches either {@code ${docs.version}} or {@code ${doc.version}}.
     */
    private static final Pattern DOC_VERSION_PATTERN = Pattern.compile("\\$\\{docs?\\.version\\}");

    /**
     * Matches  {@code ${application.help.space}}.
     */
    private static final Pattern APPLICATION_HELP_SPACE_PATTERN = Pattern.compile("\\$\\{application\\.help\\.space\\}");

    private final String docsVersion;
    private final Option<String> applicationHelpSpaceVersion;

    /**
     * Constructs this factory with specified replacements for <tt>url-prefix</tt> property.
     *
     * @param docsVersion                 will be used to replace {@code ${docs.version}} or {@code ${doc.version}} placeholder
     * @param applicationHelpSpaceVersion will be used to replace {@code ${application.help.space}} placeholder. If set
     *                                    to empty the replacement will not occur.
     */
    HelpUrlBuilderFactoryTemplate(String docsVersion, final Option<String> applicationHelpSpaceVersion) {
        this.docsVersion = docsVersion;
        this.applicationHelpSpaceVersion = applicationHelpSpaceVersion;
    }

    @Override
    public final HelpUrlBuilder get(final String prefix, final String suffix) {
        return newUrlBuilder(substitute(trimToNull(prefix)), trimToNull(suffix));
    }

    /**
     * Substitutes all variables in the passed string for their runtime value. The variables are:
     * <dl>
     * <dt>docs.version</dt>
     * <dd>The current JIRA documentation version. See {@link BuildUtilsInfo#getDocVersion()}</dd>
     * <dt>doc.version</dt>
     * <dd>The current JIRA documentation version. An alias for {@code docs.version}.</dd>
     * <dt>application.help.space</dt>
     * <dd>Help space for application as passed in to factory constructor. If set to {@code Option.none()} this substitution will not take place.</dd>
     * </dl>
     *
     * @param inputString the string to substitute.
     * @return a string with all variables replaced with their runtime values.
     */
    private String substitute(String inputString) {
        if (inputString == null) {
            return null;
        } else {
            final Option<String> stringWithHelpSpace =
                    applicationHelpSpaceVersion.map(space -> APPLICATION_HELP_SPACE_PATTERN.matcher(inputString).replaceAll(space));
            return DOC_VERSION_PATTERN.matcher(stringWithHelpSpace.getOrElse(inputString)).replaceAll(docsVersion);
        }
    }

    /**
     * Template method called to create a {@link com.atlassian.jira.help.HelpUrlBuilder}. The passed {@code prefix}
     * and {@code suffix} have been processed to their actual values.
     *
     * @param prefix the prefix to use in the returned {@code HelpUrlBuilder}.
     * @param suffix the suffix to use in the returned {@code HelpUrlBuilder}.
     * @return a new {@code HelpUrlBuilder} that uses the passed prefix and suffix to generate URLs.
     */
    abstract HelpUrlBuilder newUrlBuilder(String prefix, String suffix);
}
