package com.atlassian.jira.license;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.I18nHelper;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.isEmpty;


/**
 * Implements a license check that compares the <em>earliest</em> maintenance date of all current licenses to the
 * current JIRA instance build date. Enterprise License Agreements (ELAs) are special-cased to always pass this license
 * check provided that all current licenses are ELAs.
 *
 * @since 7.0
 */
public final class BuildVersionLicenseCheckImpl implements BuildVersionLicenseCheck {
    private final JiraLicenseManager licenseManager;
    private final BuildUtilsInfo buildUtilsInfo;
    private final I18nHelper i18n;
    private final LicenseMaintenancePredicate isMaintenanceValid;
    private final DateTimeFormatter dateTimeFormatter;

    public BuildVersionLicenseCheckImpl(@Nonnull JiraLicenseManager licenseManager,
                                        @Nonnull BuildUtilsInfo buildUtilsInfo,
                                        @Nonnull I18nHelper i18n,
                                        @Nonnull DateTimeFormatter dateTimeFormatter,
                                        @Nonnull LicenseMaintenancePredicate isMaintenanceValid) {
        this.i18n = notNull("i18n", i18n);
        this.buildUtilsInfo = notNull("buildUtilsInfo", buildUtilsInfo);
        this.dateTimeFormatter = notNull("dateTimeFormatter", dateTimeFormatter).withSystemZone();
        this.isMaintenanceValid = notNull("isMaintenanceValid", isMaintenanceValid);
        this.licenseManager = notNull("licenseManager", licenseManager);
    }

    @Override
    public Result evaluate() {
        return evaluate(true);
    }

    @Override
    public Result evaluateWithoutGracePeriod() {
        return evaluate(false);
    }

    private Result evaluate(boolean considerGracePeriodStatus) {
        Iterable<LicenseDetails> licenses = licenseManager.getLicenses();

        // ensure there's at least 1 license
        if (isEmpty(licenses)) {
            return FAIL_NO_LICENSES;
        }

        // fail for any V1 license (as these are ancient)
        Iterable<LicenseDetails> ancientLicenses = filter(licenses, license -> license.getLicenseVersion() == 1);
        if (!isEmpty(ancientLicenses)) {
            return new Failure(copyOf(ancientLicenses),
                    i18n.getText("setup.error.invalidlicensekey.v1.license.version"));
        }

        // if any license is too old, user will be offered a chance to confirm their license is too old and to go into
        // a grace (evaluation) period. if user has already confirmed, then they pass this build version check, and
        // other mechanisms that check for license expiry will apply, see LicenseDetails#isExpired.
        if (considerGracePeriodStatus && licenseManager.hasLicenseTooOldForBuildConfirmationBeenDone()) {
            return PASS;
        }

        // ELAs are subscription based; licensees are always permitted to upgrade during their subscription period,
        // provided all licenses are ELAs.
        if (allLicensesAreELAs(licenses)) {
            return PASS;
        }

        // maintenance date check; all licenses must be within maintenance period to pass.
        List<LicenseDetails> licensesPastMaintenance = filterLicensesPastMaintenance(licenses);
        if (licensesPastMaintenance.isEmpty()) {
            return PASS;
        }

        // otherwise at least 1 failing license
        return new Failure(licensesPastMaintenance, getMaintenanceDateFailureMessage(licensesPastMaintenance));
    }

    private List<LicenseDetails> filterLicensesPastMaintenance(Iterable<LicenseDetails> licenses) {
        return StreamSupport.stream(licenses.spliterator(), false)
                .filter(isMaintenanceValid.negate())
                .collect(CollectorsUtil.toImmutableList());
    }

    private boolean allLicensesAreELAs(Iterable<LicenseDetails> licenses) {
        for (LicenseDetails license : licenses) {
            if (!license.isEnterpriseLicenseAgreement()) {
                return false;
            }
        }
        return true;
    }

    private String getMaintenanceDateFailureMessage(List<LicenseDetails> licensesPastMaintenance) {
        String expiredAppDescription = licensesPastMaintenance.stream()
                .map(LicenseDetails::getApplicationDescription)
                .collect(Collectors.joining(", "));

        // we know at this point that all of the passed licenses have finite (non-null) expiry dates
        Date expiryDateOfInstance = licensesPastMaintenance.stream()
                .map(LicenseDetails::getMaintenanceExpiryDate)
                .min(Date::compareTo).get();

        return i18n.getText("admin.license.build.update.not.possible",
                buildUtilsInfo.getVersion(), dateTimeFormatter.format(expiryDateOfInstance), expiredAppDescription);
    }
}
