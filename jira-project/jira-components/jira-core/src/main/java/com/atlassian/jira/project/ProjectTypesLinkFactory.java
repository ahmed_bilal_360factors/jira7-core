package com.atlassian.jira.project;

import com.atlassian.jira.project.type.BrowseProjectTypeManager;
import com.atlassian.jira.project.type.ProjectType;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugin.web.api.model.WebFragmentBuilder;
import com.atlassian.plugin.web.api.provider.WebItemProvider;

import java.util.List;
import java.util.Map;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static com.atlassian.jira.project.type.ProjectTypeKeyFormatter.format;

/**
 * A factory produces links of project types in the projects dropdown.
 *
 * @since v7.0
 */
public class ProjectTypesLinkFactory implements WebItemProvider {
    private final VelocityRequestContextFactory velocityRequestContextFactory;
    private final BrowseProjectTypeManager browseProjectTypeManager;
    private final JiraAuthenticationContext authenticationContext;
    static final String USER = "user";
    static final String ID_PREFIX = "project_type_";
    static final String MENU_SECTION = "browse_link/project_types_main";
    static final String ICON_URL = "iconUrl";
    static final String ICON_PREFIX = "data:image/svg+xml;base64,";
    static final String RELATIVE_URL = "/secure/BrowseProjects.jspa";
    public static final String SELECTED_CATEGORY = "selectedCategory";
    public static final String ALL = "all";
    public static final String SELECTED_PROJECT_TYPE = "selectedProjectType";

    public ProjectTypesLinkFactory(VelocityRequestContextFactory velocityRequestContextFactory,
                                   BrowseProjectTypeManager browseProjectTypeManager,
                                   JiraAuthenticationContext authenticationContext) {
        this.velocityRequestContextFactory = velocityRequestContextFactory;
        this.browseProjectTypeManager = browseProjectTypeManager;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public Iterable<WebItem> getItems(final Map<String, Object> context) {
        return convertToWebItems(browseProjectTypeManager.getAllProjectTypes((ApplicationUser) context.get(USER)));
    }

    private List<WebItem> convertToWebItems(final List<ProjectType> projectTypes) {
        return projectTypes.stream().map(
                projectType -> new WebFragmentBuilder(
                        projectType.getWeight())
                        .id(ID_PREFIX + projectType.getKey().getKey())
                        .label(format(projectType.getKey()))
                        .title(authenticationContext.getI18nHelper().getText(projectType.getDescriptionI18nKey()))
                        .addParam(ICON_URL, ICON_PREFIX + projectType.getIcon())
                        .webItem(MENU_SECTION)
                        .url(linkUrl(projectType.getKey().getKey()))
                        .build())
                .collect(toImmutableList());
    }

    private String linkUrl(final String key) {
        VelocityRequestContext requestContext = velocityRequestContextFactory.getJiraVelocityRequestContext();
        UrlBuilder urlBuilder = new UrlBuilder(requestContext.getBaseUrl() + RELATIVE_URL);
        urlBuilder.addParameter(SELECTED_CATEGORY, ALL);
        urlBuilder.addParameter(SELECTED_PROJECT_TYPE, key);
        return urlBuilder.asUrlString();
    }
}