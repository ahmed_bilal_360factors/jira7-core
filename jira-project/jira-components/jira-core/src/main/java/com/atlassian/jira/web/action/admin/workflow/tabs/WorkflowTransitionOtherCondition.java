package com.atlassian.jira.web.action.admin.workflow.tabs;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.opensymphony.workflow.loader.ResultDescriptor;

import java.util.Map;

import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

public class WorkflowTransitionOtherCondition implements Condition {

    @Override
    public void init(final Map<String, String> params) {}

    @SuppressWarnings("RedundantIfStatement")
    @Override
    public boolean shouldDisplay(final Map<String, Object> context) {
        return WorkflowTransitionContextUtils.getTransition(context).map(actionDescriptor -> {
            if (isNotEmpty(actionDescriptor.getConditionalResults())) {
                return true;
            }

            final ResultDescriptor unconditionalResult = actionDescriptor.getUnconditionalResult();
            if (unconditionalResult != null) {
                if (isNotEmpty(unconditionalResult.getValidators())) {
                    return true;
                }

                if (isNotEmpty(unconditionalResult.getPreFunctions())) {
                    return true;
                }
            }

            if (isNotEmpty(actionDescriptor.getPreFunctions())) {
                return true;
            }

            if (isNotEmpty(actionDescriptor.getPostFunctions())) {
                return true;
            }

            return false;
        }).orElse(false);
    }
}
