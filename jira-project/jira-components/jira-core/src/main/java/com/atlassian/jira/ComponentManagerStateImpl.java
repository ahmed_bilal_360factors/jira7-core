package com.atlassian.jira;

import com.atlassian.jira.startup.JiraStartupPluginSystemListener;

import static com.atlassian.jira.ComponentManager.ContainerLevel.FULL_CONTAINER;

/**
 * An implementation of {@link ComponentManager.State} interface
 */
public enum ComponentManagerStateImpl implements ComponentManager.State {
    /**
     * Not registered, plugins haven't started
     */
    NOT_STARTED(0, false, false, false, false, false, "startup.page.state.NOT_STARTED"),

    /**
     * Not registered, plugins not started, but container is initialised
     */
    CONTAINER_INITIALISED(5, true, false, false, false, false, "startup.page.state.CONTAINER_INITIALISED") {
        // For the purpose of the startup page, this status will use the message key and percentage
        // from NOT_STARTED unless we have the *full* Pico container.  Otherwise, the status would
        // jump around as the bootstrap container cycles out to be replaced with the full container.

        @Override
        public String getMessageKey() {
            return isStillInBootstrapContainer() ? NOT_STARTED.getMessageKey() : super.getMessageKey();
        }

        @Override
        public int getPercentage() {
            if (isStillInBootstrapContainer()) {
                return 0;
            }

            final int min = super.getPercentage();
            final int max = PLUGINSYSTEM_STARTED.getPercentage();
            return min + JiraStartupPluginSystemListener.getProgress(max - min);
        }

        private boolean isStillInBootstrapContainer() {
            return !FULL_CONTAINER.equals(ComponentManager.getInstance().getContainerLevel());
        }
    },

    /**
     * Not registered, plugins haven't started
     */
    PLUGINSYSTEM_STARTED(95, true, false, true, false, false, "startup.page.state.PLUGINSYSTEM_STARTED"),

    /**
     * All components registered with PICO including plugin components and plugin system has started.
     */
    COMPONENTS_REGISTERED(97, true, true, true, false, false, "startup.page.state.COMPONENTS_REGISTERED"),
    /**
     * All components have been instantiated.
     *
     * @since 7.2.5
     */
    COMPONENTS_INSTANTIATED(98, true, true, true, true, false, "startup.page.state.COMPONENTS_INSTANTIATED"),

    /**
     * All components registered with PICO including plugin components and plugin system has started.
     */
    STARTED(100, true, true, true, true, true, "startup.page.state.STARTED");

    private final int percentage;
    private final boolean componentsRegistered;
    private final boolean pluginSystemStarted;
    private final boolean started;
    private final boolean containerInitialised;
    private final boolean componentsInstantiated;
    private final String messageKey;

    ComponentManagerStateImpl(int percentage, boolean containerInitialised, boolean componentsRegistered,
                              boolean pluginSystemStarted, boolean componentsInstantiated,  boolean started, String messageKey) {
        this.percentage = percentage;
        this.containerInitialised = containerInitialised;
        this.componentsRegistered = componentsRegistered;
        this.pluginSystemStarted = pluginSystemStarted;
        this.componentsInstantiated = componentsInstantiated;
        this.started = started;
        this.messageKey = messageKey;
    }

    @Override
    public boolean isComponentsInstantiated() {
        return componentsInstantiated;
    }

    @Override
    public boolean isComponentsRegistered() {
        return componentsRegistered;
    }

    @Override
    public boolean isPluginSystemStarted() {
        return pluginSystemStarted;
    }

    @Override
    public boolean isStarted() {
        return started;
    }

    @Override
    public boolean isContainerInitialised() {
        return containerInitialised;
    }

    @Override
    public String getMessageKey() {
        return messageKey;
    }

    @Override
    public int getPercentage() {
        return percentage;
    }
}
