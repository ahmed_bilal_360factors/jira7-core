package com.atlassian.jira.bc.group;

import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

/**
 * Verifies relationships between groups
 *
 * @since v7.0
 */
public class GroupRelationshipChecker {
    private static final Logger log = LoggerFactory.getLogger(GroupRelationshipChecker.class);

    private final DirectoryManager directoryManager;

    public GroupRelationshipChecker(final DirectoryManager directoryManager) {
        this.directoryManager = directoryManager;
    }

    /**
     * "Not entirely" accurate group relationship check. When {@literal userDirectoryId} is {@link
     * java.util.Optional#empty()}, it is not possible to check possible group nesting relationship. In such case simple
     * comparison is performed.
     *
     * @param userDirectoryId directory, in which lookup is performed
     * @param checkedGroup    checked group
     * @param referenceGroup  group, against which check will be performed
     */
    public boolean isGroupEqualOrNested(final Optional<Long> userDirectoryId, final String checkedGroup, final String referenceGroup) {
        return userDirectoryId.map(id -> isGroupEqualOrNested(id, checkedGroup, referenceGroup))
                .orElse(isGroupEqual(checkedGroup, referenceGroup));
    }

    private boolean isGroupEqualOrNested(final long userDirectoryId, final String checkedGroup, final String referenceGroup) {
        return isGroupEqual(checkedGroup, referenceGroup) || (isGivenGroupNestedGroup(userDirectoryId, referenceGroup, checkedGroup));
    }

    private boolean isGroupEqual(final String group1, final String group2) {
        return IdentifierUtils.equalsInLowerCase(group1, group2);
    }

    private boolean isGivenGroupNestedGroup(final long directory, final String parentGroup, final String childGroup) {
        try {
            return directoryManager.isGroupNestedGroupMember(directory, childGroup, parentGroup);
        } catch (DirectoryNotFoundException e) {
            log.warn("Could not find directory with id {}", directory);
            return false;
        } catch (OperationFailedException e) {
            log.warn("Group inheritance check failed", e);
            return false;
        }
    }

    ;
}
