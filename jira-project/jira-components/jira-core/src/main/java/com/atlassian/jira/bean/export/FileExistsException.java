package com.atlassian.jira.bean.export;

public class FileExistsException extends Exception {
    public FileExistsException() {
    }

    public FileExistsException(String message) {
        super(message);
    }
}
