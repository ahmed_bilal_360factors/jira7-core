package com.atlassian.jira.bc.project.version;

import com.atlassian.jira.project.version.DeleteVersionWithCustomFieldParameters;
import com.atlassian.jira.project.version.DeleteVersionWithCustomFieldParameters.CustomFieldReplacement;
import com.atlassian.jira.project.version.Version;
import com.google.common.collect.Lists;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class DeleteVersionWithReplacementsParametrsBuilderImpl implements DeleteVersionWithReplacementsParameterBuilder {
    @Nullable
    private final Version versionToDelete;
    @Nullable
    private Version moveFixIssuesTo;
    @Nullable
    private Version moveAffectedIssuesTo;
    @Nullable
    private List<CustomFieldReplacement> customFieldReplacementList;

    public DeleteVersionWithReplacementsParametrsBuilderImpl(Version versionToDelete) {
        this.versionToDelete = versionToDelete;
    }

    @Override
    public DeleteVersionWithReplacementsParameterBuilder moveFixIssuesTo(Version moveFixIssuesTo) {
        this.moveFixIssuesTo = moveFixIssuesTo;

        return this;
    }

    @Override
    public DeleteVersionWithReplacementsParameterBuilder moveAffectedIssuesTo(Version moveAffectedIssuesTo) {
        this.moveAffectedIssuesTo = moveAffectedIssuesTo;

        return this;
    }

    @Override
    public DeleteVersionWithReplacementsParameterBuilder moveCustomFieldTo(long customFieldId, Version moveCustomFieldTo) {
        customFieldsReplacements().add(new CustomFieldReplacementImpl(customFieldId, moveCustomFieldTo));

        return this;
    }

    @Override
    public DeleteVersionWithCustomFieldParameters build() {
        if (null == versionToDelete) {
            throw new IllegalArgumentException("versionToDelete");
        }


        return new DeleteVersionWithCustomFieldParametersImpl(
                versionToDelete,
                Optional.ofNullable(moveAffectedIssuesTo), Optional.ofNullable(moveFixIssuesTo),
                customFieldReplacementList == null ? Collections.emptyList() : customFieldReplacementList);
    }

    @Nonnull
    private List<CustomFieldReplacement> customFieldsReplacements() {
        if (customFieldReplacementList == null) {
            customFieldReplacementList = Lists.newArrayList();
        }
        return customFieldReplacementList;
    }

    private class CustomFieldReplacementImpl implements CustomFieldReplacement {
        private final long customFieldId;
        private final Version moveCustomFieldTo;

        public CustomFieldReplacementImpl(long customFieldId, Version moveCustomFieldTo) {
            this.customFieldId = customFieldId;
            this.moveCustomFieldTo = moveCustomFieldTo;
        }

        @Override
        public long getCustomFieldId() {
            return customFieldId;
        }

        @Override
        public Optional<Version> getMoveTo() {
            return Optional.ofNullable(moveCustomFieldTo);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CustomFieldReplacementImpl that = (CustomFieldReplacementImpl) o;

            if (customFieldId != that.customFieldId) return false;
            return moveCustomFieldTo != null ? moveCustomFieldTo.equals(that.moveCustomFieldTo) : that.moveCustomFieldTo == null;

        }

        @Override
        public int hashCode() {
            int result = (int) (customFieldId ^ (customFieldId >>> 32));
            result = 31 * result + (moveCustomFieldTo != null ? moveCustomFieldTo.hashCode() : 0);
            return result;
        }
    }
}
