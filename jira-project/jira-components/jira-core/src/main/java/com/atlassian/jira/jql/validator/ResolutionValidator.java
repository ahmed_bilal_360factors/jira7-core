package com.atlassian.jira.jql.validator;

import com.atlassian.jira.issue.fields.ResolutionSystemField;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operator.OperatorClasses;
import com.atlassian.jira.jql.resolver.ResolutionIndexInfoResolver;
import com.atlassian.jira.jql.resolver.ResolutionResolver;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;

/**
 * A simple wrapper around ConstantsClauseValidator.
 *
 * @since v4.0
 */
public class ResolutionValidator implements ClauseValidator {
    ///CLOVER:OFF

    private final RawValuesExistValidator rawValuesExistValidator;
    private final SupportedOperatorsValidator supportedOperatorsValidator;

    public ResolutionValidator(final ResolutionResolver resolutionResolver, final JqlOperandResolver operandResolver, I18nHelper.BeanFactory beanFactory) {
        this.rawValuesExistValidator = new RawValuesExistValidator(operandResolver, new ResolutionIndexInfoResolver(resolutionResolver), beanFactory);
        this.supportedOperatorsValidator = getSupportedOperatorsValidator();
    }

    public MessageSet validate(final ApplicationUser searcher, final TerminalClause terminalClause) {
        final MessageSet messageSet = supportedOperatorsValidator.validate(searcher, terminalClause);
        if (!messageSet.hasAnyErrors()) {
            messageSet.addMessageSet(rawValuesExistValidator.validate(searcher, terminalClause));
        }
        return messageSet;
    }

    SupportedOperatorsValidator getSupportedOperatorsValidator() {
        return new SupportedOperatorsValidator(OperatorClasses.EQUALITY_OPERATORS_WITH_EMPTY, OperatorClasses.RELATIONAL_ONLY_OPERATORS) {
            @Override
            public MessageSet validate(ApplicationUser searcher, TerminalClause terminalClause) {
                final Operand operand = terminalClause.getOperand();
                final Operator operator = terminalClause.getOperator();
                if (OperatorClasses.RELATIONAL_ONLY_OPERATORS.contains(operator)) {
                    if (operand instanceof SingleValueOperand) {
                        String stringOperand = ((SingleValueOperand) operand).getStringValue();
                        if (ResolutionSystemField.UNRESOLVED_OPERAND.equalsIgnoreCase(stringOperand)) {
                            I18nHelper i18n = getI18n(searcher);
                            MessageSet messageSet = new MessageSetImpl();
                            messageSet.addErrorMessage(i18n.getText("jira.jql.clause.does.not.support.operator.operand.combination", operator.getDisplayString(), terminalClause.getName(), stringOperand));
                            return messageSet;
                        }
                    }
                }
                return super.validate(searcher, terminalClause);
            }
        };
    }

    ///CLOVER:ON
}
