package com.atlassian.jira.permission.management;

import com.atlassian.fugue.Either;
import com.atlassian.jira.permission.management.beans.PermissionsInputBean;
import com.atlassian.jira.permission.management.beans.ProjectPermissionAddBean;
import com.atlassian.jira.permission.management.beans.ProjectPermissionSchemeBean;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;

import java.util.List;

/**
 * A helper that can give out REST ready beans for permission scheme editing.  Allows is to make rest requests AND
 * pre-prime via data providers on the web page.
 * <p/>
 * This is different to the official permission scheme REST API in that its shaped to help support the internal JIRA
 * management screens, as opposed to the generic API.  It has UI related data items that the public REST API has no use for
 */
public interface ManagedPermissionSchemeHelper {

    /**
     * Gets information that allows the manage permission scheme to be displayed
     *
     * @param user               the user in play
     * @param permissionSchemeId the permission scheme to show
     * @return either errors or the permission scheme in bean format
     */
    Either<ErrorCollection, ProjectPermissionSchemeBean> getManagedPermissionScheme(final ApplicationUser user, final Long permissionSchemeId);

    /**
     * Gets information that allows the add to permission scheme to be displayed
     *
     * @param user                  the user in play
     * @param permissionSchemeId    the permission scheme to be added to
     * @param permissionKey         the key to add to
     * @return either errors or the permission scheme in bean format
     */
    Either<ErrorCollection, ProjectPermissionAddBean> getManagedPermissionSchemeAddView(ApplicationUser user, Long permissionSchemeId, String permissionKey);

    /**
     * Returns the security types split into primary and secondary types for display.
     *
     * @param user  the user in play
     * @return either errors or the security types in bean format
     */
    Either<ErrorCollection, ProjectPermissionAddBean> getManagedPermissionSchemeAddViewSecurityTypes(ApplicationUser user);

    /**
     * This allows a list of security types to be add to a list of permissions inside a permission scheme
     *
     * @param user               the user in play
     * @param permissionSchemeId the permission scheme to be deleted from
     * @param inputBean          the grants things to add and to what permissions to add them to
     * @return either errors or the permission scheme in bean format
     */
    Either<ErrorCollection, ProjectPermissionSchemeBean> addManagedPermissionSchemeGrants(final ApplicationUser user, final Long permissionSchemeId, final PermissionsInputBean inputBean);

    /**
     * This allows a list of security types to be removed from a permission scheme
     *
     * @param user               the user in play
     * @param permissionSchemeId the permission scheme to be deleted from
     * @param grantsToDelete     the security type grants to remove from the scheme
     * @return either errors or the permission scheme in bean format
     */
    Either<ErrorCollection, ProjectPermissionSchemeBean> removeManagedPermissionSchemeGrants(ApplicationUser user, Long permissionSchemeId, List<Long> grantsToDelete);

}
