package com.atlassian.jira.application.install;

import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.plugin.PluginPath;
import com.atlassian.jira.web.ServletContextProvider;

import java.io.File;

/**
 * Holds infomation where application files should be taken and where should be placed.
 *
 * @since v6.5
 */
public class ApplicationInstallationEnvironment {
    private static final String JIRA_HOME_INSTALL_APP_INFO = "plugins/install-app-info";
    private static final String APPLICATION_INSTALLATION = "/WEB-INF/application-installation";
    private final JiraHome jiraHome;
    private final PluginPath pluginPath;

    public ApplicationInstallationEnvironment(final JiraHome jiraHome, final PluginPath pluginPath) {
        this.jiraHome = jiraHome;
        this.pluginPath = pluginPath;
    }

    /**
     * Return folder that may contain folders with applications plugins (each application should have its own folder.
     * i.e) <ul> <li>[applications-source] <ul> <li>jira-software <ul> <li>software-plugin-1.jar</li>
     * <li>software-plugin-2.jar</li> </ul> </li> <li>jira-servicedesk <ul> <li>servicedesk-plugin-1.jar</li>
     * <li>servicedesk-plugin-2.jar</li> </ul> </li> </ul> </li> </ul>
     */
    public File getApplicationsSource() {
        return new File(ServletContextProvider.getServletContext().getRealPath(APPLICATION_INSTALLATION));
    }

    /**
     * Jira installed plugins directory.
     */
    public File getApplicationsDestination() {
        return pluginPath.getInstalledPluginsDirectory();
    }

    /**
     * Folder where information about installed applications and their versions is stored
     */
    public File getInstallInformationDir() {
        return new File(jiraHome.getLocalHome(), JIRA_HOME_INSTALL_APP_INFO);
    }
}
