package com.atlassian.jira.issue.statistics;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.web.bean.StatisticMapWrapper;
import com.atlassian.query.Query;

import java.util.Optional;
import java.util.Set;

public class ProjectStatisticsManagerImpl implements ProjectStatisticsManager {

    private StatisticsManager statisticsManager;

    public ProjectStatisticsManagerImpl(StatisticsManager statisticsManager) {
        this.statisticsManager = statisticsManager;
    }

    /** {@inheritDoc} */
    public Set<Project> getProjectsResultingFrom(Optional<Query> query) {
        StatisticMapWrapper<Object, Integer> data =
                statisticsManager.getObjectsResultingFrom(query, FilterStatisticsValuesGenerator.PROJECT);
        return (Set<Project>) (Set) data.keySet();
    }
}
