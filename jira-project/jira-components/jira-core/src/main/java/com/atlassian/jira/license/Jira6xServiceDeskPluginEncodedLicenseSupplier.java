package com.atlassian.jira.license;

import com.atlassian.cache.Supplier;
import com.atlassian.fugue.Option;
import com.atlassian.jira.config.properties.ApplicationProperties;

import static org.apache.commons.lang3.StringUtils.stripToNull;

/**
 * Supplies a encoded plugin license for Service Desk version 1 or 2.
 * <p>
 * This class is intended to be used solely for the purposes of checking & migrating pre-7.0
 * Service Desk licenses. It is not a general-purposes SD license supplier, instead, see
 * {@link JiraLicenseManager#getLicense(com.atlassian.application.api.ApplicationKey).
 * <p>
 * To start and upgrade JIRA it needs to have all its licenses in maintenance. JIRA forces the admin to add/remove
 * licenses until this is the case. This means that we can remove the old UPM SD license before migration has run
 * which is very bad because we need it to migrate
 * ({@link com.atlassian.jira.upgrade.tasks.role.MoveJira6xABPServiceDeskPermissions}). To work around this problem
 * we move the license {@link #moveToUpgradeStore()} into a new location that means it becomes inactive but is still
 * available to upgrade tasks through {@link #getUpgrade()} or {@link #getCurrentOrUpgrade()}.
 *
 * @since 7.0
 */
public class Jira6xServiceDeskPluginEncodedLicenseSupplier implements Supplier<Option<String>> {
    /**
     * This key is used to locate the service desk plugin license in the ApplicationProperties (data store).
     * UPM creates an MD5 hash value using the key
     * com.atlassian.upm.license.internal.impl.PluginSettingsPluginLicenseRepository:licenses:com.atlassian.servicedesk
     * this is to reduce the number character to 100 to accommodate column size restrictions in data store.
     * for more information see ( com.atlassian.upm.impl.LongKeyHasher )
     */
    static final String UPM_SERVICE_DESK_LICENSE_KEY =
            "com.atlassian.upm.license.internal.impl.PluginSettingsPluginLicenseR339b1704e923b2280c224738f2d48ceb";

    /**
     * This key is for storing a license that we deleted but not really. Confused? Well so am I. To start and upgrade
     * JIRA all the licenses in it must be in maintenance. JIRA forces the admins to add/remove licenses until this
     * is the case. This means that we can remove the old UPM SD license before migration has run which is very bad
     * because we need it to migrate
     * ({@link com.atlassian.jira.upgrade.tasks.role.MoveJira6xABPServiceDeskPermissions}). To work around this problem
     * we just move the license into a new location that the upgrade tasks know about (and will delete for us).
     */
    static final String UPM_SERVICE_DESK_LICENSE_DELETED_KEY = "deleted_" + UPM_SERVICE_DESK_LICENSE_KEY;

    private final ApplicationProperties applicationProperties;

    public Jira6xServiceDeskPluginEncodedLicenseSupplier(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    /**
     * Retrieves encoded Service Desk license string from plugin store. No validation is performed, so returned
     * license may be malformed or may not be a Service Desk license.
     *
     * @return Option containing encoded license or {@link Option#none()} if there was no license in the plugin
     * location.
     */
    @Override
    public Option<String> get() {
        return getApplicationText(UPM_SERVICE_DESK_LICENSE_KEY);
    }

    /**
     * Retrieves encoded Service Desk license from the upgrade location. No validation is performed, so returned
     * license may be malformed or may not be a Service Desk license.
     *
     * @return Option containing encoded license or {@link Option#none()} if there was no such license.
     */
    public Option<String> getUpgrade() {
        return getApplicationText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY);
    }

    /**
     * Retrieves encoded Service Desk license string from plugin store or the upgrade location if it exists. No
     * validation is performed, so returned license may be malformed or may not be a Service Desk license.
     *
     * @return the encoded Service Desk license string from plugin store or the upgrade location if it exists.
     */
    public Option<String> getCurrentOrUpgrade() {
        return get().orElse(this::getUpgrade);
    }

    /**
     * Move the license from the UPM store into a location for upgrade. This logically removes the license from JIRA
     * but leaves it accessible to upgrade tasks through {@link #getUpgrade()} or {@link #getCurrentOrUpgrade()}.
     */
    public void moveToUpgradeStore() {
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, get().getOrNull());
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, null);
    }

    /**
     * Remove the SD license from the plugin store and/or upgrade location.
     */
    public void clear() {
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, null);
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, null);
    }

    private Option<String> getApplicationText(final String key) {
        return Option.option(stripToNull(applicationProperties.getText(key)));
    }
}
