package com.atlassian.jira.web.action.issue;

import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;

import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.avatar.Avatar.Size.SMALL;
import static com.atlassian.jira.security.Permissions.MANAGE_WATCHER_LIST;

/**
 * The default implementation of {@link IssueMetadataHelper}.
 *
 * @since v6.0
 */
public class DefaultIssueMetadataHelper implements IssueMetadataHelper {
    private final JiraAuthenticationContext authenticationContext;
    private final AvatarService avatarService;
    private final PermissionManager permissionManager;
    private final UserSearchService userSearchService;

    public DefaultIssueMetadataHelper(
            final JiraAuthenticationContext authenticationContext,
            final AvatarService avatarService,
            final PermissionManager permissionManager,
            final UserSearchService userSearchService) {
        this.authenticationContext = authenticationContext;
        this.avatarService = avatarService;
        this.permissionManager = permissionManager;
        this.userSearchService = userSearchService;
    }

    @Override
    public Map<String, String> getMetadata(Issue issue, SearchRequest searchRequest) {
        Map<String, String> metadata = new HashMap<String, String>();
        metadata.put("can-edit-watchers", canEditWatchers(issue));
        metadata.put("can-search-users", canSearchUsers());
        metadata.put("default-avatar-url", getDefaultAvatarURL());
        metadata.put("issue-key", issue.getKey());
        metadata.put("issue-project-type", getProjectType((issue)));
        return metadata;
    }

    @Override
    public void putMetadata(
            final Issue issue,
            final SearchRequest searchRequest,
            final JiraWebResourceManager webResourceManager) {
        Map<String, String> metadata = getMetadata(issue, searchRequest);
        for (Map.Entry<String, String> entry : metadata.entrySet()) {
            webResourceManager.putMetadata(entry.getKey(), entry.getValue());
        }
    }

    /**
     * @param issue An issue.
     * @return whether the current user can manage {@code issue}'s watchers.
     */
    private String canEditWatchers(final Issue issue) {
        return Boolean.toString(permissionManager.hasPermission(
                MANAGE_WATCHER_LIST, issue, authenticationContext.getLoggedInUser()));
    }

    /**
     * @return whether the current user can search for users via AJAX.
     */
    private String canSearchUsers() {
        return Boolean.toString(userSearchService.canPerformAjaxSearch(
                authenticationContext.getLoggedInUser()));
    }

    /**
     * @return the URL of the default avatar.
     */
    private String getDefaultAvatarURL() {
        return avatarService.getAvatarURL(authenticationContext.getLoggedInUser(), (ApplicationUser) null,
                SMALL).toString();
    }

    /**
     * Attempts to get the project type of the issue.
     * @param issue An issue.
     * @return Project Type if possible or an empty string "";
     */
    private String getProjectType(final Issue issue) {
        final Project projectObject = issue.getProjectObject();
        final ProjectTypeKey projectTypeKey =  projectObject != null ? projectObject.getProjectTypeKey() : null;
        return projectTypeKey != null ? projectTypeKey.getKey() : "";
    }
}
