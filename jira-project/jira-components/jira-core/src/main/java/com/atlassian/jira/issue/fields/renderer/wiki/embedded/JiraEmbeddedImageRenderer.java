package com.atlassian.jira.issue.fields.renderer.wiki.embedded;

import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.attachments.RendererAttachment;
import com.atlassian.renderer.attachments.RendererAttachmentManager;
import com.atlassian.renderer.embedded.EmbeddedImage;
import com.atlassian.renderer.embedded.EmbeddedImageRenderer;
import com.atlassian.renderer.embedded.EmbeddedResource;

import java.util.Map;

public class JiraEmbeddedImageRenderer extends EmbeddedImageRenderer {
    private long attachmentId;
    private String attachmentSrc;
    private String attachmentName;
    private boolean isThumbNail;

    public JiraEmbeddedImageRenderer(RendererAttachmentManager attachmentManager) {
        super(attachmentManager);
    }

    @Override
    public String renderResource(EmbeddedResource resource, RenderContext context) {
        final EmbeddedImage image = (EmbeddedImage) resource;
        isThumbNail = image.isThumbNail();
        if (isThumbNail && !image.isExternal()) {
            try {
                RendererAttachment attachment = getAttachment(context, resource);
                attachmentId = attachment.getId();
                attachmentSrc = attachment.getSrc();
                attachmentName = attachment.getFileName();
            } catch (RuntimeException re) {
                // Give up on the thumbnail
            }
        }

        return super.renderResource(resource, context);
    }

    @Override
    protected String writeImage(final String imageTag, final Map<Object, Object> imageParams, final RenderContext context) {
        if (isThumbNail) {
            //wrap the thumbnail image with the link to the actual attachment (the full-sized version) JRA-11198
            final String wrappedImageTag = "<a id=\"" + attachmentId + "_thumb\" href=\"" + attachmentSrc + "\" title=\"" +
                    attachmentName + "\" file-preview-type=\"image\" file-preview-id=\""+attachmentId+"\" file-preview-title=\""+attachmentName+"\">" + imageTag + "</a>";
            return super.writeImage(wrappedImageTag, imageParams, context);
        }
        return super.writeImage(imageTag, imageParams, context);
    }
}

