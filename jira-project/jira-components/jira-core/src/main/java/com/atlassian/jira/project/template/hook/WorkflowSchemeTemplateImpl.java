package com.atlassian.jira.project.template.hook;

import com.google.common.base.Strings;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.unmodifiableList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkflowSchemeTemplateImpl implements WorkflowSchemeTemplate {
    private final String name;
    private final String description;
    private final Optional<String> defaultWorkflow;
    private final List<? extends WorkflowTemplate> workflowTemplates;

    public WorkflowSchemeTemplateImpl(
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("default-workflow") String defaultWorkflow,
            @JsonProperty("workflows") List<WorkflowTemplateImpl> workflowTemplates) {
        this.name = checkNotNull(name);
        this.description = Strings.nullToEmpty(description);
        this.defaultWorkflow = defaultWorkflow == null ? Optional.empty() : Optional.of(defaultWorkflow.toUpperCase());
        this.workflowTemplates = checkNotNull(workflowTemplates);

        validate();
    }

    @Override
    public String name() {
        return this.name;
    }

    @Override
    public String description() {
        return this.description;
    }

    @Override
    public Optional<String> defaultWorkflow() {
        return this.defaultWorkflow;
    }

    @Override
    public List<WorkflowTemplate> workflowTemplates() {
        return unmodifiableList(workflowTemplates);
    }

    @Override
    public boolean hasWorkflow(String workflowKey) {
        for (WorkflowTemplate workflowTemplate : workflowTemplates) {
            if (workflowTemplate.key().equalsIgnoreCase(workflowKey)) {
                return true;
            }
        }

        return false;
    }

    private void validate() {
        validateDefaultWorkflow();
    }

    private void validateDefaultWorkflow() {
        if (defaultWorkflow.isPresent() && !hasWorkflow(defaultWorkflow.get())) {
            throw new IllegalArgumentException("Default workflow '" + defaultWorkflow.get() +
                    "' of workflow scheme '" + name + "' does not exist.");
        }
    }
}
