package com.atlassian.jira.transaction;

import com.atlassian.core.ofbiz.util.CoreTransactionUtil;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import org.ofbiz.core.entity.GenericTransactionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

/**
 * @since v4.4.1
 */
public class TransactionSupportImpl implements TransactionSupport {
    private static final Logger log = LoggerFactory.getLogger(TransactionSupportImpl.class);

    private final ApplicationProperties applicationProperties;
    private final RequestLocalTransactionRunnableQueueFactory requestLocalTransactionRunnableQueueFactory;

    public TransactionSupportImpl(final ApplicationProperties applicationProperties, RequestLocalTransactionRunnableQueueFactory requestLocalTransactionRunnableQueueFactory) {
        this.applicationProperties = applicationProperties;
        this.requestLocalTransactionRunnableQueueFactory = requestLocalTransactionRunnableQueueFactory;
    }

    @Override
    public Transaction begin() throws TransactionRuntimeException {
        // we share the same request local runnable queue between new and joined txns
        RunnablesQueue runQueue = requestLocalTransactionRunnableQueueFactory.getRunnablesQueue();
        if (startedTransaction(false)) {
            return new TransactionImpl(runQueue);
        }
        return new JoinedTransaction(runQueue);
    }

    private boolean startedTransaction(boolean forceTxn) throws TransactionRuntimeException {
        /*
         * We can squib and revert to our JIRA code heritage and no use database
         * transactions
         */
        //noinspection SimplifiableIfStatement
        if (!forceTxn && applicationProperties.getOption(APKeys.JIRA_DB_TXN_DISABLED)) {
            return false;
        }
        return beginTxn();
    }

    // package level for testing
    boolean beginTxn() throws TransactionRuntimeException {
        try {
            return CoreTransactionUtil.begin();
        } catch (GenericTransactionException e) {
            throw new TransactionRuntimeException(e);
        }
    }

    private static class TransactionImpl implements Transaction {

        private boolean committed = false;
        private final RunnablesQueue runQueue;

        private TransactionImpl(RunnablesQueue runQueue) {
            this.runQueue = runQueue;
        }

        @Override
        public void runAfterSuccessfulCommit(@Nonnull Runnable runnable) {
            runQueue.offer(runnable);
        }

        @Override
        public void commit() throws TransactionRuntimeException {
            try {
                CoreTransactionUtil.commit(true);
                committed = true;
                runQueue.runAndClear();
            } catch (GenericTransactionException e) {
                log.error("Unable to commit transaction : " + e.getMessage(), e);
                throw new TransactionRuntimeException(e);
            } finally {
                runQueue.clear();
            }
        }

        @Override
        public void rollback() throws TransactionRuntimeException {
            if (committed) {
                throw new IllegalStateException("The transaction has already been committed and hence you cannot rollback");
            }
            try {
                // and rollback
                CoreTransactionUtil.rollback(true);
            } catch (GenericTransactionException e) {
                log.error("Unable to rollback transaction : " + e.getMessage(), e);
                throw new TransactionRuntimeException(e);
            } finally {
                runQueue.clear();
            }
        }

        @Override
        public void finallyRollbackIfNotCommitted() {
            if (!committed) {
                try {
                    rollback();
                } catch (TransactionRuntimeException ignored) {
                    // this is very deliberate.  See the method description
                }
            }
        }

        @Override
        public boolean isNewTransaction() {
            return true;
        }

    }

    private static class JoinedTransaction implements Transaction {
        private final RunnablesQueue runQueue;

        private boolean committed = false;

        private JoinedTransaction(RunnablesQueue runQueue) {
            this.runQueue = runQueue;
        }

        @Override
        public void runAfterSuccessfulCommit(@Nonnull Runnable runnable) {
            runQueue.offer(runnable);
        }

        @Override
        public void commit() {
            // We have joined an outer transaction so should not commit work in the outer transactions scope.
            // since we share an event queue any events will be also in the outer Txn
            committed = true;
        }

        @Override
        public void rollback() throws TransactionRuntimeException {
            if (committed) {
                throw new IllegalStateException("The transaction has already been committed and hence you cannot rollback");
            }
            try {
                CoreTransactionUtil.setRollbackOnly(false);
            } catch (GenericTransactionException e) {
                log.error("Unable to rollback transaction : " + e.getMessage(), e);
                throw new TransactionRuntimeException(e);
            }
        }

        @Override
        public void finallyRollbackIfNotCommitted() {
            if (!committed) {
                try {
                    rollback();
                } catch (TransactionRuntimeException ignored) {
                    // this is very deliberate.  See the method description
                }
            }
        }

        @Override
        public boolean isNewTransaction() {
            return false;
        }
    }
}
