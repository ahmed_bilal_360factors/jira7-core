package com.atlassian.jira.bc.project.version;

import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionImpl;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;

/**
 * A builder class to specify a new Version to create or an existing Version to update.
 *
 * @since v7.0
 */
public class VersionBuilderImpl implements VersionBuilder {
    private Version version;

    private Long projectId;

    private Long sequence;

    private String name;
    private String description;

    private Date startDate;
    private Date releaseDate;

    private Long scheduleAfterVersion;
    private boolean released;
    private boolean archived;

    public VersionBuilderImpl() {
        this.version = null;
    }

    public VersionBuilderImpl(Version version) {
        this.version = version;

        this.projectId = version.getProjectId();

        this.sequence = version.getSequence();

        this.name = version.getName();
        this.description = version.getDescription();

        this.startDate = version.getStartDate();
        this.releaseDate = version.getReleaseDate();

        this.released = version.isReleased();
        this.archived = version.isArchived();
    }

    @Nonnull
    @Override
    public VersionBuilder projectId(@Nonnull Long projectId) {
        this.projectId = projectId;
        return this;
    }

    @Nullable
    public Long getProjectId() {
        return projectId;
    }

    @Nonnull
    @Override
    public VersionBuilder name(@Nonnull String name) {
        this.name = name;
        return this;
    }

    @Nullable
    public String getName() {
        return name;
    }

    @Nonnull
    @Override
    public VersionBuilder description(@Nullable String description) {
        this.description = description;
        return this;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    @Nonnull
    @Override
    public VersionBuilder startDate(@Nullable Date startDate) {
        this.startDate = startDate;
        return this;
    }

    @Nullable
    public Date getStartDate() {
        return startDate;
    }

    @Nonnull
    @Override
    public VersionBuilder releaseDate(@Nullable Date releaseDate) {
        this.releaseDate = releaseDate;
        return this;
    }

    @Nullable
    public Date getReleaseDate() {
        return releaseDate;
    }

    @Nonnull
    @Override
    public VersionBuilder scheduleAfterVersion(@Nonnull Long scheduleAfterVersion) {
        this.scheduleAfterVersion = scheduleAfterVersion;
        return this;
    }

    @Nullable
    public Long getScheduleAfterVersion() {
        return scheduleAfterVersion;
    }

    @Nonnull
    @Override
    public VersionBuilder released(boolean released) {
        this.released = released;
        return this;
    }

    public boolean isReleased() {
        return released;
    }

    public boolean isArchived() {
        return archived;
    }

    @Nullable
    public Long getSequence() {
        return sequence;
    }

    @Nonnull
    @Override
    public VersionBuilder sequence(@Nonnull Long sequence) {
        this.sequence = sequence;
        return this;
    }

    @Nonnull
    public Version build() {
        return new VersionImpl(projectId, version != null ? version.getId() : null, name, description, sequence,
                archived, released, releaseDate, startDate);
    }

    @Nonnull
    public VersionBuilder archived(boolean archived) {
        this.archived = archived;
        return this;
    }

    @Nullable
    public Version getVersion() {
        return version;
    }
}