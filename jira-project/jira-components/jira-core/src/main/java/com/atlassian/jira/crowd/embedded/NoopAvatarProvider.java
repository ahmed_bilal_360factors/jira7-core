package com.atlassian.jira.crowd.embedded;

import com.atlassian.crowd.manager.avatar.AvatarProvider;
import com.atlassian.crowd.model.user.User;

import javax.annotation.Nullable;
import java.net.URI;

/**
 * Noop implementation of avatars
 *
 * @since v7.3
 */
public class NoopAvatarProvider implements AvatarProvider {
    @Nullable
    @Override
    public URI getUserAvatar(User user, int sizeHint) {
        return null;
    }

    @Nullable
    @Override
    public URI getHostedUserAvatarUrl(long applicationId, String username, int sizeHint) {
        return null;
    }
}
