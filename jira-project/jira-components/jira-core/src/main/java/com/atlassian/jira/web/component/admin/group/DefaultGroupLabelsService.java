package com.atlassian.jira.web.component.admin.group;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.group.GroupAccessLabelsManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringEscapeUtils;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;

public class DefaultGroupLabelsService implements GroupLabelsService {
    private final ApplicationRoleManager roleManager;
    private final GroupAccessLabelsManager labelsManager;
    private final JiraAuthenticationContext authenticationContext;

    public DefaultGroupLabelsService(final ApplicationRoleManager roleManager, final GroupAccessLabelsManager labelsManager, final JiraAuthenticationContext authenticationContext) {
        this.roleManager = roleManager;
        this.labelsManager = labelsManager;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public List<GroupLabelView> getGroupLabels(Group group, Optional<Long> directory) {
        final ImmutableList.Builder<GroupLabelView> labels = ImmutableList.builder();
        final I18nHelper i18n = authenticationContext.getI18nHelper();
        final GroupAccessLabelsManager.GroupAccessLabels groupLabels = labelsManager.getForGroup(group, directory);
        if (groupLabels.isAdmin()) {
            labels.add(new GroupLabelView(i18n.getText("admin.viewgroup.labels.admin.text"), i18n.getText("admin.viewgroup.labels.admin.description"), GroupLabelView.LabelType.ADMIN));
        }

        final List<ApplicationRole> apps = groupLabels.getAccessibleApplications().stream().collect(CollectorsUtil.toImmutableList());

        if (apps.size() > 0) {
            final String labelText = getLabelText(apps, i18n);
            final GroupLabelView.LabelType labelType = apps.size() == 1 ? GroupLabelView.LabelType.SINGLE : GroupLabelView.LabelType.MULTIPLE;

            if (apps.size() > 5) {
                labels.add(new GroupLabelView(labelText, i18n.getText("admin.viewgroup.labels.application.access.multiple.description"), labelType));
            } else {
                final List<String> appNamesHtml = apps.stream()
                        .map(this::getXSSSecureBoldApplicationName)
                        .collect(CollectorsUtil.toImmutableList());
                labels.add(new GroupLabelView(labelText, i18n.getText("admin.viewgroup.labels.application.access.description." + apps.size() + ".app", appNamesHtml), labelType));
            }
        }

        return labels.build();
    }

    private String getLabelText(final List<ApplicationRole> applicationRoles, final I18nHelper i18n) {
        if (applicationRoles.size() == 1) {
            return applicationRoles.get(0).getName();
        }
        return i18n.getText("admin.viewgroup.labels.application.access.multiple.text");
    }

    private String getXSSSecureBoldApplicationName(@Nonnull ApplicationRole applicationRole) {
        return "<strong>" + StringEscapeUtils.escapeHtml4(applicationRole.getName()) + "</strong>";
    }
}
