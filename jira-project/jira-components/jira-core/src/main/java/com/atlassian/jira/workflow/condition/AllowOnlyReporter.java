package com.atlassian.jira.workflow.condition;

import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.user.ApplicationUser;
import com.opensymphony.module.propertyset.PropertySet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * A Condition which passes when the user is the issue's reporter.
 */
public class AllowOnlyReporter extends AbstractJiraCondition {
    private static final Logger log = LoggerFactory.getLogger(AllowOnlyReporter.class);

    public boolean passesCondition(Map transientVars, Map args, PropertySet ps) {
        ApplicationUser caller = getCallerUser(transientVars, args);
        ApplicationUser reporter = null;
        try {
            reporter = getIssue(transientVars).getReporter();
        } catch (DataAccessException e) {
            log.warn("Could not retrieve reporter with id '" + getIssue(transientVars).getAssigneeId() + "' of issue '" + getIssue(transientVars).getKey() + "'");
        }
        if (caller != null && reporter != null && caller.equals(reporter))
            return true;
        else
            return false;
    }
}
