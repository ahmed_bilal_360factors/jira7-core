package com.atlassian.jira.issue.fields;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.export.ExportableSystemField;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.export.FieldExportPartsBuilder;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.search.LuceneFieldSorter;
import com.atlassian.jira.issue.search.parameters.lucene.sort.StringSortComparator;
import com.atlassian.jira.issue.statistics.IssueKeyStatisticsMapper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.VelocityTemplatingEngine;

import com.atlassian.jira.util.I18nHelper;
import org.apache.lucene.search.SortField;

public class KeySystemField extends NavigableFieldImpl implements ExportableSystemField {

    public KeySystemField(VelocityTemplatingEngine templatingEngine, ApplicationProperties applicationProperties, JiraAuthenticationContext authenticationContext) {
        super(IssueFieldConstants.ISSUE_KEY, "issue.field.issuekey", "issue.column.heading.issuekey", ORDER_ASCENDING, templatingEngine, applicationProperties, authenticationContext);
    }

    public LuceneFieldSorter getSorter() {
        return IssueKeyStatisticsMapper.MAPPER;
    }

    @Override
    public List<SortField> getSortFields(final boolean sortOrder) {
        List<SortField> sortFields = new ArrayList<>();
        sortFields.add(new SortField(DocumentConstants.PROJECT_KEY, new StringSortComparator(), sortOrder));
        sortFields.add(new SortField(DocumentConstants.ISSUE_KEY_NUM_PART_RANGE, new StringSortComparator(), sortOrder));

        return sortFields;
    }

    public String getColumnViewHtml(FieldLayoutItem fieldLayoutItem, Map<String, Object> displayParams, Issue issue) {
        Map<String, Object> velocityParams = getVelocityParams(fieldLayoutItem, getAuthenticationContext().getI18nHelper(), displayParams, issue);
        velocityParams.put("applicationProperties", getApplicationProperties());
        return renderTemplate("key-columnview.vm", velocityParams);
    }

    /**
     * Returns a Multi Item representation of the field. For example in the CSV export it would look like the following:
     * {pre}
     * Issue Key, Issue ID, Parent ID
     * NEAN-18,10533,10005
     * {/pre}
     *
     * @param issue to get export for the field
     * @return export representation of the field
     */
    @Override
    public FieldExportParts getRepresentationFromIssue(Issue issue) {
        I18nHelper i18nHelper = authenticationContext.getI18nHelper();
        final FieldExportPartsBuilder builder = new FieldExportPartsBuilder();

        builder.addItem("key", i18nHelper.getText("admin.issue.export.field.name.issue.key"), issue.getKey());
        builder.addItem("issueId", i18nHelper.getText("admin.issue.export.field.name.issue.id"), String.valueOf(issue.getId()));
        if (issue.getParentId() != null) {
            builder.addItem("parentId", i18nHelper.getText("admin.issue.export.field.name.issue.parent.id"), String.valueOf(issue.getParentId()));
        }

        return builder.build();
    }
}
