package com.atlassian.jira.database;

import com.atlassian.core.ofbiz.util.CoreTransactionUtil;
import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.ofbiz.DefaultOfBizConnectionFactory;
import com.atlassian.jira.ofbiz.OfBizConnectionFactory;
import org.ofbiz.core.entity.DelegatorInterface;
import org.ofbiz.core.entity.GenericTransactionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;
import java.util.function.Function;

public class DatabaseAccessorImpl implements DatabaseAccessor {
    private static final Logger log = LoggerFactory.getLogger(DatabaseAccessorImpl.class);

    private final DatabaseVendor databaseVendor;
    private final Optional<String> schemaName;
    private final OfBizConnectionFactory ofBizConnectionFactory;
    private final DelegatorInterface delegatorInterface;
    private final String databaseType;

    public DatabaseAccessorImpl(DatabaseConfigurationManager databaseConfigurationManager, final DelegatorInterface delegatorInterface) {
        this.delegatorInterface = delegatorInterface;
        this.ofBizConnectionFactory = DefaultOfBizConnectionFactory.getInstance();

        final DatabaseConfig databaseConfig = databaseConfigurationManager.getDatabaseConfiguration();
        // Find the configured database vendor
        databaseVendor = findDatabaseVendor(databaseConfig);
        databaseType = databaseConfig.getDatabaseType();
        // find optinal scheme name
        final String schemaNameString = databaseConfig.getSchemaName();
        if (schemaNameString == null || schemaNameString.isEmpty()) {
            schemaName = Optional.empty();
        } else {
            schemaName = Optional.of(schemaNameString);
        }
    }

    @Override
    @Nonnull
    public DatabaseVendor getDatabaseVendor() {
        if (databaseVendor == null) {
            // JDEV-33950 avoid throwing IllegalStateException in constructor and throw here instead.
            throw new IllegalStateException("Unrecognised database dialect '" + databaseType + "'.");
        }
        return databaseVendor;
    }

    @Override
    @Nonnull
    public Optional<String> getSchemaName() {
        return schemaName;
    }

    @Override
    public <R> R executeQuery(@Nonnull final ConnectionFunction<R> callback) {
        final Connection con = borrowConnection();
        try {
            return callback.run(new DatabaseConnectionImpl(con));
        } catch (RuntimeException ex) {
            try {
                if (!con.getAutoCommit()) {
                    con.rollback();
                }
            } catch (SQLException sqlEx) {
                log.error("Unable to rollback SQL connection.", sqlEx);
            }
            // Rethrow the RuntimeException
            throw ex;
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                // We don't want to hide any exceptions that may have already been raised during the actual work.
                log.error("Unable to close SQL connection.", ex);
            }
        }
    }

    @Override
    public <R> R runInTransaction(@Nonnull final Function<Connection, R> callback) {
        final Connection existingConnection = CoreTransactionUtil.getConnection();
        if (existingConnection == null) {
            // no current transaction in OfBiz
            return executeQuery(conn -> {
                        conn.setAutoCommit(false);
                        // wrap the underlying JDBC connection as a safety measure against devs calling commit() close() etc
                        final Connection nestedConection = new NestedConnection(conn.getJdbcConnection());
                        // if callback throws RuntimeException, then executeQuery() will rollback for us
                        final R returnVal = callback.apply(nestedConection);
                        // no errors means we commit this transaction now
                        conn.commit();
                        return returnVal;
                    }
            );
        } else {
            // wrap the underlying JDBC connection as a safety measure against devs calling commit() close() etc
            final Connection nestedConection = new NestedConnection(existingConnection);
            try {
                // We don't commit here because we are part of a broader transaction
                return callback.apply(nestedConection);
            } catch (RuntimeException ex) {
                // Mark as rollback only in the OfBiz transaction ...
                setRollbackOnlyOnOfBizTransaction();
                // ... and rethrow
                throw ex;
            }
        }
    }

    private void setRollbackOnlyOnOfBizTransaction() {
        try {
            CoreTransactionUtil.setRollbackOnly(false);
        } catch (GenericTransactionException e) {
            // Declared exception, but reading the code it never is actually thrown AFAICT
            log.error("Unable to mark transaction rollback only", e);
            // Swallow the GenericTransactionException - we will rethrow the original RuntimeException in calling code
        }
    }

    private Connection borrowConnection() {
        try {
            return ofBizConnectionFactory.getConnection();
        } catch (SQLException ex) {
            throw new DataAccessException(ex);
        }
    }

    @Nullable
    private static DatabaseVendor findDatabaseVendor(final DatabaseConfig databaseConfig) {
        if (databaseConfig.isMySql()) {
            return DatabaseVendor.MY_SQL;
        }
        if (databaseConfig.isPostgres()) {
            return DatabaseVendor.POSTGRES;
        }
        if (databaseConfig.isOracle()) {
            return DatabaseVendor.ORACLE;
        }
        if (databaseConfig.isSqlServer()) {
            return DatabaseVendor.SQL_SERVER;
        }
        if (databaseConfig.isH2()) {
            return DatabaseVendor.H2;
        }

        // JDEV-33950 this is an illegal state, but we don't want to barf in the constructor.
        // Let's remember this error state and throw from getDatabaseVendor() instead.
        return null;
    }
}
