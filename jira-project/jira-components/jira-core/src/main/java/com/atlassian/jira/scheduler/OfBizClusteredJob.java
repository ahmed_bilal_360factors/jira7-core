package com.atlassian.jira.scheduler;

import com.atlassian.scheduler.caesium.spi.ClusteredJob;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;

/**
 * @since v7.0
 */
public class OfBizClusteredJob implements ClusteredJob {
    private final Long id;
    private final ClusteredJob delegate;

    public OfBizClusteredJob(final Long id, final ClusteredJob delegate) {
        this.id = id;
        this.delegate = delegate;
    }

    public Long getId() {
        return id;
    }

    @Override
    @Nonnull
    public JobId getJobId() {
        return delegate.getJobId();
    }

    @Override
    @Nonnull
    public JobRunnerKey getJobRunnerKey() {
        return delegate.getJobRunnerKey();
    }

    @Override
    @Nonnull
    public Schedule getSchedule() {
        return delegate.getSchedule();
    }

    @Override
    @Nullable
    public Date getNextRunTime() {
        return delegate.getNextRunTime();
    }

    @Override
    public long getVersion() {
        return delegate.getVersion();
    }

    @Override
    @Nullable
    public byte[] getRawParameters() {
        return delegate.getRawParameters();
    }

    @Override
    public String toString() {
        return "OfBizClusteredJob[id=" + id + ",delegate=" + delegate + ']';
    }
}
