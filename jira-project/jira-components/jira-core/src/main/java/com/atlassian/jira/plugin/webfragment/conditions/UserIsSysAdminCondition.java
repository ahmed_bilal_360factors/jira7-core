package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;

/**
 * Will return shouldDisplay of true if the user has the {@link com.atlassian.jira.security.Permissions#SYSTEM_ADMIN}
 * global permission.
 *
 * @since v3.12
 */
public class UserIsSysAdminCondition extends AbstractFixedPermissionCondition {
    public UserIsSysAdminCondition(GlobalPermissionManager permissionManager) {
        super(permissionManager, GlobalPermissionKey.SYSTEM_ADMIN);
    }
}
