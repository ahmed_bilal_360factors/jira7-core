package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.plugin.webfragment.conditions.cache.ConditionCacheKeys;
import com.atlassian.jira.plugin.webfragment.conditions.cache.RequestCachingConditionHelper;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Checks if a project is selected (in {@link JiraHelper}) and if the user has the {@code permission} for that project
 */
public class HasProjectPermissionCondition extends AbstractProjectPermissionCondition {

    private final PermissionManager permissionManager;

    public HasProjectPermissionCondition(PermissionManager permissionManager) {
        this.permissionManager = permissionManager;
    }

    @Override
    protected boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper, ProjectPermissionKey permissionKey) {

        final Project project = jiraHelper.getProject();

        if (project == null) {
            return false;
        }

        return RequestCachingConditionHelper.cacheConditionResultInRequest(
                ConditionCacheKeys.permission(permissionKey, user, project),
                () -> permissionManager.hasPermission(permissionKey, project, user));
    }
}
