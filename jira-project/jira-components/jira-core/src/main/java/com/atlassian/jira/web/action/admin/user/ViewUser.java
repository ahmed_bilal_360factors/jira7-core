package com.atlassian.jira.web.action.admin.user;

import com.atlassian.crowd.embedded.api.CrowdDirectoryService;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.jira.bc.user.CreateUserApplicationHelper;
import com.atlassian.jira.bc.user.GroupView;
import com.atlassian.jira.bc.user.UserApplicationHelper;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.web.action.IssueActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.opensymphony.module.propertyset.PropertySet;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebSudoRequired
public class ViewUser extends IssueActionSupport {
    protected String name;
    protected ApplicationUser user;
    private boolean showPasswordUpdateMsg;
    private Map<String, String> userProperties;

    protected final CrowdService crowdService;
    protected final CrowdDirectoryService crowdDirectoryService;
    protected final UserPropertyManager userPropertyManager;
    protected final UserManager userManager;
    protected final FeatureManager featureManager;
    private final UserApplicationHelper applicationHelper;

    public ViewUser(CrowdService crowdService,
                    CrowdDirectoryService crowdDirectoryService,
                    UserPropertyManager userPropertyManager,
                    UserManager userManager,
                    FeatureManager featureManager,
                    UserApplicationHelper applicationHelper) {
        this.crowdService = crowdService;
        this.crowdDirectoryService = crowdDirectoryService;
        this.userPropertyManager = userPropertyManager;
        this.userManager = userManager;
        this.featureManager = featureManager;
        this.applicationHelper = applicationHelper;
    }

    public ViewUser(CrowdService crowdService,
                    CrowdDirectoryService crowdDirectoryService,
                    UserPropertyManager userPropertyManager,
                    UserManager userManager) {
        this(crowdService, crowdDirectoryService, userPropertyManager, userManager,
                ComponentAccessor.getComponent(FeatureManager.class),
                ComponentAccessor.getComponent(UserApplicationHelper.class));
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ApplicationUser getUser() {
        return getApplicationUser();
    }

    public ApplicationUser getApplicationUser() {
        if (user == null) {
            user = userManager.getUserByName(getName());
        }
        return user;
    }

    public List<GroupView> getUserGroups() {
        return applicationHelper.getUserGroups(getApplicationUser());
    }

    public String getDirectoryName() {
        final ApplicationUser user = getApplicationUser();
        if (userManager.isUserExisting(user)) {
            return crowdDirectoryService.findDirectoryById(user.getDirectoryId()).getName();
        }
        return "???";
    }

    protected String doExecute() throws Exception {
        retrieveUserMetaProperties();
        return super.doExecute();
    }

    protected void doValidation() {
        if (getUser() == null) {
            addErrorMessage(getText("admin.errors.users.user.does.not.exist"));
        }
    }

    /**
     * This method retrieves a user's meta properties
     */
    protected void retrieveUserMetaProperties() {
        userProperties = new HashMap<>();

        ApplicationUser user = getUser();
        if (user != null) {
            PropertySet userPropertySet = userPropertyManager.getPropertySet(user);

            @SuppressWarnings("unchecked")
            Collection<String> keys = userPropertySet.getKeys(PropertySet.STRING);
            if (keys != null) {
                for (String key : keys) {
                    if (key.startsWith(UserUtil.META_PROPERTY_PREFIX)) {
                        userProperties.put(key.substring(UserUtil.META_PROPERTY_PREFIX.length()), userPropertySet.getString(key));
                    }
                }
            }

        }
    }

    public boolean isShowPasswordUpdateMsg() {
        return showPasswordUpdateMsg;
    }

    public void setShowPasswordUpdateMsg(boolean showPasswordUpdateMsg) {
        this.showPasswordUpdateMsg = showPasswordUpdateMsg;
    }

    public Map<String, String> getUserProperties() {
        return userProperties;
    }

    public boolean isRemoteUserPermittedToEditSelectedUser() {
        return getUser() != null && (isSystemAdministrator() || !getGlobalPermissionManager().hasPermission(Permissions.SYSTEM_ADMIN, getApplicationUser()));
    }

    public boolean isSelectedUserEditable() {
        if (userManager.canUpdateUser(getApplicationUser())) {
            return isRemoteUserPermittedToEditSelectedUser();
        }
        return false;
    }

    public boolean isSelectedUsersGroupsEditable() {
        return userManager.canUpdateGroupMembershipForUser(getUser());
    }

    public boolean isSelectedUserApplicationAccessEditable() {
        return isSelectedUsersGroupsEditable();
    }

    public boolean canUpdateUserPassword() {
        return isSelectedUserEditable() && userManager.canUpdateUserPassword(getUser());
    }

    public boolean getShowNoAppsWarning() {
        return !applicationHelper.canUserLogin(getApplicationUser());
    }

    @Nonnull
    @SuppressWarnings("unused")  // used in viewuser.jsp
    public Collection<CreateUserApplicationHelper.ApplicationSelection> getSelectableApplications() {
        return applicationHelper.getApplicationsForUser(getApplicationUser());
    }
}
