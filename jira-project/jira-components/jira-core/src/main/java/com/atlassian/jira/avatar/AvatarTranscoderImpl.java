package com.atlassian.jira.avatar;

import com.google.common.annotations.VisibleForTesting;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import javax.annotation.ParametersAreNonnullByDefault;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.io.FilenameUtils.removeExtension;

@ParametersAreNonnullByDefault
public class AvatarTranscoderImpl implements AvatarTranscoder {
    private interface FileWriter {
        void write(FileOutputStream fileStream) throws IOException;
    }

    private static final String PNG_EXTENSION = ".png";

    private final AvatarTagger avatarTagger;
    private final AvatarImageDataStorage avatarImageDataStorage;

    public AvatarTranscoderImpl(
            final AvatarTagger avatarTagger,
            final AvatarImageDataStorage avatarImageDataStorage) {
        this.avatarTagger = avatarTagger;
        this.avatarImageDataStorage = avatarImageDataStorage;
    }

    @Override
    public File getOrCreateRasterizedAvatarFile(final Avatar avatar, final Avatar.Size size, InputStream inputStream) throws IOException {
        checkArgument(avatar.isSystemAvatar(), "Avatar must be a system avatar");
        return writeToFileIfEmpty(getTranscodedFile(avatar, size), fileOutputStream ->
                transcodeAndTag(inputStream, fileOutputStream, size));
    }

    @Override
    public void transcodeAndTag(final InputStream inputStream, final OutputStream outputStream) throws IOException {
        transcodeAndTag(inputStream, outputStream, Avatar.Size.MEDIUM);
    }

    @Override
    public byte[] transcodeAndTag(String imageKey, final InputStream inputStream, final Avatar.Size size) throws IOException {
        String filename = rasterFileName(sanitizeFileName(imageKey), size);

        File fileWithPng = writeToFileIfEmpty(getFile(filename), outputStream -> transcodeAndTag(inputStream, outputStream, size));

        try (InputStream stream = new FileInputStream(fileWithPng)) {
            return IOUtils.toByteArray(stream);
        }
    }

    /**
     * Converts given SVG data to PNG format.
     *
     * @param inputStream  to be converted
     * @param outputStream to which the result should be written
     * @throws IOException
     */
    private void transcodeAndTag(final InputStream inputStream, final OutputStream outputStream, Avatar.Size size) throws IOException {
        try {
            final TranscoderInput transcoderInput = new TranscoderInput(inputStream);

            final ByteArrayOutputStream transcoded = new ByteArrayOutputStream();
            final TranscoderOutput transcoderOutput = new TranscoderOutput(transcoded);

            final PNGTranscoder transcoder = newPNGTranscoder(size);
            transcoder.transcode(transcoderInput, transcoderOutput);

            avatarTagger.tag(new ByteArrayInputStream(transcoded.toByteArray()), outputStream);
        } catch (TranscoderException e) {
            throw new RuntimeException(e);
        }
    }

    @VisibleForTesting
    PNGTranscoder createPngTranscoder() {
        return new PNGTranscoder();
    }

    @VisibleForTesting
    File getTranscodedFile(final Avatar avatar, final Avatar.Size size) {
        return getFile(rasterAvatarFileName(avatar, size));
    }

    private File writeToFileIfEmpty(File file, FileWriter fileWriter) throws IOException {
        if (!file.exists()) {
            try {
                file.getParentFile().mkdirs();
                try (final FileOutputStream outputStream = new FileOutputStream(file)) {
                    fileWriter.write(outputStream);
                }
            } catch (IOException e) {
                FileUtils.deleteQuietly(file);
                throw new IOException(e);
            } catch (Exception e) {
                FileUtils.deleteQuietly(file);
                throw new RuntimeException(e);
            }
        }
        return file;
    }

    private File getFile(String filename) {
        final File base = avatarImageDataStorage.getAvatarBaseDirectory();
        return new File(base, filename);
    }

    private String rasterAvatarFileName(final Avatar avatar, final Avatar.Size size) {
        return rasterFileName(avatar.getFileName(), size);
    }

    private String rasterFileName(final String baseFilename, final Avatar.Size size) {
        return removeExtension(size.param + "_" + baseFilename) + PNG_EXTENSION;
    }

    private String sanitizeFileName(final String imageKey) {
        String forbiddenCharacters = "[^\\w+\\.]";
        return imageKey.replaceAll("^" + forbiddenCharacters + "+", "").replaceAll(forbiddenCharacters, "_");
    }

    private PNGTranscoder newPNGTranscoder(final Avatar.Size size) {
        final PNGTranscoder transcoder = createPngTranscoder();
        transcoder.addTranscodingHint(PNGTranscoder.KEY_WIDTH, (float) size.getPixels());
        transcoder.addTranscodingHint(PNGTranscoder.KEY_HEIGHT, (float) size.getPixels());

        return transcoder;
    }
}
