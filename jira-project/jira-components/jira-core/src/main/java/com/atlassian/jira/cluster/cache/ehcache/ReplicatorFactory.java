package com.atlassian.jira.cluster.cache.ehcache;

import com.atlassian.jira.cluster.ReplicatorExecutorServiceFactory;
import net.sf.ehcache.event.CacheEventListener;
import net.sf.ehcache.event.CacheEventListenerFactory;
import net.sf.ehcache.util.PropertyUtil;

import java.util.Properties;

public class ReplicatorFactory extends CacheEventListenerFactory {
    private static final String REPLICATE_PUTS = "replicatePuts";
    private static final String REPLICATE_PUTS_VIA_COPY = "replicatePutsViaCopy";
    private static final String REPLICATE_UPDATES = "replicateUpdates";
    private static final String REPLICATE_UPDATES_VIA_COPY = "replicateUpdatesViaCopy";
    private static final String REPLICATE_REMOVALS = "replicateRemovals";

    @Override
    public CacheEventListener createCacheEventListener(final Properties properties) {
        return new BlockingParallelCacheReplicator(
                extractBoolean(REPLICATE_PUTS, properties),
                extractBoolean(REPLICATE_PUTS_VIA_COPY, properties),
                extractBoolean(REPLICATE_UPDATES, properties),
                extractBoolean(REPLICATE_UPDATES_VIA_COPY, properties),
                extractBoolean(REPLICATE_REMOVALS, properties),
                ReplicatorExecutorServiceFactory.getExecutorService()
        );
    }

    private boolean extractBoolean(final String key, final Properties properties) {
        String property = PropertyUtil.extractAndLogProperty(key, properties);
        return property == null || PropertyUtil.parseBoolean(property);   // defaults to true
    }
}
