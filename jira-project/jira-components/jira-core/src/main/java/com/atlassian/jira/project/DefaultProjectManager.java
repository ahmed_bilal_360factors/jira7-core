package com.atlassian.jira.project;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.bc.project.ProjectCreationData;
import com.atlassian.jira.bc.project.ProjectTypeValidator;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.PropertiesManager;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.entity.Delete;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.entity.property.JsonEntityPropertyManager;
import com.atlassian.jira.event.ComponentManagerShutdownEvent;
import com.atlassian.jira.event.project.ProjectAvatarUpdateEvent;
import com.atlassian.jira.event.project.ProjectCategoryChangeEvent;
import com.atlassian.jira.event.project.ProjectCategoryUpdateEvent;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueKey;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.comparator.OfBizComparators;
import com.atlassian.jira.issue.comparator.ProjectNameComparator;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.model.querydsl.QProject;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.ofbiz.PrimitiveMap;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.util.ProjectKeyStore;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.task.context.Contexts;
import com.atlassian.jira.transaction.Transaction;
import com.atlassian.jira.transaction.TransactionSupport;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.ofbiz.core.entity.EntityCondition;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.ofbiz.core.entity.Transformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.regex.Pattern;

import static com.atlassian.jira.project.DefaultProjectManager.Model.ASSIGNEE_TYPE_FIELD;
import static com.atlassian.jira.project.DefaultProjectManager.Model.AVATAR_FIELD;
import static com.atlassian.jira.project.DefaultProjectManager.Model.COUNTER_FIELD;
import static com.atlassian.jira.project.DefaultProjectManager.Model.DESCRIPTION_FIELD;
import static com.atlassian.jira.project.DefaultProjectManager.Model.ENTITY_NAME;
import static com.atlassian.jira.project.DefaultProjectManager.Model.ID_FIELD;
import static com.atlassian.jira.project.DefaultProjectManager.Model.KEY_FIELD;
import static com.atlassian.jira.project.DefaultProjectManager.Model.LEAD_FIELD;
import static com.atlassian.jira.project.DefaultProjectManager.Model.NAME_FIELD;
import static com.atlassian.jira.project.DefaultProjectManager.Model.ORIGINAL_KEY_FIELD;
import static com.atlassian.jira.project.DefaultProjectManager.Model.PROJECT_TYPE_KEY;
import static com.atlassian.jira.project.DefaultProjectManager.Model.URL_FIELD;
import static com.atlassian.jira.project.ProjectRelationConstants.PROJECT_CATEGORY;
import static com.atlassian.jira.project.ProjectRelationConstants.PROJECT_CATEGORY_ASSOC;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.ImmutableSortedMap.copyOf;
import static java.lang.String.CASE_INSENSITIVE_ORDER;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.Validate.validState;
import static org.ofbiz.core.entity.EntityOperator.EQUALS;
import static org.ofbiz.core.entity.EntityUtil.getOnly;
import static com.atlassian.jira.entity.ProjectCategoryFactory.NONE_PROJECT_CATEGORY_ID;

/**
 * A class to manage interactions with projects
 */
@EventComponent
public class DefaultProjectManager extends AbstractProjectManager {
    private static final Logger log = LoggerFactory.getLogger(DefaultProjectManager.class);

    public static final String APPLINKS_LOCAL_PROPERTY_PREFIX = "applinks.local";

    static class Model {
        static final String ENTITY_NAME = "Project";
        // Fields from entitymodel.xml
        static final String ID_FIELD = "id";
        static final String NAME_FIELD = "name";
        static final String URL_FIELD = "url";
        static final String LEAD_FIELD = "lead";
        static final String DESCRIPTION_FIELD = "description";
        static final String KEY_FIELD = "key";
        static final String COUNTER_FIELD = "counter";
        static final String ASSIGNEE_TYPE_FIELD = "assigneetype";
        static final String AVATAR_FIELD = "avatar";
        static final String ORIGINAL_KEY_FIELD = "originalkey";
        static final String PROJECT_TYPE_KEY = "projecttype";
    }

    private final OfBizDelegator delegator;
    private final DbConnectionManager dbConnectionManager;
    private final NodeAssociationStore nodeAssociationStore;
    private final ProjectFactory projectFactory;
    private final ProjectRoleManager projectRoleManager;
    private final IssueManager issueManager;
    private final ProjectCategoryStore projectCategoryStore;
    private final ProjectKeyStore projectKeyStore;
    private final TransactionSupport transactionSupport;
    private final PropertiesManager propertiesManager;
    private final NextIdGenerator nextIdGenerator;
    private final JsonEntityPropertyManager jsonEntityPropertyManager;
    private final EventPublisher eventPublisher;
    private final ProjectTypeValidator projectTypeValidator;
    private final AvatarManager avatarManager;

    public DefaultProjectManager(OfBizDelegator delegator, DbConnectionManager dbConnectionManager,
                                 NodeAssociationStore nodeAssociationStore, ProjectFactory projectFactory, ProjectRoleManager projectRoleManager,
                                 IssueManager issueManager, UserManager userManager,
                                 ProjectCategoryStore projectCategoryStore, ApplicationProperties applicationProperties,
                                 ProjectKeyStore projectKeyStore, TransactionSupport transactionSupport,
                                 PropertiesManager propertiesManager, JsonEntityPropertyManager jsonEntityPropertyManager,
                                 EventPublisher eventPublisher, ProjectTypeValidator projectTypeValidator,
                                 AvatarManager avatarManager) {
        super(userManager, applicationProperties);
        this.delegator = delegator;
        this.dbConnectionManager = dbConnectionManager;
        this.nodeAssociationStore = nodeAssociationStore;
        this.projectFactory = projectFactory;
        this.projectRoleManager = projectRoleManager;
        this.issueManager = issueManager;
        this.projectCategoryStore = projectCategoryStore;
        this.projectKeyStore = projectKeyStore;
        this.transactionSupport = transactionSupport;
        this.propertiesManager = propertiesManager;
        this.jsonEntityPropertyManager = jsonEntityPropertyManager;
        this.nextIdGenerator = new NextIdGenerator(delegator, issueManager);
        this.eventPublisher = eventPublisher;
        this.projectTypeValidator = projectTypeValidator;
        this.avatarManager = avatarManager;
    }

    @Override
    public Project createProject(@Nonnull ApplicationUser user, @Nonnull ProjectCreationData projectCreationData) {
        String key = projectCreationData.getKey();
        String name = projectCreationData.getName();
        ApplicationUser lead = projectCreationData.getLead();
        String url = projectCreationData.getUrl();
        String description = projectCreationData.getDescription();
        Long avatarId = projectCreationData.getAvatarId();
        Long assigneeType = projectCreationData.getAssigneeType();
        ProjectTypeKey projectTypeKey = projectCreationData.getProjectTypeKey();

        notNull(KEY_FIELD, key);
        notNull(NAME_FIELD, name);
        notNull(LEAD_FIELD, lead);
        checkState(projectTypeValidator.isValid(user, projectTypeKey));

        if (avatarId == null) {
            avatarId = avatarManager.getDefaultAvatarId(IconType.PROJECT_ICON_TYPE);
        }

        Transaction transaction = transactionSupport.begin();
        try {
            final Map<String, Object> params = new PrimitiveMap.Builder()
                    .add(KEY_FIELD, key)
                    .add(ORIGINAL_KEY_FIELD, key)
                    .add(NAME_FIELD, name)
                    .add(URL_FIELD, url)
                    .add(LEAD_FIELD, lead.getKey())
                    .add(DESCRIPTION_FIELD, description)
                    .add(COUNTER_FIELD, 0L)
                    .add(ASSIGNEE_TYPE_FIELD, assigneeType)
                    .add(AVATAR_FIELD, avatarId)
                    .add(PROJECT_TYPE_KEY, projectTypeKey != null ? projectTypeKey.getKey() : null)
                    .toMap();
            final GenericValue projectGV = delegator.createValue(ENTITY_NAME, params);

            final Project newProject = new ProjectImpl(projectGV);

            projectKeyStore.addProjectKey(newProject.getId(), newProject.getKey());

            // Give the project role manager a chance to assign the project role default actors to this new project
            projectRoleManager.applyDefaultsRolesToProject(newProject);
            transaction.commit();

            return newProject;
        } finally {
            transaction.finallyRollbackIfNotCommitted();
            // JRA-39710
            //
            // Once the project is created on the DB, but before the transaction is committed, the CachingProjectKeyStore receives a call to addProjectKey,
            // which would internally clean all the values of its cache.
            //
            // If in that moment (when the project key store cache is empty and before the transaction is committed) another thread is scheduled and
            // accesses the CachingProjectKeyStore, the cache would get repopulated. But the new project won't be present in the cache (since the
            // transaction that created it has not been committed yet, and we are executing in a completely different thread that has no visibility over that transaction).
            //
            // Once the transaction is committed under this scenario, the new project would not exist for the CachingProjectKeyStore, until the cache is refreshed again.
            // In order to avoid this, we need to refresh the project key store cache once the transaction is committed, so whomever accesses the cache again it gets the most up to date data.
            projectKeyStore.refresh();
        }
    }

    // Create Methods --------------------------------------------------------------------------------------------------

    @Override
    public long getNextId(final Project project) {
        return nextIdGenerator.getNextId(project);
    }

    @Override
    public Project updateProject(final Project updatedProject, final String name, final String description,
                                 final String leadKey, final String url, final Long assigneeType, Long avatarId, final String projectKey) {
        notNull(NAME_FIELD, name);
        notNull(LEAD_FIELD, leadKey);

        UpdateProjectParameters updateProjectParameters = UpdateProjectParameters.forProject(updatedProject.getId())
                .name(name)
                .description(description)
                .leadUserKey(leadKey)
                .url(url)
                .assigneeType(assigneeType)
                .avatarId(avatarId)
                .key(projectKey);

        return updateProject(updateProjectParameters);
    }

    @Override
    public Project updateProject(UpdateProjectParameters parameters) {
        final Long projectId = parameters.getProjectId();
        final Project project = getProjectObj(projectId);

        notNull(ENTITY_NAME, project);

        ProjectAvatarUpdateEvent projectAvatarUpdateEvent = null;

        Transaction transaction = transactionSupport.begin();
        try {
            // Make a fresh Project GV and only add a subset of fields because we don't want to overwrite "counter"
            final GenericValue projectUpdate = delegator.makeValue(ENTITY_NAME);

            projectUpdate.set(ID_FIELD, projectId);

            final String name = parameters.getName().getOrElse(project.getName());
            projectUpdate.setString(NAME_FIELD, name);

            final String description = parameters.getDescription().getOrElse(project.getDescription());
            projectUpdate.setString(DESCRIPTION_FIELD, description);

            final String url = parameters.getUrl().getOrElse(project.getUrl());
            projectUpdate.setString(URL_FIELD, url);

            String leadKey = project.getLeadUserKey();
            if (parameters.getLeadUserKey().isDefined()) {
                leadKey = parameters.getLeadUserKey().get();
            } else if (parameters.getLeadUsername().isDefined()) {
                leadKey = userManager.getUserByName(parameters.getLeadUsername().get()).getKey();
            }
            projectUpdate.setString(LEAD_FIELD, leadKey);

            final Long assigneeType = parameters.getAssigneeType().getOrElse(project.getAssigneeType());
            projectUpdate.set(ASSIGNEE_TYPE_FIELD, assigneeType);

            if (parameters.getAvatarId().isDefined()) {
                final Long avatarId = parameters.getAvatarId().get();
                projectUpdate.set(AVATAR_FIELD, avatarId);

                if (!avatarId.equals(project.getAvatar().getId())) {
                    projectAvatarUpdateEvent = new ProjectAvatarUpdateEvent(project, avatarId);
                }
            }

            ProjectTypeKey projectTypeKey = project.getProjectTypeKey();
            if (parameters.getProjectTypeKey().isDefined()) {
               projectTypeKey = new ProjectTypeKey(parameters.getProjectTypeKey().get());
            }

            projectUpdate.setString(PROJECT_TYPE_KEY, projectTypeKey.getKey());

            if (parameters.getProjectCategoryId().isDefined()) {
                if (parameters.getProjectCategoryId().get().equals(NONE_PROJECT_CATEGORY_ID)) {
                    setProjectCategory(project, null);
                } else {
                    setProjectCategory(project, getProjectCategory(parameters.getProjectCategoryId().get()));
                }
            }

            if (parameters.getKey().isDefined()) {
                final String newKey = parameters.getKey().get();
                if (!newKey.equals(project.getKey())) {
                    projectUpdate.setString(KEY_FIELD, newKey);
                    final Long projectIdForNewKey = projectKeyStore.getProjectId(newKey);
                    if (projectIdForNewKey == null) {
                        // Record that the new project key is taken
                        projectKeyStore.addProjectKey(projectId, newKey);
                    } else if (!projectIdForNewKey.equals(projectId)) {
                        //this is defensive check against bypassing validation and inconsistent
                        //entries in ProjectKey pointing to non-existent project
                        throw new RuntimeException("Key " + newKey + " already used by project: " + projectIdForNewKey);
                    }
                    updateEntityLinks(project.getKey(), newKey);
                }
            }

            // Store the partial update
            delegator.store(projectUpdate);

            transaction.commit();
        } finally {
            transaction.finallyRollbackIfNotCommitted();
        }

        // JRA-18152: must clear the issue security level cache so that if project lead has changed, user permissions are recalculated
        getIssueSecurityLevelManager().clearUsersLevels();

        if (projectAvatarUpdateEvent != null) {
            eventPublisher.publish(projectAvatarUpdateEvent);
        }

        return getProjectObj(projectId);
    }

    @Override
    public Project updateProjectType(final ApplicationUser user, final Project project, final ProjectTypeKey newProjectType) {
        checkState(projectTypeValidator.isValid(user, newProjectType));

        dbConnectionManager.execute(dbConnection -> {
            dbConnection.update(QProject.PROJECT)
                    .set(QProject.PROJECT.projecttype, newProjectType != null ? newProjectType.getKey() : null)
                    .where(QProject.PROJECT.id.eq(project.getId()))
                    .execute();
        });

        return getProjectObj(project.getId());
    }

    @VisibleForTesting
    void updateEntityLinks(final String oldKey, final String newKey) {
        final Collection<String> oldPropertyKeys = getEntityLinkKeys(oldKey);
        for (final String oldPropertyKey : oldPropertyKeys) {
            final String value = propertiesManager.getPropertySet().getText(oldPropertyKey);
            final String newPropertyKey = oldPropertyKey
                    .replaceFirst("^" + Pattern.quote(prefix(oldKey)), prefix(newKey));
            propertiesManager.getPropertySet().setText(newPropertyKey, value);
            propertiesManager.getPropertySet().remove(oldPropertyKey);
        }
    }

    @SuppressWarnings("unchecked")
    private Collection<String> getEntityLinkKeys(final String oldKey) {
        return propertiesManager.getPropertySet().getKeys(prefix(oldKey));
    }

    private String prefix(final String key) {
        return APPLINKS_LOCAL_PROPERTY_PREFIX + "." + key + ".";
    }


    @Override
    public void removeProjectIssues(final Project project) throws RemoveException {
        removeProjectIssues(project, Contexts.nullContext());
    }

    @Override
    public void removeProjectIssues(final Project project, Context taskContext) throws RemoveException {
        notNull(ENTITY_NAME, project);

        // Because this is a long running operation and we cannot prevent issues arriving while it is in progress
        // we do it twice to be sure to be sure.  (Well almost sure)
        for (int i = 0; i < 2; i++) {
            final Collection<Long> issueIds;
            try {
                issueIds = issueManager.getIssueIdsForProject(project.getId());
                for (final Long issueId : issueIds) {
                    final Issue issue = issueManager.getIssueObject(issueId);
                    Context.Task task = taskContext.start(issue);
                    try {
                        // We have retrieved all issue ids for the project.
                        if (issue != null) {
                            issueManager.deleteIssueNoEvent(issue);
                        } else {
                            log.debug("Issue with id '" + issueId + "' was not found."
                                    + " Most likely it is a sub-task and has been deleted previously with its parent.");
                        }
                    } catch (final Exception e) {
                        log.error("Exception removing issues", e);
                        throw new RemoveException("Error removing issues: " + e, e);
                    } finally {
                        task.complete();
                    }
                }
            } catch (final GenericEntityException e) {
                throw new DataAccessException(e);
            }
        }
    }

    @Override
    public void removeProject(final Project project) {
        notNull(ENTITY_NAME, project);

        // Remove all project role associations for this project from the projectRoleManager
        projectRoleManager.removeAllRoleActorsByProject(project);

        projectKeyStore.deleteProjectKeys(project.getId());

        jsonEntityPropertyManager.deleteByEntity(EntityPropertyType.PROJECT_PROPERTY.getDbEntityName(), project.getId());

        // remove the project itself
        Delete.from(ENTITY_NAME)
                .whereIdEquals(project.getId())
                .execute(delegator);
    }

    protected GenericValue getProject(final Long id) {
        return getDelegator().findById(ENTITY_NAME, id);
    }

    @Override
    public Project getProjectObj(final Long id) {
        Project project = null;
        final GenericValue gv = getProject(id);
        if (gv != null) {
            project = projectFactory.getProject(gv);
        }
        return project;
    }

    protected GenericValue getProjectByName(final String name) {
        return getOnly(getDelegator().findByAnd(ENTITY_NAME, FieldMap.build(NAME_FIELD, name)));
    }

    protected GenericValue getProjectByKey(final String key) {
        final GenericValue gv = getOnly(getDelegator().findByAnd(ENTITY_NAME, FieldMap.build(KEY_FIELD, key)));
        if (gv != null) {
            return gv;
        }
        final Long projectId = projectKeyStore.getProjectId(key);
        return projectId != null ? getProject(projectId) : null;
    }

    @Override
    public Project getProjectByCurrentKey(final String projectKey) {
        final GenericValue gv = getOnly(getDelegator().findByAnd(ENTITY_NAME, FieldMap.build(KEY_FIELD, projectKey)));
        if (gv != null) {
            return projectFactory.getProject(gv);
        }
        return null;
    }

    @Override
    public Project getProjectObjByKey(final String projectKey) {
        Project project = null;
        final GenericValue projectGv = getProjectByKey(projectKey);
        if (projectGv != null) {
            project = projectFactory.getProject(projectGv);
        }
        return project;
    }

    @Override
    public Project getProjectByCurrentKeyIgnoreCase(final String projectKey) {
        Project project = null;
        final GenericValue projectGv = getProjectByKey(projectKey);
        if (projectGv == null) {
            // Try to run through all the projects and compare on the key
            for (Project prj : getProjects()) {
                if (prj.getKey().equalsIgnoreCase(projectKey)) {
                    project = prj;
                    break;
                }
            }
        } else {
            project = projectFactory.getProject(projectGv);
        }
        return project;
    }

    @Override
    public Project getProjectObjByKeyIgnoreCase(final String projectKey) {
        final Map<String, Long> projectKeys = copyOf(projectKeyStore.getAllProjectKeys(), CASE_INSENSITIVE_ORDER);
        final Long projectId = projectKeys.get(projectKey);
        if (projectId != null) {
            final GenericValue projectGv = getProject(projectId);
            return projectGv != null ? projectFactory.getProject(projectGv) : null;
        }
        return null;
    }

    @Override
    public Set<String> getAllProjectKeys(Long projectId) {
        return projectKeyStore.getProjectKeys(projectId);
    }

    @Override
    public Project getProjectObjByName(final String projectName) {
        Project project = null;
        final GenericValue projectGv = getProjectByName(projectName);
        if (projectGv != null) {
            project = projectFactory.getProject(projectGv);
        }
        return project;
    }

    @Nonnull
    @Override
    public List<Project> getProjects() {
        final List<GenericValue> projectGVs = getDelegator().findAll(ENTITY_NAME, singletonList(NAME_FIELD));
        Collections.sort(projectGVs, OfBizComparators.NAME_COMPARATOR); // Fixes JRA-1246
        return projectFactory.getProjects(projectGVs);
    }

    @Nonnull
    @Override
    public List<Project> getProjectObjects() {
        return getProjects();
    }

    @Override
    public long getProjectCount() throws DataAccessException {
        return getDelegator().getCount(ENTITY_NAME);
    }

    // Business Logic Methods ------------------------------------------------------------------------------------------

    protected OfBizDelegator getDelegator() {
        return delegator;
    }

    @Override
    public List<ProjectCategory> getAllProjectCategories() {
        return projectCategoryStore.getAllProjectCategories();
    }

    @Override
    public ProjectCategory getProjectCategory(final Long id) {
        return projectCategoryStore.getProjectCategory(id);
    }

    @Override
    @Nullable
    public ProjectCategory getProjectCategoryObject(final Long id) {
        return getProjectCategory(id);
    }

    @Override
    public void updateProjectCategory(@Nonnull ProjectCategory projectCategory) throws DataAccessException {
        final ProjectCategory oldProjectCategory = projectCategoryStore.getProjectCategory(projectCategory.getId());

        if (!Objects.equal(oldProjectCategory.getName(), projectCategory.getName()) ||
                !Objects.equal(oldProjectCategory.getDescription(), projectCategory.getDescription())) {
            projectCategoryStore.updateProjectCategory(projectCategory);
            eventPublisher.publish(new ProjectCategoryUpdateEvent(oldProjectCategory, projectCategory));
        }
    }

    @Override
    public Collection<Project> getProjectsFromProjectCategory(ProjectCategory projectCategory)
            throws DataAccessException {
        return getProjectObjectsFromProjectCategory(projectCategory.getId());
    }

    @Override
    public Collection<Project> getProjectObjectsFromProjectCategory(final Long projectCategoryId) {
        if (projectCategoryId == null) {
            return Collections.emptyList();
        }

        final List<Long> projectIds = nodeAssociationStore.getSourceIdsFromSink(ProjectRelationConstants.PROJECT_CATEGORY_ASSOC, projectCategoryId);

        List<Project> projects = getProjectsById(projectIds);
        //alphabetic order on the project name
        Collections.sort(projects, ProjectNameComparator.COMPARATOR);
        return projects;
    }

    @Override
    public Collection<Project> getProjectObjectsWithNoCategory() throws DataAccessException {
        final Collection<Project> projects = getProjects();
        final List<Project> result = Lists.newArrayListWithCapacity(projects.size());
        for (final Project project : projects) {
            if (getProjectCategoryForProject(project) == null) {
                result.add(project);
            }
        }

        //alphabetic order on the project name
        Collections.sort(result, ProjectNameComparator.COMPARATOR);
        return result;
    }

    @Override
    @Nullable
    public ProjectCategory getProjectCategoryForProject(final Project project) throws DataAccessException {
        if (project == null) {
            return null;
        }

        final List<GenericValue> projectCats = nodeAssociationStore.getSinksFromSource(
                ENTITY_NAME, project.getId(), "ProjectCategory", PROJECT_CATEGORY);

        if ((null == projectCats) || projectCats.isEmpty()) {
            return null;
        }

        return Entity.PROJECT_CATEGORY.build(projectCats.get(0));
    }

    @Override
    public ProjectCategory createProjectCategory(String name, String description) {
        return projectCategoryStore.createProjectCategory(name, description);
    }

    @Override
    public void removeProjectCategory(Long id) {
        projectCategoryStore.removeProjectCategory(id);
    }

    @Override
    public boolean isProjectCategoryUnique(final String name) {
        //loop through all existing project categories and check that the name is unique
        Collection<ProjectCategory> projectCategories = getAllProjectCategories();
        return projectCategories == null
                || projectCategories.stream()
                .allMatch(
                        projectCategory -> !name.equalsIgnoreCase(projectCategory.getName()));
    }

    @Override
    public void setProjectCategory(final Project project, final ProjectCategory projectCategory) {
        if (project == null) {
            throw new IllegalArgumentException("Cannot associate a category with a null project");
        }

        final ProjectCategory oldProjectCategory = getProjectCategoryForProject(project);

        //when category was not changed, do not perform unnecessary actions
        if (projectCategory != null && oldProjectCategory != null &&
                Objects.equal(projectCategory.getId(), oldProjectCategory.getId())) {
            return;
        }

        final ProjectCategoryChangeEvent.Builder eventBuilder = new ProjectCategoryChangeEvent.Builder(project);

        if (oldProjectCategory != null) {
            nodeAssociationStore.removeAssociation(PROJECT_CATEGORY_ASSOC, project.getId(), oldProjectCategory.getId());
            eventBuilder.addOldCategory(oldProjectCategory);
        }

        if (projectCategory != null) {
            nodeAssociationStore.createAssociation(PROJECT_CATEGORY_ASSOC, project.getId(), projectCategory.getId());
            eventBuilder.addNewCategory(projectCategory);
        }

        if (eventBuilder.canBePublished()) {
            eventPublisher.publish(eventBuilder.build());
        }
    }

    @Override
    public List<Project> getProjectsLeadBy(ApplicationUser leadUser) {
        List<GenericValue> projects = findProjectsByLead(leadUser);
        return projectFactory.getProjects(projects);
    }

    private List<GenericValue> findProjectsByLead(final ApplicationUser leadUser) {
        if (leadUser == null) {
            return Collections.emptyList();
        }
        ApplicationUser leadAppUser = userManager.getUserByName(leadUser.getName());
        if (leadAppUser == null) {
            return Collections.emptyList();
        }
        // ordering by name of project
        return getDelegator().findByAnd(
                ENTITY_NAME, FieldMap.build(LEAD_FIELD, leadAppUser.getKey()), singletonList(NAME_FIELD));
    }

    private List<Project> getProjectsById(final List<Long> projectIds) {
        final List<Project> projects = new ArrayList<Project>(projectIds.size());
        for (Long projectId : projectIds) {
            projects.add(getProjectObj(projectId));
        }
        return projects;
    }

    @Override
    public void refresh() {
    }

    IssueSecurityLevelManager getIssueSecurityLevelManager() {
        return ComponentAccessor.getComponentOfType(IssueSecurityLevelManager.class);
    }

    @Override
    public long getCurrentCounterForProject(Long id) {
        return nextIdGenerator.getCurrentCounterForProject(id);
    }

    @Override
    public void setCurrentCounterForProject(Project project, long counter) {
        nextIdGenerator.resetCounter(project, counter);
    }

    /**
     * Responsible for generating the numerical part of the next Issue key for a given project.
     */
    static class NextIdGenerator {
        private static EntityCondition getProjectIdEqualsCondition(final long projectId) {
            return new EntityExpr(ID_FIELD, EQUALS, projectId);
        }

        private static ExecutorService createSelfCleaningExecutorService() {
            // We create daemon threads so that JIRA can shut down without waiting for these threads to age out
            final ThreadFactory threadFactory = new ThreadFactoryBuilder()
                    .setDaemon(true)
                    .setNameFormat("ProjectCounterUpdateThread-%d")
                    .build();
            return Executors.newCachedThreadPool(threadFactory);
        }

        private static long getNonNullCounter(final GenericValue project) {
            final Long counterObject = project.getLong(COUNTER_FIELD);
            return counterObject == null ? 0 : counterObject;
        }

        private final OfBizDelegator ofBizDelegator;
        private final IssueManager issueManager;
        private final ExecutorService executor;

        NextIdGenerator(final OfBizDelegator delegator, final IssueManager issueManager) {
            this.executor = createSelfCleaningExecutorService();
            this.ofBizDelegator = delegator;
            this.issueManager = issueManager;
        }

        long getNextId(final Project project) {
            if (project == null) {
                throw new IllegalArgumentException();
            }

            final EntityCondition projectIdCondition = getProjectIdEqualsCondition(project.getId());

            try {
                long nextId;
                do {
                    /**
                     * We want the obtaining of an issue ID to happen in a new short-running transaction,
                     * so that any existing transaction doesn't indefinitely lock the row in the Project
                     * table, which can lead to deadlocks between different threads creating issues.
                     *
                     * Unfortunately the simplest way to enforce a new transaction given the current OfBiz
                     * infrastructure is to start a new thread and wait for it to complete.
                     */
                    final Future<Long> issueIdFuture = executor.submit(new Callable<Long>() {
                        @Override
                        public Long call() throws Exception {
                            final GenericValue updatedProject = ofBizDelegator.transformOne(
                                    ENTITY_NAME, projectIdCondition, COUNTER_FIELD, new Transformation() {
                                        @Override
                                        public void transform(final GenericValue entity) {
                                            final long currentCounter = getNonNullCounter(entity);
                                            entity.set(COUNTER_FIELD, currentCounter + 1);
                                        }
                                    });
                            return getNonNullCounter(updatedProject);
                        }
                    });
                    nextId = issueIdFuture.get();
                }
                while (counterAlreadyExists(nextId, project));
                return nextId;
            } catch (final Exception e) {
                throw new RuntimeException(e);
            }
        }

        long getCurrentCounterForProject(final long projectId) {
            final List<GenericValue> gvs = ofBizDelegator.findByCondition(
                    ENTITY_NAME, getProjectIdEqualsCondition(projectId), ImmutableList.of(COUNTER_FIELD));
            validState(gvs.size() <= 1, "Expected at most one Project with ID %d but found these: %s", projectId, gvs);
            return gvs.isEmpty() ? 0L : getNonNullCounter(gvs.get(0));
        }

        /**
         * This is a sanity check to ensure that we are only giving out project keys that haven't already been given out.
         * <p> In an ideal world, this should never return true. </p> Note that this method isn't guaranteed to avoid
         * duplicates, as it will only work if the Issue has already been inserted in the DB.
         *
         * @param incCount the suggested Issue number
         * @param project  The project
         * @return true if this Issue Key already exists in the DB.
         */
        private boolean counterAlreadyExists(final long incCount, final Project project) throws GenericEntityException {
            final String issueKey = IssueKey.format(project, incCount);
            final boolean alreadyExists = issueManager.isExistingIssueKey(issueKey);
            if (alreadyExists) {
                log.warn("Existing issue found for key " + issueKey + ". Incrementing key.");
            }
            return (alreadyExists);
        }

        void resetCounter(final Project project, final long counter) {
            final EntityCondition projectIdCondition = getProjectIdEqualsCondition(project.getId());
            ofBizDelegator.transformOne(ENTITY_NAME, projectIdCondition, COUNTER_FIELD,
                    new Transformation() {
                        @Override
                        public void transform(final GenericValue entity) {
                            entity.set(COUNTER_FIELD, counter);
                        }
                    });
        }

        public void shutdown() {
            executor.shutdownNow();
        }
    }

    @EventListener
    public void shutdown(final ComponentManagerShutdownEvent shutdownEvent) {
        nextIdGenerator.shutdown();
    }
}
