package com.atlassian.jira.issue.search.quicksearch;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.IssueConstant;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public class ResolutionQuickSearchHandler extends SingleWordQuickSearchHandler {
    private static final String UNRESOLVED = "unresolved";
    private static final Map<String, String> UNRESOLVED_RESULT = ImmutableMap.of("resolution", "-1");
    public final ConstantsManager constantsManager;

    public ResolutionQuickSearchHandler(ConstantsManager constantsManager) {
        this.constantsManager = constantsManager;
    }

    public IssueConstant getResolutionsByName(String name) {
        return getIssueConstantByName(constantsManager.getResolutions(), name);
    }

    protected Map handleWord(String word, QuickSearchResult searchResult) {
        if (UNRESOLVED.equalsIgnoreCase(word))
            return UNRESOLVED_RESULT;

        IssueConstant resolutionByName = getResolutionsByName(word);
        return resolutionByName != null ? EasyMap.build("resolution", resolutionByName.getId()) : null;
    }
}
