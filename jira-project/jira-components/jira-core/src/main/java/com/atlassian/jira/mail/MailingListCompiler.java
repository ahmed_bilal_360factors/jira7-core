package com.atlassian.jira.mail;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.LookAndFeelBean;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.event.issue.IssueEventSource;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.worklog.TimeTrackingIssueUpdater;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.mail.builder.EmailBuilder;
import com.atlassian.jira.mail.util.MailAttachmentsManager;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.template.TemplateManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.bean.I18nBean;
import com.atlassian.jira.web.util.OutlookDate;
import com.atlassian.mail.MailThreader;
import com.atlassian.mail.queue.MailQueue;
import com.atlassian.mail.queue.SingleMailQueueItem;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.apache.velocity.exception.VelocityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.BodyPart;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Methods responsible for sending a notification email to a list of {@link NotificationRecipient}s.
 * <p>
 * Notification format (eg. text/html) is taken into account as well as comment security levels.
 */
public class MailingListCompiler {
    private static final Logger log = LoggerFactory.getLogger(MailingListCompiler.class);
    private static final String GETS_COMMENT_AND_ORIGINAL = "commentplusoriginal";
    private static final String GETS_COMMENT_NO_ORIGINAL = "commentnooriginal";
    private static final String NOCOMMENT = "nocomment";

    private final TemplateManager templateManager;
    private final ProjectRoleManager projectRoleManager;
    private final DateTimeFormatterFactory dateTimeFormatterFactory;

    public MailingListCompiler(final TemplateManager templateManager, final ProjectRoleManager projectRoleManager,
                               final DateTimeFormatterFactory dateTimeFormatterFactory) {
        this.templateManager = templateManager;
        this.projectRoleManager = projectRoleManager;
        this.dateTimeFormatterFactory = dateTimeFormatterFactory;
    }

    /**
     * Returns a comma-separated list of the given email addresses.
     *
     * @param addresses email addresses
     * @return the list of addresses.
     * @deprecated just use Joiner.on(",").join(addresses)
     */
    @Deprecated
    public static String getEmailAddresses(final Set<String> addresses) {
        // this appears to be dead code no longer called in production
        return Joiner.on(",").join(addresses);
    }

    /**
     * This function works out where the mail message has originated and then sets up the correct
     * parameters.  It allows comments to be hidden in the email from users that do not have permissions
     * to see them. After all messages are added to the queue
     *
     * @param recipients    Set of {@link NotificationRecipient}s
     * @param sender        sender
     * @param senderFrom    sender from
     * @param templateId    the velocity template ID
     * @param baseUrl       base url
     * @param contextParams map of context parameters
     * @param threader      mail threader
     * @throws VelocityException if notification compiler fails
     *
     * @deprecated use {@link #prepareEmail(Set, String, String, Long, String, Map, MailThreader)}
     * and add emails to desired queue by yourself
     */
    @Deprecated
    public void sendLists(final Set<NotificationRecipient> recipients, final String sender, final String senderFrom,
                          final Long templateId, final String baseUrl, final Map<String, Object> contextParams,
                          final MailThreader threader)
            throws VelocityException {
        prepareEmail(recipients, sender, senderFrom, templateId, baseUrl, contextParams, threader).stream()
                .forEach(getMailQueue()::addItem);
    }

    /**
     * This function works out where the mail message has originated and then sets up the correct
     * parameters.  It allows comments to be hidden in the email from users that do not have permissions
     * to see them.
     *
     * @param recipients    Set of {@link NotificationRecipient}s
     * @param sender        sender
     * @param senderFrom    sender from
     * @param templateId    the velocity template ID
     * @param baseUrl       base url
     * @param contextParams map of context parameters
     * @param threader      mail threader
     * @return Collection of prepared {@link SingleMailQueueItem}s ready to be sent or added to a queue
     * @throws VelocityException if notification compiler fails
     *
     * @since v7.1.5
     */
    public Collection<SingleMailQueueItem> prepareEmail(final Set<NotificationRecipient> recipients, final String sender,
                                                        final String senderFrom, final Long templateId, final String baseUrl,
                                                        final Map<String, Object> contextParams, final MailThreader threader)
            throws VelocityException {
        final NotificationCompiler compiler =
                new NotificationCompiler(recipients, sender, senderFrom, templateId, contextParams, threader);

        final Object eventParams = contextParams.get("params");
        if (eventParams != null) {
            final Object commentObject = contextParams.get("comment");
            final Object worklogObject = contextParams.get("worklog");

            if (worklogObject != null) {
                final Worklog worklog = (Worklog) worklogObject;
                final ProjectRole roleLevel = worklog.getRoleLevel();
                final String roleLevelName = roleLevel == null ? null : roleLevel.getName();

                final Worklog originalWorklog = (Worklog) ((Map) eventParams).get(TimeTrackingIssueUpdater.EVENT_ORIGINAL_WORKLOG_PARAMETER);

                String originalRoleLevelName = null;
                String originalGroupLevel = null;

                if (originalWorklog != null) {
                    final ProjectRole originalRoleLevel = originalWorklog.getRoleLevel();
                    originalRoleLevelName = originalRoleLevel == null ? null : originalRoleLevel.getName();
                    originalGroupLevel = originalWorklog.getGroupLevel();
                }

                return compiler.evaluateForEvent((Map) eventParams, worklog.getGroupLevel(), roleLevelName, originalGroupLevel, originalRoleLevelName);
            } else if (commentObject != null) {
                final Comment comment = (Comment) commentObject;

                final Comment originalComment = (Comment) ((Map) eventParams).get(CommentManager.EVENT_ORIGINAL_COMMENT_PARAMETER);

                String originalRoleLevelName = null;
                String originalGroupLevel = null;

                if (originalComment != null) {
                    final ProjectRole originalRoleLevel = originalComment.getRoleLevel();
                    originalRoleLevelName = originalRoleLevel == null ? null : originalRoleLevel.getName();
                    originalGroupLevel = originalComment.getGroupLevel();
                }

                final ProjectRole roleLevel = comment.getRoleLevel();
                final String roleLevelName = roleLevel == null ? null : roleLevel.getName();
                return compiler.evaluateForEvent((Map) eventParams, comment.getGroupLevel(), roleLevelName, originalGroupLevel, originalRoleLevelName);
            } else {
                log.debug("The event does not have a comment or a worklog. Sending to all recipients.");
                return compiler.evaluateForAll();
            }
        } else {
            log.debug("Do not have any context params. Sending to all recipients.");
            return compiler.evaluateForAll();
        }
    }

    /**
     * Group users by whether they get a comment, and format, eg:
     * { "nocomment": { "text": [joe@company.com, fred@company.com],
     * "html" : [john@company.com],
     * "custom" : [foo@company.com],
     * "comment" : { "text" : [bob@foo.com],
     * "html" : [joe@bar.com] }
     * }
     *
     * @param recipients          set of recipients
     * @param group               notification group level
     * @param role                notification role level
     * @param originalGroup       original group level
     * @param originalRole        original role level
     * @param issue               issue
     * @param sendOnlyWhenInGroup flag to override visibility permission
     * @return a map of recipient emails by type
     */
    private Map<String, Map<String, Set<NotificationRecipient>>> groupEmailsByType(
            final Set<NotificationRecipient> recipients,
            final String group,
            final String role,
            final String originalGroup,
            final String originalRole,
            final TemplateIssue issue,
            final boolean sendOnlyWhenInGroup) {
        final Map<String, Map<String, Set<NotificationRecipient>>> recipientEmailsByType = new HashMap<>();
        final Map<String, Set<NotificationRecipient>> usersGettingCommentAndOriginal = new HashMap<>();
        final Map<String, Set<NotificationRecipient>> usersGettingCommentNoOriginal = new HashMap<>();
        final Map<String, Set<NotificationRecipient>> usersNotGettingComment = new HashMap<>();
        recipientEmailsByType.put(GETS_COMMENT_AND_ORIGINAL, usersGettingCommentAndOriginal);
        recipientEmailsByType.put(GETS_COMMENT_NO_ORIGINAL, usersGettingCommentNoOriginal);
        recipientEmailsByType.put(NOCOMMENT, usersNotGettingComment);

        for (final NotificationRecipient recipient : recipients) {
            final Map<String, Set<NotificationRecipient>> currentGroup;
            if (recipientHasVisibility(group, role, recipient, issue)) {
                if (recipientHasVisibility(originalGroup, originalRole, recipient, issue)) {
                    currentGroup = usersGettingCommentAndOriginal;
                } else {
                    currentGroup = usersGettingCommentNoOriginal;
                }
            } else if (!sendOnlyWhenInGroup) {
                // Not in the comment-level group, but it doesn't matter; send mail without comments
                currentGroup = usersNotGettingComment;
            } else {
                // Not in group and user has to be; ditch user
                continue;
            }
            final String format = recipient.getFormat();
            Set<NotificationRecipient> currentUsers = currentGroup.get(format);
            if (currentUsers == null) {
                currentUsers = new HashSet<>();
                currentGroup.put(format, currentUsers);
            }
            currentUsers.add(recipient);
        }
        return recipientEmailsByType;
    }

    private boolean recipientHasVisibility(final String group, final String role, final NotificationRecipient recipient,
                                           final TemplateIssue issue) {
        return (group == null && role == null) ||
                (group != null && recipient.isInGroup(group)) ||
                (role != null && issue != null && isInRole(recipient, issue.getProjectObject(), role));
    }

    private boolean isInRole(final NotificationRecipient recipient, final Project project, final String role) {
        final ApplicationUser user = recipient.getUserRecipient();
        if (user == null) {
            // If we do not have a user then he/she cannot be part of a role. Return false
            return false;
        }

        // Retrieve a role by its name
        final ProjectRole projectRole = projectRoleManager.getProjectRole(role);
        // Return false if we could not find a role or if the user is not part of the role
        return projectRole != null && projectRoleManager.isUserInProjectRole(user, projectRole, project);
    }

    private Collection<SingleMailQueueItem> evaluateEmails(final Set<NotificationRecipient> textRecipients, final Long templateId, final String format,
                                      final Map<String, Object> contextParams, final String sender, final String senderFrom,
                                      final MailThreader threader) throws VelocityException {
        final String subjectTemplate = templateManager.getTemplateContent(templateId, "subject");
        final String bodyTemplate = templateManager.getTemplateContent(templateId, format);

        if (!textRecipients.isEmpty()) {
            final NotificationRecipientProcessor processor = getNotificationRecipientProcessor(textRecipients,
                    contextParams, sender, senderFrom, threader, subjectTemplate, bodyTemplate);
            return processor.evaluateEmails();
        }
        return Collections.emptyList();
    }

    private NotificationRecipientProcessor getNotificationRecipientProcessor(
            final Set<NotificationRecipient> textRecipients,
            final Map<String, Object> contextParams,
            final String sender,
            final String senderFrom,
            final MailThreader threader,
            final String subjectTemplate,
            final String bodyTemplate) {

        return new NotificationRecipientProcessor(textRecipients) {

            @Override
            Optional<SingleMailQueueItem> evaluateEmailForRecipient(final NotificationRecipient recipient) throws Exception {
                final ApplicationUser recipientUser = recipient.getUserRecipient();

                final String recipientEmail = recipient.getEmail();
                // skip user with no e-mail address - http://jira.atlassian.com/browse/JRA-13558
                if (recipientEmail == null || recipientEmail.length() == 0) {
                    if (recipientUser == null) { // case of e-mail address only
                        log.warn("Can not send e-mail, e-mail address ('" + recipientEmail + "') not set. Skipping...");
                    } else {
                        final String fullName = recipientUser.getDisplayName();
                        final String userName = recipientUser.getName();
                        log.warn("Can not send e-mail to '" + fullName + "' [" + userName + "],"
                                + " e-mail address ('" + recipientEmail + "') not set. Skipping...");
                    }
                    return Optional.empty();
                } else {
                    // Pass the i18nHelper to the template - allows the email notification to be displayed in the language of the recipient
                    // Specify the translation file as an additional resource
                    final I18nHelper i18nBean = new I18nBean(recipientUser);
                    contextParams.put("i18n", i18nBean);
                    final IssueTemplateContext templateContext = (IssueTemplateContext) contextParams.get("context");

                    if (templateContext != null) {
                        contextParams.put("eventTypeName", templateContext.getEventTypeName(i18nBean));
                    }

                    // Provide an OutlookDate formatter with the users locale
                    final OutlookDate formatter = new OutlookDate(i18nBean.getLocale());
                    contextParams.put("dateformatter", formatter);
                    contextParams.put("datetimeformatter", dateTimeFormatterFactory.formatter()
                            .withLocale(i18nBean.getLocale()));
                    contextParams.put("lfbean", LookAndFeelBean.getInstance(ComponentAccessor.getComponent(ApplicationProperties.class)));

                    // Place the recipient in the velocity context
                    if (recipientUser != null) {
                        contextParams.put("recipient", recipientUser);
                    }

                    final Email email = new Email(recipient.getEmail());
                    email.setFrom(sender);
                    email.setFromName(senderFrom);

                    final Optional<MailAttachmentsManager> attachmentsManager = Optional.ofNullable((MailAttachmentsManager) contextParams.get("attachmentsManager"));
                    final Iterable<BodyPart> attachmentsBodyParts = attachmentsManager
                            .map(MailAttachmentsManager::buildAttachmentsBodyParts)
                            .orElse(Lists.newArrayList());

                    final SingleMailQueueItem item = new EmailBuilder(email, recipient)
                            .addAttachments(Lists.newArrayList(attachmentsBodyParts))
                            .withSubject(subjectTemplate)
                            .withBody(bodyTemplate)
                            .addParameters(contextParams)
                            .renderNowAsQueueItem();

                    item.setMailThreader(threader);
                    return Optional.of(item);
                }
            }

            @Override
            void handleException(final NotificationRecipient recipient, final Exception ex) {
                if (recipient == null) {
                    log.error("Failed adding mail, notification recipient was null", ex);
                } else {
                    log.error("Failed adding mail for notification recipient: [email=" + recipient.getEmail()
                            + ", user=" + recipient.getUserRecipient() + ']', ex);
                }
            }

        };
    }

    @VisibleForTesting
    MailQueue getMailQueue() {
        return ComponentAccessor.getMailQueue();
    }

    /**
     * Helper inner class.
     */
    private class NotificationCompiler {
        private final Set<NotificationRecipient> recipients;
        private final String sender;
        private final String senderFrom;
        private final Long templateId;
        private final Map<String, Object> contextParams;
        private final MailThreader threader;

        public NotificationCompiler(final Set<NotificationRecipient> recipients, final String sender, final String senderFrom,
                                    final Long templateId, final Map<String, Object> contextParams,
                                    final MailThreader threader) {
            this.recipients = recipients;
            this.sender = sender;
            this.senderFrom = senderFrom;
            this.templateId = templateId;
            this.contextParams = contextParams;
            this.threader = threader;
        }

        public Collection<SingleMailQueueItem> evaluateForEvent(final Map eventParams, final String groupLevel,
                                                                final String roleLevel, final String originalGroupLevel,
                                                                final String originalRoleLevel)
                throws VelocityException {
            final Object eventSource = eventParams.get("eventsource");
            //Check to see if the map has a value of where the event came from Workflow or Action
            if (eventSource != null) {
                //Check to see if the comment has a level
                log.debug("Do have an event source so from a comment or workflow");
                //Check what the event source was
                if (IssueEventSource.ACTION.equals(eventSource)) {
                    log.debug("Event source is action");
                    return sendLists(groupLevel, roleLevel, originalGroupLevel, originalRoleLevel, true);
                } else if (IssueEventSource.WORKFLOW.equals(eventSource)) {
                    log.debug("Event source is workflow");
                    return sendLists(groupLevel, roleLevel, originalGroupLevel, originalRoleLevel, false);
                } else {
                    log.debug("Event source is unknown, this should not happen");
                    return sendNoLevelsIgnoreGroup();
                }
            } else {
                log.debug("No event source so must be from a subscription");
                return sendNoLevelsIgnoreGroup();
            }
        }

        public Collection<SingleMailQueueItem> evaluateForAll() throws VelocityException {
            return sendNoLevelsIgnoreGroup();
        }

        private Collection<SingleMailQueueItem> sendNoLevelsIgnoreGroup() throws VelocityException {
            return sendLists(null, null, null, null, false);
        }

        /**
         * Analyzes recipients and evaluates messages for them.
         *
         * @param groupLevel          group notification level for comments or worklogs, null otherwise
         * @param roleLevel           role notification level for comments or worklogs, null otherwise
         * @param originalGroupLevel  original notification group level
         * @param originalRoleLevel   original notification role level
         * @param sendOnlyWhenInGroup flag that indicates whether the users should get the notification if they are not in 'groupLevel'
         * @return Collection of prepared {@link SingleMailQueueItem}s ready to be sent or added to a queue
         * @throws VelocityException if fails to add emails to the queue
         */
        private Collection<SingleMailQueueItem> sendLists(final String groupLevel, final String roleLevel,
                                                          final String originalGroupLevel, final String originalRoleLevel,
                                                          final boolean sendOnlyWhenInGroup)
                throws VelocityException {
            final Collection<SingleMailQueueItem> result = new ArrayList<>();

            // Note that issue can be null if a subscription is being sent
            final TemplateIssue issue = (TemplateIssue) contextParams.get("issue");
            final Map<String, Map<String, Set<NotificationRecipient>>> recipientEmailsByType = groupEmailsByType(recipients, groupLevel, roleLevel, originalGroupLevel, originalRoleLevel, issue, sendOnlyWhenInGroup);

            result.addAll(addEmailsToQueue(recipientEmailsByType.get(GETS_COMMENT_AND_ORIGINAL)));

            // Remove the original comment for those who do not have the permission to see it
            // Showing the comment in the templates depends on the presence of this key in the context
            contextParams.remove("originalcomment");
            // Remove the original worklog for those who do not have the permission to see it
            contextParams.remove("originalworklog");
            result.addAll(addEmailsToQueue(recipientEmailsByType.get(GETS_COMMENT_NO_ORIGINAL)));

            // Remove the comment from the context for those who do not have the permission to see it
            // Showing the comment in the templates depends on the presence of this key in the context
            contextParams.remove("comment");
            // NOTE: we do not need to remove the worklog from the context because it is always invoked with
            // sendOnlyWhenInGroup as true which means that in the case of the user not being able to see the
            // worklog we will not send any mail.
            result.addAll(addEmailsToQueue(recipientEmailsByType.get(NOCOMMENT)));

            return result;
        }

        /**
         * Gets the user sets by each format and evaluates messages for them.
         *
         * @param userSets a map that maps format[String] to users[Set]
         * @throws VelocityException if fails to add emails to the queue
         */
        private Collection<SingleMailQueueItem> addEmailsToQueue(final Map<String, Set<NotificationRecipient>> userSets) throws VelocityException {
            return userSets.entrySet().stream()
                    .map(entry -> evaluateEmails(entry.getValue(), templateId, entry.getKey(), contextParams, sender, senderFrom, threader))
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());
        }
    }
}
