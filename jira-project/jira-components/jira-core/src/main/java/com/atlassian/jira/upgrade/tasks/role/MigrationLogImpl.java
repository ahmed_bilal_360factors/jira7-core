package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.util.dbc.Assertions;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;

public class MigrationLogImpl implements MigrationLog {
    private final List<AuditEntry> events;

    public MigrationLogImpl() {
        events = ImmutableList.of();
    }

    public MigrationLogImpl(@Nonnull List<AuditEntry> events, @Nonnull AuditEntry event) {
        Assertions.notNull("event", event);
        ImmutableList.Builder<AuditEntry> builder = ImmutableList.builder();
        this.events = builder.addAll(events).add(event).build();
    }

    @Nonnull
    public Collection<AuditEntry> events() {
        return events;
    }

    @Override
    public MigrationLog log(@Nonnull AuditEntry event) {
        return new MigrationLogImpl(events, event);
    }

    @Override
    public String toString() {
        return "MigrationLogImpl{" +
                "events=" + events +
                '}';
    }
}
