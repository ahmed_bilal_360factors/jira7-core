package com.atlassian.jira.servermetrics;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.instrumentation.operations.OpSnapshot;
import com.atlassian.instrumentation.operations.OpTimerFactory;
import com.atlassian.jira.instrumentation.Instrumentation;
import com.atlassian.jira.instrumentation.InstrumentationName;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.ThreadSafe;
import javax.servlet.http.HttpServletRequest;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@ParametersAreNonnullByDefault
@ThreadSafe
public class RequestMetricsDispatcher {
    private static final String SERVER_RENDER_END_CHECKPOINT = RequestCheckpoints.serverRenderEnd.name();

    private final Set<String> DISABLED_SERVLET_PREFIXES = ImmutableSet.of(
            "/s/", "/rest/", "/images/", "/download/"
    );

    private final MultiThreadedRequestMetricsCollector requestPartitioning;
    private final EventPublisher eventPublisher;
    private final TimingInformationToEvent timingInformationToEvent;
    private final MultiThreadedRequestKeyResolver requestKeyResolver;
    private final OpTimerFactory instrumentRegistry;


    public RequestMetricsDispatcher(MultiThreadedRequestMetricsCollector requestPartitioning,
                                    EventPublisher eventPublisher,
                                    TimingInformationToEvent timingInformationToEvent,
                                    MultiThreadedRequestKeyResolver requestKeyResolver,
                                    OpTimerFactory instrumentRegistry) {
        this.requestPartitioning = requestPartitioning;
        this.eventPublisher = eventPublisher;
        this.timingInformationToEvent = timingInformationToEvent;
        this.requestKeyResolver = requestKeyResolver;
        this.instrumentRegistry = instrumentRegistry;
    }

    @Nonnull
    public CollectorHandle startCollection(HttpServletRequest httpServletRequest) {
        final String requestPath = getRequestPath(httpServletRequest);

        if (isRequestPathAllowedForCollection(requestPath)) {
            requestKeyResolver.requestStarted(httpServletRequest);
            requestPartitioning.startCollectionInCurrentThread();
        }

        return new CollectorHandle(httpServletRequest);
    }

    public void stopCollection(HttpServletRequest httpServletRequest) {
        final String requestPath = getRequestPath(httpServletRequest);

        if ( isRequestPathAllowedForCollection(requestPath)) {
            requestPartitioning.checkpointReachedOverride(SERVER_RENDER_END_CHECKPOINT);
            fillSystemMetrics(requestPartitioning);

            final Optional<TimingInformation> requestTimingStats = requestPartitioning.finishCollectionInCurrentThread();
            final Optional<String> requestKey = requestKeyResolver.requestFinished(httpServletRequest);

            requestTimingStats
                    .map(timingInformation -> timingInformationToEvent.createStatEvent(
                            timingInformation,
                            httpServletRequest,
                            requestKey))
                    .ifPresent(eventPublisher::publish);
        }
    }

    static String getRequestPath(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getServletPath() + (httpServletRequest.getPathInfo() != null ? ("/" + httpServletRequest.getPathInfo()) : "");
    }

    private boolean isRequestPathAllowedForCollection(String servletPath) {
        return !DISABLED_SERVLET_PREFIXES.stream()
                .anyMatch(servletPath::startsWith);
    }

    private static final Map<String, String> RECORDED_INSTRUMENTS_2_ACTIVITY_NAMES = ImmutableMap.<String, String>builder()
            .put(InstrumentationName.DB_READS.getInstrumentName(), RequestActivities.dbRead.name())
            .put(InstrumentationName.DB_CONNECTIONS.getInstrumentName(), RequestActivities.dbConnected.name())
            .build();

    private void fillSystemMetrics(MultiThreadedRequestMetricsCollector collector) {
        final List<OpSnapshot> opSnapshots = Instrumentation.snapshotThreadLocalOperations(instrumentRegistry);
        opSnapshots.stream()
                .filter(opSnapshot -> RECORDED_INSTRUMENTS_2_ACTIVITY_NAMES.containsKey(opSnapshot.getName()))
                .limit(RECORDED_INSTRUMENTS_2_ACTIVITY_NAMES.size())
                .forEach(opSnapshot ->
                        collector.setTimeSpentInActivity(
                                RECORDED_INSTRUMENTS_2_ACTIVITY_NAMES.get(opSnapshot.getName()),
                                Duration.ofNanos(opSnapshot.getElapsedTotalTime(TimeUnit.NANOSECONDS)))
                );
    }

    public class CollectorHandle implements AutoCloseable {
        private final HttpServletRequest httpServletRequest;

        CollectorHandle(HttpServletRequest httpServletRequest) {
            this.httpServletRequest = httpServletRequest;
        }

        @Override
        public void close() {
            stopCollection(httpServletRequest);
        }
    }
}
