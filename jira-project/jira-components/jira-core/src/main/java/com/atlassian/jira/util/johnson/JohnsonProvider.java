package com.atlassian.jira.util.johnson;

import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.config.JohnsonConfig;

/**
 * A wrapper around Johnson static methods to allow easier mocking in tests.
 *
 * @since 7.0
 */
public interface JohnsonProvider {
    /**
     * Retrieves the container for the current ServletContext, as defined by ServletActionContext.getServletContext().
     * This is equivalent to calling getContainer(ServletContextProvider.getServletContext()) but saves an import, a
     * null check on the servlet context and a static call to help avoid testing problems.
     */
    JohnsonEventContainer getContainer();

    /**
     * Retrieves the config for the current ServletContext, as defined by ServletActionContext.getServletContext(). This
     * is equivalent to calling getConfig(ServletContextProvider.getServletContext()) but saves an import, a null check
     * on the servlet context and a static call to help avoid testing problems.
     */
    JohnsonConfig getConfig();
}
