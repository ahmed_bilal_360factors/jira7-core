package com.atlassian.jira.project.type;

import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.base.Supplier;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Stream.concat;

/**
 * Default implementation for {@link BrowseProjectTypeManager}
 */
public class BrowseProjectTypeManagerImpl implements BrowseProjectTypeManager {
    private final ProjectTypeManager projectTypeManager;
    private final PermissionManager permissionManager;
    private static final String BUSINESS = "business";

    public BrowseProjectTypeManagerImpl(ProjectTypeManager projectTypeManager, PermissionManager permissionManager) {
        this.projectTypeManager = projectTypeManager;
        this.permissionManager = permissionManager;
    }

    @Override
    public List<ProjectType> getAllProjectTypes(ApplicationUser user) {
        return getAllProjectTypesFor(permissionManager.getProjects(ProjectPermissions.BROWSE_PROJECTS, user));
    }

    @Override
    public boolean isProjectTypeChangeAllowed(Project project) {
        List<Project> projects = newArrayList(project);

        return getAllProjectTypesFor(projects).size() > 1;
    }

    private List<ProjectType> getAllProjectTypesFor(Collection<Project> projects) {
        Stream<ProjectType> installedProjectTypes = getProjectTypes(projectTypeManager.getAllAccessibleProjectTypes());
        Stream<ProjectType> projectTypesOfProjects = getProjectTypes(projects);
        ProjectType businessProjectType = getProjectTypeFor(new ProjectTypeKey(BUSINESS));

        return concat(installedProjectTypes, concat(projectTypesOfProjects, Stream.of(businessProjectType)))
                .distinct()
                .sorted(Comparator.comparingInt(ProjectType::getWeight))
                .collect(toImmutableList());
    }

    private Stream<ProjectType> getProjectTypes(Collection<Project> projects) {
        return getUniqueProjectTypes(projects.stream().map(Project::getProjectTypeKey));
    }

    private Stream<ProjectType> getProjectTypes(List<ProjectType> projectTypes) {
        return getUniqueProjectTypes(projectTypes.stream().map(ProjectType::getKey));
    }

    private Stream<ProjectType> getUniqueProjectTypes(Stream<ProjectTypeKey> projectTypeKeySet) {
        return projectTypeKeySet.distinct().map(this::getProjectTypeFor);
    }

    private ProjectType getProjectTypeFor(ProjectTypeKey key) {
        return projectTypeManager.getByKey(key).getOrElse((Supplier) () -> getInaccessibleProjectType(key));
    }

    private ProjectType getInaccessibleProjectType(ProjectTypeKey projectTypeKey) {
        ProjectType inaccessibleProjectType = projectTypeManager.getInaccessibleProjectType();
        return new ProjectType(
                projectTypeKey,
                inaccessibleProjectType.getDescriptionI18nKey(),
                inaccessibleProjectType.getIcon(),
                inaccessibleProjectType.getColor(),
                inaccessibleProjectType.getWeight()
        );
    }
}
