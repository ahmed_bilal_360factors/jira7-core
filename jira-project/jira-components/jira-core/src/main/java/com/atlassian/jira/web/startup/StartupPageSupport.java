package com.atlassian.jira.web.startup;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.web.action.RedirectSanitiser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import static com.atlassian.jira.web.filters.johnson.JiraJohnsonFilter.getServletPathFromRequest;
import static java.util.function.Function.identity;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Static infrastructure related to the startup page, including filtering, constants, redirects, etc.
 * <p>
 * The startup page is delivered to the end user if any HTTP requests are received during the bootstrap process.
 * </p>
 *
 * @see StartupServlet
 * @see com.atlassian.jira.startup.LauncherContextListener
 * @see com.atlassian.jira.web.filters.StartupProgressFilter
 * @since v7.1.0
 */
public class StartupPageSupport {
    private static final Logger LOG = LoggerFactory.getLogger(StartupPageSupport.class);
    private static final String RETURN_TO = "returnTo";
    private static final String STARTUP_JSP = "/startup.jsp";

    private static final Map<Locale, Function<String, String>> TRANSLATIONS = new ConcurrentHashMap<>(4);

    private static volatile boolean launched = false;

    /**
     * Returns {@code true} if JIRA's servlet context is still initializing
     *
     * @return {@code true} if JIRA's servlet context is still initializing
     */
    public static boolean isLaunched() {
        return launched;
    }

    /**
     * Used to mark JIRA as fully launched
     */
    public static void setLaunched(boolean launched) {
        StartupPageSupport.launched = launched;
    }

    /**
     * Returns {@code true} if the provided request targets the startup page.
     *
     * @param request the current request
     * @return {@code true} if the provided request targets the startup page; {@code false} if it is anything else.
     */
    public static boolean isStartupPage(ServletRequest request) {
        return isStartupPage((HttpServletRequest) request);
    }

    /**
     * Returns {@code true} if the provided request targets the startup page.
     *
     * @param request the current request
     * @return {@code true} if the provided request targets the startup page; {@code false} if it is anything else.
     */
    public static boolean isStartupPage(HttpServletRequest request) {
        return isStartupPage(getServletPathFromRequest(request));
    }

    /**
     * Returns {@code true} if the provided servlet path targets the startup page.
     *
     * @param servletPath the servlet path for the current request
     * @return {@code true} if the provided servlet path targets the startup page; {@code false} if it is anything else.
     */
    static boolean isStartupPage(String servletPath) {
        return STARTUP_JSP.equals(servletPath);
    }

    /**
     * Redirects from the current page to {@code startup.jsp}.
     *
     * @param request  the current HTTP request
     * @param response the current HTTP response
     * @throws IOException if something goes wrong when sending the redirect (probably because the client disconnected)
     */
    public static void redirectToStartupPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final String contextPath = request.getContextPath();
        final String servletPath = getServletPathFromRequest(request);
        if (isStartupPage(servletPath)) {
            throw new IllegalStateException("Redirected to startup.jsp when already there?!");
        }

        LOG.debug("Redirecting request from '{}' to '{}'", servletPath, STARTUP_JSP);
        final String url = makeStartupPageUrl(contextPath, servletPath, request.getQueryString());
        response.sendRedirect(url);
    }

    private static String makeStartupPageUrl(final String contextPath, final String servletPath, final String query) {
        final String base = StringUtils.isBlank(contextPath) ? "/" : contextPath;
        final UrlBuilder url = new UrlBuilder(base).addPaths(STARTUP_JSP);
        final StringBuilder sb = new StringBuilder(64);
        if (isNotBlank(servletPath) && !isStartupPage(servletPath)) {
            sb.append(servletPath);
        }
        if (isNotBlank(query)) {
            sb.append('?').append(query);
        }
        if (sb.length() > 0) {
            url.addParameter(RETURN_TO, sb.toString());
        }
        return url.asUrlString();
    }

    /**
     * Redirects from {@code startup.jsp} to the original page.
     *
     * @param request  the current HTTP request
     * @param response the current HTTP response
     * @throws IOException if something goes wrong when sending the redirect (probably because the client disconnected)
     */
    public static void returnFromStartupJsp(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(sanitize(request.getContextPath(), request.getParameter(RETURN_TO)));
    }

    /**
     * Sanitizes a proposed redirect request.
     * <p>
     * This filtering is to prevent an <a href="https://www.owasp.org/index.php/Open_redirect">open redirect</a>,
     * which would facilitate phishing attacks.  If we don't like the proposed target, then we just send them home.
     * </p>
     *
     * @param contextPath the application's context path
     * @param returnTo    the proposed redirect target
     * @return the sanitized redirect target, which will be the target path if {@code returnTo} is not trusted
     */
    private static String sanitize(final String contextPath, @Nullable final String returnTo) {
        // Substitute '/' for an otherwise blank redirect to prevent a redirect loop
        final String home = StringUtils.isBlank(contextPath) ? "/" : contextPath;
        if (StringUtils.isBlank(returnTo)) {
            return home;
        }
        return ComponentAccessor.getComponentSafely(RedirectSanitiser.class)
                .map(rs -> rs.makeSafeRedirectUrl(contextPath + returnTo))
                .orElse(home);
    }

    /**
     * Obtains a simple translation function for mapping message keys to translated text.
     * <p>
     * The locale is taken from the current request instead of the normal JIRA pattern of using
     * a setting from the user's preferences or application properties because during startup
     * those kinds of things may or may not be available, and changing what language is displayed
     * for the same user in the same web browser would be very surprising, especially since the
     * use of AJAX means that the same page would end up displaying more than one language at
     * a time.  It seems safest to just guess the locale based on the {@code Accept-Language}
     * header from the user's browser, instead.
     * </p><p>
     * Note: These translations can only be provided by the special {@code StartupPageSupport}
     * properties files and cannot be added to or altered by plugins (including language packs).
     * What we build into JIRA is all you get.
     * </p>
     *
     * @param request the servlet request that wants a translated string
     * @return the translated string
     */
    public static Function<String, String> getTranslator(HttpServletRequest request) {
        return TRANSLATIONS.computeIfAbsent(request.getLocale(), StartupPageSupport::createTranslator);
    }

    private static Function<String, String> createTranslator(Locale locale) {
        try {
            final ResourceBundle bundle = ResourceBundle.getBundle(StartupPageSupport.class.getName(), locale,
                    StartupPageSupport.class.getClassLoader());
            return msgKey ->
            {
                try {
                    return bundle.getString(msgKey);
                } catch (RuntimeException re) {
                    return msgKey;
                }
            };
        } catch (RuntimeException re) {
            LOG.debug("Unable to create a translator for locale {}", locale);
            return identity();
        }
    }
}
