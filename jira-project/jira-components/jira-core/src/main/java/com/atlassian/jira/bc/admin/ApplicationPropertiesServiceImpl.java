package com.atlassian.jira.bc.admin;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.ApplicationPropertiesStore;
import com.atlassian.jira.event.config.ApplicationPropertyChangeEvent;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.validation.Validated;
import com.atlassian.validation.Validator;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.atlassian.jira.event.config.ApplicationPropertyChangeEvent.KEY_METADATA;

public class ApplicationPropertiesServiceImpl implements ApplicationPropertiesService {


    private static final Logger log = LoggerFactory.getLogger(ApplicationPropertiesServiceImpl.class);

    private final ApplicationPropertiesStore applicationPropertiesStore;
    private EventPublisher eventPublisher;
    private PermissionManager permissionManager;
    private JiraAuthenticationContext authenticationContext;
    private final Predicate<ApplicationProperty> featurePredicate;
    private FeatureManager featureManager;

    public ApplicationPropertiesServiceImpl(ApplicationPropertiesStore applicationPropertiesStore, EventPublisher eventPublisher, PermissionManager permissionManager, JiraAuthenticationContext authenticationContext, FeatureManager featureManager) {
        this.applicationPropertiesStore = applicationPropertiesStore;
        this.eventPublisher = eventPublisher;
        this.permissionManager = permissionManager;
        this.authenticationContext = authenticationContext;
        this.featureManager = featureManager;
        this.featurePredicate = input -> {
            if (input.getMetadata().getRequiredFeatureKey() == null) {
                return true;
            }
            if (input.getMetadata().getRequiredFeatureKey().second()) {
                return ApplicationPropertiesServiceImpl.this.featureManager.isEnabled(input.getMetadata().getRequiredFeatureKey().first());
            } else {
                return !ApplicationPropertiesServiceImpl.this.featureManager.isEnabled(input.getMetadata().getRequiredFeatureKey().first());
            }
        };
    }

    @Override
    public List<ApplicationProperty> getEditableApplicationProperties(String permissionLevel, String keyFilter) {
        if (permissionLevel == null || permissionLevel.isEmpty()) {
            if (isSysAdmin()) {
                return getEditableApplicationProperties(EditPermissionLevel.SYSADMIN, keyFilter);
            } else if (isNormalAdmin()) {
                return getEditableApplicationProperties(EditPermissionLevel.ADMIN, keyFilter);
            }
            return null;
        }
        EditPermissionLevel editPermission;
        try {
            editPermission = EditPermissionLevel.valueOf(permissionLevel.toUpperCase());
        } catch (IllegalArgumentException iae) {
            log.error("Unexpected", iae);
            return null;
        }
        return getEditableApplicationProperties(editPermission, keyFilter);
    }

    @Override
    public List<ApplicationProperty> getEditableAdvancedProperties() throws DataAccessException {
        if (!hasAdministrativePermissions()) {
            return Collections.emptyList();
        }
        return applicationPropertiesStore.getEditableAdvancedProperties(isSysAdmin() ? EditPermissionLevel.SYSADMIN : EditPermissionLevel.ADMIN);
    }

    /**
     * {@inheritDoc}
     * <p>
     * The implementation below calls only {@code isNormalAdmin()} as the SYSTEM_ADMIN has an implicit ADMINISTER privilege.
     */
    @Override
    public boolean hasAdministrativePermissions() {
        return isNormalAdmin();
    }

    private boolean isNormalAdmin() {
        return permissionManager.hasPermission(Permissions.ADMINISTER, authenticationContext.getUser());
    }

    private boolean isSysAdmin() {
        return permissionManager.hasPermission(Permissions.SYSTEM_ADMIN, authenticationContext.getUser());
    }

    @Override
    public boolean hasPermissionForLevel(String permissionLevel) {
        if (StringUtils.isBlank(permissionLevel)) {
            return hasAdministrativePermissions();
        }

        EditPermissionLevel editPermission;
        try {
            editPermission = EditPermissionLevel.valueOf(permissionLevel.toUpperCase());
        } catch (IllegalArgumentException iae) {
            log.error("Unexpected", iae);
            return false;
        }

        if (editPermission == EditPermissionLevel.SYSADMIN || editPermission == EditPermissionLevel.SYSADMIN_ONLY) {
            return isSysAdmin();
        } else if (editPermission == EditPermissionLevel.ADMIN) {
            return hasAdministrativePermissions();
        }
        return false;
    }


    @Override
    public List<ApplicationProperty> getEditableApplicationProperties(EditPermissionLevel permissionLevel, String keyFilter)
            throws DataAccessException {
        return Lists.newArrayList(Iterables.filter(applicationPropertiesStore.getEditableApplicationProperties(permissionLevel, keyFilter), this.featurePredicate));
    }

    @Override
    public ApplicationProperty getApplicationProperty(final String key) {
        return applicationPropertiesStore.getApplicationPropertyFromKey(key);
    }

    @Override
    public Validated<ApplicationProperty> setApplicationProperty(final String key, String value) {
        ApplicationProperty applicationProperty = applicationPropertiesStore.getApplicationPropertyFromKey(key);
        String oldValue = applicationProperty.getCurrentValue();

        log.debug("validating value: " + value);
        ApplicationPropertyMetadata metadata = applicationProperty.getMetadata();
        Validator.Result result = metadata.validate(value);
        if (result.isValid()) {
            applicationProperty = applicationPropertiesStore.setApplicationProperty(key, value);

            eventPublisher.publish(createEvent(metadata, oldValue, value));
        }
        return new Validated<>(result, applicationProperty);

    }

    protected ApplicationPropertyChangeEvent createEvent(ApplicationPropertyMetadata metadata, String oldValue, String newValue) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(KEY_METADATA, Assertions.notNull("metadata", metadata));
        params.put(ApplicationPropertyChangeEvent.KEY_OLD_VALUE, oldValue);
        params.put(ApplicationPropertyChangeEvent.KEY_NEW_VALUE, newValue);
        return new ApplicationPropertyChangeEvent(params);
    }

}
