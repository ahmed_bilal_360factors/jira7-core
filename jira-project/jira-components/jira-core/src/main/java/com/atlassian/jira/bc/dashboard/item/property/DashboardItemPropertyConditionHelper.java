package com.atlassian.jira.bc.dashboard.item.property;

import com.atlassian.jira.bc.dashboard.DashboardItem;
import com.atlassian.jira.bc.dashboard.DashboardItemPropertyService;
import com.atlassian.jira.entity.property.AbstractEntityPropertyConditionHelper;
import com.atlassian.jira.plugin.webfragment.JiraWebContext;

import java.util.Map;
import java.util.Optional;

public final class DashboardItemPropertyConditionHelper extends AbstractEntityPropertyConditionHelper<DashboardItem> {

    public DashboardItemPropertyConditionHelper(DashboardItemPropertyService propertyService) {
        super(propertyService, DashboardItem.class, "dashboarditem");
    }

    @Override
    public Optional<Long> getEntityId(JiraWebContext context) {
        return context.get("dashboardItem", Map.class).map(item ->
                item.get("id") != null ? Long.valueOf(item.get("id").toString()) : null);
    }
}
