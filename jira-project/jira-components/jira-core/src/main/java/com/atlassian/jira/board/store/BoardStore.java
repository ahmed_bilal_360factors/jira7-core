package com.atlassian.jira.board.store;

import com.atlassian.jira.board.Board;
import com.atlassian.jira.board.BoardCreationData;
import com.atlassian.jira.board.BoardId;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Provide access to board in database.
 *
 * @since v7.1
 */
public interface BoardStore {
    Board createBoard(BoardCreationData boardCreationData);

    Optional<Board> getBoard(BoardId boardId);

    boolean deleteBoard(BoardId boardId);

    List<Board> getBoardsForProject(long projectId);

    boolean hasBoardForProject(long projectId);

    Optional<Date> getBoardDataChangedTime(BoardId boardId);
}
