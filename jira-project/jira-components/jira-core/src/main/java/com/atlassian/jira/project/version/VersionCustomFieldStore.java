package com.atlassian.jira.project.version;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.fugue.Option;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.impl.VersionCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.project.version.DeleteVersionWithCustomFieldParameters.CustomFieldReplacement;
import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.querydsl.core.types.Projections;


import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.jira.model.querydsl.QCustomField.CUSTOM_FIELD;
import static com.atlassian.jira.model.querydsl.QCustomFieldValue.CUSTOM_FIELD_VALUE;

/**
 * This compoment perform operation on verisons stored in custom fields.
 * While it's performing raw database operation it's tested on integration level in
 * {@code com.atlassian.jira.webtests.ztests.bundledplugins2.rest.TestVersionResource}
 */
public class VersionCustomFieldStore {
    private final QueryDslAccessor queryDslAccessor;
    final Supplier<CustomFieldManager> customFieldManagerSupplier;

    public VersionCustomFieldStore(QueryDslAccessor queryDslAccessor) {
        this.queryDslAccessor = queryDslAccessor;
        this.customFieldManagerSupplier = Suppliers.memoize(ComponentAccessor::getCustomFieldManager);
    }

    @Nonnull
    public List<CustomFieldVersionTransformation> createValueReplacers(Version deletedVersion, List<CustomFieldReplacement> customFieldReplacementList) {
        final Collection<CustomField> customFieldsTypesUsing = getCustomFieldsTypesUsing(deletedVersion);

        final Map<Long, Version> customFieldIdToTargetVersion = customFieldReplacementList
                .stream()
                .filter(replacemet -> replacemet.getMoveTo().isPresent())
                .collect(Collectors.toMap(
                        CustomFieldReplacement::getCustomFieldId,
                        replacement -> replacement.getMoveTo().get()));

        return customFieldsTypesUsing
                .stream()
                .map(customField -> {
                            final Option<Version> replacements = option(customFieldIdToTargetVersion.get(customField.getIdAsLong()));
                            return getCustomFieldTransformer(deletedVersion, customField, replacements);
                        }
                )
                .collect(CollectorsUtil.toImmutableListWithSizeOf(customFieldsTypesUsing));
    }

    @Nonnull
    public List<CustomFieldVersionTransformation> createValueReplacers(Version deletedVersion, Option<Version> targetVersion) {
        final Collection<CustomField> customFieldsTypesUsing = getCustomFieldsTypesUsing(deletedVersion);

        return customFieldsTypesUsing
                .stream()
                .map(customField -> getCustomFieldTransformer(deletedVersion, customField, targetVersion))
                .collect(CollectorsUtil.toImmutableListWithSizeOf(customFieldsTypesUsing));
    }

    private CustomFieldVersionTransformation getCustomFieldTransformer(Version deletedVersion, CustomField customField, Option<Version> replacement) {
        return new CustomFieldVersionTransformation(
                customField,
                replacement.fold(
                        () -> VersionCollectionManipulators.versionRemover(deletedVersion),
                        versionReplacement -> VersionCollectionManipulators.versionReplacer(deletedVersion, versionReplacement)
                )
        );
    }

    @Nonnull
    public Collection<? extends CustomFieldWithVersionUsage> getCustomFieldsUsing(final Version version) {
        Preconditions.checkNotNull(version.getId());

        return queryDslAccessor.executeQuery(connection -> connection.newSqlQuery()
                .select(Projections.constructor(CustomFieldUsageImpl.class, CUSTOM_FIELD_VALUE.customfield, CUSTOM_FIELD.name, CUSTOM_FIELD_VALUE.issue.countDistinct()))
                .from(CUSTOM_FIELD_VALUE)
                .leftJoin(CUSTOM_FIELD)
                .on(CUSTOM_FIELD_VALUE.customfield.eq(CUSTOM_FIELD.id))
                .where(CUSTOM_FIELD_VALUE.numbervalue.eq(version.getId().doubleValue())
                        .and(CUSTOM_FIELD.customfieldtypekey.eq(VersionCFType.SINGLE_VERSION_TYPE)
                                .or(CUSTOM_FIELD.customfieldtypekey.eq(VersionCFType.MULTIPLE_VERSION_TYPE))
                        ))
                .groupBy(CUSTOM_FIELD.name, CUSTOM_FIELD_VALUE.customfield)
                .fetch());
    }

    @Nonnull
    public Collection<CustomField> getCustomFieldsTypesUsing(final Version version) {
        Preconditions.checkNotNull(version.getId());

        final List<Long> customFieldIds = queryDslAccessor.executeQuery(connection -> connection.newSqlQuery()
                .select(CUSTOM_FIELD_VALUE.customfield)
                .from(CUSTOM_FIELD_VALUE)
                .leftJoin(CUSTOM_FIELD)
                .on(CUSTOM_FIELD_VALUE.customfield.eq(CUSTOM_FIELD.id))
                .where(CUSTOM_FIELD_VALUE.numbervalue.eq(version.getId().doubleValue())
                        .and(CUSTOM_FIELD.customfieldtypekey.eq(VersionCFType.SINGLE_VERSION_TYPE)
                                .or(CUSTOM_FIELD.customfieldtypekey.eq(VersionCFType.MULTIPLE_VERSION_TYPE))
                        ))
                .distinct()
                .fetch());

        final CustomFieldManager customFieldManager = customFieldManagerSupplier.get();
        return customFieldIds.stream().map(customFieldManager::getCustomFieldObject).collect(CollectorsUtil.toImmutableListWithSizeOf(customFieldIds));
    }

    public List<Long> getIssueIdsWithCustomFieldsFor(@Nonnull final Version version) {
        Preconditions.checkNotNull(version.getId());

        return queryDslAccessor.executeQuery(connection -> connection.newSqlQuery()
                .select(CUSTOM_FIELD_VALUE.issue)
                .from(CUSTOM_FIELD_VALUE)
                .leftJoin(CUSTOM_FIELD)
                .on(CUSTOM_FIELD_VALUE.customfield.eq(CUSTOM_FIELD.id))
                .where(CUSTOM_FIELD_VALUE.numbervalue.eq(version.getId().doubleValue())
                        .and(CUSTOM_FIELD.customfieldtypekey.eq(VersionCFType.SINGLE_VERSION_TYPE)
                                .or(CUSTOM_FIELD.customfieldtypekey.eq(VersionCFType.MULTIPLE_VERSION_TYPE))))
                .distinct()
                .fetch());
    }

    public long getCustomFieldIssuesCount(Version version) {
        Preconditions.checkNotNull(version.getId());

        return queryDslAccessor.executeQuery(connection -> connection.newSqlQuery()
                .select(CUSTOM_FIELD_VALUE.issue.countDistinct())
                .from(CUSTOM_FIELD_VALUE)
                .leftJoin(CUSTOM_FIELD)
                .on(CUSTOM_FIELD_VALUE.customfield.eq(CUSTOM_FIELD.id))
                .where(CUSTOM_FIELD_VALUE.numbervalue.eq(version.getId().doubleValue())
                        .and(CUSTOM_FIELD.customfieldtypekey.eq(VersionCFType.SINGLE_VERSION_TYPE)
                                .or(CUSTOM_FIELD.customfieldtypekey.eq(VersionCFType.MULTIPLE_VERSION_TYPE))
                        ))
                .fetchOne());
    }

    public static class CustomFieldUsageImpl implements CustomFieldWithVersionUsage {
        private final long customFieldId;
        private final String fieldName;
        private final long fieldCount;

        public CustomFieldUsageImpl(long customFieldId, String fieldName, long fieldCount) {
            this.fieldName = fieldName;
            this.fieldCount = fieldCount;
            this.customFieldId = customFieldId;
        }

        @Override
        public long getCustomFieldId() {
            return customFieldId;
        }

        @Override
        public String getFieldName() {
            return fieldName;
        }

        @Override
        public long getIssueCountWithVersionInCustomField() {
            return fieldCount;
        }
    }
}