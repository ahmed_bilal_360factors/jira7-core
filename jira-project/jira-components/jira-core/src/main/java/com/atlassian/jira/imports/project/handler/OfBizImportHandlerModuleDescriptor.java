package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.plugin.AbstractJiraModuleDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.module.ModuleFactory;

/**
 * Module descriptor for Project Import OfBiz Import handlers
 *
 * @since v6.5
 */
public class OfBizImportHandlerModuleDescriptor extends AbstractJiraModuleDescriptor<PluggableImportOfBizEntityHandler> {
    public OfBizImportHandlerModuleDescriptor(final JiraAuthenticationContext authenticationContext, final ModuleFactory moduleFactory) {
        super(authenticationContext, moduleFactory);
    }
}
