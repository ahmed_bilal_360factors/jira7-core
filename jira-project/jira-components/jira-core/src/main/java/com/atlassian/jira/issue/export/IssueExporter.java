package com.atlassian.jira.issue.export;

import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.plugin.searchrequestview.SearchRequestParams;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

/**
 * Exporter used to take a list of fields and a search request and export it to a given output.
 *
 * @since 7.2.0
 */
public interface IssueExporter {

    /**
     * Export the given search and fields and output it to the supplied writer.
     *
     * @param writer to export the output to
     * @param searchRequest to collect issues for
     * @param searchRequestParams to provide the context for the search
     * @param fields to export for these issues
     */
    void export(final Writer writer, final SearchRequest searchRequest,
                final SearchRequestParams searchRequestParams, final List<Field> fields) throws IOException, SearchException;
}
