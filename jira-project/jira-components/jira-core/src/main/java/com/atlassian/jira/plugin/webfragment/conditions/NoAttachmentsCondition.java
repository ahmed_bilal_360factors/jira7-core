package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Condition that determines whether given issue does not have any attachments
 *
 * @since 6.4
 */
public class NoAttachmentsCondition extends AbstractIssueWebCondition {
    @Override
    public boolean shouldDisplay(ApplicationUser applicationUser, Issue issue, JiraHelper jiraHelper) {
        return issue.getAttachments().size() == 0;
    }
}
