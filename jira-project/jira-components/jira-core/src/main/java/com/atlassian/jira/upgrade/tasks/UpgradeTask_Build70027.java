package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.upgrade.AbstractDelayableUpgradeTask;

/**
 * It's a marker for the change to the attachment file system so that
 * stable knows which versions of 6.4 are compatiable with the new system by
 * matching to the corresponding downgrade task
 *
 * @since v7.0
 */
public class UpgradeTask_Build70027 extends AbstractDelayableUpgradeTask {
    @Override
    public int getBuildNumber() {
        return 70027;
    }

    @Override
    public String getShortDescription() {
        return "Marker for change to attachment format";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception {
        //Not needed as upgraded
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        return true;
    }
}
