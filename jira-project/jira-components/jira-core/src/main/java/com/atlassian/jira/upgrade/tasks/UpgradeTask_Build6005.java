package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.upgrade.LegacyImmediateUpgradeTask;

import javax.annotation.Nullable;

/**
 * Only show the admin gadget "Getting Started" task list on new instances.
 */
public class UpgradeTask_Build6005 extends LegacyImmediateUpgradeTask {
    private final ApplicationProperties applicationProperties;

    public UpgradeTask_Build6005(ApplicationProperties applicationProperties) {
        super();
        this.applicationProperties = applicationProperties;
    }

    public int getBuildNumber() {
        return 6005;
    }

    public void doUpgrade(boolean setupMode) throws Exception {
        applicationProperties.setOption("jira.admin.gadget.task.list.enabled", false);
    }

    public String getShortDescription() {
        return "Only show the admin gadget 'Getting Started' task list on new instances";
    }

    @Nullable
    @Override
    public Integer dependsUpon() {
        return 6001;
    }

}
