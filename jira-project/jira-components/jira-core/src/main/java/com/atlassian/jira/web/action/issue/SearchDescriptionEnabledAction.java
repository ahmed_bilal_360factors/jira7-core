package com.atlassian.jira.web.action.issue;

import com.atlassian.core.util.collection.EasyList;
import com.atlassian.jira.bc.favourites.FavouritesService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.SearchContextImpl;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.managers.IssueSearcherManager;
import com.atlassian.jira.issue.search.searchers.IssueSearcher;
import com.atlassian.jira.issue.search.searchers.SearcherGroup;
import com.atlassian.jira.issue.transport.FieldValuesHolder;
import com.atlassian.jira.issue.transport.impl.IssueNavigatorActionParams;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.IssueActionSupport;
import com.atlassian.jira.web.action.filter.FilterOperationsBean;
import org.apache.commons.lang.StringUtils;
import webwork.action.ActionContext;

import java.util.Collection;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;

public class SearchDescriptionEnabledAction extends IssueActionSupport {
    protected final IssueSearcherManager issueSearcherManager;
    protected FieldValuesHolder fieldValuesHolder;
    protected PermissionManager permissionManager;
    private JiraAuthenticationContext authenticationContext;
    private SearchContext searchContext;
    private IssueNavigatorActionParams actionParams;
    private FavouritesService favouritesService;
    private FilterOperationsBean filterOperationsBean;
    private final SearchService searchService;

    public SearchDescriptionEnabledAction(IssueSearcherManager issueSearcherManager, SearchService searchService) {
        this.issueSearcherManager = issueSearcherManager;
        this.searchService = searchService;
        this.permissionManager = ComponentAccessor.getPermissionManager();
        this.authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        this.favouritesService = ComponentAccessor.getComponentOfType(FavouritesService.class);
    }

    public boolean isShown(SearcherGroup searcherGroup) {
        return searcherGroup.isShown(getLoggedInUser(), getSearchContext());
    }

    protected IssueNavigatorActionParams getActionParams() {
        if (actionParams == null) {
            actionParams = new IssueNavigatorActionParams(ActionContext.getParameters());
        }

        return actionParams;
    }

    protected Collection<IssueSearcher<?>> getSearchers() {
        return issueSearcherManager.getAllSearchers();
    }

    protected SearchContext getSearchContext() {
        if (getActionParams().isUpdateParamsRequired()) {
            final String query = getActionParams().getFirstValueForKey("jql");
            if (query != null) {
                return getSeachContextFromQueryString(query);
            } else {
                return getActionParams().getSearchContext();
            }
        } else {
            final SearchRequest searchRequest = getSearchRequest();
            if (searchRequest != null) {
                return searchService.getSearchContext(authenticationContext.getUser(), searchRequest.getQuery());
            } else {
                if (searchContext == null) {
                    searchContext = createSearchContext();
                }
                return searchContext;
            }
        }
    }

    private SearchContext getSeachContextFromQueryString(final String query) {
        if (StringUtils.isNotBlank(query)) {
            final SearchService.ParseResult jqlQuery = searchService.parseQuery(getLoggedInUser(), query);
            if (jqlQuery.isValid()) {
                return searchService.getSearchContext(getLoggedInUser(), jqlQuery.getQuery());
            }
        }
        return createSearchContext();
    }

    private SearchContext createSearchContext() {
        Collection<Project> visibleProjects = permissionManager.getProjects(BROWSE_PROJECTS, authenticationContext.getUser());
        if (visibleProjects != null && visibleProjects.size() == 1) {
            return new SearchContextImpl(null, EasyList.build(visibleProjects.iterator().next().getId()), null);
        } else {
            return new SearchContextImpl();
        }
    }

    public boolean validateSearchFilterIsSavedFilter(SearchRequest searchRequest, String i18n) {
        if (searchRequest.getId() == null) {
            // The id and name should be null but if they are not I want to know about it.
            String name = searchRequest.getName();
            log.error("Tried to perform operation on unsaved filter with id:" + searchRequest.getId() + " and name: " + name);
            addErrorMessage(getText(i18n, name == null ? "" : name));
            return false;
        }
        return true;
    }

    public FilterOperationsBean getFilterOperationsBean() {
        if (filterOperationsBean == null) {
            filterOperationsBean = createFilterOperationsBean(getLoggedInUser());
        }
        return filterOperationsBean;

    }

    protected FilterOperationsBean createFilterOperationsBean(final ApplicationUser user) {
        return FilterOperationsBean.create(getSearchRequest(), isFilterValid(), user, false);
    }

    /**
     * Check if the filter in session is valid or not.
     *
     * @return true iff the current filter is valid or false otherwise.
     * @deprecated Since 7.1. Storing information on the httpsession is obsoleted. Please move this functionality to the frontend or other type of storage. This method will be removed in 8.0.
     */
    @Deprecated
    public boolean isFilterValid() {
        final SearchRequest searchRequest = getSearchRequest();
        return !(searchRequest != null && searchService.validateQuery(getLoggedInUser(), searchRequest.getQuery(), searchRequest.getId()).hasAnyErrors());
    }
}
