package com.atlassian.jira.plugin.icon;


import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarFilenames;
import com.atlassian.jira.avatar.AvatarManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;

public class DefaultSystemIconImageProvider implements SystemIconImageProvider {
    private static final Logger log = LoggerFactory.getLogger(DefaultSystemIconImageProvider.class);
    private static final String ICON_CLASSPATH_PREFIX = "/avatars/";

    @Nonnull
    @Override
    public InputStream getSystemIconInputStream(Avatar icon, Avatar.Size size) throws IOException {
        final String path = getIconPath(icon, size);
        InputStream data = this.getClass().getResourceAsStream(path);

        if (data == null) {
            log.error("System Avatar not found at the following resource path: " + path);
            throw new IOException("File not found");
        }

        return data;
    }

    private String getIconPath(final Avatar icon, final Avatar.Size size) {
        if (isSvgContentType(icon.getContentType())) {
            return ICON_CLASSPATH_PREFIX + icon.getFileName();
        } else {
            return ICON_CLASSPATH_PREFIX + AvatarFilenames.getFilenameFlag(size) + icon.getFileName();
        }
    }

    public static boolean isSvgContentType(final String mimeType) {
        return AvatarManager.SVG_CONTENT_TYPE.equals(mimeType);
    }
}
