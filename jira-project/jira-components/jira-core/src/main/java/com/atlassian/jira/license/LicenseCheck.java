package com.atlassian.jira.license;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Root interface of classes that perform license-related checks.
 *
 * @since 7.0
 */
public interface LicenseCheck {
    /**
     * Performs this license check.
     */
    Result evaluate();

    /**
     * Indicates this license check has been passed.
     */
    Result PASS = new Result(ImmutableList.of(), "");

    /**
     * Indicates that a license check has failed but does not have any failing {@link LicenseDetails licenses}.
     */
    Result FAIL = new Failure(ImmutableList.of(), "");

    /**
     * Indicates that a license check has failed but because no {@link LicenseDetails licenses} are present.
     */
    Result FAIL_NO_LICENSES = new Failure(ImmutableList.of(), "No licenses present");

    /**
     * Encapsulates the result of a {@link LicenseCheck}, which may be either passed or failed, depending on the
     * return value of {@link #isPass()}.
     */
    class Result {
        private final List<LicenseDetails> failedLicenses;
        private final String reason;

        public Result(Iterable<? extends LicenseDetails> failedLicenses, String reason) {
            this.failedLicenses = ImmutableList.copyOf(failedLicenses);
            this.reason = reason;
        }

        /**
         * Returns a {@link List} of {@link LicenseDetails licenses} that
         * failed this {@link LicenseCheck}.
         */
        public List<LicenseDetails> getFailedLicenses() {
            return failedLicenses;
        }

        /**
         * Returns the reason why this {@link LicenseCheck} failed, or the empty string if it passed.
         */
        public String getFailureMessage() {
            return reason;
        }

        /**
         * Returns true if this {@link LicenseCheck} was passed.
         */
        public boolean isPass() {
            return failedLicenses.isEmpty();
        }
    }

    /**
     * Implements a {@link com.atlassian.jira.license.LicenseCheck.Result} that always {@link #isPass() fails}.
     */
    class Failure extends Result {
        public Failure(final List<? extends LicenseDetails> failedLicenses, final String reason) {
            super(failedLicenses, reason);
        }

        @Override
        public boolean isPass() {
            return false;
        }
    }
}
