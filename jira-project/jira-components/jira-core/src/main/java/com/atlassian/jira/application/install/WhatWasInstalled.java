package com.atlassian.jira.application.install;

import com.google.common.annotations.VisibleForTesting;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Optional;

import static com.atlassian.collectors.CollectorsUtil.toImmutableListWithCapacity;
import static java.util.Arrays.stream;

/**
 * Can retrieve information if given application source was previously installed.
 *
 * @since v6.5
 */
@Nonnull
public class WhatWasInstalled {
    @VisibleForTesting
    static final String APP_INSTALL_INFO_FILE_BASE = "%s.properties";
    private final BundlesVersionDiscovery bundlesVersionDiscovery;
    private final ApplicationInstallationEnvironment applicationInstallationEnvironment;
    private final WhatWasInstalledInApplicationFactory whatWasInstalledInApplicationFactory;

    public WhatWasInstalled(
            final ApplicationInstallationEnvironment applicationInstallationEnvironment,
            final BundlesVersionDiscovery bundlesVersionDiscovery,
            final WhatWasInstalledInApplicationFactory whatWasInstalledInApplicationFactory) {
        this.bundlesVersionDiscovery = bundlesVersionDiscovery;
        this.applicationInstallationEnvironment = applicationInstallationEnvironment;
        this.whatWasInstalledInApplicationFactory = whatWasInstalledInApplicationFactory;
    }

    public boolean wasApplicationSourceInstalled(final ApplicationSource applicationSource)
            throws IOException {
        final File applicationConfigFile = getApplicationInstallInfoFile(applicationSource.getApplicationSourceName());
        final WhatWasInstalledInApplication whatWasInstalledInApplication = whatWasInstalledInApplicationFactory.load(applicationConfigFile);
        final Collection<BundlesVersionDiscovery.PluginIdentification> pluginIdentifications = mapBundlesToVersion(applicationSource.getApplicationBundles());

        return whatWasInstalledInApplication.wereBundlesInstalled(pluginIdentifications);
    }

    public void storeInstalledApplicationSource(final ApplicationSource applicationSource, final ReversibleFileOperations reversibleFileOperations)
            throws IOException {
        final Iterable<BundlesVersionDiscovery.PluginIdentification> pluginIdentifications = mapBundlesToVersion(applicationSource.getApplicationBundles());
        final WhatWasInstalledInApplication whatWasInstalledInApplication = whatWasInstalledInApplicationFactory.load(pluginIdentifications);

        whatWasInstalledInApplicationFactory.store(
                getApplicationInstallInfoFile(applicationSource.getApplicationSourceName()),
                whatWasInstalledInApplication,
                reversibleFileOperations);
    }

    private File getApplicationInstallInfoFile(final String applicationName) {
        final File configRoot = applicationInstallationEnvironment.getInstallInformationDir();
        return new File(configRoot, String.format(APP_INSTALL_INFO_FILE_BASE, applicationName));
    }

    private Collection<BundlesVersionDiscovery.PluginIdentification> mapBundlesToVersion(final File[] sourceBundles) {
        return stream(sourceBundles)
                .map(bundlesVersionDiscovery::getBundleNameAndVersion)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toImmutableListWithCapacity(sourceBundles.length));
    }
}
