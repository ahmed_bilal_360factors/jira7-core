package com.atlassian.jira.issue.index;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.index.DefaultIssueIndexer.WorklogRetriever;
import com.atlassian.jira.issue.worklog.Worklog;

import java.util.List;

public class DefaultWorklogRetriever implements WorklogRetriever {
    @Override
    public List<Worklog> apply(final Issue issue) {
        return ComponentAccessor.getWorklogManager().getByIssue(issue);
    }

}
