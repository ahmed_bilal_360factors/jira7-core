package com.atlassian.jira.model.querydsl;

import com.querydsl.core.types.dsl.*;
import com.querydsl.sql.ColumnMetadata;

import javax.annotation.Generated;
import java.sql.Types;

/**
 * QOSPropertyDate is a Querydsl query object.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QOSPropertyDate extends JiraRelationalPathBase<OSPropertyDateDTO> {
    public static final QOSPropertyDate O_S_PROPERTY_DATE = new QOSPropertyDate("O_S_PROPERTY_DATE");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final DateTimePath<java.sql.Timestamp> value = createDateTime("value", java.sql.Timestamp.class);

    public QOSPropertyDate(String alias) {
        super(OSPropertyDateDTO.class, alias, "propertydate");
        addMetadata();
    }

    private void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(value, ColumnMetadata.named("propertyvalue").withIndex(2).ofType(Types.TIMESTAMP).withSize(35));
    }

    /**
     * Returns the ofbiz model's entity name for this query object.
     *
     * @return the ofbiz model's entity name for this query object.
     */
    @Override
    public String getEntityName() {
        return "OSPropertyDate";
    }

    @Override
    public NumberPath<Long> getNumericIdPath() {
        return id;
    }
}

