package com.atlassian.jira.instrumentation;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

@ParametersAreNonnullByDefault
public class RequestData {
    private final Optional<String> path;
    private final Optional<String> traceId;
    private final Optional<String> spanId;
    private final Optional<String> parentSpanId;
    private final Optional<String> queryString;
    private final Optional<Long> startNanoTime;

    private RequestData(Optional<String> path, Optional<String> traceId, Optional<String> spanId,
                        Optional<String> parentSpanId, Optional<String> queryString, Optional<Long> startNanoTime) {
        this.path = path;
        this.traceId = traceId;
        this.spanId = spanId;
        this.parentSpanId = parentSpanId;
        this.queryString = queryString;
        this.startNanoTime = startNanoTime;
    }

    /**
     * Gets the path of the URL.
     */
    public Optional<String> getPath() {
        return path;
    }

    /**
     * Gets the  ID for this request
     */
    public Optional<String> getTraceId() {
        return traceId;
    }

    /**
     * Gets the Zipkin spanId from the request
     */
    public Optional<String> getSpanId() {
        return spanId;
    }

    /**
     * Gets the Zipkin parent span Id for the request
     */
    public Optional<String> getParentSpanId() {
        return parentSpanId;
    }

    /**
     * Gets the query string for the request
     */
    public Optional<String> getQueryString() {
        return queryString;
    }

    public Optional<Long> getStartNanoTime() {
        return startNanoTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RequestData that = (RequestData) o;
        return Objects.equals(path, that.path) &&
                Objects.equals(traceId, that.traceId) &&
                Objects.equals(spanId, that.spanId) &&
                Objects.equals(parentSpanId, that.parentSpanId) &&
                Objects.equals(queryString, that.queryString) &&
                Objects.equals(startNanoTime, that.startNanoTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path, traceId, spanId, parentSpanId, queryString);
    }

    public static class Builder {
        private Optional<String> path = Optional.empty();
        private Optional<String> traceId = Optional.empty();
        private Optional<String> spanId = Optional.empty();
        private Optional<String> parentSpanId = Optional.empty();
        private Optional<String> queryString = Optional.empty();
        private Optional<Long> startNanoTime = Optional.empty();

        public Builder setPath(@Nullable String path) {
            this.path = Optional.ofNullable(requireNonNull(path));
            return this;
        }

        public Builder setPath(Optional<String> path) {
            this.path = path;
            return this;
        }

        public Builder setTraceId(@Nullable String traceId) {
            this.traceId = Optional.ofNullable(requireNonNull(traceId));
            return this;
        }

        public Builder setTraceId(Optional<String> traceId) {
            this.traceId = traceId;
            return this;
        }

        public Builder setSpanId(@Nullable String spanId) {
            this.spanId = Optional.ofNullable(spanId);
            return this;
        }

        public Builder setParentSpanId(@Nullable String parentSpanId) {
            this.parentSpanId = Optional.ofNullable(parentSpanId);
            return this;
        }

        public Builder setQueryString(@Nullable String queryString) {
            this.queryString = Optional.ofNullable(queryString);
            return this;
        }

        public Builder setStartNanoTime(@Nullable Long startNanoTime) {
            this.startNanoTime = Optional.ofNullable(startNanoTime);
            return this;
        }

        public RequestData build() {
            return new RequestData(path, traceId, spanId, parentSpanId, queryString, startNanoTime);
        }
    }
}
