package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;

/**
 * Helps USE permission based migration. For use based migration we:
 * <p>
 * <ul>
 * <li>Take USE, ADMIN and SYSADMIN permission groups and move them over to application roles.</li>
 * <li>Set groups that have USE but not (SYSADMIN or ADMIN) as defaults.</li>
 * </ul>
 *
 * @since v7.0
 */

abstract class UseBasedMigration {
    abstract MigrationState addUsePermissionToRoles(MigrationState state, Iterable<ApplicationKey> keys);
}
