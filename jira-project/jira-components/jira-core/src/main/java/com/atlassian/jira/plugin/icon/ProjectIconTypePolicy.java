package com.atlassian.jira.plugin.icon;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.icon.IconOwningObjectId;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;

/**
 * Implements an {@link IconTypePolicy} for icons for {@link Project}s.
 */
public class ProjectIconTypePolicy implements IconTypePolicy {
    private final GlobalPermissionManager globalPermissionManager;
    private final PermissionManager permissionManager;
    private final ProjectService projectService;
    private final ProjectManager projectManager;
    private static final Logger log = LoggerFactory.getLogger(ProjectIconTypePolicy.class);

    public ProjectIconTypePolicy(
            GlobalPermissionManager globalPermissionManager, PermissionManager permissionManager, ProjectService projectService, ProjectManager projectManager) {
        this.globalPermissionManager = globalPermissionManager;
        this.permissionManager = permissionManager;
        this.projectService = projectService;
        this.projectManager = projectManager;
    }

    @Override
    public boolean userCanView(@Nullable ApplicationUser remoteUser, @Nonnull Avatar icon) {
        String owningObjectID = icon.getOwner();
        boolean result = false;

        if (!icon.getIconType().equals(IconType.PROJECT_ICON_TYPE)) {
            result = false;
        } else if (icon.isSystemAvatar()) {
            result = true;
        } else if (owningObjectID == null) {
            // Unlikely, as generally isSystemAvatar() imples owner=null
            result = false;
        } else {
            long owningProjectId;
            try {
                owningProjectId = Long.parseLong(owningObjectID);
            } catch (NumberFormatException e) {
                log.error("Could not parse project ID " + owningObjectID);
                return false;
            }
            final Project project = projectManager.getProjectObj(owningProjectId);

            result = null != project;
            result = result && hasUserPermissionToProject(remoteUser, project);
        }

        if (!result && log.isDebugEnabled()) {
            try {
                log.debug("User " + remoteUser.getKey() + " did not have permission to view icon for project id=" + owningObjectID);
            } catch (Throwable t) {
                // eat it
            }
        }
        return result;
    }

    private boolean hasUserPermissionToProject(ApplicationUser remoteUser, Project project) {
        final boolean isAdmin = permissionManager.hasPermission(Permissions.ADMINISTER, remoteUser);
        final boolean isProjectAdmin = permissionManager.hasPermission(ADMINISTER_PROJECTS, project, remoteUser);
        final boolean hasBrowseProject = permissionManager.hasPermission(BROWSE_PROJECTS, project, remoteUser);

        return hasBrowseProject || isProjectAdmin || isAdmin;
    }

    @Override
    public boolean userCanDelete(@Nullable ApplicationUser remoteUser, @Nonnull Avatar icon) {
        if (remoteUser == null) {
            return false;
        }

        String owningObjectID = icon.getOwner();

        if (owningObjectID == null) {
            return false;
        } else {
            Project project = projectManager.getProjectObj(Long.parseLong(owningObjectID));

            if (project == null) {
                return false;
            } else {
                return globalPermissionManager.hasPermission(ADMINISTER, remoteUser) ||
                        permissionManager.hasPermission(ProjectPermissions.ADMINISTER_PROJECTS, project, remoteUser);
            }

        }
    }

    @Override
    public boolean userCanCreateFor(@Nullable ApplicationUser remoteUser, @Nonnull IconOwningObjectId owningObjectId) {
        if (remoteUser == null) {
            return false;
        }
        final long owningProjectId;
        try {
            owningProjectId = Long.parseLong(owningObjectId.getId());
        } catch (Exception x) {
            log.error("Could not map " + owningObjectId.getId() + " to a project.");
            return false;
        }
        final ProjectService.GetProjectResult getProjectResult =
                projectService.getProjectByIdForAction(remoteUser, owningProjectId, ProjectAction.EDIT_PROJECT_CONFIG);

        return getProjectResult.isValid();
    }
}
