package com.atlassian.jira.cache.monitor;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.cache.CacheStatisticsKey;
import com.google.common.collect.ImmutableMap;

import java.lang.management.ManagementFactory;
import java.time.Duration;
import java.util.Map;

/**
 * Analytics event representing usage statistics of ManagedCache.
 * NOTE: this implementation does not provide access to Heap Size
 * parameter as it is considered expensive and inaccurate.
 *
 * @since v6.5
 */
@EventName("jira.cache.stats")
public class CacheStatisticsAnalyticEvent {

    private static final Long INITIAL_CACHE_VALUE = 0L;

    private final String name;
    private final ImmutableMap<CacheStatisticsKey, Long> stats;

    public CacheStatisticsAnalyticEvent(final String name, final Map<CacheStatisticsKey, Long> stats) {
        this.name = name;
        this.stats = ImmutableMap.copyOf(stats);
    }

    public String getName() {
        return name;
    }

    /* This property can't be called "size" as property parser will get mad */
    @SuppressWarnings("unused")
    public Long getCacheSize() {
        return getStatistics(CacheStatisticsKey.SIZE);
    }

    @SuppressWarnings("unused")
    public Long getUptime() {
        return ManagementFactory.getRuntimeMXBean().getUptime();
    }

    @SuppressWarnings("unused")
    public Long getEvictionCount() {
        return getStatistics(CacheStatisticsKey.EVICTION_COUNT);
    }

    @SuppressWarnings("unused")
    public Long getHitCount() {
        return getStatistics(CacheStatisticsKey.HIT_COUNT);
    }

    @SuppressWarnings("unused")
    public Long getMissCount() {
        return getStatistics(CacheStatisticsKey.MISS_COUNT);
    }

    @SuppressWarnings("unused")
    public Long getPutCount() {
        return getStatistics(CacheStatisticsKey.PUT_COUNT);
    }

    @SuppressWarnings("unused")
    public Long getRemoveCount() {
        return getStatistics(CacheStatisticsKey.REMOVE_COUNT);
    }

    /* Total miss time converted to milliseconds */
    public Long getTotalMissTime() {
        return Duration.ofNanos(getStatistics(CacheStatisticsKey.TOTAL_MISS_TIME)).toMillis();
    }

    private Long getStatistics(final CacheStatisticsKey key) {
        return stats.getOrDefault(key, INITIAL_CACHE_VALUE);
    }
}
