package com.atlassian.velocity;

import com.atlassian.extension.util.Streams;
import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.velocity.util.introspection.MethodMap;

import javax.annotation.Nonnull;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static com.atlassian.collectors.CollectorsUtil.toImmutableSet;

/**
 * Overrides Velocity method map to utilize its find function but not populate methods cache.
 */
public class JiraMethodMap extends MethodMap {

    private final Class<?> clazz;
    private final LoadingCache<String, List<Method>> methodsCache = CacheBuilder.newBuilder()
            .softValues()
            .weakKeys()
                    //this cache is only to initialize cache in JiraIntrospectorCache so we do not need to keep it longer than
                    //longest "hypothetical" single request, there is no real harm if this get unloaded
            .expireAfterAccess(1, TimeUnit.MINUTES)
            .build(new CacheLoader<String, List<Method>>() {
                @Override
                public List<Method> load(@Nonnull final String s) throws Exception {
                    return loadMethodsForName(s);
                }
            });

    public JiraMethodMap(final Class<?> clazz) {
        this.clazz = clazz;
    }

    @Override
    public List<Method> get(final String methodName) {
        if (methodName == null) {
            return Collections.emptyList();
        }
        return methodsCache.getUnchecked(methodName);
    }

    @Override
    public void add(final Method method) {
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public Method find(final String methodName, final Object[] args) throws AmbiguousException {
        Preconditions.checkNotNull(args, "params cannot not be null");

        //first try direct matches
        final Class<?>[] parametersTypes = VelocityTypesUtil.getParametersTypes(args);
        final Collection<Method> methods = get(methodName).stream()
                .filter(method -> Arrays.equals(VelocityTypesUtil.getMethodArgsTypes(method), parametersTypes))
                .collect(toImmutableList());

        if (methods.size() == 1) {
            return Iterables.getOnlyElement(methods);
        } else if (methods.isEmpty()) {
            return super.find(methodName, args);
        } else {
            throw new MethodMap.AmbiguousException();
        }
    }

    private List<Method> loadMethodsForName(final String methodName) {
        final ImmutableSet<MethodComparator> uniqueMethods = getSuperClasses(clazz).stream()
                // get all interfaces:
                .map(ClassAndAllInterfacesIterator::new)
                .flatMap(Streams::stream)
                // filter public classes:
                .filter(clazz1 -> Modifier.isPublic(clazz1.getModifiers()))
                // get all public methods:
                .map(clazz1 -> Arrays.asList(clazz1.getDeclaredMethods()))
                .flatMap(Collection::stream)
                .filter(method -> Modifier.isPublic(method.getModifiers()))
                // get those with the name that we want:
                .filter(method -> method.getName().equals(methodName))
                .map(MethodComparator::new)
                .collect(toImmutableSet());

        return uniqueMethods.stream()
                .map(m -> m.method)
                .collect(toImmutableList());
    }

    private List<Class<?>> getSuperClasses(final Class<?> clazz) {
        final ImmutableList.Builder<Class<?>> retBuilder = ImmutableList.builder();
        for (Class<?> current = clazz; current != null; current = current.getSuperclass()) {
            retBuilder.add(current);
        }
        return retBuilder.build();
    }

    private static class ClassAndAllInterfacesIterator implements Iterable<Class<?>> {

        private final Class<?> clazz;

        public ClassAndAllInterfacesIterator(final Class<?> clazz) {
            this.clazz = clazz;
        }

        @Override
        public Iterator<Class<?>> iterator() {
            return new Iterator<Class<?>>() {
                private final Queue<Class<?>> interfacesToVisit = Lists.<Class<?>>newLinkedList(Collections.singleton(clazz));

                @Override
                public boolean hasNext() {
                    return !interfacesToVisit.isEmpty();
                }

                @Override
                public Class<?> next() {
                    final Class<?> currentInterface = interfacesToVisit.remove();
                    interfacesToVisit.addAll(Arrays.asList(currentInterface.getInterfaces()));
                    return currentInterface;
                }

                @Override
                public void remove() {
                    throw new UnsupportedOperationException("Not supported");
                }
            };
        }
    }

    private static class MethodComparator {
        private final Method method;

        private MethodComparator(final Method method) {
            this.method = method;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final MethodComparator that = (MethodComparator) o;

            return method.getName().equals(that.method.getName())
                    && Arrays.equals(method.getParameterTypes(), that.method.getParameterTypes());
        }

        @Override
        public int hashCode() {
            int result = method.getName().hashCode();
            result = 31 * result + Arrays.hashCode(method.getParameterTypes());
            return result;
        }
    }
}
