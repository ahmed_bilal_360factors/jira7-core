package com.atlassian.jira.servermetrics;

import com.google.common.base.Stopwatch;
import com.google.common.base.Ticker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.NotThreadSafe;
import java.time.Duration;
import java.util.Optional;

/**
 * Handles cases where initial request contains formwards and new, nested request are
 * created during original request handling. In such situation first request contains metrics
 * for all nested - sub requests.
 */
@NotThreadSafe
@ParametersAreNonnullByDefault
class NestedRequestsMetricsCollector implements ServerMetricsDetailCollector {
    private static final Logger LOGGER = LoggerFactory.getLogger(NestedRequestsMetricsCollector.class);

    private Optional<RequestMetricsCollector> activeCollector = Optional.empty();
    private final Ticker ticker;
    private int referenceCount;

    public NestedRequestsMetricsCollector(Ticker ticker) {
        this.ticker = ticker;
    }

    public void startCollectionInCurrentThread() {
        if (0 == referenceCount) {
            activeCollector = Optional.of(RequestMetricsCollector.started(Stopwatch.createUnstarted(ticker)));
        }
        ++referenceCount;
    }

    public void checkpointReached(final String checkpointName) {
        activeCollector.ifPresent(requestPartitioning ->
                requestPartitioning.checkpointReached(checkpointName)
        );
    }

    @Override
    public void checkpointReachedOnce(String checkpointName) {
        activeCollector.ifPresent(requestPartitioning ->
                requestPartitioning.checkpointReachedOnce(checkpointName)
        );
    }

    @Override
    public void checkpointReachedOverride(String checkpointName) {
        activeCollector.ifPresent(requestPartitioning ->
                requestPartitioning.checkpointReachedOverride(checkpointName)
        );
    }

    @Override
    public void addTimeSpentInActivity(String activityName, Duration duration) {
        activeCollector.ifPresent(requestMetricsCollector ->
                requestMetricsCollector.addTimeSpentInActivity(activityName, duration)
        );
    }

    public void setTimeSpentInActivity(String activityName, Duration duration) {
        activeCollector.ifPresent(requestMetricsCollector ->
            requestMetricsCollector.setTimeSpentInActivity(activityName, duration));
    }

    public Optional<TimingInformation> finishCollection() {
        --referenceCount;
        if (0 == referenceCount) {
            final Optional<TimingInformation> timingInformation = activeCollector.map(
                    RequestMetricsCollector::getCurrentTiming
            );
            activeCollector = Optional.empty();

            return timingInformation;
        } else {
            if (referenceCount < 0) {
                referenceCount = 0;
                LOGGER.warn("startCollectionInCurrentThread call count doesn't match finishCollection call count.");
            }
            return Optional.empty();
        }
    }
}
