package com.atlassian.jira.util.thread;

import org.apache.log4j.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;


/**
 * A concrete implementation of {@link JiraThreadLocalUtil} so that plugin
 * developers can have an API route into the {@link JiraThreadLocalUtils}
 * cleanup code.
 *
 * @since v6.0
 */
public class JiraThreadLocalUtilImpl implements JiraThreadLocalUtil {
    @Override
    public void preCall() {
        JiraThreadLocalUtils.preCall();
    }

    @Override
    public void postCall(@Nonnull final Logger log) {
        JiraThreadLocalUtils.postCall(log, null);
    }

    @Override
    public void postCall(@Nonnull final Logger log, @Nullable final WarningCallback warningCallback) {
        JiraThreadLocalUtils.postCall(log, warningCallback);
    }
}
