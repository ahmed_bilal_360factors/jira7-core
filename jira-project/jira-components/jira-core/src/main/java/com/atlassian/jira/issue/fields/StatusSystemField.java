package com.atlassian.jira.issue.fields;

import java.util.Map;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.export.ExportableSystemField;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.export.FieldExportPartsBuilder;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.rest.FieldJsonRepresentation;
import com.atlassian.jira.issue.fields.rest.FieldTypeInfo;
import com.atlassian.jira.issue.fields.rest.FieldTypeInfoContext;
import com.atlassian.jira.issue.fields.rest.RestAwareField;
import com.atlassian.jira.issue.fields.rest.json.JsonData;
import com.atlassian.jira.issue.fields.rest.json.JsonType;
import com.atlassian.jira.issue.fields.rest.json.JsonTypeBuilder;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.StatusJsonBean;
import com.atlassian.jira.issue.search.LuceneFieldSorter;
import com.atlassian.jira.issue.search.SearchHandler;
import com.atlassian.jira.issue.search.handlers.SearchHandlerFactory;
import com.atlassian.jira.issue.search.handlers.StatusSearchHandlerFactory;
import com.atlassian.jira.issue.statistics.StatusStatisticsMapper;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.issue.status.StatusFormatter;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.util.I18nHelper;

import com.opensymphony.util.TextUtils;

/**
 * Copyright (c) 2002-2004
 * All rights reserved.
 */
public class StatusSystemField extends NavigableFieldImpl  implements SearchableField, RestAwareField, ExportableSystemField {
    private final StatusStatisticsMapper statusStatisticsMapper;
    private final ConstantsManager constantsManager;
    private final SearchHandlerFactory searchHandlerFactory;
    private final JiraBaseUrls jiraBaseUrls;
    private final StatusFormatter statusFormatter;

    public StatusSystemField(VelocityTemplatingEngine templatingEngine, ApplicationProperties applicationProperties,
                             JiraAuthenticationContext authenticationContext, StatusStatisticsMapper statusStatisticsMapper,
                             ConstantsManager constantsManager, StatusSearchHandlerFactory searchHandlerFactory, JiraBaseUrls jiraBaseUrls,
                             StatusFormatter statusFormatter) {
        super(IssueFieldConstants.STATUS, "issue.field.status", "issue.column.heading.status", ORDER_DESCENDING,
                templatingEngine, applicationProperties, authenticationContext);
        this.statusStatisticsMapper = statusStatisticsMapper;
        this.constantsManager = constantsManager;
        this.searchHandlerFactory = searchHandlerFactory;
        this.jiraBaseUrls = jiraBaseUrls;
        this.statusFormatter = statusFormatter;
    }

    public LuceneFieldSorter getSorter() {
        return statusStatisticsMapper;
    }

    public String getColumnViewHtml(FieldLayoutItem fieldLayoutItem, Map<String, Object> displayParams, Issue issue) {
        return statusFormatter.getColumnViewHtml(issue.getStatusObject(), displayParams);
    }

    private Long getStatusTypeIdByName(String stringValue) throws FieldValidationException {
        for (Status status : constantsManager.getStatuses()) {
            if (stringValue.equalsIgnoreCase(status.getName())) {
                return Long.valueOf(status.getId());
            }
        }

        throw new FieldValidationException("Invalid status name '" + stringValue + "'.");
    }

    public SearchHandler createAssociatedSearchHandler() {
        return searchHandlerFactory.createHandler(this);
    }

    /**
     * Return an internationalized value for the changeHistory item - a status name in this case.
     *
     * @param changeHistory name of status
     * @param i18nHelper    used to translate the status name
     * @return String
     */
    public String prettyPrintChangeHistory(String changeHistory, I18nHelper i18nHelper) {
        if (TextUtils.stringSet(changeHistory)) {
            Long statusId = getStatusTypeIdByName(changeHistory);

            if (statusId != null) {
                Status status = constantsManager.getStatusObject(statusId.toString());
                if (status != null) {
                    return status.getNameTranslation(i18nHelper);
                }
            }
        }
        // Otherwise return the original string
        return changeHistory;
    }

    @Override
    public FieldTypeInfo getFieldTypeInfo(FieldTypeInfoContext fieldTypeInfoContext) {
        throw new UnsupportedOperationException("This method is only called for fields that implement " + OrderableField.class.getSimpleName() + " interface. But " + this.getClass().getSimpleName() + " does not implement it.");
    }

    @Override
    public JsonType getJsonSchema() {
        return JsonTypeBuilder.system(JsonType.STATUS_TYPE, IssueFieldConstants.STATUS);
    }

    @Override
    public FieldJsonRepresentation getJsonFromIssue(Issue issue, boolean renderedVersionRequired, FieldLayoutItem fieldLayoutItem) {
        return new FieldJsonRepresentation(new JsonData(StatusJsonBean.bean(issue.getStatusObject(), jiraBaseUrls)));
    }

    /**
     * Returns the name translation of the status, for example: In Progress, etc.
     *
     * @param issue to get the field representation for
     * @return exportable representation of the issue's field.
     */
    @Override
    public FieldExportParts getRepresentationFromIssue(Issue issue) {
        return FieldExportPartsBuilder.buildSinglePartRepresentation(getId(), getName(), issue.getStatus().getNameTranslation());
    }
}
