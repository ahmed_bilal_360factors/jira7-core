package com.atlassian.jira.startup;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.dataimport.DataImportParams;
import com.atlassian.jira.bc.dataimport.DataImportService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.license.MultiLicenseStore;
import com.atlassian.jira.task.TaskProgressSink;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.commons.io.IOUtils;
import org.ofbiz.core.entity.DelegatorInterface;
import org.ofbiz.core.entity.GenericEntityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

/**
 * Loads a startup set ot data into the JIRA database
 *
 * @since v6.0
 */
public class DatabaseInitialImporter {
    private static final Logger log = LoggerFactory.getLogger(DatabaseInitialImporter.class);

    private static final String STARTUP_XML = "startupdatabase.xml";
    private static final String PERMISSION_SCHEME_ENTITY_NAME = "PermissionScheme";

    public DatabaseInitialImporter() {
    }

    public boolean dataAlreadyLoaded() {
        // JIRA will as a very minimum always have at least one (default) permission scheme
        DelegatorInterface delegator = ComponentAccessor.getOfBizDelegator().getDelegatorInterface();
        try {
            return delegator.countAll(PERMISSION_SCHEME_ENTITY_NAME) > 0;
        } catch (GenericEntityException e) {
            throw new RuntimeException(e);
        }
    }

    public void importInitialData(ApplicationUser loggedInUser) {
        importInitialData(loggedInUser, Option.none(), Option.none());
    }

    public void importInitialData(ApplicationUser loggedInUser, Option<String> serverId, Option<String> license) {
        final Path path = copyStartupXmlIntoHome();
        final DataImportService dataImportService = ComponentAccessor.getComponent(DataImportService.class);
        final DataImportService.ImportValidationResult result
                = dataImportService.validateImport(loggedInUser, buildDataImportParameters(path, license));
        DataImportService.ImportResult importResult = dataImportService.doImport(loggedInUser, result,
                TaskProgressSink.NULL_SINK);
        if (!importResult.isValid()) {
            log.error(importResult.getSpecificErrorMessage());
            importResult.getErrorCollection().getErrorMessages().forEach(log::error);
            importResult.getErrorCollection().getErrors().values().forEach(log::error);
        }

        if (serverId.isDefined()) {
            final MultiLicenseStore component = ComponentAccessor.getComponent(MultiLicenseStore.class);
            component.storeServerId(serverId.get());
        }
    }

    private DataImportParams buildDataImportParameters(Path xmlPath, Option<String> license) {
        final DataImportParams.Builder builder = new DataImportParams.Builder(xmlPath.getFileName().toString())
                .setUseDefaultPaths(true)
                .setAllowDowngrade(false)
                .setupImport()
                .setStartupDataOnly()
                .setOutgoingEmailTo(true)
                .setQuickImport(true)
                .setUnsafeJiraBackup(xmlPath.toFile());

        if (license.isDefined()) {
            builder.setLicenseString(license.get());
        }

        return builder.build();
    }

    private Path copyStartupXmlIntoHome() {
        final JiraHome jiraHome = ComponentAccessor.getComponent(JiraHome.class);
        final Path importPath = jiraHome.getImportDirectory().toPath().resolve(STARTUP_XML);
        final InputStream startupStream = getClass().getResourceAsStream("/" + STARTUP_XML);
        try {
            Files.copy(startupStream, importPath, StandardCopyOption.REPLACE_EXISTING);
            return importPath;
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(startupStream);
        }
    }

}
