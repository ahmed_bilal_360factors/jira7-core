package com.atlassian.jira.project;

import com.atlassian.jira.model.querydsl.ProjectChangedTimeDTO;
import com.google.common.annotations.VisibleForTesting;

import java.sql.Timestamp;
import java.util.Objects;

public class ProjectChangedTimeImpl implements ProjectChangedTime {
    private final long projectId;
    private final Timestamp issueChangedTime;

    ProjectChangedTimeImpl(ProjectChangedTimeDTO projectChangedTimeDTO) {
        this.projectId = projectChangedTimeDTO.getProjectId();
        this.issueChangedTime = projectChangedTimeDTO.getIssueChangedTime();
    }

    @Override
    public long getProjectId() {
        return projectId;
    }

    @Override
    public Timestamp getIssueChangedTime() {
        return issueChangedTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProjectChangedTimeImpl that = (ProjectChangedTimeImpl) o;
        return projectId == that.projectId &&
                Objects.equals(issueChangedTime, that.issueChangedTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectId, issueChangedTime);
    }
}
