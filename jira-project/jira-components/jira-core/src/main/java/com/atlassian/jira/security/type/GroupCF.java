package com.atlassian.jira.security.type;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.GroupSelectorField;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.permission.PermissionContext;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.util.GroupSelectorUtils;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Issue Security and Permission type for a Group Selector custom field, or select-list custom fields which specify groups.
 *
 * @since 3.6
 */
public class GroupCF extends AbstractIssueFieldSecurityType {
    private static final Logger log = LoggerFactory.getLogger(GroupCF.class);
    public static final String TYPE = "groupCF";

    private JiraAuthenticationContext jiraAuthenticationContext;
    private GroupSelectorUtils groupSelectorUtils;
    private final CustomFieldManager customFieldManager;
    private final GroupManager groupManager;

    public GroupCF(JiraAuthenticationContext jiraAuthenticationContext, GroupSelectorUtils groupSelectorUtils, CustomFieldManager customFieldManager, GroupManager groupManager) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.groupSelectorUtils = groupSelectorUtils;
        this.customFieldManager = customFieldManager;
        this.groupManager = groupManager;
    }

    public String getDisplayName() {
        return jiraAuthenticationContext.getI18nHelper().getText("admin.permission.types.group.custom.field");
    }

    public String getType() {
        return TYPE;
    }

    @Override
    @Nullable
    protected BooleanQuery getQueryForProject(@Nonnull Project project, @Nullable ApplicationUser searcher, @Nonnull String fieldName) {
        Collection<String> groupNames = groupManager.getGroupNamesForUser(searcher.getName());
        if (groupNames != null && !groupNames.isEmpty()) {
            BooleanQuery projectAndGroupQuery = new BooleanQuery();
            Query projectQuery = new TermQuery(new Term(DocumentConstants.PROJECT_ID, project.getId().toString()));
            projectAndGroupQuery.add(projectQuery, BooleanClause.Occur.MUST);

            BooleanQuery groupQuery = getQueryForGroups(groupNames, fieldName);
            projectAndGroupQuery.add(groupQuery, BooleanClause.Occur.MUST);
            return projectAndGroupQuery;
        } else {
            return null;
        }
    }

    private BooleanQuery getQueryForGroups(Collection<String> groupNames, String fieldName) {
        BooleanQuery groupQuery = new BooleanQuery();
        for (String groupName : groupNames) {
            CustomField customField = customFieldManager.getCustomFieldObject(fieldName);
            Query queryForGroupName = getQueryForGroupName(customField, groupName, fieldName);
            groupQuery.add(queryForGroupName, BooleanClause.Occur.SHOULD);
        }
        return groupQuery;
    }

    private Query getQueryForGroupName(final CustomField customField, final String groupName, String fieldName) {
        if (customField.getCustomFieldType() instanceof GroupSelectorField) {
            return ((GroupSelectorField) customField.getCustomFieldType()).getQueryForGroup(fieldName, groupName);
        } else {
            return new TermQuery(new Term(fieldName, groupName));
        }
    }

    @Override
    protected BooleanQuery getQueryForSecurityLevel(IssueSecurityLevel issueSecurity, ApplicationUser searcher, String fieldName) {
        Collection<String> groupNames = groupManager.getGroupNamesForUser(searcher.getName());
        if (groupNames != null && !groupNames.isEmpty()) {
            BooleanQuery securityLevelAndGroupQuery = new BooleanQuery();
            Query securityLevelQuery = new TermQuery(new Term(DocumentConstants.ISSUE_SECURITY_LEVEL, issueSecurity.getId().toString()));
            securityLevelAndGroupQuery.add(securityLevelQuery, BooleanClause.Occur.MUST);

            BooleanQuery groupQuery = getQueryForGroups(groupNames, fieldName);
            securityLevelAndGroupQuery.add(groupQuery, BooleanClause.Occur.MUST);

            return securityLevelAndGroupQuery;
        } else {
            return null;
        }
    }

    public void doValidation(String key, Map<String, String> parameters, JiraServiceContext jiraServiceContext) {
        //JRA-13808: Need to check whether or not the group CF has a searcher set.
        String customFieldOption = parameters.get(getType());
        if (StringUtils.isEmpty(customFieldOption)) {
            String localisedMessage = jiraServiceContext.getI18nBean().getText("admin.permissions.errors.please.select.group.customfield");
            jiraServiceContext.getErrorCollection().addErrorMessage(localisedMessage);
        } else {
            // passed in parameters names a Custom Field - lets investigate.
            CustomField customField = customFieldManager.getCustomFieldObject(customFieldOption);
            if (customField != null && customField.getCustomFieldSearcher() == null) {
                // In order to use a Custom Field it must be indexed in Lucene Index. Currently we only index custom fields if they have a Searcher.
                // Message: "Custom field '{0}' is not indexed for searching - please add a searcher to this Custom Field."
                String localisedMessage = jiraServiceContext.getI18nBean().getText("admin.permissions.errors.customfieldnotindexed", customField.getName());
                jiraServiceContext.getErrorCollection().addErrorMessage(localisedMessage);
            }
        }
    }

    @Override
    protected String getFieldName(String parameter) {
        return parameter;
    }

    @Override
    protected boolean hasProjectPermission(ApplicationUser user, boolean issueCreation, Project project) {
        return !issueCreation;
    }

    @Override
    protected boolean hasIssuePermission(ApplicationUser user, boolean issueCreation, Issue issue, String parameter) {
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        boolean hasPermission = false;

        CustomField field = customFieldManager.getCustomFieldObject(parameter);

        if (field == null) {
            log.warn("custom field '" + parameter + "' is missing, can't use it to determine permissions.");
        } else {
            hasPermission = groupSelectorUtils.isUserInCustomFieldGroup(issue, field, user);
        }

        return hasPermission;
    }

    public List<Field> getDisplayFields() {
        return groupSelectorUtils.getCustomFieldsSpecifyingGroups();
    }

    @Override
    public String getArgumentDisplay(String argument) {
        FieldManager fieldManager = ComponentAccessor.getFieldManager();
        if (fieldManager.isCustomField(argument)) {
            CustomField field = fieldManager.getCustomField(argument);
            return field.getName();
        } else {
            return argument;
        }
    }

    /**
     * Get user specified by the Custom Field
     *
     * @param customFieldId eg. 'customfield_10000'
     */
    @Override
    public Set<ApplicationUser> getUsers(PermissionContext ctx, String customFieldId) {
        return groupSelectorUtils.getUsers(ctx.getIssue(), customFieldId);
    }
}
