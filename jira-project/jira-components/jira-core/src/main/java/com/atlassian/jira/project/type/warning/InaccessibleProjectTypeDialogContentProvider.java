package com.atlassian.jira.project.type.warning;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeKeyFormatter;
import com.atlassian.jira.project.type.ProjectTypeManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;

/**
 * Class responsible for generating the content for the dialogs attached to the project types warnings displayed on several pages.
 */
public class InaccessibleProjectTypeDialogContentProvider {
    private final ProjectTypeManager projectTypeManager;
    private final I18nHelper.BeanFactory i18nFactory;

    public InaccessibleProjectTypeDialogContentProvider(
            ProjectTypeManager projectTypeManager,
            I18nHelper.BeanFactory i18nFactory) {
        this.projectTypeManager = projectTypeManager;
        this.i18nFactory = i18nFactory;
    }

    /**
     * Returns the content for the dialog attached to the project types warning associated with the given project type key.
     *
     * @param user    The user that will see the dialog.
     * @param project The project.
     * @return The content for the dialog attached to the warning for the given project type
     */
    public InaccessibleProjectTypeDialogContent getContent(ApplicationUser user, Project project) {
        I18nHelper i18n = i18nFactory.getInstance(user);
        ProjectTypeKey projectTypeKey = project.getProjectTypeKey();
        String projectTypeName = ProjectTypeKeyFormatter.format(projectTypeKey);

        String title = i18n.getText("project.type.warning.dialog.title", projectTypeName);
        String firstParagraph = i18n.getText(getFirstParagraphKey(projectTypeKey), projectTypeName);
        String secondParagraph = i18n.getText("project.type.warning.dialog.project.still.accessible", projectTypeName);
        String callToActionText = i18n.getText("project.type.warning.dialog.change.project.type");

        return new InaccessibleProjectTypeDialogContent(title, firstParagraph, secondParagraph, callToActionText, project.getId());
    }

    private String getFirstParagraphKey(ProjectTypeKey projectTypeKey) {
        if (isProjectTypeUninstalled(projectTypeKey)) {
            return "project.type.warning.dialog.project.type.uninstalled";
        }
        return "project.type.warning.dialog.project.type.unlicensed";
    }

    private boolean isProjectTypeUninstalled(ProjectTypeKey projectTypeKey) {
        return projectTypeManager.isProjectTypeUninstalled(projectTypeKey);
    }
}
