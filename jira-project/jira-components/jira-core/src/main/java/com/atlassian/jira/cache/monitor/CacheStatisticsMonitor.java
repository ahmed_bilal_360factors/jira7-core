package com.atlassian.jira.cache.monitor;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheStatisticsKey;
import com.atlassian.cache.ManagedCache;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.instrumentation.InstrumentationListenerManager;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.util.concurrent.Supplier;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.atlassian.cache.CacheStatisticsKey.EVICTION_COUNT;
import static com.atlassian.cache.CacheStatisticsKey.HIT_COUNT;
import static com.atlassian.cache.CacheStatisticsKey.MISS_COUNT;
import static com.atlassian.cache.CacheStatisticsKey.PUT_COUNT;
import static com.atlassian.cache.CacheStatisticsKey.REMOVE_COUNT;
import static com.atlassian.cache.CacheStatisticsKey.SIZE;
import static com.atlassian.cache.CacheStatisticsKey.TOTAL_MISS_TIME;
import static java.util.stream.Collectors.toList;


/**
 * Sends managed cache statistics to analytics system once a day
 *
 * @since v6.5
 */
public class CacheStatisticsMonitor implements Startable, JobRunner {
    private static final Logger LOG = LoggerFactory.getLogger(CacheStatisticsMonitor.class);

    public static final String JIRA_CACHE_INSTRUMENTATION = "jira.instrumentation.cache";
    protected static final long RUN_INTERVAL = TimeUnit.DAYS.toMillis(1);
    private static final JobRunnerKey JOB_RUNNER_KEY = JobRunnerKey.of(CacheStatisticsMonitor.class.getName());
    private static final JobId JOB_ID = JobId.of(CacheStatisticsMonitor.class.getName());
    private static final long FOUR_HOURS = Duration.ofHours(4).toMillis();
    private static final Set<CacheStatisticsKey> ALLOWED_CACHE_STATISTICS = ImmutableSet.of(SIZE, HIT_COUNT, PUT_COUNT, REMOVE_COUNT, MISS_COUNT, TOTAL_MISS_TIME, EVICTION_COUNT);

    private final CacheManager cacheManager;
    private final EventPublisher eventPublisher;
    private final Random random;
    private final InstrumentationListenerManager instrumentationListenerManager;
    private final JiraProperties jiraSystemProperties;

    public CacheStatisticsMonitor(final CacheManager cacheManager, final EventPublisher eventPublisher, InstrumentationListenerManager instrumentationListenerManager, final JiraProperties jiraSystemProperties) {
        this.cacheManager = cacheManager;
        this.eventPublisher = eventPublisher;
        this.instrumentationListenerManager = instrumentationListenerManager;
        this.random = new Random(Runtime.getRuntime().freeMemory());
        this.jiraSystemProperties = jiraSystemProperties;
    }

    @Nullable
    @Override
    public JobRunnerResponse runJob(final JobRunnerRequest jobRunnerRequest) {
        try {
            final List<CacheStatisticsAnalyticEvent> cacheStatistics = getCacheStatistics();
            sendToAnalytics(cacheStatistics);
        } catch (final Exception ex) {
            LOG.warn("Exception occurred when running " + JOB_RUNNER_KEY + " job.", ex);
            return JobRunnerResponse.failed(ex);
        }
        return JobRunnerResponse.success();
    }

    @Override
    public void start() throws Exception {
        final SchedulerService scheduler = ComponentAccessor.getComponent(SchedulerService.class);
        scheduler.registerJobRunner(JOB_RUNNER_KEY, this);

        // we need to scatter a bit sending events to analytics system,
        // lets not start this service on every instance in the same moment
        final Date firstRun = Date.from(Instant.now().plusMillis(random.nextLong() % FOUR_HOURS));

        final JobConfig jobConfig = JobConfig.forJobRunnerKey(JOB_RUNNER_KEY)
                .withRunMode(RunMode.RUN_LOCALLY)
                .withSchedule(Schedule.forInterval(RUN_INTERVAL, firstRun));

        scheduler.scheduleJob(JOB_ID, jobConfig);

        // Add all the caches to the request based listener that have a collector.
        final Boolean cacheInstrumentationIsEnabled = jiraSystemProperties.getBoolean(JIRA_CACHE_INSTRUMENTATION) == null
                ? false
                : jiraSystemProperties.getBoolean(JIRA_CACHE_INSTRUMENTATION);

        cacheManager.getManagedCaches().stream()
                .map(ManagedCache::getCacheCollector)
                .filter(Objects::nonNull)
                .forEach(collector ->
                {
                    collector.setEnabled(cacheInstrumentationIsEnabled);
                    instrumentationListenerManager.addRequestListener(collector);
                });
    }

    private List<CacheStatisticsAnalyticEvent> getCacheStatistics() {
        return cacheManager.getManagedCaches().stream()
                .filter(ManagedCache::isStatisticsEnabled)
                .map(cache -> new CacheStatisticsAnalyticEvent(cache.getName(), calculateStatistics(cache)))
                .collect(toList());
    }

    private Map<CacheStatisticsKey, Long> calculateStatistics(final ManagedCache cache) {
        final Map<CacheStatisticsKey, Long> stats = Maps.newHashMap();
        for (final Map.Entry<CacheStatisticsKey, Supplier<Long>> entry : cache.getStatistics().entrySet()) {
            if (isCacheStatisticAllowed(entry.getKey())) {
                stats.put(entry.getKey(), entry.getValue().get());
            }
        }
        return stats;
    }

    private void sendToAnalytics(final List<CacheStatisticsAnalyticEvent> stats) {
        stats.stream().forEach(eventPublisher::publish);
    }

    //Only statistics from whitelist are allowed
    private boolean isCacheStatisticAllowed(final CacheStatisticsKey key) {
        return ALLOWED_CACHE_STATISTICS.contains(key);
    }
}
