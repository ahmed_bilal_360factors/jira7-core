package com.atlassian.jira.license;

/**
 * Checks that licenses are valid for a JIRA installation.
 *
 * @see JiraLicenseManager#hasLicenseTooOldForBuildConfirmationBeenDone()
 * @see JiraLicenseManager#confirmProceedUnderEvaluationTerms(String)
 * @since 7.0
 */
public interface BuildVersionLicenseCheck extends LicenseCheck {
    /**
     * Performs this build version license check without consideration of whether or not the current instance users
     * have accepted the grace (evaluation) period that is offered to users after the maintenance period of one or
     * more of their licenses expires (or more accurately, when {@link #evaluate} returns failure.
     *
     * @return the result of this license check, without consideration of grace period status
     * @see JiraLicenseManager#hasLicenseTooOldForBuildConfirmationBeenDone()
     * @see JiraLicenseManager#confirmProceedUnderEvaluationTerms(String)
     */
    Result evaluateWithoutGracePeriod();
}
