package com.atlassian.jira.plugin.renderercomponent;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.plugin.AbstractJiraModuleDescriptor;
import com.atlassian.jira.plugin.OrderableModuleDescriptor;
import com.atlassian.jira.plugin.util.ModuleDescriptorXMLUtils;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.renderer.v2.components.PluggableRendererComponentFactory;
import org.dom4j.Element;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Looks for renderer components that allow plugins to add new wiki renderer components to the render chain
 *
 * @since 3.12
 */
public class RendererComponentFactoryDescriptor extends AbstractJiraModuleDescriptor<PluggableRendererComponentFactory>
        implements OrderableModuleDescriptor {
    public static final String USE_OLD_IMAGE_RENDERER_ORDER = "jira.wiki.use.old.image.renderer.order";
    private static final int OLD_IMAGE_RENDERER_ORDER = 105;

    private int order = 0;
    private final Map<String, List<String>> listParams = new HashMap<>();
    private final FeatureManager featureManager;

    public RendererComponentFactoryDescriptor(final JiraAuthenticationContext authenticationContext,
                                              final ModuleFactory moduleFactory,
                                              final FeatureManager featureManager) {
        super(authenticationContext, moduleFactory);
        this.featureManager = featureManager;
    }

    public void init(final Plugin plugin, final Element element) throws PluginParseException {
        super.init(plugin, element);

        //read extra list of params if they exist
        final Element paramListElement = element.element("list-param");
        if (paramListElement != null) {
            @SuppressWarnings({"unchecked"})
            final List<Element> valueElements = paramListElement.elements("value");
            if (valueElements != null) {
                final List<String> paramList = valueElements.stream()
                        .map(Element::getTextTrim)
                        .collect(Collectors.toList());
                listParams.put(paramListElement.attribute("name").getText(), paramList);
            }
        }

        order = ModuleDescriptorXMLUtils.getOrder(element);
    }

    @Override
    public void enabled() {
        super.enabled();
        assertModuleClassImplements(PluggableRendererComponentFactory.class);
    }

    public Map<String, List<String>> getListParams() {
        return listParams;
    }

    public int getOrder() {
        // override the image renderer order to disable the fix for JRA-32688
        if ("embeddedimagerenderer".equals(key) && featureManager.isEnabled(USE_OLD_IMAGE_RENDERER_ORDER)) {
            return OLD_IMAGE_RENDERER_ORDER;
        }
        return order;
    }

    public boolean hasHelp() {
        return getResourceDescriptor("velocity", "help") != null;
    }

    public String getHelpSection() {
        return getResourceDescriptor("velocity", "help").getParameter("help-section");
    }

    public String getHelp() {
        return getHtml("help");
    }
}
