package com.atlassian.jira.application;

import com.atlassian.application.host.plugin.ApplicationEventListener;
import com.atlassian.jira.extension.Startable;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Class to register an {@code ApplicationEventListener} on JIRA's event bus. The {@code register} method of the
 * event listener needs to be called when JIRA starts. There is no cross product way of doing this so we do it
 * here in this simple wrapper class.
 *
 * @since v7.0
 */
public class JiraApplicationEventListener implements Startable {
    private final ApplicationEventListener delegate;

    public JiraApplicationEventListener(final ApplicationEventListener delegate) {
        this.delegate = notNull("delegate", delegate);
    }

    @Override
    public void start() throws Exception {
        delegate.register();
    }
}
