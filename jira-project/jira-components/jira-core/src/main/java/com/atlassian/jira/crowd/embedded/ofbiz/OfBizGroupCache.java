package com.atlassian.jira.crowd.embedded.ofbiz;

/**
 * The Group Cache is used to enhance the performance of access to group data.
 *
 * @since v7.0
 */
interface OfBizGroupCache {
    /**
     * Retrieve a Group by name case insensitively.
     *
     * @param directoryId The directory.
     * @param name        Group name.
     * @return A group.
     */
    OfBizGroup getCaseInsensitive(long directoryId, String name);

    /**
     * Remove a group from the cache.
     *
     * @param directoryId The directory.
     * @param name        Group name.
     */
    void remove(long directoryId, String name);

    /**
     * Remove a group from the cache.
     *
     * @param key The Groups key
     */
    void remove(DirectoryEntityKey key);

    /**
     * Refresh the cache entry for the supplied Group.
     * Should be called when a group is added or updated.  The implementation may do this
     * either by storing he supplied value or refreshing the data from the underlying persistent store.
     *
     * @param group Group to be refreshed in the cache
     * @return The key of the group.
     */
    DirectoryEntityKey refresh(OfBizGroup group);

    /**
     * Refresh all entries in the cache.
     */
    void refresh();

    /**
     * Return true if the cache is initialised and can be used.
     * JIRA relies heavily on having access to users and if the cache needs time to initialise before it
     * can be relied on to provide correct results, JIRA needs to wait.
     *
     * @return true if initialised.
     */
    boolean isCacheInitialized();
}
