package com.atlassian.jira.upgrade;

/**
 * Downgrade Tasks can use this to request a reindex after the downgrade is complete.
 *
 * @since v6.4.6
 */
public enum ReindexRequirement {
    /**
     * No reindex is required
     */
    NONE,
    /**
     * JIRA will start up and a reindex will happen in a background thread.
     */
    BACKGROUND,
    /**
     * JIRA will do a reindex in the foreground thread and user will be locked out until it is complete.
     */
    FOREGROUND
}
