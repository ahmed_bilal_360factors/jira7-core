package com.atlassian.jira.permission.management;


import com.atlassian.jira.permission.management.beans.SecurityTypeBean;
import com.atlassian.jira.user.ApplicationUser;

import java.util.List;

public interface SecurityTypeValuesService {

    /**
     * Given a user return primary security types returning their bean information
     * @param user to get types for
     * @return primary security types
     */
    List<SecurityTypeBean> buildPrimarySecurityTypes(final ApplicationUser user);

    /**
     * Given a user return secondary security types returning their bean information
     * @param user to get types for
     * @return secondary security types
     */
    List<SecurityTypeBean> buildSecondarySecurityTypes(final ApplicationUser user);

    /**
     * Sorts the grants into a UI specific and consistent order
     *
     * @param grants the list to sort inline
     */
    void sort(final List<SecurityTypeBean> grants);
}
