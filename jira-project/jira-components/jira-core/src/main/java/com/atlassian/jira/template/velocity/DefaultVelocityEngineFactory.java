package com.atlassian.jira.template.velocity;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class DefaultVelocityEngineFactory implements VelocityEngineFactory {
    private static final Logger log = LoggerFactory.getLogger(DefaultVelocityEngineFactory.class);

    private final String propertiesFileName;

    public DefaultVelocityEngineFactory(String propertiesFileName) {
        this.propertiesFileName = propertiesFileName;
    }

    public DefaultVelocityEngineFactory() {
        this.propertiesFileName = "velocity.properties";
    }

    @Override
    public VelocityEngine getEngine() {
        final VelocityEngine velocityEngine = new VelocityEngine();
        initialiseVelocityEngine(velocityEngine);
        return velocityEngine;
    }

    private void initialiseVelocityEngine(final VelocityEngine velocityEngine) {
        try {
            final Properties velocityPropertiesFile = new Properties();

            try {
                velocityPropertiesFile.load(ClassLoaderUtils.getResourceAsStream(propertiesFileName, getClass()));
            } catch (final Exception e) {
                log.warn("Could not configure the Velocity Engine from the velocity.properties, manually configuring.");
                velocityPropertiesFile.put("resource.loader", "class");
                velocityPropertiesFile.put("class.resource.loader.description", "Velocity Classpath Resource Loader");
                velocityPropertiesFile.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
            }

            enableDevMode(velocityPropertiesFile);

            velocityEngine.init(velocityPropertiesFile);
        } catch (final Exception e) {
            log.error("Exception initialising Velocity: " + e, e);
        }
    }

    public static void enableDevMode(final Properties velocityPropertiesFile) {
        // override caching options if were in dev mode
        if (JiraSystemProperties.getInstance().isDevMode()) {
            // Turn off velocity caching
            velocityPropertiesFile.put("class.resource.loader.cache", "false");
            velocityPropertiesFile.put("velocimacro.library.autoreload", "true");
            velocityPropertiesFile.put("plugin.resource.loader.cache", "false");
        }
    }
}
