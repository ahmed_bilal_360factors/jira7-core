package com.atlassian.jira.issue.fields.renderer.wiki;

import com.atlassian.event.api.EventListener;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.FeatureDisabledEvent;
import com.atlassian.jira.config.FeatureEnabledEvent;
import com.atlassian.jira.config.FeatureEvent;
import com.atlassian.jira.plugin.renderer.RendererComponentDecoratorFactoryDescriptor;
import com.atlassian.jira.plugin.renderercomponent.RendererComponentFactoryDescriptor;
import com.atlassian.jira.plugin.util.ModuleDescriptorComparator;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.renderer.RendererConfiguration;
import com.atlassian.renderer.embedded.EmbeddedResourceRenderer;
import com.atlassian.renderer.links.LinkRenderer;
import com.atlassian.renderer.v2.MutableRenderer;
import com.atlassian.renderer.v2.Renderer;
import com.atlassian.renderer.v2.V2RendererFacade;
import com.atlassian.renderer.v2.components.PluggableRendererComponentFactory;
import com.atlassian.renderer.v2.components.RendererComponent;
import com.atlassian.util.concurrent.ResettableLazyReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.plugin.renderercomponent.RendererComponentFactoryDescriptor.USE_OLD_IMAGE_RENDERER_ORDER;


/**
 * Creates a wiki renderer.
 */
public class WikiRendererFactory {
    private static final Logger logger = LoggerFactory.getLogger(WikiRendererFactory.class);

    private final ResettableLazyReference<V2RendererFacade> wikiRendererRef = new ResettableLazyReference<V2RendererFacade>() {
        @Override
        protected V2RendererFacade create() throws Exception {
            initializeComponents();
            return new V2RendererFacade(ComponentAccessor.getComponent(RendererConfiguration.class),
                    ComponentAccessor.getComponent(LinkRenderer.class),
                    ComponentAccessor.getComponent(EmbeddedResourceRenderer.class),
                    ComponentAccessor.getComponent(Renderer.class));
        }

        private void initializeComponents() {
            final ArrayList<RendererComponent> components = new ArrayList<>();
            final MutableRenderer renderer = (MutableRenderer) ComponentAccessor.getComponent(Renderer.class);

            final PluginAccessor pluginAccessor = ComponentAccessor.getComponentOfType(PluginAccessor.class);
            final List<RendererComponentFactoryDescriptor> descriptors = new ArrayList<>(pluginAccessor.getEnabledModuleDescriptorsByClass(RendererComponentFactoryDescriptor.class));

            Collections.sort(descriptors, ModuleDescriptorComparator.COMPARATOR);

            for (final RendererComponentFactoryDescriptor descriptor : descriptors) {
                final PluggableRendererComponentFactory rendererComponentFactory = descriptor.getModule();
                if (rendererComponentFactory != null) {
                    final RendererComponent rendererComponent = rendererComponentFactory.getRendererComponent();
                    if (rendererComponent != null) {
                        components.add(rendererComponent);
                    } else {
                        logger.warn("Renderer component factory " + rendererComponentFactory + " returned null renderer component");
                    }
                } else {
                    logger.warn("Got null renderer component factory module from descriptor " + descriptor);
                }
            }
            // TokenRendererComponent must always appear at the end of the list of renderers as it does substitutions
            // that other renderers can specify.
            Collections.sort(components, TokenRendererAwareRendererComparator.COMPARATOR);
            // now change the state of the renderer so that it is useful
            renderer.setComponents(components);
        }
    };

    public V2RendererFacade getWikiRenderer() {
        return wikiRendererRef.get();
    }

    @EventListener
    public void onPluginModuleEnabled(final PluginModuleEnabledEvent event) {
        onPluginModuleEvent(event.getModule());
    }

    @EventListener
    public void onPluginModuleDisabled(final PluginModuleDisabledEvent event) {
        onPluginModuleEvent(event.getModule());
    }

    @EventListener
    public void onPluginEnabled(final PluginEnabledEvent event) {
        onPluginEvent(event.getPlugin());
    }

    @EventListener
    public void onPluginDisabled(final PluginDisabledEvent event) {
        onPluginEvent(event.getPlugin());
    }

    @EventListener
    public void onFeatureEnabled(final FeatureEnabledEvent event) {
        resetOnFeatureChange(event);
    }

    @EventListener
    public void onFeatureDisabled(final FeatureDisabledEvent event) {
        resetOnFeatureChange(event);
    }

    private void resetOnFeatureChange(final FeatureEvent event) {
        if ((USE_OLD_IMAGE_RENDERER_ORDER.equals(event.feature()))) {
            wikiRendererRef.reset();
        }
    }

    private void onPluginModuleEvent(final ModuleDescriptor<?> descriptor) {
        if (descriptor instanceof RendererComponentFactoryDescriptor || descriptor instanceof RendererComponentDecoratorFactoryDescriptor) {
            // refresh underlying renderer components
            wikiRendererRef.reset();
        }
    }

    private void onPluginEvent(final Plugin plugin) {
        plugin.getModuleDescriptors().stream().forEach(this::onPluginModuleEvent);
    }

}
