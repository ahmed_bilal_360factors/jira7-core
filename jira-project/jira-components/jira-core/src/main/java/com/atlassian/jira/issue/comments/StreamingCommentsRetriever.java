package com.atlassian.jira.issue.comments;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.issue.ActionConstants;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.model.querydsl.ActionDTO;
import com.atlassian.jira.model.querydsl.QAction;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.JiraDateUtils;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.sql.SQLQuery;
import com.querydsl.sql.StatementOptions;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * This class is used to ensure that all the comments for a given issue are not loaded into memory.
 *
 * @since 7.2
 */
public class StreamingCommentsRetriever {
    private static final int COMMENTS_FETCH_SIZE = 10000;
    private final QueryDslAccessor queryDslAccessor;
    private final ProjectRoleManager projectRoleManager;
    private final UserManager userManager;
    private final CommentPermissionManager commentPermissionManager;
    private final DatabaseConfig databaseConfiguration;

    public StreamingCommentsRetriever(
            QueryDslAccessor queryDslAccessor,
            ProjectRoleManager projectRoleManager,
            UserManager userManager,
            CommentPermissionManager commentPermissionManager,
            DatabaseConfigurationManager databaseConfigurationManager
    ) {
        this.queryDslAccessor = queryDslAccessor;
        this.projectRoleManager = projectRoleManager;
        this.userManager = userManager;
        this.commentPermissionManager = commentPermissionManager;
        this.databaseConfiguration = databaseConfigurationManager.getDatabaseConfiguration();
    }

    /**
     * This method iterates over each comment of the given issue and executes a callback on it.
     * Comments are iterated over in the order that they were created in, and those that are not
     * visible to the logged in user are filtered out.
     *
     * @param user
     * @param issue     issue for which to fetch comments
     * @return          the total number of visible comments for the issue
     */
    public Stream<Comment> stream(final @Nullable ApplicationUser user, final @Nonnull Issue issue) {
        return StreamSupport.stream(new Spliterators.AbstractSpliterator<ActionDTO>(Long.MAX_VALUE, Spliterator.IMMUTABLE) {
            ActionDTO lastComment = null;
            Spliterator<ActionDTO> batch = Spliterators.emptySpliterator();
            boolean lastBatch = false;

            @Override
            public boolean tryAdvance(final Consumer<? super ActionDTO> action) {
                boolean advanced = batch.tryAdvance(action);

                if (!advanced && !lastBatch) {
                    List<ActionDTO> comments = getCommentsForBatch(issue, lastComment);
                    if (!comments.isEmpty()) {
                        lastComment = comments.get(comments.size() - 1);
                    }
                    if (comments.size() < COMMENTS_FETCH_SIZE) {
                        lastBatch = true;
                    }

                    batch = comments.spliterator();

                    return batch.tryAdvance(action);
                }

                return advanced;
            }

        }, false)
                .map(dto -> convertToComment(dto, issue))
                .filter(c -> commentPermissionManager.hasBrowsePermission(user, c));
    }

    @Nonnull
    private List<ActionDTO> getCommentsForBatch(final Issue issue, final ActionDTO lastComment) {
        return queryDslAccessor.executeQuery(con -> {
            final BooleanBuilder where = new BooleanBuilder();
            where.and(QAction.ACTION.issue.eq(issue.getId()));
            where.and(QAction.ACTION.type.eq(ActionConstants.TYPE_COMMENT));
            if (lastComment != null) {
                where.and(QAction.ACTION.created.gt(lastComment.getCreated())
                    .or(QAction.ACTION.created.eq(lastComment.getCreated())
                        .and(QAction.ACTION.id.gt(lastComment.getId()))
                    )
                );
            }

            return newSqlQuery(con)
                    .select(QAction.ACTION)
                    .from(QAction.ACTION)
                    .where(where)
                    .orderBy(QAction.ACTION.created.asc(), QAction.ACTION.id.asc())
                    .limit(COMMENTS_FETCH_SIZE)
                    .fetch();
        });
    }

    private SQLQuery<?> newSqlQuery(final DbConnection con) {
        SQLQuery<?> query = con.newSqlQuery();
        int fetchSize;
        if (databaseConfiguration.isMySql()) {
            fetchSize = Integer.MIN_VALUE;
        } else {
            fetchSize = COMMENTS_FETCH_SIZE;
        }
        query.setStatementOptions(StatementOptions.builder().setFetchSize(fetchSize).build());
        return query;
    }

    private Comment convertToComment(ActionDTO actionDTO, Issue issue) {
        CommentImpl comment = new CommentImpl(
                projectRoleManager,
                userManager.getUserByKeyEvenWhenUnknown(actionDTO.getAuthor()),
                userManager.getUserByKeyEvenWhenUnknown(actionDTO.getUpdateauthor()),
                actionDTO.getBody(),
                actionDTO.getLevel(),
                actionDTO.getRolelevel(),
                JiraDateUtils.copyDateNullsafe(actionDTO.getCreated()),
                JiraDateUtils.copyDateNullsafe(actionDTO.getUpdated()),
                issue
        );

        comment.setId(actionDTO.getId());
        return comment;
    }
}
