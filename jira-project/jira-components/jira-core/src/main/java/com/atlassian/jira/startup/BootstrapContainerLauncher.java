package com.atlassian.jira.startup;


import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.database.DatabaseDriverRegisterer;
import com.atlassian.jira.config.database.DatabaseType;
import com.atlassian.jira.config.database.InvalidDatabaseDriverException;
import com.atlassian.jira.config.webwork.WebworkConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;

/**
 * The BootstrapContainerLauncher will bootstrap enough of JIRA during run level 0.  It has the ability to check the
 * state of the database connection and configure it if need be.  If the database is operational then it will move to
 * the next level and start JIRA completely.
 *
 * @see ComponentContainerLauncher
 */
public class BootstrapContainerLauncher implements JiraLauncher {
    private static final Logger log = LoggerFactory.getLogger(BootstrapContainerLauncher.class);

    public void start() {
        try {
            bootstrapJIRA();
        } catch (RuntimeException rte) {
            log.error("A RuntimeException occurred during BootstrapContainerLauncher servlet context initialisation - " + rte.getMessage() + ".", rte);
            throw rte;
        } catch (Error error) {
            log.error("An Error occurred during BootstrapContainerLauncher servlet context initialisation - " + error.getMessage() + ".", error);
            throw error;
        }
    }

    private void bootstrapJIRA() {
        try {
            ComponentManager.getInstance().bootstrapInitialise();

            if (JiraStartupChecklist.startupOK()) {
                // if the database is not setup yet, then we need the setup bootstrap container to be in play.
                DatabaseConfigurationManager dbcm = ComponentAccessor.getComponent(DatabaseConfigurationManager.class);
                if (!dbcm.isDatabaseSetup()) {
                    bootstrapJIRAWhenDBIsNotSetup(dbcm);
                    return;
                }
            }
            // We currently have the bootstrap container in play, but to render Johnson events from here onwards we
            // need a few extra components. Let's make sure we can actually render them then ;-)
            ComponentManager.getInstance().extendBootstrapContainerForFailedStartup();
        } catch (Exception ex) {
            log.error("A fatal error occurred during bootstrapping. JIRA has been locked.", ex);
            String message = ex.getMessage() == null ? ex.getClass().getSimpleName() : ex.getMessage();
            JiraStartupChecklist.setFailedStartupCheck(new FailedStartupCheck("Component Manager", message));
        }
    }

    private void bootstrapJIRAWhenDBIsNotSetup(final DatabaseConfigurationManager dbcm) {
        // initialise the SetupContainer
        ComponentManager.getInstance().setupInitialise();
        // check to see if the driver is not setup
        try {
            dbcm.getDatabaseConfiguration();
        } catch (InvalidDatabaseDriverException e) {
            String errorMessage = getErrorMessageForDatabase(e);
            JiraStartupChecklist.setFailedStartupCheck(new FailedStartupCheck("Database Driver Check", errorMessage));
            return;
        } catch (Exception e) {
            //swallow everything except the driver exception
            if (e.getCause() instanceof FileNotFoundException) {
                dbcm.createDbConfigFromEntityDefinition();
            }
        }
        ComponentManager.getInstance().start();

        // we need to have configuration setup for webwork here
        WebworkConfigurator.setupConfiguration();
    }

    public void stop() {
        // nothing to stop
    }

    private String getErrorMessageForDatabase(InvalidDatabaseDriverException e) {
        DatabaseDriverRegisterer registrar = DatabaseDriverRegisterer.forType(DatabaseType.forJdbcDriverClassName(e.driverClassName()));
        StringBuilder errorMessage = new StringBuilder("");
        if (registrar != null) {
            for (String s : registrar.getErrorMessage()) {
                errorMessage.append(s);
                errorMessage.append("\n");
            }
        } else {
            errorMessage.append(e.getMessage());
        }
        return errorMessage.toString();
    }
}
