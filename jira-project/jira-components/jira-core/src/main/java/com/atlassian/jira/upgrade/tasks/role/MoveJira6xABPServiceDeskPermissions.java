package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.GroupWithAttributes;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.auditing.AssociatedItem;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.util.lang.Pair;
import com.google.common.base.Joiner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Collections.emptyList;

/**
 * Migrates Service Desk permission for Agent Based Pricing (this also includes Role Based Pricing licenses in the old
 * store). This task should be only called if Service Desk license is present and it is of ABP or RBP type.
 * <p>
 * Migration works as follows:
 * For group that have SD_AGENT permission:
 * <pre>
 * 1. If all users in the group have USE permissions group will be added to Service Desk application role;
 *  additionally: if group is named 'service-desk-agent' and has  'synch.created.by.jira.service.desk' attribute and
 *  does not have ADMIN permissions it will be set as default application role for service desk
 *
 * 2. If all users in the group do not have USE permission then nothing will happen
 *
 * 3. If some users in the group have USE and others don't then group and its users will be logged, but group
 * will not be given Service Desk application roles, which means that after migration some of the users that had access
 * to Service Desk will not be able lose it.
 * </pre>
 *
 * @since v7.0
 */
public class MoveJira6xABPServiceDeskPermissions extends MigrationTask {
    public static final String PROPERTY_SERVICEDESK_NOT_MIGRATED_GROUPS = "renaissance.migration.servicedesk.not.migrated.groups";

    private static final String GROUP_ATTR_SERVICE_DESK = "synch.created.by.jira.service.desk";
    private static final String GROUP_ATTR_SERVICE_DESK_VALUE = "synch.created.by.jira.service.desk";
    private static final Group GROUP_SD_AGENTS = new ImmutableGroup("service-desk-agents");

    private final MigrationGroupService migrationGroupService;
    private final GlobalPermissionDao globalPermissionDao;
    private final ApplicationProperties applicationProperties;

    MoveJira6xABPServiceDeskPermissions(final MigrationGroupService migrationGroupService,
                                        final GlobalPermissionDao globalPermissionDao, final ApplicationProperties applicationProperties) {
        this.migrationGroupService = migrationGroupService;
        this.globalPermissionDao = globalPermissionDao;
        this.applicationProperties = applicationProperties;
    }

    @Override
    MigrationState migrate(MigrationState state, final boolean licenseSuppliedByUser) {
        final Set<Group> sdAgentGroups = globalPermissionDao.groupsWithSdAgentPermission();
        final List<String> notMigratedGroups = new ArrayList<>();
        for (Group sdAgentGroup : sdAgentGroups) {
            Pair<MigrationState, Boolean> result = checkGroupAndMigrate(sdAgentGroup, state);
            if (!result.second()) {
                notMigratedGroups.add(sdAgentGroup.getName());
            }
            state = result.first();
        }
        return state.withAfterSaveTask(() -> saveNotMigratedGroups(notMigratedGroups));
    }

    /**
     * Migrates agent group to service desk application role
     *
     * @return Pair of new state and boolean indicating whether group could have been migrated (true) or migration
     * has been impossible due to privileges escalation (not every user of the group have use)
     */
    private Pair<MigrationState, Boolean> checkGroupAndMigrate(final Group sdAgentGroup, final MigrationState state) {
        final Set<UserWithPermissions> usersInAgentGroup = migrationGroupService.getUsersInGroup(sdAgentGroup);

        final Set<String> agentsWithoutUsePermission = new HashSet<>();

        boolean allUsersHaveUse = true;
        boolean anyUserHasUse = false;
        for (UserWithPermissions user : usersInAgentGroup) {
            final boolean hasUse = user.hasAdminPermission() || user.hasUsePermission();
            allUsersHaveUse &= hasUse;
            anyUserHasUse |= hasUse;
            if (!hasUse) {
                agentsWithoutUsePermission.add(user.getUser().getName());
            }
        }

        if (allUsersHaveUse) {
            MigrationState migrationState = state.changeApplicationRole(ApplicationKeys.SERVICE_DESK, role -> migrateGroup(role, sdAgentGroup));
            final MigrationChangedValue changedValue = new MigrationChangedValue(sdAgentGroup.getName(),
                    "USE (permission), Agent (permission)",
                    ApplicationKeys.SERVICE_DESK.value() + " (role)");
            final AuditEntry event = new AuditEntry(getClass(), "Group added to Service Desk: " + sdAgentGroup.getName(),
                    "Group was migrated as all users have both JIRA USE and Agent permissions",
                    AssociatedItem.Type.APPLICATION_ROLE,
                    ApplicationKeys.SERVICE_DESK.value(),
                    EVENT_SHOWS_IN_CLOUD_LOG,
                    changedValue);
            return Pair.of(migrationState.log(event), true);
        } else {
            final int numUsersInAgentsGroup = usersInAgentGroup.size();
            if (anyUserHasUse) {
                StringBuilder builder = new StringBuilder()
                        .append("Service Desk Migration - cannot add Service Desk application role to group: '")
                        .append(sdAgentGroup.getName()).append("'\n")
                        .append("Group contains users with misconfigured permissions - not all users in the group are ")
                        .append("able to log in to JIRA, migration would cause privilege escalation.\n")
                        .append("To fix this issue, disable these users or remove them from the group ")
                        .append("then associate that group with the Service Desk application.\n")
                        .append("Agents without use permission:\n");

                String sep = "";
                for (String userName : agentsWithoutUsePermission) {
                    builder.append(sep).append(userName);
                    sep = ", ";
                }
                MigrationState result = state.log(new AuditEntry(MoveJira6xABPServiceDeskPermissions.class,
                        "Service Desk not migrated", builder.toString(), AssociatedItem.Type.APPLICATION_ROLE, null,
                        EVENT_SHOWS_IN_CLOUD_LOG, emptyList(), AuditEntrySeverity.WARNING));
                return Pair.of(result, false);
            } else {
                MigrationState result = state.log(
                        new AuditEntry(MoveJira6xABPServiceDeskPermissions.class, "Service Desk not migrated",
                                "There are " + numUsersInAgentsGroup + " users with Service Desk Agent permission who"
                                        + " do not have JIRA USE permission. In order to avoid allowing these users to log"
                                        + " in, the group '" + sdAgentGroup.getName() + "' has not been assigned to"
                                        + " Service Desk.",
                                AssociatedItem.Type.APPLICATION_ROLE));
                return Pair.of(result, true);
            }
        }

    }

    private ApplicationRole migrateGroup(ApplicationRole applicationRole, Group sdAgentGroup) {
        ApplicationRole roleWithGroup = applicationRole.addGroup(sdAgentGroup);
        if (shouldSetGroupAsDefault(sdAgentGroup)) {
            roleWithGroup = roleWithGroup.addGroupAsDefault(sdAgentGroup);
        }

        return roleWithGroup;
    }

    private boolean shouldSetGroupAsDefault(final Group sdAgentGroup) {
        final GroupWithAttributes groupWithAttributes = migrationGroupService.getGroupWithAttributes(sdAgentGroup);
        final boolean groupHasAdmin = globalPermissionDao.groupsWithAdminPermission().contains(sdAgentGroup);

        return !groupHasAdmin && groupWithAttributes != null && GROUP_SD_AGENTS.equals(sdAgentGroup)
                && GROUP_ATTR_SERVICE_DESK_VALUE.equals(groupWithAttributes.getValue(GROUP_ATTR_SERVICE_DESK));
    }

    private void saveNotMigratedGroups(final List<String> groups) {
        if (groups.isEmpty()) {
            applicationProperties.setString(PROPERTY_SERVICEDESK_NOT_MIGRATED_GROUPS, null);
        } else {
            applicationProperties.setString(PROPERTY_SERVICEDESK_NOT_MIGRATED_GROUPS, Joiner.on(", ").join(groups));
        }
    }
}
