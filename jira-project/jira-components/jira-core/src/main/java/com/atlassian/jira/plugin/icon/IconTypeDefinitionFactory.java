package com.atlassian.jira.plugin.icon;

import com.atlassian.jira.icon.IconType;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * A factory to get {@link IconTypeDefinition}s from plugins for a given IconType.
 */
public interface IconTypeDefinitionFactory {
    /**
     * Get the type definition for the icon type.
     * @param iconType The icon type to look for.
     * @return The definition, or null if there is no definition for this icon type.
     */
    @Nullable
    IconTypeDefinition getDefinition(@Nonnull IconType iconType);

    /**
     * Get the default system icon filename for this icon type.
     * @param iconType The icon type to look for.
     * @return The filename.
     */
    @Nonnull
    String getDefaultSystemIconFilename(@Nonnull IconType iconType);
}
