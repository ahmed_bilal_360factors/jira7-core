package com.atlassian.jira.project.template.descriptor;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.plugin.webfragment.descriptors.ConditionDescriptorFactory;
import com.atlassian.jira.project.template.module.DemoProjectModule;
import com.atlassian.jira.project.template.module.DemoProjectModuleBuilder;
import com.atlassian.jira.project.template.module.Icon;
import com.atlassian.jira.util.lang.Pair;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.Resources;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.descriptors.ConditionalDescriptor;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Element;

import javax.annotation.Nonnull;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.stream.Stream;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * @since v7.1
 */
public class DemoProjectModuleDescriptor extends AbstractModuleDescriptor<DemoProjectModule> implements ConditionalDescriptor {

    private final ResourceDescriptorFactory resourceDescriptorFactory;
    private final WebResourceUrlProvider urlProvider;
    private final ConditionDescriptorFactory conditionDescriptorFactory;

    private Element element;
    private DemoProjectModule module;
    private Condition condition;

    public DemoProjectModuleDescriptor(final ModuleFactory moduleFactory,
                                       final ResourceDescriptorFactory resourceDescriptorFactory,
                                       final WebResourceUrlProvider urlProvider,
                                       final ConditionDescriptorFactory conditionDescriptorFactory) {
        super(moduleFactory);
        this.resourceDescriptorFactory = resourceDescriptorFactory;
        this.urlProvider = urlProvider;
        this.conditionDescriptorFactory = conditionDescriptorFactory;
    }

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException {
        super.init(plugin, element);
        createModuleFromXml(element);
    }

    @Override
    public void enabled() {
        super.enabled();
        condition = conditionDescriptorFactory.retrieveCondition(getPlugin(), element);
    }

    @Override
    public void disabled() {
        super.disabled();
        condition = null;
    }

    @Override
    public DemoProjectModule getModule() {
        return module;
    }

    private Pair<Icon, ResourceDescriptor> createIconFor(Element element) {
        if (element == null) {
            return null;
        }
        final String location = element.attributeValue("location");
        final String contentType = StringUtils.trimToNull(element.attributeValue("content-type"));
        if (isBlank(location)) {
            return null;
        }

        final String resourceName = Paths.get(location).getFileName().toString();

        ResourceDescriptor resource = resourceDescriptorFactory.makeResourceDescriptorNode(resourceName, location, contentType);
        return Pair.of(new Icon(urlProvider, location, getCompleteKey(), resource.getName()), resource);
    }

    private void createModuleFromXml(Element element) {
        final DemoProjectModuleBuilder builder = new DemoProjectModuleBuilder();

        final Optional<Integer> weight = ofNullable(element.attributeValue("weight")).map(Integer::valueOf);
        final Pair<Icon, ResourceDescriptor> icon = createIconFor(element.element("icon"));
        final Optional<Pair<Icon, ResourceDescriptor>> backgroundIcon = ofNullable(createIconFor(element.element("backgroundIcon")));

        module = builder.setKey(getKey())
                .setWeight(weight.orElse(100))
                .setLabelKey(checkNotNull(element.element("label").attributeValue("key")))
                .setDescriptionKey(checkNotNull(element.element("description").attributeValue("key")))
                .setLongDescriptionKey(ofNullable(element.element("longDescription")).map(e -> e.attributeValue("key")))

                .setIcon(checkNotNull(icon).first())
                .setBackgroundIcon(backgroundIcon.map(Pair::first))

                .setProjectTemplateKey(ofNullable(element.element("projectTemplate")).map(e -> e.attributeValue("key")))
                .setProjectTypeKey(ofNullable(element.element("projectType")).map(e -> e.attributeValue("key")))
                .setImportFile(checkNotNull(element.element("importFile").attributeValue("location")))
                .setModulePlugin(getPlugin())
                .createDemoProjectModule();

        resources = new Resources(Stream.of(Optional.of(icon), backgroundIcon)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(Pair::second)
                .collect(CollectorsUtil.toImmutableList())
        );

        this.element = element;
    }

    @Override
    public Condition getCondition() {
        return condition;
    }
}
