package com.atlassian.jira.security.groups;

import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.QueryCallback;
import com.atlassian.jira.database.SqlPredicates;
import com.atlassian.jira.model.querydsl.QMembership;
import com.atlassian.jira.model.querydsl.QUser;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.sql.SQLQuery;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A query-callback that retrieves user names that exist in all groups in a directory.
 * Usually the set of groups will be a single group or a set of nested groups.
 * All the rules of Crowd groups are followed, including directory shadowing; nested groups per directory;
 * inactive users ignored.
 */
public class FilterUsersInAllGroupsQueryCallback implements QueryCallback<List<FilterUsersInAllGroupsQueryCallback.Result>> {
    /**
     * The maximum number of group names we'll allow before switching to use literals instead of parameters in SQL Server.
     * This avoids SQL Server's 2000 parameter limit per query.
     * We'll assume there are no more than 100 additional parameters beyond the list of expanded group names.
     */
    private static final int SQL_SERVER_USE_LITERALS_THRESHOLD = SqlPredicates.MAX_SQL_SERVER_PARAMETER_LIMIT - 100;

    /**
     * The directory ID
     */
    private final Long directoryId;

    private final UserIsNotShadowedPredicate userIsNotShadowedPredicate;

    /**
     * Group names to search
     */
    private final Collection<String> lowerCaseGroupNames;

    /**
     * User names to search
     */
    private final Collection<String> lowerCaseUserNames;

    private final DatabaseConfig dbConfig;

    private final SqlPredicates sqlPredicates;

    /**
     * Constructor
     *
     * @param directoryId                the directory ID to count within
     * @param higherPriorityDirectoryIds higher-priority directory IDs, used to filter out shadowed users
     * @param groupNames                 the expanded list of nested group names
     * @param dbConfig                   database config
     */
    @SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
    public FilterUsersInAllGroupsQueryCallback(final Long directoryId, final Collection<Long> higherPriorityDirectoryIds, final Collection<String> userNames, final Collection<String> groupNames, DatabaseConfig dbConfig) {
        this.directoryId = directoryId;
        userIsNotShadowedPredicate = new UserIsNotShadowedPredicate(higherPriorityDirectoryIds);
        lowerCaseGroupNames = groupNames.stream().map(IdentifierUtils::toLowerCase).collect(Collectors.toSet());
        lowerCaseUserNames = userNames.stream().map(IdentifierUtils::toLowerCase).collect(Collectors.toSet());
        this.dbConfig = dbConfig;
        sqlPredicates = new SqlPredicates(dbConfig);
    }

    @Override
    public List<Result> runQuery(final DbConnection dbConnection) {
        QMembership m = new QMembership("m");
        QUser u = new QUser("u");

        // Predicate to only include users in their canonical directory (i.e. they are not shadowed)
        BooleanExpression userIsNotShadowed = userIsNotShadowedPredicate.apply(u);

        // Query to get active users who are not shadowed, and are a member of at least one of these groups
        SQLQuery<Tuple> query = dbConnection.newSqlQuery()
                .select(u.userName, m.parentName)
                .from(u).innerJoin(m).on(u.lowerUserName.eq(m.lowerChildName))
                .where(u.active.eq(1).and(u.directoryId.eq(directoryId))
                        .and(userIsNotShadowed)
                        .and(m.membershipType.eq("GROUP_USER"))
                        .and(m.directoryId.eq(directoryId))
                        .and(sqlPredicates.partitionedIn(m.lowerChildName, lowerCaseUserNames))
                        .and(sqlPredicates.partitionedIn(m.lowerParentName, lowerCaseGroupNames)))
                .orderBy(u.lowerUserName.asc(), m.lowerChildName.asc());


        // SQL Server has a limit of 2000 parameters per query. If we hit that, use literals instead
        if (dbConfig.isSqlServer() && lowerCaseGroupNames.size() > SQL_SERVER_USE_LITERALS_THRESHOLD) {
            query.setUseLiterals(true);
        }

        return query.fetch().stream()
                .map(tuple -> new Result(tuple.get(u.userName), tuple.get(m.parentName)))
                .collect(Collectors.toList());
    }

    public static class Result {
        private final String userName;
        private final String groupName;

        public Result(String userName, String groupName) {
            this.userName = userName;
            this.groupName = groupName;
        }

        public String getUserName() {
            return userName;
        }

        public String getGroupName() {
            return groupName;
        }
    }
}
