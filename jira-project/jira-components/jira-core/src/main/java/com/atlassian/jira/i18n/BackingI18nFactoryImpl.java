package com.atlassian.jira.i18n;

import com.atlassian.jira.plugin.language.TranslationTransform;
import com.atlassian.jira.util.i18n.I18nTranslationMode;
import com.atlassian.jira.util.resourcebundle.I18NResourceBundleLoader;
import com.atlassian.jira.web.bean.i18n.TranslationStore;
import com.atlassian.jira.web.bean.i18n.TranslationStoreFactory;

import java.util.Locale;

/**
 * @since v6.2.3
 */
public class BackingI18nFactoryImpl implements BackingI18nFactory {
    private final TranslationStoreFactory storeFactory;
    private final I18NResourceBundleLoader resourceLoader;
    private final I18nTranslationMode translationMode;

    public BackingI18nFactoryImpl(final TranslationStoreFactory storeFactory, final I18NResourceBundleLoader resourceLoader,
                                  final I18nTranslationMode translationMode) {
        this.storeFactory = storeFactory;
        this.resourceLoader = resourceLoader;
        this.translationMode = translationMode;
    }

    @Override
    public BackingI18n create(final Locale locale,
                              final Iterable<? extends TranslationTransform> translationTransforms) {
        final TranslationStore translationStore = storeFactory.createTranslationStore(resourceLoader.load(locale));
        return new BackingI18n(locale, translationMode, translationTransforms, translationStore);
    }
}
