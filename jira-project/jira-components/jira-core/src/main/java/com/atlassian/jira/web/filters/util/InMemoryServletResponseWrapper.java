package com.atlassian.jira.web.filters.util;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.PrintWriter;

public class InMemoryServletResponseWrapper extends HttpServletResponseWrapper {
    private final InMemoryServletOutputStream stream = new InMemoryServletOutputStream();

    public InMemoryServletResponseWrapper(final HttpServletResponse response) {
        super(response);
    }

    @Override
    public PrintWriter getWriter() throws IOException {
        return new PrintWriter(stream);
    }

    @Override
    public InMemoryServletOutputStream getOutputStream() throws IOException {
        return stream;
    }
}
