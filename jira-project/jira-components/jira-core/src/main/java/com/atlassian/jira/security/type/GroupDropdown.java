package com.atlassian.jira.security.type;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.permission.PermissionContext;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserUtils;
import com.atlassian.jira.user.util.UserUtil;
import com.google.common.collect.Sets;

import java.util.Map;
import java.util.Set;

import static java.util.Collections.singletonList;

public class GroupDropdown extends AbstractProjectsSecurityType {
    public static final String DESC = "group";
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public GroupDropdown(JiraAuthenticationContext jiraAuthenticationContext) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    public String getDisplayName() {
        return jiraAuthenticationContext.getI18nHelper().getText("admin.permission.types.group");
    }

    @Override
    public String getType() {
        return DESC;
    }

    @Override
    public boolean hasPermission(Project project, String group) {
        // If there is no user passed to this security type then they have the permission if there is no group set
        return (group == null);
    }

    @Override
    public boolean hasPermission(Issue issue, String parameter) {
        // If there is no user passed to this security type then they have the permission if there is no group set
        return (parameter == null);
    }

    @Override
    public boolean hasPermission(Project project, String parameter, ApplicationUser user, boolean issueCreation) {
        return hasPermission(parameter, user);
    }

    @Override
    public boolean hasPermission(Issue issue, String parameter, ApplicationUser user, boolean issueCreation) {
        return hasPermission(parameter, user);
    }

    private boolean hasPermission(String groupName, ApplicationUser user) {
        if (user == null) {
            throw new IllegalArgumentException("User passed must not be null");
        }
        // If there is no group then it is Anyone so it is true
        if (groupName == null) {
            return true;
        }
        GroupManager groupManager = ComponentAccessor.getGroupManager();
        // JDEV-30356: Passing a user object instead of username means less work to do and so is faster
        return groupManager.isUserInGroup(user, groupName);
    }

    @Override
    public void doValidation(String key, Map parameters, JiraServiceContext jiraServiceContext) {
        // No specific validation
    }

    @Override
    public Set<ApplicationUser> getUsers(PermissionContext ctx, String groupName) {
        if (groupName == null) {
            return Sets.newHashSet(UserUtils.getAllUsers());
        }

        return Sets.newHashSet(getUserUtil().getAllUsersInGroupNamesUnsorted(singletonList(groupName)));
    }

    private UserUtil getUserUtil() {
        return ComponentAccessor.getUserUtil();
    }
}
