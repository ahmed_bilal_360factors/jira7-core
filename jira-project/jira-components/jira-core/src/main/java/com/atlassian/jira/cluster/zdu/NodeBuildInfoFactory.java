package com.atlassian.jira.cluster.zdu;

import com.atlassian.jira.cluster.Node;
import com.atlassian.jira.model.querydsl.ClusterUpgradeStateDTO;

/**
 * @since v7.3
 */
public interface NodeBuildInfoFactory {
    NodeBuildInfo create(ClusterUpgradeStateDTO clusterUpgradeStateDTO);
    NodeBuildInfo create(Node node);
    NodeBuildInfo create(long buildNumber, String version);
    NodeBuildInfo currentApplicationInfo();
}
