package com.atlassian.jira.issue.fields;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.model.querydsl.CustomFieldDTO;

import org.ofbiz.core.entity.GenericValue;

/**
 * Factory responsible of instantiating {@link com.atlassian.jira.issue.fields.CustomField} objects.
 */
@PublicApi
public interface CustomFieldFactory {
    /**
     * Creates a new instance of {@link com.atlassian.jira.issue.fields.CustomField}.
     *
     * @param genericValue A {@link GenericValue} that represents the custom field.
     * @return A new instance of {@link com.atlassian.jira.issue.fields.CustomField}, created from the given generic value.
     */
    @Deprecated
    CustomField create(final GenericValue genericValue);

    /**
     * Creates a new instance of {@link com.atlassian.jira.issue.fields.CustomField}.
     *
     * @param customFieldDTO A {@link CustomFieldDTO} that represents the custom field.
     * @return A new instance of {@link com.atlassian.jira.issue.fields.CustomField}, created from the given DTO.
     */
    CustomField create(final CustomFieldDTO customFieldDTO);
}
