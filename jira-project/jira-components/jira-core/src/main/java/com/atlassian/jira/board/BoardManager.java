package com.atlassian.jira.board;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @since v7.1
 */
public interface BoardManager {

    Board createBoard(@Nonnull BoardCreationData boardCreationData);

    Optional<Board> getBoard(BoardId boardId);

    List<Board> getBoardsForProject(long projectId);

    boolean hasBoardForProject(long projectId);

    boolean deleteBoard(BoardId boardId);

    /**
     * Get board data last changed time for given boardId.
     * @param boardId   the board id
     * @return board data last changed time for given boardId.
     */
    Optional<Date> getBoardDataChangedTime(BoardId boardId);
}
