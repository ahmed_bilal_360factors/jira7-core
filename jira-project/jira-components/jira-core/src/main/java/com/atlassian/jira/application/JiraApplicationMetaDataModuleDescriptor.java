package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.host.plugin.ApplicationMetaDataModuleDescriptor;
import com.atlassian.application.host.plugin.DefaultApplicationMetaDataModuleDescriptor;
import com.atlassian.application.host.plugin.PluginApplicationMetaData;
import com.atlassian.jira.project.type.ProjectType;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Element;

import java.util.ArrayList;
import java.util.List;

/**
 * Parses application XML tags and returns {@link JiraPluginApplicationMetaData}. The XML for a JIRA application is the
 * same as any other regular application, except that the definition of project types is allowed:
 * <pre>
 * {@code
 *  <application key="completeModule" name="ModuleDescriptorName">
 *      <applicationKey>com.atlassian.jira.platform</applicationKey>
 *      <applicationName>Test Product</applicationName>
 *      <applicationDescriptionKey>some.key</applicationDescriptionKey>
 *      <configURI>/configureMe</configURI>
 *      <postInstallURI>/postInstall</postInstallURI>
 *      <postUpdateURI>/postUpdate/actions.do</postUpdateURI>
 *      <userCountKey>other.key</userCountKey>
 *      <applicationPlugins>
 *          <plugin>one</plugin>
 *          <plugin>two</plugin>
 *          <plugin>three</plugin>
 *      </applicationPlugins>
 *      <utilityPlugins>
 *          <plugin>two</plugin>
 *          <plugin>three</plugin>
 *          <plugin>four</plugin>
 *      </utilityPlugins>
 *      <defaultGroup>jira-testers</defaultGroup>
 *      <projectTypes>
 *          <projectType>
 *              <key>type1</key>
 *              <descriptionI18nKey>projects.of.first.type</descriptionI18nKey>
 *              <icon>PD94bWwgdmVyc2l (base64 encoded svg)</icon>
 *              <color>#FFFFFF</color>
 *          </projectType>
 *          <projectType>
 *              <key>type2</key>
 *              <descriptionKey>projects.of.second.type</descriptionKey>
 *              <icon>PD94bWwgdmVyc2l (base64 encoded svg)</icon>
 *              <color>#FFFFFF</color>
 *          </projectType>
 *      <projectTypes/>
 *  </application>
 * }
 * </pre>
 * <p>
 * The {@code applicationKey}, {@code applicationName} and {@code applicationDescriptionKey} attributes are required.
 * All other attributes are optional.
 */
public class JiraApplicationMetaDataModuleDescriptor extends AbstractModuleDescriptor<PluginApplicationMetaData>
        implements ApplicationMetaDataModuleDescriptor {
    private final ApplicationMetaDataModuleDescriptor metadataModuleDescriptor;

    private volatile JiraPluginApplicationMetaData jiraModule;

    public JiraApplicationMetaDataModuleDescriptor(final ModuleFactory moduleFactory) {
        super(moduleFactory);
        this.metadataModuleDescriptor = new DefaultApplicationMetaDataModuleDescriptor(moduleFactory);
    }

    @Override
    public void init(final Plugin plugin, final Element element) throws PluginParseException {
        super.init(plugin, element);
        metadataModuleDescriptor.init(plugin, element);
        this.jiraModule = new JiraPluginApplicationMetaData(metadataModuleDescriptor.getModule(), parseProjectTypes(element));
    }

    @Override
    public JiraPluginApplicationMetaData getModule() {
        return jiraModule;
    }

    private List<ProjectType> parseProjectTypes(Element root) {
        final List<ProjectType> projectTypes = new ArrayList<>();
        final Element projectTypesElement = root.element(Elements.PROJECT_TYPES);
        if (projectTypesElement != null) {
            for (Object element : projectTypesElement.elements()) {
                projectTypes.add(parseProjectType((Element) element));
            }
        }
        return projectTypes;
    }

    private ProjectType parseProjectType(Element projectTypeElement) {
        final ProjectTypeKey key = new ProjectTypeKey(getRequiredElement(projectTypeElement, Elements.KEY));
        final String weightString = getRequiredElement(projectTypeElement, Elements.WEIGHT);
        if (!StringUtils.isNumeric(weightString)) {
            throw new PluginParseException(String.format("Element 'weight' must have a numeric value: '%s'", weightString));
        }
        return new ProjectType(
                key,
                getRequiredElement(projectTypeElement, Elements.DESC),
                getRequiredElement(projectTypeElement, Elements.ICON),
                getRequiredElement(projectTypeElement, Elements.COLOR),
                Integer.parseInt(weightString)
        );
    }

    private static String getRequiredElement(final Element root, final String name) {
        final Element element = root.element(name);
        if (element == null) {
            throw new PluginParseException(String.format("Element '%s' is required.", name));
        }
        final String value = StringUtils.stripToNull(element.getText());
        if (value == null) {
            throw new PluginParseException(String.format("Element '%s' must have a value.", name));
        }
        return value;
    }

    @Override
    public ApplicationKey getApplicationKey() {
        return this.metadataModuleDescriptor.getApplicationKey();
    }

    private static final class Elements {
        public static final String KEY = "key";
        public static final String DESC = "descriptionI18nKey";
        public static final String ICON = "icon";
        public static final String COLOR = "color";
        public static final String PROJECT_TYPES = "projectTypes";
        public static final String WEIGHT = "weight";
    }
}
