package com.atlassian.jira.database;

import com.querydsl.sql.OracleTemplates;
import com.querydsl.sql.SQLTemplates;

/**
 * This class is used to preserve the behaviour that querydsl 3 had, namely reserved words are allowed as unquoted table identifiers.
 *
 * @since v7.1
 */
public class JiraOracleTemplates extends OracleTemplates {

    @SuppressWarnings("FieldNameHidesFieldInSuperclass") //Intentional
    public static final JiraOracleTemplates DEFAULT = new JiraOracleTemplates();

    @SuppressWarnings("FieldNameHidesFieldInSuperclass") //Intentional
    public static Builder builder() {
        return new Builder() {
            @Override
            protected SQLTemplates build(char escape, boolean quote) {
                return new JiraOracleTemplates(escape, quote);
            }
        };
    }

    public JiraOracleTemplates() {
        super();
    }

    public JiraOracleTemplates(boolean quote) {
        super(quote);
    }

    public JiraOracleTemplates(char escape, boolean quote) {
        super(escape, quote);
        setSupportsUnquotedReservedWordsAsIdentifier(true);
    }
}
