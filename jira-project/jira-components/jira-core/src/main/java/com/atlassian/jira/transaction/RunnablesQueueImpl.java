package com.atlassian.jira.transaction;

import com.atlassian.ozymandias.SafePluginPointAccess;
import com.google.common.annotations.VisibleForTesting;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A support class that provides the ability to track events and then publish them
 * at the one time (eg via a commit)
 */
@ParametersAreNonnullByDefault
class RunnablesQueueImpl implements RunnablesQueue {

    final Queue<Runnable> runnableQueue;

    RunnablesQueueImpl() {
        this.runnableQueue = new ConcurrentLinkedQueue<>();
    }

    public void offer(Runnable runnable) {
        checkNotNull(runnable);
        runnableQueue.add(runnable);
    }

    public void clear() {
        runnableQueue.clear();
    }

    @Override
    public void runAndClear() {
        Runnable runnable;
        while ((runnable = runnableQueue.poll()) != null) {
            runIt(runnable);
        }
    }

    private void runIt(Runnable codeToRun) {
        SafePluginPointAccess.to().runnable(codeToRun);
    }

    @VisibleForTesting
    Queue<Runnable> queue() {
        return runnableQueue;
    }
}
