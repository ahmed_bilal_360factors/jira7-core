package com.atlassian.jira.license;

import com.atlassian.jira.application.ApplicationKeys;

import static com.atlassian.extras.common.LicensePropertiesConstants.ACTIVE_FLAG;
import static com.atlassian.extras.common.LicensePropertiesConstants.MAX_NUMBER_OF_USERS;
import static com.atlassian.extras.common.LicensePropertiesConstants.NAMESPACE_SEPARATOR;
import static com.atlassian.jira.license.DefaultLicensedApplications.JIRA_PRODUCT_NAMESPACE;

/**
 * Some constants for parsing out Service Desk (SD) licenses. SD has three different types of license:
 * <p>
 * <table summary="License Summary">
 * <tr>
 * <td>License Type</td>
 * <td>SD Version</td>
 * <td>Plugin or JIRA License</td>
 * <td>Description</td>
 * </tr>
 * <tr>
 * <td>Tier Based Pricing (TBP)</td>
 * <td>1.x</td>
 * <td>Plugin</td>
 * <td>This is where SD has to be licensed for the same number of users as JIRA. For example, a JIRA licensed
 * with a JIRA commercial 500 user license will require a SD commercial 500 agent license.
 * This means that every user is a SD agent (even customers).</td>
 * </tr>
 * <tr>
 * <td>Agent Based Pricing (ABP)</td>
 * <td>2.x</td>
 * <td>Plugin</td>
 * <td>This is where SD has its own user limit on the number of agents. The only requirement is that the the
 * SD agent limit be less than or equal to the JIRA user limit. This allows SD to bless a subset of
 * the JIRA users as SD agents (e.g supporters) which allows customers to only pay for the agents
 * they need. As an added advantage people using the customer view are free.</td>
 * </tr>
 * <tr>
 * <td>Role Based Pricing (RBP)</td>
 * <td>3.x</td>
 * <td>JIRA</td>
 * <td>Same as the ABP (mostly) except that it is licensed as a JIRA Product rather than a plugin.</td>
 * </tr>
 * </table>
 *
 * @see {@link LicenseDetailsFactoryImpl}
 * @see {@link com.atlassian.jira.license.DefaultLicensedApplications}
 * @since v7.0
 */
public final class ServiceDeskLicenseConstants {
    public static final String SD_LEGACY_NAMESPACE = "com.atlassian.servicedesk";

    /**
     * The SD active flag in TBP or ABP SD licenses. This flag will also be present in JIRA SD RBP licenses so it must
     * not be used as classifier for TBP/ABP vs RBP licenses.
     */
    public static final String SD_LEGACY_ACTIVE = SD_LEGACY_NAMESPACE + NAMESPACE_SEPARATOR + ACTIVE_FLAG;

    /**
     * The SD user limit property in ABP licenses. This can be used to classify TBP vs ABP SD licenses.
     */
    public static final String SD_USER_COUNT_ABP = SD_LEGACY_NAMESPACE + NAMESPACE_SEPARATOR + "numRoleCount";

    /**
     * The SD active flag in RBP SD license. This flag will not be present in TBP/ABP licenses.
     */
    public static final String SD_RBP_ACTIVE = JIRA_PRODUCT_NAMESPACE + ApplicationKeys.SERVICE_DESK.value() +
            NAMESPACE_SEPARATOR + ACTIVE_FLAG;

    /**
     * The SD user limit property in TBP licenses. This will be present in both ABP and TBP licenses which means it
     * cannot be used as a TBP vs ABP classifier.
     */
    static final String SD_USER_COUNT_TBP = MAX_NUMBER_OF_USERS;

    private ServiceDeskLicenseConstants() {
    }
}
