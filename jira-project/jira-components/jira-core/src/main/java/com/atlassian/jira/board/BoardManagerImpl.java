package com.atlassian.jira.board;

import com.atlassian.jira.board.store.BoardStore;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class BoardManagerImpl implements BoardManager {

    private final BoardStore boardStore;

    public BoardManagerImpl(BoardStore boardStore) {
        this.boardStore = boardStore;
    }

    @Override
    public Board createBoard(@Nonnull BoardCreationData boardCreationData) {
        return boardStore.createBoard(boardCreationData);
    }

    @Override
    public Optional<Board> getBoard(BoardId boardId) {
        return boardStore.getBoard(boardId);
    }

    @Override
    public List<Board> getBoardsForProject(long projectId) {
        return boardStore.getBoardsForProject(projectId);
    }

    @Override
    public boolean hasBoardForProject(long projectId) {
        return boardStore.hasBoardForProject(projectId);
    }

    @Override
    public boolean deleteBoard(BoardId boardId) {
        return boardStore.deleteBoard(boardId);
    }

    @Override
    public Optional<Date> getBoardDataChangedTime(BoardId boardId) {
        return boardStore.getBoardDataChangedTime(boardId);
    }
}
