package com.atlassian.jira.plugin.webpanel.notification;

import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

public final class IsAnyApplicationRoleUserLimitExceeded implements Condition {

    private final ApplicationRoleManager applicationRoleManager;

    public IsAnyApplicationRoleUserLimitExceeded(final ApplicationRoleManager applicationRoleManager) {
        this.applicationRoleManager = applicationRoleManager;
    }


    @Override
    public void init(final Map<String, String> map) {
    }

    @Override
    public boolean shouldDisplay(final Map<String, Object> map) {
        return applicationRoleManager.isAnyRoleLimitExceeded();
    }
}
