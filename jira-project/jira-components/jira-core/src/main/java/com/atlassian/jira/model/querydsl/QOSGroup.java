package com.atlassian.jira.model.querydsl;

import com.querydsl.core.types.dsl.*;
import com.querydsl.sql.ColumnMetadata;

import javax.annotation.Generated;
import java.sql.Types;

/**
 * QOSGroup is a Querydsl query object.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QOSGroup extends JiraRelationalPathBase<OSGroupDTO> {
    public static final QOSGroup O_S_GROUP = new QOSGroup("O_S_GROUP");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final StringPath name = createString("name");

    public QOSGroup(String alias) {
        super(OSGroupDTO.class, alias, "groupbase");
        addMetadata();
    }

    private void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(name, ColumnMetadata.named("groupname").withIndex(2).ofType(Types.VARCHAR).withSize(255));
    }

    /**
     * Returns the ofbiz model's entity name for this query object.
     *
     * @return the ofbiz model's entity name for this query object.
     */
    @Override
    public String getEntityName() {
        return "OSGroup";
    }

    @Override
    public NumberPath<Long> getNumericIdPath() {
        return id;
    }
}

