package com.atlassian.jira.upgrade.tasks.role;

/**
 * An exception that captures all the renaissance migration (i.e. JIRA 6.x single use permission to JIRA 7.x multiple
 * applications) failures.
 *
 * @since v7.0
 */
public class MigrationFailedException extends RuntimeException {
    MigrationFailedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    MigrationFailedException(final Throwable cause) {
        this(cause.getMessage(), cause);
    }

    public MigrationFailedException(final String message) {
        super(message);
    }
}
