package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarTranscoder;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.upgrade.AbstractDelayableUpgradeTask;
import net.spy.memcached.compat.log.Logger;
import net.spy.memcached.compat.log.LoggerFactory;

import java.io.File;
import java.nio.file.Files;

import static org.apache.commons.io.FilenameUtils.removeExtension;

/**
 * Deletes old issue-type-icon PNG-files that has been converted from SVG using
 * {@link AvatarTranscoder}.
 *
 * This is needed as we have updated the set of issue-type-icons and SVG-files are only
 * converted to PNG-files of various sizes once. From there on it simply does a lookup of
 * the PNG-file and returns it. We therefore need to wipe out all the issue-type-icons (PNGs)
 * to be sure the PNG-files are in sync with the new SVG-files.
 */
public class UpgradeTask_Build72002 extends AbstractDelayableUpgradeTask {
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask_Build72002.class);
    private static final String TAG = UpgradeTask_Build72002.class.getSimpleName();

    private final String PNG_EXTENSION = ".png";
    private final AvatarManager avatarManager;

    public UpgradeTask_Build72002(AvatarManager avatarManager) {
        this.avatarManager = avatarManager;
    }

    @Override
    public int getBuildNumber() {
        return 72002;
    }

    @Override
    public String getShortDescription() {
        return "Removing old issue type icon files (PNGs)";
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception {
        File avatarBaseDirectory = avatarManager.getAvatarBaseDirectory();
        for(Avatar avatar : avatarManager.getAllSystemAvatars(IconType.ISSUE_TYPE_ICON_TYPE)) {
            // removing the extension from the filename (e.g. bug.svg -> bug)
            String filename = removeExtension(avatar.getFileName());
            for(Avatar.Size size : Avatar.Size.values()) {
                // PNG files can be stored in two separate files, depending on if you are using
                // 'images/icons/issuetypes/task.svg?format=png' or 'secure/viewavatar?avatarType=issuetype&?avatarId=10318'

                // files are on format: <size>_<iconname>.png (e.g. medium_bug.png)
                String filenamePng = size.getParam() + "_" + filename + PNG_EXTENSION;
                // files are on format: <size>_images_icons_issuetypes_<iconname>.png (e.g. medium_bug.png)
                String filenamePng2 = size.getParam() + "_images_icons_issuetypes_" + filename + PNG_EXTENSION;
                try {
                    File file = new File(avatarBaseDirectory, filenamePng);
                    Files.deleteIfExists(file.toPath());
                    File file2 = new File(avatarBaseDirectory, filenamePng2);
                    Files.deleteIfExists(file2.toPath());
                } catch (Exception e) {
                    log.error(TAG, e);
                }
            }
        }
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        // this upgrade task removes old icon files
        return false;
    }
}
