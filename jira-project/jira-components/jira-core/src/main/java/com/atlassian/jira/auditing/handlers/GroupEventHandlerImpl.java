package com.atlassian.jira.auditing.handlers;

import com.atlassian.crowd.event.group.GroupCreatedEvent;
import com.atlassian.crowd.event.group.GroupDeletedEvent;
import com.atlassian.crowd.event.group.GroupMembershipCreatedEvent;
import com.atlassian.crowd.event.group.GroupMembershipDeletedEvent;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.fugue.Option;
import com.atlassian.jira.auditing.AffectedGroup;
import com.atlassian.jira.auditing.AffectedUser;
import com.atlassian.jira.auditing.AuditingCategory;
import com.atlassian.jira.auditing.RecordRequest;
import com.atlassian.jira.component.ComponentAccessor;


/**
 * @since v6.2
 */
public class GroupEventHandlerImpl implements GroupEventHandler {

    public GroupEventHandlerImpl() {

    }

    @Override
    public Option<RecordRequest> onGroupCreatedEvent(GroupCreatedEvent event) {
        return Option.some(new RecordRequest(AuditingCategory.GROUP_MANAGEMENT, "jira.auditing.group.created")
                .forObject(new AffectedGroup(event.getGroup().getName(), event.getDirectory())));
    }

    @Override
    public Option<RecordRequest> onGroupDeletedEvent(final GroupDeletedEvent event) {
        return Option.some(new RecordRequest(AuditingCategory.GROUP_MANAGEMENT, "jira.auditing.group.deleted")
                .forObject(new AffectedGroup(event.getGroupName(), event.getDirectory())));
    }

    @Override
    public Option<RecordRequest> onGroupMembershipCreatedEvent(final GroupMembershipCreatedEvent event) {
        if (event.getMembershipType().equals(MembershipType.GROUP_GROUP)) {
            return Option.some(new RecordRequest(AuditingCategory.GROUP_MANAGEMENT, "jira.auditing.group.added.to.group")
                    .forObject(new AffectedGroup(event.getGroupName(), event.getDirectory()))
                    //event.getEntityName() returns group name for this MembershipType
                    .withAssociatedItems(new AffectedGroup(event.getEntityName(), event.getDirectory())));
        }
        if (event.getMembershipType().equals(MembershipType.GROUP_USER)) {
            final String userKey = ComponentAccessor.getUserKeyService().getKeyForUsername(event.getEntityName());
            return Option.some(new RecordRequest(AuditingCategory.GROUP_MANAGEMENT, "jira.auditing.user.added.to.group")
                    .forObject(new AffectedGroup(event.getGroupName(), event.getDirectory()))
                    //event.getEntityName() returns user name for this MembershipType
                    .withAssociatedItems(new AffectedUser(event.getEntityName(), userKey, event.getDirectory())));
        }
        throw new IllegalArgumentException("Unknown membership type: " + event.getMembershipType());
    }

    @Override
    public Option<RecordRequest> onGroupMembershipDeletedEvent(final GroupMembershipDeletedEvent event) {
        switch (event.getMembershipType()) {
            case GROUP_GROUP:
                return Option.some(new RecordRequest(AuditingCategory.GROUP_MANAGEMENT, "jira.auditing.group.removed.from.group")
                        //event.getEntityName() returns group name for this MembershipType
                        .forObject(new AffectedGroup(event.getGroupName(), event.getDirectory()))
                        .withAssociatedItems(new AffectedGroup(event.getEntityName(), event.getDirectory())));
            case GROUP_USER:
                final String userKey = ComponentAccessor.getUserKeyService().getKeyForUsername(event.getEntityName());
                return Option.some(new RecordRequest(AuditingCategory.GROUP_MANAGEMENT, "jira.auditing.user.removed.from.group")
                        //event.getEntityName() returns user name for this MembershipType
                        .forObject(new AffectedGroup(event.getGroupName(), event.getDirectory()))
                        .withAssociatedItems(new AffectedUser(event.getEntityName(), userKey, event.getDirectory())));
            default:
                throw new IllegalArgumentException("Unknown membership type: " + event.getMembershipType());
        }
    }

}
