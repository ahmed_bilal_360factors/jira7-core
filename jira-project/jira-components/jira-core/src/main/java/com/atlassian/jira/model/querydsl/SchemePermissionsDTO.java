package com.atlassian.jira.model.querydsl;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Generated;

/**
 * Data Transfer Object for the SchemePermissions entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 *
 * @see QSchemePermissions
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class SchemePermissionsDTO implements DTO {
    private final Long id;
    private final Long scheme;
    private final Long permission;
    private final String type;
    private final String parameter;
    private final String permissionKey;

    public Long getId() {
        return id;
    }

    public Long getScheme() {
        return scheme;
    }

    public Long getPermission() {
        return permission;
    }

    public String getType() {
        return type;
    }

    public String getParameter() {
        return parameter;
    }

    public String getPermissionKey() {
        return permissionKey;
    }

    public SchemePermissionsDTO(Long id, Long scheme, Long permission, String type, String parameter, String permissionKey) {
        this.id = id;
        this.scheme = scheme;
        this.permission = permission;
        this.type = type;
        this.parameter = parameter;
        this.permissionKey = permissionKey;
    }

    /**
     * Creates a GenericValue object from the values in this Data Transfer Object.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
     * @return a GenericValue object constructed from the values in this Data Transfer Object.
     */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator) {
        return ofBizDelegator.makeValue("SchemePermissions", new FieldMap()
                .add("id", id)
                .add("scheme", scheme)
                .add("permission", permission)
                .add("type", type)
                .add("parameter", parameter)
                .add("permissionKey", permissionKey)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */
    public static SchemePermissionsDTO fromGenericValue(GenericValue gv) {
        return new SchemePermissionsDTO(
                gv.getLong("id"),
                gv.getLong("scheme"),
                gv.getLong("permission"),
                gv.getString("type"),
                gv.getString("parameter"),
                gv.getString("permissionKey")
        );
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(SchemePermissionsDTO schemePermissionsDTO) {
        return new Builder(schemePermissionsDTO);
    }

    public static class Builder {
        private Long id;
        private Long scheme;
        private Long permission;
        private String type;
        private String parameter;
        private String permissionKey;

        public Builder() {
        }

        public Builder(SchemePermissionsDTO schemePermissionsDTO) {
            this.id = schemePermissionsDTO.id;
            this.scheme = schemePermissionsDTO.scheme;
            this.permission = schemePermissionsDTO.permission;
            this.type = schemePermissionsDTO.type;
            this.parameter = schemePermissionsDTO.parameter;
            this.permissionKey = schemePermissionsDTO.permissionKey;
        }

        public SchemePermissionsDTO build() {
            return new SchemePermissionsDTO(id, scheme, permission, type, parameter, permissionKey);
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }
        public Builder scheme(Long scheme) {
            this.scheme = scheme;
            return this;
        }
        public Builder permission(Long permission) {
            this.permission = permission;
            return this;
        }
        public Builder type(String type) {
            this.type = type;
            return this;
        }
        public Builder parameter(String parameter) {
            this.parameter = parameter;
            return this;
        }
        public Builder permissionKey(String permissionKey) {
            this.permissionKey = permissionKey;
            return this;
        }
    }
}