package com.atlassian.jira.issue.fields.option;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.QueryCallback;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.util.log.RateLimitingLogger;
import com.google.common.annotations.VisibleForTesting;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static com.atlassian.collectors.CollectorsUtil.toNewArrayListWithSizeOf;
import static com.atlassian.jira.model.querydsl.QFieldConfiguration.FIELD_CONFIGURATION;
import static com.atlassian.jira.model.querydsl.QOptionConfiguration.OPTION_CONFIGURATION;
import static java.util.Objects.requireNonNull;

@ParametersAreNonnullByDefault
public class OptionSetManagerImpl implements OptionSetManager {
    private static final RateLimitingLogger LOG = new RateLimitingLogger(OptionSetManagerImpl.class);

    private final ConstantsManager constantsManager;
    private final QueryDslAccessor queryDslAccessor;

    public OptionSetManagerImpl(ConstantsManager constantsManager, QueryDslAccessor queryDslAccessor) {
        this.constantsManager = constantsManager;
        this.queryDslAccessor = queryDslAccessor;
    }

    @Nonnull
    public OptionSet getOptionsForConfig(FieldConfig config) {
        validateParametersAreNonNull(config);

        return getOptionsForConfig(config.getFieldId(), config.getId());
    }

    @Nonnull
    public OptionSet getOptionsForConfig(@Nonnull final String fieldId, @Nonnull Long configId) {
        return queryDslAccessor.executeQuery(db -> getOptionsForConfig(db, fieldId, configId));
    }

    private OptionSet getOptionsForConfig(DbConnection db, String fieldId, Long configId) {
        final List<String> optionIds = getOptionIds(db, fieldId, configId);
        final List<IssueConstant> constants = toIssueConstants(fieldId, optionIds);
        return new IssueConstantOptionSet(constantsManager, constants);
    }

    private static List<String> getOptionIds(DbConnection db, String fieldId, Long configId) {
        return db.newSqlQuery()
                .select(OPTION_CONFIGURATION.optionid)
                .from(OPTION_CONFIGURATION)
                .where(OPTION_CONFIGURATION.fieldid.eq(fieldId))
                .where(OPTION_CONFIGURATION.fieldconfig.eq(configId))
                .orderBy(OPTION_CONFIGURATION.sequence.asc())
                .fetch();
    }

    private List<IssueConstant> toIssueConstants(String fieldId, List<String> optionIds) {
        return optionIds.stream()
                .map(optionId -> constantsManager.getConstantObject(fieldId, optionId))
                .filter(Objects::nonNull)
                .collect(toNewArrayListWithSizeOf(optionIds));
    }


    @Nonnull
    public OptionSet createOptionSet(FieldConfig config, Collection<String> optionIds) {
        return createOrReplaceOptionSet(config, optionIds);
    }

    @Nonnull
    public OptionSet updateOptionSet(FieldConfig config, Collection<String> optionIds) {
        return createOrReplaceOptionSet(config, optionIds);
    }

    @Override
    @Nonnull
    public OptionSet addOptionToOptionSet(FieldConfig config, String optionId) {
        validateParametersAreNonNull(config);

        final String fieldId = config.getFieldId();
        final Long configId = config.getId();

        return runWithFieldConfigurationLocked(
                config,
                db -> {
                    appendSingleOptionToOptionSet(db, fieldId, configId, optionId);
                    return getOptionsForConfig(db, fieldId, configId);
                }
        );
    }

    @Override
    @Nonnull
    public OptionSet removeOptionFromOptionSet(FieldConfig config, String optionId) {
        validateParametersAreNonNull(config);

        final String fieldId = config.getFieldId();
        final Long configId = config.getId();

        return runWithFieldConfigurationLocked(config, db -> {
            deleteSingleOption(db, fieldId, configId, optionId);
            return getOptionsForConfig(db, fieldId, configId);
        });
    }

    private OptionSet createOrReplaceOptionSet(FieldConfig config, Collection<String> optionIds) {
        validateParametersAreNonNull(config);

        final String fieldId = config.getFieldId();
        final Long configId = config.getId();

        return queryDslAccessor.executeQuery(db -> {
            deleteAllOptionsFromOptionSet(db, fieldId, configId);
            storeAllOptions(db, fieldId, configId, optionIds);
            return getOptionsForConfig(db, fieldId, configId);
        });
    }

    private void storeAllOptions(DbConnection db, String fieldId, Long fieldConfigId, @Nullable Collection<String> optionIds) {
        // For historic reasons, tolerate null for optionIds even though the API now says that we won't.
        if (optionIds == null) {
            LOG.warnWithTrace("Please use removeOptionSet instead of passing in a null collection of optionIds");
            return;
        }

        long seq = 0L;
        for (String optionId : optionIds) {
            db.insert(OPTION_CONFIGURATION)
                    .set(OPTION_CONFIGURATION.fieldconfig, fieldConfigId)
                    .set(OPTION_CONFIGURATION.fieldid, fieldId)
                    .set(OPTION_CONFIGURATION.sequence, seq)
                    .set(OPTION_CONFIGURATION.optionid, optionId)
                    .executeWithId();
            ++seq;
        }
    }

    /**
     * Store a single option into the list of options. This will be placed at the end, so the sequence number of the
     * option will be 1 more than the last current on, or 0 if there are currently no options.
     *
     * @param db       to run the query on
     * @param fieldId  is the type of field that the option is being used for
     * @param optionId for the given field
     */
    private void appendSingleOptionToOptionSet(DbConnection db, String fieldId, Long fieldConfigId, String optionId) {
        final Long seq = getNextSequenceNumber(db, fieldId, fieldConfigId);

        insertSingleOption(db, fieldId, fieldConfigId, optionId, seq);
    }

    private static Long getNextSequenceNumber(DbConnection db, String fieldId, Long fieldConfigId) {
        final Long seq = db.newSqlQuery()
                .select(OPTION_CONFIGURATION.sequence)
                .from(OPTION_CONFIGURATION)
                .where(OPTION_CONFIGURATION.fieldid.eq(fieldId))
                .where(OPTION_CONFIGURATION.fieldconfig.eq(fieldConfigId))
                .orderBy(OPTION_CONFIGURATION.sequence.desc())
                .fetchFirst();

        if (seq == null) {
            return 0L;
        } else {
            return seq + 1;
        }
    }

    /**
     * This is visible testing so we can test race conditions that we do not insert options with the same sequence
     * number.
     */
    @VisibleForTesting
    void insertSingleOption(DbConnection db, String fieldId, Long fieldConfigId, String optionId, Long sequence) {
        db.insert(OPTION_CONFIGURATION)
                .set(OPTION_CONFIGURATION.fieldconfig, fieldConfigId)
                .set(OPTION_CONFIGURATION.fieldid, fieldId)
                .set(OPTION_CONFIGURATION.sequence, sequence)
                .set(OPTION_CONFIGURATION.optionid, optionId)
                .executeWithId();
    }

    private void deleteSingleOption(DbConnection db, String fieldId, Long fieldConfigId, String optionId) {
        db.delete(OPTION_CONFIGURATION)
                .where(OPTION_CONFIGURATION.fieldid.eq(fieldId))
                .where(OPTION_CONFIGURATION.fieldconfig.eq(fieldConfigId))
                .where(OPTION_CONFIGURATION.optionid.eq(optionId))
                .execute();
    }

    public void removeOptionSet(FieldConfig config) {
        validateParametersAreNonNull(config);

        queryDslAccessor.execute(db -> deleteAllOptionsFromOptionSet(db, config.getFieldId(), config.getId()));
    }

    private void deleteAllOptionsFromOptionSet(DbConnection db, String fieldId, Long fieldConfigId) {
        db.delete(OPTION_CONFIGURATION)
                .where(OPTION_CONFIGURATION.fieldid.eq(fieldId))
                .where(OPTION_CONFIGURATION.fieldconfig.eq(fieldConfigId))
                .execute();
    }

    private <T> T runWithFieldConfigurationLocked(final FieldConfig fieldConfig, final QueryCallback<T> callback) {
        return queryDslAccessor.withNewConnection().executeQuery(db -> {
            db.setAutoCommit(false);
            lockFieldConfigurationForEditing(db, fieldConfig.getId());
            final T result = callback.runQuery(db);
            db.commit();
            return result;
        });
    }

    private void lockFieldConfigurationForEditing(DbConnection db, Long fieldConfigId) {
        final List<Long> fieldsLocked = db.newSqlQuery()
                .select(FIELD_CONFIGURATION.id)
                .from(FIELD_CONFIGURATION)
                .where(FIELD_CONFIGURATION.id.eq(fieldConfigId))
                .forUpdate()
                .fetch();

        if (fieldsLocked.isEmpty()) {
            throw new IllegalStateException("Trying to update option set sequences for non-existent field config id=" + fieldConfigId);
        }
    }

    private static void validateParametersAreNonNull(FieldConfig config) {
        requireNonNull(config, "config");
        requireNonNull(config.getFieldId(), "config.fieldId");
        requireNonNull(config.getId(), "config.id");
    }
}
