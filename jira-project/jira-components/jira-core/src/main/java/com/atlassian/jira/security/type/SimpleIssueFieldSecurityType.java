package com.atlassian.jira.security.type;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;

/**
 * The common class for IssueField SecurityTypes that rely on a simple field (ie a field of the Issue Generic Value).
 *
 * @since v4.3
 */
public abstract class SimpleIssueFieldSecurityType extends AbstractIssueFieldSecurityType {
    protected abstract String getField();

    @Override
    protected boolean hasIssuePermission(ApplicationUser user, boolean issueCreation, Issue issue, String parameter) {
        final String appUserKey = ApplicationUsers.getKeyFor(user);
        return appUserKey != null && appUserKey.equals(getFieldValue(issue));
    }


    /**
     * Returns the user key value for the given issue for the field that this security type checks.
     *
     * @param issue the Issue
     * @return the user key value for the given issue for the field that this security type checks.
     */
    protected abstract String getFieldValue(Issue issue);
}
