package com.atlassian.jira.permission.management;

/**
 * Helper used to determine if we want to use the old edit permission page and what the URLs are for these.
 * @since 7.1
 */
public interface ProjectPermissionFeatureHelper {

    /**
     * @return whether we want to use the old project permission page
     */
    Boolean useOldProjectPermissionPage();

    /**
     * Get the old edit permission scheme url
     * @param schemeId to be edited
     * @return url for editing on the old edit permission page
     */
    String getOldEditPermissionUrl(final Long schemeId);

    /**
     * Get the new edit permission scheme url
     * @param schemeId to be edited
     * @return url for editing on the new edit permission page
     */
    String getNewEditPermissionUrl(final Long schemeId);
}
