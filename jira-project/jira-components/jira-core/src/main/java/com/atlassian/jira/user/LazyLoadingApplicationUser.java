package com.atlassian.jira.user;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.User;

/**
 * Application user that is created just from a name and loads its other details from Crowd when needed.
 * <p>
 * Take care not to call equals() and hashCode() unless absolutely needed,
 * as this will cause the full user to be loaded from the database.
 *
 * @since 7.0
 */
public class LazyLoadingApplicationUser implements ApplicationUser {
    private final String name;

    private final CrowdService crowdService;
    private ApplicationUser fullUser;

    public LazyLoadingApplicationUser(String name, CrowdService crowdService) {
        this.name = name;
        this.crowdService = crowdService;
    }

    private synchronized ApplicationUser fullUser() {
        if (fullUser == null) {
            fullUser = readFullUser();
        }

        return fullUser;
    }

    private ApplicationUser readFullUser() {
        User user = crowdService.getUser(name);
        return ApplicationUsers.from(user);
    }

    @Override
    public long getDirectoryId() {
        return fullUser().getDirectoryId();
    }

    @Override
    public User getDirectoryUser() {
        return fullUser().getDirectoryUser();
    }

    @Override
    public String getDisplayName() {
        return fullUser().getDisplayName();
    }

    @Override
    public String getEmailAddress() {
        return fullUser().getEmailAddress();
    }

    @Override
    public String getKey() {
        return fullUser().getKey();
    }

    @Override
    public String getName() {
        //We have this value from the start
        return name;
    }

    @Override
    public String getUsername() {
        return fullUser().getUsername();
    }

    @Override
    public boolean isActive() {
        return fullUser().isActive();
    }

    @Override
    public Long getId() {
        return fullUser().getId();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof ApplicationUser) {
            final ApplicationUser other = (ApplicationUser) obj;
            return getKey().equals(other.getKey());
        }
        if (obj instanceof User) {
            throw new IllegalArgumentException("You must update your code to use ApplicationUser (you passed User to equals here)");
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getKey().hashCode();
    }

    @Override
    public String toString() {
        return getName();
    }
}