package com.atlassian.jira.issue.link;

import com.atlassian.jira.cache.request.RequestCache;
import com.atlassian.jira.cache.request.RequestCacheFactory;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.issue.util.IssueUpdateBean;
import com.atlassian.jira.issue.util.IssueUpdater;
import com.atlassian.jira.model.querydsl.QIssueLink;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.CollectionReorderer;
import com.google.common.collect.ImmutableList;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import static com.atlassian.jira.component.ComponentAccessor.getIssueFactory;
import static com.atlassian.jira.entity.IssueLinkFactory.DESTINATION;
import static com.atlassian.jira.entity.IssueLinkFactory.LINK_TYPE;
import static com.atlassian.jira.entity.IssueLinkFactory.SEQUENCE;
import static com.atlassian.jira.entity.IssueLinkFactory.SOURCE;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

public class DefaultIssueLinkManager implements IssueLinkManager {
    private static final Logger log = LoggerFactory.getLogger(DefaultIssueLinkManager.class);

    private final OfBizDelegator delegator;
    private final QueryDslAccessor queryDslAccessor;
    private final IssueLinkCreator issueLinkCreator;
    private final IssueLinkTypeManager issueLinkTypeManager;
    private final IssueUpdater issueUpdater;
    private final IssueIndexingService issueIndexingService;
    private final ApplicationProperties applicationProperties;

    private static final String OUTWARD_LINKS_CACHE_KEY = DefaultIssueLinkManager.class.getName() + ".outwardLinks";
    private static final String INWARD_LINKS_CACHE_KEY = DefaultIssueLinkManager.class.getName() + ".inwardLinks";
    private final RequestCache<Long, List<IssueLink>> outwardLinksCache;
    private final RequestCache<Long, List<IssueLink>> inwardLinksCache;

    public DefaultIssueLinkManager(OfBizDelegator genericDelegator, QueryDslAccessor queryDslAccessor,
           IssueLinkCreator issueLinkCreator, IssueLinkTypeManager issueLinkTypeManager,
           IssueUpdater issueUpdater, IssueIndexingService issueIndexingService,
           ApplicationProperties applicationProperties, RequestCacheFactory requestCacheFactory) {
        this.delegator = genericDelegator;
        this.queryDslAccessor = queryDslAccessor;
        this.issueLinkCreator = issueLinkCreator;
        this.issueLinkTypeManager = issueLinkTypeManager;
        this.issueUpdater = issueUpdater;
        this.issueIndexingService = issueIndexingService;
        this.applicationProperties = applicationProperties;

        this.outwardLinksCache = requestCacheFactory.createRequestCache(OUTWARD_LINKS_CACHE_KEY,
                (issueId) -> ImmutableList.copyOf(this.getLinks(SOURCE, issueId)));
        this.inwardLinksCache = requestCacheFactory.createRequestCache(INWARD_LINKS_CACHE_KEY,
                (issueId) -> ImmutableList.copyOf(this.getLinks(DESTINATION, issueId)));
    }

    public void createIssueLink(Long sourceId, Long destinationId, Long issueLinkTypeId, Long sequence, ApplicationUser remoteUser)
            throws CreateException {
        //if the link is already created, then don't do anything
        if (getIssueLink(sourceId, destinationId, issueLinkTypeId) != null) {
            return;
        }
        // JRA-30953: if the link type does not exist  throw an exception
        if (!validateIssueLinkType(issueLinkTypeId)) {
            String msg = String.format("There is no IssueLinkType with id: %s", issueLinkTypeId);
            log.error(msg);
            throw new CreateException(msg);
        }

        IssueLink issueLink = null;
        try {
            issueLink = storeIssueLink(sourceId, destinationId, issueLinkTypeId, sequence);

            final IssueLinkType issueLinkType = issueLink.getIssueLinkType();
            // Create change record only if the issue link is not of a system issue link type
            if (!issueLinkType.isSystemLinkType()) {
                // Manually do our changelogs and issue updates as we have two issues
                createCreateIssueLinkChangeItems(issueLink, issueLinkType, remoteUser);
            }
        } finally {
            if (issueLink != null) {
                invalidateRequestCache(issueLink);
                // We always need to reindex linked Issues - the updated date of both issues is updated. see JRA-7156
                reindexLinkedIssues(issueLink);
            }
        }
    }

    protected void reindexLinkedIssues(IssueLink issueLink) {
        try {
            issueIndexingService.reIndex(issueLink.getSourceObject());
            issueIndexingService.reIndex(issueLink.getDestinationObject());
        } catch (IndexException e) {
            throw new RuntimeException(e);
        }
    }

    private List<IssueLink> getIssueLinks(final Map<String, ?> key) {
        List<GenericValue> result = delegator.findByAnd(OfBizDelegator.ISSUE_LINK, key);
        if (result == null) {
            result = Collections.emptyList();
        }
        return buildIssueLinks(result);
    }

    private void createCreateIssueLinkChangeItems(IssueLink issueLink, IssueLinkType issueLinkType, ApplicationUser remoteUser) {
        final Issue source = issueLink.getSourceObject();
        final Issue destination = issueLink.getDestinationObject();

        // Create change item for source issue
        ChangeItemBean cib = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "Link", null, null, destination.getKey(),
                "This issue " + issueLinkType.getOutward() + ' ' + destination.getKey());
        createChangeItem(source, cib, remoteUser);

        // Create change item for destination issue
        cib = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "Link", null, null, source.getKey(),
                "This issue " + issueLinkType.getInward() + ' ' + source.getKey());
        createChangeItem(destination, cib, remoteUser);
    }

    private void createChangeItem(Issue issue, ChangeItemBean changeItemBean, ApplicationUser remoteUser) {
        // Note the event will not be dispatched. The issue is updated however so we pass ISSUE_UPDATED constant
        IssueUpdateBean issueUpdateBean = new IssueUpdateBean(issue, issue, EventType.ISSUE_UPDATED_ID, remoteUser);
        issueUpdateBean.setDispatchEvent(false);
        issueUpdateBean.setChangeItems(ImmutableList.of(changeItemBean));
        issueUpdater.doUpdate(issueUpdateBean, true);
    }

    public void removeIssueLink(IssueLink issueLink, ApplicationUser remoteUser) {
        removeIssueLinkInternal(issueLink, remoteUser, true);
    }

    /**
     * Does the work.
     *
     * @param issueLink  the Issue Link
     * @param remoteUser Nullable if we dont want to create a Change Item
     */
    private void removeIssueLinkInternal(IssueLink issueLink, ApplicationUser remoteUser, boolean createChangeItem) {
        if (issueLink == null) {
            throw new IllegalArgumentException("Link cannot be null");
        }

        try {
            // Delete the link type from the database
            delegator.removeByAnd(OfBizDelegator.ISSUE_LINK, FieldMap.build("id", issueLink.getId()));

            if (log.isDebugEnabled()) {
                log.debug("Deleted link with id '" + issueLink.getId() + "'.");
            }

            // Do we want a changeItem?
            if (createChangeItem) {
                // Create change record only if the issue link is not of a system issue link type
                final IssueLinkType issueLinkType = issueLink.getIssueLinkType();
                if (!issueLinkType.isSystemLinkType()) {
                    createRemoveIssueLinkChangeItems(issueLink, issueLinkType, remoteUser);
                }
            }
        } finally {
            invalidateRequestCache(issueLink);
            // We always need to reindex after removing a link - the updated date of both issues is updated.
            // See JRA-7156, and JRA-14877
            reindexLinkedIssues(issueLink);
        }
    }

    private void createRemoveIssueLinkChangeItems(IssueLink issueLink, IssueLinkType issueLinkType, ApplicationUser remoteUser) {
        final Issue source = issueLink.getSourceObject();
        final Issue destination = issueLink.getDestinationObject();

        // Create change item for source issue
        ChangeItemBean cib = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "Link", destination.getKey(),
                "This issue " + issueLinkType.getOutward() + ' ' + destination.getKey(), null, null);
        createChangeItem(source, cib, remoteUser);

        // Create change item for destination issue
        cib = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "Link", source.getKey(),
                "This issue " + issueLinkType.getInward() + ' ' + source.getKey(), null, null);
        createChangeItem(destination, cib, remoteUser);
    }

    @Override
    public int removeIssueLinks(Issue issue, ApplicationUser remoteUser) {
        return removeIssueLinksInternal(issue, remoteUser, true);
    }

    @Override
    public int removeIssueLinks(GenericValue issue, ApplicationUser remoteUser) {
        if (issue == null) {
            return 0;
        }

        return removeIssueLinksInternal(getIssueFactory().getIssue(issue), remoteUser, true);
    }

    @Override
    public int removeIssueLinksNoChangeItems(Issue issue) {
        return removeIssueLinksInternal(issue, null, false);
    }

    /**
     * Does the work.
     *
     * @param issue
     * @param remoteUser       null if we do not log a change item
     * @param createChangeItem createChangeItem?
     * @return
     */
    private int removeIssueLinksInternal(Issue issue, ApplicationUser remoteUser, boolean createChangeItem) {
        List<IssueLink> outwardLinks = getOutwardLinks(issue.getId());
        deleteIssueLinksFromIssue(outwardLinks, remoteUser, createChangeItem);
        int totalLinksDeleted = outwardLinks.size();
        if (log.isDebugEnabled()) {
            log.debug("Deleted " + outwardLinks.size() + " outward links from issue " + issue.getKey());
        }

        List<IssueLink> inwardLinks = getInwardLinks(issue.getId());
        deleteIssueLinksFromIssue(inwardLinks, remoteUser, createChangeItem);
        totalLinksDeleted += inwardLinks.size();
        if (log.isDebugEnabled()) {
            log.debug("Deleted " + inwardLinks.size() + " inward links from issue " + issue.getKey());
        }
        return totalLinksDeleted;
    }

    private void deleteIssueLinksFromIssue(List<IssueLink> issueLinks, ApplicationUser remoteUser, boolean createChangeItem) {
        if (issueLinks != null) {
            for (final IssueLink issueLink : issueLinks) {
                removeIssueLinkInternal(issueLink, remoteUser, createChangeItem);
            }
        }
    }

    public LinkCollection getLinkCollection(GenericValue issue, ApplicationUser remoteUser) {
        return _getLinkCollection(getIssueFactory().getIssue(issue), remoteUser, false, true);
    }

    public LinkCollection getLinkCollection(final Issue issue, final ApplicationUser remoteUser) {
        return _getLinkCollection(issue, remoteUser, false, true);
    }

    @Override
    public LinkCollection getLinkCollection(Issue issue, ApplicationUser remoteUser, boolean excludeSystemLinks) {
        return _getLinkCollection(issue, remoteUser, false, excludeSystemLinks);
    }

    public LinkCollection getLinkCollectionOverrideSecurity(final Issue issue) {
        return _getLinkCollection(issue, null, true, true);
    }

    private LinkCollection _getLinkCollection(final Issue issue, final ApplicationUser remoteUser,
                                              final boolean overrideSecurity, boolean excludeSystemLinks) {
        Set<IssueLinkType> linkTypes = new TreeSet<>();
        Map<String, List<Issue>> outwardLinkMap = new HashMap<>();
        Collection<IssueLink> outwardLinks = getOutwardLinks(issue.getId());

        if (outwardLinks != null) {
            for (final IssueLink issueLink : outwardLinks) {
                IssueLinkType issueLinkType = issueLinkTypeManager.getIssueLinkType(issueLink.getLinkTypeId(),
                        excludeSystemLinks);

                if (!excludeSystemLinks || !issueLinkType.isSystemLinkType()) {
                    linkTypes.add(issueLinkType);

                    Issue linkedIssue = issueLink.getDestinationObject();
                    if (issueLinkType.isSubTaskLinkType() && linkedIssue instanceof MutableIssue) {
                        ((MutableIssue) linkedIssue).setParentObject(issue);
                    }
                    storeInLinkMap(outwardLinkMap, issueLinkType.getName(), linkedIssue);
                }
            }
        }

        Collection<IssueLink> inwardLinks = getInwardLinks(issue.getId());
        Map<String, List<Issue>> inwardLinkMap = new HashMap<>();

        if (inwardLinks != null) {
            for (final IssueLink issueLink : inwardLinks) {
                IssueLinkType issueLinkType = issueLinkTypeManager.getIssueLinkType(issueLink.getLinkTypeId(),
                        excludeSystemLinks);

                if (!excludeSystemLinks || !issueLinkType.isSystemLinkType()) {
                    linkTypes.add(issueLinkType);

                    Issue linkedIssue = issueLink.getSourceObject();
                    storeInLinkMap(inwardLinkMap, issueLinkType.getName(), linkedIssue);
                }
            }
        }

        return new LinkCollectionImpl(issue.getId(), linkTypes, outwardLinkMap, inwardLinkMap, remoteUser,
                overrideSecurity, applicationProperties);
    }

    public List<IssueLink> getOutwardLinks(final Long sourceId) {
        return Optional.ofNullable(sourceId)
                .map(outwardLinksCache::get)
                .orElse(ImmutableList.of());
    }

    public List<IssueLink> getInwardLinks(final Long destinationId) {
        return Optional.ofNullable(destinationId)
                .map(inwardLinksCache::get)
                .orElse(ImmutableList.of());
    }

    private List<IssueLink> getLinks(final String fieldId, final Long issueId) {
        return Select.from(Entity.ISSUE_LINK)
                .whereEqual(fieldId, issueId)
                .runWith(delegator)
                .asList();
    }

    public void moveIssueLink(List<IssueLink> issueLinks, Long currentSequence, Long sequence) {
        if (currentSequence == null) {
            throw new IllegalArgumentException("Current sequence cannot be null.");
        }

        if (sequence == null) {
            throw new IllegalArgumentException("Sequence cannot be null.");
        }

        final int currentIndex = currentSequence.intValue();
        final int index = sequence.intValue();

        CollectionReorderer.moveToPosition(issueLinks, currentIndex, index);

        resetSequences(issueLinks);
    }

    public void resetSequences(final List<IssueLink> issueLinks) {
        queryDslAccessor.execute(dbConnection -> {
            long i = 0;
            for (final IssueLink issueLink : issueLinks) {
                dbConnection.update(QIssueLink.ISSUE_LINK)
                        .set(QIssueLink.ISSUE_LINK.sequence, i)
                        .where(QIssueLink.ISSUE_LINK.id.eq(issueLink.getId()))
                        .execute();

                i++;
            }
        });
        invalidateRequestCache();
    }

    public IssueLink getIssueLink(Long sourceId, Long destinationId, Long issueLinkTypeId) {
        List<IssueLink> links = getIssueLinks(FieldMap.build(SOURCE, sourceId));
        for (IssueLink link : links) {
            if (link.getDestinationId().equals(destinationId) && link.getLinkTypeId().equals(issueLinkTypeId)) {
                return link;
            }
        }
        return null;
    }

    public Collection<IssueLink> getIssueLinks(Long issueLinkTypeId) {
        return getIssueLinks(FieldMap.build(LINK_TYPE, issueLinkTypeId));
    }

    @Override
    public IssueLink getIssueLink(Long issueLinkId) {
        notNull("issueLinkId", issueLinkId);
        GenericValue issueLinkGV = delegator.findByPrimaryKey(OfBizDelegator.ISSUE_LINK, issueLinkId);
        if (issueLinkGV == null) {
            return null;
        }
        return issueLinkCreator.createIssueLink(issueLinkGV);
    }

    public void changeIssueLinkType(IssueLink issueLink, IssueLinkType swapLinkType, ApplicationUser remoteUser) {
        final IssueLinkType oldIssueLinkType = issueLink.getIssueLinkType();
        if (!oldIssueLinkType.isSystemLinkType() && swapLinkType.isSystemLinkType()) {
            log.warn("Changing non-system link type to a system link type.");
        } else if (oldIssueLinkType.isSystemLinkType() && !swapLinkType.isSystemLinkType()) {
            log.warn("Changing system link type to a non-system link type.");
        }

        try {
            updateIssueLinkType(issueLink, swapLinkType);
        } finally {
            invalidateRequestCache(issueLink);
        }

        // If the link we are swapping from is not a system link type, it means its creation should have
        // been recorded in 'change history', so we should update change history here.
        if (!oldIssueLinkType.isSystemLinkType()) {
            createRemoveIssueLinkChangeItems(issueLink, oldIssueLinkType, remoteUser);
            createCreateIssueLinkChangeItems(issueLink, swapLinkType, remoteUser);
        }
    }

    private void updateIssueLinkType(final IssueLink issueLink, final IssueLinkType issueLinkType) {
        queryDslAccessor.execute(dbConnection -> dbConnection.update(QIssueLink.ISSUE_LINK)
                .set(QIssueLink.ISSUE_LINK.linktype, issueLinkType.getId())
                .where(QIssueLink.ISSUE_LINK.id.eq(issueLink.getId()))
                .execute());
    }

    public boolean isLinkingEnabled() {
        return applicationProperties.getOption(APKeys.JIRA_OPTION_ISSUELINKING);
    }

    private IssueLink storeIssueLink(Long sourceId, Long destinationId, Long issueLinkTypeId, Long sequence) {
        // create the outward link from issue -> destination
        FieldMap fields = FieldMap.build(SOURCE, sourceId, DESTINATION, destinationId,
                LINK_TYPE, issueLinkTypeId, SEQUENCE, sequence);
        return buildIssueLink(delegator.createValue(OfBizDelegator.ISSUE_LINK, fields));
    }

    private List<IssueLink> buildIssueLinks(final Collection<GenericValue> issueLinkGVs)
    {
        List<IssueLink> issueLinks = new ArrayList<>(issueLinkGVs.size());
        for (final GenericValue issueLinkGV : issueLinkGVs) {
            issueLinks.add(buildIssueLink(issueLinkGV));
        }
        return issueLinks;
    }


    private IssueLink buildIssueLink(GenericValue issueLinkGV) {
        return issueLinkCreator.createIssueLink(issueLinkGV);
    }

    private void storeInLinkMap(Map<String, List<Issue>> linkMap, String linkTypeName, Issue linkedIssue) {
        List<Issue> matchingLinks = linkMap.get(linkTypeName);

        if (matchingLinks == null) {
            matchingLinks = new ArrayList<>();
            linkMap.put(linkTypeName, matchingLinks);
        }

        matchingLinks.add(linkedIssue);
    }

    private void invalidateRequestCache() {
        inwardLinksCache.removeAll();
        outwardLinksCache.removeAll();
    }

    private void invalidateRequestCache(final IssueLink issueLink) {
        outwardLinksCache.remove(issueLink.getSourceId());
        inwardLinksCache.remove(issueLink.getDestinationId());
    }

    private boolean validateIssueLinkType(long linkTypeId) {
        return issueLinkTypeManager.getIssueLinkType(linkTypeId, false) != null;
    }
}
