package com.atlassian.jira.project.type;

import com.atlassian.fugue.Option;

import java.util.Optional;

/**
 * Default implementation for {@link ProjectTypeIconRenderer}
 */
public class ProjectTypeIconRendererImpl implements ProjectTypeIconRenderer {
    private final ProjectTypeManager projectTypeManager;

    public ProjectTypeIconRendererImpl(ProjectTypeManager projectTypeManager) {
        this.projectTypeManager = projectTypeManager;
    }

    @Override
    public Optional<String> getIconToRender(ProjectTypeKey key) {
        if (key == null || key.getKey() == null) {
            return getInaccessibleProjectTypeIcon();
        }

        Option<ProjectType> projectType = projectTypeManager.getByKey(key);
        if (projectType.isDefined()) {
            return Optional.of(projectType.get().getIcon());
        }
        return getInaccessibleProjectTypeIcon();
    }

    private Optional<String> getInaccessibleProjectTypeIcon() {
        return Optional.of(projectTypeManager.getInaccessibleProjectType().getIcon());
    }
}
