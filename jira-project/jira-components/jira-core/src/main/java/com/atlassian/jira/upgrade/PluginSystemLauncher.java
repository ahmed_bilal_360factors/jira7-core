package com.atlassian.jira.upgrade;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.event.ComponentManagerShutdownEvent;
import com.atlassian.jira.startup.FailedStartupCheck;
import com.atlassian.jira.startup.JiraLauncher;
import com.atlassian.jira.startup.JiraStartupChecklist;
import com.atlassian.jira.util.ExceptionUtil;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Starts the JIRA plugins system with ALL available plugins
 *
 * @since v4.4
 */
public class PluginSystemLauncher implements JiraLauncher {
    private static final Logger log = LoggerFactory.getLogger(PluginSystemLauncher.class);
    public static final String INSTANT_DOWN_PROPERTY = "skip.plugin.system.shutdown";
    
    private final JiraProperties jiraSystemProperties;

    public PluginSystemLauncher(final JiraProperties jiraSystemProperties) {
        this.jiraSystemProperties = jiraSystemProperties;
    }

    /**
     * Start the plugin system. If there is no tenant, this will only start phase 1. Other plugins will be delayed.
     */
    @Override
    public void start() {
        try {
            ComponentManager.getInstance().start();
            clearTomcatResources();
        } catch (final Exception ex) {
            log.error("A fatal error occured during initialisation. JIRA has been locked.", ex);
            JiraStartupChecklist.setFailedStartupCheck(new FailedStartupCheck("Component Manager", ExceptionUtil.getMessage(ex)));
        }
    }

    /**
     * Start delayed plugins (if any). Plugins will have been delayed if there was no tenant when this launcher was originally
     * started
     */
    public void lateStart() {
        ComponentManager.getInstance().lateStart();
    }

    /**
     * If we are running in tomcat that uses WebappClassLoader lets clear resources cache. This prevents from excessive
     * load of resources (tomcat6) or classes (tomcat7) binary data. For reference see
     * https://jdog.jira-dev.com/browse/JDEV-28620. More information about tomcat here:
     * http://issues.apache.org/bugzilla/show_bug.cgi?id=53081 https://issues.apache.org/bugzilla/show_bug.cgi?id=56293
     *
     * @deprecated we keep this as long as we will allow to upgrade from versions that were released before May 2014
     */
    private void clearTomcatResources() {
        try {
            final ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
            final Class<? extends ClassLoader> contextClassLoaderClass = contextClassLoader.getClass();
            if ("org.apache.catalina.loader.WebappClassLoader".equals(contextClassLoaderClass.getName())) {
                final Field resourceEntriesField = getResourceEntriesField(contextClassLoaderClass);
                resourceEntriesField.setAccessible(true);
                final Map resourceEntries = (Map) resourceEntriesField.get(contextClassLoader);
                // this mimics the behaviour of WebappClassLoader where writes to this field are guarded by lock on the
                // resourceEntries
                //noinspection SynchronizationOnLocalVariableOrMethodParameter
                // JZERO-148 - also sychronize on the classloader itself to avoid deadlock
                synchronized (contextClassLoader) {
                    synchronized (resourceEntries) {
                        resourceEntries.clear();
                    }
                }
            }
        } catch (final NoSuchFieldException exception) {
            //It is safe to ignore those as we just will not clean the cache
            log.warn(String.format("Tomcat's WebappClassLoader cache for resourceEntries not cleared."
                            + "Probably using different Tomcat version than 6, 7 or 8 Message: %s",
                    ExceptionUtil.getMessage(exception)
            ));
        } catch (final IllegalAccessException exception) {
            log.warn("Tomcat's WebappClassLoader cache for resourceEntries not cleared. This should never happen?",
                    exception);
        } catch (final SecurityException exception) {
            log.warn("Tomcat's WebappClassLoader cache for resourceEntries not cleared. Have we started to use security managers?",
                    exception);
        } catch (final Exception exception) {
            log.warn(String.format("Tomcat's WebappClassLoader cache for resourceEntries not cleared. Message: %s",
                    ExceptionUtil.getMessage(exception)));
        }

    }

    @VisibleForTesting
    protected static Field getResourceEntriesField(final Class<? extends ClassLoader> contextClassLoaderClass)
            throws NoSuchFieldException {
        final Stream<Class<?>> superClassStream = superclassStream(contextClassLoaderClass);

        final Optional<Field> resourceEntriesField =
                superClassStream
                        .flatMap(aClass -> Arrays.stream(aClass.getDeclaredFields()))
                        .filter(field -> field.getName().equals("resourceEntries"))
                        .findFirst();

        return resourceEntriesField.orElseThrow(() -> new NoSuchFieldException("Can't find resourceEntries in class tree of: " + contextClassLoaderClass.getName()));
    }

    /**
     * Returns stream that contains this class and all its superclasses in order.
     */
    private static Stream<Class<?>> superclassStream(final Class<?> clazz) {
        final Stream.Builder<Class<?>> superclassStreamBuilder = Stream.builder();
        for (Class<?> current = clazz; current != null; current = current.getSuperclass()) {
            superclassStreamBuilder.accept(current);
        }

        return superclassStreamBuilder.build();
    }

    @Override
    public void stop() {
        if (jiraSystemProperties.getBoolean(INSTANT_DOWN_PROPERTY)) {
            ComponentManager.getComponent(EventPublisher.class).publish(ComponentManagerShutdownEvent.INSTANCE);
        } else {
            ComponentManager.getInstance().stop();
        }
    }
}
