package com.atlassian.jira.tenancy;

import com.atlassian.jira.config.properties.JiraProperties;


/**
 * Helper class to test if tenancy is enabled.  Can be controlled by either system property or dark feature.
 * By default is always true for OnDemand instances.
 *
 * @since v6.4
 */
public class JiraTenancyCondition implements TenancyCondition {
    private final JiraProperties jiraProperties;

    public JiraTenancyCondition(final JiraProperties jiraProperties) {
        this.jiraProperties = jiraProperties;
    }

    public boolean isEnabled() {
        return jiraProperties.getBoolean("atlassian.tenancy.enabled");
    }
}
