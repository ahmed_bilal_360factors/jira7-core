package com.atlassian.jira.project;

import java.util.Optional;

public class ProjectChangedTimeManagerImpl implements ProjectChangedTimeManager {
    private final ProjectChangedTimeStore projectChangedTimeStore;

    public ProjectChangedTimeManagerImpl(ProjectChangedTimeStore projectChangedTimeStore) {
        this.projectChangedTimeStore = projectChangedTimeStore;
    }

    @Override
    public Optional<ProjectChangedTime> getProjectChangedTime(long projectId) {
        return projectChangedTimeStore.getProjectChangedTime(projectId);
    }
}
