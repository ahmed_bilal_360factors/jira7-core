package com.atlassian.jira.servermetrics;

import com.atlassian.core.filters.AbstractHttpFilter;

import javax.annotation.Nullable;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This filter manages metrics collection in current request.
 */
public class MetricsCollectorFilter extends AbstractHttpFilter {
    private final ServletSafeComponentReference<RequestMetricsDispatcher> requestStatsCollector =
            ServletSafeComponentReference.build(RequestMetricsDispatcher.class);

    public MetricsCollectorFilter() {
    }

    @Override
    protected void doFilter(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws IOException, ServletException {
        try (RequestMetricsDispatcher.CollectorHandle ignored = startCollection(httpServletRequest)) {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        }
    }

    @Nullable
    private RequestMetricsDispatcher.CollectorHandle startCollection(HttpServletRequest httpServletRequest) {
        return requestStatsCollector.get()
                .map(requestStatsCollector -> requestStatsCollector.startCollection(httpServletRequest))
                .orElse(null);
    }
}
