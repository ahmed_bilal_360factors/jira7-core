package com.atlassian.jira.security.roles;

import com.atlassian.jira.project.Project;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This is an interface that defines the storage class for ProjectRoles and RoleActors.
 */
public interface ProjectRoleAndActorStore {
    ProjectRole addProjectRole(ProjectRole projectRole);

    void updateProjectRole(ProjectRole projectRole);

    Collection<ProjectRole> getAllProjectRoles();

    ProjectRole getProjectRole(Long id);

    ProjectRole getProjectRoleByName(String name);

    void deleteProjectRole(ProjectRole projectRole);

    /**
     * Get the {@link ProjectRoleActors} for a given project role ID and project ID.
     *
     * @param projectRoleId the ID of the project role.
     * @param projectId     the ID of the project.
     * @return A {@link ProjectRoleActors} object containing the role actors for the given project with the given
     * project role id. If none are found, a {@link ProjectRoleActors} object with an empty set will be returned.
     */
    ProjectRoleActors getProjectRoleActors(@Nonnull Long projectRoleId, Long projectId);

    /**
     * Get all of the {@link ProjectRoleActors} that have a given project role ID, including the default ones (null project).
     *
     * @param projectRoleId the ID of the project role.
     * @return A set of project role actors for the given project role ID. If there are no ProjectRoleActors for that
     * ID, it will return an empty set.
     */
    @Nonnull Collection<ProjectRoleActors> getProjectRoleActorsByRoleId(@Nonnull Long projectRoleId);

    void updateProjectRoleActors(ProjectRoleActors projectRoleActors);

    void updateDefaultRoleActors(DefaultRoleActors defaultRoleActors);

    /**
     * Get the default {@link ProjectRoleActors} for a given project role ID.
     *
     * @param projectRoleId the ID of the project role.
     * @return A {@link ProjectRoleActors} object containing the default role actors for the given project. If none are
     * found, a {@link ProjectRoleActors} object with an empty set will be returned.
     */
    DefaultRoleActors getDefaultRoleActors(@Nonnull Long projectRoleId);

    void applyDefaultsRolesToProject(Project project);

    void removeAllRoleActorsByKeyAndType(String key, String type);

    void removeAllRoleActorsByProject(Project project);

    Collection<Long> getProjectIdsContainingRoleActorByKeyAndType(String key, String type);

    List<Long> roleActorOfTypeExistsForProjects(List<Long> projectsToLimitBy, ProjectRole projectRole, String projectRoleType, String projectRoleParameter);

    Map<Long, List<String>> getProjectIdsForUserInGroupsBecauseOfRole(List<Long> projectsToLimitBy, ProjectRole projectRole, String projectRoleType, String userKey);

    boolean isGroupUsed(@Nonnull final String group);
}
