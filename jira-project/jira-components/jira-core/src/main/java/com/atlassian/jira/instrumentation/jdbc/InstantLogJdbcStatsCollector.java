package com.atlassian.jira.instrumentation.jdbc;

import com.atlassian.instrumentation.driver.Instrumentation;
import com.atlassian.instrumentation.instruments.Context;
import com.atlassian.instrumentation.instruments.EventType;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import java.io.IOException;
import java.util.Collections;

/**
 * Collects jdbc statics from jdbc instrumentation and logs them as soon as they come.
 *
 * @since v7.2
 */
public class InstantLogJdbcStatsCollector implements Instrumentation.SplitFactory {
    private final Logger log;

    private static final ImmutableList<EventType> SUPPORTED_EVENTS = ImmutableList.of(EventType.CONNECTION_POOL,
            EventType.EXECUTION);

    private final ObjectWriter writer = new ObjectMapper().writer();


    public InstantLogJdbcStatsCollector(Logger log) {
        this.log = log;
    }

    public void register() {
        Instrumentation.registerFactory(this);
    }

    public void unregister() {
        Instrumentation.unregisterFactory(this);
    }

    @Override
    public Instrumentation.Split startSplit(Context context) {
        return context.getEventType()
                .filter(SUPPORTED_EVENTS::contains)
                .map((event) -> {
                    long startTime = System.currentTimeMillis();
                    return (Instrumentation.Split) (() -> eventEnded(context, System.currentTimeMillis() - startTime));
                })
                .orElse(() -> {
                });
    }

    private void eventEnded(Context context, long durationInMs) {
        try {
            final ImmutableMap<String, Object> data = ImmutableMap.<String, Object>builder()
                    .put("query", context.getSql().orElse(null))
                    .put("time", durationInMs)
                    .put("event", context.getEventType().orElse(null))
                    .put("stackTrace", StackTraceHelper.filterStackTrace(context.getStackTrace().orElse(Collections.emptyList())))
                    .build();
            log.info(writer.writeValueAsString(data));
        } catch (IOException e) {
            log.debug("Failed to write instrumentation log", e);
        }
    }
}
