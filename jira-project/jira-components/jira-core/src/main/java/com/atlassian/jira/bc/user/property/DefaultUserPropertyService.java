package com.atlassian.jira.bc.user.property;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.user.UserPropertyService;
import com.atlassian.jira.entity.property.BaseEntityWithKeyPropertyService;
import com.atlassian.jira.entity.property.DelegatingEntityWithKeyPropertyService;
import com.atlassian.jira.entity.property.JsonEntityPropertyManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;

/**
 * @since v6.5
 */
public class DefaultUserPropertyService extends DelegatingEntityWithKeyPropertyService<ApplicationUser> implements UserPropertyService {

    public DefaultUserPropertyService(JsonEntityPropertyManager jsonEntityPropertyManager,
                                      I18nHelper i18nHelperDelegate,
                                      EventPublisher eventPublisher,
                                      UserPropertyHelper propertyHelper) {
        super(new BaseEntityWithKeyPropertyService<ApplicationUser>(jsonEntityPropertyManager, i18nHelperDelegate, eventPublisher, propertyHelper));
    }
}
