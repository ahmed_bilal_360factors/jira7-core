package com.atlassian.jira.security;

import com.atlassian.cache.CacheManager;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.CoreFeatures;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.group.GroupConfigurable;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.event.permission.GlobalPermissionAddedEvent;
import com.atlassian.jira.event.permission.GlobalPermissionDeletedEvent;
import com.atlassian.jira.license.LicenseCountService;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.GlobalPermissionType;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.plugin.GlobalPermissionTypesManager;
import com.atlassian.jira.security.type.GroupDropdown;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.RecoveryMode;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;
import static com.atlassian.jira.permission.GlobalPermissionKey.SYSTEM_ADMIN;
import static com.atlassian.jira.permission.GlobalPermissionKey.USE;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static java.util.Collections.unmodifiableCollection;

@EventComponent
public class DefaultGlobalPermissionManager implements GlobalPermissionManager, GroupConfigurable {
    /**
     * Permissions that are managed by User Management in On Demand mode.
     */
    private static final ImmutableSet<GlobalPermissionKey> ONDEMAND_UM_MANAGED_PERMISSIONS = ImmutableSet.of(ADMINISTER, SYSTEM_ADMIN, USE);

    private final GlobalPermissionsCache globalPermissionsCache;
    private final CrowdService crowdService;
    private final OfBizDelegator ofBizDelegator;
    private final EventPublisher eventPublisher;
    private final GlobalPermissionTypesManager globalPermissionTypesManager;
    private final GroupManager groupManager;
    private final RecoveryMode recoveryMode;
    private final ApplicationRoleManager applicationRoleManager;
    private final FeatureManager featureManager;

    public DefaultGlobalPermissionManager(final CrowdService crowdService, final OfBizDelegator ofBizDelegator,
                                          final EventPublisher eventPublisher, final GlobalPermissionTypesManager globalPermissionTypesManager,
                                          final CacheManager cacheManager,
                                          final ApplicationRoleManager applicationRoleManager,
                                          final GroupManager groupManager,
                                          final RecoveryMode recoveryMode, final FeatureManager featureManager) {
        this.crowdService = crowdService;
        this.ofBizDelegator = ofBizDelegator;
        this.eventPublisher = eventPublisher;
        this.globalPermissionTypesManager = globalPermissionTypesManager;
        this.recoveryMode = notNull("recoveryMode", recoveryMode);
        this.groupManager = groupManager;
        this.globalPermissionsCache = new GlobalPermissionsCache(ofBizDelegator, cacheManager);
        this.applicationRoleManager = notNull("applicationRoleManager", applicationRoleManager);
        this.featureManager = notNull("featureManager", featureManager);
    }

    @SuppressWarnings("unused")
    @EventListener
    public void onClearCache(final ClearCacheEvent event) {
        globalPermissionsCache.clearCache();
    }

    @Override
    public Collection<GlobalPermissionType> getAllGlobalPermissions() {
        // The USE Global Permission is deprecated as of JIRA 7 and is excluded by globalPermissionTypesManager
        return globalPermissionTypesManager.getAll();
    }

    @Override
    public Option<GlobalPermissionType> getGlobalPermission(final int permissionId) {
        final GlobalPermissionKey translatedPermissionKey = GlobalPermissionKey.GLOBAL_PERMISSION_ID_TRANSLATION.get(permissionId);
        if (translatedPermissionKey == null) {
            return Option.none();
        }
        return getGlobalPermission(translatedPermissionKey);
    }

    @Override
    public Option<GlobalPermissionType> getGlobalPermission(@Nonnull final String permissionKey) {
        return globalPermissionTypesManager.getGlobalPermission(permissionKey);
    }

    @Override
    public Option<GlobalPermissionType> getGlobalPermission(@Nonnull final GlobalPermissionKey permissionKey) {
        return globalPermissionTypesManager.getGlobalPermission(permissionKey);
    }

    /**
     * Adds a global permission
     *
     * @param permissionId must be a global permission type
     * @param group        can be null if it is anyone permission
     * @return True if the permission was added
     */
    @Override
    public boolean addPermission(final int permissionId, final String group) {
        return getGlobalPermission(permissionId).fold(() -> {
                    throw new IllegalArgumentException("Permission id passed must be a global permission, " + permissionId + " is not");
                },
                globalPermissionType -> addPermission(globalPermissionType, group)
        );
    }

    @Override
    public boolean addPermission(@Nonnull GlobalPermissionType globalPermissionType, final String group) {
        // When roles enabled, passing USE as a permission is unsupported; roles should be used
        ensureUsePermissionNotUsed(globalPermissionType.getGlobalPermissionKey());

        // as a final check we don't allow the group Anyone (null) to be added to a permission without anonymous allowed.  It should be protected by the UI
        // so as a last resort we check it here.
        if (!globalPermissionType.isAnonymousAllowed() && (group == null)) {
            throw new IllegalArgumentException("The group Anyone cannot be added to the global permission JIRA Users");
        }

        ofBizDelegator.createValue("GlobalPermissionEntry",
                FieldMap.build("permission", globalPermissionType.getKey())
                        .add("group_id", group));
        globalPermissionsCache.clearCache();
        clearActiveUserCountIfNecessary(globalPermissionType.getGlobalPermissionKey());

        eventPublisher.publish(new GlobalPermissionAddedEvent(globalPermissionType, group));
        return true;
    }

    @Override
    public Collection<JiraPermission> getPermissions(final int permissionType) {
        // When roles enabled, passing USE as a permission is unsupported; roles should be used
        ensureUsePermissionNotUsed(permissionType);

        Option<GlobalPermissionType> globalPermissionOpt = getGlobalPermission(permissionType);
        if (globalPermissionOpt.isEmpty()) {
            return Collections.emptyList();
        } else {
            final Collection<GlobalPermissionEntry> permissionEntries = getPermissions(globalPermissionOpt.get().getGlobalPermissionKey());
            // Translate these into JiraPermission objects for the sake of backwards compatibility;
            final Collection<JiraPermission> translatedEntries = Lists.newArrayListWithCapacity(permissionEntries.size());
            for (GlobalPermissionEntry permissionEntry : permissionEntries) {
                translatedEntries.add(new JiraPermissionImpl(permissionType, permissionEntry.getGroup(), GroupDropdown.DESC));
            }
            return translatedEntries;
        }
    }

    @Override
    public Collection<GlobalPermissionEntry> getPermissions(final GlobalPermissionType globalPermissionType) {
        return getPermissions(globalPermissionType.getGlobalPermissionKey());
    }

    @Override
    @Nonnull
    public Collection<GlobalPermissionEntry> getPermissions(@Nonnull final GlobalPermissionKey globalPermissionKey) {
        // When roles enabled, passing USE as a permission is unsupported; roles should be used
        ensureUsePermissionNotUsed(globalPermissionKey);

        return globalPermissionsCache.getPermissions(globalPermissionKey.getKey());
    }

    @Override
    public boolean removePermission(final int permissionId, final String group) {
        // When roles enabled, passing USE as a permission is unsupported; roles should be used
        ensureUsePermissionNotUsed(permissionId);

        return getGlobalPermission(permissionId).fold(() -> {
                    throw new IllegalArgumentException("Permission id passed must be a global permission, " + permissionId + " is not");
                },
                globalPermissionType -> removePermission(globalPermissionType, group)
        );
    }

    @Override
    public boolean removePermission(final GlobalPermissionType globalPermissionType, final String group) {
        // When roles enabled, passing USE as a permission is unsupported; roles should be used
        ensureUsePermissionNotUsed(globalPermissionType.getGlobalPermissionKey());

        final GlobalPermissionEntry jiraPermission = new GlobalPermissionEntry(globalPermissionType.getKey(), group);
        if (hasPermission(jiraPermission)) {
            removePermission(jiraPermission);
            globalPermissionsCache.clearCache();
            clearActiveUserCountIfNecessary(globalPermissionType.getGlobalPermissionKey());

            eventPublisher.publish(new GlobalPermissionDeletedEvent(globalPermissionType, group));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean removePermissions(@Nonnull final String group) {
        notNull("group", group);

        if (crowdService.getGroup(group) == null) {
            throw new IllegalArgumentException("Group passed must exist");
        }

        final Set<GlobalPermissionEntry> permissions = globalPermissionsCache.getPermissions();
        for (final GlobalPermissionEntry permission : permissions) {
            if (group.equals(permission.getGroup())) {
                removePermission(permission);
                clearActiveUserCountIfNecessary(GlobalPermissionKey.of(permission.getPermissionKey()));
            }
        }
        globalPermissionsCache.clearCache();
        return true;
    }

    private void removePermission(GlobalPermissionEntry permission) {
        ofBizDelegator.removeByAnd("GlobalPermissionEntry",
                FieldMap.build("permission", permission.getPermissionKey())
                        .add("group_id", permission.getGroup()));
    }

    /**
     * Check if a global anonymous permission exists
     *
     * @param permissionId must be global permission
     */
    @Override
    public boolean hasPermission(final int permissionId) {
        Option<GlobalPermissionType> globalPermissionOpt = getGlobalPermission(permissionId);
        if (globalPermissionOpt.isEmpty()) {
            // Permission doesn't exist, therefore no one can have permission to it
            return false;
        } else {
            return hasPermission(globalPermissionOpt.get().getGlobalPermissionKey(), null);
        }
    }

    @Override
    public boolean hasPermission(@Nonnull final GlobalPermissionType globalPermissionType) {
        return hasPermission(new GlobalPermissionEntry(globalPermissionType.getKey()));
    }

    @Override
    public boolean hasPermission(final int permissionId, final ApplicationUser user) {
        final GlobalPermissionKey globalPermissionKey = GlobalPermissionKey.GLOBAL_PERMISSION_ID_TRANSLATION.get(permissionId);
        if (globalPermissionKey == null) {
            // Permission doesn't exist, therefore no one can have permission to it
            return false;
        } else {
            return hasPermission(globalPermissionKey, user);
        }
    }

    @Override
    public boolean hasPermission(@Nonnull GlobalPermissionKey globalPermissionKey, @Nullable ApplicationUser user) {
        return hasPermissionIgnoreRecovery(globalPermissionKey, user) ||
                (isRecoveryPermission(globalPermissionKey) && recoveryMode.isRecoveryUser(user));
    }

    private static boolean isRecoveryPermission(@Nonnull final GlobalPermissionKey globalPermissionKey) {
        return isAdminPermission(globalPermissionKey)
                || GlobalPermissionKey.USE.equals(globalPermissionKey);
    }

    private static boolean isAdminPermission(@Nonnull final GlobalPermissionKey globalPermissionKey) {
        return GlobalPermissionKey.ADMINISTER.equals(globalPermissionKey)
                || GlobalPermissionKey.SYSTEM_ADMIN.equals(globalPermissionKey);
    }

    private boolean hasPermissionIgnoreRecovery(@Nonnull GlobalPermissionKey globalPermissionKey, @Nullable ApplicationUser user) {
        if (user == null) {
            // then user is anonymous; return the global permission
            return hasPermission(new GlobalPermissionEntry(globalPermissionKey.getKey(), null));
        }

        if (!user.isActive()) {
            // inactive users have no permissions
            return false;
        }

        // If application roles are not dark, then USE permission checks are replaced by a check that
        // the user is a member of any group that has an installed application role.
        if (GlobalPermissionKey.USE.getKey().equals(globalPermissionKey.getKey())) {
            return applicationRoleManager.hasAnyRole(user);
        }

        // Check the anonymous global permission first
        if (hasPermission(new GlobalPermissionEntry(globalPermissionKey.getKey(), null))) {
            return true;
        }

        // Loop through the users groups and see if there is a global permission for one of them
        final Iterable<String> userGroups = groupManager.getGroupNamesForUser(user);
        return Iterables.any(userGroups, groupName -> hasPermission(new GlobalPermissionEntry(globalPermissionKey.getKey(), groupName)));
    }

    @Override
    public boolean hasPermission(@Nonnull GlobalPermissionType globalPermissionType, @Nullable ApplicationUser user) {
        return hasPermission(globalPermissionType.getGlobalPermissionKey(), user);
    }

    @Override
    public Collection<Group> getGroupsWithPermission(int permissionId) {
        // When roles enabled, passing USE as a permission is unsupported; roles should be used
        ensureUsePermissionNotUsed(permissionId);

        Option<GlobalPermissionType> globalPermissionOpt = getGlobalPermission(permissionId);
        if (globalPermissionOpt.isEmpty()) {
            return Collections.emptyList();
        } else {
            return getGroupsWithPermission(globalPermissionOpt.get().getGlobalPermissionKey());
        }
    }

    @Override
    public Collection<Group> getGroupsWithPermission(@Nonnull final GlobalPermissionType globalPermissionType) {
        return getGroupsWithPermission(globalPermissionType.getGlobalPermissionKey());
    }

    @Override
    @Nonnull
    public Collection<Group> getGroupsWithPermission(@Nonnull final GlobalPermissionKey permissionKey) {
        // When roles enabled, passing USE as a permission is unsupported; roles should be used
        ensureUsePermissionNotUsed(permissionKey);

        final Collection<String> groupNames = getGroupNamesWithPermission(permissionKey);
        final Collection<Group> groups = Lists.newArrayListWithCapacity(groupNames.size());
        for (final String groupName : groupNames) {
            Group group = crowdService.getGroup(groupName);
            if (group != null) {
                groups.add(group);
            }
        }
        return unmodifiableCollection(groups);
    }

    @Nonnull
    @Override
    public Collection<String> getGroupNames(final int permissionId) {
        // When roles enabled, passing USE as a permission is unsupported; roles should be used
        ensureUsePermissionNotUsed(permissionId);

        Option<GlobalPermissionType> globalPermissionOpt = getGlobalPermission(permissionId);
        if (globalPermissionOpt.isEmpty()) {
            return Collections.emptyList();
        } else {
            return getGroupNamesWithPermission(globalPermissionOpt.get().getGlobalPermissionKey());
        }
    }

    @Override
    public Collection<String> getGroupNames(@Nonnull final GlobalPermissionType globalPermissionType) {
        ensureUsePermissionNotUsed(globalPermissionType.getGlobalPermissionKey());

        return getGroupNamesWithPermission(globalPermissionType.getGlobalPermissionKey());
    }

    @Override
    @Nonnull
    public Collection<String> getGroupNamesWithPermission(@Nonnull final GlobalPermissionKey permissionKey) {
        // When roles enabled, passing USE as a permission is unsupported; roles should be used
        ensureUsePermissionNotUsed(permissionKey);

        final Collection<GlobalPermissionEntry> permissions = globalPermissionsCache.getPermissions(permissionKey.getKey());
        return permissions.stream().map(GlobalPermissionEntry::getGroup)
                .filter(Objects::nonNull)
                .collect(CollectorsUtil.toImmutableSet());
    }

    @Override
    public boolean isGlobalPermission(final int permissionId) {
        // Even though the USE permission has been deprecated it is still seen as a global permission
        // This is so that it can be distinguish between project permissions and global permissions
        return GlobalPermissionKey.GLOBAL_PERMISSION_ID_TRANSLATION.containsKey(permissionId);
    }

    @Override
    public void clearCache() {
        globalPermissionsCache.clearCache();
    }

    @Override
    public boolean isPermissionManagedByJira(@Nonnull final GlobalPermissionKey permissionKey) {
        final boolean umManagementEnabled = featureManager.isEnabled(CoreFeatures.PERMISSIONS_MANAGED_BY_UM);
        return !(umManagementEnabled && ONDEMAND_UM_MANAGED_PERMISSIONS.contains(permissionKey));
    }

    /////////////// Cache Access methods ////////////////////////////////////////////////
    protected boolean hasPermission(final GlobalPermissionEntry permissionEntry) {
        if (GlobalPermissionKey.USE.getKey().equals(permissionEntry.getPermissionKey())) {
            // The USE permission has been deprecated and has been replaced with {@link ApplicationRole}'s
            // Retrieving configuration for the USE permission during renaissance mode is not supported and could introduce
            // unexpected behaviour.
            return false;
        }

        // HACK - Since the JIRA System Administer permission implies the JIRA Administrator permission we
        // need to check if a user has the Sys perm if we are asking about the Admin perm
        if (GlobalPermissionKey.ADMINISTER.getKey().equals(permissionEntry.getPermissionKey())) {
            // Do a check where if they have the current "Admin" permission we will short circuit, otherwise do
            // a second check with the sam group and PermType against the "SYS" permission.
            return globalPermissionsCache.hasPermission(permissionEntry) ||
                    globalPermissionsCache.hasPermission(new GlobalPermissionEntry(GlobalPermissionKey.SYSTEM_ADMIN, permissionEntry.getGroup()));
        } else {
            return globalPermissionsCache.hasPermission(permissionEntry);
        }
    }

    private void clearActiveUserCountIfNecessary(final GlobalPermissionKey permissionKey) {
        if (permissionKey.equals(GlobalPermissionKey.USE) || permissionKey.equals(GlobalPermissionKey.ADMINISTER)
                || permissionKey.equals(GlobalPermissionKey.SYSTEM_ADMIN)) {
            ComponentAccessor.getComponent(LicenseCountService.class).flush();
        }
    }

    private void ensureUsePermissionNotUsed(final int permissionKey) {
        //noinspection deprecation
        if (permissionKey == Permissions.USE) {
            throwUnsupportedOperationExceptionForUse();
        }
    }

    private void ensureUsePermissionNotUsed(@Nonnull GlobalPermissionKey permissionKey) {
        //noinspection deprecation
        if (GlobalPermissionKey.USE.getKey().equals(permissionKey.getKey())) {
            throwUnsupportedOperationExceptionForUse();
        }
    }

    /**
     * ApplicationRoles replace the use of USE permission when {@link ApplicationRoleManager#rolesEnabled() roles are
     * enabled}, such that any attempt to retrieve/add/remove USE permission when roles are enabled will fail fast with
     * a {@link java.lang.UnsupportedOperationException} rather than risk unexpected and/or non-deterministic behaviour
     * further down the track.
     *
     * @throws UnsupportedOperationException if roles are enabled and the permission type passed is USE
     * @see com.atlassian.jira.application.ApplicationRoleManager
     * @see com.atlassian.jira.application.ApplicationRole
     */
    private void throwUnsupportedOperationExceptionForUse() {
        throw new UnsupportedOperationException("Can't retrieve/add/remove USE permission in JIRA 7.");
    }

    @Override
    public boolean isGroupUsed(@Nonnull final Group group) {
        return globalPermissionsCache.isGroupUsed(group);
    }
}
