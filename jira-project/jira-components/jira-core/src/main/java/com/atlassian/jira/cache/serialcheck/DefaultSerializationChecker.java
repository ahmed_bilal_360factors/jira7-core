package com.atlassian.jira.cache.serialcheck;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CachedReference;
import org.apache.commons.io.output.NullOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Supplier;

@ParametersAreNonnullByDefault
public class DefaultSerializationChecker implements SerializationChecker {

    private static final Logger log = LoggerFactory.getLogger(DefaultSerializationChecker.class);

    private final List<Exception> errors = new CopyOnWriteArrayList<>();

    public <V> void checkValue(CachedReference<V> cachedReference, String cacheName, @Nullable V value) {

        verifyValueIsSerializable(value, () -> "Error serializing cached reference " + cacheName + ": " + value);
    }

    public <K, V> void checkValue(Cache<K, V> cache, K key, @Nullable V value) {

        verifyValueIsSerializable(value, () -> "Error serializing cache value: " + cache.getName() + ": " + key + "=" + value);
    }

    private void verifyValueIsSerializable(@Nullable Object value, Supplier<String> errorMessageGenerator) {
        if (value == null) {
            return;
        }

        try (ObjectOutputStream os = new ObjectOutputStream(NullOutputStream.NULL_OUTPUT_STREAM)) {
            os.writeObject(value);
        } catch (IOException e) {
            RuntimeException ex = new RuntimeException(errorMessageGenerator.get(), e);
            errors.add(ex);
            log.warn(ex.getMessage(), ex);
        }
    }

    @Override
    public Collection<? extends Exception> getErrors() {
        return new ArrayList<>(errors);
    }

    @Override
    public void reset() {
        errors.clear();
    }
}
