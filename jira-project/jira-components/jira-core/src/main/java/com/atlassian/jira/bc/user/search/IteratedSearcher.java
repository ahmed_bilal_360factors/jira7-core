package com.atlassian.jira.bc.user.search;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.List;

/**
 * Iteratively queries an underlying data source that supports query offset and limit to perform a search with a
 * client-side filter.
 * <p>
 * Note: if the source changes between calls from the iterated searcher during iteration, results may be inconsistent.
 *
 * @since 7.0
 */
public class IteratedSearcher {
    private int maxResultsPerQuery = 10000;
    private double initialLimitFactor = 2.0;

    public <T> List<T> iteratedSearch(Source<T> source, Predicate<? super T> filter, int limit) {
        int resultCount = 0;

        final List<T> allFilteredResults = new ArrayList<>(limit);

        boolean atEndOfResults = false;

        int currentOffset = 0;
        int currentLimit;

        //Optimize for case where the filter accepts everything
        if (Predicates.alwaysTrue().equals(filter)) {
            currentLimit = limit;
        } else {
            currentLimit = (int) (limit * initialLimitFactor);
        }

        while (!atEndOfResults && resultCount < limit) {
            int remaining = (limit - resultCount);

            List<T> curResults = source.search(currentOffset, currentLimit);
            int numUnfilteredResults = curResults.size();
            currentOffset += numUnfilteredResults;

            //Underlying searcher will return less results than we ask for if it has reached the end
            atEndOfResults = (numUnfilteredResults < currentLimit);

            curResults = filterList(curResults, filter);
            int numFilteredResults = curResults.size();
            allFilteredResults.addAll(curResults.subList(0, Math.min(remaining, numFilteredResults)));
            resultCount += numFilteredResults;

            //Adjust currentLimit for next iteration

            //What fraction of results remained after the filter applied?
            double filterFactor;

            //Special case when we get no results back - treat it the same as if we got back one
            if (numFilteredResults == 0) {
                filterFactor = 1.0 / numUnfilteredResults;
            } else {
                filterFactor = (double) numFilteredResults / numUnfilteredResults;
            }

            currentLimit = (int) (currentLimit / filterFactor);
            currentLimit = Math.min(maxResultsPerQuery, currentLimit);
        }

        return allFilteredResults;
    }

    private <T> List<T> filterList(List<T> unfiltered, Predicate<? super T> filter) {
        return ImmutableList.copyOf(Collections2.filter(unfiltered, filter));
    }

    public interface Source<T> {
        public List<T> search(int offset, int limit);
    }
}
