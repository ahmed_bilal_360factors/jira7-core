package com.atlassian.jira.web.action.admin.user;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.group.GroupService;
import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.subscription.FilterSubscription;
import com.atlassian.jira.issue.subscription.SubscriptionManager;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserIssueHistoryManager;
import com.atlassian.jira.web.action.IssueActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;
import org.ofbiz.core.entity.GenericEntityException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;

@WebSudoRequired
public class DeleteGroup extends IssueActionSupport {
    private String name;
    private String swapGroup;
    private boolean confirm;
    private List<ApplicationRole> applicationsForGroup;
    private Integer numberOfAffectedUsers;
    private Group group;

    private final SubscriptionManager subscriptionManager;
    private final SearchRequestService searchRequestService;
    private final GroupService groupService;
    private final GroupManager groupManager;
    private final ApplicationRoleManager roleManager;


    public DeleteGroup(IssueManager issueManager, CustomFieldManager customFieldManager,
                       AttachmentManager attachmentManager, ProjectManager projectManager, PermissionManager permissionManager,
                       VersionManager versionManager, SubscriptionManager subscriptionManager,
                       SearchRequestService searchRequestService, GroupService groupService,
                       UserIssueHistoryManager userHistoryManager, TimeTrackingConfiguration timeTrackingConfiguration,
                       GroupManager groupManager, ApplicationRoleManager roleManager) {
        super(issueManager, customFieldManager, attachmentManager, projectManager, permissionManager, versionManager,
                userHistoryManager, timeTrackingConfiguration);

        this.subscriptionManager = subscriptionManager;
        this.searchRequestService = searchRequestService;
        this.groupService = groupService;
        this.groupManager = groupManager;
        this.roleManager = roleManager;
    }

    public String doDefault() throws Exception {
        groupService.isAdminDeletingSysAdminGroup(getJiraServiceContext(), name);
        groupService.areOnlyGroupsGrantingUserAdminPermissions(getJiraServiceContext(), ImmutableList.of(name));
        return super.doDefault();
    }

    protected void doValidation() {
        groupService.validateDelete(getJiraServiceContext(), name, swapGroup);
    }

    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        if (confirm) {
            groupService.delete(getJiraServiceContext(), name, swapGroup);
        }

        if (getHasErrorMessages()) {
            return ERROR;
        } else {
            return getRedirect("GroupBrowser.jspa");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isConfirm() {
        return confirm;
    }

    public void setConfirm(boolean confirm) {
        this.confirm = confirm;
    }

    @SuppressWarnings("unused") // used in jsp
    public String getSwapGroup() {
        return swapGroup;
    }

    @SuppressWarnings("unused") // used in jsp
    public void setSwapGroup(String swapGroup) {
        this.swapGroup = swapGroup;
    }

    @SuppressWarnings("unused") // used in jsp
    public long getMatchingCommentsAndWorklogsCount() throws GenericEntityException {
        return groupService.getCommentsAndWorklogsGuardedByGroupCount(name);
    }

    /**
     * @return all other groups except this one
     */
    @SuppressWarnings("unused") // used in jsp
    public Collection getOtherGroups() {
        List<String> otherGroups = new ArrayList<String>();

        try {
            Collection<Group> groups = groupManager.getAllGroups();

            for (final Group group : groups) {
                if (!group.getName().equals(name)) {
                    otherGroups.add(group.getName());
                }
            }
        } catch (Exception e) {
            addErrorMessage(getText("admin.errors.groups.error.occured.getting.other.groups"));
        }

        return otherGroups;
    }


    public boolean hasSubscriptions() {
        try {
            List<FilterSubscription> subList = subscriptionManager.getAllFilterSubscriptions();
            for (final FilterSubscription subscription : subList) {
                if (name.equals(subscription.getGroupName())) {
                    return true;
                }
            }
        } catch (DataAccessException e) {
            log.error(e, e);
        }
        return false;
    }

    public Collection<String> getSubscriptions() {
        try {
            final List<FilterSubscription> subList = subscriptionManager.getAllFilterSubscriptions();
            final Collection<String> subscriptions = new ArrayList<String>();
            for (final FilterSubscription subscription : subList) {
                if (name.equals(subscription.getGroupName())) {
                    final String userkey = subscription.getUserKey();
                    final Long filterID = subscription.getFilterId();

                    final ApplicationUser user = getUserManager().getUserByKey(userkey);
                    final JiraServiceContext ctx = new JiraServiceContextImpl(user);
                    final SearchRequest request = searchRequestService.getFilter(ctx, filterID);

                    if (request != null) {
                        final String filterName = request.getName();
                        final String username = (user == null ? null : user.getUsername());
                        final String text = getText("admin.deletegroup.subscriptions.item", filterName, username);
                        subscriptions.add(text);
                    }
                }
            }
            return subscriptions;
        } catch (Exception e) {
            log.error(e, e);
            return Collections.emptyList();
        }
    }

    /**
     * Returns true if the {@link #getName() currently set group} is associated with (ie: grants access to)
     * any application ({@link com.atlassian.jira.application.ApplicationRole}).
     */
    @SuppressWarnings("unused") // used in jsp
    public boolean isAssociatedToAnyApplication() {
        return !getRolesForGroup().isEmpty();
    }

    /**
     * Returns the list of unique {@link com.atlassian.jira.application.ApplicationRole}s that are associated to the current
     * {@link #getName() group name}, ordered alphabetically.
     *
     * @since 7.0
     */
    @SuppressWarnings("unused") // used in jsp
    public List<ApplicationRole> getRolesForGroup() {
        if (applicationsForGroup != null) {
            return applicationsForGroup;
        }

        assert name != null && name.length() > 0 : "expected name to be set by params: " + name;
        final Group groupToDelete = getGroup();
        if (groupToDelete == null) {
            return applicationsForGroup = ImmutableList.of();
        } else {
            return applicationsForGroup = Ordering.natural()
                    .onResultOf(ApplicationRole::getName)
                    .immutableSortedCopy(roleManager.getRolesForGroup(groupToDelete));
        }
    }

    /**
     * Returns the total number of users that would lose access to one or more application(s)
     * if the {@link #getName() currently set group} were deleted.
     *
     * @since 7.0
     */
    @SuppressWarnings("unused") // used in jsp
    public Integer getApplicationUsersAffected() {
        if (numberOfAffectedUsers != null) {
            return numberOfAffectedUsers;
        }

        final Group groupBeingRemoved = getGroup();
        if (group == null) {
            return numberOfAffectedUsers = 0;
        }

        final Set<ApplicationUser> usersLosingRoles = newHashSet();
        final Set<ApplicationUser> usersInDeletionGroup = newHashSet(groupManager.getUsersInGroup(groupBeingRemoved));

        for (ApplicationRole role : getRolesForGroup()) {
            Set<Group> groupsForRole = role.getGroups();

            if (groupsForRole.size() > 1) {
                // only users not in one of the other groups will lose a role when group `name` deleted
                Set<ApplicationUser> users = newHashSet(usersInDeletionGroup);
                for (Group group : groupsForRole) {
                    if (!group.equals(groupBeingRemoved)) {
                        users.removeAll(groupManager.getUsersInGroup(group));
                    }
                }
                usersLosingRoles.addAll(users);
            } else {
                // then only 1 group associated to role
                // removing group will remove role from all users in group
                usersLosingRoles.addAll(usersInDeletionGroup);
            }
        }

        return numberOfAffectedUsers = usersLosingRoles.size();
    }

    private Group getGroup() {
        if (group != null) {
            return group;
        }

        return group = groupManager.getGroup(getName());
    }
}
