package com.atlassian.jira.bc.license;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.extras.api.LicenseException;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.LicensedApplications;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.atlassian.sal.api.license.ProductLicense.UNLIMITED_USER_COUNT;
import static com.google.common.collect.Sets.newHashSet;

/**
 * The implementation of JiraLicenseService
 *
 * @since v4.0
 */
public class JiraLicenseServiceImpl implements JiraLicenseUpdaterService {
    /**
     * the minimal license version accepted by JIRA (from 4.0)
     */
    private static final int MIN_LICENSE_VERSION = 2;

    private final JiraLicenseManager licenseManager;
    private final ClusterManager clusterManager;
    private final ApplicationManager applicationManager;
    private final UserManager userManager;
    private final HelpUrls helpUrls;

    public JiraLicenseServiceImpl(@Nonnull JiraLicenseManager licenseManager, @Nonnull ClusterManager clusterManager,
                                  @Nonnull ApplicationManager applicationManager, @Nonnull UserManager userManager,
                                  @Nonnull HelpUrls helpUrls) {
        this.helpUrls = helpUrls;
        this.licenseManager = notNull("licenseManager", licenseManager);
        this.clusterManager = notNull("clusterManager", clusterManager);
        this.applicationManager = notNull("applicationManager", applicationManager);
        this.userManager = notNull("userManager", userManager);
    }

    @Override
    public String getServerId() {
        return licenseManager.getServerId();
    }

    @Override
    public ValidationResult validate(@Nonnull final I18nHelper i18nHelper, @Nonnull final String licenseString) {
        LicenseValidationHelper helper = new LicenseValidationHelper(i18nHelper, helpUrls);
        Either<String, LicenseDetails> errorOrLicenseDetails = helper.decodeLicenseDetails(licenseString, licenseManager)
                .flatMap(helper.validateLicenseTypeAndVersion(isDataCenterConfigured(), MIN_LICENSE_VERSION));
        int totalUserCount = userManager.getTotalUserCount();

        if (errorOrLicenseDetails.isLeft()) {
            OurValidationResult validationResult = new OurValidationResult(totalUserCount, licenseString, null);
            validationResult.addError(errorOrLicenseDetails.left().get());
            return validationResult;
        } else {
            return new OurValidationResult(totalUserCount, licenseString, errorOrLicenseDetails.right().get());
        }
    }

    @Override
    public ValidationResult validate(@Nonnull final ApplicationKey applicationKey, @Nonnull final String licenseString, @Nonnull final I18nHelper i18nHelper) {
        if (applicationKey == null) {
            final OurValidationResult validationResult = new OurValidationResult(userManager.getTotalUserCount(), licenseString);
            validationResult.addError(i18nHelper.getText("jira.license.validation.invalid.product.key"));
            return validationResult;
        }
        return validate(ImmutableSet.of(applicationKey), licenseString, i18nHelper);
    }

    @Override
    public ValidationResult validateApplicationLicense(@Nonnull final I18nHelper i18nHelper, @Nonnull final String licenseString) {
        final Set<ApplicationKey> appKeys = newHashSet();
        for (Application application : applicationManager.getApplications()) {
            appKeys.add(application.getKey());
        }
        return validate(appKeys, licenseString, i18nHelper);
    }

    private ValidationResult validate(@Nonnull final Set<ApplicationKey> applicationKeys, @Nonnull final String licenseString,
                                      @Nonnull final I18nHelper i18nHelper) {
        int totalUserCount = userManager.getTotalUserCount();
        if (applicationKeys.isEmpty()) {
            OurValidationResult validationResult = new OurValidationResult(totalUserCount, licenseString);
            validationResult.addError(i18nHelper.getText("jira.license.validation.invalid.product.key"));
            return validationResult;
        }

        LicenseValidationHelper helper = new LicenseValidationHelper(i18nHelper, helpUrls);
        Either<String, LicenseDetails> errorOrLicenseDetails = helper.decodeLicenseDetails(licenseString, licenseManager)
                .flatMap(helper.validateLicenseTypeAndVersion(isDataCenterConfigured(), MIN_LICENSE_VERSION))
                .flatMap(helper.validateLicenseExpiration())
                .flatMap(helper.validateLicenseUserLimit(applicationKeys))
                .flatMap(helper.validateNonStarterNonEvaluationLicenseChange(licenseManager.getLicenses()))
                .flatMap(helper.validateMaintenanceForApplications(applicationKeys, applicationManager))
                .flatMap(helper.validateMixLicenses(licenseManager.getLicenses(), isDataCenterConfigured()));

        if (errorOrLicenseDetails.isLeft()) {
            return new OurValidationResult(totalUserCount, licenseString, null, errorOrLicenseDetails.left().get());
        } else {
            return new OurValidationResult(totalUserCount, licenseString, errorOrLicenseDetails.right().get());
        }

    }

    @Override
    public SortedSet<String> getSupportEntitlementNumbers() {
        return licenseManager.getSupportEntitlementNumbers();
    }

    @Override
    @Nonnull
    public Iterable<ValidationResult> validate(I18nHelper i18nHelper) {
        List<ValidationResult> results = new ArrayList<>();
        for (LicenseDetails license : getLicenses()) {
            results.add(validate(i18nHelper, license.getLicenseString()));
        }

        return results;
    }

    private boolean isDataCenterConfigured() {
        return clusterManager.isClustered();
    }

    @Override
    public Iterable<ValidationResult> validate(final I18nHelper i18n, final Iterable<String> licenses) {
        if (Iterables.isEmpty(licenses)) {
            OurValidationResult validationResult = new OurValidationResult(userManager.getTotalUserCount(), null, null, i18n.getText("admin.errors.no.license.supplied"));
            return Lists.newArrayList(validationResult);
        }

        List<ValidationResult> results = new ArrayList<>(Iterables.size(licenses));
        for (String license : licenses) {
            results.add(validate(i18n, license));
        }
        return results;
    }

    @Override
    public boolean isLicenseSet() {
        return licenseManager.isLicenseSet();
    }

    @Override
    public LicenseDetails setLicense(final ValidationResult validationResult) {
        if (validationResult == null || validationResult.getErrorCollection() == null || validationResult.getErrorCollection().hasAnyErrors()) {
            throw new IllegalStateException("setLicense called with illegal ValidationResult object");
        }
        return licenseManager.setLicense(validationResult.getLicenseString());
    }

    @Override
    public LicenseDetails setLicenseNoEvent(ValidationResult validationResult) {
        if (validationResult == null || validationResult.getErrorCollection() == null || validationResult.getErrorCollection().hasAnyErrors()) {
            throw new IllegalStateException("setLicense called with illegal ValidationResult object");
        }
        return licenseManager.clearAndSetLicenseNoEvent(validationResult.getLicenseString());
    }

    @Override
    public void removeLicenses(Iterable<? extends LicenseDetails> licensesToRemove) {
        licenseManager.removeLicenses(Lists.newArrayList(licensesToRemove));
    }

    @Nonnull
    @Override
    public Iterable<LicenseDetails> getLicenses() {
        return licenseManager.getLicenses();
    }

    private static final class OurValidationResult implements ValidationResult {
        private static final String LICENSE_FIELD = "license";

        private final SimpleErrorCollection errorCollection;
        private final String licenseString;
        private final LicenseDetails licenseDetails;
        private final int totalUserCount;

        private int licenseVersion;

        public OurValidationResult(int totalUserCount, @Nonnull String licenseString) {
            this(totalUserCount, licenseString, null);
        }

        public OurValidationResult(int totalUserCount, @Nonnull String licenseString, @Nullable LicenseDetails license) {
            this.licenseString = notNull(licenseString);
            this.licenseDetails = license;
            this.errorCollection = new SimpleErrorCollection();
            this.totalUserCount = totalUserCount;

            if (licenseDetails != null) {
                licenseVersion = licenseDetails.getLicenseVersion();
            }
        }

        public OurValidationResult(int totalUserCount, @Nullable String licenseString, @Nullable LicenseDetails license, String... errors) {
            if (licenseString == null && errors.length == 0) {
                throw new IllegalArgumentException("License string can only be null if there are validation errors");
            }
            this.licenseDetails = license;
            this.licenseString = licenseString;
            this.errorCollection = new SimpleErrorCollection();
            this.totalUserCount = totalUserCount;

            if (licenseDetails != null) {
                licenseVersion = licenseDetails.getLicenseVersion();
            }
            for (String error : errors) {
                this.addError(error);
            }
        }

        private void addError(String message) {
            // we always use the license field!
            errorCollection.addError(LICENSE_FIELD, message);
        }

        @Override
        public ErrorCollection getErrorCollection() {
            final SimpleErrorCollection copied = new SimpleErrorCollection();
            copied.addErrorCollection(errorCollection);
            return copied;
        }

        @Nullable
        @Override
        public LicenseDetails getLicenseDetails() {
            return licenseDetails;
        }

        @Override
        public String getLicenseString() {
            return licenseString;
        }

        @Deprecated
        @Override
        public int getLicenseVersion() {
            return licenseVersion;
        }

        @Deprecated
        @Override
        public int getTotalUserCount() {
            return this.totalUserCount;
        }
    }

    private static final class LicenseValidationHelper {
        /**
         * Checks if both licenses share at least one application
         *
         * @return true if licenses share at least one application, false otherwise
         */
        public static boolean licensesShareAtLeastOneApplication(LicenseDetails license1, LicenseDetails license2) {
            Set<ApplicationKey> licenseKeys = new HashSet<>(license1.getLicensedApplications().getKeys());
            licenseKeys.retainAll(license2.getLicensedApplications().getKeys());
            return !licenseKeys.isEmpty();
        }

        private final I18nHelper i18nHelper;
        @Nonnull
        private final HelpUrls helpUrls;

        public LicenseValidationHelper(@Nonnull final I18nHelper i18nHelper, @Nonnull HelpUrls helpUrls) {
            this.i18nHelper = i18nHelper;
            this.helpUrls = helpUrls;
        }

        /**
         * Takes license string as an input and returns decoded LicenseDetails or error description.
         */
        public Either<String, LicenseDetails> decodeLicenseDetails(final String licenseString,
                                                                   @Nonnull final JiraLicenseManager licenseManager) {
            if (StringUtils.isBlank(licenseString)) // getLicense returns empty license details for blank string
            {
                return Either.left(i18nHelper.getText("jira.license.validation.invalid.license.key"));
            }
            try {
                final LicenseDetails jiraLicenseDetails = licenseManager.getLicense(licenseString);
                return Either.right(jiraLicenseDetails);
            } catch (LicenseException licenseNotDecodable) {
                return Either.left(i18nHelper.getText("jira.license.validation.invalid.license.key"));
            }
        }

        /**
         * Checks if license is expired
         */
        public Function<LicenseDetails, Either<String, LicenseDetails>> validateLicenseExpiration() {
            return jiraLicenseDetails ->
            {
                if (jiraLicenseDetails.isExpired()) {
                    return Either.left(i18nHelper.getText("jira.license.validation.expired"));
                }
                return Either.right(jiraLicenseDetails);
            };
        }

        /**
         * Checks if:
         * <pre>
         *      - is for correct installation type (datacenter)
         *      - is not for too old version
         *  </pre>
         */
        public Function<LicenseDetails, Either<String, LicenseDetails>> validateLicenseTypeAndVersion(final boolean isDataCenterConfigured,
                                                                                                      final int minLicenseVersion) {
            return jiraLicenseDetails ->
            {
                if (isDataCenterConfigured && !jiraLicenseDetails.isDataCenter()) {
                    return Either.left(i18nHelper.getText("jira.license.validation.not.datacenter", "<a href=\"" + licenseCompatibilityUrl() + "\">", "</a>"));
                }

                if (jiraLicenseDetails.getLicenseVersion() < minLicenseVersion) {
                    return Either.left(i18nHelper.getText("setup.error.invalidlicensekey.v1.license.version"));
                }
                return Either.right(jiraLicenseDetails);
            };
        }

        private String licenseCompatibilityUrl() {
            return helpUrls.getUrl("license.compatibility").getUrl();
        }


        /**
         * If given applications are present in the license checks if user limits are properly set.
         *
         * @param applicationsToLicense The applications that the license is being checked for
         */
        public Function<LicenseDetails, Either<String, LicenseDetails>> validateLicenseUserLimit(@Nonnull final Set<ApplicationKey> applicationsToLicense) {
            return jiraLicenseDetails ->
            {
                final LicensedApplications applicationsInLicense = jiraLicenseDetails.getLicensedApplications();
                final Set<ApplicationKey> keysInLicense = applicationsInLicense.getKeys();

                if (keysInLicense.isEmpty()) {
                    return Either.left(i18nHelper.getText("jira.license.validation.invalid.license.key"));
                }

                for (ApplicationKey applicationKey : applicationsToLicense) {
                    if (keysInLicense.contains(applicationKey)) {
                        final int licUserLimit = applicationsInLicense.getUserLimit(applicationKey);
                        if (licUserLimit != UNLIMITED_USER_COUNT && licUserLimit < 1) {
                            return Either.left(i18nHelper.getText("jira.license.validation.product.user.limit.invalid"));
                        }
                    }
                }
                return Either.right(jiraLicenseDetails);
            };
        }

        /**
         * Checks if currently installed license(s) can be changed:
         * <pre>
         *     - if there is only one license currently installed it can be changed to different license type
         *     {@link com.atlassian.extras.api.LicenseType}
         *     as long as existing license and new one share at least one application.
         *     - if there are many licenses installed then only license of the same type will be accepted.
         *     - if the new license is a starter or evaluation license then it can be changed.
         * </pre>
         */
        public Function<LicenseDetails, Either<String, LicenseDetails>> validateNonStarterNonEvaluationLicenseChange(@Nonnull final Iterable<LicenseDetails> existingLicenses) {
            return jiraLicenseDetails ->
            {
                if (!jiraLicenseDetails.isStarter() && !jiraLicenseDetails.isEvaluation()) {
                    boolean hasMoreThanOneLicense = false;
                    for (LicenseDetails existingLicense : existingLicenses) {
                        if (existingLicense.isStarter() || existingLicense.isEvaluation()) {
                            continue;
                        }
                        boolean licenseTypesDiffer = (existingLicense.isPaidType() != jiraLicenseDetails.isPaidType());
                        if (licenseTypesDiffer
                                && (hasMoreThanOneLicense || !licensesShareAtLeastOneApplication(existingLicense, jiraLicenseDetails))) {
                            return Either.left(i18nHelper.getText(
                                    "jira.license.validation.does.not.match.existing.license",
                                    jiraLicenseDetails.getLicenseType().name(),
                                    existingLicense.getLicenseType().name(),
                                    "<a href=\"" + licenseCompatibilityUrl() + "\">",
                                    "</a>"));
                        }
                        hasMoreThanOneLicense = true;
                    }
                }
                return Either.right(jiraLicenseDetails);
            };
        }

        /**
         * Note, no need for a special case for a starter license as they are always for 10 users and datacenter starts
         * at 500. Also, no case for evaluation license as we will generate them for the customer to include
         * datacenter.
         */
        public Function<LicenseDetails, Either<String, LicenseDetails>> validateMixLicenses
        (@Nonnull final Iterable<LicenseDetails> licenses, final boolean isDataCenterConfigured) {
            return jiraLicenseDetails ->
            {
                boolean instanceCurrentlyHasDataCenterLicense = false;
                final Set<ApplicationKey> existingRoleKeys = newHashSet();
                for (LicenseDetails existingLicence : licenses) {
                    existingRoleKeys.addAll(existingLicence.getLicensedApplications().getKeys());
                    if (existingLicence.isDataCenter()) {
                        instanceCurrentlyHasDataCenterLicense = true;
                    }
                }
                existingRoleKeys.removeAll(jiraLicenseDetails.getLicensedApplications().getKeys());
                final boolean newLicenseReplacesAllExistingLicense = existingRoleKeys.isEmpty();

                final boolean wouldEndUpWithMixOfLicenses = !newLicenseReplacesAllExistingLicense &&
                        instanceCurrentlyHasDataCenterLicense != jiraLicenseDetails.isDataCenter();
                if (wouldEndUpWithMixOfLicenses) {
                    if (isDataCenterConfigured) {
                        // This _should_ never happen as we fail earlier in this case, but it's good to be ready.
                        return Either.left(i18nHelper.getText("jira.license.validation.not.datacenter.mix", "<a href=\"" + licenseCompatibilityUrl() + "\">", "</a>"));
                    } else {
                        return Either.left(i18nHelper.getText("jira.license.validation.datacenter.mix", "<a href=\"" + licenseCompatibilityUrl() + "\">", "</a>"));
                    }
                }
                return Either.right(jiraLicenseDetails);
            };
        }

        /**
         * Checks maintenance for given application keys if they are installed.
         */
        public Function<LicenseDetails, Either<String, LicenseDetails>> validateMaintenanceForApplications(@Nonnull final Set<ApplicationKey> installedAppKeys,
                                                                                                           @Nonnull final ApplicationManager applicationManager) {
            return jiraLicenseDetails ->
            {
                for (ApplicationKey appKey : installedAppKeys) {
                    final Option<Application> application = applicationManager.getApplication(appKey);
                    if (application.isDefined()) {
                        if (!jiraLicenseDetails.isMaintenanceValidForBuildDate(application.get().buildDate().toDate())) {
                            final LicenseDetails.LicenseStatusMessage maintenanceMessage =
                                    jiraLicenseDetails.getMaintenanceMessage(i18nHelper, application.get().getName());
                            //Subscription license does not have any messages and are valid
                            if (maintenanceMessage.hasAnyMessages()) {
                                return Either.left(maintenanceMessage.getAllMessages("<br/><br/>"));
                            }
                        }
                    }
                }
                return Either.right(jiraLicenseDetails);
            };
        }
    }
}
