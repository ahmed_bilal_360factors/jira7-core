package com.atlassian.jira.imports.xml;

import com.atlassian.jira.bc.dataimport.DefaultExportService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.util.XmlReader;
import com.atlassian.jira.util.xml.JiraFileInputStream;
import com.atlassian.jira.util.xml.SecureXmlEntityResolver;
import com.atlassian.jira.util.xml.XMLEscapingReader;
import com.atlassian.security.xml.SecureXmlParserFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * @since v3.13
 */
public class DefaultBackupXmlParser implements BackupXmlParser {
    private static final Logger log = LoggerFactory.getLogger(DefaultBackupXmlParser.class);
    private static final EntityResolver EMPTY_ENTITY_RESOLVER = new SecureXmlEntityResolver();

    private final ApplicationProperties applicationProperties;

    public DefaultBackupXmlParser(final ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    @Override
    public void parseOfBizBackupXml(final String fileName, final DefaultHandler handler) throws IOException, SAXException {
        parseXml(fileName, handler, DefaultExportService.ENTITIES_XML);
    }

    @Override
    public void parseAoBackupXml(final String fileName, final DefaultHandler handler) throws IOException, SAXException {
        parseXml(fileName, handler, DefaultExportService.ACTIVEOBJECTS_XML);
    }

    private void parseXml(final String fileName, final DefaultHandler handler, final String entryName) throws IOException, SAXException {
        XmlParser xmlParser = new XmlParser(handler);
        try {
            parse(xmlParser, fileName, entryName);
        } catch (IOException | SAXException | RuntimeException e) {
            log.error("Unexpected import failure", e);
            throw e;
        }
    }

    private void parse(final XmlParser xmlParser, final String fileName, final String zipEntryName)
            throws IOException, SAXException {
        // Get an InputStream for the named file.
        final InputStream is = new JiraFileInputStream(new File(fileName), zipEntryName);
        try (CloseableInputSource inputSource = cleanXml() ? new CloseableInputSource(getFilteredReader(is)) : new CloseableInputSource(is)) {
            xmlParser.parseXml(inputSource);
        }
    }

    private Reader getFilteredReader(final InputStream is) throws IOException {
        // Create an XmlReader, just so we can get the encoding.
        final XmlReader xmlReader = XmlReader.createReader(is);
        return new XMLEscapingReader(new InputStreamReader(xmlReader.getInputStream(), xmlReader.getEncoding()));
    }

    private boolean cleanXml() {
        return applicationProperties.getOption(APKeys.JIRA_IMPORT_CLEAN_XML);
    }

    private class CloseableInputSource extends InputSource implements Closeable {
        private final Closeable closeable;

        private CloseableInputSource(final Reader reader) {
            super(reader);
            this.closeable = reader;
        }

        private CloseableInputSource(final InputStream is) {
            super(is);
            this.closeable = is;
        }

        @Override
        public void close() throws IOException {
            closeable.close();
        }
    }

    private class XmlParser {
        private final DefaultHandler handler;

        private XmlParser(final DefaultHandler handler) {
            this.handler = handler;
        }

        public void parseXml(final InputSource inputSource) throws IOException, SAXException {
            XMLReader reader = SecureXmlParserFactory.newXmlReader();
            //        this can not be set in the secure factory
            //        saxfactory.setValidating(false);
            reader.setEntityResolver(EMPTY_ENTITY_RESOLVER);


            if (log.isDebugEnabled()) {
                log.debug("Start parsing XML with SAX Parser");
            }
            reader.setContentHandler(handler);
            reader.parse(inputSource);

            if (log.isDebugEnabled()) {
                log.debug("XML successfully parsed");

            }
        }
    }
}
