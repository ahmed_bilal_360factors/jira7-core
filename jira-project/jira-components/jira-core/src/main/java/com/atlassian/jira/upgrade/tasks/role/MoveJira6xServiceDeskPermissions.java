package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.fugue.Option;

import static com.atlassian.jira.upgrade.tasks.role.LicenseUtils.determineServiceDeskLicenseType;

/**
 * Migrates groups with Jira Service Desk permissions to application roles.
 * <p>
 * This task retrieves service desk license from plugin store and based on it performs one of the following:
 * <pre>
 * - Tier Based Pricing - executes {@link MoveJira6xTBPServiceDeskPermissions} task. See javadoc for details.
 * - Agent Based Pricing - executes {@link MoveJira6xABPServiceDeskPermissions} task. See javadoc for details.
 * - No license - migration does not do anything
 * - Invalid license or not a Service Desk license - {@link MigrationFailedException} is thrown.
 * </pre>
 * <p>
 * Additionally it saves license type as a {@link com.opensymphony.module.propertyset.PropertySet} in Service Desk
 * namespace ('vp.properties') under key 'com.atlassian.servicedesk.renaissance.migration.type'. Based on license type
 * this property can have following values: 'ABP', 'TBP' or 'none'.
 *
 * @since v7.0
 */
final class MoveJira6xServiceDeskPermissions extends MigrationTask {
    /**
     * Values of constants below SHOULD NOT be changed as they are referenced from Service Desk.
     * If  you want to change them make sure you make corresponding changes in Service Desk code base.
     */
    private static final String PROPERTY_ABP_LICENSE = "ABP";
    private static final String PROPERTY_TBP_LICENSE = "TBP";
    private static final String PROPERTY_LICENSE_NONE = "none";
    private static final String PROPERTY_KEY_MIGRATION_LICENSE = "com.atlassian.servicedesk.renaissance.migration.type";

    private final Jira6xServiceDeskLicenseProvider jira6xServiceDeskLicenseProvider;
    private final ServiceDeskPropertySetDao serviceDeskPropertySetDao;
    private final MoveJira6xTBPServiceDeskPermissions moveJira6XTBPServiceDeskPermissions;
    private final MoveJira6xABPServiceDeskPermissions moveJira6xABPServiceDeskPermissions;


    MoveJira6xServiceDeskPermissions(final Jira6xServiceDeskLicenseProvider jira6xServiceDeskLicenseProvider,
                                     final ServiceDeskPropertySetDao serviceDeskPropertySetDao,
                                     final MoveJira6xTBPServiceDeskPermissions moveJira6XTBPServiceDeskPermissions,
                                     final MoveJira6xABPServiceDeskPermissions moveJira6xABPServiceDeskPermissions) {
        this.jira6xServiceDeskLicenseProvider = jira6xServiceDeskLicenseProvider;
        this.serviceDeskPropertySetDao = serviceDeskPropertySetDao;
        this.moveJira6XTBPServiceDeskPermissions = moveJira6XTBPServiceDeskPermissions;
        this.moveJira6xABPServiceDeskPermissions = moveJira6xABPServiceDeskPermissions;
    }

    @Override
    MigrationState migrate(final MigrationState state, final boolean licenseSuppliedByUser) {
        final Option<License> license = jira6xServiceDeskLicenseProvider.serviceDeskLicense();
        if (license.isEmpty()) {
            return state.withAfterSaveTask(() -> setSdMigrationProperty(PROPERTY_LICENSE_NONE));
        }

        final LicenseUtils.ServiceDeskLicenseType licenseType = determineServiceDeskLicenseType(license.get());
        switch (licenseType) {
            case TierBasedPricing:
                return moveJira6XTBPServiceDeskPermissions.migrate(state, licenseSuppliedByUser)
                        .withAfterSaveTask(() -> setSdMigrationProperty(PROPERTY_TBP_LICENSE));
            case AgentBasedPricing:
                return moveJira6xABPServiceDeskPermissions.migrate(state, licenseSuppliedByUser)
                        .withAfterSaveTask(() -> setSdMigrationProperty(PROPERTY_ABP_LICENSE));
            default:
                throw new MigrationFailedException("Unsupported service desk license type: " + licenseType);
        }
    }

    private void setSdMigrationProperty(String migrationType) {
        serviceDeskPropertySetDao.writeStringProperty(PROPERTY_KEY_MIGRATION_LICENSE, migrationType);
    }

}
