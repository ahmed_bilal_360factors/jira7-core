package com.atlassian.jira.crowd.embedded.lucene;

import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction;
import com.atlassian.crowd.search.query.entity.restriction.MatchMode;
import com.atlassian.crowd.search.query.entity.restriction.NullRestriction;
import com.atlassian.crowd.search.query.entity.restriction.PropertyRestriction;
import com.atlassian.jira.bc.user.search.UserIndexer;
import com.google.common.collect.ImmutableList;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.PrefixQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TermRangeQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Translates Crowd query API to Lucene query API.
 */
public class CrowdQueryTranslator {

    private static final Logger LOG = LoggerFactory.getLogger(CrowdQueryTranslator.class);

    private final Analyzer analyzer;

    public CrowdQueryTranslator(Analyzer analyzer) {
        this.analyzer = analyzer;
    }

    /**
     * @param query crowd query
     * @return present translated Lucene query or absent if translation is unsuitable
     */
    public Optional<Query> translateQuery(EntityQuery<?> query) {
        if (query.getMaxResults() == EntityQuery.ALL_RESULTS) {
            LOG.debug("Refused an unbounded search");
            return Optional.empty();
        }
        return translateRestriction(query.getSearchRestriction());
    }

    public Optional<Query> tryQuerying(long directoryId, EntityQuery<?> crowdQuery) {
        return translateQuery(crowdQuery)
                .map(query -> {
                    Query directoryFilter = new TermQuery(new Term(UserIndexer.DIRECTORY_ID, String.valueOf(directoryId)));
                    BooleanQuery usersInDirectory = new BooleanQuery();
                    usersInDirectory.add(query, BooleanClause.Occur.MUST);
                    usersInDirectory.add(directoryFilter, BooleanClause.Occur.MUST);
                    return usersInDirectory;
                });
    }

    private Optional<Query> translateRestriction(@Nullable SearchRestriction restriction) {
        if ((restriction == null) || (restriction instanceof NullRestriction)) {
            return Optional.of(new MatchAllDocsQuery());
        }
        if (restriction instanceof BooleanRestriction) {
            return translateBoolean((BooleanRestriction) restriction);
        }
        if (restriction instanceof PropertyRestriction) {
            return translateProperty((PropertyRestriction<?>) restriction);
        }
        LOG.debug("Unsupported restriction type " + restriction.getClass());
        return Optional.empty();
    }

    private Optional<Query> translateBoolean(BooleanRestriction crowdRestriction) {
        return translateOccurrence(crowdRestriction.getBooleanLogic()).flatMap(
                occurrence -> {
                    List<Optional<Query>> restrictions = crowdRestriction.getRestrictions()
                            .stream()
                            .map(this::translateRestriction)
                            .collect(Collectors.toList());
                    return collapse(restrictions, occurrence);
                }
        );
    }

    private Optional<Query> collapse(List<Optional<Query>> subQueries, BooleanClause.Occur occurrence) {
        if (subQueries.isEmpty()) {
            return Optional.of(new MatchAllDocsQuery());
        }
        if (subQueries.stream().allMatch(Optional::isPresent)) {
            if (subQueries.size() == 1) {
                return subQueries.get(0);
            } else {
                BooleanQuery query = new BooleanQuery();
                subQueries
                        .stream()
                        .flatMap(subQuery -> subQuery.isPresent() ? Stream.of(subQuery.get()) : Stream.empty())
                        .forEach(subQuery -> query.add(subQuery, occurrence));
                return Optional.of(query);
            }
        } else {
            LOG.debug("Refusing to collapse subQueries=" + subQueries + " because some of them are absent");
            return Optional.empty();
        }
    }

    private Optional<BooleanClause.Occur> translateOccurrence(BooleanRestriction.BooleanLogic function) {
        switch (function) {
            case AND:
                return Optional.of(BooleanClause.Occur.MUST);
            case OR:
                return Optional.of(BooleanClause.Occur.SHOULD);
            default:
                LOG.debug("Unsupported boolean function: " + function);
                return Optional.empty();
        }
    }

    private Optional<Query> translateProperty(PropertyRestriction<?> restriction) {
        MatchMode matchMode = restriction.getMatchMode();
        String name = restriction.getProperty().getPropertyName();
        String value = restriction.getValue().toString();

        Analyzer analyzerToUse;
        String nameToUse;
        if (matchMode == MatchMode.EXACTLY_MATCHES) {
            nameToUse = UserIndexer.exactMatchFieldName(name);
            return translateTerm(nameToUse, IdentifierUtils.toLowerCase(value), matchMode);
        } else {
            analyzerToUse = analyzer;
            nameToUse = name;

            List<Optional<Query>> tokenizedQueries = tokenize(nameToUse, value, analyzerToUse)
                    .stream()
                    .map(valueToken -> translateTerm(nameToUse, valueToken, matchMode))
                    .collect(Collectors.toList());
            return collapse(tokenizedQueries, BooleanClause.Occur.MUST);
        }
    }

    private Optional<Query> translateTerm(String name, String value, MatchMode matchMode) {
        switch (matchMode) {
            case STARTS_WITH:
                return Optional.of(new PrefixQuery(new Term(name, value)));
            case EXACTLY_MATCHES:
                return Optional.of(new TermQuery(new Term(name, value)));
            case LESS_THAN:
                return Optional.of(new TermRangeQuery(name, "*", value, false, false));
            case GREATER_THAN:
                return Optional.of(new TermRangeQuery(name, value, "*", false, false));
            case CONTAINS:
                LOG.debug("Refused a substring search: name=" + name + ", value=" + value + ", matchMode=" + matchMode);
                return Optional.empty();
            default:
                LOG.debug("Unsupported match mode: " + matchMode);
                return Optional.empty();
        }
    }

    private List<String> tokenize(String name, String value, Analyzer analyzer) {
        ImmutableList.Builder<String> tokenListBuilder = ImmutableList.builder();
        try (TokenStream tokenStream = analyzer.tokenStream(name, new StringReader(value))) {
            tokenStream.reset();
            while (tokenStream.incrementToken()) {
                tokenListBuilder.add(tokenStream.getAttribute(CharTermAttribute.class).toString());
            }
            tokenStream.end();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return tokenListBuilder.build();
    }
}
