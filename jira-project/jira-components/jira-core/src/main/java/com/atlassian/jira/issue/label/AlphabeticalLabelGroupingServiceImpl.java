package com.atlassian.jira.issue.label;

import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.StatisticAccessorBean;
import com.atlassian.jira.web.bean.StatisticMapWrapper;
import com.google.common.collect.Sets;

import java.util.Set;
import java.util.stream.Collectors;

public class AlphabeticalLabelGroupingServiceImpl implements AlphabeticalLabelGroupingService {
    @Override
    public AlphabeticalLabelGroupingSupport getAlphabeticallyGroupedLabels(final ApplicationUser user, final Long projectId, final String fieldId) {
        final StatisticAccessorBean statBean = new StatisticAccessorBean(user, getProjectFilter(projectId));
        try {
            @SuppressWarnings("unchecked")
            final StatisticMapWrapper<Label, Number> statWrapper = statBean.getAllFilterBy(fieldId, StatisticAccessorBean.OrderBy.NATURAL, StatisticAccessorBean.Direction.ASC);
            final Set<String> uniqueLabels = Sets.newHashSet();

            uniqueLabels.addAll(
                    statWrapper.keySet()
                            .stream()
                            .filter(label -> label != null && label.getLabel() != null)
                            .map(Label::getLabel)
                            .collect(Collectors.toList())
            );

            return new AlphabeticalLabelGroupingSupport(uniqueLabels);
        } catch (SearchException e) {
            throw new RuntimeException(e);
        }
    }

    private SearchRequest getProjectFilter(Long projectId) {
        final JqlQueryBuilder jqlQueryBuilder = JqlQueryBuilder.newBuilder();
        JqlClauseBuilder jqlClauseBuilder = jqlQueryBuilder.where();
        jqlClauseBuilder.project(projectId);
        return new SearchRequest(jqlClauseBuilder.buildQuery());
    }
}
