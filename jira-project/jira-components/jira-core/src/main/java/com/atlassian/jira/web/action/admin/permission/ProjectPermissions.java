package com.atlassian.jira.web.action.admin.permission;

import com.atlassian.jira.event.mau.MauApplicationKey;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.permission.ProjectPermissionSchemeHelper;
import com.atlassian.jira.permission.management.ProjectPermissionFeatureHelper;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeManager;
import com.atlassian.jira.scheme.SchemeManagerFactory;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@WebSudoRequired
public class ProjectPermissions extends JiraWebActionSupport {

    private static final ObjectMapper OBJECT_MAPPER;

    private final PageBuilderService pageBuilderService;
    private final PermissionSchemeManager permissionSchemeManager;
    private final ProjectPermissionSchemeHelper helper;
    private final ProjectPermissionFeatureHelper projectPermissionFeatureHelper;
    private final SchemeManager schemeManager;

    private long schemeId = 0;

    static {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
        OBJECT_MAPPER = mapper;
    }

    public ProjectPermissions(
            final PageBuilderService pageBuilderService,
            final PermissionSchemeManager permissionSchemeManager,
            final ProjectPermissionSchemeHelper helper,
            final ProjectPermissionFeatureHelper projectPermissionFeatureHelper,
            final SchemeManagerFactory schemeManagerFactory) {
        this.pageBuilderService = pageBuilderService;
        this.permissionSchemeManager = permissionSchemeManager;
        this.helper = helper;
        this.projectPermissionFeatureHelper = projectPermissionFeatureHelper;
        this.schemeManager = schemeManagerFactory.getSchemeManager(SchemeManagerFactory.PERMISSION_SCHEME_MANAGER);
    }

    @Override
    public String doDefault() throws Exception {
        // a null permission scheme means it no longer exists (was deleted) or the URL was changed to create an invalid
        // id - for both scenarios we simply want to redirect the user back to the index page
        final Scheme scheme = permissionSchemeManager.getSchemeObject(schemeId);
        if (scheme == null) {
            return getRedirect("ViewPermissionSchemes.jspa?invalidPermissionSchemeRequested=" + schemeId);
        }

        if (projectPermissionFeatureHelper.useOldProjectPermissionPage()) {
            return getRedirect(projectPermissionFeatureHelper.getOldEditPermissionUrl(getSchemeId()));
        }

        tagMauEventAccordingToAssociatedProjects(scheme);

        pageBuilderService.assembler().resources().requireWebResource("jira.webresources:projectpermissions");
        pageBuilderService.assembler().data().requireData("permissionSchemeId", schemeId);
        pageBuilderService.assembler().data().requireData("sharedProjects", writer -> {
            OBJECT_MAPPER.writeValue(writer, getUsedIn());
        });
        return SUCCESS;
    }

    private void tagMauEventAccordingToAssociatedProjects(Scheme scheme) {
        // let's default the application key to the JIRA family,
        // unless all projects associated to this scheme have the same project type
        MauApplicationKey mauApplicationKey = MauApplicationKey.family();

        final List<Project> projects = schemeManager.getProjects(scheme);
        if (projects.isEmpty() == false) {
            final Set<ProjectTypeKey> projectTypeKeys = projects.stream().map(project -> project.getProjectTypeKey()).collect(Collectors.toSet());
            // this means all projects associated with this scheme belong to the same project type
            if (projectTypeKeys.size() == 1) {
                mauApplicationKey = MauApplicationKey.forProjectTypeKey(projectTypeKeys.iterator().next());
            }
        }

        tagMauEventWithApplication(mauApplicationKey);
    }

    public long getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(final long schemeId) {
        this.schemeId = schemeId;
    }

    private List<ProjectBean> getUsedIn() {
        final Scheme permissionScheme = permissionSchemeManager.getSchemeObject(schemeId);
        return helper.getSharedProjects(permissionScheme)
                .stream()
                .map(ProjectBean::new)
                .collect(toList());
    }

    private static class ProjectBean {
        private final Long id;
        private final String key;
        private final String name;

        public ProjectBean(final Project project) {
            this.id = project.getId();
            this.key = project.getKey();
            this.name = project.getName();
        }

        public Long getId() {
            return id;
        }

        public String getKey() {
            return key;
        }

        public String getName() {
            return name;
        }
    }
}
