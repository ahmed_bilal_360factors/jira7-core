package com.atlassian.jira.jql.validator;

import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.changehistory.JqlChangeItemMapping;
import com.atlassian.jira.issue.index.ChangeHistoryFieldConfigurationManager;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.query.clause.WasClause;
import com.atlassian.query.operand.Operand;

import java.util.List;

public class WasValuesExistValidator {
    private final JqlOperandResolver operandResolver;
    private final SearchHandlerManager searchHandlerManager;
    private final ChangeHistoryManager changeHistoryManager;
    private final JqlChangeItemMapping jqlChangeItemMapping;
    private final ChangeHistoryFieldConfigurationManager configurationManager;
    private final HistoryFieldValueValidator historyFieldValueValidator;

    public WasValuesExistValidator(JqlOperandResolver operandResolver, SearchHandlerManager searchHandlerManager, ChangeHistoryManager changeHistoryManager, JqlChangeItemMapping jqlChangeItemMapping, ChangeHistoryFieldConfigurationManager configurationManager, HistoryFieldValueValidator historyFieldValueValidator) {
        this.operandResolver = operandResolver;
        this.searchHandlerManager = searchHandlerManager;
        this.changeHistoryManager = changeHistoryManager;
        this.jqlChangeItemMapping = jqlChangeItemMapping;
        this.configurationManager = configurationManager;
        this.historyFieldValueValidator = historyFieldValueValidator;
    }

    public MessageSet validate(ApplicationUser searcher, WasClause clause) {

        final Operand operand = clause.getOperand();
        final String fieldName = clause.getName();
        final MessageSet messages = new MessageSetImpl();
        if (operandResolver.isValidOperand(operand)) {
            final List<QueryLiteral> rawValues = operandResolver.getValues(searcher, operand, clause);
            messages.addMessageSet(historyFieldValueValidator.validateValues(searcher, fieldName, rawValues));
        }
        return messages;
    }


}
