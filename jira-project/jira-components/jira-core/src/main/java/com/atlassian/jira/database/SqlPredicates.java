package com.atlassian.jira.database;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.google.common.collect.Iterables;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.SimpleExpression;

import java.util.Collection;
import java.util.List;

/**
 * Additional query predicates not provided directly by QueryDSL
 */
public class SqlPredicates {
    /**
     * The maximum number of parameters SQL Server allows in a single query
     */
    public static final int MAX_SQL_SERVER_PARAMETER_LIMIT = 2000;

    private DatabaseConfig dbConfig;

    /**
     * Oracle's maximum list size for an IN clause
     */
    private static final int MAX_LIST_SIZE_ORACLE = 1000;

    public SqlPredicates(final DatabaseConfig dbConfig) {
        this.dbConfig = dbConfig;
    }

    /**
     * Create a SQL IN clause, if necessary partitioning to account for Oracle's maximum of 1000 parameters for each IN clause.
     * <p>
     * For Oracle, where the collection has greater than 1000 parameters, the clause will be partitioned into multiple
     * IN...OR...IN clauses.
     * </p>
     * <p>
     * For other databases, a standard IN clause will be created.
     * </p>
     * This method can be removed once we upgrade to QueryDSL 3.6.0 or above, when QueryDSL will split large IN clauses for us.
     * See https://github.com/querydsl/querydsl/issues/1022
     *
     * @param left  left value
     * @param right right collection
     * @return the IN clause(s)
     */
    public <D> Predicate partitionedIn(final SimpleExpression<D> left, final Collection<? extends D> right) {
        if (dbConfig.isOracle() && right.size() > MAX_LIST_SIZE_ORACLE) {
            final Iterable<? extends List<? extends D>> partitioned = Iterables.partition(right, MAX_LIST_SIZE_ORACLE);
            return inAny(left, partitioned);
        } else {
            return left.in(right);
        }
    }

    /**
     * Create a {@code left in right or...} expression for each list
     * This method can be removed once we upgrade to QueryDSL 3.6.0 or above, when QueryDSL introduces ExpressionUtils.inAny()
     *
     * @param <D>   expression type
     * @param left  expression
     * @param lists
     * @param <D>
     * @return a {@code left in right or...} expression
     */
    public <D> Predicate inAny(final SimpleExpression<D> left, final Iterable<? extends List<? extends D>> lists) {
        BooleanBuilder builder = new BooleanBuilder();
        lists.forEach(list -> {
            builder.or(left.in(list));
        });
        return builder;
    }
}
