package com.atlassian.jira.cluster.lock;

import com.atlassian.annotations.ExperimentalApi;

import javax.annotation.Nonnull;

/**
 * Saves the shared home node status of a node.
 *
 * @see com.atlassian.jira.cluster.lock.SharedHomeNodeStatusReader
 * @since 6.3.4
 */
@ExperimentalApi
public interface SharedHomeNodeStatusWriter {
    void writeNodeStatus(@Nonnull NodeSharedHomeStatus status);

    void removeNodeStatus(@Nonnull String nodeId);
}
