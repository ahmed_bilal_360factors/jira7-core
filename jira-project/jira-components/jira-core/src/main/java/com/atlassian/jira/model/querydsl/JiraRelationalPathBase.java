package com.atlassian.jira.model.querydsl;

import com.atlassian.jira.database.DatabaseSchema;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.sql.RelationalPathBase;
import com.querydsl.sql.SchemaAndTable;

/**
 * A base class for all the JIRA Q objects to extend.
 * <p>
 * We do this so we can dynamically add in the configured schema name.
 * </p>
 *
 * @param <T> Entity type for the DTO object that holds data for this table.
 * @since v6.4.4
 */
public abstract class JiraRelationalPathBase<T> extends RelationalPathBase<T> {
    private final String tableName;

    public JiraRelationalPathBase(final Class<? extends T> type, final String alias, final String tableName) {
        super(type, PathMetadataFactory.forVariable(alias), null, tableName);
        this.tableName = tableName;
    }

    @Override
    public String getSchemaName() {
        return DatabaseSchema.getSchemaName();
    }

    @Override
    public SchemaAndTable getSchemaAndTable() {
        return new SchemaAndTable(getSchemaName(), tableName);
    }

    public abstract String getEntityName();

    public boolean hasNumericId() {
        return getNumericIdPath() != null;
    }

    public NumberPath<Long> getNumericIdPath() {
        return null;
    }
}
