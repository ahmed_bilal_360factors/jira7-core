package com.atlassian.jira.issue.watchers;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.association.UserAssociationStore;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.event.issue.IssueWatcherAddedEvent;
import com.atlassian.jira.event.issue.IssueWatcherDeletedEvent;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.comparator.ApplicationUserBestNameComparator;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.index.IssueIndexingParams;
import com.atlassian.jira.model.querydsl.QIssue;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.task.context.Contexts;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import static com.atlassian.collectors.CollectorsUtil.toNewArrayListWithSizeOf;
import static com.atlassian.jira.security.JiraAuthenticationContextImpl.getRequestCache;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.cache.CacheBuilder.newBuilder;
import static com.google.common.cache.CacheLoader.from;
import static java.util.stream.Collectors.toList;

@SuppressWarnings("deprecation")
public class DefaultWatcherManager implements WatcherManager {
    private static final Logger log = LoggerFactory.getLogger(DefaultWatcherManager.class);
    private static final String CACHE_KEY = DefaultWatcherManager.class.getName() + ".watchers";

    public static final String ASSOCIATION_TYPE = "WatchIssue";

    private final UserAssociationStore userAssociationStore;
    private final ApplicationProperties applicationProperties;
    private final IssueIndexManager indexManager;
    private final IssueFactory issueFactory;
    private final EventPublisher eventPublisher;
    private final IssueManager issueManager;
    private final UserManager userManager;
    private final QueryDslAccessor queryDslAccessor;

    public DefaultWatcherManager(final UserAssociationStore userAssociationStore, final ApplicationProperties applicationProperties,
                                 final IssueIndexManager indexManager, IssueFactory issueFactory, EventPublisher eventPublisher, IssueManager issueManager, final UserManager userManager,
                                 final QueryDslAccessor queryDslAccessor) {
        this.userAssociationStore = userAssociationStore;
        this.applicationProperties = applicationProperties;
        this.indexManager = indexManager;
        this.issueFactory = issueFactory;
        this.eventPublisher = eventPublisher;
        this.issueManager = issueManager;
        this.userManager = userManager;
        this.queryDslAccessor = queryDslAccessor;
    }

    @Nonnull
    @Override
    public Issue startWatching(@Nonnull ApplicationUser user, @Nonnull Issue issue) {
        return Iterables.getOnlyElement(startWatching(user, ImmutableList.of(issue), Contexts.nullContext()));
    }

    @Nonnull
    @Override
    public Collection<Issue> startWatching(@Nonnull ApplicationUser user, @Nonnull Collection<Issue> issues, @Nonnull Context taskContext) {
        List<Issue> updatedIssues = Lists.newArrayListWithCapacity(issues.size());
        for (Issue issue : issues) {
            Context.Task task = taskContext.start(issue);
            updatedIssues.add(updateWatch(true, user, issue));
            task.complete();
        }
        reindex(updatedIssues);
        return updatedIssues;
    }

    @Nonnull
    @Override
    public Issue stopWatching(@Nonnull ApplicationUser user, @Nonnull Issue issue) {
        return Iterables.getOnlyElement(stopWatching(user, ImmutableList.of(issue), Contexts.nullContext()));
    }

    @Nonnull
    @Override
    public Collection<Issue> stopWatching(@Nonnull ApplicationUser user, @Nonnull Collection<Issue> issues, @Nonnull final Context taskContext) {
        List<Issue> updatedIssues = new ArrayList<>(issues.size());
        for (Issue issue : issues) {
            Context.Task task = taskContext.start(issue);
            updatedIssues.add(updateWatch(false, user, issue));
            task.complete();
        }
        reindex(updatedIssues);
        return updatedIssues;
    }

    public List<String> getCurrentWatcherUsernames(@Nonnull final Issue issue) throws DataAccessException {
        final Collection<String> watcherKeys = getWatcherUserKeys(issue);
        return watcherKeys.stream().map(userManager::getUserByKeyEvenWhenUnknown).filter(user -> user != null).map(ApplicationUser::getUsername).collect(toNewArrayListWithSizeOf(watcherKeys));
    }

    public boolean isWatchingEnabled() {
        return applicationProperties.getOption(APKeys.JIRA_OPTION_WATCHING);
    }

    @Override
    public boolean isWatching(@Nullable ApplicationUser user, @Nonnull Issue issue) {
        if (user == null) {
            return false;
        }
        // For performance: if there are no watches for the issue then this dude isn't watching it.
        Long watches = issue.getWatches();
        return !(watches == null || watches == 0) && getWatchersCache().getUnchecked(issue.getId()).contains(user.getKey());
    }

    // Determine whether the current user is already watching the issue or not
    public boolean isWatching(final ApplicationUser user, final GenericValue issue) {
        return isWatching(user, issueFactory.getIssue(issue));
    }

    @Override
    public List<ApplicationUser> getWatchers(@Nonnull Issue issue, @Nonnull Locale userLocale) {
        final Collection<String> watcherKeys = getWatcherUserKeys(issue);
        return watcherKeys.stream()
                .map(userManager::getUserByKeyEvenWhenUnknown)
                .filter(user -> user != null)
                .sorted(new ApplicationUserBestNameComparator(userLocale)).collect(toNewArrayListWithSizeOf(watcherKeys));
    }

    @Override
    public Collection<ApplicationUser> getWatchersUnsorted(@Nonnull Issue issue) {
        final Collection<String> watcherKeys = getWatcherUserKeys(issue);
        return watcherKeys.stream()
                .map(userManager::getUserByKeyEvenWhenUnknown)
                .filter(user -> user != null)
                .collect(toNewArrayListWithSizeOf(watcherKeys));
    }

    @Override
    public int getWatcherCount(@Nonnull Issue issue) {
        return userAssociationStore.getUserkeysFromIssue(ASSOCIATION_TYPE, issue.getId()).size();
    }

    @Override
    public Collection<String> getWatcherUserKeys(@Nonnull Issue issue) {
        return getWatchersCache().getUnchecked(issue.getId());
    }

    private Issue updateWatch(final boolean addWatch, @Nullable final ApplicationUser user, final Issue issue) {
        if (validateUpdate(user)) {
            try {
                if (addWatch) {
                    if (!isWatching(user, issue)) {
                        getWatchersCache().invalidate(issue.getId());
                        userAssociationStore.createAssociation(ASSOCIATION_TYPE, user, issue);
                        final Issue updatedIssue = adjustWatchCount(issue, 1);
                        eventPublisher.publish(new IssueWatcherAddedEvent(updatedIssue, user));
                        return updatedIssue;
                    }
                } else {
                    if (isWatching(user, issue)) {
                        userAssociationStore.removeAssociation(ASSOCIATION_TYPE, user, issue);
                        getWatchersCache().invalidate(issue.getId());
                        final Issue updatedIssue = adjustWatchCount(issue, -1);
                        eventPublisher.publish(new IssueWatcherDeletedEvent(updatedIssue, user));
                        return updatedIssue;
                    }
                }
            } catch (final GenericEntityException e) {
                log.error("Error changing watch association", e);
                return issue;
            }
        }
        return issue;
    }

    /**
     * Validates that the params andd the system are in a correct state to change a watch
     *
     * @param user The user who is watching
     * @return whether or not to go ahead with the watch.
     */
    private boolean validateUpdate(@Nullable final ApplicationUser user) {
        if (user == null) {
            log.error("You must specify a user.");
            return false;
        }
        return true;
    }

    /**
     * Adjusts the watch count for an issue.
     *
     * @param originalIssue the issue to change count for
     * @param adjustValue   the value to change it by
     * @throws GenericEntityException If there was a persistence problem
     */
    private Issue adjustWatchCount(final Issue originalIssue, final int adjustValue) throws GenericEntityException {
        final Long issueId = originalIssue.getId();

        final Issue issueObject = issueManager.getIssueObject(issueId);
        final long watches = recalculateWatches(issueObject, adjustValue);
        queryDslAccessor.execute(dbConnection -> dbConnection.update(QIssue.ISSUE)
                .set(QIssue.ISSUE.watches, watches)
                .where(QIssue.ISSUE.id.eq(issueId))
                .execute());
        return issueManager.getIssueObject(issueId);
    }

    private long recalculateWatches(final Issue issue, final int adjustValue) {
        Long watches = issue.getWatches();
        if (watches == null) {
            watches = 0L;
        }
        watches = watches + adjustValue;

        if (watches < 0) {
            watches = 0L;
        }
        return watches;
    }

    @Override
    public void removeAllWatchesForUser(@Nonnull final ApplicationUser user) {
        notNull("User", user);
        // Find the Issues that this User watches - we need to reindex them later
        final List<GenericValue> watchedIssues = userAssociationStore.getSinksFromUser(ASSOCIATION_TYPE, user, "Issue");

        userAssociationStore.removeUserAssociationsFromUser(ASSOCIATION_TYPE, user, "Issue");
        getWatchersCache().invalidateAll();

        reindex(watchedIssues.stream().map(issueFactory::getIssue).map(issue -> {
            eventPublisher.publish(new IssueWatcherDeletedEvent(issue, user));
            return issue;
        }).collect(toList()));
    }

    private void reindex(final Collection<Issue> issues) {
        try {
            indexManager.reIndexIssueObjects(issues, IssueIndexingParams.INDEX_ISSUE_ONLY);
        } catch (final IndexException e) {
            throw new RuntimeException(e);
        }
    }

    protected LoadingCache<Long, ImmutableSet<String>> getWatchersCache() {
        final LoadingCache<Long, ImmutableSet<String>> cache = newBuilder().build(from(issueId -> ImmutableSet.copyOf(userAssociationStore.getUserkeysFromIssue(ASSOCIATION_TYPE, issueId))));
        if (ExecutingHttpRequest.get() != null) {
            return getRequestCache(CACHE_KEY, () -> cache);
        } else {
            return cache;
        }
    }
}
