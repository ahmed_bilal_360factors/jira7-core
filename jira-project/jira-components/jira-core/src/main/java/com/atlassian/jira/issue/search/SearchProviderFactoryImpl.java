package com.atlassian.jira.issue.search;

import com.atlassian.jira.issue.index.IssueIndexManager;

import org.apache.lucene.search.IndexSearcher;

import static java.util.Objects.requireNonNull;

public class SearchProviderFactoryImpl implements SearchProviderFactory {
    private final IssueIndexManager indexManager;

    public SearchProviderFactoryImpl(final IssueIndexManager indexManager) {
        this.indexManager = requireNonNull(indexManager, "indexManager");
    }

    public IndexSearcher getSearcher(final String searcherName) {
        if (ISSUE_INDEX.equals(searcherName)) {
            return indexManager.getIssueSearcher();
        } else if (COMMENT_INDEX.equals(searcherName)) {
            return indexManager.getCommentSearcher();
        } else if (CHANGE_HISTORY_INDEX.equals(searcherName)) {
            return indexManager.getChangeHistorySearcher();
        } else if (WORKLOG_INDEX.equals(searcherName)) {
            return indexManager.getWorklogSearcher();
        }
        throw new UnsupportedOperationException("Only issue, comment and change history indexes are catered for currently");
    }
}
