package com.atlassian.jira.plugin;

import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.loaders.ClassPathPluginLoader;
import com.atlassian.plugin.loaders.PluginLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * This is the default plugin loader factory of JIRA
 */
public class DefaultPluginLoaderFactory implements PluginLoaderFactory {
    private static final Logger log = LoggerFactory.getLogger(DefaultPluginLoaderFactory.class);

    private final PluginFactoryAndLoaderRegistrar pluginFactoryAndLoaderRegistrar;
    private final PluginDirectoryLoaderFactory pluginDirectoryLoaderFactory;
    private final JiraProperties jiraSystemProperties;

    public DefaultPluginLoaderFactory(final JiraProperties jiraSystemProperties, final PluginFactoryAndLoaderRegistrar pluginFactoryAndLoaderRegistrar, final PluginDirectoryLoaderFactory pluginDirectoryLoaderFactory) {
        this.jiraSystemProperties = jiraSystemProperties;
        this.pluginFactoryAndLoaderRegistrar = pluginFactoryAndLoaderRegistrar;
        this.pluginDirectoryLoaderFactory = pluginDirectoryLoaderFactory;
    }

    public List<PluginLoader> getPluginLoaders() {
        final List<PluginFactory> pluginFactories = pluginFactoryAndLoaderRegistrar.getDefaultPluginFactories();

        final CollectionBuilder<PluginLoader> pluginLoaderBuilder = CollectionBuilder.newBuilder();

        // plugin loaders for our system-xxx plugins
        pluginLoaderBuilder.addAll(pluginFactoryAndLoaderRegistrar.getDefaultSystemPluginLoaders());

        // for loading plugin1 plugins the old way (ie. via WEB-INF/lib)
        pluginLoaderBuilder.add(new ClassPathPluginLoader());

        if (jiraSystemProperties.isBundledPluginsDisabled()) {
            log.warn("Bundled plugins have been disabled. Removing bundled plugin loader.");
        } else {
            pluginLoaderBuilder.add(pluginFactoryAndLoaderRegistrar.getBundledPluginsLoader(pluginFactories));
        }

        if (jiraSystemProperties.isCustomPathPluginsEnabled()) {
            PluginLoader pluginLoader = pluginFactoryAndLoaderRegistrar.getCustomDirectoryPluginLoader(pluginFactories);
            if (pluginLoader != null) {
                pluginLoaderBuilder.add(pluginLoader);
            }
        }

        if (jiraSystemProperties.isPluginsRosterFileEnabled()) {
            final PluginLoader pluginLoader =
                    pluginFactoryAndLoaderRegistrar.getRosterFilePluginLoader(pluginFactories);
            if (pluginLoader != null) {
                pluginLoaderBuilder.add(pluginLoader);
            }
        }

        pluginLoaderBuilder.add(pluginDirectoryLoaderFactory.getDirectoryPluginLoader(pluginFactories));

        return pluginLoaderBuilder.asList();
    }

}
