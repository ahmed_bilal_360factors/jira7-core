package com.atlassian.jira.bc.workflow;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.workflow.events.WorkflowPublished;
import com.atlassian.jira.config.FeatureFlag;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.workflow.AssignableWorkflowScheme;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowException;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowScheme;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.jira.workflow.WorkflowUtil;
import com.google.common.annotations.VisibleForTesting;
import com.opensymphony.workflow.loader.StepDescriptor;
import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;

import static com.atlassian.jira.config.FeatureFlag.featureFlag;
import static com.atlassian.jira.util.ErrorCollection.Reason;
import static com.atlassian.jira.workflow.JiraWorkflow.DEFAULT_WORKFLOW_NAME;
import static com.google.common.collect.Iterables.isEmpty;
import static java.lang.String.format;
import static java.util.Objects.isNull;
import static java.util.Optional.ofNullable;
import static java.util.stream.StreamSupport.stream;

/**
 * Default workflow service implementation.  Provides some 'nice' error handling and delegates straight through
 * to the underlying {@link com.atlassian.jira.workflow.WorkflowManager}
 */
public class DefaultWorkflowService implements WorkflowService, Startable {
    public static final FeatureFlag PROJECT_LEVEL_ADMIN_EDIT_WORKFLOW_FEATURE = featureFlag("com.atlassian.jira.project-level-admin.edit_workflow").onByDefault();

    @VisibleForTesting
    static final String OVERWRITE_WORKFLOW_LOCK_NAME = DefaultWorkflowService.class.getName() + ".overwriteWorkflow";

    private static final Logger log = LoggerFactory.getLogger(DefaultWorkflowService.class);

    private final ClusterLockService clusterLockService;
    private final WorkflowManager workflowManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final PermissionManager permissionManager;
    private final WorkflowSchemeManager workflowSchemeManager;
    private final GlobalPermissionManager globalPermissionManager;
    private final ProjectManager projectManager;
    private final FeatureManager featureManager;
    private final EventPublisher eventPublisher;

    private Lock overwriteWorkflowLock;

    public DefaultWorkflowService(final WorkflowManager workflowManager, final JiraAuthenticationContext jiraAuthenticationContext,
                                  final PermissionManager permissionManager, final WorkflowSchemeManager workflowSchemeManager,
                                  final ClusterLockService clusterLockService, final GlobalPermissionManager globalPermissionManager,
                                  final ProjectManager projectManager, final FeatureManager featureManager, EventPublisher eventPublisher) {
        this.clusterLockService = clusterLockService;
        this.workflowManager = workflowManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.permissionManager = permissionManager;
        this.workflowSchemeManager = workflowSchemeManager;
        this.globalPermissionManager = globalPermissionManager;
        this.projectManager = projectManager;
        this.featureManager = featureManager;
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void start() {
        this.overwriteWorkflowLock = clusterLockService.getLockForName(OVERWRITE_WORKFLOW_LOCK_NAME);
    }

    public JiraWorkflow getDraftWorkflow(final JiraServiceContext jiraServiceContext, final String parentWorkflowName) {
        if (!hasEditWorkflowPermission(jiraServiceContext, parentWorkflowName)) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.no.admin.permission"));
            return null;
        }
        if (StringUtils.isEmpty(parentWorkflowName)) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.no.parent"));
            return null;
        }
        final JiraWorkflow parentWorkflow = workflowManager.getWorkflow(parentWorkflowName);
        if (parentWorkflow == null) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.retrieve.no.parent"));
            return null;
        }
        return workflowManager.getDraftWorkflow(parentWorkflowName);
    }

    public JiraWorkflow createDraftWorkflow(final JiraServiceContext jiraServiceContext, final String parentWorkflowName) {
        if (!hasEditWorkflowPermission(jiraServiceContext, parentWorkflowName)) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.no.admin.permission"));
            return null;
        }
        if (StringUtils.isEmpty(parentWorkflowName)) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.no.parent"));
            return null;
        }
        final JiraWorkflow parentWorkflow = workflowManager.getWorkflow(parentWorkflowName);
        if (parentWorkflow == null) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.no.parent"));
            return null;
        }
        if (!workflowManager.isActive(parentWorkflow)) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.parent.not.active"));
            return null;
        }
        if (parentWorkflow.isSystemWorkflow()) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.not.editable"));
            return null;
        }

        try {
            return workflowManager.createDraftWorkflow(jiraServiceContext.getLoggedInApplicationUser(), parentWorkflowName);
        } catch (final IllegalStateException e) {
            jiraServiceContext.getErrorCollection().addErrorMessage(
                    getI18nBean().getText("admin.workflows.service.error.draft.exists.or.workflow.not.active"));
            return null;
        }
    }

    public boolean deleteDraftWorkflow(final JiraServiceContext jiraServiceContext, final String parentWorkflowName) {
        if (StringUtils.isEmpty(parentWorkflowName)) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.delete.no.parent"));
            return false;
        }
        if (!hasEditWorkflowPermission(jiraServiceContext, parentWorkflowName)) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.no.admin.permission"));
            return false;
        }
        return workflowManager.deleteDraftWorkflow(parentWorkflowName);
    }

    @Override
    public ServiceOutcome<Void> deleteWorkflow(ApplicationUser deletingUser, String workflowName) {
        JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(deletingUser);
        JiraWorkflow workflow = getWorkflow(jiraServiceContext, workflowName);

        if (jiraServiceContext.getErrorCollection().hasAnyErrors()) {
            return ServiceOutcomeImpl.from(jiraServiceContext.getErrorCollection(), null);
        }

        if (workflow == null) {
            return ServiceOutcomeImpl.error(getI18nBean().getText("admin.errors.workflow.with.name.does.not.exist", "'" + workflowName + "'"));
        }

        if (!workflow.isEditable()) {
            return ServiceOutcomeImpl.error(getI18nBean().getText("admin.errors.workflow.cannot.be.deleted.as.it.is.not.editable"));
        }

        // Ensure that the workflow is not associated with any schemes
        Iterable<WorkflowScheme> workflowSchemes = workflowSchemeManager.getSchemesForWorkflowIncludingDrafts(workflow);
        if (!isEmpty(workflowSchemes)) {
            StringBuilder schemes = new StringBuilder();
            for (WorkflowScheme scheme : workflowSchemes) {
                if (scheme.isDraft()) {
                    schemes.append(getI18nBean().getText("admin.workflows.service.workflow.draft.of", scheme.getName()));
                } else {
                    schemes.append('\'').append(scheme.getName()).append('\'');
                }
                schemes.append(", ");
            }
            schemes.delete(schemes.length() - 2, schemes.length() - 1);
            return ServiceOutcomeImpl.error(getI18nBean().getText("admin.errors.cannot.delete.this.workflow") + " " + schemes);
        }

        workflowManager.deleteWorkflow(workflow);

        return ServiceOutcomeImpl.ok(null);
    }

    public void overwriteActiveWorkflow(final JiraServiceContext jiraServiceContext, final String parentWorkflowName) {
        if (!hasEditWorkflowPermission(jiraServiceContext, parentWorkflowName)) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.no.admin.permission"));
            return;
        }
        //need to protect the validation and overwrite by an explicit lock.  The updateWorkflow method is protected
        //by the same lock.  This guarantees the workflow that is used to validate, won't change in between validation
        //and the overwrite itself.  This works since we only use the workflowName, and the overwrite workflow
        //looks up the latest version of the workflow from the underlying persistence layer.
        // Note that only the updateWorkflow() method is protected by this lock.  Deleting an draftWorkflow() is ok
        // as the worst thing that could happen is that the live workflow is overwritten by a draft that's been deleted
        // already.  We know that that draft was in a valid state however before it was deleted otherwise the
        // validateOverwriteWorkflow() wouldn't have passed.
        overwriteWorkflowLock.lock();
        try {
            validateOverwriteWorkflow(jiraServiceContext, parentWorkflowName);

            if (jiraServiceContext.getErrorCollection().hasAnyErrors()) {
                return;
            }

            final boolean isWorkflowIsolated = isWorkflowIsolated(parentWorkflowName);
            workflowManager.overwriteActiveWorkflow(jiraServiceContext.getLoggedInApplicationUser(), parentWorkflowName);
            eventPublisher.publish(new WorkflowPublished(isWorkflowIsolated, hasAdminPermission(jiraServiceContext)));
        } finally {
            overwriteWorkflowLock.unlock();
        }
    }

    public void validateOverwriteWorkflow(final JiraServiceContext jiraServiceContext, final String workflowName) {
        // Check that the user has the required permission
        if (!hasEditWorkflowPermission(jiraServiceContext, workflowName)) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.no.admin.permission"));
            return;
        }
        // Check that a workflow name was provided
        if (StringUtils.isEmpty(workflowName)) {
            jiraServiceContext.getErrorCollection().addErrorMessage(
                    getI18nBean().getText("admin.workflows.service.error.overwrite.no.parent", workflowName));
            return;
        }
        // get hold of the Published workflow to be overwritten.
        final JiraWorkflow liveJiraWorkflow = workflowManager.getWorkflow(workflowName);
        if (liveJiraWorkflow == null) {
            // No actual workflow
            jiraServiceContext.getErrorCollection().addErrorMessage(
                    getI18nBean().getText("admin.workflows.service.error.overwrite.no.parent", workflowName));
            return;
        }
        if (!liveJiraWorkflow.isActive()) {
            // No actual workflow
            jiraServiceContext.getErrorCollection().addErrorMessage(
                    getI18nBean().getText("admin.workflows.service.error.overwrite.inactive.parent", workflowName));
            return;
        }
        // get a hold of the Draft workflow that will be saved
        final JiraWorkflow draftJiraWorkflow = workflowManager.getDraftWorkflow(workflowName);
        if (draftJiraWorkflow == null) {
            // No draft workflow
            jiraServiceContext.getErrorCollection().addErrorMessage(
                    getI18nBean().getText("admin.workflows.service.error.overwrite.no.draft", workflowName));
            return;
        }
        // TODO: Should we deal with trying to save to default workflow ("jira") explicitly, or just rely on it not making a draft?
        // We have actually got hold of a live and draft workflow for the given name, check that the changes are valid:
        validateOverwriteWorkflow(liveJiraWorkflow, draftJiraWorkflow, jiraServiceContext);
    }

    public void updateWorkflow(final JiraServiceContext jiraServiceContext, final JiraWorkflow workflow) {
        if (!hasEditWorkflowPermission(jiraServiceContext.getLoggedInApplicationUser(), workflow)) {
            addError(jiraServiceContext, "admin.workflows.service.error.no.admin.permission", Reason.FORBIDDEN);
            return;
        }
        if ((workflow == null) || (workflow.getDescriptor() == null)) {
            addError(jiraServiceContext, "admin.workflows.service.error.update.no.workflow", Reason.VALIDATION_FAILED);
            return;
        }
        if (!workflow.isEditable()) {
            addError(jiraServiceContext, "admin.workflows.service.error.not.editable", Reason.VALIDATION_FAILED);
            return;
        }
        // This lock ensures that the overwriteWorkflow action above allows for an atomic validation and overwrite
        // of the workflow.  
        overwriteWorkflowLock.lock();
        try {
            workflowManager.updateWorkflow(jiraServiceContext.getLoggedInApplicationUser(), workflow);
        } finally {
            overwriteWorkflowLock.unlock();
        }
    }

    public void validateUpdateWorkflowNameAndDescription(final JiraServiceContext jiraServiceContext, final JiraWorkflow currentWorkflow, final String newWorkflowName) {
        if (!hasEditWorkflowPermission(jiraServiceContext.getLoggedInApplicationUser(), currentWorkflow)) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.no.admin.permission"));
            return;
        }
        if ((currentWorkflow == null) || (currentWorkflow.getDescriptor() == null)) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.update.no.workflow"));
            return;
        }

        if (!currentWorkflow.isEditable()) {
            jiraServiceContext.getErrorCollection().addErrorMessage(
                    getI18nBean().getText("admin.errors.workflow.cannot.be.edited.as.it.is.not.editable"));
            return;
        }

        //Draft workflows are not allowed to change name!
        if (currentWorkflow.isDraftWorkflow() && !newWorkflowName.equals(currentWorkflow.getName())) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.edit.name.draft.workflow"));
            return;
        }

        if (!WorkflowUtil.isAcceptableName(newWorkflowName, "newWorkflowName", jiraServiceContext.getErrorCollection())) {
            return;
        }
        //If we're changeing name, check that no workflow already exists with the new name.
        if (!newWorkflowName.equals(currentWorkflow.getName()) && workflowManager.workflowExists(newWorkflowName)) {
            jiraServiceContext.getErrorCollection().addError("newWorkflowName",
                    getI18nBean().getText("admin.errors.a.workflow.with.this.name.already.exists"));
            return;
        }
    }

    public void updateWorkflowNameAndDescription(final JiraServiceContext jiraServiceContext, final JiraWorkflow currentWorkflow, final String newName, final String newDescription) {
        if (!hasEditWorkflowPermission(jiraServiceContext.getLoggedInApplicationUser(), currentWorkflow)) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.no.admin.permission"));
            return;
        }
        workflowManager.updateWorkflowNameAndDescription(jiraServiceContext.getLoggedInApplicationUser(), currentWorkflow, newName, newDescription);
    }

    public JiraWorkflow getWorkflow(final JiraServiceContext jiraServiceContext, final String name) {
        if (StringUtils.isEmpty(name)) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.null.name"));
            return null;
        }

        return workflowManager.getWorkflow(name);
    }

    public void validateCopyWorkflow(final JiraServiceContext jiraServiceContext, final String newWorkflowName) {
        if (!hasAdminPermission(jiraServiceContext)) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.no.admin.permission"));
            return;
        }
        if (!WorkflowUtil.isAcceptableName(newWorkflowName, "newWorkflowName", jiraServiceContext.getErrorCollection())) {
            return;
        }

        try {
            if (workflowManager.workflowExists(newWorkflowName)) {
                jiraServiceContext.getErrorCollection().addError("newWorkflowName",
                        getI18nBean().getText("admin.errors.a.workflow.with.this.name.already.exists"));
            }
        } catch (final WorkflowException e) {
            log.error("Error occurred while accessing workflow information.", e);
            jiraServiceContext.getErrorCollection().addErrorMessage(
                    getI18nBean().getText("admin.errors.workflows.error.occurred.accessing.information"));
        }
    }

    public JiraWorkflow copyWorkflow(final JiraServiceContext jiraServiceContext, final String clonedWorkflowName, final String clonedWorkflowDescription, final JiraWorkflow workflowToClone) {
        if (!hasEditWorkflowPermission(jiraServiceContext.getLoggedInApplicationUser(), workflowToClone)) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.no.admin.permission"));
            return null;
        }
        return workflowManager.copyWorkflow(jiraServiceContext.getLoggedInApplicationUser(), clonedWorkflowName, clonedWorkflowDescription, workflowToClone);
    }

    public boolean isStepOnDraftWithNoTransitionsOnParentWorkflow(final JiraServiceContext jiraServiceContext, final JiraWorkflow workflow, final int stepId) {
        return false;
    }

    public void validateAddWorkflowTransitionToDraft(final JiraServiceContext jiraServiceContext, final JiraWorkflow newJiraWorkflow, final int stepId) {
        if (!hasEditWorkflowPermission(jiraServiceContext.getLoggedInApplicationUser(), newJiraWorkflow)) {
            jiraServiceContext.getErrorCollection().addErrorMessage(getI18nBean().getText("admin.workflows.service.error.no.admin.permission"));
            return;
        }
    }

    private void validateOverwriteWorkflow(final JiraWorkflow oldJiraWorkflow, final JiraWorkflow newJiraWorkflow, final JiraServiceContext jiraServiceContext) {
        // For each step in the original workflow, we want that step to exist in the new workflow with the same ID and
        // the same associated Issue Status.
        final List<GenericValue> linkedStatuses = oldJiraWorkflow.getLinkedStatuses();
        for (final GenericValue gvStatus : linkedStatuses) {
            // get the old and new StepDescriptor
            final StepDescriptor oldStepDescriptor = oldJiraWorkflow.getLinkedStep(gvStatus);
            final StepDescriptor newStepDescriptor = newJiraWorkflow.getLinkedStep(gvStatus);

            // Firstly we must have a step for the new status
            if (newStepDescriptor == null) {
                jiraServiceContext.getErrorCollection().addErrorMessage(
                        getI18nBean().getText("admin.workflows.service.error.overwrite.missing.status", gvStatus.getString("name")));
                break;
            }
            // The step associated with this status must be the same step ID.
            // Otherwise os_currentstep table and possibly others will become invalid
            if (oldStepDescriptor.getId() != newStepDescriptor.getId()) {
                jiraServiceContext.getErrorCollection().addErrorMessage(
                        getI18nBean().getText("admin.workflows.service.error.overwrite.step.associated.with.wrong.status",
                                String.valueOf(oldStepDescriptor.getId()), gvStatus.getString("name")));
                break;
            }

            validateAddWorkflowTransitionToDraft(jiraServiceContext, newJiraWorkflow, oldStepDescriptor.getId());
            if (jiraServiceContext.getErrorCollection().hasAnyErrors()) {
                break;
            }
        }
    }

    @VisibleForTesting
    I18nHelper getI18nBean() {
        return jiraAuthenticationContext.getI18nHelper();
    }

    @VisibleForTesting
    boolean hasAdminPermission(final JiraServiceContext jiraServiceContext) {
        return hasAdminPermission(ofNullable(jiraServiceContext).map(JiraServiceContext::getLoggedInApplicationUser).orElse(null));
    }

    boolean hasAdminPermission(final ApplicationUser user) {
        return globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user);
    }

    private void addError(final JiraServiceContext jiraServiceContext, final String msgKey, final Reason reason) {
        final ErrorCollection errorCollection = jiraServiceContext.getErrorCollection();
        errorCollection.addErrorMessage(getI18nBean().getText(msgKey));
        errorCollection.addReason(reason);
    }

    private boolean hasEditWorkflowPermission(JiraServiceContext jiraServiceContext, String workflowName) {
        JiraWorkflow workflow = workflowManager.getWorkflow(workflowName);
        return hasEditWorkflowPermission(ofNullable(jiraServiceContext).map(JiraServiceContext::getLoggedInApplicationUser).orElse(null), workflow);
    }

    private boolean isWorkflowIsolated(final String workflowName) {
        return getNumberOfUsedProjects(workflowName) == 1;
    }

    private int getNumberOfUsedProjects(final String workflowName) {
        return projectManager.getProjects().stream()
                .map(p -> {
                    Map<String, String> workflowMap = ADD_DEFAULT_WORKFLOW_IF_MISSING(workflowSchemeManager.getWorkflowMap(p));
                    final String defaultWorkflowName = workflowMap.get(null);
                    return p.getIssueTypes().stream()
                            .map(it -> workflowMap.get(it.getId()))
                            .map(wf -> isNull(wf) ? defaultWorkflowName : wf)
                            .anyMatch(wf -> wf.equals(workflowName)) ? 1 : 0;
                }).reduce(0, (a, b) -> a + b);
    }

    /**
     * * User has to satisfy any condition of the following:
     * - have ADMINISTER permission
     * - have {@code ProjectPermissions.ADMINISTER_PROJECTS} in every project that uses passed workflow and workflow is not shared with other projects and workflow is used in at least 1 project.
     */
    @Override
    public boolean hasEditWorkflowPermission(final ApplicationUser user, final JiraWorkflow workflow) {
        if (hasAdminPermission(user)) {
            return true;
        }
        if (!featureManager.isEnabled(PROJECT_LEVEL_ADMIN_EDIT_WORKFLOW_FEATURE)) {
            log.debug(format("Feature is off: %s", PROJECT_LEVEL_ADMIN_EDIT_WORKFLOW_FEATURE));
            return false;
        }
        if (workflow == null) {
            log.debug("project level admin cannot edit null workflow");
            return false;
        }
        if (workflow.isSystemWorkflow()) {
            log.debug("project level admin cannot edit system workflow {}", workflow.getName());
            return false;
        }

        Iterable<WorkflowScheme> schemes = workflowSchemeManager.getSchemesForWorkflowIncludingDrafts(workflow);
        if (isEmpty(schemes)) {
            log.debug("project level admin cannot edit workflow that is not used by any project {}", workflow.getName());
            return false;
        }

        final int usedInProjectsCount = getNumberOfUsedProjects(workflow.getName());
        if (usedInProjectsCount > 1) {
            log.debug("project level admin cannot edit workflow shared by other projects {}", workflow.getName());
            return false;
        } else if (usedInProjectsCount == 0) {
            log.debug("project level admin cannot edit workflow not used by any project {}", workflow.getName());
            return false;
        }

        return stream(workflowSchemeManager.getSchemesForWorkflowIncludingDrafts(workflow).spliterator(), false)
                .filter(wf -> wf instanceof AssignableWorkflowScheme)
                .map(wf -> workflowSchemeManager.getProjectsUsing((AssignableWorkflowScheme) wf))
                .flatMap(Collection::stream)
                .allMatch(project -> permissionManager.hasPermission(ProjectPermissions.ADMINISTER_PROJECTS, project, user));
    }

    public static Map<String, String> ADD_DEFAULT_WORKFLOW_IF_MISSING (Map<String, String> workflowMap) {
        final Map<String, String> newMap = new HashMap<>(workflowMap);
        final String defaultWorkflow = newMap.get(null);
        if (isNull(defaultWorkflow)) {
            newMap.put(null, DEFAULT_WORKFLOW_NAME);
        }
        return newMap;
    };
}
