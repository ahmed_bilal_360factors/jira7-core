package com.atlassian.jira.bc.user;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.jira.bc.user.UserService.FieldName;
import com.atlassian.jira.plugin.user.PasswordPolicyManager;
import com.atlassian.jira.plugin.user.WebErrorMessage;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.SimpleWarningCollection;
import com.atlassian.jira.util.WarningCollection;
import com.google.common.collect.ImmutableList;
import com.opensymphony.util.TextUtils;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Helper user to perform user related validations.
 *
 * @since v7.0
 */
public class UserValidationHelper {
    private final I18nHelper.BeanFactory i18nFactory;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final PermissionManager permissionManager;
    private final UserManager userManager;
    private final PasswordPolicyManager passwordPolicyManager;

    public UserValidationHelper(final I18nHelper.BeanFactory i18nFactory, final JiraAuthenticationContext jiraAuthenticationContext,
                                final PermissionManager permissionManager, final UserManager userManager,
                                final PasswordPolicyManager passwordPolicyManager) {
        this.i18nFactory = i18nFactory;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.permissionManager = permissionManager;
        this.userManager = userManager;
        this.passwordPolicyManager = passwordPolicyManager;
    }

    /**
     * Create a validations object using the provided users locale. Each validation adds an error to the {@link
     * Validations#getErrors()} collection.
     *
     * @return a validations object that contains user related validations.
     */
    public Validations validations(@Nullable ApplicationUser applicationUser) {
        return new Validations(Optional.ofNullable(applicationUser));
    }

    class Validations {
        private final int MAX_FIELD_LENGTH = 255;
        private final char[] INVALID_USERNAME_CHARS = {'<', '>', '&'};

        private final ErrorCollection errors;
        private final WarningCollection warnings;
        private final I18nHelper i18nBean;

        public Validations(Optional<ApplicationUser> applicationUser) {
            errors = new SimpleErrorCollection();
            warnings = new SimpleWarningCollection();
            if (applicationUser.isPresent()) {
                i18nBean = i18nFactory.getInstance(applicationUser.get());
            } else {
                i18nBean = jiraAuthenticationContext.getI18nHelper();
            }
        }

        public ErrorCollection getErrors() {
            return errors;
        }

        public WarningCollection getWarnings() {
            return warnings;
        }

        private void addErrorMessage(String i18nKey) {
            errors.addErrorMessage(i18nBean.getText(i18nKey));
        }

        private void addErrorMessage(String i18nKey, Object param) {
            errors.addErrorMessage(i18nBean.getText(i18nKey, param));
        }

        private void addError(final String fieldName, String i18nKey) {
            errors.addError(fieldName, i18nBean.getText(i18nKey));
        }

        /**
         * Determine whether there is a writable user directory.
         *
         * @return true if there is a writable user directory else false.
         */
        public boolean hasWritableDirectory() {
            if (!userManager.hasWritableDirectory()) {
                addErrorMessage("admin.errors.cannot.add.user.all.directories.read.only");
                return false;
            }
            return true;
        }


        /**
         * Determine whether the specified user directory is writable.
         *
         * @param directoryId id used to identify user directory. {@link null} indicates that the directory could not be
         *                    found.
         * @return true if the specified user directory is writable for user creation. {@link null} indicates that the
         * directory could not be found and would return false.
         */
        public boolean writableDirectory(final Long directoryId) {
            Directory directory = userManager.getDirectory(directoryId);
            if (directory == null) {
                addErrorMessage("admin.errors.cannot.add.user.no.such.directory", directoryId);
                return false;
            } else {
                if (!directory.getAllowedOperations().contains(OperationType.CREATE_USER)) {
                    addErrorMessage("admin.errors.cannot.add.user.read.only.directory", directory.getName());
                    return false;
                }
            }
            return true;
        }

        /**
         * Determine whether the provided user has access to create users.
         *
         * @param userPerformingCreate user that is performing the create.
         * @return true if the provided user is able to create other users, false when provided user does not have
         * permission to create other users.
         */
        public boolean hasCreateAccess(@Nonnull final ApplicationUser userPerformingCreate) {
            if (!permissionManager.hasPermission(Permissions.ADMINISTER, userPerformingCreate)) {
                addErrorMessage("admin.errors.user.no.permission.to.create");
                return false;
            }
            return true;
        }

        /**
         * Validate that the required password has been provided.
         *
         * @param password              the required password.
         * @param shouldConfirmPassword should password be confirmed
         * @return true if the required password has been provided else false;
         */
        public boolean passwordRequired(final String password, final boolean shouldConfirmPassword) {
            if (StringUtils.isEmpty(password)) {
                final String errorI18nKey = shouldConfirmPassword
                        ? "signup.error.password.required" : "signup.error.password.required.without.confirmation";
                errors.addError(FieldName.PASSWORD, i18nBean.getText(errorI18nKey));
                return false;
            }
            return true;
        }

        /**
         * Validate that when the confirm password has been provided that it matches the password.
         *
         * @param password        the password.
         * @param confirmPassword the confirm password.
         * @return true if the confirm password matches the password else false;
         */
        public boolean validateConfirmPassword(final String password, final String confirmPassword) {
            // If a password has been specified then we need to check they are the same
            // else there is no password specified then check to see if we need one.
            if (StringUtils.isNotEmpty(confirmPassword) || StringUtils.isNotEmpty(password)) {
                if (password == null || !password.equals(confirmPassword)) {
                    addError(FieldName.CONFIRM_PASSWORD, "signup.error.password.mustmatch");
                    return false;
                }
            }
            return true;
        }

        /**
         * Validate that the provided password conforms to the password policy.
         *
         * @param password    the user password.
         * @param username    the username.
         * @param displayName the user display name.
         * @param email       the user email address.
         * @return error messages if password has been rejected else an empty list.
         */
        public List<WebErrorMessage> validatePasswordPolicy(final String password, String username, String displayName, String email) {
            final Collection<WebErrorMessage> webErrorMessages = passwordPolicyManager.checkPolicy(username, displayName, email, password);
            if (!webErrorMessages.isEmpty()) {
                addError(FieldName.PASSWORD, "signup.error.password.rejected");
            }
            return ImmutableList.copyOf(webErrorMessages);
        }

        /**
         * Validate required user email address.
         *
         * @param emailAddress user email address.
         * @return true if user email address has been provided and match email address policy.
         */
        public boolean validateEmailAddress(final String emailAddress) {
            if (StringUtils.isEmpty(emailAddress)) {
                addError(FieldName.EMAIL, "signup.error.email.required");
                return false;
            } else if (emailAddress.length() > MAX_FIELD_LENGTH) {
                addError(FieldName.EMAIL, "signup.error.email.greater.than.max.chars");
                return false;
            } else if (!TextUtils.verifyEmail(emailAddress)) {
                addError(FieldName.EMAIL, "signup.error.email.valid");
                return false;
            }
            return true;
        }

        /**
         * Validate required user display name.
         *
         * @param displayName user display name.
         * @return true if the user display name is valid else false.
         */
        public boolean validateDisplayName(final String displayName) {
            if (StringUtils.isEmpty(displayName)) {
                addError(FieldName.FULLNAME, "signup.error.fullname.required");
                return false;
            } else if (displayName.length() > MAX_FIELD_LENGTH) {
                addError(FieldName.FULLNAME, "signup.error.full.name.greater.than.max.chars");
                return false;
            }
            return true;
        }

        /**
         * Validate the provided username. Make sure that username has been provided and that it meets the username
         * policy. This would also validate that the username is not already in use.
         *
         * @param username    new username.
         * @param directoryId id used to identify user directory, default user directory is indicated by providing
         *                    {@link null}.
         * @return true if the username is valid else false.
         */
        public boolean hasValidUsername(final String username, final Long directoryId) {
            if (hasRequiredUsername(username)) {
                if (validateUsernamePolicy(username)) {
                    return usernameDoesNotExist(directoryId, username);
                }
            }
            return false;
        }

        /**
         * Determine if a username has been provided.
         *
         * @param username new username.
         * @return true if the username has been provided.
         */
        public boolean hasRequiredUsername(final String username) {
            if (StringUtils.isEmpty(username)) {
                addError(FieldName.NAME, "signup.error.username.required");
                return false;
            }
            return true;
        }

        /**
         * Validate username to ensure that username meet username policy.
         *
         * @param username username that should meet username policy.
         * @return true if specified username meet username policy else false.
         */
        public boolean validateUsernamePolicy(final String username) {
            if (username.length() > MAX_FIELD_LENGTH) {
                addError(FieldName.NAME, "signup.error.username.greater.than.max.chars");
                return false;
            } else if (StringUtils.containsAny(username, INVALID_USERNAME_CHARS)) {
                addError(FieldName.NAME, "signup.error.username.invalid.chars");
                return false;
            }
            return true;
        }

        /**
         * Validate that provided username does not already exist.
         *
         * @param directoryId id used to identify user directory, default user directory is indicated by providing
         *                    {@link null}.
         * @param username    the username that should not exist in the specified user directory.
         * @return false if the username is already in use else true.
         */
        public boolean usernameDoesNotExist(Long directoryId, @Nonnull final String username) {
            if (directoryId != null) {
                // Check if the username exists in the given directory - we allow duplicates in other directories
                if (userManager.findUserInDirectory(username, directoryId) != null) {
                    addError(FieldName.NAME, "signup.error.username.exists");
                    return false;
                }
                return true;
            } else {
                // Check if the username exists in any directory
                if (userManager.getUserByName(username) != null) {
                    addError(FieldName.NAME, "signup.error.username.exists");
                    return false;
                }
                return true;
            }
        }

        /**
         * Validate whether this JIRA Instance has a default directory that is writable for create user.
         *
         * @return true if this instance has a writable directory, false if this instance does not have a writable
         * directory.
         */
        public boolean hasWritableDefaultCreateDirectory() {
            final Optional<Directory> defaultDirectory = userManager.getDefaultCreateDirectory();
            if (!defaultDirectory.isPresent()) {
                addErrorMessage("admin.errors.cannot.add.user.all.directories.read.only");
                return false;
            }
            return true;
        }
    }
}