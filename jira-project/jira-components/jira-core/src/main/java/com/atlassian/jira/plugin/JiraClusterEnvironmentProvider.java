package com.atlassian.jira.plugin;

import com.atlassian.jira.cluster.ClusterInfo;
import com.atlassian.plugin.manager.ClusterEnvironmentProvider;

import static java.util.Objects.requireNonNull;

/**
 * Allows to check if JIRA is launched in the cluster mode
 * This is required to skip plugins disabling when started in clustered environment
 * @since 7.3
 */
public class JiraClusterEnvironmentProvider implements ClusterEnvironmentProvider {

    private final ClusterInfo clusterInfo;

    public JiraClusterEnvironmentProvider(ClusterInfo clusterInfo) {
        this.clusterInfo = requireNonNull(clusterInfo, "ClusterInfo must not be null");
    }

    @Override
    public boolean isInCluster() {
        return clusterInfo.isClustered();
    }
}
