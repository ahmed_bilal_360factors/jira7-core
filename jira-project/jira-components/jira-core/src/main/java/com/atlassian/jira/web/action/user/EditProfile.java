package com.atlassian.jira.web.action.user;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.runtime.CommunicationException;
import com.atlassian.crowd.exception.runtime.OperationFailedException;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.security.login.LoginResult;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.event.user.UserProfileUpdatedEvent;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import org.apache.commons.lang.StringUtils;

public class EditProfile extends JiraWebActionSupport {
    private static final int MAX_LENGTH = 255;

    private final CrowdService crowdService;
    private final UserManager userManager;
    private final EventPublisher eventPublisher;
    private final LoginService loginService;

    private String username;
    private String fullName;
    private String email;

    private String password;

    public EditProfile(final CrowdService crowdService, final UserManager userManager,
                       final EventPublisher eventPublisher, LoginService loginService) {
        this.crowdService = crowdService;
        this.userManager = userManager;
        this.eventPublisher = eventPublisher;
        this.loginService = loginService;
    }


    public String doDefault() throws Exception {
        final ApplicationUser current = getLoggedInUser();

        if (current == null || !current.getName().equals(username)) {
            return ERROR;
        }
        if (!userManager.userCanUpdateOwnDetails(current)) {
            addErrorMessage(getText("editprofile.not.allowed"));
            return ERROR;
        }

        fullName = current.getDisplayName();
        email = current.getEmailAddress();

        return super.doDefault();
    }

    protected void doValidation() {
        final ApplicationUser current = getLoggedInUser();
        if (current == null) {
            addErrorMessage("generic.notloggedin.title");
            return;
        }

        if (StringUtils.isBlank(fullName)) {
            addError("fullName", getText("admin.errors.invalid.full.name.specified"));
        } else if (fullName.length() > MAX_LENGTH) {
            addError("fullName", getText("signup.error.full.name.greater.than.max.chars"));
        }
        if (StringUtils.isBlank(email)) {
            addError("email", getText("admin.errors.invalid.email"));
        } else if (email.length() > MAX_LENGTH) {
            addError("email", getText("signup.error.email.greater.than.max.chars"));
        }

        if (detailsHaveChanged(current)) {
            validatePassword(current);
        }
    }

    private void validatePassword(final ApplicationUser current) {
        if (password == null) {
            addError("password", getText("user.profile.password.mismatch"));
        }
        try {
            final LoginResult loginResult = loginService.authenticate(current, password);
            switch (loginResult.getReason()) {
                case OK:
                    break;
                case AUTHENTICATION_DENIED:
                    // the login service will not even acquire information whether the login is correct or not,
                    // if the elevated security check is triggered.
                    addErrorMessage(getText("user.profile.elevated.authorisation.required"));
                    break;
                case AUTHORISATION_FAILED:
                    // user was chanfged since the current session's login: admin revoked application rights etc.
                    addErrorMessage(getText("user.profile.user.auth.failed"));
                    break;
                case AUTHENTICATED_FAILED:
                    // incorrect password:
                    addError("password", getText("user.profile.password.mismatch"));
                    break;
            }
        } catch (CommunicationException communicationException) {
            log.debug("Error communicating with remote user directory.", communicationException);
            // Network Error trying to communicate with remote Crowd or LDAP server
            addErrorMessage(getText("login.error.communication"));
        } catch (OperationFailedException operationFailedException) {
            log.debug("Error while trying to authenticate user '" + current.getName() + "'.", operationFailedException);
            addError("password", getText("login.error.misc"));
        } catch (Exception e) {
            log.debug("Exception occurred while trying to authorise current user.", e);
            // there was a problem accessing the user data:
            addErrorMessage(getText("user.profile.user.auth.failed"));
        }

    }

    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        final ApplicationUser current = getLoggedInUser();

        if (current == null || !current.getUsername().equals(username)) {
            return ERROR;
        }
        if (!userManager.userCanUpdateOwnDetails(current)) {
            addErrorMessage(getText("editprofile.not.allowed"));
            return ERROR;
        }
        if (detailsHaveChanged(current)) {

            UserTemplate user = new UserTemplate(current.getDirectoryUser());
            user.setDisplayName(fullName);
            user.setEmailAddress(email);


            try {
                crowdService.updateUser(user);
                eventPublisher.publish(new UserProfileUpdatedEvent(current, getLoggedInUser()));
            } catch (OperationNotPermittedException e) {
                addErrorMessage(getText("admin.errors.cannot.edit.user.directory.read.only"));
            }
        }

        return returnComplete("ViewProfile.jspa");

    }

    private boolean detailsHaveChanged(ApplicationUser current) {
        return !eq(current.getDisplayName(), fullName) || !eq(current.getEmailAddress(), email);
    }

    private boolean eq(String s, String s1) {
        return StringUtils.defaultString(s).equals(StringUtils.defaultString(s1));
    }

    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean userCanUpdateOwnDetails() {
        return userManager.userCanUpdateOwnDetails(getLoggedInUser());
    }

    public boolean getElevatedSecurityCheckRequired() {
        return loginService.getLoginInfo(this.username).isElevatedSecurityCheckRequired();
    }
}
