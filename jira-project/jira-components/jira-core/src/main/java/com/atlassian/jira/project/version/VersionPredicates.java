package com.atlassian.jira.project.version;

import com.google.common.base.Predicate;

import javax.annotation.Nullable;

public class VersionPredicates {
    public static Predicate<Version> isArchived() {
        return new Predicate<Version>() {
            @Override
            public boolean apply(@Nullable final Version version) {
                return version != null && version.isArchived();
            }
        };
    }

    public static Predicate<Version> isReleased() {
        return new Predicate<Version>() {
            @Override
            public boolean apply(@Nullable final Version version) {
                return version != null && version.isReleased();
            }
        };
    }
}
