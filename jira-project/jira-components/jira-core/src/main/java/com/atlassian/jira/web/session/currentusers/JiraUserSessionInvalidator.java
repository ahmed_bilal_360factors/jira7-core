package com.atlassian.jira.web.session.currentusers;

import com.atlassian.crowd.event.user.UserCredentialUpdatedEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import webwork.action.ActionContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * If you want to invalidate a session in JIRA with given id, this class (component) is for you.
 * <p>
 * It will use {@link JiraUserSessionTracker} to track invalidated sessions to invalidate them during next request using given session.
 * It is intended to be used by filter that runs before any authentication filter
 * and invalidates {@link HttpSession} for current request if it is marked as invalidated.
 * <p>
 * It uses {@link JiraUserSessionTracker} to check for sessions and invalidation flag.
 *
 * @see com.atlassian.jira.web.filters.JiraLoginFilter
 * @since v7.1.2
 */
@EventComponent
public class JiraUserSessionInvalidator {
    private static final Logger LOG = LoggerFactory.getLogger(JiraUserSessionInvalidator.class);

    private final JiraUserSessionTracker jiraUserSessionTracker;

    public JiraUserSessionInvalidator() {
        this.jiraUserSessionTracker = JiraUserSessionTracker.getInstance();
    }

    /**
     * Will invalidate session passed in argument if it's id was marked as invalid.
     * Should be called for every request to ensure invalidation of sessions that were supposed to be invalid.
     *
     * @param request current ServletRequest
     */
    public void handleSessionInvalidation(HttpServletRequest request) {
        String sessionId = null;

        try {
            final HttpSession session = request.getSession(false);

            if (session == null) {
                return;
            }

            sessionId = session.getId();
            if (jiraUserSessionTracker.isSessionMarkedForInvalidation(sessionId)) {
                session.invalidate();
            }
        } catch (IllegalStateException illegalStateException) {
            LOG.debug("Can't handle invalidation of session.", illegalStateException);
        } finally {
            if (StringUtils.isNotEmpty(sessionId)) {
                jiraUserSessionTracker.removeInvalidateFlagFromSession(sessionId);
            }
        }
    }

    /**
     * Mark id of session as invalid to invalidate session with this id next time it is used in request.
     *
     * @param sessionId - id of session to be invalidated
     * @see #handleSessionInvalidation
     */
    public void invalidateSession(String sessionId) {
        if (StringUtils.isEmpty(sessionId)) {
            return;
        }

        jiraUserSessionTracker.markSessionAsInvalid(sessionId);
    }

    /**
     * Removes invalidation flag from given session.
     *
     * @param httpServletRequest session which invalidation flag should be removed
     */
    public void removeInvalidationFlagFromSession(HttpServletRequest httpServletRequest) {
        if (httpServletRequest != null) {
            final HttpSession session = httpServletRequest.getSession(false);
            if (session != null) {
                jiraUserSessionTracker.removeInvalidateFlagFromSession(session.getId());
            }
        }
    }

    /**
     * Mark all active sessions for given user as invalid. Session will be invalidated next time it is used in request.
     * It does not invalidate current session.
     *
     * @param username - username associated with session to be removed
     * @see #handleSessionInvalidation
     */
    private void invalidateAllSessionsForUser(String username) {
        final String currentSessionId = getSessionId(ActionContext.getRequest());

        jiraUserSessionTracker.getSnapshot()
                .stream()
                .filter(activeSession -> StringUtils.equals(activeSession.getUserName(), username))
                .filter(activeSession -> !StringUtils.equals(activeSession.getId(), currentSessionId))
                .map(JiraUserSession::getId)
                .forEach(this::invalidateSession);
    }

    private String getSessionId(final HttpServletRequest request) {
        if (request != null) {
            final HttpSession session = request.getSession(false);
            if (session != null) {
                return session.getId();
            }
        }
        return null;
    }

    @EventListener
    public void onUserCredentialUpdatedEvent(UserCredentialUpdatedEvent event) {
        invalidateAllSessionsForUser(event.getUsername());
    }
}
