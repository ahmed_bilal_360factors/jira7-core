package com.atlassian.jira.bc.user.property;

import com.atlassian.jira.bc.user.UserPropertyService;
import com.atlassian.jira.entity.WithId;
import com.atlassian.jira.entity.property.AbstractEntityPropertyConditionHelper;
import com.atlassian.jira.plugin.webfragment.JiraWebContext;
import com.atlassian.jira.user.ApplicationUser;

import java.util.Map;
import java.util.Optional;

public final class UserPropertyConditionHelper extends AbstractEntityPropertyConditionHelper<ApplicationUser> {

    public UserPropertyConditionHelper(UserPropertyService propertyService) {
        super(propertyService, ApplicationUser.class, "user");
    }

    @Override
    public Optional<Long> getEntityId(JiraWebContext context) {
        return context.getUser().map(WithId::getId);
    }
}
