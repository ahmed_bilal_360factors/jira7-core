package com.atlassian.jira.license;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.user.util.Users;
import com.atlassian.jira.util.BaseUrl;
import com.atlassian.soy.renderer.SoyException;
import com.google.common.collect.ImmutableMap;
import com.opensymphony.module.propertyset.PropertySet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

import static java.lang.Math.min;

/**
 * Implements license-related reminders and warnings.
 * <p>
 * When one or more licensed applications are about to expire, a warning will inform the administrators. Applications
 * may be licensed within a single or multiple licenses.
 * <p>
 * Either the maintenance or the subscription, for non-perpetual licenses, may be expiring.
 * <p>
 * When the administrator dismisses the flag ot gets snoozed. The reminder schedule is 90, 45, 30, 15 and 7 days before due
 * date, as well as on the day of expiry. For subscription licenses the administrator is reminded by a banner, that can
 * not be suppressed. When multiple licenses are expiring at different times within the 90 day notification period, the
 * schedule will follow the earliest license's reminder epochs.
 * <p>
 * The remind me later flag will be cleared when all licenses are renewed, hence are beyond the 90 day notification
 * period
 * <p>
 * Special care is taken to display correct messages for single or multiple applications expiring.
 * <p>
 * Expiring licenses take priority over maintenance expiry, maintaining only one flag or banner displaying for all
 * expirations.
 *
 * @since v6.3
 */
public class LicenseBannerHelperImpl implements LicenseBannerHelper {
    private static final Logger LOG = LoggerFactory.getLogger(LicenseBannerHelperImpl.class);

    /**
     * The database property name that stores the license expiry reminder date.
     */
    static final String EXPIRY_KEY = "license.expiry.remindme";

    /**
     * The database property name that stores the maintenance expiry reminder date.
     */
    static final String MAINTENANCE_KEY = "license.maintenance.remindme";
    private static final int EARLIEST_EXPIRY_ERROR_FROM_EXPIRY = 1; //Banner
    private static final int EARLIEST_EXPIRY_WARNING_FROM_EXPIRY = 90; //Flag
    private static final String LINK_MANAGE_APPLICATION = "/plugins/servlet/applications/versions-licenses";

    private final JiraAuthenticationContext context;
    private final GlobalPermissionManager globalPermissionManager;
    private final UserPropertyManager propertyManager;
    private final JiraLicenseManager jiraLicenseManager;
    private final SoyTemplateRendererProvider rendererProvider;
    private final BaseUrl baseUrl;
    private final FeatureManager featureManager;

    public LicenseBannerHelperImpl(final JiraAuthenticationContext context,
                                   final GlobalPermissionManager globalPermissionManager,
                                   final UserPropertyManager propertyManager,
                                   final JiraLicenseManager jiraLicenseManager,
                                   final SoyTemplateRendererProvider rendererProvider,
                                   final BaseUrl baseUrl,
                                   final FeatureManager featureManager) {
        this.context = context;
        this.globalPermissionManager = globalPermissionManager;
        this.propertyManager = propertyManager;
        this.jiraLicenseManager = jiraLicenseManager;
        this.rendererProvider = rendererProvider;
        this.baseUrl = baseUrl;
        this.featureManager = featureManager;
    }

    @Nonnull
    @Override
    public String getExpiryBanner() {
        if (!havePermission() || !jiraLicenseManager.isLicenseSet()) {
            return "";
        }

        ExpirationStateAccumulator subscriptionChecker = getEarliestExpiringLicenseChecker(EARLIEST_EXPIRY_ERROR_FROM_EXPIRY);
        if (subscriptionChecker.isExpiring() || subscriptionChecker.getDaysToExpire() <= EARLIEST_EXPIRY_ERROR_FROM_EXPIRY) {
            return renderExpiryBanner(subscriptionChecker);
        }
        if (subscriptionChecker.getDaysToExpire() > EARLIEST_EXPIRY_WARNING_FROM_EXPIRY) {
            clearHideUntilExpiry();
        }
        // Maintenance never shows banners
        return "";
    }

    @Nonnull
    @Override
    public String getLicenseFlag() {
        if (!havePermission() || !jiraLicenseManager.isLicenseSet()) {
            return "";
        }

        ExpirationStateAccumulator subscriptionChecker = getEarliestExpiringLicenseChecker(EARLIEST_EXPIRY_WARNING_FROM_EXPIRY);
        if (subscriptionChecker.isExpiring()
                && subscriptionChecker.getDaysToExpire() > EARLIEST_EXPIRY_ERROR_FROM_EXPIRY) // last day it's a banner
        {
            // Maintenance flags do not display if there are expiration flags to show so we return here
            return renderExpiryFlag(subscriptionChecker);
        }

        ExpirationStateAccumulator maintenanceChecker = getEarliestMaintenanceExpiringLicenseChecker();
        if (shouldNotDisplayMaintenanceFlag(subscriptionChecker, maintenanceChecker)) {
            if (maintenanceChecker.getDaysToExpire() > EARLIEST_EXPIRY_WARNING_FROM_EXPIRY) {
                //reset the maintenance flags
                clearHideUtilMaintenance();
            }
            return "";
        }
        return renderMaintenanceFlag(maintenanceChecker);
    }

    /**
     * Maintenance flag wont display if:
     * - Maintenance is not expiring
     * - A subscription banner is displaying for the same license
     */
    private boolean shouldNotDisplayMaintenanceFlag(ExpirationStateAccumulator subscriptionChecker,
                                                    ExpirationStateAccumulator maintenanceChecker) {
        if (featureManager.isEnabled("never.display.maintenance.flag")) {
            // never.display.maintenance.flag is used in testing only so not added to DarkFeatures classes
            // the flag can obscure other elements on the page, making webdriver fail, so we turn it off.
            return true;
        }
        return !maintenanceChecker.isExpiring() || subscriptionChecker.getDaysToExpire() <= EARLIEST_EXPIRY_ERROR_FROM_EXPIRY &&
                maintenanceChecker.getApplicationDescription().equals(subscriptionChecker.getApplicationDescription());
    }

    private ExpirationStateAccumulator getEarliestExpiringLicenseChecker(int maxDaysFromExpiry) {
        int daysFromExpiry = min(getHideUntilExpiry(), maxDaysFromExpiry);
        EarliestExpiringLicenseChecker licenseChecker = new EarliestExpiringLicenseChecker(daysFromExpiry,
                jiraLicenseManager.getLicenses());
        return licenseChecker.getSubscriptionExpiringLicense();
    }

    private ExpirationStateAccumulator getEarliestMaintenanceExpiringLicenseChecker() {
        int daysFromExpiry = min(getHideUntilMaintenance(), EARLIEST_EXPIRY_WARNING_FROM_EXPIRY);
        EarliestExpiringLicenseChecker licenseChecker = new EarliestExpiringLicenseChecker(daysFromExpiry,
                jiraLicenseManager.getLicenses());
        return licenseChecker.getMaintenanceExpiringLicense();
    }

    static private abstract class ExpirationStateAccumulator {
        private int daysToExpire = Integer.MAX_VALUE;
        private String applicationDescription = "";
        private boolean isEvaluation = false;
        private int expiringApps = 0;
        int notificationPeriod = EARLIEST_EXPIRY_WARNING_FROM_EXPIRY;

        ExpirationStateAccumulator(int notificationPeriod) {
            this.notificationPeriod = notificationPeriod;
        }

        boolean checkLicense(LicenseDetails license) {
            if (ignoreLicense(license)) {
                return false;
            }

            int thisLicenseDaysToExpiry = getDaysToExpireFrom(license);
            daysToExpire = min(thisLicenseDaysToExpiry, daysToExpire);
            if (notificationPeriod >= thisLicenseDaysToExpiry) {
                // will be false when the first license is detected,
                // and true if it is second time around or more than one apps in single license
                expiringApps += license.getLicensedApplications().getKeys().size();
                applicationDescription = license.getApplicationDescription();
                isEvaluation = license.isEvaluation();
                return true;
            }
            return false;
        }

        public int getDaysToExpire() {
            return daysToExpire;
        }

        /**
         * will return the last license's application description(s) that was within the notification period.
         */
        public String getApplicationDescription() {
            return applicationDescription;
        }

        public boolean isEvaluation() {
            return isEvaluation;
        }

        public boolean isMultipleExpiring() {
            return expiringApps > 1;
        }

        public boolean isExpiring() {
            return expiringApps > 0;
        }

        private boolean ignoreLicense(final LicenseDetails license) {
            return license.isEvaluation() && license.getDaysToLicenseExpiry() > 15;
        }

        abstract int getDaysToExpireFrom(@Nonnull LicenseDetails license);
    }

    static private class EarliestExpiringLicenseChecker {
        ExpirationStateAccumulator subscriptionExpiringLicense = null;
        ExpirationStateAccumulator maintenanceExpiringLicense = null;

        EarliestExpiringLicenseChecker(int notificationPeriod, Iterable<LicenseDetails> theLicenses) {
            subscriptionExpiringLicense = new ExpirationStateAccumulator(notificationPeriod) {
                @Override
                int getDaysToExpireFrom(@Nonnull final LicenseDetails license) {
                    return license.getDaysToLicenseExpiry();
                }
            };

            maintenanceExpiringLicense = new ExpirationStateAccumulator(notificationPeriod) {
                @Override
                int getDaysToExpireFrom(@Nonnull final LicenseDetails license) {
                    return license.getDaysToMaintenanceExpiry();
                }
            };

            for (LicenseDetails license : theLicenses) {
                checkLicense(license);
            }
        }

        public ExpirationStateAccumulator getSubscriptionExpiringLicense() {
            return subscriptionExpiringLicense;
        }

        public ExpirationStateAccumulator getMaintenanceExpiringLicense() {
            return maintenanceExpiringLicense;
        }


        private boolean checkLicense(LicenseDetails license) {
            boolean result = subscriptionExpiringLicense.checkLicense(license);
            result |= maintenanceExpiringLicense.checkLicense(license);
            return result;
        }
    }

    @Override
    public void remindMeLater() {
        if (!havePermission()) {
            return;
        }

        if (!jiraLicenseManager.isLicenseSet()) {
            clearHideUntilExpiry();
            clearHideUtilMaintenance();
            return;
        }

        EarliestExpiringLicenseChecker licenseChecker =
                new EarliestExpiringLicenseChecker(EARLIEST_EXPIRY_WARNING_FROM_EXPIRY,
                        jiraLicenseManager.getLicenses());
        ExpirationStateAccumulator subscriptionChecker = licenseChecker.getSubscriptionExpiringLicense();
        ExpirationStateAccumulator maintenanceChecker = licenseChecker.getMaintenanceExpiringLicense();
        //If expiry is less than 90 days then the user is going to be interested about expiry (i.e. ignore maintenance)
        if (!subscriptionChecker.isExpiring()) { // Maintenance only
            //All subscriptions licenses are up to date, remove any remind-me-later for subscription
            clearHideUntilExpiry();
            //If maintenance expiry is less than 90 days then the user is going to be interested about maintenance.
            int daysToMaintenanceExpiry = maintenanceChecker.getDaysToExpire();
            if (maintenanceChecker.isExpiring()) {
                if (daysToMaintenanceExpiry <= -7) {
                    //Don't spam more until renewal
                    setHideUtilMaintenance(Integer.MIN_VALUE);
                } else if (daysToMaintenanceExpiry <= 0) {
                    //Remind 7 days from now if the maintenance has already expired.
                    setHideUtilMaintenance(daysToMaintenanceExpiry - 7);
                } else if (daysToMaintenanceExpiry <= 1) {
                    //Remind on maintenance expiration when within a day of it.
                    setHideUtilMaintenance(0);
                } else if (daysToMaintenanceExpiry <= 7) {
                    //Remind on maintenance expiration when within 1 days of it.
                    setHideUtilMaintenance(1);
                } else if (daysToMaintenanceExpiry <= 15) {
                    //Remind 7 days from maintenance expiry when within 15 days of it.
                    setHideUtilMaintenance(7);
                } else if (daysToMaintenanceExpiry <= 30) {
                    //Remind 15 days from maintenance expiry when within 30 days of it.
                    setHideUtilMaintenance(15);
                } else if (daysToMaintenanceExpiry <= 45) {
                    //Remind 30 days from maintenance expiry when within 45 days of it.
                    setHideUtilMaintenance(30);
                } else {
                    //Remind 45 days from maintenance expiry when within 90 days of it.
                    setHideUtilMaintenance(45);
                }
            } else {
                //Maintenance expiry is outside of 90 days, so just forget any remember-me settings.
                clearHideUtilMaintenance();
            }
        } else { //suspending subscription license
            int daysToLicenseExpiry = licenseChecker.getSubscriptionExpiringLicense().getDaysToExpire();

            if (daysToLicenseExpiry <= 1) {
                // rest manually called
                clearHideUntilExpiry();
            } else if (daysToLicenseExpiry <= 7) {
                setHideUtilExpiry(1);
            } else if (daysToLicenseExpiry <= 15) {
                //If within 15 days, then remind again at 7 days.
                setHideUtilExpiry(7);
            } else if (daysToLicenseExpiry <= 30) {
                //If within 30 days, then remind again at 15 days.
                setHideUtilExpiry(15);
            } else if (daysToLicenseExpiry <= 45) {
                //If within 45 days, then remind again at 30 days.
                setHideUtilExpiry(30);
            } else {
                //If within 90 days, then remind again at 45 days.
                setHideUtilExpiry(45);
            }
        }
    }

    @Override
    public void clearRemindMe() {
        if (!havePermission()) {
            return;
        }

        clearHideUntilExpiry();
        clearHideUtilMaintenance();
    }

    private String renderNotification(String templateToRender, ExpirationStateAccumulator licenseChecker) {
        try {
            return rendererProvider.getRenderer().render(
                    "jira.webresources:soy-templates",
                    templateToRender,
                    ImmutableMap.of(
                            "days", licenseChecker.getDaysToExpire(),
                            "mac", getMacUrl(),
                            "licenseId", licenseChecker.getApplicationDescription(),
                            "isEvaluation", licenseChecker.isEvaluation()
                    ));
        } catch (SoyException e) {
            LOG.debug("Unable to render banner.", e);
            return "";
        }
    }

    @Nonnull
    private String renderExpiryBanner(ExpirationStateAccumulator licenseChecker) {
        final String templateToRender = licenseChecker.isMultipleExpiring() ?
                "JIRA.Templates.LicenseBanner.expiryBannerMultiple" : "JIRA.Templates.LicenseBanner.expiryBanner";
        return renderNotification(templateToRender, licenseChecker);
    }

    private String renderMaintenanceFlag(ExpirationStateAccumulator licenseChecker) {
        final String templateToRender = licenseChecker.isMultipleExpiring() ?
                "JIRA.Templates.LicenseBanner.maintenanceFlagMultiple" : "JIRA.Templates.LicenseBanner.maintenanceFlag";
        return renderNotification(templateToRender, licenseChecker);
    }

    private String renderExpiryFlag(ExpirationStateAccumulator licenseChecker) {
        final String templateToRender = licenseChecker.isMultipleExpiring() ?
                "JIRA.Templates.LicenseBanner.expiryFlagMultiple" : "JIRA.Templates.LicenseBanner.expiryFlag";
        return renderNotification(templateToRender, licenseChecker);
    }

    /**
     * Returns a URL with appropriate params to renew the given {@link LicenseDetails}.
     */
    private String getMacUrl() {
        return baseUrl.getBaseUrl() + LINK_MANAGE_APPLICATION;
    }

    /**
     * If we dismiss license expiry we also dismiss maintenance ones because they have lower priority
     */
    private void setHideUtilExpiry(int days) {
        setRemindMeDays(days, EXPIRY_KEY);
        setHideUtilMaintenance(days);
    }

    /**
     * If we dismiss maintenance flags we will still get expiry ones because they have higher priority
     */
    private void setHideUtilMaintenance(final int days) {
        setRemindMeDays(days, MAINTENANCE_KEY);
    }

    private void setRemindMeDays(int days, final String key) {
        PropertySet propertySet = getPropertySet();
        propertySet.setInt(key, days);
    }

    private void clearHideUntilExpiry() {
        clearRemindMeDays(EXPIRY_KEY);
    }

    private void clearHideUtilMaintenance() {
        clearRemindMeDays(MAINTENANCE_KEY);
    }

    private void clearRemindMeDays(final String key) {
        PropertySet propertySet = getPropertySet();
        if (propertySet != null && propertySet.exists(key)) {
            propertySet.remove(key);
        }
    }

    private int getHideUntilMaintenance() {
        return getRemindMeDays(MAINTENANCE_KEY);
    }

    private int getHideUntilExpiry() {
        return getRemindMeDays(EXPIRY_KEY);
    }

    private int getRemindMeDays(final String key) {
        PropertySet propertySet = getPropertySet();
        if (propertySet != null && propertySet.exists(key)) {
            return propertySet.getInt(key);
        } else {
            return Integer.MAX_VALUE;
        }
    }

    private PropertySet getPropertySet() {
        return propertyManager.getPropertySet(context.getUser());
    }

    private boolean havePermission() {
        ApplicationUser user = context.getUser();
        return !Users.isAnonymous(user) && globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user);
    }
}
