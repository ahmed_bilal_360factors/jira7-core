package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.Update;
import com.atlassian.jira.upgrade.AbstractUpgradeTask;
import com.atlassian.jira.upgrade.UpgradeTask;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;

/**
 * Updates radiobutton and multi-checkbox searchers to be multi-select searchers
 *
 * @since v5.2
 */
public class UpgradeTask_Build810 extends AbstractUpgradeTask {
    private static final String MULTI_SELECT_SEARCHER = "com.atlassian.jira.plugin.system.customfieldtypes:multiselectsearcher";
    private static final String RADIOBUTTONS_SEARCHER = "com.atlassian.jira.plugin.system.customfieldtypes:radiosearcher";
    private static final String MULTI_CHECKBOX_SEARCHER = "com.atlassian.jira.plugin.system.customfieldtypes:checkboxsearcher";
    private static final String FIELD_NAME = "customfieldsearcherkey";

    private final EntityEngine entityEngine;

    public UpgradeTask_Build810(EntityEngine entityEngine) {
        this.entityEngine = entityEngine;
    }

    @Override
    public int getBuildNumber() {
        return 810;
    }

    @Override
    public String getShortDescription() {
        return "Updating radiobutton and multi-checkbox searchers to be multi-select searchers";
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        // Flagged as not downgradable because it was created before the Downgrade Task framework was built.
        return true;
    }

    @Override
    public void doUpgrade(boolean setupMode) {
        entityEngine.execute(Update.into("CustomField").set(FIELD_NAME, MULTI_SELECT_SEARCHER).whereEqual(FIELD_NAME, RADIOBUTTONS_SEARCHER));
        entityEngine.execute(Update.into("CustomField").set(FIELD_NAME, MULTI_SELECT_SEARCHER).whereEqual(FIELD_NAME, MULTI_CHECKBOX_SEARCHER));
    }

    @Override
    public Collection<String> getErrors() {
        return Collections.emptyList();
    }

    @Override
    public ScheduleOption getScheduleOption() {
        return ScheduleOption.BEFORE_JIRA_STARTED;
    }

    @Nullable
    @Override
    public Integer dependsUpon() {
        return 809;
    }

}
