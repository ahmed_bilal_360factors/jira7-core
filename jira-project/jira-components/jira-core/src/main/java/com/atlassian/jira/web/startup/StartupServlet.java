package com.atlassian.jira.web.startup;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.ComponentManager.State;
import com.atlassian.jira.ComponentManagerStateImpl;
import com.atlassian.johnson.Johnson;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableMap;
import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.MappingJsonFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.atlassian.jira.web.startup.StartupPageSupport.returnFromStartupJsp;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_SERVICE_UNAVAILABLE;

/**
 * The startup servlet serves up {@code startup.jsp}, or returns a JSON blob reporting the status,
 * as indicated by the request headers.
 *
 * @since v7.1.0
 */
public class StartupServlet extends HttpServlet {
    static final String ACCEPT = "Accept";
    static final String APPLICATION_JSON = "application/json";
    static final String TEXT_HTML = "text/html";
    static final String CHARSET_UTF_8 = ";charset=UTF-8";
    static final JsonFactory JSON = new MappingJsonFactory();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (APPLICATION_JSON.equals(request.getHeader(ACCEPT))) {
            //startup.jsp polls for progress by requesting JSON. To facilitate that usage, even if startup is
            //complete, always return progress information rather than redirecting
            respondWithJson(request, response);
        } else if (hasJohnsonEvents(request) || getCurrentState().isStarted()) {
            returnFromStartupJsp(request, response);
        } else {
            response.setContentType(TEXT_HTML + CHARSET_UTF_8);
            response.setStatus(SC_SERVICE_UNAVAILABLE);
            includeStartupJsp(request, response);
        }
    }

    private void respondWithJson(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        if (hasJohnsonEvents(request)) {
            // Force startup.jsp to reload the page
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
            response.getWriter().flush();
        } else {
            final State state = getCurrentState();
            response.setStatus(SC_OK);
            response.setContentType(APPLICATION_JSON + CHARSET_UTF_8);
            writeJson(request, response, state);
        }
    }

    @VisibleForTesting
    protected void writeJson(final HttpServletRequest request, final HttpServletResponse response, final State state) throws IOException {
        try (JsonGenerator jsonGenerator = JSON.createJsonGenerator(response.getOutputStream(), JsonEncoding.UTF8)) {
            jsonGenerator.writeObject(ImmutableMap.of(
                    "progress", ImmutableMap.of(
                            "message", StartupPageSupport.getTranslator(request).apply(state.getMessageKey()),
                            "percentage", state.getPercentage()),
                    "state", state));
        }
    }

    @VisibleForTesting
    protected State getCurrentState() {
        final State state = ComponentManager.getInstance().getState();
        if (state.isStarted() && !StartupPageSupport.isLaunched()) {
            // Hide the STARTED state if we haven't unlocked requests, yet
            return ComponentManagerStateImpl.COMPONENTS_INSTANTIATED;
        }
        return state;
    }

    @VisibleForTesting
    protected boolean hasJohnsonEvents(HttpServletRequest request) {
        return Johnson.getEventContainer(request.getServletContext()).hasEvents();
    }

    @VisibleForTesting
    protected void includeStartupJsp(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/startup.jsp").include(request, response);
    }

}
