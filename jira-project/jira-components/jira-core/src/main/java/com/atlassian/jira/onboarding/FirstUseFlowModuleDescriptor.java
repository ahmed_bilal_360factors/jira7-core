package com.atlassian.jira.onboarding;

import com.atlassian.jira.plugin.AbstractJiraModuleDescriptor;
import com.atlassian.jira.plugin.webfragment.descriptors.ConditionDescriptorFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.descriptors.ConditionalDescriptor;
import com.google.common.annotations.VisibleForTesting;
import org.dom4j.Element;

import javax.annotation.Nullable;

public class FirstUseFlowModuleDescriptor extends AbstractJiraModuleDescriptor<FirstUseFlow>
        implements Comparable<FirstUseFlowModuleDescriptor>, ConditionalDescriptor {
    @VisibleForTesting
    static final int UNWEIGHTED = -1;

    private int weight;
    private Condition condition;

    private Element element;
    private Plugin plugin;

    private final ConditionDescriptorFactory conditionDescriptorFactory;

    public FirstUseFlowModuleDescriptor(final JiraAuthenticationContext authenticationContext, final ModuleFactory moduleFactory,
                                        final ConditionDescriptorFactory conditionDescriptorFactory) {
        super(authenticationContext, moduleFactory);
        this.conditionDescriptorFactory = conditionDescriptorFactory;
    }

    @Override
    public void init(final Plugin plugin, final Element element) throws PluginParseException {
        super.init(plugin, element);

        final String weightValue = element.attributeValue("weight");
        try {
            this.weight = Integer.parseInt(weightValue);
        } catch (NumberFormatException e) {
            this.weight = UNWEIGHTED;
        }

        this.plugin = plugin;
        this.element = element;
    }

    @Override
    public void enabled() {
        super.enabled();

        // this was moved to the enabled() method because spring beans declared
        // by the plugin are not available for injection during the init() phase
        condition = conditionDescriptorFactory.retrieveCondition(plugin, element);
    }

    @Override
    public void disabled() {
        super.disabled();
        condition = null;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public int compareTo(@Nullable final FirstUseFlowModuleDescriptor o) {
        if (o == null) {
            // Nulls are pushed to the "right" (or the end) of a sort
            return -1;
        }
        return Integer.valueOf(o.getWeight()).compareTo(getWeight());
    }

    @Override
    public Condition getCondition() {
        return condition;
    }
}
