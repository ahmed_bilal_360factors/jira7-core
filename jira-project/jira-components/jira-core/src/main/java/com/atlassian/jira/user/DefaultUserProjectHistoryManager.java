package com.atlassian.jira.user;

import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.google.common.base.Function;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Convienience wrapper for the {@link com.atlassian.jira.user.UserHistoryManager} that deals directly with Projects.
 *
 * @since v4.0
 */
public class DefaultUserProjectHistoryManager implements UserProjectHistoryManager {
    private final PermissionManager permissionManager;
    private final ProjectManager projectManager;
    private final UserHistoryManager userHistoryManager;

    public DefaultUserProjectHistoryManager(UserHistoryManager userHistoryManager, ProjectManager projectManager,
                                            PermissionManager permissionManager) {
        this.userHistoryManager = userHistoryManager;
        this.projectManager = projectManager;
        this.permissionManager = permissionManager;
    }

    public void addProjectToHistory(ApplicationUser user, Project project) {
        notNull("project", project);
        userHistoryManager.addItemToHistory(UserHistoryItem.PROJECT, user, project.getId().toString());
    }

    public boolean hasProjectHistory(int permission, ApplicationUser user) {
        final List<UserHistoryItem> history = userHistoryManager.getHistory(UserHistoryItem.PROJECT, user);
        if (history != null) {
            for (final UserHistoryItem historyItem : history) {
                final Project project = projectManager.getProjectObj(Long.valueOf(historyItem.getEntityId()));
                if (project != null && permissionManager.hasPermission(new ProjectPermissionKey(permission), project, user)) {
                    return true;
                }
            }
        }
        return false;
    }

    public Project getCurrentProject(int permission, ApplicationUser user) {
        final List<UserHistoryItem> history = userHistoryManager.getHistory(UserHistoryItem.PROJECT, user);
        if (history != null) {
            for (final UserHistoryItem historyItem : history) {
                final Project project = projectManager.getProjectObj(Long.valueOf(historyItem.getEntityId()));
                if (project != null && permissionManager.hasPermission(new ProjectPermissionKey(permission), project, user)) {
                    return project;
                }
            }
        }
        return null;
    }

    public List<UserHistoryItem> getProjectHistoryWithoutPermissionChecks(ApplicationUser user) {
        return userHistoryManager.getHistory(UserHistoryItem.PROJECT, user);
    }

    @Override
    public List<Project> getProjectHistoryWithPermissionChecks(final int permission, final ApplicationUser user) {
        return getProjectHistoryWithPermissionChecks(user, new Function<Project, Boolean>() {
            @Override
            public Boolean apply(@Nullable final Project project) {
                return permissionManager.hasPermission(new ProjectPermissionKey(permission), project, user);
            }
        });
    }

    @Override
    public List<Project> getProjectHistoryWithPermissionChecks(final ProjectAction projectAction, final ApplicationUser user) {
        return getProjectHistoryWithPermissionChecks(user, new Function<Project, Boolean>() {
            @Override
            public Boolean apply(@Nullable final Project project) {
                return projectAction.hasPermission(permissionManager, user, project);
            }
        });
    }

    private List<Project> getProjectHistoryWithPermissionChecks(final ApplicationUser user, Function<Project, Boolean> permissionCheck) {
        final List<UserHistoryItem> history = getProjectHistoryWithoutPermissionChecks(user);
        if (history != null) {
            final List<Project> returnList = new ArrayList<Project>();
            for (UserHistoryItem userHistoryItem : history) {
                final Project project = projectManager.getProjectObj(Long.valueOf(userHistoryItem.getEntityId()));

                if (project != null && permissionCheck.apply(project)) {
                    returnList.add(project);
                }
            }
            return returnList;
        } else {
            return Collections.emptyList();
        }
    }
}
