package com.atlassian.jira.user;

import com.atlassian.jira.issue.Issue;
import com.google.common.collect.ImmutableSortedSet;
import org.springframework.util.Assert;

import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.SortedSet;

public class ImmutableUserIssueRelevance implements UserIssueRelevance {
    private final ApplicationUser user;
    private final Issue issue;
    private final ImmutableSortedSet<IssueInvolvement> issueInvolvements;
    private final Optional<Integer> highestIssueInvolvementRank;
    private final Optional<Date> latestCommentCreationTime;

    private ImmutableUserIssueRelevance(final ApplicationUser user,
                                        final Issue issue,
                                        final ImmutableSortedSet<IssueInvolvement> issueInvolvements,
                                        final Date latestCommentCreationTime) {
        this.user = user;
        this.issue = issue;
        this.issueInvolvements = issueInvolvements;
        this.highestIssueInvolvementRank = issueInvolvements.isEmpty() ? Optional.empty() : Optional.of(issueInvolvements.first().ordinal());
        this.latestCommentCreationTime = Optional.ofNullable(latestCommentCreationTime);
    }

    public ApplicationUser getUser() {
        return user;
    }

    public Issue getIssue() {
        return issue;
    }

    public SortedSet<IssueInvolvement> getIssueInvolvements() {
        return issueInvolvements;
    }

    public Optional<Integer> getHighestIssueInvolvementRank() {
        return highestIssueInvolvementRank;
    }

    public Optional<Date> getLatestCommentCreationTime() {
        return latestCommentCreationTime;
    }

    /**
     * Order by highest involvement, then by latest comment date, then by
     * username.
     * <p>
     * Note: can only compare relevancy of users for the same issue.
     *
     * @param o The object we're comparing to
     * @return A negative integer, zero, or a positive integer as this object is
     * less than, equal to, or greater than the specified object
     */
    public int compareTo(final UserIssueRelevance o) {
        Assert.isTrue(
                this.getIssue().equals(o.getIssue()),
                "UserIssueRelevance's can only be compared if they share the same issue."
        );


        int result = compareOptionals(
                this.getHighestIssueInvolvementRank(),
                o.getHighestIssueInvolvementRank(),
                Integer::compareTo);

        if (result == 0) {
            result = -compareOptionals(
                    this.getLatestCommentCreationTime(),
                    o.getLatestCommentCreationTime(),
                    Date::compareTo);
        }

        if (result == 0) {
            result = this.getUser().getDisplayName().compareTo(o.getUser().getDisplayName());
        }

        return result;
    }

    public static class IssueUserBuilder {
        private ApplicationUser user;
        private Issue issue;
        private final HashSet<IssueInvolvement> issueInvolvements;
        private Date latestCommentCreationTime;

        public IssueUserBuilder() {
            issueInvolvements = new HashSet<>();
        }

        public IssueUserBuilder withUser(final ApplicationUser user) {
            this.user = user;
            return this;
        }

        public IssueUserBuilder withIssue(final Issue issue) {
            this.issue = issue;
            return this;
        }

        public IssueUserBuilder withAssigneeInvolvement() {
            this.issueInvolvements.add(IssueInvolvement.ASSIGNEE);
            return this;
        }

        public IssueUserBuilder withReporterInvolvement() {
            this.issueInvolvements.add(IssueInvolvement.REPORTER);
            return this;
        }

        public IssueUserBuilder withCommenterInvolvement(final Date latestCommentCreationTime) {
            this.issueInvolvements.add(IssueInvolvement.COMMENTER);
            this.latestCommentCreationTime = latestCommentCreationTime;
            return this;
        }

        public IssueUserBuilder withWatcherInvolvement() {
            this.issueInvolvements.add(IssueInvolvement.WATCHER);
            return this;
        }

        public IssueUserBuilder withVoterInvolvement() {
            this.issueInvolvements.add(IssueInvolvement.VOTER);
            return this;
        }

        public UserIssueRelevance build() {
            return new ImmutableUserIssueRelevance(
                    user,
                    issue,
                    ImmutableSortedSet.copyOf(issueInvolvements),
                    latestCommentCreationTime
            );
        }
    }

    private <T> int compareOptionals(final Optional<T> a,
                                     final Optional<T> b,
                                     final Comparator<T> comparator) {
        if (a.isPresent()) {
            return b.isPresent() ? comparator.compare(a.get(), b.get()) : -1;
        } else {
            return b.isPresent() ? 1 : 0;
        }
    }
}
