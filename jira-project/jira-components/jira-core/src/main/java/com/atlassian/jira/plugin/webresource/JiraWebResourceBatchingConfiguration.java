package com.atlassian.jira.plugin.webresource;

import com.atlassian.jira.config.InstanceFeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.util.UserAgentUtil;
import com.atlassian.jira.util.UserAgentUtilImpl;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.plugin.webresource.DefaultResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.google.common.collect.ImmutableList;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.atlassian.plugin.webresource.DefaultResourceBatchingConfiguration.PLUGIN_WEBRESOURCE_BATCHING_OFF;
import static com.atlassian.plugin.webresource.DefaultResourceBatchingConfiguration.PLUGIN_WEB_RESOURCE_JAVASCRIPT_TRY_CATCH_WRAPPING;

/**
 * Determines which resources are included superbatched on every page!
 *
 * @since v4.3
 */
public class JiraWebResourceBatchingConfiguration implements ResourceBatchingConfiguration {
    @SuppressWarnings("RedundantStringToString")
    public static final String PLUGIN_WEB_RESOURCE_BATCH_CONTENT_TRACKING = "plugin.webresource.batch.content.tracking".toString();
    private static final String PLUGIN_WEB_RESOURCE_BATCH_CONTEXT_ALWAYS_RESPLIT_FEATURE_KEY = "plugin.webresource.batch.context.always.resplit";
    private static final String USER_AGENT = "USER-AGENT";

    private final List<String> resources;

    private final JiraProperties jiraSystemProperties;
    private final InstanceFeatureManager featureManager;

    public JiraWebResourceBatchingConfiguration(final JiraProperties jiraSystemProperties,
                                                final InstanceFeatureManager featureManager) {
        this.jiraSystemProperties = jiraSystemProperties;
        this.featureManager = featureManager;
        final ImmutableList.Builder<String> resourcesBuilder = ImmutableList.<String>builder().add("jira.webresources:superbatch-default");
        if (featureManager.isInstanceFeatureEnabled("plugin.webresource.batch.timezone")) {
            resourcesBuilder.add("com.atlassian.jira.jira-tzdetect-plugin:tzdetect-lib");
        }
        if (featureManager.isInstanceFeatureEnabled("APDEX-218")) {
            resourcesBuilder.add("com.atlassian.applinks.applinks-plugin:applinks-public");
            resourcesBuilder.add("com.atlassian.jira.jira-quick-edit-plugin:quick-create-issue");
            resourcesBuilder.add("com.atlassian.jira.jira-quick-edit-plugin:quick-edit-issue");
            resourcesBuilder.add("com.atlassian.auiplugin:aui-alignment");
            resourcesBuilder.add("com.atlassian.auiplugin:aui-help");
            resourcesBuilder.add("com.atlassian.auiplugin:dialog2");
            resourcesBuilder.add("com.atlassian.auiplugin:ajs-raf");
            resourcesBuilder.add("com.atlassian.plugins.helptips.jira-help-tips:common");
            resourcesBuilder.add("com.atlassian.auiplugin:aui-inline-dialog2");
            resourcesBuilder.add("com.atlassian.jira.jira-share-plugin:share-resources");
            resourcesBuilder.add("com.atlassian.jira.jira-share-plugin:share-resources-init");
            resourcesBuilder.add("com.atlassian.jira.jira-view-issue-plugin:soy-templates");
            resourcesBuilder.add("com.atlassian.plugins.browser.metrics.browser-metrics-plugin:api");
            resourcesBuilder.add("com.atlassian.jira.jira-projects-plugin:browser-util");
            resourcesBuilder.add("com.atlassian.plugins.atlassian-plugins-webresource-rest:curl");
            resourcesBuilder.add("com.atlassian.pocketknife.featureflags-plugin:pocketknife-feature-flags");
            resourcesBuilder.add("com.atlassian.plugins.atlassian-plugins-webresource-rest:web-resource-manager");
            resourcesBuilder.add("com.atlassian.servicedesk:cv-sd-shared");
            resourcesBuilder.add("com.atlassian.servicedesk:sd-request-type-selector");
            resourcesBuilder.add("com.atlassian.servicedesk:sd-sla-simple-view-resource");
            resourcesBuilder.add("com.atlassian.servicedesk:servicedesk-global");
            resourcesBuilder.add("is.origo.jira.tempo-plugin:tempo-global");
            resourcesBuilder.add("jira.webresources:momentjs");
            resourcesBuilder.add("com.atlassian.jira.jira-projects-plugin:sidebar-switch-analytics");
            resourcesBuilder.add("com.pyxis.greenhopper.jira:gh-analytics-tracker");
        }
        this.resources = resourcesBuilder.build();
    }

    @Override
    public boolean isSuperBatchingEnabled() {
        return (!resources.isEmpty() && !jiraSystemProperties.isSuperBatchingDisabled()) || forceBatchingInThisRequest();
    }

    @Override
    public boolean isContextBatchingEnabled() {
        return (!jiraSystemProperties.isDevMode() && !featureManager.isInstanceFeatureEnabled("APDEX-162")
                && !jiraSystemProperties.isContextBatchingDisabled()) || forceBatchingInThisRequest();
    }

    @Override
    public boolean isPluginWebResourceBatchingEnabled() {
        final boolean forced = forceBatchingInThisRequest();
        if (forced) {
            return true;
        }

        if (jiraSystemProperties.getProperty(PLUGIN_WEBRESOURCE_BATCHING_OFF) != null) {
            return !Boolean.parseBoolean(jiraSystemProperties.getProperty(PLUGIN_WEBRESOURCE_BATCHING_OFF));
        } else {
            return !jiraSystemProperties.isDevMode();
        }
    }

    @Override
    public List<String> getSuperBatchModuleCompleteKeys() {
        return resources;
    }

    @Override
    public boolean isJavaScriptTryCatchWrappingEnabled() {
        return jiraSystemProperties.getBoolean(PLUGIN_WEB_RESOURCE_JAVASCRIPT_TRY_CATCH_WRAPPING);
    }

    @Override
    public boolean isBatchContentTrackingEnabled() {
        return jiraSystemProperties.getBoolean(PLUGIN_WEB_RESOURCE_BATCH_CONTENT_TRACKING);
    }

    @Override
    public boolean resplitMergedContextBatchesForThisRequest() {
        return featureManager.isInstanceFeatureEnabled(PLUGIN_WEB_RESOURCE_BATCH_CONTEXT_ALWAYS_RESPLIT_FEATURE_KEY);
    }

    @Override
    public boolean isSourceMapEnabled() {
        return jiraSystemProperties.getBoolean(DefaultResourceBatchingConfiguration.PLUGIN_WEB_RESOURCE_SOURCE_MAP_ENABLED);
    }

    @Override
    public boolean optimiseSourceMapsForDevelopment() {
        return jiraSystemProperties.getBoolean(DefaultResourceBatchingConfiguration.PLUGIN_WEB_RESOURCE_SOURCE_MAP_OPTIMISED_FOR_DEVELOPMENT);
    }

    private boolean forceBatchingInThisRequest() {
        // only force batching if in DEV mode -- this whole "forcing" concept is a developer helper only
        return jiraSystemProperties.isDevMode() && isCurrentRequestIE();
    }

    private boolean isCurrentRequestIE() {
        final HttpServletRequest httpRequest = ExecutingHttpRequest.get();
        if (httpRequest == null) {
            return false;
        }
        final String userAgent = httpRequest.getHeader(USER_AGENT);
        final UserAgentUtil userAgentUtil = new UserAgentUtilImpl();
        final UserAgentUtil.UserAgent userAgentInfo = userAgentUtil.getUserAgentInfo(userAgent);

        // force batching if we are on IE, disable otherwise
        return userAgentInfo.getBrowser().getBrowserFamily().equals(UserAgentUtil.BrowserFamily.MSIE)
                || userAgentInfo.getBrowser().getBrowserFamily().equals(UserAgentUtil.BrowserFamily.IE);
    }
}
