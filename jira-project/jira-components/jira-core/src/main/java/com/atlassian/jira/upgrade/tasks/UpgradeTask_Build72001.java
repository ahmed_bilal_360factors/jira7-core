package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.model.querydsl.GenericConfigurationDTO;
import com.atlassian.jira.model.querydsl.JiraRelationalPathBase;
import com.atlassian.jira.upgrade.AbstractDelayableUpgradeTask;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpgradeTask_Build72001 extends AbstractDelayableUpgradeTask {
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask_Build72001.class);
    private static final String DEFAULT_DATE = "<sql-timestamp>0001-01-01 00:00:00.0</sql-timestamp>";
    private final QueryDslAccessor db;

    public UpgradeTask_Build72001(final QueryDslAccessor dbConnectionManager) {
        this.db = dbConnectionManager;
    }

    @Override
    public int getBuildNumber() {
        return 72001;
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        // This upgrade task fixes wrong data introduced by a bug in serialization
        return false;
    }

    @Override
    public String getShortDescription() {
        return "Fix default date and time values for custom fields";
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception {
        final QGenericConfiguration GENERIC_CONFIGURATION = new QGenericConfiguration("GENERIC_CONFIGURATION");
        
        long affectedRows;
        if (this.isMSSQL()) {
            // For MSSQL we're using deprecated ntext for xml value field
            // and NOT EQUALS throws "The data types ntext and varchar are incompatible in the not equal to operator."
            affectedRows = this.db.executeQuery(dbConnection -> dbConnection.update(GENERIC_CONFIGURATION)
                    .set(GENERIC_CONFIGURATION.xmlvalue, DEFAULT_DATE)
                    .where(GENERIC_CONFIGURATION.xmlvalue.like("<sql-timestamp>0001-%"),
                            GENERIC_CONFIGURATION.xmlvalue.notLike(DEFAULT_DATE))
                    .execute());
        } else {
            affectedRows = this.db.executeQuery(dbConnection -> dbConnection.update(GENERIC_CONFIGURATION)
                    .set(GENERIC_CONFIGURATION.xmlvalue, DEFAULT_DATE)
                    .where(GENERIC_CONFIGURATION.xmlvalue.like("<sql-timestamp>0001-%"),
                            GENERIC_CONFIGURATION.xmlvalue.ne(DEFAULT_DATE))
                    .execute());
        }
        if (affectedRows > 0) {
            log.info("Fixed {} default custom fields dates.", affectedRows);
        }
    }

    // Copied definition here in case the model changes in the future
    private static class QGenericConfiguration extends JiraRelationalPathBase<GenericConfigurationDTO> {
        public final NumberPath<Long> id = createNumber("id", Long.class);
        public final StringPath datatype = createString("datatype");
        public final StringPath datakey = createString("datakey");
        public final StringPath xmlvalue = createString("xmlvalue");

        public QGenericConfiguration(String alias) {
            super(GenericConfigurationDTO.class, alias, "genericconfiguration");
        }

        @Override
        public String getEntityName() {
            return "GenericConfiguration";
        }
    }
}
