package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.model.querydsl.QAvatar;
import com.atlassian.jira.model.querydsl.QIssueType;
import com.atlassian.jira.model.querydsl.QOSPropertyEntry;
import com.atlassian.jira.model.querydsl.QOSPropertyString;
import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.querydsl.core.group.GroupBy;
import com.querydsl.sql.SQLExpressions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;


/**
 * Replacing system avatars with new SVG icons.
 *
 * @since v7.0
 */
public class UpgradeTask_Build70024 extends AbstractImmediateUpgradeTask {
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask_Build70024.class);

    static final int TRUE = 1;
    static final String ISSUE_TYPE_AVATAR = "issuetype";
    static final String PROJECT_AVATAR = "project";
    static final String USER_AVATAR = "user";
    static final String SVG_CONTENT_TYPE = "image/svg+xml";
    static final String DEFAULT_PROJECT_AVATAR_KEY = "jira.avatar.default.id";
    static final String DEFAULT_PROJECT_AVATAR_NAME = "default.svg";

    static final Map<String, String> issueTypeIconsToReplace = ImmutableMap.<String, String>builder()
            .put("subtask_alternate.png", "subtask.png")
            .put("task_agile.png", "task.png")
            .put("delete.png", "remove_feature.png")
            .put("blank.png", "genericissue.png")
            .put("all_unassigned.png", "genericissue.png")
            .build();

    static final Map<String, String> issueTypeIconsToRename = ImmutableMap.<String, String>builder()
            .put("task.png", "task.svg")
            .put("subtask.png", "subtask.svg")
            .put("story.png", "story.svg")
            .put("improvement.png", "improvement.svg")
            .put("sales.png", "sales.svg")
            .put("health.png", "design_task.svg")
            .put("remove_feature.png", "remove_feature.svg")
            .put("requirement.png", "requirement.svg")
            .put("newfeature.png", "newfeature.svg")
            .put("exclamation.png", "exclamation.svg")
            .put("documentation.png", "documentation.svg")
            .put("defect.png", "defect.svg")
            .put("epic.png", "epic.svg")
            .put("genericissue.png", "genericissue.svg")
            .put("bug.png", "bug.svg")
            .put("undefined.png", "question.svg")
            .build();

    static final Map<String, String> defaultIssueTypeIconsToRename = ImmutableMap.<String, String>builder()
            .put("/images/icons/issuetypes/bug.png", "bug.svg")
            .put("/images/icons/issuetypes/newfeature.png", "newfeature.svg")
            .put("/images/icons/issuetypes/task.png", "task.svg")
            .put("/images/icons/issuetypes/improvement.png", "improvement.svg")
            .put("/images/icons/issuetypes/subtask_alternate.png", "subtask.svg")
            .put("/images/icons/issuetypes/subtask.png", "subtask.svg")
            .put("/images/icons/issuetypes/genericissue.png", "genericissue.svg")
            .put("/images/icons/issuetypes/epic.png", "epic.svg")
            .put("/images/icons/issuetypes/story.png", "story.svg")
            .put("/images/icons/issuetypes/sales.png", "sales.svg")
            .put("/images/icons/ico_epic.png", "epic.svg")
            .put("/images/icons/ico_story.png", "story.svg")
            .put("/download/resources/com.pyxis.greenhopper.jira:greenhopper-webactions/images/ico_epic.png", "epic.svg")
            .put("/download/resources/com.pyxis.greenhopper.jira:greenhopper-webactions/images/ico_defect.png", "defect.svg")
            .put("/download/resources/com.pyxis.greenhopper.jira:greenhopper-webactions/images/ico_bug.png", "bug.svg")
            .put("/download/resources/com.pyxis.greenhopper.jira:greenhopper-webactions/images/ico_story.png", "story.svg")
            .build();

    static final List<String> issueTypeIconsToAdd = ImmutableList.of("development_task.svg", "feedback.svg", "request_access.svg");

    static final Map<String, String> projectIconsToRename = ImmutableMap.<String, String>builder()
            .put("cloud.png", "cloud.svg")
            .put("config.png", "spanner.svg")
            .put("disc.png", "cd.svg")
            .put("eamesbird.png", "bird.svg")
            .put("finance.png", "money.svg")
            .put("hand.png", "mouse-hand.svg")
            .put("kangaroo.png", "koala.svg")
            .put("new_monster.png", "yeti.svg")
            .put("power.png", "power.svg")
            .put("rainbow.png", "nature.svg")
            .put("refresh.png", "refresh.svg")
            .put("rocket.png", "rocket.svg")
            .put("servicedesk.png", "phone.svg")
            .put("settings.png", "settings.svg")
            .put("storm.png", "storm.svg")
            .put("travel.png", "plane.svg")
            .build();

    static final List<String> projectIconsToAdd = ImmutableList.of("default.svg", "code.svg", "coffee.svg",
            "design.svg", "drill.svg", "food.svg", "notes.svg", "red-flag.svg", "science.svg", "spanner.svg", "support.svg");

    static final Map<String, String> userIconsToUpdate = ImmutableMap.<String, String>builder()
            .put("Avatar-default.png", "Avatar-default.svg")
            .build();

    static final List<String> userIconsToAdd = ImmutableList.of("bull.svg", "cat.svg", "dog.svg", "female_1.svg", "female_2.svg",
            "female_3.svg", "female_4.svg", "ghost.svg", "male_1.svg", "male_2.svg", "male_3.svg", "male_4.svg", "male_5.svg",
            "male_6.svg", "male_8.svg", "owl.svg", "pirate.svg", "robot.svg", "vampire.svg");

    private final DbConnectionManager dbConnectionManager;

    public UpgradeTask_Build70024(final DbConnectionManager dbConnectionManager) {
        this.dbConnectionManager = dbConnectionManager;
    }

    @Override
    public int getBuildNumber() {
        return 70024;
    }

    @Override
    public String getShortDescription() {
        return "Replacing system avatars with new SVG icons.";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception {
        upgradeIssueTypeIcons();
        upgradeProjectIcons();
        upgradeUserIcons();
    }

    private void upgradeIssueTypeIcons() {
        dbConnectionManager.execute(callback -> {

            callback.setAutoCommit(false);

            Long fallBackIconId = prepareFallBackIconForIssueType(callback);

            if (fallBackIconId == null) {
                log.warn("No system avatar icons found for issue types. No icons will be replaced.");
                return;
            }

            final QAvatar a = QAvatar.AVATAR;
            final QIssueType it = QIssueType.ISSUE_TYPE;

            final Map<String, Long> iconsToReplace = callback.newSqlQuery()
                    .from(a)
                    .where(a.avatarType.eq(ISSUE_TYPE_AVATAR)
                            .and(a.systemAvatar.eq(TRUE))
                            .and(a.fileName.in(issueTypeIconsToReplace.keySet())))
                    .groupBy(a.fileName)
                    .transform(GroupBy.groupBy(a.fileName).as(a.id.min()));

            final Map<String, Long> replacementIcons = callback.newSqlQuery()
                    .from(a)
                    .where(a.avatarType.eq(ISSUE_TYPE_AVATAR)
                            .and(a.systemAvatar.eq(TRUE))
                            .and(a.fileName.in(issueTypeIconsToReplace.values())))
                    .groupBy(a.fileName)
                    .transform(GroupBy.groupBy(a.fileName).as(a.id.min()));

            for (Map.Entry<String, String> iconToReplaceEntry : issueTypeIconsToReplace.entrySet()) {
                final String iconToReplace = iconToReplaceEntry.getKey();
                final String replacementIcon = iconToReplaceEntry.getValue();

                final Long iconToReplaceId = iconsToReplace.get(iconToReplace);
                Long replacementIconId = replacementIcons.get(replacementIcon);

                if (iconToReplaceId == null) {
                    log.warn("System icon: " + iconToReplace + " is not present.");
                    continue;
                }

                if (replacementIconId == null) {
                    log.warn("System icon: " + replacementIcon + " is not present. Using fallback icon.");
                    replacementIconId = fallBackIconId;
                }

                callback
                        .update(it)
                        .where(it.avatar.eq(iconToReplaceId))
                        .set(it.avatar, replacementIconId)
                        .execute();
            }

            callback
                    .delete(a)
                    .where(a.avatarType.eq(ISSUE_TYPE_AVATAR)
                            .and(a.systemAvatar.eq(TRUE))
                            .and(a.fileName.in(issueTypeIconsToReplace.keySet())))
                    .execute();

            for (Map.Entry<String, String> iconToRenameEntry : issueTypeIconsToRename.entrySet()) {
                final String oldIconName = iconToRenameEntry.getKey();
                final String newIconName = iconToRenameEntry.getValue();

                callback
                        .update(a)
                        .where(a.avatarType.eq(ISSUE_TYPE_AVATAR)
                                .and(a.systemAvatar.eq(TRUE))
                                .and(a.fileName.eq(oldIconName)))
                        .set(a.fileName, newIconName)
                        .set(a.contentType, SVG_CONTENT_TYPE)
                        .execute();
            }

            for (String iconToAdd : issueTypeIconsToAdd) {
                final long count = callback.newSqlQuery()
                        .select(a)
                        .from(a)
                        .where(a.avatarType.eq(ISSUE_TYPE_AVATAR)
                                .and(a.systemAvatar.eq(TRUE))
                                .and(a.fileName.eq(iconToAdd))
                                .and(a.contentType.eq(SVG_CONTENT_TYPE)))
                        .fetchCount();

                if (count == 0) {
                    callback
                            .insert(a)
                            .set(a.fileName, iconToAdd)
                            .set(a.contentType, SVG_CONTENT_TYPE)
                            .set(a.avatarType, ISSUE_TYPE_AVATAR)
                            .set(a.systemAvatar, TRUE)
                            .withId()
                            .execute();
                }
            }

            for (Map.Entry<String, String> defaultIssueTypeIcon : defaultIssueTypeIconsToRename.entrySet()) {
                final String oldIconUrl = defaultIssueTypeIcon.getKey();
                final String newIconFileName = defaultIssueTypeIcon.getValue();

                callback
                        .update(it)
                        .where(it.avatar.isNull()
                                .and(it.iconurl.eq(oldIconUrl)))
                        .set(it.avatar, SQLExpressions.select(a.id)
                                .from(a)
                                .where(a.avatarType.eq(ISSUE_TYPE_AVATAR)
                                        .and(a.systemAvatar.eq(TRUE))
                                        .and(a.fileName.eq(newIconFileName))))
                        .execute();
            }

            callback.commit();
        });
    }

    private Long prepareFallBackIconForIssueType(final DbConnection callback) {
        final QAvatar a = QAvatar.AVATAR;

        final Optional<Long> undefinedIconId = ofNullable(callback.newSqlQuery()
                .select(a.id)
                .from(a)
                .where((a.avatarType.eq(ISSUE_TYPE_AVATAR))
                        .and(a.systemAvatar.eq(TRUE))
                        .and(a.fileName.eq("undefined.png")))
                .orderBy(a.id.asc())
                .limit(1)
                .fetchOne());

        return undefinedIconId.orElseGet(
                () -> callback.newSqlQuery()
                        .select(a.id)
                        .from(a)
                        .where(a.avatarType.eq(ISSUE_TYPE_AVATAR)
                                .and(a.systemAvatar.eq(TRUE)))
                        .limit(1)
                        .fetchOne());
    }

    private void upgradeProjectIcons() {
        dbConnectionManager.execute(callback -> {

            callback.setAutoCommit(false);

            final QAvatar a = QAvatar.AVATAR;

            for (Map.Entry<String, String> projectIconToRenameEntry : projectIconsToRename.entrySet()) {
                final String oldIconName = projectIconToRenameEntry.getKey();
                final String newIconName = projectIconToRenameEntry.getValue();

                callback
                        .update(a)
                        .where(a.avatarType.eq(PROJECT_AVATAR)
                                .and(a.systemAvatar.eq(TRUE))
                                .and(a.fileName.eq(oldIconName)))
                        .set(a.fileName, newIconName)
                        .set(a.contentType, SVG_CONTENT_TYPE)
                        .execute();
            }

            for (String iconToAdd : projectIconsToAdd) {
                final long count = callback.newSqlQuery()
                        .select(a)
                        .from(a)
                        .where(a.avatarType.eq(PROJECT_AVATAR)
                                .and(a.systemAvatar.eq(TRUE))
                                .and(a.fileName.eq(iconToAdd))
                                .and(a.contentType.eq(SVG_CONTENT_TYPE)))
                        .fetchCount();

                if (count == 0) {
                    callback
                            .insert(a)
                            .set(a.fileName, iconToAdd)
                            .set(a.contentType, SVG_CONTENT_TYPE)
                            .set(a.avatarType, PROJECT_AVATAR)
                            .set(a.systemAvatar, TRUE)
                            .withId()
                            .execute();
                }
            }

            final QOSPropertyEntry entry = QOSPropertyEntry.O_S_PROPERTY_ENTRY;
            final QOSPropertyString property = QOSPropertyString.O_S_PROPERTY_STRING;

            final Long defaultProjectAvatarId = callback.newSqlQuery()
                    .select(a.id)
                    .from(a)
                    .where(a.avatarType.eq(PROJECT_AVATAR)
                            .and(a.systemAvatar.eq(TRUE))
                            .and(a.fileName.eq(DEFAULT_PROJECT_AVATAR_NAME)))
                    .limit(1)
                    .fetchOne();

            callback
                    .update(property)
                    .where(property.id.eq(SQLExpressions
                            .select(entry.id)
                            .from(entry)
                            .where(entry.propertyKey.eq(DEFAULT_PROJECT_AVATAR_KEY))))
                    .set(property.value, defaultProjectAvatarId.toString())
                    .execute();

            callback.commit();
        });
    }

    private void upgradeUserIcons() {
        dbConnectionManager.execute(callback -> {

            callback.setAutoCommit(false);

            final QAvatar a = QAvatar.AVATAR;

            for (final String userIcon : userIconsToAdd) {
                final long count = callback.newSqlQuery()
                        .select(a)
                        .from(a)
                        .where(a.avatarType.eq(USER_AVATAR)
                                .and(a.systemAvatar.eq(TRUE))
                                .and(a.fileName.eq(userIcon))
                                .and(a.contentType.eq(SVG_CONTENT_TYPE)))
                        .fetchCount();

                if (count == 0) {
                    callback.insert(a)
                            .set(a.fileName, userIcon)
                            .set(a.contentType, SVG_CONTENT_TYPE)
                            .set(a.avatarType, USER_AVATAR)
                            .set(a.systemAvatar, TRUE)
                            .withId()
                            .execute();
                }
            }

            for (Map.Entry<String, String> userIconToUpdate : userIconsToUpdate.entrySet()) {
                final String oldIconName = userIconToUpdate.getKey();
                final String newIconName = userIconToUpdate.getValue();

                callback.update(a)
                        .where(a.avatarType.eq(USER_AVATAR)
                                .and(a.systemAvatar.eq(TRUE))
                                .and(a.fileName.eq(oldIconName)))
                        .set(a.fileName, newIconName)
                        .set(a.contentType, SVG_CONTENT_TYPE)
                        .execute();
            }

            callback.commit();
        });
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        return true;
    }
}
