package com.atlassian.jira.issue.fields.layout.field;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.issue.fields.renderer.wiki.AtlassianWikiRenderer;

import static com.atlassian.jira.config.properties.APKeys.JIRA_OPTION_HTML_IN_CUSTOM_FIELDS_ENABLED;

/**
 * Render the system field description using wiki renderer for On Demand
 *
 * @since v5.1.1
 */
public class FieldDescriptionHelper {
    private final RendererManager rendererManager;
    private final ApplicationProperties applicationProperties;


    public FieldDescriptionHelper(RendererManager rendererManager, ApplicationProperties applicationProperties) {
        this.rendererManager = rendererManager;
        this.applicationProperties = applicationProperties;
    }

    public String getDescription(String fieldDescription) {
        String description = fieldDescription;
        if (!applicationProperties.getOption(JIRA_OPTION_HTML_IN_CUSTOM_FIELDS_ENABLED)) {
            description = rendererManager.getRenderedContent(AtlassianWikiRenderer.RENDERER_TYPE, fieldDescription, null);
        }
        return description;
    }
}
