package com.atlassian.jira.plugin.webresource;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.InitializingComponent;
import com.atlassian.jira.bc.dataimport.DataImportFinishedEvent;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;
import org.apache.commons.lang.math.NumberUtils;

/**
 * Invalidates cached superbatch when the plugin system finishes starting.
 * <p>
 * If the superbatch is retrieved during plugin system startup / bootstrap, an incomplete set of dependencies may be cached (JRA-32692).
 * </p>
 *
 * @since v6.5
 */
public class SuperBatchInvalidator implements InitializingComponent {
    private final ApplicationProperties applicationProperties;
    private final EventPublisher eventPublisher;

    public SuperBatchInvalidator(final ApplicationProperties applicationProperties, final EventPublisher eventPublisher) {
        this.applicationProperties = applicationProperties;
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void afterInstantiation() throws Exception {
        eventPublisher.register(this);
    }

    @EventListener
    public void pluginFrameworkStarted(final PluginFrameworkStartedEvent event) {
        incrementSuperbatchVersionIfJiraNotSetup();
    }

    @EventListener
    public void dataImportFinished(final DataImportFinishedEvent ignore) {
        incrementSuperbatchVersionIfJiraNotSetup();
    }

    private void incrementSuperbatchVersionIfJiraNotSetup() {
        if (applicationProperties.getString(APKeys.JIRA_SETUP) == null) {
            incrementSuperbatchVersion();
        }
    }

    private void incrementSuperbatchVersion() {
        final String versionString = getSuperBatchVersion();
        final Long superbatchVersion = NumberUtils.toLong(versionString, 1L);
        applicationProperties.setString(APKeys.WEB_RESOURCE_SUPER_BATCH_FLUSH_COUNTER, Long.toString(superbatchVersion + 1));
    }

    private String getSuperBatchVersion() {
        return applicationProperties.getDefaultBackedString(APKeys.WEB_RESOURCE_SUPER_BATCH_FLUSH_COUNTER);
    }
}
