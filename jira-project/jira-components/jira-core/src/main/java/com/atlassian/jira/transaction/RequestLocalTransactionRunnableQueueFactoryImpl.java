package com.atlassian.jira.transaction;

import com.atlassian.vcache.RequestCache;
import com.atlassian.vcache.VCacheFactory;

public class RequestLocalTransactionRunnableQueueFactoryImpl implements RequestLocalTransactionRunnableQueueFactory {

    private static final String KEY = RequestLocalTransactionRunnableQueueFactoryImpl.class.getName() + ".transactionRunnableQueue";

    //
    // This is tracked in a "request cached" because 1) sub transactions want to share the current runnable queue
    // and 2) it will be cleaned up AFTER the request finishes IF the Txn code does not clear
    // it first (which it does, but just in case)
    private final RequestCache<String, RunnablesQueue> requestAlignedEventQueue;

    public RequestLocalTransactionRunnableQueueFactoryImpl(VCacheFactory vCacheFactory) {
        requestAlignedEventQueue = vCacheFactory.getRequestCache(KEY);
    }

    @Override
    public RunnablesQueue getRunnablesQueue() {
        return requestAlignedEventQueue.get(KEY, RunnablesQueueImpl::new);
    }
}
