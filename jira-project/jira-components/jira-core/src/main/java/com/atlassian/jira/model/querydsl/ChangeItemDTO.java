package com.atlassian.jira.model.querydsl;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Generated;

/**
 * Data Transfer Object for the ChangeItem entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 *
 * @see QChangeItem
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class ChangeItemDTO implements DTO {
    private final Long id;
    private final Long group;
    private final String fieldtype;
    private final String field;
    private final String oldvalue;
    private final String oldstring;
    private final String newvalue;
    private final String newstring;

    public Long getId() {
        return id;
    }

    public Long getGroup() {
        return group;
    }

    public String getFieldtype() {
        return fieldtype;
    }

    public String getField() {
        return field;
    }

    public String getOldvalue() {
        return oldvalue;
    }

    public String getOldstring() {
        return oldstring;
    }

    public String getNewvalue() {
        return newvalue;
    }

    public String getNewstring() {
        return newstring;
    }

    public ChangeItemDTO(Long id, Long group, String fieldtype, String field, String oldvalue, String oldstring, String newvalue, String newstring) {
        this.id = id;
        this.group = group;
        this.fieldtype = fieldtype;
        this.field = field;
        this.oldvalue = oldvalue;
        this.oldstring = oldstring;
        this.newvalue = newvalue;
        this.newstring = newstring;
    }

    /**
     * Creates a GenericValue object from the values in this Data Transfer Object.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
     * @return a GenericValue object constructed from the values in this Data Transfer Object.
     */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator) {
        return ofBizDelegator.makeValue("ChangeItem", new FieldMap()
                .add("id", id)
                .add("group", group)
                .add("fieldtype", fieldtype)
                .add("field", field)
                .add("oldvalue", oldvalue)
                .add("oldstring", oldstring)
                .add("newvalue", newvalue)
                .add("newstring", newstring)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */
    public static ChangeItemDTO fromGenericValue(GenericValue gv) {
        return new ChangeItemDTO(
                gv.getLong("id"),
                gv.getLong("group"),
                gv.getString("fieldtype"),
                gv.getString("field"),
                gv.getString("oldvalue"),
                gv.getString("oldstring"),
                gv.getString("newvalue"),
                gv.getString("newstring")
        );
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(ChangeItemDTO changeItemDTO) {
        return new Builder(changeItemDTO);
    }

    public static class Builder {
        private Long id;
        private Long group;
        private String fieldtype;
        private String field;
        private String oldvalue;
        private String oldstring;
        private String newvalue;
        private String newstring;

        public Builder() {
        }

        public Builder(ChangeItemDTO changeItemDTO) {
            this.id = changeItemDTO.id;
            this.group = changeItemDTO.group;
            this.fieldtype = changeItemDTO.fieldtype;
            this.field = changeItemDTO.field;
            this.oldvalue = changeItemDTO.oldvalue;
            this.oldstring = changeItemDTO.oldstring;
            this.newvalue = changeItemDTO.newvalue;
            this.newstring = changeItemDTO.newstring;
        }

        public ChangeItemDTO build() {
            return new ChangeItemDTO(id, group, fieldtype, field, oldvalue, oldstring, newvalue, newstring);
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }
        public Builder group(Long group) {
            this.group = group;
            return this;
        }
        public Builder fieldtype(String fieldtype) {
            this.fieldtype = fieldtype;
            return this;
        }
        public Builder field(String field) {
            this.field = field;
            return this;
        }
        public Builder oldvalue(String oldvalue) {
            this.oldvalue = oldvalue;
            return this;
        }
        public Builder oldstring(String oldstring) {
            this.oldstring = oldstring;
            return this;
        }
        public Builder newvalue(String newvalue) {
            this.newvalue = newvalue;
            return this;
        }
        public Builder newstring(String newstring) {
            this.newstring = newstring;
            return this;
        }
    }
}