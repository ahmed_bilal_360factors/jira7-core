package com.atlassian.jira.web.action.admin.user;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.runtime.GroupNotFoundException;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.user.CreateUserApplicationHelper;
import com.atlassian.jira.bc.user.UserApplicationHelper.ApplicationSelection;
import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.bc.user.UserService.CreateUserRequest;
import com.atlassian.jira.event.web.action.admin.UserAddedEvent;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.UserDetails;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.JiraUrlCodec;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.ozymandias.SafePluginPointAccess;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.common.net.UrlEscapers;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.emptyMap;
import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

/**
 * Responsible for handling the requests to add a new JIRA User.
 */
@WebSudoRequired
public class AddUser extends JiraWebActionSupport {
    private static final Logger log = LoggerFactory.getLogger(AddUser.class);
    private static final String ADD_USER_COUNT_EXTENSION_WEB_PANEL_LOCATION = "webpanels.admin.adduser.count";
    private static final String ADD_USER_EXTENSION_WEB_PANEL_LOCATION = "webpanels.admin.adduser";

    @VisibleForTesting
    static final String APPLICATION_ACCESS_EXTENSION_WEB_PANEL_LOCATION = "webpanels.admin.adduser.applicationaccess";

    private final UserService userService;
    private final UserManager userManager;
    private final WebInterfaceManager webInterfaceManager;
    private final EventPublisher eventPublisher;
    private final CreateUserApplicationHelper applicationHelper;
    private final ApplicationRoleManager roleManager;

    private String username;
    private String password;
    private String fullname;
    private String email;
    private Long directoryId;
    private boolean sendEmail = false;
    private boolean createAnother = false;
    private UserService.CreateUserValidationResult result;
    private List<String> passwordErrors = ImmutableList.of();
    private String[] selectedApplications;
    private String[] createdUser;

    public AddUser(UserService userService, UserManager userManager, WebInterfaceManager webInterfaceManager,
                   EventPublisher eventPublisher, CreateUserApplicationHelper applicationHelper, final ApplicationRoleManager roleManager) {
        this.userService = userService;
        this.userManager = userManager;
        this.webInterfaceManager = webInterfaceManager;
        this.eventPublisher = eventPublisher;
        this.applicationHelper = applicationHelper;
        this.roleManager = roleManager;
    }

    /**
     * Processes a request to render the input form to fill out the new user's details(username, password, full-name,
     * email ...)
     *
     * @return {@link #INPUT} the input form to fill out the new user's details(username, password, full-name, email
     * ...)
     */
    @Override
    public String doDefault() {
        if (this.selectedApplications == null) {
            this.selectedApplications = roleManager.getDefaultApplicationKeys().stream()
                    .map(ApplicationKey::toString)
                    .toArray(String[]::new);

            final Collection<ApplicationSelection> selectableApplications = getSelectableApplications();
            // select one application if there is only one, unselected and selectable application
            if (selectedApplications.length == 0 && selectableApplications.size() == 1 && selectableApplications.iterator().next().isSelectable()) {
                selectedApplications = selectableApplications.stream()
                        .map(ApplicationSelection::getKey)
                        .toArray(String[]::new);
            }
        }

        return INPUT;
    }

    protected void doValidation() {
        final CreateUserRequest createUserRequest = CreateUserRequest
                .withUserDetails(getLoggedInUser(), getUsername(), getPassword(), getEmail(), getFullname())
                .inDirectory(getDirectoryId())
                .withApplicationAccess(getSelectedApplicationKeys())
                .sendNotification(sendEmail);
        result = userService.validateCreateUser(createUserRequest);
        if (!result.isValid()) {
            addErrorCollection(result.getErrorCollection());
        }
        passwordErrors = result.getPasswordErrors().stream().map(er -> er.getSnippet()).collect(Collectors.toList());
    }

    /**
     * Processes a request to create a user using the specified url parameters.
     *
     * @return if there are input error this will return {@link #ERROR}; otherwise, it will redirect to the View User
     * page for the created user.
     */
    @Override
    @RequiresXsrfCheck
    protected String doExecute() {

        try {
            userService.createUser(result);

            eventPublisher.publish(new UserAddedEvent(request.getParameterMap()));
        } catch (PermissionException e) {
            addError("username", getText("admin.errors.user.no.permission.to.create"));
        } catch (CreateException e) {
            addError("username", getText("admin.errors.user.cannot.create", e.getMessage()));
        } catch (GroupNotFoundException e) {
            final String directoryName = userManager.getDirectory(getDirectoryId()).getName();
            log.warn("Error occurred while adding user to group, the directory {} may be out of sync.", directoryName, e);
            final String warningMessage = escapeHtml4(getText("admin.warn.user.create.no.group",
                    getUsername(), e.getMessage(), directoryName));
            return returnCompleteWithInlineRedirectAndMsg(viewUserUrl(), warningMessage, MessageType.WARNING, true, null);
        }

        if (getHasErrorMessages()) {
            return ERROR;
        } else if (createAnother) {
            return returnCompleteWithInlineRedirect(addUserUrl());
        } else {
            return returnCompleteWithInlineRedirect(userBrowserUrl());
        }
    }

    private String addUserUrl() {
        ImmutableList.Builder<String> builder = ImmutableList.<String>builder().add("createAnother=true");
        if (sendEmail) {
            builder.add("sendEmail=true");
        }
        if (selectedApplications != null && selectedApplications.length > 0) {
            Arrays.stream(selectedApplications).forEach(appKey -> builder.add("application=" + appKey));
        } else {
            builder.add("application=");
        }
        if (getDirectories().size() > 1) {
            builder.add("directoryId=" + directoryId);
        }
        builder.addAll(createdUsersWithCurrentUser());

        return String.format("AddUser!default.jspa?%s", Joiner.on("&").join(builder.build()));
    }

    private String viewUserUrl() {
        return "ViewUser.jspa?name=" + JiraUrlCodec.encode(username.toLowerCase());
    }

    private String userBrowserUrl() {
        return UserBrowser.getActionUrl(
                Optional.of(Joiner.on("&").join(createdUsersWithCurrentUser())),
                Optional.of("userCreatedFlag")
        );
    }

    @VisibleForTesting
    protected List<String> createdUsers() {
        if (createdUser == null) {
            return ImmutableList.of();
        }
        return Arrays.stream(createdUser)
                .map(SAFE_CREATED_USER_PARAM)
                .collect(CollectorsUtil.toImmutableList());
    }

    private List<String> createdUsersWithCurrentUser() {
        return Stream.concat(createdUsers().stream(), Stream.of(SAFE_CREATED_USER_PARAM.apply(getUsername())))
                .collect(CollectorsUtil.toImmutableList());
    }

    private static final Function<String, String> SAFE_CREATED_USER_PARAM = s -> "createdUser=" + UrlEscapers.urlFormParameterEscaper().escape(s);

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = StringUtils.trim(username);
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = StringUtils.trim(email);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if (StringUtils.isEmpty(password)) {
            this.password = null;
        } else {
            this.password = password;
        }
    }

    public List<String> getPasswordErrors() {
        return passwordErrors;
    }

    public boolean hasPasswordWritableDirectory() {
        return userManager.hasPasswordWritableDirectory();
    }

    public boolean isSendEmail() {
        return sendEmail;
    }

    public void setSendEmail(boolean sendEmail) {
        this.sendEmail = sendEmail;
    }

    public boolean isCreateAnother() {
        return createAnother;
    }

    public void setCreateAnother(final boolean createAnother) {
        this.createAnother = createAnother;
    }

    public Long getDirectoryId() {
        return directoryId;
    }

    public void setDirectoryId(Long directoryId) {
        this.directoryId = directoryId;
    }

    public List<Directory> getDirectories() {
        return userManager.getWritableDirectories();
    }

    public String getCancelUrl() {
        return UserBrowser.getActionUrl(
                Optional.of(Joiner.on("&").join(createdUsers())),
                createdUsers().isEmpty() ? Optional.empty() : Optional.of("userCreatedFlag")
        );
    }

    public Map<Long, Boolean> getCanDirectoryUpdatePasswordMap() {
        final List<Directory> directories = getDirectories();
        final Map<Long, Boolean> result = new HashMap<Long, Boolean>(directories.size());
        for (final Directory directory : directories) {
            result.put(directory.getId(), userManager.canDirectoryUpdateUserPassword(directory));
        }
        return result;
    }

    public String getUserCountWebPanelHtml() {
        return getPanels(ADD_USER_COUNT_EXTENSION_WEB_PANEL_LOCATION);
    }

    public String getWebPanelHtml() {
        return getPanels(ADD_USER_EXTENSION_WEB_PANEL_LOCATION);
    }

    public String getApplicationAccessWebPanelHtml() {
        return getPanels(APPLICATION_ACCESS_EXTENSION_WEB_PANEL_LOCATION);
    }

    private String getPanels(final String panelLocation) {
        final StringBuilder builder = new StringBuilder();
        final List<WebPanelModuleDescriptor> panels = webInterfaceManager.getDisplayableWebPanelDescriptors(panelLocation, Collections.<String, Object>emptyMap());
        for (final WebPanelModuleDescriptor panel : panels) {
            final Option<String> fragment = SafePluginPointAccess.call(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    if (panel == null || panel.getModule() == null) {
                        return null;
                    } else {
                        return panel.getModule().getHtml(Collections.<String, Object>emptyMap());
                    }

                }
            });

            if (fragment.isDefined()) {
                builder.append(fragment.get());
            }

        }
        return builder.toString();
    }

    public String[] getSelectedApplications() {
        return selectedApplications;
    }

    public void setSelectedApplications(final String[] selectedApplications) {
        this.selectedApplications = selectedApplications;
    }

    public boolean getHasCreatedUsers() {
        return createdUser != null && createdUser.length > 0;
    }

    public String[] getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(final String[] createdUser) {
        this.createdUser = createdUser;
    }

    @Nonnull
    public Set<ApplicationKey> getSelectedApplicationKeys() {
        final Set<ApplicationKey> applicationKeys = Sets.newHashSet();
        if (selectedApplications != null) {
            for (String selectedApplication : selectedApplications) {
                applicationKeys.add(ApplicationKey.valueOf(selectedApplication));
            }
        }
        return ImmutableSet.copyOf(applicationKeys);
    }

    public boolean hasOnlyOneApplication() {
        int jiraApplicationCount = roleManager.getRoles().size();
        int extraApplicationCount = webInterfaceManager.getDisplayableWebPanelDescriptors(APPLICATION_ACCESS_EXTENSION_WEB_PANEL_LOCATION, emptyMap()).size();
        return jiraApplicationCount + extraApplicationCount == 1;
    }

    @Nonnull
    @SuppressWarnings("unused")  // used in adduser.jsp
    public List<ApplicationSelection> getSelectableApplications() {
        Set<ApplicationKey> selectedKeys = getApplicationKeyObjects(selectedApplications);
        Set<ApplicationKey> applicationKeys = ImmutableSet.<ApplicationKey>builder()
                .addAll(selectedKeys).build();

        // This feature is broken for multiple directories with nested groups
        // User may change directory in create user form UI and if group relationship is different,
        // effective application display won't update.
        final Optional<Long> directoryId = userManager.getDefaultCreateDirectory().map(Directory::getId);

        return applicationHelper.getApplicationsForSelection(applicationKeys, directoryId);
    }

    private Set<ApplicationKey> getApplicationKeyObjects(String[] applicationKeys) {
        if (applicationKeys == null) {
            return ImmutableSet.of();
        } else {
            return Arrays.stream(applicationKeys)
                    .map(ApplicationKey::valueOf)
                    .collect(CollectorsUtil.toImmutableSet());
        }
    }

    private UserDetails toApplicationUserCreationData() {
        return new UserDetails(getUsername(), getFullname())
                .withDirectory(getDirectoryId())
                .withPassword(getPassword())
                .withEmail(getEmail());
    }
}
