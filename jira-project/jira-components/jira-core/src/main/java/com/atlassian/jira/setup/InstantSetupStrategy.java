package com.atlassian.jira.setup;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.exception.CrowdException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.fugue.Iterables;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationConfigurationHelper;
import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.bc.user.UserService.CreateUserRequest;
import com.atlassian.jira.bc.user.UserService.CreateUserValidationResult;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.LocaleManager;
import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.extension.JiraStartedEvent;
import com.atlassian.jira.issue.fields.layout.field.EditableDefaultFieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.renderer.RenderableField;
import com.atlassian.jira.issue.fields.renderer.wiki.AtlassianWikiRenderer;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.startup.DatabaseInitialImporter;
import com.atlassian.jira.upgrade.SetupUpgradeService;
import com.atlassian.jira.upgrade.UpgradeResult;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.action.setup.IndexLanguageToLocaleMapper;
import com.atlassian.johnson.event.Event;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * Default implementation of SetupStrategy. It is intended to be used during Instant Setup's SetupFinishing step.
 *
 * @since v6.4
 */
public class InstantSetupStrategy
        implements SetupStrategy<InstantSetupStrategy.SetupParameters, InstantSetupStrategy.Step> {
    private static final Logger log = Logger.getLogger(InstantSetupStrategy.class);
    private static final String DEFAULT_GROUP_ADMINS = "jira-administrators";

    public static void setupJiraBaseUrl(final String baseUrl) {
        final ApplicationProperties applicationProperties = ComponentAccessor.getComponent(ApplicationProperties.class);
        if (applicationProperties.getString(APKeys.JIRA_BASEURL) == null) {
            applicationProperties.setString(APKeys.JIRA_BASEURL, baseUrl);
        }
    }

    private static UserService.CreateUserValidationResult createAdministrator(final Map<String, String> credentials) {
        final ApplicationUser administrator;

        final String email = credentials.get("email");
        final String username = credentials.get("username");
        final String fullname = credentials.get("email");
        final String password = credentials.get("password");
        final UserService userService = ComponentAccessor.getComponent(UserService.class);


        final CreateUserRequest createUserRequest = CreateUserRequest
                .withUserDetails(null, username, password, email, fullname)
                .confirmPassword(password)
                .passwordRequired()
                .performPermissionCheck(false)
                .sendNotification(false);
        CreateUserValidationResult result = userService.validateCreateUser(createUserRequest);

        if (result.isValid()) {
            try {
                administrator = userService.createUser(result);
            } catch (final PermissionException | CreateException e) {
                throw new RuntimeException(e);
            }

            final GroupManager groupManager = ComponentAccessor.getGroupManager();

            final Group groupAdmins = getOrCreateGroup(groupManager, DEFAULT_GROUP_ADMINS);

            if (administrator != null && groupAdmins != null) {
                try {
                    if (!groupManager.isUserInGroup(administrator, groupAdmins)) {
                        groupManager.addUserToGroup(administrator, groupAdmins);
                    }
                } catch (CrowdException e) {
                    throw new RuntimeException(e);
                }

                // Make sure to enable admin users to change licenses during install (in case the license used is too
                // old for the JIRA version)
                final GlobalPermissionManager globalPermissionManager = ComponentAccessor.getGlobalPermissionManager();
                if (!globalPermissionManager.getGroupNames(Permissions.ADMINISTER).contains(DEFAULT_GROUP_ADMINS)) {
                    globalPermissionManager.addPermission(Permissions.ADMINISTER, DEFAULT_GROUP_ADMINS);
                }

                final ApplicationConfigurationHelper appConfigHelper = ComponentAccessor.getComponent(ApplicationConfigurationHelper.class);
                appConfigHelper.configureApplicationsForSetup(Collections.singleton(groupAdmins), true);
                appConfigHelper.setupAdminForDefaultApplications(administrator);
            }

        }

        return result;
    }

    /**
     * try and get or create a group, if we get a problem let the user know
     *
     * @param groupName the name of the group to get or create
     * @return a Group if one is found or can be created, null otherwise
     */
    private static Group getOrCreateGroup(final GroupManager groupManager, final String groupName) {
        final Group group = groupManager.getGroup(groupName);
        if (group != null) {
            return group;
        }
        try {
            return groupManager.createGroup(groupName);
        } catch (final OperationNotPermittedException | InvalidGroupException e) {
            throw new RuntimeException(e);
        }
    }

    private static void setCorrectLocale(final String locale) {
        setJiraLocale(locale);
        setIndexingLanguageForDefaultServerLocale(locale);
    }

    private static void setJiraLocale(final String locale) {
        // Always get a fresh copy of the application properties because during the short life of some actions
        // (eg. SetupDatabase) the Pico container can be swapped out from underneath us.
        final ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();

        if (isLocaleValid(locale)) {
            applicationProperties.setString(APKeys.JIRA_I18N_DEFAULT_LOCALE, locale);
        }
    }

    private static void setIndexingLanguageForDefaultServerLocale(final String locale) {
        // Always get a fresh copy of the application properties because during the short life of some actions
        // (eg. SetupDatabase) the Pico container can be swapped out from underneath us.
        final ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();
        final IndexLanguageToLocaleMapper languageToLocaleMapper = ComponentAccessor.getComponent(IndexLanguageToLocaleMapper.class);

        applicationProperties.setString(APKeys.JIRA_I18N_LANGUAGE_INPUT, languageToLocaleMapper.getLanguageForLocale(locale));
    }

    private static boolean isLocaleValid(final String locale) {
        final LocaleManager localeManager = ComponentAccessor.getComponentOfType(LocaleManager.class);
        final Set<Locale> installedLocales = localeManager.getInstalledLocales();

        return installedLocales.contains(localeManager.getLocale(locale));
    }

    private static String getText(final String key) {
        return getI18nHelper().getText(key);
    }

    private static String getText(final String key, final String object) {
        return getI18nHelper().getText(key, object);
    }

    private static I18nHelper getI18nHelper() {
        return ComponentAccessor.getComponent(JiraAuthenticationContext.class).getI18nHelper();
    }

    private void doStepTask(final StepSwitcher<Step> switcher, final InstantSetupStrategy.Step step, final StepTask stepTask)
            throws Exception {
        switcher.withStep(step, stepTask);

        final SetupJohnsonUtil johnsonUtil = ComponentAccessor.getComponent(SetupJohnsonUtil.class);
        final Collection johnsonEvents = johnsonUtil.getEvents();

        if (johnsonEvents.size() > 0) {
            throw new RuntimeException(((Event) johnsonEvents.iterator().next()).getDesc());
        }
    }

    @Override
    public void setup(final SetupParameters setupParameters,
                      final StepSwitcher<Step> switcher) throws Exception {
        doStepTask(switcher, Step.DATABASE, this::setupDatabase);
        doStepTask(switcher, Step.PLUGINS, this::startPlugins);
        doStepTask(switcher, Step.ENVIRONMENT, () -> {
            importInitialData(setupParameters.getJiraLicenseKey(), setupParameters.getServerId());
            setupJiraBaseUrl(setupParameters.getBaseUrl());
        });
        doStepTask(switcher, Step.FINISHING, () -> upgradeJiraAndFinishSetup(setupParameters, switcher));
    }

    @Override
    public ImmutableMap<Step, Status> getInitialSteps() {
        return ImmutableMap.of(
                Step.DATABASE, Status.PENDING,
                Step.PLUGINS, Status.AWAITING,
                Step.ENVIRONMENT, Status.AWAITING,
                Step.FINISHING, Status.AWAITING);
    }

    private void setupDatabase() {
        final DatabaseConfigurationManager databaseConfigurationManager = ComponentAccessor.getComponent(DatabaseConfigurationManager.class);
        if (!databaseConfigurationManager.isDatabaseSetup()) {
            final DatabaseConfig hsqlDatabaseConfiguration = databaseConfigurationManager.getInternalDatabaseConfiguration();
            databaseConfigurationManager.setDatabaseConfiguration(hsqlDatabaseConfiguration);
            // now do the post db setup work
            databaseConfigurationManager.activateDatabaseWithoutRunningPostDbSetupRunnables();
        }
    }

    private void startPlugins() {
        final DatabaseConfigurationManager databaseConfigurationManager = ComponentAccessor.getComponent(DatabaseConfigurationManager.class);
        databaseConfigurationManager.runPostDbSetupRunnables();
    }

    private void importInitialData(final String jiraLicenseKey, final String serverId) {
        final DatabaseInitialImporter databaseInitialImporter = ComponentAccessor.getComponent(DatabaseInitialImporter.class);

        // Load initial data with instant evaluation bits injected
        databaseInitialImporter.importInitialData(null, Option.option(serverId), Option.option(jiraLicenseKey));
        // initialize information on instant setup after all data are re-created
        // variables set during setup phase are lost
        final ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();
        applicationProperties.setOption(APKeys.JIRA_SETUP_IS_INSTANT, true);
        applicationProperties.setString(APKeys.JIRA_TITLE, "JIRA");
        applicationProperties.setString(APKeys.JIRA_MODE, "private");
    }

    private void upgradeJiraAndFinishSetup(final SetupParameters setupParameters,
                                           final StepSwitcher switcher) throws Exception {
        final ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();
        applicationProperties.setString(APKeys.JIRA_SETUP, "true");

        // SetupComplete.initialiseSystemPropertiesBeforeSetupUpgradeTasks():
        applicationProperties.setOption(APKeys.JIRA_OPTION_USER_EXTERNALMGT, false);
        applicationProperties.setOption(APKeys.JIRA_OPTION_VOTING, true);
        applicationProperties.setOption(APKeys.JIRA_OPTION_WATCHING, true);
        applicationProperties.setOption(APKeys.JIRA_OPTION_ISSUELINKING, true);
        applicationProperties.setString(APKeys.JIRA_OPTION_EMAIL_VISIBLE, "show");

        final SetupUpgradeService upgradeService = ComponentAccessor.getComponentOfType(SetupUpgradeService.class);
        final UpgradeResult upgradeResult = upgradeService.runUpgrades();

        if (!upgradeResult.successful()) {
            Iterables.first(upgradeResult.getErrors()).foreach(switcher::setError);
            throw new RuntimeException("Upgrade has failed");
        }

        // SetupComplete.initialiseSystemPropertiesAfterSetupUpgradeTasks():
        try {
            ComponentAccessor.getSubTaskManager().enableSubTasks();
        } catch (final CreateException e) {
            log.error("Error encountered when trying to enable sub tasks", e);
            throw new RuntimeException(e);
        }

        //setWikiRendererOnAllRenderableFields();
        final FieldLayoutManager fieldLayoutManager = ComponentAccessor.getFieldLayoutManager();
        final EditableDefaultFieldLayout editableDefaultFieldLayout = fieldLayoutManager.getEditableDefaultFieldLayout();
        final List<FieldLayoutItem> fieldLayoutItems = editableDefaultFieldLayout.getFieldLayoutItems();
        for (final FieldLayoutItem fieldLayoutItem : fieldLayoutItems) {
            if (fieldLayoutItem.getOrderableField() instanceof RenderableField) {
                final RenderableField field = (RenderableField) fieldLayoutItem.getOrderableField();
                if (field.isRenderable()) {
                    editableDefaultFieldLayout.setRendererType(fieldLayoutItem, AtlassianWikiRenderer.RENDERER_TYPE);
                }
            }
        }
        fieldLayoutManager.storeEditableDefaultFieldLayout(editableDefaultFieldLayout);


        // This is here so that al SAL lifeCycleAware components get notified of JIRA
        // being started since this only happens when JIRA is setup.
        ComponentAccessor.getPluginEventManager().broadcast(new JiraStartedEvent());

        setCorrectLocale(setupParameters.getLocale());
        final UserService.CreateUserValidationResult result = createAdministrator(setupParameters.getCredentials());

        if (!result.isValid()) {
            final String errorMessage;

            if (StringUtils.isEmpty(setupParameters.getCredentials().get("email"))) {
                errorMessage = getText("setup.finishing.error.create.administrator.empty.email");
            } else if (StringUtils.isEmpty(setupParameters.getCredentials().get("password"))) {
                errorMessage = getText("setup.finishing.error.create.administrator.empty.password");
            } else {
                errorMessage = getText("setup.finishing.error.create.administrator.generic", setupParameters.getCredentials().get("email"));
            }
            switcher.setError(errorMessage);
            throw new RuntimeException(errorMessage);
        } else {
            applicationProperties.setString(APKeys.JIRA_SETUP_INSTANT_USER, result.getUsername());
        }
    }

    public enum Step {
        DATABASE,
        PLUGINS,
        ENVIRONMENT,
        FINISHING
    }

    public static final class SetupParameters {
        private final String jiraLicenseKey;
        private final String baseUrl;
        private final String serverId;
        private final Map<String, String> credentials;
        private final String locale;

        private SetupParameters(final String jiraLicenseKey, final String baseUrl, final String serverId, final Map<String, String> credentials, final String locale) {
            this.jiraLicenseKey = jiraLicenseKey;
            this.baseUrl = baseUrl;
            this.serverId = serverId;
            this.credentials = credentials;
            this.locale = locale;
        }

        public static SetupParametersBuilder builder() {
            return new SetupParametersBuilder();
        }

        public String getJiraLicenseKey() {
            return jiraLicenseKey;
        }

        public String getBaseUrl() {
            return baseUrl;
        }

        public String getServerId() {
            return serverId;
        }

        public Map<String, String> getCredentials() {
            return credentials;
        }

        public String getLocale() {
            return locale;
        }

        public static class SetupParametersBuilder {
            private String jiraLicenseKey;
            private String baseUrl;
            private String serverId;
            private Map<String, String> credentials;
            private String locale;

            private SetupParametersBuilder() {
            }

            public SetupParametersBuilder setJiraLicenseKey(final String jiraLicenseKey) {
                this.jiraLicenseKey = jiraLicenseKey;
                return this;
            }

            public SetupParametersBuilder setBaseUrl(final String baseUrl) {
                this.baseUrl = baseUrl;
                return this;
            }

            public SetupParametersBuilder setServerId(final String serverId) {
                this.serverId = serverId;
                return this;
            }

            public SetupParametersBuilder setCredentials(final Map<String, String> credentials) {
                this.credentials = credentials;
                return this;
            }

            public SetupParametersBuilder setLocale(final String locale) {
                this.locale = locale;
                return this;
            }

            public SetupParameters build() {
                return new SetupParameters(jiraLicenseKey, baseUrl, serverId, credentials, locale);
            }
        }
    }
}
