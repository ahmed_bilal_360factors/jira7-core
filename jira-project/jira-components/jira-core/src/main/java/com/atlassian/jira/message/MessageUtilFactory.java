package com.atlassian.jira.message;

import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.BaseUrl;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.util.ExternalLinkUtil;

import java.util.Locale;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Factory responsible for creating a {@link MessageUtil}.
 *
 * @since 7.0
 */
public class MessageUtilFactory implements MessageUtil.Factory {
    private final I18nHelper.BeanFactory i1BeanFactory;
    private final HelpUrls helpUrls;
    private final ExternalLinkUtil externalLinkUtil;
    private final JiraAuthenticationContext context;
    private final BaseUrl baseUrl;

    public MessageUtilFactory(final JiraAuthenticationContext context, final I18nHelper.BeanFactory i1BeanFactory,
                              final ExternalLinkUtil externalLinkUtil, final HelpUrls helpUrls, final BaseUrl baseUrl) {
        this.context = notNull("context", context);
        this.i1BeanFactory = notNull("i1BeanFactory", i1BeanFactory);
        this.externalLinkUtil = notNull("externalLinkUtil", externalLinkUtil);
        this.helpUrls = notNull("helpUrls", helpUrls);
        this.baseUrl = notNull("baseUrl", baseUrl);
    }

    @Override
    public MessageUtil getNewInstance() {
        return new MessageUtilImpl(context.getI18nHelper(), helpUrls, externalLinkUtil, baseUrl);
    }

    @Override
    public MessageUtil getNewInstance(final ApplicationUser user) {
        return new MessageUtilImpl(i1BeanFactory.getInstance(user), helpUrls, externalLinkUtil, baseUrl);
    }

    @Override
    public MessageUtil getNewInstance(final Locale locale) {
        return new MessageUtilImpl(i1BeanFactory.getInstance(locale), helpUrls, externalLinkUtil, baseUrl);
    }

}