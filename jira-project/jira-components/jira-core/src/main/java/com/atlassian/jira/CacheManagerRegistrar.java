package com.atlassian.jira;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettings;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.cache.CacheSettingsDefaultsProvider;
import com.atlassian.cache.ehcache.EhCacheManager;
import com.atlassian.cache.impl.jmx.MBeanRegistrar;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.fugue.Pair;
import com.atlassian.jira.cache.CacheCompactor;
import com.atlassian.jira.cache.EhCacheCompactor;
import com.atlassian.jira.cache.JiraVCacheRequestContextSupplier;
import com.atlassian.jira.cache.JiraVCacheServiceCreator;
import com.atlassian.jira.cache.NullCacheCompactor;
import com.atlassian.jira.cache.serialcheck.DefaultSerializationChecker;
import com.atlassian.jira.cache.serialcheck.SerializationCheckedCacheManager;
import com.atlassian.jira.cache.serialcheck.SerializationChecker;
import com.atlassian.jira.cache.slomo.SloMoCacheManager;
import com.atlassian.jira.cluster.ClusterNodeProperties;
import com.atlassian.jira.cluster.EhCacheConfigurationFactory;
import com.atlassian.jira.cluster.ReplicatorExecutorServiceFactory;
import com.atlassian.jira.cluster.cache.ehcache.ReplicatorConfigFactory;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.vcache.VCacheFactory;
import com.atlassian.vcache.internal.VCacheLifecycleManager;
import com.atlassian.vcache.internal.VCacheManagement;
import com.atlassian.vcache.internal.core.service.AbstractVCacheService;
import com.google.common.annotations.VisibleForTesting;
import net.sf.ehcache.config.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.management.MBeanServer;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

import static com.atlassian.jira.ComponentContainer.Scope.INTERNAL;
import static com.atlassian.jira.ComponentContainer.Scope.PROVIDED;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Registers the {@link CacheManager} with the Pico container.
 *
 * @since 6.2
 */
class CacheManagerRegistrar {
    /**
     * The name of the system property which if set provides the URL of the
     * Ehcache XML configuration file (overriding the built-in ehcache.xml
     * file). If this URL begins with "/", it will be resolved relative to the
     * classpath.
     */
    public static final String EHCACHE_CONFIGURATION = "atlassian.ehcache.config";

    /**
     * The name of the system property which if set to "true" enables JMX
     * monitoring of atlassian-cache (which MBeans are available depends upon
     * the atlassian-cache implementation).
     */
    public static final String ENABLE_JMX = "atlassian.cache.jmx";

    /**
     * The name of the system property which if set to "true" forces the use of
     * Ehcache.
     */
    public static final String FORCE_EHCACHE = "atlassian.cache.ehcache";

    /**
     * The name of the system property which if set to "true" enables the use of statistics by default.
     */
    public static final String ENABLE_STATISTICS = "atlassian.cache.statistics.enabled";

    /**
     * Decorates every single cache such that every operation (whether get or remove) takes
     * at least the specified number of milliseconds.  Non-positive values disable this.
     * Values larger than a few milliseconds would probably make the cache slower than just
     * going to the database, so there probably isn't much point in that.  Consider using
     * something in the neighborhood of {@code 5} to {@code 10}.
     */
    public static final String ENABLE_SLOMO = "atlassian.cache.slomo.millis";

    /**
     * If set to true, every value stored in the cache will be checked for serializability.
     * This incurs a performance penalty.
     *
     * @since 7.2.0
     */
    public static final String ENABLE_SERIALIZATION_CHECK = "atlassian.cache.serialization.check";

    // The scope under which the CacheManager is registered
    @VisibleForTesting
    static final ComponentContainer.Scope MANAGER_SCOPE = PROVIDED;
    static final ComponentContainer.Scope COMPACTOR_SCOPE = INTERNAL;

    // The key under which the CacheManager is registered
    @VisibleForTesting
    static final Class<CacheManager> MANAGER_KEY = CacheManager.class;
    static final Class<CacheCompactor> COMPACTOR_KEY = CacheCompactor.class;

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheManagerRegistrar.class);

    /**
     * Registers the CacheManager with the given container.
     *
     * @param container   the container to receive the CacheManager (required)
     * @param mBeanServer the JMX server with which to register any MBeans (ignored if null)
     */
    public static void registerCacheManager(final ComponentContainer container, final MBeanServer mBeanServer) {
        final JiraProperties jiraProperties = JiraSystemProperties.getInstance();
        final ClusterNodeProperties clusterNodeProperties = container.getComponentInstance(ClusterNodeProperties.class);
        final boolean useEhcache = useEhcache(jiraProperties, clusterNodeProperties);
        //set only to onlu for CI needs
        LOGGER.debug("Using Ehcache = {}", useEhcache);
        final CacheManager cacheManager;
        final CacheCompactor cacheCompactor;

        final CacheSettingsDefaultsProvider defaultSettingsProvider = new JiraSettingsDefaultsProvider(jiraProperties);

        if (useEhcache) {
            ReplicatorExecutorServiceFactory.init();
            net.sf.ehcache.CacheManager delegate = getCacheManagerDelegate(container, jiraProperties, clusterNodeProperties);
            final ReplicatorConfigFactory replicatorConfigFactory = new ReplicatorConfigFactory();
            cacheManager = new EhCacheManager(delegate, replicatorConfigFactory, defaultSettingsProvider);
            cacheCompactor = new EhCacheCompactor(delegate);
        } else {
            cacheManager = new MemoryCacheManager(defaultSettingsProvider);
            cacheCompactor = new NullCacheCompactor();
        }

        registerVCache(container, jiraProperties, cacheManager, useEhcache);

        enableJmxIfNecessary(jiraProperties, cacheManager, mBeanServer);

        container.implementation(PROVIDED, SerializationChecker.class, DefaultSerializationChecker.class);

        container.instance(MANAGER_SCOPE, MANAGER_KEY, wrapForSerializationCheck(wrapForSloMo(cacheManager), container));
        container.instance(COMPACTOR_SCOPE, COMPACTOR_KEY, cacheCompactor);
    }

    private static void registerVCache(final ComponentContainer container, final JiraProperties jiraProperties,
                                       final CacheManager cacheManager, boolean avoidCas) {

        JiraVCacheServiceCreator vCacheFactoryCreator = new JiraVCacheServiceCreator(jiraProperties,
                Optional.ofNullable(cacheManager), avoidCas);
        final Pair<JiraVCacheRequestContextSupplier, AbstractVCacheService> pair = vCacheFactoryCreator
                .createVCacheService();
        container.instance(INTERNAL, JiraVCacheRequestContextSupplier.class, pair.left());

        final AbstractVCacheService vCacheService = pair.right();
        container.instance(INTERNAL, VCacheManagement.class, vCacheService);
        container.instance(INTERNAL, VCacheLifecycleManager.class, vCacheService);
        container.instance(PROVIDED, VCacheFactory.class, vCacheService);
    }

    public static void shutDownCacheManager(final ComponentContainer container, final MBeanServer mBeanServer) {
        ReplicatorExecutorServiceFactory.shutdown();

        final JiraProperties jiraProperties = JiraSystemProperties.getInstance();
        final CacheManager cacheManager = container.getComponentInstance(MANAGER_KEY);

        if (jiraProperties.getBoolean(ENABLE_JMX) && cacheManager instanceof MBeanRegistrar) {
            ((MBeanRegistrar) cacheManager).unregisterMBeans(mBeanServer);
        }

        if (cacheManager != null) {
            cacheManager.shutdown();
        }
    }

    private static CacheManager wrapForSloMo(CacheManager delegate) {
        final int delay = JiraSystemProperties.getInstance().getInteger(ENABLE_SLOMO, 0);
        return (delay > 0) ? new SloMoCacheManager(delegate, delay) : delegate;
    }

    private static CacheManager wrapForSerializationCheck(CacheManager delegate, ComponentContainer container) {

        if (JiraSystemProperties.getInstance().isDevMode() || Boolean.TRUE.equals(JiraSystemProperties.getInstance().getBoolean(ENABLE_SERIALIZATION_CHECK))) {
            SerializationChecker checker = container.getComponentInstance(SerializationChecker.class);
            return new SerializationCheckedCacheManager(delegate, checker);
        } else {
            return delegate;
        }
    }

    private static net.sf.ehcache.CacheManager getCacheManagerDelegate(final ComponentContainer container, final JiraProperties jiraProperties,
                                                                       final ClusterNodeProperties clusterNodeProperties) {
        URL configUrl = CacheManagerRegistrar.class.getResource("/ehcache.xml");
        final String customEhcacheConfig = jiraProperties.getProperty(EHCACHE_CONFIGURATION);
        if (isNotBlank(customEhcacheConfig)) {
            // The user has specified a custom Ehcache configuration; apply it
            final File customEhcacheConfigFile = new File(customEhcacheConfig);
            if (customEhcacheConfigFile.isFile()) {
                try {
                    configUrl = customEhcacheConfigFile.toURI().toURL();
                } catch (final MalformedURLException e) {
                    throw new IllegalStateException("Could not create a URL from " + customEhcacheConfigFile);
                }
            } else {
                LOGGER.error(customEhcacheConfigFile + " is not a file; defaulting to JIRA's built-in Ehcache configuration");
            }
        }

        final Configuration config = buildConfiguration(container, configUrl, clusterNodeProperties);
        return NoopCacheEventListenerFactory.workAroundCache95(net.sf.ehcache.CacheManager.newInstance(config));
    }

    private static Configuration buildConfiguration(final ComponentContainer container, final URL configUrl, final ClusterNodeProperties clusterNodeProperties) {
        final EhCacheConfigurationFactory configurationFactory = container.getComponentInstance(EhCacheConfigurationFactory.class);

        return configurationFactory.newConfiguration(configUrl, clusterNodeProperties);
    }

    private static void enableJmxIfNecessary(
            final JiraProperties jiraProperties, final CacheManager cacheManager, final MBeanServer mBeanServer) {
        if (jiraProperties.getBoolean(ENABLE_JMX)) {
            if (cacheManager instanceof MBeanRegistrar) {
                ((MBeanRegistrar) cacheManager).registerMBeans(mBeanServer);
            }
        }
    }

    private static boolean useEhcache(final JiraProperties jiraProperties, final ClusterNodeProperties clusterNodeProperties) {
        final boolean isEhCacheForced = jiraProperties.getBoolean(FORCE_EHCACHE);
        // Ideally we would invoke ClusterManager#isClustered(), however this causes a circular dependency
        final boolean isJiraClustered = clusterNodeProperties.getNodeId() != null;
        return isEhCacheForced || isJiraClustered;
    }

    private static boolean enableStatistics(final JiraProperties jiraProperties) {
        return jiraProperties.getBoolean(ENABLE_STATISTICS);
    }

    private static class JiraSettingsDefaultsProvider implements CacheSettingsDefaultsProvider {
        final boolean enableStatistics;

        private JiraSettingsDefaultsProvider(final JiraProperties jiraProperties) {
            this.enableStatistics = enableStatistics(jiraProperties);
        }

        @Nonnull
        @Override
        public CacheSettings getDefaults(@Nonnull final String cacheName) {
            CacheSettingsBuilder cacheSettingsBuilder = new CacheSettingsBuilder();
            if (enableStatistics) {
                return cacheSettingsBuilder.statisticsEnabled().build();
            } else {
                return cacheSettingsBuilder.statisticsDisabled().build();
            }
        }
    }
}
