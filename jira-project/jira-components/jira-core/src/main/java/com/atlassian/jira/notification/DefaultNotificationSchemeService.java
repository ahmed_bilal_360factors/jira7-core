package com.atlassian.jira.notification;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.fugue.Options;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.event.type.EventTypeManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.notification.type.NotificationType;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.Page;
import com.atlassian.jira.util.PageRequest;
import com.atlassian.jira.util.Pages;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimaps;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Predicate;

import static com.atlassian.jira.util.PageRequests.limit;
import static java.util.Collections.emptyList;

/**
 * @since 6.5
 */
public class DefaultNotificationSchemeService implements NotificationSchemeService {
    public static final int MAX_PAGE_RESULT = 50;

    private final NotificationSchemeManager notificationSchemeManager;
    private final EventTypeManager eventTypeManager;
    private final UserManager userManager;
    private final GroupManager groupManager;
    private final ProjectRoleManager projectRoleManager;
    private final CustomFieldManager customFieldManager;
    private final PermissionManager permissionManager;
    private final GlobalPermissionManager globalPermissionManager;
    private final I18nHelper i18n;
    private final ProjectManager projectManager;

    public DefaultNotificationSchemeService(final NotificationSchemeManager notificationSchemeManager,
                                            final EventTypeManager eventTypeManager,
                                            final UserManager userManager,
                                            final GroupManager groupManager,
                                            final ProjectRoleManager projectRoleManager,
                                            final CustomFieldManager customFieldManager,
                                            final PermissionManager permissionManager,
                                            final GlobalPermissionManager globalPermissionManager,
                                            final I18nHelper i18n,
                                            final ProjectManager projectManager) {
        this.notificationSchemeManager = notificationSchemeManager;
        this.eventTypeManager = eventTypeManager;
        this.userManager = userManager;
        this.groupManager = groupManager;
        this.projectRoleManager = projectRoleManager;
        this.customFieldManager = customFieldManager;
        this.permissionManager = permissionManager;
        this.globalPermissionManager = globalPermissionManager;
        this.i18n = i18n;
        this.projectManager = projectManager;
    }

    @Override
    public ServiceOutcome<NotificationScheme> getNotificationScheme(final ApplicationUser user, @Nonnull final Long id) {
        return withUserNotNullCheck(user, id, (applicationUser, schemeId) -> {
            final Scheme scheme = notificationSchemeManager.getSchemeObject(schemeId);
            if (scheme == null) {
                return notificationSchemeDoesntExist(schemeId);
            }

            if (!hasPermissions(applicationUser, scheme)) {
                return notificationSchemeDoesntExist(schemeId);
            }

            return ServiceOutcomeImpl.ok(transformToNotificationScheme(scheme));
        });
    }

    @Override
    public Page<NotificationScheme> getNotificationSchemes(final ApplicationUser user, @Nonnull final PageRequest pageRequest) {
        if (user == null) {
            return Pages.toPage(emptyList(), pageRequest);
        }
        Predicate<Scheme> hasSchemePermissions = scheme -> hasPermissions(user, scheme);

        final List<Scheme> schemeObjects = notificationSchemeManager.getSchemeObjects();

        return Pages.toPage(schemeObjects, limit(pageRequest, MAX_PAGE_RESULT), hasSchemePermissions, this::transformToNotificationScheme);
    }

    @Override
    public ServiceOutcome<NotificationScheme> getNotificationSchemeForProject(final ApplicationUser user, @Nonnull final Long projectId) {
        return withUserNotNullCheck(user, projectId, (applicationUser, id) -> {
            Project project = projectManager.getProjectObj(id);
            return getNotificationSchemeForProject(applicationUser, project);
        });
    }

    @Override
    public ServiceOutcome<NotificationScheme> getNotificationSchemeForProject(final ApplicationUser user, @Nonnull final String projectKey) {
        return withUserNotNullCheck(user, projectKey, (applicationUser, key) -> {
            Project project = projectManager.getProjectObjByKey(key);
            return getNotificationSchemeForProject(applicationUser, project);
        });
    }

    private <T> ServiceOutcome<NotificationScheme> withUserNotNullCheck(final ApplicationUser user,
                                                                        final T id,
                                                                        final BiFunction<ApplicationUser, T, ServiceOutcome<NotificationScheme>> function) {
        if (user != null) {
            return function.apply(user, id);
        } else {
            return ServiceOutcomeImpl.error(i18n.getText("createissue.not.logged.in"), ErrorCollection.Reason.NOT_LOGGED_IN);
        }
    }

    private ServiceOutcome<NotificationScheme> getNotificationSchemeForProject(final ApplicationUser user, final Project project) {
        if (project == null) {
            return ServiceOutcomeImpl.error(i18n.getText("common.share.project.does.not.exist"), ErrorCollection.Reason.NOT_FOUND);
        }
        if (!(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)
                || permissionManager.hasPermission(ProjectPermissions.ADMINISTER_PROJECTS, project, user))) {
            return ServiceOutcomeImpl.error(i18n.getText(i18n.getText("common.share.project.does.not.exist"), ErrorCollection.Reason.FORBIDDEN));
        }

        final Scheme scheme = notificationSchemeManager.getSchemeFor(project);
        if (scheme != null) {
            return ServiceOutcomeImpl.ok(transformToNotificationScheme(scheme));
        } else {
            return ServiceOutcomeImpl.error(i18n.getText("admin.notifications.scheme.for.project.does.not.exist"), ErrorCollection.Reason.NOT_FOUND);
        }
    }

    private NotificationScheme transformToNotificationScheme(final Scheme scheme) {
        final ImmutableListMultimap<EventType, SchemeEntity> notificationEntitiesByEventType = Multimaps.index(scheme.getEntities(), new Function<SchemeEntity, EventType>() {
            @Override
            public EventType apply(final SchemeEntity schemeEntity) {
                return eventTypeManager.getEventType((Long) schemeEntity.getEntityTypeId());
            }
        });

        final ListMultimap<EventType, Option<Notification>> notificationsByEventType = Multimaps.transformEntries(notificationEntitiesByEventType, new Maps.EntryTransformer<EventType, SchemeEntity, Option<Notification>>() {
            @Override
            public Option<Notification> transformEntry(final EventType key, final SchemeEntity schemeEntity) {
                return createNotificationForSchemeEntity(schemeEntity);
            }
        });

        Iterable<EventNotifications> notificationEvents = Iterables.transform(notificationsByEventType.asMap().entrySet(), new Function<Map.Entry<EventType, Collection<Option<Notification>>>, EventNotifications>() {
            @Override
            public EventNotifications apply(final Map.Entry<EventType, Collection<Option<Notification>>> input) {
                final Iterable<Notification> notifications = ImmutableList.copyOf(Options.flatten(input.getValue()));
                return new EventNotifications(input.getKey(), notifications);
            }
        });

        return new NotificationScheme(scheme.getId(), scheme.getName(), scheme.getDescription(), notificationEvents);
    }

    private ServiceOutcome<NotificationScheme> notificationSchemeDoesntExist(final Long id) {
        return ServiceOutcomeImpl.error(i18n.getText("admin.notifications.scheme.does.not.exist", id), ErrorCollection.Reason.NOT_FOUND);
    }

    private boolean hasPermissions(final ApplicationUser user, final Scheme scheme) {
        return globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)
                || notificationSchemeManager.getProjects(scheme)
                .stream()
                .anyMatch(project ->
                        permissionManager.hasPermission(ProjectPermissions.ADMINISTER_PROJECTS, project, user));
    }

    private Option<Notification> createNotificationForSchemeEntity(final SchemeEntity schemeEntity) {
        final NotificationType notificationType = NotificationType.from(schemeEntity.getType());
        final Long schemeEntityId = schemeEntity.getId();
        final String parameter = schemeEntity.getParameter();
        switch (notificationType) {
            case CURRENT_ASSIGNEE:
            case REPORTER:
            case CURRENT_USER:
            case PROJECT_LEAD:
            case COMPONENT_LEAD:
            case ALL_WATCHERS:
                return createRoleBasedNotification(notificationType, schemeEntityId);
            case SINGLE_USER:
                return createUserNotification(notificationType, schemeEntityId, parameter);
            case GROUP:
                return createGroupNotification(notificationType, schemeEntityId, parameter);
            case PROJECT_ROLE:
                return createProjectRoleNotification(notificationType, schemeEntityId, parameter);
            case USER_CUSTOM_FIELD_VALUE:
            case GROUP_CUSTOM_FIELD_VALUE:
                return createCustomFieldNotification(notificationType, schemeEntityId, parameter);
            case SINGLE_EMAIL_ADDRESS:
                return createSingleEmailAddressNotification(schemeEntityId, parameter);
            default:
                return Option.none();
        }
    }

    private Option<Notification> createSingleEmailAddressNotification(final Long schemeEntityId, final String parameter) {
        return Option.<Notification>some(new EmailNotification(schemeEntityId, parameter));
    }

    private Option<Notification> createRoleBasedNotification(final NotificationType notificationType, final Long schemeEntityId) {
        return Option.<Notification>some(new RoleNotification(schemeEntityId, notificationType));
    }

    private Option<Notification> createCustomFieldNotification(final NotificationType notificationType,
                                                               final Long schemeEntityId,
                                                               final String customFieldId) {
        final CustomField customField = customFieldManager.getCustomFieldObject(customFieldId);
        return Option.<Notification>some(new CustomFieldValueNotification(schemeEntityId, notificationType, customField, customFieldId));
    }

    private Option<Notification> createProjectRoleNotification(final NotificationType notificationType,
                                                               final Long schemeEntityId,
                                                               final String projectRoleId) {
        final ProjectRole projectRole = projectRoleManager.getProjectRole(Long.parseLong(projectRoleId));
        return Option.<Notification>some(new ProjectRoleNotification(schemeEntityId, notificationType, projectRole, projectRoleId));
    }

    private Option<Notification> createGroupNotification(final NotificationType notificationType,
                                                         final Long schemeEntityId,
                                                         final String groupName) {
        final Group group = groupManager.getGroup(groupName);
        return Option.<Notification>some(new GroupNotification(schemeEntityId, notificationType, group, groupName));
    }

    private Option<Notification> createUserNotification(final NotificationType notificationType,
                                                        final Long schemeEntityId,
                                                        final String userKey) {
        final ApplicationUser userToNotify = userManager.getUserByKey(userKey);
        return Option.<Notification>some(new UserNotification(schemeEntityId, notificationType, userToNotify, userKey));
    }
}
