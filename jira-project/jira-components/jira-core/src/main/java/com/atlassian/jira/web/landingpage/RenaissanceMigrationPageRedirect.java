package com.atlassian.jira.web.landingpage;

import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.bc.dataimport.ImportCompletedEvent;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.license.RenaissanceMigrationStatus;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.ApplicationUser;

import java.util.Optional;

/**
 * Redirects admin to the Renaissance migration summary page. Only the first admin to log in is redirected.
 *
 * @since v7.0
 */
@EventComponent
public class RenaissanceMigrationPageRedirect implements PageRedirect, Startable {
    public static final String PROPERTY_POST_MIGRATION_PAGE_DISPLAYED = "post.migration.page.displayed";
    private static final String URL = "/secure/MigrationSummary.jspa";
    private static final int REDIRECT_PRIORITY = 1;

    private final ApplicationProperties properties;
    private final LandingPageRedirectManager landingPageRedirectManager;
    private final GlobalPermissionManager globalPermissionManager;
    private final RenaissanceMigrationStatus renaissanceMigrationStatus;

    public RenaissanceMigrationPageRedirect(final ApplicationProperties properties,
                                            final LandingPageRedirectManager landingPageRedirectManager,
                                            final GlobalPermissionManager globalPermissionManager,
                                            final RenaissanceMigrationStatus renaissanceMigrationStatus) {
        this.properties = properties;
        this.landingPageRedirectManager = landingPageRedirectManager;
        this.globalPermissionManager = globalPermissionManager;
        this.renaissanceMigrationStatus = renaissanceMigrationStatus;
    }


    @Override
    public Optional<String> url(final ApplicationUser user) {
        return shouldDisplaySummary() && user != null
                && globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)
                ? Optional.of(URL) : Optional.empty();
    }

    private boolean shouldDisplaySummary() {
        return renaissanceMigrationStatus.hasMigrationRun()
                && !wasPageDisplayed();
    }

    private boolean wasPageDisplayed() {
        //We do not ever show this in Cloud so treat it as already displayed
        return properties.getOption(PROPERTY_POST_MIGRATION_PAGE_DISPLAYED);
    }


    @Override
    public void start() {
        //start is called BEFORE migration, so we do need to register this redirect even though hasMigrationRun is false
        if (!wasPageDisplayed()) {
            landingPageRedirectManager.registerRedirect(this, REDIRECT_PRIORITY);
        }
    }

    @EventListener
    public void onImportCompletedEvent(ImportCompletedEvent event) {
        start();
    }

    public void unregisterSelf() {
        landingPageRedirectManager.unregisterRedirect(this);
    }
}
