package com.atlassian.jira.issue.search.providers;

import com.atlassian.jira.util.RuntimeIOException;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;

import java.io.IOException;

class DocumentProvider {
    private final IndexSearcher issueSearcher;

    DocumentProvider(IndexSearcher issueSearcher) {
        this.issueSearcher = issueSearcher;
    }

    Document fetchFromIndex(int docId) {
        try {
            return this.issueSearcher.doc(docId);
        } catch (IOException e) {
            throw new RuntimeIOException(e);
        }
    }
}
