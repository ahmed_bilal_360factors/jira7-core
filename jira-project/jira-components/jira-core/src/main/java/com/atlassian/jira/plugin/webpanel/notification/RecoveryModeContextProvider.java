package com.atlassian.jira.plugin.webpanel.notification;

import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.RecoveryMode;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

public final class RecoveryModeContextProvider implements ContextProvider {
    private final RecoveryMode recoveryMode;
    private final HelpUrls helpUrls;
    private final JiraAuthenticationContext authContext;
    private final GlobalPermissionManager permissionManager;

    public RecoveryModeContextProvider(final RecoveryMode recoveryMode, final HelpUrls urls,
                                       final JiraAuthenticationContext authContext, GlobalPermissionManager permissionManager) {
        this.permissionManager = notNull("permissionManager", permissionManager);
        this.recoveryMode = notNull("recoveryMode", recoveryMode);
        this.helpUrls = notNull("helpUrls", urls);
        this.authContext = notNull("authContext", authContext);
    }

    @Override
    public void init(final Map<String, String> params) {
    }

    @Override
    public Map<String, Object> getContextMap(final Map<String, Object> context) {
        final ApplicationUser user = authContext.getUser();
        return ImmutableMap.of("helpUrl", helpUrls.getUrl("recovery-mode").getUrl(),
                "isRecoveryAdmin", recoveryMode.isRecoveryUser(user),
                "isAdmin", permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user));
    }
}
