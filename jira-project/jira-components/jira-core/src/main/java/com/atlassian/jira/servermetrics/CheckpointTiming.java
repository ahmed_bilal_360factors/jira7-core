package com.atlassian.jira.servermetrics;


import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Duration;

@ParametersAreNonnullByDefault
public class CheckpointTiming {
    public final String checkpointName;
    public final Duration checkpointTime;

    public CheckpointTiming(String checkpointName, Duration checkpointTime) {
        this.checkpointName = checkpointName;
        this.checkpointTime = checkpointTime;
    }

    @Nonnull
    public String getCheckpointName() {
        return checkpointName;
    }

    @Nonnull
    public Duration getCheckpointTime() {
        return checkpointTime;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("checkpointName", checkpointName)
                .add("checkpointTime", checkpointTime)
                .toString();
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CheckpointTiming that = (CheckpointTiming) o;
        return Objects.equal(checkpointName, that.checkpointName) &&
                Objects.equal(checkpointTime, that.checkpointTime);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(checkpointName, checkpointTime);
    }
}
