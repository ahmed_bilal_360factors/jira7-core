package com.atlassian.jira.bc.user.search;

import com.atlassian.crowd.embedded.api.ApplicationFactory;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Directories;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.Property;
import com.atlassian.crowd.search.query.entity.restriction.TermRestriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.crowd.embedded.EventuallyConsistentQuery;
import com.atlassian.jira.issue.comparator.UserCachingComparator;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.UserFilter;
import com.atlassian.jira.user.util.UserKeyStore;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.StopWatch;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import static com.atlassian.crowd.search.query.entity.EntityQuery.MAX_MAX_RESULTS;
import static com.atlassian.jira.bc.user.search.UserSearchUtilities.userSearchTermRestrictions;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.lowerCase;
import static org.apache.commons.lang.StringUtils.trimToEmpty;

public class DefaultUserPickerSearchService implements UserPickerSearchService, UserSearchService {
    private static final Logger log = LoggerFactory.getLogger(DefaultUserPickerSearchService.class);

    private final UserManager userManager;
    private final ApplicationProperties applicationProperties;
    private final JiraAuthenticationContext authenticationContext;
    private final PermissionManager permissionManager;
    private final GroupManager groupManager;
    private final ProjectManager projectManager;
    private final ProjectRoleManager projectRoleManager;

    private final CrowdService crowdService;
    private final DirectoryManager directoryManager;
    private final ApplicationFactory applicationFactory;
    private final UserKeyStore userKeyStore;

    public DefaultUserPickerSearchService(final UserManager userManager, final ApplicationProperties applicationProperties,
                                          final JiraAuthenticationContext authenticationContext, final PermissionManager permissionManager, final GroupManager groupManager, final ProjectManager projectManager,
                                          final ProjectRoleManager projectRoleManager, final CrowdService crowdService, final DirectoryManager directoryManager,
                                          final ApplicationFactory applicationFactory, final UserKeyStore userKeyStore) {
        this.userManager = userManager;
        this.applicationProperties = applicationProperties;
        this.authenticationContext = authenticationContext;
        this.permissionManager = permissionManager;
        this.groupManager = groupManager;
        this.projectManager = projectManager;
        this.projectRoleManager = projectRoleManager;
        this.crowdService = crowdService;
        this.directoryManager = directoryManager;
        this.applicationFactory = applicationFactory;
        this.userKeyStore = userKeyStore;
    }

    @Override
    public List<ApplicationUser> findUsers(final JiraServiceContext jiraServiceContext, final String query) {
        if (isBlank(query)) {
            return emptyList();
        }

        return findUsers(jiraServiceContext, query, UserSearchParams.ACTIVE_USERS_IGNORE_EMPTY_QUERY);
    }

    @Override
    public ApplicationUser getUserByName(JiraServiceContext jiraServiceContext, String query) {
        return userManager.getUserByName(query);
    }

    @Override
    public List<ApplicationUser> findUsersAllowEmptyQuery(final JiraServiceContext jiraServiceContext, final String query) {
        return findUsers(jiraServiceContext, query, UserSearchParams.ACTIVE_USERS_ALLOW_EMPTY_QUERY);
    }

    @Override
    public List<ApplicationUser> findUsers(JiraServiceContext jiraServiceContext, String query, UserSearchParams userSearchParams) {
        //If browser users is not allowed, then return empty list
        if (userSearchParams.ignorePermissionCheck() || canPerformAjaxSearch(jiraServiceContext)) {
            return findUsers(query, UserSearchParams.builder(userSearchParams)
                    .canMatchEmail(canShowEmailAddresses(jiraServiceContext)).build());
        }

        return emptyList();
    }

    @Override
    public List<ApplicationUser> findUsers(final String query, final UserSearchParams userSearchParams) {
        return findUsers(query, null, userSearchParams);
    }

    /**
     * Attempt to use crowd query API to search for users.  Returns null if such a search is not possible (maybe because of
     * incompatible search parameters).
     *
     * @throws SearchNotSupportedWithCrowdException if crowd does not support the search, in which case the search needs to
     *                                              be performed manually with Java-side filtering.
     */
    @Nonnull
    private <T, C extends Comparable<C>> Iterable<T> attemptFindUsersWithCrowd(final String nameQuery, final String emailQuery,
                                                                               final UserSearchParams userSearchParams,
                                                                               final UserQueryResultType<T, C> resultType)
            throws SearchNotSupportedWithCrowdException {
        //A stupid query but we should handle it properly - always zero results
        if (!userSearchParams.includeActive() && !userSearchParams.includeInactive()) {
            return emptyList();
        }

        //Does not support user filter - this is a completely different query
        if (userSearchParams.getUserFilter() != null && userSearchParams.getUserFilter().isEnabled()) {
            throw new SearchNotSupportedWithCrowdException();
        }

        //Post-proc filters only work with User results, so if we have a post-proc filter and non-User result type then
        //do the actual search with User results and convert all results afterwards
        //Not much we can do in this case
        if (resultType.getCrowdResultType() != User.class && userSearchParams.getPostProcessingFilter() != Predicates.<User>alwaysTrue()) {
            Iterable<ApplicationUser> results = attemptFindUsersWithCrowd(nameQuery, emailQuery, userSearchParams, new ApplicationUserResultType());
            return resultType.convertApplicationUserResults(results);
        }

        //At this point, we know that either postProcFilter is Predicates.alwaysTrue() or C is User
        //Either way this raw cast is safe
        @SuppressWarnings("unchecked") Predicate<C> postProcFilter = (Predicate) userSearchParams.getPostProcessingFilter();

        SearchRestriction with = null;

        if (!isNullOrEmpty(nameQuery)) {
            final List<Property<String>> searchFields = userSearchParams.canMatchEmail()
                    ? ImmutableList.of(UserTermKeys.USERNAME, UserTermKeys.DISPLAY_NAME, UserTermKeys.EMAIL)
                    : ImmutableList.of(UserTermKeys.USERNAME, UserTermKeys.DISPLAY_NAME);

            final Collection<SearchRestriction> searchRestrictions = userSearchTermRestrictions(nameQuery, searchFields);
            with = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, searchRestrictions);
        }

        if (!isNullOrEmpty(emailQuery)) {
            final Collection<SearchRestriction> searchRestrictions = userSearchTermRestrictions(emailQuery, ImmutableList.of(UserTermKeys.EMAIL));
            final BooleanRestrictionImpl emailRestriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, searchRestrictions);

            if (with != null) {
                with = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, with, emailRestriction);
            } else {
                with = emailRestriction;
            }
        }

        //Do we need to filter by active / inactive?
        if (!userSearchParams.includeInactive()) {
            SearchRestriction activeFilter = new TermRestriction<>(UserTermKeys.ACTIVE, true);
            if (with != null) {
                with = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, with, activeFilter);
            } else {
                with = activeFilter;
            }
        }
        if (!userSearchParams.includeActive()) {
            SearchRestriction activeFilter = new TermRestriction<>(UserTermKeys.ACTIVE, false);
            if (with != null) {
                with = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, with, activeFilter);
            } else {
                with = activeFilter;
            }
        }

        if (with == null) {
            with = NullRestrictionImpl.INSTANCE;
        }

        IteratedSearcher searcher = new IteratedSearcher();

        List<C> results = searcher.iteratedSearch(
                new CrowdSearchSource<>(
                        QueryBuilder.queryFor(resultType.getCrowdResultType(), EntityDescriptor.user()).with(with)
                ),
                postProcFilter,
                normalizeRidiculousUserPickerLimits(userSearchParams.getMaxResults())
        );
        return resultType.convertCrowdResults(results);
    }

    private int normalizeRidiculousUserPickerLimits(Integer resultLimit) {
        if (resultLimit == null || resultLimit == EntityQuery.ALL_RESULTS) {
            return MAX_MAX_RESULTS;
        } else {
            return resultLimit;
        }
    }

    private class CrowdSearchSource<T extends Comparable<T>> implements IteratedSearcher.Source<T> {
        private final QueryBuilder.PartialEntityQueryWithRestriction<T> query;

        public CrowdSearchSource(QueryBuilder.PartialEntityQueryWithRestriction<T> query) {
            this.query = query;
        }

        @Override
        public List<T> search(int offset, int limit) {
            log.debug("Crowd query: offset=" + offset + ", limit=" + limit);
            return ImmutableList.copyOf(searchCrowdLimitedResults(query.startingAt(offset).returningAtMost(limit)));
        }
    }

    @Override
    public List<ApplicationUser> findUsers(final String nameQuery, final String emailQuery, UserSearchParams userSearchParams) {
        return findUsersGeneric(nameQuery, emailQuery, userSearchParams, new ApplicationUserResultType());
    }

    @Override
    public List<String> findUserNames(String query, UserSearchParams userSearchParams) {
        return findUserNames(query, null, userSearchParams);
    }

    @Override
    public List<String> findUserNames(String nameQuery, String emailQuery, UserSearchParams userSearchParams) {
        return findUsersGeneric(nameQuery, emailQuery, userSearchParams, new UserIdResultType());
    }

    private <T> List<T> findUsersGeneric(final String nameQuery, final String emailQuery, UserSearchParams userSearchParams, final UserQueryResultType<T, ?> resultType) {
        // Allow empty queries?
        if (areQueriesNotAllowed(nameQuery, emailQuery, userSearchParams)) {
            return emptyList();
        }

        final String convertedQuery = convertQuery(nameQuery);
        final String convertedEmailQuery = convertQuery(emailQuery);

        // If we can express this as a straight crowd query then we get huge performance increase
        try {
            Iterable<T> usersFromCrowd = attemptFindUsersWithCrowd(convertedQuery, convertedEmailQuery, userSearchParams, resultType);
            log.debug("We did a nice efficient crowd query!");
            return ImmutableList.copyOf(usersFromCrowd);
        } catch (SearchNotSupportedWithCrowdException e) {
            log.debug("Yuck we are doing a horrible slow all-in-memory query");

            StopWatch stopWatch = new StopWatch();
            if (log.isDebugEnabled()) {
                log.debug("Running user-picker search: '" + convertedQuery + "', emailQuery '" + convertedEmailQuery + "'");
            }
            List<ApplicationUser> returnUsers = new ArrayList<>();

            // search using additional parameters in userFilter
            Collection<ApplicationUser> allUsers = getUsersByUserFilter(userSearchParams.getUserFilter(), userSearchParams.getProjectIds());
            if (allUsers == null) {
                // userFilter is disabled, resorting to all users :|
                allUsers = userManager.getAllApplicationUsers();
            }

            if (log.isDebugEnabled()) {
                log.debug("Found all " + allUsers.size() + " users in " + stopWatch.getIntervalTime() + "ms");
            }

            final Predicate<ApplicationUser> userMatcher = new UserMatcherPredicate(convertedQuery, convertedEmailQuery, userSearchParams.canMatchEmail());
            for (final ApplicationUser user : allUsers) {
                if (userMatchesQueries(user, userSearchParams, userMatcher)) {
                    returnUsers.add(user);
                }
            }

            if (log.isDebugEnabled()) {
                log.debug("Matched " + returnUsers.size() + " users in " + stopWatch.getIntervalTime() + "ms");
            }

            Collections.sort(returnUsers, new UserCachingComparator(authenticationContext.getLocale()));

            if (log.isDebugEnabled()) {
                log.debug("Sorted top " + returnUsers.size() + " users in " + stopWatch.getIntervalTime() + "ms");
                log.debug("ApplicationUser-picker search completed in " + stopWatch.getTotalTime() + "ms");
            }

            final Integer maxResults = userSearchParams.getMaxResults();
            final List<ApplicationUser> applicationUsers;
            if (maxResults != null && maxResults >= 0 && maxResults < returnUsers.size()) {
                // if a max exists, let's use it
                applicationUsers = new ArrayList<>(returnUsers.subList(0, maxResults));
            } else {
                // if the max results is not sane or greater than actual size, do as this method always did and return everything
                applicationUsers = returnUsers;
            }

            return ImmutableList.copyOf(resultType.convertApplicationUserResults(applicationUsers));
        }
    }

    private Set<ApplicationUser> getUsersByUserFilter(final UserFilter userFilter, Set<Long> projectIds) {
        if (userFilter == null || !userFilter.isEnabled()) {
            return null;
        }

        Set<ApplicationUser> allUsers = getUsersByGroups(userFilter.getGroups(), null);
        allUsers = getUsersByRoles(userFilter.getRoleIds(), projectIds, allUsers);

        return allUsers == null ? Sets.newHashSet() : allUsers;
    }

    private Set<ApplicationUser> getUsersByRoles(final Set<Long> roleIds, final Set<Long> projectIds, final Set<ApplicationUser> existingUsers) {
        if (CollectionUtils.isEmpty(projectIds) || CollectionUtils.isEmpty(roleIds)) {
            return existingUsers;
        }

        // only search by roles if projectIds is not empty
        // Note that projectIds list should have been at least populated with the list of browsable projects by the current user
        // create the set to inform following codes that it's not search all
        Set<ApplicationUser> allUsers = existingUsers == null ? Sets.<ApplicationUser>newHashSet() : existingUsers;

        for (Project project : getProjects(projectIds)) {
            for (long roleId : roleIds) {
                // ok to repeat calls to projectRoleManager, as it has cache
                ProjectRole projectRole = projectRoleManager.getProjectRole(roleId);
                if (projectRole != null) {
                    allUsers.addAll(projectRoleManager.getProjectRoleActors(projectRole, project).getUsers());
                }
            }
        }
        return allUsers;
    }

    private Set<ApplicationUser> getUsersByGroups(final Set<String> groupNames, final Set<ApplicationUser> existingUsers) {
        if (CollectionUtils.isEmpty(groupNames)) {
            return existingUsers;
        }
        // create the set to inform following codes that we have performed a search
        Set<ApplicationUser> allUsers = existingUsers == null ? Sets.<ApplicationUser>newHashSet() : existingUsers;
        // retrieve users in the groups, instead of getting all users
        for (String groupName : groupNames) {
            allUsers.addAll(groupManager.getUsersInGroup(groupName));
        }

        return allUsers;
    }

    @Override
    public boolean userMatches(final ApplicationUser user, final UserSearchParams userSearchParams) {
        final UserSearchParams allowEmptyQueryParams = userSearchParams.allowEmptyQuery() ? userSearchParams :
                UserSearchParams.builder(userSearchParams).allowEmptyQuery(true).build();

        // the max results value has never been used by this matches check, so continue to ignore it, if set
        final UserSearchParams noMaxResultsParams = UserSearchParams.builder(allowEmptyQueryParams).maxResults(null).sorted(false).build();

        return !filterUsers(singletonList(user), null, null, noMaxResultsParams).isEmpty();
    }

    private boolean userMatchesByUserFilter(final ApplicationUser user, final UserFilter userFilter, final Set<Long> projectIds) {
        return userFilter == null || !userFilter.isEnabled() // return true if userFilter is disabled
                || userMatchesByGroups(user, userFilter.getGroups())
                || userMatchesByRoles(user, userFilter.getRoleIds(), projectIds);
    }

    private boolean userMatchesByRoles(final ApplicationUser user, final Set<Long> roleIds, final Set<Long> projectIds) {
        if (CollectionUtils.isEmpty(roleIds)) {
            return false;
        }

        for (Project project : getProjects(projectIds)) {
            for (long roleId : roleIds) {
                // ok to repeat calls to projectRoleManager, as it has cache
                ProjectRole projectRole = projectRoleManager.getProjectRole(roleId);
                if (projectRole != null) {
                    if (projectRoleManager.isUserInProjectRole(user, projectRole, project)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Retrieve the list of project objects based on a set of project id's with some smartness
     * to avoid too many db round trips.
     *
     * @param projectIds the ids of the projects to be retrieved
     * @return the project objects
     */
    private Collection<Project> getProjects(final Set<Long> projectIds) {
        if (CollectionUtils.isEmpty(projectIds)) {
            // when used in a context where specific projects could be found, the caller is responsible for
            // making sure that all browsable project ids by the current user is passed in as <code>projectIds</code>
            // because only the caller knows the calling user.
            return ImmutableList.of();
        } else if (projectIds.size() == 1) {
            Project project = projectManager.getProjectObj(projectIds.iterator().next());
            return project == null ? ImmutableList.<Project>of() : ImmutableList.of(project);
        } else {
            // try to retrieve all projects at one go, and return those whose ID is in the set
            final List<Project> projects = projectManager.getProjectObjects();
            return newArrayList(Iterables.filter(projects, new Predicate<Project>() {
                @Override
                public boolean apply(@Nullable final Project input) {
                    return input != null && projectIds.contains(input.getId());
                }
            }));
        }
    }

    private boolean userMatchesByGroups(final ApplicationUser user, final Set<String> groupNames) {
        if (CollectionUtils.isEmpty(groupNames)) {
            return false;
        }
        Collection<String> groupsUser = groupManager.getGroupNamesForUser(user);
        for (String groupUser : groupsUser) {
            if (groupNames.contains(groupUser)) {
                return true;
            }
        }

        return false;
    }

    private static String convertQuery(final String nameQuery) {
        return lowerCase(trimToEmpty(nameQuery));
    }

    private static boolean areQueriesNotAllowed(final String nameQuery, final String emailQuery, final UserSearchParams userSearchParams) {
        return !userSearchParams.allowEmptyQuery() && isBlank(nameQuery) && (!userSearchParams.canMatchEmail() || isBlank(emailQuery));
    }

    private static boolean userMatchesQueries(final ApplicationUser user, final UserSearchParams userSearchParams, final Predicate<ApplicationUser> userMatcher) {
        return (user.isActive() ? userSearchParams.includeActive() : userSearchParams.includeInactive())
                && userMatcher.apply(user) && userSearchParams.getPostProcessingFilter().apply(user.getDirectoryUser());
    }

    private static final String VISIBILITY_PUBLIC = "show";
    private static final String VISIBILITY_USER = "user";
    private static final String VISIBILITY_MASKED = "mask";

    /**
     * @see UserSearchService#canShowEmailAddresses(com.atlassian.jira.bc.JiraServiceContext)
     */
    @Override
    public boolean canShowEmailAddresses(final JiraServiceContext jiraServiceContext) {
        if (canPerformAjaxSearch(jiraServiceContext)) {
            final String emailVisibility = applicationProperties.getDefaultBackedString(APKeys.JIRA_OPTION_EMAIL_VISIBLE);
            if (VISIBILITY_PUBLIC.equals(emailVisibility) || (VISIBILITY_MASKED.equals(emailVisibility)) || (VISIBILITY_USER.equals(emailVisibility) && (jiraServiceContext.getLoggedInApplicationUser() != null))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean canPerformAjaxSearch(final JiraServiceContext jiraServiceContext) {
        ApplicationUser loggedInUser = (jiraServiceContext != null) ? jiraServiceContext.getLoggedInApplicationUser() : null;
        return canPerformAjaxSearch(loggedInUser);
    }

    @Override
    public boolean canPerformAjaxSearch(final ApplicationUser user) {
        return permissionManager.hasPermission(Permissions.USER_PICKER, user);
    }

    private <T extends Comparable<T>> Iterable<T> searchCrowdLimitedResults(EntityQuery<T> query) {
        Preconditions.checkArgument(query.getMaxResults() >= 0, "Only result-limited queries can be searched by this method.");

        List<T> allResults = new ArrayList<T>();

        final int resultsPlusOffset = query.getStartIndex() + query.getMaxResults();
        for (Directory directory : getActiveDirectories()) {
            final EntityQuery<T> directoryQuery = QueryBuilder.queryFor(query.getReturnType(), query.getEntityDescriptor(),
                    query.getSearchRestriction(), 0, resultsPlusOffset);
            EntityQuery<T> eventuallyConsistentQuery = new EventuallyConsistentQuery<>(directoryQuery);
            try {
                List<T> directoryResults = directoryManager.searchUsers(directory.getId(), eventuallyConsistentQuery);
                allResults.addAll(directoryResults);
            } catch (final DirectoryNotFoundException e) {
                // directory does not exist, just skip
            } catch (final OperationFailedException e) {
                // keep cycling
                log.error(e.getMessage(), e);
            }
        }

        //Finally sort the results ourselves and trim the list
        Collections.sort(allResults);

        return getPagedResults(query, allResults);
    }

    @VisibleForTesting
    <T extends Comparable<T>> Iterable<T> getPagedResults(final EntityQuery<T> query, List<T> allResults) {
        // harden up the paged results
        final int fromIndex = query.getStartIndex();
        final int toIndex = Math.min(query.getMaxResults() + query.getStartIndex(), allResults.size());

        if (fromIndex > toIndex) {
            log.warn("Something weird happened on getting a paged result. Returning empty list because our search fromIndex is " + fromIndex + " but the toIndex is only " + toIndex);
            return emptyList();
        }

        return ImmutableList.copyOf(allResults.subList(fromIndex, toIndex));
    }

    private Iterable<Directory> getActiveDirectories() {
        return Iterables.filter(Iterables.transform(applicationFactory.getApplication().getDirectoryMappings(), DIRECTORY_FROM_MAPPING), Directories.ACTIVE_FILTER);
    }

    @Nonnull
    @Override
    public Iterable<String> findUserKeysByFullName(@Nullable String fullName) {
        if (isNullOrEmpty(fullName)) {
            return (emptyList());
        }

        //Initial results might be null if a user key lookup fails so post-filter the results to ignore these
        Iterable<String> results = Iterables.transform(crowdService.search(getUserNameFullNameEqualsQuery(fullName)), new KeyFromUserNameFunction());
        results = Iterables.filter(results, Predicates.notNull());

        return results;
    }

    @Nonnull
    @Override
    public Iterable<String> findUserKeysByEmail(@Nullable String email) {
        if (isNullOrEmpty(email)) {
            return (emptyList());
        }

        //Initial results might be null if a user key lookup fails so post-filter the results to ignore these
        Iterable<String> results = Iterables.transform(crowdService.search(getUserNameEmailEqualsQuery(email)), new KeyFromUserNameFunction());
        results = Iterables.filter(results, Predicates.notNull());

        return results;
    }

    @Nonnull
    @Override
    public Iterable<ApplicationUser> findUsersByFullName(@Nullable String fullName) {
        if (isNullOrEmpty(fullName)) {
            return (emptyList());
        }

        Iterable<User> results = crowdService.search(getUserFullNameEqualsQuery(fullName));
        return ApplicationUsers.from(results);
    }

    @Nonnull
    @Override
    public Iterable<ApplicationUser> findUsersByEmail(@Nullable String email) {
        if (isNullOrEmpty(email)) {
            return (emptyList());
        }

        Iterable<User> results = crowdService.search(getUserEmailEqualsQuery(email));
        return ApplicationUsers.from(results);
    }

    @Nonnull
    @Override
    public List<ApplicationUser> filterUsers(List<ApplicationUser> applicationUsers, String nameQuery, UserSearchParams userSearchParams) {
        return filterUsers(applicationUsers, nameQuery, null, userSearchParams);
    }

    @Nonnull
    @Override
    public List<ApplicationUser> filterUsers(List<ApplicationUser> applicationUsers, String nameQuery, String emailQuery, UserSearchParams userSearchParams) {
        // Allow empty queries?
        if (areQueriesNotAllowed(nameQuery, emailQuery, userSearchParams)) {
            return emptyList();
        }

        final Predicate<ApplicationUser> userMatcher = new UserMatcherPredicate(convertQuery(nameQuery), convertQuery(emailQuery), userSearchParams.canMatchEmail());

        Stream<ApplicationUser> applicationUserStream = applicationUsers.stream()
                .filter(Objects::nonNull)
                // check whether the user matches the queries
                .filter(applicationUser -> userMatchesQueries(applicationUser, userSearchParams, userMatcher))
                // check using additional parameters in userFilter
                .filter(applicationUser -> userMatchesByUserFilter(applicationUser, userSearchParams.getUserFilter(), userSearchParams.getProjectIds()));

        if (userSearchParams.isSorted()) {
            // sort first, as using a caching comparator for duplicates. Those duplicates will then be removed by
            // DISTINCT call, which works faster if already ORDERED
            applicationUserStream = applicationUserStream
                    .sorted(new UserCachingComparator(authenticationContext.getLocale()))
                    .distinct();
        }

        // if there is a valid limit specified, then enforce it here
        if (userSearchParams.getMaxResults() != null && userSearchParams.getMaxResults() >= 0) {
            applicationUserStream = applicationUserStream.limit(userSearchParams.getMaxResults());
        }

        return applicationUserStream
                .collect(toList());
    }

    private EntityQuery<User> getUserFullNameEqualsQuery(final String fullName) {
        return QueryBuilder.queryFor(User.class, EntityDescriptor.user())
                .with(new TermRestriction<String>(UserTermKeys.DISPLAY_NAME, fullName))
                .returningAtMost(MAX_MAX_RESULTS);
    }

    private EntityQuery<String> getUserNameFullNameEqualsQuery(final String fullName) {
        return QueryBuilder.queryFor(String.class, EntityDescriptor.user())
                .with(new TermRestriction<String>(UserTermKeys.DISPLAY_NAME, fullName))
                .returningAtMost(MAX_MAX_RESULTS);
    }

    private EntityQuery<User> getUserEmailEqualsQuery(final String email) {
        return QueryBuilder.queryFor(User.class, EntityDescriptor.user())
                .with(new TermRestriction<String>(UserTermKeys.EMAIL, email))
                .returningAtMost(MAX_MAX_RESULTS);
    }

    private EntityQuery<String> getUserNameEmailEqualsQuery(final String email) {
        return QueryBuilder.queryFor(String.class, EntityDescriptor.user())
                .with(new TermRestriction<String>(UserTermKeys.EMAIL, email))
                .returningAtMost(MAX_MAX_RESULTS);
    }

    private static Function<DirectoryMapping, Directory> DIRECTORY_FROM_MAPPING = new Function<DirectoryMapping, Directory>() {
        public Directory apply(final DirectoryMapping from) {
            return from.getDirectory();
        }
    };

    private class KeyFromUserNameFunction implements Function<String, String> {
        @Override
        public String apply(@Nullable String userName) {
            if (userName == null) {
                return null;
            }

            return userKeyStore.getKeyForUsername(userName);
        }
    }

    private static class SearchNotSupportedWithCrowdException extends Exception {
    }

    /**
     * @param <T> the actual result type.
     * @param <C> the Crowd result type when performing Crowd queries.
     */
    private static abstract class UserQueryResultType<T, C extends Comparable<C>> {
        private final Class<C> crowdResultType;

        protected UserQueryResultType(Class<C> crowdResultType) {
            this.crowdResultType = crowdResultType;
        }

        public final Class<C> getCrowdResultType() {
            return crowdResultType;
        }

        public abstract Iterable<T> convertCrowdResults(Iterable<C> crowdResults);

        public abstract Iterable<T> convertApplicationUserResults(Iterable<ApplicationUser> applicationUserResults);
    }

    private static class ApplicationUserResultType extends UserQueryResultType<ApplicationUser, User> {
        public ApplicationUserResultType() {
            super(User.class);
        }

        @Override
        public Iterable<ApplicationUser> convertApplicationUserResults(Iterable<ApplicationUser> applicationUserResults) {
            return applicationUserResults;
        }

        @Override
        public Iterable<ApplicationUser> convertCrowdResults(Iterable<User> crowdResults) {
            return ApplicationUsers.from(crowdResults);
        }
    }

    private static class UserIdResultType extends UserQueryResultType<String, String> {
        public UserIdResultType() {
            super(String.class);
        }

        @Override
        public Iterable<String> convertApplicationUserResults(Iterable<ApplicationUser> applicationUserResults) {
            return ImmutableList.copyOf(Iterables.transform(applicationUserResults, ApplicationUser::getUsername));
        }

        @Override
        public Iterable<String> convertCrowdResults(Iterable<String> crowdResults) {
            return crowdResults;
        }
    }


}
