package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;

import javax.annotation.Nullable;
import java.util.EnumSet;

/**
 * Indexes attachments by name so they are JQL searchable.
 */
public class UpgradeTask_Build6205 extends AbstractReindexUpgradeTask {
    @Override
    public int getBuildNumber() {
        return 6205;
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception {
        getReindexRequestService().requestReindex(ReindexRequestType.DELAYED, EnumSet.of(AffectedIndex.ISSUE), EnumSet.noneOf(SharedEntityType.class));
    }

    @Override
    public String getShortDescription() {
        return "Indexes if issue has attachments so they are JQL searchable.";
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        // Flagged as not downgradable because it was created before the Downgrade Task framework was built.
        return true;
    }

    @Nullable
    @Override
    public Integer dependsUpon() {
        return 6200;
    }

}
