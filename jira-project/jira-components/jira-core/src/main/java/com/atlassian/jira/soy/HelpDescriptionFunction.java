package com.atlassian.jira.soy;

import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.HelpUrls;

import javax.annotation.Nonnull;

/**
 * getJiraHelpDesc('help_url_key_here') soy function.
 *
 * <p>Soy function that returns the description for a help URL specified by the provided help key.
 *
 * @since v7.0
 */
public class HelpDescriptionFunction extends AbstractHelpFunction {

    public HelpDescriptionFunction(HelpUrls helpUrls) {
        super(helpUrls);
    }

    @Override
    public String getName() {
        return "getJiraHelpDesc";
    }

    @Override
    @Nonnull
    String getHelpValue(HelpUrl helpUrl) {
        return helpUrl.getDescription();
    }
}
