package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.util.map.CacheObject;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import java.util.concurrent.TimeUnit;

import static com.atlassian.jira.crowd.embedded.ofbiz.DirectoryEntityKey.getKeyLowerCase;

/**
 * A lazy loading User Cache.
 *
 * @since v7.0
 */
class LazyOfBizGroupCache implements OfBizGroupCache {
    private final OfBizDelegator ofBizDelegator;
    private final Cache<DirectoryEntityKey, CacheObject<OfBizGroup>> cache;

    public LazyOfBizGroupCache(final CacheManager cacheManager, final OfBizDelegator ofBizDelegator) {
        this.ofBizDelegator = ofBizDelegator;
        this.cache = cacheManager.getCache(LazyOfBizGroupCache.class.getName() + ".groupCache",
                new GroupCacheLoader(),
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).flushable()
                        .build());
    }

    @Override
    public boolean isCacheInitialized() {
        //The lazy cache is always ready for action
        return true;
    }

    @Override
    public void refresh() {
        cache.removeAll();
    }

    @Override
    public OfBizGroup getCaseInsensitive(long directoryId, String name) {
        CacheObject<OfBizGroup> cacheValue = cache.get(getKeyLowerCase(directoryId, name));
        if (cacheValue == null) {
            return null;
        }
        return cacheValue.getValue();
    }

    @Override
    public DirectoryEntityKey refresh(final OfBizGroup group) {
        //Remove entry from lazy cache so it is re-read from the database on next access
        DirectoryEntityKey key = getKeyLowerCase(group);
        cache.remove(key);
        return key;
    }

    @Override
    public void remove(final long directoryId, final String name) {
        cache.remove(getKeyLowerCase(directoryId, name));
    }

    @Override
    public void remove(DirectoryEntityKey key) {
        cache.remove(key);
    }

    private class GroupCacheLoader implements CacheLoader<DirectoryEntityKey, CacheObject<OfBizGroup>> {
        @Nonnull
        @Override
        public CacheObject<OfBizGroup> load(@Nonnull final DirectoryEntityKey key) {
            final GenericValue group = Select.columns(OfBizGroup.SUPPORTED_FIELDS)
                    .from(GroupEntity.ENTITY)
                    .whereEqual(GroupEntity.DIRECTORY_ID, key.getDirectoryId())
                    .andEqual(GroupEntity.LOWER_NAME, key.getName())
                    .runWith(ofBizDelegator)
                    .singleValue();

            if (group == null) {
                return CacheObject.NULL();
            } else {
                return CacheObject.wrap(OfBizGroup.from(group));
            }
        }
    }

}
