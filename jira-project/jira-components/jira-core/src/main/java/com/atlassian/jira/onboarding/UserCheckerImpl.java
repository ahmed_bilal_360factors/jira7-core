package com.atlassian.jira.onboarding;

import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.PropertiesManager;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserKeyStore;
import com.atlassian.jira.web.HttpServletVariables;
import com.atlassian.jira.web.util.CookieUtils;
import com.opensymphony.module.propertyset.PropertySet;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class UserCheckerImpl implements UserChecker {
    private static final Logger log = LoggerFactory.getLogger(UserCheckerImpl.class);

    private static final String IMPERSONATION_COOKIE = "um.user.impersonated.username";

    private final UserKeyStore userKeyStore;
    private final LoginService loginService;
    private final FeatureManager featureManager;
    private final GlobalPermissionManager permissionManager;
    private final PropertiesManager propertiesManager;
    private final HttpServletVariables httpServetVariables;

    public UserCheckerImpl(final UserKeyStore userKeyStore,
                           final LoginService loginService,
                           final FeatureManager featureManager,
                           final GlobalPermissionManager permissionManager,
                           final PropertiesManager propertiesManager,
                           final HttpServletVariables httpServetVariables) {
        this.userKeyStore = userKeyStore;
        this.loginService = loginService;
        this.featureManager = featureManager;
        this.permissionManager = permissionManager;
        this.propertiesManager = propertiesManager;
        this.httpServetVariables = httpServetVariables;
    }

    @Override
    public boolean firstTimeLoggingIn(@Nonnull final ApplicationUser user) {
        log.debug("current user id is {}, user id threshold is {}", userKeyStore.getIdForUserKey(user.getKey()), getUserIdThreshold());

        final Long loginCount = loginService.getLoginInfo(user.getUsername()).getLoginCount();
        // veteran users
        if (userKeyStore.getIdForUserKey(user.getKey()) <= getUserIdThreshold()) {
            /**
             * For veteran users (below or equals the id threshold), we can't assume that a loginCount of null is an
             * appropriate condition to show the onboarding (they might have been using JIRA a lot, despite the null
             * loginCount), so we return true only if loginCount is exactly 1.
             */
            return loginCount != null && loginCount == 1;
        } else { // new users
            /**
             * For new users (above the id threshold), differently from the conditions above for veteran users, we can
             * assign the onboarding not only when the loginCount is equals to 1, but also when it is null, because we
             * know this user is new and was created after the execution of upgrade task 65001.
             */
            return loginCount == null || loginCount == 1;
        }
    }

    @Override
    public boolean isOnDemandSysAdmin(@Nonnull final ApplicationUser user) {
        return false;
    }

    @Override
    public boolean isImpersonationActive(@Nonnull final ApplicationUser user) {
        String impersonationCookieValue = CookieUtils.getCookieValue(IMPERSONATION_COOKIE, httpServetVariables.getHttpRequest());

        if (impersonationCookieValue != null) {
            try {
                impersonationCookieValue = URLDecoder.decode(impersonationCookieValue, "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                // no-op
            }
        }

        // to prevent a scenario where the impersonation cookie is in a stale state, we compare its
        // value with the username currently logged in to ensure we have a match
        return StringUtils.isNotBlank(user.getUsername()) && user.getUsername().equals(impersonationCookieValue);
    }

    /**
     * Return the app_user.id that should be used to identify new and veteran users
     *
     * @return Long
     */
    private Long getUserIdThreshold() {
        final PropertySet propertySet = propertiesManager.getPropertySet();
        if (propertySet.exists(APKeys.ONBOARDING_APP_USER_ID_THRESHOLD)) {
            try {
                return Long.valueOf(propertySet.getString(APKeys.ONBOARDING_APP_USER_ID_THRESHOLD));
            } catch (Exception e) {
                // no-op, defaults to the value below
            }
        }
        // this value is set by a background upgrade task, so we need a default value to use until the upgrade task has run.
        return Long.MAX_VALUE;
    }
}
