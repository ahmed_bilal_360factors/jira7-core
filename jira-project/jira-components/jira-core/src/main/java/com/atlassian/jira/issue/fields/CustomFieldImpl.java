package com.atlassian.jira.issue.fields;

import com.atlassian.jira.issue.customfields.CustomFieldUtils;

@Deprecated
/**
 * @deprecated since v7.2.5 use {@link ImmutableCustomField}}
 */
public class CustomFieldImpl {
    /**
     * Name of the parameter that stores the issue id in the current context in CustomFieldParams. This is useful for
     * getting data such as the previous value.
     *
     * @deprecated since v7.2.5 use {@link CustomFieldUtils#getParamKeyIssueId()} instead.
     */
    public static String getParamKeyIssueId() {
        return CustomFieldUtils.getParamKeyPrefixAtl() + "issue_id";
    }

    /**
     * Name of the parameter that stores the project id of the associated issue in the current context in
     * CustomFieldParams.
     *
     * @deprecated since v7.2.5 use {@link CustomFieldUtils#getParamKeyProjectId()} instead.
     */
    public static String getParamKeyProjectId() {
        return CustomFieldUtils.getParamKeyPrefixAtl() + "project_id";
    }
}
