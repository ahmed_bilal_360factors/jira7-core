package com.atlassian.jira.config.feature;

import com.atlassian.fugue.Option;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.config.FeatureFlag;
import com.atlassian.jira.config.FeatureFlagProvider;
import com.atlassian.ozymandias.PluginPointFunction;
import com.atlassian.ozymandias.SafePluginPointAccess;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.collect.Maps;

import java.util.Map;

public class FeatureFlagProviderAccessor {
    private final PluginAccessor pluginAccessor;

    public FeatureFlagProviderAccessor(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    public Map<String, FeatureFlag> getFeatureFlags() {
        final Map<String, FeatureFlag> flags = Maps.newHashMap();
        SafePluginPointAccess.to(pluginAccessor).forType(FeatureFlagModuleDescriptor.class, new PluginPointFunction<FeatureFlagModuleDescriptor, FeatureFlagProvider, Unit>() {
            @Override
            public Unit onModule(final FeatureFlagModuleDescriptor moduleDescriptor, final FeatureFlagProvider provider) {
                for (FeatureFlag flag : moduleDescriptor.getFeatureFlags()) {
                    flags.put(flag.featureKey(), flag);
                }
                return Unit.VALUE;
            }
        });
        return flags;
    }

    public Option<FeatureFlag> findFeatureFlag(String featureKey) {
        FeatureFlag featureFlag = getFeatureFlags().get(featureKey);
        return Option.option(featureFlag);
    }
}