package com.atlassian.jira.upgrade.tasks.role;

/**
 * Exists for writing migration log messages to the relevant logs, typically the JIRA output logs and Audit log.
 *
 * @since 7.0
 */
public interface MigrationLogDao {
    /**
     * Records the provided logs in the record for later retrieval.
     *
     * @param record contains the log messages for writing
     */
    void write(MigrationLog record);

    /**
     * Persists the auditing entry in the database. Changes are written immediately.
     *
     * @param event the log entry to persist.
     */
    void store(AuditEntry event);
}
