package com.atlassian.jira.config.feature;

import com.atlassian.fugue.Option;
import com.atlassian.jira.config.CoreFeatures;
import com.atlassian.jira.config.Feature;
import com.atlassian.jira.config.FeatureFlag;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.plugin.profile.DarkFeatures;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Set;

/**
 * Simple feature manager used during bootstrap. Setup does not use features at all currently.
 *
 * @since v5.0
 */
public class BootstrapFeatureManager implements FeatureManager {
    @Override
    public boolean isEnabled(String featureKey) {
        return false;
    }

    @Override
    public boolean isEnabled(Feature coreFeature) {
        return false;
    }

    @Override
    public boolean isEnabled(FeatureFlag featureFlag) {
        return false;
    }

    @Override
    public Option<FeatureFlag> getFeatureFlag(String featureKey) {
        return null;
    }

    @Override
    public boolean isEnabled(CoreFeatures coreFeature) {
        return false;
    }

    @Override
    public Set<String> getEnabledFeatureKeys() {
        return Collections.emptySet();
    }

    @Override
    public DarkFeatures getDarkFeatures() {
        return new DarkFeatures(Collections.<String>emptySet(), Collections.<String>emptySet(), Collections.<String>emptySet());
    }

    @Override
    public Set<FeatureFlag> getRegisteredFlags() {
        return Collections.emptySet();
    }

    @Override
    public void enableUserDarkFeature(ApplicationUser user, String feature) {
    }

    @Override
    public void disableUserDarkFeature(ApplicationUser user, String feature) {

    }

    @Override
    public void enableSiteDarkFeature(String feature) {
    }

    @Override
    public void disableSiteDarkFeature(String feature) {
    }

    @Override
    public boolean hasSiteEditPermission() {
        return true;
    }

    @Override
    public DarkFeatures getDarkFeaturesForUser(@Nullable ApplicationUser user) {
        return getDarkFeatures();
    }

    @Override
    public boolean isEnabledForUser(ApplicationUser user, String featureKey) {
        return isEnabled(featureKey);
    }
}
