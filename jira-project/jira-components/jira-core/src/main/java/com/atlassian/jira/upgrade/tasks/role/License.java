package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.extras.api.LicenseException;
import com.atlassian.jira.license.JiraProductLicense;
import com.atlassian.jira.license.LicenseDetailsFactoryImpl.JiraProductLicenseManager;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nullable;
import java.util.Set;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Represents a license for the renaissance migration (i.e. JIRA 6.x single use permission to JIRA 7.x multiple
 * applications).
 *
 * @since v7.0
 */
final class License {
    private static final JiraProductLicenseManager FACTORY = JiraProductLicenseManager.INSTANCE;

    private final JiraProductLicense productLicense;
    private final String licenseString;

    License(final String licenseString) throws LicenseException {
        this.licenseString = notNull("licenseString", licenseString);
        try {
            this.productLicense = FACTORY.getProductLicense(licenseString);
        } catch (LicenseException e) {
            String msg = String.format("Unable to parse license: %s", abbreviate(licenseString));
            throw new MigrationFailedException(msg, e);
        }
    }

    JiraProductLicense productLicense() {
        return productLicense;
    }

    String licenseString() {
        return licenseString;
    }

    Set<ApplicationKey> applicationKeys() {
        return productLicense.getApplications().getKeys();
    }

    String abbreviatedLicenseString() {
        return abbreviate(licenseString);
    }

    String getSEN() {
        return productLicense.getSupportEntitlementNumber();
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final License license = (License) o;
        return licenseString.equals(license.licenseString);
    }

    @Override
    public int hashCode() {
        return licenseString.hashCode();
    }

    @Override
    public String toString() {
        return String.format("License for %s: %s.", productLicense.getApplications().getKeys(),
                abbreviatedLicenseString());
    }

    private static String abbreviate(final String licenseString) {
        return StringUtils.abbreviate(licenseString, 40);
    }
}
