package com.atlassian.jira.plugin.myjirahome;

import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;
import com.atlassian.plugin.web.model.WebLink;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Map;

/**
 * Resolves the current My JIRA Home location by looking up the plugin and returning the rendered url. If the plugin is
 * not enabled, the {@link #DEFAULT_HOME_NOT_ANON} is returned for logged in users, or {@link #DEFAULT_HOME_OD_ANON} for anons.
 *
 * @since 5.1
 */
public class MyJiraHomeLinkerImpl implements MyJiraHomeLinker {
    private static final String CONTEXT_KEY_USER = "user";
    private static final String CONTEXT_KEY_HELPER = "helper";

    private final PluginAccessor pluginAccessor;
    private final MyJiraHomePreference myJiraHomePreference;
    private final ProjectService projectService;

    public MyJiraHomeLinkerImpl(@Nonnull final PluginAccessor pluginAccessor, @Nonnull final MyJiraHomePreference myJiraHomePreference, @Nonnull final ProjectService projectService) {
        this.pluginAccessor = pluginAccessor;
        this.myJiraHomePreference = myJiraHomePreference;
        this.projectService = projectService;
    }

    @Nonnull
    @Override
    public String getHomeLink(@Nullable final ApplicationUser user) {
        final String completePluginModuleKey = myJiraHomePreference.findHome(user);
        try {
            // Avoid going through an IllegalArgumentException for empty plugin module key
            if (StringUtils.isEmpty(completePluginModuleKey)) {
                return DEFAULT_HOME_NOT_ANON;
            }

            if (!pluginAccessor.isPluginModuleEnabled(completePluginModuleKey)) {
                return DEFAULT_HOME_NOT_ANON;
            }

            final WebLink link = getWebLinkFromWebItemModuleDescriptor(completePluginModuleKey, user);
            if (link != null) {
                return link.getRenderedUrl(Collections.<String, Object>emptyMap());
            } else {
                return DEFAULT_HOME_NOT_ANON;
            }
        } catch (IllegalArgumentException e) {
            return DEFAULT_HOME_NOT_ANON;
        }
    }

    @Nullable
    private WebLink getWebLinkFromWebItemModuleDescriptor(@Nonnull final String completePluginModuleKey,
                                                          @Nullable ApplicationUser user) {
        final WebItemModuleDescriptor webItemModuleDescriptor = getWebItemModuleDescriptorFromKey(completePluginModuleKey);
        if (webItemModuleDescriptor != null && isConditionSatisfied(webItemModuleDescriptor, user)) {
            return webItemModuleDescriptor.getLink();
        } else {
            return null;
        }
    }

    @Nullable
    private WebItemModuleDescriptor getWebItemModuleDescriptorFromKey(@Nonnull final String completePluginModuleKey) {
        final ModuleDescriptor<?> pluginModule = pluginAccessor.getPluginModule(completePluginModuleKey);
        if (pluginModule instanceof WebItemModuleDescriptor) {
            return (WebItemModuleDescriptor) pluginModule;
        } else {
            return null;
        }
    }

    private boolean isConditionSatisfied(@Nonnull WebItemModuleDescriptor webItemModuleDescriptor,
                                         @Nullable ApplicationUser user) {
        Condition condition = webItemModuleDescriptor.getCondition();
        if (condition == null) {
            return true;
        }

        return condition.shouldDisplay(createContext(user));
    }

    private Map<String, Object> createContext(@Nullable ApplicationUser user) {
        JiraHelper jiraHelper = new JiraHelper(ExecutingHttpRequest.get());

        Map<String, Object> context = Maps.newHashMap(jiraHelper.getContextParams());
        context.put(CONTEXT_KEY_USER, user);
        context.put(CONTEXT_KEY_HELPER, jiraHelper);
        return context;
    }

    @Override
    @Nonnull
    public String getDefaultUserHome() {
        return DEFAULT_HOME_NOT_ANON;
    }
}
