package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.atlassian.jira.CachingComponent;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.auditing.AssociatedItem;
import com.atlassian.jira.auditing.ChangedValue;
import com.atlassian.jira.auditing.ChangedValueImpl;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.jira.application.ApplicationKeys.CORE;
import static com.atlassian.jira.application.ApplicationKeys.SOFTWARE;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * @since v7.0
 */
public class DefaultApplicationServiceImpl implements DefaultApplicationService {
    private static final Logger log = LoggerFactory.getLogger(DefaultApplicationServiceImpl.class);
    private static final boolean EVENT_SHOWS_IN_CLOUD_LOG = true;

    private final MigrationLogDao migrationLogDao;
    private final LicenseDao licenseDao;
    private final DefaultApplicationDao defaultApplicationDao;

    private final ApplicationRoleManager applicationRoleManager;
    private final OfBizDelegator db;
    private final ApplicationProperties applicationProperties;
    private final JiraLicenseManager jiraLicenseManager;

    public DefaultApplicationServiceImpl(final ApplicationRoleManager applicationRoleManager, final OfBizDelegator db,
                                         final ApplicationProperties applicationProperties, final JiraLicenseManager jiraLicenseManager) {
        this.applicationRoleManager = applicationRoleManager;
        this.db = db;
        this.applicationProperties = applicationProperties;
        this.jiraLicenseManager = jiraLicenseManager;
        this.migrationLogDao = new MigrationLogDaoImpl(db);
        this.licenseDao = new LicenseDaoImpl(this.applicationProperties, this.db, this.jiraLicenseManager);
        this.defaultApplicationDao = new DefaultApplicationDaoImpl(db);
    }

    @VisibleForTesting
    DefaultApplicationServiceImpl(final MigrationLogDao migrationLogDao,
                                  final LicenseDao licenseDao, final DefaultApplicationDao defaultApplicationDao, final ApplicationRoleManager applicationRoleManager) {
        this.migrationLogDao = notNull("migrationLogDao", migrationLogDao);
        this.licenseDao = notNull("licenseDao", licenseDao);
        this.defaultApplicationDao = notNull("defaultApplicationDao", defaultApplicationDao);
        this.applicationRoleManager = notNull("applicationRoleManager", applicationRoleManager);
        this.db = null;
        this.applicationProperties = null;
        this.jiraLicenseManager = null;
    }

    public void setApplicationsAsDefaultDuring6xTo7xUpgrade() {
        final Licenses licenses = licenseDao.getLicenses();
        Set<ApplicationKey> licensedApps = getLicensedApplications(licenses);
        Option<ApplicationKey> licensedAppOption = getAppToSetAsDefault(licensedApps);

        if (licensedAppOption.isEmpty()) {
            log.info("Found no licenses. Default applications will not be selected.");
            return;
        }
        try {
            final ApplicationKey applicationKey = licensedAppOption.get();

            createAuditingRecord(applicationKey);
            defaultApplicationDao.setApplicationsAsDefault(Collections.singleton(applicationKey));
        } finally {
            if (applicationRoleManager instanceof CachingComponent) {
                log.info("Clearing caches for application roles.");
                ((CachingComponent) applicationRoleManager).clearCache();
            }
        }
    }

    private void createAuditingRecord(ApplicationKey applicationKey) {
        log.debug("Creating audit entry for {}. The application will be marked as a default.", applicationKey);
        final List<ChangedValue> changedValues = ImmutableList.of(new ChangedValueImpl(
                        applicationKey.toString(), "", applicationKey + " (default)")
        );
        migrationLogDao.store(new AuditEntry(this.getClass(),
                "Setting the application defaults", "Applications were marked as default, based on the product license."
                + " New users will be created with respect to the default applications selection.",
                AssociatedItem.Type.APPLICATION_ROLE,
                applicationKey.toString(), EVENT_SHOWS_IN_CLOUD_LOG,
                changedValues, AuditEntrySeverity.INFO));
    }

    private Option<ApplicationKey> getAppToSetAsDefault(final Set<ApplicationKey> licensedApps) {
        if (licensedApps.contains(SOFTWARE)) {
            return Option.option(SOFTWARE);
        } else if (licensedApps.contains(CORE)) {
            return Option.option(CORE);
        }
        return Option.none();
    }

    @VisibleForTesting
    Set<ApplicationKey> getLicensedApplications(final Licenses licenses) {
        return licenses.get().stream()
                .map(License::applicationKeys)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }
}
