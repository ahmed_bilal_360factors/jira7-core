package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * @since v6.3
 */
public class DefaultApplicationRoleAdminService implements ApplicationRoleAdminService {
    private final GroupManager groupManager;
    private final ApplicationRoleManager applicationRoleManager;
    private final JiraAuthenticationContext ctx;
    private final GlobalPermissionManager permissionManager;

    public DefaultApplicationRoleAdminService(@Nonnull final GroupManager groupManager,
                                              @Nonnull final ApplicationRoleManager applicationRoleManager,
                                              @Nonnull final JiraAuthenticationContext ctx,
                                              @Nonnull final GlobalPermissionManager permissionManager) {
        this.ctx = notNull("ctx", ctx);
        this.permissionManager = notNull("permissionManager", permissionManager);
        this.groupManager = notNull("groupManager", groupManager);
        this.applicationRoleManager = notNull("applicationRoleManager", applicationRoleManager);
    }

    @Nonnull
    @Override
    public ServiceOutcome<Set<ApplicationRole>> getRoles() {
        final ServiceOutcome<Set<ApplicationRole>> outcome = validatePermission();
        if (!outcome.isValid()) {
            return outcome;
        }

        return ServiceOutcomeImpl.ok(applicationRoleManager.getRoles());
    }

    @Nonnull
    @Override
    public ServiceOutcome<ApplicationRole> getRole(@Nonnull final ApplicationKey key) {
        notNull("key", key);

        final ServiceOutcome<ApplicationRole> outcome = validatePermission();
        if (!outcome.isValid()) {
            return outcome;
        }

        final Option<ApplicationRole> roleOption = applicationRoleManager.getRole(key);
        if (roleOption.isDefined()) {
            return ServiceOutcomeImpl.ok(roleOption.get());
        } else {
            return generateNotFoundOutcomeFor(key);
        }
    }

    @Nonnull
    @Override
    public ServiceOutcome<ApplicationRole> setRole(@Nonnull final ApplicationRole role) {
        notNull("role", role);

        ServiceOutcome<ApplicationRole> outcome = validatePermission();
        if (!outcome.isValid()) {
            return outcome;
        }

        if (!applicationRoleManager.getRole(role.getKey()).isDefined()) {
            return generateNotFoundOutcomeFor(role.getKey());
        }

        outcome = validateGroups(role);
        if (!outcome.isValid()) {
            return outcome;
        }

        return ServiceOutcomeImpl.ok(applicationRoleManager.setRole(role));
    }

    @Nonnull
    @Override
    public ServiceOutcome<Set<ApplicationRole>> setRoles(@Nonnull final Collection<ApplicationRole> roles) {
        notNull("roles", roles);

        ServiceOutcome<Set<ApplicationRole>> outcome = validatePermission();
        if (!outcome.isValid()) {
            return outcome;
        }

        Set<ApplicationRole> appRoles = new HashSet<>();
        for (ApplicationRole role : roles) {
            if (!applicationRoleManager.getRole(role.getKey()).isDefined()) {
                return generateNotFoundOutcomeFor(role.getKey());
            }

            outcome = validateGroups(role);
            if (!outcome.isValid()) {
                return outcome;
            }

            appRoles.add(applicationRoleManager.setRole(role));
        }

        return ServiceOutcomeImpl.ok(appRoles);
    }

    private <T> ServiceOutcome<T> validatePermission() {
        if (!permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, ctx.getLoggedInUser())) {
            return ServiceOutcomeImpl.error(ctx.getI18nHelper().getText("application.role.service.permission.denied"),
                    ErrorCollection.Reason.FORBIDDEN);
        } else {
            return ServiceOutcomeImpl.ok(null);
        }
    }

    private <T> ServiceOutcome<T> validateGroups(ApplicationRole role) {
        for (Group group : role.getGroups()) {
            if (!groupManager.groupExists(group)) {
                return generateErrorOutcomeFor(ApplicationRoleAdminService.ERROR_GROUPS,
                        ctx.getI18nHelper().getText("application.role.service.group.does.not.exist", group.getName()));
            }
        }

        return ServiceOutcomeImpl.ok(null);
    }

    private <T> ServiceOutcome<T> generateErrorOutcomeFor(final String key, final String message) {
        final SimpleErrorCollection collection = new SimpleErrorCollection();
        collection.addReason(ErrorCollection.Reason.VALIDATION_FAILED);
        collection.addError(key, message);

        return new ServiceOutcomeImpl<>(collection);
    }

    private <T> ServiceOutcome<T> generateNotFoundOutcomeFor(ApplicationKey key) {
        final String message = ctx.getI18nHelper().getText("application.role.service.role.does.not.exist", key.value());
        return ServiceOutcomeImpl.error(message, ErrorCollection.Reason.NOT_FOUND);
    }
}
