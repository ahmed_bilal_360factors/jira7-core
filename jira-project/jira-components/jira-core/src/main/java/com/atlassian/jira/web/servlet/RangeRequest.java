package com.atlassian.jira.web.servlet;

import javax.annotation.Nullable;

/**
 * Represents a Range Request HTTP header as per IETF RFC 7233.
 * See https://tools.ietf.org/html/rfc7233
 *
 * @since v7.2
 */
public class RangeRequest {
    private final Integer startIndex;
    private final Integer endIndex;

    public RangeRequest(Integer startIndex, Integer endIndex) {
        if (startIndex == null && endIndex == null) {
            throw new IllegalArgumentException("Must have at least one non-null value");
        }
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    public Integer getStartIndex() {
        return startIndex;
    }

    public Integer getEndIndex() {
        return endIndex;
    }

    /**
     * Parse the given HTTP Range request header value.
     *
     * @param headerValue eg "bytes=500-699,800-999"
     * @return parsed RangeRequest or null to ignore the Range header (its not in "bytes")
     *
     * @throws BadRequestException if the Range header value is malformed
     */
    @Nullable
    public static RangeRequest parse(final String headerValue) throws BadRequestException {
        String text = headerValue.trim();
        if (text.startsWith("bytes=")) {
            text = text.substring(6);
        } else {
            // This is not malformed. The RFC allows for extensible Range Units, but we only support "bytes".
            // Return null to ignore the Range Header.
            return null;
        }

        // we might recieve a multi-range request like "bytes=500-700,601-999"
        final int idx = text.indexOf(",");
        if (idx > -1) {
            // For simplicitly we use the first range, and ignore the rest
            text = text.substring(0, idx);
        }

        // String.split will ignore the trailing empty strings. Deal with "100-" style strings specially
        if (text.endsWith("-")) {
            if (text.trim().equals("-")) {
                throw new BadRequestException("Malformed Range header");
            }
            return new RangeRequest(parseInt(text.substring(0, text.length() - 1)), null);
        }

        String[] split = text.split("-");
        if (split.length != 2) {
            throw new BadRequestException("Malformed Range header");
        }
        return new RangeRequest(parseInt(split[0].trim()), parseInt(split[1].trim()));
    }

    private static Integer parseInt(String text) throws BadRequestException {
        if (text.isEmpty()) {
            return null;
        }
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException e) {
            throw new BadRequestException("Malformed Range header");
        }
    }

    RangeResponse calculateRangeResponse(int totalFileLength) throws RangeNotSatisfiableException {
        // prefix range or suffix range?
        if (startIndex == null) {
            // https://tools.ietf.org/html/rfc7233#section-2.1
            // this is a suffix-byte-range-spec like "bytes=-1000" which means "give me the last 1000 bytes"
            int numberOfBytesRequested = getEndIndex();
            if (totalFileLength < numberOfBytesRequested) {
                // return the whole file
                return new RangeResponse(0, totalFileLength - 1, totalFileLength);
            } else {
                return new RangeResponse(totalFileLength - numberOfBytesRequested, totalFileLength - 1, totalFileLength);
            }
        } else {
            // normal byte-range-spec eg "bytes=12-14" or "bytes=48-"
            // Check if it is satisfiable
            if (startIndex < totalFileLength) {
                // Happy days
                return new RangeResponse(startIndex, endIndex, totalFileLength);
            } else {
                throw new RangeNotSatisfiableException("Cannot return request range '" + toString() + "'. Attachment has only " + totalFileLength + " bytes", totalFileLength);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RangeRequest that = (RangeRequest) o;

        if (startIndex != null ? !startIndex.equals(that.startIndex) : that.startIndex != null) return false;
        if (endIndex != null ? !endIndex.equals(that.endIndex) : that.endIndex != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = startIndex != null ? startIndex.hashCode() : 0;
        result = 31 * result + (endIndex != null ? endIndex.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "bytes=" + format(startIndex) + "-" + format(endIndex);
    }

    private String format(Integer index) {
        return index == null ? "" : index.toString();
    }
}
