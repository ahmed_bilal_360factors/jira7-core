package com.atlassian.jira.config.component;

import org.picocontainer.PicoCompositionException;
import org.picocontainer.PicoContainer;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

// TODO Remove this class in favour of {@link org.picocontainer.injectors.ProviderAdapter} pattern
public abstract class AbstractSwitchingInvocationAdaptor<T> extends AbstractComponentAdaptor<T> {

    private final Class<? extends T> enabledClass;
    private final Class<? extends T> disabledClass;

    protected AbstractSwitchingInvocationAdaptor(
            final Class<T> interfaceClass, final Class<? extends T> enabledClass, final Class<? extends T> disabledClass) {
        super(interfaceClass);
        this.enabledClass = enabledClass;
        this.disabledClass = disabledClass;
    }

    protected boolean isEnabled() {
        return getInvocationSwitcher().isEnabled();
    }

    /**
     * Returns the interface class that this adaptor was registered with.
     * <p>
     * <strong>WARNING</strong>: This should <strong>NOT</strong> return either concrete implementation class!
     * </p>
     * <p>
     * When Pico needs to satisfy a dependency but the class it is asked for is not registered, it reacts by asking
     * every component adaptor that <strong>is</strong> registered what its concrete implementation class is.  The
     * thinking is that maybe, just maybe, it can find something registered under a different key that can satisfy
     * this dependency.
     * </p>
     * <p>
     * This is the method that Pico uses to do that, and what it's trying to figure out is "When I ask you for your
     * component instance, of what class will it be?"  Previously, this method returned the specific implementation
     * class that it would <strong>delegate</strong> to right now, but there are two major problems with doing that:
     * </p>
     * <ol>
     * <li>If by some strange coincidence the class we return accidentally does satisfy the missing dependency,
     * then Pico will expect a call to {@link #getComponentInstance(PicoContainer)} to yield an object of
     * that class.  However, what we actually return is a dynamic proxy for our own registration interface,
     * not either of the concrete implementations.  Our own registration interface cannot be the desired one
     * or Pico would have resolved it to us without asking this question in the first place, so this would
     * <strong>NEVER</strong> work.</li>
     * <li>The act of determining the concrete implementation will probably access other components, most likely
     * application properties and the caching property set that backs them.  If they also haven't been resolved yet,
     * then the result is likely to be infinite recursion leading to a {@code StackOverflowError} instead of gracefully
     * reporting the unsatisfiable dependency.  This makes the problem harder to diagnose and fix, so let's not do
     * that.</li>
     * </ol>
     * <p>
     * The most accurate answer here would be to return the actual dynamic proxy class we will use, but returning the
     * interface we registered under is simpler, still honest, and should be good enough.
     * </p>
     *
     * @return the interface class that this adaptor was registered with.
     */
    public Class<? extends T> getComponentImplementation() {
        return interfaceClass;
    }

    protected InvocationHandler getHandler(final PicoContainer container) {
        final T enabled = container.getComponent(enabledClass);
        final T disabled = container.getComponent(disabledClass);
        return new SwitchingInvocationHandler<>(enabled, disabled, getInvocationSwitcher());
    }

    public T getComponentInstance(final PicoContainer container) throws PicoCompositionException {
        //noinspection unchecked
        return (T) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{interfaceClass}, getHandler(container));
    }

    protected abstract InvocationSwitcher getInvocationSwitcher();
}
