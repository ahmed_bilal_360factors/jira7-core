package com.atlassian.jira.help;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.fugue.Option;

import javax.annotation.Nonnull;

/**
 * Returns application help space uri retrieved from application descriptor.
 *
 * @since 7.0
 */
public class DefaultApplicationHelpSpaceProvider implements ApplicationHelpSpaceProvider {

    private final ApplicationManager applicationManager;

    public DefaultApplicationHelpSpaceProvider(final ApplicationManager applicationManager) {
        this.applicationManager = applicationManager;
    }

    @Override
    @Nonnull
    public Option<String> getHelpSpace(@Nonnull final ApplicationKey applicationKey) {
        return applicationManager.getApplication(applicationKey)
                .flatMap(app -> app.getProductHelpServerSpaceURI())
                .map(uri -> uri.toString());
    }

}
