package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.gadgets.dashboard.DashboardId;
import com.atlassian.gadgets.dashboard.spi.DashboardPermissionService;
import com.atlassian.jira.bc.portal.PortalPageService;
import com.atlassian.jira.plugin.webfragment.JiraWebInterfaceManager;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

/**
 * Decides wheter or not to show the Manage Dashboards link.  This depends on if the user is logged in and if
 * the dashboard is the default dashboard being viewed on the HOME page (rather than admin section).
 *
 * @since v4.0
 */
public class ShowDashboardToolsMenuCondition implements Condition {
    public static final String CONTEXT_KEY_DASHBOARD_ID = "dashboardId";

    private final DashboardPermissionService dashboardPermissionService;
    private final PortalPageService portalPageService;
    private final UserUtil userUtil;

    public ShowDashboardToolsMenuCondition(final DashboardPermissionService dashboardPermissionService, final PortalPageService portalPageService, final UserUtil userUtil) {
        this.dashboardPermissionService = dashboardPermissionService;
        this.portalPageService = portalPageService;
        this.userUtil = userUtil;
    }

    public void init(final Map<String, String> params) throws PluginParseException {
    }

    public boolean shouldDisplay(final Map<String, Object> context) {
        final PortalPage defaultPortalPage = portalPageService.getSystemDefaultPortalPage();
        final DashboardId dashboardId = (DashboardId) context.get(CONTEXT_KEY_DASHBOARD_ID);
        final String username = (String) context.get(JiraWebInterfaceManager.CONTEXT_KEY_USERNAME);

        //if we're viewing the default dashboard and the user has permission to edit it means we're in the admin section and
        //we should not show the Manage dashboards link.
        if (dashboardId != null
                && defaultPortalPage.getId().equals(Long.valueOf(dashboardId.value()))
                && dashboardPermissionService.isWritableBy(dashboardId, username)) {
            return false;
        }
        final ApplicationUser user = userUtil.getUserByName(username);

        //for any other dashboard if the user is logged in show the Manage link.
        return user != null;
    }

}
