package com.atlassian.jira.plugin.icon;

import com.atlassian.jira.icon.IconType;

import javax.annotation.Nonnull;
import java.util.Objects;

public class IssueTypeIconTypeDefinition implements IconTypeDefinition {
    private long defaultId = -1;

    private IssueTypeIconTypePolicy iconTypePolicy;
    private SystemIconImageProvider systemIconImageProvider;

    public IssueTypeIconTypeDefinition(IssueTypeIconTypePolicy iconTypePolicy, DefaultSystemIconImageProvider systemIconImageProvider) {
        this.iconTypePolicy = iconTypePolicy;
        this.systemIconImageProvider = systemIconImageProvider;
    }

    @Nonnull
    @Override
    public String getKey() {
        return IconType.ISSUE_TYPE_ICON_TYPE.getKey();
    }

    @Nonnull
    @Override
    public IconTypePolicy getPolicy() {
        return iconTypePolicy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IssueTypeIconTypeDefinition)) return false;
        IssueTypeIconTypeDefinition that = (IssueTypeIconTypeDefinition) o;
        return Objects.equals(getKey(), that.getKey());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getKey());
    }

    @Nonnull
    @Override
    public SystemIconImageProvider getSystemIconImageProvider() {
        return systemIconImageProvider;
    }
}
