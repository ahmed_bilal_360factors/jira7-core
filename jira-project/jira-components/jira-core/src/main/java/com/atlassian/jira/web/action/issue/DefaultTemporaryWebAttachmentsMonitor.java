package com.atlassian.jira.web.action.issue;

import com.atlassian.fugue.Option;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentMonitorStore;
import com.atlassian.jira.issue.attachment.TemporaryWebAttachment;
import com.atlassian.jira.util.ExceptionUtil;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static com.atlassian.scheduler.config.RunMode.RUN_ONCE_PER_CLUSTER;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

/**
 * This class maintains a list of temporary attachments uploaded by a user.
 * It is responsible of deleting any temporary files that were not converted to proper attachments
 * from the database.
 *
 * @since 6.4
 */
@ParametersAreNonnullByDefault
public class DefaultTemporaryWebAttachmentsMonitor implements TemporaryWebAttachmentsMonitor, Startable {

    private static final Logger log = LoggerFactory.getLogger(DefaultTemporaryWebAttachmentsMonitor.class);

    private static final JobRunnerKey JOB_RUNNER_KEY = JobRunnerKey.of(DefaultTemporaryWebAttachmentsMonitor.class.getName());
    private static final JobId JOB_ID = JobId.of(DefaultTemporaryWebAttachmentsMonitor.class.getName());
    private static final long EVERY_HOUR = TimeUnit.HOURS.toMillis(1);

    private final StreamAttachmentStore attachmentStore;
    private final TemporaryAttachmentMonitorStore temporaryAttachmentMonitorStore;
    private final SchedulerService schedulerService;

    public DefaultTemporaryWebAttachmentsMonitor(final StreamAttachmentStore attachmentStore, TemporaryAttachmentMonitorStore temporaryAttachmentMonitorStore, SchedulerService schedulerService) {
        this.attachmentStore = attachmentStore;
        this.temporaryAttachmentMonitorStore = temporaryAttachmentMonitorStore;
        this.schedulerService = schedulerService;
    }

    @Override
    public void start() throws Exception {
        scheduleCleanup();
    }

    private void scheduleCleanup() {
        schedulerService.registerJobRunner(JOB_RUNNER_KEY, periodicallyRemoveTempAttachmentRows());

        final Date oneHourFromNow = new DateTime().plusHours(1).toDate();

        final JobConfig config = JobConfig.forJobRunnerKey(JOB_RUNNER_KEY)
                .withRunMode(RUN_ONCE_PER_CLUSTER)
                .withSchedule(Schedule.forInterval(EVERY_HOUR, oneHourFromNow));
        try {
            schedulerService.scheduleJob(JOB_ID, config);
        } catch (SchedulerServiceException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * TemporaryAttachments are generally "deleted" when the attachment data is successfully attached to an issue
     * however some can be "abandoned" and hence lie around in the database forever unless we have a clean up.
     * In the old days we used HttpSession boundaries for this but in a session less world we now have a periodic
     * clean up job that runs every hour and deletes all entries older than (currently) 7 days
     * <p>
     * The number of rows will be the rate of abandonment attempts * the time window and is not expected to be very many in practice.
     *
     * @return the JobRunner to do the clean up
     */
    private JobRunner periodicallyRemoveTempAttachmentRows() {
        return new JobRunner() {
            @Override
            public JobRunnerResponse runJob(JobRunnerRequest jobRunnerRequest) {
                try {
                    //
                    // why did we choose a week?  Well we don't expect that many records
                    // so a long window is ok.  You can "sit" on create issue for a 6 days and 23 hours and
                    // still press submit.  We had to chose a number and this was it.
                    //
                    final DateTime oneWeekOld = new DateTime().minusDays(7);
                    log.debug(format("Removing all temporary attachments older than '%s'", oneWeekOld));

                    long howMany = temporaryAttachmentMonitorStore.removeOlderThan(oneWeekOld);

                    return JobRunnerResponse.success(format("Removed %d temporary attachments", howMany));
                } catch (Exception e) {
                    log.error("Unable to remove older temporary attachment rows", e);
                    return JobRunnerResponse.failed(e);
                }
            }
        };
    }


    @Override
    public Option<TemporaryWebAttachment> getById(final String temporaryAttachmentId) {
        return temporaryAttachmentMonitorStore.getById(TemporaryAttachmentId.fromString(temporaryAttachmentId));
    }

    @Override
    public Option<TemporaryWebAttachment> removeById(final String temporaryAttachmentId) {
        return temporaryAttachmentMonitorStore.removeById(TemporaryAttachmentId.fromString(temporaryAttachmentId));
    }

    @Override
    public void add(final TemporaryWebAttachment temporaryAttachment) {
        checkNotNull(temporaryAttachment, "temporaryAttachment");
        if (temporaryAttachmentMonitorStore.putIfAbsent(temporaryAttachment)) {
            throw new IllegalArgumentException(String.format("Temporary attachment with id='%s' already in monitor", temporaryAttachment.getStringId()));
        }
    }

    @Override
    public Collection<TemporaryWebAttachment> getByFormToken(final String formToken) {
        checkNotNull(formToken);
        return temporaryAttachmentMonitorStore.getByFormToken(formToken);
    }

    @Override
    public void cleanByFormToken(final String formToken) {
        checkNotNull(formToken);
        temporaryAttachmentMonitorStore.getByFormToken(formToken)
                .forEach(twa -> {
                            TemporaryAttachmentId temporaryAttachmentId = twa.getTemporaryAttachmentId();
                            safelyRemoveTemporaryAttachmentFromStore(temporaryAttachmentId);
                            temporaryAttachmentMonitorStore.removeById(temporaryAttachmentId);
                        }
                );
    }

    private void safelyRemoveTemporaryAttachmentFromStore(final TemporaryAttachmentId temporaryAttachmentId) {

        attachmentStore.deleteTemporaryAttachment(temporaryAttachmentId)
                .fail(
                        throwable -> ExceptionUtil.logExceptionWithWarn(log, "Got exception while removing temporary attachment.", throwable)
                );
        temporaryAttachmentMonitorStore.removeById(temporaryAttachmentId);
    }
}

