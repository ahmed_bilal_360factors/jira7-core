package com.atlassian.jira.model.querydsl;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Generated;

/**
 * Data Transfer Object for the FieldConfigSchemeIssueType entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 *
 * @see QFieldConfigSchemeIssueType
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class FieldConfigSchemeIssueTypeDTO implements DTO {
    private final Long id;
    private final String issuetype;
    private final Long fieldconfigscheme;
    private final Long fieldconfiguration;

    public Long getId() {
        return id;
    }

    public String getIssuetype() {
        return issuetype;
    }

    public Long getFieldconfigscheme() {
        return fieldconfigscheme;
    }

    public Long getFieldconfiguration() {
        return fieldconfiguration;
    }

    public FieldConfigSchemeIssueTypeDTO(Long id, String issuetype, Long fieldconfigscheme, Long fieldconfiguration) {
        this.id = id;
        this.issuetype = issuetype;
        this.fieldconfigscheme = fieldconfigscheme;
        this.fieldconfiguration = fieldconfiguration;
    }

    /**
     * Creates a GenericValue object from the values in this Data Transfer Object.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
     * @return a GenericValue object constructed from the values in this Data Transfer Object.
     */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator) {
        return ofBizDelegator.makeValue("FieldConfigSchemeIssueType", new FieldMap()
                .add("id", id)
                .add("issuetype", issuetype)
                .add("fieldconfigscheme", fieldconfigscheme)
                .add("fieldconfiguration", fieldconfiguration)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */
    public static FieldConfigSchemeIssueTypeDTO fromGenericValue(GenericValue gv) {
        return new FieldConfigSchemeIssueTypeDTO(
                gv.getLong("id"),
                gv.getString("issuetype"),
                gv.getLong("fieldconfigscheme"),
                gv.getLong("fieldconfiguration")
        );
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(FieldConfigSchemeIssueTypeDTO fieldConfigSchemeIssueTypeDTO) {
        return new Builder(fieldConfigSchemeIssueTypeDTO);
    }

    public static class Builder {
        private Long id;
        private String issuetype;
        private Long fieldconfigscheme;
        private Long fieldconfiguration;

        public Builder() {
        }

        public Builder(FieldConfigSchemeIssueTypeDTO fieldConfigSchemeIssueTypeDTO) {
            this.id = fieldConfigSchemeIssueTypeDTO.id;
            this.issuetype = fieldConfigSchemeIssueTypeDTO.issuetype;
            this.fieldconfigscheme = fieldConfigSchemeIssueTypeDTO.fieldconfigscheme;
            this.fieldconfiguration = fieldConfigSchemeIssueTypeDTO.fieldconfiguration;
        }

        public FieldConfigSchemeIssueTypeDTO build() {
            return new FieldConfigSchemeIssueTypeDTO(id, issuetype, fieldconfigscheme, fieldconfiguration);
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }
        public Builder issuetype(String issuetype) {
            this.issuetype = issuetype;
            return this;
        }
        public Builder fieldconfigscheme(Long fieldconfigscheme) {
            this.fieldconfigscheme = fieldconfigscheme;
            return this;
        }
        public Builder fieldconfiguration(Long fieldconfiguration) {
            this.fieldconfiguration = fieldconfiguration;
            return this;
        }
    }
}