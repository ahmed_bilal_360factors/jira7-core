package com.atlassian.jira.servermetrics;

import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Duration;

@ParametersAreNonnullByDefault
public class NoopServerMetricsDetailCollector implements ServerMetricsDetailCollector {
    @Override
    public void checkpointReached(String checkpointName) {
    }

    @Override
    public void checkpointReachedOnce(String checkpointName) {
    }

    @Override
    public void checkpointReachedOverride(String checkpointName) {
    }

    @Override
    public void addTimeSpentInActivity(String activityName, Duration duration) {
    }
}
