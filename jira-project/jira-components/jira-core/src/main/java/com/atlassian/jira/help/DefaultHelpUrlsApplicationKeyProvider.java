package com.atlassian.jira.help;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import java.util.stream.Stream;

/**
 * Provides application key based on roles enabled and installed applications available for current user.
 */
public class DefaultHelpUrlsApplicationKeyProvider implements HelpUrlsApplicationKeyProvider {
    private final JiraAuthenticationContext ctx;
    private final ApplicationRoleManager applicationRoleManager;

    public DefaultHelpUrlsApplicationKeyProvider(final JiraAuthenticationContext ctx,
                                                 final ApplicationRoleManager applicationRoleManager) {
        this.ctx = ctx;
        this.applicationRoleManager = applicationRoleManager;
    }

    @Override
    @Nonnull
    public ApplicationKey getApplicationKeyForUser() {
        return selectApplicationKeyForHelp(getApplicationKeysForUser());
    }

    private Stream<ApplicationKey> getApplicationKeysForUser() {
        final ApplicationUser loggedInUser = ctx.getLoggedInUser();
        if (loggedInUser != null) {
            return applicationRoleManager.getRolesForUser(loggedInUser).stream().filter(ApplicationRole::isDefined).map(ApplicationRole::getKey);
        } else {
            return Stream.empty();
        }
    }

    private ApplicationKey selectApplicationKeyForHelp(final Stream<ApplicationKey> availableApplications) {
        final ImmutableList<ApplicationKey> applicationsOtherThanCore =
                availableApplications.filter(e -> !ApplicationKeys.CORE.equals(e)).limit(2).collect(CollectorsUtil.toImmutableListWithCapacity(2));

        return applicationsOtherThanCore.size() == 1 ? applicationsOtherThanCore.get(0) : ApplicationKeys.CORE;
    }
}
