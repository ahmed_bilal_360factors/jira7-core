package com.atlassian.jira.instrumentation.httpsession;

import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.tenancy.TenantAware;
import com.atlassian.jira.tenancy.TenantInfo;
import com.google.common.cache.CacheBuilder;
import org.apache.log4j.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.Collections;
import java.util.Set;


/**
 * Reports on usage of Http Session attributes to allow monitoring it's removal.
 *
 * This class should not make it to a zero affinity world. Once the HttpSession usage has been removed this class
 * can be deleted. 
 *  
 * @since 7.2
 */
@TenantInfo(TenantAware.UNRESOLVED)
public class HttpSessionUsageListener  implements HttpSessionAttributeListener {

    private static final Logger log = LoggerFactory.getLogger(HttpSessionUsageListener.class);
    private static final boolean LISTENER_ENABLED = JiraSystemProperties.getInstance().getBoolean("httpsession.logger.enabled");
    private static final String SESSION_ATTRIBUTE_CLASS = "session_attribute_class";
    private static final String SESSION_KEY_NAME = "session_key_name";

    private Set<String> nameCache = Collections.newSetFromMap(
            CacheBuilder.newBuilder()
                    .maximumSize(100) // There are 68 keys in SessionKeys. 100 should give enough buffer for custom ones
                    .<String, Boolean>build().asMap());

    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        if (LISTENER_ENABLED && nameCache.add(event.getName())) {
            try {
                StringBuilder message = new StringBuilder();
                MDC.put(SESSION_KEY_NAME, event.getName());
                message.append("New http session key: ").append(event.getName());

                if (event.getValue() != null) {
                    final String clazzName = event.getValue().getClass().getName();
                    MDC.put(SESSION_ATTRIBUTE_CLASS, clazzName);
                    message.append(", class: ").append(clazzName);
                }

                log.info(message.toString());
            } finally {
                MDC.remove(SESSION_KEY_NAME);
                MDC.remove(SESSION_ATTRIBUTE_CLASS);
            }
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {
        //Nothing to do here
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent event) {
        //Not really interesting
    }
}
