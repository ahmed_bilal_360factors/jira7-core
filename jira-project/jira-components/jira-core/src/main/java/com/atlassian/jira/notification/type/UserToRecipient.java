package com.atlassian.jira.notification.type;

import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.base.Function;

/**
 * Converts {@link com.atlassian.crowd.embedded.api.User} to {@link com.atlassian.jira.notification.NotificationRecipient}.
 *
 * @since v4.4
 */
public class UserToRecipient implements Function<ApplicationUser, NotificationRecipient> {
    public static final UserToRecipient INSTANCE = new UserToRecipient();

    @Override
    public NotificationRecipient apply(final ApplicationUser user) {
        // null is checked by constructor
        return new NotificationRecipient(user);
    }
}
