package com.atlassian.jira.security.groups;

import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.QueryCallback;
import com.atlassian.jira.database.SqlPredicates;
import com.atlassian.jira.model.querydsl.QMembership;
import com.atlassian.jira.model.querydsl.QUser;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Wildcard;
import com.querydsl.sql.SQLExpressions;
import com.querydsl.sql.SQLQuery;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A query-callback that retrieves user names in a (set of) groups within a directory.
 * Usually the set of groups will be a single group or a set of nested groups.
 * All the rules of Crowd groups are followed, including directory shadowing; nested groups per directory;
 * inactive users ignored.
 */
public class GetUsersInGroupQueryCallback implements QueryCallback<List<String>> {
    /**
     * The maximum number of group names we'll allow before switching to use literals instead of parameters in SQL Server.
     * This avoids SQL Server's 2000 parameter limit per query.
     * We'll assume there are no more than 100 additional parameters beyond the list of expanded group names.
     */
    private static final int SQL_SERVER_USE_LITERALS_THRESHOLD = SqlPredicates.MAX_SQL_SERVER_PARAMETER_LIMIT - 100;

    /**
     * The directory ID
     */
    private final Long directoryId;

    private final UserIsNotShadowedPredicate userIsNotShadowedPredicate;

    /**
     * The pre-expanded set of nested group names
     */
    private final Collection<String> lowerCaseGroupNames;

    private final DatabaseConfig dbConfig;

    private final SqlPredicates sqlPredicates;

    private final int limit;

    /**
     * Constructor
     *
     * @param directoryId                the directory ID to count within
     * @param higherPriorityDirectoryIds higher-priority directory IDs, used to filter out shadowed users
     * @param groupNames                 the expanded list of nested group names
     * @param dbConfig                   database config
     */
    @SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
    public GetUsersInGroupQueryCallback(final Long directoryId, final Collection<Long> higherPriorityDirectoryIds, final Collection<String> groupNames, int limit, DatabaseConfig dbConfig) {
        this.directoryId = directoryId;
        userIsNotShadowedPredicate = new UserIsNotShadowedPredicate(higherPriorityDirectoryIds);
        lowerCaseGroupNames = groupNames.stream().map(IdentifierUtils::toLowerCase).collect(Collectors.toSet());
        this.limit = limit;
        this.dbConfig = dbConfig;
        sqlPredicates = new SqlPredicates(dbConfig);
    }

    @Override
    public List<String> runQuery(final DbConnection dbConnection) {
        QMembership m = new QMembership("m");
        QUser u = new QUser("u");

        // Predicate to only include users in their canonical directory (i.e. they are not shadowed)
        BooleanExpression userIsNotShadowed = userIsNotShadowedPredicate.apply(u);

        // Predicate to find users that are a member of one or more of the nested groups, in this directory.
        // Use exists() to ensure we count each user only once,  even if they are a member of multiple groups
        BooleanExpression userIsMemberOfGroups = SQLExpressions.select(Wildcard.all)
                .from(m).where(m.lowerChildName.eq(u.lowerUserName)
                        .and(m.membershipType.eq("GROUP_USER"))
                        .and(m.directoryId.eq(directoryId))
                        .and(sqlPredicates.partitionedIn(m.lowerParentName, lowerCaseGroupNames)))
                .exists();

        // Query to get active users who are not shadowed, and are a member of at least one of these groups
        SQLQuery<String> query = dbConnection.newSqlQuery()
                .select(u.userName)
                .from(u).where(u.active.eq(1).and(u.directoryId.eq(directoryId))
                        .and(userIsNotShadowed)
                        .and(userIsMemberOfGroups))
                .orderBy(u.lowerUserName.asc())
                .limit(limit);


        // SQL Server has a limit of 2000 parameters per query. If we hit that, use literals instead
        if (dbConfig.isSqlServer() && lowerCaseGroupNames.size() > SQL_SERVER_USE_LITERALS_THRESHOLD) {
            query.setUseLiterals(true);
        }

        return query.fetch();
    }
}
