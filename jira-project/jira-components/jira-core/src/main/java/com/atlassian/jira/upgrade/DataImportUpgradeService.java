package com.atlassian.jira.upgrade;

import com.atlassian.jira.index.request.ReindexRequestTypes;

import static com.atlassian.upgrade.api.UpgradeContext.UpgradeTrigger.IMPORT;

/**
 * Upgrade service for data import process. It runs upgrades without triggering Lucene indexing.
 */
public class DataImportUpgradeService implements UpgradeService {

    private final LoggingUpgradeService loggingUpgradeService;

    public DataImportUpgradeService(final LoggingUpgradeService loggingUpgradeService) {
        this.loggingUpgradeService = loggingUpgradeService;
    }

    @Override
    public UpgradeResult runUpgrades() {
        return loggingUpgradeService.runUpgradesWithLogging(
                ReindexRequestTypes.noneAllowed(),
                () -> IMPORT,
                "run upgrades for data import"
        );
    }

    @Override
    public boolean areUpgradesRunning() {
        return loggingUpgradeService.areUpgradesRunning();
    }
}
