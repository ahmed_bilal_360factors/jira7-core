package com.atlassian.jira.mail;

import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.subscription.FilterSubscription;
import com.atlassian.jira.mail.util.MailAttachmentsManagerFactory;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.template.TemplateManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.mail.queue.MailQueue;

public class DefaultSubscriptionMailQueueItemFactory implements SubscriptionMailQueueItemFactory {
    private final MailingListCompiler mailingListCompiler;
    private final TemplateManager templateManager;
    private final UserManager userManager;
    private final GroupManager groupManager;
    private final MailAttachmentsManagerFactory mailAttachmentsManagerFactory;
    private final DateTimeFormatterFactory dateTimeFormatterFactory;
    private final MailQueue mailQueue;

    public DefaultSubscriptionMailQueueItemFactory(
            final MailingListCompiler mailingListCompiler,
            final TemplateManager templateManager,
            final UserManager userManager,
            final GroupManager groupManager,
            final MailAttachmentsManagerFactory mailAttachmentsManagerFactory,
            final DateTimeFormatterFactory dateTimeFormatterFactory,
            final MailQueue mailQueue
    ) {
        this.mailingListCompiler = mailingListCompiler;
        this.templateManager = templateManager;
        this.userManager = userManager;
        this.groupManager = groupManager;
        this.mailAttachmentsManagerFactory = mailAttachmentsManagerFactory;
        this.dateTimeFormatterFactory = dateTimeFormatterFactory;
        this.mailQueue = mailQueue;
    }

    @Override
    public SubscriptionMailQueueItem getSubscriptionMailQueueItem(final FilterSubscription sub) {
        return new SubscriptionMailQueueItem(
                sub,
                userManager,
                groupManager,
                mailQueue,
                this);
    }

    @Override
    public SubscriptionSingleRecepientMailQueueItem createSelfEvaluatingEmailQueueItem(final FilterSubscription sub,
                                                                                       final ApplicationUser recipient) {
        return new SubscriptionSingleRecepientMailQueueItem(
                sub,
                mailingListCompiler,
                templateManager,
                userManager,
                mailAttachmentsManagerFactory.createAttachmentsManager(),
                dateTimeFormatterFactory,
                recipient);
    }
}
