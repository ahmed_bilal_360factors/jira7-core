package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Objects;
import java.util.Set;

import static com.atlassian.jira.util.dbc.Assertions.containsNoNulls;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.collect.ImmutableSet.copyOf;

/**
 * Store for storing and retrieving {@link ApplicationRoleStore.ApplicationRoleData}.
 *
 * @since v7.0
 */
public interface ApplicationRoleStore {
    /**
     * Get the {@link ApplicationRoleStore.ApplicationRoleData} for the passed
     * {@link com.atlassian.application.api.ApplicationKey}.
     *
     * @param key the key to search for.
     * @return The {@link ApplicationRoleStore.ApplicationRoleData} for the passed
     * {@link com.atlassian.application.api.ApplicationKey}.
     */
    @Nonnull
    ApplicationRoleData get(@Nonnull ApplicationKey key);

    /**
     * Save the passed {@link ApplicationRoleStore.ApplicationRoleData} to the database.
     *
     * @param data the data to save to the database.
     * @return the {@link ApplicationRoleStore.ApplicationRoleData} as saved in the database.
     */
    @Nonnull
    ApplicationRoleData save(@Nonnull ApplicationRoleData data);

    /**
     * Removes the association of the given group from all application roles in the database.
     * NOTE: Current implementation makes no effort to reassign the default group. This means that that you can be
     * left with a role that has no default groups.
     *
     * @param groupName the name of the group to remove.
     */
    void removeGroup(@Nonnull String groupName);

    /**
     * Remove the {@link ApplicationRoleStore.ApplicationRoleData} associated with provided application key.
     *
     * @param key the key that represents the {@link ApplicationRoleStore.ApplicationRoleData} to be
     *            removed.
     */
    void removeByKey(@Nonnull ApplicationKey key);

    @Immutable
    final class ApplicationRoleData {
        private final Set<String> groups;
        private final Set<String> defaultGroups;
        private final ApplicationKey key;
        private final boolean selectedByDefault;

        public ApplicationRoleData(final ApplicationKey key, final Iterable<String> groups,
                                   Iterable<String> defaultGroups, final boolean selectedByDefault) {
            this.key = notNull("id", key);
            this.groups = copyOf(containsNoNulls("groups", groups));
            this.defaultGroups = copyOf(containsNoNulls("defaultGroups", defaultGroups));
            this.selectedByDefault = selectedByDefault;
        }

        public Set<String> getGroups() {
            return groups;
        }

        public Set<String> getDefaultGroups() {
            return defaultGroups;
        }

        public ApplicationKey getKey() {
            return key;
        }

        public boolean isSelectedByDefault() {
            return selectedByDefault;
        }

        @Override
        public boolean equals(@Nullable final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            final ApplicationRoleData that = (ApplicationRoleData) o;
            return Objects.equals(selectedByDefault, that.selectedByDefault) &&
                    Objects.equals(groups, that.groups) &&
                    Objects.equals(defaultGroups, that.defaultGroups) &&
                    Objects.equals(key, that.key);
        }

        @Override
        public int hashCode() {
            return Objects.hash(groups, defaultGroups, key, selectedByDefault);
        }
    }
}
