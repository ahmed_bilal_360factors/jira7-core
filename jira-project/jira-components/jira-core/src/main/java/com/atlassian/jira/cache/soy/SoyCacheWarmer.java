package com.atlassian.jira.cache.soy;

import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.warmer.JiraWarmer;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.io.output.NullWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Set;

/**
 * Warms up the soy template cache when JIRA starts.
 */
public class SoyCacheWarmer implements JiraWarmer {

    private static final Logger log = LoggerFactory.getLogger(SoyCacheWarmer.class);

    private static final Set<String> modulesToWarm = ImmutableSet.of(
            "com.atlassian.jira.gadgets:introduction-dashboard-item-resource"
            , "com.atlassian.jira.gadgets:login-dashboard-item-resources"
            , "com.atlassian.plugins.atlassian-nav-links-plugin:rotp-menu"
            , "com.atlassian.auiplugin:aui-experimental-soy-templates"
            , "jira.webresources:soy-templates"
            , "jira.webresources:field-templates"
            , "com.atlassian.jira.jira-projects-plugin:sidebar-navigation-soy"
            , "com.atlassian.jira.jira-projects-plugin:sidebar-project-shortcuts"
            , "com.atlassian.jira.jira-projects-plugin:sidebar-content-soy"
            , "com.atlassian.jira.jira-projects-plugin:sidebar-header-soy"
            , "com.atlassian.jira.jira-projects-plugin:project-page-soy"
            , "jira.user.format:soy"
            , "jira.webresources:browseprojects"
            , "com.atlassian.jira.plugins.jira-importers-github-plugin:templates"
            , "com.atlassian.jira.plugins.jira-importers-redmine-plugin:templates"
            , "com.atlassian.jira.plugins.jira-importers-asana-plugin:templates"
            , "com.atlassian.jira.plugins.jira-importers-trello-plugin:soy-templates"
            , "com.atlassian.jira.plugins.jira-importers-plugin:templates"
            , "jira.webpanels:soy-templates"
            , "com.atlassian.jira.jira-admin-navigation-plugin:admin-header-new-nav-soy"
            , "com.atlassian.jira.jira-header-plugin:admin-side-nav-menu"
            , "com.atlassian.jira.jira-issue-nav-plugin:issuenav-common"
            , "jira.webresources:action-soy-templates"
            , "com.atlassian.upm.upm-application-plugin:soy-templates"
            , "com.atlassian.jira.jira-projects-plugin:project-subpages-soy"
            , "jira.webresources:issue-statuses"
            , "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:metadata-webpanel-soy"
            , "com.atlassian.jira.jira-view-issue-plugin:soy-templates"
            , "com.atlassian.jira.jira-issue-nav-plugin:viewissue"
            , "com.atlassian.web.atlassian-servlet-plugin:location-replacer"
            , "com.atlassian.gadgets.dashboard:server-side-soy-templates"
            , "com.atlassian.jira.jira-onboarding-assets-plugin:templates"
            , "com.atlassian.jira.plugins.jira-development-integration-plugin:devstatus-panel-soy-templates"
            , "com.atlassian.jira.plugins.jira-development-integration-plugin:devstatus-panel-soy-templates-branches"
            , "com.atlassian.jira.plugins.jira-development-integration-plugin:devstatus-panel-soy-templates-commits"
            , "com.atlassian.jira.plugins.jira-development-integration-plugin:devstatus-panel-soy-templates-pullrequests"
            , "com.atlassian.jira.plugins.jira-development-integration-plugin:devstatus-panel-soy-templates-reviews"
            , "com.atlassian.jira.plugins.jira-development-integration-plugin:devstatus-panel-soy-templates-builds"
            , "com.atlassian.jira.plugins.jira-development-integration-plugin:devstatus-panel-soy-templates-deployments"
            , "com.atlassian.jira.jira-projects-issue-navigator:issueview-templates"
            , "jira.webresources:jira-errors"
            );

    private final SoyTemplateRendererProvider soyTemplateRendererProvider;

    private final PluginAccessor pluginAccessor;

    public SoyCacheWarmer(final SoyTemplateRendererProvider soyTemplateRendererProvider,
                          final PluginAccessor pluginAccessor) {
        this.soyTemplateRendererProvider = soyTemplateRendererProvider;
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    public void run() {
        final long warmStartTime = System.currentTimeMillis();
        log.info("Warming {} soy modules", modulesToWarm.size());
        ResultAggregation finalResult = modulesToWarm
                .parallelStream()
                .map(this::compileSoy)
                .collect(ResultAggregation::new, ResultAggregation::accept, ResultAggregation::combine);
        final long warmCompleteTime = System.currentTimeMillis() - warmStartTime;
        log.info("Warmed {} soy module(s) in {} ms, {} module(s) were not present or disabled, {} failed.",
                finalResult.numWarmed, warmCompleteTime, finalResult.numAbsentOrDisabled, finalResult.numErrors);
    }

    /**
     * Outcome of compiling a single module key
     */
    private enum CompileResult {
        ABSENT_OR_DISABLED,
        ERROR,
        SUCCESS
    }

    private CompileResult compileSoy(String completeModuleKey) {
        log.debug("compileSoy {}", completeModuleKey);
        CompileResult result;
        if (pluginAccessor.getEnabledPluginModule(completeModuleKey) == null) {
            log.debug("Plugin module {} is absent or disabled", completeModuleKey);
            result = CompileResult.ABSENT_OR_DISABLED;
        } else {
            final long startTime = System.currentTimeMillis();
            boolean compileSucceeded;
            try {
                // Call the soy renderer to force the atlassian-soy-templates to compile and cache soy templates
                soyTemplateRendererProvider.getRenderer().render(new NullWriter(), completeModuleKey, "", Collections.emptyMap());
                result = CompileResult.SUCCESS;
            } catch (final Exception e) {
                if (e.getClass().getName().equals("com.google.template.soy.tofu.SoyTofuException")
                        && e.getMessage().equals("Attempting to render undefined template ''.")) {
                    // following soy file compilation the renderer will throw an exception because it can't
                    // find the "" template, so we catch and discard it
                    result = CompileResult.SUCCESS;
                } else {
                    log.error("Caught exception while compiling soy template", e);
                    result = CompileResult.ERROR;
                }
            }
            if (result == CompileResult.SUCCESS) {
                final long compileTime = System.currentTimeMillis() - startTime;
                log.debug("Compiled {} in {} ms", completeModuleKey, compileTime);
            }
        }
        log.debug("compileSoy {} done, result {}", completeModuleKey, result);
        return result;
    }

    /**
     * This class is used to combine all the compilation results.
     */
    private class ResultAggregation {
        int numAbsentOrDisabled;
        int numErrors;
        int numWarmed;

        public void accept(CompileResult resultKey) {
            switch(resultKey) {
                case ABSENT_OR_DISABLED:
                    numAbsentOrDisabled++;
                    break;
                case ERROR:
                    numErrors++;
                    break;
                case SUCCESS:
                    numWarmed++;
                    break;
                default:
                    throw new IllegalArgumentException("unexpected result key " + resultKey);
            }
        }

        public void combine(ResultAggregation resultAggregation) {
            this.numAbsentOrDisabled += resultAggregation.numAbsentOrDisabled;
            this.numErrors += resultAggregation.numErrors;
            this.numWarmed += resultAggregation.numWarmed;
        }
    }


}
