package com.atlassian.jira.database;

import com.atlassian.jira.util.Consumer;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.util.function.Function;

/**
 * @since v7.0.6
 */
public interface ConnectionProvider {
    /**
     * Executes SQL statements as defined in the callback function and returns the results.
     * <p>
     * This method is mostly useful for running SELECT statements and returning the given results in some form.
     * Executes SQL statements as defined in the callback function and manages transaction semantics.
     * <p>
     * Transaction semantics for this method will depend on how you aquired the ConnectionProvider - see appropriate
     * javadoc there eg {@link QueryDslAccessor#withNewConnection()}.
     *
     * @param callback the callback function that runs the query
     * @param <T>      type of results
     * @return results of the callback function
     * @since 7.0.6
     */
    <T> T executeQuery(@Nonnull QueryCallback<T> callback);

    /**
     * Executes SQL statements as defined in the callback function.
     * <p>
     * This method does not return results and is mostly useful for running INSERT, UPDATE, and DELETE operations.
     * <p>
     * Transaction semantics for this method will depend on how you aquired the ConnectionProvider - see appropriate
     * javadoc there eg {@link QueryDslAccessor#withNewConnection()}.
     *
     * @param callback the callback function that runs the query
     * @since 7.0.6
     */
    void execute(@Nonnull SqlCallback callback);
}
