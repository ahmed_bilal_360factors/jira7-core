package com.atlassian.jira.license;

import com.atlassian.annotations.Internal;
import com.atlassian.extras.api.Contact;
import com.atlassian.extras.api.LicenseEdition;
import com.atlassian.extras.api.LicenseType;
import com.atlassian.extras.api.Organisation;
import com.atlassian.extras.api.Partner;
import com.atlassian.extras.api.Product;
import com.atlassian.extras.api.jira.JiraLicense;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Date;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * JIRA's interpreted version of an extras {@link com.atlassian.extras.api.jira.JiraLicense}.
 * <p>
 * This is a wrapper class over {@link com.atlassian.extras.api.jira.JiraLicense} that delegates calls to the
 * underlying {@link com.atlassian.extras.api.jira.JiraLicense} instance.
 * </p>
 * <p>
 * This class should never/exposed be used outside of jira core module.
 * </p>
 *
 * @since v7.0
 */
@Internal
public final class JiraProductLicense implements JiraLicense {
    private final JiraLicense jiraLicense;
    private final LicensedApplications licensedApplications;

    JiraProductLicense(final LicensedApplications licensedApplications, final JiraLicense jiraLicense) {
        this.jiraLicense = notNull("jiraLicense", jiraLicense);
        this.licensedApplications = notNull("licensedApplications", licensedApplications);
    }

    @Nonnull
    public LicensedApplications getApplications() {
        return licensedApplications;
    }

    @Override
    public int getLicenseVersion() {
        return jiraLicense.getLicenseVersion();
    }

    @Override
    public String getDescription() {
        return jiraLicense.getDescription();
    }

    @Override
    public Product getProduct() {
        return jiraLicense.getProduct();
    }

    @Override
    public Iterable<Product> getProducts() {
        return jiraLicense.getProducts();
    }

    @Override
    public String getServerId() {
        return jiraLicense.getServerId();
    }

    @Override
    public Partner getPartner() {
        return jiraLicense.getPartner();
    }

    @Override
    public Organisation getOrganisation() {
        return jiraLicense.getOrganisation();
    }

    @Override
    public Collection<Contact> getContacts() {
        return jiraLicense.getContacts();
    }

    @Override
    public Date getCreationDate() {
        return jiraLicense.getCreationDate();
    }

    @Override
    public Date getPurchaseDate() {
        return jiraLicense.getPurchaseDate();
    }

    @Override
    public Date getExpiryDate() {
        return jiraLicense.getExpiryDate();
    }

    @Override
    public int getNumberOfDaysBeforeExpiry() {
        return jiraLicense.getNumberOfDaysBeforeExpiry();
    }

    @Override
    public boolean isExpired() {
        return jiraLicense.isExpired();
    }

    @Override
    public Date getGracePeriodEndDate() {
        return jiraLicense.getGracePeriodEndDate();
    }

    @Override
    public int getNumberOfDaysBeforeGracePeriodExpiry() {
        return jiraLicense.getNumberOfDaysBeforeGracePeriodExpiry();
    }

    @Override
    public boolean isWithinGracePeriod() {
        return jiraLicense.isWithinGracePeriod();
    }

    @Override
    public boolean isGracePeriodExpired() {
        return jiraLicense.isGracePeriodExpired();
    }

    @Override
    public String getSupportEntitlementNumber() {
        return jiraLicense.getSupportEntitlementNumber();
    }

    @Override
    public Date getMaintenanceExpiryDate() {
        return jiraLicense.getMaintenanceExpiryDate();
    }

    @Override
    public int getNumberOfDaysBeforeMaintenanceExpiry() {
        return jiraLicense.getNumberOfDaysBeforeMaintenanceExpiry();
    }

    @Override
    public boolean isMaintenanceExpired() {
        return jiraLicense.isMaintenanceExpired();
    }

    @Override
    public int getMaximumNumberOfUsers() {
        return jiraLicense.getMaximumNumberOfUsers();
    }

    @Override
    public boolean isUnlimitedNumberOfUsers() {
        return jiraLicense.isUnlimitedNumberOfUsers();
    }

    @Override
    public boolean isEvaluation() {
        return jiraLicense.isEvaluation();
    }

    @Override
    public boolean isSubscription() {
        return jiraLicense.isSubscription();
    }

    @Override
    public boolean isClusteringEnabled() {
        return jiraLicense.isClusteringEnabled();
    }

    @Override
    public LicenseType getLicenseType() {
        return jiraLicense.getLicenseType();
    }

    @Override
    public String getProperty(String s) {
        return jiraLicense.getProperty(s);
    }

    @Override
    public LicenseEdition getLicenseEdition() {
        return jiraLicense.getLicenseEdition();
    }
}
