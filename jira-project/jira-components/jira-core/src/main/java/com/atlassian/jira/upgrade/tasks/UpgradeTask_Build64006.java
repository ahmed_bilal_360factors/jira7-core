package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.index.request.ReindexRequestType;

/**
 * Indexes worklogs in their own index so they are JQL searchable by date or author.
 * Update is performed only if the time tracking option is enabled and there are any worklogs in the instance.
 */
public class UpgradeTask_Build64006 extends AbstractReindexUpgradeTask {
    @Override
    public void doUpgrade(final boolean setupMode) throws Exception {
        getReindexRequestService().requestReindex(ReindexRequestType.DELAYED, WORKLOG_ONLY, NO_SHARED_ENTITIES);
    }

    @Override
    public int getBuildNumber() {
        return 64006;
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        // Flagged as not downgradable because it was created before the Downgrade Task framework was built.
        return true;
    }

    @Override
    public String getShortDescription() {
        return "Indexes worklogs in their own index so they are JQL searchable.";
    }
}
