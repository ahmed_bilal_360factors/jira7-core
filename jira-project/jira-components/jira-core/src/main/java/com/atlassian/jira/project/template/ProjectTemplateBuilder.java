package com.atlassian.jira.project.template;

import com.atlassian.jira.project.template.hook.AddProjectModule;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class ProjectTemplateBuilder {
    static final String PT_DEFAULT_IMG_NAME = "project-template-default-icon.png";
    static final String PT_DEFAULT_BG_IMG_NAME = "default-template-background-image.png";

    private final WebResourceUrlProvider webResourceUrlProvider;

    private String key;
    private Integer weight;
    private String name;
    private String description;
    private String longDescriptionContent;
    private String iconUrl;
    private String backgroundIconUrl;
    private AddProjectModule addProjectModule;
    private String infoSoyPath;
    private String projectTypeKey;

    ProjectTemplateBuilder(WebResourceUrlProvider webResourceUrlProvider) {
        this.webResourceUrlProvider = webResourceUrlProvider;
    }

    public ProjectTemplateBuilder key(String key) {
        this.key = key;
        return this;
    }

    public ProjectTemplateBuilder weight(Integer weight) {
        this.weight = weight;
        return this;
    }

    public ProjectTemplateBuilder name(String name) {
        this.name = name;
        return this;
    }

    public ProjectTemplateBuilder description(String description) {
        this.description = description;
        return this;
    }

    public ProjectTemplateBuilder longDescriptionContent(String longDescriptionContent) {
        this.longDescriptionContent = longDescriptionContent;
        return this;
    }

    public ProjectTemplateBuilder iconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
        return this;
    }

    public ProjectTemplateBuilder backgroundIconUrl(String backgroundIconUrl) {
        this.backgroundIconUrl = backgroundIconUrl;
        return this;
    }

    public ProjectTemplateBuilder addProjectModule(AddProjectModule addProjectModule) {
        this.addProjectModule = addProjectModule;
        return this;
    }

    public ProjectTemplateBuilder infoSoyPath(String infoSoyPath) {
        this.infoSoyPath = infoSoyPath;
        return this;
    }

    public ProjectTemplateBuilder projectTypeKey(String projectTypeKey) {
        this.projectTypeKey = projectTypeKey;
        return this;
    }

    public ProjectTemplate build() {
        return new ProjectTemplate(
                new ProjectTemplateKey(key),
                new ProjectTypeKey(projectTypeKey),
                weight,
                name,
                description,
                longDescriptionContent,
                defaultIconUrl(iconUrl, PT_DEFAULT_IMG_NAME),
                defaultIconUrl(backgroundIconUrl, PT_DEFAULT_BG_IMG_NAME),
                addProjectModule,
                infoSoyPath
        );
    }

    private String defaultIconUrl(String iconUrl, String defaultLocation) {
        if (isBlank(iconUrl)) {
            UrlBuilder urlBuilder = new UrlBuilder(webResourceUrlProvider.getBaseUrl(UrlMode.ABSOLUTE), false)
                    .addPath("images")
                    .addPath("project-templates")
                    .addPath(defaultLocation);
            return urlBuilder.asUrlString();
        }

        return iconUrl;
    }
}
