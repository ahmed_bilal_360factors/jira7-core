package com.atlassian.jira.issue.fields.rest;

import com.atlassian.jira.issue.fields.rest.json.SuggestionBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.SuggestionGroupBean;
import com.atlassian.jira.permission.LegacyProjectPermissionKeyMapping;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableList;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static com.google.common.collect.Lists.newArrayListWithCapacity;
import static java.util.Optional.empty;
import static java.util.stream.StreamSupport.stream;

public class ProjectSuggestionProviderImpl implements ProjectSuggestionProvider {
    private static final int MAX_RECENT_PROJECTS = 6;
    private final SuggestionBeanFactory suggestionBeanFactory;
    private final JiraAuthenticationContext authenticationContext;
    private final UserProjectHistoryManager projectHistoryManager;
    private final PermissionManager permissionManager;
    private final ProjectManager projectManager;

    public ProjectSuggestionProviderImpl(
            SuggestionBeanFactory suggestionBeanFactory,
            JiraAuthenticationContext authenticationContext,
            UserProjectHistoryManager projectHistoryManager,
            PermissionManager permissionManager,
            ProjectManager projectManager
    ) {
        this.suggestionBeanFactory = suggestionBeanFactory;
        this.authenticationContext = authenticationContext;
        this.projectHistoryManager = projectHistoryManager;
        this.permissionManager = permissionManager;
        this.projectManager = projectManager;
    }

    @Override
    public List<SuggestionGroupBean> getProjectPickerSuggestions(final Optional<Long> selectedProject, final boolean includeRecent) {
        return getSuggestionGroupBeans(empty(), selectedProject, includeRecent);
    }

    @Override
    public List<SuggestionGroupBean> getProjectPickerSuggestions(final ProjectPermissionKey permission, final Optional<Long> selectedProject, boolean includeRecent) {
        return getSuggestionGroupBeans(Optional.of(permission), selectedProject, includeRecent);
    }

    private List<SuggestionGroupBean> getSuggestionGroupBeans(final Optional<ProjectPermissionKey> permission, final Optional<Long> selectedProject, final boolean includeRecent) {
        final ApplicationUser user = authenticationContext.getLoggedInUser();
        final I18nHelper i18n = authenticationContext.getI18nHelper();
        final List<SuggestionGroupBean> suggestions = newArrayListWithCapacity(2);
        if (includeRecent) {
            Collection<Project> recentProjects = getRecentProjects(user, permission);

            suggestions.add(new SuggestionGroupBean(
                    i18n.getText("menu.project.recent"),
                    suggestionBeanFactory.projectSuggestions(recentProjects, selectedProject)
            ));
        }
        suggestions.add(new SuggestionGroupBean(
                i18n.getText("menu.project.all"),
                suggestionBeanFactory.projectSuggestions(getAllowedProjects(user, permission), selectedProject)
        ));
        return suggestions;
    }

    private List<Project> getAllowedProjects(final ApplicationUser user, final Optional<ProjectPermissionKey> permission) {
        return permission
                .<List<Project>>map(projectPermission -> ImmutableList.copyOf(permissionManager.getProjects(projectPermission, user)))
                .orElse(projectManager.getProjectObjects());
    }

    private List<Project> getRecentProjects(final ApplicationUser user, final Optional<ProjectPermissionKey> permission) {
        final List<Project> recentProjects = permission
                .map(projectPermission -> projectHistoryManager.getProjectHistoryWithPermissionChecks(toLegacyId(projectPermission), user))
                .orElse(this.getRecentProjects(user));
        return recentProjects.subList(0, Math.min(MAX_RECENT_PROJECTS, recentProjects.size()));
    }

    private List<Project> getRecentProjects(final ApplicationUser user) {
        return stream(projectHistoryManager.getProjectHistoryWithoutPermissionChecks(user).spliterator(), false)
                .map(userHistoryItem -> projectManager.getProjectObj(Long.valueOf(userHistoryItem.getEntityId())))
                .filter(project -> project != null)
                .collect(toImmutableList());
    }

    private Integer toLegacyId(final ProjectPermissionKey permission) {
        return LegacyProjectPermissionKeyMapping.getId(permission);
    }
}
