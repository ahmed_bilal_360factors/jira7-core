package com.atlassian.jira.cluster.zdu.analytics;

import com.atlassian.analytics.api.annotations.EventName;

/**
 * @since v7.3
 */
@EventName("zdu.upgrade-state.cancel")
public class UpgradeCancelledAnalytics {
    private final Integer nodeCount;
    private final Long nodeBuildNumber;
    private final Integer databaseBuildNumber;

    public UpgradeCancelledAnalytics(Integer nodeCount, Long nodeBuildNumber, Integer databaseBuildNumber) {
        this.nodeCount = nodeCount;
        this.nodeBuildNumber = nodeBuildNumber;
        this.databaseBuildNumber = databaseBuildNumber;
    }

    public Integer getNodeCount() {
        return nodeCount;
    }

    public Long getNodeBuildNumber() {
        return nodeBuildNumber;
    }

    public Integer getDatabaseBuildNumber() {
        return databaseBuildNumber;
    }
}
