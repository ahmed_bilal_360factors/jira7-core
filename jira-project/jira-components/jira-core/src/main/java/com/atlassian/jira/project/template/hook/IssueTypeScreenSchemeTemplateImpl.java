package com.atlassian.jira.project.template.hook;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.nullToEmpty;
import static java.util.Collections.unmodifiableList;

public class IssueTypeScreenSchemeTemplateImpl implements IssueTypeScreenSchemeTemplate {
    private final String name;
    private final String description;
    private final String defaultScreenScheme;
    private final List<? extends ScreenTemplate> screenTemplates;
    private final List<? extends ScreenSchemeTemplate> screenSchemeTemplates;

    public IssueTypeScreenSchemeTemplateImpl(
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("default-screen-scheme") String defaultScreenScheme,
            @JsonProperty("screens") List<ScreenTemplateImpl> screenTemplates,
            @JsonProperty("screen-schemes") List<ScreenSchemeTemplateImpl> screenSchemeTemplates) {
        this.name = checkNotNull(name);
        this.description = nullToEmpty(description);
        this.defaultScreenScheme = checkNotNull(defaultScreenScheme).toUpperCase();
        this.screenTemplates = checkNotNull(screenTemplates);
        this.screenSchemeTemplates = checkNotNull(screenSchemeTemplates);

        validate();
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String description() {
        return description;
    }

    @Override
    public String defaultScreenScheme() {
        return defaultScreenScheme;
    }

    @Override
    public List<ScreenTemplate> screenTemplates() {
        return unmodifiableList(screenTemplates);
    }

    @Override
    public List<ScreenSchemeTemplate> screenSchemeTemplates() {
        return unmodifiableList(screenSchemeTemplates);
    }

    @Override
    public boolean hasScreenScheme(String screenSchemeKey) {
        for (ScreenSchemeTemplate screenSchemeTemplate : screenSchemeTemplates) {
            if (screenSchemeTemplate.key().equalsIgnoreCase(screenSchemeKey)) {
                return true;
            }
        }
        return false;
    }

    private boolean hasScreen(String screenKey) {
        for (ScreenTemplate screenTemplate : screenTemplates) {
            if (screenTemplate.key().equalsIgnoreCase(screenKey)) {
                return true;
            }
        }
        return false;
    }

    private void validate() {
        validateDefaultScreenScheme();
        validateScreens();
    }

    private void validateDefaultScreenScheme() {
        if (!hasScreenScheme(defaultScreenScheme)) {
            throw new IllegalArgumentException("Default screen scheme '" + defaultScreenScheme +
                    "' of issue type screen scheme '" + name + "' does not exist.");
        }
    }

    private void validateScreens() {
        for (ScreenSchemeTemplate screenSchemeTemplate : screenSchemeTemplates) {
            if (!hasScreen(screenSchemeTemplate.defaultScreen())) {
                throw new IllegalArgumentException("Default screen '" + screenSchemeTemplate.defaultScreen() +
                        "' of screen scheme '" + screenSchemeTemplate.name() + "' does not exist.");
            }

            if (screenSchemeTemplate.createScreen().isPresent() && !hasScreen(screenSchemeTemplate.createScreen().get())) {
                throw new IllegalArgumentException("Create screen '" + screenSchemeTemplate.createScreen().get() +
                        "' of screen scheme '" + screenSchemeTemplate.name() + "' does not exist.");
            }

            if (screenSchemeTemplate.editScreen().isPresent() && !hasScreen(screenSchemeTemplate.editScreen().get())) {
                throw new IllegalArgumentException("Edit screen '" + screenSchemeTemplate.editScreen().get() +
                        "' of screen scheme '" + screenSchemeTemplate.name() + "' does not exist.");
            }

            if (screenSchemeTemplate.viewScreen().isPresent() && !hasScreen(screenSchemeTemplate.viewScreen().get())) {
                throw new IllegalArgumentException("View screen '" + screenSchemeTemplate.viewScreen().get() +
                        "' of screen scheme '" + screenSchemeTemplate.name() + "' does not exist.");
            }
        }
    }
}
