package com.atlassian.jira.crowd.embedded.ofbiz.db;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Provides a simpler way of executing code within an OfBiz transaction.
 * <p>
 * To use:
 * <ul>
 * <li>Use {@link #withTransaction(Consumer)} or {@link #withTransaction(Function)} depending on whether a return value is needed</li>
 * <li>To roll back the transaction, use {@link OfBizTransaction#rollback()} inside the function passed in</li>
 * <li>The transaction is committed if the passed in function completes successfully</li>
 * <li>The transaction is rolled back if an error occurs</li>
 * </ul>
 * <p>
 * Simple usage example:
 * <pre>
 * transactionManager.withTransaction(t ->
 * {
 *     //... do database code inside transaction
 * });
 * </pre>
 * You can also return a value and rollback in case of business logic errors:
 * <pre>
 * int numUpdates = OfBizTransaction.withTransaction(t ->
 * {
 *     //... database code
 * });
 * </pre>
 * Or if business logic fails, the transaction can be rolled back:
 * <pre>
 * transactionManager.withTransaction(t ->
 * {
 *    //.. do some database code
 *    if (!businessLogicIsOk())
 *        t.rollback();
 * });
 * </pre>
 *
 * @since v7.0
 */
public interface OfBizTransactionManager {
    <R> R withTransaction(Function<? super OfBizTransaction, R> transactionUsingFunction);

    void withTransaction(Consumer<? super OfBizTransaction> transactionUsingFunction);
}
