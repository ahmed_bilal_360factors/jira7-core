package com.atlassian.jira.startup;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.service.services.analytics.JiraStartStopAnalyticHelper;
import com.atlassian.jira.service.services.analytics.start.JiraStartAnalyticEvent;
import com.atlassian.jira.service.services.analytics.stop.JiraStopAnalyticEvent;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Launcher responsible for generating start and stop analytic events
 *
 * @since v6.4
 */
public class AnalyticsLauncher implements JiraLauncher {
    private static final Logger log = LoggerFactory.getLogger(AnalyticsLauncher.class);

    private final JiraStartStopAnalyticHelper jiraAnalyticHelper;

    public AnalyticsLauncher() {
        jiraAnalyticHelper = new JiraStartStopAnalyticHelper();
    }

    @Override
    public void start() {
        if (isJiraSetup()) {
            runStartAnalyticsJob();
        }
    }

    @Override
    public void stop() {
        if (isJiraSetup()) {
            getEventPublisher().publish(new JiraStopAnalyticEvent(jiraAnalyticHelper.getOnStopUsageStats()));
        }
    }

    public EventPublisher getEventPublisher() {
        return ComponentAccessor.getComponentOfType(EventPublisher.class);
    }

    private boolean isJiraSetup() {
        final ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();
        return "true".equals(applicationProperties.getString(APKeys.JIRA_SETUP));
    }

    private void runStartAnalyticsJob() {
        final JobRunnerKey runnerKey = JobRunnerKey.of("com.atlassian.jira.startup.AnalyticsLauncher.Start");
        SchedulerService schedulerService = ComponentAccessor.getComponent(SchedulerService.class);

        if (schedulerService != null) {
            schedulerService.registerJobRunner(runnerKey, r -> {
                        getEventPublisher().publish(new JiraStartAnalyticEvent(jiraAnalyticHelper.getOnStartUsageStats(false)));
                        return JobRunnerResponse.success();
                    }
            );

            try {
                schedulerService.scheduleJobWithGeneratedId(JobConfig
                        .forJobRunnerKey(runnerKey)
                        .withSchedule(Schedule.runOnce(new Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(3)))));
            } catch (SchedulerServiceException e) {
                log.error("Start analytics not scheduled", e);
            }
        }
    }

}
