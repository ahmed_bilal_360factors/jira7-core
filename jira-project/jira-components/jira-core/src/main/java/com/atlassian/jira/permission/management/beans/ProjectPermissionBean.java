package com.atlassian.jira.permission.management.beans;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.List;

@JsonAutoDetect
public class ProjectPermissionBean {
    private String permissionKey;
    private String permissionName;
    private String permissionDesc;
    private List<SecurityTypeBean> grants;

    public ProjectPermissionBean() {
    }

    private ProjectPermissionBean(String permissionKey, String permissionName, String permissionDesc, Iterable<SecurityTypeBean> grants) {
        this.permissionKey = permissionKey;
        this.permissionName = permissionName;
        this.permissionDesc = permissionDesc;
        this.grants = grants != null ? ImmutableList.copyOf(grants) : null;
    }

    public String getPermissionKey() {
        return permissionKey;
    }

    public void setPermissionKey(String permissionKey) {
        this.permissionKey = permissionKey;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getPermissionDesc() {
        return permissionDesc;
    }

    public void setPermissionDesc(String permissionDesc) {
        this.permissionDesc = permissionDesc;
    }

    public List<SecurityTypeBean> getGrants() {
        return grants;
    }

    public void setGrants(List<SecurityTypeBean> grants) {
        this.grants = grants != null ? ImmutableList.copyOf(grants) : null;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(ProjectPermissionBean data) {
        return new Builder(data);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProjectPermissionBean that = (ProjectPermissionBean) o;

        return Objects.equal(this.permissionKey, that.permissionKey) &&
                Objects.equal(this.permissionName, that.permissionName) &&
                Objects.equal(this.permissionDesc, that.permissionDesc) &&
                Objects.equal(this.grants, that.grants);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(permissionKey, permissionName, permissionDesc, grants);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("permissionKey", permissionKey)
                .add("permissionName", permissionName)
                .add("permissionDesc", permissionDesc)
                .add("grants", grants)
                .toString();
    }

    public static final class Builder {


        private String permissionKey;
        private String permissionName;
        private String permissionDesc;
        private List<SecurityTypeBean> grants = Lists.newArrayList();

        private Builder() {
        }

        private Builder(ProjectPermissionBean initialData) {

            this.permissionKey = initialData.permissionKey;
            this.permissionName = initialData.permissionName;
            this.permissionDesc = initialData.permissionDesc;
            this.grants = initialData.grants;
        }


        public Builder setPermissionKey(String permissionKey) {
            this.permissionKey = permissionKey;
            return this;
        }


        public Builder setPermissionName(String permissionName) {
            this.permissionName = permissionName;
            return this;
        }


        public Builder setPermissionDesc(String permissionDesc) {
            this.permissionDesc = permissionDesc;
            return this;
        }


        public Builder setGrants(List<SecurityTypeBean> grants) {
            this.grants = grants;
            return this;
        }


        public Builder addGrant(SecurityTypeBean grant) {
            this.grants.add(grant);
            return this;
        }

        public Builder addGrants(Iterable<SecurityTypeBean> grants) {
            for (SecurityTypeBean grant : grants) {
                addGrant(grant);
            }
            return this;
        }


        public ProjectPermissionBean build() {
            return new ProjectPermissionBean(permissionKey, permissionName, permissionDesc, grants);
        }
    }
}
