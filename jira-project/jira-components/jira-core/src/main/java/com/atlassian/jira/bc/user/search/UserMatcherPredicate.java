package com.atlassian.jira.bc.user.search;

import com.atlassian.jira.user.ApplicationUser;
import com.google.common.base.Predicate;
import org.apache.commons.lang.StringUtils;

import static com.atlassian.jira.bc.user.search.UserSearchUtilities.createUserMatchPredicate;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Matcher to compare User parts (username, Full Name and email) with a query string and return true any part matches.
 *
 * @since v5.0
 */
public class UserMatcherPredicate implements Predicate<ApplicationUser> {
    private final String query;
    private final boolean canMatchAddresses;
    private final String emailQuery;

    /**
     * @param query             The query to compare. Query can not be null.  Empty string will return true for all, so don't pass one in.
     * @param canMatchAddresses Whether email should be searched
     */
    public UserMatcherPredicate(String query, boolean canMatchAddresses) {
        this(query, "", canMatchAddresses);
    }

    /**
     * Search both nameQuery and emailQuery unless one is empty.
     * <p>
     * If canMatchAddresses is false, email matching is ignored.
     * <p>
     * <ul>
     * <li>If both nameQuery and emailQuery are not empty and canMatchAddresses is true, the user's user name/display name has to match nameQuery
     * and email matches emailQuery before considered as a match. </li>
     * <p>
     * <li>If only nameQuery is not empty or both nameQuery and emailQuery are specified but canMatchAddresses is false,
     * nameQuery is used to match against username, display name, and email (when canMatchAddresses is true).
     * But matching on any field is considered a match. This is the same behaviour as the original
     * {@link UserMatcherPredicate#UserMatcherPredicate(String, boolean)} constructor </li>
     * <p>
     * <li>If only emailQuery is not empty and canMatchAddresses is true, emailQuery is used to match against email only.</li>
     * <p>
     * <li>if both are empty or only emailQuery is not empty but canMatchAddresses is false, it's always considered a match.
     * Avoid it.</li>
     * <p>
     * </ul>
     * <p>
     * When emailQuery is not empty and canMatchAddresses is true, it corresponds to the search behaviour in the
     * user picker popup browser where email search field is entered some value.
     *
     * @param nameQuery         The query on user name
     * @param emailQuery        The query on email
     * @param canMatchAddresses Whether email should be searched
     * @since v6.2
     */
    public UserMatcherPredicate(String nameQuery, String emailQuery, boolean canMatchAddresses) {
        notNull("query", nameQuery);
        notNull("emailquery", emailQuery);
        this.query = nameQuery.toLowerCase();
        this.emailQuery = emailQuery.toLowerCase();
        this.canMatchAddresses = canMatchAddresses;
    }

    @Override
    /**
     * @param user  The user to test. Cannot be null.
     * @return true if any part matches the query string
     */
    public boolean apply(ApplicationUser user) {
        // NOTE - we don't test against blank or null strings here. Do that once before the code that calls this method.
        boolean separateEmailQuery = StringUtils.isNotBlank(emailQuery);

        final java.util.function.Predicate<String> namePredicate = createUserMatchPredicate(query);
        final java.util.function.Predicate<String> emailPredicate = separateEmailQuery ? createUserMatchPredicate(emailQuery) : namePredicate;

        boolean usernameMatched = false;
        // 1. Try the username
        String userPart = user.getName();
        if (StringUtils.isNotBlank(userPart) && namePredicate.test(userPart)) {
            if (separateEmailQuery && canMatchAddresses) {
                // still need to check emailQuery against email
                usernameMatched = true;
            } else {
                return true;
            }
        }

        // 2. If allowed, try the User's email address
        //    at this point, either username not matched, or username matched but we need to match email separately
        if (canMatchAddresses) {
            userPart = user.getEmailAddress();
            if (StringUtils.isNotBlank(userPart) && emailPredicate.test(userPart)) {
                if (!separateEmailQuery || usernameMatched) {
                    // email matched using name query or email matched using email query and username already matched
                    return true;
                }
                // email separately matched but username not match, we want to try display name
            } else {
                if (separateEmailQuery) {
                    return false; // when emailQuery is explicitly specified, it must match
                }
            }
        }

        // 3. at this point, email matching is not required, or matched, but username not matched, just need to check display name
        userPart = user.getDisplayName();
        if (StringUtils.isNotBlank(userPart)) {
            if (namePredicate.test(userPart)) {
                return true;
            }
        }

        return false;
    }

}
