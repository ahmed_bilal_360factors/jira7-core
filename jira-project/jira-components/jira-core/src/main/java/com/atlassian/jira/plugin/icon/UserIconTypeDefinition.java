package com.atlassian.jira.plugin.icon;

import com.atlassian.jira.icon.IconType;

import javax.annotation.Nonnull;
import java.util.Objects;

public class UserIconTypeDefinition implements IconTypeDefinition {
    private long defaultId = -1;

    private UserIconTypePolicy iconTypePolicy;
    private SystemIconImageProvider systemIconImageProvider;

    public UserIconTypeDefinition(UserIconTypePolicy iconTypePolicy, DefaultSystemIconImageProvider systemIconImageProvider) {
        this.iconTypePolicy = iconTypePolicy;
        this.systemIconImageProvider = systemIconImageProvider;
    }

    @Nonnull
    @Override
    public String getKey() {
        return IconType.USER_ICON_TYPE.getKey();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserIconTypeDefinition)) return false;
        UserIconTypeDefinition that = (UserIconTypeDefinition) o;
        return Objects.equals(getKey(), that.getKey());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getKey());
    }

    @Nonnull
    @Override
    public IconTypePolicy getPolicy() {
        return iconTypePolicy;
    }

    @Nonnull
    @Override
    public SystemIconImageProvider getSystemIconImageProvider() {
        return systemIconImageProvider;
    }
}
