package com.atlassian.jira.web.action.admin.importer.project;

import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.pagebuilder.JiraPageBuilderService;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.Map;

/**
 * This provides functions to create the Progress Tracker element on the Project Import Pages
 */
public class ProjectImportProgressTrackerUtil {

    public static enum Page {
        SELECT_BACKUP("admin.project.import.progress.tracker.select.backup"),
        SELECT_PROJECT("admin.project.import.progress.tracker.select.project"),
        IMPORT_VALIDATION("admin.project.import.progress.tracker.import.validation"),
        IMPORT_PROJECT("admin.project.import.progress.tracker.import.project"),
        SUMMARY("admin.project.import.progress.tracker.summary");

        private final String stringName;

        Page(String name) {
            stringName = name;
        }

        public String getString() {
            return stringName;
        }
    }

    private final PluginAccessor pluginAccessor;
    private final I18nHelper i18n;

    public ProjectImportProgressTrackerUtil(final PluginAccessor pluginAccessor, final I18nHelper i18nHelper) {
        this.pluginAccessor = pluginAccessor;
        this.i18n = i18nHelper;
    }

    /**
     * Returns a list of Map objects which represents the steps in the progress tracker and the parameters for
     * each individual step.
     *
     * @param currentPage The page project import is currently on
     * @return a List of maps that represent steps in an individual tracker and the key value pairs being the parameters
     * for each individual step
     */
    public List<Map<String, Object>> getProgressTrackerSteps(Page currentPage) {
        ImmutableList.Builder<Map<String, Object>> builder = ImmutableList.builder();
        for (Page item : Page.values()) {
            builder.add(MapBuilder.<String, Object>newBuilder()
                    .add("text", i18n.getText(item.getString()))
                    .add("isCurrent", item == currentPage)
                    .add("width", 20)
                    .toMap());
        }

        return builder.build();
    }

    /**
     * Require the web resources needed for progress tracker.
     *
     * @param jiraPageBuilderService The jira page builder
     */
    public void requireAuiProgressTrackerResource(JiraPageBuilderService jiraPageBuilderService) {
        WebResourceAssembler webResourceAssembler = jiraPageBuilderService.assembler();

        //When JIRA's AUI version is beyond 5.8 this if statement can be deleted and just import non-experimental resource
        if (pluginAccessor.isPluginModuleEnabled("com.atlassian.auiplugin:aui-experimental-progress-tracker")) {
            webResourceAssembler.resources().requireWebResource("com.atlassian.auiplugin:aui-experimental-progress-tracker");
        } else {
            webResourceAssembler.resources().requireWebResource("com.atlassian.auiplugin:aui-progress-tracker");
        }
    }
}
