package com.atlassian.jira.web.task;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.task.TaskDescriptor;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.tenancy.TenantAware;
import com.atlassian.jira.tenancy.TenantInfo;
import com.atlassian.jira.upgrade.UpgradeService;
import com.atlassian.jira.web.util.CloudControlIPCheck;
import com.atlassian.mail.queue.MailQueue;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.core.LifecycleAwareSchedulerService;
import com.atlassian.scheduler.core.RunningJob;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.MappingJsonFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.atlassian.jira.component.ComponentAccessor.getComponentSafely;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_FORBIDDEN;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_SERVICE_UNAVAILABLE;

/**
 * This endpoint is used by the instant upgrade feature and only works in cloud.
 * GET returns the state of the task queue.
 * PUT:
 * <ul>
 * <li>Makes the queue to stop accepting requests if requested action is disableQueues</li>
 * <li>Stops the queues and cancels all running tasks if requested action is cancelTask</li>
 * <li>Restarts queues if requested on /resume path</li>
 * </ul>
 * <p>
 * This is used by zero downtime upgrades feature in cloud.
 *
 * @since v7.1.0
 */
@TenantInfo(TenantAware.TENANTLESS)
public class TaskQuiescingServlet extends HttpServlet {
    public static final int SC_UNCANCELABLE_TASKS_STILL_RUNNING = SC_INTERNAL_SERVER_ERROR;
    public static final int SC_TASKS_STILL_RUNNING = SC_SERVICE_UNAVAILABLE;
    public static final String DISABLE_QUEUES = "disableQueues";
    public static final String CANCEL_TASKS = "cancelTasks";
    private final static Logger log = LoggerFactory.getLogger(TaskQuiescingServlet.class);
    private static final JsonFactory JSON = new MappingJsonFactory();

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        ResponseObject responseJson = validateIsCloudAndFromControlIp(request);
        if (responseJson.getStatus() != SC_OK) {
            respond(response, responseJson);
            return;
        }

        Optional<TaskManager> optionalTaskManager = ComponentAccessor.getComponentSafely(TaskManager.class);
        Optional<LifecycleAwareSchedulerService> optionalScheduler = ComponentAccessor.getComponentSafely(LifecycleAwareSchedulerService.class);

        if (!optionalTaskManager.isPresent() || !optionalScheduler.isPresent()) {
            responseJson.setStatus(SC_INTERNAL_SERVER_ERROR);
            responseJson.addMessage("Task manager or scheduler are not available");
            respond(response, responseJson);
            return;
        }

        responseJson.setStatus(SC_OK);
        addTasksToResponse(responseJson, optionalTaskManager.get(), optionalScheduler.get());
        respond(response, responseJson);
    }

    private void addTasksToResponse(ResponseObject responseJson, TaskManager taskManager, LifecycleAwareSchedulerService scheduler) {
        responseJson.setBackgroundTasks(serializeBackgroundTasks(taskManager.getLiveTasks()));
        responseJson.setScheduledJobs(serializeScheduledJobs(scheduler.getLocallyRunningJobs()));
    }

    /**
     * Handles requests to stop the task manager and scheduler.
     * If path is /resume it restarts the task manager and scheduler.
     * Response code:
     * <ul>
     * <li>200 Operation correctly performed.</li>
     * <li>403 Attempting to hit this servlet from outside of the containing instance</li>
     * <li>404 Attempting to hit this servlet on a Jira Server instance</li>
     * </ul>
     * <p>
     * For operation specific response codes see {@link #quiesce}  or {@link #resume}
     * Response body: Json representation of a {@link ResponseObject}
     * <p>
     */
    @Override
    protected void doPut(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        ResponseObject responseJson = validateIsCloudAndFromControlIp(request);
        if (responseJson.getStatus() != SC_OK) {
            respond(response, responseJson);
            return;
        }

        String path = request.getPathInfo();
        if (path == null) {
            responseJson = quiesce(request);
        } else if (path.equals("/resume")) {
            responseJson = resume();
        } else {
            responseJson.setStatus(SC_BAD_REQUEST);
            responseJson.addMessage("Unknown path " + path);
        }
        respond(response, responseJson);
    }

    /**
     * Handles requests to shut down the task manager and scheduler.
     * The instance will:
     * <ul>
     * <li>Stop accepting more long running tasks.</li>
     * <li>Stop scheduling scheduled tasks if canceltasks is true.</li>
     * </ul>
     * Currently running tasks will be given some time to terminate.
     * <p>
     * Request parameters:
     * <ul>
     * <li>action=disableQueues: Stop both background task and scheduled jobs queues</li>
     * <li>action=cancelTasks: Cancel running tasks as well as stopping the queues</li>
     * </ul>
     * <p>
     * Response code:
     * <ul>
     * <li>200 All background tasks and scheduled tasks finished in given time.</li>
     * <li>400 Missing action parameter.</li>
     * <li>500 There was an unexpected error (detail will be included in response json)</li>
     * <li>503 There are non cancelable tasks running (a list of them will be included in response json)</li>
     * </ul>
     * <p>
     * Response body: Json representation of a {@link ResponseObject}
     * <p>
     */
    private ResponseObject quiesce(final HttpServletRequest request) throws IOException {
        Optional<String> optionalAction = getCancelTasksParameter(request);
        if (!optionalAction.isPresent()) {
            final ResponseObject responseJson = new ResponseObject();
            responseJson.setStatus(SC_BAD_REQUEST);
            responseJson.addMessage("Missing action parameter");
            return responseJson;
        }

        String action = optionalAction.get();

        Optional<TaskManager> optionalTaskManager = ComponentAccessor.getComponentSafely(TaskManager.class);
        Optional<LifecycleAwareSchedulerService> optionalScheduler = ComponentAccessor.getComponentSafely(LifecycleAwareSchedulerService.class);

        TaskManager taskManager;
        LifecycleAwareSchedulerService scheduler;

        if (!optionalTaskManager.isPresent()) {
            final ResponseObject responseJson = new ResponseObject();
            responseJson.setStatus(SC_INTERNAL_SERVER_ERROR);
            responseJson.addMessage("TaskManager is not available.");
            return responseJson;
        }

        if (!optionalScheduler.isPresent()) {
            final ResponseObject responseJson = new ResponseObject();
            responseJson.setStatus(SC_INTERNAL_SERVER_ERROR);
            responseJson.addMessage("Scheduler is not available.");
            return responseJson;
        }

        scheduler = optionalScheduler.get();
        taskManager = optionalTaskManager.get();

        final ResponseObject responseJson;
        if (action == CANCEL_TASKS) {
            responseJson = cancelTasks(taskManager, scheduler);

        } else {
            responseJson = stopQueues(taskManager, scheduler);
        }

        // Flush mail queue
        getComponentSafely(MailQueue.class).ifPresent(mq -> mq.sendBuffer());
        return responseJson;
    }
    
    /**
     * Restarts quiesced queues. This is a recovery step in case the new instance did not start properly.
     * Response code:
     * <ul>
     * <li>200 Background tasks and scheduled jobs correctly restarted.</li>
     * <li>500 There was an unexpected error (detail will be included in response json)</li>
     */
    private ResponseObject resume() throws ServletException, IOException {
        final ResponseObject responseJson = new ResponseObject();

        Optional<TaskManager> optionalTaskManager = ComponentAccessor.getComponentSafely(TaskManager.class);
        Optional<LifecycleAwareSchedulerService> optionalScheduler = ComponentAccessor.getComponentSafely(LifecycleAwareSchedulerService.class);

        if (optionalTaskManager.isPresent()) {
            optionalTaskManager.get().start();
            responseJson.addMessage("TaskManager successfully restarted");
        } else {
            responseJson.setStatus(SC_INTERNAL_SERVER_ERROR);
            responseJson.addMessage("TaskManager is not available; it cannot be restarted.");
        }

        if (optionalScheduler.isPresent()) {
            try {
                optionalScheduler.get().start();
                responseJson.addMessage("SchedulerService successfully restarted");
            } catch (SchedulerServiceException e) {
                responseJson.setStatus(SC_INTERNAL_SERVER_ERROR);
                log.error(e.toString());
                responseJson.addMessage("An error occurred while restarting Scheduler: " + e.getMessage());
            }
        } else {
            responseJson.setStatus(SC_INTERNAL_SERVER_ERROR);
            responseJson.addMessage("SchedulerService is not available; it cannot be restarted");
        }

        responseJson.setStatusIfNotAlreadySet(SC_OK);
        return responseJson;
    }

    private boolean areUpgradeJobsRunning() {
        Optional<UpgradeService> optionalUpgradeService = ComponentAccessor.getComponentSafely(UpgradeService.class);
        if (optionalUpgradeService.isPresent()) {
            UpgradeService upgradeService = optionalUpgradeService.get();
            return upgradeService.areUpgradesRunning();
        }
        return false;
    }
    
    private boolean areNonCancelableTasksRunning(TaskManager taskManager) {
        return taskManager.getLiveTasks().stream().anyMatch(task -> !task.isCancellable());
    }

    /**
     * Prevents TaskManager and Scheduler from accepting new tasks if there all running tasks are cancelable
     *
     * @param taskManager
     * @param scheduler
     * @return
     */
    private ResponseObject stopQueues(TaskManager taskManager, LifecycleAwareSchedulerService scheduler) {
        final ResponseObject responseJson = new ResponseObject();
        
        if (areUpgradeJobsRunning()) {
            responseJson.setStatus(SC_UNCANCELABLE_TASKS_STILL_RUNNING);
            responseJson.addMessage("There delayed upgrade tasks running");
            return responseJson;
        }
        
        if (areNonCancelableTasksRunning(taskManager)) {
            responseJson.setStatus(SC_UNCANCELABLE_TASKS_STILL_RUNNING);
            responseJson.addMessage("There are non cancelable tasks running");
            return responseJson;
        }

        try {
            taskManager.shutdownAndWait(0, TimeUnit.SECONDS);
            scheduler.standby();
            responseJson.setStatus(SC_OK);
        } catch (SchedulerServiceException e) {
            responseJson.setStatus(SC_INTERNAL_SERVER_ERROR);
            responseJson.addMessage("Could not put Scheduler in standby mode");
            log.error("There was an error shutting putting LifecycleAwareSchedulerService in standby mode: " + e.getMessage());
            log.debug(e.toString());
        }

        addTasksToResponse(responseJson, taskManager, scheduler);
        return responseJson;
    }

    /**
     * Stops queues and cancels all running tasks
     *
     * @param taskManager
     * @param scheduler
     * @return
     */
    private ResponseObject cancelTasks(TaskManager taskManager, LifecycleAwareSchedulerService scheduler) {
        final ResponseObject responseJson = stopQueues(taskManager, scheduler);
        if (responseJson.getStatus() != SC_OK) {
            return responseJson;
        }

        Collection<TaskDescriptor<?>> runningTasks = taskManager.getLiveTasks();
        Collection<RunningJob> runningJobs = scheduler.getLocallyRunningJobs();

        if (!runningTasks.isEmpty()) {
            for (TaskDescriptor task : runningTasks) {
                if (task.isCancellable()) {
                    taskManager.cancelTask(task.getTaskId());
                    log.info("Sending cancel to task: " + task.getTaskId() + " " + task.getDescription());
                } else {
                    // This is unlikely to happen as we've checked for uncancelable tasks beforehand
                    responseJson.setStatus(SC_UNCANCELABLE_TASKS_STILL_RUNNING);
                    log.warn("Task: " + task.getTaskId() + " " + task.getDescription() + " is not cancelable");
                }
            }
            if (responseJson.getStatus() != SC_UNCANCELABLE_TASKS_STILL_RUNNING) {
                responseJson.setStatus(SC_TASKS_STILL_RUNNING);
            }
        }

        if (!runningJobs.isEmpty()) {
            // Cancel all pending jobs
            for (RunningJob job : runningJobs) {
                // This does not guarantee that running jobs stop, but they're notified to do so
                job.cancel();
                log.info("Sending cancel to job: " + job.getJobId() + " " + job.getJobConfig().getJobRunnerKey() + "");
            }
            if (responseJson.getStatus() != SC_UNCANCELABLE_TASKS_STILL_RUNNING) {
                responseJson.setStatus(SC_TASKS_STILL_RUNNING);
            }
        }

        addTasksToResponse(responseJson, taskManager, scheduler);
        responseJson.setStatusIfNotAlreadySet(SC_OK);
        return responseJson;
    }

    /**
     * Responds given object
     *
     * @param response
     * @param responseJson
     * @throws IOException
     * @paran statusCode
     */
    private void respond(final HttpServletResponse response, final ResponseObject responseJson) throws IOException {
        int status = responseJson.getStatus();
        response.setStatus(status);
        response.setContentType("application/json");
        if (status == SC_OK || status == SC_TASKS_STILL_RUNNING || status == SC_UNCANCELABLE_TASKS_STILL_RUNNING
                || status == SC_BAD_REQUEST) {
            try (JsonGenerator jsonGenerator = JSON.createJsonGenerator(response.getWriter())) {
                jsonGenerator.writeObject(responseJson);
            }
        }
    }

    /**
     * Returns a list with serializable information about {@link RunningJob}s
     *
     * @param runningJobs
     * @return
     */
    private List<Map<String, Serializable>> serializeScheduledJobs(Collection<RunningJob> runningJobs) {
        // Running Jobs are not serializable objects
        List<Map<String, Serializable>> serializableRunningJobs = runningJobs.stream()
                .map(job -> ImmutableMap.<String, Serializable>of(
                        "jobID", job.getJobId().toString(),
                        "startTime", job.getStartTime(),
                        "config", job.getJobConfig().toString()
                )).collect(Collectors.toList());
        return serializableRunningJobs;
    }

    /**
     * @param backgroundTasks
     * @return a list with serializable information about background tasks, either running or pending
     */
    private List<Map<String, Serializable>> serializeBackgroundTasks(Collection<TaskDescriptor<?>> backgroundTasks) {
        // TaskDescriptor contains some non serializable stuff so they cannot be added directly to the json response.
        // That's why we're building list of serializable maps

        List<Map<String, Serializable>> serializableTasks = backgroundTasks.stream()
                .map(task -> ImmutableMap.<String, Serializable>builder()
                        .put("description", task.getDescription())
                        .put("isStarted", task.isStarted())
                        .put("isCancelable", task.isCancellable())
                        .put("isCanceled", task.isCancelled())
                        .put("taskID", task.getTaskId())
                        .put("progressURL", task.getProgressURL())
                        .put("elapsedRuntime", String.valueOf(task.getElapsedRunTime()))
                        .put("submittedTimestamp", task.getSubmittedTimestamp())
                        .build()).collect(Collectors.toList());
        return serializableTasks;
    }

    private Optional<String> getCancelTasksParameter(final HttpServletRequest request) throws NumberFormatException {
        String cancelParam = request.getParameter("action");
        if (CANCEL_TASKS.equalsIgnoreCase(cancelParam)) {
            return Optional.of(CANCEL_TASKS);
        } else if (DISABLE_QUEUES.equalsIgnoreCase(cancelParam)) {
            return Optional.of(DISABLE_QUEUES);
        } else {
            log.warn("Expected action parameter to be one of {}, {}, but was {}", CANCEL_TASKS, DISABLE_QUEUES, cancelParam);
            return Optional.empty();
        }
    }

    /**
     * Checks that the instance is a cloud one and the request comes from control IP
     * eg https://127.127.127.127:12990/quiesce.
     *
     * @param request
     * @return responseJson Response code and error messages will be set here if the security checks don't pass
     */
    private ResponseObject validateIsCloudAndFromControlIp(HttpServletRequest request) {
        final ResponseObject responseJson = new ResponseObject();
        Predicate isCloudAndControl = new CloudControlIPCheck();
        if (isCloudAndControl.test(request)) {
            responseJson.setStatus(SC_OK);
        } else {
            // Let's print attacker IP
            // The X-Forwarded-For header field discloses information about the client that initiated the request and
            // subsequent proxies in a chain of proxies
            String ipAddress = request.getHeader("X-FORWARDED-FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }

            log.warn("Attempted to access " + request.getRequestURL() + " from: " + ipAddress);
            responseJson.setStatus(SC_FORBIDDEN);
        }
        return responseJson;
    }

    private static class ResponseObject implements Serializable {
        private static int UNSET_STATUS = -1;
        private final AtomicInteger statusCode;
        @JsonProperty
        private final ConcurrentLinkedQueue<String> messages;
        @JsonProperty
        private volatile List<Map<String, Serializable>> backgroundTasks;
        @JsonProperty
        private volatile List<Map<String, Serializable>> scheduledTasks;

        public ResponseObject() {
            statusCode = new AtomicInteger(UNSET_STATUS);
            messages = new ConcurrentLinkedQueue<>();
            backgroundTasks = Lists.newArrayList();
            scheduledTasks = Lists.newArrayList();
        }

        public int getStatus() {
            return this.statusCode.get();
        }

        public void setStatus(int statusCode) {
            this.statusCode.set(statusCode);
        }

        public void setStatusIfNotAlreadySet(int statusCode) {
            this.statusCode.compareAndSet(UNSET_STATUS, statusCode);
        }

        public Collection<String> getMessages() {
            return ImmutableList.copyOf(this.messages);
        }

        public void addMessage(String message) {
            this.messages.add(message);
        }

        public void setBackgroundTasks(List<Map<String, Serializable>> backgroundTasks) {
            this.backgroundTasks = backgroundTasks;
        }

        public void setScheduledJobs(List<Map<String, Serializable>> schedulerTasks) {
            this.scheduledTasks = schedulerTasks;
        }
    }
}
