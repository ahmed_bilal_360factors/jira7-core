package com.atlassian.jira.plugin.bigpipe;

import com.atlassian.jira.concurrent.Barrier;
import com.atlassian.jira.concurrent.BarrierFactory;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.util.Supplier;
import com.atlassian.json.marshal.wrapped.JsonableString;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.apache.commons.lang3.StringEscapeUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.concurrent.CompletableFuture.supplyAsync;

/**
 * Provides methods for generating content through BigPipe at the end of the request.
 *
 * @since v7.1
 */
public class BigPipeService {
    static final String FEATURE_BIG_PIPE = "com.atlassian.jira.config.BIG_PIPE";
    private static final String PER_REQUEST_EXECUTOR = "perRequestExecutor";

    private final WebResourceIntegration webResourceIntegration;
    private final PageBuilderService pageBuilderService;
    private final FeatureManager featureManager;
    private final Barrier barrier;

    public BigPipeService(final WebResourceIntegration webResourceIntegration, final PageBuilderService pageBuilderService,
                          final FeatureManager featureManager, final BarrierFactory barrierFactory) {
        this.webResourceIntegration = webResourceIntegration;
        this.pageBuilderService = pageBuilderService;
        this.featureManager = featureManager;

        barrier = barrierFactory.getBarrier("bigpipe");
    }

    /**
     * Adds content to the pipe processing. Must be called from within thread that executes the request.
     * Currently contentGenerator will be executed in the same thread as the request, it processing will be deferred
     * till the main content will be sent to the browser.
     *
     * @param id               id of the DOM element which contents will be replaced with the one produced by contentGenerator
     * @param priority         the priority for rendering the generated content.  Higher priorities are rendered before
     *                         lower priorities.  Null priority will use the default.
     * @param contentGenerator generator for JSON content that will replace the BigPipe placeholder eventually.
     */
    public void pipeContent(@Nonnull final String id, @Nullable final Integer priority, @Nonnull Supplier<String> contentGenerator) {
        pageBuilderService.assembler().data()
                .requireData(id, supplyAsync(() -> {
                            barrier.await();
                            return new JsonableString(StringEscapeUtils.escapeJson(contentGenerator.get()));
                        },
                        perRequestExecutor().prioritized(priority)));
    }

    /**
     * Takes one of the tasks that have been added with {@link #pipeContent(String, Integer, Supplier)} method and executes it in
     * current thread.
     *
     * @return true if there was a task to run, false if there are no tasks.
     */
    public boolean executeSingleTask() {
        return perRequestExecutor().pop().map(r -> {
            r.run();
            return true;
        }).orElse(false);
    }

    /**
     * Closes the executor to make sure no new tasks will be added in the current request. Any subsequent calls to
     * {@link #pipeContent(String, Integer, Supplier)} in the current request will throw an {@link IllegalStateException}.
     */
    public void closeExecutor() {
        perRequestExecutor().close();
    }

    private CollectingExecutor perRequestExecutor() {
        CollectingExecutor executor = (CollectingExecutor) webResourceIntegration.getRequestCache()
                .get(PER_REQUEST_EXECUTOR);
        if (executor == null) {
            executor = new CollectingExecutor();
            webResourceIntegration.getRequestCache().put(PER_REQUEST_EXECUTOR, executor);
        }
        return executor;
    }

    public boolean isBigPipeEnabled() {
        return featureManager.isEnabled(FEATURE_BIG_PIPE);
    }

    /*
      IF we want to execute things in separate thread then we need at lest this things fetched in THREAD EXECUTING THE REQUEST:
     final ApplicationUser applicationUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
     final VelocityRequestContext velocityRequestContext = ComponentAccessor.getComponent(VelocityRequestContextFactory.class).getJiraVelocityRequestContext();
     final Tenant currentTenant = ComponentAccessor.getComponent(JiraTenantContext.class).getCurrentTenant();
     final HttpServletRequest httpServletRequest = ExecutingHttpRequest.get();
     final HttpServletResponse response = ExecutingHttpRequest.getResponse();

     Set like this in the thread that would be executiing generators:

     ComponentAccessor.getJiraAuthenticationContext().setLoggedInUser(applicationUser);
     ComponentAccessor.getComponent(VelocityRequestContextFactory.class).setVelocityRequestContext(velocityRequestContext);
     ComponentAccessor.getComponent(JiraTenantContext.class).setCurrentTenant(currentTenant);
     ExecutingHttpRequest.set(httpServletRequest, response);
     */
}
