package com.atlassian.jira.web.util;

import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import java.util.Map;

/**
 * Informs the UI whether JIRA owns its various avatar pickers or not.
 *
 * @since v6.5
 */
public class AvatarPickerConfigDataProvider implements WebResourceDataProvider {
    private PluginAccessor pluginAccessor;

    public AvatarPickerConfigDataProvider(final PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    public Jsonable get() {
        WebResourceDataProvider provider = getAtlassianAccountData();
        if (null != provider) {
            return provider.get();
        }

        return writer ->
        {
            try {
                new JSONObject().write(writer);
            } catch (JSONException e) {
                throw new Jsonable.JsonMappingException(e);
            }
        };
    }

    private WebResourceDataProvider getAtlassianAccountData() {
        if (pluginAccessor.isPluginEnabled("com.atlassian.id.atlassian-account-plugin")
                && pluginAccessor.isPluginModuleEnabled("com.atlassian.id.atlassian-account-plugin:jira-resources")) {
            final WebResourceModuleDescriptor module = (WebResourceModuleDescriptor) pluginAccessor.getEnabledPluginModule("com.atlassian.id.atlassian-account-plugin:jira-resources");
            if (null == module) {
                return null;
            }

            final Map<String, WebResourceDataProvider> dataProviders = module.getDataProviders();
            if (null == dataProviders) {
                return null;
            }

            for (Map.Entry<String, WebResourceDataProvider> entry : dataProviders.entrySet()) {
                if ("avatarPickerData".equals(entry.getKey())) {
                    return entry.getValue();
                }
            }
        }
        return null;
    }
}
