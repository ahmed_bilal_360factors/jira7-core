package com.atlassian.jira.bc.project;

import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.event.project.ProjectEventManager;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenScheme;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.sharing.SharePermissionDeleteUtils;
import com.atlassian.jira.task.ProvidesTaskProgress;
import com.atlassian.jira.task.TaskProgressSink;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.task.context.Contexts;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollections;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.FixedSized;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;

import java.util.concurrent.Callable;

/**
 * Command for Asynchronous Deletion of a Project
 *
 * @since v7.1.1
 */
public class DeleteProjectCommand implements Callable<ProjectService.DeleteProjectResult>, ProvidesTaskProgress {

    private final ProjectManager projectManager;
    private final WorkflowSchemeManager workflowSchemeManager;
    private final IssueTypeScreenSchemeManager issueTypeScreenSchemeManager;
    private final CustomFieldManager customFieldManager;
    private final NodeAssociationStore nodeAssociationStore;
    private final VersionManager versionManager;
    private final ProjectComponentManager projectComponentManager;
    private final SharePermissionDeleteUtils sharePermissionDeleteUtils;
    private final I18nHelper.BeanFactory i18nFactory;
    private final WorkflowManager workflowManager;
    private final PermissionSchemeManager permissionSchemeManager;
    private final ProjectEventManager projectEventManager;

    private final IssueManager issueManager;
    private ApplicationUser user;
    private final Project project;
    private ErrorCollection errorCollection;
    private TaskProgressSink taskProgressSink;

    public DeleteProjectCommand(ProjectManager projectManager, WorkflowSchemeManager workflowSchemeManager,
                                IssueTypeScreenSchemeManager issueTypeScreenSchemeManager, CustomFieldManager customFieldManager,
                                NodeAssociationStore nodeAssociationStore, VersionManager versionManager,
                                ProjectComponentManager projectComponentManager, SharePermissionDeleteUtils sharePermissionDeleteUtils,
                                I18nHelper.BeanFactory i18nFactory, WorkflowManager workflowManager, PermissionSchemeManager permissionSchemeManager, ProjectEventManager projectEventManager,
                                IssueManager issueManager, ApplicationUser user, Project project) {
        this.projectManager = projectManager;
        this.workflowSchemeManager = workflowSchemeManager;
        this.issueTypeScreenSchemeManager = issueTypeScreenSchemeManager;
        this.customFieldManager = customFieldManager;
        this.nodeAssociationStore = nodeAssociationStore;
        this.versionManager = versionManager;
        this.projectComponentManager = projectComponentManager;
        this.sharePermissionDeleteUtils = sharePermissionDeleteUtils;
        this.i18nFactory = i18nFactory;
        this.workflowManager = workflowManager;
        this.permissionSchemeManager = permissionSchemeManager;
        this.projectEventManager = projectEventManager;
        this.issueManager = issueManager;
        this.user = user;
        this.project = project;
    }

    public ErrorCollection getErrorCollection() {
        return errorCollection;
    }

    public ProjectService.DeleteProjectResult call() {

        final I18nHelper i18nBean = getI18nBean(user);
        // Remove teh permission scheme, so no one has any access to the project.
        permissionSchemeManager.removeSchemesFromProject(project);

        // Add 10% to size of the progress to allow for all the non issue items to be removed.
        long size = issueManager.getIssueCountForProject(project.getId()) * 110 / 100;
        // Set size to at least 200, so we see progress even with very small # of issues.
        size = Math.max(size, 200);
        ProgressManager progressManager = new ProgressManager(size);
        Context context = Contexts.builder()
                .sized(new FixedSized((int) size))
                .progress(taskProgressSink, i18nBean, "", "admin.projects.delete.project.subtask")
                .build();

        errorCollection = ErrorCollections.empty();

        context.setName(i18nBean.getText("admin.projects.delete.project.step.remove.issues"));
        try {
            projectManager.removeProjectIssues(project, context);
        } catch (RemoveException e) {
            errorCollection.addErrorMessage(i18nBean.getText("admin.errors.project.exception.removing", e.getMessage()));
            return new ProjectService.DeleteProjectResult(errorCollection);
        }
        
        progressManager.progressToPercentage(context, 90);

        context.setName(i18nBean.getText("admin.projects.delete.project.step.remove.custom.fields"));
        Context.Task task = context.start(project);
        try {
            // Remove all context associations with the project
            customFieldManager.removeProjectAssociations(project);
        } finally {
            task.complete();
        }
        progressManager.progressByPercentage(context, 2);

        context.setName(i18nBean.getText("admin.projects.delete.project.step.remove.screens"));
        // Remove issue type field screen scheme association. This needs tobe done separately to the code below, as extra steps need ot be taken
        // when an issue type field screen scheme is disassociated from a project. For example, remove the actual issue type field screen scheme in the
        // if the scheme is not associated with other projects.
        final IssueTypeScreenScheme issueTypeScreenScheme = issueTypeScreenSchemeManager.getIssueTypeScreenScheme(project);
        issueTypeScreenSchemeManager.removeSchemeAssociation(project, issueTypeScreenScheme);
        progressManager.progressByPercentage(context, 1);

        // removing all associations with this project
        context.setName(i18nBean.getText("admin.projects.delete.project.step.remove.components.and.versions"));
        try {
            nodeAssociationStore.removeAllAssociationsFromSource(Entity.Name.PROJECT, project.getId());
            versionManager.deleteAllVersions(project.getId());
            projectComponentManager.deleteAllComponents(project.getId());

        } catch (DataAccessException e) {
            errorCollection.addErrorMessage(i18nBean.getText("admin.errors.project.exception.removing", e.getMessage()));
            return new ProjectService.DeleteProjectResult(errorCollection);
        }
        progressManager.progressByPercentage(context, 2);

        context.setName(i18nBean.getText("admin.projects.delete.project.step.remove.permissions"));
        sharePermissionDeleteUtils.deleteProjectSharePermissions(project.getId());
        progressManager.progressByPercentage(context, 1);

        context.setName(i18nBean.getText("admin.projects.delete.project.step.remove.draft.workflows"));
        workflowSchemeManager.cleanUpSchemeDraft(project, user);
        progressManager.progressByPercentage(context, 1);

        context.setName(i18nBean.getText("admin.projects.delete.project.step.remove.project"));
        projectManager.removeProject(project);
        projectManager.refresh();
        progressManager.progressByPercentage(context, 1);

        context.setName(i18nBean.getText("admin.projects.delete.project.step.remove.workflows"));
        Scheme workflowScheme = workflowSchemeManager.getSchemeFor(project);
        final Iterable<JiraWorkflow> workflowsAssociatedToProject = workflowManager.getWorkflowsFromScheme(workflowScheme);
        // JRA-8032 - clear the active workflow name cache
        workflowSchemeManager.clearWorkflowCache();
        workflowManager.copyAndDeleteDraftsForInactiveWorkflowsIn(user, workflowsAssociatedToProject);
        progressManager.progressByPercentage(context, 1);

        projectEventManager.dispatchProjectDeleted(user, project);
        // Complete the progress fully.
        progressManager.progressByPercentage(context, 100);

        return new ProjectService.DeleteProjectResult(errorCollection);
    }

    final protected I18nHelper getI18nBean(ApplicationUser user) {
        return i18nFactory.getInstance(user);
    }

    @Override
    public void setTaskProgressSink(TaskProgressSink taskProgressSink) {
        this.taskProgressSink = taskProgressSink;
    }

    private static class ProgressManager {
        final long size;

        private ProgressManager(long size) {
            this.size = size;
        }

        void progressByPercentage(Context context, int percent) {
            long tasks = size * percent / 100;
            long remaining = context.getNumberOfTasksToCompletion();

            for (long i = 0; i < Math.min(tasks, remaining) & i < size; i++) {
                context.start(null).complete();
            }
        }
        void progressToPercentage(Context context, int percent) {
            long tasksForPercentComplete = size * percent / 100;
            long remaining = context.getNumberOfTasksToCompletion();
            long done = size - remaining;
            for (long i = done; i < tasksForPercentComplete & i < size; i++) {
                context.start(null).complete();
            }
        }
    }
}
