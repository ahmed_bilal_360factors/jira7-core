package com.atlassian.jira.upgrade.tasks;

import javax.annotation.Nullable;

/**
 * Reindexes JIRA due changes to allow native sorting of many system fields by Lucene.
 *
 * @since v5.1
 */
public class UpgradeTask_Build757 extends AbstractReindexUpgradeTask {

    @Override
    public int getBuildNumber() {
        return 757;
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception {
        reIndexAllIssues();
    }

    @Override
    public String getShortDescription() {
        return super.getShortDescription() + " due to changes to way some values are indexed.";
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        // Flagged as not downgradable because it was created before the Downgrade Task framework was built.
        return true;
    }

    @Nullable
    @Override
    public Integer dependsUpon() {
        return 756;
    }

}
