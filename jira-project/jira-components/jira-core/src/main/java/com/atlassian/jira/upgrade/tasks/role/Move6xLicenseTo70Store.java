package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.fugue.Option;
import com.atlassian.jira.auditing.AssociatedItem;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Upgrade task to move the JIRA 6.x license into the JIRA 7.0 store.
 * <p>
 * NOTE: The {@link MigrationTask#migrate(MigrationState, boolean)} of this task does not actually mutate the
 * database, it makes all its changes exclusively on the passed {@link MigrationState}. Something outside of this
 * class will persist the state once it is validated correct.
 *
 * @since v7.0
 */
final class Move6xLicenseTo70Store extends MigrationTask {
    private static final boolean EVENT_SHOWS_IN_CLOUD_LOG = false;
    private final LicenseDao dao;

    Move6xLicenseTo70Store(LicenseDao dao) {
        this.dao = notNull("dao", dao);
    }

    @Override
    MigrationState migrate(MigrationState state, final boolean licenseSuppliedByUser) {
        state = state.withAfterSaveTask(dao::remove6xLicense);
        if (!licenseSuppliedByUser) {
            //This will throw an exception if the jira6xLicense is invalid. This is correct behaviour, we can't
            //perform the migration if there are no licenses.
            final Option<License> oldLicense = dao.get6xLicense();
            if (oldLicense.isDefined()) {
                License license = oldLicense.get();
                if (state.licenses().canAdd(license)) {
                    final AuditEntry auditEntry = new AuditEntry(Move6xLicenseTo70Store.class,
                            "Moved license location",
                            "Moved license from old location in application properties to new location in license table",
                            AssociatedItem.Type.LICENSE,
                            license.getSEN(), EVENT_SHOWS_IN_CLOUD_LOG);
                    return state
                            .changeLicenses(licenses -> licenses.addLicense(license))
                            .log(auditEntry);
                } else {
                    return state.log(auditEntry("Not moving license location",
                            "Incompatible license already exists in license table."));
                }
            } else {
                return state.log(auditEntry("Not moving license location",
                        "No valid existing license present in data."));
            }
        } else {
            return state.log(auditEntry("Not moving license location",
                    "License entered during import supersedes all licenses in data."));
        }
    }

    private static AuditEntry auditEntry(String summary, String description) {
        return new AuditEntry(Move6xLicenseTo70Store.class, summary, description, AssociatedItem.Type.LICENSE,
                null, EVENT_SHOWS_IN_CLOUD_LOG);
    }
}
