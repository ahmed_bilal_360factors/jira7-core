package com.atlassian.jira.crowd.embedded.ofbiz.db;

import java.sql.Timestamp;

/**
 * Left over from the OFBiz Crowd SPI implementation.
 */
public class OfBizHelper {
    /**
     * Converts a java.sql.Timestamp to a java.util.Date.
     *
     * @param timestamp The java.sql.Timestamp
     * @return the java.util.Date.
     */
    public static java.util.Date convertToUtilDate(final Timestamp timestamp) {
        if (timestamp == null) {
            return null;
        }
        return new java.util.Date(timestamp.getTime());
    }

    /**
     * Converts a java.util.Date to a java.sql.Timestamp.
     *
     * @param date The java.util.Date
     * @return the java.sql.Timestamp.
     */
    public static Timestamp convertToSqlTimestamp(final java.util.Date date) {
        if (date == null) {
            return null;
        }
        return new Timestamp(date.getTime());
    }
}
