package com.atlassian.jira.util.system.check;

import java.util.List;
import java.util.Locale;

/**
 * This class exists purely so that classes can have the system environment checklist injected for testing as opposed to
 * looking up messages statically. System checks typically include warnings for specific detected problems such as
 * running in an unsupported JVM or container.
 *
 * @see {com.atlassian.jira.util.system.check.SystemEnvironmentChecklist}
 * @since 7.0
 */
public interface SystemEnvironmentChecklistRetriever {
    /**
     * Gets all known system warning messages as specified in the checklist. This does not retrieve all warnings in the
     * system; only those specifically configured to show in this list.
     *
     * @param locale the locale to use for these messages, usually the current user's locale
     * @param asHtml whether to provide html tag wrappers around the message for nicer display in the browser
     * @return a list of messages
     */
    public List<String> getWarningMessages(Locale locale, final boolean asHtml);

    /**
     * Gets all known system warning messages as specified in the checklist, but in English. This does not retrieve
     * all warnings in the system; only those specifically configured to show in this list. This is the same as calling
     * getWarningMessages(Locale.ENGLISH, false).
     *
     * @return a list of messages
     */
    public List<String> getEnglishWarningMessages();
}
