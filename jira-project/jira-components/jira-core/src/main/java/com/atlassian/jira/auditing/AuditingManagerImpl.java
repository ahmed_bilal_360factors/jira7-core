package com.atlassian.jira.auditing;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.auditing.handlers.FieldLayoutSchemeChangeHandler;
import com.atlassian.jira.auditing.handlers.GroupEventHandler;
import com.atlassian.jira.auditing.handlers.JiraUpgradeEventHandler;
import com.atlassian.jira.auditing.handlers.NotificationChangeHandler;
import com.atlassian.jira.auditing.handlers.PermissionChangeHandler;
import com.atlassian.jira.auditing.handlers.ProjectComponentEventHandler;
import com.atlassian.jira.auditing.handlers.ProjectEventHandler;
import com.atlassian.jira.auditing.handlers.SchemeEventHandler;
import com.atlassian.jira.auditing.handlers.SystemAuditEventHandler;
import com.atlassian.jira.auditing.handlers.UserEventHandler;
import com.atlassian.jira.auditing.handlers.VersionEventHandler;
import com.atlassian.jira.auditing.handlers.WorkflowEventHandler;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.concurrent.BarrierFactory;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.ExecutingHttpRequest;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Locale;

/**
 * @since v6.2
 */
public class AuditingManagerImpl implements AuditingManager, Startable {
    private final AuditingStore auditingStore;
    private final EventPublisher eventPublisher;
    private final AuditingEventListener auditingEventListener;
    private final JiraAuthenticationContext authenticationContext;
    private final PermissionManager permissionManager;
    private final BarrierFactory barrierFactory;

    public AuditingManagerImpl(final AuditingStore auditingStore, final EventPublisher eventPublisher,
                               final JiraAuthenticationContext authenticationContext, final PermissionManager permissionManager,
                               final PermissionChangeHandler permissionChangeHandler, final GroupEventHandler groupEventHandler,
                               final SchemeEventHandler schemeEventHandler, final UserEventHandler userEventHandler,
                               final WorkflowEventHandler workflowEventHandler, final NotificationChangeHandler notificationChangeHandler,
                               final FieldLayoutSchemeChangeHandler fieldLayoutSchemeChangeHandler,
                               final ProjectEventHandler projectEventHandler, final BarrierFactory barrierFactory,
                               final ProjectComponentEventHandler projectComponentEventHandler, final VersionEventHandler versionEventHandler,
                               final SystemAuditEventHandler systemAuditEventHandler, final JiraUpgradeEventHandler jiraUpgradeEventHandler) {
        this.auditingStore = auditingStore;
        this.eventPublisher = eventPublisher;
        this.authenticationContext = authenticationContext;
        this.permissionManager = permissionManager;
        this.barrierFactory = barrierFactory;
        this.auditingEventListener = new AuditingEventListener(this, permissionChangeHandler, groupEventHandler,
                schemeEventHandler, userEventHandler, workflowEventHandler, notificationChangeHandler,
                fieldLayoutSchemeChangeHandler, projectEventHandler, projectComponentEventHandler, versionEventHandler,
                systemAuditEventHandler, jiraUpgradeEventHandler);
    }

    @Override
    public void store(RecordRequest record) {
        final ApplicationUser author = record.getAuthor() != null ? record.getAuthor() : getLoggedInUser();
        final AuditingEntry.Builder entry = AuditingEntry.builder(record.getCategory(), record.getSummary(), record.getEventSource())
                .categoryName(record.getCategoryName())
                .remoteAddress(StringUtils.defaultString(record.getRemoteAddress(), getRemoteAddress()))
                .author(author)
                .objectItem(record.getObjectItem())
                .changedValues(record.getChangedValues())
                .associatedItems(record.getAssociatedItems())
                .isAuthorSysAdmin(permissionManager.hasPermission(Permissions.SYSTEM_ADMIN, author))
                .description(record.getDescription());
        auditingStore.storeRecord(entry.build());
    }

    @Override
    @Nonnull
    public Records getRecords(@Nullable Long maxId, @Nullable Long sinceId, @Nullable Integer maxResults, @Nullable Integer offset, @Nullable AuditingFilter filter) {
        return getRecords(maxId, sinceId, maxResults, offset, filter, true);
    }

    @Override
    @Nonnull
    public Records getRecordsWithoutSysAdmin(@Nullable Long maxId, @Nullable Long sinceId, @Nullable Integer maxResults, @Nullable Integer offset, @Nullable AuditingFilter filter) {
        return getRecords(maxId, sinceId, maxResults, offset, filter, false);
    }

    @Override
    public long countRecords(@Nullable Long maxId, @Nullable Long sinceId) {
        return countRecords(maxId, sinceId, true);
    }

    @Override
    public long countRecordsWithoutSysAdmin(@Nullable Long maxId, @Nullable Long sinceId) {
        return countRecords(maxId, sinceId, false);
    }

    protected Records getRecords(@Nullable final Long maxId, @Nullable final Long sinceId, @Nullable final Integer maxResults,
                                 final Integer offset, final @Nullable AuditingFilter filter, final boolean includeSysAdminActions) {
        barrierFactory.getBarrier("auditingGetRecords").await();
        return auditingStore.getRecords(maxId, sinceId, maxResults, offset, filter, includeSysAdminActions);
    }

    protected long countRecords(@Nullable final Long maxId, @Nullable final Long sinceId, final boolean includeSysAdminActions) {
        return auditingStore.countRecords(maxId, sinceId, includeSysAdminActions);
    }

    @Nullable
    protected String getRemoteAddress() {
        return ExecutingHttpRequest.get() != null ? ExecutingHttpRequest.get().getRemoteAddr() : null;
    }

    protected ApplicationUser getLoggedInUser() {
        return authenticationContext.getUser();
    }

    static protected I18nHelper getI18n() {
        // You must not cache I18nHelper
        return ComponentAccessor.getI18nHelperFactory().getInstance(Locale.ENGLISH);
    }

    @Override
    public void start() throws Exception {
        eventPublisher.register(auditingEventListener);
    }
}
