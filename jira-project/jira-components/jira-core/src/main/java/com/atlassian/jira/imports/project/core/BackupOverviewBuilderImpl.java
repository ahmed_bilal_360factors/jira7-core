package com.atlassian.jira.imports.project.core;

import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.external.beans.ExternalComment;
import com.atlassian.jira.external.beans.ExternalComponent;
import com.atlassian.jira.external.beans.ExternalCustomField;
import com.atlassian.jira.external.beans.ExternalIssue;
import com.atlassian.jira.external.beans.ExternalProject;
import com.atlassian.jira.external.beans.ExternalVersion;
import com.atlassian.jira.imports.project.customfield.ExternalCustomFieldConfiguration;
import com.atlassian.jira.imports.project.populator.BackupOverviewPopulator;
import com.atlassian.jira.imports.project.populator.CommentPopulator;
import com.atlassian.jira.imports.project.populator.CustomFieldPopulator;
import com.atlassian.jira.imports.project.populator.IssueIdPopulator;
import com.atlassian.jira.imports.project.populator.PluginVersionPopulator;
import com.atlassian.jira.imports.project.populator.ProjectComponentPopulator;
import com.atlassian.jira.imports.project.populator.ProjectPopulator;
import com.atlassian.jira.imports.project.populator.ProjectVersionPopulator;
import com.atlassian.jira.imports.project.populator.SystemInfoPopulator;
import com.atlassian.jira.plugin.PluginVersion;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @since v3.13
 */
public class BackupOverviewBuilderImpl implements BackupOverviewBuilder {

    private static final Logger log = LoggerFactory.getLogger(BackupOverviewBuilderImpl.class);

    private final List<ExternalProject> projects;
    private final List<PluginVersion> pluginVersions;
    private final ListMultimap<String, Long> issueIds;
    private final Map<String, String> issueIdToKeyMap;
    private final Map<String, Integer> issueTextSystemFieldMaxLength;
    private final ListMultimap<String, ExternalVersion> versions;
    private final ListMultimap<String, ExternalComponent> components;
    private final List<BackupOverviewPopulator> overviewPopulators;
    private final List<ExternalCustomField> customFields;
    private final ListMultimap<String, ConfigurationContext> configurationContexts;
    private final ListMultimap<String, FieldConfigSchemeIssueType> fieldConfigSchemeIssueTypes;
    // Map of <Key, <ProjectId, Collection<Object>>
    private final Map<String, ListMultimap<String, Object>> additionalData;
    private final Set<String> entities;
    private int entityCount;
    private String buildNumber;
    private String edition;
    private boolean unassignedIssuesAllowed;

    public BackupOverviewBuilderImpl() {
        projects = new ArrayList<>();
        pluginVersions = new ArrayList<>();
        versions = ArrayListMultimap.create();
        components = ArrayListMultimap.create();
        issueIds = ArrayListMultimap.create();
        issueIdToKeyMap = new HashMap<>();
        customFields = new ArrayList<>();
        configurationContexts = ArrayListMultimap.create();
        fieldConfigSchemeIssueTypes = ArrayListMultimap.create();
        entities = Sets.newHashSet();
        additionalData = Maps.newHashMap();

        // Register all the populators that know how to handle
        overviewPopulators = new ArrayList<>();
        registerOverviewPopulators();
        issueTextSystemFieldMaxLength = Maps.newHashMap();
    }

    public void addProject(final ExternalProject project) {
        projects.add(project);
    }

    public void addPluginVersion(final PluginVersion pluginVersion) {
        pluginVersions.add(pluginVersion);
    }

    public void addVersion(final ExternalVersion version) {
        versions.put(version.getProjectId(), version);
    }

    public void addComponent(final ExternalComponent component) {
        components.put(component.getProjectId(), component);
    }

    public void addIssue(final ExternalIssue issue) {
        // We are storing, ONLY the issue id for each project
        issueIds.put(issue.getProject(), new Long(issue.getId()));
        // We are storing all issue/key information from the backup project. This is used by the issue links
        // to keep links to/from external projects when importing.
        issueIdToKeyMap.put(issue.getId(), issue.getKey());

        storeMaximumTextFieldLengthInIssue(issue.getId(),
                Math.max(StringUtils.length(issue.getDescription()), StringUtils.length(issue.getEnvironment())));
    }

    private void storeMaximumTextFieldLengthInIssue(final String issueId, final int fieldLength) {
        if (fieldLength >= issueTextSystemFieldMaxLength.getOrDefault(issueId, 0)) {
            issueTextSystemFieldMaxLength.put(issueId, fieldLength);
        }
    }

    @Override
    public void addComment(final ExternalComment comment) {
        storeMaximumTextFieldLengthInIssue(comment.getIssueId(), StringUtils.length(comment.getBody()));
    }

    public void setBuildNumber(final String buildNumber) {
        this.buildNumber = buildNumber;
    }

    public void setEdition(final String edition) {
        this.edition = edition;
    }

    public void setUnassignedIssuesAllowed(final boolean unassignedIssuesAllowed) {
        this.unassignedIssuesAllowed = unassignedIssuesAllowed;
    }

    public BackupOverview getBackupOverview() {
        final BackupSystemInformation backupSystemInformation = new BackupSystemInformationImpl(getBuildNumber(),
                getEdition(), getPluginVersions(), unassignedIssuesAllowed(), issueIdToKeyMap, entities, entityCount);
        return new BackupOverviewImpl(backupSystemInformation, getProjects());
    }

    private boolean unassignedIssuesAllowed() {
        return unassignedIssuesAllowed;
    }

    public void populateInformationFromElement(final String elementName, final Map attributes) throws ParseException {
        entities.add(elementName);
        entityCount++;
        for (final BackupOverviewPopulator overviewPopulator : overviewPopulators) {
            overviewPopulator.populate(this, elementName, attributes);
        }
    }

    public void addExternalCustomField(final ExternalCustomField externalCustomField) {
        customFields.add(externalCustomField);
    }

    public void addConfigurationContext(final ConfigurationContext configuration) {
        configurationContexts.put(configuration.getCustomFieldId(), configuration);
    }

    public void addFieldConfigSchemeIssueType(final FieldConfigSchemeIssueType fieldConfigSchemeIssueType) {
        fieldConfigSchemeIssueTypes.put(fieldConfigSchemeIssueType.getFieldConfigScheme(), fieldConfigSchemeIssueType);
    }

    protected void registerOverviewPopulators() {
        registerOverviewPopulator(new ProjectPopulator());
        registerOverviewPopulator(new ProjectComponentPopulator());
        registerOverviewPopulator(new ProjectVersionPopulator());
        registerOverviewPopulator(new CustomFieldPopulator());
        registerOverviewPopulator(new PluginVersionPopulator());
        registerOverviewPopulator(new IssueIdPopulator());
        registerOverviewPopulator(new SystemInfoPopulator());
        registerOverviewPopulator(new CommentPopulator());
    }

    protected void registerOverviewPopulator(final BackupOverviewPopulator populator) {
        overviewPopulators.add(populator);
    }

    List<? extends BackupProject> getProjects() {
        final List<BackupProjectImpl> fullProjects = new ArrayList<>(projects.size());
        for (final ExternalProject project : projects) {
            List<ExternalVersion> versions = this.versions.get(project.getId());
            if (versions == null) {
                versions = Collections.emptyList();
            }
            List<ExternalComponent> components = this.components.get(project.getId());
            if (components == null) {
                components = Collections.emptyList();
            }
            List<Long> issueIds = this.issueIds.get(project.getId());
            if (issueIds == null) {
                issueIds = Collections.emptyList();
            }

            final List<ExternalCustomFieldConfiguration> customFieldConfigs = getCustomFieldConfigurations(project.getId());
            final Map<String, Collection<Object>> projectAdditionalData = getProjectAdditionalData(project.getId());

            final int maxTextLengthInProject = getMaxTextLengthInProject(issueIds);

            fullProjects.add(new BackupProjectImpl(project, versions, components, customFieldConfigs, issueIds, maxTextLengthInProject, projectAdditionalData));
        }

        return fullProjects;
    }

    private int getMaxTextLengthInProject(final List<Long> issueIds) {
        return issueIds.stream().map(String::valueOf).mapToInt(id -> issueTextSystemFieldMaxLength.getOrDefault(id, 0)).max().orElseGet(() -> 0);
    }

    private Map<String, Collection<Object>> getProjectAdditionalData(final String projectId) {
        final Map<String, Collection<Object>> projectAdditionalData = new HashMap<>();
        for (final Map.Entry<String, ListMultimap<String, Object>> dataEntry : additionalData.entrySet()) {
            final ListMultimap<String, Object> keyData = dataEntry.getValue();
            final List<Object> data = keyData.get(projectId);
            if (data != null) {
                projectAdditionalData.put(dataEntry.getKey(), data);
            }
        }
        return projectAdditionalData;
    }

    /**
     * Gets the List of ExternalCustomFieldConfiguration objects for the given project.
     *
     * @param projectId The project.
     * @return List of ExternalCustomFieldConfiguration objects.
     */
    List<ExternalCustomFieldConfiguration> getCustomFieldConfigurations(final String projectId) {
        final List<ExternalCustomFieldConfiguration> externalCustomFieldConfigurationList = new ArrayList<>(customFields.size());
        // We need to inspect every custom field to see if there will be a valid configuration for the project and the
        // custom field.
        for (final ExternalCustomField customField : customFields) {

            // Get the configurationContext that is relevant for the custom field and project that we are looking at.
            final List<ConfigurationContext> configurationContexts = getRelevantConfigurationContexts(projectId, customField.getId());

            for (final ConfigurationContext configurationContext : configurationContexts) {
                // Find the issue types that this custom field configuration is constrained by
                final List<FieldConfigSchemeIssueType> fieldConfigIssueTypes = fieldConfigSchemeIssueTypes.get(configurationContext.getConfigSchemeId());
                // if there is no configuration, e.g. broken DB, then custom field will be  ignored
                if (fieldConfigIssueTypes != null) {
                    final List<String> issuesTypes = getIssueTypesList(fieldConfigIssueTypes);
                    // We always want to grab the project id from the ConfigurationContext so we can tell if the
                    // config is global or specific to a project.
                    externalCustomFieldConfigurationList.add(new ExternalCustomFieldConfiguration(issuesTypes, configurationContext.getProjectId(),
                            customField, configurationContext.getConfigSchemeId()));
                } else {
                    log.warn(String.format("Skipped Custom Field %s (%s), because of missing config scheme, id: %s",
                            customField.getName(), customField.getId(), configurationContext.getConfigSchemeId()));
                }
            }
        }
        return externalCustomFieldConfigurationList;
    }

    /**
     * Gets relevant Configuration Contexts for the given project and custom field. If a configuration explicitly
     * mentions the projectId then that will be the Configuration that is relevant. There can be also a "global" configuration
     * (i.e. a configuration with a null project).
     * There can also be no relevant configuration for the project and custom field combination in which case we return empty collection
     *
     * @param projectId     identifies the project
     * @param customFieldId identifies the custom field
     * @return relevant configuration
     */
    private List<ConfigurationContext> getRelevantConfigurationContexts(final String projectId, final String customFieldId) {
        // Grab the ConfigurationContexts associated with this custom field
        return configurationContexts.get(customFieldId).stream()
                .filter(config -> (config.getProjectId() == null) || (projectId.equals(config.getProjectId())))
                .collect(Collectors.toList());
    }

    /**
     * Builds a list of IssueType Id's (as Strings) from a List of FieldConfigSchemeIssueTypes.
     *
     * @param fieldConfigIssueTypes objects containing the issue type strings
     * @return list of IssueType Id's, null if the custom field config is relevant for all issue types. NOTE: this
     * should never return an empty list, this indicates invalid data.
     */
    private List<String> getIssueTypesList(final List<FieldConfigSchemeIssueType> fieldConfigIssueTypes) {
        final List<String> ids = new ArrayList<>(fieldConfigIssueTypes.size());
        for (final FieldConfigSchemeIssueType fieldConfigIssueType : fieldConfigIssueTypes) {
            final String type = fieldConfigIssueType.getIssueType();
            // If there is an issue type of null then we are available for all issue types AND the data SHOULD only
            // contain this one FieldConfigSchemeIssueType
            if (type == null) {
                return null;
            }
            ids.add(type);
        }
        return ids;
    }

    List<PluginVersion> getPluginVersions() {
        return pluginVersions;
    }

    String getBuildNumber() {
        return buildNumber;
    }

    String getEdition() {
        return edition;
    }

    @Override
    public void addAdditionalData(final String key, final String projectId, final Object data) {
        ListMultimap<String, Object> additionalDataForKey = additionalData.get(key);
        if (additionalDataForKey == null) {
            additionalDataForKey = ArrayListMultimap.create();
            additionalData.put(key, additionalDataForKey);
        }
        additionalDataForKey.put(projectId, data);
    }
}
