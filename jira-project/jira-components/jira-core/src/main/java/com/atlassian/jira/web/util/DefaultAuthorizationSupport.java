package com.atlassian.jira.web.util;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;

/**
 * Implementation of the authorization checks defined in {@link AuthorizationSupport}
 *
 * @since v4.3
 */
public class DefaultAuthorizationSupport implements AuthorizationSupport {
    private final GlobalPermissionManager globalPermissionManager;
    private final PermissionManager permissionManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public DefaultAuthorizationSupport(final GlobalPermissionManager globalPermissionManager,
                                       final PermissionManager permissionManager, final JiraAuthenticationContext jiraAuthenticationContext) {
        this.globalPermissionManager = globalPermissionManager;
        this.permissionManager = permissionManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    public boolean hasPermission(int permissionsId) {
        return permissionManager.hasPermission(permissionsId, jiraAuthenticationContext.getLoggedInUser());
    }

    @Override
    public boolean hasGlobalPermission(GlobalPermissionKey globalPermissionKey) {
        return globalPermissionManager.hasPermission(globalPermissionKey, jiraAuthenticationContext.getLoggedInUser());
    }

    @Override
    public boolean hasGlobalPermission(String permissionKey) {
        return hasGlobalPermission(GlobalPermissionKey.of(permissionKey));
    }

    @Override
    public boolean hasIssuePermission(String permissionKey, Issue issue) {
        return permissionManager.hasPermission(new ProjectPermissionKey(permissionKey), issue, jiraAuthenticationContext.getLoggedInUser());
    }

    @Override
    public boolean hasIssuePermission(int permissionsId, Issue issue) {
        return permissionManager.hasPermission(permissionsId, issue, jiraAuthenticationContext.getLoggedInUser());
    }

    @Override
    public boolean hasIssuePermission(ProjectPermissionKey projectPermissionKey, Issue issue) {
        return permissionManager.hasPermission(projectPermissionKey, issue, jiraAuthenticationContext.getLoggedInUser());
    }

    @Override
    public boolean hasProjectPermission(int permissionsId, Project project) {
        return permissionManager.hasPermission(new ProjectPermissionKey(permissionsId), project, jiraAuthenticationContext.getLoggedInUser());
    }

    @Override
    public boolean hasProjectPermission(ProjectPermissionKey projectPermissionKey, Project project) {
        return permissionManager.hasPermission(projectPermissionKey, project, jiraAuthenticationContext.getLoggedInUser());
    }
}
