package com.atlassian.jira.issue.search.searchers.util;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.bc.user.search.UserSearchParams;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.comparator.UserCachingComparator;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserHistoryItem;
import com.atlassian.jira.user.UserHistoryManager;
import com.atlassian.jira.user.util.GroupNameComparator;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.base.Function;
import com.google.common.collect.Sets;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.base.Functions.compose;
import static com.google.common.collect.ImmutableList.copyOf;

public class UserSearcherHelperImpl implements UserSearcherHelper {
    private final GroupManager groupManager;
    private final PermissionManager permissionManager;
    private final UserManager userManager;
    private final UserHistoryManager userHistoryManager;
    private final UserSearchService userSearchService;
    private static final int LARGE_USER_COUNT_THRESHOLD = 10;

    public UserSearcherHelperImpl(final GroupManager groupManager, final PermissionManager permissionManager,
                                  final UserManager userManager,
                                  final UserHistoryManager userHistoryManager, final UserSearchService userSearchService) {
        this.groupManager = groupManager;
        this.permissionManager = permissionManager;
        this.userManager = userManager;
        this.userHistoryManager = userHistoryManager;
        this.userSearchService = userSearchService;
    }

    @Override
    public void addUserSuggestionParams(ApplicationUser user, List<String> selectedUsers, Map<String, Object> params) {
        params.put("hasPermissionToPickUsers", hasUserPickingPermission(user));
        params.put("suggestedUsers", getSuggestedUsers(user, selectedUsers, UserSearchParams.ACTIVE_USERS_ALLOW_EMPTY_QUERY));
        if (hasUserPickingPermission(user)) {
            params.put("placeholderText", getI18n(user).getText("common.concepts.sparkler.placeholder.browse_user"));
        } else {
            params.put("placeholderText", getI18n(user).getText("common.concepts.sparkler.placeholder.no_browse_user"));
        }
    }

    @Override
    public void addUserGroupSuggestionParams(ApplicationUser user, List<String> selectedUsers,
                                             Map<String, Object> params) {
        addUserGroupSuggestionParams(user, selectedUsers, UserSearchParams.ACTIVE_USERS_ALLOW_EMPTY_QUERY, params);
    }

    @Override
    public void addUserGroupSuggestionParams(final ApplicationUser user, final List<String> selectedUsers,
                                             final UserSearchParams searchParams, final Map<String, Object> params) {
        params.put("hasPermissionToPickUsers", hasUserPickingPermission(user));
        if (searchParams != null) {
            // only add suggestedUsers when the searchParams is specified
            params.put("suggestedUsers", getSuggestedUsers(user, selectedUsers, searchParams));
        }
        params.put("suggestedGroups", getSuggestedGroups(user));
        if (hasUserPickingPermission(user)) {
            params.put("placeholderText", getI18n(user).getText("common.concepts.sparkler.placeholder.browse_usergroup"));
        } else {
            params.put("placeholderText", getI18n(user).getText("common.concepts.sparkler.placeholder.no_browse_usergroup"));
        }
    }


    @Override
    public void addGroupSuggestionParams(ApplicationUser user, Map<String, Object> params) {
        params.put("hasPermissionToPickUsers", hasUserPickingPermission(user));
        params.put("suggestedGroups", getSuggestedGroups(user));
        if (hasUserPickingPermission(user)) {
            params.put("placeholderText", getI18n(user).getText("common.concepts.sparkler.placeholder.browse_group"));
        } else {
            params.put("placeholderText", getI18n(user).getText("common.concepts.sparkler.placeholder.no_browse_group"));
        }
    }


    /**
     * @param user The user performing the search.
     * @return Groups that should be suggested.
     */
    List<Group> getSuggestedGroups(ApplicationUser user) {
        // very specific implementation, do not reuse com.atlassian.jira.bc.group.search.GroupPickerSearchService.findGroups()
        List<Group> groups = new ArrayList<>();
        if (user != null) {
            // Suggest groups the user is a member of.
            groups.addAll(groupManager.getGroupsForUser(user));
        } else if (hasUserPickingPermission(user)) {
            // Suggest the first n groups in the system.
            groups.addAll(groupManager.getAllGroups());
        } else {
            return null;
        }

        Collections.sort(groups, new GroupNameComparator());
        return groups.subList(0, Math.min(5, groups.size()));
    }

    /**
     * @param appUser           The user performing the search.
     * @param selectedUserNames the values that are already selected in this search
     */
    List<ApplicationUser> getSuggestedUsers(ApplicationUser appUser, List<String> selectedUserNames, UserSearchParams searchParams) {
        if (hasUserPickingPermission(appUser)) {
            int limit = 5;
            LinkedHashSet<ApplicationUser> suggestedUsers = new LinkedHashSet<ApplicationUser>();
            Collection<ApplicationUser> recentlyUsedUsers = getRecentlyUsedUsers(appUser);
            addToSuggestList(limit, suggestedUsers, recentlyUsedUsers, Sets.newHashSet(selectedUserNames), searchParams);

            // JRADEV-15375 With a significant number of users in the system, a full sort is very slow and pointless to
            // suggest random users anyway. On the other hand, we do like putting_something_in here for evaluators...
            if (suggestedUsers.size() < limit && isEvaluatorSystem()) {
                // Suggest the first 'limit' users in the system (if they don't already appear).
                List<ApplicationUser> allUsers = userSearchService.findUsers("", searchParams);

                //No guarantee list is modifiable so make a copy before sorting
                allUsers = new ArrayList<>(allUsers);

                Collections.sort(allUsers, new UserCachingComparator());

                if (!recentlyUsedUsers.isEmpty()) {
                    addToSuggestList(limit, suggestedUsers, allUsers, Stream.concat(selectedUserNames.stream(),
                            recentlyUsedUsers.stream().map(ApplicationUser::getName)).collect(Collectors.toSet()), searchParams);
                } else {
                    addToSuggestList(limit, suggestedUsers, allUsers, Sets.newHashSet(selectedUserNames), searchParams);
                }
            }

            return copyOf(suggestedUsers);
        } else {
            return null;
        }
    }

    /**
     * @return whether this system is regarded as an 'evaluator' system that has only a small number of users in it.
     */
    boolean isEvaluatorSystem() {
        UserSearchParams params = UserSearchParams.builder()
                                    .allowEmptyQuery(true)
                                    .maxResults(LARGE_USER_COUNT_THRESHOLD + 1)
                                    .build();
        return userSearchService.findUserNames("", params).size() <= LARGE_USER_COUNT_THRESHOLD;
    }

    /**
     * Add to 'suggestedUsers' all users from 'users' (except users already in 'alreadySelected'),
     * until 'suggestedUsers' is 'limit' big.
     */
    void addToSuggestList(int limit, LinkedHashSet<ApplicationUser> suggestedUsers, Iterable<ApplicationUser> users, Set<String> alreadySelected, UserSearchParams searchParams) {
        for (ApplicationUser user : users) {
            if (suggestedUsers.size() >= limit) {
                break;
            }
            if (!suggestedUsers.contains(user) && !alreadySelected.contains(user.getName())
                    && userSearchService.userMatches(user, searchParams)) {
                suggestedUsers.add(user);
            }
        }
    }

    Set<ApplicationUser> getUsersToSelect(Stream<ApplicationUser> usersToSelectFrom, Set<String>alreadySelected, int limit, UserSearchParams searchParams) {
        return usersToSelectFrom.filter(u -> !alreadySelected.contains(u.getName()))
                .filter(u -> userSearchService.userMatches(u, searchParams))
                .limit(limit)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    private Set<ApplicationUser> getRecentlyUsedUsers(ApplicationUser user) {
        Function<String, ApplicationUser> toUser = name -> name == null ? null : userManager.getUserByName(name);

        Function<UserHistoryItem, ApplicationUser> historyItemToUser = compose(toUser, UserHistoryItem.GET_ENTITY_ID);
        return userHistoryManager.getHistory(UserHistoryItem.USED_USER, user).stream()
                .map(historyItemToUser::apply)
                .filter(appUser -> appUser != null)
                .collect(Collectors.toSet());
    }

    @Override
    public boolean hasUserPickingPermission(final ApplicationUser user) {
        return permissionManager.hasPermission(Permissions.USER_PICKER, user);
    }

    private I18nHelper getI18n(ApplicationUser user) {
        return ComponentAccessor.getComponent(I18nHelper.BeanFactory.class).getInstance(user);
    }
}
