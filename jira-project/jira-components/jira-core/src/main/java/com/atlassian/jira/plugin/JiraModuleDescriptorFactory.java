package com.atlassian.jira.plugin;

import com.atlassian.crowd.plugin.descriptors.PasswordEncoderModuleDescriptor;
import com.atlassian.jira.application.JiraApplicationMetaDataModuleDescriptor;
import com.atlassian.jira.bc.dataimport.DataImportOSPropertyValidatorModuleDescriptor;
import com.atlassian.jira.config.feature.FeatureFlagModuleDescriptor;
import com.atlassian.jira.imports.project.ao.handler.AoImportHandlerModuleDescriptor;
import com.atlassian.jira.imports.project.ao.handler.AoOverviewHandlerModuleDescriptor;
import com.atlassian.jira.imports.project.ao.handler.AoPreImportHandlerModuleDescriptor;
import com.atlassian.jira.imports.project.ao.handler.ValidatorModuleDescriptor;
import com.atlassian.jira.imports.project.handler.OfBizImportHandlerModuleDescriptor;
import com.atlassian.jira.imports.project.handler.OfBizPreImportHandlerModuleDescriptor;
import com.atlassian.jira.imports.project.handler.PostImportHandlerModuleDescriptor;
import com.atlassian.jira.imports.project.handler.PreImportHandlerModuleDescriptor;
import com.atlassian.jira.notification.NotificationFilterModuleDescriptor;
import com.atlassian.jira.onboarding.FirstUseFlowModuleDescriptor;
import com.atlassian.jira.plugin.aboutpagepanel.AboutPagePanelModuleDescriptorImpl;
import com.atlassian.jira.plugin.attachment.AttachmentProcessorModuleDescriptor;
import com.atlassian.jira.plugin.bigpipe.BigPipeWebPanelModuleDescriptor;
import com.atlassian.jira.plugin.comment.CommentFieldRendererModuleDescriptorImpl;
import com.atlassian.jira.plugin.component.ComponentModuleDescriptor;
import com.atlassian.jira.plugin.componentpanel.ComponentTabPanelModuleDescriptorImpl;
import com.atlassian.jira.plugin.contentlinkresolver.ContentLinkResolverDescriptor;
import com.atlassian.jira.plugin.customfield.CustomFieldSearcherModuleDescriptorImpl;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptorImpl;
import com.atlassian.jira.plugin.decorator.DecoratorMapperModuleDescriptor;
import com.atlassian.jira.plugin.decorator.DecoratorModuleDescriptor;
import com.atlassian.jira.plugin.entity.EntityPropertyConditionHelperModuleDescriptor;
import com.atlassian.jira.plugin.icon.IconTypeModuleDescriptor;
import com.atlassian.jira.plugin.index.EntityPropertyIndexDocumentModuleDescriptorImpl;
import com.atlassian.jira.plugin.index.EntitySearchExtractorModuleDescriptorImpl;
import com.atlassian.jira.plugin.issuelink.IssueLinkRendererModuleDescriptorImpl;
import com.atlassian.jira.plugin.issuetabpanel.IssueTabPanelModuleDescriptorImpl;
import com.atlassian.jira.plugin.issueview.IssueViewModuleDescriptorImpl;
import com.atlassian.jira.plugin.jql.function.JqlFunctionModuleDescriptorImpl;
import com.atlassian.jira.plugin.keyboardshortcut.KeyboardShortcutModuleDescriptor;
import com.atlassian.jira.plugin.language.LanguageModuleDescriptorImpl;
import com.atlassian.jira.plugin.language.TranslationTransformModuleDescriptorImpl;
import com.atlassian.jira.plugin.navigation.FooterModuleDescriptorImpl;
import com.atlassian.jira.plugin.navigation.TopNavigationModuleDescriptorImpl;
import com.atlassian.jira.plugin.permission.GlobalPermissionModuleDescriptorImpl;
import com.atlassian.jira.plugin.permission.ProjectPermissionModuleDescriptorImpl;
import com.atlassian.jira.plugin.profile.ViewProfilePanelModuleDescriptorImpl;
import com.atlassian.jira.plugin.projectoperation.ProjectOperationModuleDescriptorImpl;
import com.atlassian.jira.plugin.projectpanel.ProjectTabPanelModuleDescriptorImpl;
import com.atlassian.jira.plugin.renderer.JiraRendererModuleDescriptorImpl;
import com.atlassian.jira.plugin.renderer.RendererComponentDecoratorFactoryDescriptor;
import com.atlassian.jira.plugin.renderer.MacroModuleDescriptor;
import com.atlassian.jira.plugin.renderercomponent.RendererComponentFactoryDescriptor;
import com.atlassian.jira.plugin.report.ReportModuleDescriptorImpl;
import com.atlassian.jira.plugin.roles.ProjectRoleActorModuleDescriptor;
import com.atlassian.jira.plugin.searchrequestview.SearchRequestViewModuleDescriptorImpl;
import com.atlassian.jira.plugin.user.PasswordPolicyModuleDescriptor;
import com.atlassian.jira.plugin.user.PreDeleteUserErrorsModuleDescriptor;
import com.atlassian.jira.plugin.userformat.descriptors.UserFormatModuleDescriptorImpl;
import com.atlassian.jira.plugin.versionpanel.VersionTabPanelModuleDescriptorImpl;
import com.atlassian.jira.plugin.webfragment.descriptors.DefaultSimpleLinkFactoryModuleDescriptor;
import com.atlassian.jira.plugin.webfragment.descriptors.JiraWebItemModuleDescriptor;
import com.atlassian.jira.plugin.webfragment.descriptors.JiraWebSectionModuleDescriptor;
import com.atlassian.jira.plugin.webresource.JiraLookAndFeelWebResourceModuleDescriptor;
import com.atlassian.jira.plugin.webwork.WebworkModuleDescriptor;
import com.atlassian.jira.plugin.workflow.WorkflowConditionModuleDescriptor;
import com.atlassian.jira.plugin.workflow.WorkflowFunctionModuleDescriptor;
import com.atlassian.jira.plugin.workflow.WorkflowValidatorModuleDescriptor;
import com.atlassian.jira.project.template.descriptor.DemoProjectModuleDescriptor;
import com.atlassian.jira.project.template.descriptor.ProjectTemplateModuleDescriptor;
import com.atlassian.jira.security.auth.AuthorisationModuleDescriptor;
import com.atlassian.jira.security.plugin.ProjectPermissionOverrideModuleDescriptorImpl;
import com.atlassian.plugin.eventlistener.descriptors.EventListenerModuleDescriptor;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.schema.impl.DefaultDescribedModuleDescriptorFactory;
import com.atlassian.plugin.servlet.descriptors.ServletContextListenerModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletContextParamModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletFilterModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebItemProviderModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebPanelRendererModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebSectionProviderModuleDescriptor;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerModuleDescriptor;
import com.atlassian.soy.renderer.SoyFunctionModuleDescriptor;
import com.atlassian.upgrade.core.descriptors.UpgradeTaskFactoryModuleDescriptor;

public class JiraModuleDescriptorFactory extends DefaultDescribedModuleDescriptorFactory {
    public JiraModuleDescriptorFactory(final HostContainer hostContainer) {
        super(hostContainer);
        addModuleDescriptor("workflow-condition", WorkflowConditionModuleDescriptor.class);
        addModuleDescriptor("workflow-validator", WorkflowValidatorModuleDescriptor.class);
        addModuleDescriptor("workflow-function", WorkflowFunctionModuleDescriptor.class);
        addModuleDescriptor("customfield-type", CustomFieldTypeModuleDescriptorImpl.class);
        addModuleDescriptor("customfield-searcher", CustomFieldSearcherModuleDescriptorImpl.class);
        addModuleDescriptor("issue-tabpanel", IssueTabPanelModuleDescriptorImpl.class);
        addModuleDescriptor("project-operation", ProjectOperationModuleDescriptorImpl.class);
        addModuleDescriptor("web-section", JiraWebSectionModuleDescriptor.class);
        addModuleDescriptor("web-section-provider", WebSectionProviderModuleDescriptor.class);
        addModuleDescriptor("web-item", JiraWebItemModuleDescriptor.class);
        addModuleDescriptor("web-item-provider", WebItemProviderModuleDescriptor.class);
        addModuleDescriptor("simple-link-factory", DefaultSimpleLinkFactoryModuleDescriptor.class);
        addModuleDescriptor("single-issue-view", IssueViewModuleDescriptorImpl.class);
        addModuleDescriptor("search-request-view", SearchRequestViewModuleDescriptorImpl.class);
        addModuleDescriptor("project-tabpanel", ProjectTabPanelModuleDescriptorImpl.class);
        addModuleDescriptor("version-tabpanel", VersionTabPanelModuleDescriptorImpl.class);
        addModuleDescriptor("component-tabpanel", ComponentTabPanelModuleDescriptorImpl.class);
        addModuleDescriptor("project-roleactor", ProjectRoleActorModuleDescriptor.class);
        addModuleDescriptor("report", ReportModuleDescriptorImpl.class);
        addModuleDescriptor("web-resource", WebResourceModuleDescriptor.class);
        addModuleDescriptor("web-resource-transformer", WebResourceTransformerModuleDescriptor.class);
        addModuleDescriptor("url-reading-web-resource-transformer", UrlReadingWebResourceTransformerModuleDescriptor.class);

        addModuleDescriptor("lookandfeel-webresource", JiraLookAndFeelWebResourceModuleDescriptor.class);

        addModuleDescriptor("component", ComponentModuleDescriptor.class);
        addModuleDescriptor("webwork1", WebworkModuleDescriptor.class);

        addModuleDescriptor("icon-type", IconTypeModuleDescriptor.class);

        addModuleDescriptor("jira-renderer", JiraRendererModuleDescriptorImpl.class);
        addModuleDescriptor("macro", MacroModuleDescriptor.class);
        addModuleDescriptor("renderer-component-decorator-factory", RendererComponentDecoratorFactoryDescriptor.class);
        addModuleDescriptor("renderer-component-factory", RendererComponentFactoryDescriptor.class);
        addModuleDescriptor("content-link-resolver", ContentLinkResolverDescriptor.class);
        addModuleDescriptor("top-navigation", TopNavigationModuleDescriptorImpl.class);
        addModuleDescriptor("jira-footer", FooterModuleDescriptorImpl.class);
        addModuleDescriptor("view-profile-panel", ViewProfilePanelModuleDescriptorImpl.class);
        addModuleDescriptor("user-format", UserFormatModuleDescriptorImpl.class);
        addModuleDescriptor("jql-function", JqlFunctionModuleDescriptorImpl.class);
        addModuleDescriptor("keyboard-shortcut", KeyboardShortcutModuleDescriptor.class);

        addModuleDescriptor("issue-link-renderer", IssueLinkRendererModuleDescriptorImpl.class);

        // Crowd integration
        addModuleDescriptor("encoder", PasswordEncoderModuleDescriptor.class);
        addModuleDescriptor("authorisation", AuthorisationModuleDescriptor.class);

        // Notifications
        addModuleDescriptor("notification-filter", NotificationFilterModuleDescriptor.class);

        // Onboarding flow
        addModuleDescriptor("first-use-flow", FirstUseFlowModuleDescriptor.class);

        // User management plugin points
        addModuleDescriptor("password-policy", PasswordPolicyModuleDescriptor.class);
        addModuleDescriptor("pre-delete-user-errors", PreDeleteUserErrorsModuleDescriptor.class);

        // descriptors required by Plugins-2
        addModuleDescriptor("servlet-context-param", ServletContextParamModuleDescriptor.class);
        addModuleDescriptor("servlet", ServletModuleDescriptor.class);
        addModuleDescriptor("servlet-filter", ServletFilterModuleDescriptor.class);
        addModuleDescriptor("servlet-context-listener", ServletContextListenerModuleDescriptor.class);
        addModuleDescriptor("web-panel", BigPipeWebPanelModuleDescriptor.class);
        addModuleDescriptor("web-panel-renderer", WebPanelRendererModuleDescriptor.class);
        addModuleDescriptor("listener", EventListenerModuleDescriptor.class);

        // Sitemesh decorators
        addModuleDescriptor("decorator", DecoratorModuleDescriptor.class);
        addModuleDescriptor("decorator-mapper", DecoratorMapperModuleDescriptor.class);

        // language
        addModuleDescriptor("language", LanguageModuleDescriptorImpl.class);
        addModuleDescriptor("translation-transform", TranslationTransformModuleDescriptorImpl.class);

        // JRA-32817 - about page
        addModuleDescriptor("about-page-panel", AboutPagePanelModuleDescriptorImpl.class);

        // entity property indexing
        addModuleDescriptor("index-document-configuration", EntityPropertyIndexDocumentModuleDescriptorImpl.class);

        // entity search extractors
        addModuleDescriptor("entity-search-extractor", EntitySearchExtractorModuleDescriptorImpl.class);

        addModuleDescriptor("comment-field-renderer", CommentFieldRendererModuleDescriptorImpl.class);

        // global permissions plugin point
        addModuleDescriptor("global-permission", GlobalPermissionModuleDescriptorImpl.class);

        // project permissions plugin point
        addModuleDescriptor("project-permission", ProjectPermissionModuleDescriptorImpl.class);

        addModuleDescriptor("project-permission-override", ProjectPermissionOverrideModuleDescriptorImpl.class);

        addModuleDescriptor("application", JiraApplicationMetaDataModuleDescriptor.class);

        addModuleDescriptor("attachment-processor", AttachmentProcessorModuleDescriptor.class);

        // project import plugin points
        addModuleDescriptor("project-import-ao-overview-handler", AoOverviewHandlerModuleDescriptor.class);
        addModuleDescriptor("project-import-validator", ValidatorModuleDescriptor.class);
        addModuleDescriptor("project-preimport-handler", PreImportHandlerModuleDescriptor.class);
        addModuleDescriptor("project-postimport-handler", PostImportHandlerModuleDescriptor.class);
        addModuleDescriptor("project-ao-preimport-handler", AoPreImportHandlerModuleDescriptor.class);
        addModuleDescriptor("project-ao-import-handler", AoImportHandlerModuleDescriptor.class);
        addModuleDescriptor("project-ofbiz-preimport-handler", OfBizPreImportHandlerModuleDescriptor.class);
        addModuleDescriptor("project-ofbiz-import-handler", OfBizImportHandlerModuleDescriptor.class);
        addModuleDescriptor("project-blueprint", ProjectTemplateModuleDescriptor.class);
        addModuleDescriptor("entity-property-condition-helper", EntityPropertyConditionHelperModuleDescriptor.class);
        addModuleDescriptor("demo-project", DemoProjectModuleDescriptor.class);

        // Feature flags
        addModuleDescriptor("featureflags", FeatureFlagModuleDescriptor.class);

        // Upgrade framework for plugins
        addModuleDescriptor("upgrade-task-factory", UpgradeTaskFactoryModuleDescriptor.class);

        // Soy function
        addModuleDescriptor(SoyFunctionModuleDescriptor.XML_ELEMENT_NAME, SoyFunctionModuleDescriptor.class);

        addModuleDescriptor(DataImportOSPropertyValidatorModuleDescriptor.XML_ELEMENT_NAME, DataImportOSPropertyValidatorModuleDescriptor.class);
    }
}
