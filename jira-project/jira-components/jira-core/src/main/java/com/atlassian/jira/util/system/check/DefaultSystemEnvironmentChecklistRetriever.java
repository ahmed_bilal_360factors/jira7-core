package com.atlassian.jira.util.system.check;

import java.util.List;
import java.util.Locale;

/**
 * @since 7.0
 */
public class DefaultSystemEnvironmentChecklistRetriever implements SystemEnvironmentChecklistRetriever {
    @Override
    public List<String> getWarningMessages(final Locale locale, final boolean asHtml) {
        return SystemEnvironmentChecklist.getWarningMessages(locale, asHtml);
    }

    @Override
    public List<String> getEnglishWarningMessages() {
        return SystemEnvironmentChecklist.getEnglishWarningMessages();
    }
}
