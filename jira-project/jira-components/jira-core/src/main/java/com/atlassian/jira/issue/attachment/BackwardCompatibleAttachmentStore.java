package com.atlassian.jira.issue.attachment;

import com.atlassian.fugue.Option;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueKey;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.web.util.AttachmentException;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;
import com.google.common.base.Functions;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * {@link AttachmentStore} and {@link StreamAttachmentStore} implementation
 * that delegates calls to {@link FileSystemAttachmentStore}.
 *
 * @since v6.3
 */
@ParametersAreNonnullByDefault
public class BackwardCompatibleAttachmentStore implements AttachmentStore, StreamAttachmentStore {
    private final AttachmentDirectoryAccessor directoryAccessor;
    private final ThumbnailAccessor thumbnailAccessor;
    private final StoreAttachmentBeanMapper storeAttachmentBeanMapper;
    private final FileSystemAttachmentStore fileSystemAttachmentStore;
    private final AttachmentKeyMapper attachmentKeyMapper;
    private final ProjectManager projectManager;

    public BackwardCompatibleAttachmentStore(
            @Nonnull final AttachmentDirectoryAccessor directoryAccessor,
            @Nonnull final ThumbnailAccessor thumbnailAccessor,
            @Nonnull final StoreAttachmentBeanMapper storeAttachmentBeanMapper,
            @Nonnull final FileSystemAttachmentStore fileSystemAttachmentStore,
            @Nonnull final AttachmentKeyMapper attachmentKeyMapper,
            @Nonnull final ProjectManager projectManager) {
        this.storeAttachmentBeanMapper = storeAttachmentBeanMapper;
        this.fileSystemAttachmentStore = fileSystemAttachmentStore;
        this.attachmentKeyMapper = attachmentKeyMapper;
        this.projectManager = projectManager;
        this.directoryAccessor = Preconditions.checkNotNull(directoryAccessor);
        this.thumbnailAccessor = Preconditions.checkNotNull(thumbnailAccessor);
    }

    @Override
    @Nonnull
    public File getThumbnailDirectory(final Issue issue) {
        return directoryAccessor.getThumbnailDirectory(issue);
    }

    @Override
    public File getAttachmentDirectory(final String issueKey) {
        return directoryAccessor.getAttachmentDirectory(issueKey);
    }

    @Override
    public File getAttachmentDirectory(final Issue issue, final boolean createDirectory) {
        return directoryAccessor.getAttachmentDirectory(issue, createDirectory);
    }

    @Override
    public File getTemporaryAttachmentDirectory() {
        return directoryAccessor.getTemporaryAttachmentDirectory();
    }

    @Override
    public File getAttachmentDirectory(final Issue issue) {
        return directoryAccessor.getAttachmentDirectory(issue);
    }

    @Override
    public File getAttachmentDirectory(final String attachmentDirectory, final String projectKey, final String issueKey) {
        return directoryAccessor.getAttachmentDirectory(attachmentDirectory, projectKey, issueKey);
    }

    @Override
    public void checkValidAttachmentDirectory(final Issue issue) throws AttachmentException {
        directoryAccessor.checkValidAttachmentDirectory(issue);
    }

    @Override
    public void checkValidTemporaryAttachmentDirectory() throws AttachmentException {
        directoryAccessor.checkValidTemporaryAttachmentDirectory();
    }

    @Override
    public File getAttachmentFile(final AttachmentAdapter adapter, final File attachmentDir) {
        return getFileSystemAttachmentStore().getAttachmentFile(adapter, attachmentDir);
    }

    @Override
    @Nonnull
    public File getThumbnailFile(final Attachment attachment) {
        return thumbnailAccessor.getThumbnailFile(attachment);
    }

    @Override
    @Nonnull
    public File getThumbnailFile(final Issue issue, final Attachment attachment) {
        return thumbnailAccessor.getThumbnailFile(issue, attachment);
    }

    @Override
    public File getLegacyThumbnailFile(final Attachment attachment) {
        return thumbnailAccessor.getLegacyThumbnailFile(attachment);
    }


    @Override
    public File getAttachmentFile(final Issue issue, final Attachment attachment) throws DataAccessException {
        return getAttachmentFile(attachment);
    }

    @Override
    public File getAttachmentFile(final Attachment attachment) throws DataAccessException {
        final AttachmentKey attachmentKey = attachmentKeyMapper.fromAttachment(attachment);
        return getFileSystemAttachmentStore().getAttachmentFile(attachmentKey);
    }

    @Override
    public Promise<Attachment> putAttachment(final Attachment metadata, final InputStream source) {
        final StoreAttachmentBean storeAttachmentBean = storeAttachmentBeanMapper.mapToBean(metadata, source);

        return putAttachment(storeAttachmentBean).map(Functions.constant(metadata));
    }

    @Override
    public Promise<Attachment> putAttachment(final Attachment metadata, final File source) {
        //noinspection NullableProblems
        return storeAttachmentBeanMapper.mapToBean(metadata, source).fold(
                new com.google.common.base.Function<FileNotFoundException, Promise<Attachment>>() {
                    @Override
                    public Promise<Attachment> apply(final FileNotFoundException fileNotFoundException) {
                        return Promises.rejected(new AttachmentWriteException("Failed to open file: " + source.getAbsolutePath(), fileNotFoundException));
                    }
                }, new com.google.common.base.Function<StoreAttachmentBean, Promise<Attachment>>() {
                    @Override
                    public Promise<Attachment> apply(final StoreAttachmentBean storeAttachmentBean) {
                        return putAttachment(storeAttachmentBean).map(Functions.constant(metadata));
                    }
                });
    }

    @Override
    public Promise<StoreAttachmentResult> putAttachment(final StoreAttachmentBean storeAttachmentBean) {
        return getFileSystemAttachmentStore().putAttachment(storeAttachmentBean);
    }

    @Override
    public <A> Promise<A> getAttachment(final AttachmentKey attachmentKey, final Function<InputStream, A> inputStreamProcessor) {
        return getFileSystemAttachmentStore().getAttachment(attachmentKey, inputStreamProcessor);
    }

    @Override
    public <A> Promise<A> getAttachmentData(final AttachmentKey attachmentKey, final Function<AttachmentGetData, A> attachmentGetDataProcessor) {
        return getFileSystemAttachmentStore().getAttachmentData(attachmentKey, attachmentGetDataProcessor);
    }

    @Override
    public Promise<Unit> moveAttachment(final AttachmentKey oldAttachmentKey, final AttachmentKey newAttachmentKey) {
        return getFileSystemAttachmentStore().moveAttachment(oldAttachmentKey, newAttachmentKey);
    }

    @Override
    public Promise<Unit> copyAttachment(final AttachmentKey sourceAttachmentKey, final AttachmentKey newAttachmentKey) {
        return getFileSystemAttachmentStore().copyAttachment(sourceAttachmentKey, newAttachmentKey);
    }

    @Override
    public Promise<Unit> deleteAttachment(final AttachmentKey attachmentKey) {
        return getFileSystemAttachmentStore().deleteAttachment(attachmentKey);
    }

    @Override
    public Promise<TemporaryAttachmentId> putTemporaryAttachment(final InputStream inputStream, final long size) {
        return getFileSystemAttachmentStore().putTemporaryAttachment(inputStream, size);
    }

    @Override
    public Promise<Unit> moveTemporaryToAttachment(final TemporaryAttachmentId temporaryAttachmentId, final AttachmentKey destinationKey) {
        return getFileSystemAttachmentStore().moveTemporaryToAttachment(temporaryAttachmentId, destinationKey);
    }

    @Override
    public Promise<Unit> deleteTemporaryAttachment(final TemporaryAttachmentId temporaryAttachmentId) {
        return getFileSystemAttachmentStore().deleteTemporaryAttachment(temporaryAttachmentId);
    }

    @Override
    public Promise<Boolean> exists(final AttachmentKey attachmentKey) {
        return getFileSystemAttachmentStore().exists(attachmentKey);
    }

    @Override
    public <A> Promise<A> getAttachment(final Attachment metaData, final Function<InputStream, A> inputStreamProcessor) {
        final AttachmentKey attachmentKey = attachmentKeyMapper.fromAttachment(metaData);
        return getAttachment(attachmentKey, inputStreamProcessor);
    }

    @Override
    public Promise<Void> move(final Attachment metaData, final String newIssueKey) {
        final AttachmentKey oldAttachmentKey = attachmentKeyMapper.fromAttachment(metaData);

        final IssueKey ik = IssueKey.from(newIssueKey);
        final Project project = projectManager.getProjectObjByKey(ik.getProjectKey());
        final String originalProjectKey = project.getOriginalKey();

        final AttachmentKey newAttachmentKey = AttachmentKeys.from(
                originalProjectKey,
                newIssueKey,
                oldAttachmentKey.getAttachmentFilename(),
                oldAttachmentKey.getAttachmentId());

        return moveAttachment(oldAttachmentKey, newAttachmentKey).map(unitToVoid());
    }

    private com.google.common.base.Function<? super Unit, ? extends Void> unitToVoid() {
        return unit -> null;
    }

    @Override
    public Promise<Void> deleteAttachment(@Nonnull final Attachment attachment) {
        final AttachmentKey attachmentKey = attachmentKeyMapper.fromAttachment(attachment);
        return getFileSystemAttachmentStore().deleteAttachment(attachmentKey).map(unitToVoid());
    }

    @Override
    public Promise<Void> deleteAttachmentContainerForIssue(@Nonnull final Issue issue) {
        return getFileSystemAttachmentStore().deleteAttachmentContainerForIssue(issue).map(unitToVoid());
    }

    @Nonnull
    @Override
    public Promise<Attachment> copy(@Nonnull final Attachment originalAttachment, @Nonnull final Attachment metaData, @Nonnull final String newIssueKey) {
        return getFileSystemAttachmentStore()
                .copyAttachment(AttachmentKeys.from(originalAttachment), AttachmentKeys.from(metaData))
                .map(unit -> metaData);
    }

    @Override
    @Nonnull
    public Option<ErrorCollection> errors() {
        return getFileSystemAttachmentStore().errors();
    }

    private FileSystemAttachmentStore getFileSystemAttachmentStore() {
        return fileSystemAttachmentStore;
    }
}
