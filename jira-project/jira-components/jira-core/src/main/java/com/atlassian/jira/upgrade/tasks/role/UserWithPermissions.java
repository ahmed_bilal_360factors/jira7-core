package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.crowd.embedded.api.User;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.annotation.Nullable;
import java.util.Objects;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Encapsulates user and USE / ADMINISTER / SYSADMIN permissions.
 *
 * @since 7.0
 */
final class UserWithPermissions {
    private final User user;
    private final boolean hasUsePermission;
    private final boolean hasAdminPermission;

    UserWithPermissions(final User user, final boolean hasUsePermission, final boolean hasAdminPermission) {
        this.user = notNull("user", user);
        this.hasUsePermission = hasUsePermission;
        this.hasAdminPermission = hasAdminPermission;
    }

    User getUser() {
        return user;
    }

    /**
     * @return true if user has USE permission
     */
    boolean hasUsePermission() {
        return hasUsePermission;
    }

    /**
     * @return true if user has ADMINISTER or SYSADMIN permissions or both.
     */
    boolean hasAdminPermission() {
        return hasAdminPermission;
    }

    /**
     * @return true if the user can log in due to either Use, Admin or SysAdmin global permission
     */
    boolean canLogin() {
        return hasUsePermission() || hasAdminPermission();
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserWithPermissions that = (UserWithPermissions) o;
        return Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("user", user)
                .append("hasUsePermission", hasUsePermission)
                .append("hasAdminPermission", hasAdminPermission)
                .toString();
    }
}
