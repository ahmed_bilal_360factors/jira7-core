package com.atlassian.jira.bc.user;

import com.atlassian.annotations.Internal;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.group.GroupsToApplicationsSeatingHelper;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseCountService;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.auth.AuthorisationManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.BaseUrl;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.component.admin.group.GroupLabelView;
import com.atlassian.jira.web.component.admin.group.GroupLabelsService;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.atlassian.jira.application.ApplicationKeys.CORE;
import static com.atlassian.jira.util.dbc.Assertions.containsNoNulls;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Create User Application helper class.
 *
 * @since v7.0
 */
@Internal
public class CreateUserApplicationHelper implements UserApplicationHelper {
    public static interface ValidationFunction {
        Optional<ValidationResult> validate(ApplicationRole role, ValidationParams params);
    }

    private final Map<ValidationScope, ValidationFunction> validationFunctions;
    private final ApplicationRoleManager applicationRoleManager;
    private final I18nHelper i18nHelper;
    private final BaseUrl baseUrl;
    private final GlobalPermissionManager globalPermissionManager;
    private final LicenseCountService licenseCountService;
    private final FeatureManager featureManager;
    private final JiraLicenseManager jiraLicenseManager;
    private final GroupsToApplicationsSeatingHelper groupsToApplicationsSeatingHelper;
    private final UserManager userManager;
    private final CrowdService crowdService;
    private final GroupLabelsService groupLabels;
    private final AuthorisationManager authorisationManager;


    @VisibleForTesting
    static final String LINK_START = "<a href=\"%s\">";
    @VisibleForTesting
    static final String LINK_END = "</a>";

    public CreateUserApplicationHelper(final ApplicationRoleManager applicationRoleManager,
                                       final I18nHelper i18nHelper,
                                       final BaseUrl baseUrl,
                                       final GlobalPermissionManager globalPermissionManager,
                                       final LicenseCountService licenseCountService,
                                       final FeatureManager featureManager,
                                       final JiraLicenseManager jiraLicenseManager,
                                       final GroupsToApplicationsSeatingHelper groupsToApplicationsSeatingHelper,
                                       final UserManager userManager,
                                       final CrowdService crowdService,
                                       final GroupLabelsService groupLabels,
                                       final AuthorisationManager authorisationManager) {
        this.applicationRoleManager = applicationRoleManager;
        this.i18nHelper = i18nHelper;
        this.baseUrl = baseUrl;
        this.groupsToApplicationsSeatingHelper = groupsToApplicationsSeatingHelper;
        this.userManager = userManager;
        this.globalPermissionManager = globalPermissionManager;
        this.licenseCountService = licenseCountService;
        this.featureManager = featureManager;
        this.jiraLicenseManager = jiraLicenseManager;
        this.crowdService = crowdService;
        this.groupLabels = groupLabels;
        this.authorisationManager = authorisationManager;

        validationFunctions = ImmutableMap.of(
                ValidationScope.ACCESS, this::validateRoleAccess,
                ValidationScope.SEATS, this::validateRoleSeats,
                ValidationScope.EXPIRE, this::validateLicenseExpire
        );
    }

    @Nonnull
    public List<ApplicationSelection> getApplicationsForSelection(@Nonnull Set<ApplicationKey> selectedApplicationKeys, @Nonnull final Optional<Long> directoryId) {
        notNull("selectedApplicationKeys", selectedApplicationKeys);
        final Set<Group> selectedGroups = getSelectedGroups(selectedApplicationKeys);

        return getApplicationsFor(
                role -> selectedApplicationKeys.contains(role.getKey()),
                role -> {
                    final Set<Group> groups = role.getGroups();
                    return !groups.isEmpty() && !CollectionUtils.intersection(selectedGroups, groups).isEmpty();
                },
                directoryId
        );
    }

    @Nonnull
    @Override
    public List<ApplicationSelection> getApplicationsForUser(@Nonnull final ApplicationUser user) {
        notNull("user", user);

        final Optional<Long> directoryId = Optional.of(user.getDirectoryId());
        final Predicate<Group> groupPredicate = group -> crowdService.isUserMemberOfGroup(user.getDirectoryUser(), group);

        return getApplicationsFor(
                role -> {
                    final Set<Group> defaultGroups = role.getDefaultGroups();
                    if (!defaultGroups.isEmpty()) {
                        return defaultGroups.stream().allMatch(groupPredicate);
                    } else {
                        final Set<Group> groups = role.getGroups();
                        return !groups.isEmpty() && groups.stream().allMatch(groupPredicate);
                    }
                },
                role -> role.getGroups().stream()
                        .anyMatch(groupPredicate),
                targetRole -> applicationRoleManager.getRoles().stream()
                        .filter(role -> !role.getKey().equals(targetRole.getKey()))
                        .map(ApplicationRole::getGroups)
                        .flatMap(Collection::stream)
                        .filter(group -> targetRole.getGroups().contains(group))
                        .anyMatch(groupPredicate),
                directoryId
        );
    }

    private List<ApplicationSelection> getApplicationsFor(Predicate<ApplicationRole> isSelected, Predicate<ApplicationRole> isIndeterminate, Optional<Long> directoryId) {
        return getApplicationsFor(isSelected, isIndeterminate, isIndeterminate, directoryId);
    }

    /**
     * Fetches list of applications with some of the properties based on the provided predicates.
     * These predicates would be different depending on context. E.g. adding a user vs. viewing existing user.
     *
     * @param isSelected      used to determine if application should be rendered as selected.
     * @param isIndeterminate used to determine if application should be rendered as indeterminate (partially selected).
     * @param isEffective     used to determine if application is an effective application of a different application.
     *                        I.e. B isEffective to A if adding user to A means that they implicitly will get access to B via common groups.
     * @param directoryId     id of the user directory to check in to determine nested group dependencies
     */
    private List<ApplicationSelection> getApplicationsFor(Predicate<ApplicationRole> isSelected, Predicate<ApplicationRole> isIndeterminate, Predicate<ApplicationRole> isEffective, Optional<Long> directoryId) {
        final Set<ApplicationSelection> applicationSelection = Sets.newHashSet();
        final Set<ApplicationRole> roles = applicationRoleManager.getRoles();
        final Option<ApplicationRole> coreRole = applicationRoleManager.getRole(CORE);

        for (final ApplicationRole role : roles) {
            final String displayName;
            if (coreRole.isDefined() && !role.getKey().equals(CORE)) {
                displayName = i18nHelper.getText("admin.adduser.application.selection.name.includes.core",
                        role.getName(), coreRole.get().getName());
            } else {
                displayName = i18nHelper.getText("admin.adduser.application.selection.name.without.core",
                        role.getName());
            }
            final Optional<ValidationResult> errorMessage = validateRole(role, EnumSet.allOf(ValidationScope.class), ValidationParams.withDirectory(directoryId));
            final boolean selected = isSelected.test(role);
            final boolean selectable = !errorMessage.isPresent();
            final String message = errorMessage.isPresent() ? errorMessage.get().getMessage() : null;
            final String markup = errorMessage.isPresent() ? errorMessage.get().getMessageMarkup() : null;
            final boolean indeterminate = !selected && isIndeterminate.test(role);
            final boolean effective = indeterminate && isEffective.test(role);
            final boolean deselectable = (selected || indeterminate) && errorMessage.isPresent();
            final Set<EffectiveApplication> effectiveApplicationKeys = groupsToApplicationsSeatingHelper
                    .findEffectiveApplications(directoryId, ImmutableSet.of(role)).stream()
                    .map(effectiveRole -> new EffectiveApplication(effectiveRole.getKey(), effectiveRole.getName()))
                    .collect(CollectorsUtil.toImmutableSet());

            applicationSelection.add(new ApplicationSelection(
                    role.getKey(),
                    role.getName(),
                    displayName,
                    message,
                    markup,
                    selectable,
                    selected,
                    role.isDefined(),
                    indeterminate,
                    effective,
                    deselectable,
                    effectiveApplicationKeys
            ));
        }
        return Ordering.natural().sortedCopy(applicationSelection);
    }

    @Nonnull
    @Override
    public List<GroupView> getUserGroups(@Nonnull final ApplicationUser applicationUser) {
        final MembershipQuery<Group> membershipQuery = QueryBuilder.queryFor(Group.class, EntityDescriptor.group())
                .parentsOf(EntityDescriptor.user())
                .withName(applicationUser.getName())
                .returningAtMost(EntityQuery.ALL_RESULTS);
        return StreamSupport.stream(crowdService.search(membershipQuery).spliterator(), false)
                .map(group -> new GroupView(group.getName(), getGroupLabels(group, applicationUser)))
                .collect(CollectorsUtil.toImmutableList());
    }

    private List<GroupLabelView> getGroupLabels(Group group, ApplicationUser user) {
        return groupLabels.getGroupLabels(group, Optional.<Long>of(user.getDirectoryId()));
    }

    private Set<Group> getSelectedGroups(final @Nonnull Set<ApplicationKey> selectedApplicationKeys) {
        return selectedApplicationKeys.stream()
                .map(applicationRoleManager::getRole)
                .filter(Option::isDefined)
                .map(Option::get)
                .map(ApplicationRole::getDefaultGroups)
                .flatMap(Set::stream)
                .collect(CollectorsUtil.toImmutableSet());
    }

    /**
     * Validates if creating a user with default application access in given directory won't exceed license limit
     *
     * @param directoryId the Directory ID, or {@link java.util.Optional#empty()} if default directory should be checked
     * @return a collection of errors
     */
    @Nonnull
    @Override
    public Collection<String> validateDefaultApplications(EnumSet<ValidationScope> validationScope, Optional<Long> directoryId) {
        return validateApplicationKeys(directoryId, applicationRoleManager.getDefaultApplicationKeys(), validationScope);
    }

    /**
     * Validates if creating a user with given application access in given directory won't exceed license limit
     *
     * @param directoryId     the Directory ID, or {@link java.util.Optional#empty()} if default directory should be checked
     * @param applicationKeys validated application keys
     * @return a collection of errors
     */
    @Nonnull
    @Override
    public Collection<String> validateApplicationKeys(@Nonnull final Optional<Long> directoryId, @Nonnull final Set<ApplicationKey> applicationKeys) {
        return validateApplicationKeys(directoryId, applicationKeys, EnumSet.allOf(ValidationScope.class));
    }

    @Nonnull
    @Override
    public Collection<String> validateApplicationKeys(@Nonnull final ApplicationUser user, @Nonnull final Set<ApplicationKey> applicationKeys) {
        return validateApplicationKeys(ValidationParams.withUser(user), applicationKeys, EnumSet.allOf(ValidationScope.class));
    }

    @Nonnull
    @Override
    public Collection<String> validateApplicationKeys(@Nonnull final Optional<Long> directoryId, @Nonnull final Set<ApplicationKey> applicationKeys, @Nonnull final EnumSet<ValidationScope> validationScope) {
        return validateApplicationKeys(ValidationParams.withDirectory(directoryId), applicationKeys, validationScope);
    }

    @Nonnull
    private Collection<String> validateApplicationKeys(@Nonnull final ValidationParams validationParams, @Nonnull final Set<ApplicationKey> applicationKeys, @Nonnull final EnumSet<ValidationScope> validationScope) {
        final Collection<String> errors = new ArrayList<>();

        containsNoNulls("applicationKeys", applicationKeys);
        Set<ApplicationRole> applicationRoles = Sets.newHashSet();
        for (ApplicationKey applicationKey : applicationKeys) {
            final Option<ApplicationRole> role = applicationRoleManager.getRole(applicationKey);

            if (role.isEmpty()) {
                errors.add(i18nHelper.getText("admin.errors.user.add.user.application.not.licensed", applicationKey.value()));
            } else {
                applicationRoles.add(role.get());
            }
        }

        final Long directoryOrDefault = validationParams.getDirectoryId().orElseGet(() ->
                        userManager.getDefaultCreateDirectory().map(Directory::getId).orElse(null)
        );
        if (directoryOrDefault == null) {
            errors.add(i18nHelper.getText("admin.errors.no.writable.directory"));
            return ImmutableList.copyOf(errors);
        }

        //check whether accidentally we are not giving more permissions
        final Set<ApplicationRole> applicationsEffectivelyGranted = groupsToApplicationsSeatingHelper.findEffectiveApplications(Optional.of(directoryOrDefault), applicationRoles);

        // effectively granted must contain all requested application roles
        if (validationScope.contains(ValidationScope.ACCESS) &&
                !applicationsEffectivelyGranted.containsAll(applicationRoles)) {
            errors.add(i18nHelper.getText("admin.errors.user.add.user.application.access.effectively.not.granted"));
            return ImmutableList.copyOf(errors);
        }

        for (ApplicationRole applicationRole : applicationsEffectivelyGranted) {
            final Optional<ValidationResult> message = validateRole(applicationRole, validationScope, validationParams);
            if (message.isPresent()) {
                errors.add(message.get().getMessage());
            }
        }

        return ImmutableList.copyOf(errors);
    }

    @Override
    public boolean canUserLogin(@Nullable final ApplicationUser applicationUser) {
        return applicationUser != null && authorisationManager.hasUserAccessToJIRA(applicationUser);
    }

    /**
     * Get the default groups of the specified applications that should be used during user creation. The default groups
     * of an application is only returned if the application license permits that a new user can be added to the application.
     * <p>
     * This method is dark ages compatible (pre renaissance) in that if would return the 'JIRA Users' global permissions
     * groups if the license permits a new user.
     *
     * @param applicationKeys the keys that indicate what application access a user requires. This is ignored when in
     *                        dark ages mode (pre renaissance) {@code ApplicationRoleManager#rolesEnabled()}.
     * @return the groups that the new user can be safely added to.
     */
    public Set<Group> getDefaultGroupsForNewUser(final Set<ApplicationKey> applicationKeys) {
        return applicationKeys.stream()
                .filter(appKey -> applicationRoleManager.hasSeatsAvailable(appKey, 1))
                .flatMap(appKey -> applicationRoleManager.getDefaultGroups(appKey).stream())
                .collect(CollectorsUtil.toImmutableSet());
    }

    /**
     * Check the user can be added to the selected application.
     * <p>This check is run when the form is rendered.</p>
     *
     * @param role the application role
     * @return an error message (or {@link com.atlassian.fugue.Option#none()})
     */
    @Nonnull
    private Optional<ValidationResult> validateRole(@Nonnull final ApplicationRole role, @Nonnull final EnumSet<ValidationScope> validationScope, ValidationParams params) {
        return Stream.of(ValidationScope.EXPIRE, ValidationScope.SEATS, ValidationScope.ACCESS)
                .filter(validationScope::contains)
                .map(validationFunctions::get)
                .map(validation -> validation.validate(role, params))
                .filter(Optional::isPresent)
                .findFirst()
                .orElse(Optional.<ValidationResult>empty());
    }

    /**
     * Check if given application role has seats available for new user
     *
     * @param role   the application role
     * @param params validation parameters
     * @return an error message (or {@link com.atlassian.fugue.Option#none()})
     */
    @VisibleForTesting
    Optional<ValidationResult> validateRoleSeats(@Nonnull final ApplicationRole role, ValidationParams params) {
        if (params.hasUser() && applicationRoleManager.getRolesForUser(params.getUser().get()).contains(role)) {
            //User is already in given application, all good here
            return Optional.empty();
        }

        if (!applicationRoleManager.hasSeatsAvailable(role.getKey(), 1)) {
            final String message = i18nHelper.getText("admin.errors.user.add.user.application.license.limit.reached", role.getName(), "", "", "", "");
            final String userBrowserStart = String.format(LINK_START, getUserBrowserUrl());
            final String versionsAndLicensesStart = String.format(LINK_START, getVersionsAndLicensesUrl());
            final String markup = i18nHelper.getText("admin.errors.user.add.user.application.license.limit.reached", role.getName(), userBrowserStart, LINK_END, versionsAndLicensesStart, LINK_END);

            return Optional.of(new ValidationResult(message, markup));
        }

        return Optional.empty();
    }

    /**
     * Check if given application role access is configured properly
     *
     * @param role   the application role
     * @param params validation parameters
     * @return an error message (or {@link com.atlassian.fugue.Option#none()})
     */
    private Optional<ValidationResult> validateRoleAccess(@Nonnull final ApplicationRole role, ValidationParams params) {
        if (role.getDefaultGroups().isEmpty()) {
            final String message = i18nHelper.getText("admin.errors.user.add.user.application.no.default.group", role.getName(), "", "");
            final String start = String.format(LINK_START, getApplicationAssessUrl());
            final String markup = i18nHelper.getText("admin.errors.user.add.user.application.no.default.group", role.getName(), start, LINK_END);

            return Optional.of(new ValidationResult(message, markup));
        }

        if (params.getDirectoryId().isPresent()) {
            Directory directory = userManager.getDirectory(params.getDirectoryId().get());
            if (!directory.getAllowedOperations().contains(OperationType.CREATE_GROUP)) {
                final String message = i18nHelper.getText("admin.errors.directory.fully.read.only", directory.getName());
                return Optional.of(new ValidationResult(message, message));
            }
        }

        return Optional.empty();
    }

    /**
     * Check if given application role's license has expired
     *
     * @param role the application role
     * @return an error message (or {@link com.atlassian.fugue.Option#none()})
     */
    private Optional<ValidationResult> validateLicenseExpire(@Nonnull final ApplicationRole role, ValidationParams params) {
        if (!role.isDefined()) {
            return Optional.empty();
        }

        final Option<LicenseDetails> license = jiraLicenseManager.getLicense(role.getKey());
        if (license.isEmpty() || license.get().isExpired()) {
            final String message = i18nHelper.getText("admin.errors.user.add.user.application.license.expired", role.getName(), "", "");
            final String start = String.format(LINK_START, getVersionsAndLicensesUrl());
            final String markup = i18nHelper.getText("admin.errors.user.add.user.application.license.expired", role.getName(), start, LINK_END);

            return Optional.of(new ValidationResult(message, markup));
        }
        return Optional.empty();
    }

    private String getApplicationAssessUrl() {
        return baseUrl.getBaseUrl() + "/secure/admin/ApplicationAccess.jspa";
    }

    private String getUserBrowserUrl() {
        return baseUrl.getBaseUrl() + "/secure/admin/user/UserBrowser.jspa";
    }

    private String getVersionsAndLicensesUrl() {
        return baseUrl.getBaseUrl() + "/plugins/servlet/applications/versions-licenses";
    }

    @VisibleForTesting
    static class ValidationResult {
        private final String message;
        private final String messageMarkup;

        ValidationResult(String message, String messageMarkup) {
            this.message = message;
            this.messageMarkup = messageMarkup;
        }

        public String getMessage() {
            return message;
        }

        public String getMessageMarkup() {
            return messageMarkup;
        }
    }

    @VisibleForTesting
    static class ValidationParams {
        private final Optional<Long> directoryId;
        private final Optional<ApplicationUser> user;

        private ValidationParams(final Optional<ApplicationUser> user, final Optional<Long> directoryId) {
            this.user = user;
            this.directoryId = directoryId;
        }

        public static ValidationParams withUser(ApplicationUser user) {
            return new ValidationParams(Optional.ofNullable(user), Optional.<Long>empty());
        }

        public static ValidationParams withDirectory(Optional<Long> directoryId) {
            return new ValidationParams(Optional.empty(), directoryId);
        }

        public static ValidationParams empty() {
            return new ValidationParams(Optional.empty(), Optional.empty());
        }

        public Optional<Long> getDirectoryId() {
            return Stream.of(user.map(ApplicationUser::getDirectoryId), directoryId)
                    .filter(Optional::isPresent)
                    .findFirst()
                    .orElse(Optional.<Long>empty());
        }

        public Optional<ApplicationUser> getUser() {
            return user;
        }

        public boolean hasUser() {
            return user.isPresent();
        }
    }

}
