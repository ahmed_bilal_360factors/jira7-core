package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.index.request.ReindexRequestType;

/**
 * Indexes worklogs in their own index so they are JQL searchable by comments.
 * Update is performed only if the time tracking option is enabled and there are any worklogs in the instance.
 */
public class UpgradeTask_Build70023 extends AbstractReindexUpgradeTask {
    @Override
    public void doUpgrade(final boolean setupMode) throws Exception {
        getReindexRequestService().requestReindex(ReindexRequestType.DELAYED, WORKLOG_ONLY, NO_SHARED_ENTITIES);
    }

    @Override
    public int getBuildNumber() {
        return 70023;
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        return false;
    }

    @Override
    public String getShortDescription() {
        return "Indexes worklog comments so they can be text-searched with JQL.";
    }
}
