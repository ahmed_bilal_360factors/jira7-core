package com.atlassian.jira.index.property;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.bc.dataimport.ImportCompletedEvent;
import com.atlassian.jira.event.ComponentManagerStartedEvent;
import com.atlassian.jira.plugin.index.EntityPropertyIndexDocumentModuleDescriptor;
import com.atlassian.jira.plugin.util.PluginModuleTrackerFactory;
import com.atlassian.ozymandias.SafePluginPointAccess;
import com.atlassian.plugin.tracker.PluginModuleTracker;
import com.atlassian.tenancy.api.event.TenantArrivedEvent;
import com.google.common.base.Function;

import java.util.List;
import java.util.stream.StreamSupport;

@EventComponent
public class PluginIndexConfigurationInitializer {
    private final PluginModuleTracker<Void, EntityPropertyIndexDocumentModuleDescriptor> moduleTracker;

    public PluginIndexConfigurationInitializer(final PluginModuleTrackerFactory pluginModuleTrackerFactory) {
        moduleTracker = pluginModuleTrackerFactory.create(EntityPropertyIndexDocumentModuleDescriptor.class);
    }

    public List<String> initialisePluginIndexConfiguration() {

        final Iterable<EntityPropertyIndexDocumentModuleDescriptor> epIndexModules = moduleTracker.getModuleDescriptors();
        return StreamSupport.stream(epIndexModules.spliterator(), false)
                .map(epIndexModule -> {
                    SafePluginPointAccess.safe(new Function<EntityPropertyIndexDocumentModuleDescriptor, Void>() {
                        @Override
                        public Void apply(final EntityPropertyIndexDocumentModuleDescriptor input) {
                            //those modules may have not initialized before now
                            input.init();
                            return null;
                        }
                    }).apply(epIndexModule);
                    return epIndexModule.getCompleteKey();
                }).collect(CollectorsUtil.toImmutableList());
    }

    @EventListener
    public void onComponentManagerStartedEvent(final ComponentManagerStartedEvent ignore) {
        //Note to future developers changing when ComponentManagerStartedEvent is triggered
        //You need to invoke this method in the state when
        // plugins system was started
        // and all JIRA components were initialized
        // but *before* JIRA attempts to do any reindex.
        initialisePluginIndexConfiguration();
    }

    @EventListener
    public void onTenantArrivedEventEvent(final TenantArrivedEvent ignore) {
        //this is essential for instant on
        initialisePluginIndexConfiguration();
    }

    @EventListener
    public void onImportCompletedEventEvent(final ImportCompletedEvent ignore) {
        //this is needed for quick restore in tests
        initialisePluginIndexConfiguration();
    }
}
