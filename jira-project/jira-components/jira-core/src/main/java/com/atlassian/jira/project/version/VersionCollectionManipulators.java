package com.atlassian.jira.project.version;

import com.atlassian.jira.util.Function;
import com.atlassian.jira.util.Longs;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;
import java.util.Objects;
import java.util.function.Consumer;

import static com.atlassian.collectors.CollectorsUtil.toImmutableListWithSizeOf;
import static com.atlassian.collectors.CollectorsUtil.toNewArrayListWithSizeOf;

@ParametersAreNonnullByDefault
public class VersionCollectionManipulators {
    /**
     * Gets a function than in given version collection replaces all occurences of versionToRemove with version ToSwap
     *
     * @param versionToRemove The version to remove.
     * @param versionToSwap   The version being swapped in.  May be null (in which case nothing gets swapped in).
     */
    public static Function<Collection<Version>, Collection<Version>> versionReplacer(final Version versionToRemove, final Version versionToSwap) {
        final Long versionToRemoveId = versionToRemove.getId();

        return (versions) -> versions == null ?
                null :
                versions.stream()
                        .map(input -> Longs.nullableLongsEquals(versionToRemoveId, input.getId()) ? versionToSwap : input)
                        .collect(toNewArrayListWithSizeOf(versions));
    }

    /**
     * Gets a function than in given version collection removes all occurences of versionToRemove
     *
     * @param versionToRemove The version to remove.
     */
    public static Function<Collection<Version>, Collection<Version>> versionRemover(final Version versionToRemove) {
        final Long versionToRemoveId = versionToRemove.getId();

        return (versions) -> versions == null ?
                null :
                versions.stream()
                        .filter(input -> !Longs.nullableLongsEquals(versionToRemoveId, input.getId()))
                        .collect(toNewArrayListWithSizeOf(versions));
    }

    /**
     * Transforms given value with given function. If value is changed (nullability
     * changed or transformed value no longer {@link Object#equals(Object)} original value) after
     * transformation it's passed to a given consumer.
     *
     * @param originalValue original value
     * @param transformer   value transformation
     * @param valueConsumer object that accepts changed value
     * @param <T>           original value type
     * @param <T2>          value type after transformation
     */
    public static <T, T2> void updateValueIfChangedAfterTranformation(final T originalValue, final Function<T, T2> transformer, final Consumer<T2> valueConsumer) {
        T2 modifiedValue = transformer.get(originalValue);

        if (!Objects.equals(originalValue, modifiedValue)) {
            valueConsumer.accept(modifiedValue);
        }
    }
}
