package com.atlassian.jira.avatar;

import com.atlassian.core.util.thumbnail.Thumber;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.icon.IconOwningObjectId;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.io.MediaConsumer;
import com.atlassian.jira.io.ResourceData;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.plugin.icon.IconTypeDefinition;
import com.atlassian.jira.plugin.icon.IconTypeDefinitionFactory;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.tenancy.TenantAware;
import com.atlassian.jira.tenancy.TenantInfo;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.Consumer;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.vcache.RequestCache;
import com.atlassian.vcache.VCacheFactory;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.jira.config.properties.APKeys.JIRA_ANONYMOUS_USER_AVATAR_ID;
import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;
import static com.atlassian.jira.user.util.Users.isAnonymous;

/**
 * Manager for Avatars.
 *
 * @since v4.0
 */
public class AvatarManagerImpl implements AvatarManager {
    private static final Logger log = LoggerFactory.getLogger(AvatarManagerImpl.class);
    private static final String AVATAR_CLASSPATH_PREFIX = "/avatars/";

    private final AvatarStore store;
    private final ApplicationProperties applicationProperties;
    private final GlobalPermissionManager globalPermissionManager;
    private final PermissionManager permissionManager;
    private final ImageScaler scaler;
    private final AvatarTagger avatarTagger;
    private final EventPublisher eventPublisher;
    private final AvatarImageDataStorage avatarImageDataStorage;
    private final IconTypeDefinitionFactory iconTypeFactory;
    private final AvatarTranscoder avatarTranscoder;

    @TenantInfo(TenantAware.TENANTED)
    private final RequestCache<IconType, Long> iconDefaultIDMap;

    public AvatarManagerImpl(AvatarStore store,
                             ApplicationProperties applicationProperties,
                             GlobalPermissionManager globalPermissionManager,
                             AvatarTagger avatarTagger,
                             AvatarImageDataStorage avatarImageDataStorage,
                             PermissionManager permissionManager, EventPublisher eventPublisher,
                             IconTypeDefinitionFactory iconTypeFactory,
                             VCacheFactory vCacheFactory,
                             AvatarTranscoder avatarTranscoder) {
        this.store = store;
        this.applicationProperties = applicationProperties;
        this.globalPermissionManager = globalPermissionManager;
        this.avatarTagger = avatarTagger;
        this.eventPublisher = eventPublisher;
        this.scaler = new ImageScaler();
        this.avatarImageDataStorage = avatarImageDataStorage;
        this.permissionManager = permissionManager;
        this.iconTypeFactory = iconTypeFactory;
        this.avatarTranscoder = avatarTranscoder;

        iconDefaultIDMap = vCacheFactory.getRequestCache(AvatarManagerImpl.class.getName() + ".iconDefaultIDMap");
    }

    public Avatar getById(Long avatarId) {
        Assertions.notNull("avatarId", avatarId);
        return store.getById(avatarId);
    }

    public Avatar getByIdTagged(Long avatarId) {
        Assertions.notNull("avatarId", avatarId);
        return store.getByIdTagged(avatarId);
    }

    public boolean delete(Long avatarId) {
        return delete(avatarId, true);
    }

    public boolean delete(Long avatarId, boolean alsoDeleteAvatarFile) {
        Assertions.notNull("avatarId", avatarId);
        final Avatar avatar = store.getById(avatarId);
        if (avatar == null) {
            return false;
        }
        if (alsoDeleteAvatarFile) {
            final Avatar.Size[] imageSizes = Avatar.Size.values();
            final File[] avatarFiles = new File[imageSizes.length];
            for (int i = 0; i < imageSizes.length; i++) {
                avatarFiles[i] = getAvatarFile(avatar, imageSizes[i]);
                deleteFile(avatarFiles[i]);
            }
            eventPublisher.publish(new JiraHomeChangeEvent(JiraHomeChangeEvent.Action.FILE_DELETED,
                    JiraHomeChangeEvent.FileType.AVATAR, avatarFiles));
        }
        return store.delete(avatarId);
    }

    private void deleteFile(File file) {
        if (!file.delete()) {
            file.deleteOnExit();
        }
    }

    public void update(Avatar avatar) {
        Assertions.notNull("avatar", avatar);
        Assertions.notNull("avatar.id", avatar.getId());
        store.update(avatar);
    }

    @Override
    @Nonnull
    public Avatar create(Avatar avatar) {
        Assertions.notNull("avatar", avatar);
        Assertions.stateTrue("avatar.id must be null", avatar.getId() == null);
        return store.create(avatar);
    }

    @Override
    @Nonnull
    public Avatar create(final Avatar avatar, final InputStream imageData, final Selection croppingSelection)
            throws DataAccessException, IOException {
        Assertions.notNull("avatar", avatar);
        if (avatar.isSystemAvatar()) {
            throw new IllegalArgumentException("System avatars cannot be created with custom image data");
        }
        Assertions.notNull("imageData", imageData);

        File avatarFile = null;
        try {
            final Avatar created = create(avatar);
            final File largeAvatarFile = avatarFile = processImage(created, imageData, croppingSelection, Avatar.Size.largest());
            for (Avatar.Size size : Avatar.Size.values()) {
                if (Avatar.Size.largest().equals(size)) {
                    //already generated this one.
                    continue;
                }
                avatarFile = processImage(created, FileUtils.openInputStream(largeAvatarFile), null, size);
            }
            return created;
        } catch (RuntimeException failedCreate) {
            handleCreationFailure(avatarFile);
            throw failedCreate;
        }
    }

    @Override
    @Nonnull
    public Avatar create(Avatar.Type avatarType, @Nonnull String owningObjectId, @Nonnull AvatarImageDataProvider imageDataProvider)
            throws IOException {
        return create(toIconType(avatarType), new IconOwningObjectId(owningObjectId), imageDataProvider);
    }

    @Override
    @Nonnull
    public Avatar create(@Nonnull IconType iconType, @Nonnull IconOwningObjectId owningObjectId, @Nonnull AvatarImageDataProvider imageDataProvider)
            throws IOException {
        Assertions.notNull("iconType", iconType);
        Assertions.notNull("owningObjectId", owningObjectId);
        Assertions.notNull("imageDataProvider", imageDataProvider);

        Avatar avatarStub = new AvatarImpl(
                null,
                avatarImageDataStorage.getNextFilenameStub() + ".png",
                "image/png",
                iconType,
                owningObjectId.getId(),
                false);

        final Avatar newAvatar = store.create(avatarStub);
        try {
            avatarImageDataStorage.storeAvatarFiles(newAvatar, imageDataProvider);
        } catch (IOException e) {
            store.delete(newAvatar.getId());
            throw e;
        } catch (RuntimeException e) {
            store.delete(newAvatar.getId());
            throw e;
        }

        return newAvatar;
    }

    @Override
    @Nonnull
    public Avatar create(String fileName, String contentType, IconType iconType, IconOwningObjectId owner, InputStream imageData, Selection croppingSelection)
            throws DataAccessException, IOException {
        Assertions.notNull("fileName", fileName);
        Assertions.notNull("contentType", contentType);
        Assertions.notNull("iconType", iconType);
        Assertions.notNull("owner", owner);
        Assertions.notNull("imageData", imageData);

        return create(AvatarImpl.createCustomAvatar(fileName, contentType, owner.getId(), iconType),
                imageData, croppingSelection);
    }


    @Override
    @Nonnull
    public Avatar create(String fileName, String contentType, Project project, InputStream imageData, Selection croppingSelection)
            throws DataAccessException, IOException {
        Assertions.notNull("fileName", fileName);
        Assertions.notNull("contentType", contentType);
        Assertions.notNull("project", project);
        Assertions.notNull("imageData", imageData);

        String owner = project.getId().toString();
        return create(AvatarImpl.createCustomAvatar(fileName, contentType, owner, IconType.PROJECT_ICON_TYPE),
                imageData, croppingSelection);
    }

    @Override
    @Nonnull
    public Avatar create(String fileName, String contentType, ApplicationUser owner, InputStream imageData, Selection croppingSelection)
            throws DataAccessException, IOException {
        Assertions.notNull("fileName", fileName);
        Assertions.notNull("contentType", contentType);
        Assertions.notNull("owner", owner);
        Assertions.notNull("imageData", imageData);

        String user = owner.getKey();

        return create(AvatarImpl.createCustomAvatar(fileName, "image/png", user, IconType.USER_ICON_TYPE), imageData, croppingSelection);
    }

    private void handleCreationFailure(final File avatarFile) {
        try {
            if (avatarFile != null && avatarFile.exists() && !avatarFile.delete()) {
                log.warn("Created avatar file '" + avatarFile
                        + "' but then failed to store to db. Failed to delete the file!");
            }
        } catch (RuntimeException failedDeleteFile) {
            log.warn("Created avatar file '" + avatarFile
                    + "' but then failed to store to db. Failed to delete the file!", failedDeleteFile);
        }
    }

    File processImage(final Avatar created, final InputStream imageData, final Selection croppingSelection, final Avatar.Size size)
            throws IOException {
        RenderedImage image = scaler.getSelectedImageData(new Thumber().getImage(imageData), croppingSelection, size.getPixels());
        File file = createAvatarFile(created, size);
        avatarTagger.saveTaggedAvatar(image, AVATAR_IMAGE_FORMAT_FULL.getName(), file);
        eventPublisher.publish(new JiraHomeChangeEvent(JiraHomeChangeEvent.Action.FILE_ADD,
                JiraHomeChangeEvent.FileType.AVATAR, file));
        return file;
    }

    @VisibleForTesting
    File createAvatarFile(Avatar avatar, Avatar.Size size) throws IOException {
        final File base = getAvatarBaseDirectory();
        createDirectoryIfAbsent(base);
        return new File(base, AvatarFilenames.getAvatarFilename(avatar, size));
    }

    File getAvatarFile(final Avatar avatar, final Avatar.Size size) {
        final File base = getAvatarBaseDirectory();
        return new File(base, AvatarFilenames.getAvatarFilename(avatar, size));
    }

    @Override
    @Nonnull
    public File getAvatarBaseDirectory() {
        return avatarImageDataStorage.getAvatarBaseDirectory();
    }

    private void createDirectoryIfAbsent(final File dir) throws IOException {
        if (!dir.exists() && !dir.mkdirs()) {
            throw new IOException("Avatars directory is absent and I'm unable to create it. '" + dir.getAbsolutePath() + "'");
        }
        if (!dir.isDirectory()) {
            throw new IllegalStateException("Avatars directory cannot be created due to an existing file. '" + dir.getAbsolutePath() + "'");
        }
    }

    @Override
    @Nonnull
    public List<Avatar> getAllSystemAvatars(Avatar.Type type) {
        return getAllSystemAvatars(toIconType(type));
    }

    @Override
    @Nonnull
    public List<Avatar> getAllSystemAvatars(IconType iconType) {
        try {
            return Lists.newArrayList(Iterables.filter(store.getAllSystemAvatars(iconType), isUsableAvatarPredicate(iconType)));
        } catch (UnsupportedOperationException e) {
            // SW-1977 - System avatars are not a concept in Atlassian ID. If this exception got thrown, there will be none.
            return Lists.newArrayList();
        }
    }

    private Predicate<? super Avatar> isUsableAvatarPredicate(final IconType iconType) {
        return new Predicate<Avatar>() {
            @Override
            public boolean apply(@Nullable final Avatar input) {
                if (null == input) {
                    return false;
                }
                if (iconType.equals(IconType.PROJECT_ICON_TYPE)) {
                    return !Avatar.demotedSystemProjectAvatars.contains(input.getFileName());
                }
                if (iconType.equals(IconType.USER_ICON_TYPE)) {
                    return !Avatar.demotedSystemUserAvatars.contains(input.getFileName());
                }
                return true;
            }
        };
    }

    @Override
    @Nonnull
    public List<Avatar> getCustomAvatarsForOwner(final Avatar.Type type, final String ownerId) {
        return getCustomAvatarsForOwner(toIconType(type), ownerId);
    }

    @Override
    @Nonnull
    public List<Avatar> getCustomAvatarsForOwner(final IconType iconType, final String ownerId) {
        return store.getCustomAvatarsForOwner(iconType, ownerId);
    }

    public boolean isAvatarOwner(final Avatar avatar, final String ownerId) {
        Assertions.notNull("avatar", avatar);
        Assertions.notNull("owner", ownerId);
        return getCustomAvatarsForOwner(avatar.getIconType(), ownerId).contains(avatar);
    }

    private void processAvatarData(final Avatar avatar, final MediaConsumer mediaConsumer, final Avatar.Size size, final AvatarFormatPolicy formatPolicy)
            throws IOException {
        // There could be issues where no avatar exists in the database for a given id. Need to ensure we can handle this.
        final Option<Avatar> avatarOption = transformToJIRAAvatar(avatar);

        if (avatarOption.isEmpty()) {
            return;
        }

        final ResourceData resourceData = formatPolicy.getData(
                avatarOption.get(),
                () -> getAvatarInputStream(avatarOption.get(), size),
                () -> {
                    try (InputStream svgInputStream = getAvatarInputStream(avatarOption.get(), size)) {
                        final File transcodedAvatarFile = avatarTranscoder.getOrCreateRasterizedAvatarFile(avatarOption.get(), size,
                                svgInputStream);
                        return new FileInputStream(transcodedAvatarFile);
                    }
                });

        try (final InputStream is = resourceData.getInputStream()) {
            mediaConsumer.consumeContentType(resourceData.getContentType());
            mediaConsumer.consumeData(is);
        }
    }

    private InputStream getAvatarInputStream(final Avatar icon, Avatar.Size size) throws IOException {
        InputStream data = null;
        if (icon.isSystemAvatar()) {
            data = getIconTypeDefinitionStrict(icon.getIconType()).getSystemIconImageProvider().getSystemIconInputStream(icon, size);
            if (data == null) {
                log.error("Failed to get image data for system icon " + icon.getFileName());
                throw new IOException("File not found");
            }
        } else {
            final File file = getOrGenerateAvatarFile(icon, size);
            data = new FileInputStream(file);
        }
        return data;
    }

    /*
     * Transforms a {@link com.atlassian.jira.avatar.PluginAvatarAdapter} avatar to a proper JIRA avatar looked up via the 
     * database if possible.
     */
    private Option<Avatar> transformToJIRAAvatar(final Avatar avatar) {
        // this could fail to get id from avatar.getId() if for some reason we try to process avatar data via
        // this (old) API for a new pluggable avatar.  In theory we should never hit this method since 'new' avatars
        // should simply be included via a URL or use the {@code com.atlassian.plugins.avatar.PluginAvatar#getBytes()} method.
        return option(avatar.getId())
                .flatMap(avatarId -> option(getByIdTagged(avatarId)));
    }

    private File getOrGenerateAvatarFile(final Avatar avatar, final Avatar.Size size) throws IOException {
        final File file = getAvatarFile(avatar, size);
        //if this file doesn't exist and we requested something other than the largest size lets
        //try to generate a smaller image.
        if (!file.exists()) {
            File largeFile = getOrGenerateLargerAvatarFile(avatar, size);
            //generate a smaller image file for the avatar requested and return that!
            if (largeFile != null && largeFile.exists()) {
                return processImage(avatar, FileUtils.openInputStream(largeFile), null, size);
            }
        }
        return file;
    }

    /**
     * If we don't have a large file to downscale images from, we'll create a new larger file to do so from.
     *
     * @param avatar    the avatar to find the largest file we have on hand for.
     * @param sizeToGet the size we're either hoping to find an asset for, or generate an image at.
     * @return either a new avatar image based on the largest file we have to scale from, or the largest file we have on
     * hand, or null.
     */
    private File getOrGenerateLargerAvatarFile(final Avatar avatar, final Avatar.Size sizeToGet) {
        File largestFileOnHand = null;
        File newFile = null;
        Avatar.Size availableSize = null;

        for (Avatar.Size size : Avatar.Size.inPixelOrder()) {
            File avatarFile = getAvatarFile(avatar, size);
            if (avatarFile.exists()) {
                largestFileOnHand = avatarFile;
                availableSize = size;
            }
            if (null != availableSize && availableSize.getPixels() > sizeToGet.getPixels()) {
                break;
            }
        }

        try {
            // Upscale or downscale the largest file we have on hand (assuming there is one) to the largest size we need.
            if (largestFileOnHand != null) {
                newFile = processImage(avatar, FileUtils.openInputStream(largestFileOnHand), null, sizeToGet);
            }
        } catch (IOException ioe) {
            log.error(String.format("Failed to generate new image for '%s' from image '%s'", sizeToGet, largestFileOnHand), ioe);
        }

        return (newFile != null) ? newFile : ((largestFileOnHand != null) ? largestFileOnHand : null);
    }

    @Override
    public void readAvatarData(final Avatar avatar, ImageSize size, final Consumer<InputStream> dataAccessor) throws IOException {
        readAvatarData(avatar, size.getSize(), dataAccessor);
    }

    @Override
    public void readAvatarData(@Nonnull final Avatar avatar, @Nonnull final Avatar.Size size, @Nonnull final Consumer<InputStream> dataAccessor)
            throws IOException {
        readAvatarData(avatar, size, AvatarFormatPolicy.createOriginalDataPolicy(), new MediaConsumer(dataAccessor));
    }

    @Override
    public void readAvatarData(@Nonnull final Avatar avatar, @Nonnull final Avatar.Size size, @Nonnull final AvatarFormatPolicy avatarFormatPolicy, @Nonnull final MediaConsumer mediaConsumer)
            throws IOException {
        processAvatarData(avatar, mediaConsumer, size, avatarFormatPolicy);
    }

    @Override
    public Long getDefaultAvatarId(Avatar.Type ofType) {
        return getDefaultAvatarId(toIconType(ofType));
    }

    private Long loadDefaultAvatarId(@Nonnull IconType iconType) {
        Long result = null;

        List<Avatar> avatars = store.getSystemAvatarsForFilename(iconType, iconTypeFactory.getDefaultSystemIconFilename(iconType));
        if (avatars != null && !avatars.isEmpty()) {
            result = avatars.get(0).getId();
        }

        if (result == null) {
            // fallback to old behaviour. This could happen if you have an empty DB with no system avatars. Some tests
            // will do this, and expect to get an ID back.
            Avatar.Type oldType = Avatar.Type.getByIconType(iconType);

            if (oldType != null && (oldType == Avatar.Type.ISSUETYPE || oldType == Avatar.Type.PROJECT || oldType == Avatar.Type.USER)) {
                result = oldType.getDefaultId(applicationProperties);
            }
        }

        return result;
    }

    @Override
    public Long getDefaultAvatarId(@Nonnull IconType iconType) {
        Assertions.notNull("iconType", iconType);
        Long result = iconDefaultIDMap.get(iconType, () -> loadDefaultAvatarId(iconType));

        return result;
    }

    @Override
    public Avatar getDefaultAvatar(IconType iconType) {
        Long id = getDefaultAvatarId(iconType);
        if (id == null) {
            return null;
        }
        return getById(id);
    }

    @Override
    public boolean isValidIconType(@Nonnull IconType iconType) {
        if (iconTypeFactory.getDefinition(iconType) != null) {
            return true;
        } else {
            return false;
        }
    }

    private IconType toIconType(@Nonnull Avatar.Type avatarType) {
        if (avatarType == Avatar.Type.OTHER) {
            return null;
        } else {
            return new IconType(avatarType.getName());
        }
    }

    @Override
    public Long getAnonymousAvatarId() {
        final String avatarId = applicationProperties.getString(JIRA_ANONYMOUS_USER_AVATAR_ID);
        return avatarId != null ? Long.valueOf(avatarId) : null;
    }

    @Override
    public boolean hasPermissionToView(final ApplicationUser remoteUser, final Avatar.Type type, final String ownerId) {
        IconType iconType = toIconType(type);
        Avatar tempAvatar = new DummyAvatar(iconType, ownerId);
        return userCanView(remoteUser, tempAvatar);
    }

    @Override
    public boolean hasPermissionToView(ApplicationUser remoteUser, Project project) {
        //can't edit non-existent project!
        if (project == null) {
            return false;
        }
        IconType iconType = IconType.PROJECT_ICON_TYPE;
        Avatar tempAvatar = new DummyAvatar(iconType, String.valueOf(project.getId()));
        return userCanView(remoteUser, tempAvatar);
    }

    @Override
    public boolean hasPermissionToView(ApplicationUser remoteUser, ApplicationUser owner) {
        IconType iconType = IconType.USER_ICON_TYPE;
        Avatar tempAvatar = new DummyAvatar(iconType, owner.getKey());
        return userCanView(remoteUser, tempAvatar);
    }

    @Override
    public boolean hasPermissionToEdit(final ApplicationUser remoteUser, final Avatar.Type type, final String ownerId) {
        IconType iconType = toIconType(type);
        return userCanCreateFor(remoteUser, iconType, new IconOwningObjectId(ownerId));
    }

    @Override
    public boolean hasPermissionToEdit(ApplicationUser remoteUser, ApplicationUser owner) {
        //only logged in users can modify someone's avatar image!
        if (isAnonymous(remoteUser) || owner == null) {
            return false;
        }
        IconType iconType = IconType.USER_ICON_TYPE;
        return userCanCreateFor(remoteUser, iconType, new IconOwningObjectId(owner.getKey()));
    }

    @Override
    public boolean hasPermissionToEdit(ApplicationUser remoteUser, Project owner) {
        //can't edit non-existent project!
        if (owner == null) {
            return false;
        }
        return globalPermissionManager.hasPermission(ADMINISTER, remoteUser) ||
                permissionManager.hasPermission(ProjectPermissions.ADMINISTER_PROJECTS, owner, remoteUser);
    }

    public static boolean isSvgContentType(final String mimeType) {
        return AvatarManager.SVG_CONTENT_TYPE.equals(mimeType);
    }

    public static boolean isAvatarTranscodeable(@Nonnull final Avatar avatar) {
        return isSvgContentType(avatar.getContentType()) && avatar.isSystemAvatar();
    }

    @Override
    public boolean userCanView(@Nullable ApplicationUser remoteUser, @Nonnull Avatar icon) {
        IconTypeDefinition iconTypeDefinition = getIconTypeDefinition(icon.getIconType());
        return iconTypeDefinition != null && iconTypeDefinition.getPolicy().userCanView(remoteUser, icon);
    }

    @Override
    public boolean userCanDelete(@Nullable ApplicationUser remoteUser, @Nonnull Avatar icon) {
        IconTypeDefinition iconTypeDefinition = getIconTypeDefinition(icon.getIconType());
        return iconTypeDefinition != null && iconTypeDefinition.getPolicy().userCanDelete(remoteUser, icon);
    }

    @Override
    public boolean userCanCreateFor(@Nullable ApplicationUser remoteUser, @Nonnull IconType iconType, @Nonnull IconOwningObjectId owningObjectId) {
        IconTypeDefinition iconTypeDefinition = getIconTypeDefinition(iconType);
        return iconTypeDefinition != null && iconTypeDefinition.getPolicy().userCanCreateFor(remoteUser, owningObjectId);
    }

    @Nullable
    private IconTypeDefinition getIconTypeDefinition(IconType iconType) {
        return iconTypeFactory.getDefinition(iconType);
    }

    @Nonnull
    private IconTypeDefinition getIconTypeDefinitionStrict(IconType iconType) {
        IconTypeDefinition iconTypeDefinition = iconTypeFactory.getDefinition(iconType);
        if (iconTypeDefinition == null) {
            throw new IllegalArgumentException("Unknown IconType '" + iconType + "'");
        } else {
            return iconTypeDefinition;
        }
    }

    // This is used to make legacy permission methods work
    private class DummyAvatar implements Avatar {
        private IconType iconType;
        private String owner;

        public DummyAvatar(IconType iconType, String owner) {
            this.iconType = iconType;
            this.owner = owner;
        }

        @Nonnull
        @Override
        public Type getAvatarType() {
            if (Type.supportsName(iconType.getKey())) {
                return Type.getByName(iconType.getKey());
            } else {
                return Type.OTHER;
            }
        }

        @Nonnull
        @Override
        public IconType getIconType() {
            return iconType;
        }

        @Nonnull
        @Override
        public String getFileName() {
            throw new NotImplementedException("Legacy permissions methods cannot supply the icon file name.");
        }

        @Nonnull
        @Override
        public String getContentType() {
            return "text/plain";
        }

        @Override
        public Long getId() {
            return null;
        }

        @Nonnull
        @Override
        public String getOwner() {
            return owner;
        }

        @Override
        public boolean isSystemAvatar() {
            return false;
        }
    }
}
