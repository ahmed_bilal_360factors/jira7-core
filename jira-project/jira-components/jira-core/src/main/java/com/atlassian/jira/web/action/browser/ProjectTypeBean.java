package com.atlassian.jira.web.action.browser;

import com.atlassian.jira.project.type.ProjectType;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeKeyFormatter;

public class ProjectTypeBean {
    private final String key;
    private final String icon;
    private final String formattedKey;

    private final String id;

    private ProjectTypeBean(ProjectType projectType) {
        ProjectTypeKey key = projectType.getKey();
        this.key = key.getKey() == null ? "" : key.getKey();
        this.icon = projectType.getIcon();
        this.formattedKey = ProjectTypeKeyFormatter.format(key);
        this.id = this.key;
    }

    private ProjectTypeBean(String key, String formattedKey) {
        this.key = key;
        this.formattedKey = formattedKey;
        this.id = key;
        this.icon = null;
    }

    public static ProjectTypeBean create(ProjectType projectType) {
        return new ProjectTypeBean(projectType);
    }

    public static ProjectTypeBean create(String key, String formattedKey) {
        return new ProjectTypeBean(key, formattedKey);
    }

    public String getKey() {
        return key;
    }

    public String getIcon() {
        return icon;
    }

    public String getFormattedKey() {
        return formattedKey;
    }

    public String getId() {
        return id;
    }
}
