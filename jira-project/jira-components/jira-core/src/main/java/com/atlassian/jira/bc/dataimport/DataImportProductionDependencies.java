package com.atlassian.jira.bc.dataimport;

import com.atlassian.crowd.event.migration.XMLRestoreFinishedEvent;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.application.ApplicationConfigurationHelper;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.mail.settings.MailSettings;
import com.atlassian.jira.propertyset.CachingOfBizPropertyEntryStore;
import com.atlassian.jira.upgrade.ConsistencyCheckImpl;
import com.atlassian.jira.upgrade.ConsistencyChecker;
import com.atlassian.jira.upgrade.DataImportUpgradeService;
import com.atlassian.jira.upgrade.FirstDataImportUpgradeService;
import com.atlassian.jira.upgrade.UpgradeService;
import com.atlassian.jira.user.util.RecoveryAdminMapper;
import com.atlassian.jira.util.JiraKeyUtils;
import com.atlassian.jira.util.index.IndexLifecycleManager;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.manager.PluginPersistentState;
import com.atlassian.plugin.manager.PluginPersistentStateStore;
import com.atlassian.sal.api.upgrade.PluginUpgradeManager;
import com.atlassian.scheduler.core.LifecycleAwareSchedulerService;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Set;

/**
 * Provides a number of dependencies used during dataimport.  These *have* to come directly from the componentmanager
 * since they may get swapped out during import.  They should never be injected in this class.
 *
 * @since v4.4
 */
public class DataImportProductionDependencies {
    private static final Logger log = LoggerFactory.getLogger(DataImportProductionDependencies.class);

    //this should not get anything injected.  The wholepoint of this class is to make sure it gets the
    //latest component from the ComponentManager since components may change during import.

    IndexLifecycleManager getIndexLifecycleManager() {
        return ComponentAccessor.getComponentOfType(IndexLifecycleManager.class);
    }

    ConsistencyChecker getConsistencyChecker() {
        return new ConsistencyCheckImpl(ComponentAccessor.getComponent(JohnsonProvider.class));
    }

    /**
     * Obtains upgrade service component depending on the set up context.
     *
     * @param setupMode true if and only if we are in the middle of setup mode aka. JIRA is not set up.
     * @return suitable upgrade service component.
     */
    UpgradeService getUpgradeService(final boolean setupMode) {
        if (setupMode) {
            return ComponentAccessor.getComponentOfType(FirstDataImportUpgradeService.class);
        } else {
            return ComponentAccessor.getComponentOfType(DataImportUpgradeService.class);
        }
    }

    LifecycleAwareSchedulerService getSchedulerService() {
        return ComponentAccessor.getComponent(LifecycleAwareSchedulerService.class);
    }

    PluginEventManager getPluginEventManager() {
        return ComponentAccessor.getPluginEventManager();
    }

    PluginUpgradeManager getPluginUpgradeManager() {
        return ComponentAccessor.getOSGiComponentInstanceOfType(PluginUpgradeManager.class);
    }

    ApplicationConfigurationHelper getApplicationConfigurationHelper() {
        return ComponentAccessor.getComponent(ApplicationConfigurationHelper.class);
    }

    MailSettings getMailSettings() {
        return ComponentAccessor.getComponent(MailSettings.class);
    }

    FeatureManager getFeatureManager() {
        return ComponentAccessor.getComponent(FeatureManager.class);
    }

    void globalRefresh(final boolean quickImport) {
        if (quickImport) {
            log.warn("QuickImport is on, doing a fast refresh.");
            final PluginPersistentStateStore pluginPersistentStateStore = ComponentAccessor.getComponentOfType(PluginPersistentStateStore.class);
            // We need to keep track of which plugins were disabled in the old instance of JIRA, before we clear the cache
            final PluginPersistentState oldState = pluginPersistentStateStore.load();
            // Lets be tricky and clear the CachingOfBizPropertyEntryStore first since so much depends on these
            // through ApplicationProperties(that internally use it to cache its entries) and since caches
            // depend on it (e.g. DefaultServiceManager) (LEGO-866)
            ComponentAccessor.getComponentOfType(CachingOfBizPropertyEntryStore.class).refreshAll();
            // Clear the state of embedded crowd
            ComponentAccessor.getComponentOfType(EventPublisher.class).publish(new XMLRestoreFinishedEvent(this));
            // Clear the state out of JIRA
            ComponentAccessor.getComponentOfType(EventPublisher.class).publish(ClearCacheEvent.INSTANCE);

            // We need to call enable/disable on the plugins which states have changed because of the newly imported data
            syncPluginStateWithNewData(pluginPersistentStateStore, oldState);

            // We need to reset some static classes, which cache their state
            clearStatics();
        } else {
            ManagerFactory.globalRefresh();
        }
    }

    private void syncPluginStateWithNewData(final PluginPersistentStateStore pluginPersistentStateStore, final PluginPersistentState oldState) {
        final Set<String> pluginsToEnable = Sets.newHashSet();
        final Set<String> pluginModulesToEnable = Sets.newHashSet();
        final Set<String> pluginModulesToDisable = Sets.newHashSet();

        // Get the new state of the plugins
        final PluginPersistentState newState = pluginPersistentStateStore.load();
        final PluginAccessor pluginAccessor = ComponentAccessor.getPluginAccessor();
        final PluginController pluginController = ComponentAccessor.getPluginController();

        // Run through all plugins
        final Collection<Plugin> plugins = pluginAccessor.getPlugins();
        for (final Plugin plugin : plugins) {
            // If the plugin state has changed let the plugins system know
            final boolean enabledInOldSystem = oldState.isEnabled(plugin);
            final boolean enabledInNewSystem = newState.isEnabled(plugin);
            if (!enabledInOldSystem && enabledInNewSystem) {
                pluginsToEnable.add(plugin.getKey());
            } else if (enabledInOldSystem && !enabledInNewSystem) {
                pluginController.disablePlugin(plugin.getKey());
            }
            // Run through all the modules and let the plugins system know if the state has changed
            final Collection<ModuleDescriptor<?>> moduleDescriptors = plugin.getModuleDescriptors();
            for (final ModuleDescriptor<?> moduleDescriptor : moduleDescriptors) {
                final boolean moduleEnabledInOldSystem = oldState.isEnabled(moduleDescriptor);
                final boolean moduleEnabledInNewSystem = newState.isEnabled(moduleDescriptor);
                if (!moduleEnabledInOldSystem && moduleEnabledInNewSystem) {
                    pluginModulesToEnable.add(moduleDescriptor.getCompleteKey());
                } else if (moduleEnabledInOldSystem && !moduleEnabledInNewSystem) {
                    pluginModulesToDisable.add(moduleDescriptor.getCompleteKey());
                }
            }
        }
        //enable all plugins at once to ensure the plugin system can enable them in the right order
        pluginController.enablePlugins(pluginsToEnable.toArray(new String[pluginsToEnable.size()]));

        //now that all the plugins are in the right state, run through all modules to put them into the correct state
        for (final String moduleToDisable : pluginModulesToDisable) {
            pluginController.disablePluginModule(moduleToDisable);
        }
        for (final String moduleToEnable : pluginModulesToEnable) {
            pluginController.enablePluginModule(moduleToEnable);
        }
    }

    private void clearStatics() {
        // Data file can contain another value for KeyMatcher, so we need to reset it
        JiraKeyUtils.resetKeyMatcher();
    }

    void refreshSequencer() {
        ComponentAccessor.getOfBizDelegator().refreshSequencer();
    }

    /**
     * Ensures there is a user key for mapping in the database for recovery mode. This call will ensure there is a
     * mapping after setup, setup import and import.
     * <p>
     *     This is implemented here
     * because 1) it needs to use the objects available after the import and 2) needs allows me to mock it out.
     */
    void addRecoveryMapping() {
        new RecoveryAdminMapper().map();
    }
}
