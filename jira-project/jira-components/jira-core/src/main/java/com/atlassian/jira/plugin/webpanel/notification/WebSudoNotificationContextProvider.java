package com.atlassian.jira.plugin.webpanel.notification;

import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.security.websudo.InternalWebSudoManager;
import com.atlassian.jira.web.HttpServletVariables;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

public final class WebSudoNotificationContextProvider implements ContextProvider {
    private final InternalWebSudoManager internalWebSudoManager;
    private final HelpUrls helpUrls;
    private final HttpServletVariables httpServletVariables;

    public WebSudoNotificationContextProvider(final InternalWebSudoManager internalWebSudoManager,
                                              final HelpUrls helpUrls, HttpServletVariables httpServletVariables) {
        this.internalWebSudoManager = notNull("internalWebSudoManager", internalWebSudoManager);
        this.helpUrls = notNull("helpUrls", helpUrls);
        this.httpServletVariables = notNull("httpServletVariables", httpServletVariables);
    }

    @Override
    public void init(final Map<String, String> map) {
    }

    @Override
    public Map<String, Object> getContextMap(final Map<String, Object> map) {
        return ImmutableMap.of("helpUrl", helpUrls.getUrl("websudo").getUrl(),
                "websudoRequest", internalWebSudoManager.isWebSudoRequest(httpServletVariables.getHttpRequest()));
    }
}
