package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.Update;
import com.atlassian.jira.upgrade.AbstractUpgradeTask;
import com.atlassian.jira.upgrade.UpgradeTask;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;

/**
 * Updates user searchers to be user/group searchers and single select searchers to multi-select searchers
 *
 * @since v5.2
 */
public class UpgradeTask_Build809 extends AbstractUpgradeTask {
    private static final String USER_GROUP_SEARCHER = "com.atlassian.jira.plugin.system.customfieldtypes:userpickergroupsearcher";
    private static final String USER_SEARCHER = "com.atlassian.jira.plugin.system.customfieldtypes:userpickersearcher";
    private static final String SINGLE_SELECT_SEARCHER = "com.atlassian.jira.plugin.system.customfieldtypes:selectsearcher";
    private static final String MULTI_SELECT_SEARCHER = "com.atlassian.jira.plugin.system.customfieldtypes:multiselectsearcher";
    private static final String FIELD_NAME = "customfieldsearcherkey";

    private final EntityEngine entityEngine;

    public UpgradeTask_Build809(EntityEngine entityEngine) {
        this.entityEngine = entityEngine;
    }

    @Override
    public int getBuildNumber() {
        return 809;
    }

    @Override
    public String getShortDescription() {
        return "Updating User Searchers to User/Group Searchers and single-select searcher to be multi-select searchers";
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        // Flagged as not downgradable because it was created before the Downgrade Task framework was built.
        return true;
    }

    @Override
    public void doUpgrade(boolean setupMode) {
        entityEngine.execute(Update.into("CustomField").set(FIELD_NAME, USER_GROUP_SEARCHER).whereEqual(FIELD_NAME, USER_SEARCHER));
        entityEngine.execute(Update.into("CustomField").set(FIELD_NAME, MULTI_SELECT_SEARCHER).whereEqual(FIELD_NAME, SINGLE_SELECT_SEARCHER));
    }

    @Override
    public Collection<String> getErrors() {
        return Collections.emptyList();
    }

    @Nullable
    @Override
    public Integer dependsUpon() {
        return 808;
    }

    @Override
    public ScheduleOption getScheduleOption() {
        return ScheduleOption.BEFORE_JIRA_STARTED;
    }
}
