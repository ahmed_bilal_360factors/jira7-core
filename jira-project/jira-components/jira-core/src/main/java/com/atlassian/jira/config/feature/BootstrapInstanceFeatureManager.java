package com.atlassian.jira.config.feature;

import com.atlassian.jira.config.InstanceFeatureManager;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;
import java.util.Set;

/**
 * Simple instance feature manager used during bootstrap. Setup does not use features at all currently.
 *
 * @since v7.1
 */
public class BootstrapInstanceFeatureManager implements InstanceFeatureManager {
    @Override
    public boolean isInstanceFeatureEnabled(@Nonnull String featureKey) {
        return false;
    }

    @Override
    public Set<String> getEnabledFeatureKeys() {
        return ImmutableSet.of();
    }
}
