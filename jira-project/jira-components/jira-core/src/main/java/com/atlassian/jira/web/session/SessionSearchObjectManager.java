package com.atlassian.jira.web.session;

import com.atlassian.jira.issue.pager.NextPreviousPager;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.web.bean.PagerFilter;

/**
 * Provides a common access point for the setting and getting of current search related objects within the current session.
 * <p>
 * Candidates for this are objects which are related to searching. For example: {@link SearchRequest}, {@link NextPreviousPager},
 * {@link PagerFilter}, or the selected issue.
 *
 * @since v4.2
 * @deprecated since 7.1 Storing information on the httpsession is obsoleted. Please move this functionality to the frontend or other type of storage. This method will be removed in 8.0.
 */
@Deprecated
public interface SessionSearchObjectManager<T> {

    /**
     * @return the current object for the current user session-wide. Null signifies that there is no current object.
     * @deprecated since 7.1 Storing information on the httpsession is obsoleted. Please move this functionality to the frontend or other type of storage. This method will be removed in 8.0.
     */
    @Deprecated
    T getCurrentObject();

    /**
     * Associates the specified object to the current user's session.
     *
     * @param object the object
     * @deprecated since 7.1 Storing information on the httpsession is obsoleted. Please move this functionality to the frontend or other type of storage. This method will be removed in 8.0.
     */
    @Deprecated
    void setCurrentObject(T object);
}
