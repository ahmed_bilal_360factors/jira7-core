package com.atlassian.jira.startup;

import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.ha.PluginMessageSender;
import com.atlassian.jira.scheduler.JiraPassivatedSchedulerLauncher;
import com.atlassian.jira.scheduler.JiraSchedulerLauncher;
import com.atlassian.jira.upgrade.DowngradeReindexLauncher;
import com.atlassian.jira.upgrade.PluginUpgradeLauncher;
import com.atlassian.jira.upgrade.UpgradeLauncher;
import com.atlassian.jira.user.util.RecoveryAdminMapper;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.google.common.annotations.VisibleForTesting;

/**
 * There are a number of services that should only be started in the active mode, start them here
 *
 * @since v6.1
 */
public class ActiveServicesLauncher implements JiraLauncher {
    private final PluginUpgradeLauncher pluginUpgradeLauncher;
    private final UpgradeLauncher upgradeLauncher;
    private final DowngradeReindexLauncher downgradeReindexLauncher;
    private final AnalyticsLauncher analyticsLauncher;
    private final RecoveryAdminMapper recoveryAdminMapper;

    public ActiveServicesLauncher(final JohnsonProvider johnsonProvider) {
        this.upgradeLauncher = new UpgradeLauncher(johnsonProvider);
        this.downgradeReindexLauncher = new DowngradeReindexLauncher();
        this.pluginUpgradeLauncher = new PluginUpgradeLauncher();
        this.analyticsLauncher = new AnalyticsLauncher();
        this.recoveryAdminMapper = new RecoveryAdminMapper();
    }

    @VisibleForTesting
    ActiveServicesLauncher(final UpgradeLauncher upgradeLauncher, final DowngradeReindexLauncher downgradeReindexLauncher, final PluginUpgradeLauncher pluginUpgradeLauncher,
                           final AnalyticsLauncher analyticsLauncher, final RecoveryAdminMapper recoveryAdminMapper) {
        this.upgradeLauncher = upgradeLauncher;
        this.downgradeReindexLauncher = downgradeReindexLauncher;
        this.pluginUpgradeLauncher = pluginUpgradeLauncher;
        this.analyticsLauncher = analyticsLauncher;
        this.recoveryAdminMapper = recoveryAdminMapper;
    }

    @Override
    public void start() {
        final ClusterManager clusterManager = ComponentAccessor.getComponent(ClusterManager.class);
        if (clusterManager.isActive()) {
            upgradeLauncher.start();
            downgradeReindexLauncher.start();
            pluginUpgradeLauncher.start();
            if (clusterManager.isClustered()) {
                ComponentAccessor.getComponent(PluginMessageSender.class).activate();
            }
            getSchedulerLauncher().start();
            analyticsLauncher.start();

            /*
             * This call will ensure there is a user key for the 'recovery admin' if JIRA is started in
             * recovery mode.
             */
            recoveryAdminMapper.map();
        }
    }

    @Override
    public void stop() {
        analyticsLauncher.stop();
        pluginUpgradeLauncher.stop();
        upgradeLauncher.stop();
        downgradeReindexLauncher.stop();
        getSchedulerLauncher().stop();
        final PluginMessageSender messageSender = ComponentAccessor.getComponent(PluginMessageSender.class);
        if (messageSender != null) {
            messageSender.stop();
        }
    }

    private JiraSchedulerLauncher getSchedulerLauncher() {
        final ClusterManager clusterManager = ComponentAccessor.getComponent(ClusterManager.class);
        if (clusterManager != null && clusterManager.isActive()) {
            return new JiraSchedulerLauncher();
        } else {
            return new JiraPassivatedSchedulerLauncher();
        }
    }
}
