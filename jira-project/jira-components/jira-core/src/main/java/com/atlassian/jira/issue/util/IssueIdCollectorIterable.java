package com.atlassian.jira.issue.util;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.jql.query.IssueIdCollector;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Collection;
import java.util.Set;

/**
 * Turns issue IDs from an IssueIdCollector into an IssueIdsIssueIterable.
 *
 * @since 6.4
 */
public class IssueIdCollectorIterable extends IssueIdsIssueIterable {
    public IssueIdCollectorIterable(@Nonnull final IssueIdCollector collector, @Nonnull final IssueManager issueManager)
            throws IOException {
        super(readIssueIds(collector), issueManager);
    }

    @Nonnull
    private static Collection<Long> readIssueIds(@Nonnull IssueIdCollector collector)
            throws IOException {
        final Set<String> issueIds = collector.getIssueIds();

        return issueIds
                .stream()
                .map(Long::valueOf)
                .collect(
                        CollectorsUtil.toImmutableListWithCapacity(
                                issueIds.size()
                        )
                );
    }
}