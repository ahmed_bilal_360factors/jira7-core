package com.atlassian.jira.security.groups;

import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.QueryCallback;
import com.atlassian.jira.database.SqlPredicates;
import com.atlassian.jira.model.querydsl.QMembership;
import com.atlassian.jira.model.querydsl.QUser;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.SimpleExpression;
import com.querydsl.sql.SQLExpressions;
import com.querydsl.sql.SQLQuery;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * A query-callback that counts users in a (set of) groups within a directory.
 * Usually the set of groups will be a single group or a set of nested groups.
 * All the rules of Crowd groups are followed, including directory shadowing; nested groups per directory;
 * inactive users ignored.
 */
public class CountUsersInGroupQueryCallback implements QueryCallback<Long> {
    /**
     * The maximum number of group names we'll allow before switching to use literals instead of parameters in SQL Server.
     * This avoids SQL Server's 2000 parameter limit per query.
     * We'll assume there are no more than 100 additional parameters beyond the list of expanded group names.
     */
    private static final int SQL_SERVER_USE_LITERALS_THRESHOLD = SqlPredicates.MAX_SQL_SERVER_PARAMETER_LIMIT - 100;

    /**
     * The directory ID
     */
    private final Long directoryId;

    private final UserIsNotShadowedPredicate userIsNotShadowedPredicate;

    /**
     * The pre-expanded set of nested group names
     */
    private final Collection<String> lowerCaseGroupNames;

    private final DatabaseConfig dbConfig;

    private final SqlPredicates sqlPredicates;

    /**
     * Constructor
     *
     * @param directoryId                the directory ID to count within
     * @param higherPriorityDirectoryIds higher-priority directory IDs, used to filter out shadowed users
     * @param groupNames                 the expanded list of nested group names
     * @param dbConfig                   database config
     */
    @SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
    public CountUsersInGroupQueryCallback(final Long directoryId, final Collection<Long> higherPriorityDirectoryIds, final Collection<String> groupNames, DatabaseConfig dbConfig) {
        this.directoryId = directoryId;
        userIsNotShadowedPredicate = new UserIsNotShadowedPredicate(higherPriorityDirectoryIds);
        lowerCaseGroupNames = groupNames.stream().map(IdentifierUtils::toLowerCase).collect(Collectors.toSet());
        this.dbConfig = dbConfig;
        sqlPredicates = new SqlPredicates(dbConfig);
    }

    /**
     * Run the query
     *
     * @param dbConnection the database connection
     * @return the user count
     */
    @Override
    public Long runQuery(final DbConnection dbConnection) {
        if (dbConfig.isMySql()) {
            // The standard query performs poorly on MySql... use an alternative
            return runMySqlQuery(dbConnection);
        } else {
            return runStandardQuery(dbConnection);
        }
    }

    /**
     * This version of the query performs pretty well on all databases... except for MySql
     *
     * @param dbConnection the database connection
     * @return the user count
     */
    private Long runStandardQuery(final DbConnection dbConnection) {
        QMembership m = new QMembership("m");
        QUser u = new QUser("u");

        // Predicate to only include users in their canonical directory (i.e. they are not shadowed)
        BooleanExpression userIsNotShadowed = userIsNotShadowedPredicate.apply(u);

        // Predicate to find users that are a member of one or more of the nested groups, in this directory.
        // Use exists() to ensure we count each user only once,  even if they are a member of multiple groups
        BooleanExpression userIsMemberOfGroups = SQLExpressions
                .select(u)
                .from(m).where(m.lowerChildName.eq(u.lowerUserName)
                        .and(m.membershipType.eq("GROUP_USER"))
                        .and(m.directoryId.eq(directoryId))
                        .and(sqlPredicates.partitionedIn(m.lowerParentName, lowerCaseGroupNames)))
                .exists();

        // Query to count active users who are not shadowed, and are a member of at least one of these groups
        SQLQuery<Long> query = dbConnection.newSqlQuery()
                .select(u.lowerUserName.count())
                .from(u).where(u.active.eq(1).and(u.directoryId.eq(directoryId))
                        .and(userIsNotShadowed)
                        .and(userIsMemberOfGroups));

        // SQL Server has a limit of 2000 parameters per query. If we hit that, use literals instead
        if (dbConfig.isSqlServer() && lowerCaseGroupNames.size() > SQL_SERVER_USE_LITERALS_THRESHOLD) {
            query.setUseLiterals(true);
        }

        return query.limit(1).fetchOne();
    }

    /**
     * This version of the query runs better on MySql (but worse on other databases, particularly MS SQL Server)
     *
     * @param dbConnection the database connection
     * @return the user count
     */
    private Long runMySqlQuery(final DbConnection dbConnection) {
        QMembership m = new QMembership("m");
        QUser u = new QUser("u");

        // Predicate to only include users in their canonical directory (i.e. they are not shadowed)
        BooleanExpression userIsNotShadowed = userIsNotShadowedPredicate.apply(u);

        // Query to find only active users that are not shadowed, and that are a member of one or more of the nested
        // groups in this directory.
        // Use groupBy() to ensure we count each user only once, even if they are a member of multiple groups
        final SimpleExpression<String> uniqueUsersSubQuery = SQLExpressions
                .select(m.lowerChildName)
                .from(m).innerJoin(u).on(u.id.eq(m.childId))
                .where(u.active.eq(1)
                        .and(m.membershipType.eq("GROUP_USER"))
                        .and(m.directoryId.eq(directoryId))
                        .and(userIsNotShadowed)
                        .and(m.lowerParentName.in(lowerCaseGroupNames)))
                .groupBy(m.lowerChildName)
                .as("uniqueUsers");

        // An outer count(*) query
        SQLQuery<?> countQuery = dbConnection.newSqlQuery();
        countQuery.from(uniqueUsersSubQuery);

        return countQuery.fetchCount();
    }
}
