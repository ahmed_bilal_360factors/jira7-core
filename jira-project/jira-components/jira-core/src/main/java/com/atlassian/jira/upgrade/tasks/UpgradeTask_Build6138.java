package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;

import javax.annotation.Nullable;
import java.util.EnumSet;

import static java.lang.String.format;

/**
 * Ensures that a reindex is performed to support wildcard searches on stemmed fields.
 *
 * @since v6.1
 */
public class UpgradeTask_Build6138 extends AbstractReindexUpgradeTask {
    @Override
    public int getBuildNumber() {
        return 6138;
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception {
        getReindexRequestService().requestReindex(ReindexRequestType.DELAYED, EnumSet.of(AffectedIndex.ISSUE, AffectedIndex.COMMENT), EnumSet.noneOf(SharedEntityType.class));

    }

    @Override
    public String getShortDescription() {
        return format
                (
                        "%s This is necessary for the index to support wildcard searches on stemmed fields.",
                        super.getShortDescription()
                );
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        // Flagged as not downgradable because it was created before the Downgrade Task framework was built.
        return true;
    }

    @Nullable
    @Override
    public Integer dependsUpon() {
        return 6137;
    }

}
