package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;

import javax.annotation.Nullable;
import java.util.EnumSet;

import static java.lang.String.format;

/**
 * Requires a JIRA re-index to be able to migrate to a new aggressive stemming algorithm for the English language.
 * <p>
 * We have changed the algorithm from the Porter Stemmer to the Snowball Program for English (AKA Porter2).
 *
 * @see com.atlassian.jira.issue.index.analyzer.TokenFilters.English.Stemming
 * @since v6.1
 */
public class UpgradeTask_Build6136 extends AbstractReindexUpgradeTask {
    @Override
    public int getBuildNumber() {
        return 6136;
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception {
        getReindexRequestService().requestReindex(ReindexRequestType.DELAYED, EnumSet.of(AffectedIndex.ISSUE, AffectedIndex.COMMENT), EnumSet.noneOf(SharedEntityType.class));

    }

    @Override
    public String getShortDescription() {
        return format
                (
                        "%s Necessary to update the underlying algorithm used by the English - Aggressive Stemming "
                                + "Indexing Language setting.", super.getShortDescription()
                );
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        // Flagged as not downgradable because it was created before the Downgrade Task framework was built.
        return true;
    }

    @Nullable
    @Override
    public Integer dependsUpon() {
        return 6135;
    }

}
