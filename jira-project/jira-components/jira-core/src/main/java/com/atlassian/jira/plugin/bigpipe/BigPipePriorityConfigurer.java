package com.atlassian.jira.plugin.bigpipe;

import com.atlassian.plugin.elements.ResourceDescriptor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

/**
 * Configures the priority of a bigpipe component for a request.
 *
 * @since v7.1
 */
public interface BigPipePriorityConfigurer {
    /**
     * Calculates the priority for a bigpipe component for a request.
     *
     * @param resource descriptor for the bigpipe resource as defined in the plugin.
     * @param context  the request rendering context, unmodifiable.
     * @return the calculated priority, or <code>null</code> to use the default priority.
     */
    @Nullable
    Integer calculatePriority(@Nonnull ResourceDescriptor resource, @Nonnull Map<String, ?> context);
}
