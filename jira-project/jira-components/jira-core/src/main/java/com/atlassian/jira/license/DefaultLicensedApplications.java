package com.atlassian.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.extras.api.LicenseException;
import com.atlassian.extras.api.Product;
import com.atlassian.extras.common.LicensePropertiesConstants;
import com.atlassian.extras.decoder.api.LicenseDecoder;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.application.ApplicationRoleDefinitions;
import com.atlassian.jira.application.ApplicationRoleDefinitions.ApplicationRoleDefinition;
import com.atlassian.jira.application.UndefinedApplicationRoleName;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.extras.common.LicensePropertiesConstants.ACTIVE_FLAG;
import static com.atlassian.extras.common.LicensePropertiesConstants.DESCRIPTION;
import static com.atlassian.extras.common.LicensePropertiesConstants.MAX_NUMBER_OF_USERS;
import static com.atlassian.extras.common.LicensePropertiesConstants.NAMESPACE_SEPARATOR;
import static com.atlassian.jira.component.ComponentAccessor.getComponent;
import static com.atlassian.jira.license.DefaultLicenseDetails.ENABLED;
import static java.lang.String.format;
import static java.util.regex.Pattern.compile;
import static java.util.regex.Pattern.quote;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Default implementation of {@link LicensedApplications}.
 * <p>
 * This implementation fails fast (in the constructor) on detection of invalid license or license application/role
 * details.
 *
 * @since v7.0
 */
public final class DefaultLicensedApplications implements LicensedApplications {
    private static final Logger log = LoggerFactory.getLogger(DefaultLicensedApplications.class);

    /**
     * String that identifies a license key as being part of an application, as distinct from an addon.
     */
    public static final String APPLICATION_NAMESPACE = NAMESPACE_SEPARATOR + "product" + NAMESPACE_SEPARATOR;

    /**
     * String that prefixes a license key as being a JIRA role-based application (product), typically "jira.product.".
     */
    public static final String JIRA_PRODUCT_NAMESPACE = Product.JIRA.getNamespace() + APPLICATION_NAMESPACE;

    /**
     * Pattern that identifies an application/role property value.
     */
    private static final Pattern APPLICATION_ROLE = compile(
            quote(JIRA_PRODUCT_NAMESPACE) + "(.+?)" + quote(NAMESPACE_SEPARATOR) + MAX_NUMBER_OF_USERS);

    /**
     * String suffix for a license property that identifies/declares a "product key".
     */
    private static final String PRODUCT_SUFFIX = NAMESPACE_SEPARATOR + ACTIVE_FLAG;

    /**
     * JIRA Product active indicator, pre-7.0 license (role-less)
     */
    private static final String JIRA_PRODUCT_ACTIVE = Product.JIRA.getNamespace() + PRODUCT_SUFFIX;

    /**
     * JIRA pre-7.0 license user limit property
     */
    private static final String JIRA_USER_LIMIT = Product.JIRA.getNamespace() + NAMESPACE_SEPARATOR + MAX_NUMBER_OF_USERS;

    /**
     * Boolean indicating that license has been issued as application (role based) license
     */
    private final boolean hasNativeRole;

    /**
     * An immutable map of applications to user limit.
     */
    private final ImmutableMap<ApplicationKey, Integer> userLimitByApplication;
    private final Properties licenseProperties;

    /**
     * @param license        An encoded license string.
     * @param licenseDecoder A license decoder capable of decoding the given license.
     * @throws LicenseException on detection of invalid applications or application user counts.
     */
    public DefaultLicensedApplications(@Nonnull String license, @Nonnull LicenseDecoder licenseDecoder)
            throws LicenseException {
        this.licenseProperties = licenseDecoder.decode(license);
        final ImmutableMap<ApplicationKey, Integer> applicationRoles = extractApplicationsFrom(licenseProperties);

        if (!applicationRoles.isEmpty()) {
            hasNativeRole = true;
            this.userLimitByApplication = applicationRoles;
        } else {
            hasNativeRole = false;
            this.userLimitByApplication = interpretAsApplicationLicense(licenseProperties);
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * Note: This implementation returns an unmodifiable {@link Set}.
     */
    @Override
    @Nonnull
    public Set<ApplicationKey> getKeys() {
        return this.userLimitByApplication.keySet();
    }

    @Override
    public int getUserLimit(@Nonnull final ApplicationKey key) {
        Integer numUsers = this.userLimitByApplication.get(key);
        return (numUsers != null) ? numUsers : 0;
    }

    @Nonnull
    @Override
    public String getDescription() {
        Set<ApplicationKey> applications = getKeys();
        List<String> descriptions = new ArrayList<>(applications.size());

        for (ApplicationKey appId : applications) {
            descriptions.add(getApplicationName(appId));
        }
        return Joiner.on(", ").join(descriptions);
    }

    private String getApplicationName(ApplicationKey appId) {
        // try role name as defined by installed plugin
        ApplicationRoleDefinitions roleDefinitions = getComponent(ApplicationRoleDefinitions.class);
        Option<ApplicationRoleDefinition> role = roleDefinitions.getDefined(appId);
        if (role.isDefined()) {
            return role.get().name();
        }

        // else try to get from license property
        String name = licenseProperties.getProperty(getApplicationLicensePropertyName(appId, DESCRIPTION));
        if (!isBlank(name)) {
            return name;
        }

        // else derive heuristically
        return UndefinedApplicationRoleName.of(appId).getName();
    }

    /**
     * Returns a fully-qualified license property name for the given {@link ApplicationKey application} and license
     * property. Currently, the returned string is of form "jira.product.[application-key].[property]".
     *
     * @param app      application identifier (key)
     * @param property a license property
     * @return a fully-qualified license property
     * @see com.atlassian.extras.common.LicensePropertiesConstants
     * @see #JIRA_PRODUCT_NAMESPACE
     */
    @Nonnull
    public static final String getApplicationLicensePropertyName(@Nonnull ApplicationKey app, @Nonnull String property) {
        return JIRA_PRODUCT_NAMESPACE + app.value() + NAMESPACE_SEPARATOR + property;
    }

    /**
     * Parses the application/role information from the license.
     *
     * @param licenseProperties A map of String license property names to String values.
     * @return An immutable map of the application/roles to the number of granted users/seats for those applications.
     * @throws com.atlassian.extras.api.LicenseException on detection of license format errors.
     */
    @Nonnull
    static ImmutableMap<ApplicationKey, Integer> extractApplicationsFrom(@Nonnull Properties licenseProperties)
            throws LicenseException {
        final Map<ApplicationKey, Integer> userLimitByApplication = new HashMap<>();
        for (final String propertyName : licenseProperties.stringPropertyNames()) {
            Matcher m = APPLICATION_ROLE.matcher(propertyName);
            if (!m.matches()) {
                // then propertyName is not a application/role declaration
                continue;
            }

            final String applicationKeyStr = m.group(1);
            if (!ApplicationKey.isValid(applicationKeyStr)) {
                throw new LicenseException(format("Application '%s' is declared but not a valid application.",
                        applicationKeyStr));
            }

            final ApplicationKey applicationKey = ApplicationKey.valueOf(m.group(1));
            if (!isApplicationActivated(applicationKey, licenseProperties)) {
                log.debug("Application '{}' is declared but not activated.", applicationKey);
                continue;
            }
            // extract user count
            //
            // note: no check for negative user count -- negative use count is assumed to
            // mean "unlimited user count".
            String userLimitStr = licenseProperties.getProperty(propertyName, /*default*/ "0");
            final int userLimit = toUserLimit(userLimitStr, applicationKey);

            userLimitByApplication.put(applicationKey, userLimit);
        }
        return ImmutableMap.copyOf(userLimitByApplication);
    }

    /**
     * Interpret pre-renaissance license as JIRA Software application license and Service Desk plugin license as JIRA
     * Service Desk Application license.
     * <p>
     * This will translate a JIRA Tier Base Pricing (TBP) (i.e. JIRA 6x.x), Service Desk (SD) TBP (i.e. SD 1.x) and SD
     * Agent Based Pricing (ABP) (i.e SD 2.x) licenses into JIRA Role Based Pricing (RBP) roles. This is to allow those
     * licenses to work as JIRA RBP licenses.
     *
     * @param licenseProperties A map of String license property names to String values.
     * @return An immutable map of the application/roles to the number of granted users/seats for those applications.
     * @see {@link com.atlassian.jira.license.ServiceDeskLicenseConstants} for information on SD licenses.
     */
    private static ImmutableMap<ApplicationKey, Integer> interpretAsApplicationLicense(final Properties licenseProperties) {
        final Map<ApplicationKey, Integer> userLimitByApplication = new HashMap<>();
        if (ENABLED.equals(licenseProperties.getProperty(JIRA_PRODUCT_ACTIVE, "false"))) {
            String jiraNumUsers = licenseProperties.getProperty(JIRA_USER_LIMIT, null);
            if (jiraNumUsers == null) {
                jiraNumUsers = licenseProperties.getProperty(MAX_NUMBER_OF_USERS, "0");
            }

            final int userLimit = toUserLimit(jiraNumUsers, ApplicationKeys.SOFTWARE);

            userLimitByApplication.put(ApplicationKeys.SOFTWARE, userLimit);
        }
        if (ENABLED.equals(licenseProperties.getProperty(ServiceDeskLicenseConstants.SD_LEGACY_ACTIVE))) {
            //We now know its either a Tier Based Pricing (TBP) or an Agent Base Pricing (ABP) license.
            //We next look for the SD_USER_COUNT_ABP property that only exists in ABP licenses. More details
            //on Service Desk (SD) licensing are available in in ServiceDeskLicenseConstants.
            String jiraUsers = licenseProperties.getProperty(ServiceDeskLicenseConstants.SD_USER_COUNT_ABP);
            if (jiraUsers == null) {
                //No ABP property means that it is a TBP license.
                jiraUsers = licenseProperties.getProperty(ServiceDeskLicenseConstants.SD_USER_COUNT_TBP);
            }

            userLimitByApplication.put(ApplicationKeys.SERVICE_DESK, toUserLimit(jiraUsers, ApplicationKeys.SERVICE_DESK));
        }
        return ImmutableMap.copyOf(userLimitByApplication);
    }

    private static int toUserLimit(final String userLimitStr, final ApplicationKey applicationKey) {
        int userLimit;
        try {
            userLimit = Integer.parseInt(userLimitStr);
            if (userLimit < 0) {
                userLimit = LicensePropertiesConstants.UNLIMITED_USERS;
            }
        } catch (NumberFormatException ex) {
            String message = format("Invalid user count in license with role '%s': %s", userLimitStr, applicationKey);
            log.error(message);
            throw new LicenseException(message);
        }

        log.debug("License provides {} seats for '{}'", userLimit, applicationKey);
        return userLimit;
    }

    /**
     * Returns true if the given application would be considered activated by the given license {@link Properties}.
     *
     * @param applicationKey    key of the application.
     * @param licenseProperties Map of license properties.
     */
    static boolean isApplicationActivated(@Nonnull ApplicationKey applicationKey, @Nonnull Properties licenseProperties) {
        return ENABLED.equals(licenseProperties.getProperty(JIRA_PRODUCT_NAMESPACE + applicationKey.value() + PRODUCT_SUFFIX, "false"));
    }

    @Override
    public boolean hasNativeRole() {
        return hasNativeRole;
    }
}
