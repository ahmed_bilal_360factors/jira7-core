package com.atlassian.jira.workflow.function.issue;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.status.Status;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.StoreException;
import com.opensymphony.workflow.loader.DescriptorFactory;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import com.opensymphony.workflow.spi.SimpleStep;
import com.opensymphony.workflow.spi.WorkflowEntry;
import com.opensymphony.workflow.spi.WorkflowStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:plightbo@atlassian.com">Pat Lightbody</a>
 */
public class UpdateIssueStatusFunction implements FunctionProvider {
    private static final Logger log = LoggerFactory.getLogger(UpdateIssueStatusFunction.class);

    public void execute(Map transientVars, Map args, PropertySet ps) throws StoreException {
        MutableIssue issue = (MutableIssue) transientVars.get("issue");
        WorkflowEntry entry = (WorkflowEntry) transientVars.get("entry");

        SimpleStep step = (SimpleStep) ((WorkflowStore) transientVars.get("store")).findCurrentSteps(entry.getId()).get(0);

        WorkflowDescriptor descriptor = (WorkflowDescriptor) transientVars.get("descriptor");
        StepDescriptor stepDescriptor = descriptor.getStep(step.getStepId());

        Status oldStatus = issue.getStatus();
        // JRA-44123 Used in CreateCOmmentFunction to allow people to comment even if they only had permission in the old status.
        transientVars.put("oldStatus", oldStatus);
        Status newStatus = ComponentAccessor.getConstantsManager().getStatus((String) stepDescriptor.getMetaAttributes().get("jira.status.id"));

        // Update issue
        issue.setStatusId(newStatus.getId());

        // Generate status change item
        List changeItems = (List) transientVars.get("changeItems");
        if (changeItems == null) {
            changeItems = new LinkedList();
        }

        String from = null;
        String fromString = null;
        if (oldStatus != null) {
            from = oldStatus.getId();
            fromString = oldStatus.getName();
        }

        String to = newStatus.getId();
        String toString = newStatus.getName();

        changeItems.add(new ChangeItemBean(ChangeItemBean.STATIC_FIELD, IssueFieldConstants.STATUS, from, fromString, to, toString));
        transientVars.put("changeItems", changeItems);
    }

    public static FunctionDescriptor makeDescriptor() {
        FunctionDescriptor descriptor = DescriptorFactory.getFactory().createFunctionDescriptor();
        descriptor.setType("class");
        descriptor.getArgs().put("class.name", UpdateIssueStatusFunction.class.getName());
        return descriptor;
    }
}
