package com.atlassian.jira.project.type;

import com.atlassian.application.api.Application;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.JiraApplication;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.ImmutableList;

import java.util.Comparator;
import java.util.List;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.google.common.collect.Iterables.addAll;
import static com.google.common.collect.Lists.newArrayList;

/**
 * Default implementation for {@link ProjectTypeManager}
 */
public class ProjectTypeManagerImpl implements ProjectTypeManager {
    private static final ProjectTypeKey INACCESSIBLE_KEY = new ProjectTypeKey("jira-project-type-inaccessible");
    private static final String INACCESSIBLE_DESC = "jira.project.type.inaccessible.description";
    private static final String INACCESSIBLE_ICON = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxOC4xLjEsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAyMCAyMCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMjAgMjAiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPGc+DQoJPHBhdGggZmlsbD0iIzk5OTk5OSIgZD0iTTAuMSw4LjljMC44LDAuMSwxLjUtMC4zLDEuNy0xLjFsMC0wLjFDMi4yLDYuNCwyLjksNS4xLDQsNEM0LjUsMy41LDQuNSwyLjYsNCwyQzIuMywzLjMsMC45LDUuMiwwLjMsNy40DQoJCUMwLjIsNy45LDAuMSw4LjQsMC4xLDguOXoiLz4NCgk8cGF0aCBmaWxsPSIjOTk5OTk5IiAgZD0iTTQuMSwxOC4xYzAuNS0wLjYsMC41LTEuNS0wLjEtMkMzLDE1LDIuMiwxMy43LDEuOCwxMi4yYy0wLjItMC44LTEtMS4yLTEuNy0xLjENCgkJQzAuNCwxMy45LDEuOSwxNi40LDQuMSwxOC4xeiIvPg0KCTxwYXRoIGZpbGw9IiM5OTk5OTkiICBkPSJNMTkuOSwxMS4xYy0wLjgtMC4xLTEuNSwwLjMtMS43LDEuMWMtMC40LDEuNC0xLjEsMi44LTIuMiwzLjhjLTAuNiwwLjYtMC42LDEuNC0wLjEsMg0KCQljMS44LTEuMywzLjEtMy4yLDMuNy01LjVDMTkuOCwxMi4xLDE5LjksMTEuNiwxOS45LDExLjF6Ii8+DQoJPHBhdGggZmlsbD0iIzk5OTk5OSIgIGQ9Ik0xNCwxOS4yYy0wLjMtMC43LTEtMS4yLTEuOC0wLjljLTEuNCwwLjQtMywwLjQtNC40LDBjLTAuOC0wLjItMS41LDAuMi0xLjgsMWMwLjQsMC4yLDAuOSwwLjQsMS40LDAuNQ0KCQlDOS43LDIwLjMsMTIsMjAsMTQsMTkuMnoiLz4NCgk8cGF0aCBmaWxsPSIjOTk5OTk5IiAgZD0iTTcuOCwxLjhjMS40LTAuNCwzLTAuNCw0LjQsMGMwLjgsMC4yLDEuNS0wLjIsMS44LTFjLTAuNC0wLjItMC45LTAuNC0xLjQtMC41QzEwLjMtMC4zLDgsMCw2LDAuOQ0KCQljMC4yLDAuNSwwLjYsMC44LDEsMC45QzcuMiwxLjksNy41LDEuOSw3LjgsMS44eiIvPg0KCTxwYXRoIGZpbGw9IiM5OTk5OTkiICBkPSJNMTkuMyw4LjhjMC4yLDAuMSwwLjQsMC4xLDAuNywwYy0wLjMtMi44LTEuOC01LjMtNC02LjljLTAuNSwwLjYtMC41LDEuNSwwLjEsMmMxLjEsMSwxLjgsMi40LDIuMiwzLjgNCgkJQzE4LjMsOC4zLDE4LjgsOC43LDE5LjMsOC44eiIvPg0KPC9nPg0KPC9zdmc+";
    private static final String INACCESSIBLE_COLOR = "#999999";
    public static final int INACCESSIBLE_WEIGHT = 1_000_000_000;

    private static final ProjectType INACCESSIBLE_TYPE = new ProjectType(INACCESSIBLE_KEY, INACCESSIBLE_DESC, INACCESSIBLE_ICON, INACCESSIBLE_COLOR, INACCESSIBLE_WEIGHT);

    private final JiraApplicationAdapter adapter;

    public ProjectTypeManagerImpl(JiraApplicationAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public List<ProjectType> getAllProjectTypes() {
        final List<ProjectType> projectTypes = newArrayList(gatherProjectTypesFrom(adapter.getJiraApplications()));
        projectTypes.sort(Comparator.comparingInt(ProjectType::getWeight));
        return ImmutableList.copyOf(projectTypes);
    }

    @Override
    public List<ProjectType> getAllAccessibleProjectTypes() {
        final List<ProjectType> projectTypes = newArrayList(getProjectTypesInstalledAndLicensed());
        projectTypes.sort(Comparator.comparingInt(ProjectType::getWeight));
        return ImmutableList.copyOf(projectTypes);
    }

    @Override
    public Option<ProjectType> getByKey(final ProjectTypeKey key) {
        return findProjectTypeByKey(getAllProjectTypes(), key);
    }

    public Option<Application> getApplicationWithType(final ProjectTypeKey key) {
        for (JiraApplication jiraApplication : adapter.getJiraApplications()) {
            for (ProjectType projectType : jiraApplication.getProjectTypes()) {
                if (projectType.getKey().equals(key)) {
                    return Option.option(jiraApplication);
                }
            }
        }

        return Option.none();
    }

    @Override
    public Option<ProjectType> getAccessibleProjectType(final ProjectTypeKey key) {
        return findProjectTypeByKey(getProjectTypesInstalledAndLicensed(), key);
    }

    private Iterable<ProjectType> getProjectTypesInstalledAndLicensed() {
        return gatherProjectTypesFrom(adapter.getAccessibleJiraApplications());
    }

    @Override
    public Option<ProjectType> getAccessibleProjectType(final ApplicationUser user, final ProjectTypeKey key) {
        return findProjectTypeByKey(getProjectTypesInstalledAndLicensedForUser(user), key);
    }

    private Iterable<ProjectType> getProjectTypesInstalledAndLicensedForUser(final ApplicationUser user) {
        return gatherProjectTypesFrom(adapter.getAccessibleJiraApplications(user));
    }

    @Override
    public ProjectType getInaccessibleProjectType() {
        return INACCESSIBLE_TYPE;
    }

    @Override
    public boolean isProjectTypeUninstalled(final ProjectTypeKey projectTypeKey) {
        return getByKey(projectTypeKey).isEmpty();
    }

    @Override
    public boolean isProjectTypeInstalledButInaccessible(final ProjectTypeKey projectTypeKey) {
        return !isProjectTypeUninstalled(projectTypeKey) && getAccessibleProjectType(projectTypeKey).isEmpty();
    }

    private Option<ProjectType> findProjectTypeByKey(Iterable<ProjectType> types, ProjectTypeKey key) {
        for (ProjectType projectType : types) {
            if (key.equals(projectType.getKey())) {
                return some(projectType);
            }
        }
        return none();
    }

    private Iterable<ProjectType> gatherProjectTypesFrom(Iterable<JiraApplication> applications) {
        final List<ProjectType> projectTypes = newArrayList();
        for (JiraApplication application : applications) {
            addAll(projectTypes, application.getProjectTypes());
        }
        return projectTypes;
    }
}
