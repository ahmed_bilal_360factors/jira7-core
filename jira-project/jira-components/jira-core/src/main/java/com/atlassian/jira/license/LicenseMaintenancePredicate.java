package com.atlassian.jira.license;

import javax.annotation.Nonnull;
import java.util.function.Predicate;

/**
 * Predicate to indicate whether or not the the passed license is within maintenance.
 *
 * @since 7.0
 */
public interface LicenseMaintenancePredicate extends Predicate<LicenseDetails> {
    /**
     * Checks whether the license is within the maintenance date. Note, this checks the application and platform
     * build dates.
     *
     * @param details the license to check
     * @return true if the license is within maintenance
     */
    @Override
    boolean test(@Nonnull LicenseDetails details);
}
