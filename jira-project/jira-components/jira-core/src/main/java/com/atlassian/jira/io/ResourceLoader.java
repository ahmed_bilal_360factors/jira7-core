package com.atlassian.jira.io;

import com.atlassian.fugue.Option;
import com.atlassian.plugin.Plugin;

import javax.annotation.ParametersAreNonnullByDefault;
import java.io.InputStream;
import java.net.URL;

/**
 * Provides ability to load resources from JIRA Core or plugins class loader. Use this one whenever you want to be sure
 * that loaded resource is from JIRA Core or given plugin. Paths to resources should always be absolute not relative.
 *
 * @since v6.5
 */
@ParametersAreNonnullByDefault
public interface ResourceLoader {
    /**
     * Opens stream to given resource from JIRA Core. See {@link Class#getResourceAsStream(String)} for exact
     * semantics.
     *
     * @param resourcePath path to resource
     * @return some of inputStream if resource exists none otherwise.
     */
    Option<InputStream> getResourceAsStream(String resourcePath);

    /**
     * Opens URL to given resource from JIRA Core. See {@link Class#getResource(String)} for exact semantics.
     *
     * @param resourcePath path to resource
     * @return some of URL if resource exists none otherwise.
     */
    Option<URL> getResource(String resourcePath);

    /**
     * Opens stream to given resource from plugin. See {@link Class#getResourceAsStream(String)} for exact semantics.
     *
     * @param resourcePath path to resource
     * @return some of inputStream if resource exists none otherwise.
     */
    Option<InputStream> getResourceAsStream(Plugin plugin, String resourcePath);

    /**
     * Opens URL to given resource from plugin. See {@link Class#getResource(String)} for exact semantics.
     *
     * @param resourcePath path to resource
     * @return some of URL if resource exists none otherwise.
     */
    Option<URL> getResource(Plugin plugin, String resourcePath);
}
