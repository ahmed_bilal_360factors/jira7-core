package com.atlassian.jira.permission.management;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.permission.PermissionGrant;
import com.atlassian.jira.permission.PermissionHolder;
import com.atlassian.jira.permission.PermissionHolderType;
import com.atlassian.jira.permission.PermissionScheme;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.permission.PermissionSchemeService;
import com.atlassian.jira.permission.PermissionTypeManager;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.permission.management.beans.GrantToPermissionInputBean;
import com.atlassian.jira.permission.management.beans.PermissionsInputBean;
import com.atlassian.jira.permission.management.beans.ProjectPermissionAddBean;
import com.atlassian.jira.permission.management.beans.ProjectPermissionBean;
import com.atlassian.jira.permission.management.beans.ProjectPermissionOperationResultBean;
import com.atlassian.jira.permission.management.beans.ProjectPermissionRenderingBean;
import com.atlassian.jira.permission.management.beans.ProjectPermissionRenderingSectionBean;
import com.atlassian.jira.permission.management.beans.ProjectPermissionSchemeBean;
import com.atlassian.jira.permission.management.beans.SecurityTypeBean;
import com.atlassian.jira.permission.management.beans.SecurityTypeValueBean;
import com.atlassian.jira.permission.management.events.PermissionGrantedToSecurityTypeEvent;
import com.atlassian.jira.permission.management.events.PermissionRevokedFromSecurityTypeEvent;
import com.atlassian.jira.permission.management.events.SuccessfulPermissionSchemeGrantEvent;
import com.atlassian.jira.permission.management.events.SuccessfulPermissionSchemeRevokeEvent;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.security.type.SecurityType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.atlassian.jira.permission.JiraPermissionHolderType.USER;
import static com.atlassian.jira.permission.ProjectPermissionCategory.ATTACHMENTS;
import static com.atlassian.jira.permission.ProjectPermissionCategory.COMMENTS;
import static com.atlassian.jira.permission.ProjectPermissionCategory.ISSUES;
import static com.atlassian.jira.permission.ProjectPermissionCategory.OTHER;
import static com.atlassian.jira.permission.ProjectPermissionCategory.PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissionCategory.TIME_TRACKING;
import static com.atlassian.jira.permission.ProjectPermissionCategory.VOTERS_AND_WATCHERS;

public class ManagedPermissionSchemeHelperImpl implements ManagedPermissionSchemeHelper {
    private final PermissionManager permissionManager;
    private final PermissionSchemeManager permissionSchemeManager;
    private final PermissionSchemeService permissionSchemeService;
    private final PermissionTypeManager permissionTypeManager;
    private final I18nHelper i18nHelper;
    private final SecurityTypeValuesService securityTypeValuesService;
    private final ManagedPermissionSchemeEditingService editingHelper;
    private final EventPublisher eventPublisher;
    private final UserManager userManager;

    public ManagedPermissionSchemeHelperImpl(final PermissionSchemeManager permissionSchemeManager,
                                             final PermissionSchemeService permissionSchemeService,
                                             final PermissionManager permissionManager,
                                             final PermissionTypeManager permissionTypeManager,
                                             final I18nHelper i18nHelper,
                                             final SecurityTypeValuesService securityTypeValuesService,
                                             final ManagedPermissionSchemeEditingService managedPermissionSchemeEditingService,
                                             final EventPublisher eventPublisher, final UserManager userManager) {
        this.permissionSchemeManager = permissionSchemeManager;
        this.permissionSchemeService = permissionSchemeService;
        this.permissionManager = permissionManager;
        this.permissionTypeManager = permissionTypeManager;
        this.i18nHelper = i18nHelper;
        this.securityTypeValuesService = securityTypeValuesService;
        this.editingHelper = managedPermissionSchemeEditingService;
        this.eventPublisher = eventPublisher;
        this.userManager = userManager;
    }

    @Override
    public Either<ErrorCollection, ProjectPermissionSchemeBean> getManagedPermissionScheme(final ApplicationUser user, final Long permissionSchemeId) {
        ServiceOutcome<PermissionScheme> outcome = permissionSchemeService.getPermissionScheme(user, permissionSchemeId);
        if (!outcome.isValid()) {
            return Either.left(outcome.getErrorCollection());
        }
        Scheme schemeObject = permissionSchemeManager.getSchemeObject(permissionSchemeId);
        if (schemeObject == null) {
            return Either.left(schemeNotFound());
        }

        List<ProjectPermission> allProjectPermissions = Lists.newArrayList(getAllProjectPermissions());
        // sort the permissions into alpha order
        Collections.sort(allProjectPermissions, (o1, o2) -> o1.getKey().compareTo(o2.getKey()));
        return Either.right(generatePermissionSchemeBean(allProjectPermissions, outcome.get()));
    }

    @Override
    public Either<ErrorCollection, ProjectPermissionAddBean> getManagedPermissionSchemeAddView(final ApplicationUser user, final Long permissionSchemeId, final String permissionKey) {
        ServiceOutcome<PermissionScheme> outcome = permissionSchemeService.getPermissionScheme(user, permissionSchemeId);
        if (!outcome.isValid()) {
            return Either.left(outcome.getErrorCollection());
        }
        Scheme schemeObject = permissionSchemeManager.getSchemeObject(permissionSchemeId);
        if (schemeObject == null) {
            return Either.left(schemeNotFound());
        }
        Option<ProjectPermission> projectPermission = permissionManager.getProjectPermission(new ProjectPermissionKey(permissionKey));
        if (projectPermission.isEmpty()) {
            return Either.left(schemeNotFound());
        }
        return Either.right(generatePermissionSchemeAddBean(projectPermission.get(), user));
    }

    @Override
    public Either<ErrorCollection, ProjectPermissionAddBean> getManagedPermissionSchemeAddViewSecurityTypes(final ApplicationUser user) {
        return Either.right(ProjectPermissionAddBean.builder()
                .setPrimarySecurityType(securityTypeValuesService.buildPrimarySecurityTypes(user))
                .setSecondarySecurityType(securityTypeValuesService.buildSecondarySecurityTypes(user))
                .build());
    }

    @Override
    public Either<ErrorCollection, ProjectPermissionSchemeBean> removeManagedPermissionSchemeGrants(final ApplicationUser user, final Long permissionSchemeId, final List<Long> grantsToDelete) {
        Scheme schemeObject = permissionSchemeManager.getSchemeObject(permissionSchemeId);
        if (schemeObject == null) {
            return Either.left(schemeNotFound());
        }

        if (grantsToDelete == null || grantsToDelete.isEmpty()) {
            return Either.left(notFoundError("admin.errors.permissions.specify.permission.to.delete"));
        }

        triggerRevokeAnalyticsEvents(permissionSchemeId, grantsToDelete, user);
        permissionSchemeManager.deleteEntities(grantsToDelete);
        final Either<ErrorCollection, ProjectPermissionSchemeBean> result = getManagedPermissionScheme(user, permissionSchemeId);

        if (result.isRight()) {
            final String successMessageTranslationKey = "admin.permissions.feedback.successfuldelete." + (grantsToDelete.size() == 1 ? "single" : "multiple");
            addSuccessfulOperationDetails(result.right().get(), i18nHelper.getText(successMessageTranslationKey));
        }

        return result;
    }

    @Override
    public Either<ErrorCollection, ProjectPermissionSchemeBean> addManagedPermissionSchemeGrants(final ApplicationUser user, final Long permissionSchemeId, final PermissionsInputBean inputBean) {
        Scheme schemeObject = permissionSchemeManager.getSchemeObject(permissionSchemeId);
        if (schemeObject == null) {
            return Either.left(schemeNotFound());
        }
        ErrorCollection errors = editingHelper.validateAddPermissions(user, inputBean);
        if (errors.hasAnyErrors()) {
            return Either.left(errors);
        }

        editingHelper.addNewSecurityTypes(schemeObject, inputBean);
        final Either<ErrorCollection, ProjectPermissionSchemeBean> result = getManagedPermissionScheme(user, permissionSchemeId);

        if (result.isRight()) {
            triggerGrantAnalyticsEvents(permissionSchemeId, inputBean);
            final String successMessageTranslationKey = "admin.permissions.feedback.successfulgrant." + (inputBean.getPermissionKeys().size() == 1 ? "single" : "multiple");
            addSuccessfulOperationDetails(result.right().get(), i18nHelper.getText(successMessageTranslationKey));
        }

        return result;
    }

    private void triggerRevokeAnalyticsEvents(final Long permissionSchemeId, final List<Long> grantsToDelete, ApplicationUser user) {
        if (grantsToDelete.isEmpty()) {
            return;
        }
        final List<GenericValue> deletedEntities = permissionSchemeManager.getEntitiesByIds(grantsToDelete);
        if (deletedEntities.isEmpty()) {
            return;
        }

        // current UI implementation sends out only one permission key, but multiple are supported in the endpoint, so let's create an appropriate String for that
        final String analyticsPermissionKey = deletedEntities.stream().map(entity -> entity.get("permissionKey").toString()).sorted().distinct().collect(Collectors.joining(","));
        eventPublisher.publish(new SuccessfulPermissionSchemeRevokeEvent(permissionSchemeId, analyticsPermissionKey, grantsToDelete.size()));

        deletedEntities.stream().forEach(entity -> eventPublisher.publish(
                new PermissionRevokedFromSecurityTypeEvent(
                        permissionSchemeId,
                        entity.get("permissionKey").toString(),
                        entity.get("type").toString()
                )
        ));
    }

    private void triggerGrantAnalyticsEvents(final Long permissionSchemeId, final PermissionsInputBean inputBean) {
        // only one securityType is expected in the project permissions page, but multiple can be submitted if the
        // endpoint is called directly
        final String analyticsSecurityType = inputBean.getGrants().stream().map(GrantToPermissionInputBean::getSecurityType).sorted().collect(Collectors.joining(","));

        eventPublisher.publish(new SuccessfulPermissionSchemeGrantEvent(permissionSchemeId, analyticsSecurityType, inputBean.getPermissionKeys().size()));
        // fire individual events for each permission key used in the request
        inputBean.getPermissionKeys().forEach(permissionKey -> eventPublisher.publish(new PermissionGrantedToSecurityTypeEvent(permissionSchemeId, permissionKey, analyticsSecurityType)));
    }

    private void addSuccessfulOperationDetails(final ProjectPermissionSchemeBean projectPermissionSchemeBean, final String message) {
        final ProjectPermissionOperationResultBean operationResult = new ProjectPermissionOperationResultBean();
        operationResult.setType(ProjectPermissionOperationResultBean.SUCCESS_TYPE);
        operationResult.setMessages(Arrays.asList(message));
        projectPermissionSchemeBean.setOperationResult(operationResult);
    }

    private ProjectPermissionSchemeBean generatePermissionSchemeBean(Collection<ProjectPermission> allProjectPermissions, final PermissionScheme effectivePermissionScheme) {
        // all known permissions
        Iterable<ProjectPermissionBean> permissionBeans = Iterables.transform(allProjectPermissions, projectPermission -> {
            ProjectPermissionKey permissionKey = projectPermission.getProjectPermissionKey();
            String descriptionI18nKey = projectPermission.getDescriptionI18nKey();
            String nameI18nKey = projectPermission.getNameI18nKey();

            List<SecurityTypeBean> securityTypeGrants = buildGrants(permissionKey, effectivePermissionScheme);

            return ProjectPermissionBean.builder()
                    .setPermissionKey(permissionKey.permissionKey())
                    .setPermissionDesc(i18nHelper.getText(descriptionI18nKey))
                    .setPermissionName(i18nHelper.getText(nameI18nKey))
                    .setGrants(securityTypeGrants)
                    .build();

        });
        ProjectPermissionSchemeBean.Builder builder = ProjectPermissionSchemeBean.builder()
                .setId(effectivePermissionScheme.getId())
                .setName(effectivePermissionScheme.getName())
                .setDescription(effectivePermissionScheme.getDescription())
                .setPermissions(Lists.newArrayList(permissionBeans))
                .setDisplayRendering(buildRenderingSections(i18nHelper));
        return builder.build();
    }

    private ProjectPermissionAddBean generatePermissionSchemeAddBean(final ProjectPermission projectPermission, final ApplicationUser user) {
        ProjectPermissionKey permissionKey = projectPermission.getProjectPermissionKey();
        String descriptionI18nKey = projectPermission.getDescriptionI18nKey();
        String nameI18nKey = projectPermission.getNameI18nKey();

        ProjectPermissionBean projectPermissionBean = ProjectPermissionBean.builder()
                .setPermissionKey(permissionKey.permissionKey())
                .setPermissionDesc(i18nHelper.getText(descriptionI18nKey))
                .setPermissionName(i18nHelper.getText(nameI18nKey))
                .build();

        return ProjectPermissionAddBean.builder()
                .setPermission(projectPermissionBean)
                .setPrimarySecurityType(securityTypeValuesService.buildPrimarySecurityTypes(user))
                .setSecondarySecurityType(securityTypeValuesService.buildSecondarySecurityTypes(user))
                .build();
    }

    // so we can hold 2 bits of information in a key but ONLY be unique on one aspect
    static class SecurityTypeMapKey {
        private final PermissionHolder permissionHolder;
        private final PermissionHolderType permissionHolderType;

        SecurityTypeMapKey(final PermissionHolder permissionHolder, final PermissionHolderType permissionHolderType) {
            this.permissionHolder = permissionHolder;
            this.permissionHolderType = permissionHolderType;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final SecurityTypeMapKey that = (SecurityTypeMapKey) o;

            if (!permissionHolderType.equals(that.permissionHolderType)) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            return permissionHolderType.hashCode();
        }
    }


    private List<SecurityTypeBean> buildGrants(final ProjectPermissionKey permissionKey, final PermissionScheme effectivePermissionScheme) {
        Collection<PermissionGrant> permissionGrants = effectivePermissionScheme.getPermissions();
        List<PermissionGrant> grantsForPermissionKey = Lists.newArrayList(Iterables.filter(permissionGrants, input -> {
            return permissionKey.equals(input.getPermission());
        }));

        // now group by security type within all the different instances of security type grants
        ListMultimap<SecurityTypeMapKey, PermissionGrant> groupedSecurityTypeInstances = ArrayListMultimap.create();

        for (PermissionGrant permissionGrant : grantsForPermissionKey) {
            SecurityTypeMapKey bySecurityType = new SecurityTypeMapKey(permissionGrant.getHolder(), permissionGrant.getHolder().getType());
            groupedSecurityTypeInstances.put(bySecurityType, permissionGrant);
        }

        List<SecurityTypeBean> grants = new ArrayList<>();
        for (SecurityTypeMapKey securityTypeMapKey : groupedSecurityTypeInstances.keySet()) {
            PermissionHolder permissionHolder = securityTypeMapKey.permissionHolder;
            SecurityType securityType = getSecurityType(permissionHolder);

            SecurityTypeBean.Builder builder = SecurityTypeBean.builder()
                    .setSecurityType(permissionHolder.getType().getKey())
                    .setDisplayName(securityType.getDisplayName());

            List<PermissionGrant> typedGrants = groupedSecurityTypeInstances.get(securityTypeMapKey);
            for (PermissionGrant typedGrant : typedGrants) {
                PermissionHolder typedGrantHolder = typedGrant.getHolder();
                SecurityTypeValueBean.Builder securityTypeValueBeanBuilder = SecurityTypeValueBean.builder()
                        .setId(typedGrant.getId())
                        .setValue(typedGrantHolder.getParameter().getOrNull())
                        .setDisplayValue(getParameterDisplayValue(typedGrant));

                // For single user permissions we add an entry to indicate whether the user exists or has been deleted
                if (USER.getKey().equals(typedGrantHolder.getType().getKey())) {
                    ApplicationUser user = userManager.getUserByNameEvenWhenUnknown(typedGrantHolder.getParameter().getOrNull());
                    securityTypeValueBeanBuilder.setValid(userManager.isUserExisting(user));
                }

                builder.addValue(securityTypeValueBeanBuilder.build());
            }
            grants.add(builder.build());
        }
        // sort them into specific order for UI purposes
        this.securityTypeValuesService.sort(grants);
        return grants;

    }

    private String getParameterDisplayValue(final PermissionGrant typedGrant) {
        Option<String> parameter = typedGrant.getHolder().getParameter();
        if (parameter.isDefined()) {
            SecurityType securityType = getSecurityType(typedGrant.getHolder());
            return securityType.getArgumentDisplay(parameter.get());
        }
        return null;
    }

    private SecurityType getSecurityType(final PermissionHolder permissionHolder) {
        Map<String, SecurityType> securityTypes = permissionTypeManager.getTypes();
        return securityTypes.get(permissionHolder.getType().getKey());
    }


    private ErrorCollection notFoundError(final String key) {
        return new SimpleErrorCollection(i18nHelper.getText(key), ErrorCollection.Reason.NOT_FOUND);
    }

    private ErrorCollection schemeNotFound() {
        return notFoundError("admin.permissions.errors.mustselectscheme");
    }

    private static class SectionDefinition {
        private String sectionType;
        private String i18nKey;
        private Supplier<Collection<ProjectPermission>> permissions;

        private SectionDefinition(String sectionType, String i18nKey, Supplier<Collection<ProjectPermission>> permissions) {
            this.sectionType = Preconditions.checkNotNull(sectionType);
            this.i18nKey = Preconditions.checkNotNull(i18nKey);
            this.permissions = Preconditions.checkNotNull(permissions);
        }
    }

    private final ImmutableList<SectionDefinition> SECTION_DEFINITIONS = ImmutableList.of(
            new SectionDefinition("project", "admin.permission.group.project.permissions", this::getProjectPermissions),
            new SectionDefinition("issue", "admin.permission.group.issue.permissions", this::getIssuePermissions),
            new SectionDefinition("voterandwatcher", "admin.permission.group.voters.and.watchers.permissions", this::getVotersAndWatchersPermissions),
            new SectionDefinition("comment", "admin.permission.group.comments.permissions", this::getCommentsPermissions),
            new SectionDefinition("attachment", "admin.permission.group.attachments.permissions", this::getAttachmentsPermissions),
            new SectionDefinition("timetracking", "admin.permission.group.time.tracking.permissions", this::getTimeTrackingPermissions),
            new SectionDefinition("other", "admin.permission.group.other.permissions", this::getOtherPermissions)
    );


    /**
     * The UI requires that specific permissions be place in specific ordering
     *
     * @param i18nHelper i18n helper to i18n the section headings
     * @return the sections in rendering order
     */
    private ProjectPermissionRenderingBean buildRenderingSections(final I18nHelper i18nHelper) {

        List<ProjectPermissionRenderingSectionBean> sections = Lists.newArrayList();
        ProjectPermissionRenderingSectionBean.Builder sectionBuilder;

        for (SectionDefinition sectionDef : SECTION_DEFINITIONS) {
            sectionBuilder = ProjectPermissionRenderingSectionBean.builder();
            sectionBuilder.setSectionType(sectionDef.sectionType);
            sectionBuilder.setHeading(i18nHelper.getText(sectionDef.i18nKey));
            sectionBuilder.addPermissions(Iterables.transform(sectionDef.permissions.get(), ProjectPermission::getKey));
            sections.add(sectionBuilder.build());
        }
        return new ProjectPermissionRenderingBean(true, sections);
    }

    private Collection<ProjectPermission> getAllProjectPermissions() {
        return permissionManager.getAllProjectPermissions();
    }

    private Collection<ProjectPermission> getProjectPermissions() {
        return permissionManager.getProjectPermissions(PROJECTS);
    }

    private Collection<ProjectPermission> getIssuePermissions() {
        return permissionManager.getProjectPermissions(ISSUES);
    }

    private Collection<ProjectPermission> getVotersAndWatchersPermissions() {
        return permissionManager.getProjectPermissions(VOTERS_AND_WATCHERS);
    }

    private Collection<ProjectPermission> getTimeTrackingPermissions() {
        return permissionManager.getProjectPermissions(TIME_TRACKING);
    }

    private Collection<ProjectPermission> getCommentsPermissions() {
        return permissionManager.getProjectPermissions(COMMENTS);
    }

    private Collection<ProjectPermission> getAttachmentsPermissions() {
        return permissionManager.getProjectPermissions(ATTACHMENTS);
    }

    private Collection<ProjectPermission> getOtherPermissions() {
        return permissionManager.getProjectPermissions(OTHER);
    }
}
