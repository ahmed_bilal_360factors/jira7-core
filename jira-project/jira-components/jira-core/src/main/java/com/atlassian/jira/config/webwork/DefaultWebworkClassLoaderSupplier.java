package com.atlassian.jira.config.webwork;

/**
 *
 * @since v7.3.1
 */
public class DefaultWebworkClassLoaderSupplier implements WebworkClassLoaderSupplier {
    private volatile ClassLoader classLoader = WebworkClassLoaderSupplier.class.getClassLoader();

    @Override
    public ClassLoader get() {
        return classLoader;
    }

    @Override
    public void set(final ClassLoader classLoader) {
        this.classLoader = classLoader;
    }
}
