package com.atlassian.jira.bc.user;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.user.UserValidationHelper.Validations;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.user.UserEventDispatcher;
import com.atlassian.jira.event.user.UserEventType;
import com.atlassian.jira.event.user.UserProfileUpdatedEvent;
import com.atlassian.jira.event.user.UserRenamedEvent;
import com.atlassian.jira.exception.AddException;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.plugin.user.PreDeleteUserErrorsManager;
import com.atlassian.jira.plugin.user.WebErrorMessage;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserDeleteVeto;
import com.atlassian.jira.user.UserDetails;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.user.util.UserUtil.PasswordResetToken;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.GlobalPermissionGroupAssociationUtil;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.SimpleWarningCollection;
import com.atlassian.jira.util.WarningCollection;
import com.atlassian.jira.util.dbc.Assertions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.jira.user.util.UserUtilImpl.DIRECTORY_ID;
import static com.atlassian.jira.user.util.UserUtilImpl.DIRECTORY_NAME;
import static com.atlassian.jira.user.util.UserUtilImpl.DISPLAY_NAME;
import static com.atlassian.jira.user.util.UserUtilImpl.EMAIL;
import static com.atlassian.jira.user.util.UserUtilImpl.PASSWORD_HOURS;
import static com.atlassian.jira.user.util.UserUtilImpl.PASSWORD_TOKEN;
import static com.atlassian.jira.user.util.UserUtilImpl.SEND_EMAIL;
import static com.atlassian.jira.user.util.UserUtilImpl.USERNAME;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.collect.Iterables.transform;

/**
 * Default implementation of {@link com.atlassian.jira.bc.user.UserService} interface. Contains methods to create/delete
 * users hiding UserUtil internals.
 *
 * @since v4.0
 */
public class DefaultUserService implements UserService {
    protected final Logger log = LoggerFactory.getLogger(DefaultUserService.class);

    private final UserUtil userUtil;
    private final UserDeleteVeto userDeleteVeto;
    private final UserManager userManager;
    private final PermissionManager permissionManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final I18nHelper.BeanFactory i18nFactory;
    private final EventPublisher eventPublisher;
    private final PreDeleteUserErrorsManager preDeleteUserErrorsManager;
    private final CreateUserApplicationHelper applicationHelper;
    private final ApplicationRoleManager applicationRoleManager;
    private final UserValidationHelper validationsHelper;
    private final GlobalPermissionGroupAssociationUtil globalPermissionGroupAssociationUtil;
    private final GlobalPermissionManager globalPermissionManager;

    public DefaultUserService(final UserUtil userUtil, final UserDeleteVeto userDeleteVeto, final PermissionManager permissionManager,
                              final UserManager userManager,
                              final I18nHelper.BeanFactory i18nFactory, final JiraAuthenticationContext jiraAuthenticationContext,
                              final EventPublisher eventPublisher, final PreDeleteUserErrorsManager preDeleteUserErrorsManager,
                              final CreateUserApplicationHelper applicationHelper,
                              final ApplicationRoleManager applicationRoleManager, final UserValidationHelper validationHelper,
                              final GlobalPermissionGroupAssociationUtil globalPermissionGroupAssociationUtil,
                              final GlobalPermissionManager globalPermissionManager) {
        this.userUtil = userUtil;
        this.userDeleteVeto = userDeleteVeto;
        this.permissionManager = permissionManager;
        this.userManager = userManager;
        this.i18nFactory = i18nFactory;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.eventPublisher = eventPublisher;
        this.preDeleteUserErrorsManager = preDeleteUserErrorsManager;
        this.applicationHelper = applicationHelper;
        this.applicationRoleManager = applicationRoleManager;
        this.validationsHelper = validationHelper;
        this.globalPermissionGroupAssociationUtil = globalPermissionGroupAssociationUtil;
        this.globalPermissionManager = globalPermissionManager;
    }

    @Nonnull
    @Override
    public ApplicationUserBuilder newUserBuilder(@Nonnull final ApplicationUser user) {
        return new ApplicationUserBuilderImpl(user);
    }

    @Override
    public CreateUserValidationResult validateCreateUserForSignup(final ApplicationUser loggedInUser,
                                                                  final String username, final String password, final String confirmPassword,
                                                                  final String email, final String fullname) {
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(loggedInUser,
                username, password, email, fullname)
                .confirmPassword(confirmPassword)
                .passwordRequired()
                .performPermissionCheck(false)
                .sendUserSignupEvent();
        return validateCreateUser(createUserRequest);
    }

    @Override
    public CreateUserValidationResult validateCreateUserForSetup(final ApplicationUser loggedInUser, final String username, final String password,
                                                                 final String confirmPassword, final String email, final String fullname) {
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(loggedInUser,
                username, password, email, fullname)
                .confirmPassword(confirmPassword)
                .passwordRequired()
                .performPermissionCheck(false);
        return validateCreateUser(createUserRequest);
    }

    @Override
    public CreateUserValidationResult validateCreateUserForSignupOrSetup(final ApplicationUser loggedInUser, final String username, final String password,
                                                                         final String confirmPassword, final String email, final String fullname) {
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(loggedInUser,
                username, password, email, fullname)
                .confirmPassword(confirmPassword)
                .passwordRequired()
                .performPermissionCheck(false)
                .sendUserSignupEvent();
        return validateCreateUser(createUserRequest);
    }

    @Override
    public CreateUserValidationResult validateCreateUser(final CreateUserRequest createUserRequest) {
        final Set<ApplicationKey> applicationKeys;
        if (createUserRequest.requireDefaultApplicationAccess()) {
            applicationKeys = applicationRoleManager.getDefaultApplicationKeys();
        } else {
            applicationKeys = createUserRequest.getApplicationKeys();
        }

        if (createUserRequest.shouldSkipValidation()) {
            return new CreateUserValidationResult(createUserRequest, applicationKeys,
                    new SimpleErrorCollection(),
                    ImmutableList.of(), new SimpleWarningCollection());

        }

        final Validations validations = this.validationsHelper.validations(createUserRequest.getLoggedInUser());

        //Permission check
        if (createUserRequest.shouldPerformPermissionCheck()) {
            if (!validations.hasCreateAccess(createUserRequest.getLoggedInUser())) {
                return new CreateUserValidationResult(validations.getErrors());
            }
        }

        //Writable directory check
        if (createUserRequest.getDirectoryId() == null) {
            if (!validations.hasWritableDefaultCreateDirectory()) {
                return new CreateUserValidationResult(validations.getErrors());
            }
        } else {
            if (!validations.writableDirectory(createUserRequest.getDirectoryId())) {
                return new CreateUserValidationResult(validations.getErrors());
            }
        }

        //Required Password check
        if (createUserRequest.requirePassword()) {
            validations.passwordRequired(createUserRequest.getPassword(), createUserRequest.shouldConfirmPassword());
        }

        List<WebErrorMessage> passwordErrors = ImmutableList.of();
        //Password validation
        if (createUserRequest.getPassword() != null) {
            passwordErrors = validations.validatePasswordPolicy(createUserRequest.getPassword(),
                    createUserRequest.getUsername(), createUserRequest.getDisplayName(), createUserRequest.getEmailAddress());
        }

        //Validate Confirm password
        if (createUserRequest.shouldConfirmPassword()) {
            validations.validateConfirmPassword(createUserRequest.getPassword(), createUserRequest.getConfirmPassword());
        }

        validations.validateEmailAddress(createUserRequest.getEmailAddress());

        validations.validateDisplayName(createUserRequest.getDisplayName());

        validations.hasValidUsername(createUserRequest.getUsername(), createUserRequest.getDirectoryId());

        //Validate application keys - defaults configured - licensed - has license limit
        final Collection<String> appKeyWarnings = applicationHelper.validateApplicationKeys(
                Optional.ofNullable(createUserRequest.getDirectoryId()), applicationKeys);
        if (!appKeyWarnings.isEmpty()) {
            WarningCollection warnings = validations.getWarnings();
            appKeyWarnings.stream().forEach(warnings::addWarning);
        }

        return new CreateUserValidationResult(createUserRequest, applicationKeys,
                validations.getErrors(),
                passwordErrors, validations.getWarnings());
    }


    @Override
    public CreateUserValidationResult validateCreateUserForAdmin(final ApplicationUser loggedInUser,
                                                                 final String username, final String password, final String confirmPassword, final String email,
                                                                 final String fullname) {
        return validateCreateUserForAdmin(loggedInUser, username, password, confirmPassword, email, fullname, null);
    }

    @Override
    public CreateUserValidationResult validateCreateUserForAdmin(ApplicationUser loggedInUser, String username,
                                                                 String password, String confirmPassword, String email, String fullname, @Nullable Long directoryId) {
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(loggedInUser,
                username, password, email, fullname)
                .inDirectory(directoryId)
                .confirmPassword(confirmPassword);
        return validateCreateUser(createUserRequest);
    }

    @Override
    public CreateUsernameValidationResult validateCreateUsername(final ApplicationUser loggedInUser, final String username) {
        return validateCreateUsername(loggedInUser, username, null);
    }

    @Override
    public CreateUsernameValidationResult validateCreateUsername(final ApplicationUser loggedInUser,
                                                                 final String username, final Long directoryId) {
        final Validations validations = validationsHelper.validations(loggedInUser);
        validations.hasValidUsername(username, directoryId);
        return new CreateUsernameValidationResult(username, directoryId, validations.getErrors());
    }

    @Override
    public ApplicationUser createUser(@Nonnull final CreateUserValidationResult result)
            throws PermissionException, CreateException {
        Assertions.notNull("You can not create a user, validation result", result);
        Assertions.is("Make sure to call validateCreateUser(CreateUserRequest) before "
                + "calling createUser(CreateUserValidationResult).", result.hasCreateUserRequest());
        if (!result.isValid()) {
            throw new CreateException("Validation failed, user " + result.getUsername() + " cannot be created");
        }

        final CreateUserRequest createUserRequest = result.getCreateUserRequest();
        ApplicationUser user = null;

        try {
            //Create user in directory.
            user = userManager.createUser(toApplicationUserCreationData(result));

            // add the user to applications only when all applications and affiliated applications (from group sharing)
            // have seats available
            Collection<String> validateApplicationKeys = applicationHelper.validateApplicationKeys(
                    Optional.ofNullable(createUserRequest.getDirectoryId()), result.getApplicationKeys());
            if (validateApplicationKeys.isEmpty()) {
                //Get the group(s) the user should be added to.
                final Set<Group> groupsForCreate = applicationHelper.getDefaultGroupsForNewUser(result.getApplicationKeys());
                userUtil.addUserToGroups(groupsForCreate, user);
            } else log.warn("User with name " + user.getName() + " created, but not added to any groups.");
        } catch (AddException ignore) {
            //User already created, current functionality is to ignore error
            log.warn("User with name " + createUserRequest.getUsername() + " created, but not added to any groups.");
        } finally {
            if (user != null) {
                dispatchUserCreatedEvent(result.getUsername(), result.getEmail(), result.getFullname(), result.getDirectoryId(),
                        createUserRequest.getUserEventType(),
                        user,
                        createUserRequest.shouldSendNotification());
            }
        }
        return user;
    }

    private UserDetails toApplicationUserCreationData(CreateUserValidationResult validationResult) {
        return new UserDetails(validationResult.getUsername(), validationResult.getFullname())
                .withDirectory(validationResult.getDirectoryId())
                .withEmail(validationResult.getEmail())
                .withPassword(validationResult.getPassword());
    }

    @Override
    public ApplicationUser createUserNoNotification(final CreateUserValidationResult result)
            throws PermissionException, CreateException {
        notNull("You can not create a user with a null validation result.", result);
        Assertions.stateTrue("You can not create a user with an invalid validation result.", result.isValid());

        CreateUserRequest createUserRequest = CreateUserRequest
                .withUserDetails(null, result.getUsername(), result.getPassword(), result.getEmail(), result.getFullname())
                .inDirectory(result.getDirectoryId())
                .sendNotification(false)
                .withApplicationAccess(result.getApplicationKeys())
                .skipValidation();

        return createUser(validateCreateUser(createUserRequest));
    }

    @Override
    public ApplicationUser createUserFromSignup(final CreateUserValidationResult result)
            throws PermissionException, CreateException {
        return createUserWithNotification(result, UserEventType.USER_SIGNUP);
    }

    @Override
    public ApplicationUser createUserWithNotification(final CreateUserValidationResult result)
            throws PermissionException, CreateException {
        return createUserWithNotification(result, UserEventType.USER_CREATED);
    }

    private ApplicationUser createUserWithNotification(final CreateUserValidationResult result, int eventType)
            throws PermissionException, CreateException {
        notNull("You can not create a user, validation result", result);
        Assertions.stateTrue("You can not create a user with an invalid validation result.", result.isValid());

        CreateUserRequest createUserRequest = CreateUserRequest
                .withUserDetails(null, result.getUsername(), result.getPassword(), result.getEmail(), result.getFullname())
                .inDirectory(result.getDirectoryId())
                .sendNotification(true)
                .withApplicationAccess(result.getApplicationKeys())
                .withEventUserEvent(eventType)
                .skipValidation();

        return createUser(validateCreateUser(createUserRequest));
    }

    @Override
    public UpdateUserValidationResult validateUpdateUser(ApplicationUser user) {
        final ApplicationUser loggedInUser = jiraAuthenticationContext.getUser();
        final I18nHelper i18nBean = getI18nBean(loggedInUser);
        final ErrorCollection errors = new SimpleErrorCollection();

        if (!isAdministrator(loggedInUser)) {
            errors.addErrorMessage(i18nBean.getText("admin.errors.users.update.no.permission"));
            return new UpdateUserValidationResult(errors);
        }
        // Check the user actually exists
        ApplicationUser userToUpdate = userManager.getUserByKey(user.getKey());
        if (userToUpdate == null) {
            errors.addErrorMessage(i18nBean.getText("admin.errors.users.user.does.not.exist"));
            return new UpdateUserValidationResult(errors);
        }
        // Is the directory writable?
        if (!userManager.canUpdateUser(userToUpdate)) {
            errors.addErrorMessage(i18nBean.getText("admin.errors.cannot.edit.user.directory.read.only"));
            return new UpdateUserValidationResult(errors);
        }
        // Is a standard admin trying to update a SysAdmin?
        if (!isSysAdmin(loggedInUser) && isSysAdmin(userToUpdate)) {
            errors.addErrorMessage(i18nBean.getText("admin.errors.must.be.sysadmin.to.edit.sysadmin"));
            return new UpdateUserValidationResult(errors);
        }

        // Special checks for deactivate
        if (!user.isActive()) {
            final Collection<ProjectComponent> components = userUtil.getComponentsUserLeads(userToUpdate);
            if (components.size() > 0) {
                String projectList = getDisplayableProjectList(getProjectsFor(components));
                // Show this error against the field, because we cannot post HTML to error messages, so we put up an
                // explicit error message with link to components. See EditUser.java and editprofile.jsp
                errors.addError("active", i18nBean.getText("admin.errors.users.cannot.deactivate.due.to.component.lead", projectList));
            }

            Collection<Project> projects = userUtil.getProjectsLeadBy(userToUpdate);
            if (projects.size() > 0) {
                String projectList = getDisplayableProjectList(projects);
                // Show this error against the field, because we cannot post HTML to error messages, so we put up an
                // explicit error message with link to components. See EditUser.java and editprofile.jsp
                errors.addError("active", i18nBean.getText("admin.errors.users.cannot.deactivate.due.to.project.lead", projectList));
            }

            if (loggedInUser.getName().equalsIgnoreCase(user.getUsername())) {
                errors.addErrorMessage(i18nBean.getText("admin.errors.users.cannot.deactivate.currently.logged.in"));
            }
        }

        // Trying to rename?
        if (!IdentifierUtils.equalsInLowerCase(userToUpdate.getUsername(), user.getUsername())) {
            if (userManager.canRenameUser(userToUpdate)) {
                final Validations validations = validationsHelper.validations(loggedInUser);
                // We pass null DirectoryID because we want to check if this username exists in _any_ directory.
                if (!validations.hasValidUsername(user.getUsername(), null)) {
                    errors.addErrors(validations.getErrors().getErrors());
                }
            } else {
                errors.addErrorMessage(i18nBean.getText("admin.errors.cannot.rename.due.to.configuration"));
            }

        }

        if (errors.hasAnyErrors()) {
            return new UpdateUserValidationResult(errors);
        } else {
            return new UpdateUserValidationResult(user);
        }
    }

    private Collection<Project> getProjectsFor(Collection<ProjectComponent> components) {
        ProjectManager projectManager = ComponentAccessor.getProjectManager();
        HashSet<Project> projects = new HashSet<>(components.size());
        for (ProjectComponent component : components) {
            projects.add(projectManager.getProjectObj(component.getProjectId()));
        }
        return projects;
    }

    private String getDisplayableProjectList(Collection<Project> projects) {
        final Iterable<String> projectKeys = transform(projects, Project::getKey);
        return StringUtils.join(projectKeys, ", ");
    }

    @Override
    public void updateUser(UpdateUserValidationResult updateUserValidationResult) {
        if (updateUserValidationResult.isValid()) {
            // Keep the old user around to see what changed
            final ApplicationUser oldUser = userManager.getUserByKey(updateUserValidationResult.getApplicationUser().getKey());
            userManager.updateUser(updateUserValidationResult.getApplicationUser());
            // Send event
            if (IdentifierUtils.equalsInLowerCase(oldUser.getUsername(), updateUserValidationResult.getApplicationUser().getUsername())) {
                eventPublisher.publish(new UserProfileUpdatedEvent(updateUserValidationResult.getApplicationUser(),
                        jiraAuthenticationContext.getUser()));
            } else {
                // The username changed: send a more specific event
                eventPublisher.publish(new UserRenamedEvent(updateUserValidationResult.getApplicationUser(),
                        jiraAuthenticationContext.getUser(), oldUser.getUsername()));
            }
        } else {
            throw new IllegalStateException("Invalid UpdateUserValidationResult");
        }
    }

    @Override
    public DeleteUserValidationResult validateDeleteUser(final ApplicationUser loggedInUser, final String username) {
        final I18nHelper i18nBean = getI18nBean(loggedInUser);
        final ErrorCollection errors = new SimpleErrorCollection();

        if (username == null || username.length() == 0) {
            errors.addError("username", i18nBean.getText("admin.errors.users.cannot.delete.due.to.invalid.username"));
            return new DeleteUserValidationResult(errors);
        }

        return validateDeleteUser(loggedInUser, userManager.getUserByName(username));
    }

    @Override
    public DeleteUserValidationResult validateDeleteUser(final ApplicationUser loggedInUser, final ApplicationUser userForDelete) {
        final I18nHelper i18nBean = getI18nBean(loggedInUser);
        final ErrorCollection errors = new SimpleErrorCollection();

        if (!permissionManager.hasPermission(Permissions.ADMINISTER, loggedInUser)) {
            errors.addErrorMessage(i18nBean.getText("admin.errors.users.delete.no.permission"));
            return new DeleteUserValidationResult(errors);
        }

        if (!userManager.isUserExisting(userForDelete)) {
            errors.addErrorMessage(i18nBean.getText("admin.errors.users.user.does.not.exist"));
            return new DeleteUserValidationResult(errors);
        }

        if (userForDelete.equals(loggedInUser)) {
            errors.addErrorMessage(i18nBean.getText("admin.errors.users.cannot.delete.currently.logged.in"));
            return new DeleteUserValidationResult(errors);
        }

        if (!userManager.canUpdateUser(userForDelete)) {
            errors.addErrorMessage(i18nBean.getText("admin.errors.users.cannot.delete.user.read.only"));
            return new DeleteUserValidationResult(errors);
        }

        if (!isSysAdmin(loggedInUser) && isSysAdmin(userForDelete)) {
            errors.addErrorMessage(i18nBean.getText("admin.errors.users.cannot.delete.due.to.sysadmin"));
            return new DeleteUserValidationResult(errors);
        }

        try {
            if (!userManager.getUserState(userForDelete).isInMultipleDirectories()) {
                validateDeleteUserReferences(loggedInUser, userForDelete, i18nBean, errors);
            }
        } catch (Exception e) {
            errors.addErrorMessage(i18nBean.getText("admin.errors.exception.occured.validating") + " " + e);
        }

        if (errors.hasAnyErrors()) {
            return new DeleteUserValidationResult(errors);
        }

        return new DeleteUserValidationResult(userForDelete);
    }

    private void validateDeleteUserReferences(ApplicationUser loggedInUser, ApplicationUser userForDelete,
                                              I18nHelper i18nBean, ErrorCollection errors) throws SearchException {
        final String username = userForDelete.getUsername();

        final long numberOfReportedIssues = userUtil.getNumberOfReportedIssuesIgnoreSecurity(loggedInUser, userForDelete);
        if (numberOfReportedIssues > 0) {
            errors.addErrorMessage(i18nBean.getText("admin.errors.users.cannot.delete.due.to.reported.issues", "'"
                    + username + "'", "" + numberOfReportedIssues));
        }

        final long numberOfAssignedIssues = userUtil.getNumberOfAssignedIssuesIgnoreSecurity(loggedInUser, userForDelete);
        if (numberOfAssignedIssues > 0) {
            errors.addErrorMessage(i18nBean.getText("admin.errors.users.cannot.delete.due.to.assigned.issues", "'"
                    + username + "'", "" + numberOfAssignedIssues));
        }

        final long numberOfComments = userDeleteVeto.getCommentCountByAuthor(userForDelete);
        if (numberOfComments > 0) {
            errors.addErrorMessage(i18nBean.getText("admin.errors.users.cannot.delete.due.to.commented.issues", "'" +
                    username + "'", "" + numberOfComments));
        }

        final long numberOfProjectsUserLeads = userUtil.getProjectsLeadBy(userForDelete).size();
        if (numberOfProjectsUserLeads > 0) {
            errors.addErrorMessage(i18nBean.getText("admin.errors.users.cannot.delete.due.to.project.lead", "'"
                    + username + "'", "" + numberOfProjectsUserLeads));
        }

        ImmutableList<WebErrorMessage> lst = preDeleteUserErrorsManager.getWarnings(userForDelete);
        for (WebErrorMessage errorMessage : lst) {
            errors.addErrorMessage(errorMessage.getDescription());
        }
    }

    private boolean isAdministrator(@Nullable ApplicationUser user) {
        return permissionManager.hasPermission(Permissions.ADMINISTER, user);
    }

    private boolean isSysAdmin(@Nullable ApplicationUser user) {
        return permissionManager.hasPermission(Permissions.SYSTEM_ADMIN, user);
    }

    @Override
    public void removeUser(final ApplicationUser loggedInUser, final DeleteUserValidationResult result) {
        notNull("You can not remove a user with a null validation result.", result);
        Assertions.stateTrue("You can not remove a user with an invalid validation result.", result.isValid());

        final ApplicationUser userForDelete = result.getApplicationUser();
        userUtil.removeUser(loggedInUser, userForDelete);
    }

    I18nHelper getI18nBean(final ApplicationUser user) {
        return i18nFactory.getInstance(user);
    }

    @Override
    public AddUserToApplicationValidationResult validateAddUserToApplication(ApplicationUser user, ApplicationKey applicationKey) {
        ApplicationUser loggedInUser = null;
        if (jiraAuthenticationContext != null) {
            loggedInUser = jiraAuthenticationContext.getLoggedInUser();
        }
        return validateAddUserToApplication(loggedInUser, user, applicationKey);
    }

    @Override
    public AddUserToApplicationValidationResult validateAddUserToApplication(ApplicationUser loggedInUser, ApplicationUser user, ApplicationKey applicationKey) {
        final ErrorCollection errors = new SimpleErrorCollection();

        if (loggedInUser == null || !isAdministrator(loggedInUser)) {
            errors.addErrorMessage(getI18nBean(loggedInUser).getText("admin.errors.users.add.user.to.application.no.permission"));
            return new AddUserToApplicationValidationResult(errors);
        }

        final Collection<String> applicationKeyErrors = applicationHelper.validateApplicationKeys(user, ImmutableSet.of(applicationKey));
        errors.addErrorMessages(applicationKeyErrors);

        if (errors.hasAnyErrors()) {
            return new AddUserToApplicationValidationResult(errors);
        }

        return new AddUserToApplicationValidationResult(user, applicationRoleManager.getDefaultGroups(applicationKey));
    }

    @Override
    public void addUserToApplication(AddUserToApplicationValidationResult result)
            throws AddException, PermissionException {
        notNull("You can not add a user to an application with a null validation result.", result);
        Assertions.is("You can not add a user to an application with an invalid validation result.", result.isValid());

        userUtil.addUserToGroups(result.getApplicationDefaultGroups(), result.getUserToAdd());
    }

    @Override
    public RemoveUserFromApplicationValidationResult validateRemoveUserFromApplication(ApplicationUser user, ApplicationKey applicationKey) {
        ApplicationUser loggedInUser = null;
        if (jiraAuthenticationContext != null) {
            loggedInUser = jiraAuthenticationContext.getLoggedInUser();
        }
        return this.validateRemoveUserFromApplication(loggedInUser, user, applicationKey);
    }

    @Override
    public RemoveUserFromApplicationValidationResult validateRemoveUserFromApplication(ApplicationUser loggedInUser, ApplicationUser user, ApplicationKey applicationKey) {
        notNull("user", user);
        notNull("applicationKey", applicationKey);

        final I18nHelper i18nBean = getI18nBean(loggedInUser);
        final ErrorCollection errors = new SimpleErrorCollection();

        if (loggedInUser == null || !isAdministrator(loggedInUser)) {
            errors.addErrorMessage(i18nBean.getText("admin.errors.users.remove.user.to.application.no.permission"));
            return new RemoveUserFromApplicationValidationResult(errors);
        }

        Option<ApplicationRole> applicationRoleOption = applicationRoleManager.getRole(applicationKey);
        Set<String> applicationGroupNames = null;

        if (applicationRoleOption.isEmpty()) {
            errors.addErrorMessage(i18nBean.getText("admin.errors.application.not.defined.by.key", applicationKey));
        } else {
            applicationGroupNames = applicationRoleOption.get().getGroups().stream()
                    .map(Group::getName)
                    .collect(CollectorsUtil.toImmutableSet());
        }

        validateGroupsAreWritableInUserDirectory(user, errors);
        validateNotRemovingAllAdminGroups(loggedInUser, user, errors, applicationGroupNames);

        if (errors.hasAnyErrors()) {
            return new RemoveUserFromApplicationValidationResult(errors);
        }

        return new RemoveUserFromApplicationValidationResult(user, applicationGroupNames);
    }

    @Override
    public void removeUserFromApplication(RemoveUserFromApplicationValidationResult result)
            throws RemoveException, PermissionException {
        notNull("You can not remove a user from an application with a null validation result.", result);
        Assertions.is("You can not remove a user from an application with an invalid validation result.", result.isValid());

        for (String groupName : result.getApplicationGroupNames()) {
            Group group = userUtil.getGroup(groupName);

            if (group != null) {
                userUtil.removeUserFromGroup(group, result.getUserToRemove());
            }
        }
    }

    /**
     * Validates that the user directory is not fully read-only.
     * <p>
     * If the underlying directory is configured as "Read Only with Local Groups", it will pass the validation.
     * Therefore, there is a chance which exception will be thrown when we are trying to add/remove user to/from group
     * even when this validation is passed.
     */
    private void validateGroupsAreWritableInUserDirectory(ApplicationUser user, ErrorCollection errors) {
        final I18nHelper i18nBean = jiraAuthenticationContext.getI18nHelper();

        // LDAP read-only with Local Groups is a tricky case.
        // If we can (potentially) create a group in the User Directory then we can (potentially) add group memberships
        Directory directory = userManager.getDirectory(user.getDirectoryId());
        if (!directory.getAllowedOperations().contains(OperationType.CREATE_GROUP)) {
            errors.addErrorMessage(i18nBean.getText("admin.errors.directory.fully.read.only", directory.getName()));
        }
    }

    private void dispatchUserCreatedEvent(final String username, final String email, final String displayName,
                                          final @Nullable Long directoryId, int userEventType, @Nullable ApplicationUser user,
                                          boolean shouldSendEmail) {
        final Builder<String, Object> mapBuilder = ImmutableMap.<String, Object>builder()
                .put(USERNAME, username)
                .put(EMAIL, email)
                .put(DISPLAY_NAME, displayName);

        if (directoryId != null) {
            mapBuilder.put(DIRECTORY_NAME, userManager.getDirectory(directoryId).getName());
            mapBuilder.put(DIRECTORY_ID, directoryId);
        }

        if (userManager.canUpdateUserPassword(user)) {
            final PasswordResetToken passwordResetToken = userUtil.generatePasswordResetToken(user);
            mapBuilder.put(PASSWORD_TOKEN, passwordResetToken.getToken());
            mapBuilder.put(PASSWORD_HOURS, passwordResetToken.getExpiryHours());
        }

        if (shouldSendEmail) {
            mapBuilder.put(SEND_EMAIL, shouldSendEmail);
        }

        UserEventDispatcher.dispatchEvent(userEventType, user, mapBuilder.build());
    }

    /**
     * Validates whether admin is removing the groups, that are giving him the admin permissions
     *
     * @param loggedInUser          logged in user
     * @param user                  user to remove permissions
     * @param errors                errors
     * @param applicationGroupNames group names to check
     */
    private void validateNotRemovingAllAdminGroups(final ApplicationUser loggedInUser, final ApplicationUser user, final ErrorCollection errors, final Set<String> applicationGroupNames) {
        final I18nHelper i18nBean = getI18nBean(loggedInUser);
        if (loggedInUser.equals(user) && applicationGroupNames != null) {
            if (globalPermissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, loggedInUser)) {
                if (globalPermissionGroupAssociationUtil.isRemovingAllMySysAdminGroups(applicationGroupNames, loggedInUser)) {
                    errors.addErrorMessage(i18nBean.getText("admin.errors.application.cannot.remove.application.last.admin.group"));
                }
            } else if (globalPermissionGroupAssociationUtil.isRemovingAllMyAdminGroups(applicationGroupNames, loggedInUser)) {
                errors.addErrorMessage(i18nBean.getText("admin.errors.application.cannot.remove.application.last.admin.group"));
            }
        }
    }
}
