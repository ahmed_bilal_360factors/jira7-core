package com.atlassian.jira.security;

import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.event.user.LoginEvent;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.flag.FlagDismissalService;
import com.atlassian.jira.user.util.Users;

import javax.inject.Inject;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * An Administrator will be locked out of issues and projects if they do not have access to an application.
 * We will warn the administrators of such a configuration with a UI flag. This class provides come common utility
 * methods to help with the implementation of this flag.
 *
 * @since v7.0
 */
@EventComponent
public class AdminIssueLockoutFlagManager {
    /**
     * The flag ID displayed in the UI.
     */
    public final static String FLAG = "admin.lockout";

    private final GlobalPermissionManager globalPermissionManager;
    private final FlagDismissalService flagDismissalService;
    private final ApplicationRoleManager applicationRoleManager;

    @Inject
    public AdminIssueLockoutFlagManager(final GlobalPermissionManager globalPermissionManager,
                                        final FlagDismissalService flagDismissalService,
                                        final ApplicationRoleManager applicationRoleManager) {
        this.applicationRoleManager = notNull("applicationRoleManager", applicationRoleManager);
        this.globalPermissionManager = notNull("globalPermissionManager", globalPermissionManager);
        this.flagDismissalService = notNull("flagDismissalService", flagDismissalService);
    }

    /**
     * At each login we check if the user is an admin. If they have access to an application we clear the dismissal
     * flag so that the flag will display again if that admin loses application access. We don't want the dismissal
     * to be permanent.
     *
     * @param loginEvent the user that just logged in.
     */
    @EventListener
    public void removeDismissalOnLogin(LoginEvent loginEvent) {
        final ApplicationUser user = loginEvent.getUser();
        if (globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)
                && applicationRoleManager.hasAnyRole(user)) {
            flagDismissalService.removeDismissFlagForUser(FLAG, user);
        }
    }

    /**
     * Return {@code true} if the passed user is an admin that does not have access to projects and issues.
     *
     * @param user the user to check.
     * @return {@code true} if the passed user is an admin who does not have access to any projects or issues.
     */
    public boolean isAdminWithoutIssuePermission(ApplicationUser user) {
        return !Users.isAnonymous(user)
                && globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)
                && !applicationRoleManager.hasAnyRole(user);
    }
}
