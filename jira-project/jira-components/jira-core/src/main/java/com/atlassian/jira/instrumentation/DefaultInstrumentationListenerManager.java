package com.atlassian.jira.instrumentation;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.cache.ManagedCache;
import com.atlassian.instrumentation.caches.CacheCollector;
import com.atlassian.instrumentation.caches.RequestListener;
import com.atlassian.jira.cache.JiraVCacheRequestContextSupplier;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.vcache.internal.RequestMetrics;
import com.atlassian.vcache.internal.VCacheLifecycleManager;
import com.google.common.collect.ImmutableList;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.jira.instrumentation.CacheStatisticsUtils.convertVCacheMetrics;
import static com.atlassian.jira.instrumentation.CacheStatisticsUtils.fromRequestListenerStatistics;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Stream.concat;

/**
 * Processes instrumentation listeners.
 *
 * @since v7.1
 */
public class DefaultInstrumentationListenerManager implements InstrumentationListenerManager {

    private static final String CACHETYPE_CACHE = "C";
    private static final String CACHETYPE_CACHED_REFERENCE = "R";

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultInstrumentationListenerManager.class);

    private static final ThreadLocal<RequestData> requestData = new ThreadLocal<>();

    /**
     * List of registered listeners.
     */
    private final List<RequestListener> requestListeners = new CopyOnWriteArrayList<>();

    /**
     * The logger that saves the instrumentation data
     */
    private final InstrumentationLogger instrumentationLogger;
    private final VCacheLifecycleManager vCacheLifecycleManager;
    private final JiraVCacheRequestContextSupplier contextSupplier;
    private final CacheManager cacheManager;

    public DefaultInstrumentationListenerManager(InstrumentationLogger instrumentationLogger,
                                                 VCacheLifecycleManager vCacheLifecycleManager, JiraVCacheRequestContextSupplier contextSupplier,
                                                 final CacheManager cacheManager) {
        this.cacheManager = cacheManager;
        this.vCacheLifecycleManager = requireNonNull(vCacheLifecycleManager, "vCacheLifecycleManager cannot be null");
        this.contextSupplier = requireNonNull(contextSupplier, "contextSupplier cannot be null");
        this.instrumentationLogger = requireNonNull(instrumentationLogger, "instrumentationLogger cannot be null");
    }

    /**
     * Run all the listeners at request start up.
     */
    @Override
    public void processOnStartListeners() {
        requestListeners.stream().forEach(listener ->
        {
            try {
                listener.onRequestStart();
            } catch (RuntimeException e) {
                LOGGER.info("instrumentation listener threw an exception", e);
            }
        });
    }

    @Override
    public void processOnEndListeners(RequestData requestData, Optional<Long> requestTime) {
        // Skip javascript, css etc. Also exclude stats from the instrumentation pages.
        if (requestData.getPath().map(this::excludePath).orElseGet(() -> false)) {
            return;
        }

        final Stream<CacheStatistics> atlassianCacheStats = collectAtlassianCacheStats();
        final Stream<Statistics> otherMetrics = collectOtherMetrics();
        final Stream<CacheStatistics> vCacheStats = collectVCacheStats();

        instrumentationLogger.save(requestData, concat(concat(atlassianCacheStats, vCacheStats), otherMetrics)
                .collect(Collectors.toList()), requestTime);
    }

    @NotNull
    @Override
    public void addRequestListener(@Nonnull RequestListener listener) {
        requireNonNull(listener);
        if (!requestListeners.contains(listener)) {
            requestListeners.add(listener);
        }
    }

    @NotNull
    @Override
    public void removeRequestListener(@Nonnull RequestListener listener) {
        requireNonNull(listener);
        requestListeners.remove(listener);
    }

    /**
     * Called at the beginning of a request to handle the setup.
     *
     * @param path An identifier for the request. This will be the path for HttpRequests and the thread name for
     *             threads.
     */
    public static void startContext(@Nonnull final String path) {
        startContext(path, null, null, null, null, null);
    }

    /**
     * Called at the beginning of a request to handle the setup.
     *
     * @param path          An identifier for the request. This will be the path for HttpRequests and the thread name for
     *                      threads.
     * @param traceId       Pass in a traceId. If it is null, then we will generate our own.
     * @param spanId        If X_B3_SPAN_ID is set on the request, pass this through
     * @param parentSpanId  If X_B3_PARENT_SPAN_ID is set on the request, pass it through.
     * @param queryString   query string that was provided for that request.
     * @param startNanoTime The time that the request began in nanos.
     */
    public static void startContext(@Nonnull final String path, @Nullable final String traceId, @Nullable final String spanId,
                                    @Nullable final String parentSpanId, @Nullable final String queryString, @Nullable Long startNanoTime) {
        if (requestData.get() != null) {
            return;
        }

        // Get a UUID if there is no traceId passed in from the filter.
        DefaultInstrumentationListenerManager.requestData.set(
                new RequestData.Builder()
                        .setPath(path)
                        .setTraceId(traceId == null ? UUID.randomUUID().toString() : traceId)
                        .setSpanId(spanId)
                        .setParentSpanId(parentSpanId)
                        .setQueryString(queryString)
                        .setStartNanoTime(startNanoTime)
                        .build());

        ComponentAccessor.getComponentSafely(InstrumentationListenerManager.class)
                .ifPresent(InstrumentationListenerManager::processOnStartListeners);
    }

    /**
     * End of the context where we are not timing the execution.
     */
    public static void endContext() {
        endContext(Optional.empty());
    }

    /**
     * Called at the end of the request to flush to the currently selected persistence mechanism. By default, 50
     * requests are stored in memory.
     *
     * @param requestTime An {@link Optional} that may contain the request time
     */
    public static void endContext(Optional<Long> requestTime) {
        try {
            if (requestData.get() == null) {
                return;
            }

            // Get the real instance.
            Optional<InstrumentationListenerManager> manager = ComponentAccessor.getComponentSafely(InstrumentationListenerManager.class);
            if (manager.isPresent()) {
                RequestData data = requestData.get();
                manager.get().processOnEndListeners(data, requestTime);
            }

        } finally {
            // clean up in case someone comes along and does not obey our lifecycle correctly
            requestData.remove();
        }
    }

    @Override
    public void applyToAllListeners(Consumer<RequestListener> apply) {
        requestListeners.forEach(apply);
    }

    @Nonnull
    @Override
    public Optional<String> getCurrentPath() {
        if (requestData.get() != null) {
            return requestData.get().getPath();
        } else {
            return Optional.empty();
        }
    }

    @Nonnull
    @Override
    public Optional<String> getCurrentTraceId() {
        if (requestData.get() != null) {
            return requestData.get().getTraceId();
        } else {
            return Optional.empty();
        }
    }

    @Nonnull
    @Override
    public Optional<RequestData> getRequestData() {
        return Optional.ofNullable(requestData.get());
    }

    /**
     * List of end of URLs that we want to filter out of instrumenting.
     */
    private final List<String> exclude = ImmutableList.of(
// Get the tracking for static resources back
//            ".js", ".css", ".gif", ".png",
            "ViewUris.jspa", "ViewCacheStats.jspa", "ViewCacheDetails.jspa", "ViewCachesByRequest.jspa",
            "ManageCacheInstrumentation.jspa", "ClearBuffer.jspa", "EnableAll.jspa", "DisableAll.jspa");

    /**
     * At this point we drop JavaScript, CSS and images from collection. This keeps memory usage down, but underreports
     * cache usage.
     *
     * @param path The path of the request
     * @return True if we want to collect this URL.
     */
    private boolean excludePath(@Nonnull final String path) {
        requireNonNull(path);
        return exclude.stream().anyMatch(path::endsWith);
    }

    private Stream<CacheStatistics> collectAtlassianCacheStats() {
        // Save them all, provided we are enabled and there is data.
        return requestListeners.stream()
                // Leave out listeners that are disabled.
                .filter(listener -> listener.isEnabled() && listener instanceof CacheCollector)
                // Convert stats to bean for processing.
                .map(this::getAtlassianCacheStatistics)
                // If there are no stats to report, leave it out
                .filter(stat -> Objects.nonNull(stat) && stat.getStatsMap().size() != 0);
    }

    private CacheStatistics getAtlassianCacheStatistics(RequestListener listener) {
        try {
            // Default type is an ordinary cache. If we are a CachedReference, we'll change to 'R' for CachedReference
            String cacheType = CACHETYPE_CACHE;
            final ManagedCache managedCache = cacheManager.getManagedCache(listener.getName());
            if (managedCache instanceof CachedReference) {
                cacheType = CACHETYPE_CACHED_REFERENCE;
            }
            return fromRequestListenerStatistics(listener, cacheType);
        } catch (RuntimeException e) {
            LOGGER.warn("instrumentation listener {} threw an exception", listener.getName(), e);
            return null;
        }
    }

    private Stream<CacheStatistics> collectVCacheStats() {
        if (contextSupplier.isInitilised()) {
            final RequestMetrics metrics = vCacheLifecycleManager.metrics(contextSupplier.get());

            final Stream<CacheStatistics> vCacheJvmStats = convertVCacheMetrics("JVMCache",
                    metrics.allJvmCacheLongMetrics());
            final Stream<CacheStatistics> vCacheRequestStats = convertVCacheMetrics("RequestCache",
                    metrics.allRequestCacheLongMetrics());
            final Stream<CacheStatistics> vCacheExternalStats = convertVCacheMetrics("ExternalCache",
                    metrics.allExternalCacheLongMetrics());
            return concat(vCacheJvmStats, concat(vCacheRequestStats, vCacheExternalStats));
        } else {
            return Stream.empty();
        }
    }

    private Stream<Statistics> collectOtherMetrics() {
        List<Statistics> result = new ArrayList<>();
        requestListeners.stream()
                // Leave out listeners that are disabled.
                .filter(listener -> listener.isEnabled() && !(listener instanceof CacheCollector))
                .forEach(listener -> listener.onRequestEnd().entrySet().forEach(entry -> {
                    StatisticsAdapter a = new StatisticsAdapter(entry.getKey(), listener.getLoggingKey(), listener.getTags(), entry.getValue());
                    result.add(a);
                }));
        return result.stream();
    }

    class StatisticsAdapter implements Statistics {
        private final String name;
        private final String loggingKey;
        private final List<String> tags;
        private final Object resultsMap;

        public StatisticsAdapter(String name, String loggingKey, List<String> tags, Object entry) {
            this.name = name;
            this.loggingKey = loggingKey;
            this.tags = tags;
            this.resultsMap = entry;
        }

        @Override
        @JsonProperty
        public String getName() {
            return name;
        }

        @Override
        @JsonIgnore
        public String getLoggingKey() {
            return loggingKey;
        }

        @Override
        @JsonProperty
        public List<String> getTags() {
            return tags;
        }

        @Override
        @JsonProperty
        public Object getStats() {
            return resultsMap;
        }
    }
}
