package com.atlassian.jira.web.action.project;

import com.atlassian.fugue.Option;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import org.springframework.util.StringUtils;

/**
 * Gets project data
 *
 * @since 7.2
 */
public class ProjectDataProvider implements WebResourceDataProvider {
    private final ProjectManager projectManager;

    private static final String PROJECT_TYPE = "projectType";
    private static final String PROJECT_KEY = "projectKey";


    /**
     * NOTE: These objects are not dependency injected because this class is sometimes loaded (instance setup) when the
     * objects are not available causing a stack trace. Safely trying to access these objects stops these failures.
     */
    public ProjectDataProvider() {
        this.projectManager = getProjectManager();
    }

    @Override
    public Jsonable get() {
        return writer -> {
            try {
                getJsonData().write(writer);
            } catch (JSONException e) {
                throw new Jsonable.JsonMappingException(e);
            }
        };
    }

    private JSONObject getJsonData() throws JSONException {
        final JSONObject values = new JSONObject();

        if (canAccessComponents()) {
            Option<String> optionalProjectType = getProjectType();
            if (optionalProjectType.isDefined()) {
                values.put(PROJECT_TYPE, optionalProjectType.get());
            }
        }

        return values;
    }

    private Option<String> getProjectType() {
        String projectKey = (String)ExecutingHttpRequest.get().getAttribute(PROJECT_KEY);
        if (StringUtils.isEmpty(projectKey)) {
            return Option.none();
        }

        Project project = projectManager.getProjectByCurrentKey(projectKey);
        if (project == null) {
            return Option.none();
        }

        return Option.option(project.getProjectTypeKey().getKey());
    }

    private boolean canAccessComponents() {
        return (projectManager != null);
    }

    private ProjectManager getProjectManager() {
        return getComponentOrNull(ProjectManager.class);
    }

    private static <T> T getComponentOrNull(Class<T> clazz) {
        Option<T> componentOption = Option.option(ComponentAccessor.getComponentSafely(clazz).orElse(null));

        return componentOption.getOrNull();
    }
}