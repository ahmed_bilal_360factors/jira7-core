package com.atlassian.jira.bc.filter;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.project.property.ProjectPropertyService;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.entity.property.EntityPropertyService;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Objects;

import static com.atlassian.jira.bc.project.ProjectAction.EDIT_PROJECT_CONFIG;
import static com.atlassian.jira.bc.project.ProjectAction.VIEW_ISSUES;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;

public class ProjectSearchRequestServiceImpl implements ProjectSearchRequestService {
    private static final Logger log = LoggerFactory.getLogger(ProjectSearchRequestServiceImpl.class);
    private static final String PROJECT_SEARCH_REQUESTS_KEY = "searchRequests";
    private static final String IDS = "ids";

    private final PermissionManager permissionManager;
    private final ProjectPropertyService projectPropertyService;
    private final I18nHelper.BeanFactory i18nFactory;
    private final SearchRequestService searchRequestService;

    public ProjectSearchRequestServiceImpl(
            final PermissionManager permissionManager,
            final ProjectPropertyService projectPropertyService,
            final I18nHelper.BeanFactory i18nFactory,
            final SearchRequestService searchRequestService) {
        this.permissionManager = permissionManager;
        this.projectPropertyService = projectPropertyService;
        this.i18nFactory = i18nFactory;
        this.searchRequestService = searchRequestService;
    }

    @Override
    public ServiceOutcome<List<SearchRequest>> associateSearchRequestsWithProject(final ApplicationUser user, @Nonnull final Project project, final Long... searchRequestIds) {
        if (!EDIT_PROJECT_CONFIG.hasPermission(permissionManager, user, project)) {
            return ServiceOutcomeImpl.error(i18nFactory.getInstance(user).getText(EDIT_PROJECT_CONFIG.getErrorKey()), ErrorCollection.Reason.FORBIDDEN);
        }

        final EntityPropertyService.SetPropertyValidationResult result = projectPropertyService.validateSetProperty(
                user,
                project.getId(),
                toPropertyInput(toJSON(user, searchRequestIds)));
        if (result.isValid()) {
            projectPropertyService.setProperty(user, result);
        } else {
            return ServiceOutcomeImpl.from(result.getErrorCollection());
        }
        return ServiceOutcomeImpl.ok(getSearchRequestsWithoutPermissionCheck(user, project));
    }

    @Override
    public List<SearchRequest> getSearchRequestsForProject(final ApplicationUser user, @Nonnull final Project project) {
        if (!VIEW_ISSUES.hasPermission(permissionManager, user, project)) {
            return emptyList();
        }

        return getSearchRequestsWithoutPermissionCheck(user, project);
    }

    private List<SearchRequest> getSearchRequestsWithoutPermissionCheck(final ApplicationUser user, final @Nonnull Project project) {
        final EntityPropertyService.PropertyResult result = projectPropertyService.getProperty(user, project.getId(), PROJECT_SEARCH_REQUESTS_KEY);
        if (result.isValid()) {
            final Option<EntityProperty> entityProperty = result.getEntityProperty();
            if (entityProperty.isDefined()) {
                final String value = entityProperty.get().getValue();
                try {
                    final JSONObject json = new JSONObject(value);
                    return toSearchRequests(user, json);
                } catch (JSONException e) {
                    log.error("Error converting value '{0}' to JSONObject.", value, e);
                }
            }
        }
        return emptyList();
    }

    private List<SearchRequest> toSearchRequests(final ApplicationUser user, final JSONObject json) throws JSONException {
        final JSONArray jsonArray = json.getJSONArray(IDS);
        final List<SearchRequest> filters = Lists.newArrayList();
        for (int i = 0; i < jsonArray.length(); i++) {
            final long filterId = jsonArray.getLong(i);
            final SearchRequest filter = searchRequestService.getFilter(new JiraServiceContextImpl(user), filterId);
            if (filter != null) {
                filters.add(filter);
            }
        }
        return ImmutableList.copyOf(filters);
    }

    private EntityPropertyService.PropertyInput toPropertyInput(final JSONObject jsonObject) {
        return new EntityPropertyService.PropertyInput(jsonObject.toString(), PROJECT_SEARCH_REQUESTS_KEY);
    }

    private JSONObject toJSON(final ApplicationUser user, final Long[] searchRequestIds) {
        final JiraServiceContext ctx = new JiraServiceContextImpl(user);
        //check if the passed ids actually exist in the database.
        final ImmutableList<Long> validatedIds = stream(searchRequestIds)
                .map(id -> searchRequestService.getFilter(ctx, id))
                .filter(Objects::nonNull)
                .map(SearchRequest::getId)
                .collect(CollectorsUtil.toImmutableList());
        try {
            return new JSONObject().put(IDS, validatedIds);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }
}
