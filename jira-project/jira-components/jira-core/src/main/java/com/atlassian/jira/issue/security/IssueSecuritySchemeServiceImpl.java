package com.atlassian.jira.issue.security;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.base.Function;
import com.google.common.base.Supplier;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @since 7.0
 */
public class IssueSecuritySchemeServiceImpl implements IssueSecuritySchemeService {
    private final IssueSecuritySchemeManager issueSecuritySchemeManager;
    private final IssueSecurityLevelManager issueSecurityLevelManager;
    private final GlobalPermissionManager globalPermissionManager;
    private final I18nHelper i18n;
    private final PermissionManager permissionManager;
    private final ProjectManager projectManager;

    public IssueSecuritySchemeServiceImpl(
            IssueSecuritySchemeManager issueSecuritySchemeManager,
            IssueSecurityLevelManager issueSecurityLevelManager,
            GlobalPermissionManager globalPermissionManager,
            PermissionManager permissionManager,
            I18nHelper i18n,
            ProjectManager projectManager) {
        this.issueSecuritySchemeManager = issueSecuritySchemeManager;
        this.issueSecurityLevelManager = issueSecurityLevelManager;
        this.globalPermissionManager = globalPermissionManager;
        this.permissionManager = permissionManager;
        this.i18n = i18n;
        this.projectManager = projectManager;
    }

    @Override
    public ServiceOutcome<? extends Collection<IssueSecurityLevelScheme>> getIssueSecurityLevelSchemes(ApplicationUser user) {
        return asAdmin(user, () -> ServiceOutcomeImpl.ok(issueSecuritySchemeManager.getIssueSecurityLevelSchemes()));
    }

    private boolean canViewSchemeAsProjectAdmin(ApplicationUser user, final long schemeId) {
        Collection<Project> projects = permissionManager.getProjects(ProjectPermissions.ADMINISTER_PROJECTS, user);
        return !Collections.disjoint(projects, issueSecuritySchemeManager.getProjectsUsingScheme(schemeId));
    }

    private boolean canViewProjectAsAdmin(ApplicationUser user, final Project project) {
        return project != null && permissionManager.hasPermission(ProjectPermissions.ADMINISTER_PROJECTS, project, user);
    }

    @Override
    public ServiceOutcome<IssueSecurityLevelScheme> getIssueSecurityLevelScheme(ApplicationUser user, final long schemeId) {
        return asAdminOrProjectAdminWithAccessToScheme(user, schemeId, () -> {
            Supplier<ServiceOutcome<IssueSecurityLevelScheme>> notFoundSupplier =
                    notFoundSupplier("rest.error.issuesecurityscheme.securityscheme.not.found", String.valueOf(schemeId));
            return Option.option(issueSecuritySchemeManager.getIssueSecurityLevelScheme(schemeId))
                    .fold(notFoundSupplier, IssueSecuritySchemeServiceImpl.<IssueSecurityLevelScheme>okOutcome());
        });
    }

    @Override
    public ServiceOutcome<IssueSecurityLevelScheme> getIssueSecurityLevelSchemeForProject(final ApplicationUser user, final long projectId) {
        return getIssueSecurityLevelSchemeForProject(user, Either.left(projectId));
    }

    @Override
    public ServiceOutcome<IssueSecurityLevelScheme> getIssueSecurityLevelSchemeForProject(final ApplicationUser user, final String projectKey) {
        return getIssueSecurityLevelSchemeForProject(user, Either.right(projectKey));
    }

    @Override
    public ServiceOutcome<String> assignSchemeToProject(final ApplicationUser user, final long projectId, final Long newSchemeId, final Map<Long, Long> oldToNewSecurityLevelMappings) {
        return asAdminOrProjectAdminWithAdminAccessToProject(user, Either.left(projectId), project -> {
            return ServiceOutcomeImpl.ok(issueSecuritySchemeManager.assignSchemeToProject(project, newSchemeId, oldToNewSecurityLevelMappings));
        });
    }

    private ServiceOutcome<IssueSecurityLevelScheme> getIssueSecurityLevelSchemeForProject(final ApplicationUser user, final Either<Long, String> projectIdOrKey) {
        Supplier<ServiceOutcome<IssueSecurityLevelScheme>> notFoundSupplier =
                notFoundSupplier("rest.error.issuesecurityscheme.securitylevel.for.project.not.found", projectIdOrKey.fold(l -> l.toString(), String::toString));

        return asAdminOrProjectAdminWithAdminAccessToProject(user, projectIdOrKey, project -> {
            final Long schemeIdForProject = issueSecuritySchemeManager.getSchemeIdFor(project);
            final IssueSecurityLevelScheme issueSecurityLevelScheme = issueSecuritySchemeManager.getIssueSecurityLevelScheme(schemeIdForProject);

            return Option.option(issueSecurityLevelScheme)
                    .fold(notFoundSupplier, IssueSecuritySchemeServiceImpl.okOutcome());
        });
    }

    @Override
    public ServiceOutcome<? extends List<IssueSecurityLevel>> getIssueSecurityLevels(ApplicationUser user, final long schemeId) {
        return asAdminOrProjectAdminWithAccessToScheme(user, schemeId, () -> {
            Supplier<ServiceOutcome<List<IssueSecurityLevel>>> notFoundSupplier =
                    notFoundSupplier("rest.error.issuesecurityscheme.securityscheme.not.found", String.valueOf(schemeId));

            return Option.option(issueSecuritySchemeManager.getIssueSecurityLevelScheme(schemeId))
                    .fold(notFoundSupplier, securityLevelScheme -> ServiceOutcomeImpl.ok(issueSecurityLevelManager.getIssueSecurityLevels(schemeId)));
        });
    }

    @Override
    public ServiceOutcome<IssueSecurityLevel> getIssueSecurityLevel(ApplicationUser user, final long securityLevelId) {
        return asAdmin(user, () -> {
            Supplier<ServiceOutcome<IssueSecurityLevel>> notFoundSupplier =
                    notFoundSupplier("rest.error.issuesecurityscheme.securitylevel.not.found", String.valueOf(securityLevelId));
            return Option.option(issueSecurityLevelManager.getSecurityLevel(securityLevelId))
                    .fold(notFoundSupplier, IssueSecuritySchemeServiceImpl.<IssueSecurityLevel>okOutcome());
        });
    }

    @Override
    public ServiceOutcome<? extends Collection<IssueSecurityLevelPermission>> getPermissionsByIssueSecurityLevel(ApplicationUser user, final long securityLevelId) {
        return asAdmin(user, () -> {
            Supplier<ServiceOutcome<Collection<IssueSecurityLevelPermission>>> notFoundSupplier =
                    notFoundSupplier("rest.error.issuesecurityscheme.securitylevel.not.found", String.valueOf(securityLevelId));
            return Option.option(issueSecurityLevelManager.getSecurityLevel(securityLevelId))
                    .fold(notFoundSupplier, issueSecurityLevel -> {
                        Collection<IssueSecurityLevelPermission> collection = issueSecuritySchemeManager.getPermissionsBySecurityLevel(securityLevelId);
                        return ServiceOutcomeImpl.ok(collection);
                    });
        });
    }

    private static <T> Function<T, ServiceOutcome<T>> okOutcome() {
        return ServiceOutcomeImpl::ok;
    }

    private <T> Supplier<ServiceOutcome<T>> notFoundSupplier(final String i18nKey, final String... params) {
        return () -> ServiceOutcomeImpl.error(i18n.getText(i18nKey, params), ErrorCollection.Reason.NOT_FOUND);
    }

    private boolean isAdmin(final ApplicationUser user) {
        return globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user);
    }

    private <T> ServiceOutcome<T> asAdmin(final ApplicationUser user, ServiceAction<T> action) {
        return performActionWithUserPermissionCondition(user, () -> isAdmin(user), action);
    }

    private <T> ServiceOutcome<T> asAdminOrProjectAdminWithAccessToScheme(final ApplicationUser user, final Long schemeId, ServiceAction<T> action) {
        return performActionWithUserPermissionCondition(user, () -> isAdmin(user) || canViewSchemeAsProjectAdmin(user, schemeId), action);
    }

    private <T> ServiceOutcome<T> asAdminOrProjectAdminWithAdminAccessToProject(final ApplicationUser user, final Either<Long, String> projectIdOrKey, Function<Project, ServiceOutcome<T>> action) {
        final Project project = projectIdOrKey.fold(projectManager::getProjectObj, projectManager::getProjectObjByKey);
        if (project == null) {
            return ServiceOutcomeImpl.error(i18n.getText("admin.errors.portal.project.nonexist"), ErrorCollection.Reason.NOT_FOUND);
        } else {

            return performActionWithUserPermissionCondition(user, () -> isAdmin(user) || canViewProjectAsAdmin(user, project), () -> action.apply(project));
        }
    }

    private <T> ServiceOutcome<T> performActionWithUserPermissionCondition(ApplicationUser user, ParameterlessPredicate predicate, ServiceAction<T> action) {
        String forbiddenMessage = i18n.getText("admin.schemes.permissions.forbidden");
        if (user == null) {
            return ServiceOutcomeImpl.error(forbiddenMessage, ErrorCollection.Reason.NOT_LOGGED_IN);
        }
        if (!predicate.apply()) {
            return ServiceOutcomeImpl.error(forbiddenMessage, ErrorCollection.Reason.FORBIDDEN);
        }
        return action.perform();
    }

    private static interface ServiceAction<T> {
        ServiceOutcome<T> perform();
    }

    private static interface ParameterlessPredicate {
        boolean apply();
    }
}
