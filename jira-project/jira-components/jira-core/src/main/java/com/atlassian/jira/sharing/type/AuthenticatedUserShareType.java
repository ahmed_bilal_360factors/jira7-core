package com.atlassian.jira.sharing.type;

/**
 * Implementation of the ShareType that allows a {@link com.atlassian.jira.sharing.SharedEntity} to be shared with all logged in users.
 * This DOES NOT include the sharing with the anonymous user.
 *
 * This contrasts with the {@link GlobalShareType} that also makes the shared entity visible to anonymous users.
 *
 * @since v7.2.2
 */
public class AuthenticatedUserShareType extends AbstractShareType {
    public static final Name TYPE = Name.AUTHENTICATED;
    public static final int PRIORITY = 3;

    public AuthenticatedUserShareType(final AuthenticatedUserShareTypeRenderer renderer, final AuthenticatedUserShareTypeValidator validator) {
        super(AuthenticatedUserShareType.TYPE, true, AuthenticatedUserShareType.PRIORITY, renderer, validator, new AuthenticatedUserShareTypePermissionChecker(),
                new AuthenticatedUserShareQueryFactory(), new DefaultSharePermissionComparator(AuthenticatedUserShareType.TYPE));
    }
}
