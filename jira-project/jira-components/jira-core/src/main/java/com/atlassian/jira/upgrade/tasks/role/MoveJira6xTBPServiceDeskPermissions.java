package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.application.ApplicationKeys;
import com.google.common.collect.ImmutableSet;

/**
 * Migrates Service Desk permission for Tier Based Pricing. This task should be only called if Service Desk license is
 * present
 * and it is of TBP type.
 * <p>
 * All groups that have USE, ADMINISTER or SYSADMIN permission will be associated with the
 * Service Desk application role.
 * <p>
 * Additionally groups that have only USE permission will be marked as default group for Service Desk application role
 *
 * @since v7.0
 */
class MoveJira6xTBPServiceDeskPermissions extends MigrationTask {
    private final UseBasedMigration useBasedMigration;

    MoveJira6xTBPServiceDeskPermissions(final UseBasedMigration useBasedMigration) {
        this.useBasedMigration = useBasedMigration;
    }

    @Override
    MigrationState migrate(final MigrationState state, final boolean licenseSuppliedByUser) {
        return useBasedMigration.addUsePermissionToRoles(state, ImmutableSet.of(ApplicationKeys.SERVICE_DESK));
    }
}
