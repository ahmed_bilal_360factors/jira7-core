package com.atlassian.jira.issue.comments;

import com.atlassian.jira.bc.admin.ApplicationPropertiesService;
import com.atlassian.jira.bc.admin.ApplicationProperty;
import com.atlassian.jira.config.CoreFeatures;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.ActionConstants;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.IssueRelationConstants;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.JiraDateUtils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.ofbiz.core.entity.EntityCondition;
import org.ofbiz.core.entity.EntityConditionList;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityFieldMap;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static com.atlassian.jira.config.properties.APKeys.COMMENT_COLLAPSING_MINIMUM_HIDDEN;
import static java.util.stream.Collectors.toList;

/**
 * The CommentSearchManager is used to retrieve comments in JIRA.
 * Comments are always associated with an issue. This manager is only used internally.
 */
public class CommentSearchManager {
    public static final int NUMBER_OF_NEW_COMMENTS_TO_SHOW = 5;

    private final UserManager userManager;
    private final OfBizDelegator delegator;
    private final IssueManager issueManager;
    private final ProjectRoleManager projectRoleManager;
    private final CommentPermissionManager commentPermissionManager;
    private final FeatureManager featureManager;
    private final ApplicationPropertiesService applicationPropertiesService;
    private final StreamingCommentsRetriever streamingCommentsRetriever;

    private static final String COMMENT_ID = "id";
    private static final int DEFAULT_MINIMUM_NUMBER_OF_HIDDEN_COMMENTS = 4;
    private static final int MAX_COMMENTS_TO_LOAD_BY_ID = 100;

    public CommentSearchManager(final UserManager userManager,
                                final OfBizDelegator delegator,
                                final IssueManager issueManager,
                                final ProjectRoleManager projectRoleManager,
                                final CommentPermissionManager commentPermissionManager,
                                final FeatureManager featureManager,
                                final ApplicationPropertiesService applicationPropertiesService,
                                final StreamingCommentsRetriever streamingCommentsRetriever) {
        this.userManager = userManager;
        this.delegator = delegator;
        this.issueManager = issueManager;
        this.projectRoleManager = projectRoleManager;
        this.commentPermissionManager = commentPermissionManager;
        this.featureManager = featureManager;
        this.applicationPropertiesService = applicationPropertiesService;
        this.streamingCommentsRetriever = streamingCommentsRetriever;
    }

    public Comment convertToComment(GenericValue gv) {
        return convertToComment(gv, issueManager.getIssueObject(gv.getLong("issue")));
    }

    public Comment getCommentById(Long commentId) {
        return getMutableComment(commentId);
    }

    public MutableComment getMutableComment(Long commentId) {
        if (commentId == null) {
            throw new IllegalArgumentException("The comment id must not be null.");
        }

        GenericValue gv = delegator.findById(DefaultCommentManager.COMMENT_ENTITY, commentId);
        if (gv != null) {
            return convertToComment(gv, issueManager.getIssueObject(gv.getLong("issue")));
        }
        return null;
    }

    public List<Comment> getComments(Issue issue) {
        List<Comment> comments = new ArrayList<Comment>();

        try {
            // get a List<GenericValue> of comments
            List allComments = issueManager.getEntitiesByIssueObject(IssueRelationConstants.COMMENTS, issue);

            for (final Object allComment : allComments) {
                Comment comment = convertToComment((GenericValue) allComment, issue);
                comments.add(comment);
            }
        } catch (GenericEntityException e) {
            throw new DataAccessException(e);
        }

        Collections.sort(comments, CommentComparator.COMPARATOR);
        return comments;
    }

    @Nonnull
    public List<Comment> getCommentsForUserSince(@Nonnull Issue issue, @Nullable ApplicationUser user, @Nonnull Date since) {
        EntityCondition issueCommentsCondition = new EntityFieldMap(FieldMap.build("issue", issue.getId(), "type", ActionConstants.TYPE_COMMENT), EntityOperator.AND);
        EntityCondition dateCondition = new EntityExpr("updated", EntityOperator.GREATER_THAN, new Timestamp(since.getTime()));
        EntityCondition finalCondition = new EntityConditionList(Arrays.asList(issueCommentsCondition, dateCondition), EntityOperator.AND);

        // get a List<GenericValue> of comments since date
        List<GenericValue> commentsSinceDate = delegator.findByCondition(DefaultCommentManager.COMMENT_ENTITY, finalCondition, null, ImmutableList.of("updated DESC", "id ASC"));

        return getVisibleComments(issue, user, commentsSinceDate);
    }

    public List<Comment> getCommentsForUser(Issue issue, ApplicationUser user) {
        List<Comment> visibleComments;
        try {
            // get a List<GenericValue> of comments
            List<GenericValue> allComments = issueManager.getEntitiesByIssueObject(IssueRelationConstants.COMMENTS, issue);
            visibleComments = getVisibleComments(issue, user, allComments);
        } catch (GenericEntityException e) {
            throw new DataAccessException(e);
        }
        Collections.sort(visibleComments, CommentComparator.COMPARATOR);
        return visibleComments;
    }

    public Stream<Comment> streamComments(@Nullable ApplicationUser user, @Nonnull Issue issue) {
        return streamingCommentsRetriever.stream(user, issue);
    }

    private List<Comment> getVisibleComments(Issue issue, ApplicationUser user, List<GenericValue> commentValues) {
        List<Comment> visibleComments = new ArrayList<Comment>();

        for (final GenericValue commentGV : commentValues) {
            Comment comment = convertToComment(commentGV, issue);

            if (commentPermissionManager.hasBrowsePermission(user, comment)) {
                visibleComments.add(comment);
            }
        }
        return visibleComments;
    }

    private MutableComment convertToComment(GenericValue gv, Issue issue) {
        Timestamp createdTS = gv.getTimestamp("created");
        Timestamp updatedTS = gv.getTimestamp("updated");
        CommentImpl comment = new CommentImpl(projectRoleManager,
                userManager.getUserByKeyEvenWhenUnknown(gv.getString("author")),
                userManager.getUserByKeyEvenWhenUnknown(gv.getString("updateauthor")),
                gv.getString("body"),
                gv.getString("level"),
                gv.getLong("rolelevel"),
                JiraDateUtils.copyDateNullsafe(createdTS),
                JiraDateUtils.copyDateNullsafe(updatedTS),
                issue);

        comment.setId(gv.getLong(COMMENT_ID));
        return comment;
    }

    public CommentSummary getCommentSummary(@Nullable final ApplicationUser user, @Nonnull final Issue issue, @Nonnull Optional<Long> focusedCommentId) {
        final List<Long> visibleCommentIds = getVisibleCommentIds(issue, user);

        List<Long> commentIdsToLoad = visibleCommentIds;
        if (shouldCommentsBeLimited(visibleCommentIds)) {
            commentIdsToLoad = visibleCommentIds.subList(0, Math.min(NUMBER_OF_NEW_COMMENTS_TO_SHOW, visibleCommentIds.size()));

            //if the focused comment is not part of the sublist, then we need to load all comments.
            if (focusedCommentId.isPresent() && !commentIdsToLoad.contains(focusedCommentId.get())) {
                commentIdsToLoad = visibleCommentIds;
            }
        }

        final List<Comment> finalComments = Lists.newArrayList();

        //this might happen if comment limiting is turned off - we simply fall back to the old way of retrieving all comments.
        if (commentIdsToLoad.size() > MAX_COMMENTS_TO_LOAD_BY_ID) {
            finalComments.addAll(getCommentsForUser(issue, user));
            return new CommentSummary(finalComments.size(), finalComments);
        } else {
            finalComments.addAll(getCommentsForIds(commentIdsToLoad, issue));
            return new CommentSummary(visibleCommentIds.size(), finalComments);
        }
    }

    private boolean shouldCommentsBeLimited(List<Long> visibleCommentIds) {
        final ApplicationProperty applicationProperty = applicationPropertiesService.getApplicationProperty(COMMENT_COLLAPSING_MINIMUM_HIDDEN);
        int minimumCommentsToHide = (applicationProperty != null) ? Integer.parseInt(applicationProperty.getCurrentValue()) : DEFAULT_MINIMUM_NUMBER_OF_HIDDEN_COMMENTS;
        boolean limitComments = true;
        if ((featureManager.isEnabled(CoreFeatures.PREVENT_COMMENTS_LIMITING)) || minimumCommentsToHide == 0 || (visibleCommentIds.size() - NUMBER_OF_NEW_COMMENTS_TO_SHOW < minimumCommentsToHide)) {
            limitComments = false;
        }
        return limitComments;
    }

    private List<Comment> getCommentsForIds(List<Long> commentIds, Issue issue) {
        if (commentIds.isEmpty()) {
            return Collections.emptyList();
        }
        final EntityCondition commentIdCondition = new EntityExpr("id", EntityOperator.IN, commentIds);
        final List<GenericValue> commentsToReturn = delegator.findByCondition(DefaultCommentManager.COMMENT_ENTITY,
                commentIdCondition, null, null);
        return commentsToReturn.stream().map(comment -> convertToComment(comment, issue)).collect(toList());
    }

    private List<Long> getVisibleCommentIds(@Nonnull Issue issue, @Nullable ApplicationUser user) {
        final EntityCondition issueCommentsCondition = new EntityFieldMap(FieldMap.build("issue", issue.getId(), "type", ActionConstants.TYPE_COMMENT), EntityOperator.AND);
        final List<GenericValue> allComments = delegator.findByCondition(DefaultCommentManager.COMMENT_ENTITY,
                issueCommentsCondition, ImmutableList.of("id", "level", "rolelevel"), ImmutableList.of("created DESC"));
        return allComments.stream()
                .map(commentGv -> CommentForPermissionCheck.toComment(issue, commentGv))
                .filter(comment -> commentPermissionManager.hasBrowsePermission(user, comment))
                .map(Comment::getId)
                .collect(toList());
    }

    /**
     * This classy solely exists so we have an object to pass to the CommentPermissionManager to run
     * permission checks with.  It will only ever contain the minimum amount of information necessary and all
     * other getters will immediately throw Exceptions when called.  This should never leak outside of this class.
     */
    private static final class CommentForPermissionCheck implements Comment {
        private Issue issue;
        private String level;
        private Long id;
        private Long rolelevel;

        private CommentForPermissionCheck(Issue issue, String level, Long id, Long rolelevel) {
            this.issue = issue;
            this.level = level;
            this.id = id;
            this.rolelevel = rolelevel;
        }

        public static Comment toComment(Issue issue, GenericValue commentGv) {
            return new CommentForPermissionCheck(issue, commentGv.getString("level"), commentGv.getLong("id"), commentGv.getLong("rolelevel"));
        }

        @Override
        public String getAuthor() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public String getAuthorKey() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public ApplicationUser getAuthorUser() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public ApplicationUser getAuthorApplicationUser() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public String getAuthorFullName() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public String getBody() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public Date getCreated() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public String getGroupLevel() {
            return this.level;
        }

        @Override
        public Long getId() {
            return this.id;
        }

        @Override
        public Long getRoleLevelId() {
            return this.rolelevel;
        }

        @Override
        public ProjectRole getRoleLevel() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public Issue getIssue() {
            return this.issue;
        }

        @Override
        public String getUpdateAuthor() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public ApplicationUser getUpdateAuthorUser() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public ApplicationUser getUpdateAuthorApplicationUser() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public String getUpdateAuthorFullName() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public Date getUpdated() {
            throw new UnsupportedOperationException("Not implemented");
        }
    }
}
