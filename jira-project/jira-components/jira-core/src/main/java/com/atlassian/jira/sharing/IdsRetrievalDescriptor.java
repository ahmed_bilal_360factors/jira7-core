package com.atlassian.jira.sharing;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * {@link SharedEntityAccessor.RetrievalDescriptor} backed up by a {@link Collection} of ids
 *
 * @since v7.1
 */
public class IdsRetrievalDescriptor implements SharedEntityAccessor.RetrievalDescriptor {
    final List<Long> ids;
    final boolean preserveOrder;

    public IdsRetrievalDescriptor(final Collection<Long> ids, final boolean preserveOrder) {
        this.ids = Collections.unmodifiableList(new ArrayList<>(ids));
        this.preserveOrder = preserveOrder;
    }

    @Nonnull
    @Override
    public List<Long> getIds() {
        return ids;
    }

    @Override
    public boolean preserveOrder() {
        return preserveOrder;
    }
}
