package com.atlassian.jira.web.component.subtask;

import com.atlassian.jira.bean.SubTaskBean;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * Simple layout item  which renders sequence number column in Issue Table
 *
 * @since v7.2
 */
public class SubTaskSequenceColumnLayoutItem extends AbstractSubTaskColumnLayoutItem {
    private final static String SEQUENCE_COLUMN_TEMPLATE_PATH = "templates/jira/issue/subtask/issue-subtask-sequence.vm";

    // this is a bit ugly in that the sequence isn't passed in to us.  We just have to assume the render method is only called once
    private int displaySequence = 0;

    public SubTaskSequenceColumnLayoutItem(VelocityTemplatingEngine velocity, SubTaskBean subTaskBean, String subTaskView) {
        super(velocity, subTaskBean, subTaskView);
    }

    @Override
    protected String getTemplate() {
        return SEQUENCE_COLUMN_TEMPLATE_PATH;
    }

    @Override
    protected Map<String, Object> getContext(Issue issue) {
        return (new ImmutableMap.Builder<String, Object>())
                .put("displaySequence", ++displaySequence)
                .put("actualSequence", getCurrentSubTaskSequence(issue))
                .put("isDone", issue.getResolution() != null)
                .build();
    }

    @Override
    protected String getColumnCssClass() {
        return "stsequence";
    }


}
