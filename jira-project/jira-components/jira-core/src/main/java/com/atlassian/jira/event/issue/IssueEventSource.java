package com.atlassian.jira.event.issue;

public class IssueEventSource {
    public static final String WORKFLOW = "workflow";
    public static final String ACTION = "action";
}
