package com.atlassian.jira.web.action.admin.workflow.tabs;

import com.opensymphony.workflow.loader.ActionDescriptor;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

public class WorkflowTransitionValidatorsContextProvider extends WorkflowTransitionContextProvider {

    @Override
    protected int getCount(final Map<String, Object> context) {
        return WorkflowTransitionContextUtils.getTransition(context)
                .map(ActionDescriptor::getValidators)
                .filter(Objects::nonNull)
                .map(Collection::size)
                .orElse(0);
    }
}
