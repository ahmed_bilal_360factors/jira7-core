package com.atlassian.jira.web.action.user;

import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.user.CreateUserApplicationHelper;
import com.atlassian.jira.bc.user.UserApplicationHelper.ValidationScope;
import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.bc.user.UserService.CreateUserRequest;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.plugin.user.WebErrorMessage;
import com.atlassian.jira.servlet.JiraCaptchaService;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.jira.web.action.ActionViewData;
import com.atlassian.jira.web.action.ActionViewDataMappings;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.web.util.ExternalLinkUtil;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.octo.captcha.service.CaptchaServiceException;
import org.apache.commons.lang.StringUtils;
import webwork.action.ActionContext;

import javax.servlet.http.HttpSession;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class Signup extends JiraWebActionSupport {
    private String fullname;
    private String username;
    private String email;
    private String password;
    private String captcha;
    private final ApplicationProperties applicationProperties;
    private final UserService userService;
    private final ExternalLinkUtil externalLinkUtil;
    private final JiraCaptchaService jiraCaptchaService;
    private final CreateUserApplicationHelper applicationHelper;
    private final ApplicationRoleManager roleManager;
    private final UserUtil userUtil;
    private final PageBuilderService pageBuilderService;

    private UserService.CreateUserValidationResult result;
    private List<WebErrorMessage> passwordErrors;


    public Signup(final ApplicationProperties applicationProperties, final UserService userService,
                  final JiraCaptchaService jiraCaptchaService, ExternalLinkUtil externalLinkUtil,
                  final CreateUserApplicationHelper applicationHelper,
                  final ApplicationRoleManager roleManager, final UserUtil userUtil, final PageBuilderService pageBuilderService) {
        this.applicationProperties = applicationProperties;
        this.userService = userService;
        this.jiraCaptchaService = jiraCaptchaService;
        this.externalLinkUtil = externalLinkUtil;
        this.applicationHelper = applicationHelper;
        this.roleManager = roleManager;
        this.userUtil = userUtil;
        this.pageBuilderService = pageBuilderService;
    }

    public String doDefault() throws Exception {
        requireResources();

        if (!JiraUtils.isPublicMode()) {
            return "modebreach";
        }

        if (getLoggedInUser() != null) {
            return "alreadyloggedin";
        }

        if (!validateSeats()) {
            return "limitexceeded";
        }

        if (!validateApplicationAccess()) {
            return "appaccesserror";
        }

        return super.doDefault();
    }

    protected void doValidation() {
        if (!JiraUtils.isPublicMode()) {
            return;
        }

        if (getLoggedInUser() != null) {
            return;
        }

        if (!validateSeats()) {
            return;
        }

        if (!validateApplicationAccess()) {
            return;
        }

        validateCaptcha();

        final CreateUserRequest createUserRequest = CreateUserRequest
                .withUserDetails(getLoggedInUser(), getUsername(), getPassword(), getEmail(), getFullname())
                .passwordRequired()
                .performPermissionCheck(false)
                .sendUserSignupEvent();
        result = userService.validateCreateUser(createUserRequest);

        if (!result.isValid()) {
            addErrorCollection(result.getErrorCollection());
        }
        passwordErrors = result.getPasswordErrors();
    }

    protected String doExecute() throws Exception {
        requireResources();

        if (!JiraUtils.isPublicMode()) {
            return "modebreach";
        }

        if (getLoggedInUser() != null) {
            return "alreadyloggedin";
        }
        if (!validateSeats()) {
            return "limitexceeded";
        }

        if (!validateApplicationAccess()) {
            return "appaccesserror";
        }

        try {
            ApplicationUser user = userService.createUser(result);
            if (user == null) {
                addErrorMessage(getText("signup.error.duplicateuser"));
            }
        } catch (final CreateException e) {
            log.error("Error creating user from public sign up", e);
            return "systemerror";
        }

        return getResult();
    }

    public ExternalLinkUtil getExternalLinkUtils() {
        return externalLinkUtil;
    }

    private void validateCaptcha() {
        if (!applicationProperties.getOption(APKeys.JIRA_OPTION_CAPTCHA_ON_SIGNUP)) {
            return;
        }
        //remember that we need an id to validate!
        HttpSession session = ActionContext.getRequest().getSession(false);
        if (session == null) {
            addErrorMessage(getText("session.timeout.message.title"));
            return;
        }
        String captchaId = session.getId();

        Boolean isResponseCorrect = null;
        try {
            isResponseCorrect = jiraCaptchaService.getImageCaptchaService().validateResponseForID(captchaId, captcha);
        } catch (CaptchaServiceException e) {
            addErrorMessage(getText("session.timeout.message.title"));
        }
        if (isResponseCorrect != null && !isResponseCorrect) {
            addError("captcha", getText("signup.error.captcha.incorrect"));
        }
    }

    private boolean validateApplicationAccess() {
        return applicationHelper.validateDefaultApplications(EnumSet.allOf(ValidationScope.class), Optional.empty()).isEmpty();
    }

    private boolean validateSeats() {
        //it's not possible to not have writable directory at this point - JiraUtils.isPublicMode() guards this
        return applicationHelper.validateDefaultApplications(EnumSet.of(ValidationScope.SEATS), Optional.empty()).isEmpty();
    }

    private void requireResources() {
        pageBuilderService.assembler().resources().requireWebResource("jira.webresources:signup");
    }

    @ActionViewData("alreadyloggedin")
    public Map<String, Object> getAlreadyLoggedInData() {
        return ImmutableMap.of("xsrfToken", StringUtils.defaultIfBlank(getXsrfToken(), ""));
    }

    @ActionViewData("success")
    public Map<String, Object> getSuccessSoyData() {
        return ImmutableMap.of("newsHref", StringUtils.defaultIfBlank(getExternalLinkUtils().getProperty("external.link.atlassian.news"), ""));
    }

    @ActionViewDataMappings(value = {"appaccesserror", "limitexceeded", "systemerror", "modebreach"})
    public Map<String, Object> getSoyData() {
        return ImmutableMap.of("contactLink", getJiraContactHelper().getAdministratorContactLink(getHttpRequest().getContextPath()),
                "isContactFormEnabled", getJiraContactHelper().isAdministratorContactFormEnabled(),
                "showUpfront", isShowUpfront());
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getUsername() {
        if (username != null) {
            return username.trim();
        }
        return null;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getPasswordError() {
        if (getErrors().containsKey("password")) {
            return ImmutableList.of(getErrors().get("password"));
        }
        return ImmutableList.of();
    }

    public List<WebErrorMessage> getPasswordErrors() {
        return passwordErrors;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public boolean isShowUpfront() {
        return "GET".equalsIgnoreCase(getHttpRequest().getMethod());
    }
}
