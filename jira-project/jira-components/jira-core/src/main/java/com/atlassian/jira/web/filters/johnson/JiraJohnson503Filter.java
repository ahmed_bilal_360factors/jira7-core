package com.atlassian.jira.web.filters.johnson;

import com.atlassian.jira.web.startup.StartupPageSupport;
import com.atlassian.johnson.filters.Johnson503Filter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

import static com.atlassian.jira.web.filters.johnson.ServiceUnavailableResponder.respondWithEmpty503;

/**
 * Enhances the basic {@code Johnson503Filter} with awareness of the startup page.
 *
 * @since v7.1.0
 */
public class JiraJohnson503Filter extends Johnson503Filter {
    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException {
        if (StartupPageSupport.isLaunched() || StartupPageSupport.isStartupPage(request)) {
            super.doFilter(request, response, filterChain);
        } else {
            respondWithEmpty503(response);
        }
    }
}
