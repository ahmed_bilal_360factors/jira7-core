package com.atlassian.jira.model.querydsl;

import com.querydsl.core.types.dsl.*;
import com.querydsl.sql.ColumnMetadata;

import javax.annotation.Generated;
import java.sql.Types;

/**
 * QProjectCategory is a Querydsl query object.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QProjectCategory extends JiraRelationalPathBase<ProjectCategoryDTO> {
    public static final QProjectCategory PROJECT_CATEGORY = new QProjectCategory("PROJECT_CATEGORY");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final StringPath name = createString("name");
    public final StringPath description = createString("description");

    public QProjectCategory(String alias) {
        super(ProjectCategoryDTO.class, alias, "projectcategory");
        addMetadata();
    }

    private void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(name, ColumnMetadata.named("cname").withIndex(2).ofType(Types.VARCHAR).withSize(255));
        addMetadata(description, ColumnMetadata.named("description").withIndex(3).ofType(Types.VARCHAR).withSize(4000));
    }

    /**
     * Returns the ofbiz model's entity name for this query object.
     *
     * @return the ofbiz model's entity name for this query object.
     */
    @Override
    public String getEntityName() {
        return "ProjectCategory";
    }

    @Override
    public NumberPath<Long> getNumericIdPath() {
        return id;
    }
}

