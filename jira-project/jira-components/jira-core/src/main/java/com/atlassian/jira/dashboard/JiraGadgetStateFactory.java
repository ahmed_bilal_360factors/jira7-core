package com.atlassian.jira.dashboard;

import com.atlassian.gadgets.GadgetState;
import com.atlassian.gadgets.OpenSocialDashboardItemModuleId;
import com.atlassian.gadgets.dashboard.spi.DashboardItemStateFactory;
import com.atlassian.gadgets.dashboard.spi.GadgetStateFactory;

import java.net.URI;

/**
 * Generates a gadgetState for gadgets to be added to the dashboard.
 *
 * @since v4.0
 */
@Deprecated
public class JiraGadgetStateFactory implements GadgetStateFactory {
    private final DashboardItemStateFactory dashboardItemStateFactory;

    public JiraGadgetStateFactory(final DashboardItemStateFactory dashboardItemStateFactory) {
        this.dashboardItemStateFactory = dashboardItemStateFactory;
    }

    public GadgetState createGadgetState(final URI uri) {
        return dashboardItemStateFactory.createGadgetState(new OpenSocialDashboardItemModuleId(uri));
    }
}
