package com.atlassian.jira.project.template;

import com.atlassian.jira.project.template.descriptor.DemoProjectModuleDescriptor;
import com.atlassian.jira.project.template.module.DemoProjectModule;
import com.atlassian.jira.web.action.issue.util.ConditionalDescriptorPredicate;
import com.atlassian.ozymandias.SafePluginPointAccess;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.descriptors.ConditionalDescriptor;
import com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @since v7.1
 */
public class DefaultDemoProjectManager implements DemoProjectManager {
    private final PluginAccessor pluginAccessor;

    public DefaultDemoProjectManager(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    public List<DemoProjectModule> getDemoProjects() {
        Predicate<ConditionalDescriptor> conditionalDescriptorPredicate = new ConditionalDescriptorPredicate(ImmutableMap.of());

        List<Optional<DemoProjectModule>> modules = SafePluginPointAccess.to()
                .descriptors(pluginAccessor.getEnabledModuleDescriptorsByClass(DemoProjectModuleDescriptor.class),
                        (moduleDescriptor, module) -> conditionalDescriptorPredicate.test(moduleDescriptor) ? Optional.of(module) : Optional.empty());

        return modules.stream()
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }
}
