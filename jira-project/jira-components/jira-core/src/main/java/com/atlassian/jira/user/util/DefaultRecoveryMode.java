package com.atlassian.jira.user.util;

import com.atlassian.crowd.manager.recovery.RecoveryModeService;
import com.atlassian.fugue.Option;
import com.atlassian.jira.user.ApplicationUser;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Implementation of {@link com.atlassian.jira.user.util.RecoveryMode} that delegates to
 * {@link com.atlassian.crowd.manager.recovery.RecoveryModeService}.
 */
public final class DefaultRecoveryMode implements RecoveryMode {
    private final RecoveryModeService recoveryModeService;

    public DefaultRecoveryMode(final RecoveryModeService recoveryModeService) {
        this.recoveryModeService = notNull("recoveryModeService", recoveryModeService);
    }

    @Override
    public boolean isRecoveryModeOn() {
        return recoveryModeService.isRecoveryModeOn();
    }

    @Override
    public boolean isRecoveryUser(final ApplicationUser user) {
        return user != null && isRecoveryUsername(user.getUsername());
    }

    @Override
    public boolean isRecoveryUsername(final String username) {
        return recoveryModeService.isRecoveryModeOn()
                && recoveryModeService.getRecoveryUsername().equals(username);
    }

    @Override
    public Option<String> getRecoveryUsername() {
        if (isRecoveryModeOn()) {
            return Option.some(recoveryModeService.getRecoveryUsername());
        } else {
            return Option.none();
        }
    }
}
