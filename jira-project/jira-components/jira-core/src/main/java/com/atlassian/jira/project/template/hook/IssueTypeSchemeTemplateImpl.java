package com.atlassian.jira.project.template.hook;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.nullToEmpty;
import static java.util.Collections.unmodifiableList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IssueTypeSchemeTemplateImpl implements IssueTypeSchemeTemplate {
    private final String name;
    private final Optional<String> defaultIssueType;
    private final String description;
    private final List<? extends IssueTypeTemplate> issueTypeTemplateList;

    public IssueTypeSchemeTemplateImpl(
            @JsonProperty("name") String name,
            @JsonProperty("default-issue-type") String defaultIssueType,
            @JsonProperty("description") String description,
            @JsonProperty("issue-types") List<IssueTypeTemplateImpl> issueTypeTemplateList) {
        this.name = checkNotNull(name);
        this.defaultIssueType = (defaultIssueType == null) ? Optional.empty() : Optional.of(defaultIssueType.toUpperCase());
        this.description = nullToEmpty(description);
        this.issueTypeTemplateList = checkNotNull(issueTypeTemplateList);

        validate();
    }

    @Override
    public String name() {
        return this.name;
    }

    @Override
    public Optional<String> defaultIssueType() {
        return defaultIssueType;
    }

    @Override
    public String description() {
        return this.description;
    }

    @Override
    public List<IssueTypeTemplate> issueTypeTemplates() {
        return unmodifiableList(issueTypeTemplateList);
    }

    private void validate() {
        validateDefaultIssueType();
    }

    private void validateDefaultIssueType() {
        if (defaultIssueType.isPresent() && !hasIssueType(defaultIssueType.get())) {
            throw new IllegalArgumentException("Default issue type '" + defaultIssueType.get() +
                    "' of issue type scheme '" + name + "' does not exist.");
        }
    }

    private boolean hasIssueType(String issueTypeKey) {
        for (IssueTypeTemplate issueTypeTemplate : issueTypeTemplateList) {
            if (issueTypeTemplate.key().equalsIgnoreCase(issueTypeKey)) {
                return true;
            }
        }
        return false;
    }
}
