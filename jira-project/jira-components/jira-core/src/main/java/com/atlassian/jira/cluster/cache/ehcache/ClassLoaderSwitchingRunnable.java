package com.atlassian.jira.cluster.cache.ehcache;

class ClassLoaderSwitchingRunnable implements Runnable {
    private final Runnable runnable;
    private final ClassLoader taskClassLoader;

    ClassLoaderSwitchingRunnable(final Runnable runnable) {
        this.runnable = runnable;

        taskClassLoader = Thread.currentThread().getContextClassLoader();
    }

    @Override
    public void run() {
        final ClassLoader threadClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(taskClassLoader);
            runnable.run();
        } finally {
            Thread.currentThread().setContextClassLoader(threadClassLoader);
        }
    }
}
