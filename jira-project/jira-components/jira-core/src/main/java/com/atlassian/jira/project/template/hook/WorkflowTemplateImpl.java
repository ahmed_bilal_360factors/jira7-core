package com.atlassian.jira.project.template.hook;

import com.atlassian.plugin.Plugin;
import com.google.common.annotations.VisibleForTesting;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JacksonInject;

import java.net.URL;

import static com.google.common.base.Preconditions.checkNotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkflowTemplateImpl implements WorkflowTemplate {
    private final String key;
    private final String name;
    private final String bundlePath;
    private final URL bundleUrl;

    public WorkflowTemplateImpl(
            @JsonProperty("key") String key,
            @JsonProperty("name") String name,
            @JsonProperty("workflow-bundle") String bundlePath,
            @JacksonInject("plugin") Plugin plugin) {
        this.key = checkNotNull(key).toUpperCase();
        this.name = checkNotNull(name);
        this.bundlePath = checkNotNull(bundlePath);
        this.bundleUrl = getBundleURL(plugin);
    }

    @VisibleForTesting
    public WorkflowTemplateImpl(String key, String name, String bundlePath, URL bundleUrl) {
        this.key = key;
        this.name = name;
        this.bundlePath = bundlePath;
        this.bundleUrl = bundleUrl;
    }

    @Override
    public String key() {
        return key;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String bundlePath() {
        return bundlePath;
    }

    @Override
    public URL bundleUrl() {
        return bundleUrl;
    }

    private URL getBundleURL(Plugin plugin) {
        URL url = plugin.getResource(bundlePath());
        if (url == null) {
            throw new IllegalArgumentException("Workflow bundle for workflow '" + name + "'not found on path specified: " + bundlePath);
        }

        return url;
    }
}
