package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.LookAndFeelBean;
import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;

/**
 * Reset look and feel counter.
 */
public class UpgradeTask_Build70026 extends AbstractImmediateUpgradeTask {
    private final ApplicationProperties applicationProperties;

    public UpgradeTask_Build70026(final ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception {
        LookAndFeelBean.getInstance(applicationProperties).updateSettingsHash();
    }

    @Override
    public int getBuildNumber() {
        return 70026;
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        return false;
    }

    @Override
    public String getShortDescription() {
        return "Reset look and feel resources counter.";
    }
}
