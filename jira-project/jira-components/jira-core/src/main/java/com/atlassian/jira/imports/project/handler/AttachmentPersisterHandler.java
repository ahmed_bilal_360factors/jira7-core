package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.external.beans.ExternalAttachment;
import com.atlassian.jira.imports.project.ProjectImportPersister;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.ProjectImportOptions;
import com.atlassian.jira.imports.project.core.ProjectImportResults;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.parser.AttachmentParser;
import com.atlassian.jira.imports.project.parser.AttachmentParserImpl;
import com.atlassian.jira.imports.project.transformer.AttachmentTransformer;
import com.atlassian.jira.imports.project.transformer.AttachmentTransformerImpl;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.AttachmentStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Map;
import java.util.concurrent.Executor;

import static com.atlassian.jira.imports.project.handler.AttachmentFileValidatorHandler.canProcessEntity;

/**
 * Reads, transforms, and stores all attachment entities from a backup file and copies the actual attachment,
 * as specified in the attachment directory into the JIRA attachment format.
 * <p>
 * It is assumed that all attachment data that is processed by this handler is relevant and should be saved.
 * <p>
 * NOTE: This handler will not process attachment data if {@link com.atlassian.jira.imports.project.core.ProjectImportOptions#getAttachmentPath()}
 * is null.
 *
 * @since v3.13
 */
public class AttachmentPersisterHandler extends AbstractPersisterHandler implements ImportOfBizEntityHandler {
    private static final Logger log = LoggerFactory.getLogger(AttachmentPersisterHandler.class);

    private final ProjectImportPersister projectImportPersister;
    private final ProjectImportOptions projectImportOptions;
    private final ProjectImportMapper projectImportMapper;
    private final BackupProject backupProject;
    private final BackupSystemInformation backupSystemInformation;
    private final ProjectImportResults projectImportResults;
    private final AttachmentParser attachmentParser;
    private final AttachmentTransformer attachmentTransformer;

    public AttachmentPersisterHandler(final ProjectImportPersister projectImportPersister,
                                      final ProjectImportOptions projectImportOptions, final ProjectImportMapper projectImportMapper,
                                      final BackupProject backupProject, final BackupSystemInformation backupSystemInformation,
                                      final ProjectImportResults projectImportResults, final Executor executor,
                                      final AttachmentStore attachmentStore) {
        this(projectImportPersister, projectImportOptions, projectImportMapper, backupProject, backupSystemInformation,
                projectImportResults, executor, new AttachmentParserImpl(attachmentStore,
                        projectImportOptions.getAttachmentPath()), new AttachmentTransformerImpl());
    }

    protected AttachmentPersisterHandler(final ProjectImportPersister projectImportPersister,
                                         final ProjectImportOptions projectImportOptions, final ProjectImportMapper projectImportMapper,
                                         final BackupProject backupProject, final BackupSystemInformation backupSystemInformation,
                                         final ProjectImportResults projectImportResults, final Executor executor,
                                         final AttachmentParser attachmentParser, final AttachmentTransformer attachmentTransformer) {
        super(executor, projectImportResults);

        this.projectImportPersister = projectImportPersister;
        this.projectImportOptions = projectImportOptions;
        this.projectImportMapper = projectImportMapper;
        this.backupProject = backupProject;
        this.backupSystemInformation = backupSystemInformation;
        this.projectImportResults = projectImportResults;
        this.attachmentParser = attachmentParser;
        this.attachmentTransformer = attachmentTransformer;
    }

    public void handleEntity(final String entityName, final Map<String, String> attributes) throws ParseException, AbortImportException {
        if (canProcessEntity(entityName, projectImportOptions)) {
            final ExternalAttachment externalAttachment = attachmentParser.parse(attributes);
            if (externalAttachment.getIssueId() != null) {
                // Now find out where the actual attachment file is on disk and set this on the externalAttachment
                final String issueKey = backupSystemInformation.getIssueKeyForId(externalAttachment.getIssueId());
                final File attachedFile = attachmentParser.getAttachmentFile(externalAttachment, backupProject.getProject(),
                        issueKey);
                externalAttachment.setAttachedFile(attachedFile);

                if (attachedFile.exists()) {
                    // Transform the attachment (i.e. set the mapped issue id)
                    final ExternalAttachment transformedAttachment = attachmentTransformer.transform(projectImportMapper, externalAttachment);

                    execute(new Runnable() {
                        public void run() {
                            final Attachment createdAttachment = projectImportPersister.createAttachment(transformedAttachment);
                            // if this doesn't create the attachment for some reason, then this is already logged in DefaultAttachmentManager.
                            if (createdAttachment == null) {
                                projectImportResults.addError(projectImportResults.getI18n().getText("admin.errors.project.import.attachment.error",
                                        externalAttachment.getFileName(), issueKey));
                            } else {
                                projectImportResults.incrementAttachmentsCreatedCount();
                            }
                        }
                    });
                } else {
                    // The user already knows about this we warned them on the pre-import summary page, it is therefore
                    // not an error.
                    log.warn("Not saving attachment '" + externalAttachment.getFileName() + "' for issue '" + issueKey + "', the file does not exist in the provided attachment directory.");
                }
            } else {
                log.warn("Not saving attachment '" + externalAttachment.getFileName() + "' it appears that the issue was not created as part of the import.");
            }
        }
    }

    ///CLOVER:OFF
    public void startDocument() {
        // No-op
    }

    ///CLOVER:ON

    ///CLOVER:OFF
    public void endDocument() {
        // No-op
    }
    ///CLOVER:ON
}
