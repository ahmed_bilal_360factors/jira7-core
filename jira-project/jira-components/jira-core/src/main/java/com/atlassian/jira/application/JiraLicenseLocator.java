package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.host.license.LicenseLocator;
import com.atlassian.fugue.Option;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.sal.api.license.LicenseHandler;
import com.atlassian.sal.api.license.SingleProductLicenseDetailsView;

import javax.annotation.Nullable;

/**
 * Implementation of {@link com.atlassian.application.host.license.LicenseLocator} for JIRA.
 * <p>
 * This implementation makes use of the {@link com.atlassian.sal.api.license.LicenseHandler} exposed by the
 * OSGi {@code JIRA - SAL} plugin.
 * </p>
 *
 * @since v7.0
 */
public class JiraLicenseLocator implements LicenseLocator {
    @Override
    public Option<SingleProductLicenseDetailsView> apply(@Nullable final ApplicationKey input) {
        //This is an OSGi component provided by the {@code JIRA - SAL} plugin. It might not be there so I am being
        //extra careful. However, the SAL plugin is basically a core component of JIRA so its not a real issue.
        //
        //I considered moving LicenseHandler into JIRA core but decided against it because it was not going to deliver
        //any major advantages for the amount of work required.
        final LicenseHandler handler = ComponentAccessor.getOSGiComponentInstanceOfType(LicenseHandler.class);
        if (handler == null || input == null) {
            return Option.none(SingleProductLicenseDetailsView.class);
        } else {
            return Option.option(handler.getProductLicenseDetails(input.value()));
        }
    }
}
