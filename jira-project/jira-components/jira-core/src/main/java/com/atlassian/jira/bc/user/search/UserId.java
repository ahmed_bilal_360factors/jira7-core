package com.atlassian.jira.bc.user.search;

/**
 * Identifies a directory user.
 */
public class UserId {

    private final String name;
    private final long directoryId;

    public UserId(String name, long directoryId) {
        this.name = name;
        this.directoryId = directoryId;
    }

    public String getName() {
        return name;
    }

    public long getDirectoryId() {
        return directoryId;
    }
}
