package com.atlassian.jira.application.install;

import com.google.common.collect.ImmutableSet;

import java.util.Collection;
import java.util.Set;

/**
 * Holds information about single application installation status.
 *
 * @since v6.5
 */
class WhatWasInstalledInApplication {
    private final Set<BundlesVersionDiscovery.PluginIdentification> pluginIdentifications;

    public WhatWasInstalledInApplication(final Iterable<BundlesVersionDiscovery.PluginIdentification> pluginIdentifications) {
        this.pluginIdentifications = ImmutableSet.copyOf(pluginIdentifications);
    }

    public boolean wereBundlesInstalled(final Collection<BundlesVersionDiscovery.PluginIdentification> pluginIdentifications) {
        return this.pluginIdentifications.containsAll(pluginIdentifications);
    }

    Set<BundlesVersionDiscovery.PluginIdentification> getPluginIdentifications() {
        return pluginIdentifications;
    }
}
