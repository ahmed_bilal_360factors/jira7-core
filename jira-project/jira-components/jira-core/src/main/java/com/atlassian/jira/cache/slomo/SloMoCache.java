package com.atlassian.jira.cache.slomo;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheEntryListener;
import com.atlassian.cache.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;

/**
 * Decorator that makes a slow version of a {@code CachedReference}.
 *
 * @since v7.1.0
 */
@ParametersAreNonnullByDefault
public class SloMoCache<K, V> implements Cache<K, V> {
    private static final Logger LOG = LoggerFactory.getLogger(SloMoCache.class);

    private final Cache<K, V> delegate;
    private final SloMoCacheManager manager;

    public SloMoCache(final SloMoCacheManager manager, final Cache<K, V> delegate) {
        this.delegate = delegate;
        this.manager = manager;
    }

    @Override
    @Nonnull
    public String getName() {
        return delegate.getName();
    }

    @Override
    public boolean containsKey(final K key) {
        manager.sleep();
        return delegate.containsKey(key);
    }

    @Override
    @Nonnull
    public Collection<K> getKeys() {
        LOG.warn("Enumerating the keys for '{}' will not be supported by VCache",
                delegate.getName(),
                new IllegalArgumentException("Called getKeys() here"));
        return delegate.getKeys();
    }

    @Override
    @Nullable
    public V get(final K key) {
        manager.sleep();
        return delegate.get(key);
    }

    @Override
    @Nonnull
    public V get(final K key, final Supplier<? extends V> supplier) {
        manager.sleep();
        return delegate.get(key, supplier);
    }

    @Override
    public void put(final K key, final V value) {
        manager.sleep();
        delegate.put(key, value);
    }

    @Override
    @Nullable
    public V putIfAbsent(final K key, final V value) {
        manager.sleep();
        return delegate.putIfAbsent(key, value);
    }

    @Override
    public void remove(final K key) {
        manager.sleep();
        delegate.remove(key);
    }

    @Override
    public boolean remove(final K key, final V value) {
        manager.sleep();
        return delegate.remove(key, value);
    }

    @Override
    public void removeAll() {
        manager.sleep();
        delegate.removeAll();
    }

    @Override
    public boolean replace(final K key, final V oldValue, final V newValue) {
        manager.sleep();
        return delegate.replace(key, oldValue, newValue);
    }

    @Override
    public void addListener(final CacheEntryListener<K, V> listener, final boolean includeValues) {
        LOG.warn("CacheEntryListener for '{}' will not be supported by VCache",
                delegate.getName(),
                new IllegalArgumentException("Added here"));
        delegate.addListener(listener, includeValues);
    }

    @Override
    public void removeListener(final CacheEntryListener<K, V> cacheEntryListener) {
        delegate.removeListener(cacheEntryListener);
    }
}
