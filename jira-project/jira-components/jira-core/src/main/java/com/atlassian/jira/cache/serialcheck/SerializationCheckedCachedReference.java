package com.atlassian.jira.cache.serialcheck;

import com.atlassian.cache.CachedReference;
import com.atlassian.cache.CachedReferenceListener;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Decorator that checks {@code CachedReference} values for serializability.
 *
 * @since v7.2.0
 */
@ParametersAreNonnullByDefault
public class SerializationCheckedCachedReference<V> implements CachedReference<V> {

    private final SerializationChecker checker;
    private final String name;
    private final CachedReference<V> delegate;
    private volatile boolean valueChecked;

    public SerializationCheckedCachedReference(final SerializationChecker checker, final String name, final CachedReference<V> delegate) {
        this.checker = checker;
        this.name = name;
        this.delegate = delegate;
    }

    @Override
    @Nonnull
    public V get() {
        V result = delegate.get();

        //It's OK if we check more than once, just a bit wasteful
        //Rather do that than use a sync lock though
        if (!valueChecked) {
            valueChecked = true;
            checker.checkValue(this, name, result);
        }

        return result;
    }

    @Override
    public void reset() {
        valueChecked = false;
        delegate.reset();
    }

    @Override
    public void addListener(final CachedReferenceListener<V> listener, final boolean includeValues) {
        delegate.addListener(listener, includeValues);
    }

    @Override
    public void removeListener(final CachedReferenceListener<V> listener) {
        delegate.removeListener(listener);
    }
}
