package com.atlassian.jira.sharing.type;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.sharing.SharePermission;
import com.atlassian.jira.sharing.search.GroupShareTypeSearchParameter;
import com.atlassian.jira.sharing.search.ShareTypeSearchParameter;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.dbc.Assertions;
import org.apache.commons.lang.StringUtils;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * A validator for the {@link com.atlassian.jira.sharing.type.GroupShareType}.
 *
 * @since v3.13
 */
public class GroupShareTypeValidator implements ShareTypeValidator {
    private final PermissionManager permissionManager;
    private final GroupManager groupManager;

    public GroupShareTypeValidator(final PermissionManager permissionManager, final GroupManager groupManager) {
        this.permissionManager = permissionManager;
        this.groupManager = groupManager;
    }

    public boolean checkSharePermission(final JiraServiceContext ctx, final SharePermission permission) {
        notNull("ctx", ctx);
        notNull("permission", permission);
        final ApplicationUser user = notNull("ctx.user", ctx.getLoggedInApplicationUser());
        Assertions.equals(GroupShareType.TYPE.toString(), GroupShareType.TYPE, permission.getType());

        final boolean hasPermission = permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, user);
        if (!hasPermission) {
            ctx.getErrorCollection().addError(ShareTypeValidator.ERROR_KEY, ctx.getI18nBean().getText("common.sharing.exception.no.share.permission"));
        } else {
            if (StringUtils.isBlank(permission.getParam1())) {
                final String groupName = permission.getParam1() == null ? "" : permission.getParam1();
                ctx.getErrorCollection().addError(ShareTypeValidator.ERROR_KEY,
                        ctx.getI18nBean().getText("common.sharing.exception.group.not.valid", groupName));
            } else {
                final Group group = getGroup(permission.getParam1());
                if (group == null) {
                    ctx.getErrorCollection().addError(ShareTypeValidator.ERROR_KEY,
                            ctx.getI18nBean().getText("common.sharing.exception.group.does.not.exist", permission.getParam1()));
                } else if (!groupManager.isUserInGroup(user, group)) {
                    final String userName = user.getDisplayName();
                    ctx.getErrorCollection().addError(ShareTypeValidator.ERROR_KEY,
                            ctx.getI18nBean().getText("common.sharing.exception.not.in.group", permission.getParam1()));
                    ctx.getErrorCollection().addError(ShareTypeValidator.DELEGATED_ERROR_KEY,
                            ctx.getI18nBean().getText("common.sharing.exception.delegated.user.not.in.group", userName, permission.getParam1()));
                }
            }
        }

        return !ctx.getErrorCollection().hasAnyErrors();
    }

    public boolean checkSearchParameter(final JiraServiceContext ctx, final ShareTypeSearchParameter searchParameter) {
        notNull("ctx", ctx);
        notNull("searchParameter", searchParameter);
        Assertions.equals(GroupShareType.TYPE.toString(), GroupShareType.TYPE, searchParameter.getType());

        // the anonymous user never has permission to search by groups.
        final ApplicationUser user = ctx.getLoggedInApplicationUser();
        if (user == null) {
            ctx.getErrorCollection().addError(ShareTypeValidator.ERROR_KEY,
                    ctx.getI18nBean().getText("common.sharing.searching.exception.anonymous.group.search"));
        } else {
            final GroupShareTypeSearchParameter groupShareTypeSearchParameter = (GroupShareTypeSearchParameter) searchParameter;
            final String groupName = groupShareTypeSearchParameter.getGroupName();
            if (StringUtils.isNotBlank(groupName)) {
                final Group group = getGroup(groupName);
                if (group == null) {
                    ctx.getErrorCollection().addError(ShareTypeValidator.ERROR_KEY,
                            ctx.getI18nBean().getText("common.sharing.exception.group.does.not.exist", groupName));
                } else if (!groupManager.isUserInGroup(user, group)) {
                    ctx.getErrorCollection().addError(ShareTypeValidator.ERROR_KEY,
                            ctx.getI18nBean().getText("common.sharing.searching.exception.not.in.group", groupName));
                    ctx.getErrorCollection().addError(ShareTypeValidator.DELEGATED_ERROR_KEY,
                            ctx.getI18nBean().getText("common.sharing.exception.delegated.user.not.in.group", user.getDisplayName(), groupName));
                }
            }
        }
        return !ctx.getErrorCollection().hasAnyErrors();
    }

    ///CLOVER:OFF
    Group getGroup(final String groupName) {
        return groupManager.getGroup(groupName);
    }
    ///CLOVER:ON
}
