package com.atlassian.jira.application;

import com.atlassian.crowd.embedded.api.Group;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.annotation.Nonnull;
import java.util.Set;

import static com.atlassian.extras.common.LicensePropertiesConstants.UNLIMITED_USERS;
import static com.atlassian.jira.util.dbc.Assertions.containsNoNulls;
import static com.google.common.collect.ImmutableSet.copyOf;

/**
 * Common ancestor for {@link com.atlassian.jira.application.ApplicationRole} implementations.
 *
 * @since v7.0
 */
abstract class AbstractApplicationRole implements ApplicationRole {
    private final ImmutableSet<Group> groups;
    private final ImmutableSet<Group> defaultGroups;
    private final int numberOfSeats;
    private final boolean selectedByDefault;

    AbstractApplicationRole(final Iterable<Group> groups, final Iterable<Group> defaultGroups, final int numberOfSeats,
                            final boolean selectedByDefault) {
        containsNoNulls("groups", groups);
        containsNoNulls("defaultGroups", defaultGroups);

        if (numberOfSeats < 0 && numberOfSeats != UNLIMITED_USERS) {
            throw new IllegalArgumentException("Illegal numberOfSeats: " + numberOfSeats);
        }
        this.defaultGroups = copyOf(defaultGroups);
        this.groups = copyOf(groups);
        this.numberOfSeats = numberOfSeats;
        this.selectedByDefault = selectedByDefault;

        if (!this.groups.containsAll(this.defaultGroups)) {
            throw new IllegalArgumentException(String.format("defaultGroups '%s' not a subset of '%s'.",
                    this.defaultGroups, this.groups));
        }
    }

    @Nonnull
    @Override
    public final Set<Group> getGroups() {
        return groups;
    }

    @Nonnull
    @Override
    public final Set<Group> getDefaultGroups() {
        return defaultGroups;
    }

    @Override
    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    @Override
    public boolean isSelectedByDefault() {
        return selectedByDefault;
    }

    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof ApplicationRole) {
            final ApplicationRole that = (ApplicationRole) o;
            //It is intentional that we only look at the id.
            return getKey().equals(that.getKey());
        } else {
            return false;
        }
    }

    @Override
    public final int hashCode() {
        //It is intentional that we only look at the id.
        return getKey().hashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("key", getKey())
                .append("groups", groups)
                .append("defaultGroups", defaultGroups)
                .append("numberOfSeats", numberOfSeats)
                .append("selectedByDefault", selectedByDefault)
                .append("defined", isDefined())
                .toString();
    }
}