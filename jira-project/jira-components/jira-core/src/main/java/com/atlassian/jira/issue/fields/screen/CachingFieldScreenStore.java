package com.atlassian.jira.issue.fields.screen;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheFactory;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.cache.CachedReference;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.event.issue.field.screen.AbstractFieldScreenLayoutItemEvent;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Copyright (c) 2002-2004 All rights reserved.
 */
@EventComponent
public class CachingFieldScreenStore implements FieldScreenStore {
    private static final Logger log = LoggerFactory.getLogger(CachingFieldScreenStore.class);
    private final FieldScreenStore decoratedStore;

    private final Cache<Long, Optional<FieldScreen>> fieldScreenCache;
    private final CachedReference<List<Long>> allScreensCache;

    public CachingFieldScreenStore(FieldScreenStore decoratedStore, CacheManager cacheManager) {
        this.decoratedStore = decoratedStore;
        allScreensCache = cacheManager.getCachedReference(getClass().getName() + ".allScreensCache",
                decoratedStore::getFieldScreenIds);

        fieldScreenCache = cacheManager.getCache(getClass().getName() + ".fieldScreenCache",
                id -> Optional.ofNullable(decoratedStore.getFieldScreen(id)),
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).build());
    }

    @EventListener
    public void onClearCache(final ClearCacheEvent event) {
        refresh();
    }

    public void setFieldScreenManager(FieldScreenManager fieldScreenManager) {
        decoratedStore.setFieldScreenManager(fieldScreenManager);
    }

    @Nullable
    public FieldScreen getFieldScreen(Long id) {
        // NOTE: we are returning a mutable object from a cache here, which is rubbish. however, this cache has
        // worked like this since JIRA 3.10 so it is not causing any immediate problems. the last time I tried to
        // improve on this I ended up returning an incomplete-copied FieldScreen, which then caused performance
        // problems as everyone who got a FieldScreen from the cache ended up having to read the FieldScreenTabs
        // from the database anyway (JRA-28906).
        // We find out about changes via the AbstractFieldScreenLayoutItemEvent.
        return fieldScreenCache.get(id).orElse(null);
    }

    public List<Long> getFieldScreenIds() {
        return allScreensCache.get();
    }

    public List<FieldScreen> getFieldScreens() {
        return getFieldScreenIds().stream()
                .map(this::getFieldScreen)
                .filter(Objects::nonNull)
                .sorted(Comparator.comparing(FieldScreen::getName))
                .collect(CollectorsUtil.toImmutableList());
    }

    public void createFieldScreen(FieldScreen fieldScreen) {
        try {
            decoratedStore.createFieldScreen(fieldScreen);
        } finally {
            refresh();
        }
    }

    public void removeFieldScreen(Long id) {
        try {
            decoratedStore.removeFieldScreen(id);
        } finally {
            refresh();
        }
    }

    public void updateFieldScreen(FieldScreen fieldScreen) {
        try {
            decoratedStore.updateFieldScreen(fieldScreen);
        } finally {
            fieldScreenCache.remove(fieldScreen.getId());
        }
    }

    public void createFieldScreenTab(FieldScreenTab fieldScreenTab) {
        try {
            decoratedStore.createFieldScreenTab(fieldScreenTab);
        } finally {
            if (fieldScreenTab != null && fieldScreenTab.getFieldScreen() != null) {
                fieldScreenCache.remove(fieldScreenTab.getFieldScreen().getId());
            }
        }
    }

    public void updateFieldScreenTab(FieldScreenTab fieldScreenTab) {
        try {
            decoratedStore.updateFieldScreenTab(fieldScreenTab);
        } finally {
            if (fieldScreenTab != null && fieldScreenTab.getFieldScreen() != null) {
                fieldScreenCache.remove(fieldScreenTab.getFieldScreen().getId());
            }
        }
    }

    public List<FieldScreenTab> getFieldScreenTabs(FieldScreen fieldScreen) {
        return decoratedStore.getFieldScreenTabs(fieldScreen);
    }

    public void updateFieldScreenLayoutItem(FieldScreenLayoutItem fieldScreenLayoutItem) {
        decoratedStore.updateFieldScreenLayoutItem(fieldScreenLayoutItem);
    }

    public void removeFieldScreenLayoutItem(FieldScreenLayoutItem fieldScreenLayoutItem) {
        decoratedStore.removeFieldScreenLayoutItem(fieldScreenLayoutItem);
    }

    public void removeFieldScreenLayoutItems(FieldScreenTab fieldScreenTab) {
        decoratedStore.removeFieldScreenLayoutItems(fieldScreenTab);
    }

    public List<FieldScreenLayoutItem> getFieldScreenLayoutItems(FieldScreenTab fieldScreenTab) {
        return decoratedStore.getFieldScreenLayoutItems(fieldScreenTab);
    }

    public void refresh() {
        allScreensCache.reset();
        fieldScreenCache.removeAll();

        if (log.isTraceEnabled()) {
            log.trace("Called refresh()", new Throwable());
        }
    }

    /**
     * Refreshes a single FieldScreen when there is a change to any of its constituent FieldScreenLayoutItem's.
     *
     * @param event a AbstractFieldScreenLayoutItemEvent
     */
    @EventListener
    public void onFieldScreenLayoutChange(AbstractFieldScreenLayoutItemEvent event) {
        fieldScreenCache.remove(event.getFieldScreenId());
    }

    public void createFieldScreenLayoutItem(FieldScreenLayoutItem fieldScreenLayoutItem) {
        decoratedStore.createFieldScreenLayoutItem(fieldScreenLayoutItem);
    }

    public FieldScreenLayoutItem buildNewFieldScreenLayoutItem(GenericValue genericValue) {
        return decoratedStore.buildNewFieldScreenLayoutItem(genericValue);
    }

    public void removeFieldScreenTabs(FieldScreen fieldScreen) {
        decoratedStore.removeFieldScreenTabs(fieldScreen);
        fieldScreenCache.remove(fieldScreen.getId());
    }

    public void removeFieldScreenTab(Long id) {
        FieldScreenTab tab = getFieldScreenTab(id);
        try {
            decoratedStore.removeFieldScreenTab(id);
        } finally {
            if (tab != null && tab.getFieldScreen() != null) {
                fieldScreenCache.remove(tab.getFieldScreen().getId());
            }
        }
    }

    public FieldScreenTab getFieldScreenTab(Long tabId) {
        // @TODO getting the tab by ID is not cached. Only used once at present
        return decoratedStore.getFieldScreenTab(tabId);
    }
}
