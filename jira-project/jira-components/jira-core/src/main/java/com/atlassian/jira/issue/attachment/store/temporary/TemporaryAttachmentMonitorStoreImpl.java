package com.atlassian.jira.issue.attachment.store.temporary;

import com.atlassian.fugue.Option;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentMonitorStore;
import com.atlassian.jira.issue.attachment.TemporaryWebAttachment;
import com.atlassian.jira.model.querydsl.TempAttachmentsMonitorDTO;
import org.joda.time.DateTime;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;
import java.util.stream.Collectors;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.jira.model.querydsl.QTempAttachmentsMonitor.TEMP_ATTACHMENTS_MONITOR;

/**
 */
@ParametersAreNonnullByDefault
public class TemporaryAttachmentMonitorStoreImpl implements TemporaryAttachmentMonitorStore {

    private final QueryDslAccessor databaseAccessor;

    public TemporaryAttachmentMonitorStoreImpl(QueryDslAccessor databaseAccessor) {
        this.databaseAccessor = databaseAccessor;
    }

    @Override
    public Option<TemporaryWebAttachment> removeById(TemporaryAttachmentId temporaryAttachmentId) {
        Option<TemporaryWebAttachment> byId = getById(temporaryAttachmentId);
        byId.forEach(twa -> removeByIdImpl(temporaryAttachmentId.toStringId()));
        return byId;
    }

    @Override
    public Option<TemporaryWebAttachment> getById(TemporaryAttachmentId temporaryAttachmentId) {
        return getByIdImpl(temporaryAttachmentId.toStringId());
    }

    @Override
    public Collection<TemporaryWebAttachment> getByFormToken(String formToken) {
        return getByFormTokenImpl(formToken);
    }

    @Override
    public boolean putIfAbsent(TemporaryWebAttachment temporaryWebAttachment) {
        Option<TemporaryWebAttachment> byId = getById(temporaryWebAttachment.getTemporaryAttachmentId());
        if (byId.isEmpty()) {
            putImpl(temporaryWebAttachment);
        }
        return byId.isDefined();
    }

    @Override
    public long removeOlderThan(DateTime dateTime) {
        return removeOlderThanImpl(dateTime);
    }

    private Option<TemporaryWebAttachment> getByIdImpl(String temporaryAttachmentId) {
        return databaseAccessor.executeQuery(dbConnection -> option(dbConnection.newSqlQuery()
                .select(TEMP_ATTACHMENTS_MONITOR)
                .from(TEMP_ATTACHMENTS_MONITOR)
                .where(TEMP_ATTACHMENTS_MONITOR.temporaryAttachmentId.eq(temporaryAttachmentId))
                .fetchOne())
                .map(this::toTemporaryWebAttachment));
    }

    private Collection<TemporaryWebAttachment> getByFormTokenImpl(String formToken) {
        return databaseAccessor.executeQuery(dbConnection -> dbConnection.newSqlQuery()
                .select(TEMP_ATTACHMENTS_MONITOR)
                .from(TEMP_ATTACHMENTS_MONITOR)
                .where(TEMP_ATTACHMENTS_MONITOR.formToken.eq(formToken))
                .fetch()
                .stream()
                .map(this::toTemporaryWebAttachment)
                .collect(Collectors.toList())
        );
    }

    private long removeOlderThanImpl(DateTime then) {
        return databaseAccessor.executeQuery(dbConnection -> dbConnection.delete(TEMP_ATTACHMENTS_MONITOR)
                .where(TEMP_ATTACHMENTS_MONITOR.createdTime.lt(then.getMillis())).execute());
    }

    private long removeByIdImpl(String temporaryAttachmentId) {
        return databaseAccessor.executeQuery(conn -> conn.delete(TEMP_ATTACHMENTS_MONITOR)
                .where(TEMP_ATTACHMENTS_MONITOR.temporaryAttachmentId.eq(temporaryAttachmentId))
                .execute());
    }

    private long putImpl(TemporaryWebAttachment temporaryWebAttachment) {
        return databaseAccessor.executeQuery(
                dbConnection -> dbConnection.insert(TEMP_ATTACHMENTS_MONITOR)
                        .set(TEMP_ATTACHMENTS_MONITOR.temporaryAttachmentId, temporaryWebAttachment.getTemporaryAttachmentId().toStringId())
                        .set(TEMP_ATTACHMENTS_MONITOR.contentType, temporaryWebAttachment.getContentType())
                        .set(TEMP_ATTACHMENTS_MONITOR.fileName, temporaryWebAttachment.getFilename())
                        .set(TEMP_ATTACHMENTS_MONITOR.formToken, temporaryWebAttachment.getFormToken())
                        .set(TEMP_ATTACHMENTS_MONITOR.createdTime, temporaryWebAttachment.getCreated().getMillis())
                        .set(TEMP_ATTACHMENTS_MONITOR.fileSize, temporaryWebAttachment.getSize())
                        .execute()
        );
    }

    private TemporaryWebAttachment toTemporaryWebAttachment(@Nonnull TempAttachmentsMonitorDTO dto) {
        return new TemporaryWebAttachment(
                TemporaryAttachmentId.fromString(dto.getTemporaryAttachmentId()),
                dto.getFileName(),
                dto.getContentType(),
                dto.getFormToken(),
                dto.getFileSize(),
                new DateTime(dto.getCreatedTime())
        );
    }
}
