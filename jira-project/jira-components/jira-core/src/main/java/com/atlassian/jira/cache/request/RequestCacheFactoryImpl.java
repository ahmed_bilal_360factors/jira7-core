package com.atlassian.jira.cache.request;

import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.Supplier;

import javax.annotation.Nonnull;
import java.util.NoSuchElementException;

public class RequestCacheFactoryImpl implements RequestCacheFactory {
    @Override
    public <K, V> RequestCache<K, V> createRequestCache(final String name) {
        return createRequestCache(name, new ExceptionalCacheLoader<K, V>(name));
    }

    @Override
    public <K, V> RequestCache<K, V> createRequestCache(final String name, final CacheLoader<K, V> cacheLoader) {
        return RequestCacheController.createRequestCache(name, cacheLoader);
    }

    /**
     * A cache loader that always throws an exception.
     * <p>
     * Request caches created with {@link RequestCacheFactory#createRequestCache(String)} cannot load
     * a value unless {@link RequestCache#get(Object, Supplier)} is used.  Attempting to load a missing
     * key without providing a supplier will reach this implementation, which always throws an exception.
     * </p>
     *
     * @param <K> the type of keys that will never be accepted
     * @param <V> the type of values that will never be returned
     */
    static class ExceptionalCacheLoader<K, V> implements CacheLoader<K, V> {
        private final String name;

        ExceptionalCacheLoader(final String name) {
            this.name = name;
        }

        @Nonnull
        @Override
        public V load(@Nonnull final K key) {
            throw new NoSuchElementException("Request cache '" + name + "' attempted to load missing key '" + key +
                    "' without providing either a CacheLoader or a Supplier for it");
        }

        @Override
        public String toString() {
            return "ExceptionalCacheLoader[name=" + name + ']';
        }
    }
}
