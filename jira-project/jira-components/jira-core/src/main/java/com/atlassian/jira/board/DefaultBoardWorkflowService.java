package com.atlassian.jira.board;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.jql.context.IssueTypeContext;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.workflow.AssignableWorkflowScheme;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowScheme;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.query.Query;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.atlassian.collectors.CollectorsUtil.toImmutableSet;
import static com.atlassian.jira.jql.context.QueryContext.ProjectIssueTypeContexts;
import static java.util.stream.Collectors.toMap;

public class DefaultBoardWorkflowService implements BoardWorkflowService {
    private final SearchService searchService;
    private final ProjectManager projectManager;
    private final WorkflowManager workflowManager;
    private final WorkflowSchemeManager workflowSchemeManager;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public DefaultBoardWorkflowService(
            SearchService searchService,
            ProjectManager projectManager,
            WorkflowManager workflowManager,
            WorkflowSchemeManager workflowSchemeManager
    ) {
        this.searchService = searchService;
        this.projectManager = projectManager;
        this.workflowManager = workflowManager;
        this.workflowSchemeManager = workflowSchemeManager;
    }

    @Override
    public Set<Status> getAccessibleStatuses(ApplicationUser user, Query query) {
        Collection<JiraWorkflow> workflows = getWorkflowsForQuery(user, query);

        return allStatusesFromWorkflowCollection(workflows);
    }

    @Override
    public Set<Status> getAllActiveWorkflowStatuses() {
        Collection<JiraWorkflow> activeWorkflows = workflowManager.getActiveWorkflows();

        return allStatusesFromWorkflowCollection(activeWorkflows);
    }

    @Override
    public Collection<JiraWorkflow> getJiraWorkflows(Project projectObj) {
        return getJiraWorkflows(projectObj, null);
    }

    @Override
    public Collection<JiraWorkflow> getJiraWorkflows(Project projectObj, String issueTypeId) {
        try {
            AssignableWorkflowScheme workflowScheme = workflowSchemeManager.getWorkflowSchemeObj(projectObj);
            if (issueTypeId == null) {
                return getWorkflowsFromWorkflowScheme(workflowScheme);
            } else {
                return Collections.singleton(workflowManager.getWorkflowFromScheme(workflowScheme, issueTypeId));
            }
        } catch (DataAccessException e) {
            logger.error("Tried to retrieve workflows for a project and got an error: %s", e);
            return Collections.emptySet();
        }
    }

    @Override
    public Set<Status> getInitialStatusesForQuery(ApplicationUser user, Query query) {
        return getWorkflowsForQuery(user, query)
                .stream()
                .map(this::getInitialStatusesForWorkflow)
                .flatMap(Collection::stream)
                .collect(toImmutableSet());
    }

    @Override
    @NotNull
    public Collection<JiraWorkflow> getWorkflowsForQuery(@NotNull ApplicationUser user, @NotNull Query query) {
        Collection<ProjectIssueTypeContexts> contexts = getProjectIssueTypeContextsForQuery(user, query);

        if (containsGlobalContext(contexts)) {
            return workflowManager.getActiveWorkflows();
        }

        return contexts
                .stream()
                .map(this::getWorkflowsForProjectIssueTypeContext)
                .flatMap(Collection::stream)
                .collect(toImmutableSet());
    }

    private Set<Status> allStatusesFromWorkflowCollection(Collection<JiraWorkflow> workflows) {
        return workflows
                .stream()
                .map(JiraWorkflow::getLinkedStatusObjects)
                .flatMap(Collection::stream)
                .collect(toImmutableSet());
    }

    private boolean containsGlobalContext(Collection<ProjectIssueTypeContexts> contexts) {
        return contexts
                .stream()
                .anyMatch(context -> context.getProjectContext().isAll());
    }

    private Collection<ProjectIssueTypeContexts> getProjectIssueTypeContextsForQuery(ApplicationUser user, Query query) {
        return searchService.getQueryContext(user, query).getProjectIssueTypeContexts();
    }

    private Set<JiraWorkflow> getWorkflowsForProjectIssueTypeContext(ProjectIssueTypeContexts context) {
        Long projectId = context.getProjectContext().getProjectId();
        Project project = projectManager.getProjectObj(projectId);
        Set<String> issueTypeIdsInContext = context.getIssueTypeContexts()
                .stream()
                .map(IssueTypeContext::getIssueTypeId)
                .filter(Objects::nonNull)
                .collect(toImmutableSet());
        Set<String> issueTypeIdsInProject = getIssueTypeIdsInProject(project);
        Set<String> issueTypeIdsInProjectAndContext = issueTypeIdsInContext.isEmpty() ?
                // When no issue type is specified in the query, use the issue types of this project to get the workflows. See JC-870
                issueTypeIdsInProject :
                // Otherwise use the intersection, since the query can specify issue types not associated with this project.
                // In particular `issueType in standardIssueTypes()` puts all standard issue types into the context.
                Sets.intersection(issueTypeIdsInContext, issueTypeIdsInProject);

        return getWorkflowsForProjectAndIssueTypeIds(project, issueTypeIdsInProjectAndContext);
    }

    private Set<JiraWorkflow> getWorkflowsForProjectAndIssueTypeIds(Project project, Set<String> issueTypes) {
        return issueTypes
                .stream()
                .map(issueTypeId -> getJiraWorkflows(project, issueTypeId))
                .flatMap(Collection::stream)
                .collect(toImmutableSet());
    }

    private Set<String> getIssueTypeIdsInProject(Project project) {
        return project.getIssueTypes()
                .stream()
                .map(issueType -> issueType.getId())
                .collect(toImmutableSet());
    }

    private Set<Status> getInitialStatusesForWorkflow(JiraWorkflow workflow) {
        List<ActionDescriptor> initialActions = (List<ActionDescriptor>) workflow.getDescriptor().getInitialActions();

        return initialActions
                .stream()
                .map(actionDescriptor -> actionDescriptor.getUnconditionalResult().getStep())
                .map(stepId -> workflow.getDescriptor().getStep(stepId))
                .map(workflow::getLinkedStatus)
                .filter(Objects::nonNull)
                .collect(toImmutableSet());
    }

    private Collection<JiraWorkflow> getWorkflowsFromWorkflowScheme(@NotNull WorkflowScheme workflowScheme) {
        Iterable<JiraWorkflow> result = workflowManager.getWorkflowsFromScheme(workflowSchemeManager.getSchemeObject(workflowScheme.getId()));
        if (result == null) {
            result = Collections.emptySet();
        }
        return ImmutableList.copyOf(Iterables.filter(result, Predicates.notNull()));
    }
}
