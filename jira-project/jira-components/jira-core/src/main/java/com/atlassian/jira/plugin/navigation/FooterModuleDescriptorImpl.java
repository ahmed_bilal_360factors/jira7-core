package com.atlassian.jira.plugin.navigation;

import com.atlassian.extras.api.LicenseType;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.plugin.AbstractJiraModuleDescriptor;
import com.atlassian.jira.plugin.util.ModuleDescriptorXMLUtils;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.web.util.ExternalLinkUtilImpl;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.util.profiling.UtilTimerStack;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.config.properties.APKeys.JIRA_SHOW_CONTACT_ADMINISTRATORS_FORM;
import static com.atlassian.jira.permission.GlobalPermissionKey.SYSTEM_ADMIN;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * Module descriptor for footer modules.
 *
 * @since v3.12
 */
public class FooterModuleDescriptorImpl extends AbstractJiraModuleDescriptor<PluggableFooter> implements FooterModuleDescriptor {
    private static final String VIEW_TEMPLATE = "view";
    public static final String LICENSE_MESSAGE_INFO = "licensemessage";
    public static final String LICENSE_MESSAGE_ERROR = "licensemessagered";

    private final JiraLicenseService jiraLicenseService;
    private final BuildUtilsInfo buildUtilsInfo;
    private final GlobalPermissionManager permissionManager;
    private final ApplicationProperties applicationProperties;
    private final ClusterManager clusterManager;
    private final JiraLicenseManager licenseManager;
    private final ApplicationRoleManager applicationRoleManager;

    private int order;

    public FooterModuleDescriptorImpl(final JiraAuthenticationContext authenticationContext,
                                      final JiraLicenseService jiraLicenseService, final BuildUtilsInfo buildUtilsInfo,
                                      final ModuleFactory moduleFactory, final GlobalPermissionManager permissionManager,
                                      final ApplicationProperties applicationProperties, final ClusterManager clusterManager,
                                      final JiraLicenseManager licenseManager, final ApplicationRoleManager applicationRoleManager) {
        super(authenticationContext, moduleFactory);
        this.licenseManager = licenseManager;
        this.applicationRoleManager = applicationRoleManager;
        this.clusterManager = notNull("clusterManager", clusterManager);
        this.jiraLicenseService = notNull("jiraLicenseService", jiraLicenseService);
        this.buildUtilsInfo = notNull("buildUtilsInfo", buildUtilsInfo);
        this.permissionManager = permissionManager;
        this.applicationProperties = applicationProperties;
    }

    public void init(Plugin plugin, Element element) throws PluginParseException {
        super.init(plugin, element);
        order = ModuleDescriptorXMLUtils.getOrder(element);
    }

    public void enabled() {
        super.enabled();
    }

    public int getOrder() {
        return order;
    }

    /**
     * This method will setup the params related to the license information and render the html for the footer.
     *
     * @param request        the servlet request
     * @param startingParams any parameters that you want to have available in the context when rendering the footer.
     * @return html representing the footer.
     */
    public String getFooterHtml(HttpServletRequest request, Map<String, ?> startingParams) {
        Map<String, ?> params = createVelocityParams(request, startingParams);
        return getHtml(VIEW_TEMPLATE, params);
    }

    protected Map<String, ?> createVelocityParams(HttpServletRequest request, Map<String, ?> startingParams) {
        final Map<String, Object> params = (startingParams != null) ? new HashMap<>(startingParams) : new HashMap<String, Object>();

        String licenseMessageClass = null;


        if (!jiraLicenseService.isLicenseSet()) {
            params.put("notfull", Boolean.TRUE);

            params.put("unlicensed", Boolean.TRUE);
            licenseMessageClass = LICENSE_MESSAGE_ERROR;
            params.put("organisation", "<Unknown>");
        } else if (licenseManager.hasLicenseTooOldForBuildConfirmationBeenDone()) {
            params.put("confirmedWithOldLicense", Boolean.TRUE);
            licenseMessageClass = LICENSE_MESSAGE_ERROR;
        } else {
            Iterable<LicenseDetails> licenses = jiraLicenseService.getLicenses();
            Set<String> organisations = Sets.newLinkedHashSetWithExpectedSize(Iterables.size(licenses));
            for (LicenseDetails licenseDetails : licenses) {
                if (!isBlank(licenseDetails.getOrganisation())) organisations.add(licenseDetails.getOrganisation());

                if (licenseDetails.isEvaluation() || !(licenseDetails.isCommercial() || licenseDetails.isStarter())) {
                    params.put("notfull", Boolean.TRUE);

                    if (licenseDetails.isEvaluation()) {
                        params.put("evaluation", Boolean.TRUE);
                        licenseMessageClass = LICENSE_MESSAGE_ERROR;
                    } else if (licenseMessageClass == null) // saves going through the license details for every license.
                    { // we only ever have one type, and we only display info about it if it's not failed the other tests
                        LicenseType licenseType = licenseDetails.getLicenseType(); // easy speed-up; only parse once
                        if (licenseType == LicenseType.COMMUNITY) {
                            params.put("community", Boolean.TRUE);
                            licenseMessageClass = LICENSE_MESSAGE_INFO;
                        } else if (licenseType == LicenseType.OPEN_SOURCE) {
                            params.put("opensource", Boolean.TRUE);
                            licenseMessageClass = LICENSE_MESSAGE_INFO;
                        } else if (licenseType == LicenseType.NON_PROFIT) {
                            params.put("nonprofit", Boolean.TRUE);
                            licenseMessageClass = LICENSE_MESSAGE_INFO;
                        } else if (licenseType == LicenseType.DEMONSTRATION) {
                            params.put("demonstration", Boolean.TRUE);
                            licenseMessageClass = LICENSE_MESSAGE_INFO;
                        } else if (licenseType == LicenseType.DEVELOPER) {
                            params.put("developer", Boolean.TRUE);
                            licenseMessageClass = LICENSE_MESSAGE_INFO;
                        } else if (licenseType == LicenseType.PERSONAL) {
                            params.put("personal", licenseDetails.isPersonalLicense());
                            licenseMessageClass = LICENSE_MESSAGE_INFO;
                        }
                    }
                }
            }

            params.put("organisation", StringUtils.join(organisations, ", "));
        }

        if (licenseMessageClass != null) {
            // this is used in rendering to decide whether to display a special message at all
            params.put("licenseMessageClass", licenseMessageClass);
        }
        params.put("serverid", jiraLicenseService.getServerId());
        params.put("externalLinkUtil", ExternalLinkUtilImpl.getInstance());
        params.put("utilTimerStack", new UtilTimerStack());
        params.put("version", buildUtilsInfo.getVersion());
        params.put("buildNumber", buildUtilsInfo.getCurrentBuildNumber());
        params.put("req", request);
        params.put("build", buildUtilsInfo);
        params.put("string", new StringUtils());
        params.put("isSysAdmin", permissionManager.hasPermission(SYSTEM_ADMIN, getAuthenticationContext().getUser()));
        params.put("showContactAdminForm", applicationProperties.getOption(JIRA_SHOW_CONTACT_ADMINISTRATORS_FORM));

        if (clusterManager.isClustered()) {
            params.put("nodeId", clusterManager.getNodeId());
        }

        return params;
    }
}
