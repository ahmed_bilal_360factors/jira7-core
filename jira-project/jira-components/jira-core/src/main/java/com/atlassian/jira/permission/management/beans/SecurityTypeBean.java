package com.atlassian.jira.permission.management.beans;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.List;

@JsonAutoDetect
public class SecurityTypeBean {
    private String securityType;
    private String displayName;
    private List<SecurityTypeValueBean> values;
    private ProjectPermissionHelpBean help;

    private SecurityTypeBean(final String securityType, final String displayName,
                             final Iterable<SecurityTypeValueBean> values, final ProjectPermissionHelpBean help) {
        this.securityType = securityType;
        this.displayName = displayName;
        this.values = values != null ? ImmutableList.copyOf(values) : null;
        this.help = help;
    }

    public String getSecurityType() {
        return securityType;
    }

    public void setSecurityType(String securityType) {
        this.securityType = securityType;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<SecurityTypeValueBean> getValues() {
        return values;
    }

    public void setValues(List<SecurityTypeValueBean> values) {
        this.values = values != null ? ImmutableList.copyOf(values) : null;
    }

    public void setHelp(ProjectPermissionHelpBean help) {
        this.help = help;
    }

    public ProjectPermissionHelpBean getHelp() {
        return help;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(SecurityTypeBean data) {
        return new Builder(data);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecurityTypeBean that = (SecurityTypeBean) o;

        return Objects.equal(this.securityType, that.securityType) &&
                Objects.equal(this.displayName, that.displayName) &&
                Objects.equal(this.values, that.values);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(securityType, displayName, values);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("securityType", securityType)
                .add("displayName", displayName)
                .add("values", values)
                .add("help", help)
                .toString();
    }

    public static final class Builder {


        private String securityType;
        private String displayName;
        private List<SecurityTypeValueBean> values = Lists.newArrayList();
        private ProjectPermissionHelpBean help;

        private Builder() {
        }

        private Builder(SecurityTypeBean initialData) {

            this.securityType = initialData.securityType;
            this.displayName = initialData.displayName;
            this.values = initialData.values;
        }


        public Builder setSecurityType(String securityType) {
            this.securityType = securityType;
            return this;
        }


        public Builder setDisplayName(String displayName) {
            this.displayName = displayName;
            return this;
        }


        public Builder setValues(List<SecurityTypeValueBean> values) {
            this.values = values;
            return this;
        }


        public Builder addValue(SecurityTypeValueBean value) {
            this.values.add(value);
            return this;
        }

        public Builder addValues(Iterable<SecurityTypeValueBean> values) {
            for (SecurityTypeValueBean value : values) {
                addValue(value);
            }
            return this;
        }

        public Builder setHelp(ProjectPermissionHelpBean help) {
            this.help = help;
            return this;
        }

        public SecurityTypeBean build() {
            return new SecurityTypeBean(securityType, displayName, values, help);
        }
    }
}
