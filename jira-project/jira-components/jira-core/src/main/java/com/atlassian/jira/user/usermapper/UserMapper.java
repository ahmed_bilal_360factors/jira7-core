package com.atlassian.jira.user.usermapper;

import com.atlassian.jira.user.ApplicationUser;

public interface UserMapper {
    /**
     * Return a user for this particular email address.  If no user is found, return null.
     */
    public ApplicationUser getUserFromEmailAddress(String emailAddress);
}
