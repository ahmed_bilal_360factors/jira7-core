package com.atlassian.jira.startup;

import com.atlassian.instrumentation.operations.OpTimerFactory;
import com.atlassian.jdk.utilities.runtimeinformation.RuntimeInformationFactory;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.index.ha.DisasterRecoveryLauncher;
import com.atlassian.jira.instrumentation.DefaultInstrumentationListenerManager;
import com.atlassian.jira.instrumentation.Instrumentation;
import com.atlassian.jira.issue.index.DefaultIndexManager;
import com.atlassian.jira.tenancy.JiraTenancyCondition;
import com.atlassian.jira.tenancy.TenantManager;
import com.atlassian.jira.upgrade.ConsistencyCheckImpl;
import com.atlassian.jira.upgrade.PluginSystemLauncher;
import com.atlassian.jira.upgrade.UpgradeVersionHistoryManager;
import com.atlassian.jira.util.StopWatch;
import com.atlassian.jira.util.devspeed.JiraDevSpeedTimer;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.jira.web.ServletContextProvider;
import com.atlassian.johnson.event.Event;
import com.atlassian.mail.queue.MailQueue;
import com.atlassian.threadlocal.BruteForceThreadLocalCleanup;
import com.atlassian.threadlocal.RegisteredThreadLocals;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

import static com.atlassian.jira.component.ComponentAccessor.getComponent;
import static com.atlassian.jira.component.ComponentAccessor.getComponentSafely;
import static com.atlassian.jira.upgrade.PluginSystemLauncher.INSTANT_DOWN_PROPERTY;

/**
 * This implementation of JiraLauncher contains all of the smarts of what to start in which order to ensure that JIRA
 * starts properly.
 *
 * @since v4.3
 */
public class DefaultJiraLauncher implements JiraLauncher {
    private static final Logger log = LoggerFactory.getLogger(DefaultJiraLauncher.class);

    private final ChecklistLauncher startupChecklist;
    private final BootstrapContainerLauncher bootstrapContainerLauncher;
    private final ComponentContainerLauncher componentContainerLauncher;
    private final NotificationInstanceKiller notificationInstanceKiller;
    private final DatabaseLauncher databaseLauncher;
    private final PluginSystemLauncher pluginSystemLauncher;
    private final ConsistencyCheckImpl consistencyChecker;
    private final SystemInfoLauncher systemInfoLauncher;
    private final FailedPluginsLauncher preDbFailedPluginsLauncher;
    private final FailedPluginsLauncher postDbFailedPluginsLauncher;
    private final ClusteringLauncher clusteringLauncher;
    private final ClusteringChecklistLauncher clusteringChecklistLauncher;
    private final ActiveServicesLauncher activeServicesLauncher;
    private final JiraProperties jiraSystemProperties;
    private final DisasterRecoveryLauncher disasterRecoveryLauncher;
    private final ReindexRequestCleaner reindexRequestCleaner;
    private final EmbeddedDatabaseServerLauncher embeddedDatabaseServerLauncher;
    private final IndexRecoveryLauncher indexRecoveryLauncher;
    private final JohnsonProvider johnsonProvider;
    private final ImageIOProviderScannerLauncher imageIOProviderScannerLauncher;
    private final CacheWarmerLauncher cacheWarmerLauncher;
    private StopWatch preActivationTimer;

    public DefaultJiraLauncher(JohnsonProvider johnsonProvider) {
        this.johnsonProvider = johnsonProvider;
        this.jiraSystemProperties = JiraSystemProperties.getInstance();
        this.startupChecklist = new ChecklistLauncher(jiraSystemProperties, johnsonProvider);
        this.bootstrapContainerLauncher = new BootstrapContainerLauncher();
        this.componentContainerLauncher = new ComponentContainerLauncher();
        this.notificationInstanceKiller = new NotificationInstanceKiller();
        this.databaseLauncher = new DatabaseLauncher(jiraSystemProperties);
        this.pluginSystemLauncher = new PluginSystemLauncher(jiraSystemProperties);
        this.consistencyChecker = new ConsistencyCheckImpl(johnsonProvider);
        this.systemInfoLauncher = new SystemInfoLauncher();
        this.preDbFailedPluginsLauncher = new FailedPluginsLauncher();
        this.postDbFailedPluginsLauncher = new FailedPluginsLauncher();
        this.clusteringLauncher = new ClusteringLauncher();
        this.clusteringChecklistLauncher = new ClusteringChecklistLauncher(johnsonProvider);
        this.activeServicesLauncher = new ActiveServicesLauncher(johnsonProvider);
        this.disasterRecoveryLauncher = new DisasterRecoveryLauncher();
        this.embeddedDatabaseServerLauncher = new EmbeddedDatabaseServerLauncher();
        this.reindexRequestCleaner = new ReindexRequestCleaner();
        this.indexRecoveryLauncher = new IndexRecoveryLauncher();
        this.imageIOProviderScannerLauncher = new ImageIOProviderScannerLauncher();
        this.cacheWarmerLauncher = new CacheWarmerLauncher(jiraSystemProperties);
    }

    @Override
    public void start() {
        this.preActivationTimer = new StopWatch();
        // Initialize instrumentation
        DefaultInstrumentationListenerManager.startContext("startup");

        JiraDevSpeedTimer.run(getStartupName(), () -> {
            preDbLaunch();
            postDbLaunch();
        });
        DefaultIndexManager.flushThreadLocalSearchers();  // JRA-29587

        // Flush stats on startup information. This gives us Cache load times esp. for CachedReference type caches.
        DefaultInstrumentationListenerManager.endContext();
    }
    
    /**
     * Things that can or must be done before the database is configured.
     */
    private void preDbLaunch() {
        systemInfoLauncher.start();
        bootstrapContainerLauncher.start();
        //JRADEV-17174: We must have a bootstrap container before we do the startup checks
        startupChecklist.start();
        imageIOProviderScannerLauncher.start();
        preDbFailedPluginsLauncher.start();
    }

    /**
     * Those things that need the database to have been configured.
     */
    private void postDbLaunch() {
        if (JiraStartupChecklist.startupOK()) {
            final DatabaseConfigurationManager dbcm = getComponent(DatabaseConfigurationManager.class);
            final UpgradeVersionHistoryManager upgradeVersionHistoryManager =
                    ComponentAccessor.getComponentOfType(UpgradeVersionHistoryManager.class);

            embeddedDatabaseServerLauncher.start();

            dbcm.doNowOrWhenDatabaseConfigured(
                    () -> new DatabaseChecklistLauncher(dbcm, jiraSystemProperties, johnsonProvider, upgradeVersionHistoryManager)
                            .start(),
                    "Database Checklist Launcher"
            );

            // Delayed DB activation only happens on server (it happens in setup wizard)
            dbcm.doNowOrWhenDatabaseActivated(() -> {
                //JRADEV-20303 If the build number fails get out of Dodge.
                final Collection<Event> events = johnsonProvider.getContainer().getEvents();
                if (events.isEmpty()) {
                    componentContainerLauncher.start(); //start pico
                    databaseLauncher.start();
                    disasterRecoveryLauncher.earlyStart();
                    clusteringChecklistLauncher.start();
                    clusteringLauncher.start();
                    pluginSystemLauncher.start(); //start Plugin System - if there's no tenant, this will only start phase 1 plugins
                    postDBActivated(); // Enqueues or runs tasks that require a tenant to be present
                } else {
                    log.error("Johnson events present; aborting: {}", events);
                }
            }, "Post database-configuration launchers");
        }
    }

    private void postDBActivated() {
        final TenantManager tenantManager = getComponent(TenantManager.class);
        boolean wasAlreadyTenanted = tenantManager.isTenanted();
        tenantManager.doNowOrWhenTenantArrives(() -> {
            final Collection<Event> events = johnsonProvider.getContainer().getEvents();
            if (events.isEmpty()) {
                postTenantArrived(wasAlreadyTenanted);
            } else {
                log.error("Johnson events present; aborting: {}", events);
            }
        }, "Tenanted launchers");
    }

    private void postTenantArrived(boolean wasAlreadyTenanted) {
        if (wasAlreadyTenanted) {
            logPreActivationTime();
        }
        final InstantUpgradeManager instantUpgradeManager = getComponent(InstantUpgradeManager.class);
        instantUpgradeManager.doNowOrWhenInstanceBecomesActive(() -> {
            final Collection<Event> events = johnsonProvider.getContainer().getEvents();
            if (events.isEmpty()) {
                consistencyChecker.initialise(ServletContextProvider.getServletContext());
                reindexRequestCleaner.start();
                pluginSystemLauncher.lateStart(); // runs any delayed plugins, if any
                //Migration and JIRA startup is triggered in the activeServicesLauncher.
                activeServicesLauncher.start();
                notificationInstanceKiller.deleteAfterDelay();
                disasterRecoveryLauncher.start();
                indexRecoveryLauncher.start();
                cacheWarmerLauncher.start();
                postDbFailedPluginsLauncher.start();
            } else {
                log.error("Johnson events present; aborting: {}", events);
            }
        }, "Late startup launchers");
    }

    @Override
    public void stop() {
        log.info("Stopping launchers");
        StopWatch stopWatch = new StopWatch();

        ThreadDumper threadDumper = null;

        OpTimerFactory opTimerFactory = null;
        try {
            threadDumper = startDumper();
            getComponentSafely(MailQueue.class).ifPresent(mq -> mq.sendBuffer());
            // We moved the clustering launcher here so the other node receives the signal immediately
            // and the other nodes don't fail while this node is shutting down.
            clusteringLauncher.stop();
            activeServicesLauncher.stop();
            reindexRequestCleaner.stop();
            consistencyChecker.destroy(ServletContextProvider.getServletContext());
            pluginSystemLauncher.stop();
            databaseLauncher.stop();
            imageIOProviderScannerLauncher.stop();
            startupChecklist.stop();
            //after next command PICO dies so we have to cache interesting components
            opTimerFactory = getComponent(OpTimerFactory.class);
            componentContainerLauncher.stop();
            bootstrapContainerLauncher.stop();
            systemInfoLauncher.stop();
            preDbFailedPluginsLauncher.stop();
            postDbFailedPluginsLauncher.stop();
            embeddedDatabaseServerLauncher.stop();
        } finally {
            if (threadDumper != null) {
                threadDumper.cancelDump();
            }
        }

        cleanupAfterOurselves(opTimerFactory, stopWatch);
    }

    // Logs Phase1 startup only when tenancy enabled (onDemand)
    private void logPreActivationTime() {
        JiraTenancyCondition tenancyCondition = new JiraTenancyCondition(jiraSystemProperties);
        if (!tenancyCondition.isEnabled()) {
            return;
        }

        String logKey = "jira.launcher.start.preactivation.millis";
        String message = "JIRA early startup took {}s";
        long elapsedTime = this.preActivationTimer.getTotalTime();
        MDC.put(logKey, elapsedTime);
        log.info(message, elapsedTime/1000);
        MDC.remove(logKey);
    }

    private void cleanupAfterOurselves(OpTimerFactory opTimerFactory, StopWatch stopWatch) {
        cleanupThreadLocals(opTimerFactory);
        logShutDownTime(stopWatch);
        // and shutdown log4j for good measure
        LogManager.shutdown();
    }

    private void logShutDownTime(StopWatch stopWatch) {
        long elapsedTime = stopWatch.getTotalTime();
        String logKey = jiraSystemProperties.getBoolean(INSTANT_DOWN_PROPERTY) ? "jira.launcher.stop.instant.millis" : "jira.launcher.stop.millis";
        MDC.put(logKey, elapsedTime);
        log.info("JIRA launchers stopped in {}ms", elapsedTime);
        MDC.remove(logKey);
    }

    private ThreadDumper startDumper() {
        ThreadDumper threadDumper = new ThreadDumper(TimeUnit.MINUTES.toMillis(4));

        if (jiraSystemProperties.isDevMode() || jiraSystemProperties.getBoolean("jira.dump")) {
            // don't use a threadpool; we want independence from the rest of JIRA's threads.
            Thread thread = new Thread(threadDumper, "Thread-dumping thread from " + this.getClass().getSimpleName());

            thread.setDaemon(true);
            thread.start();
        }
        return threadDumper;
    }

    private void cleanupThreadLocals(OpTimerFactory opTimerFactory) {
        // we do instrument on the main servlet context thread so we need to clean it up
        Instrumentation.snapshotThreadLocalOperationsAndClear(opTimerFactory);

        // clean up any TL that have done the right thing and registered themselves
        RegisteredThreadLocals.reset();

        // now brute force any other thread locals for this class loader
        BruteForceThreadLocalCleanup.cleanUp(getClass().getClassLoader());
    }

    private String getStartupName() {
        final String jvmInputArguments = StringUtils.defaultString(RuntimeInformationFactory.getRuntimeInformation().getJvmInputArguments());
        final boolean rebel = jvmInputArguments.contains("jrebel.jar");
        final boolean debugMode = jvmInputArguments.contains("-Xdebug");

        return "jira.startup" + (debugMode ? ".debug" : ".run") + (rebel ? ".jrebel" : "");
    }
}
