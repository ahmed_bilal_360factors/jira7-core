package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.embedded.spi.UserDao;
import com.atlassian.crowd.event.migration.XMLRestoreFinishedEvent;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.model.user.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

/**
 * User DAO with legacy JIRA customizations.
 */
public interface ExtendedUserDao extends UserDao {

    boolean useFullCache();

    boolean useInternedUserValues();

    /**
     * We check if the cache is initialized or not.
     * This is an important cache to see if jira is available or not for the users to login.
     *
     * @return true if the cache is initialized, false if not.
     */
    boolean isCacheInitialized();

    long getUniqueUserCount(Set<Long> directoryIds) throws DirectoryNotFoundException;

    /**
     * Returns the lower-cased names of all users in the specified groups.  Only searches directly in these groups, if a
     * recursive group search is required the caller will need to pre-expand the groups.
     *
     * @param groupNames the names of the groups to search.
     * @return set of all user names in these groups, in lower case.
     */
    Collection<String> findNamesOfUsersInGroups(Collection<String> groupNames);

    OfBizUser findOfBizUser(final long directoryId, final String userName) throws UserNotFoundException;

    List<OfBizUser> findAllByNameOrNull(final long directoryId, final @Nonnull Collection<String> userNames);

    /**
     * Tries to find the user by name and returns null if not found.
     * Just like the public method should have done in the first place!
     *
     * @param directoryId Directory ID
     * @param userName    the username
     * @return the user, or {@code null} if the user does not exist
     */
    @Nullable
    OfBizUser findByNameOrNull(final long directoryId, final @Nonnull String userName);

    Collection<String> getAllAttributeKeys();

    /**
     * Performs an operation on every user in the system.
     * <p>
     * This can be a time-consuming operation on JIRA instances that contain many users.  It is recommended
     * to use one of the search methods where possible.
     * <p>
     * This method performs a live iteration over a database result set.  Care must be taken if additional database
     * operations are performed within the <code>userProcessor</code> consumer.
     *
     * @param userProcessor the operation to perform on each user.
     * @since v7.0
     */
    void processUsers(Consumer<? super User> userProcessor);

    /**
     * Invoked by {@link OfBizCacheFlushingManager} to ensure caches are being flushed in the right order on {@link
     * XMLRestoreFinishedEvent}
     */
    void flushCache();

    /**
     * Finds a user by internal user ID.
     *
     * @param internalUserId the user ID.
     *
     * @return the found user, or null if not found.
     */
    @Nullable
    OfBizUser findById(long internalUserId);
}
