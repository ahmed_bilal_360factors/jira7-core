package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * This is a utility that derives an application role's name from the key. It should be used as a last resort after
 * attempting to retrieve the name from the defined applications. This utility can be used when the application role is
 * undefined. For the 3 known renaissance applications it will return the known names, and for the rest, it will return
 * a derivative from the application key.
 *
 * @since v7.0
 */
public class UndefinedApplicationRoleName {
    /**
     * Map of known applications to their product trademark names, in the order they would appear in {@link #getName()}
     * should more than 1 application be present in a license (note: ImmutableMap preserves insertion order).
     */
    private static final Map<ApplicationKey, UndefinedApplicationRoleName> knownApplications = ImmutableMap.of(
            ApplicationKeys.SERVICE_DESK, new UndefinedApplicationRoleName("JIRA Service Desk"),
            ApplicationKeys.SOFTWARE, new UndefinedApplicationRoleName("JIRA Software"),
            ApplicationKeys.CORE, new UndefinedApplicationRoleName("JIRA Core")
    );

    private String name = "UNKNOWN";

    public static UndefinedApplicationRoleName of(ApplicationKey appId) {
        if (knownApplications.containsKey(appId)) {
            return knownApplications.get(appId);
        }
        return new UndefinedApplicationRoleName(deriveName(appId));
    }

    private UndefinedApplicationRoleName(String theName) {
        name = theName;
    }


    public String getName() {
        return name;
    }

    private static String deriveName(ApplicationKey appId) {
        final String[] nameParts = appId.value().split("[-.]");
        final StringBuilder convertedName = new StringBuilder();
        for (int index = 0; index < nameParts.length; index++) {
            final String namePart = nameParts[index];
            if (StringUtils.isBlank(namePart)) {
                continue;
            }
            if (namePart.equalsIgnoreCase("jira")) {
                convertedName.append("JIRA ");
                continue;
            }
            convertedName.append(StringUtils.capitalize(namePart));
            if (index != (nameParts.length - 1)) {
                convertedName.append(" ");
            }
        }
        return convertedName.toString();
    }
}
