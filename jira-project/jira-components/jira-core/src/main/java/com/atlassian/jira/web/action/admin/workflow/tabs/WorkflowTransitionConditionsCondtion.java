package com.atlassian.jira.web.action.admin.workflow.tabs;

import com.atlassian.plugin.web.Condition;

import java.util.Map;

import static com.atlassian.jira.web.action.admin.workflow.tabs.WorkflowTransitionContextUtils.getTransition;
import static com.atlassian.jira.web.action.admin.workflow.tabs.WorkflowTransitionContextUtils.getWorkflow;

/**
 * A web condition that controls whether the "Conditions" tab is shown when configuring a workflow transition.
 */
public class WorkflowTransitionConditionsCondtion implements Condition {

    @Override
    public void init(final Map<String, String> params) {}

    @Override
    public boolean shouldDisplay(final Map<String, Object> context) {
        // "initial" is a special case - it's the transition that happens as you create an issue.
        // You can't add conditions but you can add validators and post functions (e.g. assign to reporter).
        return getWorkflow(context).flatMap(
                workflow -> getTransition(context).map(actionDescriptor -> !workflow.isInitialAction(actionDescriptor))
        ).orElse(false);
    }
}
