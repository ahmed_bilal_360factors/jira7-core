package com.atlassian.jira.issue.customfields.impl;

import com.atlassian.core.util.StringUtils;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.export.FieldExportPartsBuilder;
import com.atlassian.jira.issue.export.customfield.CustomFieldExportContext;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.issue.export.customfield.ExportableCustomFieldType;
import com.atlassian.jira.security.JiraAuthenticationContext;

/**
 * This custom field type returns true for its isRenderable method and is meant to
 * represent a text custom field type that is renderable.
 */
public class RenderableTextCFType extends GenericTextCFType implements ExportableCustomFieldType {

    public RenderableTextCFType(
            final CustomFieldValuePersister customFieldValuePersister,
            final GenericConfigManager genericConfigManager,
            final TextFieldCharacterLengthValidator textFieldCharacterLengthValidator,
            final JiraAuthenticationContext jiraAuthenticationContext) {
        super(customFieldValuePersister, genericConfigManager, textFieldCharacterLengthValidator, jiraAuthenticationContext);
    }

    public boolean isRenderable() {
        return true;
    }

    public boolean valuesEqual(String v1, String v2) {
        if (v1 == v2) {
            return true;
        }

        if (v1 == null || v2 == null) {
            return false;
        }

        // Compare string ignoring line terminators
        return StringUtils.equalsIgnoreLineTerminators(v1, v2);
    }

    @Override
    public Object accept(VisitorBase visitor) {
        if (visitor instanceof Visitor) {
            return ((Visitor) visitor).visitRenderableText(this);
        }

        return super.accept(visitor);
    }

    @Override
    public FieldExportParts getRepresentationFromIssue(Issue issue, CustomFieldExportContext context) {
        final String value = getValueFromIssue(context.getCustomField(), issue);
        return FieldExportPartsBuilder.buildSinglePartRepresentation(context.getCustomField().getId(), context.getDefaultColumnHeader(), value);
    }

    public interface Visitor<T> extends VisitorBase<T> {
        T visitRenderableText(RenderableTextCFType renderableTextCustomFieldType);
    }
}
