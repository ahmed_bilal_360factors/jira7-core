package com.atlassian.jira.upgrade.tasks;

import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.upgrade.LegacyImmediateUpgradeTask;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;

import static com.atlassian.jira.entity.GenericValueFunctions.getLong;
import static com.google.common.collect.Collections2.transform;
import static org.ofbiz.core.entity.EntityOperator.EQUALS;

/**
 * Filling in missing external ids for users in internal directory
 *
 * @since v6.1
 */
public class UpgradeTask_Build6151 extends LegacyImmediateUpgradeTask {
    static final String ENTITY = "User";
    static final String EXTERNAL_ID = "externalId";
    static final String USER_NAME = "userName";
    static final String DIRECTORY_ID = "directoryId";

    static class Directory {
        static final String ENTITY = "Directory";
        static final String DIRECTORY_ID = "id";
        static final String TYPE = "type";
    }


    public UpgradeTask_Build6151() {
        super();
    }

    @Override
    public int getBuildNumber() {
        return 6151;
    }

    @Override
    public String getShortDescription() {
        return "Set unique id for users in internal directory";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception {
        EntityExpr directoryEqualsInternal = new EntityExpr(Directory.TYPE, EQUALS, DirectoryType.INTERNAL.toString());
        final List<GenericValue> internalDirectories = Select.columns(Directory.DIRECTORY_ID)
                .from(Directory.ENTITY).whereCondition(directoryEqualsInternal)
                .runWith(getEntityEngine()).asList();
        final Collection<Long> internalDirectoryIds = transform(internalDirectories, getLong(Directory.DIRECTORY_ID));

        Select.from(ENTITY)
                .whereCondition(new EntityExpr(DIRECTORY_ID, EntityOperator.IN, internalDirectoryIds))
                .andEqual(EXTERNAL_ID, (String) null).runWith(getEntityEngine()).visitWith(element ->
        {
            try {
                element.set(EXTERNAL_ID, InternalDirectory.generateUniqueIdentifier());
                element.store();
            } catch (GenericEntityException e) {
                throw new RuntimeException(String.format("Unable to store User %s (directory %s)",
                        element.getString(USER_NAME), element.getLong(DIRECTORY_ID)), e);
            }
        });

        UpgradeTask_Build602.flushUserCaches();
    }

    @Nullable
    @Override
    public Integer dependsUpon() {
        return 6142;
    }

}
