package com.atlassian.jira.security.groups;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.embedded.api.UnfilteredCrowdService;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.GroupQuery;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.database.QueryCallback;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.util.Page;
import com.atlassian.jira.util.PageRequest;
import com.atlassian.jira.util.Pages;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static com.atlassian.crowd.model.group.GroupType.GROUP;
import static com.atlassian.crowd.search.EntityDescriptor.group;
import static com.atlassian.crowd.search.EntityDescriptor.user;
import static com.atlassian.crowd.search.builder.Combine.anyOf;
import static com.atlassian.crowd.search.builder.Restriction.on;
import static com.atlassian.crowd.search.query.entity.EntityQuery.ALL_RESULTS;
import static com.atlassian.jira.plugin.connect.ConnectAddOnProperties.CONNECT_GROUP_NAME;
import static com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys.NAME;
import static com.atlassian.jira.user.ApplicationUsers.from;
import static com.atlassian.jira.util.JiraCollectionUtils.convertIterableToCollection;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

/**
 * Default implementation of GroupManager.
 *
 * @since v3.13
 */
public class DefaultGroupManager implements GroupManager {
    private static final Logger log = LoggerFactory.getLogger(DefaultGroupManager.class);

    private final UnfilteredCrowdService unfilteredCrowdService;
    private final CrowdService crowdService;
    private final DirectoryManager directoryManager;
    private final DbConnectionManager dbConnectionManager;
    private final DatabaseConfigurationManager dbConfigManager;
    private final FeatureManager featureManager;

    public DefaultGroupManager(final CrowdService crowdService, final DirectoryManager directoryManager,
            final DbConnectionManager dbConnectionManager, DatabaseConfigurationManager dbConfigManager,
            final UnfilteredCrowdService unfilteredCrowdService, final FeatureManager featureManager) {
        this.crowdService = crowdService;
        this.directoryManager = directoryManager;
        this.dbConnectionManager = dbConnectionManager;
        this.dbConfigManager = dbConfigManager;
        this.unfilteredCrowdService = unfilteredCrowdService;
        this.featureManager = featureManager;
    }

    @Override
    public Collection<Group> getAllGroups() {
        SearchRestriction restriction = NullRestrictionImpl.INSTANCE;
        final com.atlassian.crowd.embedded.api.Query<Group> query = new GroupQuery<>(Group.class, GROUP, restriction, 0, -1);
        return convertIterableToCollection(crowdService.search(query));
    }

    public boolean groupExists(final String groupName) {
        return getGroup(groupName) != null;
    }

    @Override
    public boolean groupExists(@Nonnull final Group group) {
        notNull("group", group);

        return groupExists(group.getName());
    }

    public Group createGroup(final String groupName) throws OperationNotPermittedException, InvalidGroupException {
        return crowdService.addGroup(new ImmutableGroup(groupName));
    }

    public Group getGroup(final String groupName) {
        return crowdService.getGroup(groupName);
    }

    @Override
    public Group getGroupEvenWhenUnknown(final String groupName) {
        if (groupExists(groupName)) {
            return getGroup(groupName);
        } else {
            return new ImmutableGroup(groupName);
        }
    }

    @Override
    public Group getGroupObject(String groupName) {
        return getGroup(groupName);
    }

    @Override
    public boolean isUserInGroup(@Nullable final String username, @Nullable final String groupname) {
        return username != null && groupname != null && crowdService.isUserMemberOfGroup(username, groupname);
    }

    @Override
    public boolean isUserInGroup(@Nullable final ApplicationUser user, @Nullable final Group group) {
        return group != null && isUserInGroup(user, group.getName());
    }

    @Override
    public boolean isUserInGroup(@Nullable final ApplicationUser user, @Nullable final String groupname) {
        return user != null && groupname != null && isUserInGroup(user.getDirectoryId(), user.getUsername(), groupname);
    }

    // JDEV-30356: Performance improvement.
    // We can talk directly to the embedded crowd DirectoryManager in order to avoid looking up the user by username
    private boolean isUserInGroup(final long directoryId, @Nonnull final String username,
                                  @Nonnull final String groupname) {
        try {
            return directoryManager.isUserNestedGroupMember(directoryId, username, groupname);
        } catch (final OperationFailedException ex) {
            // rethrow as runtime version
            throw new com.atlassian.crowd.exception.runtime.OperationFailedException(ex);
        } catch (DirectoryNotFoundException e) {
            // directory not found - very unlucky
            throw new ConcurrentModificationException("Directory mapping was removed while determining if user is a nested group member: " + e.getMessage());
        }
    }

    public Collection<ApplicationUser> getUsersInGroup(final String groupName) {
        Iterable<User> usersIterable = getAllUsersInGroup(groupName);
        return from(convertIterableToCollection(usersIterable));
    }

    public Collection<ApplicationUser> getUsersInGroup(final String groupName, final Boolean includeInactive) {
        Iterable<User> usersIterable = getAllUsersInGroup(groupName);
        List<ApplicationUser> applicationUsers = from(usersIterable);
        if (applicationUsers == null) {
            return null;
        }
        return applicationUsers.stream()
                .filter(u -> includeInactive || u.isActive())
                .collect(toList());
    }

    private Iterable<User> getAllUsersInGroup(final String groupName) {
        return crowdService.search(
                QueryBuilder.queryFor(User.class, user()).childrenOf(group()).withName(groupName).returningAtMost(ALL_RESULTS));
    }

    public Page<ApplicationUser> getUsersInGroup(final String groupName, final Boolean includeInactive, final PageRequest pageRequest) {
        final Iterable<User> allUsersInGroup = getAllUsersInGroup(groupName);

        return Pages.toPage(allUsersInGroup,
                pageRequest,
                u -> includeInactive || u.isActive(),
                ApplicationUsers::from);
    }

    public Collection<ApplicationUser> getUsersInGroup(final Group group) {
        return getUsersInGroup(group.getName());
    }

    @Override
    public int getUsersInGroupCount(final Group group) {
        return getUsersInGroupCount(group.getName());
    }

    @Override
    public int getUsersInGroupCount(final String groupName) {
        final List<Long> activeDirectoryIds = getActiveDirectoryIds();

        int totalCount = 0;
        int index = 0;
        for (Long directoryId : activeDirectoryIds) {
            Collection<String> expandedGroupNames = expandGroupNames(groupName, directoryId);
            if (expandedGroupNames.isEmpty()) {
                // Group does not exist in directory, nothing to check
                index++;
                continue;
            }

            List<Long> higherPriorityDirectoryIds = activeDirectoryIds.subList(0, index++);

            final QueryCallback<Long> countUsersQuery = new CountUsersInGroupQueryCallback(directoryId,
                    higherPriorityDirectoryIds, expandedGroupNames, dbConfigManager.getDatabaseConfiguration());
            long userCount = dbConnectionManager.executeQuery(countUsersQuery);
            totalCount += userCount;
        }

        return totalCount;
    }

    @Override
    public List<String> getNamesOfDirectMembersOfGroups(final Collection<String> groupNames, int limit) {
        if (groupNames.isEmpty()) {
            return Collections.emptyList();
        }

        final List<Long> activeDirectoryIds = getActiveDirectoryIds();
        List<String> allUserNames = new ArrayList<>();

        int index = 0;
        for (Long directoryId : activeDirectoryIds) {
            List<String> groupsInDirectory = getGroupsInDirectory(groupNames, directoryId);
            if (groupsInDirectory.isEmpty()) {
                index++;
                continue;
            }

            List<Long> higherPriorityDirectoryIds = activeDirectoryIds.subList(0, index++);

            final GetUsersInGroupQueryCallback getUsersQuery = new GetUsersInGroupQueryCallback(directoryId,
                    higherPriorityDirectoryIds, groupsInDirectory, limit, dbConfigManager.getDatabaseConfiguration());
            List<String> userNamesInDirectory = dbConnectionManager.executeQuery(getUsersQuery);
            allUserNames.addAll(userNamesInDirectory);
        }

        //Sort by lower user name - non-user-locale specific
        Collections.sort(allUserNames);

        if (allUserNames.size() > limit) {
            allUserNames = allUserNames.subList(0, limit);
        }

        return allUserNames;
    }

    @Override
    public Collection<String> filterUsersInAllGroupsDirect(Collection<String> userNames, Collection<String> groupNames) {
        if (userNames.isEmpty() || groupNames.isEmpty()) {
            return Collections.emptyList();
        }

        final List<Long> activeDirectoryIds = getActiveDirectoryIds();
        Multimap<String, String> userToGroupMap = HashMultimap.create();

        int index = 0;
        for (Long directoryId : activeDirectoryIds) {
            List<String> groupsInDirectory = getGroupsInDirectory(groupNames, directoryId);
            if (groupsInDirectory.isEmpty()) {
                index++;
                continue;
            }

            List<Long> higherPriorityDirectoryIds = activeDirectoryIds.subList(0, index++);

            final FilterUsersInAllGroupsQueryCallback getUsersQuery = new FilterUsersInAllGroupsQueryCallback(directoryId,
                    higherPriorityDirectoryIds, userNames, groupsInDirectory, dbConfigManager.getDatabaseConfiguration());
            List<FilterUsersInAllGroupsQueryCallback.Result> results = dbConnectionManager.executeQuery(getUsersQuery);

            for (FilterUsersInAllGroupsQueryCallback.Result result : results) {
                userToGroupMap.put(toLowerCase(result.getUserName()), toLowerCase(result.getGroupName()));
            }
        }

        //Only return users that have all groups, we can cheat by just comparing size
        return userNames.stream()
                .filter(userName -> userToGroupMap.get(toLowerCase(userName)).size() == groupNames.size())
                .collect(toSet());
    }

    private List<Long> getActiveDirectoryIds() {
        // The directories are already provided in priority order.
        return directoryManager.findAllDirectories().stream()
                .filter(Directory::isActive)
                .map(Directory::getId)
                .collect(toList());
    }

    private List<String> getGroupsInDirectory(final Collection<String> groupNames, final long directoryId) {
        Collection<SearchRestriction> groupNameRestrictions = groupNames.stream()
                .map(groupName -> on(NAME).exactlyMatching(groupName))
                .collect(toList());
        final EntityQuery<String> query =
                QueryBuilder.queryFor(String.class, group())
                        .with(anyOf(groupNameRestrictions))
                        .returningAtMost(ALL_RESULTS);
        try {
            return directoryManager.searchGroups(directoryId, query);
        } catch (DirectoryNotFoundException | OperationFailedException e) {
            // Directory is gone...
            log.debug("Directory " + directoryId + " may have been removed while testing group existence", e);
        }
        return Collections.emptyList();
    }

    /**
     * Expand all nested groups into a flat list.
     * Nested groups are observed per directory.
     *
     * @param groupName   the group to flatten
     * @param directoryId the directory ID
     * @return the flat list of nested groups, including the original groupName iff it exists in the target directory
     */
    private Collection<String> expandGroupNames(final String groupName, final long directoryId) {
        List<String> expandedGroups = getGroupsInDirectory(Collections.singletonList(groupName), directoryId);
        if (expandedGroups.isEmpty() || !nestedGroupsEnabled(directoryId)) {
            return expandedGroups;
        }

        final MembershipQuery<String> membershipQuery =
                QueryBuilder.queryFor(String.class, group())
                        .childrenOf(group())
                        .withName(groupName)
                        .returningAtMost(ALL_RESULTS);
        try {
            expandedGroups.addAll(directoryManager.searchNestedGroupRelationships(directoryId, membershipQuery));
        } catch (DirectoryNotFoundException | OperationFailedException e) {
            // Directory is gone...
            log.debug("Directory " + directoryId + " may have been removed while expanding group names", e);
        }

        return expandedGroups;
    }

    /**
     * @param directoryId the directory ID
     * @return true if the directory supports nested groups
     */
    private boolean nestedGroupsEnabled(long directoryId) {
        try {
            return directoryManager.supportsNestedGroups(directoryId);
        } catch (DirectoryInstantiationException | DirectoryNotFoundException e) {
            // directory is gone, ignore it
            log.debug("Directory " + directoryId + " was missing", e);
            return false;
        }
    }

    public Collection<String> getUserNamesInGroup(final Group group) {
        return getUserNamesInGroup(group.getName());
    }

    @Override
    public Collection<String> getUserNamesInGroups(final Collection<Group> groups) {
        Collection<String> users = Lists.newArrayList();
        for (Group group : groups) {
            users.addAll(getUserNamesInGroup(group));
        }
        return ImmutableSet.copyOf(users);
    }

    public Collection<String> getUserNamesInGroup(final String groupName) {
        Iterable<String> usersIterable = crowdService.search(
                QueryBuilder.queryFor(String.class, user()).childrenOf(group()).withName(groupName).returningAtMost(ALL_RESULTS));
        return convertIterableToCollection(usersIterable);
    }

    public Collection<ApplicationUser> getDirectUsersInGroup(final Group group) {
        Collection<ApplicationUser> usersInGroup = getUsersInGroup(group.getName());
        // Remove any indirect members
        for (Iterator<ApplicationUser> iter = usersInGroup.iterator(); iter.hasNext(); ) {
            if (!crowdService.isUserDirectGroupMember(iter.next().getDirectoryUser(), group)) {
                iter.remove();
            }
        }
        return usersInGroup;
    }

    public Collection<Group> getGroupsForUser(final String userName) {
        Iterable<Group> searchResults = crowdService.search(
                QueryBuilder.queryFor(Group.class, group()).parentsOf(user()).withName(userName).returningAtMost(ALL_RESULTS));
        return convertIterableToCollection(searchResults);
    }

    public Collection<Group> getGroupsForUser(@Nonnull final ApplicationUser user) {
        return getGroupsForUser(user.getName());
    }

    public Collection<String> getGroupNamesForUser(final String userName) {
        Iterable<String> usersIterable = crowdService.search(
                QueryBuilder.queryFor(String.class, group()).parentsOf(user()).withName(userName).returningAtMost(ALL_RESULTS));
        return convertIterableToCollection(usersIterable);
    }

    @Override
    public Collection<String> getGroupNamesForUser(@Nonnull final ApplicationUser user) {
        return getGroupNamesForUser(user.getName());
    }

    public void addUserToGroup(ApplicationUser user, Group group)
            throws GroupNotFoundException, UserNotFoundException, OperationNotPermittedException, OperationFailedException {
        crowdService.addUserToGroup(user.getDirectoryUser(), group);
    }

    private Set<ApplicationUser> convertUsersToApplicationUsers(Iterable<User> users) {
        final ImmutableSet.Builder<ApplicationUser> appUsers = ImmutableSet.builder();
        for (User user : users) {
            appUsers.add(from(user));
        }
        return appUsers.build();
    }

    @Override
    @Deprecated
    public Set<ApplicationUser> getConnectUsers() {
        return ImmutableSet.of();
    }
}
