package com.atlassian.jira.instrumentation.jdbc;


import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.stream.Collectors;

public final class StackTraceHelper {
    private static final List<String> PACKAGES_TO_INCLUDE = ImmutableList.of(
            "com.atlassian"
    );

    private StackTraceHelper() {
    }

    /**
     * Filters out irrelevant elements from stack trace.
     *
     * @param stackTrace to filter
     * @return list of stack trace elements
     */
    public static List<StackTraceElement> filterStackTrace(List<StackTraceElement> stackTrace) {
        return stackTrace.stream()
                .filter(element -> PACKAGES_TO_INCLUDE.stream().anyMatch(pkg -> element.getClassName().startsWith(pkg)))
                .collect(Collectors.toList());
    }
}
