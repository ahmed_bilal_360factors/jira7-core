package com.atlassian.jira.upgrade;

import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.model.querydsl.UpgradeVersionHistoryDTO;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.jira.model.querydsl.QUpgradeHistory.UPGRADE_HISTORY;
import static com.atlassian.jira.model.querydsl.QUpgradeVersionHistory.UPGRADE_VERSION_HISTORY;

public class UpgradeVersionHistoryManagerImpl implements UpgradeVersionHistoryManager {
    private static final Logger log = LoggerFactory.getLogger(UpgradeVersionHistoryManagerImpl.class);

    private static final Pattern BUILD_NUMBER_PATTERN = Pattern.compile("\\d+$");

    private final BuildVersionRegistry buildVersionRegistry;
    private final QueryDslAccessor queryDslAccessor;

    @SuppressWarnings("WeakerAccess")
    public UpgradeVersionHistoryManagerImpl(
            final BuildVersionRegistry buildVersionRegistry,
            final QueryDslAccessor queryDslAccessor
    ) {
        this.buildVersionRegistry = buildVersionRegistry;
        this.queryDslAccessor = queryDslAccessor;
    }

    @Override
    public List<UpgradeVersionHistoryItem> getAllUpgradeVersionHistory() {
        final List<UpgradeVersionHistoryItem> upgradeHistory = Lists.newArrayList();
        String previousVersion = null;
        String previousBuildNumber = null;

        final Optional<UpgradeVersionHistoryItem> latestUpgradeTaskWithNoBuildNumber = getLatestUpgradeTaskWithNoBuildNumber();
        if (latestUpgradeTaskWithNoBuildNumber.isPresent()) {
            upgradeHistory.add(latestUpgradeTaskWithNoBuildNumber.get());
            previousVersion = latestUpgradeTaskWithNoBuildNumber.get().getTargetVersion();
            previousBuildNumber = latestUpgradeTaskWithNoBuildNumber.get().getTargetBuildNumber();
        }

        final List<UpgradeVersionHistoryDTO> versionHistory = queryDslAccessor.executeQuery(connection -> connection.newSqlQuery()
                .select(UPGRADE_VERSION_HISTORY)
                .from(UPGRADE_VERSION_HISTORY)
                .orderBy(UPGRADE_VERSION_HISTORY.timeperformed.asc())
                .fetch());

        for (UpgradeVersionHistoryDTO upgradeVersionHistory : versionHistory) {
            upgradeHistory.add(
                    new UpgradeVersionHistoryItemImpl(
                            upgradeVersionHistory.getTimeperformed(),
                            upgradeVersionHistory.getTargetbuild(),
                            upgradeVersionHistory.getTargetversion(),
                            previousBuildNumber,
                            previousVersion
                    )
            );
            previousVersion = upgradeVersionHistory.getTargetversion();
            previousBuildNumber = upgradeVersionHistory.getTargetbuild();
        }

        Collections.sort(upgradeHistory, (o1, o2) -> {
            if (o1.getTimePerformed() != null && o2.getTimePerformed() != null) {
                return o2.getTimePerformed().compareTo(o1.getTimePerformed());
            } else if (o1.getTimePerformed() == null) {
                return 1;
            } else {
                return -1;
            }
        });

        return upgradeHistory;
    }

    @Override
    public void addUpgradeVersionHistory(final int buildNumber, final String version) {
        queryDslAccessor.execute(connection -> {
            final boolean alreadyContainsBuild = (connection.newSqlQuery()
                    .select(UPGRADE_VERSION_HISTORY)
                    .from(UPGRADE_VERSION_HISTORY)
                    .where(UPGRADE_VERSION_HISTORY.targetbuild.eq(String.valueOf(buildNumber)))
                    .fetchCount() > 0);

            if (alreadyContainsBuild) {
                log.debug("A record already exists for build number '{}' - skipping creation.", buildNumber);
                return;
            }

            final Timestamp timePerformed = new Timestamp(System.currentTimeMillis());
            connection
                    .insert(UPGRADE_VERSION_HISTORY)
                    .set(UPGRADE_VERSION_HISTORY.timeperformed, timePerformed)
                    .set(UPGRADE_VERSION_HISTORY.targetversion, version)
                    .set(UPGRADE_VERSION_HISTORY.targetbuild, String.valueOf(buildNumber))
                    .executeWithId();
        });
    }

    private Optional<UpgradeVersionHistoryItem> getLatestUpgradeTaskWithNoBuildNumber() {
        final Optional<Integer> latestUpgradeTaskWithNoTargetBuild = queryDslAccessor.executeQuery(connection -> connection.newSqlQuery()
                .select(UPGRADE_HISTORY.upgradeclass)
                .from(UPGRADE_HISTORY)
                .where(UPGRADE_HISTORY.targetbuild.isEmpty().or(UPGRADE_HISTORY.targetbuild.isNull()))
                .fetch()
                .stream()
                .map(this::extractBuildNumberFromUpgradeClass)
                .filter(Objects::nonNull)
                .max(Integer::compare));

        if (latestUpgradeTaskWithNoTargetBuild.isPresent()) {
            final Integer buildNumber = latestUpgradeTaskWithNoTargetBuild.get();

            final BuildVersionRegistry.BuildVersion targetVersion = buildVersionRegistry.getVersionForBuildNumber(
                    buildNumber);
            return Optional.of(new UpgradeVersionHistoryItemImpl(null,
                    Integer.toString(targetVersion.getBuildNumberAsInteger()), targetVersion.getVersion(),
                    Integer.toString(buildNumber), null, true
            ));
        } else {
            return Optional.empty();
        }
    }

    private Integer extractBuildNumberFromUpgradeClass(@Nonnull final String upgradeClassName) {
        final Matcher matcher = BUILD_NUMBER_PATTERN.matcher(upgradeClassName);
        if (matcher.find()) {
            // Pattern validates that it will parse
            return Integer.valueOf(matcher.group(0));
        }
        return null;
    }
}
