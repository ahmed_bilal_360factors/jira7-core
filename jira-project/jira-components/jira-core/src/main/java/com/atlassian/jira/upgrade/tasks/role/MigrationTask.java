package com.atlassian.jira.upgrade.tasks.role;

/**
 * Step in the renaissance migration (i.e. JIRA 6.x single use permission to JIRA 7.x multiple application).
 *
 * @since v7.0
 */
abstract class MigrationTask {
    static final boolean EVENT_SHOWS_IN_CLOUD_LOG = true;

    /**
     * Migrate the passed state. This method must be:
     * <ol>
     * <li>Side Affect free (to JIRA's model and internal state): All changes need to be done on the passed
     * migration state. Side affects can be added to the {@link MigrationState#afterSaveTasks} if required. Logging and
     * other side affects that don't change JIRA's model or internal state are fine.</li>
     * <li>Idempotent: The task must perform correctly if the passed state has already been migrated by this task
     * (e.g. simply return the passed state unchanged if it detects all necessary changes have been made).</li>
     * </ol>
     * <p>
     * Changes made to the state will not be written to the database until they are validated.</p>
     *
     * @param state                 the state to migrate.
     * @param licenseSuppliedByUser {@code true} if a license was supplied by the user during the import.
     * @return the state after the migration.
     */
    abstract MigrationState migrate(MigrationState state, boolean licenseSuppliedByUser);
}
