package com.atlassian.jira.bc.filter;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.subscription.FilterSubscription;
import com.atlassian.jira.issue.subscription.SubscriptionManager;
import com.atlassian.jira.scheduler.cron.CronValidator;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.component.cron.CronEditorBean;
import com.atlassian.jira.web.component.cron.generator.CronExpressionDescriptor;
import com.atlassian.jira.web.component.cron.parser.CronExpressionParser;
import com.atlassian.scheduler.SchedulerService;
import com.google.common.collect.ImmutableList;
import org.ofbiz.core.entity.GenericEntityException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Date;

/**
 * Uses Atlassian Scheduler
 */
public class DefaultFilterSubscriptionService implements FilterSubscriptionService {
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final SubscriptionManager subscriptionManager;
    private final I18nHelper i18nHelper;
    private SchedulerService schedulerService;

    public DefaultFilterSubscriptionService(JiraAuthenticationContext jiraAuthenticationContext,
                                            SubscriptionManager subscriptionManager, SchedulerService schedulerService) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.subscriptionManager = subscriptionManager;
        this.schedulerService = schedulerService;
        this.i18nHelper = jiraAuthenticationContext.getI18nHelper();
    }

    @Override
    public void validateCronExpression(JiraServiceContext context, String expr) {
        validateCron(context, expr);
    }

    /**
     * Validate the supplied cron expression.
     * Errors are passed back via the context.
     *
     * @param context jira service context
     * @param expr    the Cron Expression to validate and turn into trigger
     */
    private void validateCron(JiraServiceContext context, String expr) {
        final Option<String> cronError = new CronValidator(i18nHelper, schedulerService).validateCron(expr);
        if (cronError.isDefined()) {
            context.getErrorCollection().addErrorMessage(cronError.get());
        }
    }

    @Override
    public void storeSubscription(JiraServiceContext context, Long filterId, String groupName, String expr, boolean emailOnEmpty) {
        validateCronExpression(context, expr);
        if (expr != null && !context.getErrorCollection().hasAnyErrors()) {
            subscriptionManager.createSubscription(context.getLoggedInApplicationUser(), filterId, groupName, expr, emailOnEmpty);
        }
    }

    @Override
    public void updateSubscription(JiraServiceContext context, Long subscriptionId, String groupName, String expr, boolean emailOnEmpty) {
        validateCronExpression(context, expr);
        if (expr != null && !context.getErrorCollection().hasAnyErrors()) {
            subscriptionManager.updateSubscription(context.getLoggedInApplicationUser(), subscriptionId, groupName, expr, emailOnEmpty);
        }
    }

    @Override
    public String getPrettySchedule(JiraServiceContext context, String cronExpression) {
        CronExpressionParser cronExpresionParser = new CronExpressionParser(cronExpression);
        if (cronExpresionParser.isValidForEditor()) {
            CronEditorBean cronEditorBean = cronExpresionParser.getCronEditorBean();
            return new CronExpressionDescriptor(jiraAuthenticationContext.getI18nHelper()).getPrettySchedule(cronEditorBean);
        } else {
            return cronExpression;
        }
    }

    @Override
    public String getCronExpression(JiraServiceContext context, FilterSubscription subscription) {
        return subscriptionManager.getCronExpressionForSubscription(subscription);
    }

    @Override
    public Collection<FilterSubscription> getVisibleFilterSubscriptions(final ApplicationUser user, final SearchRequest filter) {
        // Anonymous users can't see any subscriptions
        if (filter == null || user == null) {
            return ImmutableList.of();
        }

        try {
            // The filter's owner can see all of its subscriptions
            if (user.equals(filter.getOwner())) {
                return subscriptionManager.getAllFilterSubscriptions(filter.getId());
            }

            // Other users can only see the subscriptions that they created
            return subscriptionManager.getFilterSubscriptions(user, filter.getId());
        } catch (GenericEntityException e) {
            throw new DataAccessException(e);
        }
    }

    @Nullable
    @Override
    public Date getNextSendTime(@Nonnull final FilterSubscription sub) {
        return subscriptionManager.getNextSendTime(sub);
    }

}
