package com.atlassian.jira.component;

import com.atlassian.jira.ComponentManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

/**
 * Worker class that insulates the API from the implementation dependencies in ManagerFactory etc.
 *
 * @since v4.3
 */
public class ComponentAccessorWorker implements ComponentAccessor.Worker {
    private static final Logger LOG = LoggerFactory.getLogger(ComponentAccessorWorker.class);

    @Override
    public <T> Optional<T> getComponentSafely(final Class<T> componentClass) {
        // if the components in the container have not been instantiated, we avoid trying to use them, because doing so
        // could cause a deadlock between different threads attempting to instantiate components.
        if (ComponentManager.getInstance().componentsAvailable()) {
            return Optional.ofNullable(getComponent(componentClass));
        }
        return Optional.empty();
    }

    @Override
    public <T> T getComponent(Class<T> componentClass) {
        try {
            return ComponentManager.getComponent(componentClass);
        } catch (RuntimeException re) {
            throw new IllegalStateException("Unable to resolve component: " + componentClass, re);
        }
    }

    @Override
    public <T> T getComponentOfType(Class<T> componentClass) {
        try {
            return ComponentManager.getComponentInstanceOfType(componentClass);
        } catch (RuntimeException re) {
            throw new IllegalStateException("Unable to resolve component: " + componentClass, re);
        }
    }

    @Override
    public <T> T getOSGiComponentInstanceOfType(Class<T> componentClass) {
        try {
            return ComponentManager.getOSGiComponentInstanceOfType(componentClass);
        } catch (RuntimeException re) {
            throw new IllegalStateException("Unable to resolve component: " + componentClass, re);
        }
    }
}