package com.atlassian.jira.instrumentation;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.InitializingComponent;
import com.atlassian.jira.config.FeatureFlag;
import com.atlassian.jira.config.FeatureFlagProvider;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.logging.log4j.layout.JsonLayout;
import com.atlassian.logging.log4j.layout.json.DefaultJsonDataProvider;
import com.atlassian.plugin.event.events.PluginFrameworkShuttingDownEvent;
import com.google.common.collect.ImmutableSet;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.apache.commons.collections.Buffer;
import org.apache.commons.collections.BufferUtils;
import org.apache.commons.collections.buffer.CircularFifoBuffer;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.log4j.spi.LoggingEvent;
import org.codehaus.jackson.map.ObjectMapper;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static java.lang.Thread.currentThread;
import static java.util.Objects.requireNonNull;
import static java.util.UUID.randomUUID;

/**
 * Logs the instrumentation data that we have collected to various collection mechanisms.
 *
 * @since v7.1
 */
@ParametersAreNonnullByDefault
public class DefaultInstrumentationLogger implements InstrumentationLogger, FeatureFlagProvider, InitializingComponent {

    protected static final String REQUEST_EXECUTION_TIME = "requestExecutionTime";
    protected static final String JIRA_INSTRUMENTATION_LAAS = "jira.instrumentation.laas";
    private static final String JIRA_INSTRUMENTATION_BUFFER_SIZE = "jira.instrumentation.cache.buffersize";

    private static final String INSTRUMENTATION = "instrumentation";

    private static final int DEFAULT_BUFFER_SIZE = 100;

    private static final Logger logger = Logger.getLogger(DefaultInstrumentationLogger.class);
    public static final String SPAN_ID = "spanId";
    public static final String PARENT_SPAN_ID = "parentSpanId";
    private static final String REQUEST_START_NANO_TIME = "startNanoTime";

    /**
     * {@link CircularFifoBuffer} to hold statistics. Order is not really critical, but
     * we don't want to lose any so we synchronise. Buffer size may be specified in the system properties. It defaults
     * to 100 requests.
     */
    private final Buffer instrumentationResults;

    /**
     * Fixed thread pool to execute the disposition of data
     */
    private final ExecutorService pool = Executors.newFixedThreadPool(1, new ThreadFactoryBuilder().setNameFormat("DefaultInstrumentationLogger-%d").build());

    private final FeatureFlag loggingToLaasFeature;
    private final FeatureManager featureManager;
    private final EventPublisher eventPublisher;

    public DefaultInstrumentationLogger(final JiraProperties jiraProperties, final FeatureManager featureManager, final EventPublisher eventPublisher) {
        this.instrumentationResults = BufferUtils.synchronizedBuffer(new CircularFifoBuffer(
                jiraProperties.getInteger(JIRA_INSTRUMENTATION_BUFFER_SIZE) == null
                        ? DEFAULT_BUFFER_SIZE
                        : jiraProperties.getInteger(JIRA_INSTRUMENTATION_BUFFER_SIZE)));
        this.featureManager = featureManager;

        boolean laasEnabled = jiraProperties.getBoolean(JIRA_INSTRUMENTATION_LAAS);
        this.loggingToLaasFeature = FeatureFlag.featureFlag("jira.instrumentation.laas").defaultedTo(laasEnabled);
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void afterInstantiation() throws Exception {
        eventPublisher.register(this);
    }

    @EventListener
    public void pluginFrameworkShuttingDown(final PluginFrameworkShuttingDownEvent pluginFrameworkShuttingDownEvent) {
        pool.shutdown();
    }

    @Override
    public void save(final String traceId, final String path, final List<? extends Statistics> requestStats, final Optional<Long> requestTime) {
        RequestData requestData = new RequestData.Builder()
                .setPath(path)
                .setTraceId(traceId)
                .build();
        save(requestData, requestStats, requestTime);
    }

    @Override
    public void save(RequestData requestData, final List<? extends Statistics> requestStats, Optional<Long> requestTime) {
        save(requestData, requestStats, requestTime, null);
    }

    @Override
    public void save(final RequestData requestData, List<? extends Statistics> requestStats, Optional<Long> requestTime,
                     @Nullable Exception exception) {
        // Make sure there are no nulls.
        requireNonNull(requestData, "requestData");
        requireNonNull(requestStats, "requestData");
        requireNonNull(requestTime, "requestTime");

        final String internalTraceId = requestData.getTraceId().orElse(randomUUID().toString());
        final String internalPath = requestData.getPath().orElse(currentThread().getName());

        // If we have no data, just return.
        if (requestStats.size() > 0) {
            // Keep the DB access out of the thread.
            final boolean enabled = this.featureManager.isEnabled(loggingToLaasFeature);

            CompletableFuture.runAsync(() -> {
                try {
                    // Log entries for data grouped by the logging key.
                    Map<String, List<Statistics>> entryMap = ((List<Statistics>) requestStats).stream()
                            .collect(Collectors.groupingBy(Statistics::getLoggingKey));

                    final String queryString = requestData.getQueryString().orElse(null);
                    final LogEntry entry = new LogEntry(internalTraceId, internalPath, queryString, entryMap);

                    // We always log the information to memory.
                    //noinspection unchecked
                    instrumentationResults.add(entry);

                    // Log entry to LaaS as a JSON object.
                    if (enabled) {
                        try {
                            String logging = getJsonString(requestData.getSpanId(), requestData.getParentSpanId(), entry,
                                    requestTime, requestData.getStartNanoTime());
                            if (exception != null) {
                                logger.info(logging, exception);
                            } else {
                                logger.info(logging);
                            }
                        } catch (RuntimeException e) {
                            logger.debug("failed to log to LaaS: ", e);
                        } finally {
                            // Clean up keys that may have been added to MDC.
                            cleanMdc();
                        }
                    }
                } catch (RuntimeException e) {
                    logger.debug("failed to log instrumentation", e);
                }
            }, pool);
        }
    }

    /**
     * Generates the JSON string object that we can log.
     *
     * @param spanId       The Zipkin spanId (if there is one, null otherwise
     * @param parentSpanId The Zipkin parent spanId (if there is on, null otherwise).
     * @param entry        The {@link LogEntry} data
     * @param requestTime  An {@link Optional} that may contain the total request time in nanoseconds if applicable.
     * @return The JSON object as a string.
     */
    protected String getJsonString(Optional<String> spanId, Optional<String> parentSpanId, LogEntry entry,
                                   Optional<Long> requestTime, Optional<Long> startNanoTime) {

        requireNonNull(entry);

        JsonLayout jsonLayout = new JsonLayout();
        jsonLayout.activateOptions();
        Jsonable json = writer -> new ObjectMapper().writeValue(writer, entry);
        MDC.put(DefaultJsonDataProvider.MdcKey.REQUEST_ID.getKey(), entry.getTraceId());

        MDC.put(REQUEST_EXECUTION_TIME, requestTime.orElse(-1L));
        startNanoTime.ifPresent(time -> MDC.put(REQUEST_START_NANO_TIME, time));
        spanId.ifPresent(span -> MDC.put(SPAN_ID, span));
        parentSpanId.ifPresent(parent -> MDC.put(PARENT_SPAN_ID, parent));

        MDC.put(INSTRUMENTATION, json);

        LoggingEvent event = new LoggingEvent(DefaultInstrumentationLogger.class.getCanonicalName(), logger, Level.INFO, "", null);
        return jsonLayout.format(event);
    }

    protected void cleanMdc() {
        MDC.remove(INSTRUMENTATION);
        MDC.remove(SPAN_ID);
        MDC.remove(PARENT_SPAN_ID);
        MDC.remove(REQUEST_EXECUTION_TIME);
        MDC.remove(DefaultJsonDataProvider.MdcKey.REQUEST_ID.getKey());
    }

    @SuppressWarnings("unchecked")
    public List<LogEntry> getLogEntriesFromBuffer() {
        // Return a copy of the buffer for processing. Using toArray() because this is thread safe.
        final LogEntry[] logEntries = (LogEntry[]) instrumentationResults.toArray(new LogEntry[instrumentationResults.size()]);
        // Get rid of any nulls if the array shrank after we got the size. (If the buffer is cleared this is possible.
        return Arrays.stream(logEntries).filter(Objects::nonNull).collect(Collectors.toList());
    }

    public void clearMemoryBuffer() {
        instrumentationResults.clear();
    }

    @Override
    public Set<FeatureFlag> getFeatureFlags() {
        return ImmutableSet.of(loggingToLaasFeature);
    }
}