package com.atlassian.jira.issue.managers;

import com.atlassian.cache.Supplier;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.events.PluginModuleAvailableEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleUnavailableEvent;
import com.atlassian.util.concurrent.ResettableLazyReference;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class CustomFieldSearcherManager {

    private final ResettableLazyReference<List<CustomFieldSearcher>> customFieldSearcherPluginsRef;

    public CustomFieldSearcherManager(final PluginAccessor pluginAccessor, final EventPublisher eventPublisher) {
        this.customFieldSearcherPluginsRef =
                fromSupplier(() -> pluginAccessor.getEnabledModulesByClass(CustomFieldSearcher.class));
        eventPublisher.register(this);
    }

    private static <T> ResettableLazyReference<T> fromSupplier(final Supplier<T> supplier) {
        return new ResettableLazyReference<T>() {
            @Override
            protected T create() throws Exception {
                return supplier.get();
            }
        };
    }

    public List<CustomFieldSearcher> getSearchersValidFor(final CustomFieldType customFieldType) {
        return customFieldSearcherPluginsRef.get()
                .stream()
                .filter(searcher -> isSearcherValidFor(searcher, customFieldType))
                .collect(toList());
    }

    private boolean isSearcherValidFor(final CustomFieldSearcher searcher, final CustomFieldType customFieldType) {
        final CustomFieldTypeModuleDescriptor customFieldTypeDescriptor = customFieldType.getDescriptor();

        // The searcher can name valid custom field types to use, or the custom field type can name valid searchers.
        return searcher.getDescriptor().getValidCustomFieldKeys().contains(customFieldType.getKey())
                || customFieldTypeDescriptor.getValidSearcherKeys().contains(searcher.getDescriptor().getCompleteKey());
    }

    @EventListener
    public void clearCacheOnPluginEnable(final PluginModuleEnabledEvent event) {
        customFieldSearcherPluginsRef.reset();
    }

    @EventListener
    public void clearCacheOnPluginDisable(final PluginModuleDisabledEvent event) {
        customFieldSearcherPluginsRef.reset();
    }

    @EventListener
    public void clearCacheOnPluginAvailable(final PluginModuleAvailableEvent event) {
        customFieldSearcherPluginsRef.reset();
    }

    @EventListener
    public void clearCacheOnPluginUnavailable(final PluginModuleUnavailableEvent event) {
        customFieldSearcherPluginsRef.reset();
    }
}
