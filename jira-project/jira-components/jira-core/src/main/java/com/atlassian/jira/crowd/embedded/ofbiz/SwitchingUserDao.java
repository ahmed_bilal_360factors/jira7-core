package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.cache.CacheManager;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.user.search.UserIndexer;
import com.atlassian.jira.cluster.ClusterMessagingService;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.crowd.embedded.lucene.CrowdQueryTranslator;
import com.atlassian.jira.crowd.embedded.ofbiz.db.OfBizTransactionManager;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.user.UserDeleteVeto;
import com.atlassian.jira.user.util.UserKeyStore;

/**
 * Switch between indexing and nonindexing {@link com.atlassian.crowd.embedded.spi.UserDao}.
 * The switch is made on construction time.
 * This is a smell, because constructors should be lightweight have no side effects.
 * Unfortunately, we cannot do it right, because of the conjunction of the following reasons:
 * <ul>
 *     <li>{@link OfBizUserDao} has a dirty constructor</li>
 *     <li>Pico implicitly publishes implementations under all supertypes of the explicit key</li>
 *     <li>Pico doesn't allow a single implementation under multiple explicit keys</li>
 * </ul>
 * <tt>bad code + unnecessary framework = more bad code = this</tt>
 */
public class SwitchingUserDao extends DelegatingUserDao {

    private final ExtendedUserDao delegate;

    public SwitchingUserDao(
            UserIndexer indexer,
            CrowdQueryTranslator translator,
            ClusterMessagingService clusterMessagingService,
            OfBizTransactionManager ofBizTransactionManager,
            int maxBatchSize,
            OfBizDelegator ofBizDelegator,
            DirectoryDao directoryDao,
            InternalMembershipDao membershipDao,
            UserKeyStore userKeyStore,
            UserDeleteVeto userDeleteVeto,
            CacheManager cacheManager,
            ClusterLockService clusterLockService,
            ApplicationProperties applicationProperties,
            DatabaseConfigurationManager databaseConfigurationManager,
            EventPublisher eventPublisher
    ) {
        ExtendedUserDao nonIndexedDao = new OfBizUserDao(
                ofBizDelegator,
                directoryDao,
                membershipDao,
                userKeyStore,
                userDeleteVeto,
                cacheManager,
                clusterLockService,
                applicationProperties,
                ofBizTransactionManager,
                databaseConfigurationManager
        );

        if (applicationProperties.getOption(APKeys.LUCENE_USER_SEARCHER)) {
            delegate = new IndexedUserDao(
                    nonIndexedDao,
                    indexer,
                    translator,
                    clusterMessagingService,
                    ofBizTransactionManager,
                    maxBatchSize
            );
            eventPublisher.register(delegate);
        } else {
            delegate = nonIndexedDao;
        }
    }

    @Override
    protected ExtendedUserDao delegate() {
        return delegate;
    }
}
