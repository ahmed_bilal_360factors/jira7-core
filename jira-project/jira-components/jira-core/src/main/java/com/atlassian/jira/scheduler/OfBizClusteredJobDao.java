package com.atlassian.jira.scheduler;

import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.entity.Delete;
import com.atlassian.jira.entity.Entity.Name;
import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.entity.Update;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.scheduler.caesium.spi.ClusteredJob;
import com.atlassian.scheduler.caesium.spi.ClusteredJobDao;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.entity.Entity.CLUSTERED_JOB;
import static com.atlassian.jira.scheduler.ClusteredJobFactory.JOB_ID;
import static com.atlassian.jira.scheduler.ClusteredJobFactory.JOB_RUNNER_KEY;
import static com.atlassian.jira.scheduler.ClusteredJobFactory.NEXT_RUN;
import static com.atlassian.jira.scheduler.ClusteredJobFactory.VERSION;

/**
 * @since v7.0
 */
@ParametersAreNonnullByDefault
public class OfBizClusteredJobDao implements ClusteredJobDao {
    private static final Logger LOG = LoggerFactory.getLogger(OfBizClusteredJobDao.class);

    private final EntityEngine entityEngine;
    private final OracleClusteredJobParametersTypeFixer oracleTypeFixer;

    public OfBizClusteredJobDao(EntityEngine entityEngine, DatabaseConfigurationManager dbConfigManager) {
        this.entityEngine = entityEngine;
        this.oracleTypeFixer = new OracleClusteredJobParametersTypeFixer(dbConfigManager);
    }

    @Nullable
    @Override
    public Date getNextRunTime(final JobId jobId) {
        final GenericValue gv = Select.columns(NEXT_RUN)
                .from(Name.CLUSTERED_JOB)
                .whereEqual(JOB_ID, jobId.toString())
                .runWith(entityEngine)
                .singleValue();
        if (gv != null) {
            final Long value = gv.getLong(NEXT_RUN);
            if (value != null) {
                return new Date(value);
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Long getVersion(final JobId jobId) {
        final GenericValue gv = Select.columns(VERSION)
                .from(Name.CLUSTERED_JOB)
                .whereEqual(JOB_ID, jobId.toString())
                .runWith(entityEngine)
                .singleValue();
        return (gv != null) ? gv.getLong(VERSION) : null;
    }

    @Nullable
    @Override
    public ClusteredJob find(final JobId jobId) {
        oracleTypeFixer.fix();
        return Select.from(CLUSTERED_JOB)
                .whereEqual(JOB_ID, jobId.toString())
                .runWith(entityEngine)
                .singleValue();
    }

    @Nonnull
    @Override
    public Collection<ClusteredJob> findByJobRunnerKey(final JobRunnerKey jobRunnerKey) {
        oracleTypeFixer.fix();
        final List<OfBizClusteredJob> list = Select.from(CLUSTERED_JOB)
                .whereEqual(JOB_RUNNER_KEY, jobRunnerKey.toString())
                .runWith(entityEngine)
                .asList();
        return ImmutableList.copyOf(list);
    }

    @Nonnull
    @Override
    public Map<JobId, Date> refresh() {
        final ImmutableMap.Builder<JobId, Date> jobs = ImmutableMap.builder();
        Select.columns(JOB_ID, NEXT_RUN)
                .from(Name.CLUSTERED_JOB)
                .whereCondition(new EntityExpr(NEXT_RUN, EntityOperator.NOT_EQUAL, null))
                .runWith(entityEngine)
                .visitWith(gv -> jobs.put(JobId.of(gv.getString(JOB_ID)), new Date(gv.getLong(NEXT_RUN))));
        return jobs.build();
    }

    @Nonnull
    @Override
    public Set<JobRunnerKey> findAllJobRunnerKeys() {
        final ImmutableSet.Builder<JobRunnerKey> keys = ImmutableSet.builder();
        Select.distinctString(JOB_RUNNER_KEY)
                .from(Name.CLUSTERED_JOB)
                .runWith(entityEngine)
                .visitWith(jobRunnerKey -> keys.add(JobRunnerKey.of(jobRunnerKey)));
        return keys.build();
    }

    @Override
    public boolean create(final ClusteredJob clusteredJob) {
        try {
            entityEngine.createValue(CLUSTERED_JOB, new OfBizClusteredJob(null, clusteredJob));
            return true;
        } catch (DataAccessException dae) {
            LOG.debug("Mid-air collision creating a clustered job (probably)", dae);
            return false;
        }
    }

    @Override
    public boolean updateNextRunTime(final JobId jobId, @Nullable final Date date, final long version) {
        final Long nextRun = (date != null) ? date.getTime() : null;
        final int rows = Update.into(Name.CLUSTERED_JOB)
                .set(NEXT_RUN, nextRun)
                .set(VERSION, version + 1L)
                .whereEqual(JOB_ID, jobId.toString())
                .andEqual(VERSION, version)
                .execute(entityEngine);
        return matchedOneRow(rows, () -> LOG.error("Multiple rows matched jobId={}; version={}", jobId, version));
    }

    @Override
    public boolean delete(final JobId jobId) {
        final int rows = Delete.from(Name.CLUSTERED_JOB)
                .whereEqual(JOB_ID, jobId.toString())
                .execute(entityEngine);
        return matchedOneRow(rows, () -> LOG.error("Multiple rows matched jobId={}", jobId));
    }

    private static boolean matchedOneRow(int rows, Runnable onMultipleRows) {
        if (rows > 1) {
            onMultipleRows.run();
        }
        return rows > 0;
    }
}
