package com.atlassian.jira.license;

import com.atlassian.jira.user.ApplicationUser;

/**
 * Implements the license check required for users to be able to create issues.
 *
 * @since 7.0
 */
public interface CreateIssueLicenseCheck extends LicenseCheck {
    /**
     * Perform this license check using the given {@link com.atlassian.jira.user.ApplicationUser}.
     *
     * @param user the user this license check should be performed as
     * @return the result of the check
     */
    Result evaluateWithUser(ApplicationUser user);
}
