package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.fugue.Option;
import com.atlassian.jira.CachingComponent;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;

import java.util.Set;

import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static org.apache.commons.lang3.StringUtils.stripToNull;

/**
 * @since v7.0
 */
public final class LicenseDaoImpl extends LicenseDao {
    private static final String ENTITY_NAME = "ProductLicense";
    private static final String COL_LICENSE = "license";

    private final ApplicationProperties properties;
    private final CachingComponent licenseCache;
    private final OfBizDelegator db;

    public LicenseDaoImpl(final ApplicationProperties properties,
                          final OfBizDelegator db,
                          final JiraLicenseManager licenseCache) {
        notNull("licenseCache", licenseCache);
        this.properties = notNull("properties", properties);
        this.db = notNull("db", db);

        if (licenseCache instanceof CachingComponent) {
            this.licenseCache = (CachingComponent) licenseCache;
        } else {
            throw new IllegalArgumentException("'licenseCache' is not a cache.");
        }
    }

    @Override
    Option<License> get6xLicense() {
        try {
            return Option.option(stripToNull(properties.getText(APKeys.JIRA_LICENSE))).map(License::new);
        } catch (MigrationFailedException e) {
            throw new MigrationFailedException("JIRA 6.x license stored in JIRA is not valid: " + e.getMessage(), e);
        }
    }

    @Override
    Licenses getLicenses() {
        try {
            final Iterable<License> licenses = db.findAll(ENTITY_NAME)
                    .stream()
                    .map(gv -> gv.getString(COL_LICENSE))
                    .map(License::new)
                    .collect(CollectorsUtil.toImmutableSet());
            return new Licenses(licenses);
        } catch (MigrationFailedException e) {
            throw new MigrationFailedException("Licenses stored in JIRA are not valid: " + e.getMessage(), e);
        }
    }

    @Override
    void setLicenses(Licenses licenses) {
        final Set<License> newLicenses = notNull("licenses", licenses).get();
        final Set<License> currentLicenses = getLicenses().get();
        if (!newLicenses.containsAll(currentLicenses)) {
            throw new MigrationFailedException("Unable to remove licenses from the store.");
        }

        for (License licenseToAdd : Sets.difference(newLicenses, currentLicenses)) {
            db.createValue(ENTITY_NAME, ImmutableMap.of(COL_LICENSE, licenseToAdd.licenseString()));
        }

        licenseCache.clearCache();
    }

    @Override
    void remove6xLicense() {
        properties.setText(APKeys.JIRA_LICENSE, null);
    }
}
