package com.atlassian.jira.cache;

import com.atlassian.cache.CacheFactory;
import com.atlassian.fugue.Pair;
import com.atlassian.jira.cache.monitor.CacheStatisticsMonitor;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.vcache.ChangeRate;
import com.atlassian.vcache.ExternalCacheSettings;
import com.atlassian.vcache.ExternalCacheSettingsBuilder;
import com.atlassian.vcache.JvmCacheSettings;
import com.atlassian.vcache.JvmCacheSettingsBuilder;
import com.atlassian.vcache.internal.RequestContext;
import com.atlassian.vcache.internal.VCacheCreationHandler;
import com.atlassian.vcache.internal.VCacheSettingsDefaultsProvider;
import com.atlassian.vcache.internal.core.DefaultVCacheCreationHandler;
import com.atlassian.vcache.internal.core.PlainExternalCacheKeyGenerator;
import com.atlassian.vcache.internal.core.Sha1ExternalCacheKeyGenerator;
import com.atlassian.vcache.internal.core.metrics.DefaultMetricsCollector;
import com.atlassian.vcache.internal.core.metrics.MetricsCollector;
import com.atlassian.vcache.internal.core.metrics.NoopMetricsCollector;
import com.atlassian.vcache.internal.core.service.AbstractVCacheService;
import com.atlassian.vcache.internal.legacy.LegacyServiceSettingsBuilder;
import com.atlassian.vcache.internal.legacy.LegacyVCacheService;
import com.atlassian.vcache.internal.memcached.MemcachedVCacheService;
import com.atlassian.vcache.internal.memcached.MemcachedVCacheServiceSettingsBuilder;
import com.google.common.base.Suppliers;
import net.spy.memcached.MemcachedClient;
import net.spy.memcached.MemcachedClientIF;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.time.Duration;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Creates and configures implementation of {@link AbstractVCacheService} based on configuration properties. Currently
 * the following properties are supported:
 * <ul>
 * <li>com.atlassian.vcache.max_ttl_secs - maximum ttl in seconds. Defaults to 24 hours if not specified.</li>
 * <li>com.atlassian.vcache.max_entries - maximum number of entries that can be stored in the cache. Defaults to
 * {@link Integer#MAX_VALUE} if not specified.</li>
 * <li> com.atlassian.vcache.force_vcache_backend - if defined it forces creation of a given vcache backed. Supported
 * values are: legacy, redis (not implemented yet), memcached.</li>
 * <li>com.atlassian.vcache.memcached.host - memcached host</li>
 * <li>com.atlassian.vcache.memcached.port - memcached port</li>
 * <li>com.atlassian.vcache.warn_no_context - if defined warning will be logged every time VCache is accessed from
 * uninitialised context</li>
 * <li>{@link com.atlassian.jira.cache.monitor.CacheStatisticsMonitor#JIRA_CACHE_INSTRUMENTATION} - if defined vcache
 * statistics will be collected.</li>
 * </ul>
 *
 * @since v7.1
 */
public class JiraVCacheServiceCreator {
    private static final Logger log = LoggerFactory.getLogger(JiraVCacheServiceCreator.class);

    private static final String VCACHE_PREFIX = "com.atlassian.vcache.";
    static final String PROPERTY_MAX_TTL_IN_SEC = VCACHE_PREFIX + "max_ttl_secs";
    static final String PROPERTY_MAX_ENTRIES = VCACHE_PREFIX + "max_entries";
    static final String PROPERTY_FORCE_VCACHE_TYPE = VCACHE_PREFIX + "force_vcache_backend";
    static final String PROPERTY_MEMCACHED_HOST = VCACHE_PREFIX + "memchached.host";
    static final String PROPERTY_MEMCACHED_PORT = VCACHE_PREFIX + "memchached.port";
    private static final int DEFAULT_MEMCACHED_PORT = 11211;
    private static final String PROPERTY_WARN_NO_CONTEXT = VCACHE_PREFIX + "warn_no_context";

    private static final int DEFAULT_MAX_ENTRIES = Integer.MAX_VALUE;
    private static final long DEFAULT_MAX_TTL = Duration.ofHours(24).getSeconds();
    private static final Duration DEFAULT_TTL_EXTERNAL_CACHE = Duration.ofHours(1);
    private static final Duration DEFAULT_TTL_JVM_CACHE = Duration.ofHours(24);
    private static final String PRODUCT_IDENTIFIER = "JIRA";

    private final JiraProperties jiraProperties;
    private final Optional<CacheFactory> cacheFactory;
    private final boolean avoidCasOps;

    /**
     * Constructs <tt>JiraVCacheServiceCreator</tt>.
     *
     * @param jiraProperties that would be used to get properties that determine cache type that should be created.
     * @param cacheFactory   required if cache to be created is of type {@link LegacyVCacheService}.
     * @param avoidCasOps    if set to true vcache would try to avoid calling CAS operations on underlying cache backend.
     *                       It is required for certain types of caches (e.g. ehcache) that do not support CAS.
     */
    public JiraVCacheServiceCreator(final JiraProperties jiraProperties,
                                    Optional<CacheFactory> cacheFactory, boolean avoidCasOps) {
        this.jiraProperties = jiraProperties;
        this.cacheFactory = cacheFactory;
        this.avoidCasOps = avoidCasOps;
    }

    /**
     * Instantiates and configures appropriate implementation of {@link AbstractVCacheService} and
     * {@link JiraVCacheRequestContextSupplier} based on JIRA environment and properties.
     *
     * @return Pair of configured instances of JiraVCacheRequestContextSupplier and AbstractVCacheService.
     */
    @Nonnull
    public Pair<JiraVCacheRequestContextSupplier, AbstractVCacheService> createVCacheService() {
        final Boolean warnNoContext = jiraProperties.getBoolean(PROPERTY_WARN_NO_CONTEXT);
        JiraVCacheRequestContextSupplier contextSupplier = new JiraVCacheRequestContextSupplier(
                warnNoContext == null ? false : warnNoContext);

        final CacheType cacheType = determineCacheType();
        switch (cacheType) {
            case LEGACY:
                return Pair.pair(contextSupplier, createLegacyService(contextSupplier));
            case MEMCACHED:
                return Pair.pair(contextSupplier, createMemcachedService(contextSupplier));
            default:
                throw new UnsupportedOperationException("Unsupported cache type: " + cacheType);
        }
    }

    private AbstractVCacheService createMemcachedService(Supplier<RequestContext> contextSupplier) {
        final String host = jiraProperties.getProperty(PROPERTY_MEMCACHED_HOST);
        final Integer port = jiraProperties.getInteger(PROPERTY_MEMCACHED_PORT, DEFAULT_MEMCACHED_PORT);

        if (StringUtils.isBlank(host)) {
            throw new IllegalStateException("Cannot initialise VCache - memcached host is not defined");
        }

        //MemcachedClient is thread safe so we just need one instance of it.
        final com.google.common.base.Supplier<MemcachedClientIF> lazySupplier = Suppliers.memoize(() ->
                createMemcachedClient(host, port));

        MemcachedVCacheServiceSettingsBuilder builder = new MemcachedVCacheServiceSettingsBuilder()
                .productIdentifier(PRODUCT_IDENTIFIER)
                .clientSupplier(lazySupplier::get)
                .contextSupplier(contextSupplier)
                .defaultsProvider(new JiraVCacheDefaultsProvider())
                .creationHandler(createVCacheCreationHandler())
                .metricsCollector(createMetricsCollector(contextSupplier));

        return new MemcachedVCacheService(builder.build());
    }


    private MemcachedClientIF createMemcachedClient(String host, int port) {
        try {
            return new MemcachedClient(new InetSocketAddress(host, port));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private AbstractVCacheService createLegacyService(Supplier<RequestContext> contextSupplier) {
        if (!cacheFactory.isPresent()) {
            throw new IllegalStateException("Cannot instantiate LegacyVCacheService - cache factory was not provided.");
        }
        final LegacyServiceSettingsBuilder settingsBuilder = new LegacyServiceSettingsBuilder();
        if (avoidCasOps) {
            settingsBuilder.enableAvoidCasOperations();
        }

        return new LegacyVCacheService(PRODUCT_IDENTIFIER, contextSupplier, new JiraVCacheDefaultsProvider(),
                createVCacheCreationHandler(), createMetricsCollector(contextSupplier), cacheFactory::get,
                settingsBuilder.build(), requestContext -> {});
    }

    private CacheType determineCacheType() {
        String forcedType = jiraProperties.getProperty(PROPERTY_FORCE_VCACHE_TYPE);

        if (forcedType != null) {
            final CacheType cacheType = EnumUtils.getEnum(CacheType.class, forcedType.toUpperCase());
            if (cacheType != null) {
                return cacheType;
            }
            log.warn("Cannot force usage of provided cache type: '{}' - unsupported backend type.");
        }

        return CacheType.LEGACY;
    }

    private MetricsCollector createMetricsCollector(Supplier<RequestContext> contextSupplier) {
        final Boolean collectMetrics = jiraProperties.getBoolean(CacheStatisticsMonitor.JIRA_CACHE_INSTRUMENTATION);
        return collectMetrics ? new DefaultMetricsCollector(contextSupplier) : new NoopMetricsCollector();
    }

    private VCacheCreationHandler createVCacheCreationHandler() {
        final Integer maxEntries = jiraProperties.getInteger(PROPERTY_MAX_ENTRIES, DEFAULT_MAX_ENTRIES);
        return new DefaultVCacheCreationHandler(maxEntries,
                Duration.ofSeconds(jiraProperties.getInteger(PROPERTY_MAX_TTL_IN_SEC, (int) DEFAULT_MAX_TTL)),
                maxEntries,
                ChangeRate.HIGH_CHANGE,
                ChangeRate.HIGH_CHANGE
        );
    }

    private class JiraVCacheDefaultsProvider implements VCacheSettingsDefaultsProvider {
        @Nonnull
        @Override
        public ExternalCacheSettings getExternalDefaults(final String name) {
            return new ExternalCacheSettingsBuilder().defaultTtl(DEFAULT_TTL_EXTERNAL_CACHE)
                    .dataChangeRateHint(ChangeRate.LOW_CHANGE)
                    .entryGrowthRateHint(ChangeRate.LOW_CHANGE)
                    .build();
        }

        @Nonnull
        @Override
        public JvmCacheSettings getJvmDefaults(final String name) {
            return new JvmCacheSettingsBuilder()
                    .defaultTtl(DEFAULT_TTL_JVM_CACHE)
                    .build();
        }
    }

    private enum CacheType {
        LEGACY, REDIS, MEMCACHED;
    }
}
