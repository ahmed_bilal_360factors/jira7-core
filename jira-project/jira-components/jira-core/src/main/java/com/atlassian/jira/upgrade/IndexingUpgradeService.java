package com.atlassian.jira.upgrade;

import com.atlassian.jira.index.request.ReindexRequestTypes;

import static com.atlassian.upgrade.api.UpgradeContext.UpgradeTrigger.UPGRADE;

/**
 * Upgrade service that triggers Lucene indexing after upgrading process is finished.
 */
public class IndexingUpgradeService implements UpgradeService {

    private final LoggingUpgradeService loggingUpgradeService;

    public IndexingUpgradeService(final LoggingUpgradeService loggingUpgradeService) {
        this.loggingUpgradeService = loggingUpgradeService;
    }

    public UpgradeResult runUpgrades() {
        return loggingUpgradeService.runUpgradesWithLogging(
                ReindexRequestTypes.allAllowed(),
                () -> UPGRADE,
                "run upgrades"
        );
    }

    @Override
    public boolean areUpgradesRunning() {
        return loggingUpgradeService.areUpgradesRunning();
    }
}
