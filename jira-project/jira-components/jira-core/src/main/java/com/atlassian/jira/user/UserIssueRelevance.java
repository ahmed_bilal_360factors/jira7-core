package com.atlassian.jira.user;

import com.atlassian.jira.issue.Issue;

import java.util.Date;
import java.util.Optional;
import java.util.SortedSet;

/**
 * Represents a user's relevancy to an issue.
 * <p>
 * Implements the <code>Comparable</code> interface, so that users can be sorted
 * according to relevancy.
 * <p>
 * Also exposes the fields used for sorting so that clients can implement their
 * own comparator, or display relevant fields to end-users.
 *
 * @since v7.2
 */
public interface UserIssueRelevance extends Comparable<UserIssueRelevance> {
    /**
     * @return The user
     */
    ApplicationUser getUser();

    /**
     * @return The issue
     */
    Issue getIssue();

    /**
     * @return The set of involvements the user has with the issue
     */
    SortedSet<IssueInvolvement> getIssueInvolvements();

    /**
     * @return The highest issue involvement rank from the set of involvements
     * the user has with the issue, where 0 is a closer involvement than 1
     */
    Optional<Integer> getHighestIssueInvolvementRank();

    /**
     * @return The time of the user's latest comment on the issue, or empty if none
     */
    Optional<Date> getLatestCommentCreationTime();
}
