package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.upgrade.AbstractDelayableUpgradeTask;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nullable;

/**
 * Remove any plugin state related to the {@code com.atlassian.jira.plugin.system.licenseroles plugin}.
 * Complexity O(2).
 */
public class UpgradeTask_Build6323 extends AbstractDelayableUpgradeTask {
    public UpgradeTask_Build6323() {
        super();
    }

    @Override
    public int getBuildNumber() {
        return 6323;
    }

    @Override
    public String getShortDescription() {
        return "Remove plugin state for the 'LicenseRoles' plugin.";
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        // Flagged as not downgradable because it was created before the Downgrade Task framework was built.
        return true;
    }

    @Override
    public void doUpgrade(final boolean setupMode) {
        deletePluginState("com.atlassian.jira.plugin.system.licenseroles");
        deletePluginState("com.atlassian.jira.plugin.system.licenseroles:businessUser");
    }

    private void deletePluginState(String key) {
        getOfBizDelegator().removeByAnd("PluginState", ImmutableMap.of("key", key));
    }

    @Nullable
    @Override
    public Integer dependsUpon() {
        return 6322;
    }

}
