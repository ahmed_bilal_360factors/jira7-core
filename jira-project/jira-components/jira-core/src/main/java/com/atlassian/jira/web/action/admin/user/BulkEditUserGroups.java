package com.atlassian.jira.web.action.admin.user;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.bc.group.GroupRemoveChildMapper;
import com.atlassian.jira.bc.group.GroupService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.fields.option.GroupOption;
import com.atlassian.jira.issue.fields.option.UserOption;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.GlobalPermissionGroupAssociationUtil;
import com.atlassian.jira.util.JiraUrlCodec;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.base.Stopwatch;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.opensymphony.util.TextUtils;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.google.common.base.Predicates.not;
import static java.util.stream.Collectors.toList;

@WebSudoRequired
public class BulkEditUserGroups extends JiraWebActionSupport {
    private String[] selectedGroupsStr;
    private List<Group> selectedGroups;

    private String[] usersToUnassign;

    private Collection<String> usersToAssignMultiSelect = new ArrayList<>();
    private HashSet<String> prunedUsersToAssign = new HashSet<>();

    private ArrayList<GroupOption> membersList;
    private ArrayList<String> overloadedGroups;

    //form buttons
    private String assign;
    private String unassign;

    private Integer maxMembers;

    private final ApplicationProperties applicationProperties;
    private final GlobalPermissionGroupAssociationUtil globalPermissionGroupAssociationUtil;
    private final GroupService groupService;
    private final GroupManager groupManager;
    private final UserManager userManager;

    private static final int MAX_LIST_SIZE = 20;
    private static final String OPTION_VALUE_SEPARATOR = ",";

    public BulkEditUserGroups(GroupManager groupManager,
                              ApplicationProperties applicationProperties,
                              GlobalPermissionGroupAssociationUtil globalPermissionGroupAssociationUtil,
                              GroupService groupService,
                              UserManager userManager) {
        this.globalPermissionGroupAssociationUtil = globalPermissionGroupAssociationUtil;
        this.applicationProperties = applicationProperties;
        this.groupService = groupService;
        this.groupManager = groupManager;
        this.userManager = userManager;
    }

    public void doValidation() {
        super.doValidation();

        // Common validation for both add/remove
        if (selectedGroupsStr == null || selectedGroupsStr.length == 0) {
            addErrorMessage(getText("admin.bulkeditgroups.error.no.group.selected"));
            return;
        }

        // Perform the add specific validation
        if (TextUtils.stringSet(assign)) {
            if (getUsersToAssign() == null || getUsersToAssign().isEmpty()) {
                addErrorMessage(getText("admin.bulkeditgroups.error.no.users.to.add"));
                return;
            }

            GroupService.BulkEditGroupValidationResult bulkEditGroupValidationResult =
                    groupService.validateAddUsersToGroup(getJiraServiceContext(), Arrays.asList(selectedGroupsStr), getUsersToAssign());

            if (!bulkEditGroupValidationResult.isSuccess()) {
                HashSet<String> validUsers = new HashSet<>(getUsersToAssign());
                Collection<String> invalidChildren = bulkEditGroupValidationResult.getInvalidChildren();
                validUsers.removeAll(invalidChildren);
                prunedUsersToAssign = validUsers;
            }
        } else if (TextUtils.stringSet(unassign)) {
            // Perform the remove specific validation
            if (usersToUnassign == null || usersToUnassign.length <= 0) {
                addErrorMessage(getText("admin.bulkeditgroups.error.no.users.to.remove"));
                return;
            }

            groupService.validateRemoveUsersFromGroups(getJiraServiceContext(), getGroupRemoveUserMapper());
        }
    }

    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        if (TextUtils.stringSet(assign)) {
            groupService.addUsersToGroups(getJiraServiceContext(), Arrays.asList(selectedGroupsStr), getUsersToAssign());
        } else if (TextUtils.stringSet(unassign)) {
            groupService.removeUsersFromGroups(getJiraServiceContext(), getGroupRemoveUserMapper());
        }
        return redirectToView();
    }

    /**
     * gets all the groups, used to populate the groups select list
     */
    public Collection<Group> getAllVisibleGroups() {
        return globalPermissionGroupAssociationUtil.getGroupsModifiableByCurrentUser(getLoggedInUser(), new ArrayList<>(groupManager.getAllGroups()));
    }

    /**
     * Checks if the group is selected
     */
    public boolean getIsGroupSelected(Group group) {
        return (getSelectedGroupsUserHasPermToSee() != null && getSelectedGroupsUserHasPermToSee().contains(group));
    }

    /**
     * Used to populate the assigned users of the selected groups.<br>
     * Always has the 'All' group which represents all the members of the selected groups.<br>
     * Rest of the users are added under individual group names.
     */
    public Collection<GroupOption> getMembersList() {
        if (membersList != null) {
            return membersList;
        }

        List<Group> groups = getSelectedGroupsUserHasPermToSee();
        if (groups == null) {
            return null;
        }

        Stopwatch timer = log.isDebugEnabled() ? Stopwatch.createStarted() : null;

        membersList = new ArrayList<>();
        overloadedGroups = new ArrayList<>();

        Multimap<Group, String> groupedUsers = LinkedHashMultimap.create();
        Set<String> allUserNames = new HashSet<>();

        int limit = getMaxUsersDisplayedPerGroup();
        for (Group curGroup : groups) {
            //limit + 1 so we can detect overflow
            Collection<String> userNames = groupManager.getNamesOfDirectMembersOfGroups(Collections.singleton(curGroup.getName()), limit + 1);

            //Fill in overloaded groups if necessary
            if (userNames.size() > limit) {
                overloadedGroups.add(curGroup.getName());
                userNames = userNames.stream().limit(limit).collect(toList());
            }

            allUserNames.addAll(userNames);
            groupedUsers.putAll(curGroup, userNames);
        }

        if (groups.size() > 1) {
            // If more than one group, find users in all groups
            List<String> groupNames = groups.stream().map(Group::getName).collect(toList());
            Collection<String> userNamesInAllGroups = groupManager.filterUsersInAllGroupsDirect(allUserNames, groupNames);

            if (!userNamesInAllGroups.isEmpty()) {
                // Build 'all' group option
                addGroupOption(new GroupOption(getText("admin.bulkeditgroups.all.selected.groups")), userNamesInAllGroups);

                //Remove individuals that are in all groups
                groupedUsers = Multimaps.filterValues(groupedUsers, not(userNamesInAllGroups::contains));
            }
        }

        // Build group options
        for (Map.Entry<Group, Collection<String>> groupEntry : groupedUsers.asMap().entrySet()) {
            Collection<String> userNamesInGroup = groupEntry.getValue();
            if (!userNamesInGroup.isEmpty()) {
                addGroupOption(new GroupOption(groupEntry.getKey()), userNamesInGroup);
            }
        }

        if (timer != null) {
            log.debug("Retrieving [" + allUserNames.size() + "] members in [" + groups.size() + "] groups took " + timer);
        }

        return membersList;
    }

    private void addGroupOption(final GroupOption groupOption, final Collection<String> userNamesInGroup) {
        for (String userName : userNamesInGroup) {
            groupOption.addChildOption(new UserOption(userName));
        }
        membersList.add(groupOption);
    }

    /**
     * Counts the total number of user entries from the memberslist.<br>
     * NOTE: This does not count distinct users - so with multiple selected groups, the count may be off
     */
    public int getAssignedUsersCount() {
        int assignedUsersCount = 0;
        for (final GroupOption groupOption : getMembersList()) {
            assignedUsersCount += groupOption.getChildOptions().size();
        }
        return assignedUsersCount;
    }

    /**
     * determine what size the assigned users select list should be (capped at MAX_LIST_SIZE)
     */
    public int getAssignedUsersListSize() {
        return getListSize(getAssignedUsersCount() + getMembersList().size());
    }

    /**
     * use this to limit the listSizes to MAX_LIST_SIZE
     */
    public int getListSize(int size) {
        return size < MAX_LIST_SIZE ? size : MAX_LIST_SIZE;
    }

    /**
     * used to determine what the option value (format) for a UserOption should be
     */
    public String getOptionValue(UserOption userOption) {
        if (userOption != null) {
            GroupOption parentOption = userOption.getParentOption();
            if (parentOption != null && parentOption.getGroup() != null) {
                return userOption.getName() + OPTION_VALUE_SEPARATOR + parentOption.getRawName();
            } else {
                return userOption.getName();
            }
        }
        return "";
    }

    public String getUnassign() {
        return unassign;
    }

    public void setUnassign(String unassign) {
        this.unassign = unassign;
    }

    public String getAssign() {
        return assign;
    }

    public void setAssign(String assign) {
        this.assign = assign;
    }

    public String[] getSelectedGroupsStr() {
        return selectedGroupsStr;
    }

    public void setSelectedGroupsStr(String[] selectedGroupsStr) {
        this.selectedGroupsStr = selectedGroupsStr;
    }

    /**
     * Of the groups the user has selected, return only those the current user has permission to edit.
     *
     * @return those {@link Group Groups}.
     */
    public List<Group> getSelectedGroupsUserHasPermToSee() {
        if (selectedGroupsStr == null) {
            return new ArrayList<>();
        }

        if (selectedGroups == null) {
            ArrayList<Group> selectedGroupsHolder = new ArrayList<>();
            for (String groupName : selectedGroupsStr) {
                Group group = groupManager.getGroup(groupName);
                if (group != null) {
                    selectedGroupsHolder.add(group);
                }
            }
            selectedGroups = globalPermissionGroupAssociationUtil.getGroupsModifiableByCurrentUser(getLoggedInUser(), selectedGroupsHolder);
        }
        return selectedGroups;
    }

    @Deprecated
    public void setUsersToAssignStr(String usersToAssignStr) {
        if (StringUtils.isNotBlank(usersToAssignStr)) {
            final Collection<String> processedUserList =
                Arrays.asList(usersToAssignStr.split(",")).stream()
                        .map(StringUtils::trimToNull)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());
            setUsersToAssignMultiSelect(processedUserList);
        }
    }

    @Deprecated
    public String getUsersToAssignStr() {
        return StringUtils.join(getUsersToAssign(), ", ");
    }

    public String[] getUsersToAssignMultiSelect() {
        return usersToAssignMultiSelect.toArray(new String[usersToAssignMultiSelect.size()]);
    }

    public void setUsersToAssignMultiSelect(String[] usersToAssignMultiSelect) {
        setUsersToAssignMultiSelect(Arrays.asList(usersToAssignMultiSelect));
    }

    public void setUsersToAssignMultiSelect(Collection<String> usersToAssignMultiSelect) {
        this.usersToAssignMultiSelect = usersToAssignMultiSelect;
    }

    @SuppressWarnings("unused") // Used in bulkeditusergroups.jsp
    public Collection<ApplicationUser> getUsersToAssignToTheMultiSelect() {
        final Collection<ApplicationUser> users = new ArrayList<>();
        for (String username : getUsersToAssign()) {
            users.add(userManager.getUserByName(username));
        }
        return users;
    }

    private Collection<String> getUsersToAssign() {
        return usersToAssignMultiSelect;
    }

    /**
     * @return collection of valid user names to assign to the currently selected groups
     */
    public Collection<String> getPrunedUsersToAssign() {
        return prunedUsersToAssign;
    }

    public void setUsersToUnassign(String[] usersToUnassign) {
        this.usersToUnassign = usersToUnassign;
    }

    public String[] getUsersToUnassign() {
        return usersToUnassign;
    }

    public boolean isTooManyUsersListed() {
        return overloadedGroups != null && !overloadedGroups.isEmpty();
    }

    public int getMaxUsersDisplayedPerGroup() {
        if (maxMembers == null) {
            final int MAX_USERS_DISPLAYED_PER_GROUP = 200;

            String maxMembersStr = applicationProperties.getDefaultBackedString(APKeys.USER_MANAGEMENT_MAX_DISPLAY_MEMBERS);
            if (maxMembersStr != null) {
                try {
                    maxMembers = Integer.valueOf(maxMembersStr.trim());
                } catch (NumberFormatException e) {
                    log.warn("Invalid format of '" + APKeys.USER_MANAGEMENT_MAX_DISPLAY_MEMBERS + "' property: '" + maxMembersStr + "'. Value should be an integer. Using " + MAX_USERS_DISPLAYED_PER_GROUP);
                    maxMembers = MAX_USERS_DISPLAYED_PER_GROUP;
                }
            } else {
                log.debug("'" + APKeys.USER_MANAGEMENT_MAX_DISPLAY_MEMBERS + "' is missing. Using " + MAX_USERS_DISPLAYED_PER_GROUP + " instead.");
                maxMembers = MAX_USERS_DISPLAYED_PER_GROUP;
            }
        }

        return maxMembers.intValue();
    }

    public String getPrettyPrintOverloadedGroups() {
        StringBuilder sb = new StringBuilder();
        int length = overloadedGroups.size();
        for (int i = 0; i < length; i++) {
            sb.append(overloadedGroups.get(i));
            if (i == length - 2 && length > 1)
                sb.append(" ").append(getText("common.words.and")).append(" ");
            else if (i < length - 1)
                sb.append(", ");
        }
        return sb.toString();
    }

    private GroupRemoveChildMapper getGroupRemoveUserMapper() {
        GroupRemoveChildMapper groupRemoveChildMapper = new GroupRemoveChildMapper(Arrays.asList(selectedGroupsStr));

        for (final String anUsersToUnassign : usersToUnassign) {
            //extract the username and the group name
            String username = extractUserName(anUsersToUnassign);
            String groupname = extractGroupName(anUsersToUnassign);
            if (groupname != null) {
                groupRemoveChildMapper.register(username, groupname);
            } else {
                groupRemoveChildMapper.register(username);
            }
        }
        return groupRemoveChildMapper;
    }

    /**
     * Returns the username without without the appended group name
     *
     * @param optionValue
     * @return
     */
    private String extractUserName(String optionValue) {
        int splitIndex = optionValue.indexOf(OPTION_VALUE_SEPARATOR);
        //JRA-14495: Need to allow for usernames that are 1 character long too.
        if (splitIndex >= 1) {
            return optionValue.substring(0, splitIndex);
        }

        return optionValue;
    }

    /**
     * Returns the group name the user is to be removed from.<br>
     * The group name is null if the user is a member of all the selected groups.
     *
     * @param optionValue
     */
    private String extractGroupName(String optionValue) {
        int splitIndex = optionValue.indexOf(OPTION_VALUE_SEPARATOR);
        //JRA-14495: Need to allow for usernames that are 1 character long too.
        if (splitIndex >= 1) {
            return optionValue.substring(splitIndex + OPTION_VALUE_SEPARATOR.length());
        }
        return null;
    }

    private String redirectToView() {
        StringBuilder redirectUrl = new StringBuilder("BulkEditUserGroups!default.jspa?");

        Iterator<Group> groups = getSelectedGroupsUserHasPermToSee().iterator();
        while (groups.hasNext()) {
            Group group = groups.next();
            redirectUrl.append("selectedGroupsStr=").append(JiraUrlCodec.encode(group.getName()));
            if (groups.hasNext())
                redirectUrl.append("&");
        }

        return getRedirect(redirectUrl.toString());
    }
}
