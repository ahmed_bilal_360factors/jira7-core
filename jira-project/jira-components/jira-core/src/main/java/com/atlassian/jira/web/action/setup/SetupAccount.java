package com.atlassian.jira.web.action.setup;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.license.JiraProductLicense;
import com.atlassian.jira.license.LicenseDetailsFactoryImpl;
import com.atlassian.jira.util.FileFactory;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.JiraProductInformation;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.web.action.ActionViewData;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class SetupAccount extends AbstractSetupAction {

    private static final Logger log = LoggerFactory.getLogger(SetupAccount.class);
    public static final String LICENSE_PROBLEM = "licenseproblem";

    private final SetupSharedVariables setupSharedVariables;

    private String setupOption = "classic";
    private String productLicense;
    private String email;
    private String password;

    public SetupAccount(final FileFactory fileFactory, final JiraProperties jiraProperties,
                        final JiraProductInformation jiraProductInformation, final SetupSharedVariables setupSharedVariables) {
        super(fileFactory, jiraProperties, jiraProductInformation);
        this.setupSharedVariables = setupSharedVariables;
    }

    @Override
    public String doDefault() throws Exception {
        if (setupAlready()) {
            return SETUP_ALREADY;
        }

        if (!validateLicense()) {
            return LICENSE_PROBLEM;
        }

        return INPUT;
    }

    @Override
    public String doExecute() throws Exception {
        if (setupAlready()) {
            return SETUP_ALREADY;
        }

        if (!validateLicense()) {
            return LICENSE_PROBLEM;
        }

        if (StringUtils.isBlank(getEmail()) || StringUtils.isBlank(getPassword())) {
            return INPUT;
        }

        if (setupSharedVariables.getIsInstantSetup()) {
            return forceRedirect("SetupFinishing!default.jspa");
        } else {
            return forceRedirect(SetupDatabase.REDIRECT_URL);
        }
    }

    /*
     * This needs to be vaulted over to javascript because in setup time there is no i18n transformer available.
     */
    @ActionViewData
    public String getErrorTextsJson() throws JSONException {
        final I18nHelper i18nHelper = ComponentAccessor.getI18nHelperFactory().getInstance(getLocale());
        final JSONObject json = new JSONObject();

        json.put("invalidEmail", i18nHelper.getText("setup.account.form.email.invalid"));
        json.put("invalidUsername", i18nHelper.getText("setup.account.form.username.invalid"));
        json.put("tooLongUsername", i18nHelper.getText("setup.account.form.username.too.long"));
        json.put("emailRequired", i18nHelper.getText("setup.account.form.email.required"));
        json.put("usernameRequired", i18nHelper.getText("setup.account.form.username.required"));
        json.put("passwordRequired", i18nHelper.getText("setup.account.form.password.required"));
        json.put("passwordRetypeRequired", i18nHelper.getText("setup.account.form.retype.password.required"));
        json.put("passwordsDoNotMatch", i18nHelper.getText("setup.account.form.retype.password.mismatch"));

        return json.toString();
    }

    /*
     * This needs to be vaulted over to javascript because in setup time there is no i18n transformer available.
     */
    @ActionViewData
    public String getLicenseFlagTextsJson() throws JSONException {
        final I18nHelper i18nHelper = ComponentAccessor.getI18nHelperFactory().getInstance(getLocale());

        final JSONObject json = new JSONObject();

        json.put("successTitle", i18nHelper.getText("setup.license.flag.title"));
        json.put("successContent", i18nHelper.getText("setup.license.flag.content"));
        json.put("errorTitle", i18nHelper.getText("setup.license.flag.error.title"));
        json.put("errorContent", i18nHelper.getText("setup.license.flag.error.content",
                String.format("<a href=\"%s\">", setupSharedVariables.getBaseUrl()),
                "</a>"));

        return json.toString();
    }

    private boolean validateLicense() {

        final I18nHelper i18nHelper = ComponentAccessor.getI18nHelperFactory().getInstance(getLocale());

        if (productLicense == null || productLicense.trim().isEmpty()) {
            addError("licenseError", i18nHelper.getText("setup.license.error.no.license.provided"));
            return false;
        }

        // try to decode the license:
        final JiraProductLicense jiraProductLicense;
        try {
            jiraProductLicense = LicenseDetailsFactoryImpl.JiraProductLicenseManager.INSTANCE.getProductLicense(this.productLicense);
        } catch (RuntimeException e) {
            log.error("Provided license could not be decoded or is not a JIRA license", e);
            addError("licenseError", i18nHelper.getText("setup.license.error.not.a.jira.license"));
            return false;
        }

        // expect that there is a JIRA licensed in there somewhere:
        if (!jiraProductLicense.getApplications().getKeys().stream()
                .filter(key -> key.value().toLowerCase().contains("jira"))
                .findAny()
                .isPresent()) {
            addError("licenseError", i18nHelper.getText("setup.license.error.jira.not.licensed"));
            return false;
        }
        return true;
    }

    @ActionViewData
    @Override
    public Map<String, String> getErrors() {
        return super.getErrors();
    }

    public void setProductLicense(final String productLicense) {
        this.productLicense = productLicense;
    }

    @ActionViewData
    public String getProductLicense() {
        return productLicense;
    }

    public String getSetupOption() {
        return setupOption;
    }

    public void setSetupOption(final String setupOption) {
        this.setupOption = setupOption;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
}
