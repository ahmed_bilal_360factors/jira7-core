package com.atlassian.jira.instrumentation;

import com.atlassian.annotations.Internal;
import com.atlassian.instrumentation.caches.RequestListener;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import java.util.Optional;
import java.util.function.Consumer;

/**
 * Manager that handles calling registered listeners to send data to the
 * analytics package.
 *
 * @since v7.1
 */
@Internal
@ParametersAreNonnullByDefault
public interface InstrumentationListenerManager {
    /**
     * Runs all the registered RequestListeners at the beginning of the Request processing.
     */
    void processOnStartListeners();

    /**
     * Runs all the registered RequestListeners at the end of the
     * Request processing.
     *
     * @param requestData        The request path
     * @param requestTime An @{link Optional} that may contain the request time.
     */
    void processOnEndListeners(RequestData requestData, Optional<Long> requestTime);

    /**
     * Add a listener
     *
     * @param listener The listener to add.
     */
    void addRequestListener(RequestListener listener);

    /**
     * Remove a listener.
     *
     * @param listener The listener to remove.
     */
    void removeRequestListener(RequestListener listener);

    /**
     * Apply function to each listener.
     *
     * @param apply Run a function on each listener.
     */
    void applyToAllListeners(Consumer<RequestListener> apply);

    /**
     * @return the path of the current request (applies to the current thread).
     */
    @Nonnull
    Optional<String> getCurrentPath();

    /**
     * @return the trace ID of the current request (applies to the current thread).
     */
    @Nonnull
    Optional<String> getCurrentTraceId();

    @Nonnull
    Optional<RequestData> getRequestData();
}
