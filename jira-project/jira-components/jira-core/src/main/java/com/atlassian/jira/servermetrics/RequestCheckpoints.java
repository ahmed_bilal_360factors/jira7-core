package com.atlassian.jira.servermetrics;

/**
 * Put your checkpoints here to ensure that those are checked against whitelist.
 */
public enum RequestCheckpoints {
    beforeWorkActionPrepareDispatcher,
    beforeWorkActionStarts,
    afterWorkActionExecute,
    webworkViewDispatch,
    preProcessingFilterChainFinished,
    postProcessingFilterChainStarts,
    beforePageBuilderFinish,
    afterPageBuilderWriteHead,
    afterPageBuilderPreBodyDecorator,
    afterPageBuilderWriteBody,
    pageBuilderFinish,
    beforePageBuilderFlushHeadEarly,
    pageBuilderFlushHeadEarly,
    beforeSecurityFilter,
    jiraSecurityFilter,
    serverRenderEnd
}