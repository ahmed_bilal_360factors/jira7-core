package com.atlassian.jira.portal;

import com.atlassian.fugue.Option;
import com.atlassian.gadgets.dashboard.Color;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.plugin.ModuleCompleteKey;

import java.net.URI;
import java.util.List;
import java.util.Map;

public class PortletConfigurationManagerImpl implements PortletConfigurationManager {
    private final PortletConfigurationStore portletConfigurationStore;

    public PortletConfigurationManagerImpl(final PortletConfigurationStore portletConfigurationStore) {
        this.portletConfigurationStore = Assertions.notNull("portletConfigurationStore", portletConfigurationStore);
    }

    @Override
    public List<PortletConfiguration> getByPortalPage(final Long portalPageId) {
        return portletConfigurationStore.getByPortalPage(portalPageId);
    }

    @Override
    public PortletConfiguration getByPortletId(final Long portletId) {
        return portletConfigurationStore.getByPortletId(portletId);
    }

    public void delete(final PortletConfiguration pc) {
        portletConfigurationStore.delete(pc);
    }

    @Override
    public PortletConfiguration addGadget(final Long portalPageId, final Integer column, final Integer row, final URI gadgetXml, final Color color, final Map<String, String> userPreferences) {
        return addDashBoardItem(portalPageId, column, row, Option.some(gadgetXml), color, userPreferences, Option.<ModuleCompleteKey>none());
    }

    @Override
    public PortletConfiguration addDashBoardItem(final Long portalPageId, final Integer column, final Integer row, final Option<URI> openSocialSpecUri,
                                                 final Color color, final Map<String, String> userPreferences, final Option<ModuleCompleteKey> moduleKey) {
        return portletConfigurationStore.addDashboardItem(portalPageId, null, column, row, openSocialSpecUri, color, userPreferences, moduleKey);
    }

    @Override
    public void store(final PortletConfiguration pc) {
        portletConfigurationStore.store(pc);
    }
}
