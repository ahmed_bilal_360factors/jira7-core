package com.atlassian.jira.config.properties;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.propertyset.CachingOfBizPropertySet;
import com.atlassian.jira.propertyset.OfBizPropertyEntryStore;
import com.opensymphony.module.propertyset.PropertySet;

/**
 * Static methods to get the default property set.  This currently holds:
 * <ul>
 * <li>Application properties</li>
 * <li>Plugin settings</li>
 * </ul>
 * It used to also hold plugin states, but those have been moved to their own table.  This
 * should only ever be called by {@link BackingPropertySetManager} implementations; any
 * other code should use the {@link com.atlassian.jira.propertyset.JiraPropertySetFactory},
 * instead.
 *
 * @since v4.4
 */
class PropertySetUtils {
    private final static String SEQUENCE = "jira.properties";
    private final static Long ID = 1L;

    private PropertySetUtils() {
    }

    static PropertySet createDatabaseBackedPropertySet() {
        return ComponentAccessor.getComponentSafely(OfBizPropertyEntryStore.class)
                .map(store -> new CachingOfBizPropertySet(store, SEQUENCE, ID))
                .orElseGet(() -> new CachingOfBizPropertySet(SEQUENCE, ID));
    }
}
