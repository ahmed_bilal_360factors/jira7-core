package com.atlassian.jira.web.action.admin.instrumentation.cache;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.ManagedCache;
import com.atlassian.fugue.Pair;
import com.atlassian.jira.instrumentation.CacheStatistics;
import com.atlassian.jira.instrumentation.InstrumentationLogger;
import com.atlassian.jira.instrumentation.LogEntry;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import webwork.action.ActionContext;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.reducing;

/**
 * Displays the average usage of a cache over the list of URLs accessed.
 *
 * @since v7.1
 */
@WebSudoRequired
public class ViewCacheDetails extends JiraWebActionSupport {
    private final InstrumentationLogger instrumentationLogger;
    private final CacheManager cacheManager;
    private String name;
    private Predicate<LogEntry> cacheStatNotNull = logData -> logData.getData().get(CacheStatistics.CACHE_LAAS_ID) != null;

    public ViewCacheDetails(CacheManager cacheManager, InstrumentationLogger instrumentationLogger) {
        this.cacheManager = cacheManager;
        this.instrumentationLogger = instrumentationLogger;
    }

    public Collection<CacheStatsAverager> getCacheDetails() {
        name = (String) ActionContext.getSingleValueParameters().getOrDefault("name", "com.atlassian.jira.propertyset.CachingOfBizPropertyEntryStore.entries");

        List<LogEntry> stats = instrumentationLogger.getLogEntriesFromBuffer();

        // Get counts of URL for this cache to calculate averages later.
        Map<String, Long> urlCounts = stats.stream()
                .filter(cacheStatNotNull.and(logData -> logData.getData().get(CacheStatistics.CACHE_LAAS_ID).stream().anyMatch(s -> s.getName().equals(name))))
                .collect(Collectors.groupingBy(LogEntry::getPath, Collectors.counting()));

        return stats.stream()
                .filter(cacheStatNotNull)
                .flatMap(logData -> logData.getData().get(CacheStatistics.CACHE_LAAS_ID).stream()
                        .filter(stat -> stat.getName().equals(name) && stat instanceof CacheStatistics)
                        .map(stat -> Pair.pair(logData.getPath(), (CacheStatistics) stat)))
                .map(stat -> CacheStatsAverager.build(stat.left(), stat.right()))
                .collect(Collectors.groupingBy(CacheStatsAverager::getName, collectingAndThen(
                        /* reduce to calculate sums of each metric for each group. */
                        reducing((a, b) ->
                                a.addAndSetCount(b, urlCounts.containsKey(a.getName()) ? urlCounts.get(a.getName()) : 0)),
                        Optional::get)))
                .values();
    }

    public String getCacheEntryCount() {
        String name = (String) ActionContext.getSingleValueParameters().get("name");
        if (name == null) {
            return "0";
        }
        ManagedCache cache = cacheManager.getManagedCache(name);
        if (cache != null) {
            return String.valueOf(cache.getCacheCollector().getCacheSize());
        }
        return "0";
    }

    public String getRequestCount() {
        String name = (String) ActionContext.getSingleValueParameters().getOrDefault("name", "com.atlassian.jira.propertyset.CachingOfBizPropertyEntryStore.entries");
        List<LogEntry> cacheRequestInfo = instrumentationLogger.getLogEntriesFromBuffer();
        return String.valueOf(cacheRequestInfo.stream()
                .filter(cacheStatNotNull.and(logData -> logData.getData().get(CacheStatistics.CACHE_LAAS_ID).stream().anyMatch(s -> s.getName().equals(name))))
                .count());
    }


    /**
     * Returns the total load time for this cache.
     *
     * @return The load time value.
     */
    public String getTotalLoadTime() {
        NumberFormat formatter = new DecimalFormat("#.00");
        String name = (String) ActionContext.getSingleValueParameters().get("name");
        return String.valueOf(formatter.format(instrumentationLogger.getLogEntriesFromBuffer().stream()
                .filter(cacheStatNotNull.and(logData -> logData.getData().get(CacheStatistics.CACHE_LAAS_ID).stream().anyMatch(s -> s.getName().equals(name))))
                .flatMap(logData -> logData.getData().get(CacheStatistics.CACHE_LAAS_ID).stream())
                .filter(stat -> stat instanceof CacheStatistics)
                .map(stat -> (CacheStatistics) stat)
                .mapToDouble(CacheStatistics::loadTime)
                .sum()));
    }

    public String getName() {
        return name;
    }
}
