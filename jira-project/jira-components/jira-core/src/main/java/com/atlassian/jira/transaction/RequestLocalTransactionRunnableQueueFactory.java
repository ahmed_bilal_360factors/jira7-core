package com.atlassian.jira.transaction;

/**
 * A factory that creates Txn runnable queues per request thread, which mirrors
 * the way Txn's are tracked per request thread.
 */
public interface RequestLocalTransactionRunnableQueueFactory {
    /**
     * @return the runnable queue to use for this request thread
     */
    RunnablesQueue getRunnablesQueue();
}
