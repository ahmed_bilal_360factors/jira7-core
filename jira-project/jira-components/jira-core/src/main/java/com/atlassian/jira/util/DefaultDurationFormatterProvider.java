package com.atlassian.jira.util;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.event.DurationFormatChanged;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.JiraDurationUtils.AuthContextI18nLocator;
import com.atlassian.jira.util.JiraDurationUtils.DaysDurationFormatter;
import com.atlassian.jira.util.JiraDurationUtils.DurationFormatter;
import com.atlassian.jira.util.JiraDurationUtils.HoursDurationFormatter;
import com.atlassian.jira.util.JiraDurationUtils.PrettyDurationFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

@EventComponent
public class DefaultDurationFormatterProvider implements DurationFormatterProvider {
    private static final Logger log = LoggerFactory.getLogger(DefaultDurationFormatterProvider.class);

    private CachedReference<DurationFormatter> formatter;
    private ApplicationProperties applicationProperties;
    private TimeTrackingConfiguration timeTrackingConfiguration;
    private I18nHelper.BeanFactory i18nFactory;
    private JiraAuthenticationContext authenticationContext;

    public DefaultDurationFormatterProvider(final CacheManager cacheManager,
                                            final ApplicationProperties applicationProperties,
                                            final TimeTrackingConfiguration timeTrackingConfiguration,
                                            final I18nHelper.BeanFactory i18nFactory,
                                            final JiraAuthenticationContext authenticationContext) {
        this.applicationProperties = applicationProperties;
        this.timeTrackingConfiguration = timeTrackingConfiguration;
        this.i18nFactory = i18nFactory;
        this.authenticationContext = authenticationContext;
        formatter = cacheManager.getCachedReference(getClass().getName() + ".formatter", this::load);
    }

    @Override
    public DurationFormatter getFormatter() {
        return formatter.get();
    }

    @SuppressWarnings({"UnusedDeclaration"})
    @EventListener
    public void onDurationFormatChanged(final DurationFormatChanged event) {
        reset();
    }

    @SuppressWarnings({"UnusedDeclaration"})
    @EventListener
    public void onClearCacheEvent(final ClearCacheEvent event) {
        reset();
    }

    private void reset() {
        formatter.reset();
    }

    private DurationFormatter load() {
        final BigDecimal hoursPerDay = timeTrackingConfiguration.getHoursPerDay();
        final BigDecimal daysPerWeek = timeTrackingConfiguration.getDaysPerWeek();
        final String format = applicationProperties.getDefaultBackedString(APKeys.JIRA_TIMETRACKING_FORMAT);
        final AuthContextI18nLocator i18nLocator = new AuthContextI18nLocator(i18nFactory, authenticationContext);

        if (JiraDurationUtils.FORMAT_HOURS.equals(format)) {
            return new HoursDurationFormatter(i18nLocator);
        } else if (JiraDurationUtils.FORMAT_DAYS.equals(format)) {
            return new DaysDurationFormatter(hoursPerDay, i18nLocator);
        } else if (JiraDurationUtils.FORMAT_PRETTY.equals(format)) {
            return new PrettyDurationFormatter(hoursPerDay, daysPerWeek, i18nLocator);
        } else {
            log.warn("Duration format not configured! Please set the " + APKeys.JIRA_TIMETRACKING_FORMAT + " property");
            return new PrettyDurationFormatter(hoursPerDay, daysPerWeek, i18nLocator);
        }

    }
}
