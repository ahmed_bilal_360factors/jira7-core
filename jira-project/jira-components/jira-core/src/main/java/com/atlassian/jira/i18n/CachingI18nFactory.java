package com.atlassian.jira.i18n;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.InitializingComponent;
import com.atlassian.jira.cache.GoogleCacheInstruments;
import com.atlassian.jira.cluster.ClusterSafe;
import com.atlassian.jira.concurrent.ResettableLazyReference;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.plugin.language.LanguageModuleDescriptor;
import com.atlassian.jira.plugin.language.TranslationTransform;
import com.atlassian.jira.plugin.language.TranslationTransformModuleDescriptor;
import com.atlassian.jira.plugin.util.ModuleDescriptorComparator;
import com.atlassian.jira.plugin.util.PluginsTracker;
import com.atlassian.jira.plugin.util.SimplePluginsTracker;
import com.atlassian.jira.tenancy.TenantAware;
import com.atlassian.jira.tenancy.TenantInfo;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserLocaleStore;
import com.atlassian.jira.util.ComponentLocator;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.resourcebundle.DefaultResourceBundle;
import com.atlassian.jira.web.util.JiraLocaleUtils;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.event.events.PluginRefreshedEvent;
import com.google.common.base.Supplier;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.UncheckedExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static com.atlassian.jira.plugin.util.ModuleDescriptorPredicates.isPluginWithModuleDescriptor;
import static com.atlassian.jira.plugin.util.ModuleDescriptorPredicates.isPluginWithResourceType;

/**
 * As the name implies: a factory that caches for different locales {@link com.atlassian.jira.util.I18nHelper}.
 * <p>
 * With the advent of Plugins-2 we need to iterate through all enabled plugins and get their i18n resources when an
 * {@code I18nHelper} is required.
 */
@TenantInfo(value = TenantAware.UNRESOLVED, comment = "Depends on plugin state.  Exactly how I18N will work in vertigo is not yet known.")
public class CachingI18nFactory implements I18nHelper.BeanFactory, InitializingComponent, Startable {
    static final String I18N_RESOURCE_TYPE = "i18n";
    @ClusterSafe
    private final LoadingCache<Locale, I18nHelper> cache;

    private final JiraLocaleUtils jiraLocaleUtils;
    private final UserLocaleStore userLocaleStore;
    private final Supplier<PluginAccessor> pluginAccessorSupplier;
    private final EventPublisher eventPublisher;
    private final AtomicBoolean jiraStarted = new AtomicBoolean(false);
    private final ResourceBundleCacheCleaner resourceBundleCacheCleaner;

    @ClusterSafe("Driven by plugin state, which is kept in sync across the cluster")
    private final ResettableLazyReference<PluginsTracker> involvedPluginsTracker = new ResettableLazyReference<>();
    /**
     * It's lazy, because it's used during cache loading. Persists through cache expires, but not through invalidation.
     */
    @ClusterSafe("Driven by plugin state, which is kept in sync across the cluster")
    private final ResettableLazyReference<List<TranslationTransform>> translationTransformsRef = new ResettableLazyReference<>();


    public CachingI18nFactory(
            final JiraLocaleUtils jiraLocaleUtils,
            final EventPublisher eventPublisher,
            final BackingI18nFactory i18nBackingFactory,
            final UserLocaleStore userLocaleStore,
            final ComponentLocator locator,
            final ResourceBundleCacheCleaner resourceBundleCacheCleaner) {

        this.jiraLocaleUtils = jiraLocaleUtils;
        this.eventPublisher = eventPublisher;
        this.userLocaleStore = userLocaleStore;
        this.pluginAccessorSupplier = locator.getComponentSupplier(PluginAccessor.class);
        this.resourceBundleCacheCleaner = resourceBundleCacheCleaner;
        this.cache = buildCache(i18nBackingFactory);
    }

    private LoadingCache<Locale, I18nHelper> buildCache(final BackingI18nFactory i18nBackingFactory) {

        final CacheBuilder<Object, Object> cacheBuilder = CacheBuilder.newBuilder();

        return cacheBuilder.build(CacheLoader.from(
                locale -> {
                    final I18nHelper i18nHelper = i18nBackingFactory.create(locale, translationTransforms());
                    if (jiraStarted.get()) {
                        resourceBundleCacheCleaner.cleanPluginBundlesFromResourceBundleCache();
                    }
                    return i18nHelper;
                }
        ));
    }

    // Note: We can not use @EventComponent to take care of this for us because we
    // need to clear the cache after we've registered to make sure we haven't missed
    // anything.
    @Override
    public void start() {
        jiraStarted.set(true);
        clearCaches();
        resourceBundleCacheCleaner.cleanPluginBundlesFromResourceBundleCache();
        new GoogleCacheInstruments("i18n." + getClass().getSimpleName()).addCache(cache).install();
    }

    @Override
    public void afterInstantiation() {
        eventPublisher.register(this);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    @EventListener
    public void pluginEnabled(final PluginEnabledEvent event) {
        if (affectsI18n().test(event.getPlugin())) {
            clearCaches();
        }
    }

    @SuppressWarnings({"UnusedDeclaration"})
    @EventListener
    public void pluginModuleDisabled(final PluginModuleDisabledEvent event) {
        if (jiraStarted.get() || getPluginsTracker().isPluginInvolved(event.getModule())) {
            clearCaches();
        }
    }

    @SuppressWarnings({"UnusedDeclaration"})
    @EventListener
    public void pluginModuleEnabled(final PluginModuleEnabledEvent event) {
        if (jiraStarted.get() && affectsI18n().test(event.getModule().getPlugin())) {
            clearCaches();
        }
    }

    private Predicate<Plugin> affectsI18n() {
        return isPluginWithModuleDescriptor(LanguageModuleDescriptor.class)
                .or(isPluginWithResourceType(I18N_RESOURCE_TYPE))
                .or(isPluginWithModuleDescriptor(TranslationTransformModuleDescriptor.class));
    }

    @SuppressWarnings({"UnusedDeclaration"})
    @EventListener
    public void pluginRefreshed(final PluginRefreshedEvent event) {
        if (getPluginsTracker().isPluginInvolved(event.getPlugin())) {
            clearCaches();
        }
    }

    private void clearCaches() {
        involvedPluginsTracker.reset();
        cache.invalidateAll();
        jiraLocaleUtils.resetInstalledLocales();    // <-- synchronised method
        translationTransformsRef.reset();
    }

    @Override
    public I18nHelper getInstance(final Locale locale) {
        if (DefaultResourceBundle.isDefaultResourceBundleStale(locale)) {
            clearCaches();
        }
        try {
            return cache.getUnchecked(locale);
        } catch (final UncheckedExecutionException ex) {
            throw unwind(ex);
        }
    }

    @Override
    public I18nHelper getInstance(final ApplicationUser user) {
        return getInstance(userLocaleStore.getLocale(user));
    }

    private RuntimeException unwind(final RuntimeException re) {
        final Throwable cause = re.getCause();
        if (cause instanceof RuntimeException) {
            return (RuntimeException) cause;
        }

        if (cause instanceof Error) {
            throw (Error) cause;
        }

        return re;
    }

    /**
     * An opaque string that changes whenever the underlying i18n bundles change (e.g. when a new translation pack is
     * installed)
     */
    public String getStateHashCode() {
        return getPluginsTracker().getStateHashCode();
    }

    private PluginsTracker getPluginsTracker() {
        return involvedPluginsTracker.getOrCreate(this::createPluginsTracker);
    }

    private PluginsTracker createPluginsTracker() {
        final PluginAccessor pluginAccessor = pluginAccessorSupplier.get();
        final ImmutableList<Plugin> allInvolvedPlugins = pluginAccessor.getEnabledPlugins()
                .stream()
                .filter(affectsI18n())
                .collect(toImmutableList());
        return new SimplePluginsTracker(allInvolvedPlugins);
    }

    private Iterable<? extends TranslationTransform> translationTransforms() {
        return translationTransformsRef.getOrCreate(this::createTranslationTransforms);
    }

    private List<TranslationTransform> createTranslationTransforms() {
        final Stream<TranslationTransformModuleDescriptor> sortedDescriptors =
                pluginAccessorSupplier.get().getEnabledModuleDescriptorsByClass(TranslationTransformModuleDescriptor.class)
                        .stream().sorted(ModuleDescriptorComparator.COMPARATOR);
        return sortedDescriptors.map(TranslationTransformModuleDescriptor::getModule).collect(toImmutableList());
    }
}
