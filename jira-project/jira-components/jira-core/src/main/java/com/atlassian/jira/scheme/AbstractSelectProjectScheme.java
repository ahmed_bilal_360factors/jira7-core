package com.atlassian.jira.scheme;

import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.opensymphony.util.TextUtils;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collection;

import static com.atlassian.jira.security.Permissions.ADMINISTER;

public abstract class AbstractSelectProjectScheme extends AbstractProjectAndSchemeAwareAction {
    private String[] schemeIds = new String[]{""};

    @Override
    public String doDefault() throws Exception {
        if (getProject() != null) {
            final Scheme schemeFor = getSchemeManager().getSchemeFor(getProject());
            if (schemeFor != null && schemeFor.getId() != null) {
                setSchemeIds(new String[]{schemeFor.getId().toString()});
            }
        }

        if (hasPermission()) {
            return super.doDefault();
        } else {
            return "securitybreach";
        }
    }

    @Override
    protected void doValidation() {
        if (getProject() == null) {
            addErrorMessage("You must select a project for this scheme");
        }
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        getSchemeManager().removeSchemesFromProject(getProject());
        for (String schemeId : getSchemeIds()) {
            if (TextUtils.stringSet(schemeId)) {
                Scheme scheme = getSchemeManager().getSchemeObject(new Long(schemeId));
                getSchemeManager().addSchemeToProject(getProject(), scheme);
            }
        }

        if (hasPermission()) {
            return getRedirect(getProjectReturnUrl());
        } else {
            return "securitybreach";
        }
    }

    @Override
    public String getRedirectURL() {
        return null;
    }

    protected String getProjectReturnUrl() {
        return "/plugins/servlet/project-config/" + getProject().getKey() + "/summary";
    }

    protected boolean hasPermission() {
        return getPermissionManager().hasPermission(ADMINISTER, getLoggedInUser());
    }

    public Collection<GenericValue> getSchemes() throws GenericEntityException {
        return getSchemeManager().getSchemes();
    }

    public String[] getSchemeIds() {
        return schemeIds;
    }

    public void setSchemeIds(String[] schemeIds) {
        this.schemeIds = schemeIds;
    }
}
