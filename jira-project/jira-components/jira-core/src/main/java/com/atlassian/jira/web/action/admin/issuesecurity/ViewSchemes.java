package com.atlassian.jira.web.action.admin.issuesecurity;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.AbstractViewSchemes;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeManager;
import com.atlassian.sal.api.websudo.WebSudoRequired;

import java.util.List;

@WebSudoRequired
public class ViewSchemes extends AbstractViewSchemes {
    public SchemeManager getSchemeManager() {
        return ComponentAccessor.getComponent(IssueSecuritySchemeManager.class);
    }

    public String getRedirectURL() {
        return null;
    }

    public boolean isCanDelete(final Scheme scheme) {
        if (scheme != null) {
            final List<Project> projects = getProjects(scheme);
            if (projects == null || projects.isEmpty()) {
                return true;
            }
        }

        return false;
    }
}
