package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.cache.CacheManager;
import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.embedded.spi.GroupDao;
import com.atlassian.crowd.event.migration.XMLRestoreFinishedEvent;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.model.group.DelegatingGroupWithAttributes;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupWithAttributes;
import com.atlassian.crowd.model.group.InternalDirectoryGroup;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.util.Function;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.EntityCondition;
import org.ofbiz.core.entity.EntityConditionList;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static com.atlassian.jira.crowd.embedded.ofbiz.DirectoryEntityKey.getKeyLowerCase;
import static com.atlassian.jira.crowd.embedded.ofbiz.GroupEntity.DESCRIPTION;
import static com.atlassian.jira.crowd.embedded.ofbiz.GroupEntity.ENTITY;
import static com.atlassian.jira.crowd.embedded.ofbiz.GroupEntity.LOWER_DESCRIPTION;
import static com.atlassian.jira.crowd.embedded.ofbiz.PrimitiveMap.of;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static java.util.Collections.singletonList;

public class OfBizGroupDao implements GroupDao {
    private final OfBizDelegator ofBizDelegator;
    private final InternalMembershipDao membershipDao;

    /**
     * Lazy cache of groups by group name.
     */
    private final OfBizGroupCache groupCaseInsensitiveCache;

    public OfBizGroupDao(final OfBizDelegator ofBizDelegator, final DirectoryDao directoryDao,
                         final InternalMembershipDao membershipDao, final CacheManager cacheManager,
                         final ClusterLockService clusterLockService, final ApplicationProperties applicationProperties) {
        this.ofBizDelegator = ofBizDelegator;
        this.membershipDao = membershipDao;

        if (applicationProperties.getOption(APKeys.CACHE_ALL_USERS_AND_GROUPS)) {
            this.groupCaseInsensitiveCache = new EagerOfBizGroupCache(clusterLockService, cacheManager, directoryDao, ofBizDelegator);
        } else {
            this.groupCaseInsensitiveCache = new LazyOfBizGroupCache(cacheManager, ofBizDelegator);
        }
    }

    @Nonnull
    @Override
    public InternalDirectoryGroup findByName(final long directoryId, @Nonnull final String name) throws GroupNotFoundException {
        return findOfBizGroup(directoryId, name);
    }

    @Nonnull
    OfBizGroup findOfBizGroup(final long directoryId, final String name) throws GroupNotFoundException {
        final OfBizGroup group = findByNameOrNull(directoryId, name);
        if (group == null) {
            // Because the SPI says we should do this.
            throw new GroupNotFoundException(name);
        }
        return group;
    }


    /**
     * Tries to find the group by name and returns {@code null} if not found.
     * Just like the public method should have done in the first place!
     *
     * @param directoryId Directory ID
     * @param name        the group name
     * @return the group, or {@code null} if the group does not exist
     */
    @Nullable
    OfBizGroup findByNameOrNull(final long directoryId, final String name) {
        return groupCaseInsensitiveCache.getCaseInsensitive(directoryId, name);
    }

    @Override
    public GroupWithAttributes findByNameWithAttributes(final long directoryId, final String name)
            throws GroupNotFoundException {
        return withAttributes(findOfBizGroup(directoryId, name));
    }

    private GroupWithAttributes withAttributes(final OfBizGroup group) {
        final List<GenericValue> attributes = Select.columns(OfBizAttributesBuilder.SUPPORTED_FIELDS)
                .from(GroupAttributeEntity.ENTITY)
                .whereEqual(GroupAttributeEntity.DIRECTORY_ID, group.getDirectoryId())
                .andEqual(GroupAttributeEntity.GROUP_ID, group.getId())
                .runWith(ofBizDelegator)
                .asList();
        return new DelegatingGroupWithAttributes(group, OfBizAttributesBuilder.toAttributes(attributes));
    }

    @Override
    public Group add(final Group group) {
        return add(group, false);
    }

    @Override
    public Group addLocal(final Group group) {
        return add(group, true);
    }

    private Group add(final Group group, boolean local) {
        // Create the new Group in the DB
        final Timestamp currentTimestamp = getCurrentTimestamp();
        final Map<String, Object> groupData = GroupEntity.getData(group, currentTimestamp, currentTimestamp, local);
        final GenericValue gvGroup = ofBizDelegator.createValue(GroupEntity.ENTITY, groupData);
        final OfBizGroup ofBizGroup = OfBizGroup.from(gvGroup);
        groupCaseInsensitiveCache.refresh(ofBizGroup);

        return ofBizGroup;
    }

    @Override
    public BatchResult<Group> addAll(Set<? extends Group> groups) throws DirectoryNotFoundException {
        BatchResult<Group> results = new BatchResult<>(groups.size());
        for (Group group : groups) {
            try {
                final Group addedGroup = add(group);
                results.addSuccess(addedGroup);
            } catch (DataAccessException e) {
                // Try to catch problems so that at least the *other* groups will be added
                results.addFailure(group);
            }
        }
        return results;
    }

    @Override
    public Group update(final Group group) throws GroupNotFoundException {
        // Get the latest GenericValue from the DB
        final GenericValue groupGenericValue = findGroupGenericValue(group);
        // Update the relevant values
        groupGenericValue.set(GroupEntity.ACTIVE, BooleanUtils.toInteger(group.isActive()));
        groupGenericValue.set(GroupEntity.UPDATED_DATE, getCurrentTimestamp());
        groupGenericValue.set(GroupEntity.DESCRIPTION, group.getDescription());
        groupGenericValue.set(GroupEntity.LOWER_DESCRIPTION, toLowerCaseAllowNull(group.getDescription()));
        // Save to DB
        storeGroup(groupGenericValue);
        // Convert GenericValue to an object
        OfBizGroup ofBizGroup = OfBizGroup.from(groupGenericValue);
        // Shove it in the cache
        groupCaseInsensitiveCache.refresh(ofBizGroup);

        return ofBizGroup;
    }

    private void storeGroup(final GenericValue groupGenericValue) {
        // JRA-43495: Make sure that the description is MAX_DESCRIPTION_LENGTH characters. Some providers will provide more data, so we truncate it.
        if (groupGenericValue.get(DESCRIPTION) != null
                && ((String) groupGenericValue.get(DESCRIPTION)).length() > GroupEntity.MAX_DESCRIPTION_LENGTH) {
            groupGenericValue.set(DESCRIPTION, GroupEntity.truncateDescriptionIfRequired(groupGenericValue.getString(DESCRIPTION)));
        }
        if (groupGenericValue.get(LOWER_DESCRIPTION) != null
                && ((String) groupGenericValue.get(LOWER_DESCRIPTION)).length() > GroupEntity.MAX_DESCRIPTION_LENGTH) {
            groupGenericValue.set(LOWER_DESCRIPTION, GroupEntity.truncateDescriptionIfRequired(groupGenericValue.getString(LOWER_DESCRIPTION)));
        }
        ofBizDelegator.store(groupGenericValue);
    }

    @Override
    public Group rename(final Group group, final String newName) {
        throw new UnsupportedOperationException("Renaming groups is not supported!");
    }

    @Override
    public void storeAttributes(final Group group, final Map<String, Set<String>> attributes)
            throws GroupNotFoundException {
        for (final Map.Entry<String, Set<String>> attribute : notNull(attributes).entrySet()) {
            // remove attributes before adding new ones.
            // Duplicate key values are allowed, but we always add as a complete set under the key.
            removeAttribute(group, attribute.getKey());
            if ((attribute.getValue() != null) && !attribute.getValue().isEmpty()) {
                storeAttributeValues(group, attribute.getKey(), attribute.getValue());
            }
        }
    }

    private void storeAttributeValues(final Group group, final String name, final Set<String> values)
            throws GroupNotFoundException {
        for (final String value : values) {
            if (StringUtils.isNotEmpty(value)) {
                storeAttributeValue(group, name, value);
            }
        }
    }

    private void storeAttributeValue(final Group group, final String name, final String value)
            throws GroupNotFoundException {
        final GenericValue groupGenericValue = findGroupGenericValue(group);
        ofBizDelegator.createValue(GroupAttributeEntity.ENTITY, GroupAttributeEntity.getData(group.getDirectoryId(),
                groupGenericValue.getLong(GroupEntity.ID), name, value));
    }


    @Override
    public void removeAttribute(final Group group, final String attributeName) throws GroupNotFoundException {
        notNull(attributeName);
        final GenericValue gv = findGroupGenericValue(group);
        ofBizDelegator.removeByAnd(GroupAttributeEntity.ENTITY, of(GroupAttributeEntity.GROUP_ID, gv.getLong(GroupEntity.ID),
                GroupAttributeEntity.NAME, attributeName));
    }

    @Override
    public void remove(final Group group) throws GroupNotFoundException {
        final GenericValue groupGenericValue = findGroupGenericValue(group);

        try {
            // remove memberships
            membershipDao.removeAllMembersFromGroup(group);
            membershipDao.removeAllGroupMemberships(group);

            ofBizDelegator.removeByAnd(GroupAttributeEntity.ENTITY, of(GroupAttributeEntity.GROUP_ID, groupGenericValue.getLong(GroupEntity.ID)));
            ofBizDelegator.removeValue(groupGenericValue);
        } finally {
            groupCaseInsensitiveCache.remove(getKeyLowerCase(group));
        }
    }

    @Override
    public <T> List<T> search(final long directoryId, final EntityQuery<T> query) {
        final SearchRestriction searchRestriction = query.getSearchRestriction();
        final EntityCondition baseCondition = new GroupEntityConditionFactory(ofBizDelegator).getEntityConditionFor(searchRestriction);
        final EntityExpr directoryCondition = new EntityExpr(GroupEntity.DIRECTORY_ID, EntityOperator.EQUALS, directoryId);
        final EntityCondition entityCondition;
        final List<EntityCondition> entityConditions = new ArrayList<>(2);
        if (baseCondition != null) {
            entityConditions.add(baseCondition);
        }
        entityConditions.add(directoryCondition);
        entityCondition = new EntityConditionList(entityConditions, EntityOperator.AND);

        final Function<GenericValue, T> valueFunction = getTransformer(query.getReturnType());

        final List<T> results = Select.from(ENTITY)
                .whereCondition(entityCondition)
                .orderBy(GroupEntity.NAME)
                .limit(query.getStartIndex(), query.getMaxResults())
                .runWith(ofBizDelegator)
                .asList(valueFunction);

        return results;
    }

    @SuppressWarnings("unchecked")
    private <T> Function<GenericValue, T> getTransformer(final Class<T> returnType) {
        if (returnType.equals(String.class)) {
            return (Function<GenericValue, T>) TO_GROUPNAME_FUNCTION;
        }
        if (returnType.isAssignableFrom(OfBizGroup.class)) {
            return (Function<GenericValue, T>) TO_GROUP_FUNCTION;
        }
        if (returnType.isAssignableFrom(GroupWithAttributes.class)) {
            return (Function<GenericValue, T>) toGroupWithAttributesFunction;
        }

        throw new IllegalArgumentException("Class type for return values ('" + returnType + "') is not supported");
    }

    static final Function<GenericValue, String> TO_GROUPNAME_FUNCTION = gv -> gv.getString(GroupEntity.NAME);

    static final Function<GenericValue, OfBizGroup> TO_GROUP_FUNCTION = OfBizGroup::from;

    final Function<GenericValue, GroupWithAttributes> toGroupWithAttributesFunction = gv -> withAttributes(TO_GROUP_FUNCTION.get(gv));

    @Override
    public BatchResult<String> removeAllGroups(long directoryId, Set<String> groupNames) {
        BatchResult<String> results = new BatchResult<>(groupNames.size());
        for (String groupName : groupNames) {
            try {
                remove(findByName(directoryId, groupName));
                results.addSuccess(groupName);
            } catch (GroupNotFoundException e) {
                results.addFailure(groupName);
            }
        }
        return results;
    }

    /**
     * Invoked by {@link OfBizCacheFlushingManager} to ensure caches are being flushed in the right order on
     * {@link XMLRestoreFinishedEvent}
     */
    public void flushCache() {
        groupCaseInsensitiveCache.refresh();
    }

    private GenericValue findGroupGenericValue(final Group group) throws GroupNotFoundException {
        return findGroupGenericValue(group.getDirectoryId(), group.getName());
    }

    // Note: These are relatively expensive as it fetches *all* fields.
    // Only use them to get fresh info for an update request, not to load these more generally.
    GenericValue findGroupGenericValue(final long directoryId, final String name) throws GroupNotFoundException {
        notNull("name", name);
        final GenericValue groupGenericValue = Select.from(GroupEntity.ENTITY)
                .whereEqual(GroupEntity.DIRECTORY_ID, directoryId)
                .andEqual(GroupEntity.LOWER_NAME, toLowerCase(name))
                .runWith(ofBizDelegator)
                .singleValue();
        if (groupGenericValue == null) {
            throw new GroupNotFoundException(name);
        }
        return groupGenericValue;
    }

    @Nullable
    private static String toLowerCaseAllowNull(final String value) {
        return (value != null) ? toLowerCase(value) : null;
    }

    private static Timestamp getCurrentTimestamp() {
        return new Timestamp(System.currentTimeMillis());
    }
}
