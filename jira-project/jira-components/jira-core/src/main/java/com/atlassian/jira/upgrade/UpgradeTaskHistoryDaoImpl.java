package com.atlassian.jira.upgrade;

import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.model.querydsl.UpgradeHistoryDTO;
import com.atlassian.jira.model.querydsl.UpgradeTaskHistoryDTO;
import com.atlassian.upgrade.core.HostUpgradeTaskCollector;
import com.atlassian.upgrade.core.dao.UpgradeTaskHistory;
import com.atlassian.upgrade.core.dao.UpgradeTaskHistoryDao;
import com.querydsl.sql.SQLExpressions;
import org.joda.time.DateTimeUtils;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.Collectors;

import static com.atlassian.jira.model.querydsl.QUpgradeHistory.UPGRADE_HISTORY;
import static com.atlassian.jira.model.querydsl.QUpgradeTaskHistory.UPGRADE_TASK_HISTORY;
import static com.atlassian.jira.model.querydsl.QUpgradeTaskHistoryAuditLog.UPGRADE_TASK_HISTORY_AUDIT_LOG;
import static com.atlassian.upgrade.core.dao.UpgradeTaskHistory.Status.COMPLETED;

/**
 * DAO used to store the current build version of the host and the plugins.
 */
public class UpgradeTaskHistoryDaoImpl implements UpgradeTaskHistoryDao {

    private static final String TASK_ACTION_UPGRADE = "upgrade";
    private final QueryDslAccessor queryDslAccessor;


    public UpgradeTaskHistoryDaoImpl(final QueryDslAccessor queryDslAccessor) {
        this.queryDslAccessor = queryDslAccessor;
    }

    @Override
    public int createUpgradeTaskHistory(final UpgradeTaskHistory upgradeTaskHistory) {
        final long id = queryDslAccessor.withNewConnection().executeQuery(dbConnection -> dbConnection
                .insert(UPGRADE_TASK_HISTORY)
                .set(UPGRADE_TASK_HISTORY.buildNumber, upgradeTaskHistory.getBuildNumber())
                .set(UPGRADE_TASK_HISTORY.status, upgradeTaskHistory.getStatus().toString())
                .set(UPGRADE_TASK_HISTORY.upgradeTaskFactoryKey, upgradeTaskHistory.getUpgradeTaskFactoryKey())
                .set(UPGRADE_TASK_HISTORY.upgradeType, upgradeTaskHistory.getUpgradeType().toString())
                .executeWithId());

        return (int) id;
    }

    @Override
    public void updateStatus(final int id, final UpgradeTaskHistory.Status status) {
        queryDslAccessor.withNewConnection().execute(dbConnection -> {
            final long rowsUpdated = dbConnection
                    .update(UPGRADE_TASK_HISTORY)
                    .set(UPGRADE_TASK_HISTORY.status, status.toString())
                    .where(UPGRADE_TASK_HISTORY.id.eq((long) id))
                    .execute();
            if (status != UpgradeTaskHistory.Status.IN_PROGRESS && rowsUpdated > 0) {
                final Timestamp time = new Timestamp(DateTimeUtils.currentTimeMillis());
                final UpgradeTaskHistoryDTO taskHistory = dbConnection.newSqlQuery()
                        .select(UPGRADE_TASK_HISTORY)
                        .from(UPGRADE_TASK_HISTORY)
                        .where(UPGRADE_TASK_HISTORY.id.eq((long) id))
                        .fetchOne();
                dbConnection
                        .insert(UPGRADE_TASK_HISTORY_AUDIT_LOG)
                        .set(UPGRADE_TASK_HISTORY_AUDIT_LOG.buildNumber, taskHistory.getBuildNumber())
                        .set(UPGRADE_TASK_HISTORY_AUDIT_LOG.status, taskHistory.getStatus())
                        .set(UPGRADE_TASK_HISTORY_AUDIT_LOG.upgradeTaskFactoryKey, taskHistory.getUpgradeTaskFactoryKey())
                        .set(UPGRADE_TASK_HISTORY_AUDIT_LOG.upgradeType, taskHistory.getUpgradeType())
                        .set(UPGRADE_TASK_HISTORY_AUDIT_LOG.timeperformed, time)
                        .set(UPGRADE_TASK_HISTORY_AUDIT_LOG.action, TASK_ACTION_UPGRADE)
                        .executeWithId();
            }
        });
    }

    @Override
    public Optional<Integer> getIdByBuildNumber(final String upgradeFactoryKey, final int buildNumber) {
        return Optional.ofNullable(queryDslAccessor.withNewConnection()
                .executeQuery(dbConnection -> dbConnection.newSqlQuery()
                        .select(SQLExpressions.select(UPGRADE_TASK_HISTORY.id))
                        .from(UPGRADE_TASK_HISTORY)
                        .where(UPGRADE_TASK_HISTORY.upgradeTaskFactoryKey.eq(upgradeFactoryKey)
                                .and(UPGRADE_TASK_HISTORY.buildNumber.eq(buildNumber)))
                        .fetchOne()))
                .map(Long::intValue);
    }

    @Override
    public int getDatabaseBuildNumber(final String upgradeFactoryKey) {
        final List<UpgradeTaskHistoryDTO> results = queryDslAccessor.withNewConnection()
                .executeQuery(dbConnection -> dbConnection.newSqlQuery()
                        .select(UPGRADE_TASK_HISTORY)
                        .from(UPGRADE_TASK_HISTORY)
                        .where(UPGRADE_TASK_HISTORY.upgradeTaskFactoryKey.eq(upgradeFactoryKey)
                                .and(UPGRADE_TASK_HISTORY.status.eq(COMPLETED.name())))
                        .fetch());

        //A SQL Max selector did not work in the above query so needed to do the own max here.
        final OptionalInt optionalBuildNumber = results.stream()
                .mapToInt(UpgradeTaskHistoryDTO::getBuildNumber)
                .max();

        if (optionalBuildNumber.isPresent()) {
            return optionalBuildNumber.getAsInt();
        }

        //If it is the host and there are no values in the new UpgradeTaskHistory table we get the build number from
        //the old table
        if (HostUpgradeTaskCollector.HOST_APP_KEY.equals(upgradeFactoryKey)) {
            final OptionalInt hostBuildNumber = getBuildNumberFromUpgradeHistoryTable();

            // In cloud we threw if the buildnumber is 0 but there are test data, such as TestEmpty.xml
            // that has no upgrade history so 0 is a valid version in this case.
            return hostBuildNumber.orElse(0);
        } else {
            return 0;
        }
    }

    /**
     * <p>
     *     This gets the build number from the old table. As the UpgradeHistory table has changed over time there are
     *     different cases that must be considered for getting the build number.
     * </p>
     * <p>
     *     The first case is that the data has been upgraded above build number 64006 where we added the status. We
     *     search for any of these builds that have the status as complete and grab the highest one.
     * </p>
     * <p>
     *     If there are no upgrades with the complete status, we look for the highest build with a targetbuild value set.
     *     The value here must not have a value in the status field. This covers builds above 519 (4.1 version).
     * </p>
     * <p>
     *     If there are no upgrades with a target build number set we look for the highest upgrade from the upgrade class.
     *     We convert the upgrade class string to a build number, which is fine because they all have the same format:
     *     com.atlassian.jira.upgrade.tasks.UpgradeTask_Buildxxx
     * </p>
     *
     * @return the optional build number that was able to be found in the old table
     */
    private OptionalInt getBuildNumberFromUpgradeHistoryTable() {
        return queryDslAccessor.withNewConnection()
                    .executeQuery(dbConnection -> {
                        final List<UpgradeHistoryDTO> upgrades = dbConnection.newSqlQuery()
                                .select(UPGRADE_HISTORY)
                                .from(UPGRADE_HISTORY)
                                .fetch();

                        //After build 64006 we had a status value in the upgrade tasks
                        final OptionalInt versionAbove64006 = upgrades.stream()
                                .filter(upgrade -> upgrade.getStatus() != null && upgrade.getStatus().equals("complete"))
                                .filter(upgrade -> upgrade.getTargetbuild() != null && upgrade.getTargetbuild().matches("^\\d+$"))
                                .mapToInt(upgrade -> Integer.parseInt(upgrade.getTargetbuild()))
                                .max();

                        if (versionAbove64006.isPresent()) {
                            return versionAbove64006;
                        }

                        //Build 519 = 4.1 and higher version had the targetbuild set for all of the upgrade tasks.
                        final OptionalInt versionAbove519 = upgrades.stream()
                                .filter(upgrade -> (upgrade.getStatus() == null || upgrade.getStatus().equals("")))
                                .filter(upgrade -> (upgrade.getTargetbuild() != null && upgrade.getTargetbuild().matches("^\\d+$")))
                                .mapToInt(upgrade -> Integer.parseInt(upgrade.getTargetbuild()))
                                .max();

                        if (versionAbove519.isPresent()) {
                            return versionAbove519;
                        }

                        //Any version above 466 but below 519 had no targetbuild and so the upgradeclass is needing
                        //to be used to get the buildnumber
                        //noinspection UnnecessaryLocalVariable
                        final OptionalInt versionAbove466 = upgrades.stream()
                                .filter(upgrade -> (upgrade.getTargetbuild() == null || upgrade.getTargetbuild().equals("")))
                                .filter(upgrade -> (upgrade.getUpgradeclass() != null && upgrade.getUpgradeclass().matches("^com.atlassian.jira.upgrade.tasks.UpgradeTask_Build\\d+$")))
                                .mapToInt(upgrade -> {
                                    String upgradeClass = upgrade.getUpgradeclass();

                                    String buildNumberString = upgradeClass.replace("com.atlassian.jira.upgrade.tasks.UpgradeTask_Build", "");

                                    return Integer.parseInt(buildNumberString);
                                })
                                .max();

                        return versionAbove466;
                    });
    }

    @Override
    public Collection<UpgradeTaskHistory> getAllUpgradeTaskHistory(final String upgradeFactoryKey) {
        final List<UpgradeTaskHistoryDTO> upgradeTaskHistoryDTOs = queryDslAccessor.withNewConnection()
                .executeQuery(dbConnection -> dbConnection.newSqlQuery()
                        .select(UPGRADE_TASK_HISTORY)
                        .from(UPGRADE_TASK_HISTORY)
                        .where(UPGRADE_TASK_HISTORY.upgradeTaskFactoryKey.eq(upgradeFactoryKey))
                        .fetch());

        return upgradeTaskHistoryDTOs.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    private UpgradeTaskHistory convert(final UpgradeTaskHistoryDTO dto) {
        return new UpgradeTaskHistory(
                dto.getId().intValue(),
                dto.getUpgradeTaskFactoryKey(),
                dto.getBuildNumber(),
                UpgradeTaskHistory.Status.valueOf(dto.getStatus()),
                UpgradeTaskHistory.UpgradeType.valueOf(dto.getUpgradeType()));
    }
}
