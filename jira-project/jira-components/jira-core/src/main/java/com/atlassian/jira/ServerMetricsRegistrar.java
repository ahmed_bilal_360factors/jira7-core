package com.atlassian.jira;

import com.atlassian.jira.servermetrics.MultiThreadedRequestKeyResolver;
import com.atlassian.jira.servermetrics.MultiThreadedRequestMetricsCollector;
import com.atlassian.jira.servermetrics.RequestKeyResolver;
import com.atlassian.jira.servermetrics.RequestMetricsDispatcher;
import com.atlassian.jira.servermetrics.ServerMetricsDetailCollector;
import com.atlassian.jira.servermetrics.TimingInformationToEvent;
import com.google.common.base.Ticker;

/**
 * Register components for server metrics.
 *
 * @since v7.2
 */
public class ServerMetricsRegistrar {
    static void registerServerMetrics(ComponentContainer register) {
        final MultiThreadedRequestMetricsCollector serverMetricsDataCollector =
                new MultiThreadedRequestMetricsCollector(Ticker.systemTicker());

        register.instance(ComponentContainer.Scope.PROVIDED, ServerMetricsDetailCollector.class, serverMetricsDataCollector);
        register.instance(ComponentContainer.Scope.INTERNAL, MultiThreadedRequestMetricsCollector.class, serverMetricsDataCollector);
        register.implementation(ComponentContainer.Scope.INTERNAL, RequestMetricsDispatcher.class);
        register.implementation(ComponentContainer.Scope.INTERNAL, TimingInformationToEvent.class);
        register.implementation(ComponentContainer.Scope.INTERNAL, MultiThreadedRequestKeyResolver.class);
        register.implementation(ComponentContainer.Scope.INTERNAL, RequestKeyResolver.class);
    }
}
