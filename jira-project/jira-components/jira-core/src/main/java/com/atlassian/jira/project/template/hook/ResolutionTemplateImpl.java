package com.atlassian.jira.project.template.hook;

import org.codehaus.jackson.annotate.JsonProperty;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.nullToEmpty;

public class ResolutionTemplateImpl implements ResolutionTemplate {
    private final String name;
    private final String description;

    public ResolutionTemplateImpl(
            @JsonProperty("name") String name,
            @JsonProperty("description") String description) {
        this.name = checkNotNull(name);
        this.description = nullToEmpty(description);
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String description() {
        return description;
    }
}
