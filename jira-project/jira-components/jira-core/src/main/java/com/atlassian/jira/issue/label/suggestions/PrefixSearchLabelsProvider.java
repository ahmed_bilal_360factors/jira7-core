package com.atlassian.jira.issue.label.suggestions;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.statistics.util.PrefixFieldableHitCollector;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.PrefixQuery;
import org.apache.lucene.search.Query;

import java.util.Set;
import java.util.TreeSet;

public class PrefixSearchLabelsProvider {
    private final IssueIndexManager issueIndexManager;

    public PrefixSearchLabelsProvider(final IssueIndexManager issueIndexManager) {
        this.issueIndexManager = issueIndexManager;
    }

    public Set<String> findByPrefixToken(final String prefix, final String searchField, final String displayField, final ApplicationUser searchUser) {
        final Set<String> suggestions = new TreeSet<>();

        try {
            getSearchProvider().search(
                    new SearchRequest().getQuery(),
                    searchUser,
                    createCollector(prefix, displayField, suggestions),
                    createLuceneQuery(prefix, searchField)
            );
        } catch (SearchException e) {
            throw new RuntimeException(e);
        }

        return suggestions;
    }

    private PrefixFieldableHitCollector createCollector(final String prefix, final String displayField, final Set<String> suggestions) {
        return new PrefixFieldableHitCollector(
                issueIndexManager.getIssueSearcher(),
                displayField,
                prefix,
                suggestions
        );
    }

    private Query createLuceneQuery(final String prefix, final String searchField) {
        return new PrefixQuery(new Term(searchField, prefix.toLowerCase()));
    }

    private SearchProvider getSearchProvider() {
        return ComponentAccessor.getComponentOfType(SearchProvider.class);
    }
}
