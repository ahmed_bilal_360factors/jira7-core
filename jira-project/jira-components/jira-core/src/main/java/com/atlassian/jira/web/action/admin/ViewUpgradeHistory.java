package com.atlassian.jira.web.action.admin;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.LocaleManager;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.startup.PluginInfoProvider;
import com.atlassian.jira.upgrade.UpgradeVersionHistoryItem;

import java.util.Date;
import java.util.List;

/**
 * @since v4.1
 */
public class ViewUpgradeHistory extends ViewSystemInfo {
    public ViewUpgradeHistory(LocaleManager localeManager, final PluginInfoProvider pluginInfoProvider, FeatureManager featureManager) {
        super(localeManager, pluginInfoProvider, featureManager);
    }

    public List<UpgradeVersionHistoryItem> getUpgradeHistory() {
        return getExtendedSystemInfoUtils().getUpgradeHistory();
    }

    public String getFormattedTimePerformed(final Date timePerformed) {
        if (timePerformed == null) {
            return getText("common.words.unknown");
        } else {
            return getDateTimeFormatter().withStyle(DateTimeStyle.COMPLETE).format(timePerformed);
        }
    }
}
