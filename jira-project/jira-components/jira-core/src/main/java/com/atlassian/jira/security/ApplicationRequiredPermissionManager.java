package com.atlassian.jira.security;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.permission.ProjectPermissionCategory;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.ImmutableList;
import com.opensymphony.workflow.loader.ActionDescriptor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * This is a {@link PermissionManager} to ensure that any user has an application
 * ({@link ApplicationRoleManager#hasAnyRole(ApplicationUser)}). The user's permissions are completely revoked
 * when the user has no application access.
 * <p>
 * This is a delegate that redirects all requests to a {@code PermissionManager}. When the user has access
 * to at least one application it redirects the call for the delegate to compute. When the user does not have access
 * it simply returns as if the delegate returned denied (without calling the delegate).
 *
 * @since v7.0
 */
public class ApplicationRequiredPermissionManager implements PermissionManager {
    private final PermissionManager permissionManager;

    public ApplicationRequiredPermissionManager(final PermissionManager permissionManager) {
        this.permissionManager = notNull("permissionManager", permissionManager);
    }

    @Override
    public Collection<ProjectPermission> getAllProjectPermissions() {
        return permissionManager.getAllProjectPermissions();
    }

    @Override
    public Collection<ProjectPermission> getProjectPermissions(@Nonnull final ProjectPermissionCategory category) {
        return permissionManager.getProjectPermissions(category);
    }

    @Override
    @Deprecated
    public boolean hasPermission(final int permissionsId, final ApplicationUser user) {
        //This is a global permission.
        return permissionManager.hasPermission(permissionsId, user);
    }

    @Override
    public Option<ProjectPermission> getProjectPermission(@Nonnull final ProjectPermissionKey permissionKey) {
        return permissionManager.getProjectPermission(permissionKey);
    }

    @Override
    @Deprecated
    public boolean hasPermission(final int permissionsId, final Issue issue, final ApplicationUser user) {
        return checkUserHasApplicationOrFalse(user, () -> permissionManager.hasPermission(permissionsId, issue, user));
    }

    @Override
    public boolean hasPermission(@Nonnull final ProjectPermissionKey permissionKey, @Nonnull final Issue issue, @Nullable final ApplicationUser user) {
        return checkUserHasApplicationOrFalse(user, () -> permissionManager.hasPermission(permissionKey, issue, user));
    }

    @Override
    public boolean hasPermission(@Nonnull final ProjectPermissionKey permissionKey, @Nonnull final Issue issue, @Nullable final ApplicationUser user, @Nullable final ActionDescriptor actionDescriptor) {
        return checkUserHasApplicationOrFalse(user, () -> permissionManager.hasPermission(permissionKey, issue, user, actionDescriptor));
    }

    @Override
    public boolean hasPermission(@Nonnull final ProjectPermissionKey permissionKey, @Nonnull final Issue issue, @Nullable final ApplicationUser user, @Nonnull final Status status) {
        return checkUserHasApplicationOrFalse(user, () -> permissionManager.hasPermission(permissionKey, issue, user, status));
    }

    @Override
    @Deprecated
    public boolean hasPermission(final int permissionsId, final Project project, final ApplicationUser user) {
        return checkUserHasApplicationOrFalse(user, () -> permissionManager.hasPermission(permissionsId, project, user));
    }

    @Override
    public boolean hasPermission(@Nonnull final ProjectPermissionKey permissionKey, @Nonnull final Project project, @Nullable final ApplicationUser user) {
        return checkUserHasApplicationOrFalse(user, () -> permissionManager.hasPermission(permissionKey, project, user));
    }

    @Nonnull
    @Override
    public ProjectWidePermission hasProjectWidePermission(@Nonnull final ProjectPermissionKey permissionKey,
                                                          @Nonnull final Project project, @Nullable final ApplicationUser user) {
        return checkUserHasApplicationOrElse(user,
                () -> permissionManager.hasProjectWidePermission(permissionKey, project, user),
                () -> ProjectWidePermission.NO_ISSUES);
    }

    @Override
    @Deprecated
    public boolean hasPermission(final int permissionsId, final Project project, final ApplicationUser user, final boolean issueCreation) {
        return checkUserHasApplicationOrFalse(user, () -> permissionManager.hasPermission(permissionsId, project, user, issueCreation));
    }

    @Override
    public boolean hasPermission(@Nonnull final ProjectPermissionKey permissionKey, @Nonnull final Project project, @Nullable final ApplicationUser user, final boolean issueCreation) {
        return checkUserHasApplicationOrFalse(user, () -> permissionManager.hasPermission(permissionKey, project, user, issueCreation));
    }

    @Override
    public void removeGroupPermissions(final String group) throws RemoveException {
        permissionManager.removeGroupPermissions(group);
    }

    @Override
    public void removeUserPermissions(final ApplicationUser user) throws RemoveException {
        permissionManager.removeUserPermissions(user);
    }

    @Override
    @Deprecated
    public boolean hasProjects(final int permissionId, final ApplicationUser user) {
        return checkUserHasApplicationOrFalse(user, () -> permissionManager.hasProjects(permissionId, user));
    }

    @Override
    public boolean hasProjects(@Nonnull final ProjectPermissionKey permissionKey, @Nullable final ApplicationUser user) {
        return checkUserHasApplicationOrFalse(user, () -> permissionManager.hasProjects(permissionKey, user));
    }

    @Override
    @Deprecated
    public Collection<Project> getProjects(final int permissionId, final ApplicationUser user) {
        return checkUserHasApplicationOrEmpty(user, () -> permissionManager.getProjects(permissionId, user));
    }

    @Override
    public Collection<Project> getProjects(@Nonnull final ProjectPermissionKey permissionKey, @Nullable final ApplicationUser user) {
        return checkUserHasApplicationOrEmpty(user, () -> permissionManager.getProjects(permissionKey, user));
    }

    @Override
    @Deprecated
    public Collection<Project> getProjects(final int permissionId, final ApplicationUser user, final ProjectCategory projectCategory) {
        return checkUserHasApplicationOrEmpty(user, () -> permissionManager.getProjects(permissionId, user, projectCategory));
    }

    @Override
    public Collection<Project> getProjects(@Nonnull final ProjectPermissionKey permissionKey, @Nullable final ApplicationUser user, @Nullable final ProjectCategory projectCategory) {
        return checkUserHasApplicationOrEmpty(user, () -> permissionManager.getProjects(permissionKey, user, projectCategory));
    }

    @Override
    public void flushCache() {
        permissionManager.flushCache();
    }

    @Override
    public Collection<Group> getAllGroups(final int permissionId, final Project project) {
        return permissionManager.getAllGroups(permissionId, project);
    }

    private ApplicationRoleManager applicationRoleManager() {
        return ComponentAccessor.getComponent(ApplicationRoleManager.class);
    }

    private boolean checkUserHasApplicationOrFalse(ApplicationUser user, BooleanSupplier delegate) {
        final GlobalPermissionManager globalPermissions = ComponentAccessor.getGlobalPermissionManager();
        final ApplicationRoleManager roleManager = applicationRoleManager();

        return (!globalPermissions.hasPermission(GlobalPermissionKey.ADMINISTER, user)
                || roleManager.hasAnyRole(user)) && delegate.getAsBoolean();
    }

    private Collection<Project> checkUserHasApplicationOrEmpty(ApplicationUser user, Supplier<Collection<Project>> delegate) {
        return checkUserHasApplicationOrElse(user, delegate, ImmutableList::of);
    }

    private <T> T checkUserHasApplicationOrElse(ApplicationUser user,
                                                Supplier<T> application,
                                                Supplier<? extends T> noApplication) {
        final GlobalPermissionManager globalPermissions = ComponentAccessor.getGlobalPermissionManager();

        final ApplicationRoleManager roleManager = applicationRoleManager();
        if (!globalPermissions.hasPermission(GlobalPermissionKey.ADMINISTER, user)
                || roleManager.hasAnyRole(user)) {
            return application.get();
        } else {
            return noApplication.get();
        }
    }
}
