package com.atlassian.jira.user.util;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.user.ApplicationUserEntity;

import java.util.Optional;

/**
 * @since v6.0
 */
@Internal
public interface UserKeyStore {
    String getUsernameForKey(String key);

    String getKeyForUsername(String username);

    @Internal
    void renameUser(String oldUsername, String newUsername);

    @Internal
    String ensureUniqueKeyForNewUser(String username);

    @Internal
    Long getIdForUserKey(String name);

    @Internal
    Optional<ApplicationUserEntity> getUserForId(Long id);

    @Internal
    Optional<ApplicationUserEntity> getUserForKey(String key);

    @Internal
    Optional<ApplicationUserEntity> getUserForUsername(String username);

    /**
     * <strong>It is dangerous to use this method.</strong> In general operation JIRA needs to keep the userName -> key
     * mapping around even if the user is deleted. This is so that it can show something in the UI after the user is
     * deleted.
     * <p>
     * This method is used by cloud for import where all the users are deleted anyways (see JiraUserServiceImpl).
     *
     * @param key the key to remove.
     * @return the name of the user deleted.
     */
    @Internal
    String removeByKey(String key);
}
