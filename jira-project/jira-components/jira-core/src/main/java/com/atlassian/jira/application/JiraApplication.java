package com.atlassian.jira.application;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationAccess;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.atlassian.jira.project.type.ProjectType;
import com.atlassian.sal.api.license.SingleProductLicenseDetailsView;
import com.google.common.collect.ImmutableList;
import org.joda.time.DateTime;

import java.net.URI;

/**
 * Represents an application defined for JIRA.
 * A JIRA application is exactly the same as a regular application, except that it also has a list of project types defined by it.
 */
public class JiraApplication implements Application {
    private final Application application;
    private final Iterable<ProjectType> projectTypes;

    public JiraApplication(final Application application, final Iterable<ProjectType> projectTypes) {
        this.application = application;
        this.projectTypes = ImmutableList.copyOf(projectTypes);
    }

    public Iterable<ProjectType> getProjectTypes() {
        return projectTypes;
    }

    @Override
    public ApplicationKey getKey() {
        return application.getKey();
    }

    @Override
    public String getName() {
        return application.getName();
    }

    @Override
    public String getDescription() {
        return application.getDescription();
    }

    @Override
    public String getVersion() {
        return application.getVersion();
    }

    @Override
    public String getUserCountDescription(final Option<Integer> integers) {
        return application.getUserCountDescription(integers);
    }

    @Override
    public Option<URI> getConfigurationURI() {
        return application.getConfigurationURI();
    }

    @Override
    public Option<URI> getPostInstallURI() {
        return application.getPostInstallURI();
    }

    @Override
    public Option<URI> getPostUpdateURI() {
        return application.getPostUpdateURI();
    }

    @Override
    public Option<URI> getProductHelpServerSpaceURI() {
        return application.getProductHelpServerSpaceURI();
    }

    @Override
    public Option<URI> getProductHelpCloudSpaceURI() {
        return application.getProductHelpCloudSpaceURI();
    }

    @Override
    public DateTime buildDate() {
        return application.buildDate();
    }

    @Override
    public Option<SingleProductLicenseDetailsView> getLicense() {
        return application.getLicense();
    }

    @Override
    public ApplicationAccess getAccess() {
        return application.getAccess();
    }

    @Override
    public String getDefaultGroup() {
        return application.getDefaultGroup();
    }

    @Override
    public void clearConfiguration() {
        application.clearConfiguration();
    }
}
