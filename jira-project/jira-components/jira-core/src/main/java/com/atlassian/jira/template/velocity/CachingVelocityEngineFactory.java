package com.atlassian.jira.template.velocity;

import com.atlassian.jira.cluster.ClusterSafe;
import com.atlassian.util.concurrent.Lazy;
import com.atlassian.util.concurrent.Supplier;
import org.apache.velocity.app.VelocityEngine;

public class CachingVelocityEngineFactory implements VelocityEngineFactory {

    @ClusterSafe("Program artifacts only.")
    private final Supplier<VelocityEngine> velocityEngine;

    public CachingVelocityEngineFactory(final VelocityEngineFactory velocityEngineFactory) {
        velocityEngine = Lazy.supplier(velocityEngineFactory::getEngine);
    }

    @Override
    public VelocityEngine getEngine() {
        return velocityEngine.get();
    }
}
