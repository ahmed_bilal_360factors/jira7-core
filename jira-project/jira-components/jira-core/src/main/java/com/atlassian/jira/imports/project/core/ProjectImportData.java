package com.atlassian.jira.imports.project.core;

import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.util.AoImportTemporaryFiles;
import com.atlassian.jira.imports.project.util.ProjectImportTemporaryFiles;

/**
 * Stores project specific data that is used to make the import happen. Provides the project import mapper
 * and the paths, on disk, to the partitioned XML files that contain the backup projects data.
 *
 * @since v3.13
 */
public interface ProjectImportData {
    /**
     * The path to the partitioned XML file for the specified entity.
     *
     * @return path to the partitioned XML file for the specified entity.
     */
    String getPathToEntityXml(String entityName);

    /**
     * @return The count of the number of custom field values that are stored in the custom field values XML file.
     */
    int getCustomFieldValuesEntityCount();

    /**
     * @return The count of the number of issue related values that are stored in the issue related XML file.
     */
    int getIssueRelatedEntityCount();

    /**
     * @return The count of the number of issues that are stored in the issue XML file.
     */
    int getIssueEntityCount();

    /**
     * @return The count of the number of file attachment values that are stored in the attachment XML file.
     */
    int getFileAttachmentEntityCount();

    /**
     * The central object used to map all the data the backup project contains.
     *
     * @return object used to map all the data the backup project contains.
     */
    ProjectImportMapper getProjectImportMapper();

    /**
     * Sets the count of the number of valid attachments the import will try to create.
     *
     * @param validAttachmentCount the count of the number of valid attachments the import will try to create.
     */
    void setValidAttachmentsCount(int validAttachmentCount);

    /**
     * Gets the count of the number of valid attachments the import will try to create.
     *
     * @return the count of the number of valid attachments the import will try to create.
     */
    int getValidAttachmentsCount();

    /**
     * @return The count of the number of change item values that are stored in the change item XML file.
     */
    int getChangeItemEntityCount();

    /**
     * Returns the ProjectImportTemporaryFiles containing the path to the Project Import's temporary directory and all the partitioned XML files.
     *
     * @return the ProjectImportTemporaryFiles containing the path to the Project Import's temporary directory and all the partitioned XML files.
     */
    ProjectImportTemporaryFiles getTemporaryFiles();

    /**
     * Returns the ProjectImportTemporaryFiles containing the path to the Project Import's temporary directory and all the partitioned XML files.
     *
     * @return the ProjectImportTemporaryFiles containing the path to the Project Import's temporary directory and all the partitioned XML files.
     */
    AoImportTemporaryFiles getTemporaryAoFiles();

    int getNonIssueEntityCount();
}
