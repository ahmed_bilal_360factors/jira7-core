package com.atlassian.jira.config.component;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static com.google.common.base.MoreObjects.firstNonNull;

public class SwitchingInvocationHandler<T> implements InvocationHandler {
    private final T enabled;
    private final T disabled;
    private final InvocationSwitcher invocationSwitcher;

    protected SwitchingInvocationHandler(T enabled, T disabled, InvocationSwitcher invocationSwitcher) {
        this.enabled = enabled;
        this.disabled = disabled;
        this.invocationSwitcher = invocationSwitcher;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
            return method.invoke(getImplementingObject(), args);
        } catch (InvocationTargetException ite) {
            // JDEV-36204: Exceptions thrown by our target will get wrapped in InvocationTargetException due to
            // the fact that we are calling it reflectively.  We don't want this handler to change the exception
            // behaviour of the component it proxies, so we need to unwrap the cause and throw that instead.
            throw firstNonNull(ite.getCause(), ite);
        }
    }

    /**
     * Cache the value of the object returned from the container
     */
    public T getImplementingObject() {
        return isEnabled() ? enabled : disabled;
    }

    protected boolean isEnabled() {
        return invocationSwitcher.isEnabled();
    }
}
