package com.atlassian.jira.issue.export.customfield;

import com.atlassian.jira.issue.export.IssueExporter;

/**
 * Represents an IssueExporter for exporting CSV representations of Issues. Has no extra methods for this but is used
 * for dependency injection.
 *
 * @since 7.2.0
 */
public interface CsvIssueExporter extends IssueExporter {
}
