package com.atlassian.jira.template.velocity;

import com.atlassian.event.api.EventPublisher;
import org.apache.velocity.app.VelocityEngine;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;

public class InstrumentedVelocityEngineFactory implements VelocityEngineFactory {

    private final VelocityEngineFactory velocityEngineFactory;
    private final EventPublisher eventPublisher;
    private final Clock clock;

    public InstrumentedVelocityEngineFactory(
            final VelocityEngineFactory velocityEngineFactory,
            final EventPublisher eventPublisher,
            final Clock clock
    ) {
        this.velocityEngineFactory = velocityEngineFactory;
        this.eventPublisher = eventPublisher;
        this.clock = clock;
    }

    @Override
    public VelocityEngine getEngine() {
        final Instant start = clock.instant();

        final VelocityEngine velocityEngine = velocityEngineFactory.getEngine();

        final Duration initialization = Duration.between(start, clock.instant());
        eventPublisher.publish(new VelocityEngineInitializedEvent(initialization));

        return velocityEngine;
    }
}
