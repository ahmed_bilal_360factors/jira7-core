package com.atlassian.jira.upgrade;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Upgrade service that schedules execution of upgrade tasks based on JIRA configuration.
 *
 * <p>
 *     It uses {@link APKeys#JIRA_UPGRADE_DELAY_MINS} value for delay duration in minutes or the default value of one
 *     minute if not specified. If {@link APKeys#JIRA_UPGRADE_FORCE_MANUAL_SCHEDULE} is set the upgrades
 *     will not be scheduled.
 * </p>
 */
public class DelayedUpgradeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DelayedUpgradeService.class);
    private static final int DEFAULT_DELAY_IN_MINUTES = 1;

    private final UpgradeScheduler upgradeScheduler;
    private final ApplicationProperties applicationProperties;

    public DelayedUpgradeService(
            final UpgradeScheduler upgradeScheduler,
            final ApplicationProperties applicationProperties
    ) {
        this.upgradeScheduler = upgradeScheduler;
        this.applicationProperties = applicationProperties;
    }

    public UpgradeResult scheduleUpgrades() {

        if (applicationProperties.getOption(APKeys.JIRA_UPGRADE_FORCE_MANUAL_SCHEDULE)) {
            LOGGER.info("Upgrades not scheduled, {}=true", APKeys.JIRA_UPGRADE_FORCE_MANUAL_SCHEDULE);
            return UpgradeResult.OK;
        } else {
            return upgradeScheduler.scheduleUpgrades(getDelayInMinutes());
        }
    }

    private int getDelayInMinutes() {
        final String delay = applicationProperties.getDefaultBackedString(APKeys.JIRA_UPGRADE_DELAY_MINS);

        try {
            return Integer.valueOf(delay);
        } catch (NumberFormatException ignore) {
            LOGGER.warn("{} property value {} is not an integer", APKeys.JIRA_UPGRADE_DELAY_MINS, delay);
            LOGGER.warn("Using default value {} for {} property ", DEFAULT_DELAY_IN_MINUTES, APKeys.JIRA_UPGRADE_DELAY_MINS);

            return DEFAULT_DELAY_IN_MINUTES;
        }
    }
}
