package com.atlassian.jira.util.resourcebundle;

import com.atlassian.jira.util.ComponentLocator;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.Resources;
import com.atlassian.plugin.metadata.PluginMetadataManager;

import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.Predicate;

/**
 * Loads help paths of requested type.
 */
public class HelpResourceBundleLoader {
    private static final String URL_PREFIX_ONDEMAND_KEY = "url-prefix.ondemand";
    private static final String URL_PREFIX_ONDEMAND_VALUE_DARK_AGES = "https://confluence.atlassian.com/display/JIRACloud/";
    private static final String URL_PREFIX_ONDEMAND_VALUE_RENAISSANCE = "https://confluence.atlassian.com/display/${application.help.space}/";

    private final ComponentLocator locator;

    public HelpResourceBundleLoader(final ComponentLocator locator) {
        this.locator = locator;
    }

    public enum Type {
        LEGACY_HELP("helpPaths", rb -> !rb.containsKey(URL_PREFIX_ONDEMAND_KEY) || !URL_PREFIX_ONDEMAND_VALUE_RENAISSANCE.equals(rb.getString(URL_PREFIX_ONDEMAND_KEY))),
        USER_HELP("helpPaths", rb -> !rb.containsKey(URL_PREFIX_ONDEMAND_KEY) || !URL_PREFIX_ONDEMAND_VALUE_DARK_AGES.equals(rb.getString(URL_PREFIX_ONDEMAND_KEY))),
        ADMIN_HELP("helpPathsAdmin", rb -> true);

        private final Resources.TypeFilter filter;
        private final Predicate<ResourceBundle> includeResourceBundleFilter;

        Type(final String filterString, Predicate<ResourceBundle> includeResourceBundleFilter) {
            this.filter = new Resources.TypeFilter(filterString);
            this.includeResourceBundleFilter = includeResourceBundleFilter;
        }

        Resources.TypeFilter filter() {
            return filter;
        }
    }

    public Map<String, String> load(final Type type, final Locale locale) {
        return new PluginResourceLoaderInvocation(locator.getComponent(PluginAccessor.class),
                locator.getComponent(PluginMetadataManager.class),
                type.filter(), false, locale, type.includeResourceBundleFilter).load();
    }
}
