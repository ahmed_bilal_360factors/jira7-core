package com.atlassian.jira.issue.views.conditions;

import java.util.Map;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

public class IsExcelExportEnabledCondition implements Condition {
    protected static final String FEATURE_EXCEL_EXPORT_ENABLED = "jira.export.excel.enabled";

    private final ApplicationProperties applicationProperties;

    public IsExcelExportEnabledCondition(final ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    @Override
    public void init(final Map<String, String> params) throws PluginParseException { }

    @Override
    public boolean shouldDisplay(final Map<String, Object> context) {
        return applicationProperties != null && applicationProperties.getOption(FEATURE_EXCEL_EXPORT_ENABLED);
    }
}
