package com.atlassian.jira.upgrade.util;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.util.DowngradeUtilsImpl;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;

/**
 * Provides utilities around writing all the various build number / version number values in the DB.
 * Shared functionality used in both Upgrades and Downgrades.
 *
 * @since v6.4.5
 */
public class BuildNumberDao {
    private static final String ENTITY_UPGRADE_VERSION_HISTORY = "UpgradeVersionHistory";
    private static final String FIELD_TIMEPERFORMED = "timeperformed";
    private static final String FIELD_TARGETBUILD = "targetbuild";
    private static final String FIELD_TARGETVERSION = "targetversion";

    private static final Logger log = LoggerFactory.getLogger(BuildNumberDao.class);
    private final DowngradeUtilsImpl downgradeUtilsInfo;
    private final ApplicationProperties applicationProperties;

    public BuildNumberDao(final DowngradeUtilsImpl downgradeUtilsInfo,
                          final ApplicationProperties applicationProperties) {
        this.downgradeUtilsInfo = downgradeUtilsInfo;
        this.applicationProperties = applicationProperties;
    }

    /**
     * Sets the JIRA version (eg 6.4.7) in the database.
     */
    public void setJiraVersion(String version) {
        log.info("Setting current version to " + version);
        applicationProperties.setString(APKeys.JIRA_VERSION, version);
    }

    /**
     * Set the DB build number (eg 64023) in the database.
     *
     * @param buildNumber the build number to write
     */
    public void setDatabaseBuildNumber(final String buildNumber) {
        log.info("Setting current build number to " + buildNumber);
        applicationProperties.setString(APKeys.JIRA_PATCHED_VERSION, buildNumber);
    }

    /**
     * Store the minimum downgrade version in the database so that it will be exposed in the xml backup.
     */
    public void setMinimumDowngradeVersion() {
        log.info("Setting downgrade version to " + downgradeUtilsInfo.getDowngradeAllowedVersion());
        applicationProperties.setString(APKeys.JIRA_DOWNGRADE_VERSION, downgradeUtilsInfo.getDowngradeAllowedVersion());
    }

    public void insertUpgradeVersionHistory(OfBizDelegator ofBizDelegator, String currentBuildNumber, String version) {
        // first check if the record already exists (targetbuild is the PK? WTF?)
        final GenericValue record = ofBizDelegator.findByPrimaryKey(ENTITY_UPGRADE_VERSION_HISTORY,
                FieldMap.build(FIELD_TARGETBUILD, currentBuildNumber));
        if (record == null) {
            final Timestamp timePerformed = new Timestamp(System.currentTimeMillis());
            ofBizDelegator.createValue(ENTITY_UPGRADE_VERSION_HISTORY, FieldMap.build(
                    FIELD_TIMEPERFORMED, timePerformed,
                    FIELD_TARGETBUILD, currentBuildNumber,
                    FIELD_TARGETVERSION, version));
        } else {
            if (log.isDebugEnabled()) {
                log.debug("A record already exists for build number '" + currentBuildNumber + "' - skipping creation.");
            }
        }
    }
}
