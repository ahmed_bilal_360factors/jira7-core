package com.atlassian.jira.web.action.setup;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.JiraProductInformation;
import com.atlassian.jira.web.HttpServletVariables;
import com.atlassian.license.SIDManager;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;

import java.util.Optional;
import java.util.stream.Collectors;

import static com.atlassian.jira.util.http.JiraUrl.constructBaseUrl;

/**
 * Component to handle redirects to my.atlassian.com in order to get an evaluation license.
 *
 * The interaction is as follows: build the redirect and specify a URL where the license will
 * be POST-ed back so you can consume it.
 *
 * @since v7.0
 */
public class MyAtlassianComRedirect {

    private final JiraProductInformation jiraProductInformation;
    private final BuildUtilsInfo buildUtilsInfo;
    private final SIDManager sidManager;
    private final ApplicationProperties applicationProperties;
    private final HttpServletVariables servletVariables;

    public MyAtlassianComRedirect(final JiraProductInformation jiraProductInformation, final BuildUtilsInfo buildUtilsInfo,
                                  final SIDManager sidManager, final ApplicationProperties applicationProperties,
                                  final HttpServletVariables servletVariables) {
        this.jiraProductInformation = jiraProductInformation;
        this.buildUtilsInfo = buildUtilsInfo;
        this.sidManager = sidManager;
        this.applicationProperties = applicationProperties;
        this.servletVariables = servletVariables;
    }

    /**
     * Creates a new builder for a given callback url.
     *
     * @param postCallbackUrl the url without the base url at which my.atlassian.com will post the generated
     *                        trial license
     */
    public MyAtlassianComRedirectBuilder newRedirectWithCallbackTo(final String postCallbackUrl) {
        return new MyAtlassianComRedirectBuilder(postCallbackUrl);
    }

    private String getServerId() {
        return Optional.ofNullable(applicationProperties.getString(APKeys.JIRA_SID))
                .orElseGet(this::generateAndStoreSid);
    }

    private String generateAndStoreSid() {
        final String serverId = sidManager.generateSID();
        applicationProperties.setString(APKeys.JIRA_SID, serverId);
        return serverId;
    }

    public final class MyAtlassianComRedirectBuilder {
        private final String postCallabckUrl;

        /**
         * Creates a new builder for a given callback url.
         *
         * This is a shorthand builder pattern ensuring that none of the two parameter has any optional arguments but
         * still reads like a builder.
         * @param postCallbackUrl the url without the base url at which my.atlassian.com will post the generated
         *                        trial license
         */
        private MyAtlassianComRedirectBuilder(final String postCallbackUrl) {
            if (StringUtils.isBlank(postCallbackUrl)) {
                throw new IllegalArgumentException();
            }
            this.postCallabckUrl = postCallbackUrl;
        }

        /**
         * Builds the redirect url using an attribute name that will be used by my.atlassian.com callback to carry the
         * generated license.
         * @param dataAttribute the name of the data attribute.
         * @return the redirect url.
         */
        public String buildWithPostReturnParameter(final String dataAttribute) {

            if (StringUtils.isBlank(dataAttribute)) {
                throw new IllegalArgumentException();
            }

            final String macUrl = "https://www.atlassian.com/ex/GenerateLicense.jspa";

            final ImmutableMap<String, String> params = ImmutableMap.<String, String>builder()
                        .put("utm_nooverride", "1")      // magic m.a.c params
                        .put("ref", "prod")              // dunno what this is
                        .put("product", jiraProductInformation.getLicenseProductKey())
                        .put("version", buildUtilsInfo.getVersion())
                        .put("build", Integer.toString(buildUtilsInfo.getApplicationBuildNumber()))
                        .put("sid", getServerId())
                        .put("licensefieldname", dataAttribute)
                        .put("callback", buildUrl())
                        .build();
            return macUrl + "?" + encodeParams(params);
        }

        private String encodeParams(final ImmutableMap<String, String> params) {
            return params.entrySet().stream()
                        .map(entry -> entry.getKey() + "=" + entry.getValue())
                        .collect(Collectors.joining("&"));
        }

        private String buildUrl() {
            final String baseUrl = constructBaseUrl(servletVariables.getHttpRequest());
            final String sanitizedReturnUrl = postCallabckUrl.startsWith("/") ? postCallabckUrl.substring(1) : postCallabckUrl;
            return baseUrl.endsWith("/") ? baseUrl + sanitizedReturnUrl : baseUrl + "/" + sanitizedReturnUrl;
        }
    }
}
