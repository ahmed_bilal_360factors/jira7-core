package com.atlassian.jira.issue.tabpanels;

import com.atlassian.jira.bc.issue.comment.property.CommentPropertyService;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.issue.action.IssueActionComparator;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.comments.CommentPermissionManager;
import com.atlassian.jira.issue.comments.CommentSearchManager;
import com.atlassian.jira.issue.comments.CommentSummary;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.renderer.comment.CommentFieldRenderer;
import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueTabPanel3;
import com.atlassian.jira.plugin.issuetabpanel.GetActionsRequest;
import com.atlassian.jira.plugin.issuetabpanel.IssueAction;
import com.atlassian.jira.plugin.issuetabpanel.ShowPanelRequest;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class CommentTabPanel extends AbstractIssueTabPanel3 {
    private final CommentManager commentManager;
    private final CommentPermissionManager commentPermissionManager;
    private final FieldLayoutManager fieldLayoutManager;
    private final RendererManager rendererManager;
    private final IssueManager issueManager;
    private final DateTimeFormatter dateTimeFormatter;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final CommentFieldRenderer commentFieldRenderer;
    private final CommentPropertyService commentPropertyService;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public CommentTabPanel(final CommentManager commentManager,
                           final CommentPermissionManager commentPermissionManager,
                           final IssueManager issueManager,
                           final FieldLayoutManager fieldLayoutManager,
                           final RendererManager rendererManager,
                           final DateTimeFormatter dateTimeFormatter,
                           final SoyTemplateRendererProvider soyTemplateRendererProvider,
                           final CommentFieldRenderer commentFieldRenderer,
                           final CommentPropertyService commentPropertyService,
                           final JiraAuthenticationContext jiraAuthenticationContext) {
        this.commentManager = commentManager;
        this.commentPermissionManager = commentPermissionManager;
        this.issueManager = issueManager;
        this.fieldLayoutManager = fieldLayoutManager;
        this.rendererManager = rendererManager;
        this.dateTimeFormatter = dateTimeFormatter;
        this.commentFieldRenderer = commentFieldRenderer;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.soyTemplateRenderer = soyTemplateRendererProvider.getRenderer();
        this.commentPropertyService = commentPropertyService;
    }

    @Override
    public boolean showPanel(final ShowPanelRequest request) {
        return true;
    }

    @Override
    public List<IssueAction> getActions(final GetActionsRequest request) {
        final List<IssueAction> commentActions = getComments(request);

        // This is a bit of a hack to indicate that there are no comments to display
        if (commentActions.isEmpty()) {
            IssueAction action = new GenericMessageAction(descriptor.getI18nBean().getText("viewissue.nocomments"));
            return Collections.singletonList(action);
        }

        // TODO: We should retrieve them sorted correctly in the first place.
        Collections.sort(commentActions, IssueActionComparator.COMPARATOR);

        return commentActions;
    }

    private List<IssueAction> getComments(final GetActionsRequest request) {
        final Issue issue = request.issue();
        final ApplicationUser user = request.loggedInUser();

        final List<Comment> commentsForIssue;
        final int totalCommentCount;
        if (request.isShowAll()) {
            commentsForIssue = commentManager.getCommentsForUser(issue, user);
            totalCommentCount = commentsForIssue.size();
        } else {
            final Optional<Long> focusId = Optional.ofNullable(request.getFocusId()).filter(StringUtils::isNumeric).map(Long::parseLong);

            final CommentSummary commentSummary = commentManager.getCommentSummary(user, issue, focusId);
            commentsForIssue = commentSummary.getComments();
            totalCommentCount = commentSummary.getTotal();
        }

        final List<IssueAction> commentActions = Lists.newArrayListWithCapacity(commentsForIssue.size());
        final CommentEditablePermission permissions = new CommentEditablePermission(issueManager, commentManager, commentPermissionManager, user, issue);

        addShowMoreLinkIfNecessary(request, totalCommentCount, commentsForIssue, commentActions);
        for (final Comment comment : commentsForIssue) {
            commentActions.add(new CommentAction(descriptor, comment, permissions.canEdit(comment), permissions.canDelete(comment),
                    false, rendererManager, fieldLayoutManager, dateTimeFormatter, commentFieldRenderer,
                    commentPropertyService, jiraAuthenticationContext));
        }

        return commentActions;
    }

    private void addShowMoreLinkIfNecessary(GetActionsRequest request, int totalCommentCount, List<Comment> commentsForIssue, List<IssueAction> commentActions) {
        if (totalCommentCount > commentsForIssue.size()) {
            int numberHiddenComments = totalCommentCount - CommentSearchManager.NUMBER_OF_NEW_COMMENTS_TO_SHOW;
            String showOlder = soyTemplateRenderer.render("jira.webresources:soy-templates", "JIRA.Templates.IssueTabPanels.Comment.showOlder",
                    MapBuilder.<String, Object>build("issueKey", request.issue().getKey(), "numberHiddenComments", numberHiddenComments));
            commentActions.add(new GenericMessageAction(showOlder));
        }
    }

    private static class CommentEditablePermission {
        private final IssueManager issueManager;
        private final CommentManager commentManager;
        private final CommentPermissionManager commentPermissionManager;
        private final ApplicationUser user;

        boolean issueIsInEditableWorkflow = false;
        boolean canDeleteAllComments = false;
        boolean canDeleteOwnComments = false;
        boolean canEditAllComments = false;
        boolean canEditOwnComments = false;

        public CommentEditablePermission(final IssueManager issueManager,
                                         final CommentManager commentManager,
                                         final CommentPermissionManager commentPermissionManager,
                                         final ApplicationUser user,
                                         final Issue issue) {
            this.issueManager = issueManager;
            this.commentManager = commentManager;
            this.commentPermissionManager = commentPermissionManager;
            this.user = user;

            this.init(issue);
        }

        private void init(final Issue issue) {
            this.issueIsInEditableWorkflow = issueManager.isEditable(issue);
            this.canDeleteAllComments = issueIsInEditableWorkflow && commentPermissionManager.hasDeleteAllPermission(user, issue);
            this.canDeleteOwnComments = canDeleteAllComments || (issueIsInEditableWorkflow && commentPermissionManager.hasDeleteOwnPermission(user, issue));
            this.canEditAllComments = issueIsInEditableWorkflow && commentPermissionManager.hasEditAllPermission(user, issue);
            this.canEditOwnComments = canEditAllComments || (issueIsInEditableWorkflow && commentPermissionManager.hasEditOwnPermission(user, issue));
        }

        public boolean canDelete(Comment comment) {
            return canDeleteAllComments || (canDeleteOwnComments && commentManager.isUserCommentAuthor(user, comment));
        }

        public boolean canEdit(Comment comment) {
            return canEditAllComments || (canEditOwnComments && commentManager.isUserCommentAuthor(user, comment));
        }
    }
}
