package com.atlassian.jira.soy;

import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.HelpUrls;

import javax.annotation.Nonnull;

/**
 * getJiraHelpTitle('help_url_key_here') soy function.
 *
 * <p>Soy function that returns the title for a help URL specified by the provided help key.
 *
 * @since v7.0
 */
public class HelpTitleFunction extends AbstractHelpFunction {

    public HelpTitleFunction(HelpUrls helpUrls) {
        super(helpUrls);
    }

    @Override
    public String getName() {
        return "getJiraHelpTitle";
    }

    @Override
    @Nonnull
    String getHelpValue(HelpUrl helpUrl) {
        return helpUrl.getTitle();
    }
}
