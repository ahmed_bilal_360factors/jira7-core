package com.atlassian.jira.web.filters;

import com.atlassian.core.filters.encoding.AbstractEncodingFilter;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.util.mime.MimeManager;
import com.atlassian.jira.web.ExecutingHttpRequest;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;

import static com.atlassian.jira.component.ComponentAccessor.getComponentSafely;

/**
 * This filter sets the request and response encoding.
 */
public class JiraEncodingFilter extends AbstractEncodingFilter {
    private static final String DEFAULT_CONTENT_TYPE = "text/html; charset=UTF-8";
    private static final String IMAGE = "image";

    protected String getEncoding() {
        // Just default to UTF-8 if Pico isn't alive, yet
        return getApplicationProperties()
                .map(ApplicationProperties::getEncoding)
                .orElse("UTF-8");
    }

    protected String getContentType() {
        try {
            HttpServletRequest httpServletRequest = ExecutingHttpRequest.get();
            if (null != httpServletRequest) {
                String url = httpServletRequest.getRequestURI();
                if (!StringUtils.isBlank(url)) {
                    final String mimeType = getComponentSafely(MimeManager.class)
                            .map(mimeManager -> mimeManager.getSuggestedMimeType(url))
                            .orElse(null);
                    // The check is performed in such a way that, only for images, we do the handling
                    // It is too dangerous to change the mime type behaviour for all other extensions/requests
                    if (StringUtils.startsWith(mimeType, IMAGE)) {
                        return mimeType;//Just for images, the charset addition is avoided
                    }
                }
            }
            // In all other cases, do the existing handling (default text/html mime type)
            return getApplicationProperties()
                    .map(ApplicationProperties::getContentType)
                    .orElse(DEFAULT_CONTENT_TYPE);
        } catch (Exception e) {
            // No telling what is wrong...  Just play it safe...
            return DEFAULT_CONTENT_TYPE;
        }
    }

    private static Optional<ApplicationProperties> getApplicationProperties() {
        return getComponentSafely(ApplicationProperties.class);
    }
}
