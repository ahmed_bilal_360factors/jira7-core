package com.atlassian.jira.service.services.analytics;

import com.atlassian.jira.service.services.analytics.start.GroupAnalyticTask;
import com.atlassian.jira.service.services.analytics.start.JiraBasicStatsAnalyticTask;
import com.atlassian.jira.service.services.analytics.start.JiraSystemAnalyticTask;
import com.atlassian.jira.service.services.analytics.start.UserAnalyticTask;
import com.atlassian.jira.service.services.analytics.start.WorkflowAnalyticTask;
import com.atlassian.jira.service.services.analytics.stop.ReportUptimeAnalyticsTask;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Map;

/**
 * Service that grabs the configuration of the user and sends analytics.
 * <p>
 * This analytic is extensible by adding a new class that implements JiraAnalyticTask and registering here
 *
 * @since 6.4
 */

public class JiraStartStopAnalyticHelper {
    private final static Logger log = LoggerFactory.getLogger(JiraStartStopAnalyticHelper.class);

    @Nonnull
    public Map<String, Object> getOnStartUsageStats(final boolean onlyDataShapeStatistics) {
        final List<? extends JiraAnalyticTask> tasks = getStartupTasks();
        final ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();

        for (final JiraAnalyticTask task : tasks) {
            if (onlyDataShapeStatistics && !task.isReportingDataShape()) {
                continue;
            }

            try {
                task.init();
                builder.putAll(task.getAnalytics());
            } catch (final Exception e) {
                log.error(" Error trying to grab analytics from [{}]", task.getClass().getName(), e);
            }
        }
        return builder.build();
    }


    public Map<String, Object> getOnStopUsageStats() {
        final List<? extends JiraAnalyticTask> tasks = getShutdownTasks();
        final ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();

        for (final JiraAnalyticTask task : tasks) {
            try {
                task.init();
                builder.putAll(task.getAnalytics());
            } catch (final Exception e) {
                log.error(" Error trying to grab analytics from [{}]", task.getClass().getName(), e);
            }
        }

        return builder.build();
    }

    /**
     * Returns all the tasks to run on startup
     *
     * @return the list of task to run
     */
    @Nonnull
    public List<? extends JiraAnalyticTask> getStartupTasks() {
        return ImmutableList.of(
                new JiraSystemAnalyticTask(),
                new JiraBasicStatsAnalyticTask(),
                new UserAnalyticTask(),
                new WorkflowAnalyticTask(),
                new GroupAnalyticTask());
    }

    /**
     * Returns all the tasks to run on shutdown
     *
     * @return the list of task to run
     */
    @Nonnull
    public List<? extends JiraAnalyticTask> getShutdownTasks() {
        return ImmutableList.of(
                new ReportUptimeAnalyticsTask());
    }
}
