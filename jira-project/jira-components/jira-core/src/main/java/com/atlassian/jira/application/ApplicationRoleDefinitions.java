package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Manager for accessing the {@link ApplicationRoleDefinition}s in JIRA.
 * <p>
 * An {@code ApplicationRoleDefinition} can be in these states:
 * <dl>
 * <dt>Defined</dt>
 * <dd>
 * The {@code ApplicationRoleDefinition} defined by a plugin or by JIRA directly. A defined plugin
 * may or may not have a license associated.</dd>
 * <dt>Licensed</dt>
 * <dd>
 * The {@code ApplicationRoleDefinition} has an associated license in JIRA
 * (irrespective of the license state whether it has expired or it user limit has been exceeded).
 * </dd>
 * </dl>
 *
 * @see com.atlassian.jira.application.ApplicationRole
 * @since v7.0
 */
@ParametersAreNonnullByDefault
public interface ApplicationRoleDefinitions {
    /**
     * Return all the defined {@code ApplicationRoleDefinition}s.
     *
     * @return all the defined {@code ApplicationRoleDefinition}s.
     */
    @Nonnull
    Iterable<ApplicationRoleDefinition> getDefined();

    /**
     * Return the {@link ApplicationRoleDefinition} associated with the passed {@link ApplicationKey}
     * provided it is defined. A value of {@link com.atlassian.fugue.Option#none()} is returned if the passed
     * {@code ApplicationKey} is undefined.
     *
     * @param key the ID to check.
     * @return Return the {@link ApplicationRoleDefinition} associated with the passed
     * {@link ApplicationKey} if it is defined or {@link com.atlassian.fugue.Option#none()}
     * otherwise.
     */
    @Nonnull
    Option<ApplicationRoleDefinition> getDefined(ApplicationKey key);

    /**
     * Return {@code true} if the {@link ApplicationRoleDefinition} identified by the given {@link ApplicationKey}
     * is defined.
     *
     * @param key the key to check.
     * @return {@code true} when the passed {@link ApplicationRoleDefinition} is defined.
     */
    boolean isDefined(ApplicationKey key);

    /**
     * Return all the licensed {@code ApplicationRoleDefinition}s.
     *
     * @return all the licensed {@code ApplicationRoleDefinition}s.
     */
    @Nonnull
    Iterable<ApplicationRoleDefinition> getLicensed();

    /**
     * Return the {@link ApplicationRoleDefinition} associated with the passed {@link ApplicationKey}
     * provided it has a license (irrespective of the license state whether it has expired or it user limit has been
     * exceeded). A value of {@link com.atlassian.fugue.Option#none()} is returned if the passed {@code ApplicationKey}
     * does not have a license.
     *
     * @param key the ID to check.
     * @return Return the {@link ApplicationRoleDefinition} associated with the passed
     * {@link ApplicationKey} if it has a license or {@link com.atlassian.fugue.Option#none()}
     * otherwise.
     */
    Option<ApplicationRoleDefinition> getLicensed(ApplicationKey key);

    /**
     * Return {@code true} if the {@link ApplicationRoleDefinition} identified by the given {@link ApplicationKey}
     * is licensed.
     *
     * @param key the key to check.
     * @return {@code true} when the passed {@link ApplicationRoleDefinition} is licensed.
     */
    boolean isLicensed(ApplicationKey key);

    /**
     * Definition of an {@code ApplicationRole}.
     */
    interface ApplicationRoleDefinition {
        /**
         * Returns the {@code ApplicationKey} that defines the application.
         *
         * @return the {@code ApplicationKey} that defines the application.
         */
        ApplicationKey key();

        /**
         * The name of the {@code ApplicationRole}
         *
         * @return the name of the {@code ApplicationRole}
         */
        String name();
    }
}
