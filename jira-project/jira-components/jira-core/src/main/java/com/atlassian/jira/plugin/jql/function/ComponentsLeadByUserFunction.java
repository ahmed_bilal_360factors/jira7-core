package com.atlassian.jira.plugin.jql.function;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.query.operand.FunctionOperand;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.concat;

/**
 * This function returns a list of components lead by a user.
 * <p>
 * This function expects zero or one argument. If zero arguments are supplied the current logged in user will be used as
 * component lead.
 *
 * @since v4.2
 */
public class ComponentsLeadByUserFunction extends AbstractUserBasedFunction {
    public static final String FUNCTION_COMPONENTS_LEAD_BY_USER = "componentsLeadByUser";
    private static final String JIRA_JQL_COMPONENT_NO_SUCH_USER = "jira.jql.component.no.such.user";

    private final PermissionManager permissionManager;
    private final ProjectComponentManager componentManager;

    public ComponentsLeadByUserFunction(final PermissionManager permissionManager, final ProjectComponentManager componentManager, final UserUtil userUtil) {
        super(userUtil);
        this.permissionManager = permissionManager;
        this.componentManager = notNull("componentManager", componentManager);
    }

    public JiraDataType getDataType() {
        return JiraDataTypes.COMPONENT;
    }

    protected List<QueryLiteral> getFunctionValuesList(final QueryCreationContext queryCreationContext, final FunctionOperand functionOperand, final ApplicationUser user) {
        return concat(queryCreationContext.isSecurityOverriden() ? getLeadComponents(user).stream() : Stream.empty(),
                permissionManager.getProjects(BROWSE_PROJECTS, queryCreationContext.getApplicationUser()).stream()
                        .map(Project::getProjectComponents)
                        .flatMap(Collection::stream)
                        .filter(isComponentLead(user)))
                .map(component -> new QueryLiteral(functionOperand, component.getId()))
                .collect(toList());
    }

    private Collection<ProjectComponent> getLeadComponents(ApplicationUser user) {
        return componentManager.findComponentsByLead(user.getName());
    }

    private Predicate<ProjectComponent> isComponentLead(final ApplicationUser user) {
        return component -> user.getKey().equals(component.getLead());
    }

    protected String getUserNotFoundMessageKey() {
        return JIRA_JQL_COMPONENT_NO_SUCH_USER;
    }
}
