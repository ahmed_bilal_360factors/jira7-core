package com.atlassian.jira.board;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.query.Query;

/**
 * Provides services for augmenting a board's query
 *
 * @since v7.1
 */
public interface BoardQueryService {
    /**
     * Augments a query to add additional clauses for filtering done issues
     * for the purposes of display.
     *
     * @param query The query to augment with additional clauses for displaying
     *              the board
     * @return      The augmented {@link Query}
     */
    Query getAugmentedQueryForDoneIssues(Query query);

    /**
     * Returns the query originally associated with the JQL of a board, with the
     * additional constraint of including only standard issue types.
     *
     * @param user  The user to fetch the board query as
     * @param board The board to fetch the query from
     * @return      The {@link Query} if possible
     */
    ServiceOutcome<Query> getBaseQueryForBoard(ApplicationUser user, Board board);
}
