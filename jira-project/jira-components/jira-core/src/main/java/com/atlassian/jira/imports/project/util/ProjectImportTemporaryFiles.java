package com.atlassian.jira.imports.project.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Single class to handle IO for the Temporary XML partition files.
 *
 * @since v3.13
 */
public interface ProjectImportTemporaryFiles {
    /**
     * Returns the temporary directory which is the parent of all the temporary partition files.
     *
     * @return the temporary directory which is the parent of all the temporary partition files.
     */
    File getParentDirectory();

    /**
     * Returns the temporary XML partition file an entity.
     *
     * @return the temporary XML partition file an entity.
     */
    File getEntityXmlFile(String entity);

    /**
     * Returns a writer capable of writing to an entity file.
     *
     * @param entity the entity being written
     * @return the writer to use to write that entity
     * @throws IOException if the file cannot be opened for writing
     */
    PrintWriter getWriter(String entity) throws IOException;

    /**
     * Queries the encoding being used by any print writers opened by {@link #getWriter(String)}.
     *
     * @return the encoding being used
     */
    String getEncoding();

    /**
     * Closes writers opened with {@link #getWriter(String)}. <b>Must be called once writers are no longer needed.</b>
     */
    void closeWriters();

    /**
     * Deletes the temporary files held in this object.
     * It is safe to call this method twice as it will check if the files actually exist first.
     */
    void deleteTempFiles();
}
