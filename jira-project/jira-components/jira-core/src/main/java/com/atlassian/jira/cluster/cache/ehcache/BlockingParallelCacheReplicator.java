package com.atlassian.jira.cluster.cache.ehcache;

import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.Status;
import net.sf.ehcache.distribution.CachePeer;
import net.sf.ehcache.distribution.CacheReplicator;
import net.sf.ehcache.distribution.RemoteCacheException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

class BlockingParallelCacheReplicator implements CacheReplicator {

    private static final Logger LOG = LoggerFactory.getLogger(BlockingParallelCacheReplicator.class);

    private volatile Status status;

    private final boolean replicatePuts;
    private final boolean replicatePutsViaCopy;
    private final boolean replicateUpdates;
    private final boolean replicateUpdatesViaCopy;
    private final boolean replicateRemovals;

    private final ExecutorService executorService;

    BlockingParallelCacheReplicator(final boolean replicatePuts, final boolean replicatePutsViaCopy,
                                    final boolean replicateUpdates, final boolean replicateUpdatesViaCopy,
                                    final boolean replicateRemovals, final ExecutorService executorService) {
        this.replicatePuts = replicatePuts;
        this.replicatePutsViaCopy = replicatePutsViaCopy;
        this.replicateUpdates = replicateUpdates;
        this.replicateUpdatesViaCopy = replicateUpdatesViaCopy;
        this.replicateRemovals = replicateRemovals;
        this.executorService = executorService;

        this.status = Status.STATUS_ALIVE;
    }

    @Override
    public boolean isReplicateUpdatesViaCopy() {
        return true;
    }

    @Override
    public boolean notAlive() {
        return !alive();
    }

    @Override
    public boolean alive() {
        return Status.STATUS_ALIVE.equals(status);
    }

    @Override
    public void notifyElementRemoved(final Ehcache cache, final Element element) throws CacheException {
        if (notAlive() || !replicateRemovals) {
            return;
        }

        if (!element.isKeySerializable()) {
            LOG.warn("Key {} is not Serializable and removal cannot be replicated. Cache: {}", element.getObjectKey(),
                    cache.getName());
            return;
        }

        replicateRemovalNotification(cache, (Serializable) element.getObjectKey());
    }

    @Override
    public void notifyElementPut(final Ehcache cache, final Element element) throws CacheException {
        if (notAlive() || !replicatePuts) {
            return;
        }

        if (replicatePutsViaCopy) {
            replicateViaCopy(cache, element);
        } else {
            replicateViaKeyInvalidation(cache, element);
        }
    }

    @Override
    public void notifyElementUpdated(final Ehcache cache, final Element element) throws CacheException {
        if (notAlive() || !replicateUpdates) {
            return;
        }

        if (replicateUpdatesViaCopy) {
            if (!element.isSerializable()) {
                LOG.warn("Object with key {} is not Serializable and cannot be updated via copy. Cache: {}",
                        element.getObjectKey(), cache.getName());
                return;
            }

            replicatePutNotification(cache, element);
        } else {
            if (!element.isKeySerializable()) {
                LOG.warn("Key {} is not Serializable and update cannot be replicated. Cache: {}", element.getObjectKey(),
                        cache.getName());
                return;
            }

            replicateRemovalNotification(cache, (Serializable) element.getObjectKey());
        }
    }

    @Override
    public void notifyRemoveAll(final Ehcache cache) {
        if (notAlive() || !replicateRemovals) {
            return;
        }

        replicateRemoveAllNotification(cache);
    }

    private void replicateViaCopy(final Ehcache cache, final Element element) {
        if (element.isSerializable()) {
            replicatePutNotification(cache, element);
            return;
        }
        if (!element.isKeySerializable()) {
            logUnserializableKey(element);
        }
        if (!isValueSerializable(element)) {
            LOG.error("Value class {} is not Serializable => cannot be replicated. Cache: {}",
                    element.getObjectValue().getClass().getName(), cache.getName());
        }
    }

    private boolean isValueSerializable(final Element element) {
        return element.getObjectValue() instanceof Serializable;
    }

    private void replicateViaKeyInvalidation(final Ehcache cache, final Element element) {
        if (element.isKeySerializable()) {
            replicateRemovalNotification(cache, (Serializable) element.getObjectKey());
            return;
        }
        logUnserializableKey(element);
    }

    private void replicatePutNotification(final Ehcache cache, final Element element) throws RemoteCacheException {
        forEachCachePeer(cache, peer -> {
            try {
                peer.put(element);
            } catch (Throwable t) {
                onReplicationError(cache, peer, t, "put");
            }
        });
    }

    private void replicateRemovalNotification(final Ehcache cache, final Serializable key) throws RemoteCacheException {
        forEachCachePeer(cache, peer -> {
            try {
                peer.remove(key);
            } catch (Throwable t) {
                onReplicationError(cache, peer, t, "remove");
            }
        });
    }

    private void replicateRemoveAllNotification(final Ehcache cache) {
        forEachCachePeer(cache, (peer) -> {
            try {
                peer.removeAll();
            } catch (Throwable t) {
                onReplicationError(cache, peer, t, "removeAll");
            }
        });
    }

    protected void onReplicationError(final Ehcache cache, final CachePeer peer, final Throwable t, String operation) {
        LOG.error("Exception on replication of {}. {}. Cache: {} Peer: {}", operation, t.getMessage(),
                cache.getName(), getPeerName(peer), t);
    }

    private String getPeerName(final CachePeer peer) {
        try {
            return peer.getName();
        } catch (RemoteException e) {
            return "undefined";
        }
    }

    @SuppressWarnings("unchecked")
    private void forEachCachePeer(final Ehcache cache, final Consumer<CachePeer> operation) {
        List<CachePeer> cachePeers = cache
                .getCacheManager()
                .getCacheManagerPeerProvider("RMI")
                .listRemoteCachePeers(cache);

        CompletableFuture.allOf(cachePeers.stream()
                .map(
                        peer -> CompletableFuture.runAsync(
                                new ClassLoaderSwitchingRunnable(() -> operation.accept(peer)),
                                executorService
                        )
                )
                .toArray(CompletableFuture[]::new)).join();
    }

    @Override
    public void notifyElementEvicted(final Ehcache cache, final Element element) {

    }

    @Override
    public void notifyElementExpired(final Ehcache cache, final Element element) {

    }

    private void logUnserializableKey(final Element element) {
        LOG.error("Key class {} is not Serializable => cannot be replicated",
                element.getObjectKey().getClass().getName());
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        super.clone();
        return new BlockingParallelCacheReplicator(replicatePuts, replicatePutsViaCopy, replicateUpdates,
                replicateUpdatesViaCopy, replicateRemovals, this.executorService);
    }

    @Override
    public void dispose() {
        status = Status.STATUS_SHUTDOWN;
    }
}
