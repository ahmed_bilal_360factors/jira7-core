package com.atlassian.jira.config.webwork;

/**
 * Responsible for providing a classloader instance to configure webwork.
 *
 * @see WebworkConfigurator
 * @since v7.3.1
 */
public interface WebworkClassLoaderSupplier {

    /**
     * Retrieves the classloader instance to use for webwork configuration.
     *
     * @return A classloader instance to configure webwork with.
     */
    ClassLoader get();

    /**
     * Sets the classloader instance to use for webwork configuration.
     */
    void set(ClassLoader classLoader);
}
