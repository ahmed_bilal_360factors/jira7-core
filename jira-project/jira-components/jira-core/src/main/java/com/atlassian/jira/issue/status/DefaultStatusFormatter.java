package com.atlassian.jira.issue.status;

import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.velocity.exception.VelocityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

import static com.atlassian.jira.issue.fields.NavigableField.TEMPLATE_DIRECTORY_PATH;
import static com.atlassian.jira.template.TemplateSources.file;
import static org.apache.commons.lang3.StringUtils.defaultString;

/**
 * @since v6.5
 */
@EventComponent
public class DefaultStatusFormatter implements StatusFormatter {
    private static final Logger log = LoggerFactory.getLogger(DefaultStatusFormatter.class);
    private static final String CACHE_KEY = DefaultStatusFormatter.class.getName() + ".statuses";

    private final VelocityTemplatingEngine templatingEngine;
    private final SoyTemplateRendererProvider soyTemplateRendererProvider;

    public DefaultStatusFormatter(final VelocityTemplatingEngine templatingEngine, final SoyTemplateRendererProvider soyTemplateRendererProvider) {
        this.templatingEngine = templatingEngine;
        this.soyTemplateRendererProvider = soyTemplateRendererProvider;
    }

    @Nonnull
    protected LoadingCache<ImmutableMap<String, Object>, String> getRequestCache() {
        return JiraAuthenticationContextImpl.getRequestCache(CACHE_KEY, () ->
                CacheBuilder.newBuilder().build(CacheLoader.from(params -> {
                    Map<String, Object> velocityParams = Maps.newHashMap(params);
                    velocityParams.put("soyRenderer", soyTemplateRendererProvider.getRenderer());
                    return renderTemplate("status-columnview.vm", velocityParams);
                })));
    }

    @Nonnull
    @Override
    public String getColumnViewHtml(@Nullable final Status status, @Nonnull Map<String, Object> displayParams) {
        return getColumnViewHtml(status != null ? status.getSimpleStatus() : null, displayParams);
    }

    @Nonnull
    @Override
    public String getColumnViewHtml(@Nullable final SimpleStatus status, @Nonnull Map<String, Object> displayParams) {
        final ImmutableMap.Builder<String, Object> velocityParams = ImmutableMap.<String, Object>builder().putAll(displayParams);
        if (status != null) {
            velocityParams.put(IssueFieldConstants.STATUS, status);
        }
        return defaultString(getRequestCache().getUnchecked(velocityParams.build()), "");
    }

    @SuppressWarnings({"UnusedDeclaration"})
    @EventListener
    public void onClearCache(ClearCacheEvent ignored) {
        refresh();
    }

    @SuppressWarnings({"UnusedDeclaration"})
    @EventListener
    public void onClearStatusCacheEvent(ClearStatusCacheEvent ignored) {
        refresh();
    }

    private void refresh() {
        getRequestCache().invalidateAll();
    }

    protected String renderTemplate(final String template, final Map<String, Object> velocityParams) {
        try {
            return templatingEngine.render(file(TEMPLATE_DIRECTORY_PATH + template)).applying(velocityParams).asHtml();
        } catch (final VelocityException e) {
            log.error("Error occurred while rendering velocity template for '" + TEMPLATE_DIRECTORY_PATH + "/" + template + "'.", e);
        }

        return "";
    }
}
