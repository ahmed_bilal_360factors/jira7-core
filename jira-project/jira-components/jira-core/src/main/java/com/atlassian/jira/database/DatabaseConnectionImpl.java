package com.atlassian.jira.database;

import com.atlassian.jira.exception.DataAccessException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @since v7.0
 */
class DatabaseConnectionImpl implements DatabaseConnection {
    private final Connection con;

    DatabaseConnectionImpl(final Connection con) {
        this.con = con;
    }

    @Override
    public Connection getJdbcConnection() {
        return con;
    }

    @Override
    public void setAutoCommit(final boolean autoCommit) {
        try {
            con.setAutoCommit(autoCommit);
        } catch (SQLException ex) {
            throw new DataAccessException(ex);
        }
    }

    @Override
    public void commit() {
        try {
            con.commit();
        } catch (SQLException ex) {
            throw new DataAccessException(ex);
        }
    }

    @Override
    public void rollback() {
        try {
            con.rollback();
        } catch (SQLException ex) {
            throw new DataAccessException(ex);
        }
    }
}
