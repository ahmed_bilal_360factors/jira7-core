package com.atlassian.jira.permission.management.events;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.jira.event.AbstractEvent;
import com.google.common.base.Objects;

/**
 * Event fired to measure permission types usage (delete operation)
 */
@EventName("jira.projectpermissions.revoke.details")
public class PermissionRevokedFromSecurityTypeEvent extends AbstractEvent {
    private final Long schemeId;
    private final String permissionKey;
    private final String securityType;

    public PermissionRevokedFromSecurityTypeEvent(
            final Long schemeId,
            final String permissionKey,
            final String securityType
    ) {
        this.schemeId = schemeId;
        this.permissionKey = permissionKey;
        this.securityType = securityType;
    }

    public Long getSchemeId() {
        return schemeId;
    }

    public String getPermissionKey() {
        return permissionKey;
    }

    public String getSecurityType() {
        return securityType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PermissionRevokedFromSecurityTypeEvent that = (PermissionRevokedFromSecurityTypeEvent) o;
        return Objects.equal(schemeId, that.schemeId) &&
                Objects.equal(permissionKey, that.permissionKey) &&
                Objects.equal(securityType, that.securityType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), schemeId, permissionKey, securityType);
    }

    @Override
    public String toString() {
        return "PermissionDeletedForSecurityTypeEvent{" +
                "schemeId=" + schemeId +
                ", permissionKey='" + permissionKey + '\'' +
                ", securityType='" + securityType + '\'' +
                '}';
    }
}
