package com.atlassian.jira.util;

import com.atlassian.jira.upgrade.UpgradeVersionHistoryItem;
import com.atlassian.jira.upgrade.UpgradeVersionHistoryManager;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Provides information about this jira instance.
 *
 * @since v7.1
 */
public class InstanceInfoImpl implements InstanceInfo {

    private final UpgradeVersionHistoryManager upgradeVersionHistoryManager;

    public InstanceInfoImpl(final UpgradeVersionHistoryManager upgradeVersionHistoryManager) {
        this.upgradeVersionHistoryManager = upgradeVersionHistoryManager;
    }

    @Override
    public Optional<Date> getInstanceCreatedDate() {
        final List<UpgradeVersionHistoryItem> upgradeVersionHistoryItems = upgradeVersionHistoryManager.getAllUpgradeVersionHistory();
        if (upgradeVersionHistoryItems != null && upgradeVersionHistoryItems.size() > 0) {
            return upgradeVersionHistoryItems.stream().filter(item -> Objects.nonNull(item.getTimePerformed())).map(UpgradeVersionHistoryItem::getTimePerformed).min((d1, d2) -> d1.compareTo(d2));
        }
        return Optional.empty();
    }
}
