package com.atlassian.jira.imports.project.util;

import com.atlassian.jira.action.admin.export.DefaultSaxEntitiesExporter;
import com.atlassian.jira.util.TempDirectoryUtil;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ProjectImportTemporaryFilesImpl implements ProjectImportTemporaryFiles {
    private static final Logger log = LoggerFactory.getLogger(ProjectImportTemporaryFilesImpl.class);

    private final File parentDirectory;
    private final Map<String, File> entityToXmlFileMap;
    private final String encoding;
    private final Map<String, PrintWriter> writers;

    public ProjectImportTemporaryFilesImpl(String projectKey, Set<String> entities, String encoding) throws IOException {
        // Create the parent directory
        parentDirectory = TempDirectoryUtil.createTempDirectory("JiraProjectImport" + projectKey);
        this.writers = new HashMap<String, PrintWriter>();
        this.entityToXmlFileMap = new HashMap<String, File>();
        this.encoding = encoding;
        for (String entity : entities) {
            openWriter(entity);
        }
    }

    public File getEntityXmlFile(String entity) {
        return entityToXmlFileMap.get(entity);
    }

    public void deleteTempFiles() {
        try {
            FileUtils.deleteDirectory(parentDirectory);
        } catch (IOException e) {
            log.warn("Unable to delete temporary files", e);
        }
    }

    public PrintWriter getWriter(String entity) throws IOException {
        PrintWriter printWriter = writers.get(entity);
        if (printWriter == null) {
            printWriter = openWriter(entity);
        }
        return printWriter;
    }

    private PrintWriter openWriter(final String entity) throws IOException {
        final PrintWriter printWriter;
        File xmlFile = getEntityXmlFile(entity);
        if (xmlFile == null) {
            xmlFile = new File(parentDirectory, entity + ".xml");
            xmlFile.createNewFile();
            xmlFile.deleteOnExit();
            entityToXmlFileMap.put(entity, xmlFile);
        }
        final FileOutputStream fos = new FileOutputStream(xmlFile);
        final OutputStreamWriter out = new OutputStreamWriter(fos, encoding);
        final BufferedWriter bufferedWriter = new BufferedWriter(out, DefaultSaxEntitiesExporter.DEFAULT_BUFFER_SIZE);
        printWriter = new PrintWriter(bufferedWriter);
        printWriter.println("<?xml version=\"1.0\" encoding=\"" + getEncoding() + "\"?>");
        printWriter.println("<entity-engine-xml>");
        writers.put(entity, printWriter);
        return printWriter;
    }

    public String getEncoding() {
        return encoding;
    }

    public void closeWriters() {
        for (PrintWriter printWriter : writers.values()) {
            printWriter.print("</entity-engine-xml>");
            printWriter.close();
        }
    }

    @VisibleForTesting
    public File getParentDirectory() {
        return parentDirectory;
    }
}
