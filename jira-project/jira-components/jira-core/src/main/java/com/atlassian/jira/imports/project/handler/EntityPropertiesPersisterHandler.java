package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.external.beans.ExternalEntityProperty;
import com.atlassian.jira.imports.project.ProjectImportPersister;
import com.atlassian.jira.imports.project.core.EntityRepresentation;
import com.atlassian.jira.imports.project.core.ProjectImportResults;
import com.atlassian.jira.imports.project.mapper.SimpleProjectImportIdMapper;
import com.atlassian.jira.imports.project.parser.EntityPropertyParser;
import com.atlassian.jira.imports.project.parser.EntityPropertyParserImpl;
import com.atlassian.jira.imports.project.transformer.EntityPropertyTransformer;
import com.atlassian.jira.imports.project.transformer.EntityPropertyTransformerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.Executor;

/**
 * Used to inspect issue properties entries in a backup file, transform the entities and persist them to the database.
 *
 * @since v6.2
 */
public class EntityPropertiesPersisterHandler extends AbstractPersisterHandler implements ImportOfBizEntityHandler {

    private static final Logger log = LoggerFactory.getLogger(EntityPropertiesPersisterHandler.class);
    private final ProjectImportResults projectImportResults;
    private final ProjectImportPersister projectImportPersister;
    private final EntityPropertyType entityPropertyType;
    private final SimpleProjectImportIdMapper idMapperForType;
    private final EntityPropertyParser entityPropertyParser = new EntityPropertyParserImpl();
    private final EntityPropertyTransformer entityPropertyTransformer = new EntityPropertyTransformerImpl();

    public EntityPropertiesPersisterHandler(
            final Executor executor,
            final ProjectImportResults projectImportResults,
            final ProjectImportPersister projectImportPersister,
            final EntityPropertyType entityPropertyType,
            final SimpleProjectImportIdMapper idMapperForType) {
        super(executor, projectImportResults);
        this.projectImportResults = projectImportResults;
        this.projectImportPersister = projectImportPersister;
        this.entityPropertyType = entityPropertyType;
        this.idMapperForType = idMapperForType;
    }

    @Override
    public void handleEntity(final String entityName, final Map<String, String> attributes)
            throws ParseException, AbortImportException {
        if (EntityPropertyParser.ENTITY_PROPERTY_ENTITY_NAME.equals(entityName)) {
            final ExternalEntityProperty externalEntityProperty = getParser().parse(attributes);

            if (entityPropertyType.getDbEntityName().equals(externalEntityProperty.getEntityName()) && externalEntityProperty.getEntityId() != null) {
                final String newEntityIdStr = idMapperForType.getMappedId(String.valueOf(externalEntityProperty.getEntityId()));
                if (newEntityIdStr != null) {
                    final EntityRepresentation representation = getTransformer().getEntityRepresentation(externalEntityProperty, Long.valueOf(newEntityIdStr));
                    execute(() -> {
                        final Long entityId = projectImportPersister.createEntity(representation);
                        if (entityId == null) {
                            projectImportResults.addError(projectImportResults.getI18n().
                                    getText("admin.errors.project.import.entity.property.error",
                                            String.valueOf(externalEntityProperty.getId()),
                                            externalEntityProperty.getEntityName(),
                                            String.valueOf(externalEntityProperty.getEntityId())));
                        }
                    });
                } else {
                    log.debug("Ignoring entity property id=" + externalEntityProperty.getId() + " entityName = " + externalEntityProperty.getEntityName());
                }
            }
        }
    }


    @Override
    public void startDocument() {
//        / No-op
    }

    @Override
    public void endDocument() {
//        / No-op
    }

    private EntityPropertyParser getParser() {
        return entityPropertyParser;
    }

    private EntityPropertyTransformer getTransformer() {
        return entityPropertyTransformer;
    }
}
