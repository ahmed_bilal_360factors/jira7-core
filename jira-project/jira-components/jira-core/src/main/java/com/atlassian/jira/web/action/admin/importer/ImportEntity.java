package com.atlassian.jira.web.action.admin.importer;

import webwork.action.Action;

public interface ImportEntity extends Action, Comparable {
    public int getActionOrder();
}
