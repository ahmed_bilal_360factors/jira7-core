package com.atlassian.jira.servermetrics;


import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Duration;
import java.util.List;

@ParametersAreNonnullByDefault
public class TimingInformation {
    private final List<CheckpointTiming> activitiesTimings;
    private final List<CheckpointTiming> timingEventList;

    private final Duration totalTime;
    private final Duration userTime;
    private final Duration cpuTime;
    private final Duration garbageCollectionTime;
    private final long garbageCollectionCount;

    public TimingInformation(List<CheckpointTiming> timingEventList,
                             List<CheckpointTiming> activitiesTimings, Duration totalTime,
                             Duration userTime,
                             Duration cpuTime,
                             Duration garbageCollectionTime,
                             long garbageCollectionCount) {
        this.timingEventList = timingEventList;
        this.activitiesTimings = activitiesTimings;
        this.totalTime = totalTime;
        this.userTime = userTime;
        this.cpuTime = cpuTime;
        this.garbageCollectionTime = garbageCollectionTime;
        this.garbageCollectionCount = garbageCollectionCount;
    }

    @Nonnull
    public Duration getTotalTime() {
        return totalTime;
    }

    @Nonnull
    public List<CheckpointTiming> getTimingEventList() {
        return timingEventList;
    }

    @Nonnull
    public Duration getUserTime() {
        return userTime;
    }

    @Nonnull
    public Duration getCpuTime() {
        return cpuTime;
    }

    @Nonnull
    public Duration getGarbageCollectionTime() {
        return garbageCollectionTime;
    }

    public long getGarbageCollectionCount() {
        return garbageCollectionCount;
    }

    @Nonnull
    public List<CheckpointTiming> getActivityDurations() {
        return activitiesTimings;
    }
}
