package com.atlassian.jira.issue.customfields.config.item;

import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.VersionCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.option.GenericImmutableOptions;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigItemType;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.util.I18nHelper;

import java.util.Collections;

/**
 * Represents version order configuration item on version picker custom field configuration page
 */
public class VersionOrderConfigItem implements FieldConfigItemType {
    private final VersionManager versionManager;
    private final GenericConfigManager genericConfigManager;
    private final I18nHelper i18nHelper;

    public VersionOrderConfigItem(VersionManager versionManager, GenericConfigManager genericConfigManager, I18nHelper i18nHelper) {
        this.versionManager = versionManager;
        this.genericConfigManager = genericConfigManager;
        this.i18nHelper = i18nHelper;
    }

    @Override
    public String getDisplayName() {
        return "";
    }

    @Override
    public String getDisplayNameKey() {
        return "admin.issuefields.customfields.versionorder";
    }

    @Override
    public String getViewHtml(FieldConfig fieldConfig, FieldLayoutItem fieldLayoutItem) {
        final Object versionOrderId = genericConfigManager.retrieve(VersionCFType.VersionOrder.getDatabaseType(), fieldConfig.getId().toString());
        final Option<VersionCFType.VersionOrder> version = VersionCFType.VersionOrder.findById(versionOrderId);
        if (version.isDefined()) {
            return version.get().asHtml(i18nHelper);
        }
        return VersionCFType.VersionOrder.RELEASED_FIRST.asHtml(i18nHelper);
    }

    @Override
    public String getObjectKey() {
        return "versionOrder";
    }

    @Override
    public Object getConfigurationObject(Issue issue, FieldConfig config) {
        if (issue != null && issue.getProjectId() != null) {
            return new GenericImmutableOptions(versionManager.getVersions(issue.getProjectId()), config);
        } else {
            return new GenericImmutableOptions(Collections.emptyList(), config);
        }
    }

    @Override
    public String getBaseEditUrl() {
        return "EditVersionPickerCustomFieldOptionsOrder!default.jspa";
    }
}