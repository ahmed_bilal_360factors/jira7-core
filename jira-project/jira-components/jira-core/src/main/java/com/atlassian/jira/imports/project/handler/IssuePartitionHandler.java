package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.parser.IssueParser;
import com.atlassian.jira.imports.project.util.ProjectImportTemporaryFiles;
import com.atlassian.jira.util.dbc.Null;
import org.ofbiz.core.entity.DelegatorInterface;
import org.ofbiz.core.entity.GenericDelegator;
import org.ofbiz.core.entity.GenericEntity;
import org.ofbiz.core.entity.model.ModelEntity;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Parses an XML import file and creates a reduced XML file with just the Issues for the given project.
 *
 * @since v3.13
 */
public class IssuePartitionHandler implements ImportOfBizEntityHandler {
    protected final GenericDelegator delegator;
    private final BackupProject backupProject;
    private final ModelEntity modelEntity;
    private final ProjectImportTemporaryFiles projectImportTemporaryFiles;
    private int entityCount;
    private PrintWriter printWriter;

    /**
     * @param backupProject               contains the issue id's that we are interested in partitioning.
     * @param projectImportTemporaryFiles the temp files provider
     * @param modelEntity                 is the ModelEntity for the "Issue" entity
     * @param delegatorInterface          required for persistence
     */
    public IssuePartitionHandler(final BackupProject backupProject, final ProjectImportTemporaryFiles projectImportTemporaryFiles,
                                 final ModelEntity modelEntity, final DelegatorInterface delegatorInterface) {
        assertModelEntityForName(modelEntity, IssueParser.ISSUE_ENTITY_NAME);
        this.projectImportTemporaryFiles = projectImportTemporaryFiles;
        this.delegator = GenericDelegator.getGenericDelegator(delegatorInterface.getDelegatorName());
        this.backupProject = backupProject;
        this.modelEntity = modelEntity;
    }

    public void handleEntity(final String entityName, final Map<String, String> attributes) throws ParseException {
        if (IssueParser.ISSUE_ENTITY_NAME.equals(entityName)) {
            if (printWriter == null) {
                try {
                    printWriter = projectImportTemporaryFiles.getWriter(IssueParser.ISSUE_ENTITY_NAME);
                } catch (IOException e) {
                    throw new ParseException(e.getMessage());
                }
            }
            // check if it is in our project
            if (backupProject.containsIssue(getId(attributes))) {
                // Create a GenericEntity
                final GenericEntity genericEntity = new GenericEntity(delegator, modelEntity, attributes);
                genericEntity.writeXmlText(printWriter, null);
                entityCount++;
            }
        } else {
            endDocument();
        }
    }

    public int getEntityCount() {
        return entityCount;
    }

    String getId(final Map attributes) {
        return (String) attributes.get("id");
    }

    public void startDocument() {
        // NOOP - document header created lazily on first element write
    }

    public void endDocument() {
        printWriter = null;
    }

    public void assertModelEntityForName(final ModelEntity modelEntity, final String expectedName) {
        Null.not("modelEntity", modelEntity);
        Null.not("expectedName", expectedName);
        if (!expectedName.equals(modelEntity.getEntityName())) {
            throw new IllegalArgumentException("This handler must only be created with a " + expectedName + " model entity");
        }
    }
}
