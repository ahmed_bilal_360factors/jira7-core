package com.atlassian.jira.web.action.admin.issuetypes;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.IssueTypeService;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.action.admin.constants.AbstractDeleteConstant;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.collect.Lists;

import java.util.Collection;

import static com.atlassian.fugue.Option.option;

@WebSudoRequired
public class DeleteIssueType extends AbstractDeleteConstant<IssueType> {
    private final IssueTypeService issueTypeService;

    public DeleteIssueType(final IssueTypeService issueTypeService) {
        this.issueTypeService = issueTypeService;
    }

    protected String getConstantEntityName() {
        return "IssueType";
    }

    protected String getNiceConstantName() {
        return getText("admin.issue.constant.issuetype.lowercase");
    }

    protected String getIssueConstantField() {
        return "type";
    }

    protected IssueType getConstant(final String id) {
        return issueTypeService.getIssueType(getLoggedInUser(), id).getOrNull();
    }

    protected IssueType getIssueTypeObject() {
        return getConstant(id);
    }

    protected String getRedirectPage() {
        return "ViewIssueTypes.jspa";
    }

    protected Collection<IssueType> getConstants() {
        return Lists.newArrayList(issueTypeService.getIssueTypes(getLoggedInUser()));
    }

    protected void clearCaches() {
        getConstantsManager().refreshIssueTypes();
        ComponentAccessor.getFieldManager().refresh();
    }

    protected void doValidation() {
        final IssueTypeService.DeleteValidationResult validationResult = issueTypeService.validateDeleteIssueType(
                getLoggedInUser(), new IssueTypeService.IssueTypeDeleteInput(id, option(newId)));

        if (!validationResult.isValid()) {
            addErrorMessages(validationResult.getErrorCollection().getErrorMessages());
        }
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        final IssueTypeService.DeleteValidationResult validationResult = issueTypeService.validateDeleteIssueType(
                getLoggedInUser(), new IssueTypeService.IssueTypeDeleteInput(id, option(newId)));

        if (!validationResult.isValid()) {
            return ERROR;
        } else {
            issueTypeService.deleteIssueType(getLoggedInUser(), validationResult);
            return getRedirect(getRedirectPage());
        }
    }

    public Collection<IssueType> getAvailableIssueTypes() {
        return Lists.newArrayList(issueTypeService.getAvailableAlternativesForIssueType(getLoggedInUser(), getIssueTypeObject().getId()));
    }

}
