package com.atlassian.jira.license;

import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.event.ClearCacheEvent;
import com.google.common.annotations.VisibleForTesting;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * A {@link MultiLicenseStore} that switches its behaviour based on if renaissance migration has run or not. When the
 * migration has run it will only look at the production data store in the {@link ProductLicense} table. When migration
 * has not run it will also return the licenses from the JIRA 6.3.x and UPM SD locations. We need to do this because
 * there is code that relies on being able to read the licenses before the migration has run
 * (e.g. JIRA will only run the upgrade when all the licenses are in maintenance).
 *
 * @since v7.0
 */
@EventComponent
public class RenaissanceSwitchingMultiLicenseStore implements MultiLicenseStore {
    private final MultiLicenseStore jira6xStore;
    private final MultiLicenseStore jira7xStore;
    private final RenaissanceMigrationStatus predicate;
    private volatile boolean migrationDone = false;

    @Inject
    public RenaissanceSwitchingMultiLicenseStore(final LegacyMultiLicenseStore jira6xStore,
                                                 final MultiLicenseStoreImpl jira7xStore,
                                                 final RenaissanceMigrationStatus predicate) {
        this((MultiLicenseStore) jira6xStore, jira7xStore, predicate);
    }

    @VisibleForTesting
    RenaissanceSwitchingMultiLicenseStore(final MultiLicenseStore jira6xStore,
                                          final MultiLicenseStore jira7xStore,
                                          final RenaissanceMigrationStatus predicate) {
        this.jira6xStore = notNull("jira6xStore", jira6xStore);
        this.jira7xStore = notNull("jira7xStore", jira7xStore);
        this.predicate = notNull("predicate", predicate);
    }

    @Nonnull
    @Override
    public Iterable<String> retrieve() {
        return delegate().retrieve();
    }

    @Override
    public void store(@Nonnull final Iterable<String> licenses) {
        delegate().store(licenses);
    }

    @Override
    public String retrieveServerId() {
        return delegate().retrieveServerId();
    }

    @Override
    public void storeServerId(final String serverId) {
        delegate().storeServerId(serverId);
    }

    @Override
    public void resetOldBuildConfirmation() {
        delegate().resetOldBuildConfirmation();
    }

    @Override
    public void confirmProceedUnderEvaluationTerms(final String userName) {
        delegate().confirmProceedUnderEvaluationTerms(userName);
    }

    @Override
    public void clear() {
        delegate().clear();
    }

    @EventListener
    public void onClearCache(ClearCacheEvent dataImport) {
        migrationDone = false;
    }

    @Nonnull
    private MultiLicenseStore delegate() {
        if (migrationDone) {
            return jira7xStore;
        }

        migrationDone = predicate.hasMigrationRun();
        if (!migrationDone) {
            return jira6xStore;
        } else {
            return jira7xStore;
        }
    }
}
