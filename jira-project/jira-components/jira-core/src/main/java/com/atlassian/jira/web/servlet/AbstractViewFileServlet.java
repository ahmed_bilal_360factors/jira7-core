package com.atlassian.jira.web.servlet;

import com.atlassian.fugue.Unit;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.AttachmentNotFoundException;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.AttachmentReadException;
import com.atlassian.jira.issue.attachment.NoAttachmentDataException;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.http.JiraHttpUtils;
import com.atlassian.jira.util.io.InputStreamConsumer;
import com.atlassian.jira.web.exception.WebExceptionChecker;
import com.atlassian.seraph.util.RedirectUtils;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Optional;

public abstract class AbstractViewFileServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(ViewAttachmentServlet.class);
    private static final int BUFFER_SIZE = 4 * 1024;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String attachmentQuery;
            try {
                attachmentQuery = attachmentQuery(request);
            } catch (InvalidAttachmentPathException e) {
                response.sendError(400, "Invalid attachment path");
                return;
            } catch (AttachmentNotFoundException nfe) {
                send404(request, response);
                return;
            }

            streamFileData(request, response, attachmentQuery);
        } catch (Exception e) {
            if (WebExceptionChecker.canBeSafelyIgnored(e)) {
                return;
            }
            log.error("Error serving file for path " + request.getPathInfo() + ": " + e.getMessage(), e);
            throw new ServletException(e);
        }
    }

    private void send404(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendError(404, String.format("Attachment %s was not found", request.getPathInfo()));
    }

    private void redirectForSecurityBreach(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (getLoggedInUser() != null) {
            RequestDispatcher rd = request.getRequestDispatcher("/secure/views/securitybreach.jsp");
            JiraHttpUtils.setNoCacheHeaders(response);
            rd.forward(request, response);
        } else {
            response.sendRedirect(RedirectUtils.getLoginUrl(request));
        }
    }

    private void streamFileData(final HttpServletRequest request, final HttpServletResponse response, final String attachmentPath)
            throws IOException, ServletException {
        // JDEV-36274 Support Range requests for attachments (RFC 7233)
        final Optional<RangeResponse> rangeResponse;
        try {
            if (supportsRangeRequests()) {
                RangeRequest rangeRequest = parseRangeHeader(request);
                rangeResponse = rangeRequest == null
                        ? Optional.empty()
                        : Optional.of(rangeRequest.calculateRangeResponse(getContentLength(attachmentPath)));
            } else {
                rangeResponse = Optional.empty();
            }
        } catch (BadRequestException ex) {
            send400(response, ex);
            return;
        } catch (RangeNotSatisfiableException ex) {
            send416(response, ex);
            return;
        }

        final OutputStream out = response.getOutputStream();

        try {
            getInputStream(attachmentPath, is -> {
                // can only set headers after knowing that we have the file - otherwise we can't do response.sendError()
                setResponseHeaders(request, rangeResponse, response);

                if (rangeResponse.isPresent()) {
                    copyRange(is, out, rangeResponse.get());
                } else {
                    copyAll(is, out);
                }

                return Unit.Unit();
            });
        } catch (AttachmentNotFoundException | FileNotFoundException | NoAttachmentDataException ex) {
            // JDEV-36419 It seems we keep changing the Exception for "file not found on disk" - latest seems to be NoAttachmentDataException
            log.error("Error finding " + request.getPathInfo() + " : " + ex.getMessage());
            // the outcome of this will only be complete if nothing else has written to the OutputStream, so we must
            // do this before setResponseHeaders() is called
            send404(request, response);
            return;
        } catch (IOException e) {
            // we suspect this to be a Broken Pipe exception, probably due to the user closing the connection by pressing
            // the stop button in their browser, which we don't really care about logging
            if (log.isDebugEnabled()) {
                log.debug("Error serving content to client", e);
            }
        } catch (PermissionException e) {
            redirectForSecurityBreach(request, response);
            return;
        } finally {
            IOUtils.closeQuietly(out);
        }
    }

    private void send400(HttpServletResponse response, BadRequestException ex) throws IOException {
        response.sendError(400, ex.getMessage());
    }

    private void send416(HttpServletResponse response, RangeNotSatisfiableException ex) throws IOException {
        // https://tools.ietf.org/html/rfc7233#section-4.2
        //    A server generating a 416 (Range Not Satisfiable) response to a
        //    byte-range request SHOULD send a Content-Range header field with an
        //    unsatisfied-range value, as in the following example:
        //
        //    Content-Range: bytes */1234

        response.setHeader("Accept-Ranges", "bytes");
        response.setHeader("Content-Range", "bytes */" + ex.getActualContentLength());
        response.sendError(416, ex.getMessage());
    }

    protected abstract int getContentLength(String attachmentPath);

    /**
     * Returns true if this implementation supports RFC 7233 "Range Requests".
     * <p>
     * Returning true means that you must set the appropriate "Content-Range" and "Content-Length" headers for the
     * partial range we will return in the {@code #setResponseHeaders} implementation.
     * </p>
     *
     * @return true if this servlet supports RFC 7233 "Range Requests".
     * @see #setResponseHeaders(HttpServletRequest, Optional, HttpServletResponse)
     */
    protected abstract boolean supportsRangeRequests();

    @VisibleForTesting
    protected void copyRange(InputStream is, OutputStream out, RangeResponse rangeResponse) throws IOException {
        // skip initial bytes
        int idx = rangeResponse.getStartIndex();
        is.skip(idx);
        if (rangeResponse.getEndIndex() == null) {
            // copy the rest of the input stream up to EOF
            IOUtils.copy(is, out);
        } else {
            // write until the non-null endIndex
            final int endIndex = rangeResponse.getEndIndex();
            byte[] buffer = new byte[BUFFER_SIZE];
            while (idx <= endIndex) {
                int numBytesWanted = Math.min(BUFFER_SIZE, endIndex - idx + 1);
                int numBytesRead = is.read(buffer, 0, numBytesWanted);
                if (numBytesRead == -1) {
                    // EOF
                    return;
                }
                out.write(buffer, 0, numBytesRead);
                idx += numBytesRead;
            }
        }
    }

    private void copyAll(InputStream is, OutputStream out) throws IOException {
        IOUtils.copy(is, out);
    }

    @Nullable
    private RangeRequest parseRangeHeader(HttpServletRequest request) throws BadRequestException {
        String headerValue = request.getHeader("Range");
        if (headerValue == null) {
            return null;
        }
        return RangeRequest.parse(headerValue);
    }

    /**
     * Validates that path is valid attachment path.
     *
     * @param request HTTP request
     * @return attachment path
     */
    protected final String attachmentQuery(final HttpServletRequest request) {
        String pi = request.getPathInfo();
        if (pi == null || pi.length() == 1 || pi.indexOf('/', 1) == -1) {
            throw new InvalidAttachmentPathException();
        }
        return pi;
    }

    /**
     * Looks up the attachment by reading the id from the query string.
     *
     * @param query eg. '/10000/foo.txt'
     * @return attachment found
     * @throws AttachmentNotFoundException if no attachment is found after parsing the query string
     *                                     or if the resulting attachment id does not exist.
     * @throws DataAccessException         if there is a problem accessing the database.
     */
    protected Attachment getAttachment(String query) {
        int x = query.indexOf('/', 1);
        final String idStr = query.substring(1, x);
        Long id;
        try {
            id = new Long(idStr);
        } catch (NumberFormatException e) {
            throw new AttachmentNotFoundException(idStr);
        }
        if (query.indexOf('/', x + 1) != -1) {
            // JRA-14580. only one slash is allowed to prevent infinite recursion by web crawlers.
            throw new AttachmentNotFoundException(idStr);
        }

        return ComponentAccessor.getAttachmentManager().getAttachment(id);
    }

    /**
     * Checks if the currently logged in user has permission to see the attachemnt.
     *
     * @param attachment attachment to be checked
     * @return true if user can see the attachment, false otherwise
     * @throws DataAccessException if no such user exists.
     */
    protected boolean loggedInUserHasPermissionToViewAttachment(Attachment attachment) throws DataAccessException {
        Issue issue = attachment.getIssueObject();
        return ComponentAccessor.getPermissionManager().hasPermission(ProjectPermissions.BROWSE_PROJECTS, issue, getLoggedInUser());
    }

    /**
     * Gets the attachment file (not the file name) that corresponds to the requested attachment.
     *
     * @param attachmentPath the attachment path
     * @param consumer       a procedure that consumes the stream with the attachment data.
     * @throws DataAccessException         If there is a problem looking up the data to support the attachment.
     * @throws AttachmentNotFoundException if the attachment does not exist.
     * @throws PermissionException         if the user has insufficient permission to see the attachment.
     * @throws IOException                 if there is a problem getting the attachment.
     * @throws NoAttachmentDataException   if the attachment's contents cannot be found.
     * @throws AttachmentReadException     if a problem occurs when the consumer processes the attachment's contents.
     */
    protected abstract void getInputStream(String attachmentPath, InputStreamConsumer<Unit> consumer)
            throws IOException, PermissionException;

    /**
     * Sets the content type, content length and "Content-Disposition" header
     * of the response based on the values of the attachment found.
     * <p>
     *     The rangeResponse parameter will only be present if this Concrete class declares it supports Range Requests,
     *     and the request actually contains a Range header.
     * </p>
     *
     * @param request  HTTP request
     * @param rangeResponse If present, the partial byte range we will return
     * @param response HTTP response
     *
     * @see #supportsRangeRequests()
     */
    protected abstract void setResponseHeaders(HttpServletRequest request, Optional<RangeResponse> rangeResponse, HttpServletResponse response)
            throws InvalidAttachmentPathException, DataAccessException, IOException;

    protected final ApplicationUser getLoggedInUser() {
        return ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    }
}
