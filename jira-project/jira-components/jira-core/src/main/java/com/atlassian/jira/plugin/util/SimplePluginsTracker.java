package com.atlassian.jira.plugin.util;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;

import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.List;
import java.util.Set;

import static com.atlassian.collectors.CollectorsUtil.toImmutableSet;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * @since v6.2.3
 */
@ThreadSafe
@Immutable
public class SimplePluginsTracker implements PluginsTracker {
    private final Set<PluginInfo> pluginsInvolved;

    public SimplePluginsTracker(final List<Plugin> trackedPlugins) {
        this.pluginsInvolved = trackedPlugins.stream().map(this::toInfo).collect(toImmutableSet());
    }

    private PluginInfo toInfo(final Plugin plugin) {
        return new PluginInfo(plugin.getKey(), plugin.getPluginInformation().getVersion());
    }


    @Override
    public boolean isPluginInvolved(final Plugin plugin) {
        return pluginsInvolved.contains(toInfo(notNull("plugin", plugin)));
    }

    @Override
    public boolean isPluginInvolved(final ModuleDescriptor moduleDescriptor) {
        return pluginsInvolved.contains(toInfo(notNull("moduleDescriptor", moduleDescriptor).getPlugin()));
    }

    @Override
    public String getStateHashCode() {
        return Integer.toString(pluginsInvolved.hashCode(), Character.MAX_RADIX);
    }

    @Override
    public String toString() {
        return super.toString() + " " + pluginsInvolved.toString();
    }
}
