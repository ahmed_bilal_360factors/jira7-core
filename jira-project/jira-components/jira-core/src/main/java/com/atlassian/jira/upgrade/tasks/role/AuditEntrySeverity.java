package com.atlassian.jira.upgrade.tasks.role;

public enum AuditEntrySeverity {
    INFO, WARNING
}
