package com.atlassian.jira.issue.fields.renderer.wiki;

import com.atlassian.jira.issue.fields.renderer.wiki.links.JiraAttachmentLink;
import com.atlassian.renderer.DefaultIconManager;
import com.atlassian.renderer.Icon;
import com.atlassian.renderer.links.UrlLink;
import com.atlassian.renderer.util.RendererProperties;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import java.util.HashMap;
import java.util.Map;

/**
 * Icon manager for Jira that adds icons that the renderer will show that are specific to Jira.
 */
public class JiraIconManager extends DefaultIconManager {
    private volatile Map<String, Icon> emoticonsMap;

    protected Map getIconsMap() {
        @SuppressWarnings({"unchecked"})
        Map<String, Icon> iconsMap = super.getIconsMap();
        if (iconsMap != null) {
            iconsMap = new HashMap<String, Icon>(iconsMap);
            iconsMap.put(JiraAttachmentLink.ATTACHMENT_ICON, Icon.makeRenderIcon("icons/link_attachment_7.gif", Icon.ICON_RIGHT, 7, 7));
            // JRADEV-825
            iconsMap.remove(UrlLink.EXTERNAL_ICON);
        }
        return iconsMap;
    }


    private Map<String, Icon> generateEmoticonsMap() {
        Map<String, Icon> emoticonsMap = super.getEmoticonsMap();
        if (emoticonsMap != null) {
            emoticonsMap = addPngIcons(emoticonsMap);
        }
        return emoticonsMap;
    }

    @Override
    protected Map<String, Icon> getEmoticonsMap() {
        if (emoticonsMap == null) {
            emoticonsMap = ImmutableMap.copyOf(generateEmoticonsMap());
        }
        return emoticonsMap;
    }

    private Map<String, Icon> addPngIcons(Map<String, Icon> originalMap) {
        Map<String, Icon> resultMap = Maps.newHashMap(originalMap);
        final String emoticonsPath = RendererProperties.EMOTICONS_PATH;

        resultMap.put(":-)", Icon.makeEmoticon(emoticonsPath + "smile.png", 16, 16));
        resultMap.put(":)", Icon.makeEmoticon(emoticonsPath + "smile.png", 16, 16));
        resultMap.put(":P", Icon.makeEmoticon(emoticonsPath + "tongue.png", 16, 16));
        resultMap.put(":p", Icon.makeEmoticon(emoticonsPath + "tongue.png", 16, 16));
        resultMap.put(";-)", Icon.makeEmoticon(emoticonsPath + "wink.png", 16, 16));
        resultMap.put(";)", Icon.makeEmoticon(emoticonsPath + "wink.png", 16, 16));
        resultMap.put(":D", Icon.makeEmoticon(emoticonsPath + "biggrin.png", 16, 16));
        resultMap.put(":-(", Icon.makeEmoticon(emoticonsPath + "sad.png", 16, 16));
        resultMap.put(":(", Icon.makeEmoticon(emoticonsPath + "sad.png", 16, 16));
        resultMap.put("(y)", Icon.makeEmoticon(emoticonsPath + "thumbs_up.png", 16, 16));
        resultMap.put("(n)", Icon.makeEmoticon(emoticonsPath + "thumbs_down.png", 16, 16));
        resultMap.put("(i)", Icon.makeEmoticon(emoticonsPath + "information.png", 16, 16));
        resultMap.put("(/)", Icon.makeEmoticon(emoticonsPath + "check.png", 16, 16));
        resultMap.put("(x)", Icon.makeEmoticon(emoticonsPath + "error.png", 16, 16));
        resultMap.put("(+)", Icon.makeEmoticon(emoticonsPath + "add.png", 16, 16));
        resultMap.put("(-)", Icon.makeEmoticon(emoticonsPath + "forbidden.png", 16, 16));
        resultMap.put("(!)", Icon.makeEmoticon(emoticonsPath + "warning.png", 16, 16));
        resultMap.put("(?)", Icon.makeEmoticon(emoticonsPath + "help_16.png", 16, 16));
        resultMap.put("(on)", Icon.makeEmoticon(emoticonsPath + "lightbulb_on.png", 16, 16));
        resultMap.put("(off)", Icon.makeEmoticon(emoticonsPath + "lightbulb.png", 16, 16));
        resultMap.put("(*)", Icon.makeEmoticon(emoticonsPath + "star_yellow.png", 16, 16));
        resultMap.put("(*b)", Icon.makeEmoticon(emoticonsPath + "star_blue.png", 16, 16));
        resultMap.put("(*y)", Icon.makeEmoticon(emoticonsPath + "star_yellow.png", 16, 16));
        resultMap.put("(*g)", Icon.makeEmoticon(emoticonsPath + "star_green.png", 16, 16));
        resultMap.put("(*r)", Icon.makeEmoticon(emoticonsPath + "star_red.png", 16, 16));
        resultMap.put("(flag)", Icon.makeEmoticon(emoticonsPath + "flag.png", 16, 16));
        resultMap.put("(flagoff)", Icon.makeEmoticon(emoticonsPath + "flag_grey.png", 16, 16));

        return resultMap;
    }
}