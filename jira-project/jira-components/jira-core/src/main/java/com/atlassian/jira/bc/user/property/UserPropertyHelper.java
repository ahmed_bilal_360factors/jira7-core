package com.atlassian.jira.bc.user.property;

import com.atlassian.fugue.Function2;
import com.atlassian.fugue.Option;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.entity.property.EntityWithKeyPropertyHelper;
import com.atlassian.jira.event.entity.EntityPropertyDeletedEvent;
import com.atlassian.jira.event.entity.EntityPropertySetEvent;
import com.atlassian.jira.event.user.property.UserPropertyDeletedEvent;
import com.atlassian.jira.event.user.property.UserPropertySetEvent;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollections;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.base.Function;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.jira.permission.GlobalPermissionKey.SYSTEM_ADMIN;

/**
 * @since v6.5
 */
public class UserPropertyHelper implements EntityWithKeyPropertyHelper<ApplicationUser> {
    protected final GlobalPermissionManager permissionManager;
    private final UserManager userManager;
    private final I18nHelper i18n;

    public UserPropertyHelper(final UserManager userManager, final GlobalPermissionManager permissionManager, final I18nHelper i18n) {
        this.userManager = userManager;
        this.permissionManager = permissionManager;
        this.i18n = i18n;
    }

    @Override
    public CheckPermissionFunction<ApplicationUser> hasReadPermissionFunction() {
        return (currentUser, editedUser) -> errorIfPermissionCheckFailed(hasPermissionsToReadProperties(currentUser, editedUser),
                i18n.getText("user.properties.forbidden.read", currentUser.getUsername(), editedUser.getUsername()));
    }

    @Override
    public CheckPermissionFunction<ApplicationUser> hasEditPermissionFunction() {
        return (currentUser, editedUser) -> errorIfPermissionCheckFailed(hasPermissionsToEditProperties(currentUser, editedUser),
                i18n.getText("user.properties.forbidden.edit", currentUser.getUsername(), editedUser.getUsername()));
    }

    @Override
    public Function<Long, Option<ApplicationUser>> getEntityByIdFunction() {
        return id -> option(userManager.getUserById(id).orElse(null));
    }

    @Override
    public Function<String, Option<ApplicationUser>> getEntityByKeyFunction() {
        return key -> option(userManager.getUserByKey(key));
    }

    @Override
    public Function2<ApplicationUser, EntityProperty, ? extends EntityPropertySetEvent> createSetPropertyEventFunction() {
        return (currentUser, property) -> new UserPropertySetEvent(property, currentUser);
    }

    @Override
    public Function2<ApplicationUser, EntityProperty, ? extends EntityPropertyDeletedEvent> createDeletePropertyEventFunction() {
        return (currentUser, property) -> new UserPropertyDeletedEvent(property, currentUser);
    }

    private ErrorCollection errorIfPermissionCheckFailed(boolean hasPermission, String errorMessage) {
        return hasPermission ? ErrorCollections.empty() : ErrorCollections.create(errorMessage, ErrorCollection.Reason.FORBIDDEN);
    }


    private boolean hasPermissionsToReadProperties(final ApplicationUser currentUser, final ApplicationUser accessedUser) {
        return hasPermissionsToEditProperties(currentUser, accessedUser);
    }

    private boolean hasPermissionsToEditProperties(ApplicationUser currentUser, ApplicationUser accessedUser) {
        return isTheSameUser(currentUser, accessedUser) || canAdministerOtherUser(currentUser, accessedUser);
    }

    @Override
    public EntityPropertyType getEntityPropertyType() {
        return EntityPropertyType.USER_PROPERTY;
    }


    protected final boolean canAdministerOtherUser(ApplicationUser currentUser, ApplicationUser accessedUser) {
        return permissionManager.hasPermission(SYSTEM_ADMIN, currentUser) ||
                permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, currentUser) && !permissionManager.hasPermission(SYSTEM_ADMIN, accessedUser); // regular admin cannot administer system admin
    }

    protected final boolean isTheSameUser(ApplicationUser user1, ApplicationUser user2) {
        return user1.getKey().equals(user2.getKey());
    }
}
