package com.atlassian.jira.upgrade;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.bc.license.JiraLicenseService.ValidationResult;
import com.atlassian.jira.event.JiraUpgradedEvent;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.license.BuildVersionLicenseCheck;
import com.atlassian.jira.license.LicenseCheck;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.upgrade.util.BuildNumberDao;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.upgrade.api.UpgradeContext;
import com.atlassian.upgrade.core.HostUpgradeTaskCollector;
import com.atlassian.upgrade.core.UpgradeTaskManager;
import com.atlassian.upgrade.core.dao.UpgradeTaskHistoryDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class LicenseCheckingUpgradeService {
    private static final Logger LOGGER = LoggerFactory.getLogger(LicenseCheckingUpgradeService.class);

    private static final String RUNNING_UPGRADES_CALLABLE_EXCEPTION_ERROR =
            "There was an exception thrown when trying to complete the upgrades";
    private static final String REINDEX_FAILURE = "There was a failure when running the reindex";
    private static final String UPGRADE_FRAMEWORK_FAILED_UPGRADE_ERROR =
            "The upgrade task manager has returned a non-successful result from running the upgrades";

    private final UpgradeTaskHistoryDao upgradeTaskHistoryDao;
    private final UpgradeTaskManager upgradeTaskManager;
    private final BuildNumberDao buildNumberDao;
    private final BuildUtilsInfo buildUtilsInfo;
    private final UpgradeVersionHistoryManager upgradeVersionHistoryManager;
    private final UpgradeIndexManager upgradeIndexManager;
    private final JiraLicenseService jiraLicenseService;
    private final BuildVersionLicenseCheck buildVersionLicenseCheck;
    private final I18nHelper.BeanFactory i18nHelperFactory;
    private final EventPublisher eventPublisher;
    private final OfBizDelegator ofBizDelegator;

    public LicenseCheckingUpgradeService(
            final UpgradeTaskHistoryDao upgradeTaskHistoryDao,
            final UpgradeTaskManager upgradeTaskManager,
            final BuildNumberDao buildNumberDao,
            final BuildUtilsInfo buildUtilsInfo,
            final UpgradeVersionHistoryManager upgradeVersionHistoryManager,
            final UpgradeIndexManager upgradeIndexManager,
            final JiraLicenseService jiraLicenseService,
            final BuildVersionLicenseCheck buildVersionLicenseCheck,
            final I18nHelper.BeanFactory i18nHelperFactory,
            final EventPublisher eventPublisher,
            final OfBizDelegator ofBizDelegator
            ) {
        this.upgradeTaskHistoryDao = upgradeTaskHistoryDao;
        this.upgradeTaskManager = upgradeTaskManager;
        this.buildNumberDao = buildNumberDao;
        this.buildUtilsInfo = buildUtilsInfo;
        this.upgradeVersionHistoryManager = upgradeVersionHistoryManager;
        this.upgradeIndexManager = upgradeIndexManager;
        this.jiraLicenseService = jiraLicenseService;
        this.buildVersionLicenseCheck = buildVersionLicenseCheck;
        this.i18nHelperFactory = i18nHelperFactory;
        this.eventPublisher = eventPublisher;
        this.ofBizDelegator = ofBizDelegator;
    }

    public UpgradeResult runUpgrades(
            final Set<ReindexRequestType> reindexRequestTypes,
            final UpgradeContext upgradeContext
    ) {
        if (jiraLicenseService.isLicenseSet()) {
            final LicenseCheck.Result licenseCheckResult = buildVersionLicenseCheck.evaluate();

            if (!licenseCheckResult.isPass()) {
                LOGGER.error("Cannot proceed with upgrades. License not valid");
                return new UpgradeResult(licenseCheckResult.getFailureMessage());
            }

            final I18nHelper licenseValidationLocale = i18nHelperFactory.getInstance(Locale.ENGLISH);
            final UpgradeResult validationResult = deriveStatus(jiraLicenseService.validate(licenseValidationLocale));

            if (!validationResult.successful()) {
                LOGGER.error("The current licenses are incompatible with this installation of JIRA.");
                return validationResult;
            }
        }

        final UpgradeResult upgradeResult = executeUpgrades(reindexRequestTypes, upgradeContext);

        if (upgradeResult.successful()) {
            eventPublisher.publish(new JiraUpgradedEvent(false));
        }

        return upgradeResult;
    }

    private UpgradeResult deriveStatus(final Iterable<ValidationResult> validationResults) {
        final List<String> errors = new ArrayList<>();
        for (ValidationResult result : validationResults) {
            final ErrorCollection errorCollection = result.getErrorCollection();
            errors.addAll(errorCollection.getErrorMessages());

            for (Map.Entry<String, String> entry : errorCollection.getErrors().entrySet()) {
                final LicenseDetails license = result.getLicenseDetails();
                if (license != null) {
                    errors.add(String.format("%s for %s : %s",
                            entry.getKey(), license.getApplicationDescription(), entry.getValue()));
                } else {
                    errors.add(entry.getKey() + " : " + entry.getValue());
                }
            }
        }

        return new UpgradeResult(errors);
    }

    private UpgradeResult executeUpgrades(
            final Set<ReindexRequestType> reindexRequestTypes,
            final UpgradeContext upgradeContext
    ) {
        final UpgradeResult result = new UpgradeResult();

        try {
            final int oldHostBuildNumber = upgradeTaskHistoryDao.getDatabaseBuildNumber(HostUpgradeTaskCollector.HOST_APP_KEY);
            final boolean upgradesSuccessful = upgradeTaskManager.upgradeHostApp(upgradeContext);
            final int newHostBuildNumber = upgradeTaskHistoryDao.getDatabaseBuildNumber(HostUpgradeTaskCollector.HOST_APP_KEY);

            if (upgradesSuccessful) {
                buildNumberDao.setDatabaseBuildNumber(buildUtilsInfo.getCurrentBuildNumber());

                final boolean someHostUpgradesWereRun = (oldHostBuildNumber != newHostBuildNumber);
                if (someHostUpgradesWereRun) {
                    upgradeVersionHistoryManager.addUpgradeVersionHistory(newHostBuildNumber, buildUtilsInfo.getVersion());

                    logUpgradedToVersion(newHostBuildNumber);
                }

                final boolean foregroundReindexFailed = !upgradeIndexManager.runReindexIfNeededAndAllowed(reindexRequestTypes);
                if(foregroundReindexFailed){
                    LOGGER.error("There was an error trying to complete the re-index after an upgrade");
                    result.addError(REINDEX_FAILURE);
                }
            } else {
                buildNumberDao.setDatabaseBuildNumber(String.valueOf(newHostBuildNumber));

                LOGGER.error("There was an error running the upgrades");
                result.addError(UPGRADE_FRAMEWORK_FAILED_UPGRADE_ERROR);
            }

            //We still want to set these versions into the system
            buildNumberDao.setJiraVersion(buildUtilsInfo.getVersion());
            buildNumberDao.setMinimumDowngradeVersion();
        } catch (Exception e) {
            LOGGER.error("An exception occurred when running to call a callable method, this shouldn't happen.", e);
            result.addError(RUNNING_UPGRADES_CALLABLE_EXCEPTION_ERROR);
        } finally {
            ofBizDelegator.refreshSequencer();
        }

        return result;
    }

    private void logUpgradedToVersion(final int newBuildNumber) {
        final String msg = "\n\n"
                + "*******************************************************************\n"
                + " Upgrade Succeeded! JIRA has been upgraded to build number " + newBuildNumber + '\n'
                + "*******************************************************************\n";
        LOGGER.info(msg);
    }
}
