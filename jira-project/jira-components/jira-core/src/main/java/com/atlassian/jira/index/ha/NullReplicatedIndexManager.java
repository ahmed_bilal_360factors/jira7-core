package com.atlassian.jira.index.ha;

import com.atlassian.jira.entity.WithId;
import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.index.IssueIndexingParams;
import com.atlassian.jira.issue.util.IssuesIterable;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.sharing.SharedEntity;

import java.util.Collection;
import java.util.Set;

/**
 * A no-op implementation of the ReplicatedIndexManager. Used when JIRA is not clustered.
 *
 * @since v6.5
 */
public class NullReplicatedIndexManager implements ReplicatedIndexManager {
    @Override
    public void reindexIssues(IssuesIterable issuesIterable, IssueIndexingParams indexingParams) {
        // do nothing
    }

    @Override
    public void reindexComments(Collection<Comment> comments) {
        // do nothing
    }

    @Override
    public void reindexWorklogs(Collection<Worklog> worklogs) {
        // do nothing
    }

    @Override
    public <T extends WithId> void reindexEntity(Collection<T> entities, AffectedIndex index) {
        // do nothing
    }

    @Override
    public void deIndexIssues(Set<Issue> issuesToDelete) {
        // do nothing
    }

    @Override
    public void reindexProject(Project project) {
        // do nothing
    }

    @Override
    public void indexSharedEntity(SharedEntity entity) {
        // do nothing
    }

    @Override
    public void deIndexSharedEntity(SharedEntity entity) {
        // do nothing
    }
}
