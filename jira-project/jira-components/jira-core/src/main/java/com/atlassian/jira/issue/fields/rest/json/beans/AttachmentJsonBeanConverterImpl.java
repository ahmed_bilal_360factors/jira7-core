package com.atlassian.jira.issue.fields.rest.json.beans;

import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.thumbnail.ThumbnailManager;
import com.atlassian.jira.issue.thumbnail.ThumbnailedImage;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.jira.util.JiraUrlCodec;
import com.google.common.collect.Lists;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collection;

/**
 * @since v6.5
 */
public class AttachmentJsonBeanConverterImpl implements AttachmentJsonBeanConverter {
    private final JiraBaseUrls urls;
    private final ThumbnailManager thumbnailManager;
    private final JiraAuthenticationContext authenticationContext;
    private final EmailFormatter emailFormatter;
    private final TimeZoneManager timeZoneManager;
    private final UserBeanFactory userBeanFactory;

    public AttachmentJsonBeanConverterImpl(final JiraBaseUrls urls, final ThumbnailManager thumbnailManager, final JiraAuthenticationContext authenticationContext, final EmailFormatter emailFormatter, final TimeZoneManager timeZoneManager, final UserBeanFactory userBeanFactory) {
        this.urls = urls;
        this.thumbnailManager = thumbnailManager;
        this.authenticationContext = authenticationContext;
        this.emailFormatter = emailFormatter;
        this.timeZoneManager = timeZoneManager;
        this.userBeanFactory = userBeanFactory;
    }

    @Override
    public Collection<AttachmentJsonBean> shortBeans(final Collection<Attachment> attachments) {
        Collection<AttachmentJsonBean> result = Lists.newArrayListWithCapacity(attachments.size());
        for (Attachment from : attachments) {
            result.add(shortBean(from));
        }

        return result;
    }

    @Override
    public AttachmentJsonBean shortBean(final Attachment attachment) {
        if (attachment == null) {
            return null;
        }
        final AttachmentJsonBean bean;
        try {
            bean = new AttachmentJsonBean();
            bean.setSelf(urls.restApi2BaseUrl() + "attachment/" + JiraUrlCodec.encode(attachment.getId().toString()));
            bean.setId(attachment.getId().toString());
            bean.setFilename(attachment.getFilename());
            bean.setSize(attachment.getFilesize());
            bean.setMimeType(attachment.getMimetype());
            ApplicationUser author = attachment.getAuthorObject();
            bean.setAuthor(userBeanFactory.createBean(author, authenticationContext.getUser(), urls, emailFormatter, timeZoneManager));
            bean.setCreated(attachment.getCreated());
            String encodedFilename = URLEncoder.encode(attachment.getFilename(), "UTF-8");
            bean.setContent(String.format("%s/secure/attachment/%s/%s", urls.baseUrl(), attachment.getId(), encodedFilename));

            ThumbnailedImage thumbnail = thumbnailManager.toThumbnailedImage(thumbnailManager.getThumbnail(attachment.getIssueObject(), attachment));
            if (thumbnail != null) {
                bean.setThumbnail(thumbnail.getImageURL());
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Error encoding file name", e);
        }

        return bean;
    }
}
