package com.atlassian.jira.issue.priority;

import com.atlassian.jira.issue.IssueConstantImpl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.BaseUrl;
import com.atlassian.jira.web.action.admin.translation.TranslationManager;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.ofbiz.core.entity.GenericValue;

import java.util.Map;

public class PriorityImpl extends IssueConstantImpl implements Priority {
    private final GenericValue genericValue;
    private static final Map<String, String> SVG_PRIORITY_ICONS = new ImmutableMap.Builder<String, String>()
            .put("/images/icons/priorities/blocker.png", "/images/icons/priorities/blocker.svg")
            .put("/images/icons/priorities/critical.png", "/images/icons/priorities/critical.svg")
            .put("/images/icons/priorities/high.png", "/images/icons/priorities/high.svg")
            .put("/images/icons/priorities/highest.png", "/images/icons/priorities/highest.svg")
            .put("/images/icons/priorities/low.png", "/images/icons/priorities/low.svg")
            .put("/images/icons/priorities/lowest.png", "/images/icons/priorities/lowest.svg")
            .put("/images/icons/priorities/major.png", "/images/icons/priorities/major.svg")
            .put("/images/icons/priorities/medium.png", "/images/icons/priorities/medium.svg")
            .put("/images/icons/priorities/minor.png", "/images/icons/priorities/minor.svg")
            .put("/images/icons/priorities/trivial.png", "/images/icons/priorities/trivial.svg")
            .build();

    public PriorityImpl(GenericValue genericValue, TranslationManager translationManager,
                        JiraAuthenticationContext authenticationContext, BaseUrl locator) {
        super(genericValue, translationManager, authenticationContext, locator);
        this.genericValue = genericValue;
    }

    public String getStatusColor() {
        return genericValue.getString("statusColor");
    }

    public void setStatusColor(final String statusColor) {
        genericValue.setString("statusColor", statusColor);
    }

    @Override
    public String getSvgIconUrl() {
        return SVG_PRIORITY_ICONS.get(getRasterIconUrl());
    }

    @Override
    public String getRasterIconUrl() {
        return super.getIconUrl();
    }

    @Override
    public String getIconUrl() {
        final String svgImage = getSvgIconUrl();
        if (StringUtils.isBlank(svgImage)) {
            return getRasterIconUrl();
        }
        return svgImage;
    }
}
