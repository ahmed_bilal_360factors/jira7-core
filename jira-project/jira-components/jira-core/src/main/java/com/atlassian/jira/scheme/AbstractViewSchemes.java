package com.atlassian.jira.scheme;

import com.atlassian.jira.project.Project;

import java.util.List;

public abstract class AbstractViewSchemes extends AbstractSchemeAwareAction {
    public List<Scheme> getSchemeObjects() {
        return getSchemeManager().getSchemeObjects();
    }

    public List<Project> getProjects(Scheme scheme) {
        return getSchemeManager().getProjects(scheme);
    }
}
