package com.atlassian.jira.project;

import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.model.querydsl.ProjectChangedTimeDTO;

import java.sql.Timestamp;
import java.util.Optional;

import static com.atlassian.jira.model.querydsl.QProjectChangedTime.PROJECT_CHANGED_TIME;

public class ProjectChangedTimeStoreImpl implements ProjectChangedTimeStore {
    private final QueryDslAccessor queryDslAccessor;

    public ProjectChangedTimeStoreImpl(QueryDslAccessor queryDslAccessor) {
        this.queryDslAccessor = queryDslAccessor;
    }

    @Override
    public Optional<ProjectChangedTime> getProjectChangedTime(long projectId) {
        ProjectChangedTimeDTO projectChangedDto = queryDslAccessor.executeQuery(
                dbConnection -> dbConnection.newSqlQuery()
                        .select(PROJECT_CHANGED_TIME)
                        .from(PROJECT_CHANGED_TIME)
                        .where(PROJECT_CHANGED_TIME.projectId.eq(projectId))
                        .fetchOne());

        return Optional.ofNullable(projectChangedDto)
                       .map(ProjectChangedTimeImpl::new);
    }

    @Override
    public void updateOrAddIssueChangedTime(long projectId, Timestamp issueChangedTime) {
        queryDslAccessor.execute(dbConnection -> {
            long rowCount = dbConnection.update(PROJECT_CHANGED_TIME)
                    .set(PROJECT_CHANGED_TIME.issueChangedTime, issueChangedTime)
                    .where(PROJECT_CHANGED_TIME.projectId.eq(projectId))
                    .execute();
            if (rowCount == 0) {
                dbConnection.insert(PROJECT_CHANGED_TIME)
                        .set(PROJECT_CHANGED_TIME.projectId, projectId)
                        .set(PROJECT_CHANGED_TIME.issueChangedTime, issueChangedTime)
                        .execute();
            }
        });
    }
}
