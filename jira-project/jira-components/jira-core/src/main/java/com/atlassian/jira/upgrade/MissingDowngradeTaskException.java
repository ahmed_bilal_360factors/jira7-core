package com.atlassian.jira.upgrade;

/**
 * Indicates that the downgrade cannot proceed because at least one required Downgrade task is missing.
 *
 * @since v6.4.6
 */
public class MissingDowngradeTaskException extends DowngradeException {
    public MissingDowngradeTaskException(final String message) {
        super(message);
    }
}
