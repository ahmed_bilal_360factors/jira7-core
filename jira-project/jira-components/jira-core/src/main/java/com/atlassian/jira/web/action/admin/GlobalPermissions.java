package com.atlassian.jira.web.action.admin;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.GlobalPermissionType;
import com.atlassian.jira.security.GlobalPermissionEntry;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.util.GlobalPermissionGroupAssociationUtil;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.atlassian.jira.web.util.ExternalLinkUtil;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.base.Supplier;
import com.google.common.collect.Maps;
import com.opensymphony.util.TextUtils;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;
import static com.atlassian.jira.permission.GlobalPermissionKey.BULK_CHANGE;
import static com.atlassian.jira.permission.GlobalPermissionKey.CREATE_SHARED_OBJECTS;
import static com.atlassian.jira.permission.GlobalPermissionKey.MANAGE_GROUP_FILTER_SUBSCRIPTIONS;
import static com.atlassian.jira.permission.GlobalPermissionKey.SYSTEM_ADMIN;
import static com.atlassian.jira.permission.GlobalPermissionKey.USE;
import static com.atlassian.jira.permission.GlobalPermissionKey.USER_PICKER;
import static java.util.Arrays.asList;

@SuppressWarnings("UnusedDeclaration")
@WebSudoRequired
public class GlobalPermissions extends ProjectActionSupport {
    private static final String EXTERNAL_LINK_GLOBAL_PERMISSIONS_MANAGEMENT = "external.link.global.permissions.management";

    private static final class Actions {
        private static final String VIEW = "view";
        private static final String ADD = "add";
        private static final String DEL = "del";
        private static final String DELETE = "delete";
        private static final String CONFIRM = "confirm";
    }

    private Map<String, String> globalPermTypes;
    private String groupName;
    private String globalPermTypeName = "";
    private String action = Actions.VIEW;
    private final GlobalPermissionGroupAssociationUtil globalPermissionGroupAssociationUtil;

    private final GlobalPermissionManager globalPermissionManager;
    private final GroupManager groupManager;
    private final ApplicationRoleManager applicationRoleManager;
    private final ExternalLinkUtil externalLinkUtil;

    public GlobalPermissions(GlobalPermissionManager globalPermissionManager,
                             GlobalPermissionGroupAssociationUtil globalPermissionGroupAssociationUtil,
                             GroupManager groupManager, ApplicationRoleManager applicationRoleManager,
                             final ExternalLinkUtil externalLinkUtil) {
        super();
        this.globalPermissionGroupAssociationUtil = globalPermissionGroupAssociationUtil;
        this.globalPermissionManager = globalPermissionManager;
        this.groupManager = groupManager;
        this.applicationRoleManager = applicationRoleManager;
        this.externalLinkUtil = externalLinkUtil;
    }

    @Override
    public String doDefault() throws Exception {
        return SUCCESS;
    }

    public void doValidation() {
        if (StringUtils.isNotBlank(globalPermTypeName)) {
            Option<GlobalPermissionType> globalPermissionOpt = globalPermissionManager.getGlobalPermission(globalPermTypeName);
            Group group = groupName == null ? null : groupManager.getGroup(groupName);

            if (globalPermissionOpt.isEmpty()) {
                addError("groupName", getText("admin.errors.permissions.inexistent.permission"));
            } else {
                if (!isAnonymous() && group == null // Group not exists
                        && !Actions.DEL.equals(action) && !Actions.CONFIRM.equals(action)) //It's VIEW, ADD or DELETE
                {
                    addError("groupName", getText("admin.errors.permissions.inexistent.group", "'" + groupName + "'"));
                }

                GlobalPermissionType globalPermissionType = globalPermissionOpt.get();
                validateAdd(group, globalPermissionType);
                validateDelete(globalPermissionType);
            }
        } else if (Actions.ADD.equals(action)) {
            addError("groupName", getText("admin.errors.permissions.must.select.permission"));
        }

        super.doValidation();
    }

    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        if (StringUtils.isNotBlank(globalPermTypeName)) {
            Option<GlobalPermissionType> globalPermissionOpt = globalPermissionManager.getGlobalPermission(globalPermTypeName);
            if (globalPermissionOpt.isDefined()) {
                GlobalPermissionType globalPermissionType = globalPermissionOpt.get();
                if (Actions.DEL.equals(action)) {
                    removePermission(globalPermissionType, groupName);
                    action = Actions.VIEW;
                    return getPermissionRedirect();
                } else if (Actions.CONFIRM.equals(action)) {
                    return "confirm";
                } else if (Actions.ADD.equals(action)) {
                    final Group group = (groupName == null ? null : groupManager.getGroup(groupName));
                    addPermission(globalPermissionType, group);
                    return getPermissionRedirect();
                }
            }
        }

        return getResult();
    }

    private void validateDelete(GlobalPermissionType globalPermissionType) {
        if (!Actions.DEL.equals(action) && !Actions.CONFIRM.equals(action)) {
            return;
        }

        if (groupName != null) {
            //check that the group we're trying to remove is part of one of the permissions
            final Collection<String> groupNames = globalPermissionManager.getGroupNamesWithPermission(globalPermissionType.getGlobalPermissionKey());
            if (!groupNames.contains(groupName)) {
                addErrorMessage(getText("admin.errors.permissions.delete.group.not.in.permission", groupName,
                        getText(globalPermissionType.getNameI18nKey())));
            }
        }

        if (!isManagedByJira(globalPermTypeName)) {
            addErrorMessage(getText("admin.errors.permissions.permission.not.managed.by.jira"));
        }
        // if we're deleting an admin permission, check that another group exists
        if (globalPermTypeName.equals(ADMINISTER.getKey())) {
            boolean removingAllPerms = globalPermissionGroupAssociationUtil.isRemovingAllMyAdminGroups(asList(groupName), getLoggedInUser())
                    && !globalPermissionManager.hasPermission(SYSTEM_ADMIN, getLoggedInUser());

            if (removingAllPerms) {
                addErrorMessage(getText("admin.errors.permissions.no.permission"));
            }
        } else if (globalPermTypeName.equals(SYSTEM_ADMIN.getKey())) {
            if (!globalPermissionManager.hasPermission(SYSTEM_ADMIN, getLoggedInUser())) {
                addErrorMessage(getText("admin.errors.permissions.no.permission.sys.admin.only"));
            } else if (globalPermissionGroupAssociationUtil.isRemovingAllMySysAdminGroups(asList(groupName), getLoggedInUser())) {
                addErrorMessage(getText("admin.errors.permissions.no.permission.sys.admin"));
            }
        }
    }

    private void validateAdd(@Nullable Group group, GlobalPermissionType globalPermissionType) {
        if (!Actions.ADD.equals(action)) {
            return;
        }

        if (isAnonymous()) {
            if (!globalPermissionType.isAnonymousAllowed()) {
                addError("groupName", getText("admin.errors.permissions.group.notallowed.for.permission",
                        getText("admin.common.words.anyone"), getText(globalPermissionType.getNameI18nKey())));
            }
        } else if (group != null) { // Group exists
            if (globalPermTypeName.equals(USE.getKey()) && getAdministrativeGroups().contains(group)) {
                // JRA-22984 Prevent admin groups from being added to JIRA USE permisson
                addError("groupName", getText("admin.errors.permissions.group.notallowed.for.permission",
                        groupName, getText(globalPermissionType.getNameI18nKey())));
            }
        }

        if (!getGlobalPermTypes().containsKey(globalPermTypeName)) {
            addErrorMessage(getText("admin.errors.permissions.not.have.permission.to.add"));
        } else if (!isManagedByJira(globalPermTypeName)) {
            addErrorMessage(getText("admin.errors.permissions.permission.not.managed.by.jira"));
        }
    }

    private boolean isAnonymous() {
        return groupName == null;
    }

    private String getPermissionRedirect() throws Exception {
        return getRedirect("GlobalPermissions!default.jspa");
    }

    private void addPermission(GlobalPermissionType globalPermissionType, Group group) {
        String groupName = (group == null ? null : group.getName());
        // We need to get all the groupNames for the permission type and see if the list contains this groupName
        if (!globalPermissionManager.getGroupNamesWithPermission(globalPermissionType.getGlobalPermissionKey()).contains(groupName)) {
            globalPermissionManager.addPermission(globalPermissionType, groupName);
        }
    }

    private void removePermission(GlobalPermissionType globalPermissionType, String groupName) {
        final Group group = (groupName == null ? null : groupManager.getGroup(groupName));

        String groupToDelete = null;
        if (group != null) {
            groupToDelete = group.getName();
        } else if (groupName != null) {
            //JRA-15911: Check if by chance the group returned was null but we did have a groupName
            groupToDelete = groupName;
        }
        globalPermissionManager.removePermission(globalPermissionType, groupToDelete);
    }

    public Collection<GlobalPermissionEntry> getPermissionGroups(String globalPermType) {
        return globalPermissionManager.getPermissions(GlobalPermissionKey.of(globalPermType));
    }

    public Collection getGroups() {
        return groupManager.getAllGroups();
    }

    public String getGlobalPermType() {
        return globalPermTypeName;
    }

    public String getPermTypeName() {
        return globalPermissionManager.getGlobalPermission(globalPermTypeName).fold(
                returnUnknownString,
                globalPermissionType -> getText(globalPermissionType.getNameI18nKey())
        );
    }

    public void setGlobalPermType(String globalPermType) {
        this.globalPermTypeName = globalPermType;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        if (TextUtils.stringSet(groupName)) {
            this.groupName = groupName;
        } else {
            this.groupName = null;
        }
    }

    public void setAction(String action) {
        if (Actions.DEL.equalsIgnoreCase(action) || Actions.DELETE.equalsIgnoreCase(action)) {
            this.action = Actions.DEL;
        } else if (Actions.CONFIRM.equalsIgnoreCase(action)) {
            this.action = Actions.CONFIRM;
        } else {
            this.action = Actions.ADD;
        }
    }

    public boolean isConfirm() {
        return Actions.CONFIRM.equalsIgnoreCase(action);
    }

    public Map<String, String> getGlobalPermTypes() {
        if (globalPermTypes == null) {
            globalPermTypes = new LinkedHashMap<>();

            // Only show the SYS_ADMIN perm to those who have it. We know this permission exists.
            if (globalPermissionManager.hasPermission(SYSTEM_ADMIN, getLoggedInUser())) {
                GlobalPermissionType sysAdminPermission = globalPermissionManager.getGlobalPermission(SYSTEM_ADMIN).get();
                globalPermTypes.put(SYSTEM_ADMIN.getKey(), getText(sysAdminPermission.getNameI18nKey()));
            }
            permissionMapHelper(ADMINISTER, globalPermTypes);
            permissionMapHelper(USE, globalPermTypes);
            permissionMapHelper(USER_PICKER, globalPermTypes);
            permissionMapHelper(CREATE_SHARED_OBJECTS, globalPermTypes);
            permissionMapHelper(MANAGE_GROUP_FILTER_SUBSCRIPTIONS, globalPermTypes);
            permissionMapHelper(BULK_CHANGE, globalPermTypes);

            // The above list is ordered by order of insertion. We want the plugin global permissions to be inserted after.
            // Only add things that aren't already in the map
            globalPermissionManager.getAllGlobalPermissions().stream()
                    .filter(gpT -> !globalPermTypes.containsKey(gpT.getKey()) && !SYSTEM_ADMIN.equals(gpT.getGlobalPermissionKey()))
                    .forEach(gpT -> {
                        globalPermTypes.put(gpT.getKey(), getText(gpT.getNameI18nKey()));
                    });

            globalPermTypes.remove(USE.getKey());
        }

        return globalPermTypes;
    }

    public Map<String, String> getManagablePermissions() {
        return Maps.filterEntries(getGlobalPermTypes(), entry -> {
            return isManagedByJira(entry.getKey());
        });
    }

    private void permissionMapHelper(GlobalPermissionKey permissionKey, Map<String, String> map) {
        Option<GlobalPermissionType> permissionOpt = globalPermissionManager.getGlobalPermission(permissionKey);
        if (permissionOpt.isDefined()) {
            GlobalPermissionType permission = permissionOpt.get();
            map.put(permission.getKey(), getText(permission.getNameI18nKey()));
        }
    }

    public String getDescription(String permType) {
        return globalPermissionManager.getGlobalPermission(permType).fold(
                returnUnknownString,
                globalPermissionType -> getText(globalPermissionType.getDescriptionI18nKey())
        );
    }

    public boolean isManagedByJira(String permKey) {
        return globalPermissionManager.isPermissionManagedByJira(GlobalPermissionKey.of(permKey));
    }

    public String getExternalPermissionManagementUrl() {
        return externalLinkUtil.getProperty(EXTERNAL_LINK_GLOBAL_PERMISSIONS_MANAGEMENT);
    }

    public boolean hasExceededUserLimit() {
        return applicationRoleManager.isAnyRoleLimitExceeded();
    }

    private Collection<Group> getAdministrativeGroups() {
        final Collection<Group> groups = new ArrayList<Group>(globalPermissionManager.getGroupsWithPermission(ADMINISTER));
        groups.addAll(globalPermissionManager.getGroupsWithPermission(SYSTEM_ADMIN));
        return Collections.unmodifiableCollection(groups);
    }

    private final Supplier<String> returnUnknownString = () -> getText("common.words.unknown");
}
