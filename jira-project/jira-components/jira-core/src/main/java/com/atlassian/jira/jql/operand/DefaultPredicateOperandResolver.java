package com.atlassian.jira.jql.operand;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.query.operand.Operand;

import java.util.List;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Default implementation of the {@link PredicateOperandResolver}
 *
 * @since v4.3
 */
public class DefaultPredicateOperandResolver implements PredicateOperandResolver {
    private final PredicateOperandHandlerRegistry predicateOperandHandlerRegistry;

    public DefaultPredicateOperandResolver(PredicateOperandHandlerRegistry handlerRegistry) {
        this.predicateOperandHandlerRegistry = handlerRegistry;
    }

    public List<QueryLiteral> getValues(ApplicationUser searcher, String field, final Operand operand) {
        notNull("operand", operand);
        return predicateOperandHandlerRegistry.getHandler(searcher, field, operand).getValues();
    }

    @Override
    public boolean isEmptyOperand(ApplicationUser searcher, String field, Operand operand) {
        return predicateOperandHandlerRegistry.getHandler(searcher, field, operand).isEmpty();
    }

    @Override
    public boolean isFunctionOperand(ApplicationUser searcher, String field, Operand operand) {
        return predicateOperandHandlerRegistry.getHandler(searcher, field, operand).isFunction();
    }

    @Override
    public boolean isListOperand(ApplicationUser searcher, String field, Operand operand) {
        return predicateOperandHandlerRegistry.getHandler(searcher, field, operand).isList();
    }
}
