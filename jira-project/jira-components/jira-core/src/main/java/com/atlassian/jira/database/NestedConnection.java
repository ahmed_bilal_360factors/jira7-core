package com.atlassian.jira.database;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * A wrapper around a connection that exists in a broader transaction.
 * <p>
 * We want to disallow any transaction control like commit(), rollback() etc as well as close()
 *
 * @since v7.0
 */
class NestedConnection extends AbstractDelegatingConnection {
    NestedConnection(final Connection existingConnection) {
        super(existingConnection);
    }

    @Override
    public void setAutoCommit(final boolean autoCommit) throws SQLException {
        if (autoCommit) {
            throw new UnsupportedOperationException("This connection has a managed transaction");
        }
    }

    @Override
    public void commit() throws SQLException {
        throw new UnsupportedOperationException("This connection has a managed transaction");
    }

    @Override
    public void rollback() throws SQLException {
        throw new UnsupportedOperationException("This connection has a managed transaction");
    }

    @Override
    public void close() throws SQLException {
        throw new UnsupportedOperationException("You cannot close this connection - it will be returned to the DB pool automatically.");
    }
}
