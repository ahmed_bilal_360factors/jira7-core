package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.upgrade.LegacyImmediateUpgradeTask;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.ofbiz.core.entity.model.ModelEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Swaps Cloners links to be in correct direction
 *
 * @since v6.1
 */
public class UpgradeTask_Build6152 extends LegacyImmediateUpgradeTask {
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask_Build6152.class);

    private final EntityEngine entityEngine;

    public UpgradeTask_Build6152(EntityEngine entityEngine) {
        super();
        this.entityEngine = entityEngine;
    }

    @Override
    public int getBuildNumber() {
        return 6152;
    }

    @Override
    public String getShortDescription() {
        return "Swaps Cloners links to be in correct direction";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception {
        Select.from("IssueLinkType")
                .whereEqual("linkname", "Cloners")
                .runWith(entityEngine)
                .visitWith(this::swapLinkDirectionsIfLegacy);
    }

    private void swapLinkDirectionsIfLegacy(final GenericValue clonersLinkType) {
        try (Connection connection = getDatabaseConnection()) {
            swapLinkDirectionsIfLegacy(connection, clonersLinkType.getLong("id"));
        } catch (GenericEntityException | SQLException e) {
            throw new DataAccessException(e);
        }
    }

    @edu.umd.cs.findbugs.annotations.SuppressWarnings(value = {"SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING", "SQL_NONCONSTANT_STRING_PASSED_TO_EXECUTE"}, justification = "Non-constant but safe.")
    private void swapLinkDirectionsIfLegacy(final Connection connection, final Long linkType)
            throws GenericEntityException, SQLException {
        final ModelEntity issueTable = getOfBizDelegator().getModelReader().getModelEntity("IssueLink");
        final String idColumn = issueTable.getField("id").getColName();
        final String linkTypeColumn = issueTable.getField("linktype").getColName();
        final String sourceColumn = issueTable.getField("source").getColName();
        final String destinationColumn = issueTable.getField("destination").getColName();

        try (Statement statement = connection.createStatement()) {
            final String update;
            if (isMYSQL()) {
                update = "UPDATE " + convertToSchemaTableName("issuelink") + " links1, "
                        + convertToSchemaTableName("issuelink") + " links2 "
                        + " SET links1." + sourceColumn + " = links1." + destinationColumn + ", links1." + destinationColumn + " = links2." + sourceColumn
                        + " WHERE links1." + idColumn + " = links2." + idColumn
                        + " AND links1." + linkTypeColumn + " = " + linkType
                        + " AND links1." + sourceColumn + " < links1." + destinationColumn
                        + " AND links1." + sourceColumn + " IS NOT NULL "
                        + " AND links1." + destinationColumn + " IS NOT NULL ";
            } else {
                update = "UPDATE " + convertToSchemaTableName("issuelink")
                        + " SET " + sourceColumn + " = " + destinationColumn + ", " + destinationColumn + " = " + sourceColumn
                        + " WHERE " + linkTypeColumn + " = " + linkType
                        + " AND " + sourceColumn + " < " + destinationColumn
                        + " AND " + sourceColumn + " IS NOT NULL "
                        + " AND " + destinationColumn + " IS NOT NULL ";
            }
            final int updated = statement.executeUpdate(update);
            log.info("Swapped {} link(s).", updated);
        }
    }

    @Nullable
    @Override
    public Integer dependsUpon() {
        return 6151;
    }
}
