package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

import static com.atlassian.jira.application.ApplicationKeys.CORE;
import static com.atlassian.jira.application.ApplicationKeys.SOFTWARE;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * <p>Moves users who have access to JIRA 6.x over to either JIRA Software or JIRA Core.
 * <p>
 * NOTE: The {@link MigrationTask#migrate(MigrationState, boolean)} of this task does not actually mutate the
 * database, it makes all its changes exclusively on the passed {@link MigrationState}. Something outside of this class
 * will persist the state once it is validated correct.
 * <p>
 * Pre-renaissance (before the introduction of JIRA Applications) JIRA users were able to access JIRA by being in a
 * group that has been assigned to the Global Permission "USE" (JIRA Users). JIRA renaissance introduces the concept of
 * multiple "Applications" that runs on the JIRA Platform. A JIRA Application is basically one or more plugins that are
 * branded as separate products provided by Atlassian. These "Applications" have their own distributions that's
 * downloadable from WAC, they also have their own licenses. In order for a JIRA user to make use of a JIRA Application
 * they need be be in a group that has been associated with an {@code com.atlassian.jira.application.ApplicationRole}
 * licenseable entity. This indicates that the group has been assigned to the application that has a backing license.
 * <p>
 * This migration is responsible for moving the groups configured against the Global Permission "USE" to the JIRA
 * Software AND Core Applications.
 * <p>
 * All JIRA Applications includes the JIRA Core Application (the distribution is bundled with JIRA Core).
 * Pre-renaissance licenses, typically licenses that were issued before JIRA renaissance, are now interpreted as JIRA
 * Software application licenses. All JIRA Application licenses includes access to JIRA Core features.
 * <p>
 * Migration rules: This migration is based on the Pre-renaissance view of JIRA and would not try to guess things about
 * future products. What is this view? Pre-renaissance JIRA instances are JIRA Software Application instances. JIRA
 * Software includes JIRA Core and users won't get double counted when they are in JIRA Core and another JIRA
 * Application. They would only count towards the other JIRA Application's user count.
 * <p>
 * All the groups associated with the Global Permission "USE" would be associated with: JIRA Software AND
 * JIRA Core, even though they are not currently licensed.  Future products (Other JIRA Applications) would be
 * responsible for performing there own migrations.
 * <p>
 * The first JIRA renaissance release would be v7.0.
 *
 * @see com.atlassian.jira.application.ApplicationRole
 */
final class Move6xUsePermissionOverToCoreAndSoftwareApplications extends MigrationTask {
    private static final Set<ApplicationKey> APPLICATIONS_TO_ASSIGN = ImmutableSet.of(CORE, SOFTWARE);

    private final UseBasedMigration useBasedMigration;

    Move6xUsePermissionOverToCoreAndSoftwareApplications(final UseBasedMigration useBasedMigration) {
        this.useBasedMigration = notNull("useBasedMigration", useBasedMigration);
    }

    @Override
    MigrationState migrate(MigrationState state, final boolean licenseSuppliedByUser) {
        return useBasedMigration.addUsePermissionToRoles(state, APPLICATIONS_TO_ASSIGN);
    }
}
