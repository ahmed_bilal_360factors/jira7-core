package com.atlassian.jira.transaction;

/**
 * Factory for JIRA setup container phase. Creates instances of {@link BootstrapRunnablesQueue}
 */
public class BootstrapRequestLocalTransactionRunnableQueryFactory implements RequestLocalTransactionRunnableQueueFactory {

    private final BootstrapRunnablesQueue queue = new BootstrapRunnablesQueue();

    @Override
    public RunnablesQueue getRunnablesQueue() {
        return queue;
    }
}
