package com.atlassian.jira.scheduler.cron;

import com.atlassian.fugue.Option;
import com.atlassian.jira.service.util.CronValidationErrorMappingUtil;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.cron.CronSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Date;
import java.util.TimeZone;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.jira.service.util.CronValidationErrorMappingUtil.internalError;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * This is similar to the {@link com.atlassian.scheduler.cron.CronExpressionValidator}, except that
 * the error messages go through translation.
 *
 * @since v6.4 (moved packages in v7.0)
 */
public class CronValidator {
    private static final Logger LOG = LoggerFactory.getLogger(CronValidator.class);

    private final I18nHelper i18nHelper;
    private final SchedulerService schedulerService;

    public CronValidator(final I18nHelper i18nHelper, final SchedulerService schedulerService) {
        this.i18nHelper = notNull("i18nHelper", i18nHelper);
        this.schedulerService = notNull("schedulerService", schedulerService);
    }

    /**
     * Validates a cron expression using JIRA's default time zone
     *
     * @param expr the Cron Expression to validate and turn into trigger
     * @return Option containing error description if error occurred or none if cron expression is valid.
     */
    public Option<String> validateCron(String expr) {
        return validateCron(expr, null);
    }

    /**
     * Validates a cron expression using the specified time zone.
     *
     * @param expr     the Cron Expression to validate and turn into trigger
     * @param timeZone the time zone to use when checking that the cron expression will result in a job that runs;
     *                 may be {@code null} to indicate that JIRA's default time zone should be used
     * @return Option containing error description if error occurred or none if cron expression is valid.
     */
    public Option<String> validateCron(String expr, @Nullable TimeZone timeZone) {
        try {
            return validateWillRun(expr, timeZone);
        } catch (CronSyntaxException cse) {
            LOG.debug("Error parsing cron expression '{}'", expr, cse);
            return some(CronValidationErrorMappingUtil.mapError(cse, i18nHelper));
        } catch (Exception e) {
            LOG.debug("Unexpected error parsing cron expression '{}'", expr, e);
            return some(internalError(e.toString(), i18nHelper));
        }
    }

    private Option<String> validateWillRun(final String expr, final TimeZone timeZone) throws SchedulerServiceException {
        if (expr == null) {
            LOG.debug("Asked to validate a null cron expression");
            return some(internalError("null", i18nHelper));
        }

        final Date date = schedulerService.calculateNextRunTime(Schedule.forCronExpression(expr, timeZone));
        if (date == null) {
            return some(i18nHelper.getText("cron.expression.invalid.will.never.run", expr));
        }

        return none();
    }
}
