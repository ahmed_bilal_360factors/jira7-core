package com.atlassian.jira.web.action.admin.importer.project;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.web.action.JiraWebActionSupportEvent;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import java.util.Collections;
import java.util.Map;

/**
 * Helper service for publishing project import web analytics events.
 */
public class ProjectImportWebAnalyticsService {

    private final EventPublisher eventPublisher;

    public ProjectImportWebAnalyticsService(final EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    public void publishAnalyticsEvent(JiraWebActionSupport jiraWebActionSupport) {
        publishAnalyticsEvent(jiraWebActionSupport, Collections.emptyMap());
    }

    public void publishAnalyticsEvent(JiraWebActionSupport jiraWebActionSupport, Map<String, Object> attributes) {
        JiraWebActionSupportEvent event = new ProjectImportWebAnalyticsEvent(jiraWebActionSupport, attributes);
        if (!event.isEmpty()) {
            eventPublisher.publish(event);
        }
    }

}
