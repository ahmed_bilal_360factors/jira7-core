package com.atlassian.jira.database;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.ofbiz.DefaultOfBizConnectionFactory;
import com.atlassian.jira.ofbiz.OfBizConnectionFactory;
import com.querydsl.sql.SQLTemplates;
import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.DelegatorInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.sql.SQLException;

public class DbConnectionManagerImpl implements DbConnectionManager {
    private static final Logger log = LoggerFactory.getLogger(DbConnectionManagerImpl.class);

    private final OfBizConnectionFactory ofBizConnectionFactory;
    private final SQLTemplates dialect;
    private final DelegatorInterface delegatorInterface;

    public DbConnectionManagerImpl(DatabaseConfigurationManager databaseConfigurationManager, final DelegatorInterface delegatorInterface) {
        this.delegatorInterface = delegatorInterface;
        this.ofBizConnectionFactory = DefaultOfBizConnectionFactory.getInstance();

        final DatabaseConfig databaseConfig = databaseConfigurationManager.getDatabaseConfiguration();
        // Find the Querydsl template for the configured database vendor
        final SQLTemplates.Builder dialectBuilder = DefaultQueryDslAccessor.findDialectBuilder(databaseConfig);

        if (!StringUtils.isEmpty(databaseConfig.getSchemaName())) {
            // tell Querydsl to include the schema name in SQL it generates
            dialectBuilder.printSchema();
        }
        this.dialect = dialectBuilder.build();
    }

    @Override
    public <T> T executeQuery(@Nonnull final QueryCallback<T> callback) {
        final Connection con = borrowConnection();
        try {
            final DbConnection dbConn = new DbConnectionImpl(con, dialect, delegatorInterface);
            return callback.runQuery(dbConn);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                log.error("Unable to close database connection.", ex);
            }
        }
    }

    private Connection borrowConnection() {
        try {
            return ofBizConnectionFactory.getConnection();
        } catch (SQLException ex) {
            throw new DataAccessException(ex);
        }
    }

    @Override
    @Nonnull
    public SQLTemplates getDialect() {
        return dialect;
    }

    @Override
    public void execute(@Nonnull final SqlCallback callback) {
        final Connection con = borrowConnection();
        try {
            callback.run(new DbConnectionImpl(con, dialect, delegatorInterface));
        } catch (RuntimeException ex) {
            try {
                if (!con.getAutoCommit()) {
                    con.rollback();
                }
            } catch (SQLException sqlEx) {
                log.error("Unable to rollback SQL connection.", sqlEx);
            }
            // Rethrow the RuntimeException
            throw ex;
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                // We don't want to hide any exceptions that may have already been raised during the actual work.
                log.error("Unable to close SQL connection.", ex);
            }
        }
    }
}
