package com.atlassian.jira.bc.project;

import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.fields.layout.field.FieldConfigurationScheme;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenScheme;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeManager;
import com.atlassian.jira.scheme.SchemeManagerFactory;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Optional;

import static com.atlassian.jira.issue.IssueFieldConstants.ISSUE_TYPE;
import static java.util.Optional.ofNullable;

/**
 * Provides functionality to help with associating a project with schemes.
 *
 * @since v7.0
 */
public class ProjectSchemeAssociationManager {
    private final SchemeManagerFactory schemeManagerFactory;
    private final IssueTypeSchemeManager issueTypeSchemeManager;
    private final IssueTypeScreenSchemeManager issueTypeScreenSchemeManager;
    private final FieldConfigSchemeManager fieldConfigSchemeManager;
    private final FieldManager fieldManager;
    private final ProjectManager projectManager;
    private final FieldLayoutManager fieldLayoutManager;

    public ProjectSchemeAssociationManager(
            SchemeManagerFactory schemeManagerFactory,
            IssueTypeSchemeManager issueTypeSchemeManager,
            IssueTypeScreenSchemeManager issueTypeScreenSchemeManager,
            FieldConfigSchemeManager fieldConfigSchemeManager,
            FieldManager fieldManager,
            ProjectManager projectManager,
            FieldLayoutManager fieldLayoutManager) {
        this.schemeManagerFactory = schemeManagerFactory;
        this.issueTypeSchemeManager = issueTypeSchemeManager;
        this.issueTypeScreenSchemeManager = issueTypeScreenSchemeManager;
        this.fieldConfigSchemeManager = fieldConfigSchemeManager;
        this.fieldManager = fieldManager;
        this.projectManager = projectManager;
        this.fieldLayoutManager = fieldLayoutManager;
    }

    void associateDefaultSchemesWithProject(Project newProject) {
        for (SchemeManager schemeManager : schemeManagerFactory.getAllSchemeManagers()) {
            schemeManager.addDefaultSchemeToProject(newProject);
        }
        issueTypeScreenSchemeManager.associateWithDefaultScheme(newProject);
    }

    void associateSchemesOfExistingProjectWithNewProject(Project newProject, Project existingProject) {
        for (SchemeManager schemeManager : schemeManagerFactory.getAllSchemeManagers()) {
            Optional<Scheme> projectScheme = ofNullable(schemeManager.getSchemeFor(existingProject));
            projectScheme.ifPresent(scheme -> schemeManager.addSchemeToProject(newProject, scheme));
        }
        associateIssueTypeScheme(newProject, existingProject);

        Optional<IssueTypeScreenScheme> optionalIssueTypeScreenScheme = ofNullable(issueTypeScreenSchemeManager.getIssueTypeScreenScheme(existingProject));
        optionalIssueTypeScreenScheme.ifPresent(scheme -> issueTypeScreenSchemeManager.addSchemeAssociation(newProject, scheme));

        Optional<FieldConfigurationScheme> optionalFieldConfigScheme = ofNullable(fieldLayoutManager.getFieldConfigurationScheme(existingProject));
        optionalFieldConfigScheme.ifPresent(scheme -> fieldLayoutManager.addSchemeAssociation(newProject, scheme.getId()));
    }

    private void associateIssueTypeScheme(final Project newProject, final Project existingProject) {
        final Optional<FieldConfigScheme> fieldConfigScheme = ofNullable(issueTypeSchemeManager.getConfigScheme(existingProject));
        fieldConfigScheme
                .filter(scheme -> !issueTypeSchemeManager.isDefaultIssueTypeScheme(scheme))
                .ifPresent(scheme -> {
                    final List<Long> projectIds = Lists.newArrayList(scheme.getAssociatedProjectIds());
                    projectIds.add(newProject.getId());

                    final List<JiraContextNode> contexts = CustomFieldUtils.buildJiraIssueContexts(false, projectIds.stream().toArray(Long[]::new), projectManager);
                    fieldConfigSchemeManager.updateFieldConfigScheme(scheme, contexts, fieldManager.getConfigurableField(ISSUE_TYPE));

                    fieldManager.refresh();
                });
    }
}
