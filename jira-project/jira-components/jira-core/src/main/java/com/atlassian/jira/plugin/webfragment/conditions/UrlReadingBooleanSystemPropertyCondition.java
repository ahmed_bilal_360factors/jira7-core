package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.webresource.condition.SimpleUrlReadingCondition;

import java.util.Map;

/**
 * An url reading condition which is true iff the configured system property is set and has case insensitive value "true".
 *
 * @since v6.3
 */
public class UrlReadingBooleanSystemPropertyCondition extends SimpleUrlReadingCondition {
    private volatile String property;

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {
        property = params.get("property");
    }

    @Override
    protected String queryKey() {
        return property;
    }

    @Override
    protected boolean isConditionTrue() {
        return JiraSystemProperties.getInstance().getBoolean(property);
    }
}
