package com.atlassian.jira.startup;

import com.atlassian.jira.bc.ServiceResult;

import java.util.concurrent.TimeUnit;

/**
 * Delays tasks during an InstantUp scenario.
 * @since v7.1
 */
public interface InstantUpgradeManager {

    /**
     * State of the instant upgrade manager.
     */
    public enum State {
        DISABLED,               // instant upgrade is not enabled
        ERROR,                  // error during startup
        EARLY_STARTUP,          // system running early startup
        WAITING_FOR_ACTIVATION, // waiting for activation to go into late startup
        LATE_STARTUP,           // system running late startup
        FULLY_STARTED           // All startup phases have run
    }

    /** The boolean system property indicating that an instant upgrade is in progress */
    public static final String INSTANT_UPGRADE = "instant.upgrade";

    /**
     * Run this function immediately during a normal startup. If this is an instant upgrade scenario, delay the function
     * until triggered.
     * @param runnable the function to run
     * @param description description of the function
     */
    public void doNowOrWhenInstanceBecomesActive(Runnable runnable, String description);

    /**
     * @return the state of the instant upgrade manager.
     */
    public State getState();

    /**
     * Called when the new instance is activated in an InstantUp scenario
     * @returns service result
     **/
    public ServiceResult activateInstance();

    /**
     * Waits until the instance is fully started.
     * @param timeout the maximum time to wait
     * @param unit the time unit of the {@code timeout} argument
     * @return {@code true} if the count reached zero and {@code false}
     *         if the waiting time elapsed before the count reached zero
     * @throws InterruptedException if the current thread is interrupted
     *         while waiting*
     */
    public boolean waitTillFullyStarted(long timeout, TimeUnit unit) throws InterruptedException;
}
