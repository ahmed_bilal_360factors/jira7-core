package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.atlassian.jira.CachingComponent;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * @since v7.0
 */
public final class ApplicationRolesDaoImpl extends ApplicationRolesDao {
    private static final Logger LOG = LoggerFactory.getLogger(ApplicationRolesDaoImpl.class);

    private static final String APPLICATION_ROLE_ENTITY = "LicenseRoleGroup";

    private static class Columns {
        private static final String NAME = "licenseRoleName";
        private static final String GROUP_ID = "groupId";
        private static final String DEFAULT = "primaryGroup";
    }

    private final OfBizDelegator db;
    private final CachingComponent roleCache;

    public ApplicationRolesDaoImpl(final OfBizDelegator db, ApplicationRoleManager applicationRoleManager) {
        notNull("applicationRoleManager", applicationRoleManager);
        this.db = notNull("db", db);

        if (applicationRoleManager instanceof CachingComponent) {
            roleCache = (CachingComponent) applicationRoleManager;
        } else {
            throw new IllegalArgumentException("'applicationRoleManager' is not a cache.");
        }
    }

    @Override
    ApplicationRoles get() {
        final List<GenericValue> group = db.findAll(APPLICATION_ROLE_ENTITY);
        final Map<ApplicationKey, ApplicationRole> roles = Maps.newHashMap();

        for (GenericValue genericValue : group) {
            final String keyString = genericValue.getString(Columns.NAME);
            try {
                final ApplicationKey key = ApplicationKey.valueOf(keyString);
                final String groupName = genericValue.getString(Columns.GROUP_ID);
                if (groupName != null) {
                    ApplicationRole newRole = roles.computeIfAbsent(key, ApplicationRole::forKey);
                    if (Boolean.TRUE.equals(genericValue.getBoolean(Columns.DEFAULT))) {
                        newRole = newRole.addGroupAsDefault(new ImmutableGroup(groupName));
                    } else {
                        newRole = newRole.addGroup(new ImmutableGroup(groupName));
                    }
                    roles.put(key, newRole);
                }
            } catch (IllegalArgumentException ignored) {
                //ignored: Happens if an invalid appKey is in the database. We don't care if this happens because
                //we are only interested in valid keys.

                LOG.debug("Invalid key '{}' in the database.", keyString);
            }
        }
        return new ApplicationRoles(roles.values());
    }

    @Override
    void put(final ApplicationRoles applicationRoles) {
        try {
            final Set<ApplicationKey> wantedRoles = applicationRoles.asMap().keySet();
            if (!wantedRoles.containsAll(getCurrentApplicationKeys())) {
                throw new MigrationFailedException("Trying to remove application roles.");
            }

            applicationRoles.asMap().values().forEach(this::put);
        } finally {
            roleCache.clearCache();
        }
    }

    private Set<ApplicationKey> getCurrentApplicationKeys() {
        return db.findAll(APPLICATION_ROLE_ENTITY)
                .stream()
                .map(gv -> gv.getString(Columns.NAME))
                .filter(ApplicationKey::isValid)
                .map(ApplicationKey::valueOf)
                .collect(Collectors.toSet());
    }

    private void put(ApplicationRole role) {
        final Set<Group> currentGroups = Sets.newHashSet();
        for (GenericValue row : db.findByAnd(APPLICATION_ROLE_ENTITY, ImmutableMap.of(Columns.NAME, role.key().value()))) {
            String groupId = row.getString(Columns.GROUP_ID);
            if (groupId == null) {
                db.removeValue(row);
            } else {
                Group group = new ImmutableGroup(groupId);

                boolean hasLowerCaseIdentifier = IdentifierUtils.toLowerCase(groupId).equals(groupId);
                //If group does not have correct lower case identifier it would be deleted and optionally added later
                if (hasLowerCaseIdentifier && role.groups().contains(group)) {
                    currentGroups.add(group);
                    //This can be null which indicates "false" too. We just make sure the "false" is explicitly stored.
                    final Boolean currentDefault = row.getBoolean(Columns.DEFAULT);
                    final boolean wantedDefault = role.defaultGroups().contains(group);
                    if (currentDefault == null || !currentDefault.equals(wantedDefault)) {
                        row.set(Columns.DEFAULT, wantedDefault);
                        db.store(row);
                    }
                } else {
                    db.removeValue(row);
                }
            }
        }

        for (final Group newGroup : Sets.difference(role.groups(), currentGroups)) {
            db.createValue(APPLICATION_ROLE_ENTITY, FieldMap.build(
                    Columns.NAME, role.key().value(),
                    Columns.GROUP_ID, IdentifierUtils.toLowerCase(newGroup.getName()),
                    Columns.DEFAULT, role.defaultGroups().contains(newGroup)));
        }
    }
}
