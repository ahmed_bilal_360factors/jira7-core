package com.atlassian.jira.web.action.setup;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.landingpage.LandingPageRedirectManager;

import java.util.Optional;

public class SetupCompleteRedirectHelper {
    private static final String REDIRECT_DEFAULT_URL = "/secure/Dashboard.jspa";


    public String getRedirectUrl(final ApplicationUser loggedInUser) {
        // attempt to use the OnboardingService; we have to use Component Accessor, because instant path
        // initializes this component in bootstrap, but uses it after setup completes.
        Optional<String> redirectUrl = ComponentAccessor.getComponentOfType(LandingPageRedirectManager.class)
                .redirectUrl(loggedInUser);

        return redirectUrl.orElse(REDIRECT_DEFAULT_URL);
    }
}
