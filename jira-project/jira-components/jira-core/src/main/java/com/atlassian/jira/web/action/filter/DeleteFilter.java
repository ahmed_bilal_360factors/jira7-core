package com.atlassian.jira.web.action.filter;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.filter.FilterDeletionWarningViewProvider;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.search.managers.IssueSearcherManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;

/**
 * Action class for managing filters.  Also used by delete filter.
 */
public class DeleteFilter extends AbstractFilterAction {
    private final SearchRequestService searchRequestService;
    private FilterDeletionWarningViewProvider filterDeletionWarningViewProvider;

    public DeleteFilter(final IssueSearcherManager issueSearcherManager,
                        final SearchRequestService searchRequestService,
                        final SearchService searchService,
                        final FilterDeletionWarningViewProvider filterDeletionWarningViewProvider) {
        super(issueSearcherManager, searchService);
        this.searchRequestService = searchRequestService;
        this.filterDeletionWarningViewProvider = filterDeletionWarningViewProvider;
    }

    public boolean canDelete() {
        return !hasAnyErrors();
    }

    @Override
    public String doDefault() throws Exception {
        final JiraServiceContext ctx = getJiraServiceContext();

        if (getFilterId() != null) {
            searchRequestService.validateForDelete(ctx, getFilterId());

            if (hasAnyErrors()) {
                return ERROR;
            }
        } else {
            addErrorMessage(getText("admin.errors.filters.cannot.delete.filter"));
            return ERROR;
        }

        return INPUT;
    }


    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        final JiraServiceContext ctx = getJiraServiceContext();

        if (getFilterId() != null) {
            searchRequestService.validateForDelete(ctx, getFilterId());

            if (hasAnyErrors()) {
                return ERROR;
            }
            searchRequestService.deleteFilter(ctx, getFilterId());
            if (hasAnyErrors()) {
                return ERROR;
            }
        } else {
            addErrorMessage(getText("admin.errors.filters.cannot.delete.filter"));
            return ERROR;
        }

        setSearchRequest(null);
        return returnComplete(getReturnUrl());
    }
    
    public String getWarningHtml() {
        return filterDeletionWarningViewProvider.getWarningHtml(getFilter());
    }
}
