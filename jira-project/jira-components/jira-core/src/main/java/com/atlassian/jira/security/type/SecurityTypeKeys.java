package com.atlassian.jira.security.type;

import com.atlassian.jira.bc.projectroles.ProjectRoleService;

/**
 * Helper enum to be able to get the keys for different security types as there implementation is not consistent
 * @since 7.1
 */
public enum SecurityTypeKeys {

    PROJECT_ROLE(ProjectRoleService.PROJECTROLE_PERMISSION_TYPE),
    APPLICATION_ROLE(ApplicationRoleSecurityType.ID),
    GROUP(GroupDropdown.DESC),
    USER( SingleUser.DESC),
    USER_CF(UserCF.TYPE),
    GROUP_CF(GroupCF.TYPE);

    private final String key;

    SecurityTypeKeys(final String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
