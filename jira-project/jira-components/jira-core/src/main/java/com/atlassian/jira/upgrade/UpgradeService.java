package com.atlassian.jira.upgrade;

/**
 * Service for managing Upgrades.
 */
public interface UpgradeService {

    /**
     * Run the upgrades pending upgrades.
     * <p>
     *     This is not linked to provisioning or import but when we just want to run the upgrades,
     *     e.g. via the REST endpoint. A re-index will be run after finishing the upgrades.
     * </p>
     *
     * @return the result of running the upgrades.
     */
    UpgradeResult runUpgrades();

    /**
     * Checks if any upgrades are running.
     *
     * @return true if and only if upgrade job is running, false otherwise.
     */
    boolean areUpgradesRunning();
}
