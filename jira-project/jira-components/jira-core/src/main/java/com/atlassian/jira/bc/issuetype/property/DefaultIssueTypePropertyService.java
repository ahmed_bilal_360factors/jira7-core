package com.atlassian.jira.bc.issuetype.property;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.entity.property.BaseEntityPropertyService;
import com.atlassian.jira.entity.property.JsonEntityPropertyManager;
import com.atlassian.jira.issue.issuetype.IssueTypeWithID;
import com.atlassian.jira.util.I18nHelper;

/**
 * @since 7.0
 */
public class DefaultIssueTypePropertyService extends BaseEntityPropertyService<IssueTypeWithID> implements IssueTypePropertyService {
    public DefaultIssueTypePropertyService(final JsonEntityPropertyManager jsonEntityPropertyManager, final I18nHelper i18n, final EventPublisher eventPublisher, final IssueTypePropertyHelper entityPropertyHelper) {
        super(jsonEntityPropertyManager, i18n, eventPublisher, entityPropertyHelper);
    }
}
