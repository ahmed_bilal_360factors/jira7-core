package com.atlassian.jira.cache.slomo;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheException;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettings;
import com.atlassian.cache.CachedReference;
import com.atlassian.cache.ManagedCache;
import com.atlassian.cache.Supplier;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;

/**
 * A rather obnoxious implementation of {@code CacheManager} that makes every remote cache slower.
 * <p>
 * This is intended for simulating what happens when caches live externally to the JVM and is
 * only intended for development and testing purposes.  As tempting as it might be, you must
 * resist the urge to enable this on a production system.  Your end users would be somewhat
 * less than thrilled with that experience.
 * </p>
 *
 * @since v7.1.0
 */
@ParametersAreNonnullByDefault
public class SloMoCacheManager implements CacheManager {
    private final CacheManager delegate;
    private final int delay;

    @Override
    @Deprecated
    @Nonnull
    public Collection<Cache<?, ?>> getCaches() {
        return delegate.getCaches();
    }

    @Override
    @Nonnull
    public Collection<ManagedCache> getManagedCaches() {
        return delegate.getManagedCaches();
    }

    @Override
    public void flushCaches() {
        delegate.flushCaches();
    }

    @Override
    @Nullable
    public ManagedCache getManagedCache(final String name) {
        return delegate.getManagedCache(name);
    }

    @Override
    public void shutdown() {
        delegate.shutdown();
    }

    @Override
    @Nonnull
    public <V> CachedReference<V> getCachedReference(final String name, final Supplier<V> supplier) {
        return wrap(delegate.getCachedReference(name, supplier));
    }

    @Override
    @Nonnull
    public <V> CachedReference<V> getCachedReference(final String name, final Supplier<V> supplier,
                                                     final CacheSettings cacheSettings) {
        final CachedReference<V> ref = delegate.getCachedReference(name, supplier, cacheSettings);
        return isLocal(cacheSettings) ? ref : wrap(ref);
    }

    @Override
    @Nonnull
    public <V> CachedReference<V> getCachedReference(final Class<?> owningClass, final String name,
                                                     final Supplier<V> supplier) {
        return wrap(delegate.getCachedReference(owningClass, name, supplier));
    }

    @Override
    @Nonnull
    public <V> CachedReference<V> getCachedReference(final Class<?> owningClass, final String name,
                                                     final Supplier<V> supplier, final CacheSettings cacheSettings) {
        final CachedReference<V> ref = delegate.getCachedReference(owningClass, name, supplier, cacheSettings);
        return isLocal(cacheSettings) ? ref : wrap(ref);
    }

    @Override
    @Nonnull
    public <K, V> Cache<K, V> getCache(final String name) {
        return wrap(delegate.getCache(name));
    }

    @Override
    @Nonnull
    public <K, V> Cache<K, V> getCache(final Class<?> owningClass, final String name) {
        return wrap(delegate.getCache(owningClass, name));
    }

    @Override
    @Nonnull
    public <K, V> Cache<K, V> getCache(final String name, @Nullable final CacheLoader<K, V> cacheLoader) {
        return wrap(delegate.getCache(name, cacheLoader));
    }

    @Override
    @Nonnull
    public <K, V> Cache<K, V> getCache(final String name, @Nullable final CacheLoader<K, V> cacheLoader,
                                       final CacheSettings cacheSettings) {
        final Cache<K, V> cache = delegate.getCache(name, cacheLoader, cacheSettings);
        return isLocal(cacheSettings) ? cache : wrap(cache);
    }

    @Override
    @Deprecated
    @Nonnull
    public <K, V> Cache<K, V> getCache(final String name, final Class<K> keyClass, final Class<V> valueClass) {
        return wrap(delegate.getCache(name, keyClass, valueClass));
    }

    public SloMoCacheManager(final CacheManager delegate, final int delay) {
        this.delegate = delegate;
        this.delay = delay;
    }

    private <T> CachedReference<T> wrap(CachedReference<T> delegate) {
        return new SloMoCachedReference<>(this, delegate);
    }

    private <K, V> Cache<K, V> wrap(Cache<K, V> delegate) {
        return new SloMoCache<>(this, delegate);
    }

    private static boolean isLocal(CacheSettings settings) {
        return Boolean.TRUE.equals(settings.getLocal());
    }

    void sleep() {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException ie) {
            throw new CacheException("Interrupted inside cache delay", ie);
        }
    }
}
