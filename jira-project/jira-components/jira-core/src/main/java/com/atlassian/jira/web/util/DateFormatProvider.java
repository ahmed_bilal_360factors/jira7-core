package com.atlassian.jira.web.util;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.collect.ImmutableMap;

import java.io.IOException;
import java.io.Writer;
import java.text.DateFormatSymbols;
import java.util.Arrays;

/**
 * Provides localized version of names of months, days, eras and meridiem.
 */
public class DateFormatProvider implements WebResourceDataProvider {
    private final JiraAuthenticationContext authenticationContext;
    private final ApplicationProperties applicationProperties;

    public DateFormatProvider(final JiraAuthenticationContext authenticationContext, final ApplicationProperties applicationProperties) {
        this.authenticationContext = authenticationContext;
        this.applicationProperties = applicationProperties;
    }

    @Override
    public Jsonable get() {
        return new Jsonable() {
            @Override
            public void write(final Writer writer) throws IOException {
                try {
                    getAllFormatsAsJson().write(writer);
                } catch (JSONException e) {
                    throw new JsonMappingException(e);
                }
            }
        };
    }

    private JSONObject getAllFormatsAsJson() {
        final DateFormatSymbols dateFormatSymbols = DateFormatSymbols.getInstance(authenticationContext.getLocale());
        final ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();

        builder.put("dateFormats", getDateFormatsJson(dateFormatSymbols));
        builder.put("lookAndFeelFormats", getLookAndFeelFormatsJson());

        return new JSONObject(builder.build());
    }

    private JSONObject getLookAndFeelFormatsJson() {
        final ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();

        builder.put("relativize", applicationProperties.getDefaultBackedString(APKeys.JIRA_LF_DATE_RELATIVE));
        builder.put("time", applicationProperties.getDefaultBackedString(APKeys.JIRA_LF_DATE_TIME));
        builder.put("day", applicationProperties.getDefaultBackedString(APKeys.JIRA_LF_DATE_DAY));
        builder.put("dmy", applicationProperties.getDefaultBackedString(APKeys.JIRA_LF_DATE_DMY));
        builder.put("complete", applicationProperties.getDefaultBackedString(APKeys.JIRA_LF_DATE_COMPLETE));

        return new JSONObject(builder.build());
    }

    private JSONObject getDateFormatsJson(final DateFormatSymbols dateFormatSymbols) {
        final ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();

        builder.put("meridiem", Arrays.asList(dateFormatSymbols.getAmPmStrings()));
        builder.put("eras", Arrays.asList(dateFormatSymbols.getEras()));
        builder.put("months", Arrays.asList(dateFormatSymbols.getMonths()).subList(0, 12));
        builder.put("monthsShort", Arrays.asList(dateFormatSymbols.getShortMonths()).subList(0, 12));
        builder.put("weekdaysShort", Arrays.asList(dateFormatSymbols.getShortWeekdays()).subList(1, 8));
        builder.put("weekdays", Arrays.asList(dateFormatSymbols.getWeekdays()).subList(1, 8));

        return new JSONObject(builder.build());
    }
}
