package com.atlassian.jira.database;

import com.atlassian.core.ofbiz.util.CoreTransactionUtil;
import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.exception.DataAccessException;
import com.querydsl.sql.H2Templates;
import com.querydsl.sql.HSQLDBTemplates;
import com.querydsl.sql.MySQLTemplates;
import com.querydsl.sql.PostgreSQLTemplates;
import com.querydsl.sql.SQLServerTemplates;
import com.querydsl.sql.SQLTemplates;
import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.DelegatorInterface;
import org.ofbiz.core.entity.GenericTransactionException;

import javax.annotation.Nonnull;
import java.sql.Connection;

public class DefaultQueryDslAccessor implements QueryDslAccessor {
    private final DatabaseAccessor databaseAccessor;
    private final DelegatorInterface delegatorInterface;
    private final SQLTemplates dialect;

    public DefaultQueryDslAccessor(DatabaseAccessor databaseAccessor, DelegatorInterface delegatorInterface, DatabaseConfigurationManager databaseConfigurationManager) {
        this.databaseAccessor = databaseAccessor;
        this.delegatorInterface = delegatorInterface;

        final DatabaseConfig databaseConfig = databaseConfigurationManager.getDatabaseConfiguration();
        // Find the Querydsl template for the configured database vendor
        final SQLTemplates.Builder dialectBuilder = findDialectBuilder(databaseConfig);

        if (!StringUtils.isEmpty(databaseConfig.getSchemaName())) {
            // tell Querydsl to include the schema name in SQL it generates
            dialectBuilder.printSchema();
        }
        this.dialect = dialectBuilder.build();
    }

    static SQLTemplates.Builder findDialectBuilder(final DatabaseConfig databaseConfig) {
        if (databaseConfig.isMySql()) {
            return MySQLTemplates.builder();
        }
        if (databaseConfig.isPostgres()) {
            return PostgreSQLTemplates.builder();
        }
        if (databaseConfig.isOracle()) {
            return JiraOracleTemplates.builder();
        }
        if (databaseConfig.isSqlServer()) {
            return SQLServerTemplates.builder();
        }
        if (databaseConfig.isH2()) {
            return H2Templates.builder();
        }
        // should be removed in 8.0; only required during migration of 6.x HSQL installation to 7.x H2
        if (databaseConfig.isHSql()) {
            return HSQLDBTemplates.builder();
        }
        throw new IllegalStateException("Unrecognised database dialect '" + databaseConfig.getDatabaseType() + "'.");
    }

    @Override
    public <T> T executeQuery(@Nonnull final QueryCallback<T> callback) {
        return databaseAccessor.runInTransaction(con ->
                callback.runQuery(new DbConnectionImpl(con, dialect, delegatorInterface)));
    }

    @Override
    public void execute(@Nonnull final SqlCallback callback) {
        databaseAccessor.runInTransaction(con -> {
            callback.run(new DbConnectionImpl(con, dialect, delegatorInterface));
            return null;
        });
    }

    @Override
    public ConnectionProvider withNewConnection() {
        return new ConnectionProvider() {
            @Override
            public <T> T executeQuery(@Nonnull QueryCallback<T> callback) {
                return databaseAccessor.executeQuery(con ->
                        callback.runQuery(new DbConnectionImpl(con, dialect, delegatorInterface)));
            }

            @Override
            public void execute(@Nonnull SqlCallback callback) {
                databaseAccessor.executeQuery(con -> {
                            callback.run(new DbConnectionImpl(con, dialect, delegatorInterface));
                            return null;
                        }
                );
            }
        };
    }

    @Override
    public ConnectionProvider withLegacyOfBizTransaction() {
        return new ConnectionProvider() {
            @Override
            public <T> T executeQuery(@Nonnull QueryCallback<T> callback) {
                try {
                    return executeWithLegacyOfBizTransaction(callback);
                } catch (GenericTransactionException ex) {
                    throw new DataAccessException(ex);
                }
            }

            @Override
            public void execute(@Nonnull SqlCallback callback) {
                try {
                    executeWithLegacyOfBizTransaction((dbConnection) -> {
                        callback.run(dbConnection);
                        return null;
                    });
                } catch (GenericTransactionException ex) {
                    throw new DataAccessException(ex);
                }
            }
        };
    }

    private <T> T executeWithLegacyOfBizTransaction(QueryCallback<T> callback) throws GenericTransactionException {
        final boolean began = CoreTransactionUtil.begin();
        boolean success = false;
        try {
            // wrap the underlying JDBC connection as a safety measure against devs calling commit() close() etc
            final Connection jdbcConnection = new NestedConnection(CoreTransactionUtil.getConnection());
            final DbConnection dbConnection = new DbConnectionImpl(jdbcConnection, dialect, delegatorInterface);
            T val = callback.runQuery(dbConnection);
            CoreTransactionUtil.commit(began);
            success = true;
            return val;
        } finally {
            if (!success) {
                // Mark as rollback only in the OfBiz transaction ...
                CoreTransactionUtil.rollback(began);
            }
        }
    }

    @Override
    public DbConnection withDbConnection(Connection connection) {
        return new DbConnectionImpl(connection, dialect, delegatorInterface);
    }
}
