package com.atlassian.jira.web.sitemesh;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.SimpleLink;
import com.atlassian.jira.plugin.webfragment.model.SimpleLinkSection;
import com.atlassian.jira.user.ApplicationUser;

import java.util.Optional;

/**
 * This implementation only works in Cloud, for Server it always returns empty.
 *
 * @since v7.0
 */
@Internal
public class DefaultAdminDecoratorSectionLinkItemHelper implements AdminDecoratorSectionLinkItemHelper {
    @Override
    @Deprecated
    public Optional<SimpleLink> findSectionLink(final SimpleLinkSection currentSection, final ApplicationUser loggedInUser, final JiraHelper jiraHelper) {
        return Optional.empty();
    }
}
