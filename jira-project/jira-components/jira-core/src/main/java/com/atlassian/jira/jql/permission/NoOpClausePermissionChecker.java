package com.atlassian.jira.jql.permission;

import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.user.ApplicationUser;

import java.util.Set;

/**
 * A No-Op clause permission checker that always allows you to use a clause.
 *
 * @since v4.0
 */
public final class NoOpClausePermissionChecker implements ClausePermissionChecker {
    public static final NoOpClausePermissionChecker NOOP_CLAUSE_PERMISSION_CHECKER = new NoOpClausePermissionChecker();

    // shouldn't need construction
    private NoOpClausePermissionChecker() {
    }

    public boolean hasPermissionToUseClause(final ApplicationUser user) {
        return true;
    }

    @Override
    public boolean hasPermissionToUseClause(ApplicationUser searcher, Set<FieldLayout> fieldLayouts) {
        return true;
    }
}
