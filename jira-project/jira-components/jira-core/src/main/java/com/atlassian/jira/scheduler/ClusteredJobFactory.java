package com.atlassian.jira.scheduler;

import com.atlassian.jira.entity.AbstractEntityFactory;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.scheduler.caesium.impl.ImmutableClusteredJob;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.config.Schedule.Type;
import org.ofbiz.core.entity.GenericValue;

import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import static com.atlassian.jira.entity.Entity.Name.CLUSTERED_JOB;

/**
 * @since v7.0
 */
public class ClusteredJobFactory extends AbstractEntityFactory<OfBizClusteredJob> {
    public static final String ID = "id";
    public static final String JOB_ID = "jobId";
    public static final String JOB_RUNNER_KEY = "jobRunnerKey";
    public static final String SCHED_TYPE = "schedType";
    public static final String INTERVAL_MILLIS = "intervalMillis";
    public static final String FIRST_RUN = "firstRun";
    public static final String CRON_EXPRESSION = "cronExpression";
    public static final String TIME_ZONE = "timeZone";
    public static final String NEXT_RUN = "nextRun";
    public static final String VERSION = "version";
    public static final String PARAMETERS = "parameters";

    public static final String TYPE_INTERVAL = "I";
    public static final String TYPE_CRON = "C";

    @Override
    public OfBizClusteredJob build(final GenericValue genericValue) {
        return new OfBizClusteredJob(genericValue.getLong(ID), ImmutableClusteredJob.builder()
                .jobId(JobId.of(genericValue.getString(JOB_ID)))
                .jobRunnerKey(JobRunnerKey.of(genericValue.getString(JOB_RUNNER_KEY)))
                .schedule(getSchedule(genericValue))
                .nextRunTime(toDate(genericValue.getLong(NEXT_RUN)))
                .version(genericValue.getLong(VERSION))
                .parameters((byte[]) genericValue.get(PARAMETERS))
                .build());
    }


    @Override
    public Map<String, Object> fieldMapFrom(final OfBizClusteredJob value) {
        final FieldMap fields = new FieldMap()
                .add(ID, value.getId())
                .add(JOB_ID, value.getJobId().toString())
                .add(JOB_RUNNER_KEY, value.getJobRunnerKey().toString())
                .add(NEXT_RUN, toLong(value.getNextRunTime()))
                .add(VERSION, value.getVersion())
                .add(PARAMETERS, value.getRawParameters());
        addScheduleInfo(fields, value.getSchedule());
        return fields;
    }

    @Override
    public String getEntityName() {
        return CLUSTERED_JOB;
    }


    private static Schedule getSchedule(final GenericValue genericValue) {
        final Type type = toScheduleType(genericValue.getString(SCHED_TYPE));
        switch (type) {
            case INTERVAL:
                return Schedule.forInterval(
                        genericValue.getLong(INTERVAL_MILLIS),
                        toDate(genericValue.getLong(FIRST_RUN)));

            case CRON_EXPRESSION:
                return Schedule.forCronExpression(
                        genericValue.getString(CRON_EXPRESSION),
                        toTimeZone(genericValue.getString(TIME_ZONE)));
        }
        throw badType(type);
    }

    private static void addScheduleInfo(FieldMap fields, Schedule schedule) {
        switch (schedule.getType()) {
            case INTERVAL:
                fields.add(SCHED_TYPE, TYPE_INTERVAL);
                fields.add(INTERVAL_MILLIS, schedule.getIntervalScheduleInfo().getIntervalInMillis());
                fields.add(FIRST_RUN, toLong(schedule.getIntervalScheduleInfo().getFirstRunTime()));
                return;

            case CRON_EXPRESSION:
                fields.add(SCHED_TYPE, TYPE_CRON);
                fields.add(CRON_EXPRESSION, schedule.getCronScheduleInfo().getCronExpression());
                fields.add(TIME_ZONE, toTimeZoneId(schedule.getCronScheduleInfo().getTimeZone()));
                return;
        }
        throw badType(schedule.getType());
    }

    private static Type toScheduleType(String value) {
        if (value != null && value.length() == 1) {
            switch (value.charAt(0)) {
                case 'C':
                    return Type.CRON_EXPRESSION;
                case 'I':
                    return Type.INTERVAL;
            }
        }
        throw badType(value);
    }

    private static TimeZone toTimeZone(String zoneId) {
        return (zoneId != null) ? TimeZone.getTimeZone(zoneId) : null;
    }

    private static String toTimeZoneId(TimeZone timeZone) {
        return (timeZone != null) ? timeZone.getID() : null;
    }

    private static Date toDate(Long date) {
        return (date != null) ? new Date(date) : null;
    }

    private static Long toLong(Date date) {
        return (date != null) ? date.getTime() : null;
    }

    private static IllegalArgumentException badType(Object scheduleType) {
        return new IllegalArgumentException("Unsupported schedule type: " + scheduleType);
    }
}
