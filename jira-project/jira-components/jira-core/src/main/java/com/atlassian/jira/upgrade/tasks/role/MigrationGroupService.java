package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.GroupWithAttributes;

import java.util.Set;

/**
 * Service for retrieving groups with attributes and members of a group.
 * Should only be used during renaissance migration.
 *
 * @since 7.0
 */
public interface MigrationGroupService {
    /**
     * Retrieves attributes of the group.
     *
     * @param group for which attributes should be retrieved.
     * @return group with attributes.
     */
    GroupWithAttributes getGroupWithAttributes(Group group);

    /**
     * Retrieves all ACTIVE users in the given group. This method will also return users within nested groups.
     * Please note that users that are disabled will not be returned by this method.
     *
     * @param group for which users should be retrieved.
     * @return Set of users with permissions.
     */
    Set<UserWithPermissions> getUsersInGroup(Group group);
}
