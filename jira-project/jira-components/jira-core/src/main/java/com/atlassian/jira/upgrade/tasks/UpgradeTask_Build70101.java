package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.entity.Update;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;
import com.atlassian.jira.upgrade.util.UpgradeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Goes through all of the projects on the JIRA instance, assigning appropriate project types.
 *
 * @since 7.0
 */
public class UpgradeTask_Build70101 extends AbstractImmediateUpgradeTask {
    private static final Integer BUILD_NUMBER = 70101;
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask_Build70101.class);

    private static final String SERVICEDESK_PROJECTS_TABLE = "AO_54307E_SERVICEDESK";
    private static final String SERVICEDESK_DISABLED_COLUMN = "DISABLED";
    private static final String PROJECT_ID_COLUMN = "PROJECT_ID";

    private static final String ID_COLUMN = "id";
    private static final String PROJECT_TYPE_COLUMN = "projecttype";

    private static final String SERVICEDESK_PROJECT_TYPE = "service_desk";
    private static final String SOFTWARE_PROJECT_TYPE = "software";

    private final EntityEngine entityEngine;
    private final ProjectManager projectManager;

    @Inject
    public UpgradeTask_Build70101(
            EntityEngine entityEngine,
            ProjectManager projectManager) {
        this.entityEngine = entityEngine;
        this.projectManager = projectManager;
    }

    @Override
    public int getBuildNumber() {
        return BUILD_NUMBER;
    }

    @Override
    public String getShortDescription() {
        return "Assigning appropriate project types for the projects on the instance";
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception {
        int projectsConvertedToServiceDesk = 0;
        int projectsConvertedToSoftware = 0;

        try {
            boolean serviceDeskExists = UpgradeUtils.tableExists(SERVICEDESK_PROJECTS_TABLE);
            for (Long projectId : getIdsOfProjects()) {
                if (serviceDeskExists && isServiceDesk(projectId)) {
                    setServiceDeskProjectType(projectId);
                    projectsConvertedToServiceDesk++;
                } else {
                    setSoftwareProjectType(projectId);
                    projectsConvertedToSoftware++;
                }
            }
        } finally {
            log.info("Converted {} projects to Software and {} projects to Service Desk.", projectsConvertedToSoftware, projectsConvertedToServiceDesk);
            projectManager.refresh();
        }
    }

    private List<Long> getIdsOfProjects() {
        return Select
                .id()
                .from(Entity.Name.PROJECT)
                .runWith(entityEngine)
                .asList();
    }

    private boolean isServiceDesk(Long projectId) {
        try (Connection connection = getDatabaseConnection()) {
            String sql = String.format("select %s from %s where %s = ?",
                    addQuotesIfPostgres(SERVICEDESK_DISABLED_COLUMN),
                    convertToSchemaTableName(addQuotesIfPostgres(SERVICEDESK_PROJECTS_TABLE)),
                    addQuotesIfPostgres(PROJECT_ID_COLUMN));
            try (PreparedStatement ps = connection.prepareStatement(sql)) {
                ps.setLong(1, projectId);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        return !rs.getBoolean(SERVICEDESK_DISABLED_COLUMN);
                    }
                    return false;
                }
            }
        } catch (SQLException e) {
            log.error("An exception occurred when querying the Service Desk table", e);
            return false;
        }
    }

    private String quoted(String table) {
        return String.format("\"%s\"", table);
    }

    private void setServiceDeskProjectType(Long projectId) {
        setProjectType(projectId, SERVICEDESK_PROJECT_TYPE);
    }

    private void setSoftwareProjectType(Long projectId) {
        setProjectType(projectId, SOFTWARE_PROJECT_TYPE);
    }

    private void setProjectType(Long projectId, String projectType) {
        Update.into(Entity.Name.PROJECT).set(PROJECT_TYPE_COLUMN, projectType).whereEqual(ID_COLUMN, projectId).execute(entityEngine);
    }

    private String addQuotesIfPostgres(String field) {
        return isPostgreSQL() ? quoted(field) : field;
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        return false;
    }
}
