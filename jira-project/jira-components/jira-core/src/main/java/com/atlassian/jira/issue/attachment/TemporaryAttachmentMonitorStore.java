package com.atlassian.jira.issue.attachment;

import com.atlassian.fugue.Option;
import org.joda.time.DateTime;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;

/**
 * A database backed store of the mappings between temporary attachments and form tokens
 *
 * @since v7.1.0
 */
@ParametersAreNonnullByDefault
public interface TemporaryAttachmentMonitorStore {

    Option<TemporaryWebAttachment> getById(TemporaryAttachmentId temporaryAttachmentId);

    Collection<TemporaryWebAttachment> getByFormToken(String formToken);

    Option<TemporaryWebAttachment> removeById(TemporaryAttachmentId temporaryAttachmentId);

    boolean putIfAbsent(TemporaryWebAttachment temporaryWebAttachment);

    long removeOlderThan(DateTime dateTime);
}
