package com.atlassian.jira.user.usermapper;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;

public abstract class AbstractUserMapper implements UserMapper {
    private final UserManager userManager;

    protected AbstractUserMapper(UserManager userManager) {
        this.userManager = userManager;
    }

    protected ApplicationUser getUser(String username) {
        return userManager.getUser(username);
    }

    public abstract ApplicationUser getUserFromEmailAddress(String emailAddress);
}
