package com.atlassian.jira.web.landingpage;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.user.ApplicationUser;

import java.util.Optional;

/**
 * Interface for implementing redirects of another pages. Especially useful when a particular page needs to be
 * redirected to more than one place.
 * <p>
 * See {@link LandingPageRedirectManager} for more details.
 *
 * @since v7.0
 */
@Internal
public interface PageRedirect {
    /**
     * Gets the URL of the redirect for a given user. Can return empty option, in that case redirect is not active.
     *
     * @param user for which redirect should be applied.
     * @return URL to which user will be redirected or {@link Optional#empty()} if this redirect should not be used.
     */
    Optional<String> url(ApplicationUser user);
}
