package com.atlassian.jira.imports.project.taskprogress;

import javax.annotation.Nullable;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;

public class ThrottlingTaskProgressProcessor implements TaskProgressProcessor {
    private final TaskProgressProcessor taskProgressProcessor;
    private final Clock clock;
    private Instant nextUpdateTime;
    private static final Duration UPDATE_PROGRESS_PERIOD = Duration.ofSeconds(2);


    public ThrottlingTaskProgressProcessor(@Nullable final TaskProgressProcessor taskProgressProcessor,
                                           final Clock clock) {
        this.taskProgressProcessor = taskProgressProcessor;
        this.clock = clock;
        nextUpdateTime = clock.instant();
    }

    public ThrottlingTaskProgressProcessor() {
        this.taskProgressProcessor = null;
        this.clock = null;
    }

    /**
     * Calculates, if a task progress sink is registered, the progress of processing the XML file.
     * @param qName the current top-level entity being processed
     * @param entityTypeCount
     * @param entityCount
     * @param currentEntityCount
     */
    public void processTaskProgress(final String qName,
                                    final int entityTypeCount,
                                    final long entityCount,
                                    final long currentEntityCount) {
        if ((taskProgressProcessor != null) && canUpdateProgress()) {
            nextUpdateTime = clock.instant().plus(UPDATE_PROGRESS_PERIOD);
            taskProgressProcessor.processTaskProgress(qName, entityTypeCount, entityCount, currentEntityCount);
        }
    }

    private boolean canUpdateProgress() {
        return clock.instant().isAfter(nextUpdateTime);
    }
}
