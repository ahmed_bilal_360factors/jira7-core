package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettings;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.util.Visitor;
import com.google.common.annotations.VisibleForTesting;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.locks.Lock;

import static com.atlassian.jira.util.Functions.mappedVisitor;

/**
 * A User cache that eagerly loads a full set of all users.
 *
 * @since v7.0
 */
class EagerOfBizUserCache extends UserOrGroupCache<OfBizUser> implements OfBizUserCache {
    /**
     * The EHCache settings are really coming from ehcache.xml, but we need to set unflushable() here .
     */
    @VisibleForTesting
    static final CacheSettings USER_CACHE_SETTINGS = new CacheSettingsBuilder().unflushable().replicateViaCopy().build();
    private static final String LOAD_USER_CACHE_LOCK = EagerOfBizUserCache.class.getName() + ".loadUserCacheLock";

    private ClusterLockService clusterLockService;
    private CacheManager cacheManager;
    private DirectoryDao directoryDao;
    private OfBizDelegator ofBizDelegator;

    EagerOfBizUserCache(final ClusterLockService clusterLockService, final CacheManager cacheManager,
                        final DirectoryDao directoryDao, final OfBizDelegator ofBizDelegator) {
        super(UserEntity.ENTITY);
        this.clusterLockService = clusterLockService;
        this.cacheManager = cacheManager;
        this.directoryDao = directoryDao;
        this.ofBizDelegator = ofBizDelegator;
    }

    @Override
    Lock getLock() {
        return clusterLockService.getLockForName(LOAD_USER_CACHE_LOCK);
    }

    @Override
    Cache<DirectoryEntityKey, OfBizUser> createCache() {
        return cacheManager.getCache(EagerOfBizUserCache.class.getName() + ".userCache", null, USER_CACHE_SETTINGS);
    }

    @Override
    long countAllUsingDatabase() {
        // Count by directory to make sure that what we count matches what we would actually visit.
        // We would only get a discrepancy if there are garbage users with an invalid directory ID, but
        // better safe than sorry...
        long count = 0L;
        for (Directory directory : directoryDao.findAll()) {
            count += Select.id()
                    .from(UserEntity.ENTITY)
                    .whereEqual(UserEntity.DIRECTORY_ID, directory.getId())
                    .runWith(ofBizDelegator)
                    .count();
        }
        return count;
    }

    @Override
    public void visitAllUsingDatabase(final Visitor<OfBizUser> visitor) {
        final Visitor<GenericValue> gvVisitor = mappedVisitor(OfBizUserDao.TO_USER_FUNCTION, visitor);
        for (Directory directory : directoryDao.findAll()) {
            Select.columns(OfBizUser.SUPPORTED_FIELDS)
                    .from(UserEntity.ENTITY)
                    .whereEqual(UserEntity.DIRECTORY_ID, directory.getId())
                    .runWith(ofBizDelegator)
                    .visitWith(gvVisitor);
        }
    }

    @Override
    public void visitAllUserIdsInDirectory(long directoryId, final Visitor<String> visitor) {
        visitAllInDirectory(directoryId, new Visitor<OfBizUser>() {
            @Override
            public void visit(OfBizUser element) {
                visitor.visit(element.getExternalId());
            }
        });
    }

    @Override
    public List<OfBizUser> getAllCaseInsensitive(long directoryId, Collection<String> userIds) {
        List<OfBizUser> users = new ArrayList<OfBizUser>(userIds.size());
        for (String userId : userIds) {
            OfBizUser user = getCaseInsensitive(directoryId, userId);

            //Might be null if the user was removed after user ID list was built up
            if (user != null) {
                users.add(user);
            }
        }

        return users;
    }
}
