package com.atlassian.jira.web.action.admin.instrumentation.cache;

import com.atlassian.jira.instrumentation.CacheStatistics;
import com.atlassian.jira.instrumentation.InstrumentationLogger;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.reducing;
import static webwork.action.ActionContext.getSingleValueParameters;

/**
 * Display cache usage information broken down by request path.
 *
 * @since v7.1
 */
@WebSudoRequired
public class ViewCachesByRequest extends JiraWebActionSupport {
    private String req;
    private final InstrumentationLogger instrumentationLogger;

    public ViewCachesByRequest(InstrumentationLogger instrumentationLogger) {
        this.instrumentationLogger = instrumentationLogger;
    }

    public List<CacheRequestBean> getRequestStats() {
        req = (String) getSingleValueParameters().getOrDefault("r", "/admin/login.jsp");

        List<com.atlassian.jira.instrumentation.LogEntry> cacheRequestInfo = instrumentationLogger.getLogEntriesFromBuffer();

        long count = cacheRequestInfo.stream().filter(l -> l.getData().size() > 0 && l.getPath().equals(req)).count();

        List<CacheRequestBean> result = new CopyOnWriteArrayList<>();

        // map-reduce the stats grouping by request path and then averaging the stats.
        cacheRequestInfo.stream()
                .filter(logEntry -> logEntry.getData().get(CacheStatistics.CACHE_LAAS_ID) != null && logEntry.getPath().equals(req))
                .flatMap(l -> l.getData().get(CacheStatistics.CACHE_LAAS_ID).stream())
                .filter(stat -> stat instanceof CacheStatistics)
                .map(stat -> CacheStatsAverager.build(stat.getName(), (CacheStatistics) stat))
                .collect(Collectors.groupingBy(CacheStatsAverager::getName, collectingAndThen(
                        reducing((a, b) -> a.addAndSetCount(b, count)), Optional::get)))
                .forEach((name, statsAverager) ->
                        result.add(new CacheRequestBean(name, statsAverager.getAverageHits(),
                                statsAverager.getAveragePuts(),
                                statsAverager.getAverageMisses(), statsAverager.getAverageRemoves(),
                                statsAverager.getAverageLoads(),
                                statsAverager.getAverageLoadTime())));

        return result;
    }

    public String getReq() {
        return req;
    }


    public class CacheRequestBean {
        private String name;
        private double hits;
        private double puts;
        private double misses;
        private double removes;
        private double loads;
        private double loadTime;

        public CacheRequestBean(String name, double hits, double puts, double misses, double removes, double loads, double loadTime) {
            this.name = name;
            this.hits = hits;
            this.misses = misses;
            this.puts = puts;
            this.removes = removes;
            this.loads = loads;
            this.loadTime = loadTime;
        }

        public String getName() {
            return name;
        }

        public void setName(final String name) {
            this.name = name;
        }

        public double getHits() {
            return hits;
        }

        public double getPuts() {
            return puts;
        }

        public double getMisses() {
            return misses;
        }

        public double getRemoves() {
            return removes;
        }

        public double getLoads() {
            return loads;
        }

        public double getLoadTime() {
            return loadTime;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof CacheRequestBean)) {
                return false;
            }

            CacheRequestBean r = (CacheRequestBean) o;
            return r.getName().equals(this.getName());
        }
    }
}
