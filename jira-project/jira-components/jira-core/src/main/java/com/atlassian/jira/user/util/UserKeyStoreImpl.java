package com.atlassian.jira.user.util;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.entity.Delete;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.entity.Update;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.user.ApplicationUserEntity;
import org.ofbiz.core.entity.DelegatorInterface;

import java.util.List;
import java.util.Optional;

import static com.atlassian.jira.entity.ApplicationUserEntityFactory.ID;
import static com.atlassian.jira.entity.ApplicationUserEntityFactory.USER_KEY;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static java.util.Optional.empty;

public class UserKeyStoreImpl implements UserKeyStore {
    private static final String LOWER_USER_NAME = "lowerUserName";

    private final EntityEngine entityEngine;
    private final OfBizDelegator ofBizDelegator;
    private final DelegatorInterface delegatorInterface;

    private final Cache<String, Optional<ApplicationUserEntity>> keyToUserCache;
    private final Cache<String, Optional<ApplicationUserEntity>> usernameToUserCache;
    private final Cache<Long, Optional<ApplicationUserEntity>> idToUserCache;

    public UserKeyStoreImpl(final EntityEngine entityEngine,
                            final OfBizDelegator ofBizDelegator,
                            final DelegatorInterface delegatorInterface,
                            final EventPublisher eventPublisher,
                            final CacheManager cacheManager) {
        this.entityEngine = entityEngine;
        this.ofBizDelegator = ofBizDelegator;
        this.delegatorInterface = delegatorInterface;

        keyToUserCache = cacheManager.getCache(UserKeyStoreImpl.class.getName() + ".keyToUserCache",
                key -> Optional.ofNullable(Select.from(Entity.APPLICATION_USER).whereEqual(USER_KEY, key).runWith(entityEngine).singleValue())
        );

        usernameToUserCache = cacheManager.getCache(UserKeyStoreImpl.class.getName() + ".usernameToUserCache",
                username -> Optional.ofNullable(Select.from(Entity.APPLICATION_USER).whereEqual(LOWER_USER_NAME, username).runWith(entityEngine).singleValue())
        );

        idToUserCache = cacheManager.getCache(UserKeyStoreImpl.class.getName() + ".idToUserCache",
                id -> Optional.ofNullable(Select.from(Entity.APPLICATION_USER).whereEqual(ID, id).runWith(entityEngine).singleValue())
        );

        buildCache();
        eventPublisher.register(this);
    }


    @Override
    public String getUsernameForKey(final String userKey) {
        return getUserForKey(userKey).map(ApplicationUserEntity::getUsername).orElse(null);
    }

    @Override
    public String getKeyForUsername(final String username) {
        return getUserForUsername(username).map(ApplicationUserEntity::getKey).orElse(null);
    }

    @Override
    public Long getIdForUserKey(final String userKey) {
        return getUserForKey(userKey).map(ApplicationUserEntity::getId).orElse(null);
    }

    @Override
    public Optional<ApplicationUserEntity> getUserForId(final Long id) {
        return fromCache(idToUserCache, id);
    }

    @Override
    public Optional<ApplicationUserEntity> getUserForKey(final String key) {
        return fromCache(keyToUserCache, key);
    }

    @Override
    public Optional<ApplicationUserEntity> getUserForUsername(final String username) {
        return fromCache(usernameToUserCache, IdentifierUtils.toLowerCase(username));
    }

    @Override
    public void renameUser(String oldUsername, String newUsername) {
        // Lower-case the usernames
        oldUsername = IdentifierUtils.toLowerCase(oldUsername);
        newUsername = IdentifierUtils.toLowerCase(newUsername);
        // Find the key
        final ApplicationUserEntity user = Select.from(Entity.APPLICATION_USER).whereEqual(LOWER_USER_NAME, oldUsername).runWith(ofBizDelegator).singleValue();
        if (user == null) {
            throw new IllegalStateException("Trying to rename user '" + oldUsername + "' but no user key is mapped.");
        }

        entityEngine.execute(Update.into(Entity.APPLICATION_USER).set(LOWER_USER_NAME, newUsername).whereEqual(USER_KEY, user.getKey()));

        keyToUserCache.remove(user.getKey());
        usernameToUserCache.remove(newUsername);
        usernameToUserCache.remove(oldUsername);
        idToUserCache.remove(user.getId());
    }

    @Override
    public String ensureUniqueKeyForNewUser(final String username) {
        final String lowerUsername = IdentifierUtils.toLowerCase(notNull("username", username));
        // First check if we already have a mapping for this username
        ApplicationUserEntity applicationUserEntity = Select.from(Entity.APPLICATION_USER)
                .whereEqual(LOWER_USER_NAME, lowerUsername)
                .runWith(entityEngine).singleValue();
        if (applicationUserEntity != null) {
            // a mapping already exists for this username - nothing to do
            return applicationUserEntity.getKey();
        }

        // By default we use the lower-case username as the key: is this available?
        applicationUserEntity = Select.from(Entity.APPLICATION_USER)
                .whereEqual(USER_KEY, lowerUsername)
                .runWith(entityEngine).singleValue();
        if (applicationUserEntity == null) {
            // Add an explicit mapping using the lowerUsername as the key
            final FieldMap fieldValues = FieldMap.build(USER_KEY, lowerUsername).add(LOWER_USER_NAME, lowerUsername);
            ofBizDelegator.createValue(Entity.APPLICATION_USER.getEntityName(), fieldValues);
            // Update the caches
            usernameToUserCache.remove(lowerUsername);
            keyToUserCache.remove(lowerUsername);
            return lowerUsername;
        }

        // We are creating a new user that is recycling a username that was previously used and so the default user key
        // is taken. We need to create a special unique userkey for this user.

        // First find the next ID for this database table
        final Long id = delegatorInterface.getNextSeqId(Entity.APPLICATION_USER.getEntityName());
        // now we use that id to create a guaranteed unique userkey eg ID10012
        final String userkey = "ID" + id;
        final FieldMap fieldValues = FieldMap.build("id", id).add(USER_KEY, userkey).add(LOWER_USER_NAME, lowerUsername);
        ofBizDelegator.createValue(Entity.APPLICATION_USER.getEntityName(), fieldValues);
        // Update the caches
        usernameToUserCache.remove(lowerUsername);
        keyToUserCache.remove(userkey);
        return userkey;
    }

    @Override
    public String removeByKey(final String key) {
        if (key == null) {
            return null;
        }

        try {
            final ApplicationUserEntity user = Select.from(Entity.APPLICATION_USER).whereEqual(USER_KEY, key).runWith(entityEngine).singleValue();
            if (user != null) {
                Delete.from(Entity.APPLICATION_USER).whereEqual(USER_KEY, key).execute(entityEngine);
                usernameToUserCache.remove(user.getUsername());
                idToUserCache.remove(user.getId());
                return user.getUsername();
            }
            return null;
        } finally {
            keyToUserCache.remove(key);
        }
    }

    private void buildCache() {
        // This method aggressively loads the local cache during startup or other refresh the world scenarios.
        // It does contain a possible race condition across cluster nodes, but that is regarded as a worthwhile trade off
        // for performance.

        final List<ApplicationUserEntity> users = Select.from(Entity.APPLICATION_USER).runWith(entityEngine).asList();
        for (final ApplicationUserEntity user : users) {
            final Optional<ApplicationUserEntity> cacheValue = Optional.of(user);
            keyToUserCache.put(user.getKey(), cacheValue);
            usernameToUserCache.put(user.getUsername(), cacheValue);
            idToUserCache.put(user.getId(), cacheValue);
        }
    }

    @SuppressWarnings("UnusedParameters")
    @EventListener
    public void onClearCache(final ClearCacheEvent event) {
        keyToUserCache.removeAll();
        usernameToUserCache.removeAll();
        idToUserCache.removeAll();
        buildCache();
    }

    private static <K, V> Optional<V> fromCache(final Cache<K, Optional<V>> cache, final K key) {
        return key != null ? cache.get(key) : empty();
    }
}
