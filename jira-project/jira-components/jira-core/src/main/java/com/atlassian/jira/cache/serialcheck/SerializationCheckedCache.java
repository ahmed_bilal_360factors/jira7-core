package com.atlassian.jira.cache.serialcheck;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheEntryListener;
import com.atlassian.cache.Supplier;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;

/**
 * Decorator that makes a serialization-checked version of a cache.
 *
 * @since v7.2.0
 */
@ParametersAreNonnullByDefault
public class SerializationCheckedCache<K, V> implements Cache<K, V> {

    private final Cache<K, V> delegate;
    private final SerializationChecker checker;

    public SerializationCheckedCache(final SerializationChecker checker, final Cache<K, V> delegate) {
        this.delegate = delegate;
        this.checker = checker;
    }

    @Override
    @Nonnull
    public String getName() {
        return delegate.getName();
    }

    @Override
    public boolean containsKey(final K key) {
        return delegate.containsKey(key);
    }

    @Override
    @Nonnull
    public Collection<K> getKeys() {
        return delegate.getKeys();
    }

    @Override
    @Nullable
    public V get(final K key) {
        return delegate.get(key);
    }

    @Override
    @Nonnull
    public V get(final K key, final Supplier<? extends V> supplier) {
        return delegate.get(key, supplier);
    }

    @Override
    public void put(final K key, final V value) {
        checker.checkValue(this, key, value);
        delegate.put(key, value);
    }

    @Override
    @Nullable
    public V putIfAbsent(final K key, final V value) {
        checker.checkValue(this, key, value);
        return delegate.putIfAbsent(key, value);
    }

    @Override
    public void remove(final K key) {
        delegate.remove(key);
    }

    @Override
    public boolean remove(final K key, final V value) {
        return delegate.remove(key, value);
    }

    @Override
    public void removeAll() {
        delegate.removeAll();
    }

    @Override
    public boolean replace(final K key, final V oldValue, final V newValue) {
        checker.checkValue(this, key, newValue);
        return delegate.replace(key, oldValue, newValue);
    }

    @Override
    public void addListener(final CacheEntryListener<K, V> listener, final boolean includeValues) {
        delegate.addListener(listener, includeValues);
    }

    @Override
    public void removeListener(final CacheEntryListener<K, V> cacheEntryListener) {
        delegate.removeListener(cacheEntryListener);
    }
}
