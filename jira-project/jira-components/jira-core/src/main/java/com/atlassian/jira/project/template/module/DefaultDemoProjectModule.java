package com.atlassian.jira.project.template.module;

import com.atlassian.plugin.Plugin;

import java.net.URL;
import java.util.Optional;

/**
 * @since v7.1
 */
public class DefaultDemoProjectModule implements DemoProjectModule {
    private final String key;
    private final Integer weight;
    private final String labelKey;
    private final String descriptionKey;
    private final Optional<String> longDescriptionKey;

    private final String importFile;
    private final Optional<String> projectTemplateKey;
    private final Optional<String> projectTypeKey;

    private final Icon icon;
    private final Optional<Icon> backgroundIcon;
    private Plugin modulePlugin;

    public DefaultDemoProjectModule(String key, Integer weight, String labelKey, String descriptionKey, Optional<String> longDescriptionKey, String importFile, Optional<String> projectTemplateKey, Optional<String> projectTypeKey, Icon icon, Optional<Icon> backgroundIcon, Plugin modulePlugin) {
        this.key = key;
        this.weight = weight;
        this.labelKey = labelKey;
        this.descriptionKey = descriptionKey;
        this.longDescriptionKey = longDescriptionKey;
        this.importFile = importFile;
        this.projectTemplateKey = projectTemplateKey;
        this.projectTypeKey = projectTypeKey;
        this.icon = icon;
        this.backgroundIcon = backgroundIcon;
        this.modulePlugin = modulePlugin;
    }


    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Integer getWeight() {
        return weight;
    }

    @Override
    public String getLabelKey() {
        return labelKey;
    }

    @Override
    public String getDescriptionKey() {
        return descriptionKey;
    }

    @Override
    public Optional<String> getLongDescriptionKey() {
        return longDescriptionKey;
    }

    @Override
    public Optional<String> getProjectTemplateKey() {
        return projectTemplateKey;
    }

    @Override
    public Optional<String> getProjectTypeKey() {
        return projectTypeKey;
    }

    @Override
    public String getIconUrl() {
        return icon.url();
    }

    @Override
    public Optional<String> getBackgroundIconUrl() {
        return backgroundIcon.map(Icon::url);
    }

    @Override
    public URL getImportFile() {
        return modulePlugin.getResource(importFile);
    }

}
