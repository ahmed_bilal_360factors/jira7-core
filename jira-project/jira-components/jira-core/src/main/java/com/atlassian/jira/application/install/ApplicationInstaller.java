package com.atlassian.jira.application.install;

import com.atlassian.fugue.Pair;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.osgi.framework.Version;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

/**
 * Installs all applications during startup. Checks if applications were already installed if not installs each
 * application plugin.
 *
 * @since v6.5
 */
@Nonnull
public class ApplicationInstaller {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ApplicationInstaller.class);
    private final ApplicationInstallationEnvironment environment;
    private final BundlesVersionDiscovery versionDiscovery;
    private final WhatWasInstalled whatWasInstalled;

    public ApplicationInstaller(final ApplicationInstallationEnvironment environment, final BundlesVersionDiscovery versionDiscovery, final WhatWasInstalled whatWasInstalled) {
        this.environment = environment;
        this.versionDiscovery = versionDiscovery;
        this.whatWasInstalled = whatWasInstalled;
    }

    public void installApplications() {
        final File[] applicationsSources = environment.getApplicationsSource().listFiles(File::isDirectory);
        if (null == applicationsSources || 0 == applicationsSources.length) {
            LOGGER.trace("No applications source directories - skipping.");
            return;
        }

        final PluginBundleInstaller pluginBundleInstaller =
                new PluginBundleInstaller(environment.getApplicationsDestination(), versionDiscovery);

        stream(applicationsSources)
                .map(ApplicationSource::readFromDir)
                .filter(Objects::nonNull)
                .forEach(applicationSource ->
                                installApplicationFiles(applicationSource, pluginBundleInstaller)
                );
    }

    private void installApplicationFiles(final ApplicationSource applicationSource, final PluginBundleInstaller pluginBundleInstaller) {
        try {
            if (whatWasInstalled.wasApplicationSourceInstalled(applicationSource)) {
                LOGGER.trace("Application was already installed: " + applicationSource.getApplicationSourceName());
                return;
            }

            try (ReversibleFileOperations reversibleFileOperations = new ReversibleFileOperations()) {
                for (final File sourceFile : applicationSource.getApplicationBundles()) {
                    pluginBundleInstaller.updatePlugin(sourceFile, reversibleFileOperations);
                }

                whatWasInstalled.storeInstalledApplicationSource(
                        applicationSource,
                        reversibleFileOperations);

                reversibleFileOperations.commit();
            }
        } catch (final IOException e) {
            Throwables.propagate(e);
        }
    }
}

/**
 * This class takes care of installing single plugin files into target destination. This class is embedded in this place
 * cause it's tested together with ApplicationInstaller (it's part of ApplicationInstaller black-box).
 *
 * @since v6.5
 */
class PluginBundleInstaller {
    private static final Logger LOGGER = Logger.getLogger(PluginBundleInstaller.class);

    private final BundlesVersionDiscovery versionDiscovery;
    private final File pluginTargetFolder;
    private final Supplier<Map<String, VersionAndFile>> installedVersionsSupplier;

    public PluginBundleInstaller(final File pluginTargetFolder, final BundlesVersionDiscovery versionDiscovery) {
        this.versionDiscovery = versionDiscovery;
        this.pluginTargetFolder = pluginTargetFolder;
        this.installedVersionsSupplier = getInstalledVersionsSupplier();
    }

    public void updatePlugin(
            final File sourceFile,
            final ReversibleFileOperations reversibleFileOperations)
            throws IOException {
        final File targetFile = new File(pluginTargetFolder, sourceFile.getName());

        ifSourceIsNewer(sourceFile, targetFile, (previousVersionOption) -> {
            try {
                if (previousVersionOption.isPresent()) { // to skip exceptions rethrowing
                    reversibleFileOperations.fileDelete(previousVersionOption.get().targetFile);
                }
                LOGGER.info("Installing application plugin file: " + sourceFile.getAbsolutePath());
                reversibleFileOperations.removeOnRollback(targetFile);
                Files.copy(sourceFile.toPath(), targetFile.toPath(), StandardCopyOption.COPY_ATTRIBUTES);
            } catch (IOException e) {
                Throwables.propagate(e);
            }
        });
    }

    private void ifSourceIsNewer(File sourceFile, File targetFile, Consumer<Optional<VersionAndFile>> actionIfNewer) {
        if (!targetFile.exists()) {
            final Optional<BundlesVersionDiscovery.PluginIdentification> newPluginVersionOption = versionDiscovery.getBundleNameAndVersion(sourceFile);
            final BundlesVersionDiscovery.PluginIdentification newPluginVersion =
                    newPluginVersionOption.orElseThrow(
                            () -> new RuntimeException("Source jar doesn't provide version information: " +
                                    sourceFile.getAbsolutePath()));

            final Map<String, VersionAndFile> installedVersions = installedVersionsSupplier.get();
            final VersionAndFile previousVersion = installedVersions.get(newPluginVersion.getSymbolicName());

            if (canReplaceOldPlugin(previousVersion, newPluginVersion)) {
                actionIfNewer.accept(Optional.ofNullable(previousVersion));
            } else {
                LOGGER.trace("File skipped (newer or equal version exists): " + sourceFile.getAbsolutePath());
            }
        } else {
            LOGGER.trace("File skipped (filename exists): " + sourceFile.getAbsolutePath());
        }
    }

    private boolean canReplaceOldPlugin(@Nullable final VersionAndFile previousVersion, final BundlesVersionDiscovery.PluginIdentification newVersion) {
        return null == previousVersion || previousVersion.version.compareTo(newVersion.getVersion()) < 0;
    }

    private Supplier<Map<String, VersionAndFile>> getInstalledVersionsSupplier() {
        return Suppliers.memoize(this::getInstalledPluginVersions);
    }

    private Map<String, VersionAndFile> getInstalledPluginVersions() {
        final File[] installedPluginFiles = pluginTargetFolder.listFiles(ApplicationSource::jarsOnly);
        if (ArrayUtils.isNotEmpty(installedPluginFiles)) {
            return stream(installedPluginFiles)
                    .map(file -> versionDiscovery.getBundleNameAndVersion(file).map(version -> new Pair<>(file, version)))
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(Collectors.toMap(
                            fileAndVersion -> fileAndVersion.right().getSymbolicName(),
                            fileAndVersion -> new VersionAndFile(fileAndVersion.right().getVersion(), fileAndVersion.left()),
                            PluginBundleInstaller::useNewestVersion
                    ));
        }
        return ImmutableMap.of();
    }

    private static VersionAndFile useNewestVersion(final VersionAndFile left, final VersionAndFile right) {
        return left.version.compareTo(right.version) < 0 ?
                right :
                left;
    }

    private static class VersionAndFile {
        Version version;

        File targetFile;

        public VersionAndFile(final Version version, final File targetFile) {
            this.version = version;
            this.targetFile = targetFile;
        }

    }
}
