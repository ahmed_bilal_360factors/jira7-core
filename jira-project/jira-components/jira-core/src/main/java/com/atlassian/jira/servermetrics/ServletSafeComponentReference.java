package com.atlassian.jira.servermetrics;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.util.Supplier;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import java.util.Optional;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Component reference that is safe to use from servlets, filters, etc...
 * It doesn't hold reference but retrieves it every time.<br>
 * It uses {@link ComponentAccessor#getComponentSafely(Class)} to access component.
 * 
 * @param <T>
 */
@ParametersAreNonnullByDefault
public class ServletSafeComponentReference<T> implements Supplier<Optional<T>> {
    private final Class<T> componentClass;

    public ServletSafeComponentReference(@Nonnull Class<T> componentClass) {
        this.componentClass = notNull("componentClass", componentClass);
    }

    @Override
    public Optional<T> get() {
        return ComponentAccessor.getComponentSafely(componentClass);
    }

    public static <X> ServletSafeComponentReference<X> build(Class<X> componentClass) {
        return new ServletSafeComponentReference<>(componentClass);
    }
}
