package com.atlassian.jira.security.type;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.permission.PermissionContext;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ProjectLead extends AbstractProjectsSecurityType {
    public static final String DESC = "lead";
    private JiraAuthenticationContext jiraAuthenticationContext;

    public ProjectLead(JiraAuthenticationContext jiraAuthenticationContext) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    public String getDisplayName() {
        return jiraAuthenticationContext.getI18nHelper().getText("admin.permission.types.project.lead");
    }

    @Override
    public String getType() {
        return DESC;
    }

    @Override
    public boolean hasPermission(Project project, String argument) {
        // clearly the anonymous user is not the Project Lead.
        return false;
    }

    @Override
    public boolean hasPermission(Issue issue, String parameter) {
        // clearly the anonymous user is not the Project Lead.
        return false;
    }

    @Override
    public boolean hasPermission(Project project, String argument, ApplicationUser user, boolean issueCreation) {
        if (project == null)
            throw new IllegalArgumentException("Project passed must not be null");
        if (user == null)
            throw new IllegalArgumentException("User passed must not be null");

        String projectLead = project.getLeadUserKey();
        // User has permission if they are the lead
        return projectLead != null && projectLead.equals(ApplicationUsers.getKeyFor(user));
    }

    @Override
    public boolean hasPermission(Issue issue, String parameter, ApplicationUser user, boolean issueCreation) {
        return hasPermission(issue.getProjectObject(), parameter, user, issueCreation);
    }

    @Override
    public void doValidation(String key, Map parameters, JiraServiceContext jiraServiceContext) {
        // No specific validation
    }

    @Override
    public Set<ApplicationUser> getUsers(PermissionContext ctx, String ignored) {
        Project project = ctx.getProjectObject();
        ApplicationUser user = project.getProjectLead();
        Set<ApplicationUser> result = new HashSet<ApplicationUser>(1);
        if (user != null)
            result.add(user);
        return result;
    }

    @Override
    public Query getQuery(ApplicationUser searcher, Project project, IssueSecurityLevel securityLevel, String parameter) {
        //JRA-21648 : Project Lead should not return query for issues that you have no permission for
        if (project.getLeadUserKey() == null || !project.getLeadUserKey().equals(ApplicationUsers.getKeyFor(searcher)))
            return null;

        final BooleanQuery query = new BooleanQuery();
        query.add(new TermQuery(new Term(DocumentConstants.PROJECT_ID, "" + project.getId())), BooleanClause.Occur.MUST);
        query.add(super.getQuery(securityLevel), BooleanClause.Occur.MUST);
        return query;
    }
}
