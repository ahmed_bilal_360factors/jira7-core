package com.atlassian.jira.web.filters;

public class JiraImportProgressRedirectHandlerImpl implements JiraImportProgressRedirectHandler {

    @Override
    public String getSetupPage() {
        return "/secure/SetupImport.jspa";
    }

    @Override
    public String getValidImportPage() {
        return "/secure/ImportResult.jspa";
    }

    @Override
    public String getInvalidImportPage() {
        return "/secure/admin/XmlRestore!finish.jspa";
    }

    @Override
    public String getNoProgressPage() {
        return "/";
    }
}
