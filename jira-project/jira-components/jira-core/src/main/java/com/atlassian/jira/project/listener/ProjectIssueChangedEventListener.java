package com.atlassian.jira.project.listener;

import com.atlassian.jira.event.issue.IssueEvent;

/**
 * Listens to the issue changed events and sets the project issue changed time.
 * @since v7.2
 */
public interface ProjectIssueChangedEventListener {
    void listenForIssueChangedEvent(IssueEvent issueEvent);
}
