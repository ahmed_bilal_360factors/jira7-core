package com.atlassian.jira.application.install;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

/**
 * Retrieves and stores information about previous application installations.
 *
 * @since v6.5
 */
public class WhatWasInstalledInApplicationFactory {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(WhatWasInstalledInApplicationFactory.class);
    @VisibleForTesting
    static final String CANNOT_READ_PROPS_MESSAGE = "Cannot load installed applications information from %s. Treating as not installed.";
    private static final Charset CONFIG_ENCODING = Charsets.UTF_8;

    public WhatWasInstalledInApplication load(final File file) throws IOException {
        List<BundlesVersionDiscovery.PluginIdentification> pluginIdentifications = ImmutableList.of();

        try {
            if (file.canRead()) {
                pluginIdentifications = loadPluginIdentificationsFromFile(file);
            }
        } catch (final Exception e) {
            LOGGER.error(String.format(CANNOT_READ_PROPS_MESSAGE, file.getAbsolutePath()), e);
        }
        return new WhatWasInstalledInApplication(pluginIdentifications);
    }

    public WhatWasInstalledInApplication load(final Iterable<BundlesVersionDiscovery.PluginIdentification> pluginIdentifications) {
        return new WhatWasInstalledInApplication(pluginIdentifications);
    }

    public void store(final File applicationInstallInfoFile, final WhatWasInstalledInApplication whatapp, final ReversibleFileOperations reversibleFileOperations)
            throws IOException {
        final Map<String, String> pluginsAsMap = whatapp
                .getPluginIdentifications()
                .stream()
                .collect(toMap(
                        BundlesVersionDiscovery.PluginIdentification::getSymbolicName,
                        pluginIdentification -> pluginIdentification.getVersion().toString()
                ));
        final Properties properties = new Properties();
        properties.putAll(pluginsAsMap);

        Files.createDirectories(applicationInstallInfoFile.getParentFile().toPath());
        reversibleFileOperations.fileDelete(applicationInstallInfoFile);
        reversibleFileOperations.removeOnRollback(applicationInstallInfoFile);

        try (Writer outputStream = Files.newBufferedWriter(applicationInstallInfoFile.toPath(), CONFIG_ENCODING)) {
            properties.store(outputStream, "-- application installation info --");
        }
    }

    private List<BundlesVersionDiscovery.PluginIdentification> loadPluginIdentificationsFromFile(final File file)
            throws IOException {
        final Properties properties = new Properties();
        try (final BufferedReader reader = Files.newBufferedReader(file.toPath(), CONFIG_ENCODING)) {
            properties.load(reader);
        }
        final List<BundlesVersionDiscovery.PluginIdentification> pluginIdentifications = properties
                .entrySet()
                .stream()
                .map(entry -> new BundlesVersionDiscovery.PluginIdentification((String) entry.getKey(), (String) entry.getValue()))
                .collect(Collectors.toList());
        return pluginIdentifications;
    }
}
