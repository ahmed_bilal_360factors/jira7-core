package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;
import com.atlassian.jira.upgrade.tasks.role.DefaultApplicationService;
import com.atlassian.jira.upgrade.tasks.role.DefaultApplicationServiceImpl;
import com.atlassian.jira.util.ComponentFactory;
import com.atlassian.jira.util.Supplier;
import com.google.common.annotations.VisibleForTesting;

import javax.inject.Inject;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * After the Renaissance migration, sets the application as the default ones based on the available licenses.
 *
 * @since v7.0
 */
public class UpgradeTask_Build70102 extends AbstractImmediateUpgradeTask {

    private static final Integer BUILD_NUMBER = 70102;
    private final Supplier<DefaultApplicationService> supplier;

    @Inject
    public UpgradeTask_Build70102(ComponentFactory componentFactory) {
        this(() -> componentFactory.createObject(DefaultApplicationServiceImpl.class));
    }

    @VisibleForTesting
    UpgradeTask_Build70102(Supplier<DefaultApplicationService> supplier) {
        this.supplier = notNull("supplier", supplier);
    }

    @Override
    public int getBuildNumber() {
        return BUILD_NUMBER;
    }

    @Override
    public String getShortDescription() {
        return "Setting Applications as Default.";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception {
        supplier.get().setApplicationsAsDefaultDuring6xTo7xUpgrade();
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        return false;
    }
}
