package com.atlassian.jira.web.filters.util;

import javax.servlet.ServletOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class InMemoryServletOutputStream extends ServletOutputStream {
    private final ByteArrayOutputStream stream = new ByteArrayOutputStream();

    @Override
    public void write(final int b) throws IOException {
        stream.write(b);
    }

    public ByteArrayOutputStream getStream() {
        return stream;
    }
}
