package com.atlassian.jira.message;

import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.BaseUrl;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.util.ExternalLinkUtil;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Locale;
import java.util.Set;

/**
 * A message utility that acts as a delegate to to the following utilities:
 * <p>
 * {@link I18nHelper} , {@link HelpUrls}, {@link BaseUrl} and {@link ExternalLinkUtil}.
 * <p>
 * This utility contains convenient methods to retrieve i18n messages, help urls and external urls.
 *
 * @since 7.0
 */
public interface MessageUtil extends I18nHelper, BaseUrl {
    /**
     * Returns a {@link com.atlassian.jira.help.HelpUrl} associated with the passed key. The {@link #getDefaultUrl()}
     * URL is returned if the {@code key} has no associated URL.
     *
     * @param key the key to search for.
     * @return the {@code HelpUrl}
     * @see HelpUrls#getUrl(String)
     */
    @Nonnull
    HelpUrl getUrl(String key);

    /**
     * Returns a {@link com.atlassian.jira.help.HelpUrl} that can be used for generic JIRA help. It commonly points at
     * the JIRA help index/landing page.
     *
     * @return the default {@code HelpUrl} for this instance.
     * @see HelpUrls#getDefaultUrl()
     */
    @Nonnull
    HelpUrl getDefaultUrl();

    /**
     * Return all the keys that have an associated {@link com.atlassian.jira.help.HelpUrl}.
     *
     * @return all the keys that have an associated {@code HelpUrl}.
     * @see HelpUrls#getUrlKeys()
     */
    @Nonnull
    Set<String> getUrlKeys();

    /**
     * Get the external link for the specified key.
     *
     * @param key of the external link.
     * @return the external link or the specified key if the link was not found.
     * @see ExternalLinkUtil#getProperty(String)
     */
    String getExternalLink(String key);

    /**
     * Get the external link for the specified key with the specified parameter formatted into the link.
     *
     * @param key    of the external link.
     * @param value1 specified parameter.
     * @return the external link with the parameter formatted into the link or the specified key if the link was not
     * found.
     * @see ExternalLinkUtil#getProperty(String, String)
     */
    String getExternalLink(String key, String value1);

    /**
     * Get the external link for the specified key with the specified parameters formatted into the link.
     *
     * @param key    of the external link.
     * @param value1 first specified parameter.
     * @param value2 second specified parameter.
     * @return the external link with the parameters formatted into the link or the specified key if the link was not
     * found.
     * @see ExternalLinkUtil#getProperty(String, String, String)
     */
    String getExternalLink(String key, String value1, String value2);

    /**
     * Get the external link for the specified key with the specified parameters formatted into the link.
     *
     * @param key    of the external link.
     * @param value1 first specified parameter.
     * @param value2 second specified parameter.
     * @param value3 third specified parameter.
     * @return the external link with the parameters formatted into the link or the specified key if the link was not
     * found.
     * @see ExternalLinkUtil#getProperty(String, String, String, String)
     */
    String getExternalLink(String key, String value1, String value2, String value3);

    /**
     * Get the external link for the specified key with the specified parameters formatted into the link.
     *
     * @param key    of the external link.
     * @param value1 first specified parameter.
     * @param value2 second specified parameter.
     * @param value3 third specified parameter.
     * @param value4 third specified parameter.
     * @return the external link with the parameters formatted into the link or the specified key if the link was not
     * found.
     * @see ExternalLinkUtil#getProperty(String, String, String, String, String)
     */
    String getExternalLink(String key, String value1, String value2, String value3, String value4);

    /**
     * Get the external link for the specified key with the specified parameters formatted into the link.
     *
     * @param key        of the external link.
     * @param parameters specified parameters, typically a {@link java.util.List} of parameters.
     * @return the external link with the parameters formatted into the link or the specified key if the link was not
     * found.
     * @see ExternalLinkUtil#getProperty(String, Object)
     */
    String getExternalLink(String key, Object parameters);

    /**
     * Get a {@code String} representation of an anchor tag with an internal link and link text. The internal link would
     * be the JIRA base url with the path of the specified key appended.
     * <p>
     * Example for default implementation:
     * <pre>
     *     #Config internal-help-paths.properties:
     *     app_access_local.path=/secure/admin/ApplicationAccess.jspa
     *     app_access_local.title=application.access.configuration.title
     *
     *     #Config JiraWebActionSupport.properties:
     *     application.access.configuration.title=Application Access
     *
     *     #Usage:
     *     messageUtil.getAnchorTagWithInternalLink("app_access_local");
     *
     *     #Result:
     *     <a href="http://localhost:8090/jira/secure/admin/user/ApplicationAccess.jspa">Application access</a>
     * </pre>
     *
     * @param keyOfLink the key for the internal link to be appended with the base URL.
     * @return a {@code String} representation of an anchor tag with an internal link and link text.
     * @see I18nHelper
     */
    String getAnchorTagWithInternalLink(String keyOfLink);

    @ParametersAreNonnullByDefault
    interface Factory {
        /**
         * Create a {@link MessageUtil} for the current logged-in user.
         *
         * @return MessageUtil set to the locale of the current logged-in user.
         */
        MessageUtil getNewInstance();

        /**
         * Create a {@link MessageUtil} using the specified user's locale.
         *
         * @return MessageUtil set to the locale of the specified user.
         */
        MessageUtil getNewInstance(ApplicationUser user);

        /**
         * Create a {@link MessageUtil} using the specified locale.
         *
         * @return MessageUtil based on the specified locale.
         */
        MessageUtil getNewInstance(Locale locale);
    }
}