package com.atlassian.jira.issue.statistics;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.web.bean.StatisticMapWrapper;
import com.atlassian.query.Query;

/**
 * Yet another wrapper for the nasty {@link com.atlassian.jira.issue.statistics.FilterStatisticsValuesGenerator}. 
 * Experimental as its results in future may be tuned to return or not return empty Projects/Objects etc.
 * @since 7.1
 */
@ExperimentalApi
public interface StatisticsManager {
    /**
     * Returns objects that the issues resulting from the given query belong to, and the number of issues for each
     * object. Issues that do not belong to any object are stored in the null entry.
     *
     * When a nicer wrapper for the FilterStatisticsGenerator is created, please replace this with it.
     *
     * @param query       Query to be evaluated. May be null, is then treated as an empty query (no constraints)
     * @param statsObject Type of the object that statistics are calculated for. Possible values are given as constants
     *                    in the FilterStatisticsValuesGenerator class. e.g. 'project' or 'component'.
     *                    The custom field which this string refers to must implement
     *                    {@link com.atlassian.jira.issue.customfields.statistics.CustomFieldStattable}
     *
     * @return All of the statistics for the resulting objects of the query. 
     */
    StatisticMapWrapper<Object, Integer> getObjectsResultingFrom(Optional<Query> query, String statsObject);

    /**
     * This method returns objects the issues resulting from the given query belong to. Together with the objects the
     * parent project is also returned. Issues that do not belong to any object are ignored.
     *
     * @param query                Query to be evaluated. May be null, is then treated as an empty query (no constraints)
     * @param objToProjectIdMapper Function that for an object should return the id of the project it belongs to
     * @param objectType           Type of object to be searched for. Possible values are given as constants in the
     *                             FilterStatisticsValuesGenerator class
     * @param <T>                  Type of the resulting object
     * @return See description
     */
    <T> Map<Project, Map<T, Integer>> getProjectsWithItemsWithIssueCount(Optional<Query> query, Function<T, Long> objToProjectIdMapper, String objectType);
}
