package com.atlassian.jira.permission.data;

import com.atlassian.fugue.Option;
import com.atlassian.jira.permission.JiraPermissionHolderType;
import com.atlassian.jira.permission.PermissionHolderType;
import com.google.common.base.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * This is a value class that implements the {@link com.atlassian.jira.permission.PermissionHolderType} interface.
 * <p>
 * Normally we want to use standard set of holder types as defined
 * in {@link com.atlassian.jira.permission.JiraPermissionHolderType}
 * but the types are actually somewhat extensible, so this is needed.
 * </p>
 */
@Immutable
public final class CustomPermissionHolderType implements PermissionHolderType {
    private final String key;
    private final boolean requiresParameter;

    private CustomPermissionHolderType(final String key, final boolean requiresParameter) {
        this.key = key;
        this.requiresParameter = requiresParameter;
    }

    public static PermissionHolderType permissionHolderType(@Nonnull String key, @Nullable String parameter) {
        Option<JiraPermissionHolderType> jiraHolderType = JiraPermissionHolderType.fromKey(key, parameter);
        if (jiraHolderType.isDefined()) {
            return jiraHolderType.get();
        } else {
            return new CustomPermissionHolderType(key, !isNullOrEmpty(parameter));
        }
    }

    public String getKey() {
        return key;
    }

    public boolean requiresParameter() {
        return requiresParameter;
    }

    @Override
    public String toString() {
        return key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomPermissionHolderType that = (CustomPermissionHolderType) o;

        return Objects.equal(this.key, that.key) &&
                Objects.equal(this.requiresParameter, that.requiresParameter);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(key, requiresParameter);
    }
}
