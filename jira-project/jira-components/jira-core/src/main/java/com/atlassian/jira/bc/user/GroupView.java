package com.atlassian.jira.bc.user;

import com.atlassian.jira.web.component.admin.group.GroupLabelView;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.List;

/**
 * Group with name and label
 *
 * @since v7.0
 */
@JsonAutoDetect
public class GroupView {
    private final String name;

    private final List<GroupLabelView> labels;

    public GroupView(final String name, final List<GroupLabelView> labels) {
        this.name = name;
        this.labels = labels;
    }

    public String getName() {
        return name;
    }

    public List<GroupLabelView> getLabels() {
        return labels;
    }
}