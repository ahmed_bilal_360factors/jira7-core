package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.annotation.Nullable;
import java.util.Set;

import static com.atlassian.jira.util.dbc.Assertions.containsNoNulls;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Represents an application role for the renaissance migration (i.e. JIRA 6.x single use permission to JIRA 7.x
 * multiple applications).
 *
 * @since v7.0
 */
final class ApplicationRole {
    private final ApplicationKey key;
    private final ImmutableSet<Group> groups;
    private final ImmutableSet<Group> defaultGroups;

    static ApplicationRole forKey(ApplicationKey key) {
        return new ApplicationRole(key, ImmutableSet.of(), ImmutableSet.of());
    }

    private ApplicationRole(ApplicationKey key, ImmutableSet<Group> groups, ImmutableSet<Group> defaultGroups) {
        this.key = notNull("key", key);
        this.groups = notNull("groups", groups);
        this.defaultGroups = notNull("defaultGroups", defaultGroups);
    }

    ApplicationKey key() {
        return key;
    }

    Set<Group> groups() {
        return groups;
    }

    Set<Group> defaultGroups() {
        return defaultGroups;
    }

    ApplicationRole withGroups(Iterable<Group> groups, Iterable<Group> defaultGroups) {
        containsNoNulls("groups", groups);
        containsNoNulls("defaultGroups", groups);

        ImmutableSet<Group> newGroups = ImmutableSet.copyOf(groups);
        ImmutableSet<Group> newDefaults = ImmutableSet.copyOf(defaultGroups);
        if (!newGroups.containsAll(newDefaults)) {
            throw new MigrationFailedException(String.format("Application Role %s is invalid. Trying to set invalid groups %s as default.",
                    key, Sets.difference(newDefaults, newGroups)));
        }

        return new ApplicationRole(key, newGroups, newDefaults);
    }

    ApplicationRole addGroup(Group group) {
        notNull("group", group);

        if (groups.contains(group)) {
            return this;
        }

        ImmutableSet<Group> newGroups = ImmutableSet.<Group>builder().addAll(groups).add(group).build();
        return withGroups(newGroups, defaultGroups);
    }

    ApplicationRole addGroups(final Iterable<Group> newGroups) {
        notNull("newGroups", newGroups);
        ImmutableSet<Group> mergedGroups = ImmutableSet.<Group>builder().addAll(groups).addAll(newGroups).build();
        return withGroups(mergedGroups, defaultGroups);
    }

    ApplicationRole addGroupsAsDefault(final Iterable<Group> newDefaults) {
        notNull("newDefaults", newDefaults);

        ImmutableSet<Group> mergedGroups = ImmutableSet.<Group>builder().addAll(groups).addAll(newDefaults).build();
        ImmutableSet<Group> mergedDefaults = ImmutableSet.<Group>builder().addAll(defaultGroups).addAll(newDefaults).build();

        return withGroups(mergedGroups, mergedDefaults);
    }

    ApplicationRole addGroupAsDefault(Group group) {
        notNull("group", group);

        if (groups.contains(group) && defaultGroups.contains(group)) {
            return this;
        }

        ImmutableSet<Group> newGroups = ImmutableSet.<Group>builder().addAll(groups).add(group).build();
        ImmutableSet<Group> newDefaults = ImmutableSet.<Group>builder().addAll(defaultGroups).add(group).build();

        return withGroups(newGroups, newDefaults);
    }

    ApplicationRole removeFromDefaults(final Group group) {
        notNull("group", group);

        if (!groups.contains(group) || !defaultGroups.contains(group)) {
            return this;
        }

        return withGroups(groups, Iterables.filter(defaultGroups, g -> !g.equals(group)));
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ApplicationRole that = (ApplicationRole) o;
        return key.equals(that.key) && groups.equals(that.groups) && defaultGroups.equals(that.defaultGroups);
    }

    @Override
    public int hashCode() {
        int result = key.hashCode();
        result = 31 * result + groups.hashCode();
        result = 31 * result + defaultGroups.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("key", key)
                .append("groups", groups)
                .append("defaultGroups", defaultGroups)
                .toString();
    }
}
