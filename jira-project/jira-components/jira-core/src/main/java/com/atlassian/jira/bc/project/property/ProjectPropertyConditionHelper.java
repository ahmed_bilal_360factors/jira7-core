package com.atlassian.jira.bc.project.property;

import com.atlassian.jira.entity.property.AbstractEntityPropertyConditionHelper;
import com.atlassian.jira.project.Project;

public final class ProjectPropertyConditionHelper extends AbstractEntityPropertyConditionHelper<Project> {

    public ProjectPropertyConditionHelper(ProjectPropertyService projectPropertyService) {
        super(projectPropertyService, Project.class, "project");
    }

}
