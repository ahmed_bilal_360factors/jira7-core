package com.atlassian.jira.web.action.user;

import com.atlassian.crowd.exception.runtime.CommunicationException;
import com.atlassian.crowd.exception.runtime.OperationFailedException;
import com.atlassian.jira.bc.security.login.LoginResult;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.plugin.user.PasswordPolicyManager;
import com.atlassian.jira.plugin.user.WebErrorMessage;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.web.action.admin.user.PasswordChangeService;
import com.opensymphony.util.TextUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

public class ChangePassword extends JiraWebActionSupport {
    private String current;
    private String password;
    private String confirm;
    private String username;

    private final UserUtil userUtil;
    private final UserManager userManager;
    private final PasswordPolicyManager passwordPolicyManager;
    private final List<WebErrorMessage> passwordErrors = new ArrayList<WebErrorMessage>();
    private final LoginService loginService;
    private final PasswordChangeService passwordChangeService;

    public ChangePassword(final UserUtil userUtil, final UserManager userManager,
                          final PasswordPolicyManager passwordPolicyManager, final LoginService loginService,
                          final PasswordChangeService passwordChangeService) {
        this.userUtil = userUtil;
        this.userManager = userManager;
        this.passwordPolicyManager = passwordPolicyManager;
        this.loginService = loginService;
        this.passwordChangeService = passwordChangeService;
    }

    public String doDefault() throws Exception {
        final ApplicationUser current = getLoggedInUser();

        if (current == null || !current.getUsername().equals(username)) {
            return ERROR;
        }
        if (!userManager.userCanUpdateOwnDetails(current)) {
            addErrorMessage(getText("editprofile.not.allowed"));
            return ERROR;
        }

        return super.doDefault();
    }

    protected void doValidation() {
        final ApplicationUser user = notNull("user", getLoggedInUser());
        if (user == null) {
            addErrorMessage(getText("changepassword.could.not.find.user"));
            return;
        }
        if (!userManager.userCanUpdateOwnDetails(user)) {
            addErrorMessage(getText("editprofile.not.allowed"));
            return;
        }

        try {
            final LoginResult loginResult = loginService.authenticate(user, current);
            switch (loginResult.getReason()) {
                case OK:
                    break;
                case AUTHENTICATION_DENIED:
                    // the login service will not even acquire information whether the login is correct or not,
                    // if the elevated security check is triggered.
                    addErrorMessage(getText("changepassword.elevated.authorisation.required"));
                    break;
                case AUTHORISATION_FAILED:
                    addErrorMessage(getText("changepassword.could.not.find.user"));
                    break;
                case AUTHENTICATED_FAILED:
                    addError("current", getText("changepassword.current.password.incorrect"));
                    break;
            }
        } catch (CommunicationException communicationException) {
            log.debug("Error communicating with remote user directory.", communicationException);
            // Network Error trying to communicate with remote Crowd or LDAP server
            addErrorMessage(getText("login.error.communication"));
        } catch (OperationFailedException operationFailedException) {
            log.debug("Internal error occurred while authorising current user in the user directory.", operationFailedException);
            addError("current", getText("login.error.misc"));
        } catch (Exception e) {
            log.debug("Exception occurred while trying to authorise current user.", e);
            // there was a problem accessing the user data:
            addErrorMessage(getText("changepassword.could.not.find.user"));
        }

        if (!TextUtils.stringSet(password)) {
            addError("password", getText("changepassword.new.password.required"));
        } else if (!password.equals(confirm)) {
            addError("confirm", getText("changepassword.new.password.confirmation.does.not.match"));
        } else {
            final Collection<WebErrorMessage> messages = passwordPolicyManager.checkPolicy(user, current, password);
            if (!messages.isEmpty()) {
                addError("password", getText("changepassword.new.password.rejected"));
                for (WebErrorMessage message : messages) {
                    passwordErrors.add(message);
                }
            }
        }
    }

    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        final ApplicationUser currentUser = getLoggedInUser();
        if (currentUser == null || !currentUser.getName().equals(username)) {
            return ERROR;
        }

        passwordChangeService.setPassword(this, currentUser, password);
        if (invalidInput()) {
            return ERROR;
        }
        return returnComplete();
    }

    public boolean canUpdateUserPassword() {
        return userManager.canUpdateUserPassword(getLoggedInUser());
    }

    public String doSuccess() {
        return SUCCESS;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<WebErrorMessage> getPasswordErrors() {
        return passwordErrors;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean getElevatedSecurityCheckRequired() {
        return loginService.getLoginInfo(this.username).isElevatedSecurityCheckRequired();
    }
}
