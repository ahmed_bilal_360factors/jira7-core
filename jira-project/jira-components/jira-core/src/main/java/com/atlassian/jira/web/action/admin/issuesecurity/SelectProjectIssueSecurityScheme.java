package com.atlassian.jira.web.action.admin.issuesecurity;

import com.atlassian.jira.issue.security.AssignIssueSecuritySchemeCommand;
import com.atlassian.jira.issue.security.AssignIssueSecuritySchemeTaskContext;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.scheme.AbstractSelectProjectScheme;
import com.atlassian.jira.scheme.SchemeManager;
import com.atlassian.jira.task.TaskDescriptor;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.sal.api.websudo.WebSudoRequired;

@WebSudoRequired
public class SelectProjectIssueSecurityScheme extends AbstractSelectProjectScheme {
    private final IssueSecuritySchemeManager issueSecuritySchemeManager;
    private final TaskManager taskManager;

    private TaskDescriptor<AssignIssueSecuritySchemeCommand.AssignSecurityLevelResult> currentTaskDescriptor;

    public SelectProjectIssueSecurityScheme(IssueSecuritySchemeManager issueSecuritySchemeManager, TaskManager taskManager) {
        this.issueSecuritySchemeManager = issueSecuritySchemeManager;
        this.taskManager = taskManager;
    }

    @Override
    public String doDefault() throws Exception {
        final TaskDescriptor<AssignIssueSecuritySchemeCommand.AssignSecurityLevelResult> taskDescriptor = getCurrentTaskDescriptor();
        if (taskDescriptor != null) {
            return getRedirect(taskDescriptor.getProgressURL());
        }
        return super.doDefault();
    }

    private TaskDescriptor<AssignIssueSecuritySchemeCommand.AssignSecurityLevelResult> getCurrentTaskDescriptor() {
        if (currentTaskDescriptor == null) {
            currentTaskDescriptor = taskManager.getLiveTask(new AssignIssueSecuritySchemeTaskContext(getProject()));
        }
        return currentTaskDescriptor;
    }

    @Override
    public SchemeManager getSchemeManager() {
        return issueSecuritySchemeManager;
    }
}
