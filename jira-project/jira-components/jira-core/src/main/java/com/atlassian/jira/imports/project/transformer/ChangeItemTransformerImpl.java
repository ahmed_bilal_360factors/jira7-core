package com.atlassian.jira.imports.project.transformer;

import com.atlassian.jira.external.beans.ExternalChangeItem;
import com.atlassian.jira.imports.project.customfield.ExternalCustomFieldValueImpl;
import com.atlassian.jira.imports.project.customfield.ProjectCustomFieldImporter;
import com.atlassian.jira.imports.project.customfield.ProjectImportableCustomField;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.IssueContextImpl;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.google.common.annotations.VisibleForTesting;

import java.util.Collection;

/**
 * @since v3.13
 */
public class ChangeItemTransformerImpl implements ChangeItemTransformer {
    private final String mappedProjectId;
    private final CustomFieldManager customFieldManager;

    public ChangeItemTransformerImpl(String mappedProjectId, CustomFieldManager customFieldManager) {
        this.mappedProjectId = mappedProjectId;
        this.customFieldManager = customFieldManager;
    }

    public ExternalChangeItem transform(final ProjectImportMapper projectImportMapper, final ExternalChangeItem changeItem) {
        final String newChangeGroupId = projectImportMapper.getChangeGroupMapper().getMappedId(changeItem.getChangeGroupId());
        String mappedOldValue = null;
        String mappedNewValue = null;
        if (newChangeGroupId != null && changeItem.getFieldType().equals(ChangeItemBean.STATIC_FIELD) && changeItem.getField().equals("status")) {
            // map status ids
            mappedOldValue = projectImportMapper.getStatusMapper().getMappedId(changeItem.getOldValue());
            mappedNewValue = projectImportMapper.getStatusMapper().getMappedId(changeItem.getNewValue());
        }
        if (newChangeGroupId != null && changeItem.getFieldType().equals(ChangeItemBean.CUSTOM_FIELD)) {

            // map custom field ids
            CustomField importableCustomField = getImportableCustomFieldByName(changeItem.getField());
            if (importableCustomField != null) {
                ProjectCustomFieldImporter customFieldImporter = ((ProjectImportableCustomField) importableCustomField.getCustomFieldType()).getProjectImporter();

                // Get the relevantConfig
                String mappedIssueId = null; // hard to get issue id at this point, i guess we could store changeGroupId->issueId mapping and then access it here to get the issue id
                FieldConfig relevantConfig = importableCustomField.getRelevantConfig(new IssueContextImpl(Long.valueOf(mappedProjectId), mappedIssueId));

                // create customFieldValue with a dummy id of -1
                ExternalCustomFieldValueImpl customFieldValue = new ExternalCustomFieldValueImpl("-1", importableCustomField.getId(), mappedIssueId);

                // transform old id
                mappedOldValue = mapCustomFieldIdList(customFieldValue, changeItem.getOldValue(), customFieldImporter, projectImportMapper, relevantConfig);
                // transform new id
                mappedNewValue = mapCustomFieldIdList(customFieldValue, changeItem.getNewValue(), customFieldImporter, projectImportMapper, relevantConfig);
            }
        }
        return new ExternalChangeItem(null, newChangeGroupId, changeItem.getFieldType(), changeItem.getField(),
                mappedOldValue != null ? mappedOldValue : changeItem.getOldValue(),
                changeItem.getOldString(),
                mappedNewValue != null ? mappedNewValue : changeItem.getNewValue(),
                changeItem.getNewString());
    }

    private String mapCustomFieldIdList(ExternalCustomFieldValueImpl customFieldValue, String unmappedValueList, ProjectCustomFieldImporter customFieldImporter, ProjectImportMapper projectImportMapper, FieldConfig relevantConfig) {
        if (unmappedValueList == null) {
            return null;
        } else {
            StringBuffer mappedValueList = new StringBuffer();
            for (String unmappedValue : unmappedValueList.split(",")) {
                unmappedValue = unmappedValue.trim();
                customFieldValue.setStringValue(unmappedValue);
                ProjectCustomFieldImporter.MappedCustomFieldValue mappedCustomFieldOldValue = customFieldImporter.getMappedImportValue(projectImportMapper, customFieldValue, relevantConfig);
                String mappedValue = mappedCustomFieldOldValue.getValue();
                if (mappedValueList.length() > 0) {
                    mappedValueList.append(", ");
                }
                if (mappedValue != null && mappedValue.length() > 0) {
                    mappedValueList.append(mappedValue);
                } else {
                    mappedValueList.append(unmappedValue);
                }
            }
            return mappedValueList.toString();
        }
    }

    @VisibleForTesting
    CustomField getImportableCustomFieldByName(String customFieldName) {
        Collection<CustomField> customFields = customFieldManager.getCustomFieldObjectsByName(customFieldName);
        CustomField importableCustomField = null;
        for (CustomField customField : customFields) {
            if (customField.getCustomFieldType() instanceof ProjectImportableCustomField) {
                if (importableCustomField != null) {
                    return null; // we found more than one importable field, so to avoid unpredictable behaviour we return null
                }
                importableCustomField = customField;
            }
        }
        return importableCustomField;
    }
}
