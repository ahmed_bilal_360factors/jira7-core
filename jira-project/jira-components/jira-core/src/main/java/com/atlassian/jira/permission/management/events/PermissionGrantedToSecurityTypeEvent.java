package com.atlassian.jira.permission.management.events;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.jira.event.AbstractEvent;
import com.google.common.base.Objects;

/**
 * Event fired to measure permission types usage (grant operation)
 */
@EventName("jira.projectpermissions.grant.details")
public class PermissionGrantedToSecurityTypeEvent extends AbstractEvent {
    private final Long schemeId;
    private final String permissionKey;
    private final String grantedTo;

    public PermissionGrantedToSecurityTypeEvent(
            final Long schemeId,
            final String permissionKey,
            final String grantedTo
    ) {
        this.schemeId = schemeId;
        this.permissionKey = permissionKey;
        this.grantedTo = grantedTo;
    }

    public Long getSchemeId() {
        return schemeId;
    }

    public String getPermissionKey() {
        return permissionKey;
    }

    public String getGrantedTo() {
        return grantedTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PermissionGrantedToSecurityTypeEvent that = (PermissionGrantedToSecurityTypeEvent) o;
        return Objects.equal(schemeId, that.schemeId) &&
                Objects.equal(permissionKey, that.permissionKey) &&
                Objects.equal(grantedTo, that.grantedTo);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), schemeId, permissionKey, grantedTo);
    }

    @Override
    public String toString() {
        return "PermissionGrantedToSecurityTypeEvent{" +
                "schemeId=" + schemeId +
                ", permissionKey='" + permissionKey + '\'' +
                ", grantedTo='" + grantedTo + '\'' +
                '}';
    }
}
