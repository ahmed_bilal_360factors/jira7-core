package com.atlassian.velocity;

import org.apache.velocity.util.introspection.ClassMap;
import org.apache.velocity.util.introspection.MethodMap;

import javax.annotation.Nonnull;
import java.lang.reflect.Method;

/**
 * Implementation of ClassMap that uses JiraMethodMap for methods resolution.
 *
 * @since v6.4
 */
class JiraClassMap implements ClassMap {

    private final JiraMethodMap methodMap;

    public JiraClassMap(@Nonnull final Class<?> clazz) {
        this.methodMap = new JiraMethodMap(clazz);
    }

    @Override
    public Method findMethod(final String name, final Object[] params) throws MethodMap.AmbiguousException {
        return methodMap.find(name, params);
    }
}
