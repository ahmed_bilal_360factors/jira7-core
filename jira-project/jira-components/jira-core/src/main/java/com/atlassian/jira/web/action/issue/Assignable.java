package com.atlassian.jira.web.action.issue;

import com.atlassian.jira.project.Project;

public interface Assignable {
    public String getAssignee();

    public void setAssignee(String assignee);

    public Project getAssignIn() throws Exception;
}
