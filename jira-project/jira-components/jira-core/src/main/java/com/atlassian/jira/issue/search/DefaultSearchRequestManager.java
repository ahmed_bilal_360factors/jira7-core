package com.atlassian.jira.issue.search;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutManager;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutStorageException;
import com.atlassian.jira.issue.subscription.SubscriptionManager;
import com.atlassian.jira.sharing.IndexableSharedEntity;
import com.atlassian.jira.sharing.ShareManager;
import com.atlassian.jira.sharing.SharedEntity;
import com.atlassian.jira.sharing.SharedEntity.TypeDescriptor;
import com.atlassian.jira.sharing.index.SharedEntityIndexer;
import com.atlassian.jira.sharing.search.SharedEntitySearchParameters;
import com.atlassian.jira.sharing.search.SharedEntitySearchResult;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.Resolver;
import com.atlassian.jira.util.Visitor;
import com.atlassian.jira.util.collect.CollectionEnclosedIterable;
import com.atlassian.jira.util.collect.CollectionUtil;
import com.atlassian.jira.util.collect.EnclosedIterable;
import com.atlassian.jira.util.collect.Transformed;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.query.Query;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.atlassian.collectors.CollectorsUtil.toImmutableListWithSizeOf;
import static com.atlassian.jira.util.collect.EnclosedIterable.Functions.toList;
import static com.atlassian.jira.util.collect.Transformed.enclosedIterable;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

public class DefaultSearchRequestManager implements SearchRequestManager {
    private final ColumnLayoutManager columnLayoutManager;
    private final SubscriptionManager subscriptionManager;
    private final ShareManager shareManager;
    private final SearchRequestStore searchRequestStore;
    private final SharedEntityIndexer indexer;
    private final SearchService searchService;

    /**
     * Used to set the share permissions on a SharedEntity.
     */
    private final Resolver<IndexableSharedEntity<SearchRequest>, SharedEntity> sharedEntityPermissionsResolver = this::setSharePermissions;

    public DefaultSearchRequestManager(final ColumnLayoutManager columnLayoutManager,
                                       final SubscriptionManager subscriptionManager, final ShareManager shareManager,
                                       final SearchRequestStore searchRequestStore, final SharedEntityIndexer indexer,
                                       final SearchService searchService) {
        this.columnLayoutManager = notNull("columnLayoutManager", columnLayoutManager);
        this.subscriptionManager = notNull("subscriptionManager", subscriptionManager);
        this.shareManager = notNull("shareManager", shareManager);
        this.searchRequestStore = notNull("searchRequestStore", searchRequestStore);
        this.indexer = notNull("indexer", indexer);
        this.searchService = notNull("searchService", searchService);
    }

    @Override
    public EnclosedIterable<SearchRequest> get(final RetrievalDescriptor descriptor) {
        return enclosedIterable(searchRequestStore.get(descriptor), this::setSharePermissions);
    }

    @Override
    public EnclosedIterable<SearchRequest> get(final ApplicationUser user, final RetrievalDescriptor descriptor) {
        final Function<SearchRequest, SearchRequest> transformer = createPermissionSanitisingResolver(user);
        return Transformed.enclosedIterable(searchRequestStore.get(descriptor), new com.atlassian.jira.util.Function<SearchRequest, SearchRequest>() {
            @Override
            public SearchRequest get(SearchRequest input) {
                return transformer.apply(input);
            }
        });
    }

    @Override
    public EnclosedIterable<SearchRequest> getSearchRequests(final ApplicationUser user, final RetrievalDescriptor descriptor) {
        return CollectionEnclosedIterable.from(
                toList(searchRequestStore.get(descriptor))
                        .stream()
                        .map(createPermissionSanitisingResolver(user))
                        .filter(sr -> isSharedWith(sr, user)).collect(Collectors.toList()));
    }

    @Override
    public EnclosedIterable<SearchRequest> getAll() {
        return enclosedIterable(searchRequestStore.getAll(), this::setSharePermissions);
    }

    @Override
    public void visitAll(Visitor<SearchRequestEntity> visitor) {
        searchRequestStore.visitAll(visitor);
    }

    @Override
    public EnclosedIterable<SharedEntity> getAllIndexableSharedEntities() {
        return enclosedIterable(searchRequestStore.getAllIndexableSharedEntities(), sharedEntityPermissionsResolver);
    }

    @Override
    public Collection<SearchRequest> getAllOwnedSearchRequests(final ApplicationUser user) {
        final Collection<SearchRequest> searchRequests = searchRequestStore.getAllOwnedSearchRequests(user);
        if (searchRequests == null) {
            return Collections.emptyList();
        }
        return searchRequests.stream().map(createPermissionSanitisingResolver(user)).collect(toImmutableListWithSizeOf(searchRequests));
    }

    @Override
    public SearchRequest getOwnedSearchRequestByName(final ApplicationUser author, final String name) {
        Assertions.notNull("name", name);

        final SearchRequest searchRequest = searchRequestStore.getRequestByAuthorAndName(author, name);
        return setSanitisedQuery(author, setSharePermissions(searchRequest));
    }

    @Override
    public SearchRequest getSearchRequestById(final ApplicationUser user, final Long id) {
        Assertions.notNull("id", id);

        final SearchRequest searchRequest = searchRequestStore.getSearchRequest(id);

        if ((searchRequest != null) && isSharedWith(searchRequest, user)) {
            return setSanitisedQuery(user, setSharePermissions(searchRequest));
        }
        return null;
    }

    @Override
    public SearchRequest getSearchRequestById(final Long id) {
        Assertions.notNull("id", id);

        final SearchRequest searchRequest = searchRequestStore.getSearchRequest(id);

        if (searchRequest != null) {
            return setSharePermissions(searchRequest);
        }
        return null;
    }

    @Override
    public List<SearchRequest> findByNameIgnoreCase(String name) {
        List<SearchRequest> searchRequests = searchRequestStore.findByNameIgnoreCase(name);
        for (SearchRequest searchRequest : searchRequests) {
            setSharePermissions(searchRequest);
        }
        return searchRequests;
    }

    @Override
    public String getSearchRequestOwnerUserName(final Long id) {
        Assertions.notNull("id", id);

        final SearchRequest searchRequest = searchRequestStore.getSearchRequest(id);
        if (searchRequest != null) {
            return searchRequest.getOwner() == null ? null : searchRequest.getOwner().getUsername();
        }
        return null;
    }

    @Override
    public ApplicationUser getSearchRequestOwner(final Long id) {
        Assertions.notNull("id", id);

        final SearchRequest searchRequest = searchRequestStore.getSearchRequest(id);
        if (searchRequest != null) {
            return searchRequest.getOwner();
        }
        return null;
    }

    @Override
    public SearchRequest create(final SearchRequest request) {
        validateSearchRequestCreate(request);

        final SearchRequest searchRequest = searchRequestStore.create(request);
        searchRequest.setPermissions(request.getPermissions());
        searchRequest.setPermissions(shareManager.updateSharePermissions(searchRequest));
        indexer.index(searchRequest).await();
        return setSanitisedQuery(searchRequest.getOwner(), searchRequest);
    }

    @Override
    public SearchRequest update(final SearchRequest request) {
        validateSearchRequestUpdate(request);

        final SearchRequest searchRequest = searchRequestStore.update(request);
        searchRequest.setPermissions(shareManager.updateSharePermissions(request));
        indexer.index(searchRequest).await();
        return setSanitisedQuery(searchRequest.getOwner(), searchRequest);
    }

    @Override
    public void delete(final Long id) {
        final SearchRequest searchRequest = searchRequestStore.getSearchRequest(notNull("id", id));
        if (searchRequest != null) {
            deleteSearchRequest(searchRequest);
        }
    }

    private void deleteSearchRequest(final SearchRequest searchRequest) {
        try {
            CollectionUtil.foreach(subscriptionManager.getAllFilterSubscriptions(searchRequest.getId()), subscription -> {
                try {
                    subscriptionManager.deleteSubscription(subscription.getId());
                } catch (final Exception e) {
                    throw new DataAccessException("Error occurred while deleting searchRequest.", e);
                }
            });
        } catch (final Exception e) {
            throw new DataAccessException("Error occurred while deleting searchRequest.", e);
        }

        try {
            // Check if the search request has Column Layout items
            if (columnLayoutManager.hasColumnLayout(searchRequest)) {
                // If so remove it (the restore method actually removes the column layout)
                columnLayoutManager.restoreSearchRequestColumnLayout(searchRequest);
            }
        } catch (final ColumnLayoutStorageException e) {
            throw new DataAccessException("Error occurred while deleting searchRequest.", e);
        }

        // remove all the shares.
        shareManager.deletePermissions(searchRequest);

        // Delete the actual searchRequest
        searchRequestStore.delete(searchRequest.getId());

        // deindex
        indexer.deIndex(searchRequest).await();
    }

    @Override
    public TypeDescriptor<SearchRequest> getType() {
        return SearchRequest.ENTITY_TYPE;
    }

    @Override
    public void adjustFavouriteCount(final SharedEntity entity, final int adjustmentValue) {
        Assertions.notNull("entity", entity);
        Assertions.equals("SearchRequestManager can only adjust favourite counts for Search Requests.", SearchRequest.ENTITY_TYPE,
                entity.getEntityType());

        final SearchRequest searchRequest = searchRequestStore.adjustFavouriteCount(entity.getId(), adjustmentValue);
        setSharePermissions(searchRequest);
        indexer.index(searchRequest).await();
    }

    /**
     * Sanitises the filter's {@link com.atlassian.query.Query} (if set and if necessary).
     *
     * @param searcher the user to sanitise for
     * @param filter   the search request
     * @return the input search request with the search query modified.
     */
    SearchRequest setSanitisedQuery(final ApplicationUser searcher, final SearchRequest filter) {
        if (filter != null) {
            final Query query = filter.getQuery();
            final Query sanitisedQuery = searchService.sanitiseSearchQuery(searcher, query);
            if (!query.equals(sanitisedQuery)) {
                final boolean isModified = filter.isModified();
                filter.setQuery(sanitisedQuery);
                filter.setModified(isModified);
            }
        }
        return filter;
    }

    private boolean isSharedWith(final SearchRequest entity, final ApplicationUser user) {
        return shareManager.isSharedWith(user, entity);
    }

    private SearchRequest setSharePermissions(final SearchRequest filter) {
        if (filter != null) {
            filter.setPermissions(shareManager.getSharePermissions(filter));
        }
        return filter;
    }

    private SharedEntity setSharePermissions(final IndexableSharedEntity<SearchRequest> entity) {
        if (entity != null) {
            entity.setPermissions(shareManager.getSharePermissions(entity));
        }
        return entity;
    }

    private void validateSearchRequestCreate(final SearchRequest request) {
        Assertions.notNull("request", request);
        Assertions.notNull("request.owner.user.name", request.getOwner() == null ? null : request.getOwner().getUsername());
        Assertions.notNull("request.name", request.getName());
    }

    private void validateSearchRequestUpdate(final SearchRequest request) {
        validateSearchRequestCreate(request);
        Assertions.notNull("request.id", request.getId());
    }

    @Override
    public SharedEntitySearchResult<SearchRequest> search(final SharedEntitySearchParameters searchParameters, final ApplicationUser user, final int pagePosition, final int pageWidth) {
        Assertions.notNull("searchParameters", searchParameters);
        Assertions.not("pagePosition < 0", pagePosition < 0);
        Assertions.not("pageWidth <= 0", pageWidth <= 0);

        return indexer.getSearcher(SearchRequest.ENTITY_TYPE).search(searchParameters, user, pagePosition, pageWidth);
    }

    @Override
    public SearchRequest getSharedEntity(final Long entityId) {
        Assertions.notNull("entityId", entityId);
        return setSharePermissions(searchRequestStore.getSearchRequest(entityId));
    }

    @Override
    public SearchRequest getSharedEntity(final ApplicationUser user, final Long entityId) {
        Assertions.notNull("entityId", entityId);
        return getSearchRequestById(user, entityId);
    }

    @Override
    public boolean hasPermissionToUse(final ApplicationUser user, final SearchRequest entity) {
        Assertions.notNull("entity", entity);
        Assertions.equals("SearchRequestManager can only adjust favourite counts for Search Requests.", SearchRequest.ENTITY_TYPE,
                entity.getEntityType());
        return shareManager.isSharedWith(user, entity);
    }

    private Function<SearchRequest, SearchRequest> createPermissionSanitisingResolver(final ApplicationUser searcher) {
        return request -> setSanitisedQuery(searcher, setSharePermissions(request));
    }
}
