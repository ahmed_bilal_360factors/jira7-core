package com.atlassian.jira.web.action.admin.instrumentation.cache;

import com.atlassian.instrumentation.caches.CacheKeys;
import com.atlassian.jira.instrumentation.CacheStatistics;
import com.atlassian.jira.instrumentation.Statistics;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Used to calculate averages over a number of metrics concurrently.
 *
 * @since v7.1.0
 */
public class CacheStatsAverager {
    private String name;
    private long hits;
    private long puts;
    private long misses;
    private long removes;
    private long loads;
    private double loadTime;

    private long total = 0;
    private long count = 1;

    public CacheStatsAverager(@Nonnull String name, long hits, long puts, long misses, long remove, final long loads, double loadTime) {
        this.name = requireNonNull(name);
        this.hits = hits;
        this.puts = puts;
        this.misses = misses;
        this.removes = remove;
        this.loads = loads;
        this.loadTime = loadTime;
    }

    public static CacheStatsAverager build(@Nonnull final String name, @Nonnull CacheStatistics stats) {
        requireNonNull(name, "name");
        requireNonNull(stats, "stats");
        return new CacheStatsAverager(name,
                (long) stats.getStatsMap().getOrDefault(CacheKeys.HITS.getName(), 0L),
                (long) stats.getStatsMap().getOrDefault(CacheKeys.PUTS.getName(), 0L),
                (long) stats.getStatsMap().getOrDefault(CacheKeys.MISSES.getName(), 0L),
                (long) stats.getStatsMap().getOrDefault(CacheKeys.REMOVES.getName(), 0L),
                (long) stats.getStatsMap().getOrDefault(CacheKeys.LOADS.getName(), 0L),
                (double) stats.getStatsMap().getOrDefault(CacheKeys.LOAD_TIME.getName(), 0.0));
    }

    /**
     * Returns a new {@link CacheStatsAverager} with the actual stats added together.
     *
     * @return A new {@link CacheStatsAverager}
     */
    public CacheStatsAverager addAndSetCount(CacheStatsAverager b, long count) {
        CacheStatsAverager result = new CacheStatsAverager(this.getName(),
                this.getHits() + b.getHits(),
                this.getPuts() + b.getPuts(),
                this.getMisses() + b.getMisses(),
                this.getRemoves() + b.getRemoves(),
                this.getLoads() + b.getLoads(),
                this.getLoadTime() + b.getLoadTime());
        result.setCount(count);
        return result;
    }

    public double getAverageHits() {
        if (count != 0) {
            return (double) hits / (double) count;
        } else {
            return 0.0;
        }
    }

    public double getAveragePuts() {
        if (count != 0) {
            return (double) puts / (double) count;
        } else {
            return 0.0;
        }
    }

    public double getAverageMisses() {
        if (count != 0) {
            return (double) misses / (double) count;
        } else {
            return 0.0;
        }
    }

    public double getAverageRemoves() {
        if (count != 0) {
            return (double) removes / (double) count;
        } else {
            return 0.0;
        }
    }

    public double getAverageLoadTime() {
        if (misses != 0) {
            return loadTime / (double) misses;
        } else {
            return 0.0;
        }
    }

    public double getAverageLoads() {
        if (count != 0) {
            return (double) loads / (double) count;
        }
        return 0.0;
    }

    public double getHitRate() {
        if (getMisses() + getHits() == 0) {
            return 0;
        }
        return (double) getHits() / (double) (getHits() + getMisses()) * 100;
    }

    public String getName() {
        return name;
    }

    public void setName(@Nonnull final String name) {
        this.name = requireNonNull(name);
    }

    public long getHits() {
        return hits;
    }

    public void setHits(final long hits) {
        this.hits = hits;
    }

    public long getPuts() {
        return puts;
    }

    public void setPuts(final long puts) {
        this.puts = puts;
    }

    public long getMisses() {
        return misses;
    }

    public void setMisses(final long misses) {
        this.misses = misses;
    }

    public long getRemoves() {
        return removes;
    }

    public void setRemoves(final long removes) {
        this.removes = removes;
    }

    public long getLoads() {
        return loads;
    }

    public double getLoadTime() {
        return loadTime;
    }

    public void setLoadTime(final double loadTime) {
        this.loadTime = loadTime;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(final long total) {
        this.total = total;
    }

    public long getCount() {
        return count;
    }

    public void setCount(final long count) {
        this.count = count;
    }
}
