package com.atlassian.jira.issue;

public class SeparatorSelectedException extends Exception {
    public SeparatorSelectedException() {
    }

    public SeparatorSelectedException(String message) {
        super(message);
    }
}
