package com.atlassian.jira.propertyset;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.bc.dataimport.DatabaseImportCompletedEvent;
import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.QueryCallback;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.database.SqlCallback;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.model.querydsl.JiraRelationalPathBase;
import com.atlassian.jira.propertyset.OfBizPropertyTypeRegistry.TypeMapper;
import com.atlassian.jira.tenancy.TenantAware;
import com.atlassian.jira.tenancy.TenantInfo;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.opensymphony.module.propertyset.PropertyImplementationException;
import com.querydsl.core.QueryException;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.Path;
import org.apache.commons.jexl.context.HashMapContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import static com.atlassian.jira.model.querydsl.QOSPropertyEntry.O_S_PROPERTY_ENTRY;
import static com.atlassian.jira.model.querydsl.QOSPropertyText.O_S_PROPERTY_TEXT;
import static com.atlassian.jira.propertyset.OfBizPropertyTypeRegistry.mapper;
import static com.google.common.base.Throwables.propagateIfInstanceOf;
import static com.opensymphony.module.propertyset.PropertySet.TEXT;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.defaultString;

/**
 * Caching implementation of property set entries.
 * <p>
 * Concurrency note: To minimize data inconsistency, mutative operations are (mostly) transactional.  To prevent
 * deadlocks, modifications are made to the entry first, then the value.  Modifications to entries use the row
 * count to determine whether they were successful or should be counted as overwritten.  That is, any time we
 * expect to update or delete a row, we include the type in the request.  If the row does not match, then we
 * abort that attempt.
 * </p>
 *
 * @since v6.2
 */
@EventComponent
@ParametersAreNonnullByDefault
@TenantInfo(
        value = TenantAware.UNRESOLVED,
        comment = "This is TENANTED data that should eventually use a RequestCache, but the way that these are " +
                "currently accessed, such as reading application properties or plugin settings during system " +
                "startup, make it impossible to deliver a RequestCache-based solution at this time.")
public class CachingOfBizPropertyEntryStore implements OfBizPropertyEntryStore {
    private static final Logger LOG = LoggerFactory.getLogger(CachingOfBizPropertyEntryStore.class);

    /**
     * Maximum number of value entries to attempt to remove at once.
     */
    static final int BATCH_SIZE = 900;

    /**
     * Number of attempts to make for a set operation before giving up on it.
     * <p>
     * Setting the value for a property set is supposed to be an idempotent {@code UPSERT} operation.
     * Since this involves the non-atomic test-and-set of a {@code SELECT} followed by an {@code UPDATE}
     * that may return {@code 0} rows if there is a concurrent modification.  Note that concurrent {@code INSERT}
     * does <strong>NOT</strong> throw an exception, because there is no unique constraint across the
     * (entity_name, entity_id, property_key) tuple, so exceptions surface immediately without a retry.
     * Concurrent updates should be pretty rare, but we'll try to deal with it, anyway.
     * </p>
     */
    private static final int MAX_ATTEMPTS = 5;

    private final QueryDslAccessor queryDslAccessor;

    @TenantInfo(value = TenantAware.UNRESOLVED, comment = "See class level annotation")
    private final Cache<CacheKey, PropertySetData> cache;

    public CachingOfBizPropertyEntryStore(final QueryDslAccessor queryDslAccessor, final CacheManager cacheManager) {
        this.queryDslAccessor = requireNonNull(queryDslAccessor, "queryDslAccessor");
        this.cache = cacheManager.getCache(
                CachingOfBizPropertyEntryStore.class.getName() + ".cache",
                this::loadPropertySetData,
                new CacheSettingsBuilder()
                        .expireAfterAccess(1, TimeUnit.DAYS)
                        .flushable()
                        .build());
    }

    private PropertySetData resolve(final String entityName, final long entityId) {
        final CacheKey cacheKey = new CacheKey(entityName, entityId);
        try {
            return cache.get(cacheKey);
        } catch (RuntimeException re) {
            throw propEx("Unable to load values for " + cacheKey, re);
        }
    }

    @Nonnull
    @Override
    public Collection<String> getKeys(final String entityName, final long entityId) {
        return resolve(entityName, entityId).keys();
    }

    @Nonnull
    @Override
    public Collection<String> getKeys(final String entityName, final long entityId, final int type) {
        return resolve(entityName, entityId).keys(type);
    }

    @Override
    public PropertyEntry getEntry(String entityName, long entityId, String propertyKey) {
        return resolve(entityName, entityId).get(propertyKey);
    }

    @Override
    public boolean exists(String entityName, long entityId, String propertyKey) {
        return getType(entityName, entityId, propertyKey) > 0;
    }

    @Override
    public int getType(String entityName, long entityId, String propertyKey) {
        return resolve(entityName, entityId).getType(propertyKey);
    }

    @Override
    public void setEntry(final String entityName, final long entityId, final String propertyKey, final int type, final Object value) {
        try {
            retry(new SetOperation(entityName, entityId, propertyKey, type, value));
        } finally {
            // remove cache entry regardless of whether or not we succeed
            invalidateCacheEntry(entityName, entityId);
        }
    }

    @Override
    public void removeEntry(String entityName, long entityId, String propertyKey) {
        try {
            retry(new RemoveOperation(entityName, entityId, propertyKey));
        } finally {
            // remove cache entry no matter what actually happened
            invalidateCacheEntry(entityName, entityId);
        }
    }

    /**
     * Removes an entire property set.
     * <p>
     * Implementation note: There is a concurrency race here if removal of the entire property set overlaps with
     * someone else adding a new value to it or changing the type of an existing property.  The worst case result
     * is that the new value entry would get orphaned.  Removing an entire property set is pretty rare, doing so
     * concurrently with adding a new value to it should be rare squared, and as consequences go this one is pretty
     * tolerable.  As a result, this problem is deliberately ignored.  Along those lines, this is not a transactional
     * operation, either.  Exceptions during value removal are caught and logged as warnings instead of propagated.
     * </p>
     *
     * @param entityName the entity name of the property set's owner
     * @param entityId   the entity ID of the property set's owner
     */
    @Override
    public void removePropertySet(String entityName, long entityId) {
        try {
            execute(db -> {
                final List<Tuple> rows = loadIdsAndTypes(entityName, entityId);
                db.delete(O_S_PROPERTY_ENTRY)
                        .where(O_S_PROPERTY_ENTRY.entityName.eq(entityName))
                        .where(O_S_PROPERTY_ENTRY.entityId.eq(entityId))
                        .execute();
                removeValues(db, rows);
            });
        } catch (QueryException qe) {
            throw propEx(qe);
        } finally {
            invalidateCacheEntry(entityName, entityId);
        }
    }

    static void removeValues(DbConnection db, List<Tuple> idsAndTypes) {
        groupByValuePath(idsAndTypes).forEach((path, ids) -> deleteById(db, path, ids));
    }

    private static Map<JiraRelationalPathBase<?>, List<Long>> groupByValuePath(List<Tuple> idsAndTypes) {
        final Map<JiraRelationalPathBase<?>, List<Long>> idsByValueTable = new IdentityHashMap<>(4);
        idsAndTypes.forEach(idAndType -> {
            final Long id = idAndType.get(O_S_PROPERTY_ENTRY.id);
            final Integer type = idAndType.get(O_S_PROPERTY_ENTRY.type);
            try {
                final JiraRelationalPathBase<?> path = mapper(type).getValueTable();
                idsByValueTable.computeIfAbsent(path, key -> new ArrayList<>()).add(id);
            } catch (PropertyImplementationException pie) {
                LOG.warn("Ignoring property entry with invalid type: id={}, type={}", id, type);
            }
        });
        return idsByValueTable;
    }

    /**
     * Deletes a (potentially large) collection of IDs from a value store.
     * <p>
     * Database errors are logged, but otherwise ignored.
     * </p>
     *
     * @param db   the database connection to use
     * @param path the table holding the values
     * @param ids  the IDs of the values to delete
     */
    private static void deleteById(DbConnection db, JiraRelationalPathBase<?> path, Collection<Long> ids) {
        final List<Long> list = new ArrayList<>(ids);
        Lists.partition(list, BATCH_SIZE).forEach(subList -> {
            try {
                db.delete(path)
                        .where(path.getNumericIdPath().in(subList))
                        .execute();
            } catch (QueryException qe) {
                LOG.warn("Unable to remove property set values from {}: {}", path.getSchemaAndTable(), ids, qe);
            }
        });
    }

    public void invalidateCacheEntry(String entityName, long entityId) {
        cache.remove(new CacheKey(entityName, entityId));
    }

    public void refreshAll() {
        cache.removeAll();
    }

    @EventListener
    @SuppressWarnings("UnusedParameters")
    public void onClearCache(@Nullable ClearCacheEvent event) {
        refreshAll();
    }

    @EventListener
    @SuppressWarnings("UnusedParameters")
    public void onDatabaseImportCompleted(final DatabaseImportCompletedEvent event) {
        refreshAll();
    }


    private List<Tuple> loadIdsAndTypes(String entityName, long entityId) {
        return query(db -> db.newSqlQuery()
                .select(O_S_PROPERTY_ENTRY.id, O_S_PROPERTY_ENTRY.type)
                .from(O_S_PROPERTY_ENTRY)
                .where(O_S_PROPERTY_ENTRY.entityName.eq(entityName))
                .where(O_S_PROPERTY_ENTRY.entityId.eq(entityId))
                .fetch());
    }

    @VisibleForTesting
    PropertySetData loadPropertySetData(CacheKey cacheKey) {
        return new PropertySetData(cacheKey.entityName, cacheKey.entityId);
    }

    /**
     * Executes the SQL callback on a separate connection to any ongoing transaction.
     */
    private void execute(SqlCallback callback) {
        queryDslAccessor.withNewConnection().execute(callback);
    }

    /**
     * Executes the SQL query on a separate connection to any ongoing transaction.
     */
    private <T> T query(QueryCallback<T> callback) {
        return queryDslAccessor.withNewConnection().executeQuery(callback);
    }

    /**
     * Executes the SQL query on a separate connection to any ongoing transaction, ensuring that the
     * query is wrapped in a separate transaction.
     */
    private <T> T tx(QueryCallback<T> callback) {
        return query(db -> {
            db.setAutoCommit(false);
            final T result = callback.runQuery(db);
            db.commit();
            return result;
        });
    }

    private static void retry(Operation operation) {
        try {
            for (int i = 0; i < MAX_ATTEMPTS; ++i) {
                if (operation.perform()) {
                    return;
                }
            }
        } catch (RuntimeException re) {
            propagateIfInstanceOf(re, PropertyImplementationException.class);
            throw propEx("Failed operation: " + operation, re);
        }

        throw new PropertyImplementationException("Failed operation (too many retries): " + operation);
    }

    // PropertyImplementationException does not initialize its cause properly...
    private static PropertyImplementationException propEx(Throwable cause) {
        final PropertyImplementationException ex = new PropertyImplementationException(cause);
        ex.initCause(cause);
        throw ex;
    }

    private static PropertyImplementationException propEx(String message, Throwable cause) {
        final PropertyImplementationException ex = new PropertyImplementationException(message, cause);
        ex.initCause(cause);
        throw ex;
    }


    static final class CacheKey implements Serializable {
        private static final long serialVersionUID = 4729043221678305211L;

        private final String entityName;
        private final long entityId;

        CacheKey(String entityName, long entityId) {
            this.entityName = entityName;
            this.entityId = entityId;
        }

        String getEntityName() {
            return entityName;
        }

        long getEntityId() {
            return entityId;
        }

        @Override
        public boolean equals(Object o) {
            return this == o || (o instanceof CacheKey && equals((CacheKey) o));
        }

        private boolean equals(@Nonnull CacheKey other) {
            return entityId == other.entityId && entityName.equals(other.entityName);
        }

        @Override
        public int hashCode() {
            int result = entityName.hashCode();
            result = 31 * result + (int) (entityId ^ (entityId >>> 32));
            return result;
        }

        @Override
        public String toString() {
            return "CacheKey[entityName=" + entityName + ",entityId=" + entityId + ']';
        }
    }


    static class PropertyEntryImpl implements PropertyEntry {
        private final int type;
        private final Object value;

        PropertyEntryImpl(int type, @Nullable Object value) {
            this.type = type;
            this.value = value;
        }

        public int getType() {
            return type;
        }

        @Nullable
        public Object getValue() {
            if (value == null) {
                return null;
            }
            return mapper(type).getHandler().processGet(type, value);
        }

        @Nullable
        public Object getValue(final int type) {
            if (value == null) {
                return null;
            }
            return mapper(type).getHandler().processGet(type, value);
        }

        @Override
        public String toString() {
            return "PropertyEntryImpl[type=" + type + ",value=" + value + ']';
        }
    }


    @TenantInfo(TenantAware.TENANTED)
    class PropertySetData {
        private final String entityName;
        private final long entityId;
        private final Map<String, Integer> keysAndTypes;

        @TenantInfo(TenantAware.TENANTED)
        private final Map<String, Map<String, Object>> valuesByMapperEntityName = new ConcurrentHashMap<>(4);

        @TenantInfo(TenantAware.TENANTED)
        private final Map<String, Optional<String>> textValuesCache = new ConcurrentHashMap<>();

        PropertySetData(String entityName, long entityId) {
            this.entityName = entityName;
            this.entityId = entityId;
            this.keysAndTypes = loadPropertyKeysAndTypes(entityName, entityId);
        }

        Set<String> keys() {
            return keysAndTypes.keySet();
        }

        Set<String> keys(int type) {
            return keysAndTypes.entrySet().stream()
                    .filter(entry -> type == entry.getValue())
                    .map(Map.Entry::getKey)
                    .collect(CollectorsUtil.toImmutableSet());
        }

        int getType(String key) {
            final Integer type = keysAndTypes.get(key);
            return (type != null) ? type : 0;
        }

        @Nullable
        PropertyEntry get(String key) {
            final Integer type = keysAndTypes.get(key);
            return (type != null) ? get(key, type) : null;
        }

        private PropertyEntry get(String key, int type) {
            final TypeMapper mapper = mapper(type);
            if (type == TEXT) {
                // We don't want to bulkLoad text fields, because they could be large, so they are
                // cached individually, instead.  We don't really want to be caching them at all,
                // but this compromise is currently necessary for performance reasons.
                return textValuesCache.computeIfAbsent(key, this::getText)
                        .map(value -> new PropertyEntryImpl(TEXT, value))
                        .orElse(null);
            }

            return new PropertyEntryImpl(type, bulkLoad(mapper).get(key));
        }

        private Map<String, Integer> loadPropertyKeysAndTypes(String entityName, long entityId) {
            final List<Tuple> rows = query(db -> db.newSqlQuery()
                    .select(O_S_PROPERTY_ENTRY.propertyKey, O_S_PROPERTY_ENTRY.type)
                    .from(O_S_PROPERTY_ENTRY)
                    .where(O_S_PROPERTY_ENTRY.entityName.eq(entityName))
                    .where(O_S_PROPERTY_ENTRY.entityId.eq(entityId))
                    .orderBy(O_S_PROPERTY_ENTRY.id.desc())  // For collisions, highest ID wins
                    .fetch());
            return asMap(rows, O_S_PROPERTY_ENTRY.type);
        }

        private Optional<String> getText(String key) {
            final List<String> list = query(db -> db.newSqlQuery()
                    .select(O_S_PROPERTY_TEXT.value)
                    .from(O_S_PROPERTY_ENTRY)
                    .join(O_S_PROPERTY_TEXT).on(O_S_PROPERTY_ENTRY.id.eq(O_S_PROPERTY_TEXT.id))
                    .where(O_S_PROPERTY_ENTRY.entityName.eq(entityName))
                    .where(O_S_PROPERTY_ENTRY.entityId.eq(entityId))
                    .where(O_S_PROPERTY_ENTRY.propertyKey.eq(key))
                    .orderBy(O_S_PROPERTY_ENTRY.id.desc())  // For collisions, highest ID wins
                    .fetch());
            // DESC order means higher IDs come first, so resolve duplicates by taking the *first* result
            return list.isEmpty() ? Optional.empty() : Optional.of(defaultString(list.get(0)));
        }

        private Map<String, Object> bulkLoad(TypeMapper mapper) {
            return valuesByMapperEntityName.computeIfAbsent(mapper.getValueEntity(),
                    mapperEntityName -> loadValues(entityName, entityId, mapper));
        }

        private Map<String, Object> loadValues(String entityName, long entityId, TypeMapper mapper) {
            final List<Tuple> rows = query(db -> db.newSqlQuery()
                    .select(O_S_PROPERTY_ENTRY.propertyKey, mapper.getValuePath())
                    .from(O_S_PROPERTY_ENTRY)
                    .join(mapper.getValueTable()).on(O_S_PROPERTY_ENTRY.id.eq(mapper.getValueTable().getNumericIdPath()))
                    .where(O_S_PROPERTY_ENTRY.entityName.eq(entityName))
                    .where(O_S_PROPERTY_ENTRY.entityId.eq(entityId))
                    .orderBy(O_S_PROPERTY_ENTRY.id.desc())  // For collisions, highest ID wins
                    .fetch());
            return asMap(rows, mapper.getValuePath());
        }

        /**
         * Collects result rows by distinct keys.
         * <p>
         * Note: The caller must ensure that the rows supplied are ordered by <strong>descending</strong> ID to ensure
         * deterministic handling of duplicate keys, which are possible due to the absence of any {@code UNIQUE INDEX}
         * across {@code (entity_name, entity_id, property_key)} tuples.
         * </p>
         *
         * @param rows      the result rows from the query
         * @param valuePath the path from which to extract the map's values
         * @param <T>       the inferred value type for the map
         * @return the resulting map
         */
        private <T> Map<String, T> asMap(final List<Tuple> rows, final Path<? extends T> valuePath) {
            // DESC order means higher IDs come first, so resolve duplicates by using the *first* value for that key
            // Collectors get awkward regarding duplicates and null values, so build this manually to keep it simple.
            final Map<String, T> map = Maps.newHashMapWithExpectedSize(rows.size());
            rows.forEach(tuple -> {
                final String key = tuple.get(O_S_PROPERTY_ENTRY.propertyKey);
                if (!map.containsKey(key)) {
                    map.put(key, tuple.get(valuePath));
                }
            });
            return map;
        }
    }


    /**
     * Some retry-able operation on a particular property set entry.
     */
    static abstract class Operation {
        final String entityName;
        final long entityId;
        final String propertyKey;

        protected Operation(String entityName, long entityId, String propertyKey) {
            this.entityName = entityName;
            this.entityId = entityId;
            this.propertyKey = propertyKey;
        }

        abstract boolean perform();

        // Force implementations to re-implement toString()
        public abstract String toString();
    }


    /**
     * Encapsulates a request to set a property value as an operation that can be retried if necessary.
     * <p>
     * See {@link #perform()} for the entry point.
     * </p>
     */
    class SetOperation extends Operation {
        private final int newType;
        private final TypeMapper newMapper;
        private final Object mappedValue;

        SetOperation(String entityName, long entityId, String propertyKey, int type, @Nullable Object unmappedValue) {
            super(entityName, entityId, propertyKey);
            this.newType = type;
            this.newMapper = mapper(type);
            this.mappedValue = (unmappedValue != null) ? mapper(type).getHandler().processSet(type, unmappedValue) : null;
        }

        /**
         * Attempts to perform this set operation.
         * <p>
         * The operation fails if:
         * </p>
         * <ul>
         * <li>No existing value is seen, but the attempt to create one fails; or</li>
         * <li>An existing value is seen, and it has the correct type, but the attempt to update its value returns no
         * matching rows; or</li>
         * <li>An existing value is seen, but it has a different type and the attempt to update its type returns no
         * matching rows; or</li>
         * <li>An existing value is seen, but it has a different type and the attempt to insert its value in the
         * new table fails; or</li>
         * <li>A database error occurs, such as a communication problem or a deadlock.</li>
         * </ul>
         *
         * @return {@code true} if the operation succeeded; {@code false} if the attempt failed, most likely due to
         * concurrent modification of this value in the database
         */
        @Override
        boolean perform() {
            return tx(this::upsert);
        }

        private boolean upsert(DbConnection db) {
            final List<Tuple> existing = db.newSqlQuery()
                    .select(O_S_PROPERTY_ENTRY.id, O_S_PROPERTY_ENTRY.type)
                    .forUpdate()
                    .from(O_S_PROPERTY_ENTRY)
                    .where(O_S_PROPERTY_ENTRY.entityName.eq(entityName))
                    .where(O_S_PROPERTY_ENTRY.entityId.eq(entityId))
                    .where(O_S_PROPERTY_ENTRY.propertyKey.eq(propertyKey))
                    .orderBy(O_S_PROPERTY_ENTRY.id.desc())
                    .fetch();
            return existing.isEmpty() ? insert(db) : update(db, existing);
        }

        private boolean insert(DbConnection db) {
            final long id = db.insert(O_S_PROPERTY_ENTRY)
                    .set(O_S_PROPERTY_ENTRY.entityName, entityName)
                    .set(O_S_PROPERTY_ENTRY.entityId, entityId)
                    .set(O_S_PROPERTY_ENTRY.propertyKey, propertyKey)
                    .set(O_S_PROPERTY_ENTRY.type, newType)
                    .executeWithId();
            newMapper.insert(db, id, mappedValue);
            return true;
        }

        private boolean update(DbConnection db, List<Tuple> idsAndTypes) {
            final Tuple idAndType = idsAndTypes.get(0);
            final Long id = requireNonNull(idAndType.get(O_S_PROPERTY_ENTRY.id), "id");
            final Integer oldType = requireNonNull(idAndType.get(O_S_PROPERTY_ENTRY.type), "type");

            if (update(db, id, oldType)) {
                deleteDuplicates(db, id, idsAndTypes);
                return true;
            }

            return false;
        }

        // If the update succeeds but we had multiple rows, then clean up the duplicates while we're writing...
        private void deleteDuplicates(DbConnection db, Long id, List<Tuple> idsAndTypes) {
            // We don't need to do anything unless there actually are duplicate properties...
            if (idsAndTypes.size() < 2) {
                return;
            }

            // All of the entries should have been locked when we selected them, so this should always succeed.
            db.delete(O_S_PROPERTY_ENTRY)
                    .where(O_S_PROPERTY_ENTRY.entityName.eq(entityName))
                    .where(O_S_PROPERTY_ENTRY.entityId.eq(entityId))
                    .where(O_S_PROPERTY_ENTRY.propertyKey.eq(propertyKey))
                    .where(O_S_PROPERTY_ENTRY.id.lt(id))
                    .execute();

            // With that sorted out, we should go purge the leftover values, too.  Of course,
            // we don't want to delete the current one, since that's what we just updated!
            removeValues(db, idsAndTypes.subList(1, idsAndTypes.size()));
        }

        private boolean update(DbConnection db, Long id, int oldType) {
            // Since we guarded the entry with a select for update, we should have the lock established.
            // We can therefore skip updating the entry if the type did not change.
            return (oldType != newType) ? updateTypeAndValue(db, id, oldType) : upsertValue(db, id);
        }

        private boolean updateTypeAndValue(DbConnection db, Long id, int oldType) {
            // Since we selected for update, the row should have been locked and this should always succeed...
            if (!updateType(db, id, oldType)) {
                LOG.warn("SELECT FOR UPDATE is broken: operation={}; oldType={}", this, oldType);
                return false;
            }

            // If the old type isn't supported, then I'm not sure how you got here or what to do about it.
            // The type mapper will throw a runtime exception.
            final TypeMapper oldMapper = mapper(oldType);

            // If the mappers use different tables, then we have to move the value instead of updating it in place.
            return newMapper.hasSameEntityName(oldMapper) ? upsertValue(db, id) : moveValue(db, id, oldMapper);
        }

        private boolean moveValue(DbConnection db, Long id, TypeMapper oldMapper) {
            oldMapper.delete(db, id);
            newMapper.insert(db, id, mappedValue);
            return true;
        }

        private boolean updateType(DbConnection db, long id, int oldType) {
            return db.update(O_S_PROPERTY_ENTRY)
                    .set(O_S_PROPERTY_ENTRY.type, newType)
                    .where(O_S_PROPERTY_ENTRY.id.eq(id))
                    .where(O_S_PROPERTY_ENTRY.type.eq(oldType))
                    .execute() > 0L;
        }

        // If we already have an entry whose type indicates the expected value table, then there is
        // supposed to be a value in it already and an UPDATE should succeed.  However, just in case
        // the value is missing, we will fall back on an INSERT if we have to, hence "upsert".
        private boolean upsertValue(DbConnection db, Long id) {
            return newMapper.upsert(db, id, mappedValue);
        }

        @Override
        public String toString() {
            return "SetOperation[entityName=" + entityName +
                    ",entityId=" + entityId +
                    ",propertyKey=" + propertyKey +
                    ",newType=" + newType +
                    ",newMapper=" + newMapper +
                    ']';
        }
    }


    class RemoveOperation extends Operation {
        RemoveOperation(String entityName, long entityId, String propertyKey) {
            super(entityName, entityId, propertyKey);
        }

        @Override
        boolean perform() {
            return tx(this::deleteEntryAndValue);
        }

        boolean deleteEntryAndValue(DbConnection db) {
            final List<Tuple> idsAndTypes = db.newSqlQuery()
                    .select(O_S_PROPERTY_ENTRY.id, O_S_PROPERTY_ENTRY.type)
                    .forUpdate()
                    .from(O_S_PROPERTY_ENTRY)
                    .where(O_S_PROPERTY_ENTRY.entityName.eq(entityName))
                    .where(O_S_PROPERTY_ENTRY.entityId.eq(entityId))
                    .where(O_S_PROPERTY_ENTRY.propertyKey.eq(propertyKey))
                    .orderBy(O_S_PROPERTY_ENTRY.id.desc())
                    .fetch();

            // Removal is idempotent; if there is no matching entry, then count that as success
            return idsAndTypes.isEmpty() || deleteEntriesAndValues(db, idsAndTypes);
        }

        boolean deleteEntriesAndValues(DbConnection db, List<Tuple> idsAndTypes) {
            final Tuple first = idsAndTypes.get(0);
            final long id = requireNonNull(first.get(O_S_PROPERTY_ENTRY.id));
            final int type = requireNonNull(first.get(O_S_PROPERTY_ENTRY.type));

            if (!deleteEntry(db, id, type)) {
                // If we think we have an entry to delete but the remove is unsuccessful, then something is
                // horribly wrong.  The forUpdate() flag on the select should have made this case impossible.
                LOG.warn("SELECT FOR UPDATE is broken.  operation={}", this);
                return false;
            }

            // If there were extra entries, then clean them up too...
            if (idsAndTypes.size() > 1) {
                db.delete(O_S_PROPERTY_ENTRY)
                        .where(O_S_PROPERTY_ENTRY.entityName.eq(entityName))
                        .where(O_S_PROPERTY_ENTRY.entityId.eq(entityId))
                        .where(O_S_PROPERTY_ENTRY.propertyKey.eq(propertyKey))
                        .where(O_S_PROPERTY_ENTRY.id.lt(id))
                        .execute();
            }

            removeValues(db, idsAndTypes);
            return true;
        }

        boolean deleteEntry(DbConnection db, long id, int type) {
            return db.delete(O_S_PROPERTY_ENTRY)
                    .where(O_S_PROPERTY_ENTRY.id.eq(id))
                    .where(O_S_PROPERTY_ENTRY.type.eq(type))
                    .execute() > 0L;
        }

        @Override
        public String toString() {
            return "RemoveOperation[entityName=" + entityName +
                    ",entityId=" + entityId +
                    ",propertyKey=" + propertyKey +
                    ']';
        }
    }
}

