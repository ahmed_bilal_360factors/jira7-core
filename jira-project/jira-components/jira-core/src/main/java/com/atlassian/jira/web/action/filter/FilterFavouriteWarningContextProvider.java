package com.atlassian.jira.web.action.filter;

import com.atlassian.jira.bc.favourites.FavouritesService;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.annotations.VisibleForTesting;

import java.util.Map;

import static com.atlassian.jira.bc.filter.FilterDeletionWarningViewProviderImpl.CONTEXT_KEY_SEARCH_REQUEST;

/**
 * Provides context for the template that generates warning messages whenever a user is about
 * to delete a filter that has been favourited by one or more other users.
 */
public class FilterFavouriteWarningContextProvider implements ContextProvider {
    @VisibleForTesting
    static final String CONTEXT_KEY_FAVOURITE_COUNT = "favouriteCount";
    
    private final FavouritesService favouritesService;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public FilterFavouriteWarningContextProvider(final FavouritesService favouritesService,
                                                 final JiraAuthenticationContext jiraAuthenticationContext) {
        this.favouritesService = favouritesService;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    public void init(final Map<String, String> map) {
    }

    @Override
    public Map<String, Object> getContextMap(final Map<String, Object> map) {
        SearchRequest searchRequest = (SearchRequest) map.get(CONTEXT_KEY_SEARCH_REQUEST);
        map.put(CONTEXT_KEY_FAVOURITE_COUNT, getOtherFavouriteCount(searchRequest));
        return map;
    }

    private long getOtherFavouriteCount(SearchRequest searchRequest) {
        // We want to know how many times it has been favourited by OTHER people (excluding the current user)
        ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
        final boolean isCurrentUsersFavourite = favouritesService.isFavourite(user, searchRequest);
        return isCurrentUsersFavourite ? searchRequest.getFavouriteCount().intValue() - 1 : searchRequest.getFavouriteCount().intValue();
    }
}
