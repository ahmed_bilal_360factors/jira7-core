package com.atlassian.jira.security.roles;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.group.GroupConfigurable;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.util.profiling.UtilTimerStack;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.function.Predicate;

/**
 * @see ProjectRoleManager
 */
public class DefaultProjectRoleManager implements ProjectRoleManager, GroupConfigurable {
    private ProjectRoleAndActorStore projectRoleAndActorStore;

    public DefaultProjectRoleManager(ProjectRoleAndActorStore projectRoleAndActorStore) {
        this.projectRoleAndActorStore = projectRoleAndActorStore;
    }

    @Override
    public Collection<ProjectRole> getProjectRoles() {
        return projectRoleAndActorStore.getAllProjectRoles();
    }

    @Override
    public Collection<ProjectRole> getProjectRoles(final ApplicationUser user, final Project project) {
        if (user == null) {
            return Collections.emptyList();
        }
        Collection<ProjectRole> associatedProjectRoles = new TreeSet<ProjectRole>(ProjectRoleComparator.COMPARATOR);
        Collection<ProjectRole> allProjectRoles = getProjectRoles();
        for (final ProjectRole projectRole : allProjectRoles) {
            final ProjectRoleActors projectRoleActors = getProjectRoleActors(projectRole, project);
            if (projectRoleActors != null && projectRoleActors.contains(user)) {
                associatedProjectRoles.add(projectRole);
            }
        }
        return associatedProjectRoles;
    }

    @Override
    public ProjectRole getProjectRole(Long id) {
        return projectRoleAndActorStore.getProjectRole(id);
    }

    @Override
    public ProjectRole getProjectRole(String name) {
        return projectRoleAndActorStore.getProjectRoleByName(Assertions.notBlank("ProjectRole", name));
    }

    @Override
    public ProjectRole createRole(ProjectRole projectRole) {
        if (projectRole == null || projectRole.getName() == null) {
            throw new IllegalArgumentException("ProjectRole can not be created with a null name");
        }

        if (isRoleNameUnique(projectRole.getName())) {
            return projectRoleAndActorStore.addProjectRole(projectRole);
        } else {
            throw new IllegalArgumentException("A project role with the provided name: " + projectRole.getName() + ", already exists in the system.");
        }
    }

    @Override
    public boolean isRoleNameUnique(String name) {
        return !roleWithNameExists(name);
    }

    private boolean roleWithNameExists(String name) {
        return projectRoleAndActorStore.getAllProjectRoles().stream().anyMatch(new Predicate<ProjectRole>() {
            @Override
            public boolean test(final ProjectRole projectRole) {
                return projectRole.getName().equalsIgnoreCase(name);
            }
        });
    }

    @Override
    public void deleteRole(ProjectRole projectRole) {
        projectRoleAndActorStore.deleteProjectRole(Assertions.notNull("ProjectRole", projectRole));
    }

    @Override
    public void updateRole(ProjectRole projectRole) {
        if (projectRole == null) {
            throw new IllegalArgumentException("ProjectRole can not be null");
        }
        if (projectRole.getName() == null) {
            throw new IllegalArgumentException("ProjectRole name can not be null");
        }
        projectRoleAndActorStore.updateProjectRole(projectRole);
    }

    @Override
    public ProjectRoleActors getProjectRoleActors(ProjectRole projectRole, Project project) {
        return projectRoleAndActorStore.getProjectRoleActors(Assertions.notNull("ProjectRole", projectRole).getId(),
                Assertions.notNull("Project", project).getId());
    }

    @Override
    public void updateProjectRoleActors(ProjectRoleActors projectRoleActors) {
        if (projectRoleActors == null) {
            throw new IllegalArgumentException("ProjectRoleActors can not be null");
        }
        if (projectRoleActors.getProjectId() == null) {
            throw new IllegalArgumentException("ProjectRoleActors project can not be null");
        }
        if (projectRoleActors.getProjectRoleId() == null) {
            throw new IllegalArgumentException("ProjectRoleActors projectRole can not be null");
        }
        if (projectRoleActors.getRoleActors() == null) {
            throw new IllegalArgumentException("ProjectRoleActors roleActors set can not be null");
        }
        projectRoleAndActorStore.updateProjectRoleActors(projectRoleActors);
    }

    @Override
    public DefaultRoleActors getDefaultRoleActors(ProjectRole projectRole) {
        return projectRoleAndActorStore.getDefaultRoleActors(Assertions.notNull("ProjectRole", projectRole).getId());
    }

    @Override
    public void updateDefaultRoleActors(DefaultRoleActors defaultRoleActors) {
        if (defaultRoleActors == null) {
            throw new IllegalArgumentException("DefaultRoleActors can not be null");
        }
        if (defaultRoleActors.getProjectRoleId() == null) {
            throw new IllegalArgumentException("DefaultRoleActors projectRole can not be null");
        }
        if (defaultRoleActors.getRoleActors() == null) {
            throw new IllegalArgumentException("DefaultRoleActors roleActors set can not be null");
        }
        projectRoleAndActorStore.updateDefaultRoleActors(defaultRoleActors);
    }

    @Override
    public void applyDefaultsRolesToProject(Project project) {
        projectRoleAndActorStore.applyDefaultsRolesToProject(Assertions.notNull("Project", project));
    }

    @Override
    public void removeAllRoleActorsByNameAndType(String key, String type) {
        projectRoleAndActorStore.removeAllRoleActorsByKeyAndType(
                Assertions.notNull("role actor name", key),
                Assertions.notNull("role type", type));
    }

    @Override
    public void removeAllRoleActorsByProject(Project project) {
        if (project == null || project.getId() == null) {
            throw new IllegalArgumentException("The project id can not be null");
        }
        projectRoleAndActorStore.removeAllRoleActorsByProject(project);
    }

    @Override
    public boolean isUserInProjectRole(ApplicationUser user, ProjectRole projectRole, Project project) {
        try {
            UtilTimerStack.push("DefaultProjectRoleManager.isUserInProjectRole");

            if (project == null || project.getId() == null) {
                throw new IllegalArgumentException("The project id can not be null");
            }
            if (projectRole == null) {
                throw new IllegalArgumentException("ProjectRole can not be null");
            }
            return getProjectRoleActors(projectRole, project).contains(user);
        } finally {
            UtilTimerStack.pop("DefaultProjectRoleManager.isUserInProjectRole");
        }
    }

    @Override
    public Collection<Long> getProjectIdsContainingRoleActorByNameAndType(String key, String type) {
        return projectRoleAndActorStore.getProjectIdsContainingRoleActorByKeyAndType(
                Assertions.notNull("role actor name", key),
                Assertions.notNull("role type", type));
    }

    @Override
    public List<Long> roleActorOfTypeExistsForProjects(List<Long> projectsToLimitBy, ProjectRole projectRole, String projectRoleType, String projectRoleParameter) {
        return projectRoleAndActorStore.roleActorOfTypeExistsForProjects(projectsToLimitBy, projectRole, projectRoleType, projectRoleParameter);
    }

    @Override
    public Map<Long, List<String>> getProjectIdsForUserInGroupsBecauseOfRole(List<Long> projectsToLimitBy, ProjectRole projectRole, String projectRoleType, String userName) {
        return projectRoleAndActorStore.getProjectIdsForUserInGroupsBecauseOfRole(projectsToLimitBy, projectRole, projectRoleType, userName);
    }

    @Override
    public ProjectIdToProjectRoleIdsMap createProjectIdToProjectRolesMap(final ApplicationUser user, final Collection<Long> projectIds) {
        ProjectIdToProjectRoleIdsMap map = new ProjectIdToProjectRoleIdsMap();
        if (projectIds != null && !projectIds.isEmpty()) {
            ProjectManager projectManager = ComponentAccessor.getProjectManager();
            for (final Long projectId : projectIds) {
                Collection<ProjectRole> projectRoles = getProjectRoles(user, projectManager.getProjectObj(projectId));
                for (final ProjectRole projectRole : projectRoles) {
                    map.add(projectId, projectRole.getId());
                }
            }
        }
        return map;
    }

    @Override
    public boolean isGroupUsed(@Nonnull final Group group) {
        return projectRoleAndActorStore.isGroupUsed(group.getName());
    }
}
