package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.auditing.AssociatedItem;
import com.atlassian.jira.auditing.ChangedValue;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static java.util.stream.StreamSupport.stream;

/**
 * @since 7.0
 */
public class AuditEntry {
    private final Class<?> sourceClass;
    private final String summary;
    private final List<ChangedValue> changedValues;
    private final String description;
    private final String changedObject;
    private final AssociatedItem.Type eventType;
    private final AuditEntrySeverity severity;
    private final boolean eventShowsInCloudLog;

    public AuditEntry(Class<?> sourceClass,
                      String summary,
                      String description,
                      AssociatedItem.Type eventType) {
        this(sourceClass, summary, description, eventType, null, true);
    }

    public AuditEntry(Class<?> sourceClass,
                      String summary, String description,
                      final AssociatedItem.Type eventType,
                      @Nullable String changedObject,
                      boolean eventShowsInCloudLog,
                      ChangedValue... changedValues) {
        this(sourceClass, summary, description, eventType, changedObject, eventShowsInCloudLog,
                ImmutableList.copyOf(notNull("changedValues", changedValues)), AuditEntrySeverity.INFO);
    }

    public AuditEntry(Class<?> sourceClass,
                      String summary, String description,
                      final AssociatedItem.Type eventType,
                      @Nullable String changedObject,
                      boolean eventShowsInCloudLog,
                      Iterable<ChangedValue> changedValues,
                      AuditEntrySeverity severity) {
        this.sourceClass = notNull("sourceClass", sourceClass);
        this.summary = notNull("summary", summary);
        this.changedValues = ImmutableList.copyOf(notNull("changedValues", changedValues));
        this.description = notNull("description", description);
        this.changedObject = changedObject;
        this.eventType = notNull("eventType", eventType);
        this.severity = severity;
        this.eventShowsInCloudLog = eventShowsInCloudLog;
    }

    public Class<?> getSourceClass() {
        return sourceClass;
    }

    /**
     * A short summary of what the changes made were.
     *
     * @return a summary of what was changed, typically a single sentence
     */
    public String getSummary() {
        return summary;
    }

    public Iterable<ChangedValue> getChangedValues() {
        return changedValues;
    }

    /**
     * An explanation of why the changes are being made, intended for an administrator to read.
     *
     * @return the explanation
     */
    public String getDescription() {
        return description;
    }

    @Nullable
    public String getChangedObject() {
        return changedObject;
    }

    public AssociatedItem.Type getType() {
        return eventType;
    }

    public AuditEntrySeverity getSeverity() {
        return severity;
    }

    public boolean eventShowsInCloudLog() {
        return eventShowsInCloudLog;
    }

    public String toLogMessage() {
        StringBuilder sb = new StringBuilder();
        sb.append(getSummary()).append(" - ").append(getDescription());
        if (getChangedObject() != null) {
            sb.append(" - ");
            sb.append(getChangedObject());
        }
        if (!changedValues.isEmpty()) {
            sb.append(" - ");
            String changed = stream(getChangedValues().spliterator(), false).map(changedValue ->
                    "(" + changedValue.getName() + ": "
                            + changedValue.getFrom() + " --> " + changedValue.getTo() + ")")
                    .collect(Collectors.joining(", "));
            sb.append(changed);
        }
        sb.append(", type: ").append(eventType.name());
        return sb.toString();
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AuditEntry that = (AuditEntry) o;
        return Objects.equals(sourceClass, that.sourceClass) &&
                Objects.equals(summary, that.summary) &&
                Objects.equals(changedValues, that.changedValues) &&
                Objects.equals(description, that.description) &&
                Objects.equals(changedObject, that.changedObject) &&
                Objects.equals(severity, that.severity) &&
                Objects.equals(eventType, that.eventType) &&
                eventShowsInCloudLog == that.eventShowsInCloudLog;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sourceClass, summary, changedValues, description, changedObject, eventType, severity);
    }

    @Override
    public String toString() {
        return "AuditEntry{" +
                "sourceClass=" + sourceClass +
                ", summary='" + summary + '\'' +
                ", changedValues=" + changedValues +
                ", description='" + description + '\'' +
                ", changedObject='" + changedObject + '\'' +
                ", license=" + eventType +
                ", severity=" + severity +
                '}';
    }
}
