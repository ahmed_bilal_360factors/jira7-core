package com.atlassian.jira.mail.util;

import com.atlassian.fugue.Option;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarFormatPolicy;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarManagerImpl;
import com.atlassian.jira.avatar.AvatarTranscoder;
import com.atlassian.jira.avatar.AvatarTranscoderImpl;
import com.atlassian.jira.io.CloseableResourceData;
import com.atlassian.jira.io.MediaConsumer;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.thumbnail.ThumbnailManager;
import com.atlassian.jira.util.Consumer;
import com.atlassian.jira.web.ServletContextProvider;
import com.google.common.base.Preconditions;
import com.google.common.net.MediaType;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.servlet.ServletContext;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.UUID;

import static com.atlassian.fugue.Option.option;

/**
 * @since 7.0
 * <p>
 * This is a utility classed used by {@link MailAttachmentsManagerImpl} to create different type of image attachments.
 * Previously most of this code lived inside the {@link MailAttachmentsManagerImpl}.
 */
public class MailAttachments {
    private static final Logger log = LoggerFactory.getLogger(MailAttachments.class);

    /**
     * Creates an image attachment from provided path. If the image at the path is in SVG format it will be transcoded to PNG.
     */
    public static MailAttachment newImageAttachment(final String imagePath, final AvatarTranscoder avatarTranscoder) {
        final ServletContext servletContext = ServletContextProvider.getServletContext();
        final String mimeType = servletContext.getMimeType(imagePath);

        if (!AvatarManagerImpl.isSvgContentType(mimeType)) {
            return new ImageAttachment(imagePath);
        }

        return new TranscodedImageAttachment(imagePath, avatarTranscoder);
    }

    /**
     * Creates an image attachment from provided URL.
     * Should only be used for local resources that aren't accessible via ServletContext
     */
    public static MailAttachment newUrlImageAttachment(final String imagePath) {
        return new UrlImageAttachment(imagePath);
    }

    /**
     * Creates an image attachment from provided avatar. Uses {@link AvatarManager} to read the image's data.
     */
    public static MailAttachment newAvatarAttachment(final Avatar avatar, final String avatarOwnerDescription, final AvatarManager avatarManager) {
        return new DefaultAvatarAttachment(avatar, avatarOwnerDescription, avatarManager);
    }

    /**
     * Creates an image attachment from provided avatar via {@link AvatarTranscoderImpl} to get the image in raster fromat.
     */
    public static MailAttachment newTranscodedAvatarAttachment(final Avatar avatar, final String avatarOwnerDescription, final AvatarManager avatarManager, final Avatar.Size size) {
        return new TranscodedAvatarAttachment(avatar, avatarOwnerDescription, avatarManager, size);
    }

    public static MailAttachment newTranscodedAvatarAttachment(final Avatar avatar, final String avatarOwnerDescription, final AvatarManager avatarManager) {
        return newTranscodedAvatarAttachment(avatar, avatarOwnerDescription, avatarManager, Avatar.Size.MEDIUM);
    }

    /**
     * Create a new mail attachment by streaming the content of the attachment from the attachment manager.
     *
     * @param attachment        The attachment whose contents we want to include.
     * @param attachmentManager The manager to use for streaming the content.
     * @return A {@link MailAttachment} containing the desired attachment content.
     */
    public static MailAttachment newMailAttachmentByStreamingFromAttachmentManager(final Attachment attachment, final AttachmentManager attachmentManager) {
        return new AttachmentFromAttachmentManager(attachment, attachmentManager);
    }

    /**
     * Create a new mail attachment by streaming the content of a thumbnail from the thumbnail manager.
     *
     * @param attachment       The attachment whose thumbnail we want to attach.
     * @param thumbnailManager The manager to use for streaming the content.
     * @return A {@link MailAttachment} containing the desired thumbnail.
     */
    public static MailAttachment newMailAttachmentByStreamingFromThumbnailManager(final Attachment attachment, final ThumbnailManager thumbnailManager) {
        return new ThumbnailFromThumbnailManager(attachment, thumbnailManager);
    }

    private static abstract class StreamingMailAttachment implements MailAttachment {
        private final Attachment attachment;

        public StreamingMailAttachment(Attachment attachment) {
            this.attachment = attachment;
        }

        abstract DataHandler getDataHandler() throws IOException;

        public Attachment getAttachment() {
            return attachment;
        }

        @Override
        public String getUniqueName() {
            return String.format("static-%s", UUID.randomUUID().toString());
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }

            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final StreamingMailAttachment that = (StreamingMailAttachment) o;
            return attachment.getId().equals(that.attachment.getId());
        }

        @Override
        public int hashCode() {
            return attachment.getId().hashCode();
        }

        @Override
        public BodyPart buildBodyPart() {
            try {
                final MimeBodyPart bodyPart = new MimeBodyPart();
                bodyPart.setDataHandler(getDataHandler());
                bodyPart.setDisposition(MimeBodyPart.INLINE);
                return bodyPart;
            } catch (MessagingException e) {
                log.warn(String.format("Cannot add attachment '%s' to email.", attachment.getFilename()));
                return null;
            } catch (IOException e) {
                log.warn(String.format("Cannot read attachment '%s'", attachment.getFilename()));
                return null;
            }
        }
    }

    private static class AttachmentFromAttachmentManager extends StreamingMailAttachment {
        private final AttachmentManager attachmentManager;

        private AttachmentFromAttachmentManager(final Attachment attachment, final AttachmentManager attachmentManager) {
            super(attachment);
            this.attachmentManager = attachmentManager;
        }

        @Override
        DataHandler getDataHandler() throws IOException {
            return attachmentManager.streamAttachmentContent(getAttachment(),
                    (inputStream) -> new DataHandler(new ByteArrayDataSource(inputStream, getAttachment().getMimetype())));
        }
    }

    private static class ThumbnailFromThumbnailManager extends StreamingMailAttachment {
        private final ThumbnailManager thumbnailManager;

        private ThumbnailFromThumbnailManager(final Attachment attachment, final ThumbnailManager thumbnailManager) {
            super(attachment);
            this.thumbnailManager = thumbnailManager;
        }

        @Override
        DataHandler getDataHandler() throws IOException {
            return thumbnailManager.streamThumbnailContent(getAttachment(),
                    (inputStream) -> new DataHandler(new ByteArrayDataSource(inputStream, getAttachment().getMimetype())));
        }
    }

    /**
     * Reads static image using ServletContext and build BodyPart from it.
     */
    private static class ImageAttachment implements MailAttachment {
        protected final String imagePath;

        private ImageAttachment(final String imagePath) {
            Preconditions.checkNotNull(imagePath);
            this.imagePath = imagePath;
        }

        @Override
        public BodyPart buildBodyPart() {
            try (final CloseableResourceData resourceData = getResourceData()) {
                final MimeBodyPart bodyPart = new MimeBodyPart();
                bodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(resourceData.getInputStream(), resourceData.getContentType().toString())));
                bodyPart.setDisposition(MimeBodyPart.INLINE);
                return bodyPart;
            } catch (MessagingException e) {
                log.warn(String.format("Cannot add image as Mail attachment: '%s'", imagePath), e);
                return null;
            } catch (IOException e) {
                log.warn(String.format("Cannot load resource for: '%s'", imagePath), e);
                return null;
            }
        }

        protected CloseableResourceData getResourceData() throws MessagingException, IOException {
            final ServletContext servletContext = ServletContextProvider.getServletContext();
            final InputStream resourceStream = servletContext.getResourceAsStream(imagePath);
            final String mimeType = servletContext.getMimeType(imagePath);
            return new CloseableResourceData(resourceStream, mimeType);
        }

        @Override
        public String getUniqueName() {
            final String name = FilenameUtils.getBaseName(imagePath);
            final UUID uuid = UUID.randomUUID();
            return String.format("static-%s-%s", name, uuid.toString());
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final ImageAttachment that = (ImageAttachment) o;

            return imagePath.equals(that.imagePath);

        }

        @Override
        public int hashCode() {
            return imagePath.hashCode();
        }

    }

    private static class TranscodedImageAttachment extends ImageAttachment {
        private final AvatarTranscoder avatarTranscoder;

        public TranscodedImageAttachment(final String imagePath, final AvatarTranscoder avatarTranscoder) {
            super(imagePath);
            this.avatarTranscoder = avatarTranscoder;
        }

        @Override
        protected CloseableResourceData getResourceData() throws MessagingException, IOException {
            final ServletContext servletContext = ServletContextProvider.getServletContext();

            try (final InputStream resourceStream = servletContext.getResourceAsStream(imagePath);
                 final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                final String mimeType = MediaType.PNG.toString();
                avatarTranscoder.transcodeAndTag(resourceStream, outputStream);

                return new CloseableResourceData(new ByteArrayInputStream(outputStream.toByteArray()), mimeType);
            }
        }
    }

    private static class UrlImageAttachment extends ImageAttachment {
        private UrlImageAttachment(final String imagePath) {
            super(imagePath);
        }

        @Override
        protected CloseableResourceData getResourceData() throws MessagingException, IOException {
            try {
                final URL url = new URL(imagePath);
                final URLConnection connection = url.openConnection();
                return new CloseableResourceData(connection.getInputStream(), connection.getContentType());
            } catch (MalformedURLException e) {
                throw new MessagingException("Malformed atttachment URL", e);
            }
        }
    }

    @Nonnull
    private static abstract class AbstractAvatarAttachment implements MailAttachment {
        protected final Avatar avatar;
        protected final String avatarOwnerDescription;

        private AbstractAvatarAttachment(@Nullable final Avatar avatar, final String avatarOwnerDescription) {
            this.avatarOwnerDescription = avatarOwnerDescription;
            this.avatar = avatar;
        }

        public abstract ToBodyPartConsumer prepareBodyPart() throws IOException;

        @Override
        public BodyPart buildBodyPart() {
            try {
                final ToBodyPartConsumer dataAccessor = prepareBodyPart();

                final Option<BodyPart> bodyPart = option(dataAccessor.getBodyPart());
                if (bodyPart.isDefined()) {
                    bodyPart.get().setDisposition(MimeBodyPart.INLINE);
                    return bodyPart.get();
                } else {
                    return null;
                }

            } catch (FileNotFoundException e) {
                log.warn(String.format("Cannot add avatar as Mail attachment for '%s' - file not found", avatarOwnerDescription));
                return null;
            } catch (IOException e) {
                log.warn(String.format("Cannot add avatar as Mail attachment for '%s'", avatarOwnerDescription), e);
                return null;
            } catch (MessagingException e) {
                log.warn(String.format("Problem with disposition while adding avatar as Mail attachment for '%s'", avatarOwnerDescription), e);
                return null;
            }
        }

        @Override
        public String getUniqueName() {
            final UUID uuid = UUID.randomUUID();
            return String.format("avatar-%s", uuid.toString());
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof AbstractAvatarAttachment)) {
                return false;
            }

            final AbstractAvatarAttachment that = (AbstractAvatarAttachment) o;

            return !(avatar != null ? !avatar.equals(that.avatar) : that.avatar != null);
        }

        @Override
        public int hashCode() {
            return avatar == null ? 0 : avatar.hashCode();
        }
    }

    private static class DefaultAvatarAttachment extends AbstractAvatarAttachment {
        private final AvatarManager avatarManager;

        private DefaultAvatarAttachment(@Nullable final Avatar avatar, final String avatarOwnerDescription, final AvatarManager avatarManager) {
            super(avatar, avatarOwnerDescription);
            this.avatarManager = avatarManager;
        }

        @Override
        public ToBodyPartConsumer prepareBodyPart() throws IOException {
            AvatarFormatPolicy avatarFormatPolicy = AvatarFormatPolicy.createPngFormatPolicy().withFallingBackToOriginalDataStrategy();
            final ToBodyPartContentTypeConsumer bodyPartContentTypeConsumer = new ToBodyPartContentTypeConsumer();
            final ToBodyPartConsumer dataAccessor = new ToBodyPartConsumer(AvatarManager.PNG_CONTENT_TYPE, bodyPartContentTypeConsumer);
            MediaConsumer mediaConsumer = new MediaConsumer(bodyPartContentTypeConsumer, dataAccessor);

            // The size we provide here must match the dimensions of the embedded avatar image in
            // jira-components/jira-core/src/main/resources/templates/email/html/includes/header.vm , otherwise
            // Outlook will rewrite the image and destroy our JIRA metadata needed for JRA-25705
            avatarManager.readAvatarData(avatar, Avatar.Size.MEDIUM, avatarFormatPolicy, mediaConsumer);

            return dataAccessor;
        }
    }

    private static class TranscodedAvatarAttachment extends AbstractAvatarAttachment {
        private final AvatarManager avatarManager;
        private final Avatar.Size size;

        private TranscodedAvatarAttachment(@Nullable final Avatar avatar, final String avatarOwnerDescription, final AvatarManager avatarManager, final Avatar.Size size) {
            super(avatar, avatarOwnerDescription);
            this.avatarManager = avatarManager;
            this.size = size;
        }

        @Override
        public ToBodyPartConsumer prepareBodyPart() throws IOException {
            AvatarFormatPolicy avatarFormatPolicy = AvatarFormatPolicy.createPngFormatPolicy().withFallingBackToOriginalDataStrategy();
            final ToBodyPartContentTypeConsumer bodyPartContentTypeConsumer = new ToBodyPartContentTypeConsumer();
            final ToBodyPartConsumer dataAccessor = new ToBodyPartConsumer(AvatarManager.PNG_CONTENT_TYPE, bodyPartContentTypeConsumer);
            MediaConsumer mediaConsumer = new MediaConsumer(bodyPartContentTypeConsumer, dataAccessor);

            // The size we provide here must match the dimensions of the embedded avatar image in
            // jira-components/jira-core/src/main/resources/templates/email/html/includes/header.vm , otherwise
            // Outlook will rewrite the image and destroy our JIRA metadata needed for JRA-25705
            avatarManager.readAvatarData(avatar, size, avatarFormatPolicy, mediaConsumer);

            return dataAccessor;
        }
    }

    private static class ToBodyPartContentTypeConsumer implements Consumer<String> {
        private String contentType = null;

        @Override
        public void consume(@Nonnull String element) {
            contentType = element;
        }

        public String getContentType() {
            return contentType;
        }
    }

    private static class ToBodyPartConsumer implements Consumer<InputStream> {
        private BodyPart bodyPart;
        private final String contentType;
        private final ToBodyPartContentTypeConsumer contentTypeConsumer;

        private ToBodyPartConsumer(final String contentType, ToBodyPartContentTypeConsumer contentTypeConsumer) {
            this.contentType = contentType;
            this.contentTypeConsumer = contentTypeConsumer;
        }

        @Override
        public void consume(@Nonnull final InputStream element) {
            String effectiveContentType = contentTypeConsumer.getContentType();
            if (effectiveContentType == null) {
                effectiveContentType = contentType;
            }
            try {
                bodyPart = new MimeBodyPart();
                bodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(element, effectiveContentType)));
            } catch (MessagingException | IOException e) {
                log.warn("Cannot read avatar", e);
                bodyPart = null;
            }
        }

        private BodyPart getBodyPart() {
            return bodyPart;
        }
    }

}