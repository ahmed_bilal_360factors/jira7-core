package com.atlassian.jira.issue.label.suggestions;

import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.StatisticAccessorBean;
import com.atlassian.jira.web.bean.StatisticMapWrapper;

import java.util.LinkedHashSet;
import java.util.Set;

public class PopularLabelsProvider {
    public Set<String> findMostPopular(final String searchField, final ApplicationUser user) {
        final Set<String> suggestions = new LinkedHashSet<>();
        final StatisticAccessorBean statBean = new StatisticAccessorBean(user, new SearchRequest());

        try {
            @SuppressWarnings("unchecked")
            final StatisticMapWrapper<Label, Number> statWrapper = statBean.getAllFilterBy(
                    searchField,
                    StatisticAccessorBean.OrderBy.TOTAL,
                    StatisticAccessorBean.Direction.DESC
            );
            for (Label label : statWrapper.keySet()) {
                if (label != null) {
                    suggestions.add(label.getLabel());
                }
            }
        } catch (SearchException e) {
            throw new RuntimeException(e);
        }

        return suggestions;
    }
}
