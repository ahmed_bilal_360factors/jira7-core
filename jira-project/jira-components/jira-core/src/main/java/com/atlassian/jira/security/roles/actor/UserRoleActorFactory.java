package com.atlassian.jira.security.roles.actor;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.model.querydsl.QProjectRoleActor;
import com.atlassian.jira.security.roles.ProjectRoleActor;
import com.atlassian.jira.security.roles.RoleActor;
import com.atlassian.jira.security.roles.RoleActorFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.dbc.Assertions;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.querydsl.core.Tuple;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.security.roles.ProjectRoleActor.USER_ROLE_ACTOR_TYPE;
import static com.google.common.collect.Iterables.all;

/**
 * Responsible for construction of UserRoleActor instances. Also optimises the
 * lookup where we have many users in a particular role for a project by doing
 * a map lookup based on the username.
 * <p>
 * Access to the actual User instance is via a UserFactory so we can unit-test.
 * The production dependency is set in the default ctor.
 */
@Internal
public class UserRoleActorFactory implements RoleActorFactory {

    private final UserManager userManager;
    private final DbConnectionManager dbConnectionManager;

    public UserRoleActorFactory(final UserManager userManager, final DbConnectionManager dbConnectionManager) {
        this.userManager = userManager;
        this.dbConnectionManager = dbConnectionManager;
    }

    @Override
    public ProjectRoleActor createRoleActor(Long id, Long projectRoleId, Long projectId, String type, String parameter) {
        if (!USER_ROLE_ACTOR_TYPE.equals(type)) {
            throw new IllegalArgumentException(this.getClass().getName() + " cannot create RoleActors of type: " + type);
        }
        Assertions.notNull("parameter", parameter);
        return new UserRoleActor(id, projectRoleId, projectId, parameter);
    }

    @Override
    public Set<RoleActor> optimizeRoleActorSet(Set<RoleActor> roleActors) {
        Set<RoleActor> originals = new HashSet<RoleActor>(roleActors);
        Set<UserRoleActor> userRoleActors = new HashSet<UserRoleActor>(roleActors.size());
        for (Iterator<RoleActor> it = originals.iterator(); it.hasNext(); ) {
            RoleActor roleActor = it.next();
            if (roleActor instanceof UserRoleActor) {
                userRoleActors.add((UserRoleActor) roleActor);
                it.remove();
            }
        }
        if (!userRoleActors.isEmpty()) {
            // no point aggregating if there's only one
            if (userRoleActors.size() > 1) {
                UserRoleActor prototype = userRoleActors.iterator().next();
                originals.add(new AggregateRoleActor(prototype, userRoleActors));
            } else {
                // just one? throw it back...
                originals.addAll(userRoleActors);
            }
        }
        return Collections.unmodifiableSet(originals);
    }

    @Override
    @Nonnull
    public Set<ProjectRoleActor> getAllRoleActorsForUser(@Nullable final ApplicationUser user) {
        if (user == null) {
            return ImmutableSet.of();
        }

        final String userKey = user.getKey();
        final ImmutableSet.Builder<ProjectRoleActor> resultsBuilder = ImmutableSet.builder();

        dbConnectionManager.execute(dbConnection ->
        {
            final QProjectRoleActor pra = new QProjectRoleActor("pra");

            final List<Tuple> roleActorTuples = dbConnection.newSqlQuery().
                    select(pra.id, pra.pid, pra.projectroleid, pra.roletype, pra.roletypeparameter).
                    from(pra).
                    where(pra.roletype.eq(USER_ROLE_ACTOR_TYPE).
                            and(pra.roletypeparameter.eq(user.getKey())).
                            and(pra.pid.isNotNull())).
                    fetch();

            roleActorTuples.stream().map(tuple -> new UserRoleActor(tuple, pra, userKey)).forEach(resultsBuilder::add);
        });

        return resultsBuilder.build();
    }

    class UserRoleActor extends AbstractRoleActor {
        private UserRoleActor(Long id, Long projectRoleId, Long projectId, String key) {
            super(id, projectRoleId, projectId, key);
        }

        @Deprecated
        private UserRoleActor(Long id, Long projectRoleId, Long projectId, ApplicationUser user) {
            super(id, projectRoleId, projectId, ApplicationUsers.getKeyFor(user));
        }

        private UserRoleActor(final Tuple tuple, final QProjectRoleActor projectRoleActor, final String userKey) {
            super(tuple.get(projectRoleActor.id), tuple.get(projectRoleActor.projectroleid), tuple.get(projectRoleActor.pid), userKey);
        }

        public String getType() {
            return USER_ROLE_ACTOR_TYPE;
        }

        @Override
        public boolean isActive() {
            return getUser().isActive();
        }

        public String getDescriptor() {
            return getAppUser().getDisplayName();
        }

        public Set<ApplicationUser> getUsers() {
            return CollectionBuilder.newBuilder(getUser()).asSet();
        }

        public boolean contains(ApplicationUser user) {
            return user != null && user.getKey().equals(getParameter());
        }

        private ApplicationUser getAppUser() {
            return userManager.getUserByKeyEvenWhenUnknown(getParameter());
        }

        private ApplicationUser getUser() {
            return userManager.getUserByKeyEvenWhenUnknown(getParameter());
        }

    }

    /**
     * Aggregate UserRoleActors and look them up based on the hashcode
     */
    static class AggregateRoleActor extends AbstractRoleActor {
        private final Map<String, UserRoleActor> userRoleActorMap;

        private AggregateRoleActor(ProjectRoleActor prototype, Set<UserRoleActor> roleActors) {
            super(null, prototype.getProjectRoleId(), prototype.getProjectId(), null);

            final Map<String, UserRoleActor> map = new HashMap<String, UserRoleActor>(roleActors.size());

            for (final UserRoleActor userRoleActor : roleActors) {
                map.put(userRoleActor.getParameter(), userRoleActor);
            }
            this.userRoleActorMap = Collections.unmodifiableMap(map);
        }

        @Override
        public boolean isActive() {
            return all(userRoleActorMap.values(), new Predicate<UserRoleActor>() {
                @Override
                public boolean apply(@Nullable UserRoleActor user) {
                    return user != null && user.isActive();
                }
            });
        }

        @Override
        public boolean contains(ApplicationUser user) {
            return user != null && userRoleActorMap.containsKey(user.getKey())
                    && userRoleActorMap.get(user.getKey()).contains(user);
        }

        /*
         * not enormously efficient, could cache users maybe, we want contains to be fast...
         *
         * @see com.atlassian.jira.security.roles.RoleActor#getUsers()
         */
        public Set<ApplicationUser> getUsers() {
            final Set<ApplicationUser> result = new HashSet<ApplicationUser>(userRoleActorMap.size());
            for (UserRoleActor roleActor : userRoleActorMap.values()) {
                // not the most efficient, but generally called in UI etc.
                result.addAll(roleActor.getUsers());
            }
            return Collections.unmodifiableSet(result);
        }

        public String getType() {
            return USER_ROLE_ACTOR_TYPE;
        }
    }
}
