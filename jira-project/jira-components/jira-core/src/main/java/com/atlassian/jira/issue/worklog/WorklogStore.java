package com.atlassian.jira.issue.worklog;

import com.atlassian.jira.bc.issue.worklog.DeletedWorklog;
import com.atlassian.jira.issue.Issue;

import java.util.List;
import java.util.Set;

/**
 *
 */
public interface WorklogStore {
    /**
     * Updates fields of an existing worklog in the datastore (identified by its id) with the supplied worklog.
     *
     * @param worklog identifies the worklog to update and provides the updated values.
     * @return the updated worklog.
     */
    Worklog update(Worklog worklog);

    /**
     * Creates a new worklog in the data store based on the values in the passed in Worklog object.
     *
     * @param worklog specifies the values to create the worklog with.
     * @return the representation of the created worklog, including the id.
     */
    Worklog create(Worklog worklog);

    /**
     * Deletes a worklog from the data store based on the passed in id.
     *
     * @param worklogId specifies which worklog to delete (not null)
     * @return true if the worklog was deleted, false otherwise
     * @throws IllegalArgumentException if the worklogId is null.
     */
    boolean delete(Long worklogId);

    /**
     * Deletes all worklogs which are related to the given {@link Issue}.
     *
     * @param issue issue for which worklogs will be removed
     * @return the number of worklogs deleted
     * @throws IllegalArgumentException if the issue or issue's id is null.
     */
    long deleteWorklogsForIssue(Issue issue);

    /**
     * Returns a worklog specified by it's id
     *
     * @param id the specified id (not null)
     * @return the specified worklog, or null if not found
     */
    Worklog getById(Long id);

    /**
     * Returns all child worklogs of a specified issue
     *
     * @param issue the specified parent issue (not null)
     * @return a List of Worklogs, ordered by creation date. An empty List will be returned if none are found
     */
    List<Worklog> getByIssue(Issue issue);

    /**
     * Returns the count of all {@link com.atlassian.jira.issue.worklog.Worklog}'s that have a visibility restriction
     * of the provided group.
     *
     * @param groupName identifies the group the worklogs are restricted by, this must not be null.
     * @return the count of restricted groups
     * @since v3.12
     */
    long getCountForWorklogsRestrictedByGroup(String groupName);

    /**
     * Returns the count of all {@link com.atlassian.jira.issue.worklog.Worklog}'s that have a visibility restriction
     * of the provided role.
     *
     * @param roleId identifies the role the worklogs are restricted by, this must not be null.
     * @return the count of restricted worklogs
     * @since v7.0
     */
    long getCountForWorklogsRestrictedByRole(Long roleId);

    /**
     * Updates {@link com.atlassian.jira.issue.worklog.Worklog}'s such that worklogs that have a visibility
     * restriction of the provided groupName will be changed to have a visibility restriction of the
     * provided swapGroup.
     * <p>
     * Note: There is no validation performed by this method to determine if the provided swapGroup is a valid
     * group with JIRA. This validation must be done by the caller.
     *
     * @param groupName identifies the group the worklogs are restricted by, this must not be null.
     * @param swapGroup identifies the group the worklogs will be changed to be restricted by, this must not be null.
     * @return tbe number of worklogs affected by the update.
     * @since v3.12
     */
    int swapWorklogGroupRestriction(String groupName, String swapGroup);

    /**
     * Updates {@link com.atlassian.jira.issue.worklog.Worklog}'s such that worklogs that have a visibility
     * restriction of the provided role will be changed to have a visibility restriction to the provided swapRole.
     * <p>
     * Note: There is no validation performed by this method to determine if the provided swapRoleId is a valid
     * group with JIRA. This validation must be done by the caller.
     *
     * @param roleId     identifies the role the worklogs are restricted by, this must not be null.
     * @param swapRoleId identifies the role the worklogs will be changed to be restricted by, this must not be null.
     * @return the number of worklogs affected by the update.
     * @since v7.0
     */
    int swapWorklogRoleRestriction(Long roleId, Long swapRoleId);

    /**
     * Returns {@param maxResults} of worklogs, which were updated or created after (inclusive) provided time in milliseconds.
     * Returned worklogs are ordered by the update time.
     *
     * @param sinceInMilliseconds the time (measured as a difference between the timestamp and midnight, January 1, 1970 UTC)
     *                            since which the modified worklogs will be returned.
     * @param maxResults          the maximum number of worklogs to return.
     * @return the list of worklogs updated after provided time.
     */
    List<Worklog> getWorklogsUpdateSince(Long sinceInMilliseconds, int maxResults);

    /**
     * Returns {@link com.atlassian.jira.issue.worklog.WorklogManager#WORKLOG_UPDATE_DATA_PAGE_SIZE} of worklog ids and removal dates,
     * which were removed after (inclusive) provided time in milliseconds.
     *
     * @param sinceInMilliseconds the time (measured as a difference between the timestamp and midnight, January 1, 1970 UTC)
     *                            since which the modified worklogs will be returned.
     * @param maxResults          the maximum number of {@link com.atlassian.jira.bc.issue.worklog.DeletedWorklog} to return.
     * @return ids and removal dates of worklogs which were deleted since provided time.
     */
    List<DeletedWorklog> getWorklogsDeletedSince(Long sinceInMilliseconds, int maxResults);

    /**
     * Returns a set of worklogs for provided ids.
     *
     * @param worklogIds set of worklog ids.
     * @param maxResults the maximu number of {@link com.atlassian.jira.issue.worklog.Worklog} to return.
     * @return a list of worklogs for provided ids.
     */
    Set<Worklog> getWorklogsForIds(Set<Long> worklogIds, int maxResults);
}
