package com.atlassian.jira.issue.context.persistence;

import com.atlassian.annotations.Internal;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.fields.config.ConfigurationContext;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.FieldConfigSchemeIssueType;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Caching decorator for {@link FieldConfigContextPersister}. This corresponds to the <code>configurationcontext</code>
 * table, which is essentially an association table between {@link com.atlassian.jira.issue.fields.CustomField
 * CustomField} and either a {@link ProjectCategory} or a {@link Project}, but not both (in practice it is always a
 * Project). Each association also has {@link FieldConfigScheme} as a property of the association, and this is where
 * things like default values for custom fields are ultimately stored. When both the project and projectCategory are
 * null, then that database row is in fact a special row holding the FieldConfigScheme for the "Global Context".
 * <p>
 * See <a href="https://extranet.atlassian.com/x/koEPJg">CustomField Configuration - DB Entity Model</a> for a more
 * in-depth explanation of how this all works.
 *
 * @since v5.1
 */
@Internal
@EventComponent
public class CachingFieldConfigContextPersister implements FieldConfigContextPersister, Startable {
    private static final Logger log = LoggerFactory.getLogger(CachingFieldConfigContextPersister.class);

    /**
     * A cache of FieldId -> FieldConfigContextMap.
     * Given a Project, the FieldConfigContextMap will tell us which FieldConfigSchemeId is relevant.
     */
    private final Cache<String, FieldConfigContextMap> fieldContextCache;
    /**
     * A cache of FieldConfigScheme ID -> Issue Types in context.
     * Given a FieldConfigSchemeId, we can find if the config is relevant for a given Issue Type.
     */
    private final Cache<Long, IssueTypeSet> configSchemeIssueTypeCache;

    /**
     * The real FieldConfigContextPersisterImpl.
     */
    private final FieldConfigContextPersisterWorker worker;
    private final OfBizDelegator ofBizDelegator;

    /**
     * Creates a new CachingFieldConfigContextPersister that wraps a new FieldConfigContextPersisterImpl instance.
     *
     * @param delegator      the OfBizDelegator
     * @param projectManager the ProjectManager
     */
    @SuppressWarnings("UnusedDeclaration")
    public CachingFieldConfigContextPersister(OfBizDelegator delegator, ProjectManager projectManager, CacheManager cacheManager) {
        this.ofBizDelegator = delegator;
        this.worker = new FieldConfigContextPersisterWorker(delegator, projectManager, cacheManager);
        fieldContextCache = cacheManager.getCache(getClass().getName() + ".fieldContextCache",
                this::fieldConfigContextLoader,
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).flushable().build());

        configSchemeIssueTypeCache = cacheManager.getCache(getClass().getName() + ".configSchemeIssueTypeCache",
                this::configSchemeIssueTypeLoader,
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).flushable().build());
    }

    /**
     * Registers this CachingFieldConfigContextPersister's cache in the JIRA instrumentation.
     *
     * @throws Exception
     */
    @Override
    public void start() throws Exception {
    }

    /**
     * Clears this CachingFieldConfigContextPersister's cache upon receiving a ClearCacheEvent.
     *
     * @param clearCacheEvent a ClearCacheEvent
     */
    @EventListener
    @SuppressWarnings("UnusedParameters")
    public void onClearCache(ClearCacheEvent clearCacheEvent) {
        invalidateAll();
    }

    @Override
    public List<JiraContextNode> getAllContextsForConfigScheme(FieldConfigScheme fieldConfigScheme) {
        return worker.getAllContextsForConfigScheme(fieldConfigScheme);
    }

    @Override
    public void removeContextsForConfigScheme(@Nonnull FieldConfigScheme fieldConfigScheme) {
        worker.removeContextsForConfigScheme(fieldConfigScheme);
        if (fieldConfigScheme.getField() != null) {
            fieldContextCache.remove(fieldConfigScheme.getField().getId());
            configSchemeIssueTypeCache.remove(fieldConfigScheme.getId());
        }
    }

    @Override
    public void removeContextsForProject(final Project project) {
        worker.removeContextsForProject(project);
        invalidateAll();
    }

    @Override
    @Nullable
    public Long getRelevantConfigSchemeId(@Nonnull final IssueContext issueContext, @Nonnull final String fieldId) {
        // Find the scheme for the given Project
        Long schemeId = getRelevantConfigSchemeId(issueContext.getProjectId(), fieldId);
        if (schemeId != null) {
            // Now test that it is relevant for this Issue Type
            IssueTypeSet issueTypeSet = configSchemeIssueTypeCache.get(schemeId);
            if (issueTypeSet == null) {
                // should never happen
                throw new IllegalStateException("configSchemeIssueTypeCache failed to retrieve IssueTypeSet for schemeId " + schemeId);
            }
            if (issueTypeSet.includesIssueType(issueContext.getIssueTypeId())) {
                return schemeId;
            }
        }
        return null;
    }

    @Override
    @Nullable
    public Long getRelevantConfigSchemeId(@Nullable final Long projectId, @Nonnull final String fieldId) {
        // Get all the FieldConfigContexts for this Custom Field
        FieldConfigContextMap fieldConfigContextMap = fieldContextCache.get(fieldId);
        if (fieldConfigContextMap == null) {
            // should never happen
            throw new IllegalStateException("fieldContextCache failed to retrieve FieldConfigContextMap for fieldId " + fieldId);
        }
        // Find the scheme for the given Project
        return fieldConfigContextMap.getConfigSchemeIdForProject(projectId);
    }

    @Override
    public void store(final String fieldId, final JiraContextNode contextNode, final FieldConfigScheme fieldConfigScheme) {
        store(fieldId, Arrays.asList(contextNode), fieldConfigScheme);
    }

    @Override
    public void store(final String fieldId, final Collection<? extends JiraContextNode> contextNodes, final FieldConfigScheme fieldConfigScheme) {
        // Find all the old scheme Ids for eventual flushing
        @SuppressWarnings("ConstantConditions") // can't really NPE for this cache type
        Set<Long> uniqueSchemeIds = fieldContextCache.get(fieldId).getUniqueSchemeIds();
        // The actual store in the DB
        for (JiraContextNode contextNode : contextNodes) {
            worker.store(fieldId, contextNode, fieldConfigScheme);
        }
        // now flush caches
        fieldContextCache.remove(fieldId);
        uniqueSchemeIds.forEach(configSchemeIssueTypeCache::remove);
    }

    /**
     * Clears this instance's cache.
     */
    private void invalidateAll() {
        worker.invalidateAll();
        fieldContextCache.removeAll();
        configSchemeIssueTypeCache.removeAll();
        if (log.isTraceEnabled()) {
            log.trace("called invalidateAll()", new Throwable());
        }
    }

    @Nonnull
    private FieldConfigContextMap fieldConfigContextLoader(final String fieldId) {
        // Get all the project -> FieldConfigContext ID mappings
        final List<ConfigurationContext> configurationContexts =
                Select.from(Entity.CONFIGURATION_CONTEXT)
                        .whereEqual(ConfigurationContext.FIELD_ID, fieldId)
                        .runWith(ofBizDelegator)
                        .asList();

        final Map<Long, Long> projectIdToSchemeIdMap = new HashMap<>(configurationContexts.size());

        for (final ConfigurationContext configurationContext : configurationContexts) {
            // Global will also go in here with a NULL project ID.
            projectIdToSchemeIdMap.put(configurationContext.getProjectId(), configurationContext.getFieldConfigSchemeId());
        }

        // Finally create our FieldConfigContextMap
        return new FieldConfigContextMap(projectIdToSchemeIdMap);
    }

    private static class FieldConfigContextMap {
        private final Map<Long, Long> projectIdToSchemeIdMap;

        private FieldConfigContextMap(final Map<Long, Long> projectIdToSchemeIdMap) {
            this.projectIdToSchemeIdMap = projectIdToSchemeIdMap;
        }

        private Long getConfigSchemeIdForProject(@Nullable final Long projectId) {
            // Handle null projectId as asking for global configs
            Long schemeId = projectIdToSchemeIdMap.get(projectId);

            if (schemeId == null && projectId != null) {
                // no scheme for this particular project - get the global context
                schemeId = projectIdToSchemeIdMap.get(null);
            }
            return schemeId;
        }

        private Set<Long> getUniqueSchemeIds() {
            return new HashSet<>(projectIdToSchemeIdMap.values());
        }
    }

     @Nonnull
    public IssueTypeSet configSchemeIssueTypeLoader(final Long schemeId) {
        final List<FieldConfigSchemeIssueType> issueTypeConfigs = Select.from(Entity.FIELD_CONFIG_SCHEME_ISSUE_TYPE)
                .whereEqual(FieldConfigSchemeIssueType.FIELD_CONFIG_SCHEME_ID, schemeId)
                .runWith(ofBizDelegator)
                .asList();
        return IssueTypeSet.from(issueTypeConfigs);
    }

    private static class IssueTypeSet {
        private final Set<String> issueTypeIds;

        public IssueTypeSet(final Set<String> issueTypeIds) {
            this.issueTypeIds = issueTypeIds;
        }

        @Nonnull
        public static IssueTypeSet from(final List<FieldConfigSchemeIssueType> issueTypeConfigs) {
            final Set<String> issueTypeIds = new HashSet<>(issueTypeConfigs.size());
            for (FieldConfigSchemeIssueType issueTypeConfig : issueTypeConfigs) {
                if (issueTypeConfig.getIssueTypeId() == null) {
                    // Context for all IssueTypes
                    return new IssueTypeSet(null);
                }
                issueTypeIds.add(issueTypeConfig.getIssueTypeId());
            }
            return new IssueTypeSet(issueTypeIds);
        }

        public boolean includesIssueType(final String issueTypeId) {
            // issueTypeIds == null means "All Issue Types"
            // JRA-40663 Some old data has "-1" instead of NULL for "All Issue Types"
            return issueTypeIds == null || issueTypeIds.contains("-1") || issueTypeIds.contains(issueTypeId);
        }
    }
}
