package com.atlassian.jira.model.querydsl;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.ofbiz.core.entity.GenericValue;

@Internal
public interface DTO {
    GenericValue toGenericValue(final OfBizDelegator ofBizDelegator);
}
