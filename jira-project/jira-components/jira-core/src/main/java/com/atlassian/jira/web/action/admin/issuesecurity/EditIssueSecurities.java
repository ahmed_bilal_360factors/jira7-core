package com.atlassian.jira.web.action.admin.issuesecurity;

import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.issue.security.IssueSecurityLevelPermission;
import com.atlassian.jira.issue.security.IssueSecurityLevelService;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.issue.security.ProjectIssueSecuritySchemeHelper;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.security.SecurityTypeManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.ofbiz.core.entity.GenericValue;

import java.util.List;

@SuppressWarnings("UnusedDeclaration")
@WebSudoRequired
public class EditIssueSecurities extends SchemeAwareIssueSecurityAction {

    private String name;
    private String description;
    private Long levelId;
    private List<Project> projects;

    private final ProjectIssueSecuritySchemeHelper helper;
    private final IssueSecurityLevelManager issueSecurityLevelManager;
    private final IssueSecurityLevelService issueSecurityLevelService;

    public EditIssueSecurities(IssueSecuritySchemeManager issueSecuritySchemeManager, SecurityTypeManager issueSecurityTypeManager,
                               IssueSecurityLevelManager issueSecurityLevelManager, ProjectIssueSecuritySchemeHelper helper,
                               IssueSecurityLevelService issueSecurityLevelService) {
        super(issueSecuritySchemeManager, issueSecurityTypeManager);
        this.issueSecurityLevelManager = issueSecurityLevelManager;
        this.issueSecurityLevelService = issueSecurityLevelService;
        this.helper = helper;
    }

    public List<IssueSecurityLevel> getSecurityLevels() {
        return issueSecurityLevelManager.getIssueSecurityLevels(getSchemeId());
    }

    public List<IssueSecurityLevelPermission> getSecurities(IssueSecurityLevel issueSecurityLevel) {
        return issueSecuritySchemeManager.getPermissionsBySecurityLevel(issueSecurityLevel.getId());
    }


    @RequiresXsrfCheck
    public String doAddLevel() throws Exception {
        IssueSecurityLevelService.CreateValidationResult vr = issueSecurityLevelService.validateCreate(getLoggedInUser(), getSchemeId(), getName(), getDescription());
        if (vr.isValid()) {
            ServiceResult result = issueSecurityLevelService.create(getLoggedInUser(), vr);
        } else {
            addErrorCollection(vr.getErrors());
        }
        return getRedirect(getRedirectURL());
    }

    @RequiresXsrfCheck
    public String doEditLevel() throws Exception {
        return getRedirect(getRedirectURL());
    }

    @RequiresXsrfCheck
    public String doMakeDefaultLevel() throws Exception {
        GenericValue scheme = getScheme();

        if (scheme != null) {
            if ((new Long(-1).equals(levelId))) { // -1 sets default to "none"
                scheme.set("defaultlevel", null);
            } else {
                scheme.set("defaultlevel", levelId);
            }
            issueSecuritySchemeManager.updateScheme(scheme);
        }
        return getRedirect(getRedirectURL());
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getLevelId() {
        return levelId;
    }

    public void setLevelId(Long levelId) {
        this.levelId = levelId;
    }

    public String getRedirectURL() {
        return "EditIssueSecurities!default.jspa?schemeId=" + getSchemeId();
    }

    public List<Project> getUsedIn() {
        if (projects == null) {
            final Scheme issueSecurityScheme = getSchemeObject();
            projects = helper.getSharedProjects(issueSecurityScheme);
        }
        return projects;
    }
}

