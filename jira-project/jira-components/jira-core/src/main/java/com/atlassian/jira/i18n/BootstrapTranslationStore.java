package com.atlassian.jira.i18n;

import com.atlassian.jira.util.resourcebundle.DefaultResourceBundle;
import com.atlassian.jira.web.bean.i18n.TranslationStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Simple TranslationStore to be used in the Bootstrap container, because it is needed to show start up errors.
 * <p>
 * This is a very thin wrapper around a ResourceBundle for the given Locale.
 * </p>
 * See JRA-43308
 *
 * @since v6.4.5
 */
public class BootstrapTranslationStore implements TranslationStore {
    private static final Logger LOG = LoggerFactory.getLogger(BootstrapTranslationStore.class);

    private final ResourceBundle resourceBundle;

    public BootstrapTranslationStore(final Locale locale) {
        resourceBundle = DefaultResourceBundle.getDefaultResourceBundle(locale);
    }

    @Override
    public String get(final String key) {
        try {
            return resourceBundle.getString(key);
        } catch (MissingResourceException mre) {
            LOG.warn("No bootstrap translation available for '{}'", key);
            LOG.debug("Missing resource: {}", key, mre);
            return key;
        }
    }

    @Override
    public boolean containsKey(final String key) {
        return resourceBundle.containsKey(key);
    }

    @Override
    public Iterable<String> keys() {
        return resourceBundle.keySet();
    }
}
