package com.atlassian.jira.issue.fields.rest.json.dto;

import com.atlassian.core.util.FileSize;
import com.atlassian.core.util.thumbnail.Thumbnail;
import com.atlassian.jira.bc.issue.attachment.FileNameBasedVersionedAttachmentsList;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.issue.AttachmentIndexManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.thumbnail.ThumbnailManager;
import com.atlassian.jira.issue.thumbnail.ThumbnailedImage;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.JiraUrlCodec;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadableInstant;

import java.net.URI;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @since v6.5
 */
public final class AttachmentViewDtoConverterImpl implements AttachmentViewDtoConverter {
    private final ThumbnailManager thumbnailManager;
    private final JiraAuthenticationContext authenticationContext;
    private final ApplicationProperties applicationProperties;
    private final AttachmentIndexManager attachmentIndexManager;
    private final IssueManager issueManager;
    private final PermissionManager permissionManager;
    private final DateTimeFormatter dateTimeFormatter;
    private final UserManager userManager;

    public AttachmentViewDtoConverterImpl(final ThumbnailManager thumbnailManager, final JiraAuthenticationContext authenticationContext, final ApplicationProperties applicationProperties, final AttachmentIndexManager attachmentIndexManager, final IssueManager issueManager, final PermissionManager permissionManager, final DateTimeFormatter dateTimeFormatter, final UserManager userManager) {
        this.thumbnailManager = thumbnailManager;
        this.authenticationContext = authenticationContext;
        this.applicationProperties = applicationProperties;
        this.attachmentIndexManager = attachmentIndexManager;
        this.issueManager = issueManager;
        this.permissionManager = permissionManager;
        this.dateTimeFormatter = dateTimeFormatter;
        this.userManager = userManager;
    }

    @Override
    public List<AttachmentViewJsonDto> convert(final List<Attachment> attachments) {
        final FileNameBasedVersionedAttachmentsList attachmentsList = new FileNameBasedVersionedAttachmentsList(attachments);

        final ApplicationUser loggedInUser = authenticationContext.getUser();
        final DateTimeFormatter userDateTimeFormatter = dateTimeFormatter
                .forUser(authenticationContext.getLoggedInUser())
                .withStyle(DateTimeStyle.COMPLETE);

        final Iterable<AttachmentViewJsonDto> extendedBeans = Iterables.transform(attachments, new Function<Attachment, AttachmentViewJsonDto>() {
            /**
             * Cache of Issue object by issue ID.
             * Most likely all the attachments are from the same issue, so
             * we only need to query for the Issue object once. Cache them here.
             */
            private Map<Long, Issue> issueById = new HashMap<Long, Issue>(1);

            /**
             * @param attachment the attachment
             * @return the issue to which this attachment is attached
             */
            private Issue getIssue(Attachment attachment) {
                Long issueId = attachment.getIssueId();
                Issue issue = issueById.get(issueId);
                if (issue == null) {
                    issue = attachment.getIssue();
                    issueById.put(issueId, issue);
                }
                return issue;
            }

            @Override
            public AttachmentViewJsonDto apply(final Attachment attachment) {
                final Issue issue = getIssue(attachment);
                final String urlEncodedFileName = JiraUrlCodec.encode(attachment.getFilename(), true);

                final Thumbnail thumbnail = thumbnailManager.getThumbnail(issue, attachment);
                final ThumbnailedImage thumbnailedImage = thumbnailManager.toThumbnailedImage(thumbnail);
                final URI thumbnailUrl;
                final int thumbnailedImageWidth;
                final int thumbnailedImageHeight;
                if (thumbnailedImage != null) {
                    thumbnailUrl = URI.create(thumbnailedImage.getImageURL());
                    thumbnailedImageWidth = thumbnailedImage.getWidth();
                    thumbnailedImageHeight = thumbnailedImage.getHeight();
                } else {
                    thumbnailUrl = null;
                    thumbnailedImageWidth = 0;
                    thumbnailedImageHeight = 0;
                }

                final URI attachmentUrl = URI.create(String.format("secure/attachment/%s/%s", attachment.getId(), urlEncodedFileName));
                final ApplicationUser author = userManager.getUserByKeyEvenWhenUnknown(attachment.getAuthorKey());

                final Timestamp createdTimestamp = attachment.getCreated();
                final ReadableInstant created = new DateTime(
                        createdTimestamp.getTime(),
                        DateTimeZone.forTimeZone(userDateTimeFormatter.getZone())
                );
                return new AttachmentViewJsonDto(
                        attachment.getId(),
                        attachmentsList.isLatestVersion(attachment),
                        canDeleteAttachment(issue, attachment, loggedInUser),
                        getZipSupport() && attachmentIndexManager.isExpandable(attachment),
                        thumbnailUrl,
                        thumbnailedImageWidth,
                        thumbnailedImageHeight,
                        attachmentUrl,
                        author != null ? author.getDisplayName() : null,
                        attachment.getAuthorKey(),
                        FileSize.format(attachment.getFilesize()),
                        attachment.getFilename(),
                        attachment.getMimetype(),
                        created,
                        userDateTimeFormatter.format(createdTimestamp)
                );
            }
        });

        return ImmutableList.<AttachmentViewJsonDto>builder().addAll(extendedBeans).build();
    }

    private boolean getZipSupport() {
        return applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOW_ZIP_SUPPORT);
    }

    private boolean canDeleteAttachment(Issue issue, Attachment attachment, ApplicationUser user) {
        return issueManager.isEditable(issue)
                && (permissionManager.hasPermission(Permissions.ATTACHMENT_DELETE_ALL, issue, user)
                || (permissionManager.hasPermission(Permissions.ATTACHMENT_DELETE_OWN, issue, user) && isUserAttachmentAuthor(attachment, user)));

    }

    private boolean isUserAttachmentAuthor(Attachment attachment, ApplicationUser user) {
        ApplicationUser attachmentAuthor = attachment.getAuthorObject();

        if (attachmentAuthor == null && user == null) {
            return true;
        } else if (attachmentAuthor == null || user == null) {
            return false;
        }

        return attachmentAuthor.equals(user);
    }
}
