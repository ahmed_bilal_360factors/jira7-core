package com.atlassian.jira.model.querydsl;

import com.querydsl.core.types.dsl.*;
import com.querydsl.sql.ColumnMetadata;

import javax.annotation.Generated;
import java.sql.Types;

/**
 * QOSPropertyEntry is a Querydsl query object.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QOSPropertyEntry extends JiraRelationalPathBase<OSPropertyEntryDTO> {
    public static final QOSPropertyEntry O_S_PROPERTY_ENTRY = new QOSPropertyEntry("O_S_PROPERTY_ENTRY");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final StringPath entityName = createString("entityName");
    public final NumberPath<Long> entityId = createNumber("entityId", Long.class);
    public final StringPath propertyKey = createString("propertyKey");
    public final NumberPath<Integer> type = createNumber("type", Integer.class);

    public QOSPropertyEntry(String alias) {
        super(OSPropertyEntryDTO.class, alias, "propertyentry");
        addMetadata();
    }

    private void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(entityName, ColumnMetadata.named("entity_name").withIndex(2).ofType(Types.VARCHAR).withSize(255));
        addMetadata(entityId, ColumnMetadata.named("entity_id").withIndex(3).ofType(Types.NUMERIC).withSize(18));
        addMetadata(propertyKey, ColumnMetadata.named("property_key").withIndex(4).ofType(Types.VARCHAR).withSize(255));
        addMetadata(type, ColumnMetadata.named("propertytype").withIndex(5).ofType(Types.NUMERIC).withSize(9));
    }

    /**
     * Returns the ofbiz model's entity name for this query object.
     *
     * @return the ofbiz model's entity name for this query object.
     */
    @Override
    public String getEntityName() {
        return "OSPropertyEntry";
    }

    @Override
    public NumberPath<Long> getNumericIdPath() {
        return id;
    }
}

