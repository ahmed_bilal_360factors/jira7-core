package com.atlassian.jira.util.system;

import com.atlassian.jdk.utilities.runtimeinformation.MemoryInformation;
import com.atlassian.jdk.utilities.runtimeinformation.RuntimeInformation;
import com.atlassian.jdk.utilities.runtimeinformation.RuntimeInformationFactory;
import com.atlassian.jira.config.properties.SystemPropertyKeys;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.management.ManagementFactory;
import java.util.List;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.lang.Math.max;

/**
 * Factory for getting RuntimeInformation that supports reporting in megabytes rather than just bytes.
 *
 * @since v4.0
 */
public class JiraRuntimeInformationFactory {
    private static final Logger log = LoggerFactory.getLogger(JiraRuntimeInformationFactory.class);

    public static RuntimeInformation getRuntimeInformation() {
        return new SanitisedRuntimeInformation(RuntimeInformationFactory.getRuntimeInformation());
    }

    public static RuntimeInformation getRuntimeInformationInMegabytes() {
        return new MegaByteRuntimeInformation(getRuntimeInformation());
    }

    private static class MegaByteRuntimeInformation implements RuntimeInformation {
        private final RuntimeInformation info;

        public MegaByteRuntimeInformation(final RuntimeInformation info) {
            this.info = info;
        }

        public long getTotalHeapMemory() {
            return info.getTotalHeapMemory();
        }

        public long getTotalHeapMemoryUsed() {
            return info.getTotalHeapMemoryUsed();
        }

        public String getJvmInputArguments() {
            return info.getJvmInputArguments();
        }

        public List<MemoryInformation> getMemoryPoolInformation() {
            return info.getMemoryPoolInformation().stream()
                    .map(MemoryInformationInMegabytes::new)
                    .collect(Collectors.toList());
        }

        public long getTotalPermGenMemory() {
            return info.getTotalPermGenMemory();
        }

        public long getTotalPermGenMemoryUsed() {
            return info.getTotalPermGenMemoryUsed();
        }

        public long getTotalNonHeapMemory() {
            return info.getTotalNonHeapMemory();
        }

        public long getTotalNonHeapMemoryUsed() {
            return info.getTotalNonHeapMemoryUsed();
        }
    }

    private static class MemoryInformationInMegabytes implements MemoryInformation {
        private static final int MEGABYTE = 1024 * 1024;

        private final MemoryInformation info;

        public MemoryInformationInMegabytes(final MemoryInformation info) {
            this.info = info;
        }

        public String getName() {
            return info.getName();
        }

        public long getTotal() {
            return max(getUsed(), inMegabytes(info.getTotal()));
        }

        public long getUsed() {
            // This fails on some IBM JDKs due to a bug in the MemoryInfoBean refer Jira issue JRA-19389
            try {
                return inMegabytes(info.getUsed());
            } catch (RuntimeException e) {
                log.warn("Memory pool info returned by the java runtime is invalid for pool " + this.getName());
                log.debug(e.getMessage(), e);
                return -1;
            }
        }

        public long getFree() {
            return getTotal() - getUsed();
        }

        private static long inMegabytes(final double bytes) {
            return Math.round(bytes / MEGABYTE);
        }
    }

    @VisibleForTesting
    static class SanitisedRuntimeInformation implements RuntimeInformation {
        private static final Pattern PASSWORD_PATTERN = Pattern.compile(
                "-D" + Pattern.quote(SystemPropertyKeys.RECOVERY_PASSWORD) + "\\s*=.*", Pattern.CASE_INSENSITIVE);

        private final RuntimeInformation information;
        private final Supplier<List<String>> arguments;

        private SanitisedRuntimeInformation(final RuntimeInformation information) {
            this(information, () -> ManagementFactory.getRuntimeMXBean().getInputArguments());
        }

        @VisibleForTesting
        SanitisedRuntimeInformation(final RuntimeInformation information, final Supplier<List<String>> arguments) {
            this.information = information;
            this.arguments = arguments;
        }

        @Override
        public long getTotalHeapMemory() {
            return information.getTotalHeapMemory();
        }

        @Override
        public long getTotalHeapMemoryUsed() {
            return information.getTotalHeapMemoryUsed();
        }

        @Override
        public String getJvmInputArguments() {
            return arguments.get().stream()
                    .map(SanitisedRuntimeInformation::sanitiseArgument)
                    .collect(Collectors.joining(" "));
        }

        @Override
        public List<MemoryInformation> getMemoryPoolInformation() {
            return information.getMemoryPoolInformation();
        }

        @Override
        public long getTotalPermGenMemory() {
            return information.getTotalPermGenMemory();
        }

        @Override
        public long getTotalPermGenMemoryUsed() {
            return information.getTotalPermGenMemoryUsed();
        }

        @Override
        public long getTotalNonHeapMemory() {
            return information.getTotalNonHeapMemory();
        }

        @Override
        public long getTotalNonHeapMemoryUsed() {
            return information.getTotalNonHeapMemoryUsed();
        }

        private static String sanitiseArgument(String argument) {
            if (argument != null && PASSWORD_PATTERN.matcher(argument).matches()) {
                return String.format("-D%s=****", SystemPropertyKeys.RECOVERY_PASSWORD);
            } else {
                return argument;
            }
        }
    }
}
