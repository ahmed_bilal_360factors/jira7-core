package com.atlassian.jira.database;

import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.model.querydsl.JiraRelationalPathBase;
import com.querydsl.sql.RelationalPath;
import com.querydsl.sql.SQLQuery;
import com.querydsl.sql.SQLTemplates;
import com.querydsl.sql.dml.SQLDeleteClause;
import com.querydsl.sql.dml.SQLUpdateClause;
import org.ofbiz.core.entity.DelegatorInterface;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @since v6.4
 */
public class DbConnectionImpl implements DbConnection {
    private final Connection con;
    private final SQLTemplates dialect;
    private final DelegatorInterface genericDelegator;

    DbConnectionImpl(final Connection con, final SQLTemplates dialect, final DelegatorInterface genericDelegator) {
        this.con = con;
        this.dialect = dialect;
        this.genericDelegator = genericDelegator;
    }

    DbConnectionImpl(DatabaseConnection databaseConnection, SQLTemplates dialect, DelegatorInterface delegatorInterface) {
        this(databaseConnection.getJdbcConnection(), dialect, delegatorInterface);
    }

    @Override
    public Connection getJdbcConnection() {
        return con;
    }

    @Override
    public SQLQuery newSqlQuery() {
        return new SQLQuery(con, dialect);
    }

    @Override
    public SQLUpdateClause update(final RelationalPath<?> entity) {
        return new SQLUpdateClause(con, dialect, entity);
    }

    @Override
    public IdGeneratingSQLInsertClause insert(final JiraRelationalPathBase<?> entity) {
        return new IdGeneratingSQLInsertClause(con, dialect, entity, genericDelegator);
    }

    @Override
    public SQLDeleteClause delete(final RelationalPath<?> entity) {
        return new SQLDeleteClause(con, dialect, entity);
    }

    @Override
    public void setAutoCommit(final boolean autoCommit) {
        try {
            con.setAutoCommit(autoCommit);
        } catch (SQLException ex) {
            throw new DataAccessException(ex);
        }
    }

    @Override
    public void commit() {
        try {
            con.commit();
        } catch (SQLException ex) {
            throw new DataAccessException(ex);
        }
    }

    @Override
    public void rollback() {
        try {
            con.rollback();
        } catch (SQLException ex) {
            throw new DataAccessException(ex);
        }
    }

}
