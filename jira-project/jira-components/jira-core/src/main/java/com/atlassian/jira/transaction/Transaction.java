package com.atlassian.jira.transaction;

import javax.annotation.Nonnull;

/**
 * This represents the state of a running transaction that can be comitted or rolled back
 *
 * @since v4.4.1
 */
public interface Transaction {
    /**
     * This will commit the transaction.
     *
     * @throws TransactionRuntimeException if the transaction cannot be commited
     */
    void commit() throws TransactionRuntimeException;

    /**
     * This will rollback the transaction.
     *
     * @throws TransactionRuntimeException if the transaction cannot be rollbacked
     */
    void rollback() throws TransactionRuntimeException;

    /**
     * This is designed to be called in a top level finally block and it will attempt to rollback the transaction IF it
     * has not already been committed.  This allows you to simplify your try/catch/finally blocks in and around
     * transaction code.
     * <p>
     * Note it DOES NOT throw a {@link TransactionRuntimeException) if the roll back cannot be performed.  Since this is
     * intended to be inside a finally block it most likely to late to know about this you probably dont want to do
     * anything since rollback hasnt worked either.  So it logs this condition is an ignores it to make calling code
     * more simple.
     */
    void finallyRollbackIfNotCommitted();


    /**
     * Returns if this is a new transaction.
     * A nested transaction is not new and its behaviour for commit and rollback calls is not performed immediately.
     * <p>Calls to commit for a nested transaction effectively do nothing relying on the outer transaction to perform the commit.</p>
     * <p>Calls to rollback for a nested transaction mark the outer transaction as requiring to be rolled back. The outer transaction
     * will eventually rollback, whether commit or rollback is eventually called.</p>
     *
     * @return if this represents a new transaction.
     * and hence whether calling {@link #commit()} or {@link #rollback()}
     * will actually do anything
     */
    boolean isNewTransaction();

    /**
     * This will enqueue a runnable that will be run if the transaction successfully commits.  All enqueued
     * runnable code will be run in enqueued order.  If the transaction rolls back then no code is run.
     * NO exceptions will bubble out of them into the upper Transaction code.
     *
     * @param runnable the code to run if the transaction commits successfully
     */
    void runAfterSuccessfulCommit(@Nonnull Runnable runnable);
}
