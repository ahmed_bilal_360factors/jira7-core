package com.atlassian.jira.issue.export.customfield;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.util.I18nHelper;


public class DefaultCustomFieldExportContext implements CustomFieldExportContext {
    private final CustomField customField;
    private final I18nHelper i18nHelper;
    private final String defaultColumnHeaderKey;

    @Internal
    DefaultCustomFieldExportContext(CustomField customField, I18nHelper i18nHelper, String defaultColumnHeaderKey) {
        this.customField = customField;
        this.i18nHelper = i18nHelper;
        this.defaultColumnHeaderKey = defaultColumnHeaderKey;
    }

    @Override public CustomField getCustomField() {
        return customField;
    }

    @Override public I18nHelper getI18nHelper() {
        return i18nHelper;
    }

    @Override public String getDefaultColumnHeader() {
        return i18nHelper.getText(defaultColumnHeaderKey, customField.getName());
    }
}
