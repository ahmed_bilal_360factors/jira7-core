package com.atlassian.jira.cluster;

/**
 * Start and stop clustered services
 *
 * @since v6.1
 */
public interface ClusterServicesManager {
    void startServices();

    void stopServices();
}
