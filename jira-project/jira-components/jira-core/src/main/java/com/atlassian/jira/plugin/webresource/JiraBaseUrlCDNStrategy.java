package com.atlassian.jira.plugin.webresource;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.plugin.webresource.cdn.CDNStrategy;
import com.atlassian.plugin.webresource.prebake.PrebakeConfig;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nullable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Optional;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * CDN Strategy for JIRA that produces a dynamic base URL. This will transform CDN-able resources to be served from
 * one or more hosts, with the app's hostname as the first section of the URL path. For example, {@code /my/resource.js}
 * will be transformed to be served from {@code //node-1.cdnhost.com/my.jirahost.com/my/resource.js} or
 * {@code //node-2.cdnhost.com/my.jirahost.com/my/resource.js} (depending on the number of hosts configured).
 * <p>
 * <ul>
 * <li>
 * Set the dark feature {@code jira.baseurl.cdn.enabled} to true to enable
 * </li>
 * <li>
 * Set the system property {@code jira.baseurl.cdn.prefix} to the set of CDN prefixes to use (comma separated),
 * e.g. {@code //node-1.cdnhost.com} or {@code //node-1.cdnhost.com,//node-2.cdnhost.com}. This must not end
 * with a trailing slash. The JIRA hostname (determined by the {@code jira.baseurl} application property) is
 * appended to the prefix as the first path component, i.e. {@code //my.cdnhost.com/my.jirahost.com}.
 * </li>
 * </ul>
 * <p>
 * The benefit of this strategy over {@link com.atlassian.jira.plugin.webresource.JiraPrefixCDNStrategy} is that it
 * allows the JIRA hostname to be changed at runtime and for that to be dynamically incorporated into the CDN base URL
 * to one or more CDN proxies to service multiple JIRA instances.
 *
 * @since v6.4
 */
public class JiraBaseUrlCDNStrategy implements CDNStrategy {
    static final String ENABLED_FEATURE_KEY = "jira.baseurl.cdn.enabled";
    static final String PREFIX_SYSTEM_PROPERTY = "jira.baseurl.cdn.prefix";

    private final JiraProperties jiraSystemProperties = JiraSystemProperties.getInstance();
    private final ApplicationProperties applicationProperties;
    private final Optional<PrebakeConfig> prebakeConfig;

    public JiraBaseUrlCDNStrategy(final ApplicationProperties applicationProperties, final Optional<PrebakeConfig> prebakeConfig) {
        this.applicationProperties = notNull("applicationProperties", applicationProperties);
        this.prebakeConfig = prebakeConfig;
    }

    @Override
    public boolean supportsCdn() {
        return getPrefixes().length > 0 && getAuthority() != null;
    }

    @Override
    public String transformRelativeUrl(String relativeUrl) {
        return getPrefix(relativeUrl) + relativeUrl;
    }

    /**
     * Get a CDN prefix for a relative URL.
     * <p>
     * The intention of this CDN strategy is to spread resources across a set of CDN domains (prefixes), so that the
     * browser's maximum connections-per-domain limit can be side-stepped. We don't want to randomly choose a prefix, as
     * browsers will not cache resources across multiple domains. Instead we want to deterministically choose a prefix
     * for each resource.
     *
     * @param relativeUrl
     * @return the CDN prefix to use
     */
    @Nullable
    private String getPrefix(String relativeUrl) {
        String[] prefixes = getPrefixes();
        if (prefixes.length == 0) {
            return null;
        }

        String prefix = prefixes[Math.abs(stringHashCode(relativeUrl)) % prefixes.length];

        if (jiraSystemProperties.isDevMode()) {
            return prefix;
        }
        return prefix + "/" + getAuthority();
    }

    @Nullable
    private String getAuthority() {
        // JIRA's base url is pretty ghetto, it's a string that contains the URL scheme and trailing path eg
        // "http://dogeydoge.atlassian.net/the/path/to/jira". We need only the URL authority here (in particular,
        // we don't want the trailing path).
        String baseUrl = applicationProperties.getString(APKeys.JIRA_BASEURL);

        try {
            return new URL(baseUrl).getAuthority();
        } catch (MalformedURLException ex) {
            return null;
        }
    }

    private String[] getPrefixes() {
        String prefixesString = jiraSystemProperties.getProperty(PREFIX_SYSTEM_PROPERTY);
        if (null == prefixesString) {
            return new String[0];
        } else {
            return Arrays.asList(prefixesString.split(",")).stream()
                    .filter(StringUtils::isNotBlank)
                    .toArray(String[]::new);
        }
    }

    /**
     * A hash function for String that provides cross-JVM consistent hashing.
     *
     * @param value a string to hash
     * @return a hash for a string
     */
    private static int stringHashCode(String value) {
        int hash = 0;
        for (int i = 0, length = value.length(); i < length; i++) {
            hash = 31 * hash + value.charAt(i);
        }
        return hash;
    }

    @Override
    public Optional<PrebakeConfig> getPrebakeConfig() {
        return prebakeConfig;
    }
}
