package com.atlassian.jira.config;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Effect;
import com.atlassian.fugue.Option;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.event.config.IssueTypeDeletedEvent;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.TextIssueConstant;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.status.ClearStatusCacheEvent;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.model.querydsl.IssueTypeDTO;
import com.atlassian.jira.model.querydsl.PriorityDTO;
import com.atlassian.jira.model.querydsl.QIssueType;
import com.atlassian.jira.model.querydsl.ResolutionDTO;
import com.atlassian.jira.model.querydsl.StatusDTO;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.lang.Pair;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.RelationalPath;
import org.apache.commons.lang3.StringUtils;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static com.atlassian.jira.model.querydsl.QIssueType.ISSUE_TYPE;
import static com.atlassian.jira.model.querydsl.QPriority.PRIORITY;
import static com.atlassian.jira.model.querydsl.QResolution.RESOLUTION;
import static com.atlassian.jira.model.querydsl.QStatus.STATUS;
import static com.atlassian.jira.util.ErrorCollection.Reason.CONFLICT;
import static com.atlassian.jira.util.ErrorCollection.Reason.VALIDATION_FAILED;
import static com.querydsl.core.types.Projections.bean;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.length;

@EventComponent
public class DefaultConstantsManager implements ConstantsManager {
    private static final Logger log = LoggerFactory.getLogger(DefaultConstantsManager.class);

    private CachedReference<ConstantsCache<Priority>> priorityCache;
    private CachedReference<ConstantsCache<Resolution>> resolutionCache;
    private CachedReference<ConstantsCache<Status>> statusCache;
    private CachedReference<IssueTypeCache> issueTypeCache;

    private final IssueConstant UNRESOLVED_RESOLUTION;

    private final JiraAuthenticationContext authenticationContext;
    private final OfBizDelegator ofBizDelegator;
    private final DbConnectionManager dbConnectionManager;
    private final EventPublisher eventPublisher;
    private final IssueConstantFactory issueConstantFactory;

    public DefaultConstantsManager(final JiraAuthenticationContext authenticationContext,
                                   final OfBizDelegator ofBizDelegator,
                                   final DbConnectionManager dbConnectionManager,
                                   final IssueConstantFactory issueConstantFactory,
                                   final CacheManager cacheManager,
                                   final EventPublisher eventPublisher) {
        this.dbConnectionManager = dbConnectionManager;
        this.issueConstantFactory = issueConstantFactory;
        this.authenticationContext = authenticationContext;
        this.ofBizDelegator = ofBizDelegator;
        this.eventPublisher = eventPublisher;
        UNRESOLVED_RESOLUTION = new TextIssueConstant("common.status.unresolved", "common.status.unresolved", null, authenticationContext);

        createCachedReferences(cacheManager);
    }

    private void createCachedReferences(final CacheManager cacheManager) {
        priorityCache = cacheManager.getCachedReference(getClass().getName() + ".priorityCache",
                this::loadPriorityCache);
        resolutionCache = cacheManager.getCachedReference(getClass().getName() + ".resolutionCache",
                this::loadResolutionCache);
        statusCache = cacheManager.getCachedReference(getClass().getName() + ".statusCache",
                this::loadStatusCache);
        issueTypeCache = cacheManager.getCachedReference(getClass().getName() + ".issueTypeCache",
                this::loadIssueTypeCache);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    @EventListener
    public void onClearCache(ClearCacheEvent ignored) {
        refresh();
    }

    @SuppressWarnings({"UnusedDeclaration"})
    @EventListener
    public void onIssueTypeDeleted(IssueTypeDeletedEvent event) {
        issueTypeCache.reset();
    }

    public Collection<Status> getStatuses() {
        return statusCache.get().getObjects();
    }

    public Status getStatus(String id) {
        return statusCache.get().getObject(id);
    }

    public void refreshStatuses() {
        statusCache.reset();
        eventPublisher.publish(ClearStatusCacheEvent.INSTANCE);
    }

    @Nullable
    public IssueConstant getConstantObject(String constantType, String id) {
        if (CONSTANT_TYPE.PRIORITY.getType().equalsIgnoreCase(constantType)) {
            return getPriorityObject(id);
        } else if (CONSTANT_TYPE.STATUS.getType().equalsIgnoreCase(constantType)) {
            return getStatus(id);
        } else if (CONSTANT_TYPE.RESOLUTION.getType().equalsIgnoreCase(constantType)) {
            return getResolution(id);
        } else if (CONSTANT_TYPE.ISSUE_TYPE.getType().equalsIgnoreCase(constantType)) {
            return getIssueType(id);
        }

        return null;
    }

    @Nullable
    public Collection<? extends IssueConstant> getConstantObjects(String constantType) {
        if (CONSTANT_TYPE.PRIORITY.getType().equalsIgnoreCase(constantType)) {
            return getPriorityObjects();
        } else if (CONSTANT_TYPE.STATUS.getType().equalsIgnoreCase(constantType)) {
            return getStatusObjects();
        } else if (CONSTANT_TYPE.RESOLUTION.getType().equalsIgnoreCase(constantType)) {
            return getResolutionObjects();
        } else if (CONSTANT_TYPE.ISSUE_TYPE.getType().equalsIgnoreCase(constantType)) {
            return getAllIssueTypeObjects();
        }

        return null;
    }

    @Nullable
    public List<IssueConstant> convertToConstantObjects(String constantType, Collection<?> stuff) {
        if (stuff == null || stuff.isEmpty() || constantType == null) {
            return null;
        }
        List<String> ids = new ArrayList<String>();
        for (Object o : stuff) {
            if (o instanceof GenericValue) {
                ids.add(((GenericValue) o).getString("id"));
            } else {
                ids.add((String) o);
            }
        }
        return getConstantsByIds(CONSTANT_TYPE.forTypeCaseInsensitive(constantType), ids);
    }

    private Function<String, ? extends IssueConstant> resolver(CONSTANT_TYPE constantType) {
        switch (constantType) {
            case PRIORITY:
                return this::getPriorityObject;
            case STATUS:
                return this::getStatus;
            case RESOLUTION:
                return id -> "-1".equals(id) ? UNRESOLVED_RESOLUTION : getResolution(id);
            case ISSUE_TYPE:
                return this::getIssueType;
        }

        throw new IllegalArgumentException("Constant type is not valid: " + constantType);
    }

    @Nonnull
    public List<IssueConstant> getConstantsByIds(@Nonnull CONSTANT_TYPE constantType, @Nonnull Collection<String> ids) {
        final List<IssueConstant> list = new ArrayList<IssueConstant>(ids.size());
        final Function<String, ? extends IssueConstant> resolver = resolver(constantType);

        for (String id : ids) {
            final IssueConstant constant = resolver.apply(id);
            if (constant != null) {
                list.add(constant);
            } else {
                log.debug(id + " returned a null constant of type " + constantType);
            }
        }
        return list;
    }

    public boolean constantExists(String constantType, String name) {
        return (getIssueConstantByName(constantType, name) != null);
    }

    public IssueConstant getIssueConstantByName(String constantType, String name) {
        ConstantsCache<? extends IssueConstant> constantsCache = getConstantsCache(constantType);

        for (IssueConstant issueConstant : constantsCache.getObjects()) {
            // TODO: Do we really allow null names?
            if (name == null) {
                if (issueConstant.getName() == null)
                    return issueConstant;
            } else {
                if (name.equals(issueConstant.getName())) {
                    return issueConstant;
                }
            }
        }
        // Not Found
        return null;
    }

    public IssueConstant getConstantByNameIgnoreCase(final String constantType, final String name) {
        ConstantsCache<? extends IssueConstant> constantsCache = getConstantsCache(constantType);

        for (IssueConstant issueConstant : constantsCache.getObjects()) {
            // TODO: Do we really allow null names?
            if (name == null) {
                if (issueConstant.getName() == null)
                    return issueConstant;
            } else {
                if (name.equalsIgnoreCase(issueConstant.getName())) {
                    return issueConstant;
                }
            }
        }
        // Not Found
        return null;
    }

    private ConstantsCache<? extends IssueConstant> getConstantsCache(final String constantType) {
        if (CONSTANT_TYPE.PRIORITY.getType().equalsIgnoreCase(constantType)) {
            return priorityCache.get();
        } else if (CONSTANT_TYPE.STATUS.getType().equalsIgnoreCase(constantType)) {
            return statusCache.get();
        } else if (CONSTANT_TYPE.RESOLUTION.getType().equalsIgnoreCase(constantType)) {
            return resolutionCache.get();
        } else if (CONSTANT_TYPE.ISSUE_TYPE.getType().equalsIgnoreCase(constantType)) {
            return issueTypeCache.get();
        }

        throw new IllegalArgumentException("Unknown constant type '" + constantType + "'.");
    }

    @Override
    public IssueType insertIssueType(String name, Long sequence, String style, String description, String iconurl) throws CreateException {
        Long nextSeqId = ofBizDelegator.getDelegatorInterface().getNextSeqId(CONSTANT_TYPE.ISSUE_TYPE.getType());
        try {
            dbConnectionManager.execute(dbConnection -> {
                final QIssueType it = new QIssueType("it");
                dbConnection.insert(it)
                        .set(it.id, String.valueOf(nextSeqId))
                        .set(it.name, name)
                        .set(it.sequence, sequence)
                        .set(it.style, StringUtils.trimToNull(style))
                        .set(it.description, description)
                        .set(it.iconurl, iconurl)
                        .execute();
            });
        } finally {
            refreshIssueTypes();
        }
        return getIssueType(String.valueOf(nextSeqId));
    }

    @Override
    public IssueType insertIssueType(String name, Long sequence, String style, String description, Long avatarId) throws CreateException {
        Long nextSeqId = ofBizDelegator.getDelegatorInterface().getNextSeqId(CONSTANT_TYPE.ISSUE_TYPE.getType());
        try {
            dbConnectionManager.execute(dbConnection -> {
                final QIssueType it = new QIssueType("it");
                dbConnection.insert(it)
                        .set(it.id, String.valueOf(nextSeqId))
                        .set(it.name, name)
                        .set(it.sequence, sequence)
                        .set(it.style, style)
                        .set(it.description, description)
                        .set(it.avatar, avatarId)
                        .execute();
            });
        } finally {
            refreshIssueTypes();
        }
        return getIssueType(String.valueOf(nextSeqId));
    }

    public void validateCreateIssueType(
            final String name,
            final String style,
            final String description,
            final String iconurl,
            final ErrorCollection errors,
            final String nameFieldName) {
        validateName(name, Option.<IssueType>none()).foreach(new Effect<Pair<String, ErrorCollection.Reason>>() {
            @Override
            public void apply(final Pair<String, ErrorCollection.Reason> error) {
                errors.addError(nameFieldName, authenticationContext.getI18nHelper().getText(error.first()), error.second());
            }
        });

        if (isBlank(iconurl)) {
            errors.addError("iconurl", authenticationContext.getI18nHelper().getText("admin.errors.must.specify.url.for.issue.type"));
        }
    }

    public Option<Pair<String, ErrorCollection.Reason>> validateName(final String name, final Option<IssueType> issueTypeToUpdate) {
        if (isBlank(name)) {
            return Option.some(Pair.of(authenticationContext.getI18nHelper().getText("admin.errors.must.specify.name"), VALIDATION_FAILED));
        } else if (length(name) > 60) {
            return Option.some(Pair.of(authenticationContext.getI18nHelper().getText("admin.errors.issuetypes.name.must.not.exceed.max.length"), VALIDATION_FAILED));
        } else {
            for (final IssueType issueType : getAllIssueTypeObjects()) {
                if ((issueTypeToUpdate.isDefined() && !issueTypeToUpdate.get().equals(issueType)) || issueTypeToUpdate.isEmpty()) {
                    if (name.trim().equalsIgnoreCase(issueType.getName())) {
                        return Option.some(Pair.of(authenticationContext.getI18nHelper().getText("admin.errors.issue.type.with.this.name.already.exists"), CONFLICT));
                    }
                }
            }
        }
        return Option.none();
    }

    public void validateCreateIssueTypeWithAvatar(final String name,
                                                  final String style,
                                                  final String description,
                                                  final String avatarId,
                                                  final ErrorCollection errors,
                                                  final String nameFieldName) {
        validateName(name, Option.<IssueType>none()).foreach(new Effect<Pair<String, ErrorCollection.Reason>>() {
            @Override
            public void apply(final Pair<String, ErrorCollection.Reason> error) {
                errors.addError(nameFieldName, authenticationContext.getI18nHelper().getText(error.first()), error.second());
            }
        });
        if (isBlank(avatarId)) {
            errors.addError("avatarId", authenticationContext.getI18nHelper().getText("admin.errors.issue.type.must.specify.avatar"));
        } else {
            try {
                Long.valueOf(avatarId);
            } catch (NumberFormatException e) {
                errors.addError("avatarId", authenticationContext.getI18nHelper().getText("admin.errors.issue.type.must.specify.avatar"));
            }
        }
    }

    public void updateIssueType(String id, String name, Long sequence, String style, String description, String iconurl) throws DataAccessException {
        if (id == null) {
            throw new IllegalArgumentException("Id cannot be null.");
        }

        try {
            dbConnectionManager.execute(dbConnection -> {
                final long rowCount = dbConnection.update(ISSUE_TYPE)
                        .set(ISSUE_TYPE.name, name)
                        .set(ISSUE_TYPE.sequence, sequence)
                        .set(ISSUE_TYPE.style, style)
                        .set(ISSUE_TYPE.description, description)
                        .set(ISSUE_TYPE.iconurl, iconurl)
                        .where(ISSUE_TYPE.id.eq(id))
                        .execute();
                if (rowCount == 0) {
                    throw new IllegalArgumentException("Issue Type with id '" + id + "' does not exist.");
                }
            });
        } finally {
            refreshIssueTypes();
        }

    }

    public void updateIssueType(String id, String name, Long sequence, String style, String description, Long avatarId) throws DataAccessException {
        if (id == null) {
            throw new IllegalArgumentException("Id cannot be null.");
        }

        try {
            dbConnectionManager.execute(dbConnection -> {
                final long rowCount = dbConnection.update(ISSUE_TYPE)
                        .set(ISSUE_TYPE.name, name)
                        .set(ISSUE_TYPE.sequence, sequence)
                        .set(ISSUE_TYPE.style, style)
                        .set(ISSUE_TYPE.description, description)
                        .set(ISSUE_TYPE.avatar, avatarId)
                        .where(ISSUE_TYPE.id.eq(id))
                        .execute();
                if (rowCount == 0) {
                    throw new IllegalArgumentException("Issue Type with id '" + id + "' does not exist.");
                }
            });
        } finally {
            refreshIssueTypes();
        }
    }

    public void removeIssueType(String id) throws RemoveException {
        final IssueType issueType = getIssueType(id);
        if (issueType != null) {
            try {
                dbConnectionManager.execute(dbConnection -> {
                    dbConnection.delete(ISSUE_TYPE)
                            .where(ISSUE_TYPE.id.eq(id))
                            .execute();
                });
            } finally {
                refreshIssueTypes();
            }
        } else {
            throw new RemoveException("Issue type with id '" + id + "' does not exist.");
        }
    }

    @Override
    public void recalculateIssueTypeSequencesAndStore(List<IssueType> issueTypes) {
        try {
            recalculateSequenceAndStore(issueTypes, ISSUE_TYPE, ISSUE_TYPE.sequence, ISSUE_TYPE.id);
        } finally {
            refreshIssueTypes();
        }
    }

    @Override
    public void recalculatePrioritySequencesAndStore(List<Priority> priorities) {
        try {
            recalculateSequenceAndStore(priorities, PRIORITY, PRIORITY.sequence, PRIORITY.id);
        } finally {
            refreshPriorities();
        }
    }

    @Override
    public void recalculateStatusSequencesAndStore(List<Status> statuses) {
        try {
            recalculateSequenceAndStore(statuses, STATUS, STATUS.sequence, STATUS.id);
        } finally {
            refreshStatuses();
        }
    }

    @Override
    public void recalculateResolutionSequencesAndStore(List<Resolution> resolutions) {
        try {
            recalculateSequenceAndStore(resolutions, RESOLUTION, RESOLUTION.sequence, RESOLUTION.id);
        } finally {
            refreshResolutions();
        }
    }

    private void recalculateSequenceAndStore(final List<? extends IssueConstant> constants,
                                             final RelationalPath<?> entity, final NumberPath<Long> sequencePath, final StringPath idPath) {
        dbConnectionManager.execute(dbConnection -> {
            dbConnection.setAutoCommit(false);
            long sequence = 0;
            for (IssueConstant constant : constants) {
                dbConnection.update(entity)
                        .set(sequencePath, sequence)
                        .where(idPath.eq(constant.getId()))
                        .execute();
                sequence++;
            }
            dbConnection.commit();
        });
    }

    public void storeIssueTypes(List<GenericValue> issueTypes) throws DataAccessException {
        try {
            ofBizDelegator.storeAll(issueTypes);
        } catch (DataAccessException e) {
            throw new DataAccessException("Error occurred while storing issue types to the database.", e);
        }

        refreshIssueTypes();
    }

    public void refresh() {
        invalidateAll();
    }

    public void invalidateAll() {
        // Do not load the defaults set all caches to uninitialised
        priorityCache.reset();
        resolutionCache.reset();
        issueTypeCache.reset();
        statusCache.reset();
    }

    public void invalidate(IssueConstant constant) {
        // Do not load the defaults set relevant cache to uninitialised
        if (constant instanceof Priority) {
            priorityCache.reset();
        } else if (constant instanceof Resolution) {
            resolutionCache.reset();
        } else if (constant instanceof IssueType) {
            issueTypeCache.reset();
        } else if (constant instanceof Status) {
            statusCache.reset();
        }
    }

    // Note: The logic of this method is clearly wrong, as the first "special" ID encountered overrides
    // anything else that might be in the collection.  However, it has always "worked" this way, so I'm
    // worried about trying to change it...
    public List<String> expandIssueTypeIds(Collection<String> issueTypeIds) {
        if (issueTypeIds == null) {
            return Collections.emptyList();
        }

        for (final String issueTypeId : issueTypeIds) {
            if (ALL_STANDARD_ISSUE_TYPES.equals(issueTypeId)) {
                return getConstantIds(getRegularIssueTypeObjects());
            } else if (ALL_SUB_TASK_ISSUE_TYPES.equals(issueTypeId)) {
                return getConstantIds(getSubTaskIssueTypeObjects());
            } else if (ALL_ISSUE_TYPES.equals(issueTypeId)) {
                return getAllIssueTypeIds();
            }
        }

        return new ArrayList<>(issueTypeIds);
    }

    public List<String> getAllIssueTypeIds() {
        return issueTypeCache.get().getCachedIds();
    }

    public IssueConstant getIssueConstant(GenericValue issueConstantGV) {
        if (issueConstantGV == null) {
            return null;
        }

        if (CONSTANT_TYPE.ISSUE_TYPE.getType().equals(issueConstantGV.getEntityName())) {
            return getIssueTypeObject(issueConstantGV.getString("id"));
        } else if (CONSTANT_TYPE.STATUS.getType().equals(issueConstantGV.getEntityName())) {
            return getStatusObject(issueConstantGV.getString("id"));
        } else if (CONSTANT_TYPE.PRIORITY.getType().equals(issueConstantGV.getEntityName())) {
            return getPriorityObject(issueConstantGV.getString("id"));
        } else if (CONSTANT_TYPE.RESOLUTION.getType().equals(issueConstantGV.getEntityName())) {
            return getResolutionObject(issueConstantGV.getString("id"));
        }

        throw new IllegalArgumentException("Unknown constant entity name '" + issueConstantGV.getEntityName() + "'.");
    }

    private static List<String> getConstantIds(Collection<? extends IssueConstant> issueConstants) {
        List<String> ids = new ArrayList<String>(issueConstants.size());
        for (IssueConstant issueConstant : issueConstants) {
            ids.add(issueConstant.getId());
        }
        return ids;
    }

    @Nonnull
    public Collection<Priority> getPriorities() {
        return priorityCache.get().getObjects();
    }

    public Priority getPriorityObject(String id) {
        return priorityCache.get().getObject(id);
    }

    public Priority getDefaultPriorityObject() {
        return getDefaultPriority();
    }

    public Priority getDefaultPriority() {
        final String defaultPriorityId = (ComponentAccessor.getApplicationProperties()).getString(APKeys.JIRA_CONSTANT_DEFAULT_PRIORITY);
        if (defaultPriorityId == null) {
            final Collection<Priority> priorities = getPriorityObjects();
            Priority defaultPriority = null;
            final Iterator<Priority> priorityIt = priorities.iterator();

            int times = (int) Math.ceil((double) priorities.size() / 2d);
            for (int i = 0; i < times; i++) {
                defaultPriority = priorityIt.next();
            }

            return defaultPriority;
        } else {
            return getPriorityObject(defaultPriorityId);
        }
    }


    /**
     * @param id the id of a priority
     * @return the name of the priority with the given id, or an i18n'd String indicating that
     * no priority is set (e.g. "None") if the id is null.
     */
    public String getPriorityName(String id) {
        if ("-1".equals(id)) {
            return authenticationContext.getI18nHelper().getText("constants.manager.no.priority");
        }

        Priority priorityObject = getPriorityObject(id);
        if (priorityObject == null) {
            return authenticationContext.getI18nHelper().getText("constants.manager.no.priority");
        }
        return priorityObject.getName();
    }

    public void refreshPriorities() {
        priorityCache.reset();
    }

    @Nonnull
    public Collection<Resolution> getResolutions() {
        return resolutionCache.get().getObjects();
    }

    public Resolution getResolution(String id) {
        return resolutionCache.get().getObject(id);
    }

    public void refreshResolutions() {
        resolutionCache.reset();
    }

    public Collection<IssueType> getAllIssueTypeObjects() {
        return issueTypeCache.get().getObjects();
    }

    public Collection<IssueType> getRegularIssueTypeObjects() {
        return issueTypeCache.get().getRegularIssueTypeObjects();
    }

    @Nonnull
    public Collection<IssueType> getSubTaskIssueTypeObjects() {
        return issueTypeCache.get().getSubTaskIssueTypeObjects();
    }

    public Status getStatusByName(final String name) {
        Collection<Status> statusObjects = getStatusObjects();
        return findByName(name, statusObjects);
    }

    public Status getStatusByNameIgnoreCase(final String name) {
        Collection<Status> statusObjects = getStatusObjects();
        return findByNameIgnoreCase(name, statusObjects);
    }

    public Status getStatusByTranslatedName(final String name) {
        Collection<Status> statusObjects = getStatusObjects();
        Status status = findByTranslatedName(name, statusObjects);
        if (status == null) {
            status = findByName(name, statusObjects);
        }
        return status;
    }

    @Nullable
    private static <T extends IssueConstant> T find(Collection<T> constants,
                                                    java.util.function.Predicate<T> matcher) {
        return constants.stream()
                .filter(matcher)
                .findAny()
                .orElse(null);
    }

    private static <T extends IssueConstant> T findByName(final String name, Collection<T> constants) {
        return find(constants, constant -> constant.getName().equals(name));
    }

    private static <T extends IssueConstant> T findByNameIgnoreCase(final String name, Collection<T> constants) {
        return find(constants, constant -> constant.getName().equalsIgnoreCase(name));
    }

    private static <T extends IssueConstant> T findByTranslatedName(final String name, Collection<T> constants) {
        return find(constants, constant -> constant.getNameTranslation().equals(name));
    }

    public List<IssueType> getEditableSubTaskIssueTypes() {
        final List<String> ids = dbConnectionManager.executeQuery(dbConnection -> dbConnection.newSqlQuery()
                .select(bean(ISSUE_TYPE.id))
                .from(ISSUE_TYPE)
                .where(ISSUE_TYPE.style.eq(SubTaskManager.SUB_TASK_ISSUE_TYPE_STYLE))
                .fetch());
        final ImmutableList.Builder<IssueType> listBuilder = ImmutableList.builder();
        ids.stream().map(this::getIssueType).forEach(listBuilder::add);
        return listBuilder.build();
    }

    public IssueType getIssueType(String id) {
        return issueTypeCache.get().getObject(id);
    }

    public void refreshIssueTypes() {
        issueTypeCache.reset();
    }

    static class ConstantsCache<T extends IssueConstant> {
        private final Map<String, T> idObjectMap;
        private final List<String> idList;

        /**
         * Constructs a ConstantsCache instance.
         *
         * @param issueConstantMap An ID to Constant Object Map.
         */
        public ConstantsCache(final ImmutableMap<String, T> issueConstantMap) {
            this.idObjectMap = issueConstantMap;
            // Extract and store the ids.
            // There is a big performance hit to rebuild these in complex (many projects/issuetypes/customefields) installations.
            // Only because they can get retrieved > 200,000 times per issue query.
            List<String> ids = getConstantIds(issueConstantMap.values());
            this.idList = Collections.unmodifiableList(ids);
        }

        Collection<T> getObjects() {
            return idObjectMap.values();
        }

        T getObject(final String id) {
            return idObjectMap.get(id);
        }

        List<String> getCachedIds() {
            return idList;
        }
    }

    private static class IssueTypeCache extends ConstantsCache<IssueType> {
        private final List<IssueType> regularIssueTypeObjects;
        private final List<IssueType> subTaskIssueTypeObjects;

        /**
         * Constructs a CacheData instance.
         *
         * @param idObjectMap             An ID to Constant Object Map.
         * @param regularIssueTypeObjects List of regular IssueType Objects ordered by name
         * @param subTaskIssueTypeObjects List of subTask IssueTypes ordered by name
         */
        public IssueTypeCache(ImmutableMap<String, IssueType> idObjectMap, ImmutableList<IssueType> regularIssueTypeObjects,
                              ImmutableList<IssueType> subTaskIssueTypeObjects) {
            super(idObjectMap);
            this.regularIssueTypeObjects = regularIssueTypeObjects;
            this.subTaskIssueTypeObjects = subTaskIssueTypeObjects;
        }

        public List<IssueType> getRegularIssueTypeObjects() {
            return regularIssueTypeObjects;
        }

        public List<IssueType> getSubTaskIssueTypeObjects() {
            return subTaskIssueTypeObjects;
        }
    }

    private ConstantsCache<Priority> loadPriorityCache() {
        final List<PriorityDTO> priorityList = dbConnectionManager.executeQuery(dbConnection -> dbConnection.newSqlQuery().select(PRIORITY)
                .from(PRIORITY).orderBy(PRIORITY.sequence.asc()).fetch());
        final ImmutableMap.Builder<String, Priority> builder = ImmutableMap.builder();
        priorityList.stream().map(issueConstantFactory::createPriority).forEach(p -> builder.put(p.getId(), p));

        return new ConstantsCache<>(builder.build());
    }

    private ConstantsCache<Resolution> loadResolutionCache() {
        final List<ResolutionDTO> resolutionList = dbConnectionManager.executeQuery(dbConnection -> dbConnection.newSqlQuery().select(RESOLUTION)
                .from(RESOLUTION).orderBy(RESOLUTION.sequence.asc()).fetch());
        final ImmutableMap.Builder<String, Resolution> builder = ImmutableMap.builder();
        resolutionList.stream().map(issueConstantFactory::createResolution).forEach(r -> builder.put(r.getId(), r));

        return new ConstantsCache<>(builder.build());
    }

    private ConstantsCache<Status> loadStatusCache() {
        final List<StatusDTO> statusList = dbConnectionManager.executeQuery(dbConnection -> dbConnection.newSqlQuery().select(STATUS)
                .from(STATUS).orderBy(STATUS.sequence.asc()).fetch());
        final ImmutableMap.Builder<String, Status> builder = ImmutableMap.builder();
        statusList.stream().map(issueConstantFactory::createStatus).forEach(r -> builder.put(r.getId(), r));

        return new ConstantsCache<>(builder.build());
    }

    private IssueTypeCache loadIssueTypeCache() {
        final List<IssueTypeDTO> issueTypeList = dbConnectionManager.executeQuery(dbConnection -> dbConnection.newSqlQuery().select(ISSUE_TYPE).from(ISSUE_TYPE)
                .orderBy(ISSUE_TYPE.style.asc()).orderBy(ISSUE_TYPE.sequence.asc()).fetch());
        final ImmutableMap.Builder<String, IssueType> builder = ImmutableMap.builder();
        final ImmutableList.Builder<IssueType> regularIssueTypesBuilder = ImmutableList.builder();
        final ImmutableList.Builder<IssueType> subTaskIssueTypesBuilder = ImmutableList.builder();

        issueTypeList.stream().map(issueConstantFactory::createIssueType).forEach(issueType -> {
            if (issueType.isSubTask()) {
                subTaskIssueTypesBuilder.add(issueType);
            } else {
                regularIssueTypesBuilder.add(issueType);
            }
            builder.put(issueType.getId(), issueType);
        });
        return new IssueTypeCache(builder.build(), regularIssueTypesBuilder.build(), subTaskIssueTypesBuilder.build());
    }

}
