package com.atlassian.jira.user.util;

import com.atlassian.fugue.Option;
import org.slf4j.LoggerFactory;

import static com.atlassian.jira.component.ComponentAccessor.getComponent;

/**
 * Ensures that there is a user key for the 'recovery admin'.
 * <p>
 * We put this key in the database to ensure that the 'recovery admin' still has a user key after JIRA leaves
 * recovery mode. Without this mapping JIRA will start throwing exceptions when trying to access objects
 * (e.g. workflows, issues, audit logs) associated with 'recovery admin'.
 * <p>
 * {@link com.atlassian.jira.user.util.RecoveryMode}
 *
 * @since 7.0
 */
public class RecoveryAdminMapper {
    public void map() {
        final Option<String> recoveryUsername = getComponent(RecoveryMode.class).getRecoveryUsername();
        if (recoveryUsername.isDefined()) {
            final UserKeyStore keyStore = getComponent(UserKeyStore.class);
            final String currentKey = keyStore.getKeyForUsername(recoveryUsername.get());
            if (currentKey == null) {
                final String newKey = keyStore.ensureUniqueKeyForNewUser(recoveryUsername.get());
                LoggerFactory.getLogger(RecoveryAdminMapper.class)
                        .debug("Mapping recovery admin '{}' to key '{}'.", recoveryUsername.get(), newKey);
            }
        }
    }
}
