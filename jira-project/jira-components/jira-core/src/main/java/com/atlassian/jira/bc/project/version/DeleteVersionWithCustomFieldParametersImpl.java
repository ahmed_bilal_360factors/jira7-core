package com.atlassian.jira.bc.project.version;

import com.atlassian.jira.project.version.DeleteVersionWithCustomFieldParameters;
import com.atlassian.jira.project.version.Version;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;

public class DeleteVersionWithCustomFieldParametersImpl implements DeleteVersionWithCustomFieldParameters {

    private final Version versionToDelete;
    private final Optional<Version> moveFixIssuesTo;
    private final Optional<Version> moveAffectedIssuesTo;
    private final List<CustomFieldReplacement> customFieldReplacements;


    public DeleteVersionWithCustomFieldParametersImpl(
            @Nullable
            Version versionToDelete,
            @Nonnull
            Optional<Version> moveAffectedIssuesTo,
            @Nonnull
            Optional<Version> moveFixIssuesTo,
            @Nonnull
            List<CustomFieldReplacement> customFieldReplacements) {
        this.versionToDelete = versionToDelete;
        this.moveFixIssuesTo = moveFixIssuesTo;
        this.moveAffectedIssuesTo = moveAffectedIssuesTo;
        this.customFieldReplacements = customFieldReplacements;
    }

    @Override
    public Version getVersionToDelete() {
        return versionToDelete;
    }

    @Override
    public Optional<Version> getMoveFixIssuesTo() {
        return moveFixIssuesTo;
    }

    @Override
    public Optional<Version> getMoveAffectedIssuesTo() {
        return moveAffectedIssuesTo;
    }

    @Nonnull
    @Override
    public List<CustomFieldReplacement> getCustomFieldReplacementList() {
        return customFieldReplacements;
    }
}
