package com.atlassian.jira.task;

import com.google.common.base.Objects;


/**
 * Generic context, could be used by other plugins outside JIRA to run in data center mode.
 */
public class GenericTaskContext implements TaskContext {

    private static final long serialVersionUID = -3782111398948024488L;

    private final String progressURL;

    private final String taskContextName;

    /**
     * @param progressURL     progress URL to be returned as-is by {@link #buildProgressURL}
     * @param taskContextName this name should be unique between task submitters, to identify their task
     */
    public GenericTaskContext(final String progressURL, final String taskContextName) {
        this.progressURL = progressURL;
        this.taskContextName = taskContextName;
    }


    @Override
    public String buildProgressURL(final Long taskId) {
        return progressURL;
    }

    public String getTaskContextName() {
        return taskContextName;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        if (obj.getClass() != this.getClass()) {
            return false;
        }

        GenericTaskContext that = (GenericTaskContext) obj;

        return Objects.equal(this.progressURL, that.progressURL) && Objects.equal(this.taskContextName, that.taskContextName);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.taskContextName, this.progressURL);
    }

    @Override
    public String toString() {
        return "GenericTaskContext{" +
                "progressURL='" + progressURL + '\'' +
                ", taskContextName='" + taskContextName + '\'' +
                '}';
    }
}
