package com.atlassian.jira.cluster.zdu;

import com.atlassian.jira.cluster.Node;
import com.atlassian.jira.model.querydsl.ClusterUpgradeStateDTO;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.inject.Inject;

/**
 * @since v7.3
 */
public class DefaultNodeBuildInfoFactory implements NodeBuildInfoFactory {

    private final BuildUtilsInfo buildUtilsInfo;

    @Inject
    public DefaultNodeBuildInfoFactory(final BuildUtilsInfo buildUtilsInfo) {
        this.buildUtilsInfo = buildUtilsInfo;
    }

    @Override
    public NodeBuildInfo create(final ClusterUpgradeStateDTO clusterUpgradeStateDTO) {
        return new DefaultNodeBuildInfo(clusterUpgradeStateDTO.getClusterBuildNumber(), clusterUpgradeStateDTO.getClusterVersion());
    }

    @Override
    public NodeBuildInfo create(final Node node) {
        return new DefaultNodeBuildInfo(node.getNodeBuildNumber(), node.getNodeVersion());
    }

    @Override
    public NodeBuildInfo create(final long buildNumber, final String version) {
        return new DefaultNodeBuildInfo(buildNumber, version);
    }

    @Override
    public NodeBuildInfo currentApplicationInfo() {
        return new DefaultNodeBuildInfo(buildUtilsInfo.getApplicationBuildNumber(), buildUtilsInfo.getVersion());
    }

    private static class DefaultNodeBuildInfo implements NodeBuildInfo {
        private final long buildNumber;
        private final String version;

        private DefaultNodeBuildInfo(final long buildNumber, final String version) {
            this.buildNumber = buildNumber;
            this.version = version;
        }

        @Override
        @JsonProperty("buildNumber")
        public long getBuildNumber() {
            return buildNumber;
        }

        @Override
        @JsonProperty("version")
        public String getVersion() {
            return version;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("buildNumber", buildNumber)
                    .add("version", version)
                    .toString();
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DefaultNodeBuildInfo build = (DefaultNodeBuildInfo) o;
            return buildNumber == build.buildNumber &&
                    Objects.equal(version, build.version);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(buildNumber, version);
        }
    }
}
