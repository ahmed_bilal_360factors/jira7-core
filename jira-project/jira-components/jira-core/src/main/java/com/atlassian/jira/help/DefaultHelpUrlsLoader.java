package com.atlassian.jira.help;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.resourcebundle.HelpResourceBundleLoader;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Iterables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Locale;

import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.atlassian.jira.util.resourcebundle.HelpResourceBundleLoader.Type.ADMIN_HELP;
import static com.atlassian.jira.util.resourcebundle.HelpResourceBundleLoader.Type.USER_HELP;

/**
 * @since v6.2.4
 */
public class DefaultHelpUrlsLoader implements HelpUrlsLoader {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultHelpUrlsLoader.class);

    private static final String DEFAULT_HELP_URL = "https://confluence.atlassian.com/display/JIRA/";

    private final JiraAuthenticationContext ctx;
    private final LocalHelpUrls localHelpUrls;
    private final I18nHelper.BeanFactory i18n;
    private final HelpUrlsParserBuilderFactory parserBuilderFactory;
    private final ApplicationHelpSpaceProvider applicationHelpSpaceProvider;
    private final HelpUrlsApplicationKeyProvider helpUrlsApplicationKeyProvider;
    private final HelpResourceBundleLoader helpResourceBundleLoader;


    public DefaultHelpUrlsLoader(final HelpResourceBundleLoader helpResourceBundleLoader, final JiraAuthenticationContext ctx,
                                 final LocalHelpUrls localHelpUrls, final I18nHelper.BeanFactory i18n, final HelpUrlsParserBuilderFactory parserBuilderFactory,
                                 final HelpUrlsApplicationKeyProvider helpUrlsApplicationKeyProvider, final ApplicationHelpSpaceProvider applicationHelpSpaceProvider) {
        this.i18n = i18n;
        this.parserBuilderFactory = parserBuilderFactory;
        this.applicationHelpSpaceProvider = applicationHelpSpaceProvider;
        this.ctx = ctx;
        this.localHelpUrls = localHelpUrls;
        this.helpUrlsApplicationKeyProvider = helpUrlsApplicationKeyProvider;
        this.helpResourceBundleLoader = helpResourceBundleLoader;
    }

    @Nonnull
    @Override
    public HelpUrlsLoaderKey keyForCurrentUser() {
        return new LoaderKey(ctx.getLocale(), helpUrlsApplicationKeyProvider.getApplicationKeyForUser());
    }

    @Nonnull
    @Override
    public HelpUrlsLoaderKey keyForApplication(@Nonnull final ApplicationKey applicationKey) {
        return new LoaderKey(ctx.getLocale(), applicationKey);
    }

    @Override
    public HelpUrls apply(final HelpUrlsLoaderKey input) {
        notNull(input);

        //Just make sure that the passed HelpUrlsLoaderKey actually is something that this class generated.
        if (input instanceof LoaderKey) {
            LOG.debug("Loading help urls for key '{}'.", input);
            return apply((LoaderKey) input);
        } else {
            throw new IllegalArgumentException("'input' was not created by a call to keyForCurrentUser.");
        }
    }

    private HelpUrls apply(LoaderKey key) {
        HelpUrls externalHelpUrls = getExternalHelpUrls(key);
        return new ImmutableHelpUrls(externalHelpUrls.getDefaultUrl(), Iterables.concat(localHelpUrls.load(), externalHelpUrls));
    }

    private HelpUrls getExternalHelpUrls(final LoaderKey key) {
        return createHelpUrlParser(key).parse(helpResourceBundleLoader.load(USER_HELP, key.locale), helpResourceBundleLoader.load(ADMIN_HELP, key.locale));
    }

    private HelpUrlsParser createHelpUrlParser(final LoaderKey key) {
        return parserBuilderFactory.newBuilder()
                .defaultUrl(DEFAULT_HELP_URL, i18n.getInstance(key.locale).getText("jira.help.paths.help.title"))
                .applicationHelpSpace(applicationHelpSpaceProvider.getHelpSpace(key.applicationKey)
                        // if application does not define help space uri fallback to core
                        .orElse(applicationHelpSpaceProvider.getHelpSpace(ApplicationKeys.CORE)))
                .build();
    }

    /**
     * Private implementation of {@link com.atlassian.jira.help.HelpUrlsLoader.HelpUrlsLoaderKey} returned by this
     * instance. This it encapsulates all the state needed to lookup a {@link HelpUrls} instance using this class. It is
     * used internally by {@link #apply(com.atlassian.jira.help.HelpUrlsLoader.HelpUrlsLoaderKey)} as arguments for the
     * query. It is used by callers to cache the result of the the {@code apply} call.
     */
    @VisibleForTesting
    static class LoaderKey implements HelpUrlsLoaderKey {
        private final Locale locale;
        private final ApplicationKey applicationKey;

        @VisibleForTesting
        LoaderKey(final Locale locale, final ApplicationKey applicationKey) {
            this.locale = locale;
            this.applicationKey = applicationKey;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final LoaderKey loaderKey = (LoaderKey) o;

            if (locale != null ? !locale.equals(loaderKey.locale) : loaderKey.locale != null) {
                return false;
            }
            return !(applicationKey != null ? !applicationKey.equals(loaderKey.applicationKey) : loaderKey.applicationKey != null);

        }

        @Override
        public int hashCode() {
            int result = locale != null ? locale.hashCode() : 0;
            result = 31 * result + (applicationKey != null ? applicationKey.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "LoaderKey{" +
                    "locale=" + locale +
                    ", applicationKey=" + applicationKey +
                    '}';
        }
    }
}
