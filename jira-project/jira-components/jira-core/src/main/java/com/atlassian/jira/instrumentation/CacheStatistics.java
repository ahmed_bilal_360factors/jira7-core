package com.atlassian.jira.instrumentation;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * Represents instrumentation data for caches.
 *
 * @since v7.1
 */
@ParametersAreNonnullByDefault
public final class CacheStatistics implements Statistics {

    public static final String CACHE_LAAS_ID = "cache";

    @JsonProperty
    private final String name;
    @JsonProperty
    private final List<String> tags;
    @JsonProperty
    private final String type;
    @JsonIgnore
    private final long hits;
    @JsonIgnore
    private final long misses;
    @JsonIgnore
    private final double loadTime;
    @JsonIgnore
    private final long getTime;
    @JsonIgnore
    private final long putTime;
    @JsonIgnore
    private Map<String, Object> otherStats;

    public CacheStatistics(String name, List<String> tags, final String type, long hits, long misses, double loadTime,
                           Map<String, ?> otherStats, final long getTime, final long putTime) {
        this.type = type;
        this.name = requireNonNull(name);
        this.tags = ImmutableList.copyOf(requireNonNull(tags));
        this.hits = hits;
        this.misses = misses;
        this.loadTime = loadTime;
        this.getTime = getTime;
        this.putTime = putTime;
        this.otherStats = copyNonConflictingEntries(requireNonNull(otherStats));
    }

    private Map<String, Object> copyNonConflictingEntries(Map<String, ?> otherStats) {
        final HashMap<String, Object> copy = new HashMap<>(otherStats);
        Arrays.stream(CacheMetricsKeys.values()).map(CacheMetricsKeys::key).forEach(copy::remove);
        return copy;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getLoggingKey() {
        return CACHE_LAAS_ID;
    }

    @Override
    public List<String> getTags() {
        return tags;
    }

    @JsonProperty
    @Override
    public Object getStats() {
        return getStatsMap();
    }

    @JsonIgnore
    public Map<String, Object> getStatsMap() {
        // Skip ones that have no statistics.
        if (hits == 0 && misses == 0 && loadTime == 0 && getTime == 0 && putTime == 0 && otherStats.size() == 0) {
            return ImmutableMap.of();
        }

        return ImmutableMap.<String, Object>builder()
                .put(CacheMetricsKeys.HITS.key(), hits)
                .put(CacheMetricsKeys.MISSES.key(), misses)
                .put(CacheMetricsKeys.LOAD_TIME.key(), loadTime)
                .put(CacheMetricsKeys.GET_TIME.key(), getTime)
                .put(CacheMetricsKeys.PUT_TIME.key(), putTime)
                .putAll(otherStats)
                .build();

    }

    /**
     * @return type of the cache.
     */
    public String type() {
        return type;
    }

    /**
     * @return number of cache hits
     */
    public long hits() {
        return hits;
    }

    /**
     * @return number of cache misises
     */
    public long misses() {
        return misses;
    }

    /**
     * @return mean time spent on calls to loaders / suppliers that provides value in case of miss. In nanoseconds.
     */
    public double loadTime() {
        return loadTime;
    }

    /**
     * @return mean time spent on cache get request. In nanoseconds.
     */
    public long getTime() {
        return getTime;
    }

    /**
     * @return mean time spent on cache put request. In nanoseconds.
     */
    public long putTime() {
        return putTime;
    }

    public static class CacheStatisticsBuilder {
        private String name;
        private List<String> tags = Collections.emptyList();
        private String type = "?";
        private long hits;
        private long misses;
        private double loadTime;
        private long getTime;
        private long putTime;
        private Map<String, ?> otherStats = Collections.emptyMap();

        public CacheStatisticsBuilder withName(final String name) {
            this.name = name;
            return this;
        }

        public CacheStatisticsBuilder withTags(final List<String> tags) {
            this.tags = tags;
            return this;
        }

        public CacheStatisticsBuilder withType(final String type) {
            this.type = type;
            return this;
        }

        public CacheStatisticsBuilder withHits(final long hits) {
            this.hits = hits;
            return this;
        }

        public CacheStatisticsBuilder withMisses(final long misses) {
            this.misses = misses;
            return this;
        }

        public CacheStatisticsBuilder withGetTime(final long getTime) {
            this.getTime = getTime;
            return this;
        }

        public CacheStatisticsBuilder withPutTime(final long putTime) {
            this.putTime = putTime;
            return this;
        }

        public CacheStatisticsBuilder withLoadTime(final double loadTime) {
            this.loadTime = loadTime;
            return this;
        }

        public CacheStatisticsBuilder withOtherStats(final Map<String, ?> otherStats) {
            this.otherStats = otherStats;
            return this;
        }

        public CacheStatistics build() {
            return new CacheStatistics(name, tags, type, hits, misses, loadTime, otherStats, getTime, putTime);
        }
    }
}
