package com.atlassian.jira.plugin.icon;

import com.atlassian.jira.plugin.AbstractJiraModuleDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.ModuleFactory;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Attribute;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class IconTypeModuleDescriptor extends AbstractJiraModuleDescriptor<IconTypeDefinition> {
    private static final Logger log = LoggerFactory.getLogger(IconTypeModuleDescriptor.class);
    private volatile String defaultFilename = null;

    public IconTypeModuleDescriptor(JiraAuthenticationContext authenticationContext, ModuleFactory moduleFactory) {
        super(authenticationContext, moduleFactory);
    }

    @Override
    public void init(final Plugin plugin, final Element element) throws PluginParseException {
        super.init(plugin, element);

        defaultFilename = null;
        Attribute defaultFilenameAttribute = element.attribute("defaultFilename");
        if (defaultFilenameAttribute != null) {
            defaultFilename = defaultFilenameAttribute.getValue();
        }

        if (StringUtils.isBlank(defaultFilename)) {
            throw new PluginParseException("The defaultFilename attribute was not specified.");
        }
    }

    public String getDefaultFilename() {
        return defaultFilename;
    }
}
