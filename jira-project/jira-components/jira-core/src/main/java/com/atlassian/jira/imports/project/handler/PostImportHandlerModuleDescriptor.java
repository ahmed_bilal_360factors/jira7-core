package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.plugin.AbstractJiraModuleDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.module.ModuleFactory;

/**
 * Module descriptor for Project Import Post Import handlers.
 * These handlers will be run after the OfBiz and AO data is imported.
 *
 * @since v6.5
 */
public class PostImportHandlerModuleDescriptor extends AbstractJiraModuleDescriptor<PluggableImportRunnable> {
    public PostImportHandlerModuleDescriptor(final JiraAuthenticationContext authenticationContext, final ModuleFactory moduleFactory) {
        super(authenticationContext, moduleFactory);
    }
}
