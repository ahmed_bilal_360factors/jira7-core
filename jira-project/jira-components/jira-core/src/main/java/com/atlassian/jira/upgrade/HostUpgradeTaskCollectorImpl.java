package com.atlassian.jira.upgrade;

import com.atlassian.upgrade.spi.UpgradeTaskFactory;
import com.atlassian.upgrade.core.HostUpgradeTaskCollector;

import java.util.Optional;

/**
 * This provides the {@link UpgradeTaskFactory} for running upgrades on the host application.
 */
public class HostUpgradeTaskCollectorImpl implements HostUpgradeTaskCollector {
    private final UpgradeTaskFactory upgradeTaskFactory;

    public HostUpgradeTaskCollectorImpl(final UpgradeTaskFactory upgradeTaskFactory) {
        this.upgradeTaskFactory = upgradeTaskFactory;
    }

    @Override
    public Optional<UpgradeTaskFactory> getUpgradeTaskFactory() {
        return Optional.of(upgradeTaskFactory);
    }
}
