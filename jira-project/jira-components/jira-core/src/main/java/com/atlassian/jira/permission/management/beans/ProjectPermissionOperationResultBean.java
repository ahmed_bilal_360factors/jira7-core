package com.atlassian.jira.permission.management.beans;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.List;

@JsonAutoDetect
public class ProjectPermissionOperationResultBean {
    public static final String SUCCESS_TYPE = "success";
    public static final String INFO_TYPE = "info";

    private String type;
    private List<String> messages;

    public ProjectPermissionOperationResultBean() {
    }

    public ProjectPermissionOperationResultBean(final String type, final List<String> messages) {
        this.type = type;
        this.messages = ImmutableList.copyOf(messages);
    }

    public String getType() {
        return type;
    }

    public ProjectPermissionOperationResultBean setType(String type) {
        this.type = type;
        return this;
    }

    public List<String> getMessages() {
        return messages;
    }

    public ProjectPermissionOperationResultBean setMessages(List<String> messages) {
        this.messages = ImmutableList.copyOf(messages);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectPermissionOperationResultBean that = (ProjectPermissionOperationResultBean) o;
        return Objects.equal(type, that.type) &&
                Objects.equal(messages, that.messages);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(type, messages);
    }
}
