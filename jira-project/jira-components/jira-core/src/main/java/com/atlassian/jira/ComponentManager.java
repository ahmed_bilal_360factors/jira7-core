package com.atlassian.jira;

import com.atlassian.annotations.Internal;
import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.application.install.ApplicationInstallListener;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.webwork.WebworkClassLoaderSupplier;
import com.atlassian.jira.diagnostic.PluginDiagnostics;
import com.atlassian.jira.event.ComponentManagerShutdownEvent;
import com.atlassian.jira.event.ComponentManagerStartedEvent;
import com.atlassian.jira.extension.ContainerProvider;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.plugin.OsgiServiceTrackerCache;
import com.atlassian.jira.plugin.component.ComponentModuleDescriptor;
import com.atlassian.jira.startup.JiraStartupChecklist;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.util.Shutdown;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.jira.util.index.IndexLifecycleManager;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginSystemLifecycle;
import com.atlassian.plugin.SplitStartupPluginSystemLifecycle;
import com.atlassian.plugin.event.NotificationException;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Sets;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.type.TypeBindings;
import org.codehaus.jackson.map.type.TypeFactory;
import org.joda.time.DateTime;
import org.picocontainer.ComponentAdapter;
import org.picocontainer.MutablePicoContainer;
import org.picocontainer.PicoContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.management.ManagementFactory;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;

import static com.atlassian.jira.ComponentManager.ContainerLevel.BOOTSTRAP;
import static com.atlassian.jira.ComponentManager.ContainerLevel.EXTENDED_BOOTSTRAP;
import static com.atlassian.jira.ComponentManager.ContainerLevel.FULL_CONTAINER;
import static java.lang.String.format;

/**
 * This component manager uses PicoContainer to resolve all the dependencies between components.
 * <p>
 * It is responsible for initialising a large number of components in JIRA. Any components defined here may be
 * injected via a constructor.
 * </p><p>
 * The ComponentManager also has static accessor methods for accessing components, such as
 * {@link #getComponent(Class)}. Plugins developers should use {@link com.atlassian.jira.component.ComponentAccessor}
 * for this, instead.
 * </p>
 * <p>
 *     The ComponentManager also has various static accessor methods for non-Pico-managed objects, eg.
 * <code>ComponentManager.getInstance().getProjectManager()</code>. Plugins developers should no longer use these -
 * please use {@link com.atlassian.jira.component.ComponentAccessor} instead.
 * <p>
 *     More information can be found at the <a href="http://www.picocontainer.org">picocontainer website</a>.
 */
@Internal
public class ComponentManager implements Shutdown {
    public static final String EXTENSION_PROVIDER_PROPERTY = "jira.extension.container.provider";

    private static final Logger log = LoggerFactory.getLogger(ComponentManager.class);

    private static final ComponentManager COMPONENT_MANAGER = new ComponentManager();

    //
    // instance fields
    //

    private final PluginSystem pluginSystem = new PluginSystem();

    private volatile WrappedComponentContainer container;
    private volatile ContainerLevel containerLevel = null;
    private volatile ComponentManagerStateImpl state = ComponentManagerStateImpl.NOT_STARTED;

    /**
     * Constructor made private, singleton.
     */
    private ComponentManager() {
    }

    /**
     * Initialization registers components for the bootstrap loading of JIRA.
     */
    public void bootstrapInitialise() {
        initComponentContainer(true, BOOTSTRAP);
        new BootstrapContainerRegistrar().registerComponents(container.getComponentContainer());
        changeState(ComponentManagerStateImpl.CONTAINER_INITIALISED);
    }


    /**
     * If JIRA needs to be setup, then add the extra components needed to the bootstrap container.
     */
    public void setupInitialise() {
        validateExtendBootstrapContainer("Setup Container");
        new SetupContainerRegistrar().registerComponents(container.getComponentContainer());
        setLevelToExtendedBootstrap();
    }

    public void extendBootstrapContainerForFailedStartup() {
        if (containerLevel == EXTENDED_BOOTSTRAP) {
            // already extended by FailedStartupContainerRegistrar or SetupContainerRegistrar so we should be OK to show error messages
            return;
        }

        validateExtendBootstrapContainer("Setup Container");
        new FailedStartupContainerRegistrar().registerComponents(container.getComponentContainer());
        setLevelToExtendedBootstrap();
    }

    private void setLevelToExtendedBootstrap() {
        eagerlyInstantiate();  // We need to eagerly instantiate these components here to avoid deadlocks.
        containerLevel = EXTENDED_BOOTSTRAP;
    }

    private void validateExtendBootstrapContainer(String containerName) {
        if (container == null) {
            throw new IllegalStateException("The bootstrap container has not been initialised, you cannot initialise a " + containerName);
        }
        if (containerLevel != BOOTSTRAP) {
            throw new IllegalStateException("You are only allowed to extend the Bootstrap Container, but we are currently at level " + containerLevel);
        } else if (!state.isContainerInitialised()) {
            throw new IllegalStateException("The ComponentManager is " + state.name() + " so you cannot initialise a " + containerName);
        }
    }

    @VisibleForTesting
    void initialise(final boolean useEagerInitialization) {
        registerComponents(useEagerInitialization);
        registerExtensions();
        runInitializingComponents();
        changeState(ComponentManagerStateImpl.CONTAINER_INITIALISED);
    }

    /**
     * Initialization registers components and then registers extensions.
     */
    public void initialise() {
        initialise(true);
    }

    /**
     * Starts the plugin system and registers plugin components with pico.
     * If there is no tenant, delayed (phase 2) plugins will not be started.
     */
    public synchronized void start() {
        quickStart();
    }

    /**
     * Start delayed plugins (if any)
     */
    public synchronized void lateStart() {
        pluginSystem.lateStartup();
    }

    /**
     * This is here (outside of the initialise method) as the getComponentInstancesOfType method starts instantiating
     * components and calls on the LicenseComponentAdapter which tries to get reference to this object using the {@link
     * ComponentManager#getInstance()} method. That method returns null as the reference to this object does not exist
     * until the initialise method completes. So this method should be invoked after the initialise method completes
     * execution.
     */
    private void quickStart() {
        // The Jackson TypeFactory singleton caches classes related to the first HashMap and
        // ArrayList that it sees. If this comes from a plugin, it can end up caching classes from
        // OSGi class loaders, which can hang on to memory for far longer than is desirable.  This
        // call forces population of the HashMap cache before the plugin system comes up, and hence
        // before any OSGi class loaders are on the scene. My attempts to safely populate the
        // ArrayList cache have failed - i have never seen it populated in the wild, and the obvious
        // analogue call does not seem to populate the cache, and digging down to call
        // findTypeParameters directly caused crashes in code using jackson.
        TypeFactory.instance.constructType(HashMap.class, (TypeBindings) null);

        getComponent(PluginDiagnostics.class); // eagerly load to catch events on plugin system startup
        Optional.ofNullable(getComponent(ApplicationInstallListener.class))
                .ifPresent(ApplicationInstallListener::register); // eagerly load to catch events on plugin system startup
        pluginSystem.earlyStartup();

        changeState(ComponentManagerStateImpl.PLUGINSYSTEM_STARTED);
        // now register component plugins before starting anything
        final PluginAccessor pluginAccessor = getPluginAccessor();
        final List<ComponentModuleDescriptor> funNewComponents = pluginAccessor.getEnabledModuleDescriptorsByClass(ComponentModuleDescriptor.class);

        if (!funNewComponents.isEmpty()) {
            for (final ComponentModuleDescriptor componentModuleDescriptor : funNewComponents) {
                componentModuleDescriptor.registerComponents(container.getPicoContainer());
            }
        }
        getComponent(WebworkClassLoaderSupplier.class).set(pluginAccessor.getClassLoader());

        // Before the supplier above was introduced as an indirection layer, we required a ClassLoader instance to be
        // in the container to instantiate WebworkConfigurator. This was sub-optimal and I would love to get rid of this
        // code below but who knows if there's any other code relying on this oddity so leaving the registration of the
        // component here for now.
        container.getPicoContainer().addComponent(pluginAccessor.getClassLoader());

        changeState(ComponentManagerStateImpl.COMPONENTS_REGISTERED);

        // Need to ensure that the components are eagerly instantiated after the "component" plugins had a chance to register.
        // As otherwise the default components are used to instantiate other default components. See JRA-4950.  However,
        // we should do it before we go looking for startable components and event components or unlocking web traffic.
        eagerlyInstantiate();

        changeState(ComponentManagerStateImpl.COMPONENTS_INSTANTIATED);
        // 1. Call start() if they are startable.
        runStartable();
        // 2. Register components with the EventPublisher if they annotate with @EventComponent.
        registerEventComponents();

        changeState(ComponentManagerStateImpl.STARTED);
        getComponent(EventPublisher.class).publish(ComponentManagerStartedEvent.INSTANCE);
    }


    private void initComponentContainer(boolean useEagerInitialization, ContainerLevel newContainerLevel) {
        if (container != null) {
            throw new IllegalStateException("Component container is already initialized");
        }
        final String name;
        switch (newContainerLevel) {
            case FULL_CONTAINER:
                name = "JIRAContainer";
                break;
            case BOOTSTRAP:
                name = "BootstrapContainer";
                break;
            default:
                throw new IllegalArgumentException("Cannot init to ContainerLevel " + newContainerLevel);
        }

        container = new WrappedComponentContainer(new ComponentContainer(useEagerInitialization));
        container.getPicoContainer().setName(name + '_' + DateTime.now());
        this.containerLevel = newContainerLevel;
    }

    /**
     * Determines whether components have already been instantiated regardless of which container is in play.
     *
     * The container in play can either be the full container, the setup container or the failed startup container.
     *
     * @see ContainerLevel#FULL_CONTAINER
     * @see ContainerLevel#EXTENDED_BOOTSTRAP
     * @return true, if components have already been instantiated by the container in play; otherwise, false.
     */
    public boolean componentsAvailable() {
        if (getContainerLevel() == FULL_CONTAINER) {
            return getState().isComponentsInstantiated();
        }
        // We always eagerly instantiate components before going into EXTENDED_BOOTSTRAP
        return getContainerLevel() == EXTENDED_BOOTSTRAP;
    }

    private void registerEventComponents() {
        EventPublisher eventPublisher = getComponent(EventPublisher.class);
        Set<Object> registeredListeners = Sets.newIdentityHashSet();
        Collection<ComponentAdapter<?>> componentAdapters = getContainer().getComponentAdapters();
        for (ComponentAdapter<?> componentAdapter : componentAdapters) {
            Class<?> componentKey = componentAdapter.getComponentImplementation();
            if (componentKey.getAnnotation(EventComponent.class) != null) {
                Object instance = componentAdapter.getComponentInstance(container.getPicoContainer(), ComponentAdapter.NOTHING.class);
                if (registeredListeners.add(instance)) {
                    eventPublisher.register(instance);
                }
            }
        }
    }

    private void runInitializingComponents() {
        // JDEV-31110: Prevent double-afterInstantiation()
        final Set<InitializingComponent> seen = Sets.newIdentityHashSet();
        List<InitializingComponent> components = getContainer().getComponents(InitializingComponent.class);
        for (InitializingComponent component : components) {
            try {
                if (seen.add(component)) {
                    component.afterInstantiation();
                }
            } catch (final Exception e) {
                log.error("Error occurred while initializing component '" + component.getClass().getName() + "'.", e);
                throw new InfrastructureException("Error occurred while initializing component '" + component.getClass().getName() + "'.", e);
            }
        }
    }

    private void runStartable() {

        // JDEV-31110: Prevent double-start()
        final Set<Startable> seen = Sets.newIdentityHashSet();
        List<Startable> startables = getContainer().getComponents(Startable.class);
        for (Startable startable : startables) {
            try {
                if (seen.add(startable) && !(startable instanceof PluginSystemLifecycle))  // don't start the plugin manager twice!
                {
                    startable.start();
                }
            } catch (final Exception e) {
                log.error("Error occurred while starting component '" + startable.getClass().getName() + "'.", e);
                throw new InfrastructureException("Error occurred while starting component '" + startable.getClass().getName() + "'.", e);
            }
        }

    }

    public synchronized void stop() {
        getComponent(EventPublisher.class).publish(ComponentManagerShutdownEvent.INSTANCE);
        pluginSystem.shutdown();
    }

    public void dispose() {
        //JRADEV-21332:: Ensure the cache descriptors are cleared to release any ClassLoaders they hold
        PropertyUtils.clearDescriptors();
        changeState(ComponentManagerStateImpl.NOT_STARTED);
        //JRADEV-23443 - lets try to free up the permgen before adding the new plugin system
        if (container != null) {
            CacheManagerRegistrar.shutDownCacheManager
                    (container.getComponentContainer(), ManagementFactory.getPlatformMBeanServer());

            container.dispose();
            container = null;
            containerLevel = null;
        }
        gc();
    }

    public void shutdown() {
        stop();
        dispose();
    }


    private static void gc() {
        int count = 0;
        Object obj = new Object();
        WeakReference<Object> ref = new WeakReference<>(obj);

        //noinspection UnusedAssignment
        obj = null;

        // break after 100 attempts
        while (count < 10 && ref.get() != null) {
            count++;
            log.debug("Attempting to do a garbage collection:" + count);
            System.gc();
        }
    }

    /**
     * What {@link State} is the {@link ComponentManager} in.
     *
     * @return the current state.
     */
    public State getState() {
        return state;
    }

    /**
     * What {@link com.atlassian.jira.ComponentManager.PluginSystemState} is the plugin system in?
     * This can be used to discover whether delayed plugins were started.
     * @return the current plugins system state
     */
    public PluginSystemState getPluginSystemState() {
        return pluginSystem.state;
    }

    /**
     * Returns the current Pico container level.
     */
    ContainerLevel getContainerLevel() {
        return containerLevel;
    }

    /**
     * Eagerly instantiates the container by making a call to {@link org.picocontainer.PicoContainer#getComponents()} ()} method on
     * the container that is returned by {@link ComponentManager#getContainer()} method.
     */
    @GuardedBy("this")
    private void eagerlyInstantiate() {
        // this is to work around synchronisation problems with Pico (PICO-199)
        // http://jira.codehaus.org/browse/PICO-199
        // Pico has problems if it is instantiating A+B from different threads, and both depend on C
        // and they are using synchronised component adapters. You then get a deadlock.
        // This only happens when C is not registered, or C is not registered by its interface,
        // in which case PICO does a full tree walk (from within a synchronised method!).
        // This really needs to get fixed, but one work around is to full instantiate the tree first,
        // in which case, the need for a full tree walk is decreased.
        container.getComponentContainer().initializeEagerComponents();
    }

    private void registerExtensions() {

        final ApplicationProperties applicationProperties = container.getComponentContainer().getComponentInstance(ApplicationProperties.class);
        final String extensionClassName = applicationProperties.getDefaultBackedString(EXTENSION_PROVIDER_PROPERTY);
        try {
            if (!StringUtils.isBlank(extensionClassName)) {
                container.wrapWith(((ContainerProvider) ClassLoaderUtils.loadClass(extensionClassName, getClass()).newInstance()));
            }
        } catch (Exception extensionClassLoadingException) {
            throw new RuntimeException
                    (
                            format
                                    (
                                            "Error loading PICO extension provider container class with name '%s'",
                                            extensionClassName
                                    ), extensionClassLoadingException
                    );
        }
    }

    /**
     * Returns container
     *
     * @return container
     */
    public PicoContainer getContainer() {
        final WrappedComponentContainer container = this.container;  // volatile read
        return (container != null) ? container.getPicoContainer() : null;
    }

    /**
     * Returns container
     *
     * @return container
     */
    public MutablePicoContainer getMutablePicoContainer() {
        final WrappedComponentContainer container = this.container;  // volatile read
        return (container != null) ? container.getPicoContainer() : null;
    }

    /**
     * This method registers all components with the internal pico-container.
     *
     * @param useEagerInitialization indicates whether container should initialize all components on startup
     */
    private void registerComponents(final boolean useEagerInitialization) {
        initComponentContainer(useEagerInitialization, FULL_CONTAINER);
        new ContainerRegistrar().registerComponents(container.getComponentContainer(), JiraStartupChecklist.startupOK());
    }

    private ComponentManagerStateImpl changeState(ComponentManagerStateImpl newState) {
        final ComponentManagerStateImpl currentState = state;

        //check whether we want to stop (what we can always do) or step state "by one"
        if (newState != ComponentManagerStateImpl.NOT_STARTED && newState.ordinal() != currentState.ordinal() + 1) {
            throw new IllegalStateException(String.format("Cannot change ComponentManager status from %s to %s", currentState, newState));
        }

        state = newState;
        return currentState;
    }

    /**
     * Retuns a singleton instance of this class.
     *
     * @return a singleton instance of this class
     */
    public static ComponentManager getInstance() {
        return COMPONENT_MANAGER;
    }

    /**
     * Retrieves and returns a component which is an instance of given class.
     * <p>
     * In practise, this is the same as {@link #getComponent(Class)} except it will try to find a unique component that
     * implements/extends the given Class even if the Class is not an actual component key.
     * <p> Please note that this method only gets components from JIRA's core Pico Containter. That is, it retrieves
     * core components and components declared in Plugins1 plugins, but not components declared in Plugins2 plugins.
     * Plugins2 components can be retrieved via the {@link #getOSGiComponentInstanceOfType(Class)} method, but only if
     * they are public.
     *
     * @param clazz class to find a component instance by
     * @return found component
     * @see #getOSGiComponentInstanceOfType(Class)
     * @see PicoContainer#getComponent(Class))
     */
     /*
       NOTE to JIRA DEVS : Stop using this method for general purpose component retrieval.  Use ComponentAccessor please.
     */
    public static <T> T getComponentInstanceOfType(final Class<T> clazz) {
        // Try fast approach
        T component = getComponent(clazz);
        if (component != null) {
            return component;
        }
        // Look the slow way
        component = clazz.cast(getInstance().getContainer().getComponent(clazz));
        if (component != null) {
            // Lets log this so we know there is a naughty component
            if (log.isDebugEnabled()) {
                // Debug mode - include a stacktrace to find the caller
                try {
                    throw new IllegalArgumentException();
                } catch (IllegalArgumentException ex) {
                    log.warn("Unable to find component with key '" + clazz + "' - eventually found '" + component + "' the slow way.", ex);
                }
            } else {
                log.warn("Unable to find component with key '" + clazz + "' - eventually found '" + component + "' the slow way.");
            }
        }
        return component;
    }

    /**
     * Retrieves and returns a component which is an instance of given class.
     * <p>
     * In practise, this is the same as {@link #getComponentInstanceOfType(Class)} except it will fail faster if the
     * given Class is not a known component key (it also has a shorter and more meaningful name).
     * <p>
     * Please note that this method only gets components from JIRA's core Pico Containter. That is, it retrieves
     * core components and components declared in Plugins1 plugins, but not components declared in Plugins2 plugins.
     * Plugins2 components can be retrieved via the {@link #getOSGiComponentInstanceOfType(Class)} method, but only if
     * they are public.
     *
     * @param clazz class to find a component instance by
     * @return found component, or null if not found
     * @see #getOSGiComponentInstanceOfType(Class)
     * @see PicoContainer#getComponent(Object)
     */
     /*
       NOTE to JIRA DEVS : Stop using this method for general purpose component retrieval.  Use ComponentAccessor please.
     */
    public static <T> T getComponent(final Class<T> clazz) {
        final ComponentManager componentManager = getInstance();
        if (componentManager == null) {
            return null;
        }
        final PicoContainer picoContainer = componentManager.getContainer();
        return picoContainer == null ? null : picoContainer.getComponent(clazz);


    }

    /**
     * Retrieves and returns a public component from OSGi land via its class name.  This method can be used to retrieve
     * a component provided via a plugins2 OSGi bundle.  Please note that components returned via this method should
     * *NEVER* be cached (e.g. in a static field) as they may be refreshed at any time as a plugin is enabled/disabled
     * or the componentManager is reinitialised (after an XML import).
     * <p>
     * Plugin developers should prefer the API method {@link com.atlassian.jira.component.ComponentAccessor#getOSGiComponentInstanceOfType(Class)}.
     * <p> It is important to note that this only works for public components. That is components with {@code
     * public="true"} declared in their XML configuration. This means that they are available for other plugins to
     * import.
     * <p> A use case for this is when for example for the dashboards plugin.  In several areas in JIRA we may want to
     * render gadgets via the {@link com.atlassian.gadgets.view.GadgetViewFactory}.  Whilst the interface for this
     * component is available in JIRA core, the implementation is provided by the dashboards OSGi bundle.  This method
     * will allow us to access it.
     *
     * @param clazz class to find an OSGi component instance for
     * @return found component
     * @see #getComponentInstanceOfType(Class)
     * @deprecated since 6.0 - please use the jira-api {@link com.atlassian.jira.component.ComponentAccessor#getOSGiComponentInstanceOfType(Class)} instead
     */
     /*
       NOTE to JIRA DEVS : Stop using this method for general purpose component retrieval.  Use ComponentAccessor please.
     */
    @Nullable
    public static <T> T getOSGiComponentInstanceOfType(final Class<T> clazz) {
        Assertions.notNull("class", clazz);

        OsgiServiceTrackerCache osgiServiceTrackerCache = getComponent(OsgiServiceTrackerCache.class);

        if (osgiServiceTrackerCache != null) {
            return osgiServiceTrackerCache.getOsgiComponentOfType(clazz);
        } else {
            throw new IllegalStateException();
        }
    }

    /**
     * Returns all the components currently inside of Pico which are instances of the given class.
     *
     * @param clazz the class to search for.
     * @return a list containing all the instances of the passed class registered in JIRA's pico container.
     */
    public static <T> List<T> getComponentsOfType(final Class<T> clazz) {
        final PicoContainer pico = getInstance().getContainer();
        final List<ComponentAdapter<T>> adapters = pico.getComponentAdapters(clazz);
        if (adapters.isEmpty()) {
            return Collections.emptyList();
        } else {
            final List<T> returnList = new ArrayList<>(adapters.size());
            for (final ComponentAdapter<T> adapter : adapters) {
                // remove cast when we go to a Java5 pico
                returnList.add(clazz.cast(adapter.getComponentInstance(pico)));
            }
            return Collections.unmodifiableList(returnList);
        }
    }

    /**
     * Returns all the components currently inside Pico which are instances of the given class, mapping them to their
     * component key.
     *
     * @param iface The class to search for
     * @return a map, mapping the component key, to the instances of the clas registered in JIRA's pico container.
     */
    public static <T> Map<String, T> getComponentsOfTypeMap(final Class<T> iface) {
        final PicoContainer picoContainer = getInstance().getContainer();
        final List<ComponentAdapter<T>> componentAdaptersOfType = picoContainer.getComponentAdapters(iface);

        final Map<String, T> implementations = new HashMap<>();
        for (final ComponentAdapter<T> componentAdapter : componentAdaptersOfType) {
            final T componentInstance = iface.cast(componentAdapter.getComponentInstance(picoContainer));
            implementations.put(String.valueOf(componentAdapter.getComponentKey()), componentInstance);
        }
        return Collections.unmodifiableMap(implementations);
    }

    public enum PluginSystemState {
        NOT_STARTED,
        EARLY_STARTED,
        LATE_STARTED
    }

    private static class PluginSystem {

        volatile PluginSystemState state = PluginSystemState.NOT_STARTED;

        void earlyStartup() {
            if (state != PluginSystemState.NOT_STARTED) {
                return;
            }
            // start plugin manager first manually so that the component plugins can be startable themselves.
            try {
                ComponentAccessor.getComponent(SplitStartupPluginSystemLifecycle.class).earlyStartup();
                state = PluginSystemState.EARLY_STARTED;
            } catch (final NotificationException ex) {
                // This is just a wrapper from the Plugin Events system - lets get the underlying cause.
                final Throwable cause = ex.getCause();
                throw new InfrastructureException("Error occurred while starting Plugin Manager. " + cause.getMessage(), cause);
            } catch (final Exception e) {
                throw new InfrastructureException("Error occurred while starting Plugin Manager. " + e.getMessage(), e);
            }
        }

        /**
         * Start any delayed plugins.
         * If no plugins were delayed, this is a no-op
         */
        void lateStartup() {
            if (state != PluginSystemState.EARLY_STARTED) {
                return;
            }
            state = PluginSystemState.LATE_STARTED;
            ComponentAccessor.getComponent(SplitStartupPluginSystemLifecycle.class).lateStartup();
        }

        public void shutdown() {
            if (state == PluginSystemState.NOT_STARTED) {
                return;
            }
            try {
                ComponentAccessor.getComponent(PluginSystemLifecycle.class).shutdown();
            } catch (final RuntimeException ignore) {
                // if the plugin system hasn't been started for some reason or has been closed down it will throw an IllegalState
                // we don't care as long as it gets into the not started state  - it also leaks RuntimeExceptions
                // at least log something to help the developer track this down
                log.error("Error occurred while shutting down the component manager.", ignore);
            }
            state = PluginSystemState.NOT_STARTED;
        }
    }

    /**
     * The state of the {@link ComponentManager}.
     *
     * @since 4.0
     */
    public interface State {
        /**
         * Has the PICO container initialised.
         *
         * @return true if the PICO container is set up
         */
        boolean isContainerInitialised();

        /**
         * Have the components registered been with PICO including plugin components.
         *
         * @return true if the plugin system has started.
         */
        boolean isPluginSystemStarted();

        /**
         * Have the components registered been with PICO including plugin components.
         *
         * @return true if components have been registered.
         */
        boolean isComponentsRegistered();

        /**
         * Have the components been instantiated by the full container.
         *
         * @see ContainerLevel#FULL_CONTAINER
         * @return true if components have been instantiated by the full container.
         */
        boolean isComponentsInstantiated();

        /**
         * Has the {@link ComponentManager} started
         *
         * @return true if the component manager has started.
         */
        boolean isStarted();

        /**
         * Gets a translation key for getting a brief explanation of what the state means.
         * Note that these translations *must* live in {@code JiraStartupPageSupport.properties}.
         * They <strong>cannot</strong> come from any other source.
         *
         * @return a message key for resolving a brief description of this state
         */
        String getMessageKey();

        /**
         * Returns the percentage of the way through the startup process this state can be considered to be.
         *
         * @return the startup progress percentage
         */
        int getPercentage();
    }

    private PluginAccessor getPluginAccessor() {
        return getContainer().getComponent(PluginAccessor.class);
    }

    /**
     * Retrieves and returns the index lifecycle manager instance
     *
     * @return index lifecycle manager
     */
    IndexLifecycleManager getIndexLifecycleManager() {
        return getContainer().getComponent(IndexLifecycleManager.class);
    }

    /**
     * Returns the {@link com.atlassian.jira.task.TaskManager}
     *
     * @return the {@link com.atlassian.jira.task.TaskManager}
     */
    TaskManager getTaskManager() {
        return getContainer().getComponent(TaskManager.class);
    }

    enum ContainerLevel {BOOTSTRAP, EXTENDED_BOOTSTRAP, FULL_CONTAINER}
}
