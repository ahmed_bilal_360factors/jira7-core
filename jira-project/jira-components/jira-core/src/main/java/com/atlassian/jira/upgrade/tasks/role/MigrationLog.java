package com.atlassian.jira.upgrade.tasks.role;

import javax.annotation.Nonnull;
import java.util.Collection;

/**
 * Accumulates state, for logging upon completion of migration; which messages are logged depends on whether the
 * migration was successful or not. This class does not perform any logging itself; see {@link MigrationLogDao} for that.
 *
 * @since 7.0
 */
public interface MigrationLog {
    /**
     * Accumulates a new event for logging into a new MigrationLog.
     *
     * @param event the event to log
     * @return a new MigrationLog which is a copy of this one with the new event added
     */
    MigrationLog log(@Nonnull AuditEntry event);

    /**
     * Gets all the events recorded so far.
     *
     * @return all the events accumulated in this chain of loggers
     */
    @Nonnull
    Collection<AuditEntry> events();
}
