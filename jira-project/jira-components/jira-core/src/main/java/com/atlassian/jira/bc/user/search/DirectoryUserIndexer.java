package com.atlassian.jira.bc.user.search;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.embedded.impl.ImmutableUser;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizUser;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.apache.http.annotation.GuardedBy;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.PerFieldAnalyzerWrapper;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopFieldCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static org.apache.lucene.document.Field.Index.ANALYZED_NO_NORMS;
import static org.apache.lucene.document.Field.Index.NOT_ANALYZED_NO_NORMS;
import static org.apache.lucene.document.Field.Store.NO;
import static org.apache.lucene.document.Field.Store.YES;
import static org.apache.lucene.search.BooleanClause.Occur.MUST;

/**
 * Indexes users in directory.
 * Assumes that search parameters are reasonable.
 */
public class DirectoryUserIndexer implements UserIndexer {

    /**
     * Make the queue in replaceAllUsers() big enough to be able to keep feeding worker threads without
     * waiting.
     */
    private static final int MAX_TASKS_PER_THREAD = 16;

    private final Directory directory;
    @GuardedBy("searcherLock")
    private IndexSearcher indexSearcher;
    private IndexWriter indexWriter;

    private final ReadWriteLock searcherLock = new ReentrantReadWriteLock();
    private volatile boolean initialReindexPerformed = false;

    public DirectoryUserIndexer(Directory directory, Analyzer analyzer) {
        this.directory = directory;
        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_33, analyzer);
        try {
            indexWriter = new IndexWriter(directory, config);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void replaceAllUsers(Consumer<Consumer<OfBizUser>> allUsersConsumerMaker) {
        try {
            indexWriter.deleteAll();

            int threadCount = Runtime.getRuntime().availableProcessors();
            ExecutorService executor = new ThreadPoolExecutor(threadCount, threadCount,
                    1L, TimeUnit.HOURS,
                    new ArrayBlockingQueue<>(threadCount * MAX_TASKS_PER_THREAD),
                    new ThreadFactoryBuilder().setNameFormat("directory-user-indexer-%d").build(),
                    new ThreadPoolExecutor.CallerRunsPolicy());

            allUsersConsumerMaker.accept(user -> {
                executor.submit(() -> {
                    Document document = translate(user);
                    try {
                        indexWriter.addDocument(document);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
            });

            MoreExecutors.shutdownAndAwaitTermination(executor, 1, TimeUnit.HOURS);
            indexWriter.commit();
            initialReindexPerformed = true;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        refreshSearcher();
    }

    private void refreshSearcher() {
        try {
            searcherLock.writeLock().lock();
            if (indexSearcher != null) {
                IndexReader currentReader = indexSearcher.getIndexReader();
                IndexReader reopenedReader = indexSearcher.getIndexReader().reopen();
                if (currentReader != reopenedReader) {
                    indexSearcher = new IndexSearcher(reopenedReader);
                    currentReader.close();
                }
            } else {
                indexSearcher = new IndexSearcher(directory);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            searcherLock.writeLock().unlock();
        }
    }

    @Override
    public void index(OfBizUser... users) {
        try {
            indexWriter.addDocuments(
                    Stream.of(users)
                            .map(this::translate)
                            .collect(Collectors.toList())
            );
            indexWriter.commit();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        refreshSearcher();
    }

    @Override
    public void deindex(UserId... userIds) {
        try {
            indexWriter.deleteDocuments(
                    Stream.of(userIds)
                            .map(this::translate)
                            .toArray(Query[]::new)
            );
            indexWriter.commit();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        refreshSearcher();
    }

    @Override
    public void deindexById(long... internalUserIds) {
        try {
            indexWriter.deleteDocuments(
                    LongStream.of(internalUserIds)
                            .mapToObj(this::translateInternalUserId)
                            .toArray(Query[]::new)
            );
            indexWriter.commit();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        refreshSearcher();
    }

    @Override
    public void deindexByUserName(String... userNames) {
        try {
            indexWriter.deleteDocuments(
                    Stream.of(userNames)
                            .map(this::translateUserName)
                            .toArray(Query[]::new)
            );
            indexWriter.commit();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        refreshSearcher();
    }

    private Query translateUserName(String userName) {
        BooleanQuery id = new BooleanQuery();
        id.add(new TermQuery(new Term(EXACT_USER_NAME, IdentifierUtils.toLowerCase(userName))), MUST);
        return id;
    }

    private Query translateInternalUserId(long internalUserId) {
        BooleanQuery id = new BooleanQuery();
        id.add(new TermQuery(new Term(INTERNAL_ID, String.valueOf(internalUserId))), MUST);
        return id;
    }

    private Query translate(UserId userId) {
        BooleanQuery id = new BooleanQuery();
        id.add(new TermQuery(new Term(EXACT_USER_NAME, IdentifierUtils.toLowerCase(userId.getName()))), MUST);
        id.add(new TermQuery(new Term(DIRECTORY_ID, String.valueOf(userId.getDirectoryId()))), MUST);
        return id;
    }

    @Override
    public List<User> search(Query query, int skip, int limit, Sort sort) {
        return internalSearch(() -> {
            try {
                TopFieldCollector collector = TopFieldCollector.create(sort, skip + limit, true, false, false, false);
                indexSearcher.search(query, collector);
                return collector.topDocs(skip, limit);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private List<User> internalSearch(Supplier<TopDocs> docsSupplier) {
        try {
            searcherLock.readLock().lock();
            final IndexSearcher indexSearcher = getIndexSearcher()
                    .orElseThrow(() -> new RuntimeException("Searcher not available at this moment"));
            TopDocs docs = docsSupplier.get();
            return ImmutableList.copyOf(docs.scoreDocs)
                    .stream()
                    .map(scoreDoc -> {
                        try {
                            return indexSearcher.doc(scoreDoc.doc);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    })
                    .map(doc -> ImmutableUser.newUser()
                            .active(Boolean.valueOf(doc.get(ACTIVE)))
                            .directoryId(Long.valueOf(doc.get(DIRECTORY_ID)))
                            .displayName(doc.get(DISPLAY_NAME))
                            .emailAddress(doc.get(EMAIL))
                            .name(doc.get(USER_NAME))
                            .toUser()
                    )
                    .collect(Collectors.toList());
        } finally {
            searcherLock.readLock().unlock();
        }
    }

    private Optional<IndexSearcher> getIndexSearcher() {
        if (initialReindexPerformed) {
            return Optional.of(indexSearcher);
        } else {
            return Optional.empty();
        }
    }

    private Document translate(OfBizUser user) {
        Document document = new Document();
        document.add(new Field(ACTIVE, Boolean.toString(user.isActive()), YES, NOT_ANALYZED_NO_NORMS));
        document.add(new Field(DIRECTORY_ID, String.valueOf(user.getDirectoryId()), YES, NOT_ANALYZED_NO_NORMS));
        document.add(new Field(DISPLAY_NAME, user.getDisplayName(), YES, ANALYZED_NO_NORMS));
        document.add(new Field(EXACT_DISPLAY_NAME, IdentifierUtils.toLowerCase(user.getDisplayName()), NO, NOT_ANALYZED_NO_NORMS));
        document.add(new Field(EMAIL, user.getEmailAddress(), YES, ANALYZED_NO_NORMS));
        document.add(new Field(EXACT_EMAIL, IdentifierUtils.toLowerCase(user.getEmailAddress()), NO, NOT_ANALYZED_NO_NORMS));
        document.add(new Field(USER_NAME, user.getName(), YES, ANALYZED_NO_NORMS));
        document.add(new Field(EXACT_USER_NAME, IdentifierUtils.toLowerCase(user.getName()), NO, NOT_ANALYZED_NO_NORMS));
        document.add(new Field(INTERNAL_ID, String.valueOf(user.getId()), NO, NOT_ANALYZED_NO_NORMS));
        return document;
    }
}
