package com.atlassian.jira.util.log;

import com.atlassian.core.util.Clock;
import com.atlassian.jira.util.RealClock;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * A logger for use where there is the chance for large numbers of repetitive errors.
 * This Logger will output only a limited number of stacktraces and then will only output error messages for
 * {@link #warn(String s, Throwable t)} and {@link #error(String s, Throwable t)}
 *
 * @since v6.3
 */
public class RateLimitingLogger {
    private static final int MAX_STACK_TRACES = 25;
    private static final int MAX_IDLE_TIME = 5;

    private final AtomicBoolean warningSuppressedMessageWritten = new AtomicBoolean(false);
    private final AtomicLong lastWarning = new AtomicLong(0);
    private final AtomicInteger warningsLogged = new AtomicInteger(0);

    private final int maxStackTraces;
    private final long resetAfterMinutes;
    private final Clock clock;
    private final Logger delegate;
    private final String name;

    /**
     * Create a new logger with defaults for number of stacktraces (25) and time to reset (5 minutes).
     *
     * @param clazz Class  for logger name
     */
    public RateLimitingLogger(final Class clazz) {
        this(clazz, MAX_STACK_TRACES, MAX_IDLE_TIME);
    }

    /**
     * Create a new logger
     *
     * @param clazz             Class  for logger name
     * @param maxStackTraces    Maximum number of stacktraces to print before starting suppression.
     * @param resetAfterMinutes Number of minutes where the logger is idle to cause a reset to recommence printing stacktraces.
     */
    public RateLimitingLogger(final Class clazz, int maxStackTraces, int resetAfterMinutes) {
        //Have to use RealClock.getInstance() because typically use of RateLimitingLogger is in static initializer and
        //components are not set up yet when these are run
        this(LoggerFactory.getLogger(clazz), maxStackTraces, resetAfterMinutes, RealClock.getInstance());
    }

    /**
     * Create a new logger with a specific delegate.
     *
     * @param delegate          Class  for logger name
     * @param maxStackTraces    Maximum number of stacktraces to print before starting suppression.
     * @param resetAfterMinutes Number of minutes where the logger is idle to cause a reset to recommence printing stacktraces.
     * @param clock             the clock to use for retrieving system time.
     */
    @VisibleForTesting
    RateLimitingLogger(final Logger delegate, int maxStackTraces, int resetAfterMinutes, Clock clock) {
        this.delegate = delegate;
        this.name = delegate.getName();
        this.maxStackTraces = maxStackTraces;
        this.resetAfterMinutes = TimeUnit.MINUTES.toMillis(resetAfterMinutes);
        this.clock = clock;
    }

    public void debug(final String message) {
        delegate.debug(message);
    }

    public void debug(final String message, final Throwable t) {
        delegate.debug(message, t);
    }

    public void error(final String message) {
        delegate.error(message);
    }

    public void error(final String message, final Throwable t) {
        if (wantFullStackTrace()) {
            delegate.error(message, t);
        } else {
            delegate.error(message);
        }
    }

    public boolean isDebugEnabled() {
        return delegate.isDebugEnabled();
    }

    public boolean isInfoEnabled() {
        return delegate.isInfoEnabled();
    }

    public void info(final String message) {
        delegate.info(message);
    }

    public void info(final String message, final Throwable t) {
        delegate.info(message, t);
    }

    public void warn(final String message) {
        delegate.warn(message);
    }

    public void warnWithTrace(final String message) {
        if (wantFullStackTrace()) {
            delegate.warn(message, new AssertionError("Explicit stack trace requested"));
        } else {
            delegate.warn(message);
        }
    }

    public void errorWithTrace(final String message) {
        if (wantFullStackTrace()) {
            delegate.error(message, new AssertionError("Explicit stack trace requested"));
        } else {
            delegate.error(message);
        }
    }

    public void warn(final String message, final Throwable t) {
        if (wantFullStackTrace()) {
            delegate.warn(message, t);
        } else {
            delegate.warn(message);
        }
    }

    protected boolean wantFullStackTrace() {
        if (isDebugEnabled()) {
            return true;
        }

        // Check if the last full warning is too long ago and if so reset all and start again
        long systemTime = clock.getCurrentDate().getTime();
        if (systemTime - lastWarning.get() > resetAfterMinutes) {
            warningsLogged.set(0);
            warningSuppressedMessageWritten.set(false);
        }
        lastWarning.set(systemTime);

        if (warningsLogged.incrementAndGet() <= maxStackTraces) {
            return true;
        }
        if (warningSuppressedMessageWritten.compareAndSet(false, true)) {
            delegate.warn("*******************************************************************************************************************");
            delegate.warn("Further stacktraces of this type are temporarily suppressed.");
            delegate.warn("To enable full stacktraces set logger level for '" + name + "' to 'DEBUG' ");
            delegate.warn("*******************************************************************************************************************");
        }
        return false;
    }

    @VisibleForTesting
    void reset() {
        lastWarning.set(0);
    }
}
