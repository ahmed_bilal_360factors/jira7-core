package com.atlassian.jira.permission;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.permission.data.PermissionGrantAsPureData;
import com.atlassian.jira.permission.data.PermissionGrantImpl;
import com.atlassian.jira.permission.data.PermissionSchemeImpl;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserKeyService;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollection.Reason;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.atlassian.fugue.Either.left;
import static com.atlassian.fugue.Either.right;
import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.jira.bc.ServiceOutcomeImpl.error;
import static com.atlassian.jira.permission.JiraPermissionHolderType.USER;
import static com.atlassian.jira.permission.data.PermissionGrantAsPureData.TO_GRANT_INPUT;
import static com.atlassian.jira.permission.data.PermissionGrantAsPureData.TO_PURE_DATA;
import static com.atlassian.jira.permission.data.PermissionGrantAsPureData.TO_PURE_DATA_2;
import static com.google.common.collect.ImmutableSet.copyOf;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Maps.uniqueIndex;
import static com.google.common.collect.Sets.difference;

@ParametersAreNonnullByDefault
public final class DefaultPermissionSchemeService implements PermissionSchemeService {
    private final PermissionSchemeManager permissionSchemeManager;
    private final GlobalPermissionManager globalPermissions;
    private final PermissionGrantValidator permissionGrantValidator;
    private final PermissionSchemeRepresentationConverter representationConverter;
    private final ProjectService projectService;
    private final I18nHelper i18n;
    private final UserKeyService userKeyService;
    private final UserManager userManager;

    public DefaultPermissionSchemeService(
            final PermissionSchemeManager permissionSchemeManager,
            final GlobalPermissionManager globalPermissions,
            final PermissionGrantValidator permissionGrantValidator,
            final PermissionSchemeRepresentationConverter representationConverter,
            final ProjectService projectService,
            final I18nHelper i18n,
            final UserKeyService userKeyService,
            final UserManager userManager) {
        this.permissionSchemeManager = permissionSchemeManager;
        this.globalPermissions = globalPermissions;
        this.permissionGrantValidator = permissionGrantValidator;
        this.representationConverter = representationConverter;
        this.projectService = projectService;
        this.i18n = i18n;
        this.userKeyService = userKeyService;
        this.userManager = userManager;
    }

    @Override
    public ServiceOutcome<ImmutableList<PermissionScheme>> getPermissionSchemes(final ApplicationUser user) {
        return ok(ImmutableList.copyOf(transform(Iterables.filter(permissionSchemeManager.getSchemeObjects(), new Predicate<Scheme>() {
            @Override
            public boolean apply(final Scheme scheme) {
                return administerProjectsWithScheme(user, scheme);
            }
        }), new Function<Scheme, PermissionScheme>() {
            @Override
            public PermissionScheme apply(final Scheme input) {
                return representationConverter.permissionScheme(input);
            }
        })));

    }

    @Override
    public ServiceOutcome<PermissionScheme> getPermissionScheme(@Nullable final ApplicationUser user, final Long id) {
        Either<ServiceOutcome<PermissionScheme>, PermissionScheme> schemeOrError = getScheme(user, id);
        return schemeOrError.left().on(new Function<PermissionScheme, ServiceOutcome<PermissionScheme>>() {
            @Override
            public ServiceOutcome<PermissionScheme> apply(final PermissionScheme scheme) {
                return ok(scheme);
            }
        });
    }

    @Override
    public ServiceOutcome<PermissionScheme> createPermissionScheme(@Nullable final ApplicationUser user, final PermissionSchemeInput permissionScheme) {
        return asAdmin(user, new ServiceActionWithResult<PermissionScheme>() {
            @Override
            public ServiceOutcome<PermissionScheme> perform() throws GenericEntityException {
                return validateSchemeName(permissionScheme.getName()).left().on(new Function<String, ServiceOutcome<PermissionScheme>>() {
                    @Override
                    public ServiceOutcome<PermissionScheme> apply(@Nullable final String input) {
                        ErrorCollection entitiesValidation = permissionGrantValidator.validateGrants(user, permissionScheme.getPermissions());
                        if (entitiesValidation.hasAnyErrors()) {
                            return ServiceOutcomeImpl.from(entitiesValidation);
                        } else {
                            PermissionSchemeInput permissionSchemePersistingUserkey = PermissionSchemeInput.builder(permissionScheme)
                                    .setPermissions(updatePermissionsToStoreUserKey(permissionScheme.getPermissions()))
                                    .build();

                            Scheme createdScheme = permissionSchemeManager.createSchemeAndEntities(representationConverter.scheme(permissionSchemePersistingUserkey));
                            return ok(representationConverter.permissionScheme(createdScheme));
                        }

                    }
                });

            }
        });
    }

    @Override
    public ServiceOutcome<PermissionScheme> updatePermissionScheme(@Nullable final ApplicationUser user, final Long id, final PermissionSchemeInput permissionScheme) {
        return asAdmin(user, new ServiceActionWithResult<PermissionScheme>() {
            @Override
            public ServiceOutcome<PermissionScheme> perform() throws GenericEntityException {
                final Either<ServiceOutcome<PermissionScheme>, PermissionScheme> scheme = getScheme(user, id);
                if (scheme.isRight()) {
                    final PermissionScheme originalScheme = scheme.right().get();

                    return validateSchemeName(originalScheme.getName(), permissionScheme.getName()).left().on(new Function<String, ServiceOutcome<PermissionScheme>>() {
                        @Override
                        public ServiceOutcome<PermissionScheme> apply(@Nullable final String input) {

                            final PermissionGrantsUpdateRequest updateRequest = resolvePermissionGrantsUpdate(originalScheme, permissionScheme);

                            return validated(permissionGrantValidator.validateGrants(user, updateRequest.grantsToCreate), new ServiceActionWithResult<PermissionScheme>() {
                                @Override
                                public ServiceOutcome<PermissionScheme> perform() throws GenericEntityException {
                                    PermissionSchemeImpl updatedScheme = new PermissionSchemeImpl(
                                            originalScheme.getId(),
                                            permissionScheme.getName(),
                                            permissionScheme.getDescription().getOrNull());

                                    final PermissionGrantsUpdateRequest updateRequestPersistingUserkey = new PermissionGrantsUpdateRequest(updateRequest.schemeId,
                                            updateRequest.grantsToRemove, updatePermissionsToStoreUserKey(updateRequest.grantsToCreate));

                                    permissionSchemeManager.updateScheme(representationConverter.scheme(updatedScheme));
                                    updatePermissionGrants(updateRequestPersistingUserkey);
                                    return getPermissionScheme(user, originalScheme.getId());
                                }
                            });
                        }
                    });
                } else {
                    return scheme.left().get();
                }
            }
        });
    }

    private PermissionGrantsUpdateRequest resolvePermissionGrantsUpdate(PermissionScheme originalScheme, PermissionSchemeInput newScheme) {
        final Map<PermissionGrantAsPureData, PermissionGrant> originalIndex = uniqueIndex(originalScheme.getPermissions(), TO_PURE_DATA);

        ImmutableSet<PermissionGrantAsPureData> originalGrants = copyOf(transform(originalScheme.getPermissions(), TO_PURE_DATA));
        ImmutableSet<PermissionGrantAsPureData> newGrants = copyOf(transform(newScheme.getPermissions(), TO_PURE_DATA_2));

        Iterable<PermissionGrant> grantsToRemove = transform(difference(originalGrants, newGrants), new Function<PermissionGrantAsPureData, PermissionGrant>() {
            @Override
            public PermissionGrant apply(@Nullable final PermissionGrantAsPureData input) {
                return originalIndex.get(input);
            }
        });
        Iterable<PermissionGrantInput> grantsToCreate = transform(difference(newGrants, originalGrants), TO_GRANT_INPUT);

        return new PermissionGrantsUpdateRequest(originalScheme.getId(), grantsToRemove, grantsToCreate);
    }

    private void updatePermissionGrants(PermissionGrantsUpdateRequest updateRequest) {
        permissionSchemeManager.deleteEntities(transform(updateRequest.grantsToRemove, new Function<PermissionGrant, Long>() {
            @Override
            public Long apply(final PermissionGrant input) {
                return input.getId();
            }
        }));

        for (PermissionGrantInput entity : updateRequest.grantsToCreate) {
            createPermissionGrant(entity, updateRequest.schemeId);
        }
    }

    private void createPermissionGrant(final PermissionGrantInput grant, Long schemeId) {
        try {
            GenericValue schemeAsGenericValue = permissionSchemeManager.getScheme(schemeId);
            permissionSchemeManager.createSchemeEntity(schemeAsGenericValue, representationConverter.schemeEntity(grant, schemeId));
        } catch (GenericEntityException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public ServiceResult deletePermissionScheme(@Nullable final ApplicationUser user, final Long id) {
        return asAdmin(user, new ServiceAction() {
            @Override
            public ServiceResult perform() throws GenericEntityException {
                if (id.equals(permissionSchemeManager.getDefaultSchemeObject().getId())) {
                    return fail(i18n.getText("admin.schemes.permissions.cannot.delete.default.scheme"), Reason.VALIDATION_FAILED);
                }

                Either<ServiceOutcome<Object>, PermissionScheme> schemeToDelete = getScheme(user, id);
                if (schemeToDelete.isRight()) {
                    if (!permissionSchemeManager.getProjects(representationConverter.scheme(schemeToDelete.right().get())).isEmpty()) {
                        return fail(i18n.getText("admin.schemes.permissions.cannot.delete.with.projects", id), Reason.VALIDATION_FAILED);
                    }
                    permissionSchemeManager.deleteScheme(id);
                    return ok();
                } else {
                    return schemeToDelete.left().get();
                }
            }
        });
    }

    @Override
    public ServiceResult assignPermissionSchemeToProject(@Nullable final ApplicationUser user, final Long schemeId, final Long projectId) {
        return asAdmin(user, new ServiceAction() {
            @Override
            public ServiceResult perform() throws GenericEntityException {
                return validate(projectService.getProjectByIdForAction(user, projectId, ProjectAction.VIEW_PROJECT)).left().on(new Function<Project, ServiceResult>() {
                    @Override
                    public ServiceResult apply(final Project project) {
                        return getScheme(user, schemeId).left().on(new Function<PermissionScheme, ServiceOutcome<Object>>() {
                            @Override
                            public ServiceOutcome<Object> apply(final PermissionScheme scheme) {
                                permissionSchemeManager.removeSchemesFromProject(project);
                                permissionSchemeManager.addSchemeToProject(project, representationConverter.scheme(scheme));
                                return ServiceOutcomeImpl.ok(null);
                            }
                        });

                    }
                });
            }
        });

    }

    @Override
    public ServiceOutcome<PermissionScheme> getSchemeAssignedToProject(@Nullable final ApplicationUser user, final Long projectId) {
        ServiceOutcome<Project> projectGetResult = projectService.getProjectByIdForAction(user, projectId, ProjectAction.EDIT_PROJECT_CONFIG);
        if (projectGetResult.isValid()) {
            Scheme scheme = option(permissionSchemeManager.getSchemeFor(projectGetResult.get())).getOrElse(new Supplier<Scheme>() {
                @Override
                public Scheme get() {
                    return permissionSchemeManager.getDefaultSchemeObject();
                }
            });
            return ok(representationConverter.permissionScheme(scheme));
        } else {
            return new ServiceOutcomeImpl<PermissionScheme>(projectGetResult.getErrorCollection());
        }
    }

    private <T> ServiceOutcome<T> asAdmin(@Nullable ApplicationUser user, final ServiceActionWithResult<T> action) {
        return this.<T>validateAdmin(user).getOrElse(new Supplier<ServiceOutcome<T>>() {
            @Override
            public ServiceOutcome<T> get() {
                try {
                    return action.perform();
                } catch (GenericEntityException e) {
                    return dbFail(e);
                }
            }
        });
    }

    private ServiceResult asAdmin(@Nullable ApplicationUser user, final ServiceAction action) {
        Option<ServiceOutcome<Object>> forbidden = validateAdmin(user);
        if (forbidden.isEmpty()) {
            try {
                return action.perform();
            } catch (GenericEntityException e) {
                return dbFail(e);
            }
        } else {
            return forbidden.get();
        }
    }

    private <T> Option<ServiceOutcome<T>> validateAdmin(@Nullable ApplicationUser user) {
        String forbiddenMessage = i18n.getText("admin.schemes.permissions.forbidden");
        if (user == null) {
            return some(DefaultPermissionSchemeService.<T>fail(forbiddenMessage, Reason.NOT_LOGGED_IN));
        } else if (!isAdmin(user)) {
            return some(DefaultPermissionSchemeService.<T>fail(forbiddenMessage, Reason.FORBIDDEN));
        }
        return none();
    }

    private boolean isAdmin(final ApplicationUser user) {
        return globalPermissions.hasPermission(GlobalPermissionKey.ADMINISTER, user);
    }

    private <T> Either<ServiceOutcome<T>, PermissionScheme> getScheme(@Nullable final ApplicationUser user, Long schemeId) {
        final ServiceOutcome<T> doesNotExist = fail(i18n.getText("admin.schemes.permissions.validation.scheme.does.not.exist", schemeId.toString()), Reason.NOT_FOUND);
        try {
            final Scheme schemeObject = permissionSchemeManager.getSchemeObject(schemeId);
            return option(schemeObject).flatMap(new Function<Scheme, Option<PermissionScheme>>() {
                @Override
                public Option<PermissionScheme> apply(final Scheme scheme) {
                    if (isAdmin(user) || administerProjectsWithScheme(user, scheme)) {
                        return some(updateSchemeToReturnUsername(representationConverter.permissionScheme(scheme)));
                    } else {
                        return none();
                    }
                }
            }).toRight(new Supplier<ServiceOutcome<T>>() {
                @Override
                public ServiceOutcome<T> get() {
                    return doesNotExist;
                }
            });
        } catch (DataAccessException ex) {
            return left(doesNotExist);
        }
    }

    private boolean administerProjectsWithScheme(final ApplicationUser user, final Scheme scheme) {
        return isAdmin(user) || !isEmpty(filter(permissionSchemeManager.getProjects(scheme), new Predicate<Project>() {
            @Override
            public boolean apply(final Project project) {
                return projectService.getProjectByIdForAction(user, project.getId(), ProjectAction.EDIT_PROJECT_CONFIG).isValid();
            }
        }));
    }

    private static <T> ServiceOutcome<T> ok(T value) {
        return ServiceOutcomeImpl.ok(value);
    }

    private static ServiceOutcome<Void> ok() {
        return ServiceOutcomeImpl.ok(null);
    }

    private static <T> ServiceOutcome<T> fail(String message, Reason reason) {
        return error(message, reason);
    }

    private <T> ServiceOutcome<T> dbFail(final GenericEntityException e) {
        return fail(i18n.getText("admin.schemes.permissions.error.database.exception", e.getMessage()), Reason.SERVER_ERROR);
    }

    private static interface ServiceActionWithResult<T> {
        ServiceOutcome<T> perform() throws GenericEntityException;
    }

    private static interface ServiceAction {
        ServiceResult perform() throws GenericEntityException;
    }

    private Either<ServiceOutcome<PermissionScheme>, String> validateSchemeName(String newName) throws GenericEntityException {
        return validateSchemeName(null, newName);
    }

    private Either<ServiceOutcome<PermissionScheme>, String> validateSchemeName(@Nullable String oldName, String newName) throws GenericEntityException {
        if (!newName.equals(oldName) && permissionSchemeManager.schemeExists(newName)) {
            return left(DefaultPermissionSchemeService.<PermissionScheme>fail(i18n.getText("admin.schemes.permissions.scheme.already.exists", newName), Reason.VALIDATION_FAILED));
        } else {
            return right(newName);
        }
    }

    private <T> ServiceOutcome<T> validated(ErrorCollection validationResult, ServiceActionWithResult<T> actionIfValid) {
        try {
            return validationResult.hasAnyErrors() ? ServiceOutcomeImpl.<T>from(validationResult) : actionIfValid.perform();
        } catch (GenericEntityException e) {
            throw new DataAccessException(e);
        }
    }

    /**
     * The front end deals with the username, but on the back end we want to persist the userkey. This checks for user
     * security types and updates their parameters from username to userkey.
     *
     * @param permissions permissions to check for user grants
     * @return permissions with any user grant parameters updated to userkeys
     */
    private Iterable<PermissionGrantInput> updatePermissionsToStoreUserKey(Iterable<PermissionGrantInput> permissions) {
        List<PermissionGrantInput> permissionsToAdd = new ArrayList(Iterables.size(permissions));
        for (PermissionGrantInput perm : permissions) {
            PermissionHolderType type = perm.getHolder().getType();
            if (USER.equals(type)) {
                PermissionHolder newHolder = PermissionHolder.holder(type, userKeyService.getKeyForUsername(perm.getHolder().getParameter().get()));
                PermissionGrantInput alteredPerm = PermissionGrantInput.newGrant(newHolder, perm.getPermission());
                permissionsToAdd.add(alteredPerm);
            } else {
                permissionsToAdd.add(perm);
            }
        }
        return ImmutableList.copyOf(permissionsToAdd);
    }

    /**
     * This updates any user security grants to have the username as the parameter rather than the userkey. This is the
     * expected representation above the service level.
     *
     * @param originalScheme the scheme to update
     * @return an updated scheme with any userkey replaced with the username
     */
    private PermissionScheme updateSchemeToReturnUsername(PermissionScheme originalScheme) {
        Collection<PermissionGrant> originalPerms = originalScheme.getPermissions();
        List<PermissionGrant> updatedPerms = new ArrayList<>(originalPerms.size());
        for (PermissionGrant perm : originalPerms) {
            PermissionHolderType type = perm.getHolder().getType();
            if (USER.equals(type)) {
                // Can't use UserKeyService because we need to preserve casing
                ApplicationUser user = userManager.getUserByKeyEvenWhenUnknown(perm.getHolder().getParameter().getOrNull());
                if (user != null) {
                    String newParameter = user.getUsername();
                    PermissionHolder newHolder = PermissionHolder.holder(type, newParameter);
                    PermissionGrant alteredPerm = new PermissionGrantImpl(perm.getId(), newHolder, perm.getPermission());
                    updatedPerms.add(alteredPerm);
                }
            } else {
                updatedPerms.add(perm);
            }
        }
        return new PermissionSchemeImpl(originalScheme.getId(), originalScheme.getName(), originalScheme.getDescription(), updatedPerms);
    }

    private static <F> Either<ServiceResult, F> validate(ServiceOutcome<F> outcome) {
        if (outcome.isValid()) {
            return right(outcome.get());
        } else {
            return Either.<ServiceResult, F>left(new ServiceResultImpl(outcome.getErrorCollection()));
        }
    }

    private static final class PermissionGrantsUpdateRequest {
        private final Long schemeId;
        private final Iterable<PermissionGrant> grantsToRemove;
        private final Iterable<PermissionGrantInput> grantsToCreate;

        public PermissionGrantsUpdateRequest(final Long schemeId, final Iterable<PermissionGrant> grantsToRemove, final Iterable<PermissionGrantInput> grantsToCreate) {
            this.schemeId = schemeId;
            this.grantsToRemove = grantsToRemove;
            this.grantsToCreate = grantsToCreate;
        }
    }
}
