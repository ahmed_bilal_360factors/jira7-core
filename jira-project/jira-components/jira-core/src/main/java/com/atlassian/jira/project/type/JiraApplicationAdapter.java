package com.atlassian.jira.project.type;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.application.api.PlatformApplication;
import com.atlassian.application.host.plugin.PluginApplicationMetaDataManager;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationAuthorizationService;
import com.atlassian.jira.application.ApplicationRoleDefinitions;
import com.atlassian.jira.application.JiraApplication;
import com.atlassian.jira.application.JiraPluginApplicationMetaData;
import com.atlassian.jira.user.ApplicationUser;

import java.util.stream.Stream;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static java.util.Arrays.asList;
import static java.util.stream.StreamSupport.stream;

/**
 * Class responsible for adapting the regular {@link Application}s defined on the instance
 * into {@link JiraApplication}, so that project types are correctly parsed and exposed to consumers.
 */
public class JiraApplicationAdapter {
    private static final ProjectTypeKey BUSINESS_KEY = new ProjectTypeKey("business");
    private static final String BUSINESS_DESC = "jira.project.type.business.description";
    private static final String BUSINESS_ICON = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE4LjEuMSwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDMwMCAzMDAiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDMwMCAzMDA7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnIGlkPSJMYXllcl8yIj4NCgk8cGF0aCBzdHlsZT0iZmlsbDojMzU3MkIwOyIgZD0iTTE1MCwwQzY2LjY2NywwLDAsNjYuNjY3LDAsMTUwczY2LjY2NywxNTAsMTUwLDE1MHMxNTAtNjYuNjY3LDE1MC0xNTBTMjMzLjMzMywwLDE1MCwweg0KCQkgTTE2Ni42NjcsMjE2LjY2N0g4My4zMzNWMjAwaDgzLjMzM1YyMTYuNjY3eiBNMjE2LjY2NywxODMuMzMzSDgzLjMzM3YtMTYuNjY3aDEzMy4zMzNWMTgzLjMzM3ogTTIxNi42NjcsMTUwSDgzLjMzM3YtMTYuNjY3DQoJCWgxMzMuMzMzVjE1MHogTTIxNi42NjcsMTE2LjY2N0g4My4zMzNWMTAwaDEzMy4zMzNWMTE2LjY2N3oiLz4NCjwvZz4NCjxyZWN0IHg9IjgzLjMzMyIgeT0iMjAwIiBzdHlsZT0iZmlsbDojRkZGRkZGOyIgd2lkdGg9IjgzLjMzMyIgaGVpZ2h0PSIxNi42NjciLz4NCjxyZWN0IHg9IjgzLjMzMyIgeT0iMTY2LjY2NyIgc3R5bGU9ImZpbGw6I0ZGRkZGRjsiIHdpZHRoPSIxMzMuMzMzIiBoZWlnaHQ9IjE2LjY2NyIvPg0KPHJlY3QgeD0iODMuMzMzIiB5PSIxMzMuMzMzIiBzdHlsZT0iZmlsbDojRkZGRkZGOyIgd2lkdGg9IjEzMy4zMzMiIGhlaWdodD0iMTYuNjY3Ii8+DQo8cmVjdCB4PSI4My4zMzMiIHk9IjEwMCIgc3R5bGU9ImZpbGw6I0ZGRkZGRjsiIHdpZHRoPSIxMzMuMzMzIiBoZWlnaHQ9IjE2LjY2NyIvPg0KPC9zdmc+DQo=";
    private static final String BUSINESS_COLOR = "#1D8832";
    private static final int BUSINESS_WEIGHT = 1000;
    static final ProjectType BUSINESS_TYPE = new ProjectType(BUSINESS_KEY, BUSINESS_DESC, BUSINESS_ICON, BUSINESS_COLOR, BUSINESS_WEIGHT);

    public final ApplicationManager applicationManager;
    public final PluginApplicationMetaDataManager metaDataManager;
    public final ApplicationAuthorizationService applicationAuthorizationService;
    private final ApplicationRoleDefinitions applicationRoleDefinitions;

    public JiraApplicationAdapter(
            ApplicationManager applicationManager,
            PluginApplicationMetaDataManager metaDataManager,
            ApplicationAuthorizationService applicationAuthorizationService,
            ApplicationRoleDefinitions applicationRoleDefinitions) {
        this.applicationManager = applicationManager;
        this.metaDataManager = metaDataManager;
        this.applicationAuthorizationService = applicationAuthorizationService;
        this.applicationRoleDefinitions = applicationRoleDefinitions;
    }

    /**
     * Returns an iterable over all the JIRA applications defined on the instance.
     *
     * @return an iterable over all the JIRA applications defined on the instance.
     */
    public Iterable<JiraApplication> getJiraApplications() {
        return buildJiraApplications().collect(toImmutableList());
    }

    /**
     * Returns an iterable over all the JIRA applications accessible in the instance.
     * <p>
     * The platform application (Business) will always be accessible at least
     * one of the applications is licensed.
     *
     * @return an iterable over all the JIRA applications accessible in the instance.
     */
    public Iterable<JiraApplication> getAccessibleJiraApplications() {
        boolean atLeastOneNonCoreApplicationLicensed = isAtLeastOneNonCoreApplicationLicensed();
        return buildJiraApplications()
                .filter(application -> atLeastOneNonCoreApplicationLicensed && isPlatformApplication(application) ||
                        applicationAuthorizationService.isApplicationInstalledAndLicensed(application.getKey()))
                .collect(toImmutableList());
    }

    /**
     * Returns an iterable over all the JIRA applications accessible for the given user.
     *
     * @param user The user
     * @return an iterable over all the JIRA applications accessible to the given user.
     */
    public Iterable<JiraApplication> getAccessibleJiraApplications(ApplicationUser user) {
        return buildJiraApplications()
                .filter(application -> applicationAuthorizationService.canUseApplication(user, application.getKey()))
                .collect(toImmutableList());
    }

    private boolean isPlatformApplication(final JiraApplication application) {
        return application.getKey().equals(applicationManager.getPlatform().getKey());
    }

    private boolean isAtLeastOneNonCoreApplicationLicensed() {
        Stream<ApplicationRoleDefinitions.ApplicationRoleDefinition> licensedDefinitions = stream(applicationRoleDefinitions.getLicensed().spliterator(), false);
        return licensedDefinitions.anyMatch(this::isNonCoreApplicationDefinition);
    }

    private boolean isNonCoreApplicationDefinition(final ApplicationRoleDefinitions.ApplicationRoleDefinition definition) {
        return !applicationManager.getPlatform().getKey().equals(definition.key());
    }

    private Stream<JiraApplication> buildJiraApplications() {
        return Stream.concat(getJiraAppForPlatformApp(), getJiraAppsForNonPlatformApps());
    }

    private Stream<JiraApplication> getJiraAppForPlatformApp() {
        PlatformApplication platform = applicationManager.getPlatform();
        return Stream.of(new JiraApplication(platform, asList(BUSINESS_TYPE)));
    }

    private Stream<JiraApplication> getJiraAppsForNonPlatformApps() {
        return getNonPlatformApplications().flatMap(this::getJiraApplicationFor);
    }

    private Stream<Application> getNonPlatformApplications() {
        Stream<Application> applications = stream(applicationManager.getApplications().spliterator(), false);
        return applications.filter(input -> !(input instanceof PlatformApplication));
    }

    private Stream<JiraApplication> getJiraApplicationFor(Application application) {
        return getMetadataForApplication(application.getKey())
                .map(meta -> new JiraApplication(application, meta.getProjectTypes()))
                .map(Stream::of)
                .getOrElse(Stream.empty());
    }

    private Option<JiraPluginApplicationMetaData> getMetadataForApplication(ApplicationKey appKey) {
        return metaDataManager.getApplication(appKey).map(JiraPluginApplicationMetaData.class::cast);
    }
}
