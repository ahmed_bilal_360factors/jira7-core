package com.atlassian.jira.web.action.admin.instrumentation.cache;

import com.atlassian.instrumentation.caches.CacheKeys;
import com.atlassian.jira.instrumentation.CacheStatistics;
import com.atlassian.jira.instrumentation.InstrumentationLogger;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.collectingAndThen;

/**
 * Pulls out cache operation information.
 *
 * @since v7.1
 */
@WebSudoRequired
public class ViewCacheStats extends JiraWebActionSupport {
    private final InstrumentationLogger instrumentationLogger;

    public ViewCacheStats(InstrumentationLogger instrumentationLogger) {
        this.instrumentationLogger = instrumentationLogger;
    }

    /**
     * Returns the JSON into the page so that we can display the table.
     *
     * @return JSON array with objects.
     */
    @SuppressWarnings("unused")
    @Nonnull
    public Collection<CacheDisplayBean> getCacheStats() {
        return instrumentationLogger.getLogEntriesFromBuffer().stream()
                .filter(logEntry -> logEntry.getData().get(CacheStatistics.CACHE_LAAS_ID) != null)
                // Flatten to a stream of Statistics
                .flatMap(logEntry -> logEntry.getData().get(CacheStatistics.CACHE_LAAS_ID).stream())
                .filter(s -> s instanceof CacheStatistics)
                .map(s -> (CacheStatistics) s)
                .map(statistic -> {

                    // Build a CacheDisplayBean that we can aggregate later.
                    final CacheDisplayBean bean = new CacheDisplayBean(statistic.getName(), statistic.type());
                    fillOutStats(statistic, bean);
                    bean.setName(statistic.getName());

                    // If the size is -1, no sizer is defined for the cache. set it to zero so we can format it out.
                    if (bean.getSize() == -1) {
                        bean.setSize(0);
                    }
                    return bean;

                })
                // Group by the cache name and then reduce by adding all the values together.
                .collect(Collectors.groupingByConcurrent(CacheDisplayBean::getName, collectingAndThen(
                        Collectors.reducing(CacheDisplayBean::add), Optional::get)))
                // After reducing we have one entry per name. We only want the CacheDisplayBeans.
                .values();
    }

    /**
     * Converts a {@link CacheStatistics} entry to a {@link CacheDisplayBean}
     *
     * @param st    The {@link CacheStatistics}
     * @param value The {@link CacheDisplayBean} that will be filled out.
     */
    private void fillOutStats(final CacheStatistics st, final CacheDisplayBean value) {
        value.setCacheType(st.type());
        value.setHitCount(st.hits());
        value.setMissCount(st.misses());
        value.setAvgLoadTime(st.loadTime());

        if (st.getStatsMap().containsKey(CacheKeys.PUTS.getName())) {
            value.setPutCount((long) st.getStatsMap().get(CacheKeys.PUTS.getName()));
        }
        if (st.getStatsMap().containsKey(CacheKeys.LOADS.getName())) {
            value.setLoads((long) st.getStatsMap().get(CacheKeys.LOADS.getName()));
        }
        if (st.getStatsMap().containsKey(CacheKeys.COUNT.getName())) {
            value.setSize((long) st.getStatsMap().get(CacheKeys.COUNT.getName()));
        }
    }
}
