package com.atlassian.jira.security.roles;

import com.atlassian.jira.plugin.roles.ProjectRoleActorModuleDescriptor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.ozymandias.PluginPointVisitor;
import com.atlassian.ozymandias.SafePluginPointAccess;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.util.profiling.UtilTimerStack;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Creator of RoleActor objects that have been registered dynamically.
 */
public class PluginDelegatingRoleActorFactory implements RoleActorFactory {

    private final PluginAccessor pluginAccessor;

    public PluginDelegatingRoleActorFactory(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    public ProjectRoleActor createRoleActor(Long id, Long projectRoleId, Long projectId, String type, String parameter)
            throws RoleActorDoesNotExistException {
        Map<String, ProjectRoleActorModuleDescriptor> implementations = getImplementationsMap();

        ProjectRoleActorModuleDescriptor roleActorModuleDescriptor = getRoleActorModuleDescriptor(type, implementations);
        if (roleActorModuleDescriptor == null) {
            throw new IllegalArgumentException("Type " + type + " is not a registered RoleActor implementation");
        }

        //We are not wrapping this in SafePluginPointAccess because there is no easy way to propagate checked exception correctly.
        RoleActorFactory roleActorFactory = roleActorModuleDescriptor.getModule();
        return roleActorFactory.createRoleActor(id, projectRoleId, projectId, type, parameter);
    }

    @Override
    public Set<RoleActor> optimizeRoleActorSet(Set<RoleActor> roleActors) {
        final OptimizeRoleActorSetVisitor visitor = new OptimizeRoleActorSetVisitor(roleActors);
        SafePluginPointAccess.to().descriptors(getImplementations(), visitor);
        return visitor.getOptimizedActors();
    }

    @Override
    @Nonnull
    public Set<ProjectRoleActor> getAllRoleActorsForUser(@Nullable final ApplicationUser user) {
        final List<Set<ProjectRoleActor>> actorsFromPluginPoints = SafePluginPointAccess.to().
                descriptors(getImplementations(),
                        (ProjectRoleActorModuleDescriptor descriptor, RoleActorFactory factory) -> factory.getAllRoleActorsForUser(user));

        return ImmutableSet.copyOf(Iterables.concat(actorsFromPluginPoints));
    }

    private ProjectRoleActorModuleDescriptor getRoleActorModuleDescriptor(String type, Map<String, ProjectRoleActorModuleDescriptor> implementations) {
        if (implementations == null) {
            return null;
        } else {
            return implementations.get(type);
        }
    }

    private Map<String, ProjectRoleActorModuleDescriptor> getImplementationsMap() {
        UtilTimerStack.push("DefaultRoleActorFactory.getImplementations");
        UtilTimerStack.push("DefaultRoleActorFactory.getImplementations-getEnabledModuleDescriptorByClass");
        List<ProjectRoleActorModuleDescriptor> descriptors = pluginAccessor.getEnabledModuleDescriptorsByClass(ProjectRoleActorModuleDescriptor.class);
        UtilTimerStack.pop("DefaultRoleActorFactory.getImplementations-getEnabledModuleDescriptorByClass");

        Map<String, ProjectRoleActorModuleDescriptor> actorsByType = Maps.newHashMapWithExpectedSize(descriptors.size());

        for (final ProjectRoleActorModuleDescriptor projectRoleModuleDescriptor : descriptors) {
            actorsByType.put(projectRoleModuleDescriptor.getKey(), projectRoleModuleDescriptor);
        }
        UtilTimerStack.pop("DefaultRoleActorFactory.getImplementations");

        return actorsByType;
    }

    private Collection<ProjectRoleActorModuleDescriptor> getImplementations() {
        return getImplementationsMap().values();
    }

    /**
     * A visitor for optimizing RoleActor sets. Used to delegate the optimization to the plugin points.
     */
    private static class OptimizeRoleActorSetVisitor implements PluginPointVisitor<ProjectRoleActorModuleDescriptor, RoleActorFactory> {
        private Set<RoleActor> optimizedActors;

        public OptimizeRoleActorSetVisitor(final Set<RoleActor> optimizedActors) {
            this.optimizedActors = optimizedActors;
        }

        @Override
        public void visit(final ProjectRoleActorModuleDescriptor projectRoleActorModuleDescriptor, final RoleActorFactory roleActorFactory) {
            optimizedActors = roleActorFactory.optimizeRoleActorSet(optimizedActors);
        }

        public Set<RoleActor> getOptimizedActors() {
            return optimizedActors;
        }
    }
}