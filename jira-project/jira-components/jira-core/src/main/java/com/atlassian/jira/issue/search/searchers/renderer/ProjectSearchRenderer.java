package com.atlassian.jira.issue.search.searchers.renderer;

import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.entity.WithId;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.issue.transport.FieldValuesHolder;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.jira.util.ParameterUtils;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.query.Query;
import webwork.action.Action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * A search renderer for the project system field searcher.
 *
 * @since v4.0
 */
public class ProjectSearchRenderer extends AbstractSearchRenderer implements SearchRenderer {
    // if the number of projects a user can see is <= this amount, we don't show recently used
    public static final int MAX_PROJECTS_BEFORE_RECENT = 10;

    // max number of recently used projects to show
    public static final int MAX_RECENT_PROJECTS_TO_SHOW = 5;

    private final ProjectManager projectManager;
    private final PermissionManager permissionManager;
    private final UserProjectHistoryManager projectHistoryManager;

    public ProjectSearchRenderer(ProjectManager projectManager, PermissionManager permissionManager,
                                 VelocityRequestContextFactory velocityRequestContextFactory, ApplicationProperties applicationProperties,
                                 VelocityTemplatingEngine templatingEngine, String searcherNameKey, UserProjectHistoryManager projectHistoryManager) {
        super(velocityRequestContextFactory, applicationProperties, templatingEngine, SystemSearchConstants.forProject(), searcherNameKey);
        this.projectManager = projectManager;
        this.permissionManager = permissionManager;
        this.projectHistoryManager = projectHistoryManager;
    }

    public String getEditHtml(final ApplicationUser user, final SearchContext searchContext, final FieldValuesHolder fieldValuesHolder, final Map<?, ?> displayParameters, final Action action) {
        Map<String, Object> velocityParams = getVelocityParams(user, searchContext, null, fieldValuesHolder, displayParameters, action);
        addParameters(user, fieldValuesHolder, false, velocityParams);
        return renderEditTemplate("project-searcher" + EDIT_TEMPLATE_SUFFIX, velocityParams);
    }

    public void addParameters(final ApplicationUser searcher, final FieldValuesHolder fieldValuesHolder, boolean noCurrentSearchRequest, Map<String, Object> velocityParams) {
        Collection<Project> allProjects = getVisibleProjects(searcher);
        velocityParams.put("visibleProjects", allProjects);
        //if there is no search request in session and no project has been specified in the params, add the single
        // visible project to the list
        if (noCurrentSearchRequest &&
                allProjects.size() == 1 &&
                !fieldValuesHolder.containsKey(SystemSearchConstants.forProject().getUrlParameter())) {
            Long singlePid = allProjects.iterator().next().getId();
            velocityParams.put("selectedProjects", Collections.singleton(Long.toString(singlePid)));
        } else {
            List projects = (List) fieldValuesHolder.get(SystemSearchConstants.forProject().getUrlParameter());
            if (projects != null && projects.size() == 1 && projects.get(0).equals("-1")) {
                velocityParams.put("selectedProjects", Collections.emptyList());
            } else {
                velocityParams.put("selectedProjects", projects != null ? projects : Collections.emptyList());
            }
        }
        if (allProjects.size() > MAX_PROJECTS_BEFORE_RECENT) {
            velocityParams.put("recentProjects", getRecentProjects(searcher));
        }
    }

    public boolean isShown(final ApplicationUser user, final SearchContext searchContext) {
        return true;
    }

    public String getViewHtml(final ApplicationUser user, final SearchContext searchContext, final FieldValuesHolder fieldValuesHolder, final Map displayParameters, final Action action) {
        Map<String, Object> velocityParams = getVelocityParams(user, searchContext, null, fieldValuesHolder, displayParameters, action);

        @SuppressWarnings("unchecked")
        final List<String> idsAsStrings = (List<String>) fieldValuesHolder.get(SystemSearchConstants.forProject().getUrlParameter());
        final Collection<Long> projectIds = ParameterUtils.makeListLong(idsAsStrings);
        final List<Project> projects = projectManager.convertToProjectObjects(projectIds);
        if (projects != null) {
            // Only need to create 'filteredOutProjects' if projects is not null.
            // This is used in context-searcher-view.vm template.
            final List<Project> filteredOutProjects = new ArrayList<Project>(projects.size());
            for (Iterator<Project> iterator = projects.iterator(); iterator.hasNext(); ) {
                Project project = iterator.next();
                if (!permissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, project, user)) {
                    filteredOutProjects.add(project);
                    iterator.remove();
                }
            }
            // only ID of these projects is displayed, let's sort them by ID
            Collections.sort(filteredOutProjects, WithId.ID_COMPARATOR);
            velocityParams.put("filteredOutProjects", filteredOutProjects);
        }
        velocityParams.put("selectedProjects", projects);

        return renderViewTemplate("project-searcher" + VIEW_TEMPLATE_SUFFIX, velocityParams);
    }

    public boolean isRelevantForQuery(final ApplicationUser user, final Query query) {
        return isRelevantForQuery(SystemSearchConstants.forProject().getJqlClauseNames(), query);
    }

    public Collection<Project> getRecentProjects(final ApplicationUser searcher) {
        List<Project> projects = projectHistoryManager.getProjectHistoryWithPermissionChecks(ProjectAction.VIEW_ISSUES, searcher);
        return projects.subList(0, Math.min(MAX_RECENT_PROJECTS_TO_SHOW, projects.size()));
    }

    public Collection<Project> getVisibleProjects(final ApplicationUser searcher) {
        return permissionManager.getProjects(ProjectPermissions.BROWSE_PROJECTS, searcher);
    }
}
