package com.atlassian.jira.appconsistency.db;

import com.atlassian.core.ofbiz.CoreFactory;
import com.atlassian.jira.bc.dataimport.DowngradeWorker;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.SystemPropertyKeys;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.database.DbConnectionManagerImpl;
import com.atlassian.jira.ofbiz.DefaultOfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.startup.StartupCheck;
import com.atlassian.jira.upgrade.DowngradeException;
import com.atlassian.jira.upgrade.MissingDowngradeTaskException;
import com.atlassian.jira.upgrade.UpgradeVersionHistoryManager;
import com.atlassian.jira.upgrade.util.UpgradeUtils;
import com.atlassian.jira.util.BuildUtilsInfo;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a database check that verifies that user is not running old version of JIRA on the data in the database
 * created by more recent version of JIRA. This check can be turned off by setting "-Djira.ignore.buildversion=true".
 */
public class BuildVersionCheck implements StartupCheck {
    private static final Logger log = LoggerFactory.getLogger(BuildVersionCheck.class);
    private static final String NAME = "JIRA Build Version Check";
    private static final String JIRA_IGNORE_BUILD_VERSION = "jira.ignore.buildversion";

    private final JiraProperties jiraSystemProperties;
    private final DatabaseConfigurationManager databaseConfigurationManager;
    private final BuildUtilsInfo buildUtilsInfo;
    private final UpgradeVersionHistoryManager upgradeVersionHistoryManager;

    private String faultDescription = null;
    private String htmlFaultDescription = null;

    public BuildVersionCheck(final JiraProperties jiraSystemProperties, final DatabaseConfigurationManager databaseConfigurationManager,
                             BuildUtilsInfo buildUtilsInfo, UpgradeVersionHistoryManager upgradeVersionHistoryManager) {
        this.jiraSystemProperties = jiraSystemProperties;
        this.databaseConfigurationManager = databaseConfigurationManager;
        this.buildUtilsInfo = buildUtilsInfo;
        this.upgradeVersionHistoryManager = upgradeVersionHistoryManager;
    }

    public String getName() {
        return NAME;
    }

    public boolean isOk() {
        // Look for the system property, if true do not perform build version check
        if (jiraSystemProperties.getBoolean(JIRA_IGNORE_BUILD_VERSION)) {
            log.warn("Not performing Jira Build Version check since " + JIRA_IGNORE_BUILD_VERSION + " is set to 'true'");
        } else if (databaseSetup()) {
            log.debug("Performing version check");

            final int applicationBuildVersionNumber = getAppBuildNumber();
            final int databaseBuildVersionNumber = getDbBuildNumber();

            if (databaseBuildVersionNumber > applicationBuildVersionNumber) {
                if (jiraSystemProperties.getBoolean(SystemPropertyKeys.JIRA_DOWNGRADE_ALLOWED)) {
                    log.info("Database build number is: " + databaseBuildVersionNumber + ", Application build number is: " + applicationBuildVersionNumber);
                    log.info("Attempting a data downgrade because " + SystemPropertyKeys.JIRA_DOWNGRADE_ALLOWED + " is set to 'true'");
                    // an in-place downgrade is allowed - but we need to check if we have sufficient downgrade tasks
                    return attemptDowngrade();
                } else {
                    log.debug("There is a data consistency error: Database build number is: " + databaseBuildVersionNumber
                            + ", Application build number is: " + applicationBuildVersionNumber);
                    // We have an error so JIRA should not be started!
                    // Check if we have all required downgrade tasks in order to attempt downgrade
                    setBuildNumberInconsistentMessages(canAttemptDowngrade());
                    return false;
                }
            } else {
                if (jiraSystemProperties.getBoolean(SystemPropertyKeys.JIRA_DOWNGRADE_ALLOWED)) {
                    // "jira.downgrade.allowed" Property is set but not required
                    log.warn("System property " + SystemPropertyKeys.JIRA_DOWNGRADE_ALLOWED +
                            " is set to 'true' but no downgrade is required. It is recommended to remove this setting unless you actually need to downgrade.");
                }
            }
        }
        return true;
    }

    private boolean canAttemptDowngrade() {
        try {
            return buildDowngradeWorker().canDowngrade();
        } catch (DowngradeException ex) {
            log.error("Unexpected error in the Downgrade Task framework.", ex);
            return false;
        }
    }

    private DowngradeWorker buildDowngradeWorker() {
        // DbConnectionManager does not exist in Bootstrap Container, but we can build one easy enough
        final DbConnectionManager dbConnectionManager = new DbConnectionManagerImpl(databaseConfigurationManager, CoreFactory.getGenericDelegator());
        // OfBizDelegator does not exist in Bootstrap Container, but we can build one easy enough.
        final OfBizDelegator ofBizDelegator = new DefaultOfBizDelegator(CoreFactory.getGenericDelegator());
        // ApplicationProperties exists even in Bootstrap Container
        final ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();
        return new DowngradeWorker(buildUtilsInfo, dbConnectionManager, ofBizDelegator, applicationProperties,
                upgradeVersionHistoryManager);
    }

    private void setBuildNumberInconsistentMessages(final boolean canAttemptDowngrade) {
        buildFaultDescriptions(
                "Failed to start JIRA due to a build number inconsistency.",
                "The data present in your database is newer than the version of JIRA you are trying to startup." +
                        "\nDatabase build number: " + getDbBuildNumber() +
                        "\nJIRA app build number: " + getAppBuildNumber(),
                "Please use the correct version of JIRA. You are running: " + buildUtilsInfo.getBuildInformation()
        );
        if (canAttemptDowngrade) {
            // Just add to log file for now.
            faultDescription += "Alternatively, you can allow a Downgrade to occur by setting the system property " +
                    SystemPropertyKeys.JIRA_DOWNGRADE_ALLOWED + "=true.\n";
        }
    }

    /**
     * Attempt an in-place downgrade.
     * <p>
     * If the downgrade fails we set appropriate Fault Description for the StartupCheck and return false which means
     * JIRA will fail its startup checks.
     * </p>
     *
     * @return true if successful
     */
    private boolean attemptDowngrade() {
        DowngradeWorker downgradeWorker = buildDowngradeWorker();
        try {
            // Run the Downgrade Tasks
            downgradeWorker.downgrade(DowngradeWorker.Mode.START_UP);

            // Now update the various version and build number values in the DB
            downgradeWorker.writeNewBuildNumbers();
            return true;
        } catch (MissingDowngradeTaskException ex) {
            buildFaultDescriptions(
                    // The MissingDowngradeTaskException has a user friendly message including missing downgrade task number
                    ex.getMessage(),
                    "Database build number: " + getDbBuildNumber(),
                    "JIRA app build number: " + getAppBuildNumber(),
                    "JIRA app version: " + buildUtilsInfo.getBuildInformation()
            );
            return false;
        } catch (DowngradeException | RuntimeException ex) {
            buildFaultDescriptions(
                    "An unexpected error occured while trying to downgrade.",
                    "Database build number: " + getDbBuildNumber(),
                    "JIRA app build number: " + getAppBuildNumber(),
                    "JIRA app version: " + buildUtilsInfo.getBuildInformation()
            );
            htmlFaultDescription += "<p>See the log files for details</p>";
            faultDescription += "Error: " + ex.getMessage() + "\n";
            log.error("An unexpected error occured while trying to downgrade.", ex);
            return false;
        }
    }

    private void buildFaultDescriptions(String... messages) {
        htmlFaultDescription = "";
        faultDescription = "";
        for (String message : messages) {
            faultDescription += message + "\n";
            htmlFaultDescription += "<p>" + message.replace("\n", "<br>") + "</p>";
        }
    }

    private boolean databaseSetup() {
        // check to see if the project table exists
        return UpgradeUtils.tableExists("project");
    }

    public String getHTMLFaultDescription() {
        return htmlFaultDescription;
    }

    public String getFaultDescription() {
        return "\n\n" + StringUtils.repeat("*", 100) + '\n' +
                faultDescription +
                StringUtils.repeat("*", 100) + "\n";
    }

    private int getDbBuildNumber() {
        return buildUtilsInfo.getDatabaseBuildNumber();
    }

    private int getAppBuildNumber() {
        return buildUtilsInfo.getApplicationBuildNumber();
    }

    @Override
    public String toString() {
        return NAME;
    }
}
