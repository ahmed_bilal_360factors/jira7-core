package com.atlassian.jira.plugin;

import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.loaders.PluginLoader;

import java.util.List;

/**
 * @since v7.3
 */
public interface PluginDirectoryLoaderFactory {
    PluginLoader getDirectoryPluginLoader(List<PluginFactory> pluginFactories);
}
