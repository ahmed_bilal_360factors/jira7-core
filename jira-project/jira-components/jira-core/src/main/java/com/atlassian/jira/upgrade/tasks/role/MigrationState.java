package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Objects;
import java.util.function.Function;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Represents the state of JIRA in the renaissance migration (i.e. JIRA 6.x single use permission to JIRA 7.x multiple
 * application).
 *
 * @since v7.0
 */
public final class MigrationState {
    private final Licenses licenses;
    private final ApplicationRoles roles;
    private final ImmutableList<Runnable> afterSaveTasks;
    private final MigrationLog log;

    public MigrationState(Licenses licenses, final ApplicationRoles roles, ImmutableList<Runnable> afterSaveTasks,
                          MigrationLog log) {
        this.log = log;
        this.roles = notNull("roles", roles);
        this.licenses = notNull("licenses", licenses);
        this.afterSaveTasks = notNull("afterSaveTasks", afterSaveTasks);
    }

    public MigrationState log(AuditEntry event) {
        return new MigrationState(licenses, roles, afterSaveTasks, log.log(event));
    }

    ApplicationRoles applicationRoles() {
        return roles;
    }

    Licenses licenses() {
        return licenses;
    }

    public ImmutableList<Runnable> afterSaveTasks() {
        return afterSaveTasks;
    }

    public MigrationLog log() {
        return log;
    }

    MigrationState changeLicenses(Function<Licenses, Licenses> action) {
        notNull("action", action);

        return new MigrationState(action.apply(licenses), roles, afterSaveTasks, log);
    }

    MigrationState withAfterSaveTask(Runnable afterSaveTask) {
        notNull("afterSaveTask", afterSaveTask);

        return new MigrationState(licenses, roles, ImmutableList.<Runnable>builder()
                .addAll(afterSaveTasks)
                .add(afterSaveTask)
                .build(), log);
    }

    MigrationState changeApplicationRole(ApplicationKey key, Function<ApplicationRole, ApplicationRole> action) {
        notNull("action", action);
        notNull("key", key);

        Supplier<ApplicationRole> supplier = () -> ApplicationRole.forKey(key);
        ApplicationRole orElse = roles.get(key).getOrElse(supplier);
        ApplicationRole roleWithLog = action.apply(orElse);
        return new MigrationState(licenses, roles.put(roleWithLog), afterSaveTasks, log);
    }

    MigrationState changeEachRole(Function<ApplicationRole, ApplicationRole> action) {
        notNull("action", action);

        ApplicationRoles newRoles = roles;
        for (ApplicationRole role : roles.asMap().values()) {
            newRoles = newRoles.put(action.apply(role));
        }
        return new MigrationState(licenses, newRoles, afterSaveTasks, log);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final MigrationState that = (MigrationState) o;
        return Objects.equals(licenses, that.licenses) &&
                Objects.equals(roles, that.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(licenses, roles);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("licenses", licenses)
                .append("roles", roles)
                .append("tasks", afterSaveTasks)
                .append("logs", log)
                .toString();
    }

}
