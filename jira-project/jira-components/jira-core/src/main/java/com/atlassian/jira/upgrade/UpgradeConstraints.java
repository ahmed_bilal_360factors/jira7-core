package com.atlassian.jira.upgrade;

/**
 * Used to determine if upgrade task should be run and what target version number should
 * be stored in database. It allows to have one JIRA release running on different database versions
 * (e.g. same OnDemand release can run on higher version on some instances)
 *
 * @since 7.0
 */
public interface UpgradeConstraints {
    /**
     * Gets the target number of database that should be set once all upgrade tasks are run.
     *
     * @return target number of database.
     */
    int getTargetDatabaseBuildNumber();

    /**
     * Checks if given upgrade task number should be run by upgrade manager.
     *
     * @param upgradeTaskNumber number of upgrade task
     * @return true if task should be run, false otherwise.
     */
    boolean shouldRunTask(String upgradeTaskNumber);
}
