package com.atlassian.jira.project.template.hook;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.nullToEmpty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ScreenSchemeTemplateImpl implements ScreenSchemeTemplate {
    private final String key;
    private final String name;
    private final String description;
    private final String defaultScreen;
    private final Optional<String> createScreen;
    private final Optional<String> editScreen;
    private final Optional<String> viewScreen;

    public ScreenSchemeTemplateImpl(
            @JsonProperty("key") String key,
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("default-screen") String defaultScreen,
            @JsonProperty("create-screen") String createScreen,
            @JsonProperty("edit-screen") String editScreen,
            @JsonProperty("view-screen") String viewScreen) {
        this.key = checkNotNull(key).toUpperCase();
        this.name = checkNotNull(name);
        this.description = nullToEmpty(description);
        this.defaultScreen = checkNotNull(defaultScreen).toUpperCase();
        this.createScreen = toOptionalScreen(createScreen);
        this.editScreen = toOptionalScreen(editScreen);
        this.viewScreen = toOptionalScreen(viewScreen);
    }

    private static Optional<String> toOptionalScreen(String screenKey) {
        if (screenKey == null) {
            return Optional.empty();
        }
        return Optional.of(screenKey.toUpperCase());
    }

    @Override
    public String key() {
        return key;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String description() {
        return description;
    }

    @Override
    public String defaultScreen() {
        return defaultScreen;
    }

    @Override
    public Optional<String> createScreen() {
        return createScreen;
    }

    @Override
    public Optional<String> editScreen() {
        return editScreen;
    }

    @Override
    public Optional<String> viewScreen() {
        return viewScreen;
    }
}
