package com.atlassian.jira.upgrade;

import com.atlassian.jira.bc.dataimport.DowngradeUtil;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestManager;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;
import com.atlassian.jira.startup.JiraLauncher;
import com.atlassian.jira.startup.JiraStartupChecklist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumSet;

/**
 * Fires a reindex if required by a downgrade task that ran previously
 *
 * @since v6.4.6
 */
public class DowngradeReindexLauncher implements JiraLauncher {
    private static final Logger log = LoggerFactory.getLogger(DowngradeReindexLauncher.class);

    @Override
    public void start() {
        ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();
        if ("true".equals(applicationProperties.getString(APKeys.JIRA_SETUP)) && JiraStartupChecklist.startupOK()) {
            requestReindexIfRequired(applicationProperties);
        }
    }

    private void requestReindexIfRequired(final ApplicationProperties applicationProperties) {
        final ReindexRequirement reindexRequirement = DowngradeUtil.getReindexRequirement(applicationProperties);
        final boolean runInBackground;
        switch (reindexRequirement) {
            case NONE:
                return;
            case BACKGROUND:
                runInBackground = true;
                log.info("Downgrade tasks have triggered a background reindex.");
                break;
            case FOREGROUND:
                runInBackground = false;
                log.info("Downgrade tasks have triggered a foreground reindex.");
                break;
            default:
                throw new IllegalStateException();
        }
        // We use the Manager, because the Service checks permission for a logged in admin
        final ReindexRequestManager reindexRequestManager = ComponentAccessor.getComponent(ReindexRequestManager.class);
        reindexRequestManager.requestReindex(ReindexRequestType.IMMEDIATE, EnumSet.allOf(AffectedIndex.class), EnumSet.allOf(SharedEntityType.class));
        // Setting runInBackground = false will not do a "real" foreground reindex, because users are not johnsonned.
        // Set the waitForCompletion flag to reflect foreground vs background
        reindexRequestManager.processPendingRequests(!runInBackground, EnumSet.of(ReindexRequestType.IMMEDIATE), runInBackground);
        if (runInBackground) {
            log.info("Background reindex has been started.");
        } else {
            log.info("Full foreground reindex complete.");
        }
        DowngradeUtil.setReindexRequirement(applicationProperties, ReindexRequirement.NONE);
    }

    @Override
    public void stop() {
        // no op
    }
}
