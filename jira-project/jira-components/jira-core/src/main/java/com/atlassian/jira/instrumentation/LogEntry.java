package com.atlassian.jira.instrumentation;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * A list of statistics collected by request.
 *
 * @since v7.1
 */
public class LogEntry {
    @JsonIgnore
    private final String traceId;
    @JsonProperty
    private final String path;
    @JsonProperty
    private final String queryString;
    @JsonProperty
    private final Map<String, List<Statistics>> data;

    public LogEntry(@NotNull String traceId, @NotNull String path, @Nullable String queryString,
                    @Nonnull Map<String, List<Statistics>> data) {
        this.traceId = requireNonNull(traceId, "traceId");
        this.path = requireNonNull(path, "path");
        this.data = requireNonNull(data);
        this.queryString = queryString;
    }

    public String getTraceId() {
        return traceId;
    }

    public String getPath() {
        return path;
    }

    public String getQueryString() {
        return queryString;
    }

    public Map<String, List<Statistics>> getData() {
        return data;
    }
}