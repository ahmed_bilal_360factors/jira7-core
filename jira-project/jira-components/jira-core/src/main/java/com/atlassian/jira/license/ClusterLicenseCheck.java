package com.atlassian.jira.license;

/**
 * Implements license checks required to permit running JIRA in clustered mode ("DataCenter").
 *
 * @since 7.0
 */
public interface ClusterLicenseCheck extends LicenseCheck {
}
