package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.model.querydsl.QOSPropertyEntry;
import com.atlassian.jira.model.querydsl.QOSPropertyString;
import com.atlassian.jira.propertyset.CachingOfBizPropertyEntryStore;
import com.atlassian.jira.propertyset.OfBizPropertyEntryStore;
import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Remove jira.user.locale preference if current value is -1
 *
 * @since v7.0
 */
public class UpgradeTask_Build70025 extends AbstractImmediateUpgradeTask {
    private static final Integer BUILD_NUMBER = 70025;
    // Should not use more than 1000 as it will be failed in Oracle
    private static final int MAX_ROWS_IN_BATCH = 900;
    private static final Logger LOG = LoggerFactory.getLogger(UpgradeTask_Build70025.class);

    private final DbConnectionManager dbConnectionManager;

    public UpgradeTask_Build70025(final DbConnectionManager dbConnectionManager) {
        this.dbConnectionManager = dbConnectionManager;
    }

    @Override
    public int getBuildNumber() {
        return BUILD_NUMBER;
    }

    @Override
    public String getShortDescription() {
        return "Remove jira.user.locale preference if current value is -1";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception {
        dbConnectionManager.execute(dbConnection ->
        {
            dbConnection.setAutoCommit(false);

            List<Long> propertyIds = getMinusOneUserLocalePropertyIds(dbConnection);

            if (!propertyIds.isEmpty()) {
                LOG.debug("Removing {} user locale preferences with value -1", propertyIds.size());
                List<List<Long>> batches = Lists.partition(propertyIds, MAX_ROWS_IN_BATCH);
                for (int i = 0; i < batches.size(); i++) {
                    LOG.debug("Deleting batch {} of {}", i + 1, batches.size());
                    deleteMinusOneUserLocaleProperty(batches.get(i), dbConnection);
                }
            }
            dbConnection.commit();
        });

        OfBizPropertyEntryStore entryStore = ComponentAccessor.getComponent(OfBizPropertyEntryStore.class);
        if (entryStore instanceof CachingOfBizPropertyEntryStore) {
            ((CachingOfBizPropertyEntryStore) entryStore).refreshAll();
        }
    }

    private List<Long> getMinusOneUserLocalePropertyIds(DbConnection dbConnection) {
        QOSPropertyEntry propertyEntry = QOSPropertyEntry.O_S_PROPERTY_ENTRY;
        QOSPropertyString propertyString = QOSPropertyString.O_S_PROPERTY_STRING;

        // MSSQL meet incompatible data type problem when we use equal operator (IN, EQUALS) so we have to use LIKE instead
        List<Long> propertyIds = dbConnection.newSqlQuery()
                .select(propertyString.id)
                .from(propertyEntry).join(propertyString)
                .on(propertyEntry.id.eq(propertyString.id))
                .where(propertyEntry.propertyKey.eq("jira.user.locale").and(propertyString.value.like("-1")))
                .fetch();

        return propertyIds;
    }

    private void deleteMinusOneUserLocaleProperty(List<Long> ids, DbConnection dbConnection) {
        QOSPropertyEntry propertyEntry = QOSPropertyEntry.O_S_PROPERTY_ENTRY;
        QOSPropertyString propertyString = QOSPropertyString.O_S_PROPERTY_STRING;

        dbConnection.delete(propertyString).where(propertyString.id.in(ids)).execute();
        dbConnection.delete(propertyEntry).where(propertyEntry.id.in(ids)).execute();
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        return false;
    }
}
