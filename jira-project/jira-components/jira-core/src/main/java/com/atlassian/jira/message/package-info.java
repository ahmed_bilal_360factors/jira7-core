/**
 * Package for JIRA Messaging utilities.
 *
 * @since 7.0
 */
@ParametersAreNonnullByDefault package com.atlassian.jira.message;

import javax.annotation.ParametersAreNonnullByDefault;