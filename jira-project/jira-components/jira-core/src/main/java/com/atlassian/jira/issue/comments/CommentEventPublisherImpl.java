package com.atlassian.jira.issue.comments;

import java.util.Map;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.comment.CommentCreatedEvent;
import com.atlassian.jira.event.comment.CommentDeletedEvent;
import com.atlassian.jira.event.comment.CommentUpdatedEvent;
import com.atlassian.jira.event.issue.IssueEventBundle;
import com.atlassian.jira.event.issue.IssueEventBundleFactory;
import com.atlassian.jira.event.issue.IssueEventManager;
import com.atlassian.jira.event.type.EventType;

public class CommentEventPublisherImpl implements CommentEventPublisher {

    private final IssueEventManager issueEventManager;
    private final EventPublisher eventPublisher;
    private final IssueEventBundleFactory issueEventBundleFactory;

    public CommentEventPublisherImpl(final IssueEventManager issueEventManager,
                                     final EventPublisher eventPublisher,
                                     final IssueEventBundleFactory issueEventBundleFactory) {
        this.issueEventManager = issueEventManager;
        this.eventPublisher = eventPublisher;
        this.issueEventBundleFactory = issueEventBundleFactory;
    }

    @Override
    public void publishCommentUpdatedEvent(final Comment comment, Map<String, Object> parameters) {
        IssueEventBundle issueCommentBundle = issueEventBundleFactory.createCommentEditedBundle(comment.getIssue(),
                comment.getUpdateAuthorApplicationUser(),
                comment,
                parameters);

        issueEventManager.dispatchEvent(issueCommentBundle);
        issueEventManager.dispatchRedundantEvent(EventType.ISSUE_COMMENT_EDITED_ID,
                comment.getIssue(),
                comment.getUpdateAuthorUser(),
                comment,
                null,
                null,
                parameters);
    }

    @Override
    public void publishCommentCreatedEvent(final Comment comment, Map<String, Object> parameters) {
        IssueEventBundle issueCommentBundle = issueEventBundleFactory.createCommentAddedBundle(comment.getIssue(),
                comment.getUpdateAuthorApplicationUser(),
                comment,
                parameters);
        issueEventManager.dispatchEvent(issueCommentBundle);

        issueEventManager.dispatchRedundantEvent(EventType.ISSUE_COMMENTED_ID,
                comment.getIssue(),
                comment.getUpdateAuthorUser(),
                comment,
                null,
                null,
                parameters);
    }

    @Override
    public void publishCommentDeletedEvent(final Comment comment) {
        eventPublisher.publish(new CommentDeletedEvent(comment));
    }
}
