package com.atlassian.jira.web.action.admin.instrumentation.cache;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Bean to hold instrumented information about cache activity.
 *
 * @since v7.1.0
 */
public class CacheDisplayBean {
    private String name;
    private String cacheType;
    private long hitCount;
    private long missCount;
    private long loads;
    private double avgLoadTime;
    private long size;

    private long putCount;

    public CacheDisplayBean(@Nonnull String name, @Nonnull String type) {
        this.name = requireNonNull(name);
        this.cacheType = requireNonNull(type);
    }

    public String getName() {
        return this.name;
    }

    public String getShortName() {
        return shorten(name);
    }


    public String getCacheType() {
        return cacheType;
    }

    public long getHitCount() {
        return hitCount;
    }

    public long getMissCount() {
        return missCount;
    }

    public double getAvgLoadTime() {
        return avgLoadTime;
    }

    public long getLoads() {
        return loads;
    }

    public void setLoads(final long loads) {
        this.loads = loads;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setCacheType(final String cacheType) {
        this.cacheType = cacheType;
    }

    public void setHitCount(final long hitCount) {
        this.hitCount = hitCount;
    }

    public void setMissCount(final long missCount) {
        this.missCount = missCount;
    }

    public void setAvgLoadTime(final double avgLoadTime) {
        this.avgLoadTime = avgLoadTime;
    }

    public void setSize(final long size) {
        this.size = size;
    }

    public long getSize() {
        return size;
    }

    public long getPutCount() {
        return putCount;
    }

    public void setPutCount(long count) {
        putCount = count;
    }

    private static String shorten(String name) {
        String[] splits = name.split("\\.");
        if (splits.length > 2) {
            return splits[splits.length - 2] + "." + splits[splits.length - 1];
        } else {
            return name;
        }
    }

    protected static CacheDisplayBean add(@Nonnull CacheDisplayBean arg1, @Nonnull CacheDisplayBean arg2) {
        requireNonNull(arg1);
        requireNonNull(arg2);

        CacheDisplayBean result = new CacheDisplayBean(arg1.name, arg1.cacheType);
        result.setHitCount(arg1.hitCount + arg2.hitCount);
        result.setMissCount(arg1.missCount + arg2.missCount);
        result.setPutCount(arg1.putCount + arg2.putCount);
        result.setLoads(arg1.loads + arg2.loads);
        result.setAvgLoadTime(arg1.avgLoadTime + arg2.avgLoadTime);
        result.setSize(arg1.size);
        return result;
    }

}