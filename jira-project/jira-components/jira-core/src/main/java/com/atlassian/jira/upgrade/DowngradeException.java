package com.atlassian.jira.upgrade;

/**
 * Indicates an error that occured while trying to downgrade JIRA such that the downgrade cannot complete successfully.
 *
 * @since v6.4.6
 */
public class DowngradeException extends Exception {
    public DowngradeException(final String message) {
        super(message);
    }
}
