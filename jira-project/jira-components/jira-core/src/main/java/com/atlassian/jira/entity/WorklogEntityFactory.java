package com.atlassian.jira.entity;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogImpl2;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @since v6.1
 */
public class WorklogEntityFactory extends AbstractEntityFactory<Worklog> {
    private final Issue issue;
    private final ProjectRoleManager projectRoleManager;

    public WorklogEntityFactory(final Issue issue, final ProjectRoleManager projectRoleManager) {
        this.issue = issue;
        this.projectRoleManager = projectRoleManager;
    }

    @Override
    public Map<String, Object> fieldMapFrom(@Nonnull Worklog worklog) {
        if (worklog.getIssue() == null) {
            throw new IllegalArgumentException("Cannot store a worklog against a null issue.");
        }

        Map<String, Object> fields = new HashMap<String, Object>();
        fields.put("id", worklog.getId());
        fields.put("issue", worklog.getIssue().getId());
        fields.put("author", worklog.getAuthorKey());
        fields.put("updateauthor", worklog.getUpdateAuthorKey());
        fields.put("body", worklog.getComment());
        fields.put("grouplevel", worklog.getGroupLevel());
        fields.put("rolelevel", worklog.getRoleLevelId());
        fields.put("timeworked", worklog.getTimeSpent());
        fields.put("startdate", new Timestamp(worklog.getStartDate().getTime()));
        fields.put("created", new Timestamp(worklog.getCreated().getTime()));
        fields.put("updated", new Timestamp(worklog.getUpdated().getTime()));
        return fields;
    }

    @Override
    public String getEntityName() {
        return "Worklog";
    }

    @Override
    public Worklog build(final GenericValue gv) {

        Timestamp startDateTS = gv.getTimestamp("startdate");
        Timestamp createdTS = gv.getTimestamp("created");
        Timestamp updatedTS = gv.getTimestamp("updated");
        Long rolelevel = gv.getLong("rolelevel");

        return new WorklogImpl2(
                issue,
                gv.getLong("id"),
                gv.getString("author"),
                gv.getString("body"),
                startDateTS == null ? null : new Date(startDateTS.getTime()),
                gv.getString("grouplevel"),
                rolelevel,
                gv.getLong("timeworked"),
                gv.getString("updateauthor"),
                createdTS == null ? null : new Date(createdTS.getTime()),
                updatedTS == null ? null : new Date(updatedTS.getTime()),
                rolelevel != null ? projectRoleManager.getProjectRole(rolelevel) : null);
    }

}
