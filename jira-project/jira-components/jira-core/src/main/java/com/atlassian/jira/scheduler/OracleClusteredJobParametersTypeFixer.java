package com.atlassian.jira.scheduler;

import com.atlassian.fugue.Either;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.database.DatabaseSchema;
import com.atlassian.jira.ofbiz.DefaultOfBizConnectionFactory;
import com.atlassian.jira.startup.StartupCheck;
import com.atlassian.jira.upgrade.util.UpgradeUtils;
import com.atlassian.util.concurrent.LazyReference;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableSet;
import com.opensymphony.util.TextUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * Workaround for JRA-47267, an inconsistent column type on Oracle introduced in JIRA 6.4.8.
 * <p>
 * In the schema backports that enabled exports from JIRA Cloud to JIRA Server during the 7.0 development
 * period, the "clusteredjob" table's "parameters" column incorrectly got the type {@code LONG RAW} instead
 * of {@code BLOB} on Oracle.  The result of this is that the column can be written to, but attempts to read
 * from it fail, which breaks the scheduler.
 * </p>
 * <p>
 * This attempts to detect the problem and correct it, either as a one-off or in reaction to an exception while
 * trying to read from the table.  The column is modified from {@code LONG RAW} to {@code BLOB}, which Oracle
 * specifically allows because {@code LONG RAW} is an older, deprecated format and {@code BLOB} is preferred.
 * Unfortunately, this breaks all of the existing indexes on the table, and they must be rebuilt.
 * </p>
 * <p>
 * This is the kind of thing we would normally do in an upgrade task, but unfortunately that is way too late.
 * The startup checklist and the {@code start()} method of the services container try to ensure that certain
 * scheduled jobs exist, which results in them trying to read from this table before the upgrade tasks have
 * run.
 * </p>
 *
 * @since v7.0.6
 */
public class OracleClusteredJobParametersTypeFixer {
    private static final Logger LOG = LoggerFactory.getLogger(OracleClusteredJobParametersTypeFixer.class);
    private static final String NAME = "Oracle clusteredjob.parameters type check";

    static final Set<String> INDEXES = ImmutableSet.of(
            "pk_clusteredjob",
            "clusteredjob_jobid_idx",
            "clusteredjob_jrk_idx",
            "clusteredjob_nextrun_idx");


    private final DatabaseConfigurationManager dbConfigManager;

    private final LazyReference<Either<Exception, Boolean>> resultRef = new LazyReference<Either<Exception, Boolean>>() {
        @Override
        protected Either<Exception, Boolean> create() {
            try {
                return Either.right(applyFix());
            } catch (Exception e) {
                return Either.left(e);
            }
        }
    };

    public OracleClusteredJobParametersTypeFixer(DatabaseConfigurationManager dbConfigManager) {
        this.dbConfigManager = dbConfigManager;
    }

    private boolean isFixNeeded() {
        return dbConfigManager.isDatabaseSetup()
                && dbConfigManager.getDatabaseConfiguration().isOracle()
                && !"BLOB".equalsIgnoreCase(getParametersColumnType());
    }

    @VisibleForTesting
    Either<Exception, Boolean> getResult() {
        return requireNonNull(resultRef.get());
    }

    public boolean fix() {
        return getResult().getOrElse(false);
    }

    public StartupCheck asStartupCheck() {
        return new StartupCheckView();
    }

    private boolean applyFix() throws SQLException {
        if (!isFixNeeded()) {
            return false;
        }

        final String schema = getSchemaName();
        final String prefix = schema.isEmpty() ? "" : (schema + '.');

        try (Connection conn = getConnection()) {
            fixParametersColumn(conn, prefix);
            fixIndexes(conn, prefix);
        }

        LOG.info("JRA-47267: Successfully changed type of clusteredjob.parameters from LONG RAW to BLOB.");
        return true;
    }


    @VisibleForTesting
    String getParametersColumnType() {
        final String clusteredJob = UpgradeUtils.getExactTableName("ClusteredJob");
        return UpgradeUtils.getColumnType(clusteredJob, "parameters");
    }

    @VisibleForTesting
    String getSchemaName() {
        return DatabaseSchema.getSchemaName();
    }

    @VisibleForTesting
    Connection getConnection() throws SQLException {
        return DefaultOfBizConnectionFactory.getInstance().getConnection();
    }


    private static void fixParametersColumn(Connection conn, String prefix) throws SQLException {
        try (Statement statement = conn.createStatement()) {
            statement.execute("ALTER TABLE " + prefix + "clusteredjob MODIFY (parameters BLOB)");
        }
    }

    private static void fixIndexes(Connection conn, String prefix) {
        // See https://docs.oracle.com/cd/B13789_01/appdev.101/b10796/adlob_lo.htm#1022887
        // We can't trust any of the indexes after altering the type of the column, even if it
        // is a seemingly unrelated one.
        INDEXES.forEach(index -> fixIndex(conn, prefix, index));
    }

    private static void fixIndex(Connection conn, String prefix, String index) {
        try (Statement statement = conn.createStatement()) {
            statement.execute("ALTER INDEX " + prefix + index + " REBUILD");
        } catch (SQLException sqle) {
            LOG.warn("Rebuild unexpectedly failed for index '{}': {}", index, sqle.toString());
            LOG.debug("Full stack trace", sqle);
        }
    }

    class StartupCheckView implements StartupCheck {
        @Override
        public String getName() {
            return NAME;
        }

        @Override
        public boolean isOk() {
            return getResult().isRight();
        }

        @Nullable
        @Override
        public String getFaultDescription() {
            return getResult().fold(
                    e -> "\n\n" + StringUtils.repeat("*", 100) + '\n' + e + '\n' + StringUtils.repeat("*", 100) + '\n',
                    result -> null);
        }

        @Nullable
        @Override
        public String getHTMLFaultDescription() {
            return getResult().fold(
                    e -> TextUtils.plainTextToHtml(e.toString()),
                    result -> null);
        }
    }
}

