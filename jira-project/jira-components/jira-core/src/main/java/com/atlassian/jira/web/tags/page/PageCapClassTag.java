package com.atlassian.jira.web.tags.page;

import com.atlassian.jira.web.util.PageCapabilitiesImpl;
import com.opensymphony.module.sitemesh.taglib.AbstractTag;

import javax.servlet.http.HttpServletRequest;

public class PageCapClassTag extends AbstractTag {
    public int doEndTag() {
        try {
            final HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
            getOut().write(PageCapabilitiesImpl.fromRequest(request).getBodyCssClass());
        } catch (Exception e) {
            trace(e);
        }
        return EVAL_PAGE;
    }
}
