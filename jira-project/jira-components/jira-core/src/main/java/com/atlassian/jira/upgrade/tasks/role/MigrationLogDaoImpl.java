package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.auditing.AuditRecordImpl;
import com.atlassian.jira.auditing.ChangedValue;
import com.atlassian.jira.auditing.SearchTokenizer;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.sql.Timestamp;
import java.util.Map;

public class MigrationLogDaoImpl implements MigrationLogDao {
    private static final Logger log = LoggerFactory.getLogger(MigrationLogDaoImpl.class);

    private static final long SYSADMIN_AUTHOR_TYPE = 1L;
    private static final long OTHER_AUTHOR_TYPE = 0L;
    private static final String OFBIZ_AUDIT_ENTITY_NAME = "AuditLog";
    private static final String MIGRATION_AUDIT_CATEGORY_ID = "migration";
    private static final String MIGRATION_AUDIT_CATEGORY_NAME = "jira.auditing.category.migration";
    private static final String NO_KEY = "0";

    private final OfBizDelegator ofBizDelegator;

    public MigrationLogDaoImpl(final OfBizDelegator ofBizDelegator) {
        this.ofBizDelegator = ofBizDelegator;
    }

    @Override
    public void write(@Nonnull MigrationLog record) {
        for (AuditEntry event : record.events()) {
            try {
                store(event);
                log.info(event.getSourceClass() + ": JIRA 7.0 Role Migrations: " + event.toLogMessage());
            } catch (Exception e) {
                // There's some kind of bug in the logger code.
                // We can't abort migration at this point, and we don't want to stop upgrade due to this,
                // so log and continue.
                log.warn("Error in writing migration logs. Continuing...", e);
            }
        }
    }

    public void store(AuditEntry event) {
        final Map<String, Object> values = Maps.newHashMap();
        values.put(AuditRecordImpl.CREATED, new Timestamp(System.currentTimeMillis()));
        values.put(AuditRecordImpl.AUTHOR_KEY, null);
        values.put(AuditRecordImpl.SUMMARY, event.getSummary());
        values.put(AuditRecordImpl.CATEGORY, MIGRATION_AUDIT_CATEGORY_ID);
        // Cloud admins should see migration logs except for those that only make sense for BTF like license moving
        values.put(AuditRecordImpl.AUTHOR_TYPE, event.eventShowsInCloudLog() ? OTHER_AUTHOR_TYPE : SYSADMIN_AUTHOR_TYPE);
        values.put(AuditRecordImpl.EVENT_SOURCE, event.getSourceClass().getSimpleName());
        values.put(AuditRecordImpl.LONG_DESCRIPTION, event.getDescription());

        values.put(AuditRecordImpl.OBJECT_ID, NO_KEY);
        values.put(AuditRecordImpl.OBJECT_NAME, event.getChangedObject() == null ? "No change" : event.getChangedObject());
        values.put(AuditRecordImpl.OBJECT_PARENT_ID, NO_KEY);
        values.put(AuditRecordImpl.OBJECT_PARENT_NAME, "");
        values.put(AuditRecordImpl.OBJECT_TYPE, event.getType().toString());

        values.put(AuditRecordImpl.SEARCH_FIELD, computeSearchField(event.getSummary(), event.getChangedValues(),
                event.getSourceClass().getSimpleName()));

        final GenericValue gv = ofBizDelegator.createValue(OFBIZ_AUDIT_ENTITY_NAME, values);

        if (event.getChangedValues() != null) {
            Long entryId = gv.getLong("id");
            storeChangedValues(entryId, event.getChangedValues());
        }

    }

    public static String computeSearchField(@Nonnull String summary, @Nullable Iterable<ChangedValue> changedValues,
                                            String eventSource) {
        final SearchTokenizer tokenizer = new SearchTokenizer();

        tokenizer.put(summary);
        tokenizer.put(MIGRATION_AUDIT_CATEGORY_NAME);

        if (StringUtils.isNotEmpty(eventSource)) {
            tokenizer.put(eventSource);
        }

        if (changedValues != null) {
            for (final ChangedValue changedValue : changedValues) {
                tokenizer.put(changedValue.getName()); // group name
                tokenizer.put(changedValue.getFrom()); // existing permissions
                tokenizer.put(changedValue.getTo());   // new roles and permissions
            }
        }

        return tokenizer.getTokenizedString();
    }

    protected void storeChangedValues(Long entryOfBizId, @Nonnull final Iterable<ChangedValue> changedValues) {
        for (final ChangedValue changedValue : changedValues) {
            final Map<String, Object> values = Maps.newHashMap();
            values.put("logId", entryOfBizId);
            values.put("name", changedValue.getName());
            values.put("deltaFrom", changedValue.getFrom());
            values.put("deltaTo", changedValue.getTo());

            ofBizDelegator.createValue("AuditChangedValue", values);
        }
    }
}
