package com.atlassian.jira.web.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.function.Predicate;

/**
 * Tests if a request in cloud comes from the control IP.
 *
 * Control IP is used by <a href="https://extranet.atlassian.com/display/PIPE/Icebat">icebat</a> during seamless
 * upgrade (instant up) of a cloud instance to ensure communication with JIRA.
 */
public class CloudControlIPCheck implements Predicate<HttpServletRequest> {
    private static final Logger log = LoggerFactory.getLogger(CloudControlIPCheck.class);

    @Override
    public boolean test(HttpServletRequest request) {
        log.warn("{} is only available in cloud offering", request.getRequestURI());
        return false;
    }
}
