package com.atlassian.jira.servermetrics;

import com.atlassian.analytics.api.annotations.EventName;
import com.google.common.base.MoreObjects;

import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
@EventName("jira.http.request.stats")
@ParametersAreNonnullByDefault
public class RequestMetricsEvent {
    private final String url;
    private final Optional<String> requestKey;
    private final long reqTime;
    private final long requestUserTime;
    private final long requestCpuTime;
    private final long requestGCDuration;
    private final long requestGCCount;
    private final List<CheckpointTiming> checkpointReachTime;
    private final List<CheckpointTiming> activityTimings;
    private final Optional<String> requestCorrelationId;

    public RequestMetricsEvent(
            String url,
            Optional<String> requestKey,
            long reqTime,
            long requestUserTime,
            long requestCpuTime,
            long requestGCDuration,
            long requestGCCount,
            Optional<String> requestCorrelationId,
            List<CheckpointTiming> checkpointReachTimes,
            List<CheckpointTiming> activityTimings) {
        this.url = url;
        this.requestKey = requestKey;
        this.reqTime = reqTime;
        this.requestUserTime = requestUserTime;
        this.requestCpuTime = requestCpuTime;
        this.requestGCDuration = requestGCDuration;
        this.requestGCCount = requestGCCount;
        this.checkpointReachTime = checkpointReachTimes;
        this.requestCorrelationId = requestCorrelationId;
        this.activityTimings = activityTimings;
    }

    public String getKey() {
        return requestKey.orElse(null);
    }

    public String getUrl() {
        return url;
    }

    public long getReqTime() {
        return reqTime;
    }

    public Long getRequestUserTime() {
        return requestUserTime;
    }

    public long getRequestCpuTime() {
        return requestCpuTime;
    }

    public long getRequestGCDuration() {
        return requestGCDuration;
    }

    public long getRequestGCCount() {
        return requestGCCount;
    }

    /**
     * This analytics field is for compatibility with confluence events.
     */
    public String getTimingEventKeys() {
        return checkpointReachTime.stream()
                .map(CheckpointTiming::getCheckpointName)
                .collect(Collectors.joining(","));
    }

    /**
     * This analytics field is for compatibility with confluence events.
     */
    public String getTimingEventMillis() {
        return checkpointReachTime.stream()
                .map(CheckpointTiming::getCheckpointTime)
                .map(Duration::toMillis)
                .map(String::valueOf)
                .collect(Collectors.joining(","));
    }

    public Map<String, Long> getTimingEvents() {
        return checkpointReachTime.stream()
                .collect(
                        Collectors.toMap(
                                CheckpointTiming::getCheckpointName,
                                checkpointTiming -> checkpointTiming.getCheckpointTime().toMillis(),
                                (existingValue, newValue) -> existingValue
                        ));
    }

    public Map<String, Long> getActivityTimings() {
        return activityTimings.stream()
                .collect(
                        Collectors.toMap(
                                CheckpointTiming::getCheckpointName,
                                checkpointTiming -> checkpointTiming.getCheckpointTime().toMillis(),
                                (existingValue, newValue) -> existingValue
                        ));
    }

    public String getRequestCorrelationId() {
        return requestCorrelationId.orElse(null);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("url", url)
                .add("key", requestKey)
                .add("requestCorrelationId", requestCorrelationId)
                .add("reqTime", reqTime)
                .add("requestUserTime", requestUserTime)
                .add("requestCpuTime", requestCpuTime)
                .add("requestGCDuration", requestGCDuration)
                .add("requestGCCount", requestGCCount)
                .add("checkpointReachTime", checkpointReachTime)
                .add("activityTimings", activityTimings)
                .toString();
    }
}
