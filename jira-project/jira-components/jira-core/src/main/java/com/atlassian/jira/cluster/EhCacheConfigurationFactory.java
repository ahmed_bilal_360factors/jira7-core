package com.atlassian.jira.cluster;

import net.sf.ehcache.config.Configuration;

import javax.annotation.Nonnull;
import java.net.URL;

/**
 * Builder for our EhCacheConfiguration.
 *
 * @since v6.4
 */
public interface EhCacheConfigurationFactory {
    Configuration newConfiguration(@Nonnull URL baseXmlConfiguration, @Nonnull ClusterNodeProperties clusterNodeProperties);
}
