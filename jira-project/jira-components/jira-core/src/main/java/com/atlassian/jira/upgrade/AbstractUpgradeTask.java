package com.atlassian.jira.upgrade;

import com.atlassian.core.ofbiz.CoreFactory;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.index.request.ReindexRequestService;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.upgrade.api.UpgradeContext;
import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericDelegator;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericHelper;
import org.ofbiz.core.entity.config.EntityConfigUtil;
import org.ofbiz.core.entity.jdbc.DatabaseUtil;
import org.ofbiz.core.entity.jdbc.dbtype.DatabaseType;
import org.ofbiz.core.entity.jdbc.dbtype.DatabaseTypeFactory;

import javax.annotation.Nullable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.atlassian.upgrade.api.UpgradeContext.UpgradeTrigger.PROVISIONING;

public abstract class AbstractUpgradeTask implements UpgradeTask, com.atlassian.upgrade.spi.UpgradeTask {
    private final List<String> errors = new ArrayList<>();

    public AbstractUpgradeTask() {
    }

    protected static OfBizDelegator getOfBizDelegator() {
        return ComponentAccessor.getOfBizDelegator();
    }

    protected static EntityEngine getEntityEngine() {
        return ComponentAccessor.getComponent(EntityEngine.class);
    }

    static String ensureTablePrefixed(String tableName, String schemaName) {
        if (StringUtils.isNotBlank(schemaName)) {
            String prefix = schemaName + '.';
            if (!tableName.startsWith(prefix)) {
                // Prepend the schema name
                return prefix + tableName;
            }
        }
        return tableName;
    }

    @Override
    public void runUpgrade(UpgradeContext upgradeContext) {
        try {
            boolean setupMode = (upgradeContext.getTrigger() == PROVISIONING);
            doUpgrade(setupMode);
        } catch (Exception e) {
            throw new RuntimeException("Error running original upgrade task", e);
        }
    }

    @Override
    @Nullable
    public Integer dependsUpon() {
        return null;
    }

    /*
     * Please use the OfBizDelegator now.
     *
     * @since v5.0
     */
    @Deprecated
    protected GenericDelegator getDelegator() {
        return CoreFactory.getGenericDelegator();
    }

    protected ApplicationProperties getApplicationProperties() {
        return ComponentAccessor.getApplicationProperties();
    }

    protected ReindexRequestService getReindexRequestService() {
        return ComponentAccessor.getComponent(ReindexRequestService.class);
    }

    protected void addError(final String error) {
        errors.add(error);
    }

    /**
     * Useful for adding a bunch of errors (like from a command) with a prefix
     */
    public void addErrors(final String prefix, final Collection<String> errors) {
        errors.forEach(error -> addError(prefix + error));
    }

    public void addErrors(final Collection<String> errors) {
        addErrors("", errors);
    }

    @Override
    public Collection<String> getErrors() {
        return errors;
    }

    protected I18nHelper getI18nBean() {
        return ComponentAccessor.getJiraAuthenticationContext().getI18nHelper();
    }

    protected Connection getDatabaseConnection() {
        try {
            GenericHelper helper = getDelegator().getEntityHelper("User");
            DatabaseUtil utils = new DatabaseUtil(helper.getHelperName());
            return utils.getConnection();
        } catch (SQLException | GenericEntityException ex) {
            throw new DataAccessException("Unable to obtain a DB connection", ex);
        }
    }

    protected DatabaseType getDatabaseType() {
        final DatabaseConfig config = getDatabaseConfig();
        if (config.isPostgres()) {
            return DatabaseTypeFactory.POSTGRES;
        }
        if (config.isOracle()) {
            return DatabaseTypeFactory.ORACLE_10G;
        }
        if (config.isSqlServer()) {
            return DatabaseTypeFactory.MSSQL;
        }
        if (config.isMySql()) {
            return DatabaseTypeFactory.MYSQL;
        }
        if (config.isH2()) {
            return DatabaseTypeFactory.H2;
        }
        if (config.isHSql()) {
            return DatabaseTypeFactory.HSQL;
        }
        throw new DataAccessException("Unrecognized database configuration: " + config.getDatabaseType());
    }

    protected String convertToSchemaTableName(String tableName) {
        return ensureTablePrefixed(tableName, getSchemaName());
    }

    protected String getSchemaName() {
        try {
            final GenericHelper helper = getDelegator().getEntityHelper("User");
            return EntityConfigUtil.getInstance().getDatasourceInfo(helper.getHelperName()).getSchemaName();
        } catch (GenericEntityException ex) {
            throw new RuntimeException(ex);
        }
    }

    protected boolean isORACLE() throws SQLException {
        return getDatabaseConfig().isOracle();
    }

    protected boolean isMSSQL() throws SQLException {
        return getDatabaseConfig().isSqlServer();
    }

    protected boolean isMYSQL() throws SQLException {
        return getDatabaseConfig().isMySql();
    }

    protected boolean isPostgreSQL() {
        return getDatabaseConfig().isPostgres();
    }

    private static DatabaseConfig getDatabaseConfig() {
        return ComponentAccessor.getComponent(DatabaseConfigurationManager.class).getDatabaseConfiguration();
    }
}
