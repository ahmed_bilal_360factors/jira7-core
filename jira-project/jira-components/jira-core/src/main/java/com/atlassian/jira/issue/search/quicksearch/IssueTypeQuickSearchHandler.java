package com.atlassian.jira.issue.search.quicksearch;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.ofbiz.FieldMap;

import java.util.Map;

public class IssueTypeQuickSearchHandler extends SingleWordQuickSearchHandler {
    public final ConstantsManager constantsManager;

    public IssueTypeQuickSearchHandler(ConstantsManager constantsManager) {
        this.constantsManager = constantsManager;
    }

    public IssueConstant getTypeByName(String name) {
        return getIssueConstantByName(constantsManager.getRegularIssueTypeObjects(), name);
    }

    protected Map handleWord(String word, QuickSearchResult searchResult) {
        IssueConstant typeByName = getTypeByName(word);
        if (typeByName == null && (word.endsWith("S") || word.endsWith("s"))) {
            typeByName = getTypeByName(word.substring(0, word.length() - 1));
        }
        return typeByName != null ? FieldMap.build(DocumentConstants.ISSUE_TYPE, typeByName.getId()) : null;
    }
}
