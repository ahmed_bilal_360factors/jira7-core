package com.atlassian.jira.issue.search.searchers.renderer;

import com.atlassian.jira.issue.fields.option.TextOption;

public class OptionWithValidity extends TextOption {
    private final boolean valid;

    public OptionWithValidity(String id, String name) {
        this(id, name, true);
    }

    OptionWithValidity(String id, String name, boolean valid) {
        super(id, name, valid ? null : "invalid_sel");
        this.valid = valid;
    }

    public boolean isValid() {
        return valid;
    }
}
