package com.atlassian.jira.upgrade.tasks.role;


import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.opensymphony.module.propertyset.PropertySet;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collections;
import java.util.List;


public class ServiceDeskPropertySetDaoImpl implements ServiceDeskPropertySetDao {
    private static final String OS_PROPERTY_ENTRY = "OSPropertyEntry";
    private static final String OS_PROPERTY_STRING = "OSPropertyString";

    private static final int TYPE_STRING = PropertySet.STRING;

    /**
     * 'vp' stands for View Port which was the old name for Service Desk. It was left like that for backwards
     * compatibility.
     */
    private static final String SD_ENTITY_NAME = "vp.properties";
    private static final int SD_ENTITY_ID = 1;

    private static class OsPropertyEntryColumns {
        private static final String ENTITY_NAME = "entityName";
        private static final String ENTITY_ID = "entityId";
        private static final String ID = "id";
        private static final String PROPERTY_KEY = "propertyKey";

        private static final String TYPE = "type";

    }

    private static class OsPropertyStringColumns {
        private static final String ID = "id";
        private static final String VALUE = "value";
    }

    private final OfBizDelegator ofBizDelegator;

    public ServiceDeskPropertySetDaoImpl(final OfBizDelegator ofBizDelegator) {
        this.ofBizDelegator = ofBizDelegator;
    }

    @Override
    public void writeStringProperty(String key, String value) {
        final FieldMap fieldMap = FieldMap.build(
                OsPropertyEntryColumns.ENTITY_NAME, SD_ENTITY_NAME,
                OsPropertyEntryColumns.ENTITY_ID, SD_ENTITY_ID,
                OsPropertyEntryColumns.TYPE, TYPE_STRING,
                OsPropertyEntryColumns.PROPERTY_KEY, key);

        final List<GenericValue> properties = ofBizDelegator.findByAnd(OS_PROPERTY_ENTRY, fieldMap);
        GenericValue property = null;
        if (properties != null && !properties.isEmpty()) {
            property = properties.stream().max((p1, p2) -> p1.getLong(OsPropertyEntryColumns.ID)
                    .compareTo(p2.getLong(OsPropertyEntryColumns.ID)))
                    .get();
        } else {
            property = ofBizDelegator.createValue(OS_PROPERTY_ENTRY, fieldMap);
        }

        final GenericValue propertyValue = createStringValue(property.getLong(OsPropertyEntryColumns.ID), value);
        //StoreAll either creates or updates, unlike store which updates only,
        ofBizDelegator.storeAll(Collections.singletonList(propertyValue));
    }

    private GenericValue createStringValue(long propertyEntryId, String value) {
        return ofBizDelegator.makeValue(OS_PROPERTY_STRING,
                FieldMap.build(OsPropertyStringColumns.ID, propertyEntryId, OsPropertyStringColumns.VALUE, value));
    }
}
