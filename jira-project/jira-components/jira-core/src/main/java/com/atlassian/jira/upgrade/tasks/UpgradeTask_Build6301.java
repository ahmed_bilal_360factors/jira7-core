package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.upgrade.LegacyImmediateUpgradeTask;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.model.ModelEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Fix field type for AuditLog searchField
 *
 * @since v6.3
 */
public class UpgradeTask_Build6301 extends LegacyImmediateUpgradeTask {
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask_Build6301.class);

    private static final String ENTITY_NAME = "AuditLog";
    private static final String TABLE_NAME = "audit_log";

    public UpgradeTask_Build6301() {
        super();
    }

    @Override
    public int getBuildNumber() {
        return 6301;
    }

    @Override
    public String getShortDescription() {
        return "Fix field type for AuditLog searchField";
    }

    @Override
    public void doUpgrade(boolean setupMode) {
        try {
            // the alter table syntax is specific to postgresql
            // JDEV-27398 affected only customers of OnDemand (root cause was fixed in 6.2-OD9) so
            //   we do not have to deal with other databases
            if (isPostgreSQL()) {
                final ModelEntity auditLogTable = getOfBizDelegator().getModelReader().getModelEntity(ENTITY_NAME);
                final String searchFieldColumn = auditLogTable.getField("searchField").getColName();

                try (Connection connection = getDatabaseConnection()) {
                    final Statement statement = connection.createStatement();
                    statement.execute("ALTER TABLE " + convertToSchemaTableName(TABLE_NAME) + " ALTER COLUMN " +
                            searchFieldColumn + " SET DATA TYPE TEXT");
                }
            }
        } catch (SQLException e) {
            log.warn("Problem while changing field type for audit_log search_field. "
                    + "If search_field is already of type text this can be safely ignored.", e);
        } catch (GenericEntityException e) {
            throw new RuntimeException(e);
        }
    }

    @Nullable
    @Override
    public Integer dependsUpon() {
        return 6258;
    }
}
