package com.atlassian.jira.admin;

import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.ExecutingHttpRequest;

public class ProjectAdminSidebarFeatureImpl implements ProjectAdminSidebarFeature {

    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final ProjectService projectService;

    public ProjectAdminSidebarFeatureImpl(
            final JiraAuthenticationContext jiraAuthenticationContext,
            final ProjectService projectService
    ) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.projectService = projectService;
    }

    @Override
    public boolean shouldDisplay() {
        final ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
        final String projectKey = (String) ExecutingHttpRequest.get().getAttribute(ProjectService.PROJECT_KEY);

        if (user == null || StringUtils.isBlank(projectKey)) {
            return false;
        }

        return projectService.getProjectByKey(user, projectKey).isValid();
    }
}
