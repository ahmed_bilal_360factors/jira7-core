package com.atlassian.jira.util.resourcebundle;

import com.atlassian.jira.util.ComponentLocator;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.Resources.TypeFilter;
import com.atlassian.plugin.metadata.PluginMetadataManager;

import java.util.Locale;
import java.util.Map;

/**
 * Loads language translations.
 *
 * @since v7.0
 */
public class I18NResourceBundleLoader {
    private final TypeFilter filter = new TypeFilter("i18n");
    private final ComponentLocator locator;

    public I18NResourceBundleLoader(final ComponentLocator locator) {
        this.locator = locator;
    }

    public Map<String, String> load(final Locale locale) {
        final PluginAccessor pluginAccessor = locator.getComponent(PluginAccessor.class);
        final PluginMetadataManager pluginMetadataManager = locator.getComponent(PluginMetadataManager.class);
        return new PluginResourceLoaderInvocation(pluginAccessor, pluginMetadataManager, filter, true, locale).load();
    }
}
