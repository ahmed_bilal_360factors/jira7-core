package com.atlassian.jira.issue.security;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.task.TaskContext;

/**
 * Context for assigning issue security scheme tasks. There can be only one such operation
 * per project at any given time.
 *
 * @since v7.1.1
 */
public class AssignIssueSecuritySchemeTaskContext implements TaskContext {

    private final Long projectId;

    public AssignIssueSecuritySchemeTaskContext(final Project project) {
        this.projectId = project.getId();
    }

    @Override
    public String buildProgressURL(final Long taskId) {
        return "/secure/project/AssignIssueSecuritySchemeProgress.jspa?projectId=" + projectId + "&taskId=" + taskId;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssignIssueSecuritySchemeTaskContext)) {
            return false;
        }

        final AssignIssueSecuritySchemeTaskContext that = (AssignIssueSecuritySchemeTaskContext) o;

        return projectId.equals(that.projectId);
    }

    @Override
    public int hashCode() {
        return projectId.hashCode();
    }
}
