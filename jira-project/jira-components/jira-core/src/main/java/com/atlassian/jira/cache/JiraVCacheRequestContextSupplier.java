package com.atlassian.jira.cache;

import com.atlassian.vcache.internal.NameValidator;
import com.atlassian.vcache.internal.RequestContext;
import com.atlassian.vcache.internal.core.DefaultRequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Optional;
import java.util.function.Supplier;

import static com.atlassian.vcache.internal.NameValidator.requireValidPartitionIdentifier;

/**
 * Lenient request context supplier that may create request context for a thread if it hasn't been created. Normally in
 * Cloud Vertigo context is initialised at the beginning of the request with a tenant that comes in request,
 * therefore it is not allowed to call components that are tenant aware without request context. However, Server
 * instance is always tenanted and sometimes components that use vcache are called from threads that do
 * not have context initialised (e.g. in constructor during container initialisation); Since this
 * behaviour is allowed in Jira Server 7.x we will just log it as a warning and create NoopRequestContext that will
 * always compute value or return empty value.
 *
 * @since v7.1
 */
@ParametersAreNonnullByDefault
public class JiraVCacheRequestContextSupplier implements Supplier<RequestContext> {
    private static final Logger log = LoggerFactory.getLogger(JiraVCacheRequestContextSupplier.class);
    private static final ThreadLocal<RequestContext> staticContext = new ThreadLocal<>();

    private final NoopRequestContext noopContext = new NoopRequestContext();
    private final ThreadLocal<RequestContext> threadRequestContexts = new ThreadLocal<>();
    private final boolean warnNoContext;

    /**
     * Constructs JiraVCacheRequestContextSupplier.
     *
     * @param warnNoContext if set to true, every time {@link #get()} will be called on uninitialised thread request
     *                      context it will be logged as warning.
     */
    public JiraVCacheRequestContextSupplier(boolean warnNoContext) {
        this.warnNoContext = warnNoContext;
    }

    @Override
    @Nonnull
    public RequestContext get() {
        final RequestContext current = threadRequestContexts.get();
        if (current == null) {
            return handleNoRequestContext();
        }

        return current;
    }

    public boolean isInitilised() {
        return threadRequestContexts.get() != null;
    }

    private RequestContext handleNoRequestContext() {
        if (staticContext.get() != null) {
            return staticContext.get();
        }

        if (warnNoContext) {
            log.warn("Asked for request context when not initialised.",
                    new RuntimeException("VCache context no initialised - using NOOP context."));
        }
        return noopContext;
    }

    /**
     * Initialises the thread's {@link RequestContext}.
     *
     * @param partitionId the identifier for the partition. Will be validated using
     *                    {@link NameValidator#requireValidPartitionIdentifier(String)}.
     */
    public void initThread(String partitionId) {
        final RequestContext current = threadRequestContexts.get();
        if (current != null) {
            log.error(
                    "Asked to initialise thread {} that is already initialised!",
                    Thread.currentThread().getName());
            throw new IllegalStateException(
                    "Thread '" + Thread.currentThread().getName() + "' has already been initialised.");
        }

        log.trace("Initialise request context");
        threadRequestContexts.set(new DefaultRequestContext(requireValidPartitionIdentifier(partitionId)));
    }

    /**
     * Clears the thread's {@link RequestContext}.
     */
    public void clearThread() {
        final RequestContext current = threadRequestContexts.get();
        if (log.isDebugEnabled() && current == null) {
            log.debug("Asked to clear a thread that is already clear!", new RuntimeException());
        }
        threadRequestContexts.remove();
    }

     static class NoopRequestContext implements RequestContext {

        /**
         * NOTE: this method MUST return id of global tenant associated with current instance (so it must be consistent
         * with regular implemations of {@link RequestContext}'s).
         */
        @Nonnull
        @Override
        public String partitionIdentifier() {
            return JiraVCacheInitialisationUtils.getFakeTenant().getId();
        }

        @Nonnull
        @Override
        public <T> T computeIfAbsent(Object key, Supplier<T> supplier) {
            return supplier.get();
        }

        @Nonnull
        @Override
        public <T> Optional<T> get(Object key) {
            return Optional.empty();
        }
    }

    /**
     * Initialises per thread static context that might be used by {@link JiraVCacheRequestContextSupplier} if
     * no tenant context is available.
     *
     * @param partitionId unique name of the partition.
     */
    public static void initStaticContext(String partitionId) {
        staticContext.set(new DefaultRequestContext(requireValidPartitionIdentifier(partitionId)));
    }

    /**
     * Clears the static context for current thread.
     */
    public static void clearStaticContext() {
        staticContext.remove();
    }
}
