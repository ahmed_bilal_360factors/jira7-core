package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.fugue.Option;

/**
 * A simple license DAO for renaissance migration. It is only safe to use in migration from JIRA 6.x to JIRA 7.0.
 * <p>
 * NOTE: This class it written to access the database as it was in JIRA 6.x and should not be updated to reflect
 * changes that happen after this point.
 *
 * @since v7.0
 */
abstract class LicenseDao {
    /**
     * Return the license store in the JIRA 6.x store.
     *
     * @return the license or {@link Option#none()} if it does not exist.
     */
    abstract Option<License> get6xLicense();

    /**
     * Return all the licenses stored in the JIRA 7.0 store.
     *
     * @return all the licenses stored in the JIRA 7.0 store.
     */
    abstract Licenses getLicenses();

    /**
     * Set the licenses in the JIRA 7.0 store.
     *
     * @param licenses the licenses to store.
     */
    abstract void setLicenses(Licenses licenses);

    /**
     * Remove the JIRA 6.x license from JIRA.
     */
    abstract void remove6xLicense();
}
