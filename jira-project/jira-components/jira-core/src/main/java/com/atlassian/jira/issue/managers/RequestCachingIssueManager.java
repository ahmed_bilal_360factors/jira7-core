package com.atlassian.jira.issue.managers;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.Internal;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.UpdateIssueRequest;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.lang.Pair;
import com.atlassian.jira.web.ExecutingHttpRequest;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.jira.security.JiraAuthenticationContextImpl.getRequestCache;

@SuppressWarnings("deprecation")
public class RequestCachingIssueManager implements IssueManager {
    private static final String ISSUE_NAV_ROOT = "kickass-issue-nav-root";
    private static final String CACHE_KEY = RequestCachingIssueManager.class.getName() + ".issues";
    private static final String FEATURE_KEY = "jira.request.scope.issue.caching.disabled";

    private final IssueManager delegate;
    private final FeatureManager featureManager;

    public RequestCachingIssueManager(final IssueManager delegate, final FeatureManager featureManager) {
        this.delegate = delegate;
        this.featureManager = featureManager;
    }

    @Override
    public MutableIssue getIssueObject(final Long id) throws DataAccessException {
        if (shouldCacheIssues()) {
            final IssueCache cache = getRequestCache(CACHE_KEY, IssueCache::new);
            Optional<MutableIssue> issue = cache.getById(id);
            if (issue == null) {
                issue = Optional.ofNullable(delegate.getIssueObject(id));
                cache.put(id, issue);
            }
            return issue.orElse(null);
        } else {
            return delegate.getIssueObject(id);
        }
    }

    private boolean shouldCacheIssues() {
        return isViewIssueRequest() && isRequestScopeIssueCachingEnabled();
    }

    private boolean isRequestScopeIssueCachingEnabled() {
        return !featureManager.isEnabled(FEATURE_KEY);
    }

    private boolean isViewIssueRequest() {
        final HttpServletRequest httpServletRequest = ExecutingHttpRequest.get();
        return httpServletRequest != null && httpServletRequest.getAttribute(ISSUE_NAV_ROOT) != null;
    }

    @Override
    public MutableIssue getIssueObject(final String key) throws DataAccessException {
        if (shouldCacheIssues() && key != null) {
            final IssueCache cache = getRequestCache(CACHE_KEY, IssueCache::new);
            Optional<MutableIssue> issue = cache.getByKey(key);
            if (issue == null) {
                issue = Optional.ofNullable(delegate.getIssueObject(key));
                cache.put(key, issue);
            }
            return issue.orElse(null);
        } else {
            return delegate.getIssueObject(key);
        }
    }

    @Override
    @ExperimentalApi
    public MutableIssue getIssueByKeyIgnoreCase(final String key) throws DataAccessException {
        return delegate.getIssueByKeyIgnoreCase(key);
    }

    @Override
    @ExperimentalApi
    public MutableIssue getIssueByCurrentKey(final String key) throws DataAccessException {
        final MutableIssue issue = getIssueObject(key);
        if (issue != null && issue.getString("key").equals(key)) {
            return issue;
        } else {
            return null;
        }
    }

    @Override
    @Deprecated
    public List<GenericValue> getIssues(final Collection<Long> ids) {
        return delegate.getIssues(ids);
    }

    @Override
    public List<Issue> getIssueObjects(final Collection<Long> ids) {
        return delegate.getIssueObjects(ids);
    }

    @Override
    public List<Issue> getVotedIssues(final ApplicationUser user) {
        return delegate.getVotedIssues(user);
    }

    @Override
    public List<Issue> getVotedIssuesOverrideSecurity(final ApplicationUser user) {
        return delegate.getVotedIssuesOverrideSecurity(user);
    }

    @Override
    public List<ApplicationUser> getWatchers(final Issue issue) {
        return delegate.getWatchers(issue);
    }

    @Override
    public List<ApplicationUser> getWatchersFor(final Issue issue) {
        return delegate.getWatchersFor(issue);
    }

    @Override
    public List<Issue> getWatchedIssues(final ApplicationUser user) {
        return delegate.getWatchedIssues(user);
    }

    @Override
    public List<Issue> getWatchedIssuesOverrideSecurity(final ApplicationUser user) {
        return delegate.getWatchedIssuesOverrideSecurity(user);
    }

    @Override
    public List<GenericValue> getEntitiesByIssue(final String relationName, final GenericValue issue)
            throws GenericEntityException {
        return delegate.getEntitiesByIssue(relationName, issue);
    }

    @Override
    public List<GenericValue> getEntitiesByIssueObject(final String relationName, final Issue issue)
            throws GenericEntityException {
        return delegate.getEntitiesByIssueObject(relationName, issue);
    }

    @Override
    public List<GenericValue> getIssuesByEntity(final String relationName, final GenericValue entity)
            throws GenericEntityException {
        return delegate.getIssuesByEntity(relationName, entity);
    }

    @Override
    public List<Issue> getIssueObjectsByEntity(final String relationName, final GenericValue entity)
            throws GenericEntityException {
        return delegate.getIssueObjectsByEntity(relationName, entity);
    }

    @Override
    public Set<String> getAllIssueKeys(final Long issueId) {
        return delegate.getAllIssueKeys(issueId);
    }

    @Override
    @Deprecated
    public GenericValue createIssue(final String remoteUserName, final Map<String, Object> fields)
            throws CreateException {
        return delegate.createIssue(remoteUserName, fields);
    }

    @Override
    public Issue createIssueObject(final String remoteUserName, final Map<String, Object> fields)
            throws CreateException {
        return delegate.createIssueObject(remoteUserName, fields);
    }

    @Override
    @Deprecated
    public GenericValue createIssue(final ApplicationUser remoteUser, final Map<String, Object> fields)
            throws CreateException {
        return delegate.createIssue(remoteUser, fields);
    }

    @Override
    public Issue createIssueObject(final ApplicationUser remoteUser, final Map<String, Object> fields)
            throws CreateException {
        return delegate.createIssueObject(remoteUser, fields);
    }

    @Override
    @Deprecated
    public GenericValue createIssue(final ApplicationUser remoteUser, final Issue issue) throws CreateException {
        return delegate.createIssue(remoteUser, issue);
    }

    @Override
    public Issue createIssueObject(final ApplicationUser remoteUser, final Issue issue) throws CreateException {
        return delegate.createIssueObject(remoteUser, issue);
    }

    @Override
    public Issue updateIssue(final ApplicationUser user, final MutableIssue issue, final EventDispatchOption eventDispatchOption, final boolean sendMail) {
        return delegate.updateIssue(user, issue, eventDispatchOption, sendMail);
    }

    @Override
    public Issue updateIssue(final ApplicationUser user, final MutableIssue issue, final UpdateIssueRequest updateIssueRequest) {
        return delegate.updateIssue(user, issue, updateIssueRequest);
    }

    @Override
    public void deleteIssue(final ApplicationUser user, final Issue issue, final EventDispatchOption eventDispatchOption, final boolean sendMail)
            throws RemoveException {
        delegate.deleteIssue(user, issue, eventDispatchOption, sendMail);
    }

    @Override
    @Deprecated
    public void deleteIssue(final ApplicationUser user, final MutableIssue issue, final EventDispatchOption eventDispatchOption, final boolean sendMail)
            throws RemoveException {
        delegate.deleteIssue(user, issue, eventDispatchOption, sendMail);
    }

    @Override
    public void deleteIssueNoEvent(final Issue issue) throws RemoveException {
        delegate.deleteIssueNoEvent(issue);
    }

    @Override
    @Deprecated
    public void deleteIssueNoEvent(final MutableIssue issue) throws RemoveException {
        delegate.deleteIssueNoEvent(issue);
    }

    @Override
    @Deprecated
    public List<GenericValue> getProjectIssues(final GenericValue project) throws GenericEntityException {
        return delegate.getProjectIssues(project);
    }

    @Override
    public boolean isEditable(final Issue issue) {
        return delegate.isEditable(issue);
    }

    @Override
    public boolean isEditable(final Issue issue, final ApplicationUser user) {
        return delegate.isEditable(issue, user);
    }

    @Override
    public Collection<Long> getIssueIdsForProject(final Long projectId) throws GenericEntityException {
        return delegate.getIssueIdsForProject(projectId);
    }

    @Override
    public long getIssueCountForProject(final Long projectId) {
        return delegate.getIssueCountForProject(projectId);
    }

    @Override
    public boolean hasUnassignedIssues() {
        return delegate.hasUnassignedIssues();
    }

    @Override
    public long getUnassignedIssueCount() {
        return delegate.getUnassignedIssueCount();
    }

    @Override
    public long getIssueCount() {
        return delegate.getIssueCount();
    }

    @Override
    public boolean atLeastOneIssueExists() {
        return delegate.atLeastOneIssueExists();
    }

    @Override
    @Internal
    public Issue findMovedIssue(final String oldIssueKey) {
        return delegate.findMovedIssue(oldIssueKey);
    }

    @Override
    @Internal
    public void recordMovedIssueKey(final Issue oldIssue) {
        delegate.recordMovedIssueKey(oldIssue);
    }

    @Override
    @Internal
    @Nonnull
    public Set<Pair<Long, String>> getProjectIssueTypePairsByKeys(@Nonnull final Set<String> issueKeys) {
        return delegate.getProjectIssueTypePairsByKeys(issueKeys);
    }

    @Override
    @Internal
    @Nonnull
    public Set<Pair<Long, String>> getProjectIssueTypePairsByIds(@Nonnull final Set<Long> issueIds) {
        return delegate.getProjectIssueTypePairsByIds(issueIds);
    }

    @Override
    @Internal
    @Nonnull
    public Set<String> getKeysOfMissingIssues(@Nonnull final Set<String> issueKeys) {
        return delegate.getKeysOfMissingIssues(issueKeys);
    }

    @Override
    @Internal
    @Nonnull
    public Set<Long> getIdsOfMissingIssues(@Nonnull final Set<Long> issueIds) {
        return delegate.getIdsOfMissingIssues(issueIds);
    }

    @Override
    @Deprecated
    public GenericValue getIssue(final Long id) throws DataAccessException {
        return delegate.getIssue(id);
    }

    @Override
    @Deprecated
    public GenericValue getIssue(final String key) throws GenericEntityException {
        return delegate.getIssue(key);
    }

    @Override
    @Deprecated
    public GenericValue getIssueByWorkflow(final Long wfid) throws GenericEntityException {
        return delegate.getIssueByWorkflow(wfid);
    }

    @Override
    public MutableIssue getIssueObjectByWorkflow(final Long workflowId) throws GenericEntityException {
        return delegate.getIssueObjectByWorkflow(workflowId);
    }

    @Override
    @ExperimentalApi
    public boolean isExistingIssueKey(final String issueKey) throws GenericEntityException {
        return delegate.isExistingIssueKey(issueKey);
    }

    private static class IssueCache {
        private final HashMap<Long, Optional<MutableIssue>> byId = new HashMap<>();
        private final HashMap<String, Optional<MutableIssue>> byKey = new HashMap<>();

        public Optional<MutableIssue> getById(final Long id) {
            return byId.get(id);
        }

        public void put(@Nonnull final Long id, @Nonnull final Optional<MutableIssue> issue) {
            byId.put(id, issue);
            issue.ifPresent(i -> byKey.put(i.getKey(), Optional.of(i)));
        }

        public void put(@Nonnull final String key, @Nonnull final Optional<MutableIssue> issue) {
            byKey.put(key, issue);
            issue.ifPresent(i -> byId.put(i.getId(), Optional.of(i)));
        }

        public Optional<MutableIssue> getByKey(final String key) {
            return byKey.get(key);
        }
    }
}
