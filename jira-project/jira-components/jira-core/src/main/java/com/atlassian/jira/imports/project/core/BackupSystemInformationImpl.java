package com.atlassian.jira.imports.project.core;

import com.atlassian.jira.plugin.PluginVersion;
import com.atlassian.jira.util.dbc.Null;
import com.google.common.collect.ImmutableSet;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @since v3.13
 */
public class BackupSystemInformationImpl implements BackupSystemInformation {
    private final List<PluginVersion> pluginVersions;
    private final String buildNumber;
    private final String edition;
    private final boolean unassignedIssuesAllowed;
    private final Map<String, String> issueIdToKeyMap;
    private final Set<String> entities;
    private final int entityCount;

    public BackupSystemInformationImpl(final String buildNumber, final String edition, final List<PluginVersion> pluginVersions,
                                       final boolean unassignedIssuesAllowed, final Map<String, String> issueIdToKeyMap, final Set<String> entities, final int entityCount) {
        Null.not("pluginVersions", pluginVersions);
        Null.not("entityNames", entities);

        this.buildNumber = buildNumber;
        this.edition = edition;
        this.pluginVersions = Collections.unmodifiableList(pluginVersions);
        this.unassignedIssuesAllowed = unassignedIssuesAllowed;
        this.issueIdToKeyMap = Collections.unmodifiableMap(issueIdToKeyMap);
        this.entities = entities;
        this.entityCount = entityCount;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public String getEdition() {
        return edition;
    }

    public List<PluginVersion> getPluginVersions() {
        return pluginVersions;
    }

    public boolean unassignedIssuesAllowed() {
        return unassignedIssuesAllowed;
    }

    public String getIssueKeyForId(final String issueId) {
        return issueIdToKeyMap.get(issueId);
    }

    public int getEntityCount() {
        return entityCount;
    }

    public Set<String> getEntityTypes() {
        return ImmutableSet.copyOf(entities);
    }

    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("\n").append("PluginVersions: ").append("\n");
        for (final Object pluginVersion1 : pluginVersions) {
            final PluginVersion pluginVersion = (PluginVersion) pluginVersion1;
            sb.append("\n").append("     ").append(pluginVersion);
        }
        sb.append("\n");
        sb.append("JIRA Build #: ").append(getBuildNumber()).append("\n");
        sb.append("JIRA Edition: ").append(getEdition()).append("\n");
        sb.append("Allow unassigned issues: ").append(unassignedIssuesAllowed()).append("\n");
        return sb.toString();
    }
}
