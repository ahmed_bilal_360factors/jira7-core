package com.atlassian.jira.permission.management.beans;


import com.google.common.base.Objects;
import org.codehaus.jackson.annotate.JsonAutoDetect;

@JsonAutoDetect
public class ProjectPermissionHelpBean {
    private final String infoText;
    private final String helpText;
    private final String helpUrl;

    public ProjectPermissionHelpBean(final String infoText, final String helpText, final String helpUrl) {
        this.infoText = infoText;
        this.helpText = helpText;
        this.helpUrl = helpUrl;
    }

    public String getInfoText() {
        return infoText;
    }

    public String getHelpText() {
        return helpText;
    }

    public String getHelpUrl() {
        return helpUrl;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("infoText", getInfoText())
                .add("helpText", getHelpText())
                .add("helpUrl", getHelpUrl())
                .toString();
    }

}
