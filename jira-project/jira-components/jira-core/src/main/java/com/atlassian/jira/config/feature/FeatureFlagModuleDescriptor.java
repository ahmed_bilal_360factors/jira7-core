package com.atlassian.jira.config.feature;

import com.atlassian.fugue.Option;
import com.atlassian.jira.config.FeatureFlag;
import com.atlassian.jira.config.FeatureFlagProvider;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.util.concurrent.ResettableLazyReference;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;

import java.util.Collections;
import java.util.Set;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.ozymandias.SafePluginPointAccess.call;

public class FeatureFlagModuleDescriptor extends AbstractModuleDescriptor<FeatureFlagProvider> {

    private static final Set<FeatureFlag> EMPTY_FLAGS = Collections.emptySet();
    private Set<FeatureFlag> staticDescriptorFlags = EMPTY_FLAGS;

    public FeatureFlagModuleDescriptor(final ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public void init(Plugin plugin, Element element) throws PluginParseException {
        super.init(plugin, element);
        staticDescriptorFlags = FeatureFlagParser.parseFlags(element);
    }

    private final ResettableLazyReference<Option<FeatureFlagProvider>> moduleRef = new ResettableLazyReference<Option<FeatureFlagProvider>>() {
        @Override
        protected Option<FeatureFlagProvider> create() throws Exception {
            return call(() -> {
                if (StringUtils.isEmpty(moduleClassName)) {
                    return null;
                }
                return moduleFactory.createModule(moduleClassName, FeatureFlagModuleDescriptor.this);
            });
        }
    };

    @Override
    public FeatureFlagProvider getModule() {
        return moduleRef.get().getOrNull();
    }

    /**
     * @return the set of feature flags that come from this module.  This is done with ozymandias (tm) safety
     */
    public Set<FeatureFlag> getFeatureFlags() {
        //
        // This is a dynamic call - we allow the provider to change the list.  If they want to cache them statically then great.  All done with ozymandias plugin safety (tm)
        Option<Set<FeatureFlag>> moduleFlags = call(() -> {
            Option<FeatureFlagProvider> provider = option(getModule());
            if (provider.isDefined()) {
                return option(provider.get().getFeatureFlags()).getOrElse(EMPTY_FLAGS);
            }
            return EMPTY_FLAGS;
        });

        // we combine the dynamic and static flags
        Set<FeatureFlag> flags = Sets.newHashSet();
        flags.addAll(staticDescriptorFlags);
        flags.addAll(moduleFlags.getOrElse(EMPTY_FLAGS));

        return flags;
    }
}
