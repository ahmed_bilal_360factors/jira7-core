package com.atlassian.jira.web.pagebuilder;

/**
 * Interface for accessing page builders
 * <p>This is used in the following plugins:</p>
 * <ul>
 *     <li>jira-projects-plugin</li>
 *     <li>jira-projects-issue-nav-plugin</li>
 *     <li>jira-global-issue-nav-plugin</li>
 *     <li>jira-issue-nav-components-plugin</li>
 * </ul>
 * When changing this, please make sure these plugins still work.
 *
 * <pre>
 * *******************************
 * This is in the DMZ; search for usages before removing it.
 * *******************************
 * </pre>
 *
 * @since v6.1
 */
public interface JiraPageBuilderService extends com.atlassian.webresource.api.assembler.PageBuilderService {
    /**
     * Dark feature key for NOT sending the head early for the Dashboard
     */
    public static final String SEND_HEAD_EARLY_FOR_DASHBOARD_DISABLE_FEATURE_KEY = "com.atlassian.plugins.dashboard.SEND_HEAD_EARLY.disabled";

    /**
     * Gets the page builder for the current request
     *
     * @return request-local page builder
     * @throws IllegalStateException if no page builder has been set for the current request
     */
    public PageBuilder get();
}
