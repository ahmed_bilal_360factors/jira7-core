package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationAccess;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.PlatformApplication;
import com.atlassian.application.host.AbstractApplicationManager;
import com.atlassian.application.host.ApplicationAccessFactory;
import com.atlassian.application.host.ApplicationConfigurationManager;
import com.atlassian.application.host.license.LicenseLocator;
import com.atlassian.application.host.plugin.PluginApplicationMetaDataManager;
import com.atlassian.fugue.Option;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.sal.api.license.SingleProductLicenseDetailsView;
import org.joda.time.DateTime;

import java.net.URI;

import static com.atlassian.jira.application.DefaultApplicationRoleDefinitions.CoreRoleDefinition;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * JIRA's implementation of {@link com.atlassian.application.api.ApplicationManager}.
 *
 * @since v7.0
 */
public class JiraApplicationManager extends AbstractApplicationManager {
    private final PlatformApplication platformApplication;
    static final String CREATE_CORE_USER_URL = "/secure/admin/user/AddUser!default.jspa?selectedApplications=%s";

    public JiraApplicationManager(final PluginApplicationMetaDataManager pluginApplications,
                                  final BuildUtilsInfo info, final JiraAuthenticationContext ctx,
                                  final LicenseLocator locator, final ApplicationAccessFactory accessFactory,
                                  final ApplicationConfigurationManager appConfigManager) {
        super(pluginApplications, new JiraI18nResolver(ctx), locator, accessFactory, appConfigManager);
        platformApplication
                = new JiraCoreApplication(ctx, locator, accessFactory, info, appConfigManager);
    }

    @Override
    public PlatformApplication getPlatform() {
        return platformApplication;
    }

    private static class JiraCoreApplication implements PlatformApplication {
        private final JiraAuthenticationContext ctx;
        private final LicenseLocator locator;
        private final ApplicationAccessFactory accessFactory;
        private final BuildUtilsInfo info;
        private final ApplicationConfigurationManager appConfigManager;

        private JiraCoreApplication(final JiraAuthenticationContext ctx,
                                    final LicenseLocator locator, final ApplicationAccessFactory accessFactory, final BuildUtilsInfo info,
                                    final ApplicationConfigurationManager appConfigManager) {
            this.info = notNull("info", info);
            this.ctx = notNull("ctx", ctx);
            this.locator = notNull("locator", locator);
            this.accessFactory = notNull("accessFactory", accessFactory);
            this.appConfigManager = notNull("appConfigManager", appConfigManager);
        }

        @Override
        public ApplicationKey getKey() {
            return CoreRoleDefinition.INSTANCE.key();
        }

        @Override
        public String getName() {
            return CoreRoleDefinition.INSTANCE.name();
        }

        @Override
        public String getDescription() {
            return ctx.getI18nHelper().getText("jira.core.description");
        }

        @Override
        public String getVersion() {
            return info.getVersion();
        }

        @Override
        public String getUserCountDescription(final Option<Integer> count) {
            return ctx.getI18nHelper().getText("jira.core.user.count", count.getOrElse(-1));
        }

        @Override
        public Option<URI> getConfigurationURI() {
            return Option.none();
        }

        @Override
        public Option<URI> getPostInstallURI() {
            return Option.some(URI.create(String.format(CREATE_CORE_USER_URL, ApplicationKeys.CORE.value())));
        }

        @Override
        public Option<URI> getPostUpdateURI() {
            return Option.none();
        }

        @Override
        public Option<URI> getProductHelpServerSpaceURI() {
            return Option.some(URI.create("jcore-docs-" + info.getDocVersion()));
        }

        @Override
        public Option<URI> getProductHelpCloudSpaceURI() {
            return Option.some(URI.create("JIRACORECLOUD"));
        }

        @Override
        public String getDefaultGroup() {
            return "jira-core-users";
        }

        @Override
        public void clearConfiguration() {
            appConfigManager.clearConfiguration(getKey());
        }

        @Override
        public DateTime buildDate() {
            return new DateTime(info.getCurrentBuildDate());
        }

        @Override
        public Option<SingleProductLicenseDetailsView> getLicense() {
            return locator.apply(getKey());
        }

        @Override
        public ApplicationAccess getAccess() {
            return accessFactory.access(getKey(), buildDate());
        }
    }
}
