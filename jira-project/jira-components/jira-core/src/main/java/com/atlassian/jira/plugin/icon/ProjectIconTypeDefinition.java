package com.atlassian.jira.plugin.icon;

import com.atlassian.jira.icon.IconType;

import javax.annotation.Nonnull;
import java.util.Objects;

public class ProjectIconTypeDefinition implements IconTypeDefinition {
    private ProjectIconTypePolicy iconTypePolicy;
    private SystemIconImageProvider systemIconImageProvider;

    public ProjectIconTypeDefinition(ProjectIconTypePolicy iconTypePolicy, DefaultSystemIconImageProvider systemIconImageProvider) {
        this.iconTypePolicy = iconTypePolicy;
        this.systemIconImageProvider = systemIconImageProvider;
    }

    @Nonnull
    @Override
    public String getKey() {
        return IconType.PROJECT_ICON_TYPE.getKey();
    }

    @Nonnull
    @Override
    public IconTypePolicy getPolicy() {
        return iconTypePolicy;
    }

    @Nonnull
    @Override
    public SystemIconImageProvider getSystemIconImageProvider() {
        return systemIconImageProvider;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProjectIconTypeDefinition)) return false;
        ProjectIconTypeDefinition that = (ProjectIconTypeDefinition) o;
        return Objects.equals(getKey(), that.getKey());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getKey());
    }
}
