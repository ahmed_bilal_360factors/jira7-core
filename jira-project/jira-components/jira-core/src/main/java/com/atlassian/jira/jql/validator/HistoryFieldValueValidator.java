package com.atlassian.jira.jql.validator;


import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.changehistory.JqlChangeItemMapping;
import com.atlassian.jira.issue.index.ChangeHistoryFieldConfigurationManager;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.issue.search.parameters.lucene.sort.JiraLuceneFieldFinder;
import com.atlassian.jira.jql.ClauseHandler;
import com.atlassian.jira.jql.ValueGeneratingClauseHandler;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.resolver.NameResolver;
import com.atlassian.jira.jql.values.ClauseValuesGenerator;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserKeyService;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.apache.lucene.index.IndexReader;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static com.atlassian.jira.issue.index.DocumentConstants.CHANGE_FROM;
import static com.atlassian.jira.issue.index.DocumentConstants.CHANGE_HISTORY_PROTOCOL;
import static com.atlassian.jira.issue.index.DocumentConstants.CHANGE_TO;
import static com.atlassian.jira.issue.index.DocumentConstants.NEW_VALUE;
import static com.atlassian.jira.issue.index.DocumentConstants.OLD_VALUE;
import static com.atlassian.jira.issue.search.SearchProviderFactory.CHANGE_HISTORY_INDEX;

/**
 * Validates that the values in a history clause are valid for a field.  Has to take into account historical name changes.
 *
 * @since v4.4
 */
public class HistoryFieldValueValidator {
    private static final Set<String> USER_FIELDS = ImmutableSet.of(IssueFieldConstants.ASSIGNEE, IssueFieldConstants.REPORTER);
    public static final String MSG_BAD_NAME_FROM_FUNCTION = "jira.jql.clause.no.value.for.name.from.function";

    private final SearchHandlerManager searchHandlerManager;
    private final JqlChangeItemMapping jqlChangeItemMapping;
    private final JqlOperandResolver operandResolver;
    private final ChangeHistoryFieldConfigurationManager configurationManager;
    private final SearchProviderFactory searchProviderFactory;
    private final I18nHelper.BeanFactory i18nFactory;
    private final UserKeyService userKeyService;

    public HistoryFieldValueValidator(SearchHandlerManager searchHandlerManager, JqlChangeItemMapping jqlChangeItemMapping,
                                      JqlOperandResolver operandResolver, ChangeHistoryFieldConfigurationManager configurationManager,
                                      SearchProviderFactory searchProviderFactory, I18nHelper.BeanFactory i18nFactory,
                                      UserKeyService userKeyService) {
        this.searchHandlerManager = searchHandlerManager;
        this.jqlChangeItemMapping = jqlChangeItemMapping;
        this.operandResolver = operandResolver;
        this.configurationManager = configurationManager;
        this.searchProviderFactory = searchProviderFactory;
        this.i18nFactory = i18nFactory;
        this.userKeyService = userKeyService;
    }

    private boolean stringValueExists(PossibleValuesHolder possibleValuesHolder, ApplicationUser searcher, String fieldName, String rawValue) {
        final String valuePrefix = "";
        final Collection<ClauseHandler> clauseHandlers = searchHandlerManager.getClauseHandler(searcher, fieldName);
        if (clauseHandlers != null && clauseHandlers.size() == 1) {
            ClauseHandler clauseHandler = clauseHandlers.iterator().next();
            if (clauseHandler instanceof ValueGeneratingClauseHandler) {
                if (possibleValuesHolder.getCurrentValues() == null) {
                    initCurrentValues(possibleValuesHolder, searcher, fieldName, valuePrefix, clauseHandler);
                }
                if (possibleValuesHolder.getCurrentValues().contains(rawValue.toLowerCase())) {
                    return true;
                }
                if (possibleValuesHolder.getHistoricalValues() == null) {
                    initHistoricalValues(possibleValuesHolder, fieldName);
                }
                return possibleValuesHolder.getHistoricalValues().contains(rawValue.toLowerCase());
            }
        }
        return false;
    }

    private void initHistoricalValues(final PossibleValuesHolder possibleValuesHolder, final String fieldName) {
        final Set<String> possibleValuesSet = findAllPossibleFieldTerms(fieldName.toLowerCase());
        possibleValuesHolder.setHistoricalValues(possibleValuesSet);
    }

    private void initCurrentValues(final PossibleValuesHolder possibleValuesHolder, final ApplicationUser searcher,
                                   final String fieldName, final String valuePrefix, final ClauseHandler clauseHandler) {
        final ClauseValuesGenerator generator = ((ValueGeneratingClauseHandler) clauseHandler).getClauseValuesGenerator();
        final ClauseValuesGenerator.Results generatorResults = generator.getPossibleValues(searcher,
                jqlChangeItemMapping.mapJqlClauseToFieldName(fieldName), valuePrefix, Integer.MAX_VALUE);
        final List<ClauseValuesGenerator.Result> list = generatorResults.getResults();
        final Set<String> possibleValues = Sets.newHashSetWithExpectedSize(list.size());
        for (ClauseValuesGenerator.Result result : list) {
            possibleValues.add(result.getValue().toLowerCase());
        }
        possibleValuesHolder.setCurrentValues(possibleValues);
    }

    private Set<String> findAllPossibleFieldTerms(String fieldName) {
        // JRA-45287 / JDEV-34987
        // The assignee and reporter fields index the user's display name as CHANGE_FROM and CHANGE_TO, but what we
        // want is usernames.  We can get them by using OLD_VALUE and NEW_VALUE instead, which are the users' keys,
        // and using the UserKeyService to resolve them.
        if (USER_FIELDS.contains(fieldName)) {
            return convertUserkeysToUsernames(findAllPossibleFieldTerms(fieldName, OLD_VALUE, NEW_VALUE));
        }
        return findAllPossibleFieldTerms(fieldName, CHANGE_FROM, CHANGE_TO);
    }

    private Set<String> findAllPossibleFieldTerms(String fieldName, final String oldKey, final String newKey) {
        final Set<String> values = Sets.newHashSet();
        final JiraLuceneFieldFinder luceneFieldFinder = JiraLuceneFieldFinder.getInstance();
        try {
            final IndexReader reader = searchProviderFactory.getSearcher(CHANGE_HISTORY_INDEX).getIndexReader();
            final List<String> oldValues = luceneFieldFinder.getTermValuesForField(reader, fieldName + '.' + oldKey);
            final List<String> newValues = luceneFieldFinder.getTermValuesForField(reader, fieldName + '.' + newKey);
            values.addAll(oldValues);
            values.addAll(newValues);
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
        return stripProtocol(values);
    }

    private Set<String> convertUserkeysToUsernames(final Set<String> keys) {
        final Set<String> usernames = Sets.newHashSetWithExpectedSize(keys.size());
        for (String key : keys) {
            final String username = userKeyService.getUsernameForKey(key);
            if (username != null) {
                usernames.add(username);
            }
        }
        return usernames;
    }

    private static Set<String> stripProtocol(Set<String> values) {
        final Set<String> newValues = Sets.newHashSetWithExpectedSize(values.size());
        final int index = CHANGE_HISTORY_PROTOCOL.length();
        for (String value : values) {
            newValues.add(value.substring(index));
        }
        return newValues;
    }

    /**
     * Validate a set of values supplied within a JQL query.
     * <p>
     * For history clauses, such as {@code assignee WAS fred} or {@code status CHANGED FROM Open TO Resolved BY fred},
     * values are permitted if either they are currently valid or they exist anywhere in the change history.
     * </p>
     *
     * @param searcher  the user performing the search
     * @param fieldName the name of the field to be validated, such as "assignee" or "status"
     * @param rawValues the values supplied for validation
     * @return a message set containing translated error messages.  If there are no errors, then an empty message
     * set is returned
     */
    @Nonnull
    public MessageSet validateValues(ApplicationUser searcher, String fieldName, List<QueryLiteral> rawValues) {
        final PossibleValuesHolder possibleValuesHolder = new PossibleValuesHolder();
        final MessageSet messages = new MessageSetImpl();
        for (QueryLiteral rawValue : rawValues) {
            if (rawValue.getStringValue() != null) {
                validateStringValue(searcher, fieldName, possibleValuesHolder, messages, rawValue);
            } else if (rawValue.getLongValue() != null) {
                if (stringValueExists(possibleValuesHolder, searcher, fieldName, rawValue.getLongValue().toString())) {
                    return messages;
                }
                validateLongValue(searcher, fieldName, messages, rawValue);
            }
        }
        return messages;
    }

    private void validateLongValue(final ApplicationUser searcher, final String fieldName, final MessageSet messages,
                                   final QueryLiteral rawValue) {
        if (configurationManager.supportsIdSearching(fieldName.toLowerCase())) {
            validateIdValue(searcher, fieldName, messages, rawValue);
        } else if (operandResolver.isFunctionOperand(rawValue.getSourceOperand())) {
            messages.addErrorMessage(getI18n(searcher).getText(MSG_BAD_NAME_FROM_FUNCTION, rawValue.getSourceOperand().getName(), fieldName));
        } else {
            messages.addErrorMessage(getI18n(searcher).getText("jira.jql.history.clause.not.string", rawValue.getSourceOperand().getName(), fieldName));
        }
    }

    private void validateIdValue(final ApplicationUser searcher, final String fieldName, final MessageSet messages,
                                 final QueryLiteral rawValue) {
        if (!longValueExists(fieldName, rawValue.getLongValue())) {
            if (operandResolver.isFunctionOperand(rawValue.getSourceOperand())) {
                messages.addErrorMessage(getI18n(searcher).getText("jira.jql.clause.no.value.for.name.from.function",
                        rawValue.getSourceOperand().getName(), fieldName));
            } else {
                messages.addErrorMessage(getI18n(searcher).getText("jira.jql.clause.no.value.for.id",
                        fieldName, rawValue.getLongValue().toString()));
            }
        }
    }

    private void validateStringValue(final ApplicationUser searcher, final String fieldName,
                                     final PossibleValuesHolder possibleValuesHolder, final MessageSet messages, final QueryLiteral rawValue) {
        if (!stringValueExists(possibleValuesHolder, searcher, fieldName, rawValue.getStringValue())) {
            if (operandResolver.isFunctionOperand(rawValue.getSourceOperand())) {
                messages.addErrorMessage(getI18n(searcher).getText("jira.jql.clause.no.value.for.name.from.function", rawValue.getSourceOperand().getName(), fieldName));
            } else {
                messages.addErrorMessage(getI18n(searcher).getText("jira.jql.clause.no.value.for.name", fieldName, rawValue.getStringValue()));
            }
        }
    }

    private boolean longValueExists(String fieldName, Long longValue) {
        NameResolver<?> resolver = configurationManager.getNameResolver(fieldName.toLowerCase());
        return resolver.idExists(longValue);
    }

    private I18nHelper getI18n(ApplicationUser user) {
        return i18nFactory.getInstance(user);
    }

    /**
     * Holder for the current and historic values for the field being searched.
     * <p>
     * This exists primarily so that we can lazily initialize the historic values, as digging through
     * the change history to find all of them is expensive, so we only want to do that if none of
     * the currently valid values match.
     * </p>
     */
    static class PossibleValuesHolder {
        private Set<String> currentValues;
        private Set<String> historicalValues;

        private Set<String> getCurrentValues() {
            return currentValues;
        }

        private void setCurrentValues(final Set<String> currentValues) {
            this.currentValues = currentValues;
        }

        private Set<String> getHistoricalValues() {
            return historicalValues;
        }

        private void setHistoricalValues(final Set<String> historicalValues) {
            this.historicalValues = historicalValues;
        }
    }
}
