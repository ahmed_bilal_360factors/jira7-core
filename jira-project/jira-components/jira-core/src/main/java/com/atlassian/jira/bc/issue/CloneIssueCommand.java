package com.atlassian.jira.bc.issue;

import com.atlassian.fugue.Either;
import com.atlassian.jira.concurrent.Barrier;
import com.atlassian.jira.concurrent.BarrierFactory;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.AttachmentError;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.link.RemoteIssueLink;
import com.atlassian.jira.issue.link.RemoteIssueLinkBuilder;
import com.atlassian.jira.issue.link.RemoteIssueLinkManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.task.ProvidesTaskProgress;
import com.atlassian.jira.task.TaskProgressSink;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.task.context.Contexts;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.collect.FixedSized;
import com.atlassian.jira.util.dbc.Assertions;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;

import static com.atlassian.jira.permission.ProjectPermissions.MODIFY_REPORTER;

/**
 * Background command for assigning a new issue security scheme to a project
 *
 * @since v7.1.1
 */
public class CloneIssueCommand implements Callable<CloneIssueCommand.CloneIssueResult>, ProvidesTaskProgress {

    private final ApplicationUser user;
    private final Issue originalIssue;
    private final String summary;
    private final boolean cloneAttachments;
    private final boolean cloneSubTasks;
    private final boolean cloneLinks;
    private Map<CustomField, Optional<Boolean>> cloneOptionSelections;
    private final IssueManager issueManager;
    private final IssueFactory issueFactory;
    private final ApplicationProperties applicationProperties;
    private final IssueLinkTypeManager issueLinkTypeManager;
    private final IssueLinkManager issueLinkManager;
    private final RemoteIssueLinkManager remoteIssueLinkManager;
    private final AttachmentManager attachmentManager;
    private final SubTaskManager subTaskManager;
    private final PermissionManager permissionManager;
    private final CustomFieldManager customFieldManager;
    private final Logger log;
    private final I18nHelper i18nHelper;
    private volatile TaskProgressSink taskProgressSink;
    /**
     * This barrier is used for testing race conditions in background indexing. The barrier should never be raised in
     * production.
     */
    private final Barrier cloneIssueBarrier;

    public CloneIssueCommand(ApplicationUser user, Issue originalIssue, String summary, boolean cloneAttachments, boolean cloneSubTasks, boolean cloneLinks,
                             Map<CustomField, Optional<Boolean>> cloneOptionSelections, IssueManager issueManager, IssueFactory issueFactory, ApplicationProperties applicationProperties,
                             IssueLinkTypeManager issueLinkTypeManager, IssueLinkManager issueLinkManager, RemoteIssueLinkManager remoteIssueLinkManager,
                             AttachmentManager attachmentManager, SubTaskManager subTaskManager, PermissionManager permissionManager, CustomFieldManager customFieldManager, final Logger log,
                             final I18nHelper i18nHelper, BarrierFactory barrierFactory) {
        this.user = user;
        this.originalIssue = originalIssue;
        this.summary = summary;
        this.cloneAttachments = cloneAttachments;
        this.cloneSubTasks = cloneSubTasks;
        this.cloneLinks = cloneLinks;
        this.cloneOptionSelections = cloneOptionSelections;
        this.issueManager = issueManager;
        this.issueFactory = issueFactory;
        this.applicationProperties = applicationProperties;
        this.issueLinkTypeManager = issueLinkTypeManager;
        this.issueLinkManager = issueLinkManager;
        this.remoteIssueLinkManager = remoteIssueLinkManager;
        this.attachmentManager = attachmentManager;
        this.subTaskManager = subTaskManager;
        this.permissionManager = permissionManager;
        this.customFieldManager = customFieldManager;
        this.log = log;
        this.i18nHelper = i18nHelper;

        cloneIssueBarrier = barrierFactory.getBarrier("cloneIssue");
    }

    public CloneIssueResult call() {

        final int totalProgressSize = estimateProgressRequired();
        Context context = Contexts.builder()
                .sized(new FixedSized(totalProgressSize))
                .progressSubTask(taskProgressSink, i18nHelper, "cloneissue.progress.cloning.current.step")
                .build();

        try {
            MutableIssue cloneIssue = issueFactory.cloneIssue(originalIssue);

            prepareForCloningIssue(user, originalIssue, cloneIssue, cloneOptionSelections);
            cloneIssue.setSummary(summary);

            Context.Task taskStep = context.start(originalIssue);
            // Create the clone issue (without attachments)
            Issue issue = issueManager.createIssueObject(user, cloneIssue);
            taskStep.complete();

            Set<Long> originalIssueIdSet = idsOfOriginalIssueIncludingSubTaskIfNeeded(user, originalIssue, cloneSubTasks);

            cloneIssueDependencies(context, user, originalIssue, issue, originalIssueIdSet, cloneAttachments, cloneSubTasks, cloneLinks);

            cloneIssueBarrier.await();
            return new CloneIssueResult(cloneIssue.getKey());

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            ErrorCollection errorCollection = new SimpleErrorCollection();
            errorCollection.addErrorMessage(i18nHelper.getText("admin.errors.exception") + " " + e);
            return new CloneIssueResult(null, errorCollection);
        } finally {
            // In case the operation does less work than we expected (due to bugs/errors/etc), we complete the progress manually.
            for (int i = 0; i < context.getNumberOfTasksToCompletion(); i++) {
                context.start(null).complete();
            }
        }
    }

    private int estimateProgressRequired() {
        // We will count all the basic things we need to do that can be ticked off as progress.
        // Start with the original issue
        int total = 1;
        if (cloneAttachments && attachmentManager.attachmentsEnabled()) {
            total += attachmentManager.getAttachments(originalIssue).size();
        }
        if (cloneLinks && issueLinkManager.isLinkingEnabled()) {
            total += issueLinkManager.getInwardLinks(originalIssue.getId()).size();
            total += issueLinkManager.getOutwardLinks(originalIssue.getId()).size();
            total += remoteIssueLinkManager.getRemoteIssueLinksForIssue(originalIssue).size();
        }
        if (subTaskManager.isSubTasksEnabled() && cloneSubTasks) {
            total += originalIssue.getSubTaskObjects().size();
        }
        return total;
    }

    /**
     * This method is responsible for preparing details for cloning issue. Preparation consists of two parts such as
     * initializing some fields and actually cloning some fields from original issue.
     */
    private void prepareForCloningIssue(ApplicationUser user, Issue originalIssue, MutableIssue cloneIssue, Map<CustomField, Optional<Boolean>> cloneOptionSelections) {

        setNotCloningFieldsToBlankByDefault(user, originalIssue, cloneIssue);
        cloneSomeFieldsFromOriginalIssue(user, originalIssue, cloneIssue, cloneOptionSelections);
    }

    private void setNotCloningFieldsToBlankByDefault(ApplicationUser user, Issue originalIssue, MutableIssue cloneIssue) {
        cloneIssue.setCreated(null);
        cloneIssue.setUpdated(null);
        cloneIssue.setVotes(null);
        cloneIssue.setWatches(0L);
        cloneIssue.setStatus(null);
        cloneIssue.setWorkflowId(null);
        // Ensure that the 'time spent' and 'remaining estimated' are not cloned - JRA-7165
        // We need to copy the value of 'original estimate' to the value 'remaining estimate' as they must be kept in sync
        // until work is logged on an issue.
        cloneIssue.setEstimate(originalIssue.getOriginalEstimate());
        cloneIssue.setTimeSpent(null);
        //JRA-18731: Cloning a resolved issue will result in an open issue.  The resolution date should be reset.
        cloneIssue.setResolutionDate(null);

        // If the user does not have permission to modify the reporter, initialise the reporter to be the remote user
        if (!isCanModifyReporter(user, cloneIssue)) {
            cloneIssue.setReporter(user);
        }
    }

    private void cloneIssueDependencies(Context context, ApplicationUser user, Issue originalIssue, Issue cloneIssue, Set<Long> originalIssueIdSet, boolean cloneAttachments,
                                        boolean cloneSubTasks, boolean cloneLinks) throws Exception {

        final Map<Long, Long> newIssueIdMap = new HashMap<>();
        newIssueIdMap.put(originalIssue.getId(), cloneIssue.getId());

        linkCloningIssueToOriginalOne(user, originalIssue, cloneIssue);

        context.setName(i18nHelper.getText("cloneissue.progress.cloning.attachments"));
        cloneAttachmentsIfNeeded(context, user, originalIssue, cloneIssue, cloneAttachments);
        // JRA-17222 - We want to know all the issues being cloned, so we can choose to create links to the new
        // version of cloned issues.
        context.setName(i18nHelper.getText("cloneissue.progress.cloning.issue.links"));
        cloneLinksIfNeeded(context, user, originalIssue, cloneIssue, originalIssueIdSet, cloneSubTasks, cloneLinks, newIssueIdMap);

        if (isCloningSubTask(originalIssue)) {
            linkCloningSubTaskToOriginalSubTaskParent(user, originalIssue, cloneIssue);

        } else {
            context.setName(i18nHelper.getText("cloneissue.progress.cloning.subtasks"));
            Map<Long, Long> subtaskIdMap = cloneSubTasksIfNeeded(context, user, originalIssue, cloneIssue, originalIssueIdSet, cloneAttachments, cloneSubTasks, cloneLinks);
            newIssueIdMap.putAll(subtaskIdMap);
        }
    }

    /**
     * Please be informed that there ain't a 100% copy from original issue, but with modification.
     */
    private void cloneSomeFieldsFromOriginalIssue(ApplicationUser user, Issue originalIssue, MutableIssue clonedIssue, Map<CustomField, Optional<Boolean>> cloneOptionSelections) {
        clonedIssue.setFixVersions(withoutArchivedVersions(originalIssue.getFixVersions()));
        clonedIssue.setAffectedVersions(withoutArchivedVersions(originalIssue.getAffectedVersions()));
        cloneCustomFields(user, originalIssue, clonedIssue, cloneOptionSelections);
    }

    @VisibleForTesting
    void cloneCustomFields(ApplicationUser user, Issue originalIssue, MutableIssue clonedIssue, Map<CustomField, Optional<Boolean>> cloneOptionSelections) {
        List<CustomField> customFields = getCustomFields(originalIssue);
        for (CustomField cf : customFields) {
            Optional<Boolean> cloneOption = cloneOptionSelections.get(cf);
            if (cloneOption == null) {
                cloneOption = Optional.empty();
            }
            Object clonedValue = cf.getCustomFieldType().getCloneValue(cf, originalIssue, cloneOption);
            if (clonedValue != null) {
                clonedIssue.setCustomFieldValue(cf, clonedValue);
            }
        }
    }

    private Collection withoutArchivedVersions(Collection<Version> versions) {
        List<Version> notArchivedVersions = new ArrayList<Version>();
        for (final Version version : versions) {
            if (!version.isArchived()) {
                notArchivedVersions.add(version);
            }
        }
        return notArchivedVersions;
    }

    private void linkCloningIssueToOriginalOne(ApplicationUser user, Issue originalIssue, final Issue cloneIssue) throws CreateException {
        // Create link between the cloned issue and the original - sequence on links does not matter.
        final IssueLinkType cloneIssueLinkType = getCloneIssueLinkType();
        if (cloneIssueLinkType != null) {
            issueLinkManager.createIssueLink(cloneIssue.getId(), originalIssue.getId(), cloneIssueLinkType.getId(), null,
                    user);
        }
    }

    // Retrieve the issue link type specified by the clone link name in the properties file.
    // If the name is unset - issue linking of originals to clones is not required - returns null.
    // Otherwise, returns null if the issue link type with the specified name cannot be found in the system.
    public IssueLinkType getCloneIssueLinkType() {
        IssueLinkType cloneIssueLinkType = null;
        String cloneLinkTypeName = applicationProperties.getDefaultBackedString(APKeys.JIRA_CLONE_LINKTYPE_NAME);

        if (StringUtils.isBlank(cloneLinkTypeName)) {
            // Issue linking is not required
            cloneIssueLinkType = null;
        } else {
            final Collection<IssueLinkType> cloneIssueLinkTypes = issueLinkTypeManager.getIssueLinkTypesByName(cloneLinkTypeName);
            if (CollectionUtils.isEmpty(cloneIssueLinkTypes)) {
                log.warn("The clone link type '" + cloneLinkTypeName + "' does not exist. A link to the original issue will not be created.");
                cloneIssueLinkType = null;
            } else {
                for (final IssueLinkType issueLinkType : cloneIssueLinkTypes) {
                    if (issueLinkType.getName().equals(cloneLinkTypeName)) {
                        cloneIssueLinkType = issueLinkType;
                    }
                }
            }
        }
        return cloneIssueLinkType;
    }

    private void cloneAttachmentsIfNeeded(Context context, ApplicationUser user, Issue originalIssue, Issue clone, boolean cloneAttachments) throws CreateException {
        /*
         * Note, that Create Attachment permission is not checked,
         * the same way Link Issue permission is not checked for cloning links.
         */
        if (cloneAttachments && attachmentManager.attachmentsEnabled()) {
            Map<Long, Either<AttachmentError, Attachment>> result =
                    attachmentManager.copyAttachments(context, originalIssue, user, clone.getKey());
            for (final Either<AttachmentError, Attachment> either : result.values()) {
                if (either.isLeft()) {
                    final AttachmentError error = either.left().get();
                    log.warn(error.getLogMessage(), error.getException().getOrNull());
                }
            }
        }

    }

    private void cloneLinksIfNeeded(Context context, ApplicationUser user, Issue originalIssue, Issue cloneIssue, Set<Long> originalIssueIdSet, boolean cloneSubTasks, boolean cloneLinks,
                                    final Map<Long, Long> newIssueIdMap) throws CreateException {

        if (cloneLinks && issueLinkManager.isLinkingEnabled()) {
            Collection<IssueLink> inwardLinks = issueLinkManager.getInwardLinks(originalIssue.getId());
            cloneInwardLinks(context, user, originalIssue, cloneIssue, originalIssueIdSet, inwardLinks, newIssueIdMap);

            Collection<IssueLink> outwardLinks = issueLinkManager.getOutwardLinks(originalIssue.getId());
            cloneOutwardLinks(context, user, originalIssue, cloneIssue, originalIssueIdSet, outwardLinks, newIssueIdMap);

            cloneRemoteIssueLinks(context, user, originalIssue, cloneIssue);
        }
    }

    private void cloneInwardLinks(Context context, ApplicationUser user, Issue originalIssue, Issue cloneIssue, Set<Long> originalIssueIdSet,
                                  Collection<IssueLink> givenLinks, final Map<Long, Long> newIssueIdMap) throws CreateException {
        cloningGivenIssueLinks(context, user, originalIssue, cloneIssue, originalIssueIdSet, givenLinks, true, newIssueIdMap);
    }

    private void cloneOutwardLinks(Context context, ApplicationUser user, Issue originalIssue, Issue cloneIssue, Set<Long> originalIssueIdSet,
                                   Collection<IssueLink> givenLinks, final Map<Long, Long> newIssueIdMap) throws CreateException {
        cloningGivenIssueLinks(context, user, originalIssue, cloneIssue, originalIssueIdSet, givenLinks, false, newIssueIdMap);
    }

    private void cloningGivenIssueLinks(Context context, ApplicationUser user, Issue originalIssue, Issue cloneIssue, Set<Long> originalIssueIdSet,
                                        Collection<IssueLink> givenLinks, boolean isCopyingInwardLinks, final Map<Long, Long> newIssueIdMap) throws CreateException {
        for (final IssueLink issueLink : givenLinks) {
            Context.Task taskStep = context.start(originalIssue);
            try {
                if (isCopyableLink(issueLink)) {
                    // JRA-17222. Check if this link is from another Issue in the "clone set"
                    Long workingIssueId = isCopyingInwardLinks ? issueLink.getSourceId() : issueLink.getDestinationId();

                    if (originalIssueIdSet.contains(workingIssueId)) {
                        // We want to create a link to the new cloned version of that issue, not the original
                        // This can return null if that issue is not cloned yet, but that is OK, we will create the link as an outward link after we clone the second one.
                        workingIssueId = newIssueIdMap.get(workingIssueId);
                    }
                    if (workingIssueId != null) {
                        if (isCopyingInwardLinks) {
                            log.debug("Creating inward link to " + cloneIssue.getKey() + " (cloned from " + originalIssue.getKey() + ", link " + issueLink + ")");
                            issueLinkManager.createIssueLink(workingIssueId, cloneIssue.getId(), issueLink.getIssueLinkType().getId(), null,
                                    user);
                        } else {
                            log.debug("Creating outward link from " + cloneIssue.getKey() + " (cloned from " + originalIssue.getKey() + ", link " + issueLink + ")");
                            issueLinkManager.createIssueLink(cloneIssue.getId(), workingIssueId, issueLink.getIssueLinkType().getId(), null,
                                    user);
                        }
                    }
                }
            } finally {
                taskStep.complete();
            }
        }
    }

    private void cloneRemoteIssueLinks(Context context, ApplicationUser user, Issue originalIssue, Issue cloneIssue) throws CreateException {
        final List<RemoteIssueLink> originalLinks = remoteIssueLinkManager.getRemoteIssueLinksForIssue(originalIssue);
        for (final RemoteIssueLink originalLink : originalLinks) {
            Context.Task taskStep = context.start(originalLink);
            try {
                final RemoteIssueLink link = new RemoteIssueLinkBuilder(originalLink).id(null).issueId(cloneIssue.getId()).build();
                remoteIssueLinkManager.createRemoteIssueLink(link, user);
            } finally {
                taskStep.complete();
            }
        }
    }

    private boolean isCopyableLink(IssueLink checkingLink) {
        // Do not copy system links types and do not copy the cloners link type, as it is used to record the relationship between cloned issues
        // So if the cloners link type does not exists, or the link is not of cloners link type, and is not a system link, then copy it
        return !checkingLink.isSystemLink() &&
                (getCloneIssueLinkType() == null || givenLinkTypeIsNotSameAsCloneIssueLinkType(checkingLink));
    }

    private boolean givenLinkTypeIsNotSameAsCloneIssueLinkType(final IssueLink checkingLink) {
        return getCloneIssueLinkType().getId().equals(checkingLink.getIssueLinkType().getId()) == false;
    }


    /**
     * Returns the set of original issues that are being cloned.
     * This will obviously always include the given "original issue", and may also include the subtasks of this issue.
     *
     * @return Set of ID's of the issue being cloned, and its subtasks if they are being cloned as well.
     */
    private Set<Long> idsOfOriginalIssueIncludingSubTaskIfNeeded(ApplicationUser user, Issue originalIssue, boolean cloneSubTasks) {
        Set<Long> originalIssues = new HashSet<Long>();
        originalIssues.add(originalIssue.getId());
        // Add subtasks if required
        if (subTaskManager.isSubTasksEnabled() && cloneSubTasks) {
            for (final Issue subTask : originalIssue.getSubTaskObjects()) {
                originalIssues.add(subTask.getId());
            }
        }
        return originalIssues;
    }

    private boolean isCloningSubTask(Issue originalIssue) {
        return originalIssue.isSubTask();
    }

    private void linkCloningSubTaskToOriginalSubTaskParent(ApplicationUser user, Issue originalIssue, Issue cloneIssue) throws CreateException {
        // Retrieve the parent of the original subtask
        Issue subTaskParent = originalIssue.getParentObject();

        // Link the clone subtask to the parent of the original subtask (by this stage the getIssue() method returns the newly cloned issue)
        subTaskManager.createSubTaskIssueLink(subTaskParent, cloneIssue, user);
    }

    // Clone sub-tasks if subtasks are enabled and exist for the original issue
    private Map<Long, Long> cloneSubTasksIfNeeded(Context context, ApplicationUser user, Issue originalIssue, Issue cloneIssue, Set<Long> originalIssueIdSet, boolean cloneAttachments,
                                                  boolean cloneSubTasks, boolean cloneLinks) throws Exception {

        final Map<Long, Long> newIssueIdMap = new HashMap<Long, Long>();
        String clonePrefixProperties = applicationProperties.getDefaultBackedString(APKeys.JIRA_CLONE_PREFIX);

        if (subTaskManager.isSubTasksEnabled() && cloneSubTasks) {
            // Iterate over all subtask links, retrieve subtasks and copy details for clone subtask
            for (Issue originalSubTask : originalIssue.getSubTaskObjects()) {

                Context.Task taskStep = context.start(originalIssue);
                try {
                    MutableIssue cloneSubTask = issueFactory.cloneIssue(originalSubTask);

                    String subTaskSummary = cloneSubTask.getSummary();
                    String cloneSummary = StringUtils.isBlank(clonePrefixProperties) ? subTaskSummary : StringUtils.join(new Object[]{clonePrefixProperties, subTaskSummary}, " ");
                    cloneSubTask.setSummary(cloneSummary);

                    prepareForCloningIssue(user, originalSubTask, cloneSubTask, cloneOptionSelections);
                    // JRA-15949. Set the parent id to the NEW parent. Otherwise we get the wrong parentId in the IssueEvent.
                    cloneSubTask.setParentId(cloneIssue.getId());

                    // Create the clone issue (without attachments)
                    Issue subTask = issueManager.createIssueObject(user, cloneSubTask);

                    newIssueIdMap.put(originalSubTask.getId(), subTask.getId());

                    // Use  null task context here so as to not double count the progress
                    cloneLinksIfNeeded(Contexts.nullContext(), user, originalSubTask, subTask, originalIssueIdSet, cloneSubTasks, cloneLinks, newIssueIdMap);
                    // Link the clone subtask to the clone parent issue.
                    subTaskManager.createSubTaskIssueLink(cloneIssue, subTask, user);
                    cloneAttachmentsIfNeeded(Contexts.nullContext(), user, originalSubTask, subTask, cloneAttachments);
                } finally {
                    taskStep.complete();
                }
            }
        }
        return newIssueIdMap;
    }

    public boolean isCanModifyReporter(ApplicationUser user, Issue issue) {
        return permissionManager.hasPermission(MODIFY_REPORTER, issue, user);
    }

    public List<CustomField> getCustomFields(Issue issue) {
        return customFieldManager.getCustomFieldObjects(issue.getProjectId(), issue.getIssueTypeId());
    }

    public void setTaskProgressSink(final TaskProgressSink taskProgressSink) {
        this.taskProgressSink = taskProgressSink;
    }

    public static final class CloneIssueResult implements Serializable {
        private final SimpleErrorCollection errorCollection;
        private final String issueKey;

        public CloneIssueResult(String issueKey, final ErrorCollection errorCollection) {
            this.issueKey = issueKey;
            Assertions.notNull("errorCollection", errorCollection);
            this.errorCollection = new SimpleErrorCollection(errorCollection);
        }

        public CloneIssueResult(String issueKey) {
            this.issueKey = issueKey;
            this.errorCollection = new SimpleErrorCollection();
        }

        public String getIssueKey() {
            return issueKey;
        }

        public ErrorCollection getErrorCollection() {
            return errorCollection;
        }

        public boolean isSuccessful() {
            return !errorCollection.hasAnyErrors();
        }

    }
}