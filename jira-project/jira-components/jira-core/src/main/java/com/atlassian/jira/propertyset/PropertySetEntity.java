package com.atlassian.jira.propertyset;

import com.google.common.collect.ImmutableSet;

import java.util.Set;

/**
 * Entity definitions related to property set entries.
 *
 * @since v6.2
 */
interface PropertySetEntity {
    // Entity name for the entry storage
    String PROPERTY_ENTRY = "OSPropertyEntry";

    // Entity names for the value storage entities, which reuse the ID from the entry
    String PROPERTY_STRING = "OSPropertyString";
    String PROPERTY_TEXT = "OSPropertyText";
    String PROPERTY_DATE = "OSPropertyDate";
    String PROPERTY_NUMBER = "OSPropertyNumber";
    String PROPERTY_DECIMAL = "OSPropertyDecimal";

    // The field names for OSPropertyEntry
    String ID = "id";
    String ENTITY_NAME = "entityName";
    String ENTITY_ID = "entityId";
    String PROPERTY_KEY = "propertyKey";
    String TYPE = "type";

    // The field name used for the value in all of the value storage entities
    String VALUE = "value";

    // Field selectors to limit the data that is returned for each row to whatever we actually need
    Set<String> SELECT_ID_KEY_AND_TYPE = ImmutableSet.of(ID, PROPERTY_KEY, TYPE);
    Set<String> SELECT_ID_AND_TYPE = ImmutableSet.of(ID, TYPE);
    Set<String> SELECT_KEY = ImmutableSet.of(PROPERTY_KEY);
}
