package com.atlassian.jira.web.filters.johnson;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Delivers a trivial 503 response with no content.
 *
 * @since v7.1.0
 */
public class ServiceUnavailableResponder {
    public static void respondWithEmpty503(final ServletResponse response) throws IOException {
        respondWithEmpty503((HttpServletResponse) response);
    }

    public static void respondWithEmpty503(final HttpServletResponse response) throws IOException {
        response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
        response.setHeader("Retry-After", "30");
        // flushing the writer stops the app server from putting its html message into the otherwise empty response
        response.getWriter().flush();
    }
}
