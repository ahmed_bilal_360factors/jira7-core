package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.util.BatchResult;

import java.util.Collection;
import java.util.List;

/**
 * This interface is used by OfBizDelegatingMembershipDao to avoid circular dependencies with the User and Group DAOs.
 */
public interface InternalMembershipDao {
    boolean isUserDirectMember(long directoryId, String userName, String groupName);

    boolean isGroupDirectMember(long directoryId, String childGroup, String parentGroup);

    void addUserToGroup(long directoryId, UserOrGroupStub user, UserOrGroupStub group) throws MembershipAlreadyExistsException;

    BatchResult<String> addAllUsersToGroup(long directoryId, Collection<UserOrGroupStub> users, UserOrGroupStub group);

    void addGroupToGroup(long directoryId, UserOrGroupStub child, UserOrGroupStub parent);

    long countDirectMembersOfGroup(long directoryId, String groupName, MembershipType membershipType);

    void removeAllMembersFromGroup(Group group);

    void removeAllGroupMemberships(Group group);

    void removeAllUserMemberships(User user);

    void removeAllUserMemberships(long directoryId, String username);

    void removeUserFromGroup(long directoryId, UserOrGroupStub user, UserOrGroupStub group) throws MembershipNotFoundException;

    void removeGroupFromGroup(long directoryId, UserOrGroupStub childGroup, UserOrGroupStub parentGroup)
            throws MembershipNotFoundException;

    <T> List<String> search(long directoryId, MembershipQuery<T> query);

    void flushCache();

    /**
     * Finds direct group children of the specified groups in a directory.
     *
     * @param directoryId the directory.
     * @param groupNames  the names of the groups whose group children are returned.  If this is empty, an empty collection is returned.
     * @return the names of the group children.
     * @since v7.0
     */
    Collection<String> findGroupChildrenOfGroups(long directoryId, Collection<String> groupNames);

    /**
     * Finds direct user children of the specified groups in a directory.
     *
     * @param directoryId the directory.
     * @param groupNames  the name of the groups whose user children are returned.  If this is empty, an empty collection is returned.
     * @return the names of the users in the specified groups.
     * @since v7.0
     */
    Collection<String> findUserChildrenOfGroups(long directoryId, Collection<String> groupNames);

}
