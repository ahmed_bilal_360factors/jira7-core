package com.atlassian.jira.web.component.subtask;

import com.atlassian.jira.bean.SubTask;
import com.atlassian.jira.bean.SubTaskBean;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.web.component.SimpleColumnLayoutItem;

import java.util.Collection;
import java.util.Map;

import static com.atlassian.jira.template.TemplateSources.file;

/**
 * Provides basic utilities for columns in subtasks table layout.
 *
 * @since v7.2
 */
public abstract class AbstractSubTaskColumnLayoutItem extends SimpleColumnLayoutItem {

    private final VelocityTemplatingEngine velocity;
    private final SubTaskBean subTaskBean;
    private final String subTasksView;

    private final Collection<SubTask> subTasks;

    protected AbstractSubTaskColumnLayoutItem(VelocityTemplatingEngine velocity, SubTaskBean subTaskBean, String subTasksView) {
        this.velocity = velocity;
        this.subTaskBean = subTaskBean;
        this.subTasksView = subTasksView;

        subTasks = subTaskBean.getSubTasks(subTasksView);
    }

    @Override
    public String getHtml(Map displayParams, Issue issue) {
        return velocity.render(file(getTemplate())).applying(getContext(issue)).asHtml();
    }

    protected abstract Map<String, Object> getContext(Issue issue);

    protected abstract String getTemplate();

    protected Long getCurrentSubTaskSequence(Issue issue) {
        for (final SubTask subTask : subTasks) {
            if (subTask.getSubTask().equals(issue)) {
                return subTask.getSequence();
            }
        }

        //That can occur only if issue for some reason is not a subtaks of its parent anymore.
        //Never should happen in normal situation.
        return new Long(-1);
    }

    protected Collection<SubTask> getSubTasks() {
        return subTasks;
    }
}
