package com.atlassian.jira.sharing.search;

import com.atlassian.instrumentation.operations.SimpleOpTimer;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.sharing.SharedEntity;

/**
 * Simple tuple to contain search results and timer information.
 *
 * @since v7.1
 */
public class TimedSearchResult<E extends SharedEntity> {
    private final SharedEntitySearchResult<E> result;
    private final SimpleOpTimer timer;

    public TimedSearchResult(SharedEntitySearchResult<E> result, SimpleOpTimer timer) {
        this.result = result;
        this.timer = timer;
    }

    public SharedEntitySearchResult<E> getResult() {
        return result;
    }

    public SimpleOpTimer getTimer() {
        return timer;
    }
}
