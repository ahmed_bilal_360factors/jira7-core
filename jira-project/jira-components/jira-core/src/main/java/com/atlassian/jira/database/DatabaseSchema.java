package com.atlassian.jira.database;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.DatabaseConfigurationService;

import javax.annotation.Nonnull;
import java.util.Optional;

import static org.apache.commons.lang3.StringUtils.defaultString;

/**
 * Exposes the Database schema as configured in dbconfig.xml
 *
 * @since v6.4.4
 */
public class DatabaseSchema {
    private static volatile Optional<String> schemaName = Optional.empty();

    public static void reset() {
        schemaName = Optional.empty();
    }

    public static String getSchemaName() {
        return schemaName.orElseGet(DatabaseSchema::init);
    }

    // DatabaseConfigurationManager.getDatabaseConfiguration() will throw a RuntimeException if you call it before
    // we have a configured DB config (i.e on a fresh install before DB Setup).
    // Once it actually returns successfully, the schema name will not change.
    @Nonnull
    private static String init() {
        final DatabaseConfigurationService svc = ComponentAccessor.getComponent(DatabaseConfigurationService.class);
        final String name = defaultString(svc.getSchemaName());
        schemaName = Optional.of(name);
        return name;
    }
}
