package com.atlassian.jira.security.groups;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.jira.cache.request.RequestCache;
import com.atlassian.jira.cache.request.RequestCacheFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.Page;
import com.atlassian.jira.util.PageRequest;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Set;

/**
 * This class is wrapper around {@link com.atlassian.jira.security.groups.DefaultGroupManager} which adds request caching
 * functionality around some of its methods. When methods of this class are called outside of request scope all calls
 * will simply get delegated to underlying {@link com.atlassian.jira.security.groups.DefaultGroupManager}.
 *
 * @since v7.0.1
 */
public class RequestCachingGroupManager implements GroupManager {

    private static final String USERNAME_TO_GROUP_NAMES_CACHE_KEY = RequestCachingGroupManager.class.getName() + ".usernameToGroupNames";
    private static final String USERNAME_TO_GROUPS_CACHE_KEY = RequestCachingGroupManager.class.getName() + ".usernameToGroups";
    private static final String USERNAME_TO_GROUP_NAMES_LOWER_CASE_CACHE_KEY = RequestCachingGroupManager.class.getName() + ".usernameToGroupNamesInLowerCase";
    private final DefaultGroupManager defaultGroupManager;
    private final RequestCache<String, Set<Group>> usernameToGroups;
    private final RequestCache<String, Set<String>> usernameToGroupNames;
    private final RequestCache<String, Set<String>> usernameToGroupNamesInLowerCase;


    public RequestCachingGroupManager(final DefaultGroupManager defaultGroupManager, final RequestCacheFactory requestCacheFactory) {
        this.defaultGroupManager = defaultGroupManager;
        usernameToGroups = requestCacheFactory.createRequestCache(USERNAME_TO_GROUPS_CACHE_KEY,
                (username) -> ImmutableSet.copyOf(defaultGroupManager.getGroupsForUser(username)));

        usernameToGroupNames = requestCacheFactory.createRequestCache(USERNAME_TO_GROUP_NAMES_CACHE_KEY,
                (username) -> usernameToGroups.get(username).stream()
                        .map(Group::getName)
                        .collect(CollectorsUtil.toImmutableSet()));

        usernameToGroupNamesInLowerCase = requestCacheFactory.createRequestCache(USERNAME_TO_GROUP_NAMES_LOWER_CASE_CACHE_KEY,
                (username) -> usernameToGroupNames.get(username).stream()
                        .map(IdentifierUtils::toLowerCase)
                        .collect(CollectorsUtil.toImmutableSet()));
    }

    @Override
    public boolean groupExists(final String groupName) {
        return defaultGroupManager.groupExists(groupName);
    }

    @Override
    public boolean groupExists(@Nonnull final Group group) {
        return defaultGroupManager.groupExists(group);
    }

    @Override
    public Collection<Group> getAllGroups() {
        return defaultGroupManager.getAllGroups();
    }

    @Override
    public Group createGroup(final String groupName) throws OperationNotPermittedException, InvalidGroupException {
        return defaultGroupManager.createGroup(groupName);
    }

    @Override
    public Group getGroup(final String groupName) {
        return defaultGroupManager.getGroup(groupName);
    }

    @Override
    public Group getGroupEvenWhenUnknown(final String groupName) {
        return defaultGroupManager.getGroupEvenWhenUnknown(groupName);
    }

    @Override
    public Group getGroupObject(final String groupName) {
        return defaultGroupManager.getGroupObject(groupName);
    }

    @Override
    public boolean isUserInGroup(final String username, final String groupname) {
        if (username == null || groupname == null) {
            return defaultGroupManager.isUserInGroup(username, groupname);
        }
        final Set<String> groupNamesInLowerCase = usernameToGroupNamesInLowerCase.get(username);
        return groupNamesInLowerCase.contains(IdentifierUtils.toLowerCase(groupname));
    }

    @Override
    public boolean isUserInGroup(final ApplicationUser user, final Group group) {
        if (user == null || group == null) {
            return defaultGroupManager.isUserInGroup(user, group);
        }
        return isUserInGroup(user.getName(), group.getName());
    }

    @Override
    public boolean isUserInGroup(final ApplicationUser user, final String groupName) {
        if (user == null) {
            return defaultGroupManager.isUserInGroup(user, groupName);
        }
        return isUserInGroup(user.getName(), groupName);
    }

    @Override
    public Collection<ApplicationUser> getUsersInGroup(final String groupName) {
        return defaultGroupManager.getUsersInGroup(groupName);
    }

    @Override
    public Collection<ApplicationUser> getUsersInGroup(final String groupName, final Boolean includeInactive) {
        return defaultGroupManager.getUsersInGroup(groupName, includeInactive);
    }

    @Override
    public Page<ApplicationUser> getUsersInGroup(final String groupName, final Boolean includeInactive, final PageRequest pageRequest) {
        return defaultGroupManager.getUsersInGroup(groupName, includeInactive, pageRequest);
    }

    @Override
    public Collection<ApplicationUser> getUsersInGroup(final Group group) {
        return defaultGroupManager.getUsersInGroup(group);
    }

    @Override
    public int getUsersInGroupCount(final Group group) {
        return defaultGroupManager.getUsersInGroupCount(group);
    }

    @Override
    public int getUsersInGroupCount(final String groupName) {
        return defaultGroupManager.getUsersInGroupCount(groupName);
    }

    @Override
    public Collection<String> getNamesOfDirectMembersOfGroups(final Collection<String> groupNames, final int limit) {
        return defaultGroupManager.getNamesOfDirectMembersOfGroups(groupNames, limit);
    }

    @Override
    public Collection<String> filterUsersInAllGroupsDirect(final Collection<String> userNames, final Collection<String> groupNames) {
        return defaultGroupManager.filterUsersInAllGroupsDirect(userNames, groupNames);
    }

    @Override
    public Collection<String> getUserNamesInGroup(final Group group) {
        return defaultGroupManager.getUserNamesInGroup(group);
    }

    @Override
    public Collection<String> getUserNamesInGroups(final Collection<Group> groups) {
        return defaultGroupManager.getUserNamesInGroups(groups);
    }

    @Override
    public Collection<String> getUserNamesInGroup(final String groupName) {
        return defaultGroupManager.getUserNamesInGroup(groupName);
    }

    @Override
    public Collection<ApplicationUser> getDirectUsersInGroup(final Group group) {
        return defaultGroupManager.getDirectUsersInGroup(group);
    }

    @Override
    public Collection<Group> getGroupsForUser(final String userName) {
        return usernameToGroups.get(userName);
    }

    @Override
    public Collection<Group> getGroupsForUser(@Nonnull final ApplicationUser user) {
        return getGroupsForUser(user.getName());
    }

    @Override
    public Collection<String> getGroupNamesForUser(final String userName) {
        return usernameToGroupNames.get(userName);
    }

    @Override
    public Collection<String> getGroupNamesForUser(@Nonnull final ApplicationUser user) {
        return getGroupNamesForUser(user.getName());
    }

    @Override
    public void addUserToGroup(final ApplicationUser user, final Group group)
            throws GroupNotFoundException, UserNotFoundException, OperationNotPermittedException, OperationFailedException {
        defaultGroupManager.addUserToGroup(user, group);
    }

    @Override
    @Deprecated
    public Set<ApplicationUser> getConnectUsers() {
        return ImmutableSet.of();
    }
}
