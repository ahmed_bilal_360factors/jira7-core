package com.atlassian.jira.scheduler;

import com.atlassian.jira.util.thread.JiraThreadLocalUtils;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.caesium.impl.CaesiumSchedulerService;
import com.atlassian.scheduler.caesium.spi.CaesiumSchedulerConfiguration;
import com.atlassian.scheduler.caesium.spi.ClusteredJobDao;
import com.atlassian.scheduler.core.spi.RunDetailsDao;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Wraps the scheduler service so that lifecycle events are passed on to our RunDetailsDao.
 *
 * @since v7.0
 */
public class JiraCaesiumSchedulerService extends CaesiumSchedulerService {
    private static final Logger LOG = Logger.getLogger(JiraCaesiumSchedulerService.class);
    private final RunDetailsDao runDetailsDao;

    public JiraCaesiumSchedulerService(final RunDetailsDao runDetailsDao, final CaesiumSchedulerConfiguration config,
                                       final ClusteredJobDao clusteredJobDao, final JiraParameterMapSerializer jiraParameterMapSerializer) throws SchedulerServiceException {
        super(config, runDetailsDao, clusteredJobDao, jiraParameterMapSerializer);
        this.runDetailsDao = runDetailsDao;
    }

    // Starts the run details DAO first so any fast jobs that trigger during misfire handling can save status
    // successfully, but stops it again if the scheduler fails to start normally.
    @Override
    protected void startImpl() throws SchedulerServiceException {
        boolean ok = false;
        try {
            startRunDetailsDao();
            super.startImpl();
            ok = true;
        } finally {
            if (!ok) {
                stopRunDetailsDao();
            }
        }

    }

    @Override
    protected void standbyImpl() throws SchedulerServiceException {
        super.standbyImpl();
        stopRunDetailsDao();
    }

    @Override
    protected void shutdownImpl() {
        super.shutdownImpl();
        stopRunDetailsDao();
    }

    @Override
    public void preJob() {
        JiraThreadLocalUtils.preCall();
    }

    @Override
    public void postJob() {
        JiraThreadLocalUtils.postCall(LOG, null);
    }

    @SuppressWarnings("CastToConcreteClass")
    private void startRunDetailsDao() {
        if (runDetailsDao instanceof OfBizRunDetailsDao) {
            ((OfBizRunDetailsDao) runDetailsDao).start();
        }
    }

    @SuppressWarnings("CastToConcreteClass")
    private void stopRunDetailsDao() {
        if (runDetailsDao instanceof OfBizRunDetailsDao) {
            ((OfBizRunDetailsDao) runDetailsDao).stop();
        }
    }

}
