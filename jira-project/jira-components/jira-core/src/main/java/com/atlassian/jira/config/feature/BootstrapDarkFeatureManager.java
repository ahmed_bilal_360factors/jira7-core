package com.atlassian.jira.config.feature;

import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.features.EnabledDarkFeatures;
import com.atlassian.sal.api.features.EnabledDarkFeaturesBuilder;
import com.atlassian.sal.api.user.UserKey;

import java.util.Set;

import static java.util.stream.Collectors.toSet;

/**
 * Dark feature manager used during JIRA bootstrap. Effectively only dark features that are set in
 * jira-features.properties or in command line are taken into account.
 */
public class BootstrapDarkFeatureManager implements DarkFeatureManager {

    private final Set<String> coreProperties;

    public BootstrapDarkFeatureManager(final FeaturesLoader featuresLoader) {
        coreProperties = featuresLoader.loadCoreProperties()
                .entrySet().stream()
                .filter(entry -> Boolean.valueOf(entry.getValue().toString()))
                .map(entry -> entry.getKey().toString())
                .collect(toSet());
    }

    @Override
    public boolean isFeatureEnabledForAllUsers(final String featureKey) {
        return coreProperties.contains(featureKey);
    }

    @Override
    public boolean isFeatureEnabledForCurrentUser(final String featureKey) {
        return isFeatureEnabledForAllUsers(featureKey);
    }

    @Override
    public boolean isFeatureEnabledForUser(final UserKey userKey, final String featureKey) {
        return isFeatureEnabledForAllUsers(featureKey);
    }

    @Override
    public boolean canManageFeaturesForAllUsers() {
        return false;
    }

    @Override
    public void enableFeatureForAllUsers(final String s) {
        throw new UnsupportedOperationException("This operation is not supported during setup");
    }

    @Override
    public void disableFeatureForAllUsers(final String s) {
        throw new UnsupportedOperationException("This operation is not supported during setup");
    }

    @Override
    public void enableFeatureForCurrentUser(final String s) {
        throw new UnsupportedOperationException("This operation is not supported during setup");
    }

    @Override
    public void enableFeatureForUser(final UserKey userKey, final String s) {
        throw new UnsupportedOperationException("This operation is not supported during setup");
    }

    @Override
    public void disableFeatureForCurrentUser(final String s) {
        throw new UnsupportedOperationException("This operation is not supported during setup");
    }

    @Override
    public void disableFeatureForUser(final UserKey userKey, final String s) {
        throw new UnsupportedOperationException("This operation is not supported during setup");
    }

    @Override
    public EnabledDarkFeatures getFeaturesEnabledForAllUsers() {
        return new EnabledDarkFeaturesBuilder()
                .unmodifiableFeaturesEnabledForAllUsers(coreProperties)
                .build();
    }

    @Override
    public EnabledDarkFeatures getFeaturesEnabledForCurrentUser() {
        return getFeaturesEnabledForAllUsers();
    }

    @Override
    public EnabledDarkFeatures getFeaturesEnabledForUser(final UserKey userKey) {
        return getFeaturesEnabledForAllUsers();
    }
}
