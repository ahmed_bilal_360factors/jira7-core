package com.atlassian.jira.project.version;

import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.model.querydsl.QVersion;
import com.atlassian.jira.util.NamedPredicates;
import com.google.common.collect.Iterables;

import javax.annotation.Nonnull;
import java.util.Collection;

public class OfBizVersionStore implements VersionStore {
    static final String ENTITY_NAME = "Version";

    private final EntityEngine entityEngine;
    private final DbConnectionManager dbConnectionManager;

    public OfBizVersionStore(final EntityEngine entityEngine, final DbConnectionManager dbConnectionManager) {
        this.entityEngine = entityEngine;
        this.dbConnectionManager = dbConnectionManager;
    }

    @Nonnull
    @Override
    public Iterable<Version> getAllVersions() {
        return entityEngine.selectFrom(Entity.VERSION).findAll().list();
    }

    @Nonnull
    @Override
    public Iterable<Version> getVersionsByName(@Nonnull final String name) {
        return Iterables.filter(getAllVersions(), NamedPredicates.equalsIgnoreCase(name));
    }

    @Nonnull
    @Override
    public Iterable<Version> getVersionsByProject(@Nonnull final Long projectId) {
        return entityEngine.selectFrom(Entity.VERSION).whereEqual("project", projectId).orderBy("sequence");
    }

    @Nonnull
    @Override
    public Version createVersion(@Nonnull final Version version) {
        return entityEngine.createValue(Entity.VERSION, version);
    }

    @Override
    public void storeVersion(@Nonnull final Version version) {
        entityEngine.updateValue(Entity.VERSION, version);
    }

    @Override
    public void storeVersions(@Nonnull final Collection<Version> versions) {
        for (Version version : versions) {
            if (version != null) {
                storeVersion(version);
            }
        }
    }

    @Override
    public Version getVersion(@Nonnull final Long id) {
        return entityEngine.selectFrom(Entity.VERSION).findById(id);
    }

    @Override
    public void deleteVersion(@Nonnull final Version version) {
        entityEngine.removeValue(Entity.VERSION, version.getId());
    }

    @Override
    public void deleteAllVersions(@Nonnull final Long projectId) {
        dbConnectionManager.execute(dbConnection -> dbConnection.delete(QVersion.VERSION).
                where(QVersion.VERSION.project.eq(projectId)).
                execute());
    }
}
