package com.atlassian.jira.web.action.browser;

import com.atlassian.fugue.Option;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.event.mau.MauApplicationKey;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.plugin.userformat.ProfileLinkUserFormat;
import com.atlassian.jira.plugin.userformat.UserFormats;
import com.atlassian.jira.plugin.webfragment.SimpleLinkManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.SimpleLink;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.type.BrowseProjectTypeManager;
import com.atlassian.jira.project.type.ProjectType;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeKeyFormatter;
import com.atlassian.jira.project.type.ProjectTypeManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.jira.web.action.ActionViewData;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.ozymandias.SafePluginPointAccess;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.atlassian.seraph.util.RedirectUtils;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;
import org.ofbiz.core.entity.GenericEntityException;
import webwork.action.ServletActionContext;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.security.Permissions.BROWSE;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;


/**
 * Action for Browse Projects
 */
public class BrowseProjects extends JiraWebActionSupport {
    private final static ObjectMapper OBJECT_MAPPER;

    private final UserProjectHistoryManager projectHistoryManager;
    private final ProjectManager projectManager;
    private final PermissionManager permissionManager;
    private final SimpleLinkManager simpleLinkManager;
    private final WebInterfaceManager webInterfaceManager;
    private final PageBuilderService pageBuilderService;
    private final UserFormats userFormats;
    private final ProjectTypeManager projectTypeManager;
    private final BrowseProjectTypeManager browseProjectTypeManager;

    private static final String ALL = "all";
    private static final String NONE = "none";
    private static final String RECENT = "recent";
    private final Long PROJECT_DEFAULT_AVATAR_ID;

    static {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
        OBJECT_MAPPER = mapper;
    }

    /**
     * List of available categories as {@link com.atlassian.jira.web.action.browser.BrowseProjects.ProjectCategoryBean}
     * objects.
     * <p>
     * We filter out categories with no visible projects.
     * <p>
     * We add a pseudo category for "none" No Category. We add a pseudo category for "Recent Projects" if there are 2 or
     * more categories.
     */
    private final Supplier<List<ProjectCategoryBean>> categories = Suppliers.memoize(new Supplier<List<ProjectCategoryBean>>() {
        @Override
        public List<ProjectCategoryBean> get() {
            List<ProjectCategoryBean> categories = newArrayList();

            final Collection<ProjectCategory> projectCategories = projectManager.getAllProjectCategories();
            for (ProjectCategory projectCategory : projectCategories) {
                if (projectCategory != null) {
                    final Collection<Project> projects = permissionManager.getProjects(BROWSE_PROJECTS, getLoggedInUser(), projectCategory);
                    if (projects != null && !projects.isEmpty()) {
                        categories.add(new ProjectCategoryBean(projectCategory));
                    }
                }
            }

            if (!categories.isEmpty()) {
                //We only add "no category" if there are other categories
                final Collection<Project> noCategoryProjects = permissionManager.getProjects(BROWSE_PROJECTS, getLoggedInUser(), null);
                if (!noCategoryProjects.isEmpty()) {
                    categories.add(new ProjectCategoryBean(getText("browse.projects.none"), getText("browse.projects.none.desc"), NONE));
                }
            }

            if (!recentProjects.get().isEmpty()) {
                categories.add(new ProjectCategoryBean(getText("browse.projects.recent"), getText("browse.projects.recent.desc"), RECENT));
            }

            categories.add(0, new ProjectCategoryBean(getText("browse.projects.category.all"), getText("browse.projects.category.all.desc"), ALL));

            return ImmutableList.copyOf(categories);
        }
    });

    private Supplier<List<ProjectBean>> projects = Suppliers.memoize(new Supplier<List<ProjectBean>>() {
        @Override
        public List<ProjectBean> get() {
            final Collection<Project> allProjects = permissionManager.getProjects(BROWSE_PROJECTS, getLoggedInUser());

            return newArrayList(
                    Iterables.filter(
                            Iterables.transform(allProjects, new Function<Project, ProjectBean>() {
                                @Override
                                public ProjectBean apply(@Nullable final Project project) {
                                    if (project == null) {
                                        return null;
                                    } else {
                                        return new ProjectBean(project, recentProjects.get().contains(project));
                                    }
                                }
                            }),
                            Predicates.notNull()
                    ));
        }
    });

    private final Supplier<Map<String, ProjectTypeBean>> projectTypes = Suppliers.memoize(new Supplier<Map<String, ProjectTypeBean>>() {
        @Override
        public Map<String, ProjectTypeBean> get() {
            return getProjectTypeBeans();
        }

        private Map<String, ProjectTypeBean> getProjectTypeBeans() {
            List<ProjectType> projectTypes = newArrayList(projectTypeManager.getAllProjectTypes());
            projectTypes.add(projectTypeManager.getInaccessibleProjectType());
            return projectTypes.stream()
                    .map(ProjectTypeBean::create)
                    .collect(toMap(ProjectTypeBean::getKey, projectTypeBean -> projectTypeBean));
        }
    });

    private final Supplier<List<Project>> recentProjects = Suppliers.memoize(new Supplier<List<Project>>() {
        @Override
        public List<Project> get() {
            return ImmutableList.copyOf(projectHistoryManager.getProjectHistoryWithPermissionChecks(BROWSE, getLoggedInUser()));

        }
    });

    private String selectedCategory;

    private String selectedProjectType;

    private final Supplier<List<ProjectTypeBean>> availableProjectTypes = Suppliers.memoize(new Supplier<List<ProjectTypeBean>>() {
        @Override
        public List<ProjectTypeBean> get() {
            final List<ProjectTypeBean> allProjectTypes = convertToProjectTypeBeans(browseProjectTypeManager.getAllProjectTypes(getLoggedInUser()));
            allProjectTypes.add(0, ProjectTypeBean.create(ALL, getText("browse.projects.all.project.type")));
            return allProjectTypes;
        }
    });

    private List<ProjectTypeBean> convertToProjectTypeBeans(final List<ProjectType> projectTypes) {
        return projectTypes.stream().map(this::toBean).collect(toList());
    }

    private ProjectTypeBean toBean(ProjectType projectType) {
        return ProjectTypeBean.create(projectType);
    }

    public BrowseProjects(
            final UserProjectHistoryManager projectHistoryManager,
            final ProjectManager projectManager,
            final PermissionManager permissionManager,
            final SimpleLinkManager simpleLinkManager,
            final WebInterfaceManager webInterfaceManager,
            final AvatarManager avatarManager,
            final PageBuilderService pageBuilderService,
            final UserFormats userFormats,
            final ProjectTypeManager projectTypeManager,
            final BrowseProjectTypeManager browseProjectTypeManager) {
        this.projectHistoryManager = projectHistoryManager;
        this.projectManager = projectManager;
        this.permissionManager = permissionManager;
        this.simpleLinkManager = simpleLinkManager;
        this.webInterfaceManager = webInterfaceManager;
        this.pageBuilderService = pageBuilderService;
        this.userFormats = userFormats;
        this.projectTypeManager = projectTypeManager;
        this.browseProjectTypeManager = browseProjectTypeManager;

        PROJECT_DEFAULT_AVATAR_ID = avatarManager.getDefaultAvatarId(Avatar.Type.PROJECT);
    }

    @Override
    protected String doExecute() throws Exception {
        tagMauEventWithApplication(MauApplicationKey.family());

        if (!projects.get().isEmpty()) {
            pageBuilderService.assembler().resources().requireWebResource("jira.webresources:browseprojects");
            pageBuilderService.assembler().resources().requireContext("jira.browse");
            pageBuilderService.assembler().resources().requireContext("jira.browse.projects");

            //prepare data for Backbone application
            pageBuilderService.assembler().data().requireData("com.atlassian.jira.project.browse:categories", getCategoriesJsonable());
            pageBuilderService.assembler().data().requireData("com.atlassian.jira.project.browse:projects", getProjectsJsonable());
            pageBuilderService.assembler().data().requireData("com.atlassian.jira.project.browse:projectTypes", getProjectTypesJsonable());
            pageBuilderService.assembler().data().requireData("com.atlassian.jira.project.browse:selectedCategory", getSelectedCategory());
            pageBuilderService.assembler().data().requireData("com.atlassian.jira.project.browse:availableProjectTypes", getAvailableProjectTypesJsonable());
            pageBuilderService.assembler().data().requireData("com.atlassian.jira.project.browse:selectedProjectType", getSelectedProjectType());

            return super.doExecute();
        } else {
            return ERROR;
        }
    }

    @ActionViewData("success")
    public Collection<SimpleLink> getOperationLinks() {
        return simpleLinkManager.getLinksForSection("system.browse.projects.operations", getLoggedInUser(), getJiraHelper());
    }

    private JiraHelper getJiraHelper() {
        final Map<String, Object> params = Maps.newHashMap();
        return new JiraHelper(ServletActionContext.getRequest(), null, params);
    }

    @ActionViewData("success")
    public String getInfoPanelHtml() {
        final StringBuilder sb = new StringBuilder();
        final List<WebPanelModuleDescriptor> webPanelDescriptors = webInterfaceManager.getDisplayableWebPanelDescriptors("webpanels.browse.projects.info-panels", Collections.<String, Object>emptyMap());
        for (final WebPanelModuleDescriptor webPanelDescriptor : webPanelDescriptors) {
            final Option<String> result = SafePluginPointAccess.call(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    return webPanelDescriptor.getModule().getHtml(Collections.<String, Object>emptyMap());
                }
            });
            if (result.isDefined()) {
                sb.append(result.get());
            }
        }
        return sb.toString();
    }

    @ActionViewData("error")
    public Collection<String> getErrorMessages() {
        final List<String> errors = newArrayList();
        if ("true".equals(getApplicationProperties().getString(APKeys.JIRA_SETUP))) {
            boolean projectsEmpty = projectManager.getProjectObjects().isEmpty();
            if (getLoggedInUser() == null && !projectsEmpty) {
                errors.add(getText("noprojects.notloggedin"));
                String loginLink = "<a href=\"" + RedirectUtils.getLinkLoginURL(getHttpRequest()) + "\">" + getText("common.words.login") + "</a>";
                String logInOrSignup = getText("noprojects.mustfirstlogin", loginLink);
                if (JiraUtils.isPublicMode()) {
                    logInOrSignup += " " + getText("noprojects.signup",
                            "<a href=\"" + getHttpRequest().getContextPath() + "/secure/Signup!default.jspa\">", "</a>");
                }
                errors.add(logInOrSignup);
            } else if (!projectsEmpty) {
                errors.add(getText("noprojects.nopermissions"));
                if (hasGlobalPermission(GlobalPermissionKey.ADMINISTER)) {
                    String linkToProjectsAdminPage = getHttpRequest().getContextPath() + "/secure/admin/ViewPermissionSchemes.jspa";
                    errors.add(getText("noprojects.viewallprojects.message", "<a href=\"" + linkToProjectsAdminPage + "\">", "</a>"));
                } else {
                    errors.add(getText("noprojects.contactadmin.permissions", getAdministratorContactLink()));
                }
            } else {
                errors.add(getText("noprojects"));
                if (hasGlobalPermission(GlobalPermissionKey.ADMINISTER)) {
                    errors.add("<a class=\"add-project-trigger\" href=\"" + getHttpRequest().getContextPath() +
                            "/secure/project/ViewProjects.jspa\">" +
                            getText("noprojects.createprojectnow.link") + "</a>");
                } else {
                    errors.add(getText("noprojects.contactadmin.permissions", getAdministratorContactLink()));
                }
            }
        } else {
            errors.add(getText("noprojects.mustsetupfirst"));
            errors.add(getText("noprojects.createadmintocreateotheradmins"));
            errors.add("<a href=\"" + getHttpRequest().getContextPath() + "/secure/Setup!default.jspa\">" +
                    getText("noprojects.setupjira.link") + "</a>");
        }
        return errors;
    }

    /**
     * Get the currently selected category. If it is not set, "all" will be returned.
     * @return The selected category's id or "all."
     */
    public String getSelectedCategory() {
        return selectedCategory != null ? selectedCategory : ALL;
    }

    /**
     * Get the currently selected project type. If it is not set, "all" will be returned.
     * @return The selected project type or "all."
     */
    public String getSelectedProjectType() {
        return selectedProjectType != null ? selectedProjectType : ALL;
    }

    public void setSelectedProjectType(final String selectedProjectType) {
        this.selectedProjectType = selectedProjectType;
    }

    public Jsonable getCategoriesJsonable() throws GenericEntityException {
        return getJsonable(categories.get());
    }

    public Jsonable getProjectsJsonable() {
        return getJsonable(projects.get());
    }

    public Jsonable getProjectTypesJsonable() {
        return getJsonable(projectTypes.get());
    }

    public Jsonable getAvailableProjectTypesJsonable() {
        return getJsonable(availableProjectTypes.get());
    }

    private Jsonable getJsonable(Object entityToTransform) {
        return writer -> {
            OBJECT_MAPPER.writeValue(writer, entityToTransform);
        };
    }

    private static String convertCategoryToDescription(final ProjectCategory category) {
        final String name = category.getName();
        final String desc = category.getDescription();
        if (StringUtils.isBlank(desc)) {
            return name;
        } else {
            return name + " - " + desc;
        }
    }

    public boolean hasDefaultAvatar(Project project) {
        final Long avatarId = project.getAvatar().getId();
        return avatarId == null || avatarId.equals(PROJECT_DEFAULT_AVATAR_ID);
    }

    public void setSelectedCategory(final String selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public Supplier<List<ProjectTypeBean>> getAvailableProjectTypes() {
        return availableProjectTypes;
    }

    /**
     * Simple bean that contains all needed Project information
     */

    public class ProjectBean {
        private final String key;
        private final String name;
        private final Long id;
        private final boolean hasDefaultAvatar;
        private final boolean recent;
        private final String lead;
        private final String leadProfileLink;
        private final Long projectCategoryId;
        private final String url;
        private final String projectTypeKey;
        private final String projectTypeName;

        public ProjectBean(final Project project, final boolean recent) {
            this(
                    project.getKey(),
                    project.getName(),
                    project.getId(),
                    hasDefaultAvatar(project),
                    project.getProjectLead(),
                    project.getProjectCategoryObject(),
                    recent,
                    project.getUrl(),
                    project.getProjectTypeKey()
            );
        }

        public ProjectBean(
                final String key,
                final String name,
                final Long id,
                final boolean hasDefaultAvatar,
                final ApplicationUser lead,
                final ProjectCategory projectCategory,
                final boolean recent,
                final String url,
                final ProjectTypeKey projectTypeKey) {
            this.key = key;
            this.name = name;
            this.id = id;
            this.hasDefaultAvatar = hasDefaultAvatar;
            this.recent = recent;
            this.projectTypeKey = projectTypeKey.getKey();
            this.projectTypeName = ProjectTypeKeyFormatter.format(projectTypeKey);
            if (lead != null) {
                this.lead = lead.getKey();
                this.leadProfileLink = userFormats.formatter(ProfileLinkUserFormat.TYPE).formatUserkey(this.lead, "");
            } else {
                this.lead = null;
                this.leadProfileLink = null;
            }
            this.url = url;
            if (projectCategory != null) {
                this.projectCategoryId = projectCategory.getId();
            } else {
                this.projectCategoryId = null;
            }
        }

        public String getKey() {
            return key;
        }

        public String getName() {
            return name;
        }

        public Long getId() {
            return id;
        }

        public boolean isHasDefaultAvatar() {
            return hasDefaultAvatar;
        }

        public boolean isRecent() {
            return recent;
        }

        @Nullable
        public String getLead() {
            return lead;
        }

        public String getUrl() {
            return url;
        }

        @Nullable
        public String getLeadProfileLink() {
            return leadProfileLink;
        }

        @Nullable
        public Long getProjectCategoryId() {
            return projectCategoryId;
        }

        public String getProjectTypeKey() {
            return projectTypeKey;
        }

        public String getProjectTypeName() {
            return projectTypeName;
        }
    }

    /**
     * Simple bean that contains Project category information and its containing projects as GVS.
     */
    public class ProjectCategoryBean {
        private final String name;
        private final String description;
        private final String id;

        public ProjectCategoryBean(ProjectCategory category) {
            this(category.getName(), convertCategoryToDescription(category), String.valueOf(category.getId()));
        }

        public ProjectCategoryBean(final String name, final String description, final String id) {
            this.name = name;
            this.description = description;
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public String getId() {
            return id;
        }
    }

}
