package com.atlassian.jira.cluster.zdu;

import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.upgrade.PluginUpgradeService;
import com.atlassian.jira.upgrade.UpgradeScheduler;

/**
 * @since v7.3
 */
@EventComponent
public class JiraUpgradeApprovedListener {
    private static final int UPGRADES_DELAY_IN_MINUTES = 0;
    private final UpgradeScheduler upgradeScheduler;
    private final PluginUpgradeService pluginUpgradeService;
    private final ClusterUpgradeStateManager zdu;


    public JiraUpgradeApprovedListener(
            final UpgradeScheduler upgradeScheduler,
            final ClusterUpgradeStateManager zdu,
            final PluginUpgradeService pluginUpgradeService
    ) {
        this.upgradeScheduler = upgradeScheduler;
        this.zdu = zdu;
        this.pluginUpgradeService = pluginUpgradeService;
    }

    @EventListener
    public void runDelayedUpgrades(final JiraUpgradeApprovedEvent event) {
        zdu.runUpgrade();
        upgradeScheduler.scheduleUpgrades(UPGRADES_DELAY_IN_MINUTES);
        pluginUpgradeService.upgradePlugins();
    }
}
