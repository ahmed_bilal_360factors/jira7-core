package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;
import com.atlassian.jira.model.querydsl.QNodeAssociation;
import com.atlassian.jira.model.querydsl.QVersion;
import com.atlassian.jira.upgrade.AbstractDelayableUpgradeTask;
import com.querydsl.sql.SQLExpressions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumSet;

/**
 * Deletes the version association for any issues that have a reference to a non-existent project version.
 *
 * @since v7.0.1
 */
public class UpgradeTask_Build71001 extends AbstractDelayableUpgradeTask {
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask_Build71001.class);
    private final DbConnectionManager dbConnectionManager;

    public UpgradeTask_Build71001(final DbConnectionManager dbConnectionManager) {
        this.dbConnectionManager = dbConnectionManager;
    }

    @Override
    public int getBuildNumber() {
        return 71001;
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        // This upgrade task only cleans up invalid data.
        return false;
    }

    @Override
    public String getShortDescription() {
        return "Deleting invalid version associations.";
    }

    /**
     * Perform an upgrade task that will delete any association from the 'NodeAssociation' table that
     * is between an issue and a non-existent version.
     * This is necessary because of a bug--reported in JRA-22351--where versions were removed from
     * the database before the issue's version associations had a chance to be deleted. This caused
     * JQL searches like 'fixVersion is EMPTY' to be incorrect.
     */
    @Override
    public void doUpgrade(boolean setupMode) throws Exception {
        final long associationsDeleted = dbConnectionManager.executeQuery(callback -> {
            // delete from nodeassociation
            // where source_node_entity = 'Issue'
            //    and sink_node_entity = 'Version'
            //    and sink_node_id not in (select id from projectversion);

            final QNodeAssociation na = new QNodeAssociation("na");
            final QVersion v = new QVersion("v");

            long deletedCount = callback.delete(na)
                    .where(na.sourceNodeEntity.eq(Entity.Name.ISSUE)
                            .and(na.sinkNodeEntity.eq(Entity.Name.VERSION))
                            .and(na.sinkNodeId.notIn(SQLExpressions
                                    .select(v.id)
                                    .from(v))))
                    .execute();

            log.info("Deleted {} invalid issue to version associations.", deletedCount);
            return deletedCount;
        });

        if (associationsDeleted > 0) {
            getReindexRequestService().requestReindex(ReindexRequestType.DELAYED,
                    EnumSet.of(AffectedIndex.ISSUE),
                    EnumSet.noneOf(SharedEntityType.class));
        }
    }
}
