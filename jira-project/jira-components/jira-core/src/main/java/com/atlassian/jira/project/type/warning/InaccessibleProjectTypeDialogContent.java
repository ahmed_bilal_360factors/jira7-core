package com.atlassian.jira.project.type.warning;

/**
 * Class encapsulating the content that is rendered on the dialog attached to the project type warnings displayed on several pages.
 */
public class InaccessibleProjectTypeDialogContent {
    private final String title;
    private final String firstParagraph;
    private final String secondParagraph;
    private final String callToActionText;
    private final long projectId;

    public InaccessibleProjectTypeDialogContent(
            String title,
            String firstParagraph,
            String secondParagraph,
            String callToActionText,
            long projectId) {
        this.title = title;
        this.firstParagraph = firstParagraph;
        this.secondParagraph = secondParagraph;
        this.callToActionText = callToActionText;
        this.projectId = projectId;
    }

    public String getTitle() {
        return title;
    }

    public String getFirstParagraph() {
        return firstParagraph;
    }

    public String getSecondParagraph() {
        return secondParagraph;
    }

    public String getCallToActionText() {
        return callToActionText;
    }

    public long getProjectId() {
        return projectId;
    }
}
