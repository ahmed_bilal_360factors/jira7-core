package com.atlassian.jira.mail.util;

import javax.mail.BodyPart;

/**
 * A representation of an inlined email image. Use this to construct a mail {@link BodyPart} and provide a unique name
 * that can be used to identify it, from other attachments.
 */
public interface MailAttachment {
    /**
     * Create a {@link BodyPart} from this mail attachment
     *
     * @return The BodyPart
     */
    BodyPart buildBodyPart();

    /**
     * A unique string that will be used to create the Content ID for this attachment
     *
     * @return The unique identifying name
     */
    String getUniqueName();
}