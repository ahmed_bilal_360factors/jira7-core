package com.atlassian.jira.startup;

import com.atlassian.sal.api.message.LocaleResolver;
import com.atlassian.sal.api.user.UserKey;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Set;

public class BootstrapLocaleResolver implements LocaleResolver {
    private final com.atlassian.jira.config.properties.ApplicationProperties applicationProperties;

    public BootstrapLocaleResolver(final com.atlassian.jira.config.properties.ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    @Override
    public Locale getLocale(final HttpServletRequest httpServletRequest) {
        return getDefaultLocale();
    }

    @Override
    public Locale getLocale() {
        return getDefaultLocale();
    }

    @Override
    public Locale getLocale(@Nullable final UserKey userKey) {
        return getDefaultLocale();
    }

    @Override
    public Set<Locale> getSupportedLocales() {
        return ImmutableSet.of(getDefaultLocale());
    }

    private Locale getDefaultLocale() {
        return applicationProperties.getDefaultLocale();
    }
}
