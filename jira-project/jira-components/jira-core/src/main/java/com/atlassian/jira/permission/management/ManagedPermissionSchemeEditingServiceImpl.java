package com.atlassian.jira.permission.management;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.permission.JiraPermissionHolderType;
import com.atlassian.jira.permission.PermissionGrantInput;
import com.atlassian.jira.permission.PermissionGrantValidator;
import com.atlassian.jira.permission.PermissionHolder;
import com.atlassian.jira.permission.PermissionSchemeEntry;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.permission.PermissionTypeManager;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.permission.management.beans.GrantToPermissionInputBean;
import com.atlassian.jira.permission.management.beans.PermissionsInputBean;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.security.type.SecurityType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserKeyService;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Objects;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.HashMap;
import java.util.Map;

/**
 * This helper can add new security types instances (permission grants) to a permission scheme and permissions
 */
public class ManagedPermissionSchemeEditingServiceImpl implements ManagedPermissionSchemeEditingService {
    private final PermissionSchemeManager permissionSchemeManager;
    private final I18nHelper i18nHelper;
    private final PermissionGrantValidator permissionGrantValidator;
    private final PermissionTypeManager permissionTypeManager;
    private final PermissionManager permissionManager;
    private final UserKeyService userKeyService;

    public ManagedPermissionSchemeEditingServiceImpl(
            final PermissionSchemeManager permissionSchemeManager,
            final I18nHelper i18nHelper,
            final PermissionGrantValidator permissionGrantValidator,
            final PermissionTypeManager permissionTypeManager,
            final PermissionManager permissionManager,
            final UserKeyService userKeyService) {
        this.permissionSchemeManager = permissionSchemeManager;
        this.i18nHelper = i18nHelper;
        this.permissionGrantValidator = permissionGrantValidator;
        this.permissionTypeManager = permissionTypeManager;
        this.permissionManager = permissionManager;
        this.userKeyService = userKeyService;
    }

    @Override
    public ErrorCollection validateAddPermissions(final ApplicationUser applicationUser, final PermissionsInputBean inputBean) {
        SimpleErrorCollection errorCollection = new SimpleErrorCollection();

        if (inputBean.getPermissionKeys() == null || inputBean.getPermissionKeys().isEmpty()) {
            errorCollection.addErrorMessage(i18nHelper.getText(ErrorMessages.MUST_SELECT_PERMISSION_ERROR.getKey()));
            return errorCollection;
        }

        if (inputBean.getGrants() == null || inputBean.getGrants().isEmpty()) {
            errorCollection.addErrorMessage(i18nHelper.getText(ErrorMessages.MUST_SELECT_GRANT_TYPE_ERROR.getKey()));
            return errorCollection;
        }

        final JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(applicationUser, errorCollection);
        for (String permissionKey : inputBean.getPermissionKeys()) {
            for (GrantToPermissionInputBean grant : inputBean.getGrants()) {
                validatePermissionAndGrantCombination(errorCollection, applicationUser, new ProjectPermissionKey(permissionKey), grant, jiraServiceContext);
            }
        }

        return errorCollection;
    }

    private void validatePermissionAndGrantCombination(final SimpleErrorCollection errorCollection, final ApplicationUser applicationUser,
                                                       final ProjectPermissionKey projectPermissionKey, final GrantToPermissionInputBean grant,
                                                       final JiraServiceContext jiraServiceContext) {
        final Option<JiraPermissionHolderType> permissionHolderType = JiraPermissionHolderType.fromKey(grant.getSecurityType(), grant.getValue().getOrNull());

        // validations performed for any of the built-in permission types
        if (permissionHolderType.isDefined()) {
            final PermissionHolder permissionHolder = PermissionHolder.holder(permissionHolderType.get(), grant.getValue().getOrNull());
            final PermissionGrantInput permissionGrantInput = PermissionGrantInput.newGrant(permissionHolder, projectPermissionKey);
            errorCollection.addErrorCollection(permissionGrantValidator.validateGrant(applicationUser, permissionGrantInput));
            return;
        }

        // validations for pluggable permission types
        final Option<SecurityType> securityTypeOption = Option.option(permissionTypeManager.getSchemeType(grant.getSecurityType()));
        if (securityTypeOption.isEmpty()) {
            errorCollection.addErrorMessage(i18nHelper.getText(ErrorMessages.NONEXISTENT_PERMISSION.getKey(), grant.getSecurityType()));
        } else {
            final SecurityType securityType = securityTypeOption.get();
            final Map<String, String> parameters = new HashMap<String, String>() {{
                put(grant.getSecurityType(), grant.getValue().getOrNull());
            }};
            // Let the scheme type do any specific validation.
            // This will add an error messages to the errorCollection via the JiraServiceContext
            securityType.doValidation(grant.getSecurityType(), parameters, jiraServiceContext);

            if (!securityType.isValidForPermission(projectPermissionKey)) {
                final ProjectPermission projectPermission = permissionManager.getProjectPermission(projectPermissionKey).get();
                final String permName = i18nHelper.getText(projectPermission.getNameI18nKey());
                errorCollection.addErrorMessage(i18nHelper.getText(ErrorMessages.INVALID_PERMISSION_PLUS_GRANT_COMBINATION_ERROR.getKey(), permName, securityType.getDisplayName()));
            }
        }
    }

    @Override
    public boolean addNewSecurityTypes(final Scheme schemeObject, final PermissionsInputBean inputBean) {
        boolean successfulAdd = true;
        for (String permissionKey : inputBean.getPermissionKeys()) {
            for (GrantToPermissionInputBean grant : inputBean.getGrants()) {
                ProjectPermissionKey projectPermissionKey = new ProjectPermissionKey(permissionKey);
                String securityType = grant.getSecurityType();
                String value = grant.getValue().getOrNull();
                try {
                    if (!permissionExists(schemeObject, projectPermissionKey, securityType, value)) {
                        // keep it marked as successful only if all inserts returned true
                        successfulAdd = addSecurityTypeToPermission(schemeObject.getId(), permissionKey, grant) && successfulAdd;
                    }
                } catch (GenericEntityException e) {
                    throw new DataAccessException(e);
                }
            }
        }
        return successfulAdd;
    }

    private boolean addSecurityTypeToPermission(final Long permissionSchemeId, final String permissionKey, final GrantToPermissionInputBean grant) throws GenericEntityException {
        // We have no choice as there is not a replacement
        @SuppressWarnings("deprecation")
        GenericValue schemeAsGenericValue = permissionSchemeManager.getScheme(permissionSchemeId);
        SchemeEntity entity;
        if ((JiraPermissionHolderType.USER.getKey().equals(grant.getSecurityType()))) {
            // Here we create the permission scheme entry with userKey instead of userName,
            // the userKeyService maps between the internal JIRA userKey and the userName.
            // This is because the userName of a user account can change (internally and externally)
            // and the permission should still be valid. See HELIX-164 for more detail
            final String userKey = userKeyService.getKeyForUsername(grant.getValue().getOrNull());
            entity = schemeEntity(grant.getSecurityType(), userKey, permissionSchemeId, permissionKey);
        } else {
            entity = schemeEntity(grant, permissionSchemeId, permissionKey);
        }
        final GenericValue schemeEntity = permissionSchemeManager.createSchemeEntity(schemeAsGenericValue, entity);
        return schemeEntity != null;
    }

    private boolean permissionExists(Scheme schemeObject, ProjectPermissionKey permissionKey, String type, String parameter)
            throws GenericEntityException {
        for (PermissionSchemeEntry permissionSchemeEntry : permissionSchemeManager.getPermissionSchemeEntries(schemeObject, permissionKey)) {
            if (permissionSchemeEntry.getType().equals(type) && Objects.equal(parameter, permissionSchemeEntry.getParameter())) {
                return true;
            }
        }
        return false;
    }

    private SchemeEntity schemeEntity(final GrantToPermissionInputBean permissionGrant, final Long permissionSchemeId, final String permissionKey) {
        return schemeEntity(permissionGrant.getSecurityType(), permissionGrant.getValue().getOrNull(), permissionSchemeId, permissionKey);
    }

    private SchemeEntity schemeEntity(final String type, final String parameter, final Long permissionSchemeId, final String permissionKey) {
        return new SchemeEntity(
                null,
                type,
                parameter,
                permissionKey,
                null,
                permissionSchemeId
        );
    }

    @VisibleForTesting
    protected enum ErrorMessages {
        MUST_SELECT_PERMISSION_ERROR("admin.permissions.errors.mustselectpermission"),
        MUST_SELECT_GRANT_TYPE_ERROR("admin.permissions.errors.mustselecttype"),
        INVALID_PERMISSION_PLUS_GRANT_COMBINATION_ERROR("admin.permissions.errors.invalid.combination"),
        NONEXISTENT_PERMISSION("admin.errors.permissions.inexistent.permission");

        private final String key;

        ErrorMessages(final String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }

}
