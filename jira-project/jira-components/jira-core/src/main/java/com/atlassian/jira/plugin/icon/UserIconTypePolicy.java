package com.atlassian.jira.plugin.icon;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.icon.IconOwningObjectId;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;

/**
 * Implements an {@link IconTypePolicy} for icons for {@link com.atlassian.jira.issue.issuetype.IssueType}s.
 */
public class UserIconTypePolicy implements IconTypePolicy {
    private final GlobalPermissionManager globalPermissionManager;
    private final PermissionManager permissionManager;

    public UserIconTypePolicy(
            GlobalPermissionManager globalPermissionManager, PermissionManager permissionManager) {
        this.globalPermissionManager = globalPermissionManager;
        this.permissionManager = permissionManager;
    }

    @Override
    public boolean userCanView(@Nullable ApplicationUser remoteUser, @Nonnull Avatar icon) {
        String owningObjectID = icon.getOwner();

        if (owningObjectID == null) {
            return true;
        } else if (remoteUser == null) {
            return false;
        } else if (icon.getIconType().equals(IconType.USER_ICON_TYPE)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean userCanDelete(@Nullable ApplicationUser remoteUser, @Nonnull Avatar icon) {
        return userCanCreateFor(remoteUser, new IconOwningObjectId(icon.getOwner()));
    }

    @Override
    public boolean userCanCreateFor(@Nullable ApplicationUser remoteUser, @Nonnull IconOwningObjectId owningObjectId) {
        String user = owningObjectId.getId();

        if (user == null) {
            return false;
        } else {
            ApplicationUser owner = ApplicationUsers.byKey(user);
            return ((owner != null && owner.equals(remoteUser)) || // You can edit your own icon
                    globalPermissionManager.hasPermission(ADMINISTER, remoteUser)); // admin can edit someone else's icon
        }
    }
}
