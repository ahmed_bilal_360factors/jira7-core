package com.atlassian.jira.imports.project.core;

import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.util.AoImportTemporaryFiles;
import com.atlassian.jira.imports.project.util.ProjectImportTemporaryFiles;

import java.io.File;

/**
 * @since v3.13
 */
public class ProjectImportDataImpl implements ProjectImportData {
    private final ProjectImportTemporaryFiles temporaryFiles;
    private final AoImportTemporaryFiles aoImportTemporaryFiles;
    private final int issueCount;
    private final int fileAttachmentCount;
    private final int changeItemEntityCount;
    private final int nonIssueEntityCount;
    private final int issueRelatedEntitiesCount;
    private final int customFieldValuesCount;
    private final ProjectImportMapper projectImportMapper;
    private int validAttachmentCount;

    public ProjectImportDataImpl(final ProjectImportMapper projectImportMapper, final ProjectImportTemporaryFiles temporaryFiles,
                                 final AoImportTemporaryFiles aoImportTemporaryFiles, final int issueCount, final int customFieldValuesCount,
                                 final int issueRelatedEntitiesCount, final int fileAttachmentCount, final int changeItemEntityCount, final int nonIssueEntityCount) {
        this.temporaryFiles = temporaryFiles;
        this.aoImportTemporaryFiles = aoImportTemporaryFiles;
        this.issueCount = issueCount;
        this.customFieldValuesCount = customFieldValuesCount;
        this.issueRelatedEntitiesCount = issueRelatedEntitiesCount;
        this.projectImportMapper = projectImportMapper;
        this.fileAttachmentCount = fileAttachmentCount;
        this.changeItemEntityCount = changeItemEntityCount;
        this.nonIssueEntityCount = nonIssueEntityCount;
    }

    public ProjectImportTemporaryFiles getTemporaryFiles() {
        return temporaryFiles;
    }

    @Override
    public AoImportTemporaryFiles getTemporaryAoFiles() {
        return aoImportTemporaryFiles;
    }

    public String getPathToEntityXml(String entityName) {
        File entityXmlFile = temporaryFiles.getEntityXmlFile(entityName);
        return entityXmlFile == null ? null : entityXmlFile.getAbsolutePath();
    }

    public int getIssueEntityCount() {
        return issueCount;
    }

    public int getFileAttachmentEntityCount() {
        return fileAttachmentCount;
    }

    public int getIssueRelatedEntityCount() {
        return issueRelatedEntitiesCount;
    }

    public int getCustomFieldValuesEntityCount() {
        return customFieldValuesCount;
    }

    public int getChangeItemEntityCount() {
        return changeItemEntityCount;
    }

    public ProjectImportMapper getProjectImportMapper() {
        return projectImportMapper;
    }

    public void setValidAttachmentsCount(final int validAttachmentCount) {
        this.validAttachmentCount = validAttachmentCount;
    }

    public int getValidAttachmentsCount() {
        return validAttachmentCount;
    }

    @Override
    public int getNonIssueEntityCount() {
        return nonIssueEntityCount;
    }

    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if ((o == null) || (getClass() != o.getClass())) {
            return false;
        }

        final ProjectImportDataImpl that = (ProjectImportDataImpl) o;

        if (customFieldValuesCount != that.customFieldValuesCount) {
            return false;
        }
        if (fileAttachmentCount != that.fileAttachmentCount) {
            return false;
        }
        if (issueCount != that.issueCount) {
            return false;
        }
        if (issueRelatedEntitiesCount != that.issueRelatedEntitiesCount) {
            return false;
        }
        if (projectImportMapper != null ? !projectImportMapper.equals(that.projectImportMapper) : that.projectImportMapper != null) {
            return false;
        }
        if (temporaryFiles != null ? !temporaryFiles.equals(that.temporaryFiles) : that.temporaryFiles != null) {
            return false;
        }

        return true;
    }

    public int hashCode() {
        int result;
        result = (temporaryFiles != null ? temporaryFiles.hashCode() : 0);
        result = 31 * result + issueCount;
        result = 31 * result + fileAttachmentCount;
        result = 31 * result + issueRelatedEntitiesCount;
        result = 31 * result + customFieldValuesCount;
        result = 31 * result + (projectImportMapper != null ? projectImportMapper.hashCode() : 0);
        return result;
    }
}
