package com.atlassian.jira.project.template.hook;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.unmodifiableList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ScreenTabTemplateImpl implements ScreenTabTemplate {
    private final String name;
    private final List<String> fields;

    public ScreenTabTemplateImpl(
            @JsonProperty("name") String name,
            @JsonProperty("fields") List<String> fields) {
        this.name = checkNotNull(name);
        this.fields = checkNotNull(fields);
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public List<String> fields() {
        return unmodifiableList(fields);
    }
}
