package com.atlassian.jira.plugin.util;

import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.annotation.Nonnull;

/**
 * This is a glorified list of plugin keys that code can use to track what plugins are involved in it's caches
 * <p>
 * On plugin events it can then ask if the event related to one of the tracked plugins
 * <p>
 * This uses a {@link java.util.concurrent.CopyOnWriteArraySet} under the covers to ensure that the list is as safe as
 * possible.  The assumption is that the reads and writes will be of low volume and the total number of plugins tracked
 * will be smallish.  In other words its anticipated that it will just work!
 *
 * @since v6.2.3
 */
public interface PluginsTracker {

    /**
     * Returns true if the plugin is being tracked
     *
     * @param plugin the plugin in play
     * @return true if the underlying plugin is being tracked
     */
    boolean isPluginInvolved(Plugin plugin);

    /**
     * Returns true if the plugin that this ModuleDescriptor belongs to is being tracked
     *
     * @param moduleDescriptor the ModuleDescriptor of the plugin in play
     * @return true if the underlying plugin is being tracked
     */
    boolean isPluginInvolved(ModuleDescriptor moduleDescriptor);

    /**
     * Return a hash that represents all the plugins in this tracker. This hash should change if the list of plugins
     * being tracked changes.
     * <p>
     * It is used to help generate a cache busting WebResource URL prefix. That is, if this hash changes then it is
     * likely that the URLs to all of JIRA's WebResources will change which will force all browsers to request all
     * resources again.
     *
     * @return Return a hash that represents all the plugins in this tracker.
     */
    String getStateHashCode();

    /**
     * A simple class that contains plugin key and pluginVersion.
     */
    class PluginInfo implements Comparable<PluginInfo> {
        private final String pluginKey;
        private final String pluginVersion;

        public PluginInfo(final String pluginKey, final String pluginVersion) {
            this.pluginKey = Assertions.notNull("pluginKey", pluginKey);
            this.pluginVersion = Assertions.notNull("pluginVersion", pluginVersion);
        }

        public String getPluginKey() {
            return pluginKey;
        }

        public String getPluginVersion() {
            return pluginVersion;
        }

        @Override
        public int compareTo(@Nonnull final PluginInfo that) {
            int rc = pluginKey.compareTo(that.pluginKey);
            if (rc == 0) {
                rc = pluginVersion.compareTo(that.pluginVersion);
            }
            return rc;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final PluginInfo that = (PluginInfo) o;

            return pluginKey.equals(that.pluginKey) && pluginVersion.equals(that.pluginVersion);

        }

        @Override
        public int hashCode() {
            int result = pluginKey.hashCode();
            result = 31 * result + pluginVersion.hashCode();
            return result;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }
    }
}
