package com.atlassian.jira.model.querydsl;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Generated;

/**
 * Data Transfer Object for the ProductLicense entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 *
 * @see QProductLicense
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class ProductLicenseDTO implements DTO {
    private final Long id;
    private final String license;

    public Long getId() {
        return id;
    }

    public String getLicense() {
        return license;
    }

    public ProductLicenseDTO(Long id, String license) {
        this.id = id;
        this.license = license;
    }

    /**
     * Creates a GenericValue object from the values in this Data Transfer Object.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
     * @return a GenericValue object constructed from the values in this Data Transfer Object.
     */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator) {
        return ofBizDelegator.makeValue("ProductLicense", new FieldMap()
                .add("id", id)
                .add("license", license)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */
    public static ProductLicenseDTO fromGenericValue(GenericValue gv) {
        return new ProductLicenseDTO(
                gv.getLong("id"),
                gv.getString("license")
        );
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(ProductLicenseDTO productLicenseDTO) {
        return new Builder(productLicenseDTO);
    }

    public static class Builder {
        private Long id;
        private String license;

        public Builder() {
        }

        public Builder(ProductLicenseDTO productLicenseDTO) {
            this.id = productLicenseDTO.id;
            this.license = productLicenseDTO.license;
        }

        public ProductLicenseDTO build() {
            return new ProductLicenseDTO(id, license);
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }
        public Builder license(String license) {
            this.license = license;
            return this;
        }
    }
}