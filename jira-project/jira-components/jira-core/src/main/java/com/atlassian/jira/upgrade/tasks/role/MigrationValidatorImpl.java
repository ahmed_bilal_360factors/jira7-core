package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

import static com.atlassian.jira.application.ApplicationKeys.CORE;
import static com.atlassian.jira.application.ApplicationKeys.SERVICE_DESK;
import static com.atlassian.jira.application.ApplicationKeys.SOFTWARE;
import static com.atlassian.jira.upgrade.tasks.role.LicenseUtils.ServiceDeskLicenseType;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static java.util.stream.Collectors.joining;

/**
 * Validate role migration before committing them to the database
 *
 * @since v7.0
 */
public class MigrationValidatorImpl extends MigrationValidator {
    private static final Logger log = LoggerFactory.getLogger(MigrationValidatorImpl.class);

    private final GlobalPermissionDao globalPermissionDao;
    private final Jira6xServiceDeskLicenseProvider licenseSupplier;
    private final MigrationGroupService migrationGroupService;


    MigrationValidatorImpl(GlobalPermissionDao globalPermissionDao,
                           Jira6xServiceDeskLicenseProvider licenseSupplier, MigrationGroupService migrationGroupService) {
        this.globalPermissionDao = notNull("globalPermissionDao", globalPermissionDao);
        this.licenseSupplier = licenseSupplier;
        this.migrationGroupService = migrationGroupService;
    }

    @Override
    void validate(MigrationState original, MigrationState resulting) {
        notNull("original", original);
        notNull("resulting", resulting);
        verifyAdministrators(original, resulting);
        verifyUsersFor(original, resulting, CORE, SOFTWARE);
        validateServiceDesk(original, resulting);
    }

    private void validateServiceDesk(MigrationState original, MigrationState resulting) {
        final Option<ServiceDeskLicenseType> agentBasedPricing = getServiceDeskMigrationType();
        if (agentBasedPricing.isEmpty()) {
            validateServiceDeskRoleHasNoEscalations(original, resulting);
        } else {
            switch (agentBasedPricing.get()) {
                case AgentBasedPricing:
                    validateServiceDeskAgents(original, resulting);
                    break;
                case TierBasedPricing:
                    verifyUsersFor(original, resulting, SERVICE_DESK);
                    break;
                default:
                    throw new MigrationValidationFailedException("Unable to determine migration type.");
            }
        }
    }

    private void verifyAdministrators(MigrationState original, MigrationState resulting) {
        //retrieve the default groups that existed before the migration started
        Set<Group> invariantDefaultGroups = original.applicationRoles().asMap().values().stream()
                .flatMap(role -> role.defaultGroups().stream())
                .collect(CollectorsUtil.toImmutableSet());

        Set<Group> defaultGroups = resulting.applicationRoles().asMap().values().stream()
                .flatMap(role -> role.defaultGroups().stream())
                .collect(CollectorsUtil.toImmutableSet());

        // identify the default groups that were set by the migration tasks
        Set<Group> newDefaultGroups = Sets.difference(defaultGroups, invariantDefaultGroups);

        Set<Group> defaultAdminGroups = Sets.intersection(globalPermissionDao.groupsWithAdminPermission(), newDefaultGroups);
        if (!defaultAdminGroups.isEmpty()) {
            Set<String> groupNames = defaultAdminGroups.stream().map(Group::getName).
                    collect(CollectorsUtil.toImmutableSet());
            log.error("Administrator groups have become default: " + String.join(", ", groupNames));
            // there is at least one group that can give admin access to all new users for that app
            throw new MigrationValidationFailedException("Administrator groups have become default group in application role:"
                    + String.join(", ", groupNames));
        }

        // for dropped admin groups we are not worried about the initial resulting as it may have already groups that were
        // migrated before hand
        Set<Group> applicationGroups = resulting.applicationRoles().asMap().values().stream()
                .flatMap(role -> role.groups().stream())
                .collect(CollectorsUtil.toImmutableSet());
        Set<Group> droppedAdminGroups = Sets.difference(globalPermissionDao.groupsWithAdminPermission(), applicationGroups);
        if (!droppedAdminGroups.isEmpty()) {
            log.warn("The following administrator groups were not migrated properly: "
                    + droppedAdminGroups.stream().map(Group::getName).collect(joining(", ")));
        }
    }

    private void verifyUsersFor(MigrationState original, MigrationState resulting, ApplicationKey... keys) {
        Set<Group> loginGroups = Sets.union(globalPermissionDao.groupsWithUsePermission(),
                globalPermissionDao.groupsWithAdminPermission());
        Set<Group> default6xGroups = Sets.difference(loginGroups, globalPermissionDao.groupsWithAdminPermission());

        // get the applications to verify (also checks that the applications are present)
        // for each role
        for (ApplicationKey applicationKey : keys) {
            final ApplicationRole originalApp = original.applicationRoles().get(applicationKey)
                    .getOrElse(ApplicationRole.forKey(applicationKey));
            final ApplicationRole resultingApp = resulting.applicationRoles().get(applicationKey)
                    .getOrElse(ApplicationRole.forKey(applicationKey));

            // the following two verification steps will only log extra groups.
            // In case there are groups that are dropped from to the application roles
            // (i.e. they were in admin and/or use 6.x groups but not in application Group or Default Group) means that
            // 1. we are not going to over-charge
            // 2. users are not going receive escalated privileges
            // the only problem will be that they can not login, something that can be simply rectified by administrators
            verifyUseGroupsWithoutAdminMigratedToDefaultGroups(default6xGroups, applicationKey, resultingApp);
            verifyUseAndAdminGroupsMigratedToApplicationGroups(loginGroups, applicationKey, resultingApp);

            // When there are more groups in application roles' Groups or Default Groups then we may have a situation
            // of user receiving escalation of privileges. That will result in an exception
            validateDefaultGroupsNotExceeding6xGroups(default6xGroups, applicationKey, originalApp, resultingApp);
            validateApplicationGroupsNotExceeding6xGroups(loginGroups, applicationKey, originalApp, resultingApp);
        }
    }

    private void validateServiceDeskAgents(MigrationState original, MigrationState resulting) {
        // login groups may have administrator groups inside, where default groups do not have admin.
        Set<Group> sdLoginGroups = globalPermissionDao.groupsWithSdAgentPermission();
        Set<Group> sdDefaultGroups = Sets.difference(sdLoginGroups, globalPermissionDao.groupsWithAdminPermission());
        final ApplicationKey sdAppKey = SERVICE_DESK;
        final ApplicationRole resultingSdApp = resulting.applicationRoles().get(sdAppKey)
                .getOrElse(ApplicationRole.forKey(sdAppKey));
        final ApplicationRole originalSdApp = original.applicationRoles().get(sdAppKey)
                .getOrElse(ApplicationRole.forKey(sdAppKey));

        // perform the same tests for the groups as in user migration validation but with the service desk Agent groups
        verifyUseGroupsWithoutAdminMigratedToDefaultGroups(sdDefaultGroups, sdAppKey, resultingSdApp);
        verifyUseAndAdminGroupsMigratedToApplicationGroups(sdLoginGroups, sdAppKey, resultingSdApp);
        validateDefaultGroupsNotExceeding6xGroups(sdDefaultGroups, sdAppKey, originalSdApp, resultingSdApp);
        validateApplicationGroupsNotExceeding6xGroups(sdLoginGroups, sdAppKey, originalSdApp, resultingSdApp);

        //validate the groups through the users.
        // ensure that all SD users have either Admin or Use access.
        // When a user is identified not to have Admin or Use, the group the usere belongs to will be in the exception
        // note that app groups always include the default groups
        for (Group sdGroup : Sets.difference(resultingSdApp.groups(), originalSdApp.groups())) {
            for (UserWithPermissions user : migrationGroupService.getUsersInGroup(sdGroup)) {
                if (!user.canLogin()) {
                    String message = "Service Desk agent: " + user.getUser().getName() + " is part of "
                            + sdGroup.getName() + " group";
                    log.error(message);
                    throw new MigrationValidationFailedException(message);
                }
            }
        }
        // log all users that are not migrated properly to SD due to USE or Admin permissions
        for (Group sdGroup : Sets.difference(sdLoginGroups, resultingSdApp.groups())) {
            String message = "ABP group " + sdGroup.getName() + " did not migrate to Service Desk with following Users:"
                    + migrationGroupService.getUsersInGroup(sdGroup).stream()
                    .map(user -> user.getUser().getName()).collect(joining(",", " ", "."));
            log.warn(message);
        }
    }

    private void validateApplicationGroupsNotExceeding6xGroups(Set<Group> loginGroups, ApplicationKey applicationKey,
                                                               ApplicationRole originalApp, ApplicationRole resultingApp) {
        // get all the groups role and check that they don't have more than 6.x groups with USE and/or Admin
        //   -> roleGroups - role.invariantGroups > loginGroups - added groups -> MigrationException
        final Set<Group> addedAppGroup = Sets.difference(
                Sets.difference(resultingApp.groups(), originalApp.groups()), loginGroups);
        if (!addedAppGroup.isEmpty()) {
            String errorMessage = ("Following application groups for " + applicationKey.value()
                    + " did not have USE or ADMINISTRATOR permission: "
                    + addedAppGroup.stream().map(Group::getName).collect(joining(", ")));
            log.error(errorMessage);
            throw new MigrationValidationFailedException(errorMessage);
        }
    }

    private void validateDefaultGroupsNotExceeding6xGroups(Set<Group> default6xGroups, ApplicationKey applicationKey,
                                                           ApplicationRole originalApp, ApplicationRole resultingApp) {
        // get all the default groups and check that they don't have more than 6.x groups with USE
        //   -> (role.defaultGroups - role.invariantDefaultGroups) > default6xGroups - added groups -> MigrationException
        final Set<Group> addedDefaultGroups = Sets.difference(
                Sets.difference(resultingApp.defaultGroups(), originalApp.defaultGroups()), default6xGroups);
        if (!addedDefaultGroups.isEmpty()) {
            String errorMessage = ("Following default groups for " + applicationKey.value() + " did not have USE permission: "
                    + addedDefaultGroups.stream().map(Group::getName).collect(joining(", ")));
            log.error(errorMessage);
            throw new MigrationValidationFailedException(errorMessage);
        }
    }

    private void verifyUseAndAdminGroupsMigratedToApplicationGroups(Set<Group> loginGroups,
                                                                    ApplicationKey applicationKey, ApplicationRole resultingApp) {
        // get all the 6.x USE and ADMIN groups and verify they exist as groups in Roles.
        //   -> loginGroups > role.groups - lost groups -> warning
        final Set<Group> droppedGroups = Sets.difference(loginGroups, resultingApp.groups());
        if (!droppedGroups.isEmpty()) {
            log.warn("Following Use/Admin groups were not migrated as application group to " + applicationKey.value()
                    + ": " + droppedGroups.stream().map(Group::getName).collect(joining(", ")));
        }
    }

    private void verifyUseGroupsWithoutAdminMigratedToDefaultGroups(Set<Group> default6xGroups,
                                                                    ApplicationKey applicationKey, ApplicationRole resultingApp) {
        // get all the 6.x USE groups (minus admin) and verify they exist in all the application roles as default
        //   -> default6xGroups > role.defaultGroups - lost groups -> warning
        final Set<Group> droppedDefaultGroups = Sets.difference(default6xGroups, resultingApp.defaultGroups());
        if (!droppedDefaultGroups.isEmpty()) {
            log.warn("Following USE groups were not migrated as default to " + applicationKey.value() + ": " +
                    droppedDefaultGroups.stream().map(Group::getName).collect(joining(", ")));
        }
    }

    private void validateServiceDeskRoleHasNoEscalations(final MigrationState original, MigrationState resulting) {
        final Option<ApplicationRole> serviceDeskRole = resulting.applicationRoles().get(SERVICE_DESK);

        if (serviceDeskRole.isDefined()) {
            // verify that after removing all the existing service desk from the resulting serviceDesk role and
            // still have groups remaining, there is escalation of privileges.
            final ApplicationRole originalApp = original.applicationRoles().get(SERVICE_DESK)
                    .getOrElse(ApplicationRole.forKey(SERVICE_DESK));

            final Set<Group> appGroups = Sets.difference(serviceDeskRole.get().groups(),
                    originalApp.groups());
            final Set<Group> defaultAppGroups = Sets.difference(serviceDeskRole.get().defaultGroups(),
                    originalApp.defaultGroups());
            if ((!appGroups.isEmpty() || !defaultAppGroups.isEmpty())) {

                final String message = "Service Desk appears to have groups when it is not licensed: "
                        + appGroups.stream().map(Group::getName).collect(joining(", ")) + " [default]:"
                        + defaultAppGroups.stream().map(Group::getName).collect(joining(", "));
                throw new MigrationValidationFailedException(message);
            }
        }
    }

    private Option<ServiceDeskLicenseType> getServiceDeskMigrationType() {
        return licenseSupplier.serviceDeskLicense().map(LicenseUtils::determineServiceDeskLicenseType);
    }

    static class MigrationValidationFailedException extends MigrationFailedException {
        MigrationValidationFailedException(final String message) {
            super(message);
        }
    }
}
