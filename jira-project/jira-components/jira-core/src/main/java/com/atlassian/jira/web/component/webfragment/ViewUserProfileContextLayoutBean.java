package com.atlassian.jira.web.component.webfragment;


import com.atlassian.jira.user.ApplicationUser;

/**
 * Provides context when rendering user profile links
 *
 * @since v3.12
 */
public class ViewUserProfileContextLayoutBean implements ContextLayoutBean {
    private final ApplicationUser profileUser;
    private final String actionName;


    public ViewUserProfileContextLayoutBean(ApplicationUser profileUser, String actionName) {
        this.profileUser = profileUser;
        this.actionName = actionName;
    }

    public ApplicationUser getProfileUser() {
        return profileUser;
    }

    public String getActionName() {
        return actionName;
    }
}
