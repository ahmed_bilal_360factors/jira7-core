package com.atlassian.jira.avatar.types.project;

import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.types.BasicTypedTypeAvatarService;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.plugin.icon.ProjectIconTypeDefinition;

public class ProjectTypeAvatarService extends BasicTypedTypeAvatarService {
    public ProjectTypeAvatarService(final AvatarManager avatarManager, final ProjectIconTypeDefinition projectIconType) {
        super(IconType.PROJECT_ICON_TYPE, avatarManager);
    }
}
