package com.atlassian.jira.upgrade;

import com.atlassian.jira.model.querydsl.QUpgradeVersionHistory;

import java.util.List;

/**
 * This manages storing and retrieving information from the {@link QUpgradeVersionHistory} table.
 * <p>
 * This contains the human readable format for the upgrades as well as the time that they were completed.
 */
public interface UpgradeVersionHistoryManager {
    /**
     * @return all of the upgrade version history in reverse chronological order.
     */
    List<UpgradeVersionHistoryItem> getAllUpgradeVersionHistory();

    /**
     * Add the given build number and version into the history storing the time completed as now.
     *
     * @param buildNumber of the database
     * @param version of JIRA
     */
    void addUpgradeVersionHistory(int buildNumber, String version);
}
