package com.atlassian.jira.web.action.user;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.event.user.OnboardingEvent;
import com.atlassian.jira.plugin.user.PasswordPolicyManager;
import com.atlassian.jira.plugin.user.WebErrorMessage;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.web.action.admin.user.PasswordChangeService;
import org.apache.commons.lang.StringUtils;
import webwork.action.ResultException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Handles the requests to reset a password for a specific user. The link to this action will come in an email sent from
 * as a result of the execution of {@link ForgotLoginDetails#doExecute()}
 *
 * @since v4.1
 */
public class ResetPassword extends JiraWebActionSupport {
    private final UserUtil userUtil;
    private final LoginService loginService;
    private final PasswordPolicyManager passwordPolicyManager;
    private final PasswordChangeService passwordChangeService;
    private final EventPublisher eventPublisher;

    private String token;
    private String os_username;
    private String password;
    private String confirm;
    private ApplicationUser userInPlay;
    private boolean userInvalid;
    private boolean tokenTimedOut;
    private boolean tokenInvalid;
    private final List<WebErrorMessage> passwordErrors = new ArrayList<WebErrorMessage>();

    public ResetPassword(final UserUtil userUtil,
                         final LoginService loginService,
                         final EventPublisher eventPublisher,
                         final PasswordPolicyManager passwordPolicyManager,
                         final PasswordChangeService passwordChangeService) {
        this.userUtil = userUtil;
        this.loginService = loginService;
        this.eventPublisher = eventPublisher;
        this.passwordPolicyManager = passwordPolicyManager;
        this.passwordChangeService = passwordChangeService;
    }

    /**
     * Handles the request to render the Reset Password form.
     *
     * @return The name of the view to be rendered. If there are any validation errors
     * {@link webwork.action.Action#INPUT}; Otherwise, {@link webwork.action.Action#ERROR} is returned.
     */
    @Override
    public String doDefault() {
        validateUserAndToken();
        if (isOnboardingUser()) {
            eventPublisher.publish(new OnboardingEvent(os_username, tokenTimedOut));
        }
        if (hasAnyErrors()) {
            return ERROR;
        }
        return INPUT;
    }

    @Override
    protected void validate() throws ResultException {
        validateUserAndToken();
        if (!userInvalid && !tokenInvalid && !tokenTimedOut) {
            validateNewPasswords();
        }
    }

    /**
     * Handles the request to set a new password for the user in play from the Reset Password form.
     *
     * @return The name of the view to be rendered. If there are any input errors, and the token is invalid or expired
     * {@link webwork.action.Action#ERROR}; otherwise, {@link webwork.action.Action#SUCCESS} is returned.
     */
    @Override
    protected String doExecute() {
        if (userInvalid || tokenInvalid || tokenTimedOut || invalidInput()) {
            return ERROR;
        }
        passwordChangeService.setPassword(this, userInPlay, password);
        if (invalidInput()) {
            return ERROR;
        }
        return SUCCESS;
    }

    private void validateUserAndToken() {
        boolean isValid = true;
        userInPlay = userUtil.getUser(os_username);
        if (userInPlay == null) {
            isValid = false;
            userInvalid = true;
        } else {
            final UserUtil.PasswordResetTokenValidation validation = userUtil.validatePasswordResetToken(userInPlay, token);
            if (validation.getStatus() == UserUtil.PasswordResetTokenValidation.Status.EXPIRED) {
                isValid = false;
                tokenTimedOut = true;
            } else if (validation.getStatus() == UserUtil.PasswordResetTokenValidation.Status.UNEQUAL) {
                isValid = false;
                tokenInvalid = true;
            }
        }
        if (!isValid) {
            addErrorMessage(getText("resetpassword.error.invalid.user.or.token"));
        }
    }

    private void validateNewPasswords() {
        if (StringUtils.isBlank(password) || StringUtils.isBlank(confirm)) {
            addErrorMessage(getText("resetpassword.error.password.blank"));
        } else if (!nvl(password, "").equals(confirm)) {
            addErrorMessage(getText("resetpassword.error.password.mustmatch"));
        } else {
            final Collection<WebErrorMessage> messages = passwordPolicyManager.checkPolicy(userUtil.getUserByName(os_username), null, password);
            if (!messages.isEmpty()) {
                addError("password", getText("changepassword.new.password.rejected"));
                for (WebErrorMessage message : messages) {
                    passwordErrors.add(message);
                }
            }
        }
    }

    private String nvl(final String str, final String defaultStr) {
        return str == null ? defaultStr : str;
    }


    public String getToken() {
        return token;
    }

    public void setToken(final String token) {
        this.token = token;
    }

    public String getOs_username() {
        return os_username;
    }

    public void setOs_username(final String os_username) {
        this.os_username = os_username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public List<WebErrorMessage> getPasswordErrors() {
        return passwordErrors;
    }


    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(final String confirm) {
        this.confirm = confirm;
    }

    public boolean isTokenTimedOut() {
        return tokenTimedOut;
    }

    public boolean isTokenInvalid() {
        return tokenInvalid;
    }

    public boolean isUserInvalid() {
        return userInvalid;
    }

    private long getLoginCount() {
        Long loginCount = loginService.getLoginInfo(os_username).getLoginCount();
        if (loginCount == null) {
            return 0;
        }
        return loginCount;
    }

    /**
     * If a user has never logged in before and they're resetting their password, it can be assumed that they're being
     * onboarded.
     *
     * @return a boolean indicating if the using is being onboarded or not
     */
    private boolean isOnboardingUser() {
        return !userInvalid && (getLoginCount() == 0);
    }
}
