package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.auditing.ChangedValue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Objects;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * @since 7.0
 */
public class MigrationChangedValue implements ChangedValue {
    private final String name;
    private final String oldState;
    private final String newState;

    public MigrationChangedValue(String name, @Nullable String oldState, @Nullable String newState) {
        this.name = notNull(name);
        this.oldState = oldState;
        this.newState = newState;
    }

    @Nonnull
    @Override
    public String getName() {
        return name;
    }

    @Nullable
    @Override
    public String getFrom() {
        return oldState;
    }

    @Nullable
    @Override
    public String getTo() {
        return newState;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final MigrationChangedValue that = (MigrationChangedValue) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(oldState, that.oldState) &&
                Objects.equals(newState, that.newState);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, oldState, newState);
    }

    @Override
    public String toString() {
        return "MigrationChangedValue{" +
                "name='" + name + '\'' +
                ", oldState='" + oldState + '\'' +
                ", newState='" + newState + '\'' +
                '}';
    }
}
