package com.atlassian.jira.permission;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.config.group.GroupConfigurable;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.entity.EntityUtils;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.event.permission.PermissionAddedEvent;
import com.atlassian.jira.event.permission.PermissionDeletedEvent;
import com.atlassian.jira.event.permission.PermissionSchemeAddedToProjectEvent;
import com.atlassian.jira.event.permission.PermissionSchemeCopiedEvent;
import com.atlassian.jira.event.permission.PermissionSchemeCreatedEvent;
import com.atlassian.jira.event.permission.PermissionSchemeDeletedEvent;
import com.atlassian.jira.event.permission.PermissionSchemeRemovedFromProjectEvent;
import com.atlassian.jira.event.permission.PermissionSchemeUpdatedEvent;
import com.atlassian.jira.event.scheme.AbstractSchemeAddedToProjectEvent;
import com.atlassian.jira.event.scheme.AbstractSchemeCopiedEvent;
import com.atlassian.jira.event.scheme.AbstractSchemeEntityEvent;
import com.atlassian.jira.event.scheme.AbstractSchemeEvent;
import com.atlassian.jira.event.scheme.AbstractSchemeRemovedFromProjectEvent;
import com.atlassian.jira.event.scheme.AbstractSchemeUpdatedEvent;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.scheme.AbstractSchemeManager;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.scheme.SchemeFactory;
import com.atlassian.jira.scheme.SchemeType;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.ProjectWidePermission;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.security.type.AbstractIssueFieldSecurityType;
import com.atlassian.jira.security.type.GroupDropdown;
import com.atlassian.jira.security.type.SecurityType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionUtil;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.annotations.VisibleForTesting;
import org.ofbiz.core.entity.EntityCondition;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.EntityUtil;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.atlassian.jira.permission.LegacyProjectPermissionKeyMapping.getKey;
import static com.google.common.collect.Maps.newHashMap;
import static java.util.Objects.requireNonNull;

/**
 * This class is used to handle Permission Schemes.
 * <p>
 * Permission Schemes are created, removed and edited through this class
 */
public class DefaultPermissionSchemeManager extends AbstractSchemeManager implements PermissionSchemeManager, Startable, GroupConfigurable {
    private static final Logger log = LoggerFactory.getLogger(DefaultPermissionSchemeManager.class);

    @VisibleForTesting
    static final String SCHEME_ENTITY_NAME = "PermissionScheme";
    private static final String PERMISSION_ENTITY_NAME = "SchemePermissions";
    private static final String SCHEME_DESC = "Permission";
    private static final String DEFAULT_NAME_KEY = "admin.schemes.permissions.default";
    private static final String DEFAULT_DESC_KEY = "admin.schemes.permissions.default.desc";

    private final Cache<Long, SchemeEntityCacheEntry> schemeEntityCache;

    private final PermissionTypeManager permissionTypeManager;
    private final OfBizDelegator delegator;

    public DefaultPermissionSchemeManager(final ProjectManager projectManager, final PermissionTypeManager permissionTypeManager,
                                          final PermissionContextFactory permissionContextFactory, final OfBizDelegator delegator,
                                          final SchemeFactory schemeFactory, final NodeAssociationStore nodeAssociationStore, final GroupManager groupManager,
                                          final EventPublisher eventPublisher, final CacheManager cacheManager) {
        super(projectManager, permissionTypeManager, permissionContextFactory, schemeFactory,
                nodeAssociationStore, delegator, groupManager, eventPublisher, cacheManager);
        this.permissionTypeManager = permissionTypeManager;
        this.delegator = delegator;
        schemeEntityCache = cacheManager.getCache(DefaultPermissionSchemeManager.class.getName() + ".schemeEntityCache",
                SchemeEntityCacheEntry::new,
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).build());
    }

    /**
     * Registers this CachingFieldConfigContextPersister's cache in the JIRA instrumentation.
     *
     * @throws Exception
     */
    @Override
    public void start() throws Exception {
    }

    @Override
    public void onClearCache(final ClearCacheEvent event) {
        super.onClearCache(event);
        flushSchemeEntities();
    }

    @Override
    public String getSchemeEntityName() {
        return SCHEME_ENTITY_NAME;
    }

    @Override
    public String getEntityName() {
        return PERMISSION_ENTITY_NAME;
    }

    @Override
    public String getSchemeDesc() {
        return SCHEME_DESC;
    }

    @Override
    public String getDefaultNameKey() {
        return DEFAULT_NAME_KEY;
    }

    @Override
    public String getDefaultDescriptionKey() {
        return DEFAULT_DESC_KEY;
    }

    @Override
    protected AbstractSchemeEvent createSchemeCreatedEvent(final Scheme scheme) {
        return new PermissionSchemeCreatedEvent(scheme);
    }

    @Nonnull
    @Override
    protected AbstractSchemeCopiedEvent createSchemeCopiedEvent(@Nonnull final Scheme oldScheme, @Nonnull final Scheme newScheme) {
        return new PermissionSchemeCopiedEvent(oldScheme, newScheme);
    }

    @Override
    protected AbstractSchemeUpdatedEvent createSchemeUpdatedEvent(final Scheme scheme, final Scheme originalScheme) {
        return new PermissionSchemeUpdatedEvent(scheme, originalScheme);
    }

    @Override
    public void deleteScheme(Long id) throws GenericEntityException {
        final Scheme scheme = getSchemeObject(id);

        super.deleteScheme(id);
        requireNonNull(scheme);
        eventPublisher.publish(new PermissionSchemeDeletedEvent(id, scheme.getName()));
    }

    @Override
    @Nonnull
    protected AbstractSchemeAddedToProjectEvent createSchemeAddedToProjectEvent(final Scheme scheme, final Project project) {
        return new PermissionSchemeAddedToProjectEvent(scheme, project);
    }

    /**
     * Get all Generic Value permission records for a particular scheme and permission Id
     *
     * @param scheme       The scheme that the permissions belong to
     * @param permissionId The Id of the permission
     * @return List of (GenericValue) permissions
     * @throws GenericEntityException
     */
    @Override
    public List<GenericValue> getEntities(final GenericValue scheme, final Long permissionId) throws GenericEntityException {
        ProjectPermissionKey permissionKey = getKey(permissionId);
        return getEntities(scheme, permissionKey);
    }

    /**
     * Get all Generic Value permission records for a particular scheme and permission Id
     *
     * @param scheme       The scheme that the permissions belong to
     * @param permissionId The Id of the permission
     * @param parameter    The permission parameter (group name etc)
     * @return List of (GenericValue) permissions
     * @throws GenericEntityException
     */
    @Override
    public List<GenericValue> getEntities(final GenericValue scheme, final Long permissionId, final String parameter) throws GenericEntityException {
        ProjectPermissionKey permissionKey = getKey(permissionId);
        return getEntities(scheme, permissionKey, parameter);
    }

    public List<GenericValue> getEntities(@Nonnull GenericValue scheme, @Nonnull ProjectPermissionKey permissionKey, @Nonnull String parameter) throws GenericEntityException {
        return EntityUtil.filterByAnd(getEntities(scheme, permissionKey), Collections.singletonMap("parameter", parameter));
    }

    /**
     * Get all Generic Value permission records for a particular scheme and permission Id
     *
     * @param scheme       The scheme that the permissions belong to
     * @param permissionId The Id of the permission
     * @param parameter    The permission parameter (group name etc)
     * @param type         The type of the permission(Group, Current Reporter etc)
     * @return List of (GenericValue) permissions
     * @throws GenericEntityException
     */
    @Override
    public List<GenericValue> getEntities(final GenericValue scheme, final Long permissionId, final String type, final String parameter) throws GenericEntityException {
        ProjectPermissionKey permissionKey = getKey(permissionId);
        return getEntities(scheme, permissionKey, type, parameter);
    }

    @Override
    public List<GenericValue> getEntities(@Nonnull GenericValue scheme, @Nonnull ProjectPermissionKey permissionKey, @Nonnull String type, @Nonnull String parameter) throws GenericEntityException {
        return EntityUtil.filterByAnd(getEntities(scheme, permissionKey), MapBuilder.build("type", type, "parameter", parameter));
    }

    /**
     * Get all Generic Value permission records for a particular scheme and permission Id
     *
     * @param scheme       The scheme that the permissions belong to
     * @param type         The type of the permission(Group, Current Reporter etc)
     * @param permissionId The Id of the permission
     * @return List of (GenericValue) permissions
     * @throws GenericEntityException
     */
    @Override
    public List<GenericValue> getEntities(final GenericValue scheme, final String type, final Long permissionId) throws GenericEntityException {
        ProjectPermissionKey permissionKey = getKey(permissionId);
        return getEntitiesByType(scheme, permissionKey, type);
    }

    @Override
    public List<GenericValue> getEntitiesByType(@Nonnull GenericValue scheme, @Nonnull ProjectPermissionKey permissionKey, @Nonnull String type) throws GenericEntityException {
        return EntityUtil.filterByAnd(getEntities(scheme, permissionKey), Collections.singletonMap("type", type));
    }

    @Override
    public List<GenericValue> getEntities(@Nonnull GenericValue scheme, @Nonnull ProjectPermissionKey permissionKey) throws GenericEntityException {
        return getEntities(scheme, permissionKey.permissionKey());
    }

    @Override
    public List<GenericValue> getEntities(GenericValue scheme, String permissionKey) throws GenericEntityException {
        Long key = getSchemeEntityCacheKey(scheme);
        final Collection<PermissionSchemeEntry> schemeEntries = schemeEntityCache.get(key).getPermissionSchemeEntries(permissionKey);
        if (schemeEntries == null) {
            return Collections.emptyList();
        }
        return CollectionUtil.transform(schemeEntries, Entity.PERMISSION_SCHEME_ENTRY::genericValueFrom);
    }

    @Override
    public Collection<PermissionSchemeEntry> getPermissionSchemeEntries(@Nonnull final Scheme scheme, @Nonnull final ProjectPermissionKey permissionKey) {
        return getPermissionSchemeEntries(scheme.getId(), permissionKey);
    }

    @Override
    public Collection<PermissionSchemeEntry> getPermissionSchemeEntries(final long schemeId, @Nonnull final ProjectPermissionKey permissionKey) {
        final SchemeEntityCacheEntry schemeEntityCacheEntry = schemeEntityCache.get(schemeId);
        if (schemeEntityCacheEntry == null) {
            throw new IllegalArgumentException("Unknown Permission Scheme ID: " + schemeId);
        }
        final Collection<PermissionSchemeEntry> permissionSchemeEntries = schemeEntityCacheEntry.getPermissionSchemeEntries(permissionKey.permissionKey());
        if (permissionSchemeEntries == null) {
            return Collections.emptyList();
        }
        return permissionSchemeEntries;
    }

    @Override
    public Collection<PermissionSchemeEntry> getPermissionSchemeEntries(long schemeId, @Nonnull final ProjectPermissionKey permissionKey, @Nonnull final String type) {
        return getPermissionSchemeEntries(schemeId, permissionKey).stream()
                .filter(permissionSchemeEntry -> type.equals(permissionSchemeEntry.getType()))
                .collect(Collectors.toList());
    }

    /**
     * Create a new permission record in the database
     *
     * @param scheme       The scheme that the permission record is associated with
     * @param schemeEntity The scheme entity object that is to be added to the scheme
     * @return The permission object
     * @throws GenericEntityException
     */
    @Override
    public GenericValue createSchemeEntity(final GenericValue scheme, final SchemeEntity schemeEntity) throws GenericEntityException {
        GenericValue result = createSchemeEntityNoEvent(scheme, schemeEntity);
        eventPublisher.publish(new PermissionAddedEvent(scheme.getLong("id"), withFixedEntityTypeId(schemeEntity)));
        return result;
    }

    private SchemeEntity withFixedEntityTypeId(SchemeEntity original) {
        if (original.getEntityTypeId() instanceof ProjectPermissionKey) {
            return original;
        }

        ProjectPermissionKey permissionKey = PermissionSchemeUtil.getPermissionKey(original).getOrNull();

        return new SchemeEntity(original.getId(), original.getType(), original.getParameter(),
                permissionKey, original.getTemplateId(), original.getSchemeId());
    }

    protected GenericValue createSchemeEntityNoEvent(final GenericValue scheme, final SchemeEntity schemeEntity) throws GenericEntityException {
        if (scheme == null) {
            throw new IllegalArgumentException("Scheme passed must NOT be null");
        }

        if (schemeEntity.getType() == null) {
            throw new IllegalArgumentException("Type in SchemeEntity can NOT be null");
        }

        ProjectPermissionKey permissionKey = PermissionSchemeUtil.getPermissionKey(schemeEntity).getOrNull();

        final GenericValue perm = EntityUtils.createValue(PERMISSION_ENTITY_NAME,
                MapBuilder.<String, Object>newBuilder("scheme", scheme.getLong("id"))
                        .add("permissionKey", permissionKey.permissionKey())
                        .add("type", schemeEntity.getType())
                        .add("parameter", schemeEntity.getParameter()).toMap());

        schemeEntityCache.remove(getSchemeEntityCacheKey(scheme));

        return perm;
    }

    /**
     * Deletes a permission from the database
     *
     * @param id The id of the permission to be deleted
     */
    @Override
    public void deleteEntity(final Long id) throws DataAccessException {
        super.deleteEntity(id);
        flushSchemeEntities();
    }

    @Override
    public void deleteEntities(final Iterable<Long> ids) {
        super.deleteEntities(ids);
        flushSchemeEntities();
    }

    @Override
    protected AbstractSchemeEntityEvent createSchemeEntityDeletedEvent(GenericValue entity) {
        return new PermissionDeletedEvent(entity.getLong("scheme"), makeSchemeEntity(entity));
    }

    @Override
    protected SchemeEntity makeSchemeEntity(final GenericValue entity) {
        ProjectPermissionKey permissionKey = new ProjectPermissionKey(entity.getString("permissionKey"));
        SchemeEntity schemeEntity = new SchemeEntity(entity.getString("type"), entity.getString("parameter"), permissionKey);
        schemeEntity.setSchemeId(entity.getLong("scheme"));
        return schemeEntity;
    }

    @Override
    public List<GenericValue> getEntities(final GenericValue scheme) throws GenericEntityException {
        final Long key = getSchemeEntityCacheKey(scheme);
        return Collections.unmodifiableList(schemeEntityCache.get(key).getCache());
    }

    /**
     * Removes all scheme entities with this parameter
     *
     * @param type      the type of scheme entity you wish to remove 'user', 'group', 'projectrole'
     * @param parameter must NOT be null
     */
    @Override
    public boolean removeEntities(final String type, final String parameter) throws RemoveException {
        final boolean result = super.removeEntities(type, parameter);
        flushSchemeEntities();
        return result;
    }

    /**
     * Retrieves all the entites for this permission and then removes them.
     *
     * @param scheme       to remove entites from must NOT be null
     * @param permissionId to remove must NOT be a global permission
     * @return True is all the entities are removed
     * @throws RemoveException
     */
    @Override
    public boolean removeEntities(final GenericValue scheme, final Long permissionId) throws RemoveException {
        if (Permissions.isGlobalPermission(permissionId.intValue())) {
            throw new IllegalArgumentException("PermissionId passed must not be a global permissions " + permissionId.toString() + " is global");
        }

        final boolean result = super.removeEntities(scheme, permissionId);
        schemeEntityCache.remove(getSchemeEntityCacheKey(scheme));
        return result;
    }

    @Override
    public boolean hasSchemePermission(@Nonnull final ProjectPermissionKey permissionKey, @Nonnull final Project project) {
        return hasSchemePermission(permissionKey, getSchemeIdFor(project), (schemeType, parameter) -> schemeType.hasPermission(project, parameter));
    }

    @Override
    public boolean hasSchemePermission(@Nonnull final ProjectPermissionKey permissionKey, @Nonnull final Issue issue) {
        return hasSchemePermission(permissionKey, getSchemeIdFor(issue.getProjectObject()), (schemeType, parameter) -> schemeType.hasPermission(issue, parameter));
    }

    @Override
    public boolean hasSchemePermission(@Nonnull final ProjectPermissionKey permissionKey, @Nonnull final Project project, @Nullable final ApplicationUser user, final boolean issueCreation) {
        if (user == null) {
            return hasSchemePermission(permissionKey, project);
        }

        return hasSchemePermission(permissionKey, getSchemeIdFor(project), (schemeType, parameter) -> schemeType.hasPermission(project, parameter, user, issueCreation));
    }

    @Override
    public ProjectWidePermission hasProjectWidePermission(@Nonnull final ProjectPermissionKey permissionKey,
                                                          @Nonnull final Project project, @Nonnull final ApplicationUser user, final boolean issueCreation) {
        final Long schemeId = getSchemeIdFor(project);
        if (schemeId == null) {
            // There is no permission scheme associated with the project.
            // Seems broken but the old code used to handle this (perhaps unintentionally), and it shows up in Func Tests.
            return ProjectWidePermission.NO_ISSUES;
        }

        final Map<String, SecurityType> securityTypes = permissionTypeManager.getTypes();
        final Collection<PermissionSchemeEntry> entities = getPermissionSchemeEntries(schemeId, permissionKey);
        ProjectWidePermission hasPermission = ProjectWidePermission.NO_ISSUES;
        for (final PermissionSchemeEntry schemeEntry : entities) {
            final SecurityType securityType = securityTypes.get(schemeEntry.getType());
            if (securityType != null && securityType.isValidForPermission(permissionKey)) {
                if (securityType instanceof AbstractIssueFieldSecurityType) {
                    // this security type works on a per issue basis
                    hasPermission = ProjectWidePermission.ISSUE_SPECIFIC;
                } else if (securityType.hasPermission(project, schemeEntry.getParameter(), user, issueCreation)) {
                    // Project level security type said Yes - so user has this permission on all issues.
                    return ProjectWidePermission.ALL_ISSUES;
                }
            }
        }
        return hasPermission;
    }

    @Override
    public boolean hasSchemePermission(@Nonnull final ProjectPermissionKey permissionKey, @Nonnull final Issue issue,
                                       @Nullable final ApplicationUser user, final boolean issueCreation) {
        if (user == null) {
            return hasSchemePermission(permissionKey, issue);
        }

        return hasSchemePermission(permissionKey, getSchemeIdFor(issue.getProjectObject()),
                (schemeType, parameter) -> schemeType.hasPermission(issue, parameter, user, issueCreation));
    }

    @Override
    @Nullable
    public Scheme getSchemeFor(Project project) {
        Scheme scheme = super.getSchemeFor(project);
        if (scheme == null) {
            log.warn("No permission scheme is associated with project '" + project.getName() + "'");
        }
        return scheme;
    }

    @Override
    @Nullable
    public Long getSchemeIdFor(final Project project) {
        Long schemeId = super.getSchemeIdFor(project);
        if (schemeId == null) {
            log.warn("No permission scheme is associated with project '" + project.getName() + "'");
        }
        return schemeId;
    }

    @Override
    public Collection<Group> getGroups(final Long entityTypeId, final Project project) {
        ProjectPermissionKey permissionKey = getKey(entityTypeId);
        return getGroups(permissionKey, project);
    }

    @Override
    public Collection<Group> getGroups(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Project project) {
        return getGroups(permissionKey, project.getGenericValue());
    }

    @Override
    public Collection<Group> getGroups(final Long entityTypeId, final GenericValue project) {
        ProjectPermissionKey permissionKey = getKey(entityTypeId);
        return getGroups(permissionKey, project);
    }

    private Collection<Group> getGroups(ProjectPermissionKey permissionKey, final GenericValue project) {
        if (project == null) {
            throw new IllegalArgumentException("Project passed can NOT be null");
        }
        if (!"Project".equals(project.getEntityName())) {
            throw new IllegalArgumentException("Project passed must be a project not a " + project.getEntityName());
        }

        final Set<Group> groups = new HashSet<>();

        try {
            final List<GenericValue> schemes = getSchemes(project);
            for (final GenericValue scheme : schemes) {
                final List<GenericValue> entity = getEntitiesByType(scheme, permissionKey, GroupDropdown.DESC);
                groups.addAll(entity.stream().map(permission -> groupManager.getGroup(permission.getString("parameter"))).collect(Collectors.toList()));
            }
        } catch (final GenericEntityException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }

        return groups;
    }

    @Override
    public Collection<ApplicationUser> getUsers(@Nonnull ProjectPermissionKey permissionKey, @Nonnull PermissionContext ctx) {
        final Set<ApplicationUser> users = new HashSet<>();

        final Map<String, SecurityType> permTypes = securityTypeManager.getTypes();
        final Long schemeId = getSchemeIdFor(ctx.getProjectObject());
        final Collection<PermissionSchemeEntry> entries = getPermissionSchemeEntries(schemeId, permissionKey);

        for (final PermissionSchemeEntry entry : entries) {
            final SecurityType secType = permTypes.get(entry.getType());
            if (secType != null) {
                try {
                    final Set<ApplicationUser> usersToAdd = secType.getUsers(ctx, entry.getParameter());
                    users.addAll(usersToAdd.stream().filter(ApplicationUser::isActive).collect(Collectors.toList()));
                } catch (final IllegalArgumentException e) {
                    // If the entered custom field id is incorrect
                    log.warn(e.getMessage(), e);
                }
            }
        }

        return users;
    }

    @Nonnull
    @Override
    protected AbstractSchemeRemovedFromProjectEvent createSchemeRemovedFromProjectEvent(final Scheme scheme, final Project project) {
        return new PermissionSchemeRemovedFromProjectEvent(scheme, project);
    }

    /////////////// Private methods /////////////////////////////////////////////////////

    private boolean hasSchemePermission(final ProjectPermissionKey permissionKey, final Long schemeId,
                                        final PermissionSchemeEntryChecker permissionSchemeEntryChecker) {
        if (schemeId == null) {
            // There is no permission scheme associated with the project.
            // Seems broken but removing permissions is the first step in deleting a project. Helps to prevent some
            // concurrency problems like people adding new issues to the project.
            return false;
        }
        final Map<String, SecurityType> securityTypes = permissionTypeManager.getTypes();
        final Collection<PermissionSchemeEntry> entities = getPermissionSchemeEntries(schemeId, permissionKey);
        for (final PermissionSchemeEntry schemeEntry : entities) {
            if (schemeEntry != null) {
                final SecurityType securityType = securityTypes.get(schemeEntry.getType());
                if (securityType != null && securityType.isValidForPermission(permissionKey)) {
                    if (permissionSchemeEntryChecker.hasPermission(securityType, schemeEntry.getParameter())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private Long getSchemeEntityCacheKey(final GenericValue scheme) {
        return scheme.getLong("id");
    }

    public void flushSchemeEntities() {
        schemeEntityCache.removeAll();
    }

    public Collection<GenericValue> getSchemesContainingEntity(final String type, final String parameter) {
        Collection<GenericValue> schemes;

        final Collection<GenericValue> entities = delegator.findByAnd(PERMISSION_ENTITY_NAME,
                FieldMap.build("type", type, "parameter", parameter));

        // This is not needed if we can do a distinct select
        final List<EntityCondition> entityConditions = entities.stream()
                .map(schemeEntity -> schemeEntity.getLong("scheme")) // Stream of scheme IDs
                .map(id -> new EntityExpr("id", EntityOperator.EQUALS, id)) // Stream of EntityCondition
                .collect(Collectors.toList());

        if (!entityConditions.isEmpty()) {
            schemes = delegator.findByOr(SCHEME_ENTITY_NAME, entityConditions, Collections.emptyList());
        } else {
            schemes = Collections.emptyList();
        }
        return schemes;
    }

    @Override
    public void swapParameterForEntitiesOfType(final String type, final String parameter, final String resultingParameter) {
        super.swapParameterForEntitiesOfType(type, parameter, resultingParameter);
        flushSchemeEntities();
    }

    @Override
    public boolean isGroupUsed(@Nonnull final Group group) {
        final Collection<GenericValue> entities = delegator.findByAnd(PERMISSION_ENTITY_NAME,
                FieldMap.build("type", GroupDropdown.DESC, "parameter", group.getName()));
        return entities.size() > 0;
    }

    private class SchemeEntityCacheEntry {
        private final List<GenericValue> cache;
        private final Map<String, List<PermissionSchemeEntry>> entriesByPermissionKey;

        SchemeEntityCacheEntry(Long key) {
            List<GenericValue> schemeEntities;
            try {
                GenericValue scheme = DefaultPermissionSchemeManager.super.getScheme(key);
                schemeEntities = DefaultPermissionSchemeManager.super.getEntities(scheme);
            } catch (GenericEntityException e) {
                throw new RuntimeException(e);
            }
            cache = schemeEntities;

            final Map<String, List<PermissionSchemeEntry>> tempMap = newHashMap();
            for (GenericValue entity : schemeEntities) {
                String permissionKey = entity.getString("permissionKey");

                List<PermissionSchemeEntry> entitiesForPermission = tempMap.get(permissionKey);
                if (entitiesForPermission == null) {
                    entitiesForPermission = new ArrayList<>();
                    tempMap.put(permissionKey, entitiesForPermission);
                }
                entitiesForPermission.add(Entity.PERMISSION_SCHEME_ENTRY.build(entity));
            }
            entriesByPermissionKey = new HashMap<>(tempMap.size());
            // Make the lists read only so they can escape safely
            for (Map.Entry<String, List<PermissionSchemeEntry>> mapEntry : tempMap.entrySet()) {
                entriesByPermissionKey.put(mapEntry.getKey(), new ArrayList<>(mapEntry.getValue()));
            }
        }

        public List<GenericValue> getCache() {
            return cache;
        }

        @Nullable
        public Collection<PermissionSchemeEntry> getPermissionSchemeEntries(final String permissionKey) {
            return entriesByPermissionKey.get(permissionKey);
        }
    }

    private interface PermissionSchemeEntryChecker {
        boolean hasPermission(SchemeType schemeType, String parameter);
    }
}
