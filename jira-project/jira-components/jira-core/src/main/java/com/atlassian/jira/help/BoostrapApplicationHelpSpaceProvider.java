package com.atlassian.jira.help;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.atlassian.jira.util.JiraProductInformation;

import javax.annotation.Nonnull;

/**
 * Application key provided is ignored and data is loaded from 'product.properties' file. To be used during db setup.
 *
 * @since 7.0
 */
public class BoostrapApplicationHelpSpaceProvider implements ApplicationHelpSpaceProvider {
    final JiraProductInformation jiraProductInformation;

    public BoostrapApplicationHelpSpaceProvider(final JiraProductInformation jiraProductInformation) {
        this.jiraProductInformation = jiraProductInformation;
    }

    @Override
    @Nonnull
    public Option<String> getHelpSpace(@Nonnull final ApplicationKey applicationKey) {
        return Option.some(jiraProductInformation.getSetupHelpApplicationHelpSpace());
    }
}
