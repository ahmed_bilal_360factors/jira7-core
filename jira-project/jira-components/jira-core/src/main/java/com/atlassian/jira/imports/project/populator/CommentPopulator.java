package com.atlassian.jira.imports.project.populator;

import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.external.beans.ExternalComment;
import com.atlassian.jira.imports.project.core.BackupOverviewBuilder;
import com.atlassian.jira.imports.project.parser.CommentParser;
import com.atlassian.jira.imports.project.parser.CommentParserImpl;

import java.util.Map;

/**
 * Populates the comments length in the BackupOverview object
 *
 * @since v7.0
 */
public class CommentPopulator implements BackupOverviewPopulator {
    private CommentParserImpl commentParser;

    public void populate(final BackupOverviewBuilder backupOverviewBuilder, final String elementName, final Map attributes) throws ParseException {
        if (CommentParser.COMMENT_ENTITY_NAME.equals(elementName)) {
            final ExternalComment externalComment = getCommentParser().parse(attributes);
            if (externalComment != null) {
                backupOverviewBuilder.addComment(externalComment);
            }
        }
    }

    CommentParser getCommentParser() {
        if (commentParser == null) {
            commentParser = new CommentParserImpl();
        }
        return commentParser;
    }

}
