package com.atlassian.jira.permission.data;

import com.atlassian.fugue.Either;
import com.atlassian.jira.permission.PermissionGrant;
import com.atlassian.jira.permission.PermissionGrantInput;
import com.atlassian.jira.permission.PermissionHolder;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Predicate;

import static com.atlassian.fugue.Either.left;
import static com.atlassian.fugue.Either.right;

/**
 * This is a wrapper over {@link com.atlassian.jira.permission.PermissionGrant} or {@link com.atlassian.jira.permission.PermissionGrantInput}
 * which treats both of them in the same way in terms of equality. I.e. it ignores the id of grants.
 */
public final class PermissionGrantAsPureData {
    public static final Function<PermissionGrant, PermissionGrantAsPureData> TO_PURE_DATA = new Function<PermissionGrant, PermissionGrantAsPureData>() {
        @Override
        public PermissionGrantAsPureData apply(final PermissionGrant input) {
            return of(input);
        }
    };

    public static final Function<PermissionGrantInput, PermissionGrantAsPureData> TO_PURE_DATA_2 = new Function<PermissionGrantInput, PermissionGrantAsPureData>() {
        @Override
        public PermissionGrantAsPureData apply(final PermissionGrantInput input) {
            return of(input);
        }
    };

    public static final Function<PermissionGrantAsPureData, PermissionGrant> TO_GRANT = new Function<PermissionGrantAsPureData, PermissionGrant>() {
        @Override
        public PermissionGrant apply(final PermissionGrantAsPureData input) {
            return input.getGrant();
        }
    };

    public static final Function<PermissionGrantAsPureData, PermissionGrantInput> TO_GRANT_INPUT = new Function<PermissionGrantAsPureData, PermissionGrantInput>() {
        @Override
        public PermissionGrantInput apply(final PermissionGrantAsPureData input) {
            return input.getGrantInput();
        }
    };

    private final Either<PermissionGrant, PermissionGrantInput> actual;

    private PermissionGrantAsPureData(PermissionGrant grant, PermissionGrantInput grantData) {
        if (grant != null) {
            actual = left(grant);
        } else {
            actual = right(grantData);
        }
    }

    public Either<PermissionGrant, PermissionGrantInput> getActual() {
        return actual;
    }

    public PermissionGrant getGrant() {
        return actual.left().get();
    }

    public PermissionGrantInput getGrantInput() {
        return actual.right().get();
    }

    public static PermissionGrantAsPureData of(PermissionGrant grant) {
        return new PermissionGrantAsPureData(grant, null);
    }

    public static PermissionGrantAsPureData of(PermissionGrantInput grantData) {
        return new PermissionGrantAsPureData(null, grantData);
    }

    public static Predicate<PermissionGrant> equalToModuloId(final PermissionGrantInput data) {
        return new Predicate<PermissionGrant>() {
            @Override
            public boolean apply(final PermissionGrant input) {
                return PermissionGrantAsPureData.of(input).equals(PermissionGrantAsPureData.of(data));
            }
        };
    }

    private PermissionHolder getHolder() {
        return actual.fold(new Function<PermissionGrant, PermissionHolder>() {
            @Override
            public PermissionHolder apply(final PermissionGrant input) {
                return input.getHolder();
            }
        }, new Function<PermissionGrantInput, PermissionHolder>() {
            @Override
            public PermissionHolder apply(final PermissionGrantInput input) {
                return input.getHolder();
            }
        });
    }

    private ProjectPermissionKey getPermission() {
        return actual.fold(new Function<PermissionGrant, ProjectPermissionKey>() {
            @Override
            public ProjectPermissionKey apply(final PermissionGrant input) {
                return input.getPermission();
            }
        }, new Function<PermissionGrantInput, ProjectPermissionKey>() {
            @Override
            public ProjectPermissionKey apply(final PermissionGrantInput input) {
                return input.getPermission();
            }
        });
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PermissionGrantAsPureData that = (PermissionGrantAsPureData) o;

        return Objects.equal(this.getHolder().getParameter(), that.getHolder().getParameter()) &&
                Objects.equal(this.getHolder().getType().getKey(), that.getHolder().getType().getKey()) &&
                Objects.equal(this.getHolder().getType().requiresParameter(), that.getHolder().getType().requiresParameter()) &&
                Objects.equal(this.getPermission(), that.getPermission());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getHolder(), getPermission());
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("holder", getHolder())
                .add("permission", getPermission())
                .toString();
    }
}
