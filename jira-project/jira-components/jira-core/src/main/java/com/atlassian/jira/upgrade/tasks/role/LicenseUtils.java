package com.atlassian.jira.upgrade.tasks.role;


import javax.annotation.Nonnull;

import static com.atlassian.jira.license.ServiceDeskLicenseConstants.SD_LEGACY_ACTIVE;
import static com.atlassian.jira.license.ServiceDeskLicenseConstants.SD_RBP_ACTIVE;
import static com.atlassian.jira.license.ServiceDeskLicenseConstants.SD_USER_COUNT_ABP;

final class LicenseUtils {
    private LicenseUtils() {
    }

    /**
     * Determines type of a Service Desk license. Type is determined based on license properties, see {@link
     * ServiceDeskLicenseType} for list of supported Service Desk
     * license types.
     * <p>
     * Note: for migration purposes this method returns {@link ServiceDeskLicenseType#AgentBasedPricing} if provided
     * license is Role Based Pricing license for Service Desk without backward compatible properties.
     *
     * @param license which type will be determined. Must be valid Service Desk license.
     * @return type of a service desk license
     * @throws com.atlassian.jira.upgrade.tasks.role.MigrationFailedException in case license type cannot be determined
     *                                                                        or supplied license is not Service Desk license
     */
    public static ServiceDeskLicenseType determineServiceDeskLicenseType(@Nonnull final License license) {
        if (isServiceDeskLicense(license)) {
            final String roleCountStr = license.productLicense().getProperty(SD_USER_COUNT_ABP);
            if (roleCountStr != null) {
                return ServiceDeskLicenseType.AgentBasedPricing;
            } else {
                return ServiceDeskLicenseType.TierBasedPricing;
            }
        } else {
            throw new MigrationFailedException("Not a service desk license: " + license.toString());
        }
    }


    public static boolean isServiceDeskLicense(@Nonnull final License license) {
        return Boolean.parseBoolean(license.productLicense().getProperty(SD_LEGACY_ACTIVE))
                || isRbpLicense(license);
    }

    private static boolean isRbpLicense(final @Nonnull License license) {
        return Boolean.parseBoolean(license.productLicense().getProperty(SD_RBP_ACTIVE));
    }


    enum ServiceDeskLicenseType {
        AgentBasedPricing,
        TierBasedPricing
    }
}
