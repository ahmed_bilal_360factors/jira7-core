package com.atlassian.jira.plugin.renderer;

import com.atlassian.renderer.v2.components.RendererComponent;

public interface RendererComponentDecoratorFactory {
    RendererComponent decorate(Class<RendererComponent> type, RendererComponent rendererComponent);
}