package com.atlassian.jira.project.type;

/**
 * Default implementation for {@link com.atlassian.jira.project.type.ProjectTypesDarkFeature}
 *
 * @Deprecated Project type is enabled by default after JIRA 7, so we don't need it any more. Since 7.0
 */
public class ProjectTypesDarkFeatureImpl implements ProjectTypesDarkFeature {

    public ProjectTypesDarkFeatureImpl() {

    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
