package com.atlassian.jira.web.action.project;

import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.bc.project.DeleteProjectTaskContext;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.task.TaskContext;
import com.atlassian.jira.task.TaskDescriptor;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.user.UserIssueHistoryManager;
import com.atlassian.jira.web.SessionKeys;
import com.atlassian.jira.web.action.IssueActionSupport;
import com.atlassian.jira.web.bean.TaskDescriptorBean;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpSession;
import java.util.concurrent.ExecutionException;

@WebSudoRequired
public class DeleteProject extends IssueActionSupport {
    private boolean confirm;
    private Long pid;
    private final ProjectService projectService;
    private Project project;
    private Long taskId;
    private final TaskManager taskManager;
    private final TaskDescriptorBean.Factory taskBeanFactory;
    /**
     * Final destination to go to after progress is acknowledged.
     */
    private String destinationURL;

    private TaskDescriptor<ProjectService.DeleteProjectResult> currentTaskDescriptor;
    private TaskDescriptorBean<ProjectService.DeleteProjectResult> currentTask;

    public DeleteProject(final IssueManager issueManager, final CustomFieldManager customFieldManager,
                         final AttachmentManager attachmentManager, final ProjectManager projectManager,
                         final PermissionManager permissionManager, final VersionManager versionManager,
                         final ProjectService projectService,
                         final UserIssueHistoryManager userHistoryManager, final TimeTrackingConfiguration timeTrackingConfiguration, TaskManager taskManager, TaskDescriptorBean.Factory taskBeanFactory) {
        super(issueManager, customFieldManager, attachmentManager, projectManager, permissionManager, versionManager, userHistoryManager, timeTrackingConfiguration);
        this.projectService = projectService;
        this.taskManager = taskManager;
        this.taskBeanFactory = taskBeanFactory;
    }

    public String doDefault() throws Exception {
        setDestinationURL(StringUtils.isNotBlank(getReturnUrl()) ? getReturnUrl() : "/secure/project/ViewProjects.jspa");
        setReturnUrl(null);

        final TaskDescriptor<ProjectService.DeleteProjectResult> taskDescriptor = getCurrentTaskDescriptor();
        if (taskDescriptor != null) {
            return getRedirect(taskDescriptor.getProgressURL());
        }

        return super.doDefault();
    }

    protected void doValidation() {
        final Project projectObject = getProject();
        if (projectObject == null) {
            addErrorMessage(getText("admin.deleteproject.error.no.project", pid));
            return;
        }

        final ProjectService.DeleteProjectValidationResult result =
                projectService.validateDeleteProject(getLoggedInUser(), projectObject.getKey());
        if (!result.isValid()) {
            addErrorCollection(result.getErrorCollection());
        }
    }

    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        if (confirm) {
            setDestinationURL(StringUtils.isNotBlank(getReturnUrl()) ? getReturnUrl() : "/secure/project/ViewProjects.jspa");
            setReturnUrl(null);

            final TaskDescriptor<ProjectService.DeleteProjectResult> taskDescriptor = getCurrentTaskDescriptor();
            if (taskDescriptor != null) {
                return getRedirect(taskDescriptor.getProgressURL());
            }

            final ProjectService.DeleteProjectValidationResult result =
                    projectService.validateDeleteProject(getLoggedInUser(), getProject().getKey());
            if (!result.isValid()) {
                addErrorCollection(result.getErrorCollection());
                return ERROR;
            }

            ProjectService.DeleteProjectResult deleteResult = projectService.deleteProjectAsynchronous(getLoggedInUser(), result);
            if (!deleteResult.isValid()) {
                addErrorCollection(deleteResult.getErrorCollection());
                return ERROR;
            }
            // Delete the session reference to the current project.  We are in the process of blowing it away.
            HttpSession session = getHttpRequest().getSession();
            session.removeAttribute(SessionKeys.CURRENT_ADMIN_PROJECT);

            return getRedirect(deleteResult.getRedirectUrl() + "&destinationURL=" + getDestinationURL());
        }
        return getResult();
    }

    public String doProgress() throws ExecutionException, InterruptedException {
        if (taskId == null) {
            addErrorMessage(getText("common.tasks.no.task.id"));
            return ERROR;
        }
        currentTaskDescriptor = taskManager.getTask(taskId);
        if (currentTaskDescriptor == null) {
            addErrorMessage(getText("common.tasks.task.not.found"));
            return ERROR;
        }
        final TaskContext context = currentTaskDescriptor.getTaskContext();
        if (!(context instanceof DeleteProjectTaskContext)) {
            addErrorMessage(getText("common.tasks.wrong.task.context", DeleteProjectTaskContext.class.getName(), context.getClass().getName()));
            return ERROR;
        }

        if (currentTaskDescriptor.isFinished() && !currentTaskDescriptor.isCancelled()) {
            final ProjectService.DeleteProjectResult result = currentTaskDescriptor.getResult();
            addErrorCollection(result.getErrorCollection());
        }
        return "progress";
    }

    public Project getProject() {
        if (project == null) {
            project = getProjectManager().getProjectObj(pid);
        }
        return project;
    }

    public boolean isConfirm() {
        return confirm;
    }

    public void setConfirm(boolean confirm) {
        this.confirm = confirm;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getDestinationURL() {
        return destinationURL;
    }

    public void setDestinationURL(String destinationURL) {
        this.destinationURL = destinationURL;
    }

    public TaskDescriptorBean<ProjectService.DeleteProjectResult> getCurrentTask() {
        if (currentTask == null) {
            final TaskDescriptor<ProjectService.DeleteProjectResult> taskDescriptor = getCurrentTaskDescriptor();
            if (taskDescriptor != null) {
                currentTask = taskBeanFactory.create(taskDescriptor);
            }
        }
        return currentTask;
    }

    private TaskDescriptor<ProjectService.DeleteProjectResult> getCurrentTaskDescriptor() {
        if (currentTaskDescriptor == null) {
            Project project = getProject();
            if (project != null) {
                currentTaskDescriptor = taskManager.getLiveTask(new DeleteProjectTaskContext(project));
            }
        }
        return currentTaskDescriptor;
    }
}
