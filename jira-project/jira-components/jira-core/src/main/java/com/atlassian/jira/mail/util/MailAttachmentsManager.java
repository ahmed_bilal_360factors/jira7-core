package com.atlassian.jira.mail.util;

import com.atlassian.jira.mail.TemplateUser;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.NonInjectableComponent;

import javax.mail.BodyPart;

@NonInjectableComponent
public interface MailAttachmentsManager {
    /**
     * Content-ID for generated attachments.
     */
    String CID_PREFIX = "jira-generated-image-";

    /**
     * Returns a link to access user avatar in email message.
     * If avatar can be attached to email, the cid link is returned,
     * otherwise avatar URL is returned and no image is attached.
     * (E.g. If Gravatar is enabled)
     *
     * @param username
     * @return cid link or Avatar URL
     */
    String getAvatarUrl(String username);

    /**
     * Returns a link to access user avatar in email message.
     * If avatar can be attached to email, the cid link is returned,
     * otherwise avatar URL is returned and no image is attached.
     * (E.g. If Gravatar is enabled)
     *
     * @param templateUser
     * @return cid link or Avatar URL
     */
    String getAvatarUrl(TemplateUser templateUser);

    /**
     * Returns a link to access user avatar in email message.
     * If avatar can be attached to email, the cid link is returned,
     * otherwise avatar URL is returned and no image is attached.
     * (E.g. If Gravatar is enabled)
     *
     * @param user
     * @return cid link or Avatar URL
     */
    String getAvatarUrl(ApplicationUser user);

    /**
     * Adds an attachment to the mail attachments manager's collection of attachments. This collection will eventually
     * be attached to the email. This method will return a CID that can be used to reference the attachment in
     * the body of an email. Given the same mail attachment multiple times, this method will
     * return the same CID.
     *
     * @param mailAttachment
     * @return The CID for an attachment
     */
    String addAttachmentAndReturnCid(MailAttachment mailAttachment);

    /**
     * Tries to add image specified by path to email attachments.
     * Returns image cid link if succeeds or unchanged path if specified
     * path cannot be added as attachment
     *
     * @param path
     * @return
     */
    String getImageUrl(String path);

    /**
     * Tries to remove the baseUrl (eg: http://example.com/jira) or the basePath (/jira) from an URL, and return the
     * result. Some quick examples (let's assume baseUrl = http://example.com/jira)
     * <p>
     * <ul>
     * <li>http://example.com/jira/images/icon.png --> /images/icon.png</li>
     * <li>/jira/images/icon.png                   --> /images/icon.png</li>
     * <li>/images/icon.png                        --> /images/icon.png</li>
     * <li>http://example.com/test/images/icon.png --> http://example.com/test/images/icon.png</li>
     * <li>http://google.com/jira/images/icon.png  --> http://google.com/jira/images/icon.png</li>
     * </ul>
     *
     * @param url the url to remove the baseUrl from
     * @return the url without the baseUrl nor the basePath
     */
    String removeBaseUrl(String url);

    /**
     * Tries to inline all the images specified by an IMG tag.
     * Replaces the image url with the cid link or unchanged path if specified
     * path cannot be added as attachment
     *
     * @param html HTML to change.
     * @return The same HTML with the images inlined.
     */
    String inlineImages(String html);

    /**
     * If the path is a relative url tries to add the image specified as an attachment.
     * Returns a cid link if successful, or unchanged path if not
     *
     * @since JIRA 6.3
     */
    String getExternalImageUrl(String path);

    /**
     * Returns the number of attachments added to this manager
     *
     * @return number of attachments
     */
    int getAttachmentsCount();

    /**
     * Builds bodyPart for each image (including avatars) added to this manager
     *
     * @return Added attachments as list of BodyParts
     */
    Iterable<BodyPart> buildAttachmentsBodyParts();
}
