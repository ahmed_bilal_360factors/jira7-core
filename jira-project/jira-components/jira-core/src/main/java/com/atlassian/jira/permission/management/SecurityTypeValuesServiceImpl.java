package com.atlassian.jira.permission.management;

import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.notification.type.ProjectRoleSecurityAndNotificationType;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.PermissionTypeManager;
import com.atlassian.jira.permission.management.beans.ProjectPermissionHelpBean;
import com.atlassian.jira.permission.management.beans.SecurityTypeBean;
import com.atlassian.jira.permission.management.beans.SecurityTypeValueBean;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.type.ApplicationRoleSecurityType;
import com.atlassian.jira.security.type.GroupCF;
import com.atlassian.jira.security.type.SecurityType;
import com.atlassian.jira.security.type.SecurityTypeKeys;
import com.atlassian.jira.security.type.UserCF;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * SecurityTypes have magic methods of how to get their "add" possible values.  This code knows about what they are
 */
public class SecurityTypeValuesServiceImpl implements SecurityTypeValuesService {
    private final GlobalPermissionManager globalPermissionManager;
    private final PermissionTypeManager permissionTypeManager;
    private final I18nHelper i18nHelper;
    private final HelpUrls urls;

    public SecurityTypeValuesServiceImpl(final GlobalPermissionManager globalPermissionManager,
                                         final PermissionTypeManager permissionTypeManager,
                                         final I18nHelper i18nHelper,
                                         final HelpUrls helpUrls) {
        this.globalPermissionManager = globalPermissionManager;
        this.permissionTypeManager = permissionTypeManager;
        this.i18nHelper = i18nHelper;
        this.urls = helpUrls;
    }

    private final static ImmutableList<String> PRIMARY_SECURITY_TYPES = ImmutableList.of(
            SecurityTypeKeys.PROJECT_ROLE.getKey(),
            SecurityTypeKeys.APPLICATION_ROLE.getKey(),
            SecurityTypeKeys.GROUP.getKey());


    @Override
    public List<SecurityTypeBean> buildPrimarySecurityTypes(final ApplicationUser user) {
        List<SecurityTypeBean> list = new ArrayList<>();
        for (String securityTypeKey : PRIMARY_SECURITY_TYPES) {
            SecurityType securityType = permissionTypeManager.getSchemeType(securityTypeKey);
            SecurityTypeBean securityTypeBean = makeSecurityTypeBean(user, securityTypeKey, securityType);
            list.add(securityTypeBean);
        }
        return list;

    }

    @Override
    public List<SecurityTypeBean> buildSecondarySecurityTypes(final ApplicationUser user) {
        List<SecurityTypeBean> list = new ArrayList<>();
        Map<String, SecurityType> securityTypes = permissionTypeManager.getSecurityTypes();
        for (Map.Entry<String, SecurityType> entry : securityTypes.entrySet()) {
            String securityTypeKey = entry.getKey();
            if (!PRIMARY_SECURITY_TYPES.contains(securityTypeKey)) {
                SecurityType securityType = permissionTypeManager.getSchemeType(securityTypeKey);
                SecurityTypeBean securityTypeBean = makeSecurityTypeBean(user, securityTypeKey, securityType);
                list.add(securityTypeBean);
            }
        }
        return list;
    }

    private ProjectPermissionHelpBean getSecurityHelp(final ApplicationUser user, final SecurityType securityType) {
        String securityTypeKey = securityType.getType();
        if (SecurityTypeKeys.USER.getKey().equals(securityTypeKey)) {
            final Boolean hasBrowseUsers = this.globalPermissionManager.hasPermission(GlobalPermissionKey.USER_PICKER, user);

            if (!hasBrowseUsers) {
                return new ProjectPermissionHelpBean(i18nHelper.getText("user.picker.no.permission.long"),
                        i18nHelper.getText("user.picker.no.permission.help"), urls.getUrl("global_permissions").getUrl());
            }
        }

        return null;
    }

    private List<SecurityTypeValueBean> getAddValues(SecurityType securityType) {
        String securityTypeKey = securityType.getType();
        if (SecurityTypeKeys.PROJECT_ROLE.getKey().equals(securityTypeKey)) {
            return buildProjectRoles((ProjectRoleSecurityAndNotificationType) securityType);
        }
        if (SecurityTypeKeys.APPLICATION_ROLE.getKey().equals(securityTypeKey)) {
            return buildApplicationRoleSecurityType((ApplicationRoleSecurityType) securityType);
        }
        if (SecurityTypeKeys.USER_CF.getKey().equals(securityTypeKey)) {
            return buildUserCFs((UserCF) securityType);
        }
        if (SecurityTypeKeys.GROUP_CF.getKey().equals(securityTypeKey)) {
            return buildGroupCFs((GroupCF) securityType);
        }
        // all the rest are no variable input
        return Collections.emptyList();
    }


    private SecurityTypeBean makeSecurityTypeBean(final ApplicationUser user, final String type, final SecurityType securityType) {
        return SecurityTypeBean.builder()
                .setSecurityType(type)
                .setDisplayName(securityType.getDisplayName())
                .setValues(getAddValues(securityType))
                .setHelp(getSecurityHelp(user, securityType))
                .build();
    }


    private List<SecurityTypeValueBean> buildApplicationRoleSecurityType(final ApplicationRoleSecurityType securityType) {
        Set<ApplicationRole> applicationRoles = securityType.getApplicationRoles();
        return Lists.newArrayList(Iterables.transform(applicationRoles, role ->
                SecurityTypeValueBean.builder()
                        .setValue(role.getKey().value())
                        .setDisplayValue(role.getName())
                        .build()));
    }

    private List<SecurityTypeValueBean> buildGroupCFs(final GroupCF securityType) {
        Collection<Field> fields = securityType.getDisplayFields();
        return buildFromFields(fields);
    }

    private List<SecurityTypeValueBean> buildUserCFs(final UserCF securityType) {
        Collection<Field> fields = securityType.getDisplayFields();
        return buildFromFields(fields);
    }

    private List<SecurityTypeValueBean> buildFromFields(final Collection<Field> fields) {
        return Lists.newArrayList(Iterables.transform(fields, field ->
                SecurityTypeValueBean.builder()
                        .setValue(String.valueOf(field.getId()))
                        .setDisplayValue(field.getName())
                        .build()));
    }


    private List<SecurityTypeValueBean> buildProjectRoles(final ProjectRoleSecurityAndNotificationType securityType) {
        Collection<ProjectRole> projectRoles = securityType.getProjectRoles();
        return Lists.newArrayList(Iterables.transform(projectRoles, projectRole ->
                SecurityTypeValueBean.builder()
                        .setValue(String.valueOf(projectRole.getId()))
                        .setDisplayValue(projectRole.getName())
                        .build()));
    }

    /**
     * Sorts the grants into a UI specific and consistent order
     *
     * @param grants the list to sort inline
     */
    public void sort(final List<SecurityTypeBean> grants) {
        Comparator<String> primaryComparator = (o1, o2) -> {
            int i1 = PRIMARY_SECURITY_TYPES.indexOf(o1);
            int i2 = PRIMARY_SECURITY_TYPES.indexOf(o2);
            return i1 - i2;
        };

        Collections.sort(grants, new Comparator<SecurityTypeBean>() {
            @Override
            public int compare(final SecurityTypeBean o1, final SecurityTypeBean o2) {
                String securityType1 = o1.getSecurityType();
                String securityType2 = o2.getSecurityType();
                if (PRIMARY_SECURITY_TYPES.contains(securityType1) && PRIMARY_SECURITY_TYPES.contains(securityType2)) {
                    return primaryComparator.compare(securityType1, securityType2);
                }
                if (PRIMARY_SECURITY_TYPES.contains(securityType1) && !PRIMARY_SECURITY_TYPES.contains(securityType2)) {
                    return -1;
                }
                if (!PRIMARY_SECURITY_TYPES.contains(securityType1) && PRIMARY_SECURITY_TYPES.contains(securityType2)) {
                    return 1;
                }
                return securityType1.compareTo(securityType2);
            }
        });
    }
}
