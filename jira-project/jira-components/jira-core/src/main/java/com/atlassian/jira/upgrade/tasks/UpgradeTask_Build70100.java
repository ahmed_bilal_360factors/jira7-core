package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;
import com.atlassian.jira.upgrade.tasks.role.RenaissanceMigration;
import com.atlassian.jira.upgrade.tasks.role.RenaissanceMigrationImpl;
import com.atlassian.jira.util.ComponentFactory;
import com.atlassian.jira.util.Supplier;
import com.google.common.annotations.VisibleForTesting;

import javax.inject.Inject;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * /**
 * Performs renaissance migration (i.e. JIRA 6.x single use permission to JIRA 7.x multiple application). This involves:
 * <p>
 * <ol>
 * <li>Moving JIRA 6.x licenses into the new JIRA 7.0 store.</li>
 * <li>Moving JIRA 6.x users into their associated JIRA 7.0 products.</li>
 * </ol>
 *
 * @see RenaissanceMigrationImpl
 * @see RenaissanceMigration
 * @since v7.0
 */
public class UpgradeTask_Build70100 extends AbstractImmediateUpgradeTask {
    public static final int RENAISSANCE_BUILD_NUMBER = 70100;

    private final Supplier<RenaissanceMigration> supplier;

    @Inject
    public UpgradeTask_Build70100(ComponentFactory componentFactory) {
        this(() -> componentFactory.createObject(RenaissanceMigrationImpl.class));
    }

    @VisibleForTesting
    UpgradeTask_Build70100(Supplier<RenaissanceMigration> supplier) {
        this.supplier = notNull("supplier", supplier);
    }

    @Override
    public int getBuildNumber() {
        return RENAISSANCE_BUILD_NUMBER;
    }

    @Override
    public String getShortDescription() {
        return "Enabling Application Roles";
    }

    @Override
    public void doUpgrade(boolean setupMode) {
        supplier.get().migrate();
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        //We don't want to allow downgrade past this task. Thus this task has this set to true without any downgrade
        //task. We can do this because 7.0 Cloud and 7.0 Server will be released at the same time which means people
        //can move from Cloud to Server and vice versa easily.
        //
        //We can't easily write an automated downgrade task here because there is no obvious way to do so automatically.
        return true;
    }
}