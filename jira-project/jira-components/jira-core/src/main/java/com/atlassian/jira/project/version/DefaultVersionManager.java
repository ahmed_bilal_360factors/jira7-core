package com.atlassian.jira.project.version;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.association.NodeAssocationType;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.bc.project.version.VersionBuilder;
import com.atlassian.jira.bc.project.version.VersionBuilderImpl;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.event.project.VersionArchiveEvent;
import com.atlassian.jira.event.project.VersionCreateEvent;
import com.atlassian.jira.event.project.VersionDeleteEvent;
import com.atlassian.jira.event.project.VersionMergeEvent;
import com.atlassian.jira.event.project.VersionMoveEvent;
import com.atlassian.jira.event.project.VersionReleaseEvent;
import com.atlassian.jira.event.project.VersionUnarchiveEvent;
import com.atlassian.jira.event.project.VersionUnreleaseEvent;
import com.atlassian.jira.event.project.VersionUpdatedEvent;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.NotFoundException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.IssueRelationConstants;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.UpdateIssueRequest;
import com.atlassian.jira.issue.fields.AffectedVersionsSystemField;
import com.atlassian.jira.issue.fields.FixVersionsSystemField;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.model.querydsl.QNodeAssociation;
import com.atlassian.jira.model.querydsl.QVersion;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.DeleteVersionWithCustomFieldParameters.CustomFieldReplacement;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.CollectionReorderer;
import com.atlassian.jira.util.Function;
import com.atlassian.jira.util.Longs;
import com.atlassian.jira.util.NamedPredicates;
import com.atlassian.jira.util.ValidationFailureException;
import com.atlassian.jira.util.dbc.Assertions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.opensymphony.util.TextUtils;
import com.querydsl.sql.SQLExpressions;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.atlassian.collectors.CollectorsUtil.toNewArrayListWithSizeOf;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.jira.issue.IssueRelationConstants.VERSION;
import static com.atlassian.jira.project.version.VersionCollectionManipulators.updateValueIfChangedAfterTranformation;
import static com.atlassian.jira.project.version.VersionCollectionManipulators.versionRemover;
import static com.atlassian.jira.project.version.VersionCollectionManipulators.versionReplacer;
import static com.atlassian.jira.project.version.VersionPredicates.isArchived;
import static com.atlassian.jira.project.version.VersionPredicates.isReleased;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toCollection;

public class DefaultVersionManager implements VersionManager {
    private static final Logger log = LoggerFactory.getLogger(DefaultVersionManager.class);

    private final IssueManager issueManager;
    private final NodeAssociationStore nodeAssociationStore;
    private final IssueIndexingService issueIndexer;
    private final VersionStore versionStore;
    private final EventPublisher eventPublisher;
    private final DbConnectionManager dbConnectionManager;
    private final VersionCustomFieldStore versionCustomFieldStore;

    public DefaultVersionManager(
            final IssueManager issueManager,
            final NodeAssociationStore nodeAssociationStore,
            final IssueIndexingService issueIndexService,
            final VersionStore versionStore,
            final EventPublisher eventPublisher,
            final DbConnectionManager dbConnectionManager,
            final VersionCustomFieldStore versionCustomFieldStore) {
        this.issueManager = issueManager;
        this.nodeAssociationStore = nodeAssociationStore;
        this.issueIndexer = issueIndexService;
        this.versionStore = versionStore;
        this.eventPublisher = eventPublisher;
        this.dbConnectionManager = dbConnectionManager;
        this.versionCustomFieldStore = versionCustomFieldStore;
    }

    /**
     * @deprecated since version 6.0
     */
    @Deprecated
    public Version createVersion(final String name, final Date releaseDate, final String description, final GenericValue project, final Long scheduleAfterVersion)
            throws CreateException {
        if (project == null) {
            throw new CreateException("You cannot create a version without a project.");
        }
        return createVersion(name, releaseDate, description, project.getLong("id"), scheduleAfterVersion);
    }

    @Override
    public Version createVersion(final String name, final Date releaseDate, final String description, final Long projectId, final Long scheduleAfterVersion)
            throws CreateException {
        return createVersion(name, null, releaseDate, description, projectId, scheduleAfterVersion);
    }

    @Override
    public Version createVersion(final String name, final Date startDate, final Date releaseDate, final String description, final Long projectId, final Long scheduleAfterVersion)
            throws CreateException {
        return createVersion(name, startDate, releaseDate, description, projectId, scheduleAfterVersion, false);
    }

    @Override
    public Version createVersion(final String name, final Date startDate, final Date releaseDate,
                                 final String description, final Long projectId, final Long scheduleAfterVersion,
                                 boolean released)
            throws CreateException {
        if (!TextUtils.stringSet(name)) {
            throw new CreateException("You cannot create a version without a name.");
        }

        if (projectId == null) {
            throw new CreateException("You cannot create a version without a project.");
        }

        final VersionBuilder builder = new VersionBuilderImpl()
                .projectId(projectId)
                .name(name)
                .description(description)
                .released(released);

        if (startDate != null) {
            builder.startDate(new Timestamp(startDate.getTime()));
        }

        if (releaseDate != null) {
            builder.releaseDate(new Timestamp(releaseDate.getTime()));
        }

        configureSchedulingOrderForVersion(projectId, scheduleAfterVersion, builder);

        final Version newVersion = versionStore.createVersion(builder.build());
        eventPublisher.publish(new VersionCreateEvent(newVersion));
        return newVersion;
    }

    private void configureSchedulingOrderForVersion(Long projectId, Long scheduleAfterVersion, VersionBuilder builder) {
        // This determines where in the scheduling order the new version should be placed.
        if (scheduleAfterVersion != null) {
            if (scheduleAfterVersion == -1L) {
                // Decrease all version sequences in order to place new version in first position
                moveAllVersionSequences(projectId);
                // New version sequence will be first position
                builder.sequence(1L);
            } else {
                // Decrease version sequences which follow this version in order to slot in new version
                moveVersionSequences(scheduleAfterVersion);
                // New version sequence will follow this version
                final Long newSequence = getVersion(scheduleAfterVersion).getSequence() + 1L;
                builder.sequence(newSequence);
            }
        } else {
            builder.sequence(getMaxVersionSequence(projectId));
        }
    }

    // ---- Scheduling Methods ----
    @Override
    public Version moveToStartVersionSequence(final Version version) {
        final List<Version> versions = new ArrayList<>(getAllVersions(version));
        CollectionReorderer.moveToStart(versions, version);
        storeReorderedVersionList(versions);
        eventPublisher.publish(new VersionMoveEvent(version));
        return getVersion(version.getId());
    }

    @Override
    public Version increaseVersionSequence(final Version version) {
        final List<Version> versions = new ArrayList<>(getAllVersions(version));
        CollectionReorderer.increasePosition(versions, version);
        storeReorderedVersionList(versions);
        eventPublisher.publish(new VersionMoveEvent(version));
        return getVersion(version.getId());
    }

    @Override
    public Version decreaseVersionSequence(final Version version) {
        final List<Version> versions = new ArrayList<>(getAllVersions(version));
        CollectionReorderer.decreasePosition(versions, version);
        storeReorderedVersionList(versions);
        eventPublisher.publish(new VersionMoveEvent(version));
        return getVersion(version.getId());
    }

    @Override
    public Version moveToEndVersionSequence(final Version version) {
        final List<Version> versions = new ArrayList<>(getAllVersions(version));
        CollectionReorderer.moveToEnd(versions, version);
        storeReorderedVersionList(versions);
        eventPublisher.publish(new VersionMoveEvent(version));
        return getVersion(version.getId());
    }

    @Override
    public Version moveVersionAfter(final Version version, final Long scheduleAfterVersionId) {
        if (version == null) {
            throw new IllegalArgumentException("You cannot move a null version");
        }

        //Don't re-schedule if the scheduleAfterVersion id is of itself (Note: scheduleAfterVersion can be null)
        if ((version.getId() != null) && !version.getId().equals(scheduleAfterVersionId)) {
            final Version targetVersion;
            if (scheduleAfterVersionId == null) {
                targetVersion = getLastVersion(version.getProjectId());//move to last version
            } else if (scheduleAfterVersionId == -1L) {
                targetVersion = null;//move to start
            } else {
                //move to the target version
                targetVersion = getVersion(scheduleAfterVersionId);
            }

            final List<Version> versions = new ArrayList<Version>(getAllVersions(version));
            CollectionReorderer.moveToPositionAfter(versions, version, targetVersion);
            storeReorderedVersionList(versions);
            eventPublisher.publish(new VersionMoveEvent(version));
            return getVersion(version.getId());
        }
        return version;
    }

    // Increases sequence numbers for versions (make them later) to make space for new version
    private void moveVersionSequences(final Long scheduleAfterVersion) {
        final Version startVersion = getVersion(scheduleAfterVersion);
        final Collection<Version> versions = getVersions(startVersion.getProjectId());
        final List<Version> versionsChanged = Lists.newArrayListWithCapacity(versions.size());

        for (final Version version : versions) {
            if (version.getSequence() > startVersion.getSequence()) {
                final Long newSequence = version.getSequence() + 1L;
                versionsChanged.add(new VersionBuilderImpl(version).sequence(newSequence).build());
            }
        }
        versionStore.storeVersions(versionsChanged);
    }

    // Increases sequence numbers for all versions (make them later) to make space for new version
    private void moveAllVersionSequences(final Long project) {
        final Collection<Version> versions = getVersions(project);
        final List<Version> versionsChanged = Lists.newArrayListWithCapacity(versions.size());

        for (final Version version : versions) {
            final Long newSequence = version.getSequence() + 1L;
            versionsChanged.add(new VersionBuilderImpl(version).sequence(newSequence).build());
        }

        versionStore.storeVersions(versionsChanged);
    }

    @Override
    public void deleteVersion(final Version version) {
        deleteVersionWithoutPublishingAnEvent(version);
        eventPublisher.publish(VersionDeleteEvent.deleted(version));
    }

    @Override
    public void deleteVersion(final ApplicationUser applicationUser,
                              final Version versionToDelete,
                              final Option<Version> affectsSwapVersion,
                              final Option<Version> fixSwapVersion) {
        swapVersionForRelatedIssues(applicationUser, versionToDelete, affectsSwapVersion, fixSwapVersion);
        deleteVersionWithoutPublishingAnEvent(versionToDelete);
        eventPublisher.publish(new VersionDeleteEvent.VersionDeleteEventBuilder(versionToDelete)
                .affectsVersionSwappedTo(affectsSwapVersion.getOrNull())
                .fixVersionSwappedTo(fixSwapVersion.getOrNull())
                .createEvent());
    }

    private void deleteVersionWithoutPublishingAnEvent(final Version version) {
        versionStore.deleteVersion(version);

        //JRA-13766: We need to get all the versions, and re-store the versions where sequence numbers are not
        //correct.
        reorderVersionsInProject(version);
    }

    @Override
    public void deleteAllVersions(@Nonnull final Long projectId) {
        dbConnectionManager.execute(callback -> {
                    QNodeAssociation na = QNodeAssociation.NODE_ASSOCIATION;
                    QVersion v = QVersion.VERSION;

                    callback
                            .delete(na)
                            .where(na.sinkNodeEntity.eq(VERSION)
                                    .and(na.sinkNodeId.in(SQLExpressions
                                            .select(v.id)
                                            .from(v)
                                            .where(v.project.eq(projectId)))))
                            .execute();
                }
        );

        versionStore.deleteAllVersions(projectId);
    }

    @Override
    public Version update(final Version version) {
        final Version originalVersion = getVersion(version.getId());
        versionStore.storeVersion(version);
        eventPublisher.publish(new VersionUpdatedEvent(version, originalVersion));
        return version;
    }

    @Override
    public Version editVersionDetails(final Version version, final String name, final String description) {
        //There must be a name for the entity
        if (!TextUtils.stringSet(name)) {
            throw new IllegalArgumentException("You must specify a valid version name.");
        } else {
            //if the name already exists then add an Error message
            if (isDuplicateName(version, name)) {
                throw new IllegalArgumentException("A version with this name already exists in this project.");
            }
        }

        final VersionBuilderImpl versionInputParams = new VersionBuilderImpl(version);
        versionInputParams.name(name);
        versionInputParams.description(description);

        final Version newVersion = versionInputParams.build();
        versionStore.storeVersion(newVersion);
        return newVersion;
    }

    // ---- Release Version methods ----
    @Override
    public Version releaseVersion(final Version version, final boolean release) {
        releaseVersions(Collections.singleton(version), release);

        final Version updatedVersion = getVersion(version.getId());
        if (release) {
            eventPublisher.publish(new VersionReleaseEvent(updatedVersion));
        } else {
            eventPublisher.publish(new VersionUnreleaseEvent(updatedVersion));
        }
        return updatedVersion;
    }

    @Override
    public Collection<Version> releaseVersions(final Collection<Version> versions, final boolean release) {
        final List<Version> versionsChanged = Lists.newArrayListWithCapacity(versions.size());
        for (final Version version : versions) {
            validateReleaseParams(version, release);
            versionsChanged.add(new VersionBuilderImpl(version).released(release).build());
        }

        if (!versionsChanged.isEmpty()) {
            versionStore.storeVersions(versionsChanged);
        }
        return versionsChanged;
    }

    @Override
    public void moveIssuesToNewVersion(final List<Issue> issues, final Version currentVersion, final Version swapToVersion)
            throws IndexException {
        if (currentVersion != null && swapToVersion != null && !issues.isEmpty()) {
            nodeAssociationStore.swapAssociation((List<GenericValue>)
                            issues.stream().map(issue -> issue.getGenericValue()).collect(toCollection(ArrayList::new)), IssueRelationConstants.FIX_VERSION,
                    ((VersionImpl) currentVersion).toGenericValue(), ((VersionImpl) swapToVersion).toGenericValue());

            issueIndexer.reIndexIssueObjects(issues);
        }
    }

    // ---- Archive Version methods ----
    @Override
    public void archiveVersions(final String[] idsToArchive, final String[] idsToUnarchive) {
        final List<Version> versionsChanged = Lists.newArrayListWithCapacity(idsToArchive.length);

        for (String anIdsToArchive : idsToArchive) {
            final Long archiveId = new Long(anIdsToArchive);
            final Version version = getVersion(archiveId);
            if ((version != null) && !version.isArchived()) {
                versionsChanged.add(new VersionBuilderImpl(version).archived(true).build());
            }
        }

        for (String anIdsToUnarchive : idsToUnarchive) {
            final Long unArchiveId = new Long(anIdsToUnarchive);
            final Version version = getVersion(unArchiveId);
            if ((version != null) && version.isArchived()) {
                versionsChanged.add(new VersionBuilderImpl(version).archived(false).build());
            }
        }

        if (!versionsChanged.isEmpty()) {
            versionStore.storeVersions(versionsChanged);
        }
    }

    @Override
    public Version archiveVersion(final Version version, final boolean archive) {
        final Version newVersion = new VersionBuilderImpl(version).archived(archive).build();
        versionStore.storeVersion(newVersion);

        final Version updatedVersion = getVersion(version.getId());
        if (archive) {
            eventPublisher.publish(new VersionArchiveEvent(updatedVersion));
        } else {
            eventPublisher.publish(new VersionUnarchiveEvent(updatedVersion));
        }
        return updatedVersion;
    }

    // ---- Version Due Date Mthods ----
    @Override
    public Version editVersionReleaseDate(final Version version, final Date duedate) {
        //Theversion must be specified
        if (version == null) {
            throw new IllegalArgumentException("You must specify a valid version.");
        }

        if (version.getStartDate() != null && duedate != null) {
            if (version.getStartDate().after(duedate)) {
                throw new IllegalArgumentException("Release date must be after the version start date");
            }
        }

        final Version newVersion = new VersionBuilderImpl(version).releaseDate(duedate).build();
        versionStore.storeVersion(newVersion);
        return newVersion;
    }

    @Override
    public Version editVersionStartDate(final Version version, final Date startDate) {
        if (version == null) {
            throw new IllegalArgumentException("You must specify a valid version.");
        }

        if (startDate != null && version.getReleaseDate() != null) {
            if (startDate.after(version.getReleaseDate())) {
                throw new IllegalArgumentException("Start date must be before the version release date");
            }
        }

        final Version newVersion = new VersionBuilderImpl(version).startDate(startDate).build();
        versionStore.storeVersion(newVersion);
        return newVersion;
    }

    @Override
    public Version editVersionStartReleaseDate(final Version version, final Date startDate, final Date releaseDate) {
        if (version == null) {
            throw new IllegalArgumentException("You must specify a valid version.");
        }

        if (startDate != null && releaseDate != null) {
            if (startDate.after(releaseDate)) {
                throw new IllegalArgumentException("Start date must be before the version release date");
            }
        }

        final Version newVersion = new VersionBuilderImpl(version).startDate(startDate).releaseDate(releaseDate).build();
        versionStore.storeVersion(newVersion);
        return newVersion;
    }

    @Override
    public boolean isVersionOverDue(final Version version) {
        if (version.getReleaseDate() == null || version.isArchived() || version.isReleased()) {
            return false;
        }
        final Calendar releaseDate = Calendar.getInstance();
        releaseDate.setTime(version.getReleaseDate());

        final Calendar lastMidnight = Calendar.getInstance();
        lastMidnight.set(Calendar.HOUR_OF_DAY, 0);
        lastMidnight.set(Calendar.MINUTE, 0);
        lastMidnight.set(Calendar.SECOND, 0);
        lastMidnight.set(Calendar.MILLISECOND, 0);

        return releaseDate.before(lastMidnight);
    }

    // ---- Get Version(s) methods ----
    @Override
    public Collection<Version> getVersionsUnarchived(final Long projectId) {
        final Iterable<Version> versions = versionStore.getVersionsByProject(projectId);
        return filterVersions(versions, Predicates.not(isArchived()));
    }

    @Nonnull
    private List<Version> filterVersions(@Nonnull final Iterable<Version> versions, @Nonnull final Predicate<Version> predicate) {
        return newArrayList(Iterables.filter(versions, predicate));
    }

    @Override
    public Collection<Version> getVersionsArchived(final Project project) {
        final Iterable<Version> versions = versionStore.getVersionsByProject(project.getId());
        return filterVersions(versions, isArchived());
    }

    @Override
    public List<Version> getVersions(final Long projectId) {
        Assertions.notNull("projectId", projectId);
        return copyOf(versionStore.getVersionsByProject(projectId));
    }

    @Override
    public List<Version> getVersions(final Long projectId, final boolean includeArchived) {
        Assertions.notNull("projectId", projectId);
        final Iterable<Version> versions = versionStore.getVersionsByProject(projectId);
        Predicate<Version> predicate = Predicates.alwaysTrue();

        if (!includeArchived) {
            predicate = Predicates.not(isArchived());
        }

        return filterVersions(versions, predicate);
    }

    @Override
    public List<Version> getVersions(final Project project) {
        return getVersions(project.getId());
    }

    @Override
    public Collection<Version> getVersionsByName(final String versionName) {
        Assertions.notNull("versionName", versionName);
        return copyOf(versionStore.getVersionsByName(versionName));
    }

    @Override
    public Collection<Version> getAffectedVersionsFor(final Issue issue) {
        return getVersionsByIssue(issue.getGenericValue(), VERSION);
    }

    @Override
    public List<ChangeItemBean> updateIssueAffectsVersions(final Issue issue, final Collection<Version> newVersions) {
        return updateIssueValue(issue, newVersions, getAffectedVersionsFor(issue), NodeAssocationType.ISSUE_TO_AFFECTS_VERISON, AffectedVersionsSystemField.CHANGE_ITEM_FIELD);
    }

    @Override
    public List<ChangeItemBean> updateIssueFixVersions(final Issue issue, final Collection<Version> newValue) {
        return updateIssueValue(issue, newValue, getFixVersionsFor(issue), NodeAssocationType.ISSUE_TO_FIX_VERISON, FixVersionsSystemField.CHANGE_ITEM_FIELD);
    }

    private List<ChangeItemBean> updateIssueValue(final Issue issue, Collection<Version> newVersions, final Collection<Version> oldVersions, final NodeAssocationType nodeAssocationType, final String changeItemField) {
        if (newVersions == null) {
            newVersions = Collections.emptyList();
        }

        final List<ChangeItemBean> changes = new ArrayList<>(newVersions.size());

        // compare the ids not the object values - JRA-12130
        final Collection<Long> oldVersionIds = oldVersions.stream().map(Version::getId).collect(toNewArrayListWithSizeOf(oldVersions));
        final Collection<Long> newVersionIds = newVersions.stream().map(Version::getId).collect(toNewArrayListWithSizeOf(newVersions));

        newVersions.stream().filter(v -> !oldVersionIds.contains(v.getId())).forEach(v -> {
            nodeAssociationStore.createAssociation(nodeAssocationType, issue.getId(), v.getId());
            changes.add(new ChangeItemBean(ChangeItemBean.STATIC_FIELD, changeItemField, null, null, v.getId().toString(), v.getName()));
        });

        // loop through existing entities and remove any that aren't in new entities
        oldVersions.stream().filter(v -> !newVersionIds.contains(v.getId())).forEach(v -> {
            nodeAssociationStore.removeAssociation(nodeAssocationType, issue.getId(), v.getId());
            changes.add(new ChangeItemBean(ChangeItemBean.STATIC_FIELD, changeItemField, v.getId().toString(), v.getName(), null, null));
        });

        return changes;
    }

    @Override
    public Collection<Version> getFixVersionsFor(final Issue issue) {
        return getVersionsByIssue(issue.getGenericValue(), IssueRelationConstants.FIX_VERSION);
    }

    public Collection<Version> getAllVersions() {
        return copyOf(versionStore.getAllVersions());
    }

    @Override
    public Collection<Version> getAllVersionsForProjects(final Collection<Project> projects, final boolean includeArchived) {
        if (projects.isEmpty()) {
            return Collections.emptyList();
        }

        Iterable<Version> allVersions = Collections.emptyList();
        for (Project project : projects) {
            final Iterable<Version> projectVersions = versionStore.getVersionsByProject(project.getId());
            allVersions = Iterables.concat(allVersions, projectVersions);
        }

        Predicate<Version> predicate = Predicates.alwaysTrue();
        if (!includeArchived) {
            predicate = Predicates.not(isArchived());
        }

        return filterVersions(allVersions, predicate);
    }

    @Override
    public Collection<Version> getAllVersionsReleased(final boolean includeArchived) {
        Predicate<Version> predicate = isReleased();
        if (!includeArchived) {
            predicate = Predicates.and(predicate, Predicates.not(isArchived()));
        }
        return filterVersions(versionStore.getAllVersions(), predicate);
    }

    @Override
    public Collection<Version> getAllVersionsUnreleased(final boolean includeArchived) {
        Predicate<Version> predicate = Predicates.not(isReleased());
        if (!includeArchived) {
            predicate = Predicates.and(predicate, Predicates.not(isArchived()));
        }
        return filterVersions(versionStore.getAllVersions(), predicate);
    }

    @Override
    public void merge(final ApplicationUser user, @Nonnull final Version versionToDelete, @Nonnull final Version versionToMergeTo) {
        final boolean affectedIssues = swapOrRemoveVersionsFromIssues(user, versionToDelete, some(versionToMergeTo), some(versionToMergeTo), some(versionToMergeTo));
        if (affectedIssues) // this seems a bit pointless but at the moment of writing we are unsure if this behaviour can be safely changed (so that the event will be always thrown)
        {
            eventPublisher.publish(new VersionMergeEvent(versionToMergeTo, versionToDelete));
        }
        deleteVersionWithoutPublishingAnEvent(versionToDelete);
        eventPublisher.publish(VersionDeleteEvent.deletedAndMerged(versionToDelete, versionToMergeTo));
    }

    @Override
    public void deleteAndRemoveFromIssues(final ApplicationUser user, @Nonnull final Version versionToRemove) {
        swapOrRemoveVersionsFromIssues(user, versionToRemove, Option.<Version>none(), Option.<Version>none(), Option.none());
        deleteVersion(versionToRemove);
    }

    @Override
    public void swapVersionForRelatedIssues(final ApplicationUser user, final Version version, final Option<Version> affectsSwapVersion, final Option<Version> fixSwapVersion) {
        final boolean affectedIssues = swapOrRemoveVersionsFromIssues(user, version, affectsSwapVersion, fixSwapVersion, Option.none());

        if (affectedIssues && fixSwapVersion.isDefined()) {
            eventPublisher.publish(new VersionMergeEvent(fixSwapVersion.get(), version));
        }
    }

    /**
     * @return true if there were any issues affected
     */
    @ParametersAreNonnullByDefault
    private boolean swapOrRemoveVersionsFromIssues(final ApplicationUser user,
                                                   final Version version,
                                                   final Option<Version> affectsSwapVersion,
                                                   final Option<Version> fixSwapVersion,
                                                   final Option<Version> customFieldSwapVersion) {
        final Set<Long> issuesIds = getAllAssociatedIssueIds(version);

        updateIssueVersionFields(user, version, affectsSwapVersion, fixSwapVersion, versionCustomFieldStore.createValueReplacers(version, customFieldSwapVersion), issuesIds);

        return issuesIds.size() > 0;
    }

    @ParametersAreNonnullByDefault
    private boolean swapOrRemoveVersionsFromIssuesWithCustomFieldsSpec(final ApplicationUser user,
                                                                       final Version version,
                                                                       final Option<Version> affectsSwapVersion,
                                                                       final Option<Version> fixSwapVersion,
                                                                       final List<CustomFieldReplacement> customFieldReplacementList) {
        final Set<Long> issuesIds = getAllAssociatedIssueIds(version);

        final List<CustomFieldVersionTransformation> customFieldReplacers = versionCustomFieldStore.createValueReplacers(version, customFieldReplacementList);

        updateIssueVersionFields(user, version, affectsSwapVersion, fixSwapVersion, customFieldReplacers, issuesIds);

        return issuesIds.size() > 0;
    }

    @ParametersAreNonnullByDefault
    private void updateIssueVersionFields(final ApplicationUser user,
                                          final Version version,
                                          final Option<Version> affectsSwapVersion,
                                          final Option<Version> fixSwapVersion,
                                          final List<CustomFieldVersionTransformation> customFieldReplacers,
                                          final Set<Long> issuesIds) {
        final Function<Collection<Version>, Collection<Version>> affectedVersionUpdate = affectsSwapVersion.fold(
                () -> versionRemover(version),
                newVersion -> versionReplacer(version, newVersion)
        );
        final Function<Collection<Version>, Collection<Version>> fixVersionUpdate = fixSwapVersion.fold(
                () -> versionRemover(version),
                newVersion -> versionReplacer(version, newVersion)
        );

        // swap the versions on the affected issues
        for (final Long issueId : issuesIds) {
            final MutableIssue newIssue = issueManager.getIssueObject(issueId);

            updateValueIfChangedAfterTranformation(
                    newIssue.getAffectedVersions(), affectedVersionUpdate, newIssue::setAffectedVersions);
            updateValueIfChangedAfterTranformation(
                    newIssue.getFixVersions(), fixVersionUpdate, newIssue::setFixVersions);

            customFieldReplacers.forEach(replacer -> replacer.updateVersionFieldInIssue(newIssue));

            final Issue updated = issueManager.updateIssue(user, newIssue, UpdateIssueRequest.builder().
                    eventDispatchOption(EventDispatchOption.ISSUE_UPDATED).
                    sendMail(false).build());

            try {
                issueIndexer.reIndex(updated);
            } catch (IndexException e) {
                log.warn("Could not reindex issue.", e);
            }
        }
    }

    private Set<Long> getAllAssociatedIssueIds(final Version version) {
        final Set<Long> issueIds = Sets.newHashSet();
        issueIds.addAll(getIssueIdsWithAffectsVersion(version));
        issueIds.addAll(getIssueIdsWithFixVersion(version));
        issueIds.addAll(versionCustomFieldStore.getIssueIdsWithCustomFieldsFor(version));
        return issueIds;
    }

    /**
     * @param issue        the issue
     * @param relationName {@link IssueRelationConstants#VERSION} or {@link IssueRelationConstants#FIX_VERSION}.
     * @return A collection of {@link Version}s for this issue.
     */
    protected Collection<Version> getVersionsByIssue(final GenericValue issue, final String relationName) {
        final List<GenericValue> versionGVs = nodeAssociationStore.getSinksFromSource(issue, "Version", relationName);
        return Entity.VERSION.buildList(versionGVs);
    }

    @Override
    public Collection<Version> getVersions(final List<Long> ids) {
        final List<Version> versions = Lists.newArrayListWithCapacity(ids.size());
        for (final Long id : ids) {
            versions.add(getVersion(id));
        }

        return versions;
    }

    @Override
    public Collection<Version> getVersionsUnreleased(final Long projectId, final boolean includeArchived) {
        final Iterable<Version> versions = versionStore.getVersionsByProject(projectId);

        Predicate<Version> predicate = Predicates.not(isReleased());
        if (!includeArchived) {
            predicate = Predicates.and(predicate, Predicates.not(isArchived()));
        }
        return filterVersions(versions, predicate);
    }

    @Override
    public Collection<Version> getVersionsReleased(final Long projectId, final boolean includeArchived) {
        final Iterable<Version> versions = versionStore.getVersionsByProject(projectId);

        Predicate<Version> predicate = isReleased();
        if (!includeArchived) {
            predicate = Predicates.and(predicate, Predicates.not(isArchived()));
        }
        return filterVersions(versions, predicate);
    }

    @Override
    public Collection<Version> getVersionsReleasedDesc(final Long projectId, final boolean includeArchived) {
        final List<Version> released = new ArrayList<Version>(getVersionsReleased(projectId, includeArchived));
        Collections.reverse(released);
        return released;
    }

    @Override
    public Version getVersion(final Long id) {
        return versionStore.getVersion(id);
    }

    /**
     * Retrieve a specific Version in a project given the project id, or <code>null</code> if no such version exists in
     * that project.
     */
    @Override
    public Version getVersion(final Long projectId, final String versionName) {
        final Iterable<Version> versions = Iterables.filter(versionStore.getVersionsByProject(projectId), NamedPredicates.equalsIgnoreCase(versionName));
        return Iterables.getFirst(versions, null);
    }

    /**
     * NB: This is done because we can't inject a {@link IssueFactory}, this would cause circular dependency.
     */
    protected IssueFactory getIssueFactory() {
        return ComponentAccessor.getIssueFactory();
    }

    /**
     * Return all other versions in the project except this one
     */
    @Override
    public Collection<Version> getOtherVersions(final Version version) {
        final Collection<Version> otherVersions = new ArrayList<Version>(getAllVersions(version));
        otherVersions.remove(version);
        return otherVersions;
    }

    /**
     * Return all unarchived versions except this one
     */
    @Override
    public Collection<Version> getOtherUnarchivedVersions(final Version version) {
        final Collection<Version> otherUnarchivedVersions = new ArrayList<Version>(getVersionsUnarchived(version.getProjectId()));
        otherUnarchivedVersions.remove(version);
        return otherUnarchivedVersions;
    }

    @Override
    public Collection<Issue> getIssuesWithFixVersion(Version version) {
        final Collection<Long> issueIds = getIssueIdsWithFixVersion(version);
        final Collection<Issue> issues = new ArrayList<Issue>(issueIds.size());
        for (Long issueId : issueIds) {
            issues.add(issueManager.getIssueObject(issueId));
        }
        return issues;
    }

    @Override
    public Collection<Issue> getIssuesWithAffectsVersion(Version version) {
        final Collection<Long> issueIds = getIssueIdsWithAffectsVersion(version);
        final Collection<Issue> issues = new ArrayList<Issue>(issueIds.size());
        for (Long issueId : issueIds) {
            issues.add(issueManager.getIssueObject(issueId));
        }
        return issues;
    }

    @Override
    public Collection<Long> getIssueIdsWithAffectsVersion(@Nonnull final Version version) {
        return nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_AFFECTS_VERISON, version.getId());
    }

    @Override
    public Collection<Long> getIssueIdsWithFixVersion(@Nonnull final Version version) {
        return nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_FIX_VERISON, version.getId());
    }

    @Override
    @Nonnull
    public Collection<CustomFieldWithVersionUsage> getCustomFieldsUsing(@Nonnull final Version version) {

        return (Collection<CustomFieldWithVersionUsage>) versionCustomFieldStore.getCustomFieldsUsing(version);
    }

    @Override
    public long getCustomFieldIssuesCount(@Nonnull Version version) {
        return versionCustomFieldStore.getCustomFieldIssuesCount(version);
    }

    /**
     * <p><b>Testing notes</b> - side effects (reindexing, events) are tested in unitTest, actual version removal
     * is tested in {@code com.atlassian.jira.webtests.ztests.bundledplugins2.rest.TestVersionResource}.
     */
    @Override
    public void deleteVersionAndSwap(final ApplicationUser applicationUser, DeleteVersionWithCustomFieldParameters parameters) {


        validateDeleteAndReplaceParameters(parameters);

        final Version versionToDelete = parameters.getVersionToDelete();
        final Option<Version> affectsSwapVersion = Option.option(parameters.getMoveAffectedIssuesTo().orElse(null));
        final Option<Version> fixSwapVersion = Option.option(parameters.getMoveFixIssuesTo().orElse(null));

        swapOrRemoveVersionsFromIssuesWithCustomFieldsSpec(
                applicationUser,
                versionToDelete,
                affectsSwapVersion,
                fixSwapVersion,
                parameters.getCustomFieldReplacementList());

        deleteVersionWithoutPublishingAnEvent(versionToDelete);
        eventPublisher.publish(new VersionDeleteEvent.VersionDeleteEventBuilder(versionToDelete)
                .affectsVersionSwappedTo(affectsSwapVersion.getOrNull())
                .fixVersionSwappedTo(fixSwapVersion.getOrNull())
                .customFieldReplacements(parameters.getCustomFieldReplacementList())
                .createEvent());
    }

    private void validateDeleteAndReplaceParameters(DeleteVersionWithCustomFieldParameters parameters) {
        if (null == parameters.getVersionToDelete().getId()) {
            throw new NotFoundException("Target version not found");
        }

        if (!canVersionsBeSwapped(parameters.getVersionToDelete(), parameters.getMoveFixIssuesTo().orElse(null))) {
            throw new ValidationFailureException("Invalid target version.");
        }
        if (!canVersionsBeSwapped(parameters.getVersionToDelete(), parameters.getMoveAffectedIssuesTo().orElse(null))) {
            throw new ValidationFailureException("Invalid target version.");
        }
        parameters.getCustomFieldReplacementList()
                .stream()
                .filter(
                        replacement -> !canVersionsBeSwapped(parameters.getVersionToDelete(), replacement.getMoveTo().orElse(null))
                )
                .findFirst()
                .ifPresent(badReplacement -> {
                    throw new ValidationFailureException("Invalid target version.");
                });
    }


    private boolean canVersionsBeSwapped(@Nonnull Version versionToDelete, @Nullable Version replacingVersion) {
        if (replacingVersion == null) {
            return true;
        }
        if (!Longs.nullableLongsEquals(versionToDelete.getProjectId(), replacingVersion.getProjectId())) {
            return false;
        }

        return !Longs.nullableLongsEquals(versionToDelete.getId(), replacingVersion.getId());
    }

    /**
     * Returns a sorted list of all versions in the project that this version is in.
     *
     * @param version The version used to determine the project.
     * @return a sorted list of all versions in the project that this version is in.
     */
    private List<Version> getAllVersions(final Version version) {
        return getVersions(version.getProjectId());
    }

    @Override
    public boolean isDuplicateName(final Version currentVersion, final String name) {
        //Chek to see if there is already a version with that name for the project
        for (final Version version : currentVersion.getProjectObject().getVersions()) {
            if (!currentVersion.getId().equals(version.getId()) && name.trim().equalsIgnoreCase(version.getName())) {
                return true;
            }
        }
        return false;
    }

    private void validateReleaseParams(final Version version, final boolean release) {
        if ((version == null) && release) {
            throw new IllegalArgumentException("Please select a version to release");
        } else if ((version == null) && !release) {
            throw new IllegalArgumentException("Please select a version to unrelease.");
        }
    }

    private Version getLastVersion(final Long projectId) {
        long maxSequence = 0L;
        Version lastVersion = null;

        for (final Version version : getVersions(projectId)) {
            if ((version.getSequence() != null) && (version.getSequence() >= maxSequence)) {
                maxSequence = version.getSequence();
                lastVersion = version;
            }
        }
        return lastVersion;
    }

    private long getMaxVersionSequence(final Long projectId) {
        long maxSequence = 1L;

        for (final Version version : getVersions(projectId)) {
            if ((version.getSequence() != null) && (version.getSequence() >= maxSequence)) {
                maxSequence = version.getSequence() + 1L;
            }
        }
        return maxSequence;
    }

    /**
     * Given a re-ordered list of versions, commit the changes to the backend datastore.
     */
    public void storeReorderedVersionList(final List<Version> versions) {
        final List<Version> versionsChanged = Lists.newArrayListWithCapacity(versions.size());

        for (int i = 0; i < versions.size(); i++) {
            final Version version = versions.get(i);
            final long expectedSequenceNumber = i + 1L;
            if (expectedSequenceNumber != version.getSequence()) {
                versionsChanged.add(new VersionBuilderImpl(version).sequence(expectedSequenceNumber).build());
            }
        }

        versionStore.storeVersions(versionsChanged);
    }

    /**
     * For testing purposes.
     *
     * @param version The version used to determine the project.
     */
    void reorderVersionsInProject(final Version version) {
        storeReorderedVersionList(getAllVersions(version));
    }
}
