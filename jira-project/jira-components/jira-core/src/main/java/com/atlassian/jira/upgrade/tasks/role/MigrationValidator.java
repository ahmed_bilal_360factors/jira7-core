package com.atlassian.jira.upgrade.tasks.role;

/**
 * This validates the renaissance migration state for application roles before it is committed. If there is an error it
 * will throw the {@link MigrationFailedException} This class mitigates the risk of having a bug in the migration code.
 * It duplicates some of the migration logic on purpose in order to avoid unwanted escalation of user privileges that
 * will protect the customers from being either charged more money or have users having unauthorised permissions.
 *
 * @since v7.0
 */
abstract class MigrationValidator {

    /**
     * Validate that the {@link MigrationState} to ensure that the users are migrated to the correct application roles,
     * and they didn't get any privilege escalation. Users that loose privileges will be logged for later auditing and
     * will not cause the validation to fail.
     *
     * @param original  the original state before the migration started
     * @param resulting state from migration to be validated
     * @throws {@link MigrationFailedException} when it encounters an error with the migration
     */
    abstract void validate(final MigrationState original, MigrationState resulting);
}
