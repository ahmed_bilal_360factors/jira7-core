package com.atlassian.jira.startup;

import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.upgrade.UpgradeVersionHistoryManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.BuildUtilsInfoImpl;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.johnson.event.EventLevel.ERROR;

/**
 * Launcher for the {@link JiraDatabaseConfigChecklist}.
 *
 * @since v4.4
 */
public class DatabaseChecklistLauncher implements JiraLauncher {
    private static final Logger log = LoggerFactory.getLogger(DatabaseChecklistLauncher.class);

    private final DatabaseConfigurationManager dbcm;
    private final JiraProperties jiraSystemProperties;
    private final JohnsonProvider johnsonProvider;
    private final UpgradeVersionHistoryManager upgradeVersionHistoryManager;
    private final BuildUtilsInfo buildUtilsInfo = new BuildUtilsInfoImpl();

    public DatabaseChecklistLauncher(
            final DatabaseConfigurationManager dbcm,
            final JiraProperties jiraSystemProperties,
            final JohnsonProvider johnsonProvider,
            final UpgradeVersionHistoryManager upgradeVersionHistoryManager
    ) {
        this.dbcm = dbcm;
        this.jiraSystemProperties = jiraSystemProperties;
        this.johnsonProvider = johnsonProvider;
        this.upgradeVersionHistoryManager = upgradeVersionHistoryManager;
    }

    @Override
    public void start() {
        final JiraDatabaseConfigChecklist jiraPostDatabaseChecklist =
                new JiraDatabaseConfigChecklist(dbcm, jiraSystemProperties, buildUtilsInfo, upgradeVersionHistoryManager);

        if (jiraPostDatabaseChecklist.startupOK()) {
            log.info("JIRA database startup checks completed successfully.");
        } else {
            final StartupCheck failedCheck = jiraPostDatabaseChecklist.getFailedStartupCheck();
            final String desc = failedCheck.getFaultDescription();
            log.error(failedCheck.getName() + " failed: " + desc);
            log.error("Database startup check failed - this is FATAL - JIRA will not be in a working state.");
            final EventType eventType = EventType.get("database");
            final Event event = new Event(eventType, failedCheck.getHTMLFaultDescription(), EventLevel.get(ERROR));
            johnsonProvider.getContainer().addEvent(event);
        }
    }

    @Override
    public void stop() {
    }
}
