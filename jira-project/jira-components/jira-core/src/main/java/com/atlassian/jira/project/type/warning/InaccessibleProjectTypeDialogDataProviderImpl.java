package com.atlassian.jira.project.type.warning;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.google.common.annotations.VisibleForTesting;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * Default implementation of {@link InaccessibleProjectTypeDialogDataProvider}
 */
public class InaccessibleProjectTypeDialogDataProviderImpl implements InaccessibleProjectTypeDialogDataProvider {
    static final String DIALOGS_KEY = "project.type.warning.dialogs.data";

    private static final ObjectMapper OBJECT_MAPPER;

    private final GlobalPermissionManager globalPermissionManager;
    private final PermissionManager permissionManager;
    private final ProjectTypeManager projectTypeManager;
    private final InaccessibleProjectTypeDialogContentProvider dialogContentProvider;

    static {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
        OBJECT_MAPPER = mapper;
    }

    public InaccessibleProjectTypeDialogDataProviderImpl(
            GlobalPermissionManager globalPermissionManager,
            PermissionManager permissionManager,
            ProjectTypeManager projectTypeManager,
            InaccessibleProjectTypeDialogContentProvider dialogContentProvider) {
        this.globalPermissionManager = globalPermissionManager;
        this.permissionManager = permissionManager;
        this.projectTypeManager = projectTypeManager;
        this.dialogContentProvider = dialogContentProvider;
    }

    @Override
    public boolean shouldDisplayInaccessibleWarning(ApplicationUser user, Project project) {
        return isAdmin(user, project) && isProjectTypeUninstalledOrUnlicensed(project.getProjectTypeKey());
    }

    @Override
    public void provideData(WebResourceAssembler assembler, ApplicationUser user, Project project) {
        assembler.data().requireData(DIALOGS_KEY, getJsonable(dialogContentProvider.getContent(user, project)));
    }

    @VisibleForTesting
    protected Jsonable getJsonable(InaccessibleProjectTypeDialogContent content) {
        return writer -> {
            OBJECT_MAPPER.writeValue(writer, content);
        };
    }

    private boolean isAdmin(ApplicationUser user, Project project) {
        return isGlobalAdmin(user) || permissionManager.hasPermission(ProjectPermissions.ADMINISTER_PROJECTS, project, user);
    }

    private boolean isGlobalAdmin(ApplicationUser user) {
        return globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user);
    }

    private boolean isProjectTypeUninstalledOrUnlicensed(ProjectTypeKey projectTypeKey) {
        return projectTypeManager.isProjectTypeUninstalled(projectTypeKey) || projectTypeManager.isProjectTypeInstalledButInaccessible(projectTypeKey);
    }
}
