package com.atlassian.jira.tenancy;

import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import static com.atlassian.jira.startup.LauncherContextListener.STARTUP_UNEXPECTED;


public class DefaultTenantManager implements TenantManager {

    private static final Logger log = LoggerFactory.getLogger(DefaultTenantManager.class);

    private final ApplicationProperties applicationProperties;
    private final TenancyCondition tenancyCondition;
    private final JiraTenantAccessor jiraTenantAccessor;

    /**
     * Things to do after a tenant has arrived
     */
    private final AtomicBoolean hasTaskRun = new AtomicBoolean(false);
    private final AtomicBoolean hasTenantArrived = new AtomicBoolean(false);
    private volatile AtomicReference<Runnable> task = new AtomicReference<>();
    private volatile String description;

    public DefaultTenantManager(final ApplicationProperties applicationProperties,
                                final TenancyCondition tenancyCondition, final JiraTenantAccessor jiraTenantAccessor) {
        this.applicationProperties = applicationProperties;
        this.tenancyCondition = tenancyCondition;
        this.jiraTenantAccessor = jiraTenantAccessor;
    }

    public void doNowOrWhenTenantArrives(Runnable runnable, String description) {
        if (task.compareAndSet(null, runnable)) {
            this.description = description;
        } else {
            throw new IllegalStateException("Instance tenant task has already been registered");
        }

        if (isTenanted()) {
            log.info("Instance has a tenant. Now running [{}]", description);
            hasTaskRun.set(true);
            runnable.run();
        } else {
            if (hasTenantArrived.get() && hasTaskRun.compareAndSet(false, true)) {
                try {
                    log.info("Instance has a tenant. Now running [{}]", description);
                    runnable.run();
                } catch (Exception e) {
                    fatalJhonson(e);
                } catch (Error e) {
                    fatalJhonson(e);
                    throw e;
                }
            } else {
                log.info("A tenant has not yet arrived. Enqueuing [{}]", description);
                log.info("Awaiting a tenant via a landlord request");
            }
        }
    }

    public ServiceResult tenantArrived() {
        if (!isTenancyEnabled()) {
            return ServiceOutcomeImpl.error("Instance tenanting is NOT enabled, ignoring instance tenanting request.");
        }

        hasTenantArrived.set(true);
        if (task.get() != null) {
            if (hasTaskRun.compareAndSet(false, true)) {
                try {
                    log.info("Tenant arrived. Running [{}]", description);
                    task.get().run();
                    return ServiceOutcomeImpl.ok(null);
                } catch (Exception e) {
                    return fatalJhonson(e);
                } catch (Error e) {
                    fatalJhonson(e);
                    throw e;
                }
            } else {
                return ServiceOutcomeImpl.error("Instance is already tenanted, ignoring tenant arrival notification.");
            }
        } else {
            return ServiceOutcomeImpl.ok("Instance is not yet ready to accept a tenant. It will be tenanted as soon as is ready.");
        }
    }

    public boolean isTenanted() {
        if (isTenancyEnabled()) {
            return jiraTenantAccessor.getAvailableTenants().iterator().hasNext();
        } else {
          return true;
        }
    }

    public void afterInstantiation() throws Exception {
        // When tenancy is disabled (BTF), JiraTenantAccessor will by default have a system tenant already
        if (isTenancyEnabled()) {
            String baseUrl = applicationProperties.getDefaultBackedText(APKeys.JIRA_BASEURL);
            if (baseUrl != null) {
                jiraTenantAccessor.addTenant(new JiraTenantImpl(getBaseUrlWithoutProtocol(baseUrl)));
            }
        }
    }

    private boolean isTenancyEnabled() {
        return tenancyCondition.isEnabled();
    }

    private String getBaseUrlWithoutProtocol(String baseUrl) {
        try {
            URL url = new URL(baseUrl);
            return url.getAuthority() + url.getPath();
        } catch (Exception e) {
            return baseUrl;
        }
    }

    private ServiceOutcomeImpl fatalJhonson(Throwable e) {
        final JohnsonProvider johnsonProvider = ComponentAccessor.getComponentOfType(JohnsonProvider.class);
        log.error("Unable to tenant JIRA", e);
        String errorMessage = "Unexpected exception when tenant arrived. This JIRA instance will not be able to recover. Please check the logs for details.";
        johnsonProvider.getContainer().addEvent(new Event(
                EventType.get(STARTUP_UNEXPECTED), errorMessage,
                EventLevel.get("fatal")));
        return ServiceOutcomeImpl.error(errorMessage);
    }
}
