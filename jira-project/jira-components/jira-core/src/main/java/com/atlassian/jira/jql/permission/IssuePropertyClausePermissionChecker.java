package com.atlassian.jira.jql.permission;

import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.user.ApplicationUser;

import java.util.Set;

/**
 * @since v6.2
 */
public class IssuePropertyClausePermissionChecker implements ClausePermissionChecker {
    @Override
    public boolean hasPermissionToUseClause(final ApplicationUser user) {
        return true;
    }

    @Override
    public boolean hasPermissionToUseClause(final ApplicationUser user, final Set<FieldLayout> fieldLayouts) {
        return hasPermissionToUseClause(user);
    }
}
