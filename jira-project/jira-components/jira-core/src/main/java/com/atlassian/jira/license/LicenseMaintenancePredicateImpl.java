package com.atlassian.jira.license;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.fugue.Option;
import com.atlassian.jira.util.BuildUtilsInfo;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.Date;
import java.util.Set;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Predicate to indicate whether or not the the passed license is within maintenance.
 *
 * @since v7.0
 */
public final class LicenseMaintenancePredicateImpl implements LicenseMaintenancePredicate {
    private final ApplicationManager applicationManager;
    private final BuildUtilsInfo info;

    @Inject
    public LicenseMaintenancePredicateImpl(final ApplicationManager applicationManager,
                                           final BuildUtilsInfo info) {
        this.applicationManager = notNull("applicationManager", applicationManager);
        this.info = notNull("info", info);
    }

    @Override
    public boolean test(@Nonnull final LicenseDetails details) {
        notNull("details", details);

        final Set<ApplicationKey> licensedApplicationKeys = details.getLicensedApplications().getKeys();
        for (final ApplicationKey key : licensedApplicationKeys) {
            final Option<Application> appOption = applicationManager.getApplication(key);
            if (appOption.isDefined()) {
                final Date buildDate = appOption.get().buildDate().toDate();
                if (!details.isMaintenanceValidForBuildDate(buildDate)) {
                    return false;
                }
            }
        }
        // We need to also check against the platform build.
        return details.isMaintenanceValidForBuildDate(info.getCurrentBuildDate());
    }
}
