package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.UserAlreadyExistsException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.user.TimestampedUser;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.util.BatchResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

abstract class DelegatingUserDao implements ExtendedUserDao {

    protected abstract ExtendedUserDao delegate();

    @Override
    public boolean useFullCache() {
        return delegate().useFullCache();
    }

    @Override
    public boolean useInternedUserValues() {
        return delegate().useInternedUserValues();
    }

    @Override
    public boolean isCacheInitialized() {
        return delegate().isCacheInitialized();
    }

    @Override
    public long getUniqueUserCount(Set<Long> directoryIds) throws DirectoryNotFoundException {
        return delegate().getUniqueUserCount(directoryIds);
    }

    @Override
    public Collection<String> findNamesOfUsersInGroups(Collection<String> groupNames) {
        return delegate().findNamesOfUsersInGroups(groupNames);
    }

    @Override
    public Collection<String> getAllAttributeKeys() {
        return delegate().getAllAttributeKeys();
    }

    @Override
    public void processUsers(Consumer<? super User> userProcessor) {
        delegate().processUsers(userProcessor);
    }

    @Override
    public void flushCache() {
        delegate().flushCache();
    }

    @Override
    public TimestampedUser findByName(long directoryId, String userName) throws UserNotFoundException {
        return delegate().findByName(directoryId, userName);
    }

    @Override
    public TimestampedUser findByExternalId(long directoryId, String externalId) throws UserNotFoundException {
        return delegate().findByExternalId(directoryId, externalId);
    }

    @Override
    public UserWithAttributes findByNameWithAttributes(long directoryId, String userName) throws UserNotFoundException {
        return delegate().findByNameWithAttributes(directoryId, userName);
    }

    @Override
    public PasswordCredential getCredential(long directoryId, String userName) throws UserNotFoundException {
        return delegate().getCredential(directoryId, userName);
    }

    @Override
    public List<PasswordCredential> getCredentialHistory(long directoryId, String userName) throws UserNotFoundException {
        return delegate().getCredentialHistory(directoryId, userName);
    }

    @Override
    public User add(User user, PasswordCredential credential) throws UserAlreadyExistsException, IllegalArgumentException, DirectoryNotFoundException {
        return delegate().add(user, credential);
    }

    @Override
    public void storeAttributes(User user, Map<String, Set<String>> attributes) throws UserNotFoundException {
        delegate().storeAttributes(user, attributes);
    }

    @Override
    public User update(User user) throws UserNotFoundException, IllegalArgumentException {
        return delegate().update(user);
    }

    @Override
    public void updateCredential(User user, PasswordCredential credential, int maxCredentialHistory) throws UserNotFoundException, IllegalArgumentException {
        delegate().updateCredential(user, credential, maxCredentialHistory);
    }

    @Override
    public User rename(User user, String newName) throws UserNotFoundException, UserAlreadyExistsException, IllegalArgumentException {
        return delegate().rename(user, newName);
    }

    @Override
    public void removeAttribute(User user, String attributeName) throws UserNotFoundException {
        delegate().removeAttribute(user, attributeName);
    }

    @Override
    public void remove(User user) throws UserNotFoundException {
        delegate().remove(user);
    }

    @Override
    public <T> List<T> search(long directoryId, EntityQuery<T> query) {
        return delegate().search(directoryId, query);
    }

    @Override
    public BatchResult<User> addAll(Set<UserTemplateWithCredentialAndAttributes> users) {
        return delegate().addAll(users);
    }

    @Override
    public BatchResult<String> removeAllUsers(long directoryId, Set<String> userNames) {
        return delegate().removeAllUsers(directoryId, userNames);
    }

    @Override
    public void setAttributeForAllInDirectory(long directoryId, String attrName, String attrValue) {
        delegate().setAttributeForAllInDirectory(directoryId, attrName, attrValue);
    }

    @Override
    public OfBizUser findOfBizUser(long directoryId, String userName) throws com.atlassian.jira.crowd.embedded.ofbiz.UserNotFoundException {
        return delegate().findOfBizUser(directoryId, userName);
    }

    @Override
    public List<OfBizUser> findAllByNameOrNull(long directoryId, @Nonnull Collection<String> userNames) {
        return delegate().findAllByNameOrNull(directoryId, userNames);
    }

    @Nullable
    @Override
    public OfBizUser findByNameOrNull(long directoryId, @Nonnull String userName) {
        return delegate().findByNameOrNull(directoryId, userName);
    }

    @Override
    public Set<String> getAllExternalIds(long directoryId) throws DirectoryNotFoundException {
        return delegate().getAllExternalIds(directoryId);
    }

    @Override
    public long getUserCount(long directoryId) throws DirectoryNotFoundException {
        return delegate().getUserCount(directoryId);
    }

    @Nullable
    @Override
    public OfBizUser findById(long internalUserId) {
        return delegate().findById(internalUserId);
    }
}
