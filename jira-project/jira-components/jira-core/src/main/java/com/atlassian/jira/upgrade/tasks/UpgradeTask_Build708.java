package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.config.properties.PropertiesManager;

import javax.annotation.Nullable;

/**
 * Cleans up left over configuration from JIRA issues cache.
 * Need to run this again as all its good work was undone by the consistency checker.
 */
public class UpgradeTask_Build708 extends UpgradeTask_Build605 {

    public UpgradeTask_Build708(PropertiesManager propertiesManager) {
        super(propertiesManager);
    }

    @Override
    public int getBuildNumber() {
        return 708;
    }

    @Nullable
    @Override
    public Integer dependsUpon() {
        return 707;
    }

}
