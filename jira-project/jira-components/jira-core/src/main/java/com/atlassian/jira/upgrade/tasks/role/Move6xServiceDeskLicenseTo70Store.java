package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.auditing.AssociatedItem;
import com.atlassian.jira.license.Jira6xServiceDeskPluginEncodedLicenseSupplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.jira.upgrade.tasks.role.LicenseUtils.ServiceDeskLicenseType.AgentBasedPricing;
import static com.atlassian.jira.upgrade.tasks.role.LicenseUtils.ServiceDeskLicenseType.TierBasedPricing;
import static com.atlassian.jira.upgrade.tasks.role.LicenseUtils.determineServiceDeskLicenseType;

/**
 * Copy JIRA Service Desk plugin license from UPM license store to JIRA 7 multi license storage. JIRA allows a Service
 * Desk plugin license to be used as a JIRA Service Desk Application license.
 * <p>
 * NOTE: The {@link MigrationTask#migrate(MigrationState, boolean)} of this task does not actually mutate the
 * database, it makes all its changes exclusively on the passed {@link MigrationState}. Something outside of this class
 * will persist the state once it is validated correct.
 *
 * @since v7.0
 */
final class Move6xServiceDeskLicenseTo70Store extends MigrationTask {
    private static final Logger log = LoggerFactory.getLogger(Move6xServiceDeskLicenseTo70Store.class);
    private static final boolean EVENT_SHOWS_IN_CLOUD_LOG = false;

    private final Jira6xServiceDeskPluginEncodedLicenseSupplier sdEncodedLicenseSupplier;

    Move6xServiceDeskLicenseTo70Store(final Jira6xServiceDeskPluginEncodedLicenseSupplier sdEncodedLicenseSupplier) {
        this.sdEncodedLicenseSupplier = sdEncodedLicenseSupplier;
    }

    @Override
    MigrationState migrate(final MigrationState state, final boolean licenseSuppliedByUser) {
        if (licenseSuppliedByUser) {
            return state.log(new AuditEntry(Move6xServiceDeskLicenseTo70Store.class,
                    "Not moving old Service Desk license - license supplied by user",
                    "The person importing JIRA has supplied a license to use; any existing Service Desk license" +
                            " installed in your system will be ignored in preference of the new license.",
                    AssociatedItem.Type.LICENSE, null, EVENT_SHOWS_IN_CLOUD_LOG));
        } else {
            return sdEncodedLicenseSupplier.get()
                    .map(License::new)
                    .filter(this::isValid6xServiceDeskLicense)
                    .map(sdLicense -> addLicenseIfNotPresent(sdLicense, state))
                    .getOrElse(state);
        }
    }

    private MigrationState addLicenseIfNotPresent(final License sdLicense, final MigrationState state) {
        if (state.licenses().canAdd(sdLicense)) {
            return state.changeLicenses(license -> license.addLicense(sdLicense))
                    .log(new AuditEntry(Move6xServiceDeskLicenseTo70Store.class,
                            "Moved Service Desk license to JIRA license store",
                            "The existing Service Desk plugin license was moved to the JIRA license store to function as a" +
                                    " license for the JIRA Service Desk application.",
                            AssociatedItem.Type.LICENSE,
                            sdLicense.getSEN(), EVENT_SHOWS_IN_CLOUD_LOG));
        } else {
            return state.log(new AuditEntry(Move6xServiceDeskLicenseTo70Store.class,
                    "Not moving old Service Desk license - new license present already",
                    "An existing JIRA Service Desk license is installed already so will be used in preference of the" +
                            " old-style plugin license. ",
                    AssociatedItem.Type.LICENSE, null, EVENT_SHOWS_IN_CLOUD_LOG));
        }
    }

    private boolean isValid6xServiceDeskLicense(License license) {
        try {
            final LicenseUtils.ServiceDeskLicenseType licenseType = determineServiceDeskLicenseType(license);
            if (licenseType == TierBasedPricing || licenseType == AgentBasedPricing) {
                return true;
            }
        } catch (MigrationFailedException e) {
            //Either malformed or non service desk license
        }
        log.debug("Invalid Service Desk license in plugin store.");
        return false;
    }
}
