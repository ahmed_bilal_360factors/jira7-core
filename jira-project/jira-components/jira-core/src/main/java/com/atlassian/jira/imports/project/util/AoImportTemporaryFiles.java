package com.atlassian.jira.imports.project.util;

import java.io.File;
import java.util.SortedMap;

/**
 * Single interface to hold the paths to the AO XML partition files, and their temporary parent directory.
 *
 * @since v6.5
 */
public interface AoImportTemporaryFiles {
    /**
     * Returns the temporary directory which is the parent of all the temporary partition files.
     *
     * @return the temporary directory which is the parent of all the temporary partition files.
     */
    File getParentDirectory();

    /**
     * Returns a map from each AOEntity to the temporary XML partition file for the Entity.
     * The Entities are sorted according to the weight supplied by the plugin configurations. Entities that have no
     * import specification are sorted alphabetically to the end.
     *
     * @return the temporary XML partition files map.
     */
    SortedMap<String, File> getAOXmlFiles();

    /**
     * Creates a new the file object for an entity.
     *
     * @param entityName The name of the entity.
     * @return the temporary XML partition file.
     */
    File createFileForEntity(String entityName);

    /**
     * Returns the file registered for an entity.
     *
     * @param entityName The name of the entity.
     * @return the temporary XML partition file.
     */
    File getFileForEntity(String entityName);

    /**
     * Returns a map from each AOEntity to the weight used for sorting the entity.
     * The Entities are sorted according to the weight supplied by the plugin configurations.
     *
     * @return the temporary XML partition file.
     */
    SortedMap<String, Long> getAOEntityWeights();

    /**
     * Register all the AO entities that are defined to be imported by plugins.
     * This allows us to set the correct sort order for processing the XML files later.
     */
    void registerAoEntities();

    /**
     * Record the entityName -> file mapping.
     *
     * @param entityName The name of the entity.
     * @param file       the temporary XML partition file.
     */
    void registerAoXmlFile(String entityName, File file);

    /**
     * Deletes the temporary files held in this object.
     * It is safe to call this method twice as it will check if the files actually exist first.
     */
    void deleteTempFiles();
}
