package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.auditing.ChangedValue;
import com.atlassian.jira.auditing.ChangedValueImpl;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.google.common.collect.ImmutableSet.copyOf;
import static java.util.Collections.emptySet;

/**
 * Provides diffs between application roles. This class is used primarily as a helper when logging changes to roles.
 *
 * @since 7.0
 */
class ApplicationRoleDiff {
    /**
     * Gets a diff for when an application role is removed.
     *
     * @param role the role that is being removed
     * @return a diff containing the groups affected
     */
    @Nonnull
    public static ApplicationRoleDiff diffRemoved(@Nonnull final ApplicationRole role) {
        // Log that selected by default setting was true, but when it was false, ignore it, since it doesn't matter for application access.
        // This is the destination state. Since we are removing the application so if the application was set as default we have to set destination value to false (not default).
        final Optional<Boolean> selectedByDefault = role.isSelectedByDefault() ? Optional.of(false) : Optional.empty();
        return new ApplicationRoleDiff(role.getKey(), role.getGroups(), role.getDefaultGroups(), emptySet(), emptySet(), selectedByDefault);
    }

    /**
     * Gets a diff between an old version of a role and a new version. If the old role is not defined then the diff
     * created will assume that this is the initial setup for the role, that is, the role will be treated as extant
     * but empty.
     *
     * @param oldRole the old version of the role
     * @param newRole the new version of the role
     * @return a diff containing the groups affected
     * @throws IllegalArgumentException if the roles in oldRole and newRole are different
     */
    @Nonnull
    public static ApplicationRoleDiff diff(@Nonnull Option<ApplicationRole> oldRole, @Nonnull ApplicationRole newRole) {
        if (oldRole.isEmpty()) {
            final Optional<Boolean> selectedByDefault = newRole.isSelectedByDefault() ? Optional.of(true) : Optional.empty();
            return new ApplicationRoleDiff(newRole.getKey(), emptySet(), emptySet(), newRole.getGroups(), newRole.getDefaultGroups(), selectedByDefault);
        } else {
            ApplicationRole old = oldRole.get();

            if (!newRole.getKey().equals(old.getKey())) {
                throw new IllegalArgumentException("Old and new role keys must match. Old key: '" + old.getKey() +
                        "'. New key: '" + newRole.getKey() + "'.");
            }

            Set<Group> removedGroups = Sets.difference(old.getGroups(), newRole.getGroups());
            Set<Group> removedDefaultGroups = Sets.difference(old.getDefaultGroups(), newRole.getDefaultGroups());
            Set<Group> addedGroups = Sets.difference(newRole.getGroups(), old.getGroups());
            Set<Group> addedDefaultGroups = Sets.difference(newRole.getDefaultGroups(), old.getDefaultGroups());
            final Optional<Boolean> selectedByDefault = Optional.of(newRole.isSelectedByDefault())
                    .filter(Boolean.valueOf(!old.isSelectedByDefault())::equals);

            return new ApplicationRoleDiff(newRole.getKey(), removedGroups, removedDefaultGroups, addedGroups, addedDefaultGroups, selectedByDefault);
        }
    }

    @VisibleForTesting
    static final Comparator<ChangedValueImpl> CHANGED_VALUE_COMPARATOR = (o1, o2) ->
    {
        int names = o1.getName().compareTo(o2.getName());
        if (names != 0)
            return names;

        int froms = compare(o1.getFrom(), o2.getFrom());
        if (froms != 0)
            return froms;

        return compare(o1.getTo(), o2.getTo());
    };

    @VisibleForTesting
    static int compare(@Nullable String left, @Nullable String right) {
        //noinspection StringEquality
        if (left == right) return 0;
        if (left == null) return -1;
        if (right == null) return 1;
        return left.compareTo(right);
    }

    private final ApplicationKey key;
    private final Set<Group> removedGroups;
    private final Set<Group> reportableRemovedDefaultGroups;
    private final Set<Group> newGroups;
    private final Set<Group> reportableNewDefaultGroups;
    private final Optional<Boolean> selectedByDefault;

    private ApplicationRoleDiff(
            ApplicationKey key,
            Set<Group> removedGroups,
            Set<Group> reportableRemovedDefaultGroups,
            Set<Group> newGroups,
            Set<Group> reportableNewDefaultGroups,
            Optional<Boolean> selectedByDefault) {
        this.key = key;
        this.removedGroups = copyOf(removedGroups);
        this.reportableRemovedDefaultGroups = copyOf(reportableRemovedDefaultGroups);
        this.newGroups = copyOf(newGroups);
        this.reportableNewDefaultGroups = copyOf(reportableNewDefaultGroups);
        this.selectedByDefault = selectedByDefault;
    }

    public String summary() {
        return String.format("Changed access configuration of %s", key.value());
    }

    /**
     * Gets all the changed values for this diff for display in the audit log.
     *
     * @return all the changed values for this diff
     */
    public List<ChangedValue> messages() {
        final List<ChangedValueImpl> extraGroupValues = newGroups.stream().map(group -> new ChangedValueImpl(
                group.getName(),
                "Not associated",
                reportableNewDefaultGroups.contains(group) ? "Associated (default)" : "Associated"))
                .collect(Collectors.toList());
        final List<ChangedValueImpl> removedGroupValues = this.removedGroups.stream().map(group -> new ChangedValueImpl(
                group.getName(),
                reportableRemovedDefaultGroups.contains(group) ? "Associated (default)" : "Associated",
                "Not associated"))
                .collect(Collectors.toList());

        final List<ChangedValueImpl> removedDefaultGroups = this.reportableRemovedDefaultGroups.stream()
                .filter(group -> !this.removedGroups.contains(group))
                .map(group -> new ChangedValueImpl(
                        group.getName(),
                        "Associated (default)",
                        "Associated"))
                .collect(Collectors.toList());

        final List<ChangedValueImpl> newDefaultGroups = this.reportableNewDefaultGroups.stream()
                .filter(group -> !newGroups.contains(group))
                .map(group -> new ChangedValueImpl(
                        group.getName(),
                        "Associated",
                        "Associated (default)"))
                .collect(Collectors.toList());

        final List<ChangedValue> messages = Lists.newArrayList();
        messages.addAll(extraGroupValues);
        messages.addAll(removedGroupValues);
        messages.addAll(removedDefaultGroups);
        messages.addAll(newDefaultGroups);
        if (selectedByDefault.isPresent()) {
            final Boolean defaultApp = selectedByDefault.get();
            messages.add(new ChangedValueImpl("Default application for new users", Boolean.valueOf(!defaultApp).toString(), defaultApp.toString()));
        }
        return messages;
    }

    /**
     * Returns true if this diff does not contain any changed data.
     *
     * @return true if this diff does not contain any changd data, false otherwise
     */
    public boolean isEmpty() {
        return messages().isEmpty();
    }
}
