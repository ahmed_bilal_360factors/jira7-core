package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.google.common.collect.Multimap;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;

/**
 * A simple global permission DAO for renaissance migration. It is only safe to use in migration from JIRA 6.x to
 * JIRA 7.0.
 * <p>
 * NOTE: This class it written to access the database as it was in JIRA 6.x and should not be updated to reflect
 * changes that happen after this point.
 *
 * @since v7.0
 */
abstract class GlobalPermissionDao {
    public enum AdminPermission {
        ADMIN("ADMINISTER"),
        SYSADMIN("SYSTEM_ADMIN");

        private final String ofBizKey;

        AdminPermission(final String ofBizKey) {
            this.ofBizKey = ofBizKey;
        }

        @Nonnull
        public String ofBizKey() {
            return ofBizKey;
        }

        public static Option<AdminPermission> from(@Nullable final String permissionKey) {
            if (permissionKey == null)
                return none();

            if (ADMIN.ofBizKey().equalsIgnoreCase(permissionKey))
                return some(ADMIN);
            if (SYSADMIN.ofBizKey().equalsIgnoreCase(permissionKey))
                return some(SYSADMIN);

            return none();
        }
    }

    /**
     * Return the set of groups with USE permission.
     *
     * @return the set of groups with USE permission.
     */
    abstract Set<Group> groupsWithUsePermission();

    /**
     * Return the set of groups associated with the ADMIN or SYSADMIN permission.
     *
     * @return the set of groups associated with the ADMIN or SYSADMIN permission.
     */
    abstract Set<Group> groupsWithAdminPermission();

    /**
     * Return the set of groups associated with the ADMIN or SYSADMIN permission.
     *
     * @return the set of groups associated with the ADMIN or SYSADMIN permission.
     */
    abstract Multimap<Group, AdminPermission> groupsWithAdminPermissionAndPermissionType();

    /**
     * Return the set of groups associated with the service desk agent permission.
     *
     * @return the set of groups associated with the service desk agent permission.
     */
    abstract Set<Group> groupsWithSdAgentPermission();
}
