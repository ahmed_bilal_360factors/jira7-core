package com.atlassian.jira.mail;

import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.mail.queue.SingleMailQueueItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * This class takes a collection of {@link com.atlassian.jira.notification.NotificationRecipient} objects and
 * processes them by executing the {@link #evaluateEmailForRecipient(NotificationRecipient)} method.
 * <p>
 * The design of this class is to process all recipients even if the processing causes any exceptions. If an exception
 * occurs, {@link #handleException(com.atlassian.jira.notification.NotificationRecipient, Exception)} method is invoked,
 * and processing continues.
 *
 * @since v3.12.3
 */
abstract class NotificationRecipientProcessor {
    private final Collection<NotificationRecipient> recipients;

    NotificationRecipientProcessor(final Collection<NotificationRecipient> recipients) {
        this.recipients = recipients;
    }

    /**
     * Iterates through all recipients in the collection and performs
     * the {@link #evaluateEmailForRecipient(NotificationRecipient)} method on each recipient.
     * <p>
     * If the {@link #evaluateEmailForRecipient(NotificationRecipient)} method throws an exception,
     * {@link #handleException(com.atlassian.jira.notification.NotificationRecipient, Exception)} is called.
     */
    public Collection<SingleMailQueueItem> evaluateEmails() {
        List<SingleMailQueueItem> result = new ArrayList<>(recipients.size());
        for (NotificationRecipient recipient : recipients) {
            try {
                evaluateEmailForRecipient(recipient).ifPresent(result::add);
            } catch (Exception ex) // yes, we want to catch RuntimeException as well
            {
                handleException(recipient, ex);
            }
        }
        return result;
    }

    abstract Optional<SingleMailQueueItem> evaluateEmailForRecipient(final NotificationRecipient recipient) throws Exception;

    /**
     * This method logs the exception caused in the execution of
     * the {@link #evaluateEmailForRecipient(NotificationRecipient)} method.
     *
     * @param recipient recipient that caused the exception
     * @param ex        exception
     */
    abstract void handleException(final NotificationRecipient recipient, Exception ex);
}
