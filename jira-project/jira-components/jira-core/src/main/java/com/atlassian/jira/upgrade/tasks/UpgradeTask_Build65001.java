package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.config.properties.PropertiesManager;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.upgrade.AbstractDelayableUpgradeTask;
import com.atlassian.jira.user.ApplicationUserEntity;
import com.opensymphony.module.propertyset.PropertySet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.jira.config.properties.APKeys.ONBOARDING_APP_USER_ID_THRESHOLD;

/**
 * Stores the max() app_user id as a JIRA property that can be used later to determine which users are new and should
 * see the onboarding flow.
 *
 * @since v6.5
 */
public class UpgradeTask_Build65001 extends AbstractDelayableUpgradeTask {
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask_Build65001.class);

    private final EntityEngine entityEngine;
    private final PropertiesManager propertiesManager;

    public UpgradeTask_Build65001(final EntityEngine entityEngine, final PropertiesManager propertiesManager) {
        super();
        this.entityEngine = entityEngine;
        this.propertiesManager = propertiesManager;
    }

    @Override
    public int getBuildNumber() {
        return 65001;
    }

    @Override
    public String getShortDescription() {
        return "Storing max(app_user) to enable onboarding for new users";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception {
        final PropertySet propertySet = propertiesManager.getPropertySet();
        if (propertySet.exists(ONBOARDING_APP_USER_ID_THRESHOLD)) {
            log.info("{} application property already stored with value {} - nothing to do here",
                    ONBOARDING_APP_USER_ID_THRESHOLD, propertySet.getString(ONBOARDING_APP_USER_ID_THRESHOLD));
            return;
        }

        final ApplicationUserEntity maxAppUser = Select.from(Entity.APPLICATION_USER)
                .orderBy("id desc")
                .limit(1)
                .runWith(entityEngine)
                .singleValue();

        if (maxAppUser != null) {
            propertySet.setString(ONBOARDING_APP_USER_ID_THRESHOLD, maxAppUser.getId().toString());
            log.info("Adding new application property {} with value {}",
                    ONBOARDING_APP_USER_ID_THRESHOLD, maxAppUser.getId());
        } else {
            propertySet.setString(ONBOARDING_APP_USER_ID_THRESHOLD, "0");
            log.info("No users found during the execution of this upgrade task. Setting 0 as the value for {}",
                    ONBOARDING_APP_USER_ID_THRESHOLD);
        }
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        return false;
    }
}
