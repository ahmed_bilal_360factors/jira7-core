package com.atlassian.jira.web.session;

import com.atlassian.jira.util.NonInjectableComponent;

/**
 * Provides access to getting and setting the selected issue (stored as a {@link Long}) in session.
 *
 * @see SessionSearchObjectManagerFactory#createSelectedIssueManager()
 * @see SessionSearchObjectManagerFactory#createSelectedIssueManager(javax.servlet.http.HttpServletRequest)
 * @see SessionSearchObjectManagerFactory#createSelectedIssueManager(com.atlassian.jira.util.velocity.VelocityRequestSession)
 * @since v4.2
 * @deprecated since 7.1 Storing information on the httpsession is obsoleted. Please move this functionality to the frontend or other type of storage. This method will be removed in 8.0.
 */
@Deprecated
@NonInjectableComponent
public interface SessionSelectedIssueManager extends SessionSearchObjectManager<SessionSelectedIssueManager.SelectedIssueData> {
    public static class SelectedIssueData {
        private final Long selectedIssueId;
        private final int selectedIssueIndex;
        private final Long nextIssueId;

        public SelectedIssueData(final Long selectedIssueId, final int selectedIssueIndex, final Long nextIssueId) {
            this.selectedIssueId = selectedIssueId;
            this.selectedIssueIndex = Math.max(0, selectedIssueIndex);
            this.nextIssueId = nextIssueId;
        }

        public Long getSelectedIssueId() {
            return selectedIssueId;
        }

        /**
         * The 0-based index, with respect to the entire search results, of the selected issue.
         */
        public int getSelectedIssueIndex() {
            return selectedIssueIndex;
        }

        public Long getNextIssueId() {
            return nextIssueId;
        }
    }
}
