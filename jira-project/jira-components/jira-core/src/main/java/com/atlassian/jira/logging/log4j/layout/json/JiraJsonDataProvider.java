package com.atlassian.jira.logging.log4j.layout.json;

import com.atlassian.logging.log4j.layout.json.DefaultJsonDataProvider;
import org.apache.log4j.spi.LoggingEvent;

import java.util.Map;

import static com.atlassian.jira.util.BuildUtils.getCommitId;
import static com.atlassian.jira.util.BuildUtils.getCurrentBuildNumber;
import static com.atlassian.jira.util.BuildUtils.getVersion;

/**
 * {@link DefaultJsonDataProvider} specialization that adds extra JIRA fields into the logging event. Appended fields
 * will appear under the "ext." namespace.
 *
 * @since v7.2
 */
public class JiraJsonDataProvider extends DefaultJsonDataProvider {

    private final String JIRA_VERSION = "jira.version";
    private final String JIRA_BUILD_NUMBER = "jira.build.number";
    private final String COMMIT_ID = "jira.commit.id";

    @Override
    public Map<String, String> getExtraData(LoggingEvent event) {
        Map<String, String> extraData = super.getExtraData(event);
        extraData.put(JIRA_VERSION, getVersion());
        extraData.put(COMMIT_ID, getCommitId());
        extraData.put(JIRA_BUILD_NUMBER, getCurrentBuildNumber());

        return extraData;
    }
}
