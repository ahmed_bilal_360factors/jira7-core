package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizListIterator;
import com.atlassian.jira.portal.CachingPortletConfigurationStore;
import com.atlassian.jira.portal.OfbizPortletConfigurationStore;
import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericValue;

/**
 * Converts jira-introduction-gadget into jira-introduction-dashboard-item in the portletconfiguration table.
 * Introduction-gadget code was deprecated and removed from the code; This upgrade task ensures that all references to
 * it are changed to introduction-dashboard-item.
 *
 * @since v7.0
 */
public class UpgradeTask_Build70007 extends AbstractImmediateUpgradeTask {
    private static final String INTRO_DASHBOARD_ITEM_KEY = "com.atlassian.jira.gadgets:introduction-dashboard-item";
    private static final String INTRO_GADGET_XML = "rest/gadgets/1.0/g/com.atlassian.jira.gadgets:introduction-gadget/gadgets/introduction-gadget.xml";

    private final OfBizDelegator delegator;
    private final CachingPortletConfigurationStore cachingPortletConfigurationStore;


    public UpgradeTask_Build70007(final OfBizDelegator delegator, final CachingPortletConfigurationStore cachingPortletConfigurationStore) {
        this.delegator = delegator;
        this.cachingPortletConfigurationStore = cachingPortletConfigurationStore;
    }

    @Override
    public int getBuildNumber() {
        return 70007;
    }


    @Override
    public void doUpgrade(boolean setupMode) throws Exception {
        final EntityExpr condition = new EntityExpr(OfbizPortletConfigurationStore.Columns.GADGET_XML,
                EntityOperator.LIKE, INTRO_GADGET_XML);
        OfBizListIterator gadgetsIterator = delegator.findListIteratorByCondition(OfbizPortletConfigurationStore.TABLE,
                condition);
        try {
            for (GenericValue row : gadgetsIterator) {
                row.set(OfbizPortletConfigurationStore.Columns.MODULE_KEY, INTRO_DASHBOARD_ITEM_KEY);
                delegator.store(row);
            }
        } finally {
            cachingPortletConfigurationStore.flush();
            gadgetsIterator.close();
        }
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        // JDEV-33452
        return false;
    }

    @Override
    public String getShortDescription() {
        return "Converts jira-introduction-gadget to jira-introduction-dashboard-item";
    }
}
