package com.atlassian.jira.jql.resolver;

import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserKeyService;
import com.atlassian.jira.user.util.UserManager;
import com.google.common.collect.ImmutableList;
import com.opensymphony.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Resolves User objects and their names.
 *
 * @since v4.0
 */
public class UserResolverImpl implements UserResolver {
    private static final Logger log = LoggerFactory.getLogger(UserResolverImpl.class);

    private final UserKeyService userKeyService;
    private final UserManager userManager;
    private final UserSearchService userSearchService;

    public UserResolverImpl(final UserKeyService userKeyService, final UserManager userManager, final UserSearchService userSearchService) {
        this.userKeyService = userKeyService;
        this.userManager = userManager;
        this.userSearchService = userSearchService;
    }

    public List<String> getIdsFromName(final String name) {
        notNull("name", name);
        final String userkey = userKeyService.getKeyForUsername(name);
        if (userkey != null) {
            return Collections.singletonList(userkey);
        }
        return getUsersFromFullNameOrEmail(name);
    }

    public boolean nameExists(final String name) {
        notNull("name", name);
        if (userKeyService.getKeyForUsername(name) != null) {
            return true;
        }
        if (log.isDebugEnabled()) {
            log.debug("Username '" + name + "' not found - searching as email or full name.");
        }
        return hasAnyFullNameOrEmailMatches(name);
    }

    public boolean idExists(final Long id) {
        return nameExists(notNull("id", id).toString());
    }

    /**
     * Picks between the matched full name and email matches.  If both full name and email matches are
     * available, then the email matches are used if the name looks like an email address; otherwise,
     * the full name matches are used.
     *
     * @param name            the name to find matches for
     * @param fullNameMatches the names of users whose full names matched
     * @param emailMatches    the names of users whose email addresses matched
     * @return a list of user keys for the users that best match the supplied <em>name</em>.
     */
    private List<String> pickEmailOrFullNameMatches(final String name, final List<String> fullNameMatches, final List<String> emailMatches) {
        if (emailMatches.isEmpty()) {
            return fullNameMatches;
        }
        if (fullNameMatches.isEmpty() || isEmail(name)) {
            return emailMatches;
        }
        return fullNameMatches;
    }

    private List<String> getUsersFromFullNameOrEmail(String name) {
        Iterable<String> fullNameMatches = userSearchService.findUserKeysByFullName(name);
        Iterable<String> emailMatches = userSearchService.findUserKeysByEmail(name);

        return pickEmailOrFullNameMatches(name, ImmutableList.copyOf(fullNameMatches), ImmutableList.copyOf(emailMatches));
    }

    private boolean hasAnyFullNameOrEmailMatches(String name) {
        return userSearchService.findUserKeysByFullName(name).iterator().hasNext() ||
                userSearchService.findUserKeysByEmail(name).iterator().hasNext();
    }

    private boolean isEmail(final String name) {
        return TextUtils.verifyEmail(name);
    }

    public ApplicationUser get(final Long id) {
        return getApplicationUser(id);
    }

    public ApplicationUser getApplicationUser(final Long id) {
        return getApplicationUser(id.toString());
    }

    @Nonnull
    public Collection<ApplicationUser> getAll() {
        return userManager.getUsers();
    }

    ApplicationUser getApplicationUser(String name) {
        return userManager.getUserByName(name);
    }
}
