package com.atlassian.jira.issue.views;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.views.util.SearchRequestViewUtils;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.query.Query;
import com.atlassian.velocity.htmlsafe.HtmlSafe;

/**
 * Generates issue navigator search link for the given field value.
 *
 * @since 6.4
 */
public class DefaultSearchLinkGenerator implements SearchLinkGenerator {
    private final ProjectManager projectManager;
    private final JiraAuthenticationContext authenticationContext;
    private final VelocityRequestContextFactory velocityRequestContextFactory;

    public DefaultSearchLinkGenerator(final ProjectManager projectManager, final JiraAuthenticationContext authenticationContext, final VelocityRequestContextFactory velocityRequestContextFactory) {
        this.projectManager = projectManager;
        this.authenticationContext = authenticationContext;
        this.velocityRequestContextFactory = velocityRequestContextFactory;
    }

    /**
     * Get search link for component field scoped within a project of the passed component.
     *
     * @param component The component object to generate the link from.
     * @return String with search URL that includes base url, empty String if null parameter was passed.
     */
    @HtmlSafe
    @Override
    public String getComponentSearchLink(ProjectComponent component) {
        if (component == null) {
            return "";
        }

        final Query projectAndComponent = JqlQueryBuilder.newBuilder().where()
                .project(projectManager.getProjectObj(component.getProjectId()).getKey())
                .and().component(component.getName())
                .buildQuery();

        return getLinkForQuery(projectAndComponent);
    }

    /**
     * Get search link for fixVersion field scoped within a project of the passed version.
     *
     * @param version The version object to generate the link from.
     * @return String with search URL that includes base url, empty String if null parameter was passed.
     */
    @HtmlSafe
    @Override
    public String getFixVersionSearchLink(Version version) {
        if (version == null) {
            return "";
        }

        final Query projectAndFixVersion = JqlQueryBuilder.newBuilder().where()
                .project(version.getProject().getKey())
                .and().fixVersion(version.getName())
                .buildQuery();

        return getLinkForQuery(projectAndFixVersion);
    }

    private String getLinkForQuery(Query query) {
        return SearchRequestViewUtils.getLink(
                new SearchRequest(query),
                velocityRequestContextFactory.getJiraVelocityRequestContext().getBaseUrl(),
                authenticationContext.getUser()
        );
    }
}
