package com.atlassian.jira.web.filters.mau;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.core.filters.AbstractHttpFilter;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.component.ComponentReference;
import com.atlassian.jira.event.mau.MauApplicationKey;
import com.atlassian.jira.event.mau.MauEventService;

import com.google.common.annotations.VisibleForTesting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract servlet filter which provides an easy way to parse a HttpServletRequest,
 * determine the MauApplicationKey it relates to and tag it.
 * @since v7.0.1
 */
public abstract class AbstractMauRequestTaggingFilter extends AbstractHttpFilter {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractMauRequestTaggingFilter.class);

    @Override
    protected final void doFilter(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException {
        try {
            getMauEventService().ifPresent(mauEventService -> tagRequest(mauEventService, request));
        } catch (final Exception e) {
            LOG.error("Exception thrown in a MauRequestTaggingFitler", e);
        }
        filterChain.doFilter(request, response);
    }

    /**
     * Determines which application the given request belongs to.
     * Override this method with logic to decide on a MauApplicationKey and tag the given request with it.
     * @param request
     * @return Some MauApplicationKey to tag the request as, or empty to ignore the request.
     */
    public abstract void tagRequest(MauEventService mauEventService, HttpServletRequest request);

    @VisibleForTesting
    Optional<MauEventService> getMauEventService() {
        return ComponentAccessor.getComponentSafely(MauEventService.class);
    }
}