package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

/**
 * Checks if currently user is not logged in.
 */
@Internal
public class IsAnonymousUserCondition implements Condition {
    private final JiraAuthenticationContext authenticationContext;

    public IsAnonymousUserCondition(final JiraAuthenticationContext authenticationContext) {
        this.authenticationContext = authenticationContext;
    }

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {
    }

    @Override
    public boolean shouldDisplay(final Map<String, Object> context) {
        return !authenticationContext.isLoggedInUser();
    }
}
