package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.plugin.webfragment.conditions.cache.ConditionCacheKeys;
import com.atlassian.jira.plugin.webfragment.conditions.cache.RequestCachingConditionHelper;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Checks if the user is an admin or can see at least one project with the permission
 */
public class UserIsAdminOrHasVisibleProjectsCondition extends AbstractProjectPermissionCondition {

    private final UserIsAdminCondition isAdminCondition;
    private final PermissionManager permissionManager;


    public UserIsAdminOrHasVisibleProjectsCondition(GlobalPermissionManager globalPermissionManager, PermissionManager permissionManager) {
        this.permissionManager = permissionManager;
        isAdminCondition = new UserIsAdminCondition(globalPermissionManager);
    }

    @Override
    protected boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper, ProjectPermissionKey permissionKey) {

        return isAdminCondition.shouldDisplay(user, jiraHelper) ||
                RequestCachingConditionHelper.cacheConditionResultInRequest(
                        ConditionCacheKeys.custom("hasProjects", user, permissionKey),
                        () -> permissionManager.hasProjects(permissionKey, user));
    }
}