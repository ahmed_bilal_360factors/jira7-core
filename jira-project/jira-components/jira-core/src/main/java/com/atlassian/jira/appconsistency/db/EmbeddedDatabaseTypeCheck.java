package com.atlassian.jira.appconsistency.db;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.startup.StartupCheck;

/**
 * Embedded database check that stops JIRA startup if unsupported HSQL is detected.
 *
 * @since v7.1
 */
public class EmbeddedDatabaseTypeCheck implements StartupCheck {

    private final DatabaseConfig databaseConfiguration;

    public EmbeddedDatabaseTypeCheck(DatabaseConfig databaseConfiguration) {
        this.databaseConfiguration = databaseConfiguration;
    }

    @Override
    public String getName() {
        return "Embedded database support check";
    }

    @Override
    public boolean isOk() {
        if (databaseConfiguration.isHSql()) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String getFaultDescription() {
        final StringBuilder message = new StringBuilder(512);
        message.append("Failed to start due to problems with your database configuration. HSQL is no longer supported.");
        message.append("In order to migrate your HSQL database to supported H2 database, please run JIRA 7.0 which will trigger migration procedure.");
        return message.toString();
    }

    @Override
    public String getHTMLFaultDescription() {
        final StringBuilder message = new StringBuilder(512);
        message.append("<p>Failed to start due to problems with your database configuration. HSQL is no longer supported.</p>");
        message.append("<p>In order to migrate your HSQL database to supported H2 database, please run JIRA 7.0 which will trigger migration procedure.</p>");
        return message.toString();
    }

    @Override
    public void stop() {

    }
}
