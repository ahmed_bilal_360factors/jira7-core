package com.atlassian.jira.sharing.type;

import com.atlassian.jira.sharing.SharePermission;
import com.atlassian.jira.sharing.SharedEntity;
import com.atlassian.jira.sharing.search.GlobalShareTypeSearchParameter;
import com.atlassian.jira.sharing.search.ShareTypeSearchParameter;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;

/**
 * Implementation of the {@link ShareQueryFactory} for the AuthenticatedUser share type.
 *
 * @since v7.2.2
 */
public class AuthenticatedUserShareQueryFactory implements ShareQueryFactory<GlobalShareTypeSearchParameter> {
    private static final class Constant {
        static final String FIELD = "shareTypeAuthenticatedUser";
        static final String VALUE = "true";
    }

    public Field getField(final SharedEntity entity, final SharePermission permission) {
        return new Field(Constant.FIELD, Constant.VALUE, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS);
    }

    @Override
    public Term[] getTerms(final ApplicationUser user) {
        final Term term = getTerm(user);
        return (term == null) ? new Term[0] : new Term[]{term};
    }

    @Override
    public Query getQuery(final ShareTypeSearchParameter parameter, final ApplicationUser user) {
        return getQuery(parameter);
    }

    @Override
    public Query getQuery(final ShareTypeSearchParameter parameter) {
        return new TermQuery(getTerm(null));
    }

    private Term getTerm(final ApplicationUser user) {
        return (user == null) ? null : new Term(Constant.FIELD, Constant.VALUE);
    }
}
