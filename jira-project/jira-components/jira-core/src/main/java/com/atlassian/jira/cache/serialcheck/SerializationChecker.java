package com.atlassian.jira.cache.serialcheck;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CachedReference;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;

/**
 * @since 7.2.0
 */
@ParametersAreNonnullByDefault
public interface SerializationChecker {
    <V> void checkValue(CachedReference<V> cachedReference, String cacheName, @Nullable V value);
    <K, V> void checkValue(Cache<K, V> cache, K key, @Nullable V value);
    Collection<? extends Exception> getErrors();
    void reset();
}
