package com.atlassian.jira.license;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.entity.EntityEngine;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Objects;

import static com.atlassian.jira.entity.Delete.from;
import static com.atlassian.jira.entity.Entity.PRODUCT_LICENSE;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.any;


/**
 * Store that looks for licenses in the {@link ProductLicense} table.
 *
 * @since v6.3
 */
public class MultiLicenseStoreImpl implements MultiLicenseStore {
    private static final String ID_COLUMN = "id";
    private final EntityEngine entityEngine;
    private final ApplicationProperties applicationProperties;

    public MultiLicenseStoreImpl(EntityEngine entityEngine,
                                 ApplicationProperties applicationProperties) {
        this.entityEngine = notNull("entityEngine", entityEngine);
        this.applicationProperties = notNull("applicationProperties", applicationProperties);
    }

    @Override
    @Nonnull
    public List<String> retrieve() {
        return entityEngine.selectFrom(PRODUCT_LICENSE).findAll().orderBy(ID_COLUMN).stream()
                .filter(Objects::nonNull)
                .map(ProductLicense::getLicenseKey)
                .collect(CollectorsUtil.toImmutableList());
    }

    @Override
    public void store(@Nonnull final Iterable<String> licenses) {
        checkNotNull(licenses, "newLicenseKeys");

        if (Iterables.isEmpty(licenses)) {
            throw new IllegalArgumentException("You must store at least one license.");
        }

        if (any(licenses, Predicates.isNull())) {
            throw new IllegalArgumentException("You cannot store null licenses - no changes have been made to licenses.");
        }

        clear();

        for (String licenseKey : licenses) {
            entityEngine.createValue(PRODUCT_LICENSE, new ProductLicense(licenseKey));
        }
    }

    @Override
    public void resetOldBuildConfirmation() {
        applicationProperties.setOption(APKeys.JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE, false);
        applicationProperties.setString(APKeys.JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE_TIMESTAMP, "");
        applicationProperties.setString(APKeys.JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE_USER, "");
    }

    @Override
    public void confirmProceedUnderEvaluationTerms(final String userName) {
        // Record the confirmation, the user and the time, and proceed
        applicationProperties.setOption(APKeys.JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE, true);
        applicationProperties.setString(APKeys.JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE_USER, userName);
        applicationProperties.setString(APKeys.JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE_TIMESTAMP, String.valueOf(System.currentTimeMillis()));
    }

    @Override
    public void clear() {
        entityEngine.delete(from(PRODUCT_LICENSE).all());
    }

    @Override
    public String retrieveServerId() {
        return applicationProperties.getString(APKeys.JIRA_SID);
    }

    @Override
    public void storeServerId(final String serverId) {
        applicationProperties.setString(APKeys.JIRA_SID, serverId);
    }
}
