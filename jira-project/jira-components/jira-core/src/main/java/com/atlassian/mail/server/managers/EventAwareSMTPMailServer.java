package com.atlassian.mail.server.managers;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.mail.MailProtocol;
import com.atlassian.mail.event.SendMessageEvent;
import com.atlassian.mail.server.impl.SMTPMailServerImpl;

import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

/**
 * SMTP Mail Server that triggers event listener in SMTPBackdoor.
 *
 * @since 7.1
 */
public class EventAwareSMTPMailServer extends SMTPMailServerImpl {
    public EventAwareSMTPMailServer(Long id, String name, String description, String from, String prefix, boolean isSession, MailProtocol protocol, String location, String smtpPort, boolean tlsRequired, String username, String password, long timeout, String socksHost, String socksPort) {
        super(id, name, description, from, prefix, isSession, protocol, location, smtpPort, tlsRequired, username, password, timeout, socksHost, socksPort);
    }

    @Override
    protected void sendMimeMessage(final MimeMessage message, final Transport transport) throws MessagingException {
        super.sendMimeMessage(message, transport);

        final EventPublisher publisher = ComponentAccessor.getComponent(EventPublisher.class);
        publisher.publish(new SendMessageEvent(message));
    }
}
