package com.atlassian.jira.bc.user.search;

import com.atlassian.annotations.Internal;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizUser;
import com.google.common.collect.ImmutableMap;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Indexes users.
 */
@Internal
public interface UserIndexer {

    String ACTIVE = UserTermKeys.ACTIVE.getPropertyName();
    String INTERNAL_ID = "internal_id";
    String DIRECTORY_ID = "directory_id";
    String DISPLAY_NAME = UserTermKeys.DISPLAY_NAME.getPropertyName();
    String EXACT_DISPLAY_NAME = "exact_" + DISPLAY_NAME;
    String EMAIL = UserTermKeys.EMAIL.getPropertyName();
    String EXACT_EMAIL = "exact_" + EMAIL;
    String USER_NAME = UserTermKeys.USERNAME.getPropertyName();
    String EXACT_USER_NAME = "exact_" + USER_NAME;

    static final Map<String, String> FIELD_NAME_TO_EXACT_FIELD_NAME_MAP =
            ImmutableMap.of(UserIndexer.DISPLAY_NAME, UserIndexer.EXACT_DISPLAY_NAME,
                            UserIndexer.EMAIL, UserIndexer.EXACT_EMAIL,
                            UserIndexer.USER_NAME, UserIndexer.EXACT_USER_NAME);

    void index(OfBizUser... users);

    void deindex(UserId... userIds);
    void deindexById(long... internalUserId);
    void deindexByUserName(String... userNames);

    void replaceAllUsers(Consumer<Consumer<OfBizUser>> userConsumerCreator);

    List<User> search(Query query, int skip, int limit, Sort sort);

    static String exactMatchFieldName(String fieldName) {
        return FIELD_NAME_TO_EXACT_FIELD_NAME_MAP.getOrDefault(fieldName, fieldName);
    }
}
