package com.atlassian.jira.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Properties;

public class PropertiesFileBackedJiraProductInformation implements JiraProductInformation {

    private static final String PROPERTIES_FILE_NAME = "/product.properties";
    private static final Logger log = LoggerFactory.getLogger(PropertiesFileBackedJiraProductInformation.class);

    private final Properties properties;

    public PropertiesFileBackedJiraProductInformation() {
        this(loadFromPropertiesFile());
    }

    public PropertiesFileBackedJiraProductInformation(final Properties values) {
        properties = new Properties();
        if (values != null) {
            properties.putAll(values);
        }
    }

    private static Properties loadFromPropertiesFile() {
        final Properties properties = new Properties();
        try (Reader reader = new InputStreamReader(PropertiesFileBackedJiraProductInformation.class.getResourceAsStream(PROPERTIES_FILE_NAME))) {
            properties.load(reader);
        } catch (final IOException e) {
            log.warn("Could not read product information.", e);
        }
        return properties;
    }

    @Override
    public String getValueForKey(final Keys key) {
        final String value = (String) properties.get(key.getKey());
        return value != null ? value : key.getDefaultValue();
    }
}
