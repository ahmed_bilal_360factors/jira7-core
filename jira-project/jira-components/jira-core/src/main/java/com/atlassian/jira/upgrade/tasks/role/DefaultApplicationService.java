package com.atlassian.jira.upgrade.tasks.role;

/**
 * Sets the Default Application after Renaissance migration.
 * New users are created with the respect of the default application preferences.
 *
 * @since v7.0
 */
public interface DefaultApplicationService {
    /**
     * Performs the data migration.
     */
    public void setApplicationsAsDefaultDuring6xTo7xUpgrade();
}
