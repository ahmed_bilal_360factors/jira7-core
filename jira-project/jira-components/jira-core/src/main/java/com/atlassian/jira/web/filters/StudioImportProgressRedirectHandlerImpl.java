package com.atlassian.jira.web.filters;

public class StudioImportProgressRedirectHandlerImpl implements JiraImportProgressRedirectHandler {

    @Override
    public String getSetupPage() {
        return "/secure/admin/StudioImportSelect!start.jspa";
    }

    @Override
    public String getValidImportPage() {
        return "/secure/admin/StudioImport!finished.jspa";
    }

    @Override
    public String getInvalidImportPage() {
        return "/secure/admin/StudioImport!finished.jspa";
    }

    @Override
    public String getNoProgressPage() {
        return "/secure/admin/StudioImport!finished.jspa";
    }
}
