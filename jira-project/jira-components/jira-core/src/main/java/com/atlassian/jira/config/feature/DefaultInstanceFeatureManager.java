package com.atlassian.jira.config.feature;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.InitializingComponent;
import com.atlassian.jira.config.CoreFeatures;
import com.atlassian.jira.config.InstanceFeatureManager;
import com.atlassian.jira.extension.Startable;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Set;

/**
 * @since v7.1
 */
@ParametersAreNonnullByDefault
public class DefaultInstanceFeatureManager implements InstanceFeatureManager, Startable, InitializingComponent {
    private final FeaturesMapHolder features;

    private final EventPublisher eventPublisher;

    public DefaultInstanceFeatureManager(final FeaturesLoader featuresLoader, final EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
        this.features = new FeaturesMapHolder(featuresLoader);
    }

    @Override
    public Set<String> getEnabledFeatureKeys() {
        return features.enabledFeatures();
    }

    @Override
    public boolean isInstanceFeatureEnabled(final String featureKey) {
        return features.enabledFeatures().contains(featureKey);
    }

    @Override
    public void afterInstantiation() throws Exception {
        eventPublisher.register(this.features);
    }

    @Override
    public void start() throws Exception {
        features.start();
    }
}
