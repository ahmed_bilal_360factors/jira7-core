package com.atlassian.jira.cluster.cache.ehcache;

import com.atlassian.cache.CacheSettings;
import net.sf.ehcache.config.CacheConfiguration;

import javax.annotation.Nonnull;

import static net.sf.ehcache.config.CacheConfiguration.CacheEventListenerFactoryConfiguration;

public class ReplicatorConfigFactory implements com.atlassian.cache.ehcache.replication.EhCacheReplicatorConfigFactory {
    private static final String CACHE_PROPERTIES =
            "replicateAsynchronously=%s," +
                    "replicatePuts=%s," +
                    "replicatePutsViaCopy=%s," +
                    "replicateUpdates=%s," +
                    "replicateUpdatesViaCopy=%s," +
                    "replicateRemovals=true";

    @Override
    @Nonnull
    public CacheEventListenerFactoryConfiguration createCacheReplicatorConfiguration(
            final CacheSettings settings, final boolean selfLoadingCache) {
        final String cacheProperties = String.format(
                CACHE_PROPERTIES,
                settings.getReplicateAsynchronously(false),
                shouldReplicatePuts(settings, selfLoadingCache),
                settings.getReplicateViaCopy(false),
                shouldReplicateUpdates(selfLoadingCache),
                settings.getReplicateViaCopy(false)
        );

        return new CacheConfiguration.CacheEventListenerFactoryConfiguration()
                .className(ReplicatorFactory.class.getName())
                .properties(cacheProperties);
    }

    private boolean shouldReplicatePuts(final CacheSettings settings, final boolean selfLoadingCache) {
        return !selfLoadingCache && settings.getReplicateViaCopy(false);
    }

    private boolean shouldReplicateUpdates(final boolean selfLoadingCache) {
        return !selfLoadingCache;
    }
}
