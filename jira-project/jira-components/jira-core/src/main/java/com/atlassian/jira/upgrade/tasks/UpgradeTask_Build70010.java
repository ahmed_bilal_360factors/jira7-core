package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.entity.Delete;
import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.issue.subscription.DefaultSubscriptionManager;
import com.atlassian.jira.scheduler.OfBizClusteredJob;
import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;
import com.atlassian.jira.upgrade.tasks.util.CronExpressionFixer;
import com.atlassian.jira.upgrade.tasks.util.CronExpressionFixer.Result;
import com.atlassian.scheduler.caesium.impl.ImmutableClusteredJob;
import com.atlassian.scheduler.caesium.spi.ClusteredJob;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.cron.CronSyntaxException;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import static com.atlassian.jira.entity.Entity.CLUSTERED_JOB;
import static com.atlassian.jira.scheduler.ClusteredJobFactory.JOB_ID;
import static com.atlassian.jira.web.component.cron.parser.CronExpressionParser.DEFAULT_CRONSTRING;

/**
 * Migrate atlassian-scheduler data from Quartz to Caesium.
 *
 * @since v7.0.0
 */
public class UpgradeTask_Build70010 extends AbstractImmediateUpgradeTask {
    static final Logger LOG = LoggerFactory.getLogger(UpgradeTask_Build70010.class);

    private static final String TRIGGERS = "JQRTZTriggers";
    private static final String SIMPLE_TRIGGERS = "JQRTZSimpleTriggers";
    private static final String CRON_TRIGGERS = "JQRTZCronTriggers";

    private static final String JOB_NAME = "jobName";
    private static final String TRIGGER_NAME = "triggerName";
    private static final String NEXT_FIRE_TIME = "nextFireTime";
    private static final String TRIGGER_TYPE = "triggerType";
    private static final String START_TIME = "startTime";
    private static final String JOB_DATA = "jobData";

    private static final String REPEAT_INTERVAL = "repeatInterval";
    private static final String CRON_EXPRESSION = "cronExpression";
    private static final String TIME_ZONE_ID = "timeZoneId";

    private static final String TRIGGER_TYPE_SIMPLE = "SIMPLE";
    private static final String TRIGGER_TYPE_CRON = "CRON";

    final EntityEngine entityEngine;

    public UpgradeTask_Build70010(final EntityEngine entityEngine) {
        this.entityEngine = entityEngine;
    }

    @Override
    public int getBuildNumber() {
        return 70010;
    }

    @Override
    public String getShortDescription() {
        return "Migrate atlassian-scheduler data from Quartz to Caesium.";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception {
        new Upgrader().run();
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        return false;
    }

    class Upgrader {
        private final Set<String> existingClusteredJobIds = getClusteredJobIds();

        void run() {
            LOG.debug("existingClusteredJobIds: {}", existingClusteredJobIds);
            migrateSimpleTriggers();
            migrateCronTriggers();
        }

        private Set<String> getClusteredJobIds() {
            final List<String> jobIds = Select.distinctString(JOB_ID)
                    .from(CLUSTERED_JOB)
                    .runWith(entityEngine)
                    .asList();
            return ImmutableSet.copyOf(jobIds);
        }

        private void migrateSimpleTriggers() {
            LOG.debug("migrateSimpleTriggers");

            final List<GenericValue> triggers = getTriggers(TRIGGER_TYPE_SIMPLE);
            final Map<String, Long> simpleTriggers = Maps.newHashMapWithExpectedSize(triggers.size());

            Select.columns(TRIGGER_NAME, REPEAT_INTERVAL)
                    .from(SIMPLE_TRIGGERS)
                    .runWith(entityEngine)
                    .visitWith(gv -> simpleTriggers.put(gv.getString(TRIGGER_NAME), gv.getLong(REPEAT_INTERVAL)));

            for (GenericValue trigger : triggers) {
                migrateSimpleTrigger(trigger, simpleTriggers.get(trigger.getString(TRIGGER_NAME)));
            }
        }

        private void migrateSimpleTrigger(final GenericValue trigger, final Long repeatInterval) {
            final String jobId = trigger.getString(TRIGGER_NAME);
            LOG.debug("migrateSimpleTrigger: {}", jobId);

            final Date firstRunTime = toDate(trigger.getLong(START_TIME));
            final Date nextRunTime = toDate(trigger.getLong(NEXT_FIRE_TIME));
            final long intervalInMillis = (repeatInterval != null) ? repeatInterval : 0L;
            final Schedule schedule = Schedule.forInterval(intervalInMillis, firstRunTime);
            createClusteredJob(trigger, jobId, nextRunTime, schedule);
        }

        private void migrateCronTriggers() {
            LOG.debug("migrateCronTriggers");

            final List<GenericValue> triggers = getTriggers(TRIGGER_TYPE_CRON);
            final Map<String, GenericValue> cronTriggers = Maps.newHashMapWithExpectedSize(triggers.size());

            Select.columns(TRIGGER_NAME, CRON_EXPRESSION, TIME_ZONE_ID)
                    .from(CRON_TRIGGERS)
                    .runWith(entityEngine)
                    .visitWith(gv -> cronTriggers.put(gv.getString(TRIGGER_NAME), gv));

            for (GenericValue trigger : triggers) {
                migrateCronTrigger(trigger, cronTriggers.get(trigger.getString(TRIGGER_NAME)));
            }
        }

        private void migrateCronTrigger(final GenericValue trigger, final GenericValue cronTrigger) {
            final String jobId = trigger.getString(TRIGGER_NAME);
            LOG.debug("migrateCronTrigger: {}", jobId);

            final Date nextRunTime = toDate(trigger.getLong(NEXT_FIRE_TIME));
            final String cronExpression = repairCronExpression(jobId, cronTrigger.getString(CRON_EXPRESSION));
            final String timeZoneId = cronTrigger.getString(TIME_ZONE_ID);
            final TimeZone timeZone = (timeZoneId != null) ? TimeZone.getTimeZone(timeZoneId) : null;
            final Schedule schedule = Schedule.forCronExpression(cronExpression, timeZone);

            createClusteredJob(trigger, jobId, nextRunTime, schedule);
        }

        private void createClusteredJob(final GenericValue trigger, final String jobId, final Date nextRunTime,
                                        final Schedule schedule) {
            final ClusteredJob clusteredJob = ImmutableClusteredJob.builder()
                    .jobId(JobId.of(jobId))
                    .jobRunnerKey(JobRunnerKey.of(trigger.getString(JOB_NAME)))
                    .nextRunTime(nextRunTime)
                    .schedule(schedule)
                    .parameters((byte[]) trigger.get(JOB_DATA))
                    .build();
            createClusteredJob(clusteredJob);
        }

        private void createClusteredJob(final ClusteredJob clusteredJob) {
            deleteExistingJob(clusteredJob.getJobId().toString());
            entityEngine.createValue(CLUSTERED_JOB, new OfBizClusteredJob(null, clusteredJob));
        }

        // For idempotency
        private void deleteExistingJob(final String jobId) {
            if (existingClusteredJobIds.contains(jobId)) {
                Delete.from(CLUSTERED_JOB)
                        .whereEqual(JOB_ID, jobId)
                        .execute(entityEngine);
            }
        }

        private List<GenericValue> getTriggers(final String triggerType) {
            return Select.columns(JOB_NAME, TRIGGER_NAME, NEXT_FIRE_TIME, START_TIME, JOB_DATA)
                    .from(TRIGGERS)
                    .whereEqual(TRIGGER_TYPE, triggerType)
                    .runWith(entityEngine)
                    .asList();
        }

        @Nullable
        private Date toDate(@Nullable Long millis) {
            return (millis != null) ? new Date(millis) : null;
        }

        private String repairCronExpression(final String jobId, final String cronExpression) {
            final Result result = CronExpressionFixer.repairCronExpression(cronExpression);
            final String repaired = result.getNewCronExpression().orElse(null);
            if (repaired != null) {
                LOG.warn("Repairing malformed cron expression for scheduled job '{}': '{}' => '{}'",
                        jobId, cronExpression, repaired);
                return repaired;
            }

            return result.getCronSyntaxException()
                    .map(cse -> handleFailedRepair(jobId, cronExpression, cse))
                    .orElse(cronExpression);
        }

        private String handleFailedRepair(String jobId, String cronExpression, CronSyntaxException cse) {
            // If it is a filter subscription, then we can at least restore the default subscription schedule
            // so that the job will run regularly, even though it probably won't be running at the desired times.
            if (jobId.startsWith(DefaultSubscriptionManager.class.getName())) {
                LOG.warn("Reverting filter subscription '{}' to the default schedule: '{}' => '{}': {}",
                        jobId, cronExpression, DEFAULT_CRONSTRING, cse.toString());
                return DEFAULT_CRONSTRING;
            }

            // Otherwise, we have no absolutely no idea what to do with it, so just leave it broken
            LOG.error("Scheduled job '{}' has an invalid cron expression '{}'; it will never be run",
                    jobId, cronExpression, cse);
            return cronExpression;
        }
    }
}
