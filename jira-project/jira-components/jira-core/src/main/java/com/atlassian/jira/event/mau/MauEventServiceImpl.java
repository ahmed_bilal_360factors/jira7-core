package com.atlassian.jira.event.mau;

import com.atlassian.analytics.api.events.MauEvent;
import com.atlassian.application.api.Application;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.core.util.Clock;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.type.ProjectTypeManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.util.concurrent.LazyReference;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static com.atlassian.jira.security.JiraAuthenticationContextImpl.getRequestCache;
import static com.atlassian.jira.security.RequestCacheKeys.MAU_EVENT_APPLICATION_CACHE;
import static java.util.Optional.ofNullable;

public class MauEventServiceImpl implements MauEventService {
    private final ProjectTypeManager projectTypeManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final EventPublisher eventPublisher;
    private final Clock clock;
    private final JiraProperties jiraProperties;
    private final Cache<LastSentKey, Long> lastSentCache;

    public MauEventServiceImpl(
            final ProjectTypeManager projectTypeManager,
            final JiraAuthenticationContext jiraAuthenticationContext,
            final CacheManager cacheManager,
            final EventPublisher eventPublisher,
            final Clock clock,
            final JiraProperties jiraProperties,
            final PluginAccessor pluginAccessor) {
        this.projectTypeManager = projectTypeManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.eventPublisher = eventPublisher;
        this.clock = clock;
        this.jiraProperties = jiraProperties;
        this.lastSentCache = cacheManager.getCache(
                MauEventServiceImpl.class.getName() + ".lastSent",
                null,
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).build());
    }

    @Override
    public void setApplicationForThread(@Nonnull final MauApplicationKey applicationKey) {
        getRequestCache().put(MAU_EVENT_APPLICATION_CACHE, applicationKey);
    }

    @Override
    public void setApplicationForThreadBasedOnProject(@Nullable final Project project) {
        if (project == null) {
            return;
        }

        final Option<Application> application = projectTypeManager.getApplicationWithType(project.getProjectTypeKey());
        final MauApplicationKey mauKey = application
                .map(app -> MauApplicationKey.forApplication(app.getKey()))
                .getOrElse(MauApplicationKey.unknown());
        setApplicationForThread(mauKey);
    }

    @Override
    public void triggerEvent() {
        final ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
        if (isAnonymous(user)) {
            return;
        }

        final LastSentKey key = getKeyWithCurrentApplication();
        final long currentTime = clock.getCurrentDate().getTime();

        //we don't care about rate limiting in devmode.
        if (!jiraProperties.isDevMode() && wasLastSentLessThanFiveMinutesAgo(key, currentTime)) {
            return;
        }

        eventPublisher.publish(new MauEvent.Builder().application(key.getApplicationKey().getKey()).build(key.getEmail()));
        lastSentCache.put(key, currentTime);
    }

    @Override
    @Nullable
    public LastSentKey getKeyWithCurrentApplication() {
        final ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();

        if (isAnonymous(user)) {
            return null;
        }

        final MauApplicationKey mauApplicationKey =
                ofNullable((MauApplicationKey) getRequestCache().get(MAU_EVENT_APPLICATION_CACHE))
                        .orElse(MauApplicationKey.unknown());

        final String emailAddress = user.getEmailAddress();
        final long userId = user.getId();
        return new LastSentKey(emailAddress, userId, mauApplicationKey);
    }

    private boolean isAnonymous(final ApplicationUser user) {
        return user == null;
    }

    private boolean wasLastSentLessThanFiveMinutesAgo(final LastSentKey key, final long currentTime) {
        final long lastSent = ofNullable(lastSentCache.get(key)).orElse(0L);
        return lastSent > currentTime - TimeUnit.MINUTES.toMillis(5);
    }
}