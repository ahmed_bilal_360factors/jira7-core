package com.atlassian.jira.project.template.module;

import com.atlassian.jira.project.template.hook.AddProjectModule;

import javax.annotation.concurrent.Immutable;
import java.util.Optional;

/**
 * A project template module as defined in the {@code atlassian-plugin.xml}.
 *
 * @since v2.14
 */
@Immutable
public class ProjectTemplateModule {
    private final String key;
    private final Integer weight;
    private final String labelKey;
    private final String descriptionKey;
    private final Optional<String> longDescriptionKey;
    private final String infoSoyPath;
    private final String projectTypeKey;

    private final Icon icon;
    private final Icon backgroundIcon;

    private final AddProjectModule addProjectModule;

    ProjectTemplateModule(
            String key,
            Integer weight,
            String labelKey,
            String descriptionKey,
            Optional<String> longDescriptionKey,
            Icon icon,
            Icon backgroundIcon,
            AddProjectModule addProjectModule,
            String infoSoyPath,
            String projectTypeKey) {
        this.key = key;
        this.weight = weight;
        this.labelKey = labelKey;
        this.descriptionKey = descriptionKey;
        this.longDescriptionKey = longDescriptionKey;
        this.icon = icon;
        this.backgroundIcon = backgroundIcon;
        this.addProjectModule = addProjectModule;
        this.infoSoyPath = infoSoyPath;
        this.projectTypeKey = projectTypeKey;
    }

    /**
     * Returns this template module's complete key. Format is {@code "plugin.key:module.key"}.
     *
     * @return the template module key.
     * @see com.atlassian.plugin.ModuleDescriptor#getCompleteKey()
     */
    public String key() {
        return key;
    }

    /**
     * Returns the weight.
     *
     * @return the weight
     */
    public Integer weight() {
        return weight;
    }

    /**
     * Returns the labelKey.
     *
     * @return the labelKey
     */
    public String labelKey() {
        return labelKey;
    }

    /**
     * Returns the descriptionKey.
     *
     * @return the descriptionKey
     */
    public String descriptionKey() {
        return descriptionKey;
    }

    /**
     * Returns an optional longDescriptionKey.
     *
     * @return the longDescriptionKey. Could be empty if none was supplied in the plugin module.
     */
    public Optional<String> longDescriptionKey() {
        return longDescriptionKey;
    }

    /**
     * Returns the icon.
     *
     * @return the icon
     */
    public Icon icon() {
        return icon;
    }

    /**
     * Returns the backgroundIcon.
     *
     * @return the backgroundIcon
     */
    public Icon backgroundIcon() {
        return backgroundIcon;
    }

    /**
     * Returns whether an AddProjectModule is defined for this {@link ProjectTemplateModule}
     *
     * @return whether AddProjectModule is defined
     */
    public boolean hasAddProjectModule() {
        return addProjectModule != null;
    }

    /**
     * Returns the AddProjectModule.
     *
     * @return the name of the AddProjectModule
     */
    public AddProjectModule addProjectModule() {
        return addProjectModule;
    }

    /**
     * Returns the path to the soy template which explains the template.
     *
     * @return the path to information soy template.
     */
    public String getInfoSoyPath() {
        return infoSoyPath;
    }

    /**
     * Returns the key of the project type for projects created with this blueprint.
     *
     * @return the key of the project type for projects created with this blueprint.
     */
    public String projectTypeKey() {
        return projectTypeKey;
    }
}
