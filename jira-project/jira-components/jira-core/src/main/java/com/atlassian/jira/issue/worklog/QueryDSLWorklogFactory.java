package com.atlassian.jira.issue.worklog;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.model.querydsl.QWorklog;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.QueryDSLProjectRoleFactory;
import com.querydsl.core.Tuple;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Creates {@link com.atlassian.jira.issue.worklog.Worklog} implementations from QueryDSL tuple.
 *
 * @since 7.0
 */
public class QueryDSLWorklogFactory {
    private final IssueManager issueManager;
    private final QueryDSLProjectRoleFactory queryDSLProjectRoleFactory;

    public QueryDSLWorklogFactory(final IssueManager issueManager, final QueryDSLProjectRoleFactory queryDSLProjectRoleFactory) {
        this.issueManager = issueManager;
        this.queryDSLProjectRoleFactory = queryDSLProjectRoleFactory;
    }

    public Worklog createWorklog(final Tuple tuple) {
        if (tuple == null) {
            return null;
        }
        final Long issueId = tuple.get(QWorklog.WORKLOG.issue);
        final Issue issueForId = getIssueForId(issueId);
        return createWorklog(issueForId, tuple);
    }

    public Worklog createWorklog(final Issue issue, final Tuple tuple) {
        final Timestamp createdTimestamp = tuple.get(QWorklog.WORKLOG.created);
        final Timestamp updatedTimestamp = tuple.get(QWorklog.WORKLOG.updated);
        final Long roleLevel = tuple.get(QWorklog.WORKLOG.rolelevel);

        final ProjectRole projectRole = roleLevel != null ?
                queryDSLProjectRoleFactory.createProjectRole(tuple) : null;


        final Timestamp startDate = tuple.get(QWorklog.WORKLOG.startdate);
        return new WorklogImpl2(issue,
                tuple.get(QWorklog.WORKLOG.id),
                tuple.get(QWorklog.WORKLOG.author),
                tuple.get(QWorklog.WORKLOG.body),
                startDate != null ? new Date(startDate.getTime()) : null,
                tuple.get(QWorklog.WORKLOG.grouplevel),
                roleLevel,
                tuple.get(QWorklog.WORKLOG.timeworked),
                tuple.get(QWorklog.WORKLOG.updateauthor),
                createdTimestamp != null ? new Date(createdTimestamp.getTime()) : null,
                updatedTimestamp != null ? new Date(updatedTimestamp.getTime()) : null,
                projectRole);
    }

    public Worklog createWorklogWithAliasedIdColumn(final Tuple tuple) {
        if (tuple == null) {
            return null;
        }
        final Long issueId = tuple.get(QWorklog.WORKLOG.issue);
        final Issue issueForId = getIssueForId(issueId);
        return createWorklogWithAliasedIdColumn(issueForId, tuple);
    }

    private Worklog createWorklogWithAliasedIdColumn(final Issue issue, final Tuple tuple) {
        final Timestamp createdTimestamp = tuple.get(QWorklog.WORKLOG.created);
        final Timestamp updatedTimestamp = tuple.get(QWorklog.WORKLOG.updated);
        final Long roleLevel = tuple.get(QWorklog.WORKLOG.rolelevel);

        final ProjectRole projectRole = roleLevel != null ?
                queryDSLProjectRoleFactory.createProjectRole(tuple) : null;


        final Timestamp startDate = tuple.get(QWorklog.WORKLOG.startdate);
        return new WorklogImpl2(issue,
                tuple.get(QWorklog.WORKLOG.id.as("WORKLOG_ID")),
                tuple.get(QWorklog.WORKLOG.author),
                tuple.get(QWorklog.WORKLOG.body),
                startDate != null ? new Date(startDate.getTime()) : null,
                tuple.get(QWorklog.WORKLOG.grouplevel),
                roleLevel,
                tuple.get(QWorklog.WORKLOG.timeworked),
                tuple.get(QWorklog.WORKLOG.updateauthor),
                createdTimestamp != null ? new Date(createdTimestamp.getTime()) : null,
                updatedTimestamp != null ? new Date(updatedTimestamp.getTime()) : null,
                projectRole);
    }

    private Issue getIssueForId(Long issueId) {
        return issueManager.getIssueObject(issueId);
    }

}
