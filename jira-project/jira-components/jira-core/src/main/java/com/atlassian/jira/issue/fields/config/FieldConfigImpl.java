package com.atlassian.jira.issue.fields.config;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.persistence.FieldConfigPersister;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class FieldConfigImpl implements FieldConfig, Comparable<FieldConfigImpl> {
    private final Long id;
    private final String name;
    private final String description;
    private final List<FieldConfigItem> configItems;
    private final String fieldId;

    public FieldConfigImpl(final Long id, final String name, final String description, List<FieldConfigItemType> configItemTypes, final String fieldId) {
        this.id = id;
        this.name = StringUtils.abbreviate(name, FieldConfigPersister.ENTITY_LONG_TEXT_LENGTH);
        this.description = description;
        this.fieldId = fieldId;

        if (configItemTypes == null) {
            configItemTypes = Collections.emptyList();
        }
        final List<FieldConfigItem> configItems = new ArrayList<FieldConfigItem>(configItemTypes.size());
        for (final FieldConfigItemType type : configItemTypes) {
            configItems.add(new FieldConfigItemImpl(type, this));
        }

        this.configItems = Collections.unmodifiableList(configItems);
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public CustomField getCustomField() {
        // Load the custom field statically.... hmmm...
        final CustomField customField = ComponentAccessor.getCustomFieldManager().getCustomFieldObject(fieldId);
        if (customField == null) {
            throw new DataAccessException("No custom field for " + fieldId + ". This should not happen. Data is likely to be corrupt.");
        }
        return customField;
    }

    @Override
    public String getFieldId() {
        return fieldId;
    }

    @Override
    public List<FieldConfigItem> getConfigItems() {
        return configItems;
    }

    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof FieldConfigImpl && equals((FieldConfigImpl) o));
    }

    private boolean equals(@Nonnull final FieldConfigImpl other) {
        return Objects.equals(id, other.id) && Objects.equals(name, other.name);
    }

    @Override
    public int compareTo(@Nonnull final FieldConfigImpl rhs) {
        return new CompareToBuilder().append(name, rhs.name).append(id, rhs.id).toComparison();
    }

    public int hashCode() {
        return Objects.hash(id, name);
    }
}
