package com.atlassian.jira.config.feature;

import com.atlassian.jira.config.FeatureFlag;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;

import java.util.List;
import java.util.Set;

class FeatureFlagParser {
    @SuppressWarnings("unchecked")
    static Set<FeatureFlag> parseFlags(Element descriptorE) {
        Set<FeatureFlag> flags = Sets.newHashSet();
        List<Element> flagElements = descriptorE.elements("flag");
        for (Element e : flagElements) {
            String key = e.attributeValue("key");
            boolean defaultState = parseDefault(e.attributeValue("default", "false"));
            if (StringUtils.isNotEmpty(key)) {
                FeatureFlag featureFlag = FeatureFlag.featureFlag(key).defaultedTo(defaultState);
                flags.add(featureFlag);
            }
        }
        return flags;
    }

    private static boolean parseDefault(final String s) {
        return s != null && ("true".equalsIgnoreCase(s) || "on".equalsIgnoreCase(s));
    }

}
