package com.atlassian.jira.cache.slomo;

import com.atlassian.cache.CachedReference;
import com.atlassian.cache.CachedReferenceListener;
import com.atlassian.cache.ManagedCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Decorator that makes a slow version of a {@code CachedReference}.
 *
 * @since v7.1.0
 */
@ParametersAreNonnullByDefault
public class SloMoCachedReference<V> implements CachedReference<V> {
    private static final Logger LOG = LoggerFactory.getLogger(SloMoCachedReference.class);

    private final SloMoCacheManager manager;
    private final CachedReference<V> delegate;

    public SloMoCachedReference(final SloMoCacheManager manager, final CachedReference<V> delegate) {
        this.manager = manager;
        this.delegate = delegate;
    }

    @Override
    @Nonnull
    public V get() {
        manager.sleep();
        return delegate.get();
    }

    @Override
    public void reset() {
        manager.sleep();
        delegate.reset();
    }

    @Override
    public void addListener(final CachedReferenceListener<V> listener, final boolean includeValues) {
        LOG.warn("CachedReferenceListener for '{}' will not be supported by VCache",
                ((ManagedCache) delegate).getName(),
                new IllegalArgumentException("Added here"));
        delegate.addListener(listener, includeValues);
    }

    @Override
    public void removeListener(final CachedReferenceListener<V> listener) {
        delegate.removeListener(listener);
    }
}
