package com.atlassian.jira;

/**
 * Represents a JIRA component that contains a cache.
 *
 * @since v7.0
 */
public interface CachingComponent {
    /**
     * Clear the cache within the component.
     */
    void clearCache();
}
