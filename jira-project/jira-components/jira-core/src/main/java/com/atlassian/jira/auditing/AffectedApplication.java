package com.atlassian.jira.auditing;

import com.atlassian.jira.application.ApplicationRole;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * This class carries information about entries related to application roles in the Audit Log. This information appears
 * on the title of the entry.
 *
 * @since 7.0
 */
public class AffectedApplication implements AssociatedItem {
    private final String name;
    private final String key;

    public AffectedApplication(final ApplicationRole role) {
        this.name = role.getName();
        this.key = role.getKey().value();
    }

    @Nonnull
    @Override
    public String getObjectName() {
        return name;
    }

    @Nullable
    @Override
    public String getObjectId() {
        // The ID of the role is its key.
        return key;
    }

    @Nullable
    @Override
    public String getParentName() {
        return null;
    }

    @Nullable
    @Override
    public String getParentId() {
        return null;
    }

    @Nonnull
    @Override
    public Type getObjectType() {
        return Type.APPLICATION_ROLE;
    }
}
