package com.atlassian.jira.help;

import com.atlassian.jira.util.BuildUtilsInfo;

/**
 * This is the actual {@link com.atlassian.jira.help.HelpUrlsParser} in PICO. It provides
 * an {@link DefaultHelpUrlsParser} instance with an initial {@link #onDemand} that does a runtime check
 * to determine how to process {@code .ondemand} properties.
 *
 * @since v6.2.4
 */
public class InitialHelpUrlsParser extends DefaultHelpUrlsParser {
    public InitialHelpUrlsParser(final BuildUtilsInfo buildUtilsInfo, final LocalHelpUrls localUrls) {
        super(new SimpleHelpUrlBuilder.Factory(buildUtilsInfo.getDocVersion()), localUrls, null, null);
    }
}
