package com.atlassian.jira.project.template.module;

import com.atlassian.jira.project.template.descriptor.ConfigTemplateParser;
import com.atlassian.jira.project.template.descriptor.ProjectTemplateModuleDescriptor;
import com.atlassian.jira.project.template.hook.AddProjectModule;
import com.atlassian.jira.project.template.hook.AddProjectModuleImpl;
import com.atlassian.plugin.module.ModuleFactory;

/**
 * Builder for {@link com.atlassian.jira.project.template.hook.AddProjectModule}.
 */
public class AddProjectModuleBuilder {
    private ConfigTemplateParser configTemplateParser;
    private ModuleFactory moduleFactory;
    private ProjectTemplateModuleDescriptor moduleDescriptor;

    private String addProjectHookClassName;
    private String templateConfigurationFile;

    public AddProjectModuleBuilder(ConfigTemplateParser configTemplateParser,
                                   ModuleFactory moduleFactory, ProjectTemplateModuleDescriptor moduleDescriptor) {
        this.configTemplateParser = configTemplateParser;
        this.moduleFactory = moduleFactory;
        this.moduleDescriptor = moduleDescriptor;
    }

    public AddProjectModuleBuilder addProjectHookClassName(String addProjectHookClassName) {
        this.addProjectHookClassName = addProjectHookClassName;
        return this;
    }

    public AddProjectModuleBuilder templateConfigurationFile(String templateConfigurationFile) {
        this.templateConfigurationFile = templateConfigurationFile;
        return this;
    }

    public AddProjectModule build() {
        return new AddProjectModuleImpl(
                moduleFactory,
                moduleDescriptor,
                addProjectHookClassName,
                templateConfigurationFile,
                configTemplateParser);
    }
}
