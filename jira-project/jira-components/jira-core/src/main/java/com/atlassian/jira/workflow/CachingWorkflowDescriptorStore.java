package com.atlassian.jira.workflow;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.cache.CachedReference;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.event.workflow.WorkflowDescriptorXmlUpdatedEvent;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.util.dbc.Assertions;
import com.opensymphony.workflow.FactoryException;
import com.opensymphony.workflow.loader.WorkflowDescriptor;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Used to cache workflowDescriptors in JIRA. This caches {@link ImmutableWorkflowDescriptor}s.
 * These objects are very heavy weight and ideally we would not cache them, but it is the only way to
 * quickly give JIRA access to workflow objects. This is because the safe thing to cache is the workflow XML string but
 * converting this to an object graph will be expensive.  Also please note that the implementation of
 * {@link ImmutableWorkflowDescriptor} cannot guarantee 100% immutability.
 * <p>
 * This is essentially replacing the store in the {@link com.atlassian.jira.workflow.JiraWorkflowFactory}, but it adds
 * some more concurrency controls to ensure consistency with the underlying store (such as the
 * {@link com.atlassian.jira.workflow.OfBizWorkflowDescriptorStore})
 *
 * @since v3.13
 */
@EventComponent
public class CachingWorkflowDescriptorStore implements WorkflowDescriptorStore {
    private final Cache<String, Optional<ImmutableWorkflowDescriptor>> workflowCache;
    private final CachedReference<String[]> allNamesCache;
    private final WorkflowDescriptorStore delegate;

    public CachingWorkflowDescriptorStore(final WorkflowDescriptorStore delegate,
                                          final CacheManager cacheManager) {
        this.delegate = delegate;

        workflowCache = cacheManager.getCache(CachingWorkflowDescriptorStore.class.getName() + ".workflowCache",
                this::loadWorkflowDescriptor,
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).build());
        allNamesCache = cacheManager.getCachedReference(CachingWorkflowDescriptorStore.class.getName() + ".allNamesCache",
                delegate::getWorkflowNames);
    }

    @EventListener
    public void onClearCache(final ClearCacheEvent event) {
        workflowCache.removeAll();
        allNamesCache.reset();
    }

    @EventListener
    public void onWorkflowDescriptorXmlUpdatedEvent(final WorkflowDescriptorXmlUpdatedEvent event) {
        removeWorkflowFromCache(event.getWorkflowName());
    }

    public ImmutableWorkflowDescriptor getWorkflow(@Nonnull final String name) throws FactoryException {
        Assertions.notNull("Workflow name", name);
        return workflowCache.get(name).orElse(null);
    }

    public boolean removeWorkflow(@Nonnull final String name) {
        Assertions.notNull("Workflow name", name);

        boolean deleted;
        try {
            deleted = delegate.removeWorkflow(name);
        } finally {
            removeWorkflowFromCache(name);
        }
        return deleted;
    }

    public boolean saveWorkflow(@Nonnull final String name, @Nonnull final WorkflowDescriptor workflowDescriptor, final boolean replace) throws DataAccessException {
        Assertions.notNull("Workflow name", name);
        Assertions.notNull("workflowDescriptor", workflowDescriptor);

        try {
            return delegate.saveWorkflow(name, workflowDescriptor, replace);
        } finally {
            removeWorkflowFromCache(name);
        }
    }

    public String[] getWorkflowNames() {
        final String[] names = allNamesCache.get();
        return Arrays.copyOf(names, names.length);
    }

    public List<JiraWorkflowDTO> getAllJiraWorkflowDTOs() {
        return delegate.getAllJiraWorkflowDTOs();
    }

    private void removeWorkflowFromCache(final String name) {
        workflowCache.remove(name);
        allNamesCache.reset();
    }

    @Nonnull
    private Optional<ImmutableWorkflowDescriptor> loadWorkflowDescriptor(@Nonnull final String name) {
        try {
            return Optional.ofNullable(delegate.getWorkflow(name));
        } catch (final FactoryException e) {
            throw new RuntimeException(e);
        }
    }
}
