package com.atlassian.jira.portal;

import com.atlassian.core.ofbiz.util.OFBizPropertyUtils;
import com.atlassian.fugue.ImmutableMaps;
import com.atlassian.fugue.Iterables;
import com.atlassian.fugue.Option;
import com.atlassian.gadgets.dashboard.Color;
import com.atlassian.jira.entity.EntityUtils;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.entity.property.JsonEntityPropertyManager;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.ofbiz.DatabaseIterable;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizListIterator;
import com.atlassian.jira.util.Resolver;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.collect.EnclosedIterable;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.ModuleCompleteKey;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericValue;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.entity.property.EntityPropertyType.DASHBOARD_ITEM_PROPERTY;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

public class OfbizPortletConfigurationStore implements PortletConfigurationStore {
    public static final String TABLE = "PortletConfiguration";
    public static final String USER_PREFERENCES_TABLE = "GadgetUserPreference";
    private final OfBizDelegator delegator;
    private final JsonEntityPropertyManager jsonEntityPropertyManager;

    public OfbizPortletConfigurationStore(final OfBizDelegator delegator, final JsonEntityPropertyManager jsonEntityPropertyManager) {

        notNull("delegator", delegator);
        notNull("jsonEntityPropertyManager", jsonEntityPropertyManager);

        this.delegator = delegator;
        this.jsonEntityPropertyManager = jsonEntityPropertyManager;
    }

    public List<PortletConfiguration> getByPortalPage(final Long portalPageId) {
        notNull("portalPageId", portalPageId);

        final List<GenericValue> portletConfigsListGVs = delegator.findByAnd(TABLE, ImmutableMap.of(Columns.PORTALPAGE, portalPageId));
        final List<PortletConfiguration> portletConfigs = new ArrayList<PortletConfiguration>(portletConfigsListGVs.size());

        for (final GenericValue portletConfigGV : portletConfigsListGVs) {
            portletConfigs.add(createConfigurationFromGv(portletConfigGV));
        }

        return portletConfigs;
    }

    public PortletConfiguration getByPortletId(final Long portletId) {
        notNull("portletId", portletId);

        final List<GenericValue> portletConfigsListGVs = delegator.findByAnd(TABLE, MapBuilder.singletonMap(Columns.ID, portletId));
        if (!portletConfigsListGVs.isEmpty()) {
            return createConfigurationFromGv(portletConfigsListGVs.get(0));
        } else {
            return null;
        }
    }

    public void delete(final PortletConfiguration pc) {
        notNull("pc", pc);
        notNull("pc.id", pc.getId());

        final GenericValue gv = getGenericValue(pc);

        if (gv != null) {
            removePropertySet(gv);
            delegator.removeValue(gv);
            removeDashboardItemProperties(pc);
        }
    }

    public void updateGadgetPosition(final Long gadgetId, final int row, final int column, final Long dashboardId) {
        notNull("gadgetId", gadgetId);
        notNull("dashboardId", dashboardId);

        final int rowsUpdated = delegator.bulkUpdateByPrimaryKey(TABLE, MapBuilder.<String, Object>newBuilder()
                        .add(Columns.ROW, row)
                        .add(Columns.COLUMN, column)
                        .add(Columns.PORTALPAGE, dashboardId)
                        .toMap(),
                CollectionBuilder.newBuilder(gadgetId).asList());
        if (rowsUpdated != 1) {
            throw new DataAccessException("Gadget position for gadget with id '" + gadgetId + "' not updated correctly.");
        }
    }

    public void updateGadgetColor(final Long gadgetId, final Color color) {
        notNull("gadgetId", gadgetId);
        notNull("color", color);

        final int rowsUpdated = delegator.bulkUpdateByPrimaryKey(TABLE, MapBuilder.singletonMap(Columns.COLOR, color.toString()), CollectionBuilder.list(gadgetId));
        if (rowsUpdated != 1) {
            throw new DataAccessException("Gadget color for gadget with id '" + gadgetId + "' not updated correctly.");
        }
    }

    public void updateUserPrefs(final Long gadgetId, final Map<String, String> userPrefs) {
        notNull("gadgetId", gadgetId);
        notNull("userPrefs", userPrefs);

        if (isLocalDashboardItemWithoutReplacementUri(gadgetId)) {
            updateLocalDashboardItemProperties(gadgetId, userPrefs);
        } else {
            updateOpenSocialGadgetUserPreferences(gadgetId, userPrefs);
        }
    }

    public void store(final PortletConfiguration pc) {
        notNull("pc", pc);
        notNull("pc.id", pc.getId());

        final GenericValue gv = getGenericValue(pc);
        if (gv == null) {
            throw new IllegalArgumentException("Can't store a portlet configuration that has no database entry.  Must add.");
        }

        gv.set(Columns.PORTALPAGE, pc.getDashboardPageId());
        gv.set(Columns.COLUMN, pc.getColumn());
        gv.set(Columns.ROW, pc.getRow());
        gv.set(Columns.GADGET_XML, pc.getOpenSocialSpecUri().map(new Function<URI, String>() {
            @Override
            public String apply(final URI input) {
                return input.toASCIIString();
            }
        }).getOrNull());
        gv.set(Columns.COLOR, pc.getColor().toString());
        gv.set(Columns.MODULE_KEY, pc.getCompleteModuleKey().map(new Function<ModuleCompleteKey, String>() {
            @Override
            public String apply(final ModuleCompleteKey input) {
                return input.getCompleteKey();
            }
        }).getOrNull());
        updateUserPrefs(pc.getId(), pc.getUserPrefs(), pc.getOpenSocialSpecUri(), pc.getCompleteModuleKey());

        delegator.store(gv);
    }

    public PortletConfiguration addGadget(final Long pageId, final Long portletConfigurationId, final Integer column, final Integer row,
                                          final URI gadgetXml, final Color color, final Map<String, String> userPreferences) {
        return addDashboardItem(pageId, portletConfigurationId, column, row, Option.some(gadgetXml), color, userPreferences, Option.<ModuleCompleteKey>none());
    }

    @Override
    public PortletConfiguration addDashboardItem(final Long pageId, final Long portletConfigurationId, final Integer column, final Integer row,
                                                 final Option<URI> openSocialSpecUri, final Color color, final Map<String, String> userPreferences, final Option<ModuleCompleteKey> moduleKey) {
        notNull("pageId", pageId);
        notNull("column", column);
        notNull("row", row);
        notNull("color", color);
        notNull("userPreferences", userPreferences);

        final GenericValue gv = EntityUtils.createValue(TABLE, MapBuilder.<String, Object>newBuilder()
                .add(Columns.PORTALPAGE, pageId)
                .add(Columns.ID, portletConfigurationId)
                .add(Columns.COLUMN, column)
                .add(Columns.ROW, row)
                .add(Columns.GADGET_XML, openSocialSpecUri.map(new Function<URI, String>() {
                    @Override
                    public String apply(final URI input) {
                        return input.toASCIIString();
                    }
                }).getOrNull())
                .add(Columns.COLOR, color.name())
                .add(Columns.MODULE_KEY, moduleKey.map(new Function<ModuleCompleteKey, String>() {
                    @Override
                    public String apply(final ModuleCompleteKey input) {
                        return input.getCompleteKey();
                    }
                }).getOrNull())
                .toMap());

        updateUserPrefs(gv.getLong("id"), userPreferences, openSocialSpecUri, moduleKey);

        return createConfigurationFromGv(gv);
    }

    public EnclosedIterable<PortletConfiguration> getAllPortletConfigurations() {
        Resolver<GenericValue, PortletConfiguration> resolver = new Resolver<GenericValue, PortletConfiguration>() {
            public PortletConfiguration get(final GenericValue input) {
                return createConfigurationFromGv(input);
            }
        };

        return new DatabaseIterable<PortletConfiguration>(-1, resolver) {
            protected OfBizListIterator createListIterator() {
                return delegator.findListIteratorByCondition(TABLE, null);
            }
        };
    }

    protected void removePropertySet(final GenericValue gv) {
        OFBizPropertyUtils.removePropertySet(gv);
    }

    @VisibleForTesting
    Map<String, String> getUserPreferences(final Long id, final Option<URI> openSocialSpecUri, final Option<ModuleCompleteKey> completeModuleKey) {
        // according to migration plan in comment
        // https://ecosystem.atlassian.net/browse/ACJIRA-235?focusedCommentId=146468&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-146468
        // At first stage only connect dashboard items i.e. locally rendered dashboard items without replacement URI,
        // have user preferences in dashboard items' properties.

        if (isLocalDashboardItemWithoutReplacementUri(openSocialSpecUri, completeModuleKey)) {
            return getDashboardItemProperties(id);
        } else {
            return getOpenSocialGadgetUserPreferences(id);
        }
    }

    private void updateUserPrefs(final Long gadgetId, final Map<String, String> userPrefs, final Option<URI> openSocialSpecUri, final Option<ModuleCompleteKey> moduleKey) {
        notNull("gadgetId", gadgetId);
        notNull("userPrefs", userPrefs);

        if (isLocalDashboardItemWithoutReplacementUri(openSocialSpecUri, moduleKey)) {
            updateLocalDashboardItemProperties(gadgetId, userPrefs);
        } else {
            updateOpenSocialGadgetUserPreferences(gadgetId, userPrefs);
        }
    }

    private void updateLocalDashboardItemProperties(final Long gadgetId, final Map<String, String> properties) {
        removeLocalDashboardItemPreferences(gadgetId);
        for (Map.Entry<String, String> property : properties.entrySet()) {
            jsonEntityPropertyManager.put(DASHBOARD_ITEM_PROPERTY.getDbEntityName(), gadgetId, property.getKey(), toJsonString(property.getValue()));
        }
    }

    private String toJsonString(String property) {
        return "\"" + property + "\"";
    }

    private String fromJsonString(String property) {
        return property.substring(1, property.length() - 1);
    }

    private void updateOpenSocialGadgetUserPreferences(final Long gadgetId, final Map<String, String> userPrefs) {
        removeOpenSocialGadgetPreferences(gadgetId);

        for (Map.Entry<String, String> userPref : userPrefs.entrySet()) {
            EntityUtils.createValue(USER_PREFERENCES_TABLE, MapBuilder.<String, Object>build(UserPreferenceColumns.KEY, userPref.getKey(),
                    UserPreferenceColumns.VALUE, userPref.getValue(), UserPreferenceColumns.PORTLETID, gadgetId));
        }
    }

    private GenericValue getGenericValue(final PortletConfiguration pc) {
        return delegator.findById(TABLE, pc.getId());
    }

    private PortletConfiguration createConfigurationFromGv(final GenericValue portletConfigGV) {
        if (portletConfigGV == null) {
            return null;
        }

        final Long id = portletConfigGV.getLong(Columns.ID);
        final Option<URI> openSocialSpecUri = Option.option(getGadgetXmlURI(portletConfigGV.getString(Columns.GADGET_XML), id));
        final String colorString = portletConfigGV.getString(Columns.COLOR);

        //legacy portlets may not have a value for these fields
        final Color gadgetColor = StringUtils.isEmpty(colorString) ? null : Color.valueOf(colorString);

        final Option<ModuleCompleteKey> completeModuleKey = Option.option(portletConfigGV.getString(Columns.MODULE_KEY)).map(new Function<String, ModuleCompleteKey>() {
            @Override
            public ModuleCompleteKey apply(final String input) {
                return new ModuleCompleteKey(input);
            }
        });
        final Map<String, String> userPrefs = getUserPreferences(id, openSocialSpecUri, completeModuleKey);
        return new PortletConfigurationImpl(id, portletConfigGV.getLong(Columns.PORTALPAGE),
                portletConfigGV.getInteger(Columns.COLUMN), portletConfigGV.getInteger(Columns.ROW), openSocialSpecUri,
                gadgetColor, userPrefs, completeModuleKey);
    }

    private void removeDashboardItemProperties(final PortletConfiguration portletConfiguration) {
        if (isLocalDashboardItemWithoutReplacementUri(portletConfiguration.getOpenSocialSpecUri(), portletConfiguration.getCompleteModuleKey())) {
            removeLocalDashboardItemPreferences(portletConfiguration.getId());
        } else {
            removeOpenSocialGadgetPreferences(portletConfiguration.getId());
        }
    }

    private void removeLocalDashboardItemPreferences(final Long dashboardItemId) {
        jsonEntityPropertyManager.deleteByEntity(DASHBOARD_ITEM_PROPERTY.getDbEntityName(), dashboardItemId);
    }

    private int removeOpenSocialGadgetPreferences(final Long dashboardItemId) {
        return delegator.removeByAnd(USER_PREFERENCES_TABLE, ImmutableMap.of(UserPreferenceColumns.PORTLETID, dashboardItemId));
    }

    private boolean isLocalDashboardItemWithoutReplacementUri(final Option<URI> openSocialSpecUri, final Option<ModuleCompleteKey> completeModuleKey) {
        return completeModuleKey.isDefined() && openSocialSpecUri.isEmpty();
    }

    private boolean isLocalDashboardItemWithoutReplacementUri(long id) {
        notNull("portletId", id);

        final List<GenericValue> portletConfigsListGVs = delegator.findByAnd(TABLE, MapBuilder.singletonMap(Columns.ID, id));
        if (portletConfigsListGVs.isEmpty()) {
            throw new IllegalArgumentException("You are trying to update preferences of gadget witch does not exist");
        }
        final GenericValue portletConfigGV = portletConfigsListGVs.get(0);
        final Option<URI> openSocialSpecUri = Option.option(getGadgetXmlURI(portletConfigGV.getString(Columns.GADGET_XML), id));
        final Option<ModuleCompleteKey> completeModuleKey = Option.option(portletConfigGV.getString(Columns.MODULE_KEY)).map(new Function<String, ModuleCompleteKey>() {
            @Override
            public ModuleCompleteKey apply(final String input) {
                return new ModuleCompleteKey(input);
            }
        });
        return isLocalDashboardItemWithoutReplacementUri(openSocialSpecUri, completeModuleKey);
    }

    private ImmutableMap<String, String> getDashboardItemProperties(final Long id) {
        List<String> propertyKeys = jsonEntityPropertyManager.findKeys(DASHBOARD_ITEM_PROPERTY.getDbEntityName(), id);
        return ImmutableMaps.toMap(Iterables.collect(propertyKeys, new Function<String, Option<Map.Entry<String, String>>>() {
            @Override
            public Option<Map.Entry<String, String>> apply(final String propertyKey) {
                return Option.option(jsonEntityPropertyManager.get(DASHBOARD_ITEM_PROPERTY.getDbEntityName(), id, propertyKey)).map(new Function<EntityProperty, Map.Entry<String, String>>() {
                    @Override
                    public Map.Entry<String, String> apply(final EntityProperty entityProperty) {
                        final String value = fromJsonString(entityProperty.getValue());
                        return Maps.immutableEntry(propertyKey, Strings.nullToEmpty(value));
                    }
                });
            }
        }));
    }

    private Map<String, String> getOpenSocialGadgetUserPreferences(final Long id) {
        final Map<String, String> ret = new HashMap<String, String>();
        final List<GenericValue> list = delegator.findByAnd(USER_PREFERENCES_TABLE, MapBuilder.<String, Object>newBuilder().
                add(UserPreferenceColumns.PORTLETID, id).toMap());
        for (GenericValue userPrefGv : list) {
            String value = userPrefGv.getString(UserPreferenceColumns.VALUE);
            //JRA-18125: Oracle/Sybase store empty strings as null.  When reading these we need to convert them back
            //to empty strings!  Userprefs will only ever be strings and should not be null!
            if (value == null) {
                value = "";
            }
            ret.put(userPrefGv.getString(UserPreferenceColumns.KEY), value);
        }

        return ret;
    }

    private URI getGadgetXmlURI(final String gadgetXmlString, final Long portletId) {
        if (StringUtils.isNotEmpty(gadgetXmlString)) {
            try {
                return new URI(gadgetXmlString);
            } catch (URISyntaxException e) {
                throw new DataAccessException("Invalid gadget XML URI stored for portlet with id '" + portletId + "': '" + gadgetXmlString + "'", e);
            }
        }
        return null;
    }

    public static final class Columns {
        public static final String MODULE_KEY = "dashboardModuleCompleteKey";
        public static final String COLOR = "color";
        public static final String GADGET_XML = "gadgetXml";
        public static final String PORTALPAGE = "portalpage";
        public static final String PORTLETKEY = "portletId";
        public static final String COLUMN = "columnNumber";
        public static final String ROW = "position";
        public static final String ID = "id";
    }

    public static final class UserPreferenceColumns {
        public static final String KEY = "userprefkey";
        public static final String VALUE = "userprefvalue";
        public static final String PORTLETID = "portletconfiguration";
    }
}
