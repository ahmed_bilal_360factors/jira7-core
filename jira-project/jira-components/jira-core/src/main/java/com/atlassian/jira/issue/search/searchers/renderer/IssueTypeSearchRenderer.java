package com.atlassian.jira.issue.search.searchers.renderer;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.fields.option.IssueConstantOption;
import com.atlassian.jira.issue.fields.option.Option;
import com.atlassian.jira.issue.fields.option.TextOption;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.constants.SimpleFieldSearchConstants;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.issue.search.searchers.util.SearchContextRenderHelper;
import com.atlassian.jira.issue.transport.FieldValuesHolder;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.query.Query;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.querydsl.core.Tuple;
import com.querydsl.sql.SQLQuery;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import webwork.action.Action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static com.atlassian.jira.config.ConstantsManager.ALL_STANDARD_ISSUE_TYPES;
import static com.atlassian.jira.config.ConstantsManager.ALL_SUB_TASK_ISSUE_TYPES;
import static com.atlassian.jira.model.querydsl.QConfigurationContext.CONFIGURATION_CONTEXT;
import static com.atlassian.jira.model.querydsl.QFieldConfigScheme.FIELD_CONFIG_SCHEME;
import static com.atlassian.jira.model.querydsl.QFieldConfigSchemeIssueType.FIELD_CONFIG_SCHEME_ISSUE_TYPE;
import static com.atlassian.jira.model.querydsl.QFieldConfiguration.FIELD_CONFIGURATION;
import static com.atlassian.jira.model.querydsl.QOptionConfiguration.OPTION_CONFIGURATION;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static java.util.Comparator.comparing;
import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.nullsFirst;

/**
 * A search renderer for the issue type field in the new issue navigator.
 *
 * @since v5.2
 */
public class IssueTypeSearchRenderer extends AbstractSearchRenderer implements SearchRenderer {

    private static final Logger LOG = LoggerFactory.getLogger(IssueTypeSearchRenderer.class);

    /**
     * Maximum number of field configuration IDs to reference in a single query.
     */
    static final int BATCH_SIZE = 990;

    private static final String ALL_STANDARD_ISSUE_TYPES_NAME = "common.filters.allstandardissuetypes";
    private static final String ALL_SUB_TASK_ISSUE_TYPES_NAME = "common.filters.allsubtaskissuetypes";

    // VIEW only
    static final String SELECTED_OPTIONS = "selectedOptions";

    // EDIT only
    static final String OPTION_CSS_CLASSES = "optionCssClasses";
    static final String SELECTED_OPTION_IDS = "selectedOptionIds";
    static final String SPECIAL_OPTIONS = "specialOptions";
    static final String STANDARD_OPTIONS = "standardOptions";
    static final String SUBTASK_OPTIONS = "subtaskOptions";

    // Both
    static final String VALID_OPTION_IDS = "validOptionIds";

    static final Comparator<Option> ISSUE_TYPE_OPTION_COMPARATOR =
            comparing(Option::getName, nullsFirst(naturalOrder()));

    private final ConstantsManager constantsManager;
    private final QueryDslAccessor queryDslAccessor;
    private final PermissionManager permissionManager;
    private final SubTaskManager subTaskManager;

    public IssueTypeSearchRenderer(
            final ApplicationProperties applicationProperties,
            final ConstantsManager constantsManager,
            final QueryDslAccessor queryDslAccessor,
            final PermissionManager permissionManager,
            final SimpleFieldSearchConstants searchConstants,
            final String searcherNameKey,
            final SubTaskManager subTaskManager,
            final VelocityTemplatingEngine templatingEngine,
            final VelocityRequestContextFactory velocityRequestContextFactory) {
        super(velocityRequestContextFactory, applicationProperties,
                templatingEngine, searchConstants, searcherNameKey);

        this.constantsManager = constantsManager;
        this.queryDslAccessor = queryDslAccessor;
        this.permissionManager = permissionManager;
        this.subTaskManager = subTaskManager;
    }

    /**
     * Construct edit HTML parameters and add them to a template parameters map.
     *
     * @param fieldValuesHolder  Contains the values the user has selected.
     * @param searchContext      The search context.
     * @param user               The user performing the search.
     * @param velocityParameters The template parameters.
     */
    public void addEditParameters(final FieldValuesHolder fieldValuesHolder,
                                  final SearchContext searchContext,
                                  final ApplicationUser user,
                                  final Map<String, Object> velocityParameters) {
        new ParameterFactory(fieldValuesHolder, searchContext, user, velocityParameters).addEditParameters();
    }

    /**
     * Construct view HTML parameters and add them to a template parameters map.
     *
     * @param fieldValuesHolder  Contains the values the user has selected.
     * @param searchContext      The search context.
     * @param user               The user performing the search.
     * @param velocityParameters The template parameters.
     */
    public void addViewParameters(final FieldValuesHolder fieldValuesHolder,
                                  final SearchContext searchContext,
                                  final ApplicationUser user,
                                  final Map<String, Object> velocityParameters) {
        new ParameterFactory(fieldValuesHolder, searchContext, user, velocityParameters).addViewParameters();
    }

    @Override
    public String getEditHtml(ApplicationUser user,
                              SearchContext searchContext,
                              FieldValuesHolder fieldValuesHolder,
                              Map<?, ?> displayParameters, Action action) {
        Map<String, Object> velocityParameters = getVelocityParams(user, searchContext, null, fieldValuesHolder,
                displayParameters, action);
        addEditParameters(fieldValuesHolder, searchContext, user, velocityParameters);
        return renderEditTemplate("issuetype-searcher" + EDIT_TEMPLATE_SUFFIX, velocityParameters);
    }

    @Override
    public boolean isShown(ApplicationUser user, SearchContext searchContext) {
        return true;
    }

    @Override
    public String getViewHtml(ApplicationUser user,
                              SearchContext searchContext,
                              FieldValuesHolder fieldValuesHolder,
                              Map<?, ?> displayParameters,
                              Action action) {
        Map<String, Object> velocityParameters = getVelocityParams(user, searchContext, null, fieldValuesHolder,
                displayParameters, action);
        addViewParameters(fieldValuesHolder, searchContext, user, velocityParameters);
        return renderViewTemplate("issuetype-searcher" + VIEW_TEMPLATE_SUFFIX, velocityParameters);
    }

    @Override
    public boolean isRelevantForQuery(ApplicationUser user, Query query) {
        return isRelevantForQuery(SystemSearchConstants.forIssueType().getJqlClauseNames(), query);
    }


    @VisibleForTesting
    Set<String> getVisibleIssueTypeIds(Collection<Project> projects) {
        final List<Long> fcsIds = selectDistinctFieldConfigSchemeIds(projects);
        final Set<String> issueTypeIds = new HashSet<>();

        executeBatched(fcsIds, (db, batch) -> {
            final List<String> ids = selectOptions(fcsIds, db.newSqlQuery()
                    .select(OPTION_CONFIGURATION.optionid)
                    .distinct());
            issueTypeIds.addAll(ids);
        });

        return issueTypeIds;
    }

    @VisibleForTesting
    Map<String, String> getVisibleIssueTypeIdsWithCssInfo(Collection<Project> projects) {
        final List<Long> fcsIds = selectDistinctFieldConfigSchemeIds(projects);

        final Map<String, Set<Long>> visibleIssueTypesWithCssInfo = new HashMap<>();
        executeBatched(fcsIds, (db, batch) -> {
            final List<Tuple> rows = selectOptions(fcsIds, db.newSqlQuery()
                    .select(OPTION_CONFIGURATION.optionid, OPTION_CONFIGURATION.fieldconfig));
            rows.forEach(row -> {
                final String issueTypeId = row.get(OPTION_CONFIGURATION.optionid);
                final Long fieldConfigId = row.get(OPTION_CONFIGURATION.fieldconfig);
                visibleIssueTypesWithCssInfo.computeIfAbsent(issueTypeId, x -> new HashSet<>())
                        .add(fieldConfigId);
            });
        });
        return toCssClassesMap(visibleIssueTypesWithCssInfo);
    }

    private <T> void executeBatched(List<T> items, BiConsumer<DbConnection, List<T>> callback) {
        if (!items.isEmpty()) {
            final List<List<T>> batches = Lists.partition(items, BATCH_SIZE);
            queryDslAccessor.execute(db -> batches.forEach(batch -> callback.accept(db, batch)));
        }
    }

    private static Map<String, String> toCssClassesMap(Map<String, Set<Long>> visibleIssueTypesWithCssInfo) {
        final Map<String, String> result = Maps.newHashMapWithExpectedSize(visibleIssueTypesWithCssInfo.size());
        visibleIssueTypesWithCssInfo.forEach((issueTypeId, fieldConfigIds) ->
                result.put(issueTypeId, StringUtils.join(fieldConfigIds, ' ')));
        return result;
    }

    @VisibleForTesting
    Map<Long, Long> selectFieldConfigSchemeIdByProjectId() {
        // With a very large number of projects and only a few in the context, it might be worth filtering this
        // on the db side.  Not worrying about that, for now.
        final List<Tuple> rows = queryDslAccessor.executeQuery(db -> db.newSqlQuery()
                .select(CONFIGURATION_CONTEXT.project, CONFIGURATION_CONTEXT.fieldconfigscheme)
                .from(CONFIGURATION_CONTEXT)
                .where(CONFIGURATION_CONTEXT.key.eq(IssueFieldConstants.ISSUE_TYPE))
                .fetch());

        final Map<Long, Long> map = Maps.newHashMapWithExpectedSize(rows.size());
        rows.forEach(row -> {
            final Long projectId = row.get(CONFIGURATION_CONTEXT.project);
            final Long fieldConfigSchemeId = row.get(CONFIGURATION_CONTEXT.fieldconfigscheme);
            map.put(projectId, fieldConfigSchemeId);
        });
        return map;
    }

    private List<Long> selectDistinctFieldConfigSchemeIds(Collection<Project> projects) {
        if (projects.isEmpty()) {
            return ImmutableList.of();
        }

        final Map<Long, Long> fcsIdByProjectId = selectFieldConfigSchemeIdByProjectId();
        final Long defaultFcsId = fcsIdByProjectId.get(null);

        // Note: The stream is sorted before collection.  This isn't really necessary, but it should not
        // be prohibitively expensive, helps with manual troubleshooting, and may even help the database
        // search its index more efficiently.  That seems like enough to shift the burden of proof for now.
        return projects.stream()
                .map(project -> fcsIdByProjectId.getOrDefault(project.getId(), defaultFcsId))
                .filter(Objects::nonNull)
                .distinct()
                .sorted()  // See above
                .collect(CollectorsUtil.toNewArrayListWithSizeOf(projects));
    }

    private static <U> List<U> selectOptions(List<Long> fieldConfigSchemeIds, SQLQuery<U> selectColumnStub) {
        return selectColumnStub
                .from(OPTION_CONFIGURATION)
                .innerJoin(FIELD_CONFIGURATION)
                .on(FIELD_CONFIGURATION.id.eq(OPTION_CONFIGURATION.fieldconfig))
                .innerJoin(FIELD_CONFIG_SCHEME_ISSUE_TYPE)
                .on(FIELD_CONFIG_SCHEME_ISSUE_TYPE.fieldconfiguration.eq(FIELD_CONFIGURATION.id))
                .innerJoin(FIELD_CONFIG_SCHEME)
                .on(FIELD_CONFIG_SCHEME.id.eq(FIELD_CONFIG_SCHEME_ISSUE_TYPE.fieldconfigscheme))
                .where(FIELD_CONFIG_SCHEME.id.in(fieldConfigSchemeIds))
                .fetch();
    }


    // This is an ephemeral method-object; it doesn't want or need to copy the collections.
    @SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
    class ParameterFactory {
        private final FieldValuesHolder fieldValuesHolder;
        private final Collection<Project> projects;
        private final Map<String, Object> velocityParameters;
        private final I18nHelper i18n;
        private final Map<String, Option> allOptions;

        ParameterFactory(final FieldValuesHolder fieldValuesHolder, final SearchContext searchContext,
                         final ApplicationUser user, final Map<String, Object> velocityParameters) {
            this.fieldValuesHolder = fieldValuesHolder;
            this.velocityParameters = velocityParameters;
            this.i18n = getI18n(user);
            this.allOptions = getAllOptions();

            final Collection<Project> browseableProjects = permissionManager.getProjects(BROWSE_PROJECTS, user);
            this.projects = applyContextFilter(browseableProjects, searchContext);

            SearchContextRenderHelper.addSearchContextParams(searchContext, velocityParameters);
        }

        private Collection<Project> applyContextFilter(Collection<Project> allProjects, SearchContext searchContext) {
            if (searchContext.isForAnyProjects()) {
                return new ArrayList<>(allProjects);
            }

            // Build temporary set to avoid O(N^2) loop over list.contains
            final Set<Long> projectIds = new HashSet<>(searchContext.getProjectIds());
            return allProjects.stream()
                    .filter(project -> projectIds.contains(project.getId()))
                    .collect(CollectorsUtil.toNewArrayListWithSizeOf(projectIds));
        }

        /**
         * @return A map of all possible options (not just those that are visible), keyed by option ID
         */
        private Map<String, Option> getAllOptions() {
            final Collection<IssueType> allIssueTypes = constantsManager.getAllIssueTypeObjects();
            final Map<String, Option> options = Maps.newHashMapWithExpectedSize(allIssueTypes.size() + 2);
            options.put(ALL_STANDARD_ISSUE_TYPES, allStandardTypes());
            options.put(ALL_SUB_TASK_ISSUE_TYPES, allSubtaskTypes());
            allIssueTypes.stream()
                    .map(IssueConstantOption::new)
                    .forEach(option -> options.put(option.getId(), option));
            return options;
        }

        private Collection<String> getSelectedOptionIds() {
            @SuppressWarnings("unchecked")
            final Collection<String> selected = (Collection<String>) fieldValuesHolder.get(DocumentConstants.ISSUE_TYPE);
            return (selected != null) ? selected : ImmutableList.of();
        }

        private boolean subTasksAreDisabled() {
            return !subTaskManager.isSubTasksEnabled();
        }

        void addViewParameters() {
            final List<Option> selectedOptions = optionList(getSelectedOptionIds());
            final Set<String> validOptionIds = getVisibleIssueTypeIds(projects);

            applySubtaskAndSpecialOptionValidity(validOptionIds);

            velocityParameters.put(SELECTED_OPTIONS, selectedOptions);
            velocityParameters.put(VALID_OPTION_IDS, validOptionIds);
        }

        private void applySubtaskAndSpecialOptionValidity(Set<String> validOptionIds) {
            if (subTasksAreDisabled()) {
                validOptionIds.removeIf(this::isSubTask);
            } else if (hasAtLeastOneSubTask(validOptionIds)) {
                validOptionIds.add(ALL_SUB_TASK_ISSUE_TYPES);
            }
            validOptionIds.add(ALL_STANDARD_ISSUE_TYPES);
        }

        private boolean isSubTask(String optionId) {
            final Option option = allOptions.get(optionId);
            return option != null && IssueConstantOption.isSubTask(option);
        }

        private boolean hasAtLeastOneSubTask(Collection<String> optionIds) {
            return optionStream(optionIds).anyMatch(IssueConstantOption::isSubTask);
        }

        void addEditParameters() {
            final Map<String, Option> selectedOptions = optionMap(getSelectedOptionIds());
            final Map<String, String> cssInfo = getVisibleIssueTypeIdsWithCssInfo(projects);
            final Set<String> validOptionIds = new HashSet<>(cssInfo.keySet());

            applySubtaskAndSpecialOptionValidity(validOptionIds);

            final Predicate<String> isVisible = optionId -> validOptionIds.contains(optionId)
                    || selectedOptions.containsKey(optionId);
            final OptionGroups groups = new OptionGroups(allOptions, isVisible);

            velocityParameters.put(SELECTED_OPTION_IDS, selectedOptions.keySet());
            velocityParameters.put(OPTION_CSS_CLASSES, cssInfo);
            velocityParameters.put(VALID_OPTION_IDS, validOptionIds);
            velocityParameters.put(SPECIAL_OPTIONS, groups.special);
            velocityParameters.put(STANDARD_OPTIONS, groups.standard);
            velocityParameters.put(SUBTASK_OPTIONS, groups.subtasks);
        }

        private Stream<Option> optionStream(Collection<String> optionIds) {
            return optionIds.stream()
                    .map(optionId -> {
                        final Option option = allOptions.get(optionId);
                        if (option == null) {
                            LOG.debug("Ignoring invalid option ID '{}'", optionId);
                        }
                        return option;
                    })
                    .filter(Objects::nonNull);
        }

        private List<Option> optionList(Collection<String> optionIds) {
            return optionStream(optionIds).collect(CollectorsUtil.toNewArrayListWithSizeOf(optionIds));
        }

        private Map<String, Option> optionMap(Collection<String> optionIds) {
            final Map<String, Option> map = Maps.newHashMapWithExpectedSize(optionIds.size());
            optionStream(optionIds).forEach(option -> map.put(option.getId(), option));
            return map;
        }

        private Option allStandardTypes() {
            return new TextOption(ALL_STANDARD_ISSUE_TYPES, i18n.getText(ALL_STANDARD_ISSUE_TYPES_NAME));
        }

        private Option allSubtaskTypes() {
            return new TextOption(ALL_SUB_TASK_ISSUE_TYPES, i18n.getText(ALL_SUB_TASK_ISSUE_TYPES_NAME));
        }
    }

    static class OptionGroups {
        final List<Option> special = new ArrayList<>(2);
        final List<Option> standard = new LinkedList<>();
        final List<Option> subtasks = new LinkedList<>();

        OptionGroups(Map<String, Option> allOptions, Predicate<String> isVisible) {
            final Predicate<Option> isVisibleAndNotSpecial = option -> {
                final String optionId = option.getId();
                return isVisible.test(optionId) && !isSpecial(optionId);
            };

            allOptions.values().stream()
                    .filter(isVisibleAndNotSpecial)
                    .forEach(option -> {
                        if (IssueConstantOption.isSubTask(option)) {
                            subtasks.add(option);
                        } else {
                            standard.add(option);
                        }
                    });


            special.add(allOptions.get(ALL_STANDARD_ISSUE_TYPES));
            if (isVisible.test(ALL_SUB_TASK_ISSUE_TYPES)) {
                special.add(allOptions.get(ALL_SUB_TASK_ISSUE_TYPES));
            }

            Collections.sort(standard, ISSUE_TYPE_OPTION_COMPARATOR);
            Collections.sort(subtasks, ISSUE_TYPE_OPTION_COMPARATOR);
        }

        private static boolean isSpecial(String optionId) {
            return ALL_STANDARD_ISSUE_TYPES.equals(optionId) || ALL_SUB_TASK_ISSUE_TYPES.equals(optionId);
        }
    }
}
