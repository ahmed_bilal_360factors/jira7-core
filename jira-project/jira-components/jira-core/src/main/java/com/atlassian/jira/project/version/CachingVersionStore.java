package com.atlassian.jira.project.version;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.entity.WithIdFunctions;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.util.NamedPredicates;
import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import com.google.common.primitives.Longs;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;

/**
 * A caching implementation of the VersionStore that relies on a delegate to do the DB operations.
 */
@EventComponent
public class CachingVersionStore implements VersionStore {
    private final VersionStore delegate;

    private final Cache<Long, Optional<Version>> versionById;
    private final CachedReference<List<Version>> allVersions;
    private final Cache<String, List<Version>> versionsByName;
    private final Cache<Long, List<Version>> versionsByProjectId;

    public CachingVersionStore(final VersionStore delegate, final CacheManager cacheManager) {
        this.delegate = delegate;
        versionById = cacheManager.getCache(getCacheReferenceName("versionById"), id -> Optional.fromNullable(delegate.getVersion(id)));
        allVersions = cacheManager.getCachedReference(getCacheReferenceName("allVersions"), () -> copyOf(delegate.getAllVersions()));
        versionsByName = cacheManager.getCache(getCacheReferenceName("versionsByName"), name -> copyOf(loadVersionsByName(name)));
        versionsByProjectId = cacheManager.getCache(getCacheReferenceName("versionsByProjectId"), projectId -> copyOf(delegate.getVersionsByProject(projectId)));
    }

    private String getCacheReferenceName(final String name) {
        return CachingVersionStore.class.getCanonicalName() + ".cache." + name;
    }

    @EventListener
    @SuppressWarnings("unused")
    public void onClearCache(final ClearCacheEvent event) {
        refreshCache();
    }

    @Override
    @Nonnull
    public Iterable<Version> getAllVersions() {
        return allVersions.get();
    }

    @Override
    public Version getVersion(final Long id) {
        if (id == null) {
            return null;
        }
        final Optional<Version> version = versionById.get(id);
        return version != null ? version.orNull() : null;
    }

    @Override
    @Nonnull
    public Version createVersion(@Nonnull final Version version) {
        // We can call through to the delegate to make the DB changes in multi-threaded mode.
        final Version newVersion = delegate.createVersion(version);
        // Now we force a cache refresh - this is synchronised with other writers, but readers can still get the old
        // cache up until the new cache is fully populated, and swapped into memory.
        refreshCache(newVersion.getId());
        return newVersion;
    }

    @Override
    public void storeVersion(@Nonnull final Version version) {
        delegate.storeVersion(version);
        refreshCache(version.getId());
    }

    @Override
    public void storeVersions(@Nonnull final Collection<Version> versions) {
        delegate.storeVersions(versions);
        refreshCache(Longs.toArray(copyOf(transform(versions, WithIdFunctions.getId()))));
    }

    @Override
    public void deleteVersion(@Nonnull final Version version) {
        final long versionId = version.getId();
        delegate.deleteVersion(version);
        refreshCache(versionId);
    }

    @Override
    public void deleteAllVersions(@Nonnull final Long projectId) {
        delegate.deleteAllVersions(projectId);
        refreshCache();
    }

    @Nonnull
    @Override
    public Iterable<Version> getVersionsByName(final String name) {
        List<Version> result = null;
        if (name != null) {
            result = versionsByName.get(name);
        }
        return result != null ? result : Collections.<Version>emptyList();
    }

    @Nonnull
    private Iterable<Version> loadVersionsByName(String name) {
        // get all versions from cache and filter it on case-insensitive name
        return Iterables.filter(allVersions.get(), NamedPredicates.equalsIgnoreCase(name));
    }

    @Nonnull
    @Override
    public Iterable<Version> getVersionsByProject(final Long projectId) {
        List<Version> result = null;
        if (projectId != null) {
            result = versionsByProjectId.get(projectId);
        }
        return result != null ? result : Collections.<Version>emptyList();
    }

    private void refreshCache(final long... versionIds) {
        if (versionIds.length > 0) {
            for (long versionId : versionIds) {
                versionById.remove(versionId);
            }
        } else {
            versionById.removeAll();
        }
        allVersions.reset();
        versionsByName.removeAll();
        versionsByProjectId.removeAll();
    }
}
