package com.atlassian.jira.scheduler;

import com.atlassian.annotations.Internal;
import com.atlassian.scheduler.caesium.migration.LazyMigratingParameterMapSerializer;

/**
 * A ParameterMapSerializer that restricts the classes that can be
 * deserialized. See JRA-46827.
 * @since v7.1.0
 */
@Internal
public class JiraParameterMapSerializer extends LazyMigratingParameterMapSerializer {

    public static final String JIRA_PARAMETER_MAP_SERIALIZER_DISABLE_RESTRICTIVE_CLASS_LOADER = "jira.parameter.map.serializer.disable.restrictive.class.loader";

}
