package com.atlassian.jira.issue.vote;

import com.atlassian.jira.association.UserAssociationStore;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.comparator.ApplicationUserBestNameComparator;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.index.IssueIndexingParams;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableSet;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.atlassian.collectors.CollectorsUtil.toNewArrayListWithSizeOf;
import static com.atlassian.jira.security.JiraAuthenticationContextImpl.getRequestCache;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.cache.CacheBuilder.newBuilder;
import static com.google.common.cache.CacheLoader.from;

public class DefaultVoteManager implements VoteManager {
    private static final Logger log = LoggerFactory.getLogger(DefaultVoteManager.class);
    private static final String ASSOCIATION_TYPE = "VoteIssue";
    private static final String CACHE_KEY = DefaultVoteManager.class.getName() + ".voters";

    private final ApplicationProperties applicationProperties;
    private final UserAssociationStore userAssociationStore;
    private final VoteHistoryStore voteHistoryStore;
    private final IssueIndexManager indexManager;
    private final IssueManager issueManager;
    private final IssueFactory issueFactory;
    private final UserManager userManager;


    public DefaultVoteManager(final ApplicationProperties applicationProperties, final UserAssociationStore userAssociationStore, final IssueIndexManager indexManager, VoteHistoryStore voteHistoryStore, IssueManager issueManager, IssueFactory issueFactory, UserManager userManager) {
        this.applicationProperties = applicationProperties;
        this.userAssociationStore = userAssociationStore;
        this.indexManager = indexManager;
        this.voteHistoryStore = voteHistoryStore;
        this.issueManager = issueManager;
        this.issueFactory = issueFactory;
        this.userManager = userManager;
    }

    @Override
    public boolean addVote(ApplicationUser user, Issue issue) {
        return updateVote(true, user, issue.getGenericValue());
    }

    public boolean addVote(final ApplicationUser user, final GenericValue issue) {
        return updateVote(true, user, issue);
    }

    @Override
    public boolean removeVote(ApplicationUser user, GenericValue issue) {
        return updateVote(false, user, issue);
    }

    @Override
    public boolean removeVote(ApplicationUser user, Issue issue) {
        return updateVote(false, user, issue.getGenericValue());
    }

    public Collection<String> getVoterUsernames(final Issue issue) {
        final Collection<String> voterKeys = getVoterUserkeys(issue);
        return voterKeys.stream().map(userManager::getUserByKeyEvenWhenUnknown).filter(user -> user != null).map(ApplicationUser::getUsername).collect(toNewArrayListWithSizeOf(voterKeys));
    }

    public Collection<String> getVoterUsernames(final GenericValue issue) {
        return getVoterUsernames(issueFactory.getIssue(issue));
    }

    @Override
    public Collection<String> getVoterUserkeys(Issue issue) {
        return getVotersCache().getUnchecked(issue.getId());
    }

    @Override
    public int getVoteCount(Issue issue) {
        return getVoterUsernames(issue).size();
    }

    public List<VoteHistoryEntry> getVoteHistory(Issue issue) {
        return voteHistoryStore.getHistory(issue.getId());
    }

    public List<ApplicationUser> getVoters(final Issue issue, final Locale usersLocale) {
        return getVotersFor(issue, usersLocale);
    }

    @Override
    public List<ApplicationUser> getVotersFor(Issue issue, Locale usersLocale) {
        final Collection<String> voterKeys = getVoterUserkeys(issue);
        return voterKeys.stream()
                .map(userManager::getUserByKeyEvenWhenUnknown)
                .filter(user -> user != null)
                .sorted(new ApplicationUserBestNameComparator(usersLocale)).collect(toNewArrayListWithSizeOf(voterKeys));
    }

    private boolean updateVote(final boolean isVoting, final ApplicationUser user, final GenericValue issue) {
        if (validateUpdate(user, issue)) {
            try {
                if (isVoting) {
                    if (!hasVoted(user, issue)) {
                        userAssociationStore.createAssociation(ASSOCIATION_TYPE, user, issue);
                        getVotersCache().invalidate(issue.getLong("id"));
                        adjustVoteCount(issue, 1);
                        return true;
                    }
                } else {
                    if (hasVoted(user, issue)) {
                        userAssociationStore.removeAssociation(ASSOCIATION_TYPE, user.getKey(), Entity.Name.ISSUE, issue.getLong("id"));
                        getVotersCache().invalidate(issue.getLong("id"));
                        adjustVoteCount(issue, -1);
                        return true;
                    }
                }
            } catch (final GenericEntityException e) {
                log.error("Error changing vote association", e);
                return false;
            }
        }
        return false;
    }

    /**
     * Adjusts the vote count for an issue.
     *
     * @param originalIssue the issue to change count for
     * @param adjustValue   the value to change it by
     * @throws GenericEntityException If there wasa persitence problem
     */

    private void adjustVoteCount(final GenericValue originalIssue, final int adjustValue) throws GenericEntityException {
        final long votes = recalculateVoters(originalIssue, adjustValue);
        final Long issueId = originalIssue.getLong("id");

        final GenericValue clonedIssue = (GenericValue) originalIssue.clone();
        clonedIssue.clear();
        clonedIssue.set("id", originalIssue.getLong("id"));
        clonedIssue.set("votes", votes);
        clonedIssue.store();

        // The issue reference is kept in the VoteOrWatchIssue action, need to update original issue too
        originalIssue.set("votes", votes);

        final Timestamp now = new Timestamp(new Date().getTime());
        voteHistoryStore.add(new VoteHistoryEntryImpl(originalIssue.getLong("id"), now, votes));

        try {
            // indexing doesn't handles incremental updates, re-indexing the updated object from db
            final MutableIssue updatedIssue = issueManager.getIssueObject(issueId);
            indexManager.reIndex(updatedIssue, IssueIndexingParams.INDEX_ISSUE_ONLY);
        } catch (final IndexException e) {
            log.error("Exception re-indexing issue " + e, e);
        }
    }

    private Long recalculateVoters(final GenericValue issue, final int adjustValue) {
        Long votes = issue.getLong("votes");

        if (votes == null) {
            votes = 0L;
        }
        votes = votes + adjustValue;

        if (votes < 0) {
            votes = 0L;
        }
        return votes;
    }

    /**
     * Validates that the params and the system are in a correct state to change a vote
     *
     * @param user  The user who is voting
     * @param issue the issue the user is voting for
     * @return whether or not to go ahead with the vote.
     */
    private boolean validateUpdate(final ApplicationUser user, final GenericValue issue) {
        if (issue == null) {
            log.error("You must specify an issue.");
            return false;
        }

        if (!isVotingEnabled()) {
            log.error("Voting is not enabled - the change vote on issue " + issue.getString("key") + " by user " + user.getUsername() + " was unsuccessful.");

            return false;
        }

        if (issue.getString("resolution") != null) {
            log.error("Cannot change vote on issue that has been resolved.");
            return false;
        }

        if (user == null) {
            log.error("You must specify a user.");
            return false;
        }
        return true;
    }

    /**
     * Check if voting has been enabled
     */
    public boolean isVotingEnabled() {
        return applicationProperties.getOption(APKeys.JIRA_OPTION_VOTING);
    }

    @Override
    public boolean hasVoted(ApplicationUser user, Issue issue) {
        if (user == null) {
            return false;
        }
        // For performance: if there are no votes for the issue then this dude didn't vote for it.
        return issue.getVotes() != 0 && getVotersCache().getUnchecked(issue.getId()).contains(user.getKey());
    }

    public boolean hasVoted(final ApplicationUser user, final GenericValue issue) {
        if (user == null) {
            return false;
        }
        // For performance: if there are no votes for the issue then this dude didn't vote for it.
        return issue.getLong("votes") != 0 && getVotersCache().getUnchecked(issue.getLong("id")).contains(user.getKey());
    }

    @Override
    public void removeVotesForUser(ApplicationUser user) {
        notNull("user", user);
        // Get all the issues
        List<GenericValue> issueGvs = userAssociationStore.getSinksFromUser(ASSOCIATION_TYPE, user, "Issue");

        for (GenericValue issueGv : issueGvs) {
            updateVote(false, user, issueGv);
        }
    }

    @Nonnull
    protected LoadingCache<Long, ImmutableSet<String>> getVotersCache() {
        final LoadingCache<Long, ImmutableSet<String>> cache = newBuilder().build(from(issueId -> ImmutableSet.copyOf(userAssociationStore.getUserkeysFromIssue(ASSOCIATION_TYPE, issueId))));
        if (ExecutingHttpRequest.get() != null) {
            return getRequestCache(CACHE_KEY, () -> cache);
        } else {
            return cache;
        }
    }
}
