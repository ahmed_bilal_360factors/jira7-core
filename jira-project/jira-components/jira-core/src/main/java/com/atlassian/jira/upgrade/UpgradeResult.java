package com.atlassian.jira.upgrade;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.Collections;

/**
 * Result of the upgrade process
 */
public class UpgradeResult {
    public static final UpgradeResult OK = new UpgradeResult(Collections.emptyList());

    private final Collection<String> errors;

    /**
     * Creates an UpgradeResult with no errors.
     *
     * Errors can be added using {@link #addError(String)}
     */
    public UpgradeResult() {
        errors = Lists.newArrayList();
    }

    /**
     * Creates status with only one error message and indexing params set to none.
     *
     * @param error the only error message
     */
    public UpgradeResult(final String error) {
        this(ImmutableList.of(error));
    }

    /**
     * Creates status with only one error message and indexing params set to none.
     *
     * @param errors a list of errors that occurred during the upgrade
     */
    public UpgradeResult(final Collection<String> errors) {
        this.errors = errors;
    }

    /**
     * Checks whether this upgrade was successful.
     *
     * @return true if upgrade tasks returned no errors
     */
    public boolean successful() {
        return CollectionUtils.isEmpty(errors);
    }

    /**
     * List of errors that occurred during the upgrade
     *
     * @return list of errors
     */
    public Collection<String> getErrors() {
        return errors;
    }

    /**
     * Add an error message to the result.
     *
     * @param error to add to the result
     */
    public void addError(final String error) {
        errors.add(error);
    }
}
