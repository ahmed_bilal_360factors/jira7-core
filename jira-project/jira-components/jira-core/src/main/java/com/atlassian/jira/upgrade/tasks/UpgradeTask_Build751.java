package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;

import javax.annotation.Nullable;
import java.util.EnumSet;

/**
 * Reindexes JIRA due to a bug in the way {@link com.atlassian.jira.issue.customfields.impl.MultiSelectCFType} fields
 * were being indexed.
 *
 * @since v5.1
 */
public class UpgradeTask_Build751 extends AbstractReindexUpgradeTask {

    @Override
    public int getBuildNumber() {
        return 751;
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception {
        getReindexRequestService().requestReindex(ReindexRequestType.IMMEDIATE, EnumSet.of(AffectedIndex.ISSUE), EnumSet.noneOf(SharedEntityType.class));
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        // Flagged as not downgradable because it was created before the Downgrade Task framework was built.
        return true;
    }

    @Override
    public String getShortDescription() {
        return super.getShortDescription() + " due to changes to way select values are indexed.";
    }

    @Nullable
    @Override
    public Integer dependsUpon() {
        return 708;
    }

}
