package com.atlassian.jira.web.action.issue;

import com.atlassian.jira.web.bean.PagerFilter;

/**
 * Some utility code shared between the searching actions.
 *
 * @since v5.2
 * @deprecated since 7.1 Storing information on the httpsession is obsoleted. Please move this functionality to the frontend or other type of storage. This method will be removed in 8.0.
 */
@Deprecated
public interface SearchActionHelper {
    /**
     * Store the current pager in the session. The pager handles paging through the issue list.
     *
     * @return the page currently in the session.
     */
    public PagerFilter getPagerFilter();

    /**
     * Store the current pager in the session. The pager handles paging through the issue list.
     *
     * @param tempMax temporary max results per page
     * @return the page currently in the session.
     */
    public PagerFilter getPagerFilter(Integer tempMax);

    /**
     * Restart the pager in the session.
     *
     * @return the new PagerFilter that was created
     */
    public PagerFilter resetPager();
}
