package com.atlassian.jira.util.johnson;

import com.atlassian.jira.web.ServletContextProvider;
import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.config.JohnsonConfig;

import javax.servlet.ServletContext;

public class DefaultJohnsonProvider implements JohnsonProvider {
    @Override
    public JohnsonEventContainer getContainer() {
        ServletContext servletContext = ServletContextProvider.getServletContext();
        if (servletContext == null) {
            return Johnson.getEventContainer();
        }

        return Johnson.getEventContainer(servletContext);
    }

    @Override
    public JohnsonConfig getConfig() {
        ServletContext servletContext = ServletContextProvider.getServletContext();
        if (servletContext == null) {
            return Johnson.getConfig();
        }

        return Johnson.getConfig(servletContext);
    }
}
