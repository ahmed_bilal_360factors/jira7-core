package com.atlassian.jira.servermetrics;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Manages per-thread collection of request key.
 * @see RequestKeyResolver
 */
public class MultiThreadedRequestKeyResolver {
    private final RequestKeyResolver requestKeyResolver;
    private final ThreadLocal<NestedRequestKeyResolver> perThreadResolver;

    public MultiThreadedRequestKeyResolver(RequestKeyResolver requestKeyResolver) {
        this.requestKeyResolver = requestKeyResolver;
        perThreadResolver = ThreadLocal.withInitial(
                () -> new NestedRequestKeyResolver(this.requestKeyResolver)
        );
    }

    public void requestStarted(HttpServletRequest httpServletRequest) {
        perThreadResolver.get().requestStarted(httpServletRequest);
    }

    public Optional<String> requestFinished(HttpServletRequest httpServletRequest) {
        return perThreadResolver.get().requestFinished(httpServletRequest);
    }
}
