package com.atlassian.jira.security.roles.actor;

import com.atlassian.annotations.Internal;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.model.querydsl.QProjectRoleActor;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleActor;
import com.atlassian.jira.security.roles.RoleActor;
import com.atlassian.jira.security.roles.RoleActorDoesNotExistException;
import com.atlassian.jira.security.roles.RoleActorFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.ImmutableSet;
import com.mysema.commons.lang.CloseableIterator;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.Projections;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

@Internal
public class GroupRoleActorFactory implements RoleActorFactory {
    private final GroupManager groupManager;
    private final DbConnectionManager dbConnectionManager;

    public GroupRoleActorFactory(final GroupManager groupManager, final DbConnectionManager dbConnectionManager) {
        this.groupManager = groupManager;
        this.dbConnectionManager = dbConnectionManager;
    }

    public ProjectRoleActor createRoleActor(Long id, Long projectRoleId, Long projectId, String type, String groupName)
            throws RoleActorDoesNotExistException {
        if (!ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE.equals(type)) {
            throw new IllegalArgumentException(this.getClass().getName() + " cannot create RoleActors of type: " + type);
        }

        return new GroupRoleActor(id, projectRoleId, projectId, groupName);
    }

    public Set<RoleActor> optimizeRoleActorSet(Set<RoleActor> roleActors) {
        // no optimise for groups
        return roleActors;
    }

    public class GroupRoleActor extends AbstractRoleActor {
        private volatile Boolean active;

        GroupRoleActor(Long id, Long projectRoleId, Long projectId, String groupName) {
            super(id, projectRoleId, projectId, groupName);
        }

        GroupRoleActor(Long id, Long projectRoleId, Long projectId, String groupName, final boolean active) {
            super(id, projectRoleId, projectId, groupName);
            this.active = active;
        }

        @Override
        public boolean isActive() {
            // Lazy load the activity check.
            Boolean active = this.active;
            if (active == null) {
                active = groupManager.groupExists(getParameter());
                this.active = active;
            }
            return active;
        }

        public String getType() {
            return GROUP_ROLE_ACTOR_TYPE;
        }

        public String getDescriptor() {
            return getParameter();
        }

        public Set<ApplicationUser> getUsers() {
            final Set<ApplicationUser> users = new HashSet<ApplicationUser>();

            for (ApplicationUser user : groupManager.getUsersInGroup(getParameter())) {
                users.add(user);
            }
            return users;
        }

        @Override
        public boolean contains(ApplicationUser user) {
            return groupManager.isUserInGroup(user, getParameter());
        }

        /**
         * Returns a Group object that represents a valid (existing) group or throws an IllegalArgumentException
         * if the group does not exist
         *
         * @return group
         * @throws IllegalArgumentException if group does not exist
         */
        public Group getGroup() throws IllegalArgumentException {
            return groupManager.getGroupEvenWhenUnknown(getParameter());
        }

    }

    @Override
    @Nonnull
    public Set<ProjectRoleActor> getAllRoleActorsForUser(@Nullable final ApplicationUser user) {
        if (user == null) {
            return ImmutableSet.of();
        }

        final List<String> groupNames = groupManager.getGroupNamesForUser(user).stream().map(IdentifierUtils::toLowerCase).collect(Collectors.toList());

        final ImmutableSet.Builder<ProjectRoleActor> resultsBuilder = ImmutableSet.builder();

        dbConnectionManager.execute(dbConnection ->
        {
            final QProjectRoleActor pra = new QProjectRoleActor("pra");

            //We are not going to filter group names here because we need to compare them using IdentifierUtils.toLowerCase
            try (CloseableIterator<Tuple> roleActorTuples = dbConnection.newSqlQuery().
                    select(Projections.tuple(pra.id, pra.pid, pra.projectroleid, pra.roletype, pra.roletypeparameter)).
                    from(pra).
                    where(pra.roletype.eq(ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE).and(pra.pid.isNotNull())).
                    iterate()) {
                addMatchingActors(roleActorTuples, pra, groupNames, resultsBuilder);
            }
        });

        return resultsBuilder.build();
    }

    private void addMatchingActors(final Iterator<Tuple> roleActorTuples, final QProjectRoleActor pra,
                                   final Collection<String> groupNames, final ImmutableSet.Builder<ProjectRoleActor> resultsBuilder) {
        while (roleActorTuples.hasNext()) {
            final Tuple tuple = roleActorTuples.next();
            final String groupName = toLowerCase(tuple.get(pra.roletypeparameter));

            if (groupNames.contains(groupName)) {
                final GroupRoleActor actor = new GroupRoleActor(tuple.get(pra.id), tuple.get(pra.projectroleid), tuple.get(pra.pid), groupName, true);
                resultsBuilder.add(actor);
            }
        }
    }
}
