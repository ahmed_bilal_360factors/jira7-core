package com.atlassian.jira.security.auth.trustedapps;

import com.atlassian.jira.user.util.UserManager;
import com.atlassian.security.auth.trustedapps.ApplicationCertificate;
import com.atlassian.security.auth.trustedapps.CurrentApplication;
import com.atlassian.security.auth.trustedapps.TrustedApplication;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsManager;
import com.atlassian.security.auth.trustedapps.UserResolver;
import com.atlassian.seraph.filter.TrustedApplicationsFilter;

import java.security.Principal;
import java.util.Optional;

import static com.atlassian.jira.component.ComponentAccessor.getComponentSafely;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Used to make the {@link TrustedApplicationsManager} available to the filter.
 *
 * @since v3.12
 */
// /CLOVER:OFF
public class TrustedApplicationFilter extends TrustedApplicationsFilter {
    public TrustedApplicationFilter() {
        super(new DelegateManager(), new JiraUserResolver());
    }

    /**
     * Resolves application users by name.
     */
    static class JiraUserResolver implements UserResolver {
        public Principal resolve(ApplicationCertificate certificate) {
            notNull("certificate", certificate);
            return getComponentSafely(UserManager.class)
                    .map(userManager -> userManager.getUserByName(certificate.getUserName()))
                    .orElse(null);
        }
    }

    /**
     * resolve dependency on the actual manager at runtime
     */
    static class DelegateManager implements TrustedApplicationsManager {
        public TrustedApplication getTrustedApplication(String id) {
            return getDelegate()
                    .map(delegate -> delegate.getTrustedApplication(id))
                    .orElse(null);
        }

        public CurrentApplication getCurrentApplication() {
            return getDelegate()
                    .map(TrustedApplicationsManager::getCurrentApplication)
                    .orElse(null);
        }

        // Optional because it may not be there if Pico is not initialized yet
        private static Optional<TrustedApplicationsManager> getDelegate() {
            return getComponentSafely(TrustedApplicationsManager.class);
        }
    }
}
