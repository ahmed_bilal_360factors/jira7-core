package com.atlassian.jira.plugin.attachment;

import com.atlassian.jira.plugin.AbstractJiraModuleDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.ModuleFactory;
import com.google.common.collect.ImmutableList;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @since v6.3
 */
public class AttachmentProcessorModuleDescriptor extends AbstractJiraModuleDescriptor<AttachmentProcessor> {
    private static final Logger log = LoggerFactory.getLogger(AttachmentProcessorModuleDescriptor.class);

    private ImmutableList<String> fileExtensions;

    public AttachmentProcessorModuleDescriptor(final JiraAuthenticationContext authenticationContext, final ModuleFactory moduleFactory) {
        super(authenticationContext, moduleFactory);
    }

    @Override
    public void init(final Plugin plugin, final Element element) throws PluginParseException {
        super.init(plugin, element);

        final List elements = element.elements();

        if (elements == null) {
            log.error("Invalid descriptor for plugin. No child elements found.");
            fileExtensions = ImmutableList.of();
            return;
        }

        final ImmutableList.Builder<String> extensionsBuilder = ImmutableList.builder();

        for (Object o : elements) {
            try {
                final Element e = (Element) o;

                if (e.getName().equals("extension")) {
                    extensionsBuilder.add(e.getStringValue());
                }
            } catch (Exception e) {
                log.warn("Error while parsing attachment-processor child element.");
            }
        }

        fileExtensions = extensionsBuilder.build();
    }

    @Override
    public void enabled() {
        super.enabled();
        assertModuleClassImplements(AttachmentProcessor.class);
    }

    public List<String> getFileExtensions() {
        return fileExtensions;
    }
}
