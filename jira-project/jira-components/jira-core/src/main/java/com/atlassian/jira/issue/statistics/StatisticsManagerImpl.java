package com.atlassian.jira.issue.statistics;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.bean.StatisticAccessorBean;
import com.atlassian.jira.web.bean.StatisticMapWrapper;
import com.atlassian.query.Query;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Maps;

/**
 * Utilities class that returns issue statistics for objects.
 */
public class StatisticsManagerImpl implements StatisticsManager {

    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final ProjectManager projectManager;
    private final FilterStatisticsValuesGenerator filterStatisticsGenerator;

    public StatisticsManagerImpl(JiraAuthenticationContext jiraAuthenticationContext, ProjectManager projectManager) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.projectManager = projectManager;
        filterStatisticsGenerator = new FilterStatisticsValuesGenerator();
    }

    @VisibleForTesting
    StatisticsManagerImpl(JiraAuthenticationContext jiraAuthenticationContext, ProjectManager projectManager, FilterStatisticsValuesGenerator filterStatisticsGenerator) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.projectManager = projectManager;
        this.filterStatisticsGenerator = filterStatisticsGenerator;
    }
    
    public StatisticMapWrapper<Object, Integer> getObjectsResultingFrom(Optional<Query> query, String statsObject) {
        Query q = query.orElse(JqlQueryBuilder.newBuilder().buildQuery());

        SearchRequest sr = new SearchRequest(q);
        final StatisticsMapper mapper = filterStatisticsGenerator.getStatsMapper(statsObject);
        
        if (mapper == null) {
            throw new IllegalArgumentException(statsObject + " CF does not implement CustomFieldStattable so cannot be parsed by the StatsMapper");
        }
        
        final StatisticAccessorBean statsBean = new StatisticAccessorBean(jiraAuthenticationContext.getLoggedInUser(), sr);

        try {
            return statsBean.getWrapper(mapper, StatisticAccessorBean.OrderBy.NATURAL, StatisticAccessorBean.Direction.ASC);
        } catch (SearchException e) {
            throw new RuntimeException(e);
        }
    }

    public <T> Map<Project, Map<T, Integer>> getProjectsWithItemsWithIssueCount(
            Optional<Query> query, Function<T, Long> objToProjectIdMapper, String objectType) {

        // for the query and T (e.g. component) get all the projects with their statistics.
        StatisticMapWrapper<T, Integer> items = (StatisticMapWrapper<T, Integer>) getObjectsResultingFrom(query, objectType);

        // for each T, get its project, then make a map of projects.
        final Map<Project, Map<T, Integer>> out = items.keySet().stream()
                .filter(obj -> obj != null)
                .map(objToProjectIdMapper)
                .distinct()
                .collect(Collectors.toMap(projectId -> projectManager.getProjectObj(projectId), projectId -> Maps.newHashMap()));

        // for each T, find its project, and populate the T and countOfT map for that project.
        for (Map.Entry<T, Integer> e : items.getStatistics().entrySet()) {
            T item = e.getKey();
            if (item == null) {
                continue;
            }

            Integer count = e.getValue();
            
            Long projectId = objToProjectIdMapper.apply(item);
            Project project = projectManager.getProjectObj(projectId);
            
            // Guard against any inconsistencies in the statistics data. e.g. it gives us items for
            //  projects not returned from items.keySet().
            if (out.containsKey(project)) {
                out.get(project).put(item, count);
            }
        }

        return out;
    }
}
