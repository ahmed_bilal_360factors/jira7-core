package com.atlassian.jira.cache.request;

import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.Supplier;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Map;

import static com.atlassian.jira.cache.request.RequestCacheController.getContext;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

@ParametersAreNonnullByDefault
class RequestCacheImpl<K, V> implements RequestCache<K, V> {
    private final CacheLoader<K, V> cacheLoader;
    private final String name;

    RequestCacheImpl(@Nonnull final String name, @Nonnull final CacheLoader<K, V> cacheLoader) {
        this.name = notNull("name", name);
        this.cacheLoader = notNull("cacheLoader", cacheLoader);
    }

    @Nonnull
    @Override
    public V get(final K key) {
        notNull("key", key);

        final RequestCacheController.CacheContext cacheContext = getContext();
        if (cacheContext == null) {
            // no cache in context - load it the slow way each time
            return getNotNull("cacheLoader", key, cacheLoader.load(key));
        }

        final Map<Object, Object> localMap = cacheContext.getLocalMap(this);
        final Object cachedValue = localMap.get(key);
        if (cachedValue != null) {
            //noinspection unchecked
            return (V) cachedValue;
        }

        final V loadedValue = getNotNull("cacheLoader", key, cacheLoader.load(key));
        localMap.put(key, loadedValue);
        return loadedValue;
    }


    @Nullable
    @Override
    public V getIfPresent(final K key) {
        notNull("key", key);

        final RequestCacheController.CacheContext cacheContext = getContext();
        if (cacheContext == null) {
            // no cache in context, so it definitely isn't there already
            return null;
        }

        final Map<Object, Object> localMap = cacheContext.getLocalMapIfExists(this);

        //noinspection unchecked
        return (localMap != null) ? (V) (localMap.get(key)) : null;
    }

    @Nonnull
    @Override
    public V get(final K key, final Supplier<V> supplier) {
        notNull("key", key);
        final RequestCacheController.CacheContext cacheContext = getContext();
        if (cacheContext == null) {
            // no cache in context - load it the slow way each time
            return getNotNull("supplier", key, supplier.get());
        }

        final Map<Object, Object> localMap = cacheContext.getLocalMap(this);
        final Object cachedValue = localMap.get(key);
        if (cachedValue != null) {
            //noinspection unchecked
            return (V) cachedValue;
        }

        final V loadedValue = getNotNull("supplier", key, supplier.get());
        localMap.put(key, loadedValue);
        return loadedValue;
    }

    @Override
    public void remove(final K key) {
        final RequestCacheController.CacheContext cacheContext = getContext();
        if (cacheContext == null) {
            // no cache in context - therefore remove is a no op.
            return;
        }

        final Map<Object, Object> localMap = cacheContext.getLocalMapIfExists(this);
        if (localMap != null) {
            localMap.remove(key);
        }
    }

    @Override
    public void removeAll() {
        final RequestCacheController.CacheContext cacheContext = getContext();
        if (cacheContext != null) {
            cacheContext.clearLocalMap(this);
        }
    }

    @Override
    public String toString() {
        return super.toString() + "[name=" + name + ']';
    }

    private V getNotNull(String description, K key, @Nullable V loadedValue) {
        if (loadedValue == null) {
            throw new IllegalArgumentException(description + " returned null for key '" + key + '\'');
        }
        return loadedValue;
    }
}
