package com.atlassian.jira.security;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.permission.ProjectPermissionCategory;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.security.plugin.ProjectPermissionOverrideModuleDescriptor;
import com.atlassian.jira.security.plugin.ProjectPermissionTypesManager;
import com.atlassian.jira.security.type.GroupDropdown;
import com.atlassian.jira.security.type.SingleUser;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.ozymandias.SafePluginPointAccess;
import com.google.common.base.MoreObjects;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static com.atlassian.jira.permission.LegacyProjectPermissionKeyMapping.getKey;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.security.plugin.ProjectPermissionOverride.Decision;
import static com.atlassian.jira.security.plugin.ProjectPermissionOverride.Decision.DENY;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static java.util.Collections.emptyList;

/**
 * An abstract PermissionManager that implements a lot of the common functionality to all PermissionManagers.
 */
public class DefaultPermissionManager implements PermissionManager {
    private static final Logger log = LoggerFactory.getLogger(DefaultPermissionManager.class);

    private final ProjectPermissionTypesManager projectPermissionTypesManager;
    private final ProjectPermissionOverrideDescriptorCache projectPermissionOverrideDescriptorCache;

    public DefaultPermissionManager(final ProjectPermissionTypesManager projectPermissionTypesManager,
                                    final ProjectPermissionOverrideDescriptorCache projectPermissionOverrideDescriptorCache) {
        this.projectPermissionTypesManager = projectPermissionTypesManager;
        this.projectPermissionOverrideDescriptorCache = projectPermissionOverrideDescriptorCache;
    }

    @Override
    public Collection<ProjectPermission> getAllProjectPermissions() {
        return projectPermissionTypesManager.all();
    }

    @Override
    public Collection<ProjectPermission> getProjectPermissions(@Nonnull ProjectPermissionCategory category) {
        return projectPermissionTypesManager.withCategory(category);
    }

    @Override
    public Option<ProjectPermission> getProjectPermission(@Nonnull ProjectPermissionKey permissionKey) {
        return projectPermissionTypesManager.withKey(permissionKey);
    }

    public boolean hasPermission(final int permissionsId, final ApplicationUser user) {
        if (!isGlobalPermission(permissionsId)) {
            throw new IllegalArgumentException("Expected global permission, got " + permissionsId);
        }

        if (user == null) {
            return ComponentAccessor.getGlobalPermissionManager().hasPermission(permissionsId);
        } else {
            return user.isActive() && ComponentAccessor.getGlobalPermissionManager().hasPermission(permissionsId, user);
        }
    }

    public boolean hasPermission(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Issue issue, @Nullable ApplicationUser user) {
        return withPermissionOverriding(doIssuePermissionCheck(permissionKey, issue, user), permissionKey, issue.getProjectObject(), user);
    }

    @Override
    public boolean hasPermission(@Nonnull final ProjectPermissionKey permissionKey, @Nonnull final Issue issue, @Nullable final ApplicationUser user, @Nullable final ActionDescriptor actionDescriptor) {
        // This is a workflow-specific method and is taken care of in WorkflowBasedPermissionManager.
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasPermission(@Nonnull final ProjectPermissionKey permissionKey, @Nonnull final Issue issue, @Nullable final ApplicationUser user, @Nonnull final Status status) {
        // This is a workflow-specific method and is taken care of in WorkflowBasedPermissionManager.
        throw new UnsupportedOperationException();
    }

    public boolean hasPermission(int permissionsId, Issue issue, ApplicationUser user) {
        ProjectPermissionKey permissionKey = getNonGlobalKey(permissionsId);
        return hasPermission(permissionKey, issue, user);
    }

    public boolean hasPermission(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Project project, @Nullable ApplicationUser user) {
        return withPermissionOverriding(doProjectPermissionCheck(permissionKey, project, user, false), permissionKey, project, user);
    }

    public boolean hasPermission(int permissionsId, Project project, ApplicationUser user) {
        ProjectPermissionKey permissionKey = getNonGlobalKey(permissionsId);
        return hasPermission(permissionKey, project, user);
    }

    @Override
    @Nonnull
    public ProjectWidePermission hasProjectWidePermission(@Nonnull final ProjectPermissionKey permissionKey, @Nonnull final Project project, @Nullable final ApplicationUser user) {
        final ProjectWidePermission corePermissionCheckResult = doProjectWidePermissionCheck(permissionKey, project, user, false);
        return withPermissionOverriding(corePermissionCheckResult, permissionKey, project, user);
    }

    public boolean hasPermission(final int permissionsId, final Project project, final ApplicationUser user, final boolean issueCreation) {
        ProjectPermissionKey permissionKey = getNonGlobalKey(permissionsId);
        return hasPermission(permissionKey, project, user, issueCreation);
    }

    public boolean hasPermission(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Project project, @Nullable ApplicationUser user, boolean issueCreation) {
        return withPermissionOverriding(doProjectPermissionCheck(permissionKey, project, user, issueCreation), permissionKey, project, user);
    }

    private boolean doIssuePermissionCheck(ProjectPermissionKey permissionKey, final Issue issue, final ApplicationUser user) {
        // JRA-14788: if generic value of issue object is null, need to defer permission check to project object.
        if (issue.getId() != null) {
            return doIssuePermissionCheck(permissionKey, issue, user, false);
        } else {
            return doProjectPermissionCheck(permissionKey, issue.getProjectObject(), user, true);
        }
    }

    private boolean doIssuePermissionCheck(ProjectPermissionKey permissionKey, final Issue issue, final ApplicationUser user, final boolean issueCreation) {
        // Check that the user can actually see the project this issue is in
        if (!doProjectPermissionCheck(permissionKey, issue.getProjectObject(), user, false)) {
            return false;
        }

        // Check the project permissions that apply to this issue
        if (!doPermissionSchemeCheck(permissionKey, issue, user, issueCreation)) {
            return false;
        }

        // When checking Issue Visibility (BROWSE_PROJECT permission), also check the Security Level
        // JRA-40124 Don't check Security Level for other permissions - this is unnecessary and can cause false negatives
        if (ProjectPermissions.BROWSE_PROJECTS.equals(permissionKey)) {
            return ComponentAccessor.getComponent(IssueSecuritySchemeManager.class).hasSecurityLevelAccess(issue, user);
        }
        return true;
    }

    private boolean doPermissionSchemeCheck(final ProjectPermissionKey permissionKey, final Issue issue, final ApplicationUser user, final boolean issueCreation) {
        if (!projectPermissionTypesManager.exists(permissionKey)) {
            return false;
        }

        // Check scheme manager for the project to see if this project has the permission
        if (user == null) {
            return ComponentAccessor.getPermissionSchemeManager().hasSchemePermission(permissionKey, issue);
        } else {
            return user.isActive() && ComponentAccessor.getPermissionSchemeManager().hasSchemePermission(permissionKey, issue, user, issueCreation);
        }
    }

    private boolean doProjectPermissionCheck(@Nonnull ProjectPermissionKey permissionKey, final Project project, @Nullable final ApplicationUser user, final boolean issueCreation) {
        if (project == null || project.getId() == null) {
            throw new IllegalArgumentException("The Project argument and its backing generic value must not be null");
        }

        if (!projectPermissionTypesManager.exists(permissionKey)) {
            return false;
        }

        // Check scheme manager for the project to see if this project has the permission
        if (user == null) {
            return ComponentAccessor.getPermissionSchemeManager().hasSchemePermission(permissionKey, project);
        } else {
            return user.isActive() && ComponentAccessor.getPermissionSchemeManager().hasSchemePermission(permissionKey, project, user, issueCreation);
        }
    }

    private ProjectWidePermission doProjectWidePermissionCheck(@Nonnull ProjectPermissionKey permissionKey,
                                                               final Project project, @Nullable final ApplicationUser user, final boolean issueCreation) {
        if (project == null || project.getId() == null) {
            throw new IllegalArgumentException("The Project argument and its backing generic value must not be null");
        }

        if (!projectPermissionTypesManager.exists(permissionKey)) {
            return ProjectWidePermission.NO_ISSUES;
        }

        // Check scheme manager for the project to see it this project has the permission
        if (user == null) {
            // There is no "maybe" for an anonymous user, as they never get a per issue permission
            return ComponentAccessor.getPermissionSchemeManager().hasSchemePermission(permissionKey, project) ?
                    ProjectWidePermission.ALL_ISSUES : ProjectWidePermission.NO_ISSUES;
        } else {
            if (user.isActive()) {
                return ComponentAccessor.getPermissionSchemeManager().hasProjectWidePermission(permissionKey, project, user, issueCreation);
            } else {
                // inactive user == no permission
                return ProjectWidePermission.NO_ISSUES;
            }
        }
    }

    /**
     * Remove all permissions that have used this group
     *
     * @param group The name of the group that needs to be removed, must NOT be null and must be a real group
     */
    public void removeGroupPermissions(final String group) throws RemoveException {
        notNull("group", group);
        notNull(ComponentAccessor.getGroupManager().getGroup(group));

        ComponentAccessor.getGlobalPermissionManager().removePermissions(group);
        ComponentAccessor.getPermissionSchemeManager().removeEntities(GroupDropdown.DESC, group);

        //If there is issue level security check that otherwise the must be able to see the issue
        ComponentAccessor.getComponent(IssueSecuritySchemeManager.class).removeEntities(GroupDropdown.DESC, group);
    }

    public void removeUserPermissions(final ApplicationUser user) throws RemoveException {
        notNull("user", user);

        ComponentAccessor.getPermissionSchemeManager().removeEntities(SingleUser.DESC, user.getKey());

        //If there is issue level security check that otherwise the must be able to see the issue
        ComponentAccessor.getComponent(IssueSecuritySchemeManager.class).removeEntities(SingleUser.DESC, user.getKey());
    }

    public boolean hasProjects(final int permissionId, final ApplicationUser user) {
        ProjectPermissionKey permissionKey = getNonGlobalKey(permissionId);
        return hasProjects(permissionKey, user);
    }

    public boolean hasProjects(@Nonnull final ProjectPermissionKey permissionKey, final ApplicationUser user) {
        return projectPermissionTypesManager.exists(permissionKey) &&
                Iterables.any(ComponentAccessor.getProjectManager().getProjectObjects(), project -> hasPermission(permissionKey, project, user));
    }

    @Override
    public Collection<Project> getProjects(final int permissionId, final ApplicationUser user) {
        ProjectPermissionKey permissionKey = getNonGlobalKey(permissionId);
        return getProjectObjects(permissionKey, user);
    }

    @Override
    public Collection<Project> getProjects(@Nonnull ProjectPermissionKey permissionKey, ApplicationUser user) {
        return getProjectObjects(permissionKey, user);
    }

    protected Collection<Project> getProjectObjects(ProjectPermissionKey permissionKey, final ApplicationUser user) {
        return getProjectObjectsWithPermission(ComponentAccessor.getProjectManager().getProjectObjects(), permissionKey, user);
    }

    public Collection<Project> getProjects(int permissionId, ApplicationUser user, ProjectCategory projectCategory) {
        ProjectPermissionKey permissionKey = getNonGlobalKey(permissionId);
        return getProjects(permissionKey, user, projectCategory);
    }

    public Collection<Project> getProjects(@Nonnull ProjectPermissionKey permissionKey, @Nullable ApplicationUser user, @Nullable ProjectCategory projectCategory) {
        final Collection<Project> projects;
        if (projectCategory == null) {
            projects = ComponentAccessor.getProjectManager().getProjectObjectsWithNoCategory();
        } else {
            projects = ComponentAccessor.getProjectManager().getProjectsFromProjectCategory(projectCategory);
        }

        return getProjectObjectsWithPermission(projects, permissionKey, user);
    }

    @Override
    public void flushCache() {
        // no-op
    }

    private Collection<Project> getProjectObjectsWithPermission(final Collection<Project> projects, final ProjectPermissionKey permissionKey, final ApplicationUser user) {
        if (!projectPermissionTypesManager.exists(permissionKey)) {
            return emptyList();
        }

        return Lists.newArrayList(Iterables.filter(projects, project ->
        {
            return hasPermission(permissionKey, project, user);
        }));
    }

    private ProjectPermissionKey getNonGlobalKey(int permissionId) {
        if (isGlobalPermission(permissionId)) {
            throw new IllegalArgumentException("PermissionType passed to this function must NOT be a global permission, " + permissionId + " is global");
        }

        return getKey(permissionId);
    }

    protected boolean isGlobalPermission(final int permissionId) {
        return Permissions.isGlobalPermission(permissionId);
    }

    public Collection<Group> getAllGroups(int permissionId, Project project) {
        // get a set of the groups we're talking about
        final Set<Group> groups = new HashSet<Group>();

        groups.addAll(ComponentAccessor.getPermissionSchemeManager().getGroups(new ProjectPermissionKey(permissionId), project));
        groups.addAll(ComponentAccessor.getGlobalPermissionManager().getGroupsWithPermission(permissionId));

        return groups;
    }

    private ProjectWidePermission withPermissionOverriding(final ProjectWidePermission corePermissionCheckResult, final ProjectPermissionKey permissionKey,
                                                           final Project project, final ApplicationUser applicationUser) {
        // We currently don't allow overriding Browse Project because it would mean changing JQL searching.
        // Permission override was added for Service Desk Agent Based Pricing and does not require overriding Browse Project
        if (corePermissionCheckResult == ProjectWidePermission.NO_ISSUES || BROWSE_PROJECTS.equals(permissionKey)) {
            return corePermissionCheckResult;
        }

        final boolean pluginPermissionCheckDeclined = Iterables.any(projectPermissionOverrideDescriptorCache.getProjectPermissionOverrideDescriptors(),
                new Predicate<ProjectPermissionOverrideModuleDescriptor>() {
                    @Override
                    public boolean apply(final ProjectPermissionOverrideModuleDescriptor permissionOverrideModuleDescriptor) {
                        return SafePluginPointAccess.safe(new Predicate<ProjectPermissionOverrideModuleDescriptor>() {
                            @Override
                            public boolean apply(final ProjectPermissionOverrideModuleDescriptor permissionOverrideModuleDescriptor) {
                                final Decision decision = permissionOverrideModuleDescriptor.getModule().hasPermission(permissionKey, project, applicationUser);
                                if (log.isDebugEnabled() && decision == DENY) {
                                    log.debug("Permission check result to project " + project.getKey() + "was overriden by " + permissionOverrideModuleDescriptor.getCompleteKey());
                                }
                                return decision == DENY;
                            }
                        }).apply(permissionOverrideModuleDescriptor);
                    }
                });

        if (pluginPermissionCheckDeclined) {
            // Permission was vetoed
            return ProjectWidePermission.NO_ISSUES;
        } else {
            return corePermissionCheckResult;
        }
    }

    private boolean withPermissionOverriding(final boolean corePermissionCheckResult, final ProjectPermissionKey permissionKey,
                                             final Project project, final ApplicationUser applicationUser) {
        // We currently don't allow overriding Browse Project because it would mean changing JQL searching.
        // Permission override was added for Service Desk Agent Based Pricing and does not require overriding Browse Project
        if (!corePermissionCheckResult || BROWSE_PROJECTS.equals(permissionKey)) {
            return corePermissionCheckResult;
        }

        final Boolean pluginPermissionCheckDeclined = MoreObjects.firstNonNull(Iterables.any(projectPermissionOverrideDescriptorCache.getProjectPermissionOverrideDescriptors(),
                permissionOverrideModuleDescriptor -> SafePluginPointAccess.safe(new Predicate<ProjectPermissionOverrideModuleDescriptor>() {
                    @Override
                    public boolean apply(final ProjectPermissionOverrideModuleDescriptor permissionOverrideModuleDescriptor) {
                        final Decision decision = permissionOverrideModuleDescriptor.getModule().hasPermission(permissionKey, project, applicationUser);
                        if (log.isDebugEnabled() && decision == DENY) {
                            log.debug("Permission check result to project " + project.getKey() + "was overriden by " + permissionOverrideModuleDescriptor.getCompleteKey());
                        }
                        return decision == DENY;
                    }
                }).apply(permissionOverrideModuleDescriptor)), false);

        // if any plugin declined we return opposite
        return !pluginPermissionCheckDeclined;
    }
}
