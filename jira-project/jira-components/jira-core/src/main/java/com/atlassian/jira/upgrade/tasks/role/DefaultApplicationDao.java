package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;

import java.util.Set;

/**
 * Simple DAO for default applications.
 *
 * @since 7.0
 */
public interface DefaultApplicationDao {

    /**
     * Lists all the applications that are marked as default.
     *
     * @return set of application keys
     */
    Set<ApplicationKey> get();

    /**
     * Updates the default applications. The existent applications, marked as default are not updated.
     *
     * @param applicationKeys application keys to mark as default
     */
    void setApplicationsAsDefault(Set<ApplicationKey> applicationKeys);
}