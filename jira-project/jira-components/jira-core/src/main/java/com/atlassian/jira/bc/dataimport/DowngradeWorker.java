package com.atlassian.jira.bc.dataimport;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.model.querydsl.QUpgradeHistory;
import com.atlassian.jira.model.querydsl.QUpgradeVersionHistory;
import com.atlassian.jira.model.querydsl.UpgradeHistoryDTO;
import com.atlassian.jira.model.querydsl.UpgradeVersionHistoryDTO;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.upgrade.DowngradeException;
import com.atlassian.jira.upgrade.DowngradeTask;
import com.atlassian.jira.upgrade.MissingDowngradeTaskException;
import com.atlassian.jira.upgrade.ReindexRequirement;
import com.atlassian.jira.upgrade.UpgradeTask;
import com.atlassian.jira.upgrade.UpgradeVersionHistoryManager;
import com.atlassian.jira.upgrade.util.BuildNumberDao;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.DowngradeUtilsImpl;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * This class will do the actual work of finding and running required downgrade tasks.
 * <p>
 * Downgrade tasks are defined in downgrades.xml and each Upgrade Task must declare whether its needs a downgrade,
 * or is a no-op.
 * </p>
 *
 * @see UpgradeTask#isDowngradeTaskRequired()
 * @since v6.4.6
 */
public class DowngradeWorker {

    /**
     * Downgrade mode.
     * <p>
     * IMPORT means a downgrade from an import.
     * This will not schedule a reindex because the import will force a full reindex anyway.
     * </p>
     * <p>
     * START_UP means an in place DB downgrade detected at JIRA start up.
     * If any downgrade tasks require a reindex, then we store this in the DB and later schedule a job to kick off
     * a background reindex.
     * </p>
     */
    public enum Mode {
        IMPORT, START_UP
    }

    private static final Logger log = LoggerFactory.getLogger(DowngradeWorker.class);

    private final BuildUtilsInfo buildUtilsInfo;
    private final DbConnectionManager dbConnectionManager;
    private final OfBizDelegator ofBizDelegator;
    private final ApplicationProperties applicationProperties;
    private final UpgradeVersionHistoryManager upgradeVersionHistoryManager;

    public DowngradeWorker(BuildUtilsInfo buildUtilsInfo, DbConnectionManager dbConnectionManager,
                           final OfBizDelegator ofBizDelegator, ApplicationProperties applicationProperties,
                           final UpgradeVersionHistoryManager upgradeVersionHistoryManager) {
        this.buildUtilsInfo = buildUtilsInfo;
        this.dbConnectionManager = dbConnectionManager;
        this.ofBizDelegator = ofBizDelegator;
        this.applicationProperties = applicationProperties;
        this.upgradeVersionHistoryManager = upgradeVersionHistoryManager;
    }

    /**
     * Called to perform special downgrade tasks.
     * <p>
     * This method is only invoked when we have identified that you are importing data from a newer version of
     * JIRA, presumably because an OnDemand customer has exported JIRA data to import into their on premise
     * instance.
     * </p>
     */
    public void downgrade(Mode downgradeMode) throws DowngradeException {
        // Find all upgrade tasks that need to be explicitly reversed with a downgrade task
        List<Integer> downgradeTaskNumbers = findDowngradeTasksToRun();
        // Read downgrades.xml file to know which downgrade tasks actually exist in this version of JIRA
        final Map<Integer, DowngradeTask> downgradeTaskMap = DowngradeUtil.loadDowngradeTasks();
        // quickly verify that we have all the ones we need before we try to run them
        DowngradeUtil.verifyTasksExist(downgradeTaskNumbers, downgradeTaskMap);
        // OK - lets run the downgrade tasks
        log.info("______ Performing Downgrade ______");
        for (Integer downgradeTaskNumber : downgradeTaskNumbers) {
            final DowngradeTask downgradeTask = downgradeTaskMap.get(downgradeTaskNumber);
            runDowngradeTask(downgradeTask);
            persistReindexRequirement(downgradeTask, downgradeMode);
            // Downgrade succeeded - remove the old upgrade history and drop the build number
            undoUpgradeHistory(downgradeTaskNumber);
        }
        log.info("______ Downgrade Complete ______");
    }

    public void writeNewBuildNumbers() {
        final DowngradeUtilsImpl downgradeUtilsInfo = new DowngradeUtilsImpl();
        BuildNumberDao buildNumberDao = new BuildNumberDao(downgradeUtilsInfo, applicationProperties);

        buildNumberDao.setDatabaseBuildNumber(buildUtilsInfo.getCurrentBuildNumber());
        buildNumberDao.setJiraVersion(buildUtilsInfo.getVersion());
        buildNumberDao.setMinimumDowngradeVersion();

        upgradeVersionHistoryManager.addUpgradeVersionHistory(Integer.parseInt(buildUtilsInfo.getCurrentBuildNumber()),
                buildUtilsInfo.getVersion());
    }

    public boolean canDowngrade() throws DowngradeException {
        // Find all upgrade tasks that need to be explicitly reversed with a downgrade task
        List<Integer> downgradeTaskNumbers = findDowngradeTasksToRun();
        // Read downgrades.xml file to know which downgrade tasks actually exist in this version of JIRA
        final Map<Integer, DowngradeTask> downgradeTaskMap = DowngradeUtil.loadDowngradeTasks();
        // quickly verify that we have all the ones we need before we try to run them
        try {
            DowngradeUtil.verifyTasksExist(downgradeTaskNumbers, downgradeTaskMap);
            return true;
        } catch (MissingDowngradeTaskException e) {
            return false;
        }
    }

    @VisibleForTesting
    List<Integer> findDowngradeTasksToRun() throws DowngradeException {
        // Get all upgrade tasks that have run from the UpgradeHistory table in the DB
        final List<UpgradeHistoryDTO> upgradeHistoryItems = dbConnectionManager.executeQuery(
                dbConnection -> dbConnection.newSqlQuery()
                        .select(QUpgradeHistory.UPGRADE_HISTORY)
                        .from(QUpgradeHistory.UPGRADE_HISTORY)
                        .fetch()
        );
        return DowngradeUtil.findDowngradeTasksToRun(upgradeHistoryItems, buildUtilsInfo.getApplicationBuildNumber());
    }

    private void runDowngradeTask(final DowngradeTask downgradeTask) throws DowngradeException {
        log.info("Performing Downgrade Task " + downgradeTask.getBuildNumber() + ": " + downgradeTask.getShortDescription());
        try {
            downgradeTask.setDbConnectionManager(dbConnectionManager);
            downgradeTask.setOfBizDelegator(ofBizDelegator);
            downgradeTask.downgrade();
        } catch (DowngradeException ex) {
            log.error("Downgrade Task " + downgradeTask.getBuildNumber() + " failed.", ex);
            throw ex;
        }

        log.info("Downgrade Task " + downgradeTask.getBuildNumber() + " succeeded.");
    }


    private void persistReindexRequirement(final DowngradeTask downgradeTask, final Mode downgradeMode) {
        switch (downgradeMode) {
            case IMPORT:
                // A full reindex will happen after the import anyway
                return;
            case START_UP:
                // Persist any reindex requirement so we pick it up once JIRA starts
                persistReindexRequirement(downgradeTask.reindexRequired());
                break;
            default:
                // WTF?
                throw new IllegalStateException();
        }
    }

    private void persistReindexRequirement(final ReindexRequirement reindexRequirement) {
        switch (reindexRequirement) {
            case NONE:
                // Nothing to do
                return;
            case FOREGROUND:
                // This trumps any other value
                DowngradeUtil.setReindexRequirement(applicationProperties, reindexRequirement);
                return;
            case BACKGROUND:
                // Check the current value
                final ReindexRequirement currentReindexRequirement = DowngradeUtil.getReindexRequirement(applicationProperties);
                if (currentReindexRequirement == ReindexRequirement.NONE) {
                    DowngradeUtil.setReindexRequirement(applicationProperties, reindexRequirement);
                }
                return;
            default:
                throw new IllegalStateException();
        }
    }

    @VisibleForTesting
    void undoUpgradeHistory(final Integer downgradeTaskNumber) {
        // First we want to delete upgrade history for the upgrade we just reversed, and any other no-op upgrades
        // that happened after this one.
        // Not the most efficient way to do this, but safe and simple and only happens for downgrade tasks, so meh.
        dbConnectionManager.execute(dbConnection -> {
            // SELECT * FROM UpgradeHistory
            final List<UpgradeHistoryDTO> upgradeHistoryItems = dbConnection.newSqlQuery()
                    .select(QUpgradeHistory.UPGRADE_HISTORY)
                    .from(QUpgradeHistory.UPGRADE_HISTORY)
                    .fetch();

            for (UpgradeHistoryDTO upgradeHistoryItem : upgradeHistoryItems) {
                // we store the upgrade build number in the DB as a VARCHAR (facepalm)
                final String buildNumberString = upgradeHistoryItem.getTargetbuild();
                final Integer buildNumber = NumberUtils.toInt(buildNumberString, -1);
                if (buildNumber >= downgradeTaskNumber) {
                    dbConnection.delete(QUpgradeHistory.UPGRADE_HISTORY)
                            .where(QUpgradeHistory.UPGRADE_HISTORY.targetbuild.eq(buildNumberString))
                            .execute();
                }
            }
        });

        // We also have to scrub UpgradeVersionHistory - targetbuild is the PK for this table (facepalm)
        dbConnectionManager.execute(dbConnection -> {
            // SELECT * FROM UpgradeVersionHistory
            final List<UpgradeVersionHistoryDTO> upgradeHistoryItems = dbConnection.newSqlQuery()
                    .select(QUpgradeVersionHistory.UPGRADE_VERSION_HISTORY)
                    .from(QUpgradeVersionHistory.UPGRADE_VERSION_HISTORY)
                    .fetch();

            for (UpgradeVersionHistoryDTO upgradeHistoryItem : upgradeHistoryItems) {
                // we store the upgrade build number in the DB as a VARCHAR (facepalm)
                final String buildNumberString = upgradeHistoryItem.getTargetbuild();
                final Integer buildNumber = NumberUtils.toInt(buildNumberString, -1);
                if (buildNumber >= downgradeTaskNumber) {
                    dbConnection.delete(QUpgradeVersionHistory.UPGRADE_VERSION_HISTORY)
                            .where(QUpgradeVersionHistory.UPGRADE_VERSION_HISTORY.targetbuild.eq(buildNumberString))
                            .execute();
                }
            }
        });

        // Finally change the DB build number
        final int newDbBuildNumber = downgradeTaskNumber - 1;
        log.info("Setting current build number to " + newDbBuildNumber);
        applicationProperties.setString(APKeys.JIRA_PATCHED_VERSION, String.valueOf(newDbBuildNumber));
    }
}
