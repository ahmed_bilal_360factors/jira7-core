package com.atlassian.jira.web.action.admin.importer.project;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.jira.event.web.action.JiraWebActionSupportEvent;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import java.util.Map;

/**
 * Project Import Web analytics event.
 */
public class ProjectImportWebAnalyticsEvent extends JiraWebActionSupportEvent {
    private final String actionName;

    private final Map<String, Object> projectImportAttributes;

    public ProjectImportWebAnalyticsEvent(JiraWebActionSupport jiraWebActionSupport, Map<String, Object> projectImportAttributes) {
        super(jiraWebActionSupport);
        this.actionName = jiraWebActionSupport.getClass().getSimpleName();
        this.projectImportAttributes = projectImportAttributes;
    }

    @EventName
    public String getEventName() {
        return "jira.project.import.webaction";
    }

    public String getActionName() {
        return actionName;
    }

    public Map<String, Object> getAttributes() {
        return projectImportAttributes;
    }

    @Override
    public boolean isEmpty() {
        return super.isEmpty() && projectImportAttributes.isEmpty();
    }
}
