package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.project.type.BrowseProjectTypeManager;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Checks if there are multiple project types to choose from.
 * User can change the project type only if there are multiple options.
 */
public class CanChangeProjectTypeCondition extends AbstractWebCondition {

    private final BrowseProjectTypeManager browseProjectTypeManager;

    public CanChangeProjectTypeCondition(final BrowseProjectTypeManager browseProjectTypeManager) {
        this.browseProjectTypeManager = browseProjectTypeManager;
    }

    @Override
    public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper) {
        return browseProjectTypeManager.isProjectTypeChangeAllowed(jiraHelper.getProject());
    }
}
