package com.atlassian.jira.ofbiz;

import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.util.log.RateLimitingLogger;
import org.ofbiz.core.entity.jdbc.interceptors.connection.ConnectionPoolState;
import org.ofbiz.core.entity.jdbc.interceptors.connection.SQLConnectionInterceptor;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.lang.Math.max;

/**
 * An interceptor for SQL connection use that will keep track of how many connections each thread uses
 * and report warnings for various things that could cause trouble, like a single thread requesting multiple
 * connections from the pool.
 * <p>
 * We log a warning or error message when:
 * </p>
 * <ul>
 * <li>A thread requests LIMIT connections at once for the first time since it was last reset, where LIMIT is
 * normally {@code 3}, but becomes {@code 2} if in {@link #STRICT_MODE}.</li>
 * <li>A thread requests multiple connections while the connection pool is in the {@link #DANGER_ZONE}.</li>
 * <li>One of the preceding events has occurred and the thread is releasing one of the connections that had been
 * obtained before one of the above log messages had been triggered.  The idea is that these did not get
 * logged on the way in, but it may be important to see where the outer connections came from, and hopefully
 * the code that releases the connection is located near whatever allocated it.  This is a workaround for the
 * fact that creating a stack trace for everything just-in-case is too expensive.</li>
 * </ul>
 *
 * @since v7.0.0
 */
public class ConnectionPoolHealthSqlInterceptor implements SQLConnectionInterceptor {
    /**
     * The rate-limiting logger will only log this many stack traces before squelching them to prevent the
     * logs from flooding.  Set the log level to {@code DEBUG} and/or use {@link #STRICT_MODE} to force the
     * stack trace to be recorded every time.
     */
    private static final int MAX_STACK_TRACES = 3;

    /**
     * The {@link #MAX_STACK_TRACES} counter resets if no log messages are received within this many minutes.
     */
    private static final int MAX_IDLE_TIME = 15;

    /**
     * The minimum number of connections that must still be available for it to be considered safe to request
     * a second connection.  Outside of {@link #STRICT_MODE}, we would normally only log when {@code 3} or more
     * connections are requested at once; however, if the pool is within the "danger zone" of being exhausted,
     * then even requesting just {@code 2} connections is considered as a deadlock risk.
     * <p>
     * Default: {@code 2}
     * </p>
     */
    public static final String DANGER_ZONE = "jira.db.counter.danger.zone";

    /**
     * Forces all logging to be at the debug level instead of the warning level.
     */
    public static final String QUIET = "jira.db.counter.quiet";

    /**
     * Lowers the threshold of what constitutes "too many connections" from {@code 3} to {@code 2} so that we can
     * be even more aggressive in tracking down code that consumes multiple connections.  Be forewarned that there
     * are known places where multiple connections are deliberately used at times, such as when indexing issues,
     * creating an issue, or populating an ID bank for OfBiz, so the signal-to-noise ratio may be disappointing.
     * <p>
     * Default: {@code false}
     * </p>
     */
    public static final String STRICT_MODE = "jira.db.counter.strict.mode";

    static final RateLimitingLogger LOG = new RateLimitingLogger(ConnectionPoolHealthSqlInterceptor.class,
            MAX_STACK_TRACES, MAX_IDLE_TIME) {
        @Override
        protected boolean wantFullStackTrace() {
            return isStrictMode() || super.wantFullStackTrace();
        }
    };

    private static final ThreadLocal<CountHolder> COUNT_HOLDER = ThreadLocal.withInitial(CountHolder::new);

    @Override
    public void onConnectionTaken(final Connection connection, final ConnectionPoolState connectionPoolState) {
        COUNT_HOLDER.get().taken(connectionPoolState);
    }

    @Override
    public void onConnectionReplaced(final Connection connection, final ConnectionPoolState connectionPoolState) {
        COUNT_HOLDER.get().replaced(connectionPoolState);
    }

    @Override
    public void beforeExecution(final String sqlString, final List<String> parameterValues, final Statement statement) {
    }

    @Override
    public void afterSuccessfulExecution(final String sqlString, final List<String> parameterValues, final Statement statement,
                                         final ResultSet resultSet, final int rowsUpdated) {
    }

    @Override
    public void onException(final String sqlString, final List<String> parameterValues, final Statement statement,
                            final SQLException sqlException) {
    }

    static void reset() {
        COUNT_HOLDER.remove();
    }

    static int getDangerZone() {
        return JiraSystemProperties.getInstance().getInteger(DANGER_ZONE, 2);
    }

    static boolean isQuiet() {
        return JiraSystemProperties.getInstance().getBoolean(QUIET);
    }

    static boolean isStrictMode() {
        return JiraSystemProperties.getInstance().getBoolean(STRICT_MODE);
    }


    static class CountHolder {
        /**
         * An arbitrary time limit on how long this thread can have a connection allocated to it before we guess
         * that it must have been garbage collected or otherwise reclaimed.  When this happens, we reset the
         * count for this thread so that we won't accuse innocent code of consuming multiple connections
         * indefinitely.
         * <p>
         * This is a bit crappy, but the big problem here is that the connection's consumer might abandon the
         * connection without returning it to the pool.  Now, that would certainly be a bug, and it is interesting
         * to report about that if we can; however, not putting some kind of time cap for resetting the count would
         * mean that the connection got counted against this thread <strong>forever</strong>, even though it is not
         * actually using it anymore.  We need to have <strong>some</strong> way to heal from this, and putting a
         * time cap on how long we will hold this thread to account is the only way to be sure that we will.
         * </p>
         */
        private static final long STALE_AFTER = TimeUnit.MINUTES.toNanos(5L);

        /**
         * When this count holder will no longer be trusted as reliable.
         */
        private final long staleAfter = System.nanoTime() + STALE_AFTER;

        /**
         * The number of connections currently associated with this thread.
         */
        private int count;

        /**
         * The highest number of connections already seen since the last reset.
         */
        private int highWaterMark;

        /**
         * The lowest connection count seen since this thread was marked as having broken the rules.  When we first
         * detect a problem, this is set to the number of open connections.  As they are closed off and logged,
         * this gets decremented.
         */
        private int lowWaterMark = -1;


        boolean isStale() {
            return System.nanoTime() >= staleAfter;
        }

        private boolean isTooManyConnections() {
            return count >= 2 && (count > 2 || isStrictMode());
        }

        /**
         * Sets the low water mark at the lowest level that has not already been traced.
         * If the low water mark is currently not set, then use the current count to initialize it.
         * If the low water mark is already set, then use the current count only if that lowers it.
         * The point is the this is called when we are just about to log a warning message.  If the
         * low water mark is already set to a level lower than this, then we have already logged at
         * lower nesting levels than this one and don't want to log "on the way out" until we get
         * back to there.
         */
        private void initOrUpdateLowWaterMark() {
            if (lowWaterMark <= 0 || lowWaterMark >= count) {
                lowWaterMark = count - 1;
            }
        }

        /**
         * Checks whether or not the current count is below the low-water mark and adjusts the mark if appropriate.
         * If the low-water mark is already at or below the current count, then nothing happens.
         *
         * @return {@code true} iff this actually does reduce the low-water mark
         */
        private boolean isNewLowWaterMark() {
            if (lowWaterMark < count) {
                return false;
            }
            lowWaterMark = count - 1;
            return true;
        }


        /**
         * Checks whether or not the current count is below the high-water mark and adjusts the mark if appropriate.
         * If the high-water mark is already at or below the current count, then nothing happens.
         *
         * @return {@code true} iff this actually does raise the high-water mark
         */
        private boolean isNewHighWaterMark() {
            if (count <= highWaterMark) {
                return false;
            }
            highWaterMark = count;
            return true;
        }

        /**
         * We are in the "danger zone" if we are juggling multiple connections when there are very few left in the pool.
         */
        private boolean isDangerZone(final ConnectionPoolState state) {
            final int maxSize = state.getConnectionPoolInfo().getMaxSize();
            return maxSize > 0 && count > 1 && state.getBorrowedCount() + getDangerZone() >= maxSize;
        }

        void logWarn(final String event, final ConnectionPoolState state) {
            if (isQuiet()) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Dangerous use of multiple connections: " + event +
                            " => count=" + count + "; marks=[" + lowWaterMark + '-' + highWaterMark + "]; pool=" +
                            state.getBorrowedCount() + '/' + state.getConnectionPoolInfo().getMaxSize());
                }
            } else {
                LOG.warnWithTrace("Dangerous use of multiple connections: " + event +
                        " => count=" + count + "; marks=[" + lowWaterMark + '-' + highWaterMark + "]; pool=" +
                        state.getBorrowedCount() + '/' + state.getConnectionPoolInfo().getMaxSize());
            }
        }

        void taken(final ConnectionPoolState state) {
            if (isStale()) {
                LOG.debug("taken: discarding stale counter");
                reset();
                return;
            }

            ++count;
            if ((isTooManyConnections() && isNewHighWaterMark()) || isDangerZone(state)) {
                initOrUpdateLowWaterMark();
                logWarn("taken", state);
            }
        }

        void replaced(final ConnectionPoolState state) {
            if (isStale()) {
                LOG.debug("replaced: discarding stale counter");
                reset();
                return;
            }

            count = max(0, count - 1);

            if (isNewLowWaterMark() || isDangerZone(state)) {
                logWarn("replaced", state);
            }
            if (count <= 0) {
                reset();
            }
        }

        @Override
        public String toString() {
            return "CountHolder[stale=" + isStale() +
                    ",count=" + count +
                    ",highWaterMark=" + highWaterMark +
                    ",lowWaterMark=" + lowWaterMark + ']';
        }
    }
}
