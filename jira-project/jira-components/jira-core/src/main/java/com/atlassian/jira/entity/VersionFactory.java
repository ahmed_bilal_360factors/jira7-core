package com.atlassian.jira.entity;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionImpl;
import org.ofbiz.core.entity.GenericValue;

import java.util.Map;

import static com.atlassian.jira.project.version.VersionImpl.copyDate;

/**
 * Builds {@link com.atlassian.jira.project.version.Version} from {@link org.ofbiz.core.entity.GenericValue}
 *
 * @since v7.0
 */
public class VersionFactory extends AbstractEntityFactory<Version> {
    public Map<String, Object> fieldMapFrom(Version value) {
        FieldMap map = new FieldMap();
        map.add("project", value.getProjectId());
        map.add("id", value.getId());
        map.add("name", value.getName());
        map.add("description", value.getDescription());
        map.add("sequence", value.getSequence());
        map.add("archived", value.isArchived() ? "true" : null);
        map.add("released", value.isReleased() ? "true" : null);
        map.add("releasedate", copyDate(value.getReleaseDate()));
        map.add("startdate", copyDate(value.getStartDate()));
        return map;
    }

    @Override
    public String getEntityName() {
        return "Version";
    }

    @Override
    public Version build(GenericValue genericValue) {
        return new VersionImpl(
                genericValue.getLong("project"),
                genericValue.getLong("id"),
                genericValue.getString("name"),
                genericValue.getString("description"),
                genericValue.getLong("sequence"),
                "true".equals(genericValue.getString("archived")),
                "true".equals(genericValue.getString("released")),
                copyDate(genericValue.getTimestamp("releasedate")),
                copyDate(genericValue.getTimestamp("startdate")));
    }
}
