package com.atlassian.jira;

import com.atlassian.jira.scheduler.JiraCaesiumSchedulerConfiguration;
import com.atlassian.jira.scheduler.JiraCaesiumSchedulerService;
import com.atlassian.jira.scheduler.JiraParameterMapSerializer;
import com.atlassian.jira.scheduler.OfBizClusteredJobDao;
import com.atlassian.jira.scheduler.OfBizRunDetailsDao;
import com.atlassian.jira.scheduler.cron.CronValidator;
import com.atlassian.scheduler.SchedulerHistoryService;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.caesium.cron.CaesiumCronExpressionValidator;
import com.atlassian.scheduler.caesium.spi.CaesiumSchedulerConfiguration;
import com.atlassian.scheduler.caesium.spi.ClusteredJobDao;
import com.atlassian.scheduler.core.DefaultSchedulerHistoryService;
import com.atlassian.scheduler.core.DelegatingSchedulerService;
import com.atlassian.scheduler.core.LifecycleAwareSchedulerService;
import com.atlassian.scheduler.core.spi.RunDetailsDao;
import com.atlassian.scheduler.cron.CronExpressionValidator;

import static com.atlassian.jira.ComponentContainer.Scope.INTERNAL;
import static com.atlassian.jira.ComponentContainer.Scope.PROVIDED;

/**
 * @since v7.0
 */
class SchedulerServiceRegistrar {
    static void registerSchedulerService(final ComponentContainer register) {
        register.implementation(INTERNAL, ClusteredJobDao.class, OfBizClusteredJobDao.class);
        register.implementation(INTERNAL, RunDetailsDao.class, OfBizRunDetailsDao.class);
        register.implementation(INTERNAL, CaesiumSchedulerConfiguration.class, JiraCaesiumSchedulerConfiguration.class);
        register.implementation(INTERNAL, LifecycleAwareSchedulerService.class, JiraCaesiumSchedulerService.class);

        register.implementation(PROVIDED, CronExpressionValidator.class, CaesiumCronExpressionValidator.class);
        register.implementation(PROVIDED, SchedulerService.class, DelegatingSchedulerService.class,
                LifecycleAwareSchedulerService.class);
        register.implementation(PROVIDED, SchedulerHistoryService.class, DefaultSchedulerHistoryService.class);

        register.implementation(INTERNAL, CronValidator.class);
        register.implementation(INTERNAL, JiraParameterMapSerializer.class);
    }

}
