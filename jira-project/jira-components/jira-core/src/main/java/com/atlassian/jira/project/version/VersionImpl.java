package com.atlassian.jira.project.version;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nullable;
import java.sql.Timestamp;
import java.util.Date;

public class VersionImpl implements Version {
    private final Long projectId;
    private final Long id;
    private final String name;
    private final String description;
    private final Long sequence;
    private final boolean archived;
    private final boolean released;
    private final Timestamp releaseDate;
    private final Timestamp startDate;

    private ProjectManager projectManager = null;

    public VersionImpl(Long projectId, Long id, String name) {
        this(projectId, id, name, null, null, false, false, null, null);
    }

    public VersionImpl(Long projectId, Long id, String name, String description, Long sequence,
                       boolean archived, boolean released, Date releaseDate, Date startDate) {
        this.projectId = projectId;
        this.id = id;
        this.name = name;
        this.description = description;
        this.sequence = sequence;
        this.archived = archived;
        this.released = released;
        this.releaseDate = copyDate(releaseDate);
        this.startDate = copyDate(startDate);
    }

    @Override
    public Project getProject() {
        return getProjectObject();
    }

    @Override
    public Long getProjectId() {
        return projectId;
    }

    @Override
    public Project getProjectObject() {
        if (projectManager == null) {
            projectManager = ComponentAccessor.getProjectManager();
        }
        return projectManager.getProjectObj(projectId);
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Nullable
    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Long getSequence() {
        return sequence;
    }

    @Override
    public boolean isArchived() {
        return archived;
    }

    @Override
    public boolean isReleased() {
        return released;
    }

    @Nullable
    @Override
    public Date getReleaseDate() {
        return copyDate(releaseDate);
    }

    @Nullable
    public static Timestamp copyDate(@Nullable Date date) {
        return date != null ? new Timestamp(date.getTime()) : null;
    }

    public GenericValue toGenericValue() {
        final GenericValue genericValue = ComponentAccessor.getOfBizDelegator().makeValue(OfBizVersionStore.ENTITY_NAME);

        genericValue.set("project", projectId);
        genericValue.set("id", id);
        genericValue.setString("name", name);
        genericValue.setString("description", description);
        genericValue.set("sequence", sequence);
        genericValue.setString("archived", archived ? "true" : null);
        genericValue.setString("released", released ? "true" : null);
        genericValue.set("releasedate", copyDate(releaseDate));
        genericValue.set("startdate", copyDate(startDate));
        return genericValue;
    }

    @Nullable
    @Override
    public Date getStartDate() {
        return copyDate(startDate);
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VersionImpl)) return false;

        VersionImpl version = (VersionImpl) o;

        if (archived != version.archived) return false;
        if (released != version.released) return false;
        if (description != null ? !description.equals(version.description) : version.description != null) return false;
        if (id != null ? !id.equals(version.id) : version.id != null) return false;
        if (name != null ? !name.equals(version.name) : version.name != null) return false;
        if (projectId != null ? !projectId.equals(version.projectId) : version.projectId != null) return false;
        if (releaseDate != null ? !releaseDate.equals(version.releaseDate) : version.releaseDate != null) return false;
        if (sequence != null ? !sequence.equals(version.sequence) : version.sequence != null) return false;
        if (startDate != null ? !startDate.equals(version.startDate) : version.startDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = projectId != null ? projectId.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (sequence != null ? sequence.hashCode() : 0);
        result = 31 * result + (archived ? 1 : 0);
        result = 31 * result + (released ? 1 : 0);
        result = 31 * result + (releaseDate != null ? releaseDate.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        return result;
    }
}
