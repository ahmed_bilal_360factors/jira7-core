package com.atlassian.jira.help;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;

/**
 * Returns application help space for given application key.
 */
public interface ApplicationHelpSpaceProvider {
    /**
     * Returns application help space for given application key.
     *
     * @param applicationKey application key
     * @return application version or {@code Option.none()} if no application for given key is installed
     */
    Option<String> getHelpSpace(ApplicationKey applicationKey);
}
