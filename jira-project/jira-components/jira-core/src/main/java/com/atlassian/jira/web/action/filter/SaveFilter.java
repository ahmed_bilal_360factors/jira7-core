package com.atlassian.jira.web.action.filter;

import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.managers.IssueSearcherManager;
import com.atlassian.jira.issue.search.searchers.IssueSearcher;
import com.atlassian.jira.issue.transport.FieldValuesHolder;
import com.atlassian.jira.issue.transport.impl.FieldValuesHolderImpl;
import com.atlassian.jira.web.action.util.DiffViewRenderer;
import com.atlassian.query.order.SearchSort;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Action called to update a filter's search parameters and search sorts.
 */
public class SaveFilter extends AbstractFilterAction implements FilterOperationsAction {
    private final SearchRequestService searchRequestService;
    private final SearchService searchService;
    private final DiffViewRenderer diffViewRenderer;
    private SearchRequest dbSearchRequest;
    private SearchContext dbSearchContext;
    private FieldValuesHolder dbFieldValuesHolder;

    public SaveFilter(IssueSearcherManager issueSearcherManager, final SearchRequestService searchRequestService,
                      final SearchService searchService, final DiffViewRenderer diffViewRenderer) {
        super(issueSearcherManager, searchService);
        this.searchRequestService = searchRequestService;
        this.searchService = searchService;
        this.diffViewRenderer = diffViewRenderer;
    }

    public SaveFilter(final SearchRequestService searchRequestService, final SearchService searchService,
                      final DiffViewRenderer diffViewRenderer) {
        this(ComponentAccessor.getComponentOfType(IssueSearcherManager.class), searchRequestService, searchService, diffViewRenderer);
    }

    public String doDefault() throws Exception {
        if (!validateSearchRequest()) {
            return ERROR;
        } else {
            updateDbRequestState();
            return INPUT;
        }
    }

    protected String doExecute() throws Exception {
        if (!validateSearchRequest()) {
            return ERROR;
        }

        final SearchRequest newRequest = searchRequestService.updateSearchParameters(getJiraServiceContext(), getSearchRequest());
        if (newRequest == null || hasAnyErrors()) {
            return ERROR;
        } else {
            setSearchRequest(newRequest);
            return getRedirect(getHttpRequest().getContextPath() + "/issues/?filter=" + getFilterId());
        }
    }

    public List<SearchSort> getSearchSorts(SearchRequest searchRequest) {
        if (searchRequest != null && searchRequest.getQuery().getOrderByClause() != null) {
            return searchRequest.getQuery().getOrderByClause().getSearchSorts();
        }
        return Collections.emptyList();
    }

    private void updateDbRequestState() {
        if (getSearchRequest() == null || getFilterId() == null) {
            dbSearchRequest = new SearchRequest();
            dbSearchRequest.setOwner(getLoggedInUser());
        } else {
            dbSearchRequest = getFilter();
        }

        dbFieldValuesHolder = new FieldValuesHolderImpl();
        dbSearchContext = searchService.getSearchContext(getLoggedInUser(), dbSearchRequest.getQuery());
        final Collection<IssueSearcher<?>> searchers = issueSearcherManager.getAllSearchers();
        for (IssueSearcher<?> searcher : searchers) {
            searcher.getSearchInputTransformer().populateFromQuery(getLoggedInUser(), dbFieldValuesHolder, dbSearchRequest.getQuery(), dbSearchContext);
        }
    }

    private boolean validateSearchRequest() {
        if (getSearchRequest() == null) {
            addErrorMessage(getText("admin.errors.filters.no.search.request"));
            return false;
        } else if (!searchRequestService.validateUpdateSearchParameters(getJiraServiceContext(), getSearchRequest())) {
            return false;
        }
        return true;
    }

    @Override
    public Long getFilterId() {
        final SearchRequest searchRequest = getSearchRequest();
        return searchRequest != null ? searchRequest.getId() : null;
    }
}
