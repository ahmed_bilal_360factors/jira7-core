package com.atlassian.jira;

import com.atlassian.jira.cache.soy.SoyCacheWarmer;
import com.atlassian.jira.i18n.I18nWarmer;
import com.atlassian.jira.template.velocity.VelocityEngineWarmer;

/**
 * Register warmers for JIRA caches.
 *
 * @since v7.1
 */
public class WarmersRegistrar {
    public static void registerWarmers(final ComponentContainer register) {
        register.implementation(ComponentContainer.Scope.INTERNAL, SoyCacheWarmer.class);
        register.implementation(ComponentContainer.Scope.INTERNAL, I18nWarmer.class);
        register.implementation(ComponentContainer.Scope.INTERNAL, VelocityEngineWarmer.class);
    }
}
