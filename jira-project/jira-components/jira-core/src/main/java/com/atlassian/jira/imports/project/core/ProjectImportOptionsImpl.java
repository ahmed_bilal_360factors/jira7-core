package com.atlassian.jira.imports.project.core;

/**
 * @since v3.13
 */
public class ProjectImportOptionsImpl implements ProjectImportOptions {
    private final String pathToBackupZip;
    private final String attachmentPath;
    private String selectedProjectKey;
    private boolean overwriteProjectDetails;

    public ProjectImportOptionsImpl(final String pathToBackupZip, final String attachmentPath) {
        this.pathToBackupZip = pathToBackupZip;
        this.attachmentPath = attachmentPath;
    }

    public ProjectImportOptionsImpl(final String pathToBackupZip, final String attachmentPath, final boolean overwriteProjectDetails) {
        this.pathToBackupZip = pathToBackupZip;
        this.attachmentPath = attachmentPath;
        this.overwriteProjectDetails = overwriteProjectDetails;
    }

    public String getPathToBackup() {
        return pathToBackupZip;
    }

    public String getAttachmentPath() {
        return attachmentPath;
    }

    public boolean overwriteProjectDetails() {
        return overwriteProjectDetails;
    }

    public void setOverwriteProjectDetails(final boolean overwriteProjectDetails) {
        this.overwriteProjectDetails = overwriteProjectDetails;
    }

    public String getSelectedProjectKey() {
        return selectedProjectKey;
    }

    public void setSelectedProjectKey(final String selectedProjectKey) {
        this.selectedProjectKey = selectedProjectKey;
    }
}
