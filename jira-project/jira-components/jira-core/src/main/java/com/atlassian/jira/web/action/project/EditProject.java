package com.atlassian.jira.web.action.project;

import com.atlassian.fugue.Option;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.bc.project.index.ProjectReindexService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.entity.ProjectCategoryFactory;
import com.atlassian.jira.event.project.ProjectEventManager;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.renderer.ProjectDescriptionRenderer;
import com.atlassian.jira.project.type.ProjectType;
import com.atlassian.jira.project.type.ProjectTypeManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.UserKeyService;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.ObjectUtils;
import org.ofbiz.core.entity.GenericEntityException;
import webwork.action.ActionContext;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;

import static com.atlassian.jira.entity.ProjectCategoryFactory.NONE_PROJECT_CATEGORY_ID;
import static com.google.common.collect.Lists.newArrayList;

public class EditProject extends ViewProject {
    private final ProjectService projectService;
    private final UserManager userManager;
    private final ProjectTypeManager projectTypeManager;
    private final ProjectDescriptionRenderer projectDescriptionRenderer;
    private final UserKeyService userKeyService;
    private final PageBuilderService pageBuilderService;
    private final ProjectReindexService projectReindexService;
    private final HelpUrls helpUrls;
    private final ProjectEventManager projectEventManager;
    private String originalKey;
    private boolean keyEdited;
    private Long projectCategoryId;

    public EditProject(ProjectService projectService, UserManager userManager,
                       ProjectTypeManager projectTypeManager, UserKeyService userKeyService,
                       PageBuilderService pageBuilderService, ProjectReindexService projectReindexService,
                       HelpUrls helpUrls, ProjectEventManager projectEventManager) {

        this.projectService = projectService;
        this.userManager = userManager;
        this.projectTypeManager = projectTypeManager;
        this.userKeyService = userKeyService;
        this.pageBuilderService = pageBuilderService;
        this.projectReindexService = projectReindexService;
        this.helpUrls = helpUrls;
        this.projectEventManager = projectEventManager;
        projectDescriptionRenderer = ComponentAccessor.getComponentOfType(ProjectDescriptionRenderer.class);
    }

    @Override
    public String doDefault() throws Exception {
        // check if the project exists:
        final Project projectObject = getProjectObject();
        if (projectObject == null) {
            return handleProjectDoesNotExist();
        }
        if (!(hasProjectAdminPermission() || hasAdminPermission())) {
            return "securitybreach";
        }
        setName(projectObject.getName());
        setLead(userKeyService.getUsernameForKey(projectObject.getLeadUserKey()));
        setKeyEdited(false);
        final String key = projectObject.getKey();
        setKey(key);
        setOriginalKey(key);
        setUrl(projectObject.getUrl());
        setDescription(projectObject.getDescription());
        setAssigneeType(projectObject.getAssigneeType());
        setAvatarId(projectObject.getAvatar().getId());
        setProjectTypeKey(projectObject.getProjectTypeKey().getKey());

        final ProjectCategory initialProjectCategory = projectManager.getProjectCategoryForProject(projectObject);
        if (initialProjectCategory != null) {
            setProjectCategoryId(initialProjectCategory.getId());
        } else {
            setProjectCategoryId(NONE_PROJECT_CATEGORY_ID);
        }

        tagMauEventWithProject(projectObject);

        return INPUT;
    }

    @Override
    protected void doValidation() {
        // First check that the Project still exists
        if (getProjectObject() == null) {
            addErrorMessage(getText("admin.errors.project.no.project.with.id"));
            // Don't try to do any more validation.
            return;
        }

        ProjectService.UpdateProjectRequest update = getUpdateProjectRequest();

        ProjectService.UpdateProjectValidationResult validationResult = projectService.validateUpdateProject(getLoggedInUser(), update);

        if (!validationResult.isValid()) {
            //map keyed errors to JSP field names
            mapErrorCollection(validationResult.getErrorCollection());

            if (getErrors().containsKey("projectTypeKey")) {
                setProjectTypeKey(getProject().getProjectTypeKey().getKey());
            }
        }

        if (validationResult.isKeyChanged()) {
            if (!projectReindexService.isReindexPossible(getProjectObject())) {
                addError("key", getText("admin.errors.project.key.other.reindex"));
            }
        }

        // this handles an edge case: user without admin permission should not be able to change project category, the UI disables the project category select.
        // In case the submit was done immediately before their permission was revoked, this routine handles it.
        if (getErrorMessages().contains(getText("admin.projects.service.error.no.admin.permission.projectcategory"))) {
            if (getProjectObject().getProjectCategory() == null) {
                setProjectCategoryId(NONE_PROJECT_CATEGORY_ID);
            } else {
                setProjectCategoryId(getProjectObject().getProjectCategory().getId());
            }
        }

        // This validation seems to be redundant now - but leave it in case we add something special to ViewProject.doValidation()
        super.doValidation();
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        if (!(hasProjectAdminPermission() || hasAdminPermission())) {
            return "securitybreach";
        }

        ProjectService.UpdateProjectRequest projectUpdate = getUpdateProjectRequest();

        ProjectService.UpdateProjectValidationResult validationResult = projectService.validateUpdateProject(getLoggedInUser(), projectUpdate);
        projectService.updateProject(validationResult);

        if (isProjectCategoryChanging(getProjectObject())) {
            projectEventManager.dispatchProjectCategoryChanged(getRequestSourceType());
        }
        projectManager.setProjectCategory(getProject(), getProjectCategory().getOrNull());

        final String redirectURL;
        if (validationResult.isKeyChanged()) {
            redirectURL = "/secure/project/IndexProject.jspa?confirmed=true&pid=" + getProjectObject().getId();
        } else {
            redirectURL = "/secure/project/EditProject!default.jspa?pid=" + getProjectObject().getId();
        }
        if (isInlineDialogMode()) {
            return returnCompleteWithInlineRedirect(redirectURL);
        }
        return getRedirect(redirectURL);
    }

    private Option<ProjectCategory> getProjectCategory() {
        if (null == getProjectCategoryId() || getProjectCategoryId().equals(NONE_PROJECT_CATEGORY_ID)
                || null == projectManager.getProjectCategoryObject(getProjectCategoryId())) {
            return Option.none();
        }

        return Option.some(projectManager.getProjectCategoryObject(getProjectCategoryId()));
    }

    public String getAvatarUrl() {
        return ActionContext.getRequest().getContextPath() + "/secure/projectavatar?pid=" + getPid() + "&size=large&avatarId="
                + getProjectObject().getAvatar().getId();
    }

    public String getProjectDescriptionEditHtml() {
        return projectDescriptionRenderer.getEditHtml(StringUtils.defaultString(getDescription()));
    }

    public ProjectDescriptionRenderer getProjectDescriptionRenderer() {
        return projectDescriptionRenderer;
    }

    private String handleProjectDoesNotExist() throws Exception {
        if (hasAdminPermission()) {
            // User is admin - admit that the Project Doesn't exist because they have permission to see any project.
            // We will show the Edit Project Page, but without any values in the fields (and with an error message).
            // This is consistent with what happens if we start to edit a project, but it gets deleted before we save it.
            setName(getText("common.sharing.exception.project.does.not.exist"));
            addErrorMessage(getText("admin.errors.project.no.project.with.id"));

            return super.doDefault();
        } else {
            // User is not admin - show security breach because this isn't a Project they have permission to edit.
            return "securitybreach";
        }
    }

    protected ProjectService.UpdateProjectValidationResult getUpdateProjectValidationResult() {
        final Project projectObject = getProjectObject();
        if (isProjectKeyRenameAllowed() && isKeyEdited()) {
            return projectService.validateUpdateProject(getLoggedInUser(), getProjectObject(), getName(),
                    getKey(), getDescription(), projectObject.getProjectLead(),
                    getUrl(), projectObject.getAssigneeType(), getAvatarId());
        } else {
            return projectService.validateUpdateProject(getLoggedInUser(), getName(),
                    projectObject.getKey(),
                    getDescription(), projectObject.getProjectLead(), getUrl(), projectObject.getAssigneeType(), getAvatarId());
        }
    }

    public Project getProject() {
        return projectManager.getProjectObj(getPid());
    }

    public boolean hasInvalidLead() {
        final Project projectObject = getProjectObject();

        if (projectObject == null) {
            return false;
        } else {
            final String leadUserName = projectObject.getLeadUserName();
            return userManager.getUserObject(leadUserName) == null;
        }
    }

    public List<ProjectType> getProjectTypeOptions() {
        List<ProjectType> accessibleProjectTypes = Lists.newArrayList(projectTypeManager.getAllAccessibleProjectTypes());

        Option<ProjectType> currentProjectType = projectTypeManager.getByKey(getProjectObject().getProjectTypeKey());

        if (currentProjectType.isDefined()) {
            ProjectType projectType = currentProjectType.get();

            if (projectTypeManager.isProjectTypeInstalledButInaccessible(projectType.getKey())) {
                accessibleProjectTypes.add(projectType);
            }
        } else {
            ProjectType inaccessibleProjectType = projectTypeManager.getInaccessibleProjectType();

            ProjectType generatedProjectType = new ProjectType(getProjectObject().getProjectTypeKey(),
                    inaccessibleProjectType.getDescriptionI18nKey(),
                    inaccessibleProjectType.getIcon(),
                    inaccessibleProjectType.getColor(),
                    inaccessibleProjectType.getWeight());

            accessibleProjectTypes.add(generatedProjectType);
        }

        return accessibleProjectTypes;
    }

    public boolean isProjectTypeChangeAllowed() {
        return getGlobalPermissionManager().hasPermission(GlobalPermissionKey.ADMINISTER, getLoggedInUser());
    }

    public String getProjectTypeHelpUrl() {
        return helpUrls.getUrl("jira_applications_overview").getUrl();
    }

    private ProjectCategory createEmptyProjectCategory() {
        return new ProjectCategoryFactory.Builder().id(NONE_PROJECT_CATEGORY_ID).name("None").build();
    }

    public Collection<ProjectCategory> getProjectCategories() throws GenericEntityException {
        final Collection<ProjectCategory> projectCategoriesToDisplay = newArrayList(createEmptyProjectCategory());
        projectCategoriesToDisplay.addAll(projectManager.getAllProjectCategories());
        return projectCategoriesToDisplay;
    }

    public int getMaxNameLength() {
        return projectService.getMaximumNameLength();
    }

    public int getMaxKeyLength() {
        return projectService.getMaximumKeyLength();
    }

    public boolean isProjectKeyRenameAllowed() {
        return hasAdminPermission();
    }

    public void setOriginalKey(final String originalKey) {
        this.originalKey = originalKey;
    }

    public String getOriginalKey() {
        return originalKey;
    }

    public void setKeyEdited(final boolean keyEdited) {
        this.keyEdited = keyEdited;
    }

    public boolean isKeyEdited() {
        return keyEdited;
    }

    private ProjectService.UpdateProjectRequest getUpdateProjectRequest() {
        final Project originalProject = getProjectObject();

        ProjectService.UpdateProjectRequest updateProjectRequest = new ProjectService.UpdateProjectRequest(originalProject);

        if (getKey() != null && !getKey().equals(originalProject.getKey())) {
            updateProjectRequest.key(getKey());
        }

        if (!getName().equals(originalProject.getName())) {
            updateProjectRequest.name(getName());
        }

        if (!getDescription().equals(originalProject.getDescription())) {
            updateProjectRequest.description(getDescription());
        }

        if (!getUrl().equals(originalProject.getUrl())) {
            updateProjectRequest.url(getUrl());
        }

        if (!getAvatarId().equals(originalProject.getAvatar().getId())) {
            updateProjectRequest.avatarId(getAvatarId());
        }

        if (getProjectTypeKey() != null && !getProjectTypeKey().equals(originalProject.getProjectTypeKey().getKey())) {
            updateProjectRequest.projectType(getProjectTypeKey());
        }

        if (getProjectCategoryId() != null && isProjectCategoryChanging(getProjectCategoryId(), originalProject.getProjectCategory())) {
            updateProjectRequest.projectCategoryId(getProjectCategoryId());
        }

        updateProjectRequest.requestSourceType(getRequestSourceType());
        return updateProjectRequest;
    }

    private boolean isProjectCategoryChanging(final Project originalProject) {
        // both ProjectCategory objects are nullable, so code needs to handle comparisons very defensively in order to
        // determine when project category is changing
        final ProjectCategory originalProjectCategory = originalProject.getProjectCategory();
        final ProjectCategory newProjectCategory = getProjectCategory().getOrNull();

        if (originalProjectCategory == null && newProjectCategory == null) {
            return false;
        } else if ((originalProjectCategory == null && newProjectCategory != null) || (originalProjectCategory != null && newProjectCategory == null)) {
            return true;
        }

        return ObjectUtils.compare(originalProjectCategory.getId(), newProjectCategory.getId()) != 0;
    }

    @Override
    public String getKey() {
        if (!isKeyEdited()) {
            return getOriginalKey();
        } else {
            return super.getKey();
        }
    }

    public Long getProjectCategoryId() {
        return projectCategoryId;
    }

    public void setProjectCategoryId(final Long projectCategoryId) {
        this.projectCategoryId = projectCategoryId;
    }

    private boolean isProjectCategoryChanging(@Nonnull Long newProjectCategoryId, @Nullable ProjectCategory oldProjectCategory) {
        return (hasProjectCategoryChangedFromDefault(newProjectCategoryId, oldProjectCategory)
                || hasProjectCategoryChanged(newProjectCategoryId, oldProjectCategory));
    }

    private boolean hasProjectCategoryChanged(@Nonnull Long newProjectCategoryId, @Nullable ProjectCategory oldProjectCategory) {
        return oldProjectCategory != null && !oldProjectCategory.getId().equals(newProjectCategoryId);
    }

    private boolean hasProjectCategoryChangedFromDefault(@Nonnull Long newProjectCategoryId, @Nullable ProjectCategory oldProjectCategory) {
        return oldProjectCategory == null && !newProjectCategoryId.equals(NONE_PROJECT_CATEGORY_ID);
    }
}
