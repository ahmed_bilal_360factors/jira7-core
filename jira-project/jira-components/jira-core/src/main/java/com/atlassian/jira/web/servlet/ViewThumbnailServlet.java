package com.atlassian.jira.web.servlet;

import com.atlassian.fugue.Unit;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.thumbnail.ThumbnailManager;
import com.atlassian.jira.util.AttachmentUtils;
import com.atlassian.jira.util.io.InputStreamConsumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class ViewThumbnailServlet extends AbstractViewFileServlet {
    @Override
    protected int getContentLength(String attachmentPath) {
        Attachment attachment = getAttachment(attachmentPath);
        File thumbnailFile = AttachmentUtils.getThumbnailFile(attachment);
        return (int) thumbnailFile.length();
    }

    @Override
    protected boolean supportsRangeRequests() {
        return false;
    }

    /**
     * @see AbstractViewFileServlet#getInputStream(String, com.atlassian.jira.util.io.InputStreamConsumer)
     */
    @Override
    protected void getInputStream(final String attachmentPath, final InputStreamConsumer<Unit> consumer)
            throws IOException, PermissionException {
        Attachment attachment = getAttachment(attachmentPath);

        if (!loggedInUserHasPermissionToViewAttachment(attachment)) {
            throw new PermissionException("You do not have permissions to view this attachment");
        }

        ComponentAccessor.getComponentOfType(ThumbnailManager.class).streamThumbnailContent(attachment, consumer);
    }

    @Override
    protected void setResponseHeaders(HttpServletRequest request, Optional<RangeResponse> rangeResponse, HttpServletResponse response) {
        Attachment attachment = getAttachment(attachmentQuery(request));
        File thumbnailFile = AttachmentUtils.getThumbnailFile(attachment);
        // All thumbnail images are stored in JPEG format.
        response.setContentType(ThumbnailManager.MIME_TYPE.toString());
        response.setContentLength((int) thumbnailFile.length());
        response.setHeader("Content-Disposition", "inline; filename=" + thumbnailFile.getName() + ";");

        HttpResponseHeaders.cachePrivatelyForAboutOneYear(response);
    }
}
