package com.atlassian.jira.config;

import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollections;
import com.atlassian.jira.web.bean.I18nBean;

import java.util.List;

import static com.atlassian.jira.util.ErrorCollection.Reason.VALIDATION_FAILED;
import static com.atlassian.jira.util.ErrorCollection.Reason.FORBIDDEN;
import static com.atlassian.jira.util.ErrorCollection.Reason.SERVER_ERROR;

public class SubTaskServiceImpl implements SubTaskService {
    private final SubTaskManager manager;
    private final PermissionManager permissionManager;
    private final FeatureManager featureManager;

    public SubTaskServiceImpl(SubTaskManager manager, PermissionManager permissionManager, FeatureManager featureManager) {
        this.manager = manager;
        this.permissionManager = permissionManager;
        this.featureManager = featureManager;
    }

    @Override
    public ServiceResult moveSubTask(ApplicationUser user, Issue parentIssue, Long currentSequence, Long sequence) {
        final I18nBean i18n = new I18nBean(user);

        ErrorCollection result = ErrorCollections.empty();

        if (canMoveSubtask(user, parentIssue)) {
            final List<IssueLink> subTaskIssueLinks = manager.getSubTaskIssueLinks(parentIssue.getId());

            if(!withinBounds(subTaskIssueLinks, currentSequence) || !withinBounds(subTaskIssueLinks, sequence)) {
                result.addErrorMessage(
                        String.format("%s. %s",
                                i18n.getText("admin.permissions.feedback.unspecifiederror.title"),
                                i18n.getText("admin.permissions.feedback.unspecifiederror.description")),
                        VALIDATION_FAILED);
            }
            try {
                manager.moveSubTask(parentIssue, currentSequence, sequence);
            } catch (Exception e) {
                result.addErrorMessage(
                        String.format("%s. %s",
                                i18n.getText("admin.permissions.feedback.unspecifiederror.title"),
                                i18n.getText("admin.permissions.feedback.unspecifiederror.description")),
                        SERVER_ERROR);
            }

        } else {
            result.addErrorMessage(i18n.getText("editissue.error.no.edit.permission"), FORBIDDEN);
        }

        return ServiceOutcomeImpl.from(result);
    }

    @Override
    public boolean isReorderColumnShown() {
        return !featureManager.isEnabled(REORDER_COLUMN_HIDDEN_DF_KEY);
    }

    @Override
    public boolean canMoveSubtask(ApplicationUser user, Issue parentIssue) {
        return permissionManager.hasPermission(ProjectPermissions.EDIT_ISSUES, parentIssue, user);
    }

    private boolean withinBounds(List<IssueLink> list, Long index) {
        return index >= 0 && index < list.size();
    }


}
