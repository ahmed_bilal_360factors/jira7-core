package com.atlassian.jira.project.util;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.entity.Delete;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import org.ofbiz.core.entity.EntityUtil;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.google.common.collect.ImmutableSortedMap.copyOf;
import static java.lang.String.CASE_INSENSITIVE_ORDER;

/**
 * @since v6.1
 */
public class ProjectKeyStoreImpl implements ProjectKeyStore {
    public static final String PROJECT_KEY = "projectKey";
    public static final String PROJECT_ID = "projectId";
    public static String ENTITY_NAME = "ProjectKey";

    private final OfBizDelegator ofBizDelegator;

    public ProjectKeyStoreImpl(OfBizDelegator ofBizDelegator) {
        this.ofBizDelegator = ofBizDelegator;
    }

    @Override
    public Long getProjectId(final String key) {
        final GenericValue gv = EntityUtil.getOnly(ofBizDelegator.findByAnd(ENTITY_NAME, FieldMap.build(PROJECT_KEY, key)));
        return gv != null ? gv.getLong(PROJECT_ID) : null;
    }

    @Override
    public void addProjectKey(final Long projectId, final String projectKey) {
        ofBizDelegator.createValue(ENTITY_NAME, ImmutableMap.of(PROJECT_ID, projectId, PROJECT_KEY, projectKey));
    }

    @Override
    public void deleteProjectKeys(final Long projectId) {
        Delete.from("ProjectKey")
                .whereEqual(PROJECT_ID, projectId)
                .execute(ofBizDelegator);
    }

    @Nonnull
    @Override
    public Map<String, Long> getAllProjectKeys() {
        final List<GenericValue> keys = ofBizDelegator.findAll(ENTITY_NAME);
        if (keys == null) {
            return Collections.emptyMap();
        }
        return keys.stream()
                .collect(Collectors.toMap(gv -> gv.getString(PROJECT_KEY), gv -> gv.getLong(PROJECT_ID)));
    }

    @Nullable
    @Override
    public Long getProjectIdByKeyIgnoreCase(final String projectKey) {
        return copyOf(getAllProjectKeys(), CASE_INSENSITIVE_ORDER).get(projectKey);
    }

    @Override
    @Nonnull
    public Set<String> getProjectKeys(final Long projectId) {
        final List<GenericValue> keys = ofBizDelegator.findByAnd(ENTITY_NAME, FieldMap.build(PROJECT_ID, projectId));
        if (keys == null) {
            return Collections.emptySet();
        }
        return keys.stream().map(gv -> gv.getString(PROJECT_KEY)).collect(CollectorsUtil.toImmutableSet());
    }

    @Override
    public void refresh() {
    }
}
