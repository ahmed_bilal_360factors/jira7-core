package com.atlassian.jira.upgrade;

/**
 * Old upgrade tasks will extend this.
 * They are not downgradable because they were created before the Downgrade Task framework was built.
 *
 * @since v6.4.4
 */
public abstract class LegacyImmediateUpgradeTask extends AbstractImmediateUpgradeTask {
    protected LegacyImmediateUpgradeTask() {
        super();
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        // Flagged as not downgradable because it was created before the Downgrade Task framework was built.
        return true;
    }
}
