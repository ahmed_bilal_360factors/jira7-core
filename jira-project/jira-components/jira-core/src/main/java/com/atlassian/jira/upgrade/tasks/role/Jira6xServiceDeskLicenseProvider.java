package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.fugue.Option;

/**
 * Retrieves Service Desk license for Renaissance migration.
 * If JIRA is running behind the firewall license is retrieved from UPM store, in Cloud there is no separate license
 * for Service Desk and it is retrieved first from multistore and if it is not there old jira store is checked.
 *
 * @since 7.0
 */
public interface Jira6xServiceDeskLicenseProvider {
    /**
     * Gets Service Desk license for JIRA.
     *
     * @return {@link Option#some(Object)} if there is Service Desk license or {@link Option#none()} otherwise.
     */
    Option<License> serviceDeskLicense();
}
