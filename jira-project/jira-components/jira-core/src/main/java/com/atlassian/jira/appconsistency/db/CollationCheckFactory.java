package com.atlassian.jira.appconsistency.db;

import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.ofbiz.DefaultOfBizConnectionFactory;

public class CollationCheckFactory {
    public static CollationCheck createCollationCheck(DatabaseConfigurationManager databaseConfigurationManager) {
        return new CollationCheck(databaseConfigurationManager, DefaultOfBizConnectionFactory.getInstance(), JiraSystemProperties.getInstance());
    }
}
