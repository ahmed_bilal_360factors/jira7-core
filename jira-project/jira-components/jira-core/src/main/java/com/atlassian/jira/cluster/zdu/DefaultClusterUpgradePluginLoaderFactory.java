package com.atlassian.jira.cluster.zdu;

import com.atlassian.event.api.EventListenerRegistrar;
import com.atlassian.jira.cluster.ClusterMessageConsumer;
import com.atlassian.jira.cluster.ClusterMessagingService;
import com.atlassian.jira.cluster.MessageHandlerService;
import com.atlassian.jira.plugin.freeze.FreezeFileManager;
import com.atlassian.jira.plugin.freeze.FreezeFileManagerFactory;
import com.atlassian.jira.plugin.freeze.FreezeFileScannerFactory;
import com.atlassian.jira.plugin.freeze.SwitchingScanner;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.loaders.PluginLoader;
import com.atlassian.plugin.loaders.ScanningPluginLoader;
import com.atlassian.plugin.loaders.classloading.Scanner;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager.CLUSTER_UPGRADE_STATE_CHANGED;

/**
 * Creates a plugin loader which can freeze the list of loaded plugins - effectively stopping it from loading new plugins
 * put into the <code>installed-plugins</code> directory. How this works is defined in the {@link FreezeFileManager}.
 * Nodes that are in a cluster which is not in {@link UpgradeState#STABLE} state and have the same build number as the
 * cluster should freeze their plugins.
 * <p></p>
 * Specifically, it should create a freeze file when:
 * <ul>
 *     <li>It caused the transition from {@link UpgradeState#STABLE} to {@link UpgradeState#READY_TO_UPGRADE}</li>
 * </ul>
 * and similarly, it should delete the freeze file when:
 * <ul>
 *     <li>It caused the transition from {@link UpgradeState#RUNNING_UPGRADE_TASKS} to {@link UpgradeState#STABLE}</li>
 * </ul>
 * <p></p>
 * Additionally, a node should use the freeze (but not create it) for plugin loading when:
 * <ul>
 *     <li>
 *         It starts up and joins a cluster that is not in {@link UpgradeState#STABLE} state and its application build number
 *         is equal to the cluster build number.
 *     </li>
 *     <li>
 *         It receives a cluster message that the cluster upgrade state is now not {@link UpgradeState#STABLE} and
 *         its application build number is equal to the cluster build number.
 *     </li>
 * </ul>
 * @since v7.3
 * @see DefaultClusterUpgradeStateManager
 * @see FreezeFileManager
 * @see UpgradeState
 */
public class DefaultClusterUpgradePluginLoaderFactory implements ClusterUpgradePluginLoaderFactory {

    private final EventListenerRegistrar eventListenerRegistrar;
    private final ClusterUpgradeStateManager clusterUpgradeStateManager;
    private final ClusterMessagingService messageHandlerService;
    private final BuildUtilsInfo buildUtilsInfo;
    private final PluginEventManager pluginEventManager;
    private final FreezeFileScannerFactory freezeFileScannerFactory;
    private final FreezeFileManagerFactory freezeFileManagerFactory;
    private final NodeBuildInfoFactory nodeBuildInfoFactory;
    // this is necessary so the message consumer doesn't get garbage collected
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    private final Map<FreezeFileManager, ClusterMessageConsumer> messageConsumers = new HashMap<>();

    public DefaultClusterUpgradePluginLoaderFactory(final EventListenerRegistrar eventListenerRegistrar,
                                                    final ClusterUpgradeStateManager clusterUpgradeStateManager,
                                                    final MessageHandlerService messageHandlerService,
                                                    final BuildUtilsInfo buildUtilsInfo,
                                                    final PluginEventManager pluginEventManager,
                                                    final FreezeFileScannerFactory freezeFileScannerFactory,
                                                    final FreezeFileManagerFactory freezeFileManagerFactory,
                                                    final NodeBuildInfoFactory nodeBuildInfoFactory) {
        this.eventListenerRegistrar = eventListenerRegistrar;
        this.clusterUpgradeStateManager = clusterUpgradeStateManager;
        this.messageHandlerService = messageHandlerService;
        this.buildUtilsInfo = buildUtilsInfo;
        this.pluginEventManager = pluginEventManager;
        this.freezeFileScannerFactory = freezeFileScannerFactory;
        this.freezeFileManagerFactory = freezeFileManagerFactory;
        this.nodeBuildInfoFactory = nodeBuildInfoFactory;
    }

    @Override
    public PluginLoader create(final Scanner installedPluginsScanner, final File installedPluginsDirectory, final File freezeFile, final List<PluginFactory> pluginFactories) {
        Scanner freezeFileScanner = freezeFileScannerFactory.createScanner(freezeFile);
        SwitchingScanner switchingScanner = new SwitchingScanner(installedPluginsScanner, freezeFileScanner);
        FreezeFileManager freezer = freezeFileManagerFactory.create(freezeFile, installedPluginsDirectory);

        switchingScanner.switchTo(shouldUseFreezeFile());

        // Plugin event handling which will handle freezing and unfreezing
        eventListenerRegistrar.register(new ClusterUpgradeLifecycleListener(() -> {
            freezer.freeze();
            switchingScanner.switchTo(true);
        }, () -> {
            freezer.unfreeze();
            switchingScanner.switchTo(false);
        }, () -> {
            freezer.removeUnfrozenPlugins();
            freezer.unfreeze();
            switchingScanner.switchTo(false);
        }));

        // Cluster Message handling which synchronizes usage of the freeze file across nodes
        // Note: the creation of the freeze file only happens via plugin events on the node
        //       which handled the REST call
        ClusterMessageConsumer clusterMessageConsumer = (channel, message, senderId) -> {
            switchingScanner.switchTo(shouldUseFreezeFile());
        };
        messageConsumers.put(freezer, clusterMessageConsumer);
        messageHandlerService.registerListener(CLUSTER_UPGRADE_STATE_CHANGED, clusterMessageConsumer);

        return new ScanningPluginLoader(switchingScanner, pluginFactories, pluginEventManager);
    }


    /**
     * @return when the upgrade state is not {@link UpgradeState#STABLE} and the application build number is the same
     *         as the cluster build number, it means that this is an old node starting up in the cluster and should use
     *         the frozen list of plugins in the freeze file
     */
    private boolean shouldUseFreezeFile() {
        NodeBuildInfo clusterBuildNumber = clusterUpgradeStateManager.getClusterBuildInfo();
        NodeBuildInfo applicationBuildNumber = nodeBuildInfoFactory.currentApplicationInfo();

        return UpgradeState.STABLE != clusterUpgradeStateManager.getUpgradeState()
                && applicationBuildNumber.equals(clusterBuildNumber);
    }
}
