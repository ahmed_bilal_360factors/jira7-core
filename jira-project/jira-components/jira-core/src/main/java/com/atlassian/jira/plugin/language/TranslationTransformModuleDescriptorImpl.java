package com.atlassian.jira.plugin.language;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.plugin.AbstractJiraModuleDescriptor;
import com.atlassian.jira.plugin.util.ModuleDescriptorXMLUtils;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.validation.ValidationException;
import org.dom4j.Element;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Implementation of {@link TranslationTransformModuleDescriptor}.
 *
 * @since v5.1
 */
public class TranslationTransformModuleDescriptorImpl extends AbstractJiraModuleDescriptor<TranslationTransform> implements TranslationTransformModuleDescriptor {
    private int order;
    private final JiraProperties jiraProperties;
    private final List<String> exceptionsInOnDemand = Arrays.asList(
            "com.atlassian.greenhopper.web.util.AgileToSoftwareTranslationTransform",
            "com.atlassian.jira.dev.i18n.DisableTranslation"
    );

    public TranslationTransformModuleDescriptorImpl(final JiraAuthenticationContext authenticationContext, final ModuleFactory moduleFactory,
            final JiraProperties jiraProperties) {
        super(authenticationContext, moduleFactory);
        this.jiraProperties = jiraProperties;
    }

    public void init(Plugin plugin, Element element) throws PluginParseException {
        super.init(plugin, element);
        order = ModuleDescriptorXMLUtils.getOrder(element);
    }

    @Override
    public void enabled() {
        super.enabled();
        assertModuleClassImplements(TranslationTransform.class);
    }

    @Override
    public void disabled() {
        super.disabled();
    }

    @Override
    public int getOrder() {
        return order;
    }
}
