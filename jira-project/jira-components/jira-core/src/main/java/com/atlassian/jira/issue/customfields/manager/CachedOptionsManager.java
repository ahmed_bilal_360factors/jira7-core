package com.atlassian.jira.issue.customfields.manager;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.cache.CachedReference;
import com.atlassian.cache.Supplier;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigManager;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.util.CaseFolding;
import com.atlassian.jira.util.map.CacheObject;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Cache for Options Manager. Not particularly neat More a problem with how the OptionsManager is used really
 */
@EventComponent
public class CachedOptionsManager extends DefaultOptionsManager {
    private final CachedReference<List<Option>> allCache;
    private final Cache<Long, CacheObject<Options>> optionsCache;
    private final Cache<Long, CacheObject<Option>> optionCache;
    private final Cache<Long, List<Option>> parentCache;
    private final Cache<String, List<Option>> valueCache;

    public CachedOptionsManager(final OfBizDelegator delegator, final FieldConfigManager fieldConfigManager, final CacheManager cacheManager) {
        super(delegator, fieldConfigManager);
        allCache = cacheManager.getCachedReference(CachedOptionsManager.class, "allCache", new AllOptionsSupplier());

        optionsCache = cacheManager.getCache(CachedOptionsManager.class.getName() + ".optionsCache",
                new OptionsCacheLoader(),
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).build());

        optionCache = cacheManager.getCache(CachedOptionsManager.class.getName() + ".optionCache",
                new OptionCacheLoader(),
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).build());

        parentCache = cacheManager.getCache(CachedOptionsManager.class.getName() + ".parentCache",
                new ParentCacheLoader(),
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).build());

        valueCache = cacheManager.getCache(CachedOptionsManager.class.getName() + ".valueCache",
                new ValueCacheLoader(),
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).build());

        init();
    }

    @EventListener
    public void onClearCache(@SuppressWarnings("unused") final ClearCacheEvent event) {
        init();
    }

    private void init() {
        optionsCache.removeAll();
        optionCache.removeAll();
        parentCache.removeAll();
        valueCache.removeAll();
        allCache.reset();
    }

    @Override
    public List<Option> getAllOptions() {
        return allCache.get();
    }

    @Override
    public List<Option> findByOptionValue(String value) {
        if (value == null) {
            return null;
        }

        // make the cache lookup case insensitive too
        value = CaseFolding.foldString(value);

        return valueCache.get(value);
    }

    @Override
    public Options getOptions(final FieldConfig fieldConfig) {
        final Long key = (fieldConfig == null) ? null : fieldConfig.getId();
        if (key == null) {
            // we can only cache something, not nothing
            return super.getOptions(fieldConfig);
        }
        return optionsCache.get(fieldConfig.getId()).getValue();
    }

    @Override
    public void setRootOptions(final FieldConfig fieldConfig, final Options options) {
        super.setRootOptions(fieldConfig, options);

        init();
    }

    @Override
    public void removeCustomFieldOptions(final CustomField customField) {
        super.removeCustomFieldOptions(customField);

        init();
    }

    @Override
    public void removeCustomFieldConfigOptions(final FieldConfig fieldConfig) {
        super.removeCustomFieldConfigOptions(fieldConfig);

        // Nuke it all if a custom field is removed
        init();
    }

    @Override
    public void updateOptions(final Collection<Option> options) {
        super.updateOptions(options);
        for (final Option option : options) {
            removeOptionFromCaches(option);
        }
        optionsCache.removeAll();
        allCache.reset();
    }

    @Override
    public Option createOption(final FieldConfig fieldConfig, final Long parentOptionId, final Long sequence, final String value) {
        final Option option = super.createOption(fieldConfig, parentOptionId, sequence, value);
        removeOptionFromCaches(option);
        optionsCache.removeAll();
        allCache.reset();
        return option;
    }

    @Override
    public List<Option> createOptions(final FieldConfig config, final Long parentOptionId, final Long sequence, final Iterable<String> options) {
        final List<Option> result = super.createOptions(config, parentOptionId, sequence, options);
        optionsCache.removeAll();
        allCache.reset();
        return result;
    }

    @Override
    public void deleteOptionAndChildren(final Option option) {
        super.deleteOptionAndChildren(option);

        init();
    }

    @Override
    public void setValue(final Option option, final String value) {
        final String oldValue = option.getValue();
        super.setValue(option, value);
        removeOptionFromCaches(option);
        if (option.getValue() != null) {
            valueCache.remove(CaseFolding.foldString(oldValue));
        }
        if (option.getValue() != null) {
            valueCache.remove(CaseFolding.foldString(value));
        }
        optionsCache.removeAll();
        allCache.reset();
    }

    @Override
    public void disableOption(final Option option) {
        super.disableOption(option);
        removeOptionFromCaches(option);
        optionsCache.removeAll();
        allCache.reset();
    }

    @Override
    public void enableOption(final Option option) {
        super.enableOption(option);
        removeOptionFromCaches(option);
        optionsCache.removeAll();
        allCache.reset();
    }

    private void removeOptionFromCaches(final Option option) {
        optionCache.remove(option.getOptionId());
        if (option.getValue() != null) {
            valueCache.remove(CaseFolding.foldString(option.getValue()));
        }
        if (option.getParentOption() != null) {
            parentCache.remove(option.getParentOption().getOptionId());
        }
    }

    @Override
    public Option findByOptionId(final Long optionId) {
        if (optionId == null) {
            return null;
        }
        return optionCache.get(optionId).getValue();
    }


    @Override
    public List<Option> findByParentId(final Long parentOptionId) {
        return parentCache.get(parentOptionId);
    }

    private class OptionsCacheLoader implements CacheLoader<Long, CacheObject<Options>> {
        @Nonnull
        @Override
        public CacheObject<Options> load(@Nonnull final Long fieldConfigId) {
            final FieldConfig fieldConfig = fieldConfigManager.getFieldConfig(fieldConfigId);
            return CacheObject.wrap(CachedOptionsManager.super.getOptions(fieldConfig));
        }
    }

    private class AllOptionsSupplier implements Supplier<List<Option>> {
        @Override
        public List<Option> get() {
            return ImmutableList.copyOf(CachedOptionsManager.super.getAllOptions());
        }
    }

    private class OptionCacheLoader implements CacheLoader<Long, CacheObject<Option>> {
        @Nonnull
        @Override
        public CacheObject<Option> load(@Nonnull final Long id) {
            return CacheObject.wrap(CachedOptionsManager.super.findByOptionId(id));
        }
    }

    private class ParentCacheLoader implements CacheLoader<Long, List<Option>> {
        @Nonnull
        @Override
        public List<Option> load(@Nonnull final Long id) {
            return ImmutableList.copyOf(CachedOptionsManager.super.findByParentId(id));
        }
    }

    private class ValueCacheLoader implements CacheLoader<String, List<Option>> {
        @Nonnull
        @Override
        public List<Option> load(@Nonnull final String value) {
            return ImmutableList.copyOf(CachedOptionsManager.super.findByOptionValue(value));
        }
    }
}
