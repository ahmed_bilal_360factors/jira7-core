package com.atlassian.jira.cluster.zdu.analytics;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager;
import com.atlassian.jira.cluster.zdu.JiraUpgradeApprovedEvent;
import com.atlassian.jira.cluster.zdu.JiraUpgradeCancelledEvent;
import com.atlassian.jira.cluster.zdu.JiraUpgradeFinishedEvent;
import com.atlassian.jira.cluster.zdu.JiraUpgradeStartedEvent;
import com.atlassian.jira.service.services.analytics.start.JiraStartAnalyticEvent;
import com.atlassian.jira.service.services.analytics.stop.JiraStopAnalyticEvent;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.google.common.hash.Hashing;
import java.nio.charset.StandardCharsets;
import java.util.UUID;
import javax.annotation.Nonnull;
import javax.xml.bind.DatatypeConverter;

/**
 * @since v7.3
 */
@EventComponent
public class ClusterUpgradeAnalyticsListener {
    private static final int HASH_ITERATIONS = 1000;

    private static final String SALT_WORD = new String(DatatypeConverter.parseBase64Binary("" +
            "bN4KQnRrdFv7+Sa+6Ua/vVIKBlw2ki3bp6VlXMd/7KBTxMMaOgjAgXXhOpRQGPqg\n" +
            "O14FsD/KGw1Ia7TF5o3Y+gCoeZG18eCb/VBL4u5bDFsmGIBJUBU0Yoo5Xm0oua7e\n" +
            "/yr0wh0Ev1ttPH14CXvl5z4l+UIdKd/irTijLQXcEtdRA/EQRqMvSO1y/j5oEVCo\n" +
            "jekHm1xin0gKOTM7dXXVL5/WwflvmG/YxKhZnKm8hCZHqwrJXsQWV85Kf7ekbTYw\n" +
            "CjF9W4ftyypbVlq5TZj92W/M1RpNqKoDpI+7yCCJhuHfhFHH16fK7vhud9DMYIXS\n" +
            "cgJZKredT9Z2IJyT/b5qjU67X0W23W9PblIzeaGATY3ccpdL7PrulDd7Tl3XI1CR\n" +
            "i1N9CnNi42o/ZSo0ZxJcGvDlvg2VTcLJIXDJ/EKX+rLdwMiUMVw/RyhGu4FFhg+T\n" +
            "W6CFMH8MFHmI2Ny9Ue7YI/QeD1OvZIVRH+pys6BqHp1ovf7sZdSHEMDU1+PHdf38\n" +
            "XrPhtkATJ/YSM8nT7DfuiGRQwfSUs6J5WOZZ2BhTzB9wYJSiiRGCrTpOAZ5EARqz\n" +
            "zzNBApT2mLa92RM4VoTlG1CudnTYLl4av2xGWjtZM0z6B+pzI7BzS8o1850I85+Q\n" +
            "dbMaeDt6An97VH/xU8u/cfErwXO7HfP3GTKB2VxtyXU="));

    private final EventPublisher eventPublisher;
    private final ClusterUpgradeStateManager clusterUpgradeStateManager;
    private final ClusterManager clusterManager;
    private final BuildUtilsInfo buildUtilsInfo;

    public ClusterUpgradeAnalyticsListener(EventPublisher eventPublisher,
                                           ClusterUpgradeStateManager clusterUpgradeStateManager,
                                           ClusterManager clusterManager,
                                           BuildUtilsInfo buildUtilsInfo) {
        this.eventPublisher = eventPublisher;
        this.clusterUpgradeStateManager = clusterUpgradeStateManager;
        this.clusterManager = clusterManager;
        this.buildUtilsInfo = buildUtilsInfo;
    }

    @EventListener
    public void onNodeStarted(JiraStartAnalyticEvent event) {
        final String nodeId = clusterManager.getNodeId();
        if (nodeId == null) {
            return;
        }

        NodeStartedAnalytics analytics = new NodeStartedAnalytics(
                hash(nodeId),
                buildUtilsInfo.getDatabaseBuildNumber(),
                clusterUpgradeStateManager.getClusterBuildInfo().getBuildNumber(),
                clusterUpgradeStateManager.getUpgradeState().name());

        eventPublisher.publish(analytics);
    }

    @EventListener
    public void onNodeStopped(JiraStopAnalyticEvent event) {
        final String nodeId = clusterManager.getNodeId();
        if (nodeId == null) {
            return;
        }

        NodeStoppedAnalytics analytics = new NodeStoppedAnalytics(
                hash(nodeId),
                buildUtilsInfo.getDatabaseBuildNumber(),
                clusterUpgradeStateManager.getClusterBuildInfo().getBuildNumber(),
                clusterUpgradeStateManager.getUpgradeState().name());

        eventPublisher.publish(analytics);
    }

    @EventListener
    public void onUpgradeStarted(JiraUpgradeStartedEvent event) {
        UpgradeStartedAnalytics analytics = new UpgradeStartedAnalytics(
                clusterManager.findLiveNodes().size(),
                clusterUpgradeStateManager.getClusterBuildInfo().getBuildNumber());

        eventPublisher.publish(analytics);
    }

    @EventListener
    public void onUpgradeApproved(JiraUpgradeApprovedEvent event) {
        UpgradeApprovedAnalytics analytics = new UpgradeApprovedAnalytics(
                clusterManager.findLiveNodes().size(),
                event.getFromVersion().getBuildNumber(),
                event.getToVersion().getBuildNumber());

        eventPublisher.publish(analytics);
    }

    @EventListener
    public void onUpgradeFinished(JiraUpgradeFinishedEvent event) {
        UpgradeCompletedAnalytics analytics = new UpgradeCompletedAnalytics(
                clusterManager.findLiveNodes().size(),
                event.getFromVersion().getBuildNumber(),
                event.getToVersion().getBuildNumber());

        eventPublisher.publish(analytics);
    }

    @EventListener
    public void onUpgradeCancelled(JiraUpgradeCancelledEvent event) {
        UpgradeCancelledAnalytics analytics = new UpgradeCancelledAnalytics(
                clusterManager.findLiveNodes().size(),
                event.getNodeBuildInfo().getBuildNumber(),
                buildUtilsInfo.getDatabaseBuildNumber());

        eventPublisher.publish(analytics);
    }

    private static String hash(@Nonnull String value) {
        String hash = value + SALT_WORD;
        for (int i = 0; i < HASH_ITERATIONS; i++) {
            hash = Hashing.sha256().hashString(hash, StandardCharsets.UTF_8).toString();
        }
        // The analytics filter is picky about what strings it lets through - UUIDs are OK, but the hash by itself will
        // get dropped. {@link UUID#nameUUIDFromBytes} will actually do its own hashing (single pass through md5), but
        // this is safer.
        return UUID.nameUUIDFromBytes(hash.getBytes()).toString();
    }
}
