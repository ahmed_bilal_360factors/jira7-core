package com.atlassian.jira.workflow.tabs;

import com.atlassian.fugue.Option;
import com.atlassian.jira.plugin.webfragment.DefaultWebFragmentContext;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.jira.web.action.admin.workflow.tabs.WorkflowTransitionContextUtils;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.ozymandias.SafePluginPointAccess;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.opensymphony.workflow.loader.ActionDescriptor;

import javax.annotation.Nullable;
import java.util.Map;

import static com.atlassian.fugue.Iterables.findFirst;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.notNull;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Maps.newHashMap;
import static java.util.Collections.emptyList;

public class WebPanelWorkflowTransitionTabProvider implements WorkflowTransitionTabProvider {

    private static final String TAB_PANELS_LOCATION = "workflow.transition.tabs";

    private final WebInterfaceManager webInterfaceManager;

    public WebPanelWorkflowTransitionTabProvider(final WebInterfaceManager webInterfaceManager) {
        this.webInterfaceManager = checkNotNull(webInterfaceManager);
    }

    @Override
    public Iterable<WorkflowTransitionTab> getTabs(final ActionDescriptor action, final JiraWorkflow workflow) {
        final Map<String, Object> context = createContext(action, workflow);
        final Iterable<WebPanelModuleDescriptor> panels = getDisplayableWebPanelDescriptors(context);
        return copyOf(filter(transform(panels, input -> SafePluginPointAccess.call(() -> {
            final String label = input.getWebLabel().getDisplayableLabel(ExecutingHttpRequest.get(), context);
            final String count = input.getWebParams().getRenderedParam(WorkflowTransitionContextUtils.COUNT_KEY, newHashMap(context)); // mutates the context
            return new WorkflowTransitionTab(label, count, input);
        }).getOrNull()), notNull()));
    }

    private Map<String, Object> createContext(ActionDescriptor action, JiraWorkflow workflow) {
        final ImmutableMap.Builder<String, Object> context = ImmutableMap.<String, Object>builder()
                .putAll(DefaultWebFragmentContext.get(TAB_PANELS_LOCATION));
        if (action != null) {
            context.put(WorkflowTransitionContextUtils.TRANSITION_KEY, action);
        }
        if (workflow != null) {
            context.put(WorkflowTransitionContextUtils.WORKFLOW_KEY, workflow);
        }

        return context.build();
    }

    @Override
    @Nullable
    public String getTabContentHtml(final String panelKey, final ActionDescriptor action, final JiraWorkflow workflow) {
        final Map<String, Object> context = createContext(action, workflow);
        final Iterable<WebPanelModuleDescriptor> panels = getDisplayableWebPanelDescriptors(context);
        final Option<WebPanelModuleDescriptor> tabDescriptor =
                findFirst(panels, input -> Objects.equal(input.getKey(), panelKey));

        return Iterables.getFirst(SafePluginPointAccess.to().descriptors(tabDescriptor,
                (moduleDescriptor, module) -> {
                    return moduleDescriptor.getModule().getHtml(context);
                }
        ), null);
    }

    private Iterable<WebPanelModuleDescriptor> getDisplayableWebPanelDescriptors(final Map<String, Object> context) {
        return filter(SafePluginPointAccess.call(() ->
                webInterfaceManager.getDisplayableWebPanelDescriptors(TAB_PANELS_LOCATION, context))
            .getOrElse(emptyList()), notNull());
    }
}
