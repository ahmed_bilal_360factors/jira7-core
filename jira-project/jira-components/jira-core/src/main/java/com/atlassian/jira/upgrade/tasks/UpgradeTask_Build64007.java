package com.atlassian.jira.upgrade.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Remove old indexes from jiraaction and changegroup table that are replaced with better performing ones.
 *
 * @since v6.3.12 (as 6343)
 */
public class UpgradeTask_Build64007 extends DropIndexTask {
    private static final Logger LOG = LoggerFactory.getLogger(UpgradeTask_Build64007.class);

    public UpgradeTask_Build64007() {
        super();
    }

    @Override
    public int getBuildNumber() {
        return 64007;
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        // Flagged as not downgradable because it was created before the Downgrade Task framework was built.
        return true;
    }

    private void dropIndex(final Connection connection, final String tableName, final String indexName) {
        try {
            final String sql = buildDropIndexSql(tableName, indexName);
            final Statement update = connection.createStatement();
            try {
                update.execute(sql);
            } finally {
                update.close();
            }
        } catch (SQLException sqle) {
            LOG.debug("Ignoring error dropping old index", sqle);
        }
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception {
        if (setupMode) {
            // There is no need to do this on a clean install
            return;
        }

        final Connection connection = getDatabaseConnection();
        try {
            dropIndex(connection, "jiraaction", "action_author");
            dropIndex(connection, "changegroup", "chggroup_author");
        } finally {
            connection.close();
        }
    }

    @Override
    public String getShortDescription() {
        return "Remove old indexes from jiraaction and changegroup table";
    }
}
