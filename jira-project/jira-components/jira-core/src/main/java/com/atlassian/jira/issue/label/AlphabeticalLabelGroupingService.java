package com.atlassian.jira.issue.label;

import com.atlassian.jira.user.ApplicationUser;

/**
 * Provides an alphabetically grouped collection of a user's labels
 *
 * @since v7.0
 */
public interface AlphabeticalLabelGroupingService {
    /**
     * Retrieve a collection of a project's labels grouped into alphabetical blocks
     *
     * @param user      A user of JIRA
     * @param projectId The id of a project
     * @param fieldId   The id of a field
     * @return An instance of {@link AlphabeticalLabelGroupingSupport} that has the project's unique labels grouped
     * together alphabetically
     * @since v7.0
     */
    AlphabeticalLabelGroupingSupport getAlphabeticallyGroupedLabels(ApplicationUser user, Long projectId, String fieldId);
}
