package com.atlassian.jira.cluster.zdu;

/**
 * @since v7.3
 */
public interface ClusterUpgradeStateManager extends ClusterStateManager {
    String CLUSTER_UPGRADE_STATE_DARK_FEATURE = "jira.zdu.cluster-upgrade-state";
    String CLUSTER_UPGRADE_STATE_CHANGED = "Upgrade State";

    void runUpgrade();

    void startUpgrade();

    void cancelUpgrade();

    void approveUpgrade();

    NodeBuildInfo getClusterBuildInfo();

    /**
     * Checks if delayed upgrades are handled by the cluster.
     *
     * @return true if and only if it is cluster's responsibility to execute upgrade tasks
     */
    boolean areDelayedUpgradesHandledByCluster();
}
