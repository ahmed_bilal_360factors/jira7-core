package com.atlassian.jira.imports.project.transformer;

import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.external.beans.ExternalEntityProperty;
import com.atlassian.jira.imports.project.core.EntityRepresentation;
import com.atlassian.jira.imports.project.core.EntityRepresentationImpl;
import com.google.common.collect.ImmutableMap;

import static com.atlassian.jira.imports.project.parser.EntityPropertyParser.ENTITY_PROPERTY_ENTITY_NAME;

/**
 * @since v7.1
 */
public class EntityPropertyTransformerImpl implements EntityPropertyTransformer {

    @Override
    public EntityRepresentation getEntityRepresentation(final ExternalEntityProperty entityProperty, final Long newEntityId) {
        final ImmutableMap.Builder<String, String> attrBuilder = ImmutableMap.builder();
        //We don't need to pass original id of property because it doesn't have any meaning.
        //We pass null, and system will create a new id while persisting
        attrBuilder.put(EntityProperty.ENTITY_NAME, entityProperty.getEntityName());
        attrBuilder.put(EntityProperty.ENTITY_ID, newEntityId.toString());
        attrBuilder.put(EntityProperty.KEY, entityProperty.getKey());
        attrBuilder.put(EntityProperty.VALUE, entityProperty.getValue());
        attrBuilder.put(EntityProperty.CREATED, entityProperty.getCreated().toString());
        attrBuilder.put(EntityProperty.UPDATED, entityProperty.getUpdated().toString());

        return new EntityRepresentationImpl(ENTITY_PROPERTY_ENTITY_NAME, attrBuilder.build());
    }
}
