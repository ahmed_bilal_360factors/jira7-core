package com.atlassian.jira.issue.status;

import com.atlassian.jira.config.DefaultConstantsManager;

/**
 * Published when {@link DefaultConstantsManager#refreshStatuses()} is called.
 *
 * @since v6.5
 */
public class ClearStatusCacheEvent {
    public static final ClearStatusCacheEvent INSTANCE = new ClearStatusCacheEvent();

    private ClearStatusCacheEvent() {
    }
}
