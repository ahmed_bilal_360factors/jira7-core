package com.atlassian.jira.license;

import com.atlassian.annotations.Internal;
import com.atlassian.core.util.Clock;
import com.atlassian.extras.api.AtlassianLicense;
import com.atlassian.extras.api.LicenseEdition;
import com.atlassian.extras.api.LicenseException;
import com.atlassian.extras.api.LicenseManager;
import com.atlassian.extras.api.Product;
import com.atlassian.extras.api.ProductLicense;
import com.atlassian.extras.api.jira.JiraLicense;
import com.atlassian.extras.common.util.LicenseProperties;
import com.atlassian.extras.core.AtlassianLicenseFactory;
import com.atlassian.extras.core.DefaultAtlassianLicenseFactory;
import com.atlassian.extras.core.DefaultLicenseManager;
import com.atlassian.extras.core.DefaultProductLicense;
import com.atlassian.extras.core.ProductLicenseFactory;
import com.atlassian.extras.core.jira.JiraProductLicenseFactory;
import com.atlassian.extras.decoder.api.DelegatingLicenseDecoder;
import com.atlassian.extras.decoder.api.LicenseDecoder;
import com.atlassian.extras.decoder.v1.Version1LicenseDecoder;
import com.atlassian.extras.decoder.v2.Version2LicenseDecoder;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.bean.I18nBean;
import com.atlassian.jira.web.util.ExternalLinkUtil;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import java.util.Map;

import static com.atlassian.extras.common.LicensePropertiesConstants.ACTIVE_FLAG;
import static com.atlassian.extras.common.LicensePropertiesConstants.ACTIVE_VALUE;
import static com.atlassian.extras.common.LicensePropertiesConstants.NAMESPACE_SEPARATOR;
import static com.atlassian.jira.application.ApplicationKeys.SERVICE_DESK;
import static com.atlassian.jira.license.DefaultLicenseDetails.isEnterpriseSubscriptionLicense;
import static com.atlassian.jira.license.DefaultLicensedApplications.JIRA_PRODUCT_NAMESPACE;


public class LicenseDetailsFactoryImpl implements LicenseDetailsFactory {
    private final ApplicationProperties applicationProperties;
    private final ExternalLinkUtil externalLinkUtil;
    private final BuildUtilsInfo buildUtilsInfo;
    private final I18nHelper.BeanFactory i18nFactory;
    private final DateTimeFormatter dateTimeFormatter;
    private final Clock clock;

    public LicenseDetailsFactoryImpl(
            ApplicationProperties applicationProperties,
            ExternalLinkUtil externalLinkUtil,
            BuildUtilsInfo buildUtilsInfo,
            I18nBean.BeanFactory i18Factory,
            DateTimeFormatter dateTimeFormatter,
            Clock clock) {
        this.applicationProperties = applicationProperties;
        this.externalLinkUtil = externalLinkUtil;
        this.buildUtilsInfo = buildUtilsInfo;
        this.i18nFactory = i18Factory;
        this.dateTimeFormatter = dateTimeFormatter;
        this.clock = clock;
    }

    @Override
    @Nonnull
    public LicenseDetails getLicense(@Nonnull String licenseString) throws LicenseException {
        JiraProductLicense jiraLicense = JiraProductLicenseManager.INSTANCE.getProductLicense(licenseString);

        if (isEnterpriseSubscriptionLicense(jiraLicense)) {
            return new SubscriptionLicenseDetails(
                    jiraLicense, licenseString, applicationProperties, externalLinkUtil,
                    buildUtilsInfo, i18nFactory, dateTimeFormatter, clock);
        } else {
            return new DefaultLicenseDetails(
                    jiraLicense, licenseString, applicationProperties, externalLinkUtil,
                    buildUtilsInfo, i18nFactory, dateTimeFormatter, clock);
        }
    }

    @Override
    public boolean isDecodeable(@Nonnull String licenseString) {
        try {
            final JiraProductLicense productLicense = JiraProductLicenseManager.INSTANCE.getProductLicense(licenseString);
            return !productLicense.getApplications().getKeys().isEmpty();
        } catch (LicenseException e) {
            return false;
        }
    }

    /**
     * JIRA internal license manager used for decoding license keys.
     * Do not use this API outside of jira core module.
     */
    @Internal
    public static class JiraProductLicenseManager {
        public static final JiraProductLicenseManager INSTANCE = new JiraProductLicenseManager();

        //The order of this map is important. The licenses will be searched for in entry order.
        private final ImmutableMap<Product, ProductLicenseFactory> factories;
        private final LicenseManager licenseManager;
        private final LicenseDecoder licenseDecoder;

        private JiraProductLicenseManager() {
            final ImmutableMap<Product, ProductLicenseFactory> factories = getCompatibleJiraProductFactories();
            final AtlassianLicenseFactory atlassianLicenseFactory = new DefaultAtlassianLicenseFactory(factories);

            this.factories = factories;
            this.licenseDecoder = new DelegatingLicenseDecoder(
                    ImmutableList.<LicenseDecoder>of(new Version2LicenseDecoder(), new Version1LicenseDecoder()));
            this.licenseManager = new DefaultLicenseManager(licenseDecoder, atlassianLicenseFactory);
        }

        /**
         * Decode a valid license key into {@link com.atlassian.jira.license.JiraProductLicense}. If the
         * provided license key is an undecodable or not a JIRA Product license key then a {@link
         * com.atlassian.extras.api.LicenseException} is thrown.
         *
         * @param licenseKey license key for a JIRA product.
         * @return a decoded {@link com.atlassian.jira.license.JiraProductLicense}.
         * @throws LicenseException if there are any errors in handling the license.
         */
        @Internal
        @Nonnull
        public JiraProductLicense getProductLicense(@Nonnull String licenseKey) throws LicenseException {
            if (StringUtils.isBlank(licenseKey)) {
                throw new LicenseException("Null or blank license");
            }

            final AtlassianLicense atlassianLicense = licenseManager.getLicense(licenseKey);
            for (final Product product : factories.keySet()) {
                final ProductLicense productLicense = atlassianLicense.getProductLicense(product);
                if (productLicense instanceof JiraLicense) {
                    JiraLicense jiraLicense = (JiraLicense) productLicense;
                    DefaultLicensedApplications licensedApplications = new DefaultLicensedApplications(
                            licenseKey, licenseDecoder);

                    if (!licensedApplications.getKeys().isEmpty()) {
                        return new JiraProductLicense(licensedApplications, jiraLicense);
                    }
                }
            }

            throw new LicenseException("Invalid license key. Please contact Atlassian Support.");
        }

        private static ImmutableMap<Product, ProductLicenseFactory> getCompatibleJiraProductFactories() {
            return ImmutableMap.<Product, ProductLicenseFactory>of(Product.JIRA, new JiraProductLicenseFactory(),
                    JiraApplicationLicenseFactory.PRODUCT, new JiraApplicationLicenseFactory(),
                    ServiceDeskPluginLicenseFactory.PRODUCT, new ServiceDeskPluginLicenseFactory());
        }
    }

    private static class JiraApplicationLicenseFactory extends JiraProductLicenseFactory {
        private static final Product PRODUCT = new Product("JIRA Apps", "jira");

        @Override
        public ProductLicense getLicenseInternal(final Product product, final LicenseProperties licenseProperties) {
            if (hasLicense(product, licenseProperties)) {
                return new JiraApplicationLicense(product, licenseProperties);
            } else {
                throw new LicenseException("Could not create JIRA license for " + product);
            }
        }

        @Override
        public boolean hasLicense(final Product product, final LicenseProperties licenseProperties) {
            final Map<String, String> propertiesWithActiveFlag = licenseProperties.getPropertiesEndingWith(ACTIVE_FLAG);
            for (final Map.Entry<String, String> entry : propertiesWithActiveFlag.entrySet()) {
                if (entry.getKey().startsWith(JIRA_PRODUCT_NAMESPACE)) {
                    if (entry.getValue().equals(ACTIVE_VALUE)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    /**
     * Parses the Service Desk (SD) plugin licenses as JIRA licenses. This will parse the Tier Based Pricing
     * (TBP) licenses from SD 1.x or the Agent Based Pricing (ABP) licenses from SD 2.x.
     *
     * @see {@link com.atlassian.jira.license.ServiceDeskLicenseConstants} for more details about ABP and TBP licenses.
     */
    private static class ServiceDeskPluginLicenseFactory extends JiraProductLicenseFactory {
        /**
         * The Role Based Pricing (RBP) SD flag in the license. This indicates a JIRA SD 7.x license.
         */
        private static final String ACTIVE_SD70 = JIRA_PRODUCT_NAMESPACE + SERVICE_DESK + NAMESPACE_SEPARATOR + ACTIVE_FLAG;

        /**
         * The product for JIRA Service Desk.
         */
        private static final Product PRODUCT = new Product("JIRA Service Desk (Legacy)",
                ServiceDeskLicenseConstants.SD_LEGACY_NAMESPACE, true);

        @Override
        public ProductLicense getLicenseInternal(final Product product, final LicenseProperties licenseProperties) {
            if (hasLicense(product, licenseProperties)) {
                return new JiraApplicationLicense(product, licenseProperties);
            } else {
                throw new LicenseException("Could not create JIRA license for " + product);
            }
        }

        @Override
        public boolean hasLicense(final Product product, final LicenseProperties licenseProperties) {
            //For compatibility, JIRA SD 7.x licenses will also have the old license properties contained. Thus,
            //we need to ignore the old properties if the new ones exist. A license is a TBP or an ADP
            //SD license if its not a JIRA SD 7.x license and it contains the SD plugin active flag.
            return !licenseProperties.getBoolean(ACTIVE_SD70)
                    && licenseProperties.getBoolean(ServiceDeskLicenseConstants.SD_LEGACY_ACTIVE);
        }
    }

    private static class JiraApplicationLicense extends DefaultProductLicense implements JiraLicense {
        private JiraApplicationLicense(final Product product, final LicenseProperties licenseProperties) {
            super(product, licenseProperties);
        }

        @Override
        public LicenseEdition getLicenseEdition() {
            return LicenseEdition.ENTERPRISE;
        }

        @Override
        public boolean isClusteringEnabled() {
            return DefaultLicenseDetails.isDataCenterLicense(this);
        }
    }
}
