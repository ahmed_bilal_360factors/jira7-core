package com.atlassian.jira.config.group;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;

import javax.annotation.Nonnull;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Components (typically data stores) that have groups as part of their data model implements this interface to allow
 * callers to verify whether a given group has been associated with any data. This is used to identify dormant group
 * configuration that could potentially be activated by the creation of a group with the same name.
 *
 * @since 7.0
 */
public interface GroupConfigurable {
    /**
     * Determine whether configuration exists for the specified {@link Group}.
     *
     * @param group that may or may not exist.
     * @return true if the group is used in the components configuration.
     */
    boolean isGroupUsed(@Nonnull final Group group);

    /**
     * Determine whether configuration exists for the specified groupName. This method convert the {@code String}
     * groupName to a {@link Group} using {@link ImmutableGroup} and then calls through to {@link #isGroupUsed(Group)}
     *
     * @param groupName name of the group that may or may not exist.
     * @return true if the group name is used in the components configuration.
     * @see #isGroupUsed(Group)
     */
    default boolean isGroupUsed(@Nonnull final String groupName) {
        notNull("groupName", groupName);
        return isGroupUsed(new ImmutableGroup(groupName));
    }
}
