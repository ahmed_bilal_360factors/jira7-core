package com.atlassian.jira.startup;

import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.ClusterServicesManager;
import com.atlassian.jira.cluster.monitoring.ClusterMonitoringBeansRegistrar;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.FeatureManager;
import com.google.common.annotations.VisibleForTesting;

import static com.atlassian.jira.cluster.monitoring.ClusterMonitoringBeansRegistrar.CLUSTER_MONITORING_DARK_FEATURE;

/**
 * Sets up clustered services as appropriate, starts upgrade services and scheduler as needed
 *
 * @since v6.1
 */
public class ClusteringLauncher implements JiraLauncher {
    private ClusterMonitoringBeansRegistrar clusterMonitoringBeansRegistrar;

    private boolean clusterMonitoringStarted = false;

    public ClusteringLauncher() {
        this(new ClusterMonitoringBeansRegistrar());
    }

    @VisibleForTesting
    ClusteringLauncher(ClusterMonitoringBeansRegistrar clusterMonitoringBeansRegistrar) {
        this.clusterMonitoringBeansRegistrar = clusterMonitoringBeansRegistrar;
    }

    @Override
    public void start() {
        final ClusterManager clusterManager = ComponentAccessor.getComponent(ClusterManager.class);
        if (clusterManager.isClustered()) {
            clusterManager.checkIndex();
        }

        final FeatureManager featureManager = ComponentAccessor.getComponent(FeatureManager.class);
        if (featureManager.isEnabled(CLUSTER_MONITORING_DARK_FEATURE)) {
            clusterMonitoringBeansRegistrar.registerClusterMonitoringMBeans();
            clusterMonitoringStarted = true;
        }
    }

    @Override
    public void stop() {
        final ClusterServicesManager clusterServicesManager = ComponentAccessor.getComponent(ClusterServicesManager.class);
        clusterServicesManager.stopServices();

        if (clusterMonitoringStarted) {
            clusterMonitoringBeansRegistrar.unregisterClusterMonitorMBeans();
            clusterMonitoringStarted = false;
        }
    }
}
