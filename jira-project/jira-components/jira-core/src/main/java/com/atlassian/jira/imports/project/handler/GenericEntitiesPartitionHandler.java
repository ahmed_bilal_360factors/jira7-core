package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.util.ProjectImportTemporaryFiles;
import org.ofbiz.core.entity.DelegatorInterface;
import org.ofbiz.core.entity.GenericDelegator;
import org.ofbiz.core.entity.GenericEntity;
import org.ofbiz.core.entity.model.ModelEntity;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Parses an XML import file and writes a smaller "partition" containing each entity type for non-issue related entities.
 *
 * @since v3.13
 */
public class GenericEntitiesPartitionHandler implements ImportOfBizEntityHandler {
    private final BackupProject backupProject;
    private final ProjectImportTemporaryFiles projectImportTemporaryFiles;
    private final Map<String, ModelEntity> modelEntityMap;
    private final GenericDelegator delegator;
    private String currentEntity;
    private PrintWriter printWriter;
    private int entityCount = 0;

    /**
     * @param backupProject               contains the issue id's that we are interested in partitioning.
     * @param projectImportTemporaryFiles the interface to the temporary import files.
     * @param modelEntities               a List of {@link org.ofbiz.core.entity.model.ModelEntity}'s that the partitioner should be
     *                                    interested in.
     * @param delegatorInterface          required for persistence
     */
    public GenericEntitiesPartitionHandler(final BackupProject backupProject, final ProjectImportTemporaryFiles projectImportTemporaryFiles,
                                           final List<ModelEntity> modelEntities, final DelegatorInterface delegatorInterface) {
        this.backupProject = backupProject;
        this.projectImportTemporaryFiles = projectImportTemporaryFiles;
        this.delegator = GenericDelegator.getGenericDelegator(delegatorInterface.getDelegatorName());
        modelEntityMap = new HashMap<String, ModelEntity>();
        for (final ModelEntity modelEntity : modelEntities) {
            final String entityName = modelEntity.getEntityName();
            modelEntityMap.put(entityName, modelEntity);
        }
    }

    public void handleEntity(final String entityName, final Map<String, String> attributes) throws ParseException {
        final ModelEntity modelEntity = modelEntityMap.get(entityName);
        if (modelEntity == null) {
            return;
        }
        entityCount++;
        writeXmlText(entityName, attributes, modelEntity);
    }

    private void writeXmlText(final String entityName, final Map<String, String> attributes, final ModelEntity modelEntity)
            throws ParseException {
        if (!entityName.equals(currentEntity)) {
            endDocument();
        }
        if (printWriter == null) {
            try {
                currentEntity = entityName;
                printWriter = projectImportTemporaryFiles.getWriter(entityName);
            } catch (IOException e) {
                throw new ParseException(e.getMessage());
            }
        }
        // Create a GenericEntity
        final GenericEntity genericEntity = new GenericEntity(delegator, modelEntity, attributes);
        genericEntity.writeXmlText(printWriter, null);
    }

    public void startDocument() {
        // NOOP - document header created lazily on first element write
    }

    public void endDocument() {
        printWriter = null;
    }

    ///CLOVER:OFF - NOTE: this is mainly here for testing purposes
    public Map<String, ModelEntity> getRegisteredHandlers() {
        return Collections.unmodifiableMap(modelEntityMap);
    }

    public int getEntityCount() {
        return entityCount;
    }

    ///CLOVER:ON
}