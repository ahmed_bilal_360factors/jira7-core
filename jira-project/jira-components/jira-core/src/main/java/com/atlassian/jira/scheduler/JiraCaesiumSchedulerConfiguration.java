package com.atlassian.jira.scheduler;

import com.atlassian.jira.cluster.ClusterNodeProperties;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.scheduler.caesium.spi.CaesiumSchedulerConfiguration;

import javax.annotation.Nullable;
import java.util.TimeZone;

/**
 * @since v7.0
 */
public class JiraCaesiumSchedulerConfiguration implements CaesiumSchedulerConfiguration {
    private static final int REFRESH_INTERVAL_IN_MINUTES = 5;
    private static final int WORKER_THREAD_COUNT = 4;

    private final ApplicationProperties applicationProperties;
    private final ClusterNodeProperties clusterNodeProperties;

    public JiraCaesiumSchedulerConfiguration(final ApplicationProperties applicationProperties,
                                             final ClusterNodeProperties clusterNodeProperties) {
        this.applicationProperties = applicationProperties;
        this.clusterNodeProperties = clusterNodeProperties;
    }

    @Nullable
    @Override
    public TimeZone getDefaultTimeZone() {
        final String zoneId = applicationProperties.getString(APKeys.JIRA_DEFAULT_TIMEZONE);
        return (zoneId != null) ? TimeZone.getTimeZone(zoneId) : null;
    }

    @Override
    public int refreshClusteredJobsIntervalInMinutes() {
        return isClustered() ? REFRESH_INTERVAL_IN_MINUTES : 0;
    }

    @Override
    public int workerThreadCount() {
        return WORKER_THREAD_COUNT;
    }

    @Override
    public boolean useQuartzJobDataMapMigration() {
        return true;
    }

    @Override
    public boolean useFineGrainedSchedules() {
        return false;
    }

    private boolean isClustered() {
        // Using ClusterManager.isClustered() instead would create a circular dependency
        return clusterNodeProperties.getNodeId() != null;
    }
}

