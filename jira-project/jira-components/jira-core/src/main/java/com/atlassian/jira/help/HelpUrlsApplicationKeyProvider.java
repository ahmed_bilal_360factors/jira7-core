package com.atlassian.jira.help;

import com.atlassian.application.api.ApplicationKey;

import javax.annotation.Nonnull;

/**
 * Provides {@code ApplicationKey} for current user.
 */
public interface HelpUrlsApplicationKeyProvider {
    /**
     * Returns application key that will be used as a key to load and then store help urls in cache,
     *
     * @return application key for current user
     */
    @Nonnull
    ApplicationKey getApplicationKeyForUser();

    /**
     * Determines whether legacy help urls loader should be used. This will be removed after the renaissance is enabled
     * by default.
     *
     * @return {@code true} if legacy help urls should be loaded, {@code false} otherwise
     * @deprecated since 7.0.1 as it always returns false now
     */
    @Deprecated
    default boolean shouldParseLegacyHelpProperties() {
        return false;
    };
}
