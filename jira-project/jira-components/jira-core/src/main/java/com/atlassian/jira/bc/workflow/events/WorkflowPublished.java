package com.atlassian.jira.bc.workflow.events;

import com.atlassian.analytics.api.annotations.EventName;

@EventName("administration.workflow.published.isolation")
public class WorkflowPublished {

    private boolean isIsolated;
    private boolean isAdministrator;

    public WorkflowPublished(boolean isIsolated, boolean isAdministrator) {
        this.isIsolated = isIsolated;
        this.isAdministrator = isAdministrator;
    }

    public boolean isIsolated() {
        return isIsolated;
    }

    public void setIsolated(boolean isolated) {
        isIsolated = isolated;
    }

    public boolean isAdministrator() {
        return isAdministrator;
    }

    public void setAdministrator(boolean administrator) {
        isAdministrator = administrator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WorkflowPublished)) return false;

        WorkflowPublished that = (WorkflowPublished) o;

        if (isIsolated != that.isIsolated) return false;
        return isAdministrator == that.isAdministrator;

    }

    @Override
    public int hashCode() {
        int result = (isIsolated ? 1 : 0);
        result = 31 * result + (isAdministrator ? 1 : 0);
        return result;
    }
}
