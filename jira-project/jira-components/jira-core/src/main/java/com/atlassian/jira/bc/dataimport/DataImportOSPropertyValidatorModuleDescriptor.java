package com.atlassian.jira.bc.dataimport;

import com.atlassian.jira.plugin.AbstractJiraModuleDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.module.ModuleFactory;

/**
 * Plugin point to validate property entries in backup XML before import starts.
 */
public class DataImportOSPropertyValidatorModuleDescriptor extends AbstractJiraModuleDescriptor<DataImportOSPropertyValidator> {
    public static final String XML_ELEMENT_NAME = "data-import-properties-validator";

    public DataImportOSPropertyValidatorModuleDescriptor(JiraAuthenticationContext authenticationContext, ModuleFactory moduleFactory) {
        super(authenticationContext, moduleFactory);
    }
}
