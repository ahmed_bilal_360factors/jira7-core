package com.atlassian.jira.plugin.bigpipe;

import javax.annotation.Nonnull;

/**
 * A runnable that also has an execution priority.  Higher priorities are executed before lower ones when used with a
 * {@link CollectingExecutor}.
 *
 * @see CollectingExecutor
 * @since v7.1
 */
public class PrioritizedRunnable implements Runnable, Comparable<PrioritizedRunnable> {
    private final
    @Nonnull
    Runnable runnable;
    private final int priority;

    /**
     * Wraps a plain runnable, giving it a priority.
     *
     * @param priority the priority.
     * @param runnable the runnable to wrap.
     */
    public PrioritizedRunnable(int priority, @Nonnull Runnable runnable) {
        this.priority = priority;
        this.runnable = runnable;
    }

    @Override
    public void run() {
        runnable.run();
    }

    /**
     * @return the execution priority.  Higher priorities are executed before lower ones.
     */
    public int getPriority() {
        return priority;
    }

    @Override
    public int compareTo(@Nonnull PrioritizedRunnable o) {
        //Reversed so that higher values priorities come before lower ones
        return Integer.compare(o.priority, priority);
    }
}
