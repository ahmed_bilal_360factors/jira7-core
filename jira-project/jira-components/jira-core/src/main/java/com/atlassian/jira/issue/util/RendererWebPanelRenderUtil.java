package com.atlassian.jira.issue.util;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.renderer.IssueRenderContext;
import com.atlassian.jira.issue.fields.renderer.JiraRendererPlugin;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.jira.web.component.ModuleWebComponent;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.atlassian.jira.web.action.AjaxHeaders.isPjaxRequest;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * Utility class to help with rendering the pluggable web panels on the jira-renderer fields.
 *
 * @since 7.3
 */
public class RendererWebPanelRenderUtil {

    private final ApplicationUser loggedInUser;
    private final WebInterfaceManager webInterfaceManager;
    private final ModuleWebComponent moduleWebComponent;
    private final Map<String, Object> webPanelParams;

    public RendererWebPanelRenderUtil(final ApplicationUser loggedInUser,
                                      final String value,
                                      final String issueKey,
                                      final Issue issue,
                                      final String fieldId,
                                      final String fieldName,
                                      final boolean singleLine,
                                      final WebInterfaceManager webInterfaceManager,
                                      final ModuleWebComponent moduleWebComponent,
                                      final JiraRendererPlugin renderer) {
        this.loggedInUser = loggedInUser;
        this.webInterfaceManager = webInterfaceManager;
        this.moduleWebComponent = moduleWebComponent;

        Optional<Issue> issueOpt = ofNullable(issue);

        webPanelParams = new HashMap<>();
        webPanelParams.put("user", loggedInUser);
        webPanelParams.put("project", issueOpt.map(Issue::getProjectObject).orElse(null));
        webPanelParams.put("issue", issue);
        final JiraHelper jiraHelper = new JiraHelper(ExecutingHttpRequest.get(), issueOpt.map(Issue::getProjectObject).orElse(null));
        webPanelParams.put("helper", jiraHelper);
        webPanelParams.put("isAsynchronousRequest", isPjaxRequest(ExecutingHttpRequest.get()));
        webPanelParams.put("req", ExecutingHttpRequest.get());
        webPanelParams.put("issueKey", issueKey);
        webPanelParams.put("fieldId", fieldId);
        webPanelParams.put("fieldName", fieldName);
        webPanelParams.put("rendererType", renderer.getRendererType());
        webPanelParams.put("value", renderer.transformForEdit(value));

        final IssueRenderContext issueRenderContext = issueOpt.map(Issue::getIssueRenderContext).orElse(new IssueRenderContext(null));
        issueRenderContext.addParam(IssueRenderContext.WYSIWYG_PARAM, Boolean.TRUE);
        webPanelParams.put("valueRendered", isNotEmpty(value) ? renderer.render(value, issueRenderContext) : "");
        if (singleLine) {
            webPanelParams.put("singleLine", Boolean.TRUE);
        }
    }

    public List<WebPanelModuleDescriptor> getWebPanels(final String location) {
        return webInterfaceManager.getDisplayableWebPanelDescriptors(location, webPanelParams);
    }

    public String renderPanels(List<WebPanelModuleDescriptor> panels) {
        if (panels != null) {
            return moduleWebComponent.renderModules(loggedInUser, ExecutingHttpRequest.get(), panels, webPanelParams);

        }
        return "";
    }
}
