package com.atlassian.jira.imports.project.ao.handler;

import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.imports.project.taskprogress.TaskProgressProcessor;
import com.atlassian.jira.imports.project.taskprogress.ThrottlingTaskProgressProcessor;
import com.atlassian.jira.imports.project.util.XMLEscapeUtil;
import com.atlassian.jira.util.xml.SecureXmlEntityResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * A base class used for processing an AO backup file. This collects Entity information and calls the {@link
 * PluggableImportAoEntityHandler#handleEntity(String, java.util.Map)} method on its
 * registered delegates. The attributes map includes any attributes that may be nested as sub-elements. <br/> If you
 * construct this with a {@link com.atlassian.jira.task.TaskProgressSink} then the progress of the XML processing will
 * be relayed.
 * <p>
 * This only processes the <data> <row /> </data> elements from the backup.
 *
 * @since v6.5
 */
public class ChainedAoSaxHandler extends DefaultHandler {
    private static final Logger log = LoggerFactory.getLogger(ChainedAoSaxHandler.class);
    public static final SecureXmlEntityResolver EMPTY_ENTITY_RESOLVER = new SecureXmlEntityResolver();
    public static final String AO_BACKUP_XML = "backup";
    public static final String DATA = "data";
    public static final String TABLE_NAME = "tableName";
    public static final String COLUMN = "column";
    public static final String COLUMN_NAME = "name";
    public static final String ROW = "row";
    public static final String STRING = "string";
    public static final String INTEGER = "integer";
    public static final String DOUBLE = "double";
    public static final String BOOLEAN = "boolean";
    public static final String DATE = "timestamp";

    private Map<String, Object> rowData;
    private String inEntity = null;
    private StringBuffer textBuffer;
    private boolean hasRootElement = false;
    private long entityCount;
    private int entityTypeCount;
    private long currentEntityCount;

    final Collection<AoEntityHandler> delegateHandlers = new ArrayList<>();
    private String currentTable = null;
    private List<String> tableColumns = new ArrayList<>();
    private int columnIndex = 0;

    private final TaskProgressProcessor taskProgressProcessor;

    /**
     * Constructor to create an AbstractHandler with progress feedback.
     *
     * @param taskProgressProcessor the {@link TaskProgressProcessor} that relays the progress information.
     */
    public ChainedAoSaxHandler(final TaskProgressProcessor taskProgressProcessor) {
        super();
        this.taskProgressProcessor = taskProgressProcessor;
    }

    /**
     * Simple constructor for creating an AbstractHandler without progress feedback.
     */
    public ChainedAoSaxHandler() {
        this(new ThrottlingTaskProgressProcessor());
    }

    public void registerHandler(final AoEntityHandler handler) {
        delegateHandlers.add(handler);
    }

    public void registerHandlers(final Iterable<? extends AoEntityHandler> aoPluginPreImportHandlers) {
        for (AoEntityHandler importAoEntityHandler : aoPluginPreImportHandlers) {
            registerHandler(importAoEntityHandler);
        }
    }

    //===========================================================
    // SAX DocumentHandler methods

    /**
     * Provides the number of actual XML elements that the parser encounters.
     *
     * @return number of actual XML elements the parser encounters
     */
    public long getEntityCount() {
        return entityCount;
    }

    //===========================================================

    @Override
    public InputSource resolveEntity(String publicId, String systemId) throws IOException, SAXException {
        return EMPTY_ENTITY_RESOLVER.resolveEntity(publicId, systemId);
    }

    public final void startDocument() throws SAXException {
        log.debug("Starting parsing Document with ChainedAoSaxHandler.");
        entityCount = 0;
        // Call startDocument() on the registered ImportEntityHandlers
        for (final AoEntityHandler ImportAoEntityHandler : delegateHandlers) {
            ImportAoEntityHandler.startDocument();
        }
        taskProgressProcessor.processTaskProgress("Start", entityTypeCount, entityCount, currentEntityCount);
    }

    public final void endDocument() throws SAXException {
        if (hasRootElement) {
            throw new SAXException("XML file ended too early.  There was no </entity-engine-xml> tag.");
        }
        // Call endDocument() on the registered ImportEntityHandlers
        for (final AoEntityHandler ImportAoEntityHandler : delegateHandlers) {
            ImportAoEntityHandler.endDocument();
        }
        log.debug("Ended parsing Document with ChainedAoSaxHandler.");
    }

    public final void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {
        if (hasRootElement) {
            startElement(qName, attributes);
        } else if (AO_BACKUP_XML.equals(qName)) { // When we encounter the root element we should record it
            // Set that the document has started correctly
            hasRootElement = true;
        } else {
            throw new SAXException("The XML document does not contain the <backup> root element or it was closed too early.");
        }
    }

    public void endElement(final String uri, final String localName, final String qName) throws SAXException {
        // Only process the endElement if we have a root element
        if (hasRootElement) {
            // Are we closing the root element?
            if (AO_BACKUP_XML.equals(qName)) {
                hasRootElement = false;
            } else {
                endElement(qName);
            }
        } else {
            throw new SAXException("How did we get here an exception should already have been thrown");
        }
    }

    public final void characters(final char ch[], final int start, final int length) throws SAXException {
        final String s = XMLEscapeUtil.unicodeDecode(new String(ch, start, length));
        if (textBuffer == null) {
            textBuffer = new StringBuffer(s);
        } else {
            textBuffer.append(s);
        }
    }

    private void startElement(final String qName, final Attributes attributes) {
        if (DATA.equals(qName)) {
            // This is the beginning of a new table so we need to record the table name
            currentTable = decode(attributes.getValue(TABLE_NAME));
            tableColumns.clear();
        } else if (currentTable != null) {
            if (COLUMN.equals(qName)) {
                // Gather the column names, they are in the same sequence as the data in the rows
                tableColumns.add(decode(attributes.getValue(COLUMN_NAME)));
            } else if (ROW.equals(qName)) {
                // When we encounter an element we want to create a map of its attributes and record that we are
                // currently inside that entity
                inEntity = currentTable;
                rowData = new HashMap<String, Object>();
                columnIndex = 0;
            } else if (inEntity != null) {
                textBuffer = null;
            }
        }
    }

    private void endElement(final String qName) throws SAXException {
        if (DATA.equals(qName)) {
            endTable(currentTable);
        } else if (currentTable != null) {
            if (ROW.equals(qName)) {
                endRow(currentTable);
            } else if (inEntity != null) {
                // We are in a nested element and we need to add that elements contents to the current
                // attributes map
                if (columnIndex < tableColumns.size()) {
                    endNestedElement(tableColumns.get(columnIndex), qName);
                } else {
                    log.debug("Expected {} columns for table [{}], but tried to look up column {}", tableColumns.size(), currentTable, columnIndex);
                }
                columnIndex++;
            }
        }
    }

    /**
     * Examine the textBuffer set by {@link #characters(char[], int, int)} and add it to the attributes map for the
     * current inEntity entity.
     *
     * @param column identifies the name of the attribute we are examining
     * @param type
     */
    private void endNestedElement(final String column, final String type) throws SAXException {
        Object value = null;
        if (textBuffer != null) {
            if (STRING.equals(type)) {
                value = decode(textBuffer.toString());
            } else if (INTEGER.equals(type)) {
                // We use a long here because AO actually stores long values, i.e. > Integer.MAX_VALUE in
                // element called "integer"
                value = Long.valueOf(textBuffer.toString());
            } else if (DOUBLE.equals(type)) {
                value = Double.valueOf(textBuffer.toString());
            } else if (BOOLEAN.equals(type)) {
                value = Boolean.valueOf(textBuffer.toString());
            } else if (DATE.equals(type)) {
                try {
                    value = newDateFormat().parse(textBuffer.toString());
                } catch (java.text.ParseException e) {
                    throw new SAXException(e);
                }
            } else {
                throw new SAXException("Unrecognised AO Data Type");
            }
        }
        rowData.put(column, value);
        textBuffer = null;
    }

    private DateFormat newDateFormat() {
        final DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf;
    }

    /**
     * Closes the table. This method calls the delegates {@link PluggableImportAoEntityHandler#endTable(String)}
     * methods signalling that all row data for that table has been processed.
     *
     * @param tableName identifies the name of the top level element we are closing.
     *                  handleEntity
     */
    private void endTable(final String tableName) {
        // End of an entity type
        entityTypeCount++;
        currentEntityCount = 0;
        currentTable = null;

        // Call handleEntity() on the registered ImportEntityHandlers
        for (final AoEntityHandler importAoEntityHandler : delegateHandlers) {
            if (importAoEntityHandler.handlesEntity(tableName)) {
                importAoEntityHandler.endTable(tableName);
            }
        }
    }

    /**
     * Closes the row. This signals that we have filled the rowData with all attributes
     * for this entity. This method calls the delegates {@link PluggableImportAoEntityHandler#handleEntity(String,
     * java.util.Map)} methods with the collected information and then clears the state objects.
     *
     * @param tableName identifies the name of the top level element we are closing.
     * @throws org.xml.sax.SAXException if a {@link org.apache.xerces.impl.xpath.regex.ParseException} is caused by calling
     *                                  handleEntity
     */
    private void endRow(final String tableName) throws SAXException {
        inEntity = null;
        try {
            // Counts all entities found
            entityCount++;
            // Records how many of qName entities we have encountered
            currentEntityCount++;
            taskProgressProcessor.processTaskProgress(tableName, entityTypeCount, entityCount, currentEntityCount);

            // Call handleEntity() on the registered ImportEntityHandlers
            for (final AoEntityHandler importAoEntityHandler : delegateHandlers) {
                if (importAoEntityHandler.handlesEntity(tableName)) {
                    importAoEntityHandler.handleEntity(tableName, rowData);
                }
            }
        } catch (final ParseException e) {
            log.warn("Encountered a parsing exception.", e);
            throw new SAXException(e);
        }
    }

    private String decode(String value) {
        //decode escaped characters; earlier it should have been read through XMLEscapingReader
        return XMLEscapeUtil.unicodeDecode(value);
    }
}
