package com.atlassian.jira.plugin.icon;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.icon.IconOwningObjectId;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;

/**
 * Implements an {@link IconTypePolicy} for icons for {@link com.atlassian.jira.issue.issuetype.IssueType}s.
 */
public class IssueTypeIconTypePolicy implements IconTypePolicy {
    private final GlobalPermissionManager globalPermissionManager;
    private final PermissionManager permissionManager;
    private final ProjectService projectService;

    public IssueTypeIconTypePolicy(
            GlobalPermissionManager globalPermissionManager, PermissionManager permissionManager, ProjectService projectService) {
        this.globalPermissionManager = globalPermissionManager;
        this.permissionManager = permissionManager;
        this.projectService = projectService;
    }

    @Override
    public boolean userCanView(@Nullable ApplicationUser remoteUser, @Nonnull Avatar icon) {
        String owningObjectID = icon.getOwner();

        if (owningObjectID == null) {
            return true;
        } else if (icon.getIconType().equals(IconType.ISSUE_TYPE_ICON_TYPE)) {
            return true;
        } else {
            ApplicationUser owner = ApplicationUsers.byKey(owningObjectID);
            // TODO Change this to use globalPermissionManager
            return (owner == null || owner.equals(remoteUser) || permissionManager.hasPermission(Permissions.USE, remoteUser));
        }
    }

    @Override
    public boolean userCanDelete(@Nullable ApplicationUser remoteUser, @Nonnull Avatar icon) {
        return userCanCreateFor(remoteUser, new IconOwningObjectId(icon.getOwner()));
    }

    @Override
    public boolean userCanCreateFor(@Nullable ApplicationUser remoteUser, @Nonnull IconOwningObjectId owningObjectId) {
        String user = owningObjectId.getId();

        if (user == null) {
            return false;
        } else {
            return globalPermissionManager.hasPermission(ADMINISTER, remoteUser);
        }
    }
}
