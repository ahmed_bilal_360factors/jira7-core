package com.atlassian.jira.project.template.module;

import com.atlassian.jira.project.template.hook.AddProjectModule;

import java.util.Optional;

public class ProjectTemplateModuleBuilder {
    private AddProjectModule addProjectModule;
    private String key;
    private Integer weight;
    private String labelKey;
    private String descriptionKey;
    private Optional<String> longDescriptionKey;
    private Icon icon;
    private Icon backgroundIcon;
    private String infoSoyPath;
    private String projectTypeKey;

    public ProjectTemplateModuleBuilder() {
    }

    public ProjectTemplateModuleBuilder key(String key) {
        this.key = key;
        return this;
    }

    public ProjectTemplateModuleBuilder weight(int weight) {
        this.weight = weight;
        return this;
    }

    public ProjectTemplateModuleBuilder labelKey(String labelKey) {
        this.labelKey = labelKey;
        return this;
    }

    public ProjectTemplateModuleBuilder descriptionKey(String descriptionKey) {
        this.descriptionKey = descriptionKey;
        return this;

    }

    public ProjectTemplateModuleBuilder longDescriptionKey(Optional<String> longDescriptionKey) {
        this.longDescriptionKey = longDescriptionKey;
        return this;
    }

    public ProjectTemplateModuleBuilder icon(Icon icon) {
        this.icon = icon;
        return this;
    }

    public ProjectTemplateModuleBuilder backgroundIcon(Icon backgroundIcon) {
        this.backgroundIcon = backgroundIcon;
        return this;
    }

    public ProjectTemplateModuleBuilder addProjectModule(AddProjectModule addProjectModule) {
        this.addProjectModule = addProjectModule;
        return this;
    }

    public ProjectTemplateModuleBuilder infoSoyPath(String infoSoyPath) {
        this.infoSoyPath = infoSoyPath;
        return this;
    }

    public ProjectTemplateModuleBuilder projectTypeKey(String projectTypeKey) {
        this.projectTypeKey = projectTypeKey;
        return this;
    }

    public ProjectTemplateModule build() {
        return new ProjectTemplateModule(
                key,
                weight,
                labelKey,
                descriptionKey,
                longDescriptionKey,
                icon,
                backgroundIcon,
                addProjectModule,
                infoSoyPath,
                projectTypeKey
        );
    }
}
