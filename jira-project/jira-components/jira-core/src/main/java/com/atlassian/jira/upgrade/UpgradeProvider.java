package com.atlassian.jira.upgrade;

import java.util.Collection;

/**
 * Component used for providing JIRA upgrade tasks.
 */
public interface UpgradeProvider {

    /**
     * Constructs JIRA upgrade tasks.
     *
     * @param <T> is the type of the Upgrade Task as we have different types for these:
     *            {@link com.atlassian.jira.upgrade.UpgradeTask} and {@link com.atlassian.upgrade.api.UpgradeTask}.
     * @return the collection of the upgrade task.
     */
    <T> Collection<T> getUpgradeTasks();

    /**
     * Constructs all JIRA upgrade tasks for build numbers less than or equal to some limit. This allows us to only
     * run upgrade tasks up to a certain point in the database's history.
     *
     * @param <T> is the type of the Upgrade Task as we have different types for these:
     *            {@link com.atlassian.jira.upgrade.UpgradeTask} and {@link com.atlassian.upgrade.api.UpgradeTask}.
     * @return the collection of the upgrade task.
     */
    <T> Collection<T> getUpgradeTasksBoundByBuild(long buildNumberUpperBound);
}
