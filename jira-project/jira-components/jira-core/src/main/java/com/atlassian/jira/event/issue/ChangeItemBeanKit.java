package com.atlassian.jira.event.issue;

import com.atlassian.jira.issue.history.ChangeItemBean;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * Helper class to simplify access of a collection of {@link ChangeItemBean}
 */
public final class ChangeItemBeanKit {

    /**
     * Gets the change item bean out of the given collection for the given fieldName.
     * Only returns the change item bean if,
     * <ol>
     * <li>the given <code>fieldName</code> equals the name of the change item bean in the given collection</li>
     * <li>the old and the new value do not equal (don't have the same value and are not both null)</li>
     * </ol>
     *
     * @param fieldName   the name of the field to get
     * @param changeItems the collection of change item beans to search for the field
     * @return empty, if the field is not in the collection, or the field value didn't change
     */
    public static Optional<ChangeItemBean> getFieldChange(String fieldName, Collection<ChangeItemBean> changeItems) {
        return changeItems.stream().filter(hasFieldChanged(fieldName)).findFirst();
    }

    private static Predicate<ChangeItemBean> hasFieldChanged(String fieldName) {
        return changeItem -> Objects.equals(fieldName, changeItem.getField()) && hasItemChangedValue().test(changeItem);
    }

    private static Predicate<ChangeItemBean> hasItemChangedValue() {
        return changeItem -> !Objects.equals(changeItem.getTo(), changeItem.getFrom());
    }

    private ChangeItemBeanKit() {

    }
}
