package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.plugin.webfragment.conditions.cache.ConditionCacheKey;
import com.atlassian.jira.plugin.webfragment.conditions.cache.ConditionCacheKeys;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;

import java.util.Map;

import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.atlassian.jira.plugin.webfragment.conditions.cache.RequestCachingConditionHelper.cacheConditionResultInRequest;

/**
 * Checks that the current user is a project admin for the passed in Project, Version or Component
 *
 * @since v5.0
 */
public class CanAdministerProjectCondition extends AbstractWebCondition {
    private final PermissionManager permissionManager;
    private final ProjectManager projectManager;
    private final UserIsAdminCondition isAdminCondition;

    public CanAdministerProjectCondition(GlobalPermissionManager globalPermissionManager, PermissionManager permissionManager, ProjectManager projectManager) {
        this.permissionManager = permissionManager;
        this.projectManager = projectManager;
        this.isAdminCondition = new UserIsAdminCondition(globalPermissionManager);
    }

    @Override
    public void init(Map<String, String> params) {
    }

    @Override
    public boolean shouldDisplay(final ApplicationUser user, final JiraHelper jiraHelper) {
        final Project project = getProject(jiraHelper.getContextParams());
        if (project == null) {
            return false;
        }

        final ConditionCacheKey cacheKey = ConditionCacheKeys.permission(ProjectPermissions.ADMINISTER_PROJECTS, user, project.getKey());

        return isAdminCondition.shouldDisplay(user, jiraHelper)
                || cacheConditionResultInRequest(cacheKey, () -> hasPermission(user, project));
    }

    private boolean hasPermission(ApplicationUser user, Project project) {
        try {
            return permissionManager.hasPermission(ADMINISTER_PROJECTS, project, user);
        } catch (Exception e) {
            return false;
        }
    }

    private Project getProject(Map<String, Object> context) {
        if (context.containsKey("project")) {
            return (Project) context.get("project");
        }
        if (context.containsKey("issue")) {
            return ((Issue) context.get("issue")).getProjectObject();
        }
        if (context.containsKey("helper")) {
            JiraHelper helper = (JiraHelper) context.get("helper");
            if (helper.getProjectObject() != null) {
                return helper.getProjectObject();
            }

        }
        if (context.containsKey("version")) {
            return ((Version) context.get("version")).getProjectObject();
        }
        if (context.containsKey("component")) {
            final Long projectId = ((ProjectComponent) context.get("component")).getProjectId();
            return projectManager.getProjectObj(projectId);
        }

        return null;
    }
}
