package com.atlassian.jira.database;

import com.querydsl.core.types.Path;
import com.querydsl.sql.RelationalPath;
import com.querydsl.sql.dml.DefaultMapper;
import com.querydsl.sql.dml.Mapper;

import javax.annotation.Nullable;
import java.util.Comparator;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import static java.util.Optional.ofNullable;

/**
 * Workaround for the problem, that Querydsl uses HashMap in DefaultMapper.
 * https://github.com/querydsl/querydsl/issues/1594
 * <p/>
 * Because of this the order of columns in generated SQLs might be random, which in turn may be breaking tests
 * and cause suboptimal queries on our databases (changing the order of columns may reduce DB statement caching capabilities).
 * <p/>
 * In this workaround we simply wrap the returned HashMap into a TreeMap.
 */
class OrderingMapper implements Mapper<Object> {
    public static final Mapper<Object> DEFAULT = new OrderingMapper();

    /**
     * Wrapper around DefaultMapper.createMap(), which makes sure the returned map order is predictable.
     */
    @Override
    public Map<Path<?>, Object> createMap(final RelationalPath<?> path, final Object object) {
        final Map<Path<?>, Object> unorderedMap = DefaultMapper.DEFAULT.createMap(path, object);
        final TreeMap<Path<?>, Object> orderedMap = new TreeMap<>(getPathNameComparator());
        orderedMap.putAll(unorderedMap);
        return orderedMap;
    }

    private static Comparator<Path<?>> getPathNameComparator() {
        return (o1, o2) -> Objects.compare(
                getPathName(o1),
                getPathName(o2),
                String::compareTo
        );
    }

    @Nullable
    private static String getPathName(@Nullable final Path<?> path) {
        return ofNullable(path)
                .flatMap(p -> ofNullable(p.getMetadata()))
                .flatMap(metadata -> ofNullable(metadata.getName()))
                .orElse(null);
    }
}
