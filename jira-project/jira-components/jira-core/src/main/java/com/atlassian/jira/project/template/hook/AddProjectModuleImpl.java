package com.atlassian.jira.project.template.hook;

import com.atlassian.jira.project.template.descriptor.ConfigTemplateParser;
import com.atlassian.jira.project.template.descriptor.ProjectTemplateModuleDescriptor;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.module.ModuleFactory;
import com.google.common.base.Strings;

import java.io.File;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class AddProjectModuleImpl implements AddProjectModule {
    private final String addProjectHookClass;
    private final ModuleFactory moduleFactory;
    private final ProjectTemplateModuleDescriptor moduleDescriptor;
    private final ConfigTemplate configTemplate;

    public AddProjectModuleImpl(
            ModuleFactory moduleFactory,
            ProjectTemplateModuleDescriptor moduleDescriptor,
            String addProjectHookClass,
            String templateConfigurationFile,
            ConfigTemplateParser configTemplateParser) {
        this.moduleFactory = moduleFactory;
        this.moduleDescriptor = moduleDescriptor;

        this.addProjectHookClass = addProjectHookClass;

        if (templateConfigurationFile == null) {
            this.configTemplate = null;
        } else {
            this.configTemplate = configTemplateParser.parse(templateConfigurationFile, moduleDescriptor.getPlugin());
            addIconResources();
        }
    }

    private void addIconResources() {
        // Issue type icons used to be defined as URL-s served by the ProjectTemplateModuleDescriptor resources.
        // We need to keep serving them for backward compatibility, otherwise such legacy issue types will lose their icons.
        if (configTemplate.issueTypeSchemeTemplate().isPresent()) {
            for (IssueTypeTemplate issueTypeTemplate : configTemplate.issueTypeSchemeTemplate().get().issueTypeTemplates()) {
                String iconPath = issueTypeTemplate.iconPath();
                String iconName = new File(iconPath).getName();
                moduleDescriptor.addResource(iconPath, iconName);
            }
        }
    }

    @Override
    public boolean hasConfigTemplate() {
        return configTemplate() != null;
    }

    public ConfigTemplate configTemplate() {
        return configTemplate;
    }

    @Override
    public boolean hasProjectCreateHook() {
        return !isBlank(addProjectHookClass);
    }

    // Instantiate the Project Hook again for every Project Template
    public AddProjectHook addProjectHook() {
        if (Strings.isNullOrEmpty(addProjectHookClass)) {
            return null;
        }

        Object module = moduleFactory.createModule(addProjectHookClass, moduleDescriptor);
        try {
            return (AddProjectHook) module;
        } catch (ClassCastException ex) {
            throw new PluginException("The class '" + addProjectHookClass + "' is not an instance of AddProjectHook.", ex);
        }
    }
}
