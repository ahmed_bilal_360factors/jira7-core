package com.atlassian.jira.instrumentation;

/**
 * Used when comparing two executions; has a flag for whether they were equivalent.
 */
public class DeltaResultCounter extends ResultCounter {
    private final boolean equiv;

    public DeltaResultCounter(long elapsed, long results, boolean equiv) {
        super(elapsed, results);
        this.equiv = equiv;
    }

    public boolean isEquiv() {
        return equiv;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DeltaResultCounter)) return false;
        if (!super.equals(o)) return false;

        DeltaResultCounter that = (DeltaResultCounter) o;

        return equiv == that.equiv;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (equiv ? 1 : 0);
        return result;
    }
}
