package com.atlassian.jira.web.action.admin.workflow.tabs;

import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.ResultDescriptor;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

public class WorkflowTransitionPostFunctionsContextProvider extends WorkflowTransitionContextProvider {
    @Override
    protected int getCount(final Map<String, Object> context) {
        return WorkflowTransitionContextUtils.getTransition(context)
                .map(ActionDescriptor::getUnconditionalResult)
                .filter(Objects::nonNull)
                .map(ResultDescriptor::getPostFunctions)
                .filter(Objects::nonNull)
                .map(Collection::size)
                .orElse(0);
    }
}
