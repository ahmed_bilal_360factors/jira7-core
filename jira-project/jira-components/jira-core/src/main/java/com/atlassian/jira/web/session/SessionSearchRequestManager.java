package com.atlassian.jira.web.session;

import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.util.NonInjectableComponent;

/**
 * Provides access to getting and setting {@link SearchRequest} objects in session.
 *
 * @see SessionSearchObjectManagerFactory#createSearchRequestManager()
 * @see SessionSearchObjectManagerFactory#createSearchRequestManager(javax.servlet.http.HttpServletRequest)
 * @see SessionSearchObjectManagerFactory#createSearchRequestManager(com.atlassian.jira.util.velocity.VelocityRequestSession)
 * @since v4.2
 * @deprecated since 7.1 Storing information on the httpsession is obsoleted. Please move this functionality to the frontend or other type of storage. This method will be removed in 8.0.
 */
@Deprecated
@NonInjectableComponent
public interface SessionSearchRequestManager extends SessionSearchObjectManager<SearchRequest> {
}
