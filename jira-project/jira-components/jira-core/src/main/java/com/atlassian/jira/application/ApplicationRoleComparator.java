package com.atlassian.jira.application;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

public class ApplicationRoleComparator implements Comparator<ApplicationRole> {
    private final Collator collator;

    public ApplicationRoleComparator(Locale userLocale) {
        collator = Collator.getInstance(userLocale);
        collator.setStrength(Collator.SECONDARY); //case-insensitive
    }

    @Override
    public int compare(ApplicationRole o1, ApplicationRole o2) {
        int comp = Boolean.compare(o1.isPlatform(), o2.isPlatform());
        if (comp == 0) {
            //Use user's locale to compare names of application roles (since they are i18n'ed)
            comp = collator.compare(o1.getName(), o2.getName());
        }

        return comp;
    }
}
