package com.atlassian.jira.upgrade;

import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.JiraSystemProperties;

import javax.annotation.Nullable;
import java.util.Collection;

public interface UpgradeTask {
    /**
     * @return The build number that this upgrade is applicable to
     */
    int getBuildNumber();

    /**
     * A short (<50 chars) description of the upgrade action
     */
    String getShortDescription();

    /**
     * Perform the upgrade.
     *
     * @param setupMode Indicating this upgrade task is running during set up.
     */
    void doUpgrade(boolean setupMode) throws Exception;

    /**
     * Return the Upgrade task id of another upgrade task that must be run prior to this task.
     *
     * @since v6.4
     */
    @Nullable
    Integer dependsUpon();

    /**
     * Return when this upgrade task can be scheduled.
     *
     * @since v6.4
     */
    ScheduleOption getScheduleOption();

    /**
     * Return any errors that occur.  Each entry is a string.
     */
    Collection<String> getErrors();

    /**
     * Flag to claim whether this upgrade task needs an explicit downgrade task to reverse the data changes.
     * <p>
     * If a downgrade is a simple no-op then return false, and JIRA will ignore these changes during a downgrade.
     * </p>
     * <p>
     * If you need to actually undo the changes made here then declare true and add a Downgrade Task to the bug fix
     * branch.
     * </p>
     *
     * @return true if an actual Downgrade Task must run to revert these changes, false if downgrade is a no-op.
     */
    boolean isDowngradeTaskRequired();

    /**
     * Track status of a task this session, if isTaskDone(String) returns true you don't need to do it again.
     */
    class Status {
        private static JiraProperties jiraSystemProperties = JiraSystemProperties.getInstance();

        public static void setTaskDone(final String taskId) {
            jiraSystemProperties.setProperty(asPropertyName(taskId), "true");
        }

        public static boolean isTaskDone(final String taskId) {
            return jiraSystemProperties.getProperty(asPropertyName(taskId)) != null;
        }

        private static String asPropertyName(final String taskId) {
            return "jira.task." + taskId + ".complete";
        }
    }

    enum ScheduleOption {
        /**
         * The upgrade task must be run during the JIRA startup process and prior to JIRA being available for use.
         */
        BEFORE_JIRA_STARTED,
        /**
         * The upgrade task may be run after the JIRA startup process is complete and while JIRA is available for use.
         */
        AFTER_JIRA_STARTED
    }
}