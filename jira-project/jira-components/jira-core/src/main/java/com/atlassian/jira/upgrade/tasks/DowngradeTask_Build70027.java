package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.upgrade.AbstractDowngradeTask;
import com.atlassian.jira.upgrade.DowngradeException;
import com.atlassian.jira.upgrade.ReindexRequirement;

import javax.annotation.Nonnull;

/**
 * It's a marker for the change to the attachment file system in 7.0 so that
 * it is know that this version of 6.4 onwards is able to read in the new file system
 * from exports by matching to the corresponding upgrade task.
 *
 * @since v6.4.9
 */
public class DowngradeTask_Build70027 extends AbstractDowngradeTask {
    @Override
    public int getBuildNumber() {
        return 70027;
    }

    @Nonnull
    @Override
    public String getShortDescription() {
        return "Marker for change to attachment format";
    }

    @Override
    public void downgrade() throws DowngradeException {
    }

    @Nonnull
    @Override
    public ReindexRequirement reindexRequired() {
        return ReindexRequirement.NONE;
    }
}
