package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.PropertiesManager;
import com.atlassian.jira.upgrade.AbstractDelayableUpgradeTask;

import com.opensymphony.module.propertyset.PropertySet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * It will reset current admin settings for RTE
 *
 * @since v7.3
 */
public class UpgradeTask_Build73002 extends AbstractDelayableUpgradeTask {
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask_Build73002.class);

    private final PropertiesManager propertiesManager;

    public UpgradeTask_Build73002(final PropertiesManager propertiesManager) {
        super();
        this.propertiesManager = propertiesManager;
    }

    @Override
    public int getBuildNumber() {
        return 73002;
    }

    @Override
    public String getShortDescription() {
        return "Reset admin settings for Rich Text Editor.";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception {
        final PropertySet propertySet = propertiesManager.getPropertySet();

        if (propertySet.exists(APKeys.JIRA_OPTION_RTE_ENABLED)) {
            if (!propertySet.getBoolean(APKeys.JIRA_OPTION_RTE_ENABLED)) {
                propertySet.remove(APKeys.JIRA_OPTION_RTE_ENABLED);
                log.info("{} application property was reset. Rich Text Editor is enabled by default.", APKeys.JIRA_OPTION_RTE_ENABLED);
            } else {
                log.debug("{} application property was not reset. Rich Text Editor is already enabled.", APKeys.JIRA_OPTION_RTE_ENABLED);
            }
        } else {
            log.debug("{} application property was not found. Rich Text Editor is enabled by default.", APKeys.JIRA_OPTION_RTE_ENABLED);
        }
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        return false;
    }
}
