package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Either;
import com.google.common.base.Function;

/**
 * Some common {@link com.atlassian.application.api.ApplicationKey}s that JIRA needs to know about.
 *
 * @since v7.0
 */
public final class ApplicationKeys {
    public static final ApplicationKey CORE = ApplicationKey.valueOf("jira-core");
    public static final ApplicationKey SERVICE_DESK = ApplicationKey.valueOf("jira-servicedesk");
    public static final ApplicationKey SOFTWARE = ApplicationKey.valueOf("jira-software");

    private ApplicationKeys() {
        //No creation allowed.
    }

    public static final Function<String, Either<String, ApplicationKey>> TO_APPLICATION_KEY = input -> {
        if (ApplicationKey.isValid(input)) {
            return Either.right(ApplicationKey.valueOf(input));
        } else {
            return Either.left(input);
        }
    };
}
