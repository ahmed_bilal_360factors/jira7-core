package com.atlassian.jira.index.request;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Combines reindex requests into a smaller number that do the same job.
 *
 * @since 6.4
 */
public interface ReindexRequestCoalescer {
    /**
     * Attempts to combine a list of reindex requests into a fewer number.
     *
     * @param requests the requests to combine.
     * @return a new list of possibly fewer reindex requests.
     */
    @Nonnull
    public List<ReindexRequest> coalesce(@Nonnull List<ReindexRequest> requests);
}
