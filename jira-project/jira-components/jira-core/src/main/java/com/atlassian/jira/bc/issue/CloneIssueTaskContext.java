package com.atlassian.jira.bc.issue;

import com.atlassian.jira.task.NonExclusiveTaskContext;

/**
 * Task Context for cloning issues
 *
 * @since v7.1.1
 */
public class CloneIssueTaskContext extends NonExclusiveTaskContext {

    private static final long serialVersionUID = 27255699580945819L;

    private final Long originalIssueId;

    public CloneIssueTaskContext(Long originalIssueId) {
        super("/issue/CloneIssueProgress.jspa", "CloneIssue");
        this.originalIssueId = originalIssueId;
    }

    public Long getOriginalIssueId() {
        return originalIssueId;
    }
}
