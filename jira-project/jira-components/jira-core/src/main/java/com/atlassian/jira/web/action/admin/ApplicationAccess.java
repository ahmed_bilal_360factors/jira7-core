package com.atlassian.jira.web.action.admin;

import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.web.action.ActionViewDataMappings;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.ozymandias.PluginPointFunction;
import com.atlassian.ozymandias.SafePluginPointAccess;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.model.WebPanel;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.atlassian.jira.util.json.JSONEscaper.escape;


/**
 * Page for the ADMIN to do the read/update of {@link com.atlassian.jira.application.ApplicationRole}s.
 *
 * @since v6.3
 */
@WebSudoRequired
public class ApplicationAccess extends JiraWebActionSupport {
    private static final String DEFAPP_LOCATION = "webpanels.admin.defaultapp.selector";
    private static final String CLOSING_ANCHOR = "</a>";

    private final PageBuilderService pageBuilder;
    private final HelpUrls helpUrls;
    private final DynamicWebInterfaceManager dynamicWebInterfaceManager;

    public ApplicationAccess(PageBuilderService pageBuilder, HelpUrls helpUrls, final DynamicWebInterfaceManager dynamicWebInterfaceManager) {
        this.pageBuilder = pageBuilder;
        this.helpUrls = helpUrls;
        this.dynamicWebInterfaceManager = dynamicWebInterfaceManager;
    }

    @Override
    protected String doExecute() {
        pageBuilder.assembler().resources().requireWebResource("jira.webresources:application-roles-init");
        pageBuilder.assembler().data().requireData("com.atlassian.jira.web.action.admin.application-access:upgrade-jira-url", helpUrls.getUrl("upgrading").getUrl());
        pageBuilder.assembler().data().requireData("com.atlassian.jira.web.action.admin.application-access:reduce-user-count-url", helpUrls.getUrl("user_management.reduce.count").getUrl());
        pageBuilder.assembler().data().requireData("com.atlassian.jira.web.action.admin.application-access:defapp-selector-webpanels", escape(renderPanel(DEFAPP_LOCATION, ImmutableMap.of())));
        pageBuilder.assembler().data().requireData("com.atlassian.jira.web.action.admin.application-access:managing-groups-url", helpUrls.getUrl("jira-applications_access.managing_user_access_to_applications").getUrl());
        return SUCCESS;
    }

    @ActionViewDataMappings({"success"})
    public Map<String, Object> getDataMap() {
        final String helpHtml = getI18nHelper().getText("application.access.sidebar.help",
                getLink("user_management.groups"), CLOSING_ANCHOR,
                getLink("application_access"), CLOSING_ANCHOR);
        return ImmutableMap.of("helpHtml", helpHtml);
    }

    private String getLink(String key) {
        HelpUrl helpUrl = helpUrls.getUrl(key);
        return String.format("<a href=\"%s\" target=\"_blank\">", helpUrl.getUrl());
    }

    private String renderPanel(final String location, final ImmutableMap<String, Object> context) {
        return SafePluginPointAccess.call(() -> {
            final List<WebPanel> panels = dynamicWebInterfaceManager.getDisplayableWebPanels(location, ImmutableMap.of());
            return SafePluginPointAccess.to()
                    .modules(panels, (PluginPointFunction<ModuleDescriptor<WebPanel>, WebPanel, String>) (descriptor, webPanel) -> webPanel.getHtml(context))
                    .stream().collect(Collectors.joining());
        }).getOrElse("");
    }
}
