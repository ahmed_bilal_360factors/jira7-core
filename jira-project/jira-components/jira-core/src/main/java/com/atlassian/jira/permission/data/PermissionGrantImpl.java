package com.atlassian.jira.permission.data;

import com.atlassian.jira.permission.PermissionGrant;
import com.atlassian.jira.permission.PermissionHolder;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.google.common.base.Objects;

import javax.annotation.concurrent.Immutable;

import static com.google.common.base.Preconditions.checkNotNull;

@Immutable
public final class PermissionGrantImpl implements PermissionGrant {
    private final Long id;
    private final PermissionHolder holder;
    private final ProjectPermissionKey permission;

    public PermissionGrantImpl(final Long id, final PermissionHolder holder, final ProjectPermissionKey permission) {
        this.id = checkNotNull(id);
        this.holder = checkNotNull(holder);
        this.permission = checkNotNull(permission);
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public PermissionHolder getHolder() {
        return holder;
    }

    @Override
    public ProjectPermissionKey getPermission() {
        return permission;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PermissionGrantImpl that = (PermissionGrantImpl) o;

        return Objects.equal(this.id, that.id) &&
                Objects.equal(this.holder, that.holder) &&
                Objects.equal(this.permission, that.permission);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, holder, permission);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("holder", holder)
                .add("permission", permission)
                .toString();
    }
}
