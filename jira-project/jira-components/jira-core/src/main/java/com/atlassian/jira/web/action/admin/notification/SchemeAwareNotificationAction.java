package com.atlassian.jira.web.action.admin.notification;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.notification.NotificationType;
import com.atlassian.jira.notification.NotificationTypeManager;
import com.atlassian.jira.scheme.AbstractSchemeAwareAction;

public class SchemeAwareNotificationAction extends AbstractSchemeAwareAction {
    public NotificationType getType(String id) {
        return ComponentAccessor.getComponent(NotificationTypeManager.class).getNotificationType(id);
    }

    @Override
    public NotificationSchemeManager getSchemeManager() {
        return ComponentAccessor.getNotificationSchemeManager();
    }

    @Override
    public String getRedirectURL() {
        return null;
    }
}
