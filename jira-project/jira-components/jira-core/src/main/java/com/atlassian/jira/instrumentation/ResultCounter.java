package com.atlassian.jira.instrumentation;

/**
 * A tuple to store elapsed time in millis alongside the number of results observed for a search.
 */
public class ResultCounter {
    private final long elapsed;
    private final long results;

    public ResultCounter(long elapsed, long results) {
        this.elapsed = elapsed;
        this.results = results;
    }

    public long getElapsed() {
        return elapsed;
    }

    public long getResults() {
        return results;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ResultCounter)) return false;

        ResultCounter that = (ResultCounter) o;

        if (elapsed != that.elapsed) return false;
        return results == that.results;
    }

    @Override
    public int hashCode() {
        int result = (int) (elapsed ^ (elapsed >>> 32));
        result = 31 * result + (int) (results ^ (results >>> 32));
        return result;
    }
}