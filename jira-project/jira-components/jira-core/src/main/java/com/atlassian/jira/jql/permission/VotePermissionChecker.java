package com.atlassian.jira.jql.permission;

import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.issue.vote.VoteManager;
import com.atlassian.jira.user.ApplicationUser;

import java.util.Set;

/**
 * Checks if votes are enabled.
 *
 * @since v4.0
 */
public class VotePermissionChecker implements ClausePermissionChecker {
    private final VoteManager voteManager;

    public VotePermissionChecker(final VoteManager voteManager) {
        this.voteManager = voteManager;
    }

    public boolean hasPermissionToUseClause(final ApplicationUser user) {
        return voteManager.isVotingEnabled();
    }

    @Override
    public boolean hasPermissionToUseClause(ApplicationUser searcher, Set<FieldLayout> fieldLayouts) {
        return voteManager.isVotingEnabled();
    }
}
