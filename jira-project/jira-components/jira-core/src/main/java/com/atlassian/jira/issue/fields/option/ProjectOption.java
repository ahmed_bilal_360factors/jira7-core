package com.atlassian.jira.issue.fields.option;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.Function;
import com.atlassian.jira.util.collect.CollectionUtil;

import java.util.Collections;
import java.util.List;

public class ProjectOption extends AbstractOption implements Option {
    // ------------------------------------------------------------------------------------------------------- Constants
    // ------------------------------------------------------------------------------------------------- Type Properties
    private Project project;
    private List childOptions = Collections.emptyList();

    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    public ProjectOption(Project project) {
        this.project = project;
    }

    public ProjectOption(Project project, List childOptions) {
        this.project = project;
        if (childOptions != null) {
            this.childOptions = childOptions;
        }
    }

    // -------------------------------------------------------------------------------------------------- Public Methods
    public String getId() {
        return project != null ? project.getId().toString() : "";
    }

    public String getName() {
        return project.getName();
    }

    public String getDescription() {
        return project.getDescription();
    }

    public List getChildOptions() {
        return childOptions;
    }

    public static List<ProjectOption> transform(final Iterable<? extends Project> projects) {
        return CollectionUtil.transform(projects, new Function<Project, ProjectOption>() {
            @Override
            public ProjectOption get(final Project input) {
                return new ProjectOption(input);
            }
        });
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Helper Methods
}
