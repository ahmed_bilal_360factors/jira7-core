package com.atlassian.jira.config.group;

import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.atlassian.jira.ComponentManager;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Supplier;
import org.picocontainer.PicoContainer;

import javax.annotation.Nonnull;
import java.util.Set;

import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.collect.Sets.newHashSet;

/**
 * Utility class used to identify whether a group name is used in any configuration.
 * <p>
 * This is typically used to ensure that dormant configuration is not reactivated by creating a group.
 *
 * @since v7.0
 */
public class GroupConfigurationIdentifier {
    private final Supplier<PicoContainer> picoContainerSupplier;

    public GroupConfigurationIdentifier() {
        this.picoContainerSupplier = (() -> ComponentManager.getInstance().getContainer());
    }

    @VisibleForTesting
    GroupConfigurationIdentifier(final PicoContainer container) {
        this.picoContainerSupplier = (() -> container);
    }

    /**
     * Get all {@link GroupConfigurable} components each time so that when JIRA does restart the reference is not held
     * by this class.
     */
    private Set<GroupConfigurable> getGroupConfigurableComponents() {
        // getComponents returns the same object reference multiple times if its being registered via different interfaces.
        // Using Set to reduce duplicates.
        return newHashSet(picoContainerSupplier.get().getComponents(GroupConfigurable.class));
    }

    public boolean groupHasExistingConfiguration(@Nonnull final String groupName) {
        notNull("groupName", groupName);
        final ImmutableGroup group = new ImmutableGroup(groupName);
        return getGroupConfigurableComponents().stream()
                .anyMatch(groupConfigurable -> groupConfigurable.isGroupUsed(group));
    }

}