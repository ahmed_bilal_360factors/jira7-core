package com.atlassian.jira.issue.search.quicksearch;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.IssueConstant;

import java.util.Collection;
import java.util.Map;

public class PriorityQuickSearchHandler extends SingleWordQuickSearchHandler {
    public final ConstantsManager constantsManager;

    public PriorityQuickSearchHandler(ConstantsManager constantsManager) {
        this.constantsManager = constantsManager;
    }

    protected Map handleWord(String word, QuickSearchResult searchResult) {
        IssueConstant priorityByName = getPriorityByName(word);
        return priorityByName != null ? EasyMap.build("priority", priorityByName.getId()) : null;
    }

    private IssueConstant getPriorityByName(String name) {
        Collection<? extends IssueConstant> statuses = constantsManager.getPriorities();
        return getIssueConstantByName(statuses, name);
    }

}
