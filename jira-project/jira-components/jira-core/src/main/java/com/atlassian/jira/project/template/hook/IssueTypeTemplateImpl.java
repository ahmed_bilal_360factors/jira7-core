package com.atlassian.jira.project.template.hook;

import com.atlassian.plugin.Plugin;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JacksonInject;

import java.net.URL;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.nullToEmpty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IssueTypeTemplateImpl implements IssueTypeTemplate {
    private final String key;
    private final String name;
    private final String description;
    private final String iconPath;
    private final URL iconUrl;
    private final Optional<String> workflow;
    private final boolean isSubtask;
    private final Optional<String> screenScheme;
    private final Optional<String> avatar;

    public IssueTypeTemplateImpl(
            @JsonProperty("key") String key,
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("icon") String iconPath,
            @JsonProperty("workflow") String workflow,
            @JsonProperty("sub-task") Boolean isSubtask,
            @JsonProperty("screen-scheme") String screenScheme,
            @JsonProperty("avatar") String avatar,
            @JacksonInject("plugin") Plugin plugin) {
        this.key = checkNotNull(key).toUpperCase();
        this.name = checkNotNull(name);
        this.description = nullToEmpty(description);
        this.workflow = (workflow == null) ? Optional.empty() : Optional.of(workflow.toUpperCase());
        this.isSubtask = Boolean.TRUE.equals(isSubtask);
        this.iconPath = iconPath;
        this.iconUrl = getIconUrl(plugin);
        this.avatar = Optional.ofNullable(avatar);
        this.screenScheme = (screenScheme == null) ? Optional.empty() : Optional.of(screenScheme.toUpperCase());
    }

    @Override
    public String key() {
        return key;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String description() {
        return description;
    }

    @Override
    public String iconPath() {
        return iconPath;
    }

    @Override
    public URL iconUrl() {
        return iconUrl;
    }

    @Override
    public Optional<String> workflow() {
        return workflow;
    }

    @Override
    public String style() {
        if (isSubtask()) {
            return "jira_subtask";
        }

        return "";
    }

    @Override
    public boolean isSubtask() {
        return isSubtask;
    }

    @Override
    public Optional<String> screenScheme() {
        return screenScheme;
    }

    @Override
    public Optional<String> avatar() {
        return avatar;
    }

    private URL getIconUrl(Plugin plugin) {
        if (iconPath == null) {
            return null;
        }

        URL url = plugin.getResource(iconPath);
        if (url == null) {
            throw new IllegalArgumentException("Icon for issue type '" + key + "'not found on path specified: " + iconPath);
        }

        return url;
    }
}
