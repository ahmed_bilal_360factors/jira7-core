package com.atlassian.jira.cluster;

import java.util.Set;

/**
 * Provides access to cluster nodes.
 * @since v7.1.0
 */
public interface ClusterNodes {
    /**
     * Returns the current cluster node in JIRA.
     *
     * @return a non-null instance; call {@link com.atlassian.jira.cluster.Node#isClustered()} to see if it's part of a
     * cluster.
     */
    Node current();

    /**
     * Resets the cached reference to the current cluster node.
     */
    void reset();

    /**
     * Returns all the known nodes in a JIRA cluster. If not clustered this will return an empty set.
     *
     * @return a collection of {@link com.atlassian.jira.cluster.Node}s in a cluster.
     */
    Set<Node> all();

    /**
     * Returns the IP/hostname configured to be used for this JIRA Cluster.
     *
     * If none is configured, localhost is returned.
     *
     * @return The IP/hostname configured to be used for this JIRA Cluster.
     * @see com.atlassian.net.NetworkUtils#getLocalHostName()
     */
    String getHostname();
}
