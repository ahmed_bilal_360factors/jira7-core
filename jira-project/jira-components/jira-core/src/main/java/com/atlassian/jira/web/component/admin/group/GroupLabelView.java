package com.atlassian.jira.web.component.admin.group;

import org.codehaus.jackson.annotate.JsonAutoDetect;

/**
 * Group label title and description
 *
 * @since v7.0
 */
@JsonAutoDetect
public class GroupLabelView {
    private final String text;
    private final String title;
    private final LabelType type;

    public enum LabelType {
        ADMIN,
        SINGLE,
        MULTIPLE
    }

    public GroupLabelView(
            final String text,
            final String title,
            final LabelType type) {
        this.text = text;
        this.title = title;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public LabelType getType() {
        return type;
    }
}
