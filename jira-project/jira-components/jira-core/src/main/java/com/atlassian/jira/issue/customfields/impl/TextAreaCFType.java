package com.atlassian.jira.issue.customfields.impl;

import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.security.JiraAuthenticationContext;

import javax.annotation.Nonnull;

public class TextAreaCFType extends RenderableTextCFType {

    public TextAreaCFType(CustomFieldValuePersister customFieldValuePersister,
                          GenericConfigManager genericConfigManager, TextFieldCharacterLengthValidator textFieldCharacterLengthValidator, final JiraAuthenticationContext jiraAuthenticationContext) {
        super(customFieldValuePersister, genericConfigManager, textFieldCharacterLengthValidator, jiraAuthenticationContext);
    }

    @Nonnull
    protected PersistenceFieldType getDatabaseType() {
        return PersistenceFieldType.TYPE_UNLIMITED_TEXT;
    }

    @Override
    public Object accept(VisitorBase visitor) {
        if (visitor instanceof Visitor) {
            return ((Visitor) visitor).visitTextArea(this);
        }

        return super.accept(visitor);
    }

    public interface Visitor<T> extends VisitorBase<T> {
        T visitTextArea(TextAreaCFType textAreaCustomFieldType);
    }
}
