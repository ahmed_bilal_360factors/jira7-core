package com.atlassian.jira.event.project;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.RequestSourceType;

/**
 * Central dispatcher for project events
 *
 * @since v6.1
 */
public interface ProjectEventManager {
    /**
     * Dispatches {@link com.atlassian.jira.event.ProjectUpdatedEvent}.
     */
    void dispatchProjectUpdated(ApplicationUser user, Project newProject, Project oldProject, RequestSourceType requestSourceType);

    /**
     * Dispatched when the association between a project and a category is updated.
     * ONLY fired when the project was the trigger of the change.
     * @param requestSourceType
     */
    void dispatchProjectCategoryChanged(RequestSourceType requestSourceType);

    /**
     * Dispatches {@link com.atlassian.jira.event.ProjectCreatedEvent}.
     */
    void dispatchProjectCreated(ApplicationUser user, Project newProject);

    /**
     * Dispatches {@link com.atlassian.jira.event.ProjectDeletedEvent}.
     */
    void dispatchProjectDeleted(ApplicationUser user, Project oldProject);
}
