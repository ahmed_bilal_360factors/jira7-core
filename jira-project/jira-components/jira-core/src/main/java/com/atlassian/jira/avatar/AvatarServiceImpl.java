package com.atlassian.jira.avatar;

import com.atlassian.jira.avatar.Avatar.Size;
import com.atlassian.jira.cluster.ClusterSafe;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.EncodingConfiguration;
import com.atlassian.jira.icon.IconOwningObjectId;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.BaseUrl;
import com.atlassian.jira.util.DefaultBaseUrl;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.util.concurrent.LazyReference;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.opensymphony.module.propertyset.PropertySet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.net.URI;
import java.util.Optional;

import static com.google.common.base.Strings.emptyToNull;

/**
 * Implementation of the AvatarService. Uses AvatarPlugin module for user Avatars.
 *
 * @since v4.3
 */
@SuppressWarnings("UnusedParameters")
public class AvatarServiceImpl implements AvatarService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AvatarServiceImpl.class);
    private static final String HTTP_API = "http://www.gravatar.com/avatar/";
    private static final String HTTPS_API = "https://secure.gravatar.com/avatar/";

    private final UserManager userManager;
    private final AvatarManager avatarManager;
    private final UserPropertyManager userPropertyManager;
    private final VelocityRequestContextFactory velocityRequestContextFactory;
    private final ApplicationProperties applicationProperties;
    private final GravatarSettings gravatarSettings;
    private final BaseUrl baseUrl;
    private final EncodingConfiguration encodingConfiguration;

    @ClusterSafe
    private final LazyReference<Avatar.Size> defaultAvatarSize = new LazyReference<Avatar.Size>() {
        @Override
        protected Avatar.Size create() throws Exception {
            return Avatar.Size.defaultSize();
        }
    };

    /**
     * Injectable constructor.
     */
    @SuppressWarnings("UnusedDeclaration")
    public AvatarServiceImpl(
            UserManager userManager,
            AvatarManager avatarManager,
            UserPropertyManager userPropertyManager,
            VelocityRequestContextFactory velocityRequestContextFactory,
            ApplicationProperties applicationProperties,
            GravatarSettings gravatarSettings,
            EncodingConfiguration encodingConfiguration) {
        this.baseUrl = new DefaultBaseUrl(velocityRequestContextFactory);
        this.userManager = userManager;
        this.avatarManager = avatarManager;
        this.userPropertyManager = userPropertyManager;
        this.velocityRequestContextFactory = velocityRequestContextFactory;
        this.applicationProperties = applicationProperties;
        this.gravatarSettings = gravatarSettings;
        this.encodingConfiguration = encodingConfiguration;
    }

    private static ApplicationUser fromStaleUser(ApplicationUser user) {
        try {
            return user;
        } catch (IllegalStateException e) {
            return null;
        }
    }

    @Override
    public Avatar getAvatar(final ApplicationUser remoteUser, final String username) throws AvatarsDisabledException {
        ApplicationUser user = userManager.getUserByName(username);
        if (user == null) {
            Avatar anonymousAvatar = getAnonymousAvatar();
            LOGGER.debug("User with key '{}' does not exist, using anonymous avatar id {}", username, anonymousAvatar != null ? anonymousAvatar.getId() : null);
            return anonymousAvatar;
        }
        return getAvatarImpl(fromStaleUser(remoteUser), false, user, false);
    }

    private Avatar getAvatarImpl(final ApplicationUser remoteUser, boolean skipPermissionCheck, final ApplicationUser user, boolean tagged) {
        if (userManager.isUserExisting(user)) {
            // try to use the configured avatar
            Long customAvatarId = configuredAvatarIdFor(user);
            if (customAvatarId != null) {
                Avatar avatar = tagged ? avatarManager.getByIdTagged(customAvatarId) : avatarManager.getById(customAvatarId);
                if (avatar != null && (skipPermissionCheck || canViewAvatar(remoteUser, avatar))) {
                    return avatar;
                }
            }

            // fall back to the default user avatar
            Avatar defaultAvatar = getDefaultAvatar();
            LOGGER.debug("Avatar not configured for user '{}', using default id {}", user.getUsername(),
                    defaultAvatar != null ? defaultAvatar.getId() : null);

            return defaultAvatar;
        }

        Avatar anonymousAvatar = getAnonymousAvatar();
        LOGGER.debug("User is null, using anonymous avatar id {}", anonymousAvatar != null ? anonymousAvatar.getId() : null);

        return anonymousAvatar;
    }

    private boolean canViewAvatar(final ApplicationUser user, final Avatar avatar) {
        return avatarManager.userCanView(user, avatar);
    }

    /**
     * Returns the default avatar, if configured. Otherwise returns null.
     *
     * @return the default Avatar, or null
     */
    private Avatar getDefaultAvatar() {
        return avatarManager.getDefaultAvatar(IconType.USER_ICON_TYPE);
    }

    @Override
    public Avatar getAvatar(final ApplicationUser remoteUser, final ApplicationUser avatarUser) throws AvatarsDisabledException {
        if (avatarUser == null) {
            return getAnonymousAvatar();
        }
        return getAvatarImpl(remoteUser, false, avatarUser, false);
    }

    @Override
    public Avatar getAvatarTagged(final ApplicationUser remoteUser, final ApplicationUser avatarUser)
            throws AvatarsDisabledException {
        if (avatarUser == null) {
            return getAnonymousAvatar();
        }
        return getAvatarImpl(remoteUser, false, avatarUser, true);
    }

    @Override
    public URI getAvatarURL(final ApplicationUser remoteUser, final String username) throws AvatarsDisabledException {
        return getAvatarURL(fromStaleUser(remoteUser), userManager.getUserByName(username));
    }

    @Override
    public Optional<URI> getGravatarAvatarURL(final ApplicationUser avatarUser, final Avatar.Size size) {
        // JRADEV-12195: email should not be null, but we have seen it in the wild (EAC/J connected to Crowd).
        if (isGravatarEnabled() && avatarUser != null && avatarUser.getEmailAddress() != null) {
            // JRA-28913: Must use lower-case to get the correct hash
            final String hash = MD5Util.md5Hex(avatarUser.getEmailAddress().toLowerCase());
            final String apiAddress = MoreObjects.firstNonNull(emptyToNull(gravatarSettings.getCustomApiAddress()), useSSL() ? HTTPS_API : HTTP_API);

            // JRA-29934: since September 2012, gravatar.com no longer handles the d= query parameter like it used
            // to, which breaks default avatars for non-publicly accessible JIRA instances. so we just use the
            // "mystery man" avatar provided by gravatar.com.
            return Optional.of(URI.create(apiAddress + hash + "?d=mm&s=" + Integer.toString(size.getPixels())));
        }
        return Optional.empty();
    }

    /**
     * @return whether we should use the Gravatar SSL servers or not
     */
    private boolean useSSL() {
        String baseURL = velocityRequestContextFactory.getJiraVelocityRequestContext().getCanonicalBaseUrl();

        try {
            return "https".equalsIgnoreCase(URI.create(baseURL).getScheme());
        } catch (Exception e) {
            // base URL is messed up, nothing we can do about it here...
            return false;
        }
    }

    /**
     * Interface for avatar URL building strategy.
     */
    private interface UrlStrategy {
        URI get(ApplicationUser remoteUser, ApplicationUser user, @Nonnull Avatar.Size size);
    }

    @Override
    public URI getAvatarURL(final ApplicationUser remoteUser, final ApplicationUser avatarUser) throws AvatarsDisabledException {
        return getAvatarURLImpl(remoteUser, false, avatarUser, defaultAvatarSize.get());
    }

    private URI getAvatarURLImpl(final ApplicationUser remoteUser, final boolean skipPermissionCheck, final ApplicationUser avatarUser, Size size) {
        final boolean useGravatars = isUsingExternalAvatar(remoteUser, avatarUser);

        UrlStrategy urlStrategy = useGravatars ? new GravatarUrlStrategy() : new JiraUrlStrategy(skipPermissionCheck);

        return urlStrategy.get(remoteUser, avatarUser, size != null ? size : defaultAvatarSize.get());
    }

    @Override
    public URI getAvatarURL(final ApplicationUser remoteUser, final String username, final Avatar.Size size) throws AvatarsDisabledException {
        return getAvatarURLImpl(fromStaleUser(remoteUser), false, userManager.getUserByName(username), size);
    }

    @Override
    public URI getAvatarURL(final ApplicationUser remoteUser, final ApplicationUser avatarUser, final Avatar.Size size)
            throws AvatarsDisabledException {
        return getAvatarURLImpl(remoteUser, false, avatarUser, size);
    }

    @Override
    public URI getAvatarUrlNoPermCheck(final String username, final Avatar.Size size) throws AvatarsDisabledException {
        return getAvatarURLImpl(null, true, userManager.getUserByName(username), size);
    }

    @Override
    public URI getAvatarUrlNoPermCheck(final ApplicationUser avatarUser, final Avatar.Size size) throws AvatarsDisabledException {
        return getAvatarURLImpl(null, true, avatarUser, size);
    }

    /**
     * Builds a URI for a JIRA avatar with the requested size.
     *
     * @param avatarUser the ApplicationUser whose avatar we'd like to display
     * @param avatar     the Avatar whose URI we want
     * @param size       the size in which the avatar should be displayed
     * @return a URI that can be used to display the avatar
     */
    public URI getAvatarUrlNoPermCheck(final ApplicationUser avatarUser, final Avatar avatar, @Nonnull final Size size) {
        if (useGravatarFor(avatar)) {
            return new GravatarUrlStrategy().get(avatarUser, avatarUser, size);
        }

        return buildUriForAvatar(avatar, size);
    }

    @Override
    public URI getAvatarAbsoluteURL(ApplicationUser remoteUser, String username, Avatar.Size size)
            throws AvatarsDisabledException {
        return getAvatarURLImpl(fromStaleUser(remoteUser), false, userManager.getUserByName(username), size);
    }

    @Override
    public URI getAvatarAbsoluteURL(ApplicationUser remoteUser, ApplicationUser avatarUser, Avatar.Size size)
            throws AvatarsDisabledException {
        return getAvatarURLImpl(remoteUser, false, avatarUser, size);
    }

    @Override
    public boolean hasCustomUserAvatar(ApplicationUser remoteUser, String username) {
        ApplicationUser user = userManager.getUserByName(username);
        if (user == null) {
            throw new IllegalArgumentException(String.format("User '%s' does not exist", username));
        }
        return hasCustomUserAvatar(fromStaleUser(remoteUser), user);
    }

    @Override
    public boolean hasCustomUserAvatar(ApplicationUser remoteUser, ApplicationUser user) {
        // In addition to no configured avatar, if the user's avatar is external, it is considered "custom".
        // This should be finessed into the real question which is "can I have the edit link for this?" which should
        // be encapsulated by the avatar support plugin moduletype. Bonus is that question would work for admin use case
        // As of end of 2014 this belongs to the Identity team's backlog and the Atlassian ID project's ongoing work on
        // cross-product avatar APIs. Delete this comment by the end of 2016 if everything has changed again - christo
        return remoteUser != null && (configuredAvatarIdFor(user) != null || isUsingExternalAvatar(remoteUser, user));
    }

    @Override
    public void setCustomUserAvatar(ApplicationUser remoteUser, String username, Long avatarId)
            throws AvatarsDisabledException, NoPermissionException {
        ApplicationUser user = userManager.getUserByName(username);
        if (user == null) {
            throw new IllegalArgumentException(String.format("User '%s' does not exist", username));
        }
        setCustomUserAvatar(fromStaleUser(remoteUser), user, avatarId);
    }

    @Override
    public void setCustomUserAvatar(ApplicationUser remoteUser, ApplicationUser user, Long avatarId)
            throws AvatarsDisabledException, NoPermissionException {
        if (!canSetCustomUserAvatar(remoteUser, user)) {
            throw new NoPermissionException();
        }

        setConfiguredAvatarIdFor(user, avatarId);
    }

    @Override
    public boolean canSetCustomUserAvatar(ApplicationUser remoteUser, String username) {
        ApplicationUser user = userManager.getUserByName(username);
        if (user == null) {
            throw new IllegalArgumentException(String.format("User '%s' does not exist", username));
        }
        return canSetCustomUserAvatar(fromStaleUser(remoteUser), user);
    }

    @Override
    public boolean canSetCustomUserAvatar(ApplicationUser remoteUser, ApplicationUser user) {
        // TODO Consider not using UserIconType for Avatar management, and so manage permissions independently
        // from icon permission management.
        IconType iconType = IconType.USER_ICON_TYPE;
        return avatarManager.userCanCreateFor(remoteUser, iconType, new IconOwningObjectId(user.getKey()));
    }

    @Override
    public URI getProjectAvatarURL(final Project project, final Avatar.Size size) {
        final String baseUrl = velocityRequestContextFactory.getJiraVelocityRequestContext().getBaseUrl();
        return getProjectAvatarURLImpl(project, size, baseUrl);
    }

    @Override
    public URI getProjectAvatarAbsoluteURL(final Project project, final Avatar.Size size) {
        final String baseUrl = velocityRequestContextFactory.getJiraVelocityRequestContext().getCanonicalBaseUrl();
        return getProjectAvatarURLImpl(project, size, baseUrl);
    }

    private URI getProjectAvatarURLImpl(final Project project, final Avatar.Size size, final String baseUrl) {
        final Avatar avatar = project.getAvatar();
        final Long avatarId = avatar == null ? null : avatar.getId();

        if (avatarId.equals(avatarManager.getDefaultAvatarId(IconType.PROJECT_ICON_TYPE))) {
            return getProjectDefaultAvatarURLImpl(size, baseUrl);
        }

        UrlBuilder urlBuilder = new UrlBuilder(baseUrl + "/secure/projectavatar", applicationProperties.getEncoding(), false);

        if (size != null && !size.isDefault) {
            urlBuilder.addParameter("size", size.param);
        }

        urlBuilder.addParameter("pid", project.getId());

        urlBuilder.addParameter("avatarId", avatarId.toString());

        return urlBuilder.asURI();
    }

    @Override
    public URI getProjectDefaultAvatarURL(final Avatar.Size size) {
        final String baseUrl = velocityRequestContextFactory.getJiraVelocityRequestContext().getBaseUrl();
        return getProjectDefaultAvatarURLImpl(size, baseUrl);
    }

    @Override
    public URI getProjectDefaultAvatarAbsoluteURL(final Avatar.Size size) {
        final String baseUrl = velocityRequestContextFactory.getJiraVelocityRequestContext().getCanonicalBaseUrl();
        return getProjectDefaultAvatarURLImpl(size, baseUrl);
    }

    private URI getProjectDefaultAvatarURLImpl(final Avatar.Size size, final String baseUrl) {
        UrlBuilder urlBuilder = new UrlBuilder(baseUrl + "/secure/projectavatar", applicationProperties.getEncoding(), false);

        if (size != null && !size.isDefault) {
            urlBuilder.addParameter("size", size.param);
        }

        final Long defaultAvatarId = avatarManager.getDefaultAvatarId(IconType.PROJECT_ICON_TYPE);
        if (defaultAvatarId != null) {
            urlBuilder.addParameter("avatarId", defaultAvatarId.toString());
        }

        return urlBuilder.asURI();
    }

    /**
     * Returns the avatar id that is configured for the given User. If the user has not configured an avatar, this
     * method returns null.
     *
     * @param user the user whose avatar we want
     * @return an avatar id, or null
     */
    protected Long configuredAvatarIdFor(ApplicationUser user) {
        PropertySet userProperties = userPropertyManager.getPropertySet(user);
        if (userProperties.exists(AvatarManager.USER_AVATAR_ID_KEY)) {
            long avatarId = userProperties.getLong(AvatarManager.USER_AVATAR_ID_KEY);
            LOGGER.debug("Avatar configured for user '{}' is {}", user.getUsername(), avatarId);

            return avatarId;
        }

        return null;
    }

    /**
     * Returns true if Gravatar support is enabled.
     *
     * @return a boolean indicating whether Gravatar support is on
     */
    @Override
    public boolean isGravatarEnabled() {
        return gravatarSettings.isAllowGravatars();
    }

    @Override
    public boolean isUsingExternalAvatar(final ApplicationUser remoteUser, final ApplicationUser avatarUser) {
        if (isGravatarEnabled()) {
            Avatar avatar = getAvatarImpl(remoteUser, true, avatarUser, false);

            return useGravatarFor(avatar);
        }

        return false;
    }

    /**
     * Whether to use Gravatar instead of an internal Avatar.
     *
     * @param avatar an Avatar
     * @return true if JIRA should use a Gravatar instead of the given internal Avatar.
     */
    private boolean useGravatarFor(final Avatar avatar) {
        Avatar defaultAvatar = getDefaultAvatar();

        return isGravatarEnabled() && (avatar == null || Objects.equal(avatar, defaultAvatar));
    }

    /**
     * Sets the given avatar id as the configured avatar id for a user.
     *
     * @param user     the User whose avatar is being configured
     * @param avatarId the avatar id to configure
     */
    protected void setConfiguredAvatarIdFor(ApplicationUser user, Long avatarId) {
        PropertySet userProperties = userPropertyManager.getPropertySet(user);
        userProperties.setLong(AvatarManager.USER_AVATAR_ID_KEY, avatarId);
        LOGGER.debug("Set configured avatar id for user '{}' to {}", user.getUsername(), avatarId);
    }

    /**
     * Returns the anonymous avatar, if configured. Otherwise returns null.
     *
     * @return the anonymous avatar, or null
     */
    protected Avatar getAnonymousAvatar() {
        Long anonAvatarId = avatarManager.getAnonymousAvatarId();

        return anonAvatarId != null ? avatarManager.getById(anonAvatarId) : null;
    }

    /**
     * Build avatar URLs that point to Gravatar avatars. If the user does not exist, we serve the anonymous avatar
     * directly. If the user has not configured Gravatar, they will get the default avatar provided by Gravatar.
     */
    public class GravatarUrlStrategy implements UrlStrategy {
        @Override
        public URI get(ApplicationUser remoteUser, ApplicationUser user, @Nonnull Avatar.Size size) {
            return getGravatarAvatarURL(user, size).orElseGet(() ->
                    buildUriForAvatar(getAnonymousAvatar(), size));
        }
    }

    /**
     * Builds a URI to JIRA's avatar servlet for a given JIRA avatar, with the requested size.
     *
     * @param avatar      the Avatar whose URI we want
     * @param size        the size in which the avatar should be displayed
     * @return a URI that can be used to display the avatar
     */
    private URI buildUriForAvatar(Avatar avatar, @Nonnull Size size) {
        String base = baseUrl.getCanonicalBaseUrl();
        UrlBuilder builder = new UrlBuilder(base + "/secure/useravatar", encodingConfiguration.getEncoding(), false);

        if (size != Avatar.Size.defaultSize()) {
            builder.addParameter("size", size.getParam());
        }

        String ownerId = avatar != null ? avatar.getOwner() : null;
        if (ownerId != null) {
            builder.addParameter("ownerId", ownerId);
        }

        // optional avatarId
        Long avatarId = avatar != null ? avatar.getId() : null;
        if (avatarId != null) {
            builder.addParameter("avatarId", avatarId.toString());
        }

        return builder.asURI();
    }

    /**
     * Build avatar URLs that point to JIRA avatars.
     */
    private class JiraUrlStrategy implements UrlStrategy {
        private final boolean skipPermissionCheck;

        JiraUrlStrategy(boolean skipPermissionCheck) {
            this.skipPermissionCheck = skipPermissionCheck;
        }

        @Override
        public URI get(ApplicationUser remoteUser, ApplicationUser user, @Nonnull Avatar.Size size) {
            Avatar avatar = getAvatarImpl(remoteUser, skipPermissionCheck, user, false);

            return buildUriForAvatar(avatar, size);
        }
    }
}
