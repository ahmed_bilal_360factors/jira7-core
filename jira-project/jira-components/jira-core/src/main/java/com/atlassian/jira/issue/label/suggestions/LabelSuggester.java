package com.atlassian.jira.issue.label.suggestions;

import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.index.indexers.impl.CustomFieldLabelsIndexer;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class LabelSuggester {
    private final PopularLabelsProvider popularLabelsProvider;
    private final PrefixSearchLabelsProvider prefixSearchLabelsProvider;

    public LabelSuggester(final PopularLabelsProvider popularLabelsProvider, final PrefixSearchLabelsProvider prefixSearchLabelsProvider) {
        this.popularLabelsProvider = popularLabelsProvider;
        this.prefixSearchLabelsProvider = prefixSearchLabelsProvider;
    }

    public Set<String> getSuggestedLabels(final String token, final Long issueId, final Set<Label> issueLabels,
                                          final ApplicationUser user) {
        return getSuggestions(
                token,
                issueId,
                issueLabels,
                DocumentConstants.ISSUE_LABELS_FOLDED,
                DocumentConstants.ISSUE_LABELS,
                user
        );
    }

    public Set<String> getSuggestedLabels(final String token, final Long issueId, final Long customFieldId,
                                          final Set<Label> issueLabels, final ApplicationUser user) {
        final String searchField = CustomFieldUtils.CUSTOM_FIELD_PREFIX + customFieldId;

        return getSuggestions(
                token,
                issueId,
                issueLabels,
                searchField + CustomFieldLabelsIndexer.FOLDED_EXT,
                searchField,
                user
        );
    }

    private Set<String> getSuggestions(final String token, final Long issueId, final Set<Label> issueLabels,
                                       final String searchField, final String displayField, final ApplicationUser user) {
        final Set<String> suggestions;

        if (StringUtils.isEmpty(token)) {
            suggestions = popularLabelsProvider.findMostPopular(displayField, user);
        } else {
            suggestions = prefixSearchLabelsProvider.findByPrefixToken(token, searchField, displayField, user);
        }

        return postProcessSuggestions(issueId, suggestions, issueLabels, token);
    }

    private Set<String> postProcessSuggestions(final Long issueId, final Set<String> suggestions, final Set<Label> issueLabels, final String token) {
        Set<String> result = suggestions;

        if (issueId != null) {
            result = removeSuggestionsFoundInIssueLabels(
                    suggestions,
                    issueLabels
                            .stream()
                            .map(Label::getLabel)
                            .collect(Collectors.toSet())
            );
        }

        if (StringUtils.isEmpty(token)) {
            return result;
        }

        return orderSuggestionsByMatchingTokenFirst(result, token);
    }

    private Set<String> removeSuggestionsFoundInIssueLabels(final Set<String> suggestions, final Set<String> issueLabels) {
        return Sets.filter(suggestions, suggestion -> !issueLabels.contains(suggestion));
    }

    private Set<String> orderSuggestionsByMatchingTokenFirst(final Set<String> suggestions, final String token) {
        Set<String> orderedSuggestions = new LinkedHashSet<>(suggestions.size());

        orderedSuggestions.addAll(
                Sets.filter(suggestions, label -> label.startsWith(token))
        );
        orderedSuggestions.addAll(
                Sets.filter(suggestions, label -> !label.startsWith(token))
        );

        return orderedSuggestions;
    }
}
