package com.atlassian.jira.plugin.freeze;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent;
import com.atlassian.jira.cluster.zdu.DefaultClusterUpgradePluginLoaderFactory;
import com.atlassian.plugin.loaders.RosterFileScanner;
import com.google.common.base.Charsets;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent.Action.FILE_ADD;
import static com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent.Action.FILE_DELETED;
import static com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent.FileType.PLUGIN;
import static com.google.common.io.Files.asCharSink;
import static com.google.common.io.Files.asCharSource;
import static java.nio.file.Files.delete;
import static java.nio.file.Files.list;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

/**
 * This manager handles the creation and deletion of the {@link RosterFileScanner} file as well as deletion of plugins
 * that are not listed in this file.
 *
 * @since v7.3
 * @see RosterFileScanner
 * @see SwitchingScanner
 * @see DefaultClusterUpgradePluginLoaderFactory
 */
public class FreezeFileManager {
    private final EventPublisher eventPublisher;
    private final File freezeFile;
    private final File installedPluginsDirectory;

    FreezeFileManager(final EventPublisher eventPublisher, final File freezeFile, final File installedPluginsDirectory) {
        this.eventPublisher = eventPublisher;
        this.freezeFile = freezeFile;
        this.installedPluginsDirectory = installedPluginsDirectory;
    }

    /**
     * @throws RuntimeException when creation of the freeze file fails
     */
    public void freeze() {
        try {
            Stream<CharSequence> fileContent = getInstalledPlugins()
                    .map(Path::toAbsolutePath)
                    .map(freezeFile.getParentFile().toPath()::relativize)
                    .map(Path::toString);
            asCharSink(freezeFile, Charsets.UTF_8).writeLines(fileContent::iterator, "\n");
            eventPublisher.publish(new JiraHomeChangeEvent(FILE_ADD, PLUGIN, freezeFile));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @throws RuntimeException when the deletion of the freeze file fails
     */
    public void unfreeze() {
        try {
            delete(freezeFile.toPath());
            eventPublisher.publish(new JiraHomeChangeEvent(FILE_DELETED, PLUGIN, freezeFile));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @throws RuntimeException when the deletion of the freeze file fails
     */
    public void removeUnfrozenPlugins() {
        try {
            Set<String> frozenPlugins = asCharSource(freezeFile, Charsets.UTF_8)
                    .readLines()
                    .stream()
                    .collect(toSet());
            List<Path> unfrozenPlugins = getInstalledPlugins()
                    .filter(plugin -> !frozenPlugins.contains(freezeFile.getParentFile().toPath().relativize(plugin).toString()))
                    .collect(toList());
            for (Path unfrozenPlugin : unfrozenPlugins) {
                Files.delete(unfrozenPlugin);
                eventPublisher.publish(new JiraHomeChangeEvent(FILE_DELETED, PLUGIN, unfrozenPlugin.toFile()));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private Stream<Path> getInstalledPlugins() throws IOException {
        return list(installedPluginsDirectory.toPath());
    }
}
