package com.atlassian.jira.avatar;

/**
 * Static utility methods used for creating avatar filenames.
 */
public class AvatarFilenames {
    public static String getAvatarFilename(Avatar avatar, Avatar.Size size) {
        return getAvatarFilename(avatar.getId(), avatar.getFileName(), size);
    }

    public static String getAvatarFilename(long id, String filename, Avatar.Size size) {
        return id + "_" + AvatarFilenames.getFilenameFlag(size) + filename;
    }

    public static String getFilenameFlag(Avatar.Size size) {
        return size.isDefault ? "" : size.getParam() + "_";
    }


}
