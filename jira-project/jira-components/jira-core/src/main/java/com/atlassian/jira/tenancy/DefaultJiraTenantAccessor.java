package com.atlassian.jira.tenancy;

import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.bc.dataimport.ImportCompletedEvent;
import com.atlassian.jira.bc.dataimport.ImportStartedEvent;
import com.atlassian.jira.util.ComponentLocator;
import com.atlassian.tenancy.api.Tenant;
import com.atlassian.tenancy.api.TenantContext;
import com.atlassian.tenancy.api.TenantUnavailableException;
import com.atlassian.tenancy.api.UnexpectedTenantChangeException;
import com.atlassian.util.concurrent.ResettableLazyReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.concurrent.Callable;

/**
 * Implementation of TenantAccessor for InstantOn - only has a system tenant
 *
 * @since v6.4
 */
@EventComponent
public class DefaultJiraTenantAccessor implements JiraTenantAccessor {
    private final static Logger log = LoggerFactory.getLogger(DefaultJiraTenantAccessor.class);

    private final TenantContext tenantContext;
    private final ComponentLocator componentLocator;

    private Tenant systemTenant;
    private ResettableLazyReference<Iterable<Tenant>> tenantIterableRef = new ResettableLazyReference<Iterable<Tenant>>() {

        @Override
        protected Iterable<Tenant> create() throws Exception {
            initialiseSystemTenant();
            if (systemTenant == null) {
                return Collections.emptySet();
            } else {
                return Collections.singleton(systemTenant);
            }
        }
    };
    private volatile boolean doingImport;
    private boolean initialised;

    public DefaultJiraTenantAccessor(final TenantContext tenantContext, final ComponentLocator componentLocator) {
        this.tenantContext = tenantContext;
        this.componentLocator = componentLocator;
    }

    @Override
    public Iterable<Tenant> getAvailableTenants() {
        return tenantIterableRef.get();
    }

    @Override
    public <T> T asTenant(Tenant tenant, Callable<T> call) throws TenantUnavailableException, InvocationTargetException {
        Tenant currentTenant = tenantContext.getCurrentTenant();
        if (currentTenant == null) {
            log.warn("You are not associated with a tenant, so cannot call tenant specific code");
            throw new TenantUnavailableException();
        }
        if (tenant != currentTenant) {
            log.warn("You cannot invoke a runnable in another tenant's context");
            throw new UnexpectedTenantChangeException();
        }
        try {
            return call.call();
        } catch (Exception e) {
            throw new InvocationTargetException(e);
        }
    }

    @Override
    public synchronized void addTenant(final Tenant tenant) {
        if (doingImport) {
            throw new IllegalStateException("Cannot add tenant while doing an import");
        }
        if (systemTenant == null) {
            systemTenant = tenant;
            tenantIterableRef.reset();
        } else {
            throw new IllegalArgumentException("The system is already tenanted");
        }
    }

    @EventListener
    public void onImportStarted(@SuppressWarnings("UnusedParameters") ImportStartedEvent importStartedEvent) {
        doingImport = true;
    }

    @EventListener
    public void onImportCompleted(@SuppressWarnings("UnusedParameters") ImportCompletedEvent importCompletedEvent) {
        doingImport = false;
    }

    private void initialiseSystemTenant() {
        if (!initialised && systemTenant == null) {
            final TenancyCondition tenancyCondition = componentLocator.getComponent(TenancyCondition.class);
            if (!tenancyCondition.isEnabled()) {
                systemTenant = new JiraTenantImpl("system");
            }
        }
        initialised = true;
    }
}
