package com.atlassian.jira.web.sitemesh;

import com.atlassian.jira.admin.ProjectAdminSidebarFeature;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.component.ComponentAccessor;
import com.google.common.collect.ImmutableList;
import com.opensymphony.module.sitemesh.Config;
import com.opensymphony.module.sitemesh.Decorator;
import com.opensymphony.module.sitemesh.DecoratorMapper;
import com.opensymphony.module.sitemesh.Page;
import com.opensymphony.module.sitemesh.mapper.AbstractDecoratorMapper;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Properties;

/**
 * Defines when the project-centric sidebar should be loaded in the admin decorator.
 */
public class ProjectAdminDecoratorMapper extends AbstractDecoratorMapper {
    private static final List<String> ADMIN_DECORATOR_IDENTIFICATIONS = ImmutableList.of("admin", "atl.admin");
    private static final String DECORATOR_PARAM = "meta.decorator";
    private static final String ACTIVE_ADMIN_SECTION = "meta.admin.active.section";
    private static final String ACTIVE_ADMIN_SECTION_VALUE = "atl.jira.proj.config";
    private static final String META_PROJECT_KEY = "meta.projectKey";
    private static final String LOAD_SIDEBAR_KEY = "shouldLoadSidebar";

    public void init(Config config, Properties properties, DecoratorMapper parent) throws InstantiationException {
        super.init(config, properties, parent);
    }

    public Decorator getDecorator(HttpServletRequest request, Page page) {
        request.setAttribute(LOAD_SIDEBAR_KEY, "false");

        if (page != null) {
            request.setAttribute(ProjectService.PROJECT_KEY, page.getProperty(META_PROJECT_KEY));
            // load project-centric sidebar in case we are about to display any of the project settings pages
            if (shouldDisplaySidebar()
                    && ADMIN_DECORATOR_IDENTIFICATIONS.contains(page.getProperty(DECORATOR_PARAM))
                    && ACTIVE_ADMIN_SECTION_VALUE.equals(page.getProperty(ACTIVE_ADMIN_SECTION))
                    ) {
                request.setAttribute(LOAD_SIDEBAR_KEY, "true");
            }
        }

        return super.getDecorator(request, page);
    }

    private boolean shouldDisplaySidebar() {
        return ComponentAccessor.getComponentSafely(ProjectAdminSidebarFeature.class)
                .map(ProjectAdminSidebarFeature::shouldDisplay)
                .orElse(false);
    }
}
