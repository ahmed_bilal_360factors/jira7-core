package com.atlassian.jira.issue.customfields.impl;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.export.FieldExportPartsBuilder;
import com.atlassian.jira.issue.export.customfield.CustomFieldExportContext;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.issue.export.customfield.ExportableCustomFieldType;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.bean.BulkEditBean;

public class ReadOnlyCFType extends GenericTextCFType implements ExportableCustomFieldType {

    public ReadOnlyCFType(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager, TextFieldCharacterLengthValidator textFieldCharacterLengthValidator, final JiraAuthenticationContext jiraAuthenticationContext) {
        super(customFieldValuePersister, genericConfigManager, textFieldCharacterLengthValidator, jiraAuthenticationContext);
    }

    public void updateValue(CustomField customField, Issue issue, String value) {
        // Don't clear the read-only field when the update request
        // didn't contain value for it. Update requests from edit
        // issue dialog never contain read-only fields.
        if (value != null) {
            super.updateValue(customField, issue, value);
        }
    }

    public String getChangelogValue(CustomField field, String value) {
        if (value != null) {
            return super.getChangelogValue(field, value);
        } else {
            return null;
        }
    }

    // Read only - not editable
    public String availableForBulkEdit(BulkEditBean bulkEditBean) {
        return "bulk.edit.unavailable";
    }

    @Override
    public Object accept(VisitorBase visitor) {
        if (visitor instanceof Visitor) {
            return ((Visitor) visitor).visitReadOnly(this);
        }

        return super.accept(visitor);
    }

    public boolean isRenderable() {
        return true;
    }

    @Override
    public FieldExportParts getRepresentationFromIssue(Issue issue, CustomFieldExportContext context) {
        final String value = getValueFromIssue(context.getCustomField(), issue);
        return FieldExportPartsBuilder.buildSinglePartRepresentation(context.getCustomField().getId(), context.getDefaultColumnHeader(), value);
    }

    public interface Visitor<T> extends VisitorBase<T> {
        T visitReadOnly(ReadOnlyCFType readOnlyCustomFieldType);
    }
}
