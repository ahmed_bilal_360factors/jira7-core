package com.atlassian.jira.database;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.google.common.collect.Sets;

import java.util.Collection;
import java.util.HashSet;

/**
 * JIRA officially supports a small number of database collations. They are:
 * <ul>
 * <li>Oracle: BINARY</li>
 * <li>Mysql: utf8_bin</li>
 * <li>Postgres: C or POSIX</li>
 * <li>SQL Server: SQL_Latin1_General_CP437_CI_AI or SQL_Latin1_General_CI_AI</li>
 * <li>HSQL: Any collation</li>
 * <li>H2: Any collation</li>
 * </ul>
 */
public class SupportedCollations {
    private static final Collection<String> supportedOracleCollations = Sets.newHashSet("BINARY");
    private static final Collection<String> supportedMySqlCollations = Sets.newHashSet("utf8_bin");
    private static final Collection<String> supportedPostgresCollations = Sets.newHashSet("C", "C.UTF-8", "POSIX", "POSIX.UTF-8");
    private static final Collection<String> supportedSqlServerCollations = Sets.newHashSet("SQL_Latin1_General_CP437_CI_AI", "SQL_Latin1_General_CI_AI");

    /**
     * @return true if the collation is supported by JIRA
     */
    public static boolean isSupported(DatabaseConfig databaseConfig, String collation) {
        if (databaseConfig.isHSql() || databaseConfig.isH2()) {
            return true;
        }

        Collection<String> supportedCollations = forDatabase(databaseConfig);

        return supportedCollations.contains(collation);
    }

    /**
     * @return the collection of collations supported for the given database config
     */
    public static Collection<String> forDatabase(DatabaseConfig databaseConfig) {
        if (databaseConfig.isOracle()) {
            return supportedOracleCollations;
        } else if (databaseConfig.isMySql()) {
            return supportedMySqlCollations;
        } else if (databaseConfig.isPostgres()) {
            return supportedPostgresCollations;
        } else if (databaseConfig.isSqlServer()) {
            return supportedSqlServerCollations;
        }

        return new HashSet<String>();
    }
}