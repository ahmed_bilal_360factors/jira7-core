package com.atlassian.jira.web.filters;

import com.atlassian.core.filters.AbstractHttpFilter;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.startup.InstantUpgradeManager;
import com.atlassian.jira.web.util.CloudControlIPCheck;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Stopwatch;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * InstantUpgradeHoldingFilter is used to hold requests while performing instant upgrade startup.
 * It holds requests for maximum of s and releases them when the instance has finished starting up.
 * If timeout pass the request returns 510 status code.
 * 
 * Timeout is defined by holding.filter.timeout.seconds {@link com.atlassian.jira.config.properties.JiraProperties}
 * and defaults to 55s
 *
 * The aim is also to prevent data inconsistencies that may occur if a request is handled before all plugins are
 * started. For example, we attempt to save an issue before all plugins have contributed their custom fields.
 */
public class InstantUpgradeHoldingFilter extends AbstractHttpFilter {

    private static final String HOLDING_FILTER_TIMEOUT_KEY = "holding.filter.timeout.seconds";
    private static final long HOLDING_FILTER_TIMEOUT_SECONDS = 55;
    private static final int SMART_STATUS_TAKE_THE_WHEEL = 510;
    private static final String REQUEST_HELD_TIME = "jira.instant.holdingfilter.millis";
    private static final String REQUEST_METHOD = "jira.instant.holdingfilter.method";
    private static final String REQUEST_HAS_TIMEDOUT = "jira.instant.holdingfilter.hastimedout";

    private static final Logger log = LoggerFactory.getLogger(InstantUpgradeHoldingFilter.class);

    private final AtomicReference<InstantUpgradeManager.State> applicationState = new AtomicReference<>();

    // The paths that will be allowed to proceed
    private Set<String> permittedRequestPaths;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        super.init(filterConfig);
        permittedRequestPaths = new HashSet<>();
        String paths = filterConfig.getInitParameter("permittedPaths");
        if (!StringUtils.isEmpty(paths)) {
            Arrays.asList(paths.split(",")).stream().map(arg->arg.trim()).forEach(permittedRequestPaths::add);
        }
    }

    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {

        if (isFullContainerInitialised() && isHoldingRequests() && shouldHoldThisRequest(request)) {

            InstantUpgradeManager instantUpgradeManager = ComponentAccessor.getComponent(InstantUpgradeManager.class);
            JiraProperties jiraProperties = ComponentAccessor.getComponent(JiraProperties.class);

            Stopwatch stopwatch = Stopwatch.createStarted();
            log.info("Holding {} request {} while JIRA finishes starting up", request.getMethod(), request.getRequestURI());

            // hold this request until JIRA has fully started
            boolean timedOut;
            try {
                timedOut = !instantUpgradeManager.waitTillFullyStarted(
                        jiraProperties.getLong(HOLDING_FILTER_TIMEOUT_KEY, HOLDING_FILTER_TIMEOUT_SECONDS),
                        TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt(); // preserve interrupt status
                response.sendError(SMART_STATUS_TAKE_THE_WHEEL);
                return;
            }

            long heldTimeMillis = stopwatch.elapsed(TimeUnit.MILLISECONDS);
            logRequestHoldingMetadata(request, heldTimeMillis, timedOut);

            // abort this request and return 510 if we timed out, continue processing the request otherwise
            if (timedOut) {
                response.sendError(SMART_STATUS_TAKE_THE_WHEEL);
            } else {
                filterChain.doFilter(request, response);
            }
        } else {
            filterChain.doFilter(request, response);
        }
    }

    @VisibleForTesting
    boolean isFullContainerInitialised() {
        return ComponentManager.getInstance().getState().isStarted();
    }

    private void logRequestHoldingMetadata(HttpServletRequest request, long holdTimeMillis, boolean timedOut) {
        String message;
        if (timedOut) {
            message = "JIRA didn't start fast enough. Erroring {} {} after {} millis";
        } else {
            message = "Releasing previously held {} request {} after {} millis";
        }
        MDC.put(REQUEST_HELD_TIME, holdTimeMillis);
        MDC.put(REQUEST_METHOD, request.getMethod());
        MDC.put(REQUEST_HAS_TIMEDOUT, timedOut);
        log.info(message, request.getMethod(), request.getRequestURI(), holdTimeMillis);
        MDC.remove(REQUEST_HAS_TIMEDOUT);
        MDC.remove(REQUEST_HELD_TIME);
        MDC.remove(REQUEST_METHOD);
    }

    /**
     * Checks if the specific request should be held
     * @param request
     * @return {@code true} if the request should be held and {@code false} if it should be let through
     */
    private boolean shouldHoldThisRequest(HttpServletRequest request) {
        if (new CloudControlIPCheck().test(request)) {
            // Do not hold requests that come from the control IP
            String path = request.getRequestURL().substring(request.getContextPath().length());
            log.debug("Passing control IP request: {}", path);
            return false;
        }
        String path = request.getRequestURI().substring(request.getContextPath().length());
        if (permittedRequestPaths.contains(path)) {
            return false;
        }
        return true;
    }

    @VisibleForTesting
    public boolean isHoldingRequests() {
        // Performance optimization to prevent using ComponentAccessor on every request.
        // Once any of these states is reached current state won't change anymore until restart.
        if (applicationState.get() != null) {
            switch (applicationState.get()) {
                case DISABLED:
                case FULLY_STARTED:
                case ERROR:
                    return false;
            }
        }

        // Update current application state
        InstantUpgradeManager instantUpgradeManager = ComponentAccessor.getComponent(InstantUpgradeManager.class);
        applicationState.set(instantUpgradeManager.getState());
        switch (applicationState.get()) {
            case ERROR:
            case DISABLED:
            case FULLY_STARTED:
                return false;
            case EARLY_STARTUP:
                log.warn("During instant upgrade requests should not be coming through during early startup. This is probably a bug in the icebat");
            case WAITING_FOR_ACTIVATION:
            case LATE_STARTUP:
                return true;
            default:
                log.error("Unknown application status {}", applicationState.get());
                return false;
        }

    }
    
}
