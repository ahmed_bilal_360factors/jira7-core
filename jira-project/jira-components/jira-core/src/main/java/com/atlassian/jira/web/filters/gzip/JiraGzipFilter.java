package com.atlassian.jira.web.filters.gzip;

import com.atlassian.gzipfilter.GzipFilter;
import com.atlassian.gzipfilter.integration.GzipFilterIntegration;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import static com.atlassian.jira.component.ComponentAccessor.getComponentSafely;

public class JiraGzipFilter extends GzipFilter {
    private static final String ALREADY_FILTERED = GzipFilter.class.getName() + "_already_filtered";

    public JiraGzipFilter() {
        super(new JiraGzipFilterIntegration());
    }

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
            throws IOException, ServletException {
        /**
         *
         * JRADEV-21029 : when an error occurs, there is made an internal redirection but request stays the same.
         *
         * This behaviour causes a situation when request is already marked as filtered with gzip filter, but response
         * content is actually not gzipped.
         * From the browser point of view it looks like corrupted content (header "Content-Encoding: gzip"
         * with plain-text content )
         *
         * Here we are detecting that an error occured and un-marking filter as applied
         *
         */
        //
        if (req.getAttribute("javax.servlet.error.request_uri") != null && req.getAttribute(ALREADY_FILTERED) != null) {
            req.setAttribute(ALREADY_FILTERED, null);
        }

        super.doFilter(req, res, chain);
    }

    private static class JiraGzipFilterIntegration implements GzipFilterIntegration {
        public boolean useGzip() {

            return getComponentSafely(ApplicationProperties.class).
                    map(applicationProperties -> {
                        try {
                            return applicationProperties.getOption(APKeys.JIRA_OPTION_WEB_USEGZIP);
                        } catch (RuntimeException e) {
                            // if we fail to retrieve the GZIP-option from the database for any reason, let's just return
                            // false, so that we don't break the filter chain when we are rendering a johnson page.
                            return false;
                        }
                    }).
                    orElse(false);
        }

        public String getResponseEncoding(HttpServletRequest httpServletRequest) {
            return getComponentSafely(ApplicationProperties.class).
                    map(ApplicationProperties::getEncoding).
                    orElse("UTF-8");
        }
    }
}
