package com.atlassian.jira.web.session;

import com.atlassian.jira.util.NonInjectableComponent;
import com.atlassian.jira.web.bean.PagerFilter;

/**
 * Provides access to getting and setting {@link PagerFilter} objects in session.
 *
 * @see SessionSearchObjectManagerFactory#createPagerFilterManager()
 * @see SessionSearchObjectManagerFactory#createPagerFilterManager(javax.servlet.http.HttpServletRequest)
 * @see SessionSearchObjectManagerFactory#createPagerFilterManager(com.atlassian.jira.util.velocity.VelocityRequestSession)
 * @since v4.2
 * @deprecated since 7.1 Storing information on the httpsession is obsoleted. Please move this functionality to the frontend or other type of storage. This class will be removed in 8.0.
 */
@Deprecated
@NonInjectableComponent
public interface SessionPagerFilterManager extends SessionSearchObjectManager<PagerFilter> {
}
