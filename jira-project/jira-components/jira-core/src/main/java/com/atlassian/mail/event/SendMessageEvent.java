package com.atlassian.mail.event;

import javax.mail.internet.MimeMessage;

/**
 * Event that captures the message that was sent via SMTP server.
 *
 * @since 7.1
 */
public class SendMessageEvent {
    private final MimeMessage message;

    public SendMessageEvent(final MimeMessage message) {
        this.message = message;
    }

    public MimeMessage getMessage() {
        return message;
    }
}
