package com.atlassian.jira.security.roles;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.cache.CachedReference;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * A caching implementation of the {@link ProjectRoleAndActorStore} that delegates to another {@link ProjectRoleAndActorStore}.
 * <p>
 * This class maintains two separate unrelated caches, one for ProjectRoles and another for the actors associated with a
 * Project/ProjectRole combination. These use separate approaches to maintain correctness under concurrent usage.
 * <p>
 * The caching of the ProjectRoleActors maintains its correctness under concurrent updates/miss population by using
 * {@link ConcurrentMap#putIfAbsent(Object, Object)} to store the result of a retrieval operation from the database
 * (non-mutative), but {@link ConcurrentMap#put(Object, Object)} to store the result of an update.
 */
@EventComponent
public class CachingProjectRoleAndActorStore implements ProjectRoleAndActorStore {
    private final ProjectRoleAndActorStore delegate;
    private final RoleActorFactory roleActorFactory;

    /**
     * Caches all project roles, including lookup maps for by-ID or by-name
     */
    private final CachedReference<AllProjectRoles> projectRoles;

    /**
     * Caches project-specific role actors for a given project role
     */
    final Cache<Long, Map<Optional<Long>, ProjectRoleActors>> projectRoleActorsCache;

    public CachingProjectRoleAndActorStore(final ProjectRoleAndActorStore delegate,
                                           final RoleActorFactory roleActorFactory,
                                           final CacheManager cacheManager) {
        this.delegate = delegate;
        this.roleActorFactory = roleActorFactory;

        projectRoles = cacheManager.getCachedReference(getClass().getName() + ".projectRoles",
                () -> new AllProjectRoles(this.delegate.getAllProjectRoles()));

        projectRoleActorsCache = cacheManager.getCache(getClass().getName() + ".projectRoleActors",
                this::loadRolesFromDelegate,
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).build());
    }

    public Collection<ProjectRole> getAllProjectRoles() {
        return projectRoles.get().getAll();
    }

    public ProjectRole getProjectRole(final Long id) {
        return projectRoles.get().get(id);
    }

    public ProjectRole getProjectRoleByName(final String name) {
        return projectRoles.get().getByName(name);
    }

    public ProjectRole addProjectRole(final ProjectRole projectRole) {
        try {
            return delegate.addProjectRole(projectRole);
        } finally {
            projectRoles.reset();
        }
    }

    public void updateProjectRole(final ProjectRole projectRole) {
        try {
            delegate.updateProjectRole(projectRole);
        } finally {
            projectRoles.reset();
        }
    }

    public void deleteProjectRole(final ProjectRole projectRole) {
        final long projectRoleId = notNull("projectRole.getId()", notNull("projectRole", projectRole).getId());
        try {
            delegate.deleteProjectRole(projectRole);
        } finally {
            projectRoles.reset();
            projectRoleActorsCache.remove(projectRoleId);
        }
    }

    public DefaultRoleActors getDefaultRoleActors(final Long projectRoleId) {
        return getProjectRoleActors(projectRoleId, null);
    }

    public ProjectRoleActors getProjectRoleActors(@Nonnull final Long projectRoleId, final Long projectId) {
        final ProjectRoleActors actors = projectRoleActorsCache.get(notNull("projectRoleId", projectRoleId))
                .get(Optional.ofNullable(projectId));
        return actors == null? new ProjectRoleActorsImpl(projectId, projectRoleId, Sets.newHashSet()): actors;
    }

    @Override
    public Collection<ProjectRoleActors> getProjectRoleActorsByRoleId(@Nonnull final Long projectRoleId) {
        return projectRoleActorsCache.get(notNull("projectRoleId", projectRoleId))
                .values();
    }

    public void updateProjectRoleActors(final ProjectRoleActors projectRoleActors) {
        if (projectRoleActors.getProjectId() == null) {
            // Really the default role actors...
            updateDefaultRoleActors(projectRoleActors);
            return;
        }
        delegate.updateProjectRoleActors(projectRoleActors);
        projectRoleActorsCache.remove(projectRoleActors.getProjectRoleId());
    }

    public void updateDefaultRoleActors(final DefaultRoleActors defaultRoleActors) {
        delegate.updateDefaultRoleActors(defaultRoleActors);
        projectRoleActorsCache.remove(defaultRoleActors.getProjectRoleId());
    }

    public void applyDefaultsRolesToProject(final Project project) {
        delegate.applyDefaultsRolesToProject(project);
        // Applying the default roles to a project creates new entries in the database table, invalidating the cache.
        projectRoleActorsCache.removeAll();
    }

    @Override
    public void removeAllRoleActorsByKeyAndType(final String key, final String type) {
        delegate.removeAllRoleActorsByKeyAndType(key, type);
        // Nuke the whole cache since we don't know which projects/roles this will effect
        projectRoleActorsCache.removeAll();
    }

    public void removeAllRoleActorsByProject(final Project project) {
        delegate.removeAllRoleActorsByProject(project);
        projectRoleActorsCache.removeAll();
    }

    @Override
    public Collection<Long> getProjectIdsContainingRoleActorByKeyAndType(final String key, final String type) {
        return delegate.getProjectIdsContainingRoleActorByKeyAndType(key, type);
    }

    public List<Long> roleActorOfTypeExistsForProjects(final List<Long> projectsToLimitBy, final ProjectRole projectRole, final String projectRoleType, final String projectRoleParameter) {
        return delegate.roleActorOfTypeExistsForProjects(projectsToLimitBy, projectRole, projectRoleType, projectRoleParameter);
    }

    @Override
    public Map<Long, List<String>> getProjectIdsForUserInGroupsBecauseOfRole(final List<Long> projectsToLimitBy, final ProjectRole projectRole, final String projectRoleType, final String userKey) {
        return delegate.getProjectIdsForUserInGroupsBecauseOfRole(projectsToLimitBy, projectRole, projectRoleType, userKey);
    }

    @EventListener
    public void onClearCache(@SuppressWarnings("unused") final ClearCacheEvent event) {
        clearCaches();
    }

    public void clearCaches() {
        projectRoles.reset();
        projectRoleActorsCache.removeAll();
    }

    @VisibleForTesting
    protected CachedRoleActors toCachedRoleActor(@Nonnull final DefaultRoleActors defaultRoleActors) {
        final Set<RoleActor> optimizedRoleActors = roleActorFactory.optimizeRoleActorSet(defaultRoleActors.getRoleActors());
        return new CachedRoleActors(defaultRoleActors, optimizedRoleActors);
    }

    @Override
    public boolean isGroupUsed(@Nonnull final String group) {
        return delegate.isGroupUsed(group);
    }

    public Map<Optional<Long>, ProjectRoleActors> loadRolesFromDelegate(@Nonnull final Long key) {
        final ImmutableMap.Builder<Optional<Long>, ProjectRoleActors> builder = ImmutableMap.builder();
        delegate.getProjectRoleActorsByRoleId(key)
                .forEach((projectRoleActors ->
                        builder.put(Optional.ofNullable(projectRoleActors.getProjectId()), toCachedRoleActor(projectRoleActors))));
        return builder.build();
    }

    @Immutable
    static final class ProjectRoleActorsKey implements Serializable {
        final long projectRoleId;
        final long projectId;

        ProjectRoleActorsKey(final Long projectRoleId, final Long projectId) {
            this.projectRoleId = notNull("projectRoleId", projectRoleId);
            this.projectId = notNull("projectId", projectId);
        }

        public long getProjectRoleId() {
            return projectRoleId;
        }

        public long getProjectId() {
            return projectId;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final ProjectRoleActorsKey other = (ProjectRoleActorsKey) o;
            return projectRoleId == other.projectRoleId && projectId == other.projectId;
        }

        @Override
        public int hashCode() {
            final long value = projectId * 31 + projectRoleId;
            return (int) (value ^ (value >>> 32));
        }

        @Override
        public String toString() {
            return "ProjectRoleActorsKey[projectRoleId=" + projectRoleId + ",projectId=" + projectId + ']';
        }
    }

    static class AllProjectRoles {
        private final List<ProjectRole> projectRoles;
        private final Map<Long, ProjectRole> projectRolesById;
        private final Map<String, ProjectRole> projectRolesByName;

        AllProjectRoles(final Collection<ProjectRole> projectRoles) {
            final ImmutableMap.Builder<Long, ProjectRole> byId = ImmutableMap.builder();
            final ImmutableMap.Builder<String, ProjectRole> byName = ImmutableMap.builder();
            for (final ProjectRole projectRole : projectRoles) {
                byId.put(projectRole.getId(), projectRole);
                byName.put(projectRole.getName(), projectRole);
            }

            this.projectRoles = ImmutableList.copyOf(projectRoles);
            this.projectRolesById = byId.build();
            this.projectRolesByName = byName.build();
        }

        Collection<ProjectRole> getAll() {
            return projectRoles;
        }

        ProjectRole get(final Long id) {
            return projectRolesById.get(id);
        }

        ProjectRole getByName(final String name) {
            return projectRolesByName.get(name);
        }
    }


    /**
     * CachedProjectRoleActors contains an optimized contains(user) method.
     */
    static class CachedRoleActors implements ProjectRoleActors {
        private final DefaultRoleActors delegate;
        private final Set<RoleActor> optimizedProjectRoleSet;

        CachedRoleActors(final DefaultRoleActors delegate, final Set<RoleActor> optimizedProjectRoleSet) {
            this.delegate = delegate;
            this.optimizedProjectRoleSet = ImmutableSet.copyOf(optimizedProjectRoleSet);
        }

        /*
         * The optimized set of RoleActor instances is used.
         */
        public boolean contains(final ApplicationUser user) {
            for (final RoleActor o : optimizedProjectRoleSet) {
                if (o.contains(user)) {
                    return true;
                }
            }
            return false;
        }

        public Long getProjectId() {
            return (delegate instanceof ProjectRoleActors) ? ((ProjectRoleActors) delegate).getProjectId() : null;
        }

        public Set<ApplicationUser> getUsers() {
            return delegate.getUsers();
        }

        @Override
        public Set<ApplicationUser> getApplicationUsers() {
            return delegate.getApplicationUsers();
        }

        public Set<RoleActor> getRoleActors() {
            return delegate.getRoleActors();
        }

        public Long getProjectRoleId() {
            return delegate.getProjectRoleId();
        }

        public Set<RoleActor> getRoleActorsByType(final String type) {
            return delegate.getRoleActorsByType(type);
        }

        public DefaultRoleActors addRoleActors(final Collection<? extends RoleActor> roleActors) {
            return delegate.addRoleActors(roleActors);
        }

        public DefaultRoleActors addRoleActor(final RoleActor roleActor) {
            return delegate.addRoleActor(roleActor);
        }

        public DefaultRoleActors removeRoleActor(final RoleActor roleActor) {
            return delegate.removeRoleActor(roleActor);
        }

        public DefaultRoleActors removeRoleActors(final Collection<? extends RoleActor> roleActors) {
            return delegate.removeRoleActors(roleActors);
        }

        @Override
        public String toString() {
            return "CachedRoleActors[delegate=" + delegate + ",optimizedProjectRoleSet=" + optimizedProjectRoleSet + ']';
        }
    }
}