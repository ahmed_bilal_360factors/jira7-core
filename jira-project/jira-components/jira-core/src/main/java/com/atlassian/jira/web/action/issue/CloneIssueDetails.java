package com.atlassian.jira.web.action.issue;

import com.atlassian.jira.bc.issue.CloneIssueCommand;
import com.atlassian.jira.bc.issue.CloneIssueTaskContext;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.exception.IssueNotFoundException;
import com.atlassian.jira.exception.IssuePermissionException;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.TemporaryAttachmentsMonitorLocator;
import com.atlassian.jira.issue.customfields.CloneOptionConfiguration;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.SummarySystemField;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderLayoutItem;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.security.AssignIssueSecuritySchemeTaskContext;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.task.NonExclusiveTaskContext;
import com.atlassian.jira.task.TaskContext;
import com.atlassian.jira.task.TaskDescriptor;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.web.bean.TaskDescriptorBean;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import webwork.action.ActionContext;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import static com.atlassian.jira.issue.IssueFieldConstants.SUMMARY;
import static com.atlassian.jira.security.Permissions.MODIFY_REPORTER;

public class CloneIssueDetails extends CreateIssueDetails {
    private final ApplicationProperties applicationProperties;
    private final PermissionManager permissionManager;
    private final IssueLinkManager issueLinkManager;
    private final IssueLinkTypeManager issueLinkTypeManager;
    private final SubTaskManager subTaskManager;
    private final AttachmentManager attachmentManager;
    private final FieldManager fieldManager;
    private final IssueFactory issueFactory;
    private final TaskManager taskManager;
    private final TaskDescriptorBean.Factory taskBeanFactory;

    private IssueLinkType cloneIssueLinkType;
    private String cloneIssueLinkTypeName;

    private MutableIssue issueObject;

    // The original issue that is to be cloned.
    private Issue originalIssue;

    // Whether or not to clone issue links as well
    private boolean cloneLinks;
    // Whether or not to clone issue's sub-tasks
    private boolean cloneSubTasks;
    // Whether or not to clone issue's attachments
    private boolean cloneAttachments;

    private static final String BROWSE_ISSUE_PAGE_PREFIX = "/browse/";
    private IssueService.CloneValidationResult cloneValidationResult;

    private Long taskId = null;
    private TaskDescriptor<CloneIssueCommand.CloneIssueResult> currentTaskDescriptor;
    private TaskDescriptorBean<CloneIssueCommand.CloneIssueResult> currentTask;

    public CloneIssueDetails(ApplicationProperties applicationProperties, PermissionManager permissionManager,
                             IssueLinkManager issueLinkManager, IssueLinkTypeManager issueLinkTypeManager, SubTaskManager subTaskManager,
                             AttachmentManager attachmentManager, FieldManager fieldManager, IssueCreationHelperBean issueCreationHelperBean,
                             IssueFactory issueFactory, IssueService issueService, final TemporaryAttachmentsMonitorLocator temporaryAttachmentsMonitorLocator, TaskManager taskManager, TaskDescriptorBean.Factory taskBeanFactory) {
        super(issueFactory, issueCreationHelperBean, issueService, temporaryAttachmentsMonitorLocator);
        this.applicationProperties = applicationProperties;
        this.permissionManager = permissionManager;
        this.issueLinkManager = issueLinkManager;
        this.issueLinkTypeManager = issueLinkTypeManager;
        this.subTaskManager = subTaskManager;
        this.attachmentManager = attachmentManager;
        this.fieldManager = fieldManager;
        this.issueFactory = issueFactory;
        this.taskManager = taskManager;
        this.taskBeanFactory = taskBeanFactory;
    }

    public String doDefault() throws Exception {
        this.cloneSubTasks = true;
        this.cloneLinks = false;
        this.cloneAttachments = false;

        try {
            setOriginalIssue(getIssueObject(getIssue()));

            copySummaryFieldFromOriginalIssueToHolder();
        } catch (IssueNotFoundException e) {
            // Error is added above
            return ERROR;
        } catch (IssuePermissionException e) {
            return ERROR;
        }

        // Summary can be modified - require futher input
        return INPUT;
    }

    public List<CustomFieldCloneOption> getCustomFieldCloneOptions() {
        List<CustomFieldCloneOption> options = Lists.newArrayList();

        for (CustomField cf : getCustomFields(getOriginalIssue())) {
            CloneOptionConfiguration cloneOptionConfig = cf.getCustomFieldType().getCloneOptionConfiguration(cf, getOriginalIssue());
            if (cloneOptionConfig.shouldDisplayOption()) {
                options.add(new CustomFieldCloneOption(cf.getId(), cloneOptionConfig.getOptionLabel(), cloneOptionConfig.isOptionSelectedByDefault()));
            }
        }

        return options;
    }

    private void copySummaryFieldFromOriginalIssueToHolder() throws GenericEntityException {
        String summary = getOriginalIssue().getSummary();
        if (StringUtils.isNotBlank(summary)) {
            //JRADEV-1972 CLONE - the space is ignored when reading from a properties file
            String clonePrefixProperties = applicationProperties.getDefaultBackedString(APKeys.JIRA_CLONE_PREFIX);
            String cloneSummary = StringUtils.isBlank(clonePrefixProperties) ? summary : StringUtils.join(new Object[]{clonePrefixProperties, summary}, " ");
            getFieldValuesHolder().put(SUMMARY, cloneSummary);
        }
    }

    public FieldScreenRenderLayoutItem getFieldScreenRenderLayoutItem(String fieldId) {
        return getFieldScreenRenderer().getFieldScreenRenderLayoutItem(fieldManager.getOrderableField(fieldId));
    }

    public Issue getIssueObject(GenericValue genericValue) {
        return issueFactory.getIssue(genericValue);
    }

    protected void doValidation() {
        try {
            //calling getIssue() here may cause exceptions
            setOriginalIssue(getIssueObject(getIssue()));
        } catch (IssuePermissionException ipe) {
            return;
        } catch (IssueNotFoundException infe) {
            return;
        }

        // Suck the summary from the web parameters.
        SummarySystemField summaryField = (SummarySystemField) fieldManager.getOrderableField(IssueFieldConstants.SUMMARY);
        summaryField.populateFromParams(getFieldValuesHolder(), ActionContext.getContext().getParameters());
        String summary = (String) getFieldValuesHolder().get(summaryField.getId());

        List<CustomField> customFields = getCustomFields(getOriginalIssue());
        cloneValidationResult = issueService.validateClone(getLoggedInUser(), getOriginalIssue(), summary,
                cloneAttachments, cloneSubTasks, cloneLinks, getCloneOptionSelections(customFields));

        if (!cloneValidationResult.isValid()) {
            setErrorMessages(cloneValidationResult.getErrorCollection().getErrorMessages());
            setErrors(cloneValidationResult.getErrorCollection().getErrors());
        }
    }

    private Map<CustomField, Optional<Boolean>> getCloneOptionSelections(List<CustomField> customFields) {
        Set<String> requestParameters = getHttpRequest().getParameterMap().keySet();

        Map<CustomField, Optional<Boolean>> cloneOptionSelections = Maps.newHashMap();
        for(CustomField cf : customFields) {
            Optional<Boolean> cloneOptionSelection;

            CloneOptionConfiguration cloneOptionConfiguration = cf.getCustomFieldType().getCloneOptionConfiguration(cf, getOriginalIssue());
            if (cloneOptionConfiguration.shouldDisplayOption()) {
                cloneOptionSelection = Optional.of(requestParameters.contains(cf.getId()));
            } else {
                cloneOptionSelection = Optional.empty();
            }

            cloneOptionSelections.put(cf, cloneOptionSelection);
        }
        return cloneOptionSelections;
    }

    @RequiresXsrfCheck
    protected String doExecute() {
        IssueService.AsynchronousTaskResult result = issueService.clone(getLoggedInUser(), cloneValidationResult);
        setReturnUrl(null);

        if (result.isValid()) {
            try {
                Long taskId = result.getTaskId();
                // Most clones should complete very quickly if a simple issue and so we will just wait a few 100s millis and not
                // jump straight to the progress UI
                StopWatch watch = new StopWatch();
                watch.start();
                while (watch.getTime() < 500) {
                    currentTaskDescriptor = taskManager.getTask(taskId);
                    if (currentTaskDescriptor.isFinished() && !currentTaskDescriptor.isCancelled()) {
                        return getRedirectToIssue();
                    }
                    Thread.sleep(50);
                }
                // Task did not finish so we redirect to the progress page
                currentTaskDescriptor = taskManager.getTask(taskId);
                TaskContext context = currentTaskDescriptor.getTaskContext();
                if (context instanceof CloneIssueTaskContext) {
                    originalIssue = getIssueManager().getIssueObject(((CloneIssueTaskContext) context).getOriginalIssueId());
                }

                return super.returnCompleteWithInlineRedirect(currentTaskDescriptor.getProgressURL());
            } catch (Exception e) {
                log.error(e, e);
                addErrorMessage(getText("admin.errors.exception") + " " + e);
                return ERROR;
            }
        } else {
            return ERROR;
        }
    }

    public String doProgress() throws ExecutionException, InterruptedException {
        if (taskId == null) {
            addErrorMessage(getText("admin.indexing.no.task.id"));
            return ERROR;
        }
        currentTaskDescriptor = taskManager.getTask(taskId);
        if (currentTaskDescriptor == null) {
            addErrorMessage(getText("admin.indexing.no.task.found"));
            return ERROR;
        }
        final TaskContext context = currentTaskDescriptor.getTaskContext();
        if (!(context instanceof CloneIssueTaskContext)) {
            addErrorMessage(getText("common.tasks.wrong.task.context", CloneIssueTaskContext.class.getName(), context.getClass().getName()));
            return ERROR;
        }

        currentTask = taskBeanFactory.create(currentTaskDescriptor);
        // Once finished, we go directly to view the new issue.
        if (currentTaskDescriptor.isFinished() && !currentTaskDescriptor.isCancelled()) {
            return getRedirectToIssue();
        }
        originalIssue = getIssueManager().getIssueObject(((CloneIssueTaskContext) context).getOriginalIssueId());

        return "progress";
    }

    private String getRedirectToIssue() {
        final CloneIssueCommand.CloneIssueResult taskResult = currentTaskDescriptor.getResult();
        addErrorCollection(taskResult.getErrorCollection());
        if (taskResult.isSuccessful()) {
            String clonedIssue = taskResult.getIssueKey();
            return inlineRedirectToIssueWithKey(clonedIssue);
        } else {
            return ERROR;
        }
    }

    public boolean isDisplayCopyLink() {
        if (issueLinkManager.isLinkingEnabled()) {
            // See if there are any links to clone
            if (givenIssueHasAnyCopyableLink(getOriginalIssue())) {
                return true;
            } else {
                // See if there are any links to copy on sub-tasks
                if (originalIssueHasSubTask()) {
                    for (Issue subTask : getOriginalIssue().getSubTaskObjects()) {
                        if (givenIssueHasAnyCopyableLink(subTask)) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public boolean isDisplayCopyAttachments() {
        if (attachmentManager.attachmentsEnabled()) {
            if (givenIssueHasAnyAttachment(originalIssue)) {
                // If an issue has attachments then we should allow to clone them
                return true;
            } else if (subTaskManager.isSubTasksEnabled()) {
                // Otherwise need to check if at least one sub-task has an attachment
                if (originalIssueHasSubTask()) {
                    for (Issue subTask : originalIssue.getSubTaskObjects()) {
                        if (givenIssueHasAnyAttachment(subTask)) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;

    }

    public boolean isDisplayCopySubTasks() {
        return subTaskManager.isSubTasksEnabled() && originalIssueHasSubTask();
    }

    private boolean givenIssueHasAnyCopyableLink(Issue issue) {
        Collection<IssueLink> inwardLinks = issueLinkManager.getInwardLinks(issue.getId());
        if (hasAnyCopyableLinkInGivenLinks(inwardLinks)) {
            return true;
        }

        Collection<IssueLink> outwardLinks = issueLinkManager.getOutwardLinks(issue.getId());
        if (hasAnyCopyableLinkInGivenLinks(outwardLinks)) {
            return true;
        }

        return false;
    }

    private boolean originalIssueHasSubTask() {
        return CollectionUtils.isNotEmpty(getOriginalIssue().getSubTaskObjects());
    }

    public boolean isCloneLinks() {
        return cloneLinks;
    }

    public void setCloneLinks(boolean cloneLinks) {
        this.cloneLinks = cloneLinks;
    }

    public boolean isCloneSubTasks() {
        return cloneSubTasks;
    }

    public void setCloneSubTasks(boolean cloneSubTasks) {
        this.cloneSubTasks = cloneSubTasks;
    }

    public boolean isCloneAttachments() {
        return cloneAttachments;
    }

    public void setCloneAttachments(final boolean cloneAttachments) {
        this.cloneAttachments = cloneAttachments;
    }

    // ------ Getters & Setters & Helper Methods -----------------

    public Issue getOriginalIssue() {
        return originalIssue;
    }

    public void setOriginalIssue(Issue originalIssue) {
        this.originalIssue = originalIssue;
    }

    // Retrieve the issue link type specified by the clone link name in the properties file.
    // If the name is unset - issue linking of originals to clones is not required - returns null.
    // Otherwise, returns null if the issue link type with the specified name cannot be found in the system.
    public IssueLinkType getCloneIssueLinkType() {
        if (cloneIssueLinkType == null) {
            final Collection<IssueLinkType> cloneIssueLinkTypes = issueLinkTypeManager.getIssueLinkTypesByName(getCloneLinkTypeName());

            if (StringUtils.isBlank(getCloneLinkTypeName())) {
                // Issue linking is not required
                cloneIssueLinkType = null;
            } else if (CollectionUtils.isEmpty(cloneIssueLinkTypes)) {
                log.warn("The clone link type '" + getCloneLinkTypeName() + "' does not exist. A link to the original issue will not be created.");
                cloneIssueLinkType = null;
            } else {
                for (final IssueLinkType issueLinkType : cloneIssueLinkTypes) {
                    if (issueLinkType.getName().equals(getCloneLinkTypeName())) {
                        cloneIssueLinkType = issueLinkType;
                    }
                }
            }
        }

        return cloneIssueLinkType;
    }

    // Determines whether a warning should be displayed.
    // If the link type name is unset in the properties file - issue linking of originals to clones is not required - do not display warning.
    public boolean isDisplayCloneLinkWarning() {
        return (StringUtils.isNotBlank(getCloneLinkTypeName()) && getCloneIssueLinkType() == null);
    }

    // "Modify Reporter" permission required to create the clone with the original reporter set.
    public boolean isCanModifyReporter() {
        return permissionManager.hasPermission(MODIFY_REPORTER, getIssueObject(), getLoggedInUser());
    }

    public String getCloneLinkTypeName() {
        if (cloneIssueLinkTypeName == null)
            cloneIssueLinkTypeName = applicationProperties.getDefaultBackedString(APKeys.JIRA_CLONE_LINKTYPE_NAME);

        return cloneIssueLinkTypeName;
    }

    @Override
    public Project getProjectObject() {
        return getProjectManager().getProjectObj(getIssue().getLong("project"));
    }

    public Map<String, Object> getDisplayParams() {
        final Map<String, Object> displayParams = new HashMap<String, Object>();
        displayParams.put("theme", "aui");
        return displayParams;
    }

    private boolean hasAnyCopyableLinkInGivenLinks(Collection<IssueLink> givenLinks) {
        for (IssueLink checkingIssueLink : givenLinks) {
            if (isCopyableLink(checkingIssueLink)) {
                return true;
            }
        }

        return false;
    }

    private boolean isCopyableLink(IssueLink checkingLink) {
        // Do not copy system links types and do not copy the cloners link type, as it is used to record the relationship between cloned issues
        // So if the cloners link type does not exists, or the link is not of cloners link type, and is not a system link, then copy it
        return !checkingLink.isSystemLink() &&
                (getCloneIssueLinkType() == null || givenLinkTypeIsNotSameAsCloneIssueLinkType(checkingLink));
    }

    private boolean givenLinkTypeIsNotSameAsCloneIssueLinkType(final IssueLink checkingLink) {
        return !getCloneIssueLinkType().getId().equals(checkingLink.getIssueLinkType().getId());
    }

    private boolean givenIssueHasAnyAttachment(Issue givenIssue) {
        return CollectionUtils.isNotEmpty(attachmentManager.getAttachments(givenIssue));
    }

    private String inlineRedirectToIssueWithKey(String issueKey) {
        return super.returnCompleteWithInlineRedirect(BROWSE_ISSUE_PAGE_PREFIX + issueKey);
    }

    public TaskDescriptorBean<CloneIssueCommand.CloneIssueResult> getCurrentTask() {
        if (currentTask == null) {
            final TaskDescriptor<CloneIssueCommand.CloneIssueResult> taskDescriptor = getCurrentTaskDescriptor();
            if (taskDescriptor != null) {
                currentTask = taskBeanFactory.create(taskDescriptor);
            }
        }
        return currentTask;
    }

    private TaskDescriptor<CloneIssueCommand.CloneIssueResult> getCurrentTaskDescriptor() {
        if (currentTaskDescriptor == null) {
            currentTaskDescriptor = taskManager.getLiveTask(new AssignIssueSecuritySchemeTaskContext(getProject()));
        }
        return currentTaskDescriptor;
    }


    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }


    public static final class CustomFieldCloneOption {
        private final String id;
        private final String label;
        private final boolean selectedByDefault;

        public CustomFieldCloneOption(String id, String label, boolean selectedByDefault) {
            this.id = id;
            this.label = label;
            this.selectedByDefault = selectedByDefault;
        }

        public String getId() {
            return id;
        }

        public String getLabel() {
            return label;
        }

        public boolean isSelectedByDefault() {
            return selectedByDefault;
        }
    }
}
