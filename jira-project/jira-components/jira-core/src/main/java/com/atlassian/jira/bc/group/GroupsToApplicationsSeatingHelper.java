package com.atlassian.jira.bc.group;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.ImmutableMaps;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimaps;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

/**
 * @since 7.0
 */
public class GroupsToApplicationsSeatingHelper {
    private static final Logger log = LoggerFactory.getLogger(GroupsToApplicationsSeatingHelper.class);

    private final ApplicationRoleManager applicationRoleManager;
    private final GroupRelationshipChecker relationshipChecker;

    public GroupsToApplicationsSeatingHelper(final ApplicationRoleManager applicationRoleManager, final GroupRelationshipChecker relationshipChecker) {
        this.applicationRoleManager = applicationRoleManager;
        this.relationshipChecker = relationshipChecker;
    }

    /**
     * <p>Produces a relation between {@link com.atlassian.jira.application.ApplicationRole}s and {@link com.atlassian.jira.user.ApplicationUser}s
     * indicating which users will gain access to which applications if they will be added to given groups.</p>
     * <p>This method is useful only in seating counting. There is one gotcha: if users are already (or eventually will be) added to some
     * application, and we want to give them CORE access it's dry-run. We won't count them as jira-core users as it will be implied.</p>
     *
     * @param users           users to be checked
     * @param groupsToBeAdded groups to be added
     * @return users grouped by applications in which number of occupied seats should be incremented
     */
    public Map<ApplicationRole, Set<ApplicationUser>> rolesToBeAddedForSeatingCountPurpose(Set<ApplicationUser> users, Set<String> groupsToBeAdded) {
        final SetMultimap<ApplicationUser, ApplicationRole> userToRoles = HashMultimap.create();
        for (ApplicationUser user : users) {
            userToRoles.putAll(user, calculateRolesToBeAdded(user, groupsToBeAdded));
        }

        return ImmutableMaps.transformValue(
                Multimaps.invertFrom(userToRoles, HashMultimap.create()).asMap(),
                ImmutableSet::copyOf
        );
    }

    /**
     * Checks which application roles will be effectively accessible by user if he will be added to given roles.
     * It's possible mainly due to configurations which reuse default groups across applications. See {@link #findEffectiveApplicationsByGroups(java.util.Optional, java.util.Set)} for more detailed explanation.
     *
     * @param userDirectoryId directory, in which lookup is performed
     * @param roles           checked roles
     * @return a set of application roles accessible for users
     * @see #findEffectiveApplicationsByGroups(java.util.Optional, java.util.Set)
     */
    public Set<ApplicationRole> findEffectiveApplications(Optional<Long> userDirectoryId, Set<ApplicationRole> roles) {
        final Set<String> defaultGroupsNames = roles.stream()
                .map(ApplicationRole::getDefaultGroups)
                .flatMap(Collection::stream)
                .map(Group::getName)
                .collect(CollectorsUtil.toImmutableSet());
        return findEffectiveApplicationsByGroups(userDirectoryId, defaultGroupsNames);
    }

    /**
     * <p>Performs a lookup for applications which should be accessible in given directory assuming that someone is a member of mentioned groups.
     * This method does not incorporate in any way roles which users may already have. It also does not check which roles have free seats.</p>
     * <p>Due to nested group support it's not trivial to check potential application access.
     * Effectively, access may be gained by:
     * <ul>
     * <li>direct group membership (of course group has to exist in given directory)</li>
     * <li>indirect membership via group relations</li>
     * </ul>
     * </p>
     * <p><b>Notice:</b> When {@literal userDirectoryId} is {@link java.util.Optional#empty()}, it's not possible to perform exact search due to possibility of differen configurations.
     * It is advised to not perform approximate lookups.</p>
     *
     * @param userDirectoryId directory, in which lookup is performed, when {@link java.util.Optional#empty()} given, approximate search will be performed
     * @param groupsNames     groups which should be checked
     * @return a set of application roles accessible for users
     */
    public Set<ApplicationRole> findEffectiveApplicationsByGroups(Optional<Long> userDirectoryId, Set<String> groupsNames) {
        if (!userDirectoryId.isPresent()) {
            log.debug("Performing approximate Application Check check against {}", groupsNames);
        }
        return applicationRoleManager.getRoles().stream()
                .filter(role -> role.getGroups().stream().map(Group::getName)
                        .anyMatch((Predicate<String>) roleGivingGroup ->
                                groupsNames.stream().anyMatch(
                                        groupName ->
                                                relationshipChecker.isGroupEqualOrNested(userDirectoryId, groupName, roleGivingGroup)
                                )))
                .collect(CollectorsUtil.toImmutableSet());
    }

    private Set<ApplicationRole> calculateRolesToBeAdded(ApplicationUser user, Set<String> groupsToBeAdded) {
        if (log.isTraceEnabled()) {
            log.trace("Calculating application access for {} with groups {}", user, groupsToBeAdded);
        }
        final Set<ApplicationRole> existingUserRoles = applicationRoleManager.getRolesForUser(user);
        final Set<ApplicationRole> rolesToBeAdded = findEffectiveApplicationsByGroups(Optional.ofNullable(user.getDirectoryId()), groupsToBeAdded);

        final Set<ApplicationRole> rolesToBeAddedToThisUser = Sets.difference(rolesToBeAdded, existingUserRoles).copyInto(new HashSet<>());

        final Sets.SetView<ApplicationRole> effectiveRolesAfterAdd = Sets.union(existingUserRoles, rolesToBeAdded);

        //check whether user has access to any other application than core
        if (effectiveRolesAfterAdd.stream().anyMatch(r -> !r.getKey().equals(ApplicationKeys.CORE))) {
            rolesToBeAddedToThisUser.remove(applicationRoleManager.getRole(ApplicationKeys.CORE).getOrNull());
        }
        return rolesToBeAddedToThisUser;
    }


}
