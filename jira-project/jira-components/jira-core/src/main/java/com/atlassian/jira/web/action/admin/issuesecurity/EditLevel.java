package com.atlassian.jira.web.action.admin.issuesecurity;

import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelImpl;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.issue.security.IssueSecurityLevelService;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.ofbiz.core.entity.GenericEntityException;
import webwork.action.ActionSupport;

@WebSudoRequired
public class EditLevel extends JiraWebActionSupport {

    private Long levelId;
    private Long schemeId;
    private IssueSecurityLevel level;
    private String name;
    private String description;
    private IssueSecurityLevelManager levelManager;
    private IssueSecurityLevelService issueSecurityLevelService;

    public IssueSecurityLevelService getIssueSecurityLevelService() {
        return issueSecurityLevelService;
    }

    public IssueSecurityLevelManager getLevelManager() {
        return levelManager;
    }

    public EditLevel(IssueSecurityLevelManager levelManager,
                     IssueSecurityLevelService issueSecurityLevelService) {
        this.levelManager = levelManager;
        this.issueSecurityLevelService = issueSecurityLevelService;
    }

    public String getRedirectURL() {
        return "EditIssueSecurities.jspa";
    }

    public String doDefault() throws Exception {
        this.level = getLevel();
        this.name = level.getName();
        this.description = level.getDescription();
        return ActionSupport.INPUT;
    }

    public IssueSecurityLevel getLevel() throws GenericEntityException {
        if (level == null) {
            level = getLevelManager().getSecurityLevel(levelId);
        }
        return level;
    }

    @RequiresXsrfCheck
    public String doExecute() throws Exception {
        IssueSecurityLevelService.UpdateValidationResult vr = issueSecurityLevelService.validateUpdate(getLoggedInUser(), getLevel(), getName(), getDescription());
        if (vr.isValid()) {
            ServiceResult result = issueSecurityLevelService.update(getLoggedInUser(), vr);
        } else {
            addErrorCollection(vr.getErrors());
        }
        return getRedirect("EditIssueSecurities!default.jspa?schemeId=" + getSchemeId());
    }

    public Long getLevelId() {
        return levelId;
    }

    public void setLevelId(Long levelId) {
        this.levelId = levelId;
    }

    public Long getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(Long schemeId) {
        this.schemeId = schemeId;
    }

    public void setLevel(IssueSecurityLevel level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
