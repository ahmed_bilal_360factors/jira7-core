package com.atlassian.jira.portal.events;

import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @since v5.0
 */
public class DashboardUpdated {
    public final PortalPage oldPage;
    public final PortalPage newPage;
    public final ApplicationUser loggedInUser;

    public DashboardUpdated(final PortalPage oldPage, final PortalPage newPage, final ApplicationUser user) {
        this.oldPage = oldPage;
        this.newPage = newPage;
        this.loggedInUser = user;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
