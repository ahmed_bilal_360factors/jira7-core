package com.atlassian.jira.servlet;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.crowd.embedded.ofbiz.ExtendedUserDao;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.jira.util.johnson.DefaultJohnsonProvider;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.jira.util.system.status.ApplicationState;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;

import static com.atlassian.jira.component.ComponentAccessor.getComponentSafely;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;


/**
 * Servlet that provides application status information.
 */
public class ApplicationStatusServlet extends HttpServlet {
    private static final Set<String> ERROR_LEVELS = ImmutableSet.of(EventLevel.ERROR, EventLevel.FATAL);

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        ApplicationState state = getApplicationState();

        resp.setStatus(state == ApplicationState.ERROR ? SC_INTERNAL_SERVER_ERROR : SC_OK);
        resp.setContentType("application/json");
        resp.getWriter().append("{\"state\":\"").append(state.name()).append("\"}");
    }

    /**
     * Returns the application state of the server
     */
    private ApplicationState getApplicationState() {
        if (hasErrors()) {
            return ApplicationState.ERROR;
        }

        if (!hasStarted()) {
            return ApplicationState.STARTING;
        }

        if (isFirstRun()) {
            return ApplicationState.FIRST_RUN;
        }

        if (!isUserCacheInitialized()) {
            return ApplicationState.STARTING;
        }

        if (isJohnsoned()) {
            return ApplicationState.MAINTENANCE;
        }

        return ApplicationState.RUNNING;
    }

    private boolean isJohnsoned() {
        return !getJohnsonEvents().isEmpty();
    }

    /**
     * We go through all the johnson events and see if we find any ERROR or FATAL message.
     *
     * @return true if the server is reporting errors, false if not
     */
    private static boolean hasErrors() {
        return getJohnsonEvents().stream()
                .map(Event::getLevel)
                .filter(Objects::nonNull)
                .map(EventLevel::getLevel)
                .anyMatch(ERROR_LEVELS::contains);
    }

    /**
     * We evaluate several flags inside the component manager to verify that the server is running or not
     *
     * @return true if the server is starting, false if not.
     */
    private boolean hasStarted() {
        ComponentManager.State state = getState();
        return state.isComponentsRegistered() && state.isContainerInitialised()
                && state.isPluginSystemStarted() && state.isStarted();
    }

    @VisibleForTesting
    protected ComponentManager.State getState() {
        return ComponentManager.getInstance().getState();
    }

    private static Collection<Event> getJohnsonEvents() {
        return getComponentSafely(JohnsonProvider.class)
                .orElseGet(DefaultJohnsonProvider::new)
                .getContainer()
                .getEvents();
    }

    /**
     * Checks if Jira is setup or not.
     * <p>
     * JIRA normally calls this "not setup yet", not "in its first run", but Confluence and Stash have this
     * same servlet with the same convention, and since this is meant mainly for load balancers and other
     * automation software to interpret, we are trying to keep the output consistent cross-product.
     * </p>
     *
     * @return true if jira is not setup, false if is not
     */
    private static boolean isFirstRun() {
        return !JiraUtils.isSetup();
    }

    /**
     * We want to know if the cache of users is initialized so users can login, if this cache is not populated is
     * useless to tell the load balancer we are ready There are certain phases of initialization that Pico could tell me
     * that everything is good and the ComponentAccessor not returning me the proper reference. That is why we need to
     * try/catch to avoid any weird exception sent to the load balancer
     *
     * @return true if the cache is populated, false if not
     */
    @VisibleForTesting
    protected boolean isUserCacheInitialized() {
        return getComponentSafely(ExtendedUserDao.class)
                .map(ExtendedUserDao::isCacheInitialized)
                .orElse(false);
    }
}
