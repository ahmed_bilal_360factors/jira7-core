package com.atlassian.jira.project.type;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.google.common.collect.Maps.newHashMap;

public class ProjectTypeUpdatedRegistrarImpl implements ProjectTypeUpdatedRegistrar, ProjectTypeUpdatedNotifier {
    private static final Logger log = LoggerFactory.getLogger(ProjectTypeUpdatedRegistrarImpl.class);

    protected final Map<String, ProjectTypeUpdatedHandler> handlers;

    public ProjectTypeUpdatedRegistrarImpl() {
        handlers = new ConcurrentHashMap<>();
    }

    @Override
    public boolean notifyAllHandlers(ApplicationUser user, Project project, ProjectTypeKey oldProjectType, ProjectTypeKey newProjectType) {
        Map<ProjectTypeUpdatedHandler, ProjectTypeUpdatedOutcome> handlerResults = newHashMap();
        boolean handlersWereSuccessful = true;
        for (ProjectTypeUpdatedHandler handler : getHandlers()) {
            ProjectTypeUpdatedOutcome projectTypeUpdatedOutcome = handler.onProjectTypeUpdated(user, project, oldProjectType, newProjectType);
            handlerResults.put(handler, projectTypeUpdatedOutcome);
            if (!projectTypeUpdatedOutcome.isSuccessful()) {
                log.error("The handler with id " + handler.getHandlerId() + " failed while handling a notification about a project type update");
                handlersWereSuccessful = false;
                break;
            }
        }

        if (!handlersWereSuccessful) {
            handlerResults.entrySet().forEach(handlerResult -> {
                ProjectTypeUpdatedHandler handler = handlerResult.getKey();
                ProjectTypeUpdatedOutcome outcome = handlerResult.getValue();

                handler.onProjectTypeUpdateRolledBack(user, project, oldProjectType, newProjectType, outcome);
            });
        }

        return handlersWereSuccessful;
    }

    @VisibleForTesting
    protected Collection<ProjectTypeUpdatedHandler> getHandlers() {
        return handlers.values();
    }

    @Override
    public void register(ProjectTypeUpdatedHandler handlerToAdd) {
        handlers.put(handlerToAdd.getHandlerId(), handlerToAdd);
    }

    @Override
    public void unregister(ProjectTypeUpdatedHandler handlerToRemove) {
        handlers.remove(handlerToRemove.getHandlerId());
    }
}
