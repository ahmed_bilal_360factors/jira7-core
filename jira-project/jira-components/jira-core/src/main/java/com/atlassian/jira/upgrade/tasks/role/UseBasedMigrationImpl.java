package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.auditing.AssociatedItem;
import com.atlassian.jira.auditing.ChangedValue;
import com.google.common.collect.Sets;

import java.util.List;
import java.util.Set;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * @since 7.0
 */
final class UseBasedMigrationImpl extends UseBasedMigration {
    private static final boolean EVENT_SHOWS_IN_CLOUD_LOG = true;

    private final GlobalPermissionDao dao;

    UseBasedMigrationImpl(GlobalPermissionDao dao) {
        this.dao = notNull("dao", dao);
    }

    @Override
    MigrationState addUsePermissionToRoles(MigrationState state, Iterable<ApplicationKey> keys) {
        notNull("state", state);
        notNull("keys", keys);

        final Set<Group> adminGroups = dao.groupsWithAdminPermission();
        final Set<Group> useGroups = dao.groupsWithUsePermission();
        final Set<Group> allGroups = Sets.union(adminGroups, useGroups);
        final Set<Group> defaultGroups = Sets.difference(useGroups, adminGroups);

        MigrationState migratedState = state;
        for (ApplicationKey key : keys) {
            migratedState = migratedState.changeApplicationRole(key,
                    role -> role.addGroups(allGroups).addGroupsAsDefault(defaultGroups));

            List<ChangedValue> changedValues = getChangedValues(allGroups, defaultGroups, key.value());
            AuditEntry auditEntry = new AuditEntry(UseBasedMigrationImpl.class,
                    "Migrated groups to " + key.value() + ": " + allGroups.size(),
                    "Group with USE permission migrated to a role to ensure continued access for associated users.",
                    AssociatedItem.Type.APPLICATION_ROLE,
                    key.value(),
                    EVENT_SHOWS_IN_CLOUD_LOG,
                    changedValues,
                    AuditEntrySeverity.INFO);
            migratedState = migratedState.log(auditEntry);
        }

        return migratedState;
    }

    private List<ChangedValue> getChangedValues(final Set<Group> groups, final Set<Group> defaultGroups, String role) {
        return groups.stream()
                .map(group -> getMigrationChangedValue(role, group, defaultGroups))
                .collect(CollectorsUtil.toImmutableList());
    }

    private MigrationChangedValue getMigrationChangedValue(String role, final Group group, final Set<Group> defaultGroups) {
        String s = defaultGroups.contains(group) ? " (default)" : "";
        return new MigrationChangedValue(group.getName(), "USE", role + s);
    }
}
