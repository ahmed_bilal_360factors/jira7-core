package com.atlassian.jira.project.template.module;

import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;

import javax.annotation.concurrent.Immutable;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Immutable
public class Icon {
    private final WebResourceUrlProvider webResourceUrlProvider;
    private final String location;
    private final String moduleKey;
    private final String resourceName;

    public Icon() {
        this(null, null, null, null);
    }

    public Icon(WebResourceUrlProvider webResourceUrlProvider, String location, String moduleKey, String resourceName) {
        this.webResourceUrlProvider = webResourceUrlProvider;
        this.location = location;
        this.moduleKey = moduleKey;
        this.resourceName = resourceName;
    }

    /**
     * Returns the value of the {@code location} attribute for this icon.
     *
     * @return the value of the {@code location} attribute for this icon
     */
    public String location() {
        return location != null ? location : "";
    }

    /**
     * Returns the publicly-accessible URL for this Icon.
     *
     * @return a String containing the icon URL
     */
    public String url() {
        if (isBlank(location)) {
            return "";
        }

        return webResourceUrlProvider.getStaticPluginResourceUrl(moduleKey, resourceName, UrlMode.AUTO);
    }
}
