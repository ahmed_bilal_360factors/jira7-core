package com.atlassian.jira.upgrade;

import com.atlassian.jira.config.ReindexMessageManager;
import com.atlassian.jira.index.request.ReindexRequest;
import com.atlassian.jira.index.request.ReindexRequestManager;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.ReindexStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumSet;
import java.util.Set;

/**
 * Handles reindex operations for JIRA upgrades.
 */
public class UpgradeIndexManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpgradeIndexManager.class);

    private final ReindexRequestManager reindexRequestManager;
    private final ReindexMessageManager reindexMessageManager;

    public UpgradeIndexManager(
            final ReindexRequestManager reindexRequestManager,
            final ReindexMessageManager reindexMessageManager
    ) {
        this.reindexRequestManager = reindexRequestManager;
        this.reindexMessageManager = reindexMessageManager;
    }

    /**
     * Performs reindex.
     * <p>
     * If a immediate reindex is already requested a message on the admin screen will be displayed to indicate
     * a need of immediate reindex
     * </p>
     *
     * @return true if successful and there were no errors, false if there was an error performing reindex.
     */
    public boolean runReindexIfNeededAndAllowed(final Set<ReindexRequestType> reindexRequestTypes) {

        final boolean immediateReindexesRequested = reindexRequestManager.isReindexRequested(EnumSet.of(
                ReindexRequestType.IMMEDIATE));

        if (reindexRequestTypes.isEmpty()) {
            if (immediateReindexesRequested) {
                LOGGER.info("There is an immediate reindex request but a reindex is not being ran. Sending " +
                        "a message to the admin screen for admins to initiate a reindex");
                reindexMessageManager.pushRawMessage(null, "admin.upgrade.reindex.deferred");
                return true;
            } else {
                LOGGER.info("Reindexing is not allowed after this upgrade and there is no immediate reindex requests");
                return true;
            }
        } else {
            final boolean reindexesRequested = reindexRequestManager.isReindexRequested(reindexRequestTypes);
            if (reindexesRequested) {
                return executeReindex(reindexRequestTypes, immediateReindexesRequested);
            } else {
                LOGGER.info("There is no reindex requests of type {} so none will be run", reindexRequestTypes);
                return true;
            }
        }
    }

    private boolean executeReindex(
            final Set<ReindexRequestType> reindexRequestTypes,
            final boolean immediateReindexesRequested
    ) {
        LOGGER.debug("Reindex all data if indexing is turned on.");
        if (immediateReindexesRequested && reindexRequestTypes.contains(ReindexRequestType.IMMEDIATE)) {
            return executeImmediateReindex(reindexRequestTypes);
        } else {
            return executeDelayedReindex(reindexRequestTypes);
        }
    }

    private boolean executeImmediateReindex(final Set<ReindexRequestType> reindexRequestTypes) {
        LOGGER.info("Requesting immediate reindex");

        final Set<ReindexRequest> reindexRequests = reindexRequestManager.processPendingRequests(
                true,
                reindexRequestTypes,
                false
        );

        for (ReindexRequest reindexRequest : reindexRequests) {
            if (reindexRequest.getStatus() == ReindexStatus.FAILED) {
                return false;
            }
        }
        return true;
    }

    private boolean executeDelayedReindex(final Set<ReindexRequestType> reindexRequestTypes) {
        reindexRequestManager.processPendingRequests(false, reindexRequestTypes, true);
        LOGGER.info("Delayed reindex has been requested.");
        return true;
    }
}
