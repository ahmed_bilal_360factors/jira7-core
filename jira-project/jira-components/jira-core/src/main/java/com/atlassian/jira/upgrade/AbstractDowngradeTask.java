package com.atlassian.jira.upgrade;

import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.ofbiz.OfBizDelegator;

import javax.annotation.Nonnull;

/**
 * Abstract DowngradeTask - takes care of providing DB access via OfBizDelegator or DbConnectionManager.
 *
 * @since v6.4.6
 */
public abstract class AbstractDowngradeTask implements DowngradeTask {
    DbConnectionManager dbConnectionManager;
    OfBizDelegator ofBizDelegator;

    @Override
    @Nonnull
    public DbConnectionManager getDbConnectionManager() {
        return dbConnectionManager;
    }

    @Override
    @Nonnull
    public OfBizDelegator getOfBizDelegator() {
        return ofBizDelegator;
    }

    @Override
    public void setDbConnectionManager(@Nonnull final DbConnectionManager dbConnectionManager) {
        this.dbConnectionManager = dbConnectionManager;
    }

    @Override
    public void setOfBizDelegator(@Nonnull final OfBizDelegator ofBizDelegator) {
        this.ofBizDelegator = ofBizDelegator;
    }
}
