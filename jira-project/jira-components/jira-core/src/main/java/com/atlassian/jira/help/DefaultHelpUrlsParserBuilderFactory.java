package com.atlassian.jira.help;

import com.atlassian.fugue.Option;
import com.atlassian.jira.util.BuildUtilsInfo;

import javax.annotation.Nonnull;

/**
 * Factory of HelpUrlsParser builders.
 *
 * @since 7.0
 */
public class DefaultHelpUrlsParserBuilderFactory implements HelpUrlsParserBuilderFactory {

    private final LocalHelpUrls localUrls;
    private final BuildUtilsInfo buildUtilsInfo;

    public DefaultHelpUrlsParserBuilderFactory(final LocalHelpUrls localUrls, final BuildUtilsInfo buildUtilsInfo) {
        this.localUrls = localUrls;
        this.buildUtilsInfo = buildUtilsInfo;
    }

    @Nonnull
    @Override
    public HelpUrlsParserBuilder newBuilder() {
        return new DefaultHelpUrlsParserBuilder();
    }

    public class DefaultHelpUrlsParserBuilder implements HelpUrlsParserBuilder {
        private String defaultUrl;
        private String defaultTitle;
        private Option<String> applicationHelpSpace;

        private DefaultHelpUrlsParserBuilder() {
            this.applicationHelpSpace = Option.none();
            this.defaultUrl = null;
            this.defaultTitle = null;
        }

        @Nonnull
        @Override
        public HelpUrlsParserBuilder defaultUrl(@Nonnull final String url, final String title) {
            this.defaultUrl = url;
            this.defaultTitle = title;
            return this;
        }

        @Nonnull
        @Override
        public HelpUrlsParserBuilder applicationHelpSpace(@Nonnull final Option<String> applicationHelpSpace) {
            this.applicationHelpSpace = applicationHelpSpace;
            return this;
        }

        @Nonnull
        @Override
        public HelpUrlsParser build() {
            return new DefaultHelpUrlsParser(new SimpleHelpUrlBuilder.Factory(buildUtilsInfo.getDocVersion(), applicationHelpSpace),
                    localUrls, defaultUrl, defaultTitle);
        }
    }

}
