package com.atlassian.jira.issue.views.util.csv;

import java.util.Date;

import javax.annotation.Nullable;

/**
 * Used to format Dates that are a valid format for CSV exporting.
 *
 * @since 7.2.0
 */
public interface CsvDateFormatter {
    /**
     * Convert a date to a valid CSV value including the time
     * @param date to format
     * @return csv valid date format
     */
    String formatDateTime(@Nullable Date date);

    /**
     * Convert a date to a valid value
     * @param date to format
     * @return csv valid date format
     */
    String formatDate(@Nullable Date date);
}
