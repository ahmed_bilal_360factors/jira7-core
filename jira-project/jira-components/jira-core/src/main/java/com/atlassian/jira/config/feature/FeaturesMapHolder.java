package com.atlassian.jira.config.feature;

import com.atlassian.event.api.EventListener;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.tenancy.api.event.TenantArrivedEvent;
import com.atlassian.util.concurrent.ResettableLazyReference;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import static com.atlassian.collectors.CollectorsUtil.toImmutableSet;
import static com.google.common.collect.Iterables.concat;

/**
 * Initializes and manages map of JIRA features. This class is conceptually part of {@link
 * com.atlassian.jira.config.feature.DefaultFeatureManager} which controls lifecycle of those objects.
 */
public class FeaturesMapHolder {
    private final FeaturesLoader featuresLoader;
    private final ResettableLazyReference<Set<String>> enabledFeatures = new ResettableLazyReference<Set<String>>() {
        @Override
        protected Set<String> create() throws Exception {
            final Map<String, Boolean> allFeatures = initFeatures(
                    concat(ImmutableList.of(featuresLoader.loadCoreProperties()), getPluginFeatures())
            );
            return initEnabledFeatures(allFeatures);
        }
    };
    private volatile boolean jiraStarted;

    FeaturesMapHolder(final FeaturesLoader featuresLoader) {
        this.featuresLoader = featuresLoader;
    }

    Set<String> enabledFeatures() {
        return enabledFeatures.get();
    }

    private Iterable<Properties> getPluginFeatures() {
        return jiraStarted ?
                featuresLoader.loadPluginsFeatureProperties() :
                Collections.emptyList();
    }

    private Map<String, Boolean> initFeatures(final Iterable<Properties> allProperties) {
        final Map<String, Boolean> collector = new HashMap<>();
        for (final Properties singleProperties : allProperties) {
            for (final String property : singleProperties.stringPropertyNames()) {
                collector.put(property, Boolean.valueOf(singleProperties.getProperty(property)));
            }
        }
        return ImmutableMap.copyOf(collector);
    }

    private Set<String> initEnabledFeatures(final Map<String, Boolean> allFeatures) {
        return allFeatures.entrySet().stream()
                .filter(Map.Entry::getValue)
                .map(Map.Entry::getKey)
                .collect(toImmutableSet());
    }

    @EventListener
    @SuppressWarnings({"UnusedDeclaration"})
    public void onPluginEnabled(final PluginEnabledEvent event) {
        onPluginEvent(event.getPlugin());
    }

    @EventListener
    @SuppressWarnings({"UnusedDeclaration"})
    public void onPluginDisabled(final PluginDisabledEvent event) {
        onPluginEvent(event.getPlugin());
    }

    @EventListener
    @SuppressWarnings({"UnusedDeclaration"})
    public void onClearCache(final ClearCacheEvent event) {
        clearCache();
    }

    @EventListener
    @SuppressWarnings({"UnusedDeclaration"})
    public void onTenantArrived(final TenantArrivedEvent event) {
        clearCache();
    }

    public void start() {
        jiraStarted = true;
        clearCache();
    }

    private void onPluginEvent(final Plugin plugin) {
        if (!featuresLoader.hasFeatureResources(plugin)) {
            clearCache();
        }
    }

    void clearCache() {
        enabledFeatures.reset();
    }

}
