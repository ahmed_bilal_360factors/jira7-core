package com.atlassian.jira.help;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.util.JiraProductInformation;

import javax.annotation.Nonnull;

/**
 * To be used during db setup. Looks up information from 'product.properties' file.
 *
 * @since 7.0
 */
public class BootstrapHelpUrlsApplicationKeyProvider implements HelpUrlsApplicationKeyProvider {
    private final JiraProductInformation jiraProductInformation;

    public BootstrapHelpUrlsApplicationKeyProvider(final JiraProductInformation jiraProductInformation) {
        this.jiraProductInformation = jiraProductInformation;
    }

    @Nonnull
    @Override
    public ApplicationKey getApplicationKeyForUser() {
        return ApplicationKey.valueOf(jiraProductInformation.getSetupHelpApplicationKey());
    }
}
