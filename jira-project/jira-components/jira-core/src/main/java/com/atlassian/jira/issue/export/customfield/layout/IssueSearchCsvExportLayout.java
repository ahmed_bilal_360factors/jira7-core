package com.atlassian.jira.issue.export.customfield.layout;

import com.atlassian.jira.issue.export.customfield.CsvIssueExporter;
import com.atlassian.jira.issue.fields.Field;
import java.util.List;

/**
 * This represents the Layout for the {@link CsvIssueExporter}. It determines the number of columns needed for each
 * field as well as determining the order that the fields are in.
 *
 * @since 7.2
 */
public class IssueSearchCsvExportLayout {
    private final List<FieldLayout> fields;

    public IssueSearchCsvExportLayout(List<FieldLayout> fields) {
        this.fields = fields;
    }

    public List<FieldLayout> getFieldLayouts() {
        return fields;
    }

    /**
     * This represents the layout for a given field in the CsvLayout. For example, {@link com.atlassian.jira.issue.fields.ProjectSystemField}.
     */
    static public class FieldLayout {
        private final Field field;
        private final List<SubFieldLayout> subFields;

        public FieldLayout(final List<SubFieldLayout> subFields, Field field) {
            this.subFields = subFields;
            this.field = field;
        }

        public List<SubFieldLayout> getSubFields() {
            return subFields;
        }

        public Field getField() {
            return field;
        }
    }

    /**
     * Each field may contain multiple values within it. For example the ProjectSystemField outputs Key, Id, etc.
     * These sub fields can store multiple values so this stores the Id of the subfield, the column header and the
     * number of columns that is needed for it.
     */
     static public class SubFieldLayout {
        private final String fieldItemId;
        private final String columnHeader;
        private final Integer columnCount;

        public SubFieldLayout(String columnHeader, Integer columnCount, String fieldItemId) {
            this.fieldItemId = fieldItemId;
            this.columnHeader = columnHeader;
            this.columnCount = columnCount;
        }

        public String getFieldItemId() {
            return fieldItemId;
        }

        public String getColumnHeader() {
            return columnHeader;
        }

        public Integer getColumnCount() {
            return columnCount;
        }

    }
}
