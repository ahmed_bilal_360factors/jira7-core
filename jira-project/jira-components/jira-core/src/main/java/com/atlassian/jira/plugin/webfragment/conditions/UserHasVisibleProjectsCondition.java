package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.plugin.webfragment.conditions.cache.ConditionCacheKeys;
import com.atlassian.jira.plugin.webfragment.conditions.cache.RequestCachingConditionHelper;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Checks if the user can see at least one project with the permission.
 */
public class UserHasVisibleProjectsCondition extends AbstractProjectPermissionCondition {

    private final PermissionManager permissionManager;

    public UserHasVisibleProjectsCondition(PermissionManager permissionManager) {
        this.permissionManager = permissionManager;
    }

    @Override
    protected boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper, ProjectPermissionKey permissionKey) {
        return RequestCachingConditionHelper.cacheConditionResultInRequest(
                ConditionCacheKeys.custom("hasVisibleProjects", user, permissionKey),
                () -> permissionManager.hasProjects(permissionKey, user));
    }
}
