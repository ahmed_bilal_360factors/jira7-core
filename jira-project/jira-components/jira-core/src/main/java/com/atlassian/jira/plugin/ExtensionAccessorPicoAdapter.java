package com.atlassian.jira.plugin;

import com.atlassian.extension.base.CachingExtensionAccessor;
import com.atlassian.extension.plugins.PluginAccessorExtensionProvider;
import com.atlassian.plugin.manager.ProductPluginAccessor;

import com.google.common.collect.ImmutableList;

public class ExtensionAccessorPicoAdapter extends CachingExtensionAccessor {
    public ExtensionAccessorPicoAdapter(final ProductPluginAccessor pluginAccessor) {
        super(ImmutableList.of(new PluginAccessorExtensionProvider(pluginAccessor)));
    }
}
