package com.atlassian.jira.appconsistency.db;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.database.DatabaseUtil;
import com.atlassian.jira.help.StaticHelpUrls;
import com.atlassian.jira.ofbiz.DefaultOfBizConnectionFactory;
import com.atlassian.jira.startup.StartupCheck;
import org.apache.commons.httpclient.util.URIUtil;
import org.ofbiz.core.entity.config.DatasourceInfo;
import org.ofbiz.core.entity.config.JdbcDatasourceInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

import static com.atlassian.jira.config.database.SystemDatabaseConfigurationLoader.FILENAME_DBCONFIG;
import static org.apache.commons.lang3.StringUtils.contains;

public class MySqlConnectionUrlCheck implements StartupCheck {
    private static final Logger log = LoggerFactory.getLogger(MySqlConnectionUrlCheck.class);
    private final ConfigurationState configurationState;

    public MySqlConnectionUrlCheck(final ConfigurationState configurationState) {
        this.configurationState = configurationState;
    }

    @Override
    public String getName() {
        return "MySql connection url configuration check";
    }

    @Override
    public boolean isOk() {
        if (!configurationState.isConfigurationAvailable()) {
            return true;
        }
        final Optional<String> connectionUri = configurationState.getConnectionUri();
        if (configurationState.isMySql() && connectionUri.isPresent() && !configurationState.isUriMySql57Compatible(connectionUri.get())) {
            if (canWorkWithCurrentConfiguration()) {
                log.warn(getFaultDescription());
            } else {
                return false;
            }
        }
        return true;
    }


    boolean canWorkWithCurrentConfiguration() {
        Connection connection = null;
        try {
            connection = getConnection();
        } catch (SQLException e) {
            log.error("Exception occurred while trying to connect to the database: ", e);
            return false;
        } finally {
            DatabaseUtil.closeQuietly(connection);
        }
        return true;
    }

    private Connection getConnection() throws SQLException {
        return DefaultOfBizConnectionFactory.getInstance().getConnection();
    }

    @Nullable
    @Override
    public String getFaultDescription() {
        return "You have an error in your " + FILENAME_DBCONFIG + " file. Missing \"default_storage_engine\" in connection url. " +
                "The \"storage_engine\" parameter is deprecated and should be replaced with  \"default_storage_engine\"." +
                "Please refer to the " + getDocumentationLink() + ".";
    }

    @Nullable
    @Override
    public String getHTMLFaultDescription() {
        return "<p>You have an error in your " + FILENAME_DBCONFIG + " file. Missing \"default_storage_engine\" in connection url. " +
                "The \"storage_engine\" parameter is deprecated and should be replaced with  \"default_storage_engine\"." +
                "Please refer to the <a href=\"" + getDocumentationLink() + ">JIRA database documentation</a>.</p>";
    }

    String getDocumentationLink() {
        return StaticHelpUrls.getInstance().getUrl("mysql").getUrl();
    }

    public static class ConfigurationState {
        private final DatabaseConfigurationManager databaseConfigurationManager;

        public ConfigurationState(final DatabaseConfigurationManager databaseConfigurationManager) {
            this.databaseConfigurationManager = databaseConfigurationManager;
        }

        boolean isConfigurationAvailable() {
            return databaseConfigurationManager.isDatabaseSetup();
        }

        Optional<String> getConnectionUri() {
            return Optional.ofNullable(databaseConfigurationManager.getDatabaseConfiguration())
                    .map(DatabaseConfig::getDatasourceInfo)
                    .map(DatasourceInfo::getJdbcDatasource)
                    .map(JdbcDatasourceInfo::getUri);
        }

        boolean isUriMySql57Compatible(final String uri) {
            final String query = URIUtil.getQuery(uri);
            return contains(query, "default_storage_engine");
        }

        public boolean isMySql() {
            return databaseConfigurationManager.getDatabaseConfiguration().isMySql();
        }
    }
}
