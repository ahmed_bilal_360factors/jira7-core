package com.atlassian.jira.web.action.admin.notification;

import com.atlassian.jira.project.Project;
import org.ofbiz.core.entity.GenericEntityException;

public interface ProjectAware {
    public Long getProjectId();

    public void setProjectId(Long projectId);

    public Project getProject() throws GenericEntityException;
}
