package com.atlassian.jira.project.util;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.event.ClearCacheEvent;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.SetMultimap;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;
import java.util.Set;

@EventComponent
public class CachingProjectKeyStore implements ProjectKeyStore {
    private final ProjectKeyStore delegateProjectKeyStore;
    private final CachedReference<ProjectKeyCache> cache;

    public CachingProjectKeyStore(final ProjectKeyStore delegateProjectKeyStore, final CacheManager cacheManager) {
        this.delegateProjectKeyStore = delegateProjectKeyStore;
        this.cache = cacheManager.getCachedReference(getClass().getName() + ".cache",
                () -> new ProjectKeyCache(delegateProjectKeyStore.getAllProjectKeys()));

        refresh();
    }

    @EventListener
    public void onClearCache(final ClearCacheEvent event) {
        refresh();
    }

    @Nullable
    @Override
    public Long getProjectId(final String key) {
        return cache.get().getProjectId(key);
    }

    @Override
    public void addProjectKey(final Long projectId, final String projectKey) {
        delegateProjectKeyStore.addProjectKey(projectId, projectKey);
        refresh();
    }

    @Override
    public void deleteProjectKeys(final Long projectId) {
        delegateProjectKeyStore.deleteProjectKeys(projectId);
        refresh();
    }

    @Nonnull
    @Override
    public Map<String, Long> getAllProjectKeys() {
        return cache.get().getAllProjectKeys();
    }

    @Nullable
    @Override
    public Long getProjectIdByKeyIgnoreCase(final String projectKey) {
        return cache.get().getProjectIdByKeyIgnoreCase(projectKey);
    }

    @Nonnull
    @Override
    public Set<String> getProjectKeys(final Long projectId) {
        return cache.get().getProjectKeys(projectId);
    }

    @Override
    public void refresh() {
        cache.reset();
    }

    static class ProjectKeyCache {
        private final ImmutableMap<String, Long> projectsByKey;
        private final ImmutableSetMultimap<Long, String> projectKeys;
        private final ImmutableMap<String, Long> projectsByKeyIgnoreCase;

        public ProjectKeyCache(final Map<String, Long> projects) {
            final SetMultimap<Long, String> tmpProjectKeys = HashMultimap.create();
            for (final Map.Entry<String, Long> projectKey : projects.entrySet()) {
                tmpProjectKeys.put(projectKey.getValue(), projectKey.getKey());
            }
            projectsByKey = ImmutableMap.copyOf(projects);
            projectKeys = ImmutableSetMultimap.copyOf(tmpProjectKeys);
            projectsByKeyIgnoreCase = ImmutableSortedMap.copyOf(projectsByKey, String.CASE_INSENSITIVE_ORDER);
        }

        @Nullable
        public Long getProjectId(final String key) {
            return projectsByKey.get(key);
        }

        @Nonnull
        public Map<String, Long> getAllProjectKeys() {
            return projectsByKey;
        }

        @Nonnull
        public Set<String> getProjectKeys(final Long projectId) {
            return projectKeys.get(projectId);
        }

        @Nullable
        public Long getProjectIdByKeyIgnoreCase(final String projectKey) {
            return projectsByKeyIgnoreCase.get(projectKey);
        }
    }
}
