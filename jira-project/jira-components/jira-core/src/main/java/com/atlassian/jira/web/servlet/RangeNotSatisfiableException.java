package com.atlassian.jira.web.servlet;

/**
 * The requested range is not satisfiable as per RFC-7233.
 * This will result in a 416 (Range Not Satisfiable) response.
 *
 * @since v7.2
 */
public class RangeNotSatisfiableException extends Exception {
    private final int actualContentLength;

    public RangeNotSatisfiableException(String message, int actualContentLength) {
        super(message);
        this.actualContentLength = actualContentLength;
    }

    public int getActualContentLength() {
        return actualContentLength;
    }
}
