package com.atlassian.jira.web.filters;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarTranscoder;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.web.filters.util.InMemoryServletResponseWrapper;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * If an image with .svg extension is requested with query parameter
 * {@code ?format=PNG} then this filter will return a proper PNG image
 * instead of an SVG. {@code size} parameter is also supported.
 * <p>
 * Example: {@code /images/icons/priorities/minor.svg?format=PNG&size=xxxlarge@3x}.
 * </p>
 * <p>
 * If there is no {@code format=PNG} parameter or the requested image
 * does not have the {@code .svg} extension then the filter does nothing.
 * </p>
 */
public class SvgToPngTranscoderFilter implements Filter {
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse, final FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;

        final Avatar.Size size = getSize(request);

        if (needsTranscoding(request)) {
            final InMemoryServletResponseWrapper wrapper = new InMemoryServletResponseWrapper(response);

            filterChain.doFilter(request, wrapper);

            final byte[] svgBytes = wrapper.getOutputStream().getStream().toByteArray();

            if (svgBytes.length == 0) {
                return; // nothing to do here
            }

            final ByteArrayInputStream svgInputStream = new ByteArrayInputStream(svgBytes);

            Optional<AvatarTranscoder> avatarTranscoder = getAvatarTranscoder();
            final byte[] pngBytes = avatarTranscoder.orElseThrow(() -> new ServletException("AvatarTranscoder is not present"))
                    .transcodeAndTag(request.getServletPath(), svgInputStream, size);

            response.setContentLength(pngBytes.length);
            response.setContentType(AvatarManager.PNG_CONTENT_TYPE);
            response.getOutputStream().write(pngBytes);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    private Optional<AvatarTranscoder> getAvatarTranscoder() {
        return ComponentAccessor.getComponentSafely(AvatarTranscoder.class);
    }

    private boolean needsTranscoding(final HttpServletRequest request) {
        if (request.getServletPath().endsWith(".svg")) {
            final String format = request.getParameter("format");

            return isNotBlank(format) && "PNG".equals(format.toUpperCase());
        } else {
            return false;
        }
    }

    private Avatar.Size getSize(final HttpServletRequest request) {
        try {
            return Avatar.Size.getSizeFromParam(request.getParameter("size"));
        } catch (NoSuchElementException ex) {
            return Avatar.Size.defaultSize();
        }
    }

    @Override
    public void destroy() {
    }

}
