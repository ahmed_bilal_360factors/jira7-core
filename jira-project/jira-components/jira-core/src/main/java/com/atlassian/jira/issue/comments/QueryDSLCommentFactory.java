package com.atlassian.jira.issue.comments;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.QueryDSLProjectRoleFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.querydsl.core.Tuple;

import java.sql.Timestamp;
import java.util.Date;

import static com.atlassian.jira.model.querydsl.QAction.ACTION;

/**
 * Creates {@link Comment} implementations from QueryDSL tuple.
 *
 * @since 7.1
 */
public class QueryDSLCommentFactory {

    private final QueryDSLProjectRoleFactory queryDSLProjectRoleFactory;

    public QueryDSLCommentFactory(QueryDSLProjectRoleFactory queryDSLProjectRoleFactory) {
        this.queryDSLProjectRoleFactory = queryDSLProjectRoleFactory;
    }

    public Comment createComment(final Issue issue, final Tuple tuple) {
        if (tuple == null) {
            return null;
        }

        final Timestamp createdTimestamp = tuple.get(ACTION.created);
        final Timestamp updatedTimestamp = tuple.get(ACTION.updated);
        final Long roleLevel = tuple.get(ACTION.rolelevel);

        final ProjectRole projectRole = roleLevel != null ?
                queryDSLProjectRoleFactory.createProjectRole(tuple) : null;

        return new Comment2(tuple.get(ACTION.id),
                tuple.get(ACTION.author),
                tuple.get(ACTION.body),
                createdTimestamp != null ? new Date(createdTimestamp.getTime()) : null,
                updatedTimestamp != null ? new Date(updatedTimestamp.getTime()) : null,
                projectRole,
                roleLevel,
                tuple.get(ACTION.level),
                issue,
                tuple.get(ACTION.updateauthor));
    }

    private static class Comment2 implements Comment {

        private final Long id;
        private final String authorKey;
        private final String body;
        private final Date created;
        private final Date updated;
        private final ProjectRole roleLevel;
        private final Long roleLevelId;
        private final String groupLevel;
        private final Issue issue;
        private final String updatedAuthorKey;

        private Comment2(Long id,
                         String authorKey,
                         String body,
                         Date created,
                         Date updated,
                         ProjectRole roleLevel,
                         Long roleLevelId,
                         String groupLevel,
                         Issue issue,
                         String updatedAuthorKey) {
            this.id = id;
            this.authorKey = authorKey;
            this.body = body;
            this.created = created;
            this.updated = updated;
            this.roleLevel = roleLevel;
            this.roleLevelId = roleLevelId;
            this.groupLevel = groupLevel;
            this.issue = issue;
            this.updatedAuthorKey = updatedAuthorKey;
        }


        @Override
        public String getAuthor() {
            return authorKey;
        }

        @Override
        public String getAuthorKey() {
            return authorKey;
        }

        @Override
        public ApplicationUser getAuthorUser() {
            return ApplicationUsers.byKey(authorKey);
        }

        @Override
        public ApplicationUser getAuthorApplicationUser() {
            return getAuthorUser();
        }

        @Override
        public String getAuthorFullName() {
            final ApplicationUser authorUser = getAuthorUser();
            if (authorUser != null) {
                return authorUser.getDisplayName();
            }
            return authorKey;
        }

        @Override
        public String getBody() {
            return body;
        }

        @Override
        public Date getCreated() {
            return created;
        }

        @Override
        public String getGroupLevel() {
            return groupLevel;
        }

        @Override
        public Long getId() {
            return id;
        }

        @Override
        public Long getRoleLevelId() {
            return roleLevelId;
        }

        @Override
        public ProjectRole getRoleLevel() {
            return roleLevel;
        }

        @Override
        public Issue getIssue() {
            return issue;
        }

        @Override
        public String getUpdateAuthor() {
            return updatedAuthorKey;
        }

        @Override
        public ApplicationUser getUpdateAuthorUser() {
            return ApplicationUsers.byKey(updatedAuthorKey);
        }

        @Override
        public ApplicationUser getUpdateAuthorApplicationUser() {
            return getUpdateAuthorUser();
        }

        @Override
        public String getUpdateAuthorFullName() {
            final ApplicationUser updateAuthorUser = getUpdateAuthorUser();
            if (updateAuthorUser != null) {
                return updateAuthorUser.getDisplayName();
            }
            return updatedAuthorKey;
        }

        @Override
        public Date getUpdated() {
            return updated;
        }
    }
}
