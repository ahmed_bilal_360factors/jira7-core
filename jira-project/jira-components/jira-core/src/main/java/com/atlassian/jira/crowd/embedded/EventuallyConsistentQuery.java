package com.atlassian.jira.crowd.embedded;

import com.atlassian.crowd.search.query.entity.EntityQuery;

/**
 * A query, which is compatible with eventual consistency, e.g. it may return stale results.
 * @param <T> result type
 */
public class EventuallyConsistentQuery<T> extends EntityQuery<T> {

    public EventuallyConsistentQuery(EntityQuery<T> query) {
        super(query, query.getReturnType());
    }
}
