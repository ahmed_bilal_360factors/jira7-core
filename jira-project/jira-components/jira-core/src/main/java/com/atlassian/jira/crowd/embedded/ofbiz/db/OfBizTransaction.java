package com.atlassian.jira.crowd.embedded.ofbiz.db;

import org.ofbiz.core.entity.GenericTransactionException;

/**
 * Provides a simpler way of executing code within an OfBiz transaction.
 *
 * @since v7.0
 */
public interface OfBizTransaction {
    /**
     * @return true if the transaction has been committed or rolled back, false if it is still in progress.
     */
    boolean isProcessed();

    /**
     * Commits the transaction.
     */
    void commit();

    /**
     * Rolls back the transaction.
     */
    void rollback();

    /**
     * Wraps an OfBiz checked GenericTransactionException, turning it into an unchecked exception.
     */
    class TransactionException extends RuntimeException {
        public TransactionException(String message, GenericTransactionException cause) {
            super(message, cause);
        }
    }
}
