package com.atlassian.jira.imports.project.util;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.SortedMap;
import java.util.TreeMap;

public class AoImportTemporaryFilesImpl implements AoImportTemporaryFiles {
    private static final Logger log = LoggerFactory.getLogger(AoImportTemporaryFilesImpl.class);

    private final File parentDirectory;
    private final SortedMap<String, File> aoXmlFiles = new TreeMap<String, File>();

    public AoImportTemporaryFilesImpl(final File parentDirectory) throws IOException {
        this.parentDirectory = parentDirectory;
    }

    @Override
    public void registerAoEntities() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public File getParentDirectory() {
        return parentDirectory;
    }

    @Override
    public SortedMap<String, File> getAOXmlFiles() {
        return aoXmlFiles;
    }

    @Override
    public File createFileForEntity(final String entityName) {
        return new File(parentDirectory, entityName);
    }

    @Override
    public File getFileForEntity(final String entityName) {
        return aoXmlFiles.get(entityName);
    }

    @Override
    public SortedMap<String, Long> getAOEntityWeights() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void deleteTempFiles() {
        for (File file : aoXmlFiles.values()) {
            FileUtils.deleteQuietly(file);
        }
    }

    @Override
    public void registerAoXmlFile(final String entityName, final File file) {
        aoXmlFiles.put(entityName, file);
    }
}
