package com.atlassian.jira.template.velocity;

import com.atlassian.analytics.api.annotations.EventName;

import java.time.Duration;

/**
 * Event indicating that velocity engine has been initialized.
 *
 * @since v7.1
 */
@EventName("jira.velocity.engine.initialized")
public class VelocityEngineInitializedEvent {

    private final Duration initializationDuration;

    public VelocityEngineInitializedEvent(final Duration initializationDuration) {
        this.initializationDuration = initializationDuration;
    }

    public Long getInitializationTimeMillis() {
        return initializationDuration.toMillis();
    }
}
