package com.atlassian.jira.user;

import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.google.common.base.Objects;

/**
 * @since v6.0
 */
public final class ApplicationUserEntity {
    private final Long id;
    private final String key;
    private final String username;

    public ApplicationUserEntity(Long id, String key, String username) {
        this.id = id;
        this.key = key;
        this.username = IdentifierUtils.toLowerCase(username);
    }

    public Long getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    /**
     * Returns the lower-case of the username (because username must act case-insensitive).
     *
     * @return the lower-case of the username (because username must act case-insensitive).
     */
    public String getUsername() {
        return username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ApplicationUserEntity that = (ApplicationUserEntity) o;

        return Objects.equal(this.id, that.id) &&
                Objects.equal(this.key, that.key) &&
                Objects.equal(this.username, that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, key, username);
    }
}
