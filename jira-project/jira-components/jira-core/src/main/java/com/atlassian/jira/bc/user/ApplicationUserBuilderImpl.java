package com.atlassian.jira.bc.user;

import com.atlassian.crowd.embedded.impl.ImmutableUser;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.DelegatingApplicationUser;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public class ApplicationUserBuilderImpl implements ApplicationUserBuilder {
    private final ApplicationUser user;
    private boolean active;
    private String name;
    private String emailAddress;
    private String displayName;

    public ApplicationUserBuilderImpl(final ApplicationUser user) {
        this.user = user;
        this.active = user.isActive();
    }

    @Nonnull
    @Override
    public ApplicationUserBuilder active(final boolean active) {
        this.active = active;
        return this;
    }

    @Nonnull
    @Override
    public ApplicationUserBuilder name(final String name) {
        this.name = name;
        return this;
    }

    @Nonnull
    @Override
    public ApplicationUserBuilder emailAddress(final String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    @Nonnull
    @Override
    public ApplicationUserBuilder displayName(final String displayName) {
        this.displayName = displayName;
        return this;
    }

    @Nonnull
    @Override
    public ApplicationUser build() {
        final ImmutableUser.Builder userBuilder = ImmutableUser.newUser(user.getDirectoryUser());
        userBuilder.name(StringUtils.defaultIfBlank(name, user.getName()));
        userBuilder.emailAddress(StringUtils.defaultIfBlank(emailAddress, user.getEmailAddress()));
        userBuilder.displayName(StringUtils.defaultIfBlank(displayName, user.getDisplayName()));
        userBuilder.active(active);

        return new DelegatingApplicationUser(user.getId(), user.getKey(), userBuilder.toUser());
    }
}
