package com.atlassian.jira.board;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.issue.search.SearchService.ParseResult;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.query.Query;
import com.atlassian.query.clause.ChangedClauseImpl;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.history.TerminalHistoryPredicate;
import com.atlassian.query.operand.FunctionOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.SingleValueOperand;

import java.util.Optional;

import static com.atlassian.jira.bc.ServiceOutcomeImpl.error;
import static com.atlassian.jira.bc.ServiceOutcomeImpl.ok;
import static com.atlassian.jira.issue.status.category.StatusCategory.COMPLETE;
import static com.atlassian.jira.plugin.jql.function.NowFunction.FUNCTION_NOW;
import static com.atlassian.jira.util.ErrorCollection.Reason.VALIDATION_FAILED;
import static com.atlassian.query.operator.Operator.CHANGED;
import static com.atlassian.query.operator.Operator.DURING;
import static com.atlassian.query.order.SortOrder.DESC;

public class BoardQueryServiceImpl implements BoardQueryService {
    private static final String JQL_PARSE_ERROR = "core.board.data.service.jql.parse.error";

    private final I18nHelper.BeanFactory beanFactory;
    private final SearchService searchService;

    public BoardQueryServiceImpl(
            I18nHelper.BeanFactory beanFactory,
            SearchService searchService
    ) {
        this.beanFactory = beanFactory;
        this.searchService = searchService;
    }

    @Override
    public Query getAugmentedQueryForDoneIssues(Query query) {
        return JqlQueryBuilder.newBuilder(query)
                .where()
                .and()
                .addClause(incompleteIssuesAndCompleteIssuesWithinLast14Days())
                .endWhere()
                .orderBy()
                .issueKey(DESC)
                .endOrderBy()
                .buildQuery();
    }

    @Override
    public ServiceOutcome<Query> getBaseQueryForBoard(ApplicationUser user, Board board) {
        ParseResult jqlParseResult = searchService.parseQuery(user, board.getJql());

        return Optional.ofNullable(jqlParseResult.getQuery())
                .map(query -> ok(queryWithOnlyStandardIssues(query)))
                .orElse(error(beanFactory.getInstance(user).getText(JQL_PARSE_ERROR), VALIDATION_FAILED));
    }

    private Query queryWithOnlyStandardIssues(Query query) {
        return JqlQueryBuilder.newBuilder(query)
                .where()
                .and()
                .issueTypeIsStandard()
                .endWhere()
                .buildQuery();
    }

    private Clause incompleteIssuesAndCompleteIssuesWithinLast14Days() {
        TerminalHistoryPredicate historyPredicate = new TerminalHistoryPredicate(
                DURING,
                new MultiValueOperand(
                        new SingleValueOperand("-14d"),
                        new FunctionOperand(FUNCTION_NOW)
                )
        );
        Clause statusChangedWithin14Days = new ChangedClauseImpl("status", CHANGED, historyPredicate);
        
        Clause nonCompletedIssuesClause = JqlQueryBuilder.newClauseBuilder()
                .statusCategory().notEq(COMPLETE).buildClause();
        Clause completedIssuesClause = JqlQueryBuilder.newClauseBuilder()
                .statusCategory().eq(COMPLETE)
                .and()
                .addClause(statusChangedWithin14Days).buildClause();

        return JqlQueryBuilder.newClauseBuilder()
                .addClause(nonCompletedIssuesClause)
                .or()
                .addClause(completedIssuesClause)
                .buildClause();
    }
}
