package com.atlassian.jira.permission.management.beans;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.List;

@JsonAutoDetect
public class ProjectPermissionAddBean {
    ProjectPermissionBean permission;
    List<SecurityTypeBean> primarySecurityType;
    List<SecurityTypeBean> secondarySecurityType;

    private ProjectPermissionAddBean(ProjectPermissionBean permission, Iterable<SecurityTypeBean> primarySecurityType,
                                     Iterable<SecurityTypeBean> secondarySecurityType) {
        this.permission = permission;
        this.primarySecurityType = primarySecurityType != null ? ImmutableList.copyOf(primarySecurityType) : null;
        this.secondarySecurityType = secondarySecurityType != null ? ImmutableList.copyOf(secondarySecurityType) : null;
    }

    public ProjectPermissionBean getPermission() {
        return permission;
    }

    public void setPermission(ProjectPermissionBean permission) {
        this.permission = permission;
    }

    public List<SecurityTypeBean> getPrimarySecurityType() {
        return primarySecurityType;
    }

    public void setPrimarySecurityType(List<SecurityTypeBean> primarySecurityType) {
        this.primarySecurityType = primarySecurityType != null ? ImmutableList.copyOf(primarySecurityType) : null;
    }

    public List<SecurityTypeBean> getSecondarySecurityType() {
        return secondarySecurityType;
    }

    public void setSecondarySecurityType(List<SecurityTypeBean> secondarySecurityType) {
        this.secondarySecurityType = secondarySecurityType != null ? ImmutableList.copyOf(secondarySecurityType) : null;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(ProjectPermissionAddBean data) {
        return new Builder(data);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProjectPermissionAddBean that = (ProjectPermissionAddBean) o;

        return Objects.equal(this.permission, that.permission) &&
                Objects.equal(this.primarySecurityType, that.primarySecurityType) &&
                Objects.equal(this.secondarySecurityType, that.secondarySecurityType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(permission, primarySecurityType, secondarySecurityType);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("permission", permission)
                .add("primarySecurityType", primarySecurityType)
                .add("secondarySecurityType", secondarySecurityType)
                .toString();
    }

    public static final class Builder {


        private ProjectPermissionBean permission;
        private List<SecurityTypeBean> primarySecurityType = Lists.newArrayList();
        private List<SecurityTypeBean> secondarySecurityType = Lists.newArrayList();

        private Builder() {
        }

        private Builder(ProjectPermissionAddBean initialData) {

            this.permission = initialData.permission;
            this.primarySecurityType = initialData.primarySecurityType;
            this.secondarySecurityType = initialData.secondarySecurityType;
        }


        public Builder setPermission(ProjectPermissionBean permission) {
            this.permission = permission;
            return this;
        }


        public Builder setPrimarySecurityType(List<SecurityTypeBean> primarySecurityType) {
            this.primarySecurityType = primarySecurityType;
            return this;
        }


        public Builder addPrimarySecurityType(SecurityTypeBean primarySecurityType) {
            this.primarySecurityType.add(primarySecurityType);
            return this;
        }

        public Builder addPrimarySecurityType(Iterable<SecurityTypeBean> primarySecurityTypes) {
            for (SecurityTypeBean primarySecurityType : primarySecurityTypes) {
                addPrimarySecurityType(primarySecurityType);
            }
            return this;
        }


        public Builder setSecondarySecurityType(List<SecurityTypeBean> secondarySecurityType) {
            this.secondarySecurityType = secondarySecurityType;
            return this;
        }


        public Builder addSecondarySecurityType(SecurityTypeBean secondarySecurityType) {
            this.secondarySecurityType.add(secondarySecurityType);
            return this;
        }

        public Builder addSecondarySecurityType(Iterable<SecurityTypeBean> secondarySecurityTypes) {
            for (SecurityTypeBean secondarySecurityType : secondarySecurityTypes) {
                addSecondarySecurityType(secondarySecurityType);
            }
            return this;
        }

        public ProjectPermissionAddBean build() {
            return new ProjectPermissionAddBean(permission, primarySecurityType, secondarySecurityType);
        }
    }
}
