package com.atlassian.jira.index.property;

import com.atlassian.jira.index.IndexDocumentConfiguration;

import java.sql.Timestamp;

/**
 * @since v6.2
 */
public interface PluginIndexConfiguration {
    String getPluginKey();

    String getModuleKey();

    IndexDocumentConfiguration getIndexDocumentConfiguration();

    Timestamp getLastUpdated();
}
