package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.jira.config.group.GroupConfigurable;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.util.CaseFolding;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * @since v6.4
 */
public class OfBizApplicationRoleStore implements ApplicationRoleStore, GroupConfigurable {
    private static final String ENTITY_LICENSE_ROLE_GROUP = "LicenseRoleGroup";
    private static final String ENTITY_LICENSE_ROLE_DEFAULT = "LicenseRoleDefault";

    private static class GroupColumns {
        private static final String NAME = "licenseRoleName";
        private static final String GROUP_ID = "groupId";
        private static final String DEFAULT = "primaryGroup";
    }

    private static class DefaultColumns {
        private static final String NAME = "licenseRoleName";
    }

    private final OfBizDelegator delegator;

    public OfBizApplicationRoleStore(@Nonnull final OfBizDelegator delegator) {
        this.delegator = notNull("delegator", delegator);
    }

    @Nonnull
    @Override
    public ApplicationRoleData get(@Nonnull final ApplicationKey key) {
        notNull("key", key);

        final String appKey = toStringKey(key);
        final List<GenericValue> entries = getById(appKey);
        final Set<String> groups = Sets.newHashSetWithExpectedSize(entries.size());
        final Set<String> defaultGroups = new HashSet<>();

        for (GenericValue entry : entries) {
            final String groupKey = toGroupKey(entry.getString(GroupColumns.GROUP_ID));
            groups.add(groupKey);

            if (isDefault(entry)) {
                defaultGroups.add(groupKey);
            }
        }

        return new ApplicationRoleData(key, groups, defaultGroups, isSelectedByDefault(appKey));
    }

    @Nonnull
    @Override
    public ApplicationRoleData save(@Nonnull final ApplicationRoleData data) {
        notNull("data", data);

        //Delete all the current entries.
        final String applicationRoleKey = toStringKey(data.getKey());
        final Set<String> newDefaults = toGroupKeys(data.getDefaultGroups());
        final Set<String> newGroups = toGroupKeys(data.getGroups());

        for (GenericValue entry : getById(applicationRoleKey)) {
            //We use the raw value here on purpose. If the group is incorrectly stored in the database then
            //we will remove it and add it back later as a new row.
            final String currentGroupName = entry.getString(GroupColumns.GROUP_ID);
            if (newGroups.remove(currentGroupName)) {
                //Group is already in the database. Just check the default value.
                if (isDefault(entry)) {
                    //Unset as default if it is no longer in the defaults.
                    if (!newDefaults.contains(currentGroupName)) {
                        entry.set(GroupColumns.DEFAULT, false);
                        save(entry);
                    }
                } else {
                    //Set as default if it is now in the defaults.
                    if (newDefaults.contains(currentGroupName)) {
                        entry.set(GroupColumns.DEFAULT, true);
                        save(entry);
                    }
                }
            } else {
                //Group not in the wanted values so delete it.
                remove(entry);
            }
        }

        //Any groups left over not currently stored in the database.
        for (String newGroup : newGroups) {
            delegator.createValue(ENTITY_LICENSE_ROLE_GROUP, ImmutableMap.of(
                    GroupColumns.NAME, applicationRoleKey,
                    GroupColumns.GROUP_ID, newGroup,
                    GroupColumns.DEFAULT, newDefaults.contains(newGroup)));
        }

        if (!data.isSelectedByDefault() // desired state
                && isSelectedByDefault(applicationRoleKey)) { // database state
            delegator.removeByAnd(ENTITY_LICENSE_ROLE_DEFAULT,
                    ImmutableMap.of(DefaultColumns.NAME, applicationRoleKey));
        } else if (data.isSelectedByDefault() // desired state
                && !isSelectedByDefault(applicationRoleKey)) { // database state
            delegator.createValue(ENTITY_LICENSE_ROLE_DEFAULT,
                    ImmutableMap.of(DefaultColumns.NAME, applicationRoleKey));
        }

        return get(data.getKey());
    }

    @Override
    public void removeGroup(@Nonnull String groupName) {
        final String groupKey = toGroupKey(groupName);
        delegator.removeByAnd(ENTITY_LICENSE_ROLE_GROUP, ImmutableMap.of(GroupColumns.GROUP_ID, groupKey));
    }

    @Override
    public void removeByKey(@Nonnull final ApplicationKey applicationKey) {
        delegator.removeByAnd(ENTITY_LICENSE_ROLE_GROUP,
                ImmutableMap.of(DefaultColumns.NAME, toStringKey(applicationKey)));
        delegator.removeByAnd(ENTITY_LICENSE_ROLE_DEFAULT,
                ImmutableMap.of(DefaultColumns.NAME, toStringKey(applicationKey)));
    }

    private boolean isSelectedByDefault(final String licenseRoleName) {
        return !delegator.findByAnd(ENTITY_LICENSE_ROLE_DEFAULT,
                ImmutableMap.of(DefaultColumns.NAME, licenseRoleName)).isEmpty();
    }

    private void save(final GenericValue entry) {
        delegator.store(entry);
    }

    private void remove(final GenericValue entry) {
        delegator.removeValue(entry);
    }

    private List<GenericValue> getById(final String key) {
        return delegator.findByAnd(ENTITY_LICENSE_ROLE_GROUP, ImmutableMap.of(GroupColumns.NAME, key));
    }

    private static String toStringKey(ApplicationKey key) {
        return CaseFolding.foldString(key.value());
    }

    private static Set<String> toGroupKeys(Collection<String> groups) {
        return groups.stream().map(OfBizApplicationRoleStore::toGroupKey).collect(Collectors.toSet());
    }

    private static String toGroupKey(String group) {
        return IdentifierUtils.toLowerCase(group);
    }

    private boolean isDefault(GenericValue value) {
        //The default can be null on upgrade from 6.3 to 7.0.
        return Boolean.TRUE.equals(value.getBoolean(GroupColumns.DEFAULT));
    }

    @Override
    public boolean isGroupUsed(@Nonnull final Group group) {
        return !delegator.findByAnd(ENTITY_LICENSE_ROLE_GROUP,
                ImmutableMap.of(GroupColumns.GROUP_ID, toGroupKey(group.getName()))).isEmpty();
    }
}
