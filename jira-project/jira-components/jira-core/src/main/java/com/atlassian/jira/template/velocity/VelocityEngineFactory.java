package com.atlassian.jira.template.velocity;

import org.apache.velocity.app.VelocityEngine;

/**
 * Responsible for supplying an instance of the velocity engine that's been properly initialised and ready to render
 * templates.
 *
 * @since v5.1
 */
public interface VelocityEngineFactory {

    /**
     * Retrieves an instance of the Velocity Engine.
     *
     * @return A VelocityEngine instance.
     */
    VelocityEngine getEngine();
}
