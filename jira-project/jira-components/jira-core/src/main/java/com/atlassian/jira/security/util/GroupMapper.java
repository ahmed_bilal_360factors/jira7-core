package com.atlassian.jira.security.util;

import com.atlassian.jira.scheme.Scheme;

import java.util.Collection;

public interface GroupMapper {
    public Collection<Scheme> getMappedValues(String groupName);
}
