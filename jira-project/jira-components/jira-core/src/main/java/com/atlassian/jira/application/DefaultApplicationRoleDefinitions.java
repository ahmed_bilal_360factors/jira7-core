package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.host.plugin.PluginApplicationMetaData;
import com.atlassian.application.host.plugin.PluginApplicationMetaDataManager;
import com.atlassian.fugue.Option;
import com.atlassian.jira.license.JiraLicenseManager;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Set;

import static com.atlassian.fugue.Iterables.findFirst;

public class DefaultApplicationRoleDefinitions implements ApplicationRoleDefinitions {
    private final JiraLicenseManager licenseManager;
    private final PluginApplicationMetaDataManager metaDataManager;

    public DefaultApplicationRoleDefinitions(final JiraLicenseManager licenseManager,
                                             final PluginApplicationMetaDataManager metaDataManager) {
        this.metaDataManager = metaDataManager;
        this.licenseManager = licenseManager;
    }

    @Nonnull
    @Override
    public Iterable<ApplicationRoleDefinition> getDefined() {
        return Iterables.concat(getPlatformRoles(), getPluginRoles());
    }

    @Nonnull
    @Override
    public Option<ApplicationRoleDefinition> getDefined(final ApplicationKey applicationKey) {
        return findFirst(getDefined(), def -> def != null && def.key().equals(applicationKey));
    }

    @Override
    public boolean isDefined(final ApplicationKey applicationKey) {
        return getDefined(applicationKey).isDefined();
    }

    @Nonnull
    @Override
    public Iterable<ApplicationRoleDefinition> getLicensed() {
        final Set<ApplicationRoleDefinition> licensedAppRoleDefinitions = Sets.newHashSet();
        final Set<ApplicationKey> allLicensedApplicationKeys = licenseManager.getAllLicensedApplicationKeys();
        if (allLicensedApplicationKeys.isEmpty()) {
            return Option.none();
        }
        for (final ApplicationKey key : allLicensedApplicationKeys) {
            licensedAppRoleDefinitions.add(getIfDefinedOrCreateUnDefined(key));
        }
        return licensedAppRoleDefinitions;
    }

    private ApplicationRoleDefinition getIfDefinedOrCreateUnDefined(final ApplicationKey key) {
        final Option<ApplicationRoleDefinition> applicationRoleDefinitions = getDefined(key);
        if (applicationRoleDefinitions.isDefined()) {
            return applicationRoleDefinitions.get();
        } else {
            return new UnDefinedApplicationRoleDefinition(key);
        }
    }

    @Override
    public Option<ApplicationRoleDefinition> getLicensed(final ApplicationKey key) {
        if (!licenseManager.getAllLicensedApplicationKeys().contains(key)) {
            return Option.none();
        }
        return Option.some(getIfDefinedOrCreateUnDefined(key));
    }

    @Override
    public boolean isLicensed(final ApplicationKey key) {
        return getLicensed(key).isDefined();
    }

    private Iterable<PluginApplicationRoleDefinition> getPluginRoles() {
        return Iterables.transform(metaDataManager.getApplications(), PluginApplicationRoleDefinition::new);
    }

    private Iterable<CoreRoleDefinition> getPlatformRoles() {
        return Collections.singleton(CoreRoleDefinition.INSTANCE);
    }

    /**
     * Represents an {@link com.atlassian.jira.application.ApplicationRole} that is not currently defined by a plugin
     * (ie: no plugin defining this role is installed).
     */
    private static class UnDefinedApplicationRoleDefinition implements ApplicationRoleDefinition {
        private final ApplicationKey applicationKey;
        private final String name;

        private UnDefinedApplicationRoleDefinition(final ApplicationKey applicationKey) {
            this.applicationKey = applicationKey;
            this.name = UndefinedApplicationRoleName.of(applicationKey).getName();
        }

        @Override
        public ApplicationKey key() {
            return applicationKey;
        }

        @Override
        public String name() {
            return name;
        }
    }

    /**
     * Represents an {@link com.atlassian.jira.application.ApplicationRole} that is defined by a plugin.
     */
    private static class PluginApplicationRoleDefinition implements ApplicationRoleDefinition {
        private final PluginApplicationMetaData metaData;

        private PluginApplicationRoleDefinition(final PluginApplicationMetaData metaData) {
            this.metaData = metaData;
        }

        @Override
        public ApplicationKey key() {
            return metaData.getKey();
        }

        @Override
        public String name() {
            return metaData.getName();
        }
    }

    /**
     * Represents the JIRA Core application role, which is always installed.
     */
    static class CoreRoleDefinition implements ApplicationRoleDefinition {
        static final CoreRoleDefinition INSTANCE = new CoreRoleDefinition();

        private CoreRoleDefinition() {
        }

        @Override
        public ApplicationKey key() {
            return ApplicationKeys.CORE;
        }

        @Override
        public String name() {
            //Don't translate. This is a trademark.
            return "JIRA Core";
        }
    }
}
