package com.atlassian.jira.model.querydsl;

import com.querydsl.core.types.dsl.*;
import com.querydsl.sql.ColumnMetadata;

import javax.annotation.Generated;
import java.sql.Types;

/**
 * QNodeAssociation is a Querydsl query object.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QNodeAssociation extends JiraRelationalPathBase<NodeAssociationDTO> {
    public static final QNodeAssociation NODE_ASSOCIATION = new QNodeAssociation("NODE_ASSOCIATION");

    public final NumberPath<Long> sourceNodeId = createNumber("sourceNodeId", Long.class);
    public final StringPath sourceNodeEntity = createString("sourceNodeEntity");
    public final NumberPath<Long> sinkNodeId = createNumber("sinkNodeId", Long.class);
    public final StringPath sinkNodeEntity = createString("sinkNodeEntity");
    public final StringPath associationType = createString("associationType");
    public final NumberPath<Integer> sequence = createNumber("sequence", Integer.class);

    public QNodeAssociation(String alias) {
        super(NodeAssociationDTO.class, alias, "nodeassociation");
        addMetadata();
    }

    private void addMetadata() {
        addMetadata(sourceNodeId, ColumnMetadata.named("source_node_id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(sourceNodeEntity, ColumnMetadata.named("source_node_entity").withIndex(2).ofType(Types.VARCHAR).withSize(60));
        addMetadata(sinkNodeId, ColumnMetadata.named("sink_node_id").withIndex(3).ofType(Types.NUMERIC).withSize(18));
        addMetadata(sinkNodeEntity, ColumnMetadata.named("sink_node_entity").withIndex(4).ofType(Types.VARCHAR).withSize(60));
        addMetadata(associationType, ColumnMetadata.named("association_type").withIndex(5).ofType(Types.VARCHAR).withSize(60));
        addMetadata(sequence, ColumnMetadata.named("sequence").withIndex(6).ofType(Types.NUMERIC).withSize(9));
    }

    /**
     * Returns the ofbiz model's entity name for this query object.
     *
     * @return the ofbiz model's entity name for this query object.
     */
    @Override
    public String getEntityName() {
        return "NodeAssociation";
    }

}

