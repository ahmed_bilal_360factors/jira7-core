package com.atlassian.jira.servermetrics;

import com.atlassian.collectors.CollectorsUtil;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.NotThreadSafe;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.atlassian.collectors.CollectorsUtil.toNewArrayListWithCapacity;

/**
 * Collects server metrics for single request.
 */
@ParametersAreNonnullByDefault
@NotThreadSafe
class RequestMetricsCollector implements ServerMetricsDetailCollector {
    private List<CheckpointTiming> timingEventList = Lists.newArrayList();
    private Map<String, Duration> activities = Maps.newHashMap();
    private final Set<String> visitedCheckpoints = Sets.newHashSet();
    private final Stopwatch stopwatch;

    private final Duration userTimeStart;
    private final Duration cpuTimeStart;
    private final Duration garbageCollectionTimeStart;
    private final long garbageCollectionCountStart;

    public static RequestMetricsCollector started(Stopwatch stopwatch) {
        return new RequestMetricsCollector(
                stopwatch.start(),
                getCurrentThreadUserDuration(),
                getCurrentThreadCpuDuration(),
                getGarbageCollectionDuration(),
                getGarbageCollectionCount()
        );
    }

    private RequestMetricsCollector(Stopwatch stopwatch, Duration userTimeStart, Duration cpuTimeStart, Duration garbageCollectionTimeStart, long garbageCollectionCountStart) {
        this.stopwatch = stopwatch;
        this.userTimeStart = userTimeStart;
        this.cpuTimeStart = cpuTimeStart;
        this.garbageCollectionTimeStart = garbageCollectionTimeStart;
        this.garbageCollectionCountStart = garbageCollectionCountStart;
    }

    @Override
    public void checkpointReached(String checkpointName) {
        visitedCheckpoints.add(checkpointName);

        timingEventList.add(
                new CheckpointTiming(
                        checkpointName,
                        Duration.ofNanos(stopwatch.elapsed(TimeUnit.NANOSECONDS))
                ));
    }

    @Override
    public void checkpointReachedOnce(String checkpointName) {
        if (!visitedCheckpoints.contains(checkpointName)) {
            checkpointReached(checkpointName);
        }
    }

    @Override
    public void checkpointReachedOverride(String checkpointName) {
        if (visitedCheckpoints.contains(checkpointName)) {
            timingEventList = timingEventList
                    .stream()
                    .filter(checkpointOccurence -> !checkpointOccurence.checkpointName.equals(checkpointName))
                    .collect(toNewArrayListWithCapacity(timingEventList.size()));
        }

        checkpointReached(checkpointName);
    }

    @Override
    public void addTimeSpentInActivity(String activityName, Duration duration) {
        activities.compute(
                activityName,
                (name, exisitingData) -> exisitingData == null ? duration : duration.plus(exisitingData));
    }

    public void setTimeSpentInActivity(String activityName, Duration duration) {
        activities.put(activityName, duration);
    }

    @Nonnull
    public TimingInformation getCurrentTiming() {
        final ImmutableList<CheckpointTiming> activityTiming =
                activities
                        .entrySet()
                        .stream()
                        .map(stringDurationEntry -> new CheckpointTiming(stringDurationEntry.getKey(), stringDurationEntry.getValue()))
                        .collect(CollectorsUtil.toImmutableListWithCapacity(activities.size()));

        return new TimingInformation(
                ImmutableList.copyOf(timingEventList),
                activityTiming,
                Duration.ofNanos(stopwatch.elapsed(TimeUnit.NANOSECONDS)),
                getCurrentThreadUserDuration().minus(userTimeStart),
                getCurrentThreadCpuDuration().minus(cpuTimeStart),
                getGarbageCollectionDuration().minus(garbageCollectionTimeStart),
                getGarbageCollectionCount() - garbageCollectionCountStart
        );
    }

    private static long getGarbageCollectionCount() {
        return ManagementFactory.getGarbageCollectorMXBeans()
                .stream()
                .map(GarbageCollectorMXBean::getCollectionCount)
                .reduce(Long::sum)
                .orElse(0L);
    }

    @Nonnull
    private static Duration getGarbageCollectionDuration() {
        return ManagementFactory.getGarbageCollectorMXBeans()
                .stream()
                .map(GarbageCollectorMXBean::getCollectionTime)
                .reduce(Long::sum)
                .map(Duration::ofMillis)
                .orElse(Duration.ZERO);
    }

    @Nonnull
    private static Duration getCurrentThreadCpuDuration() {
        return Duration.ofNanos(ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime());
    }

    @Nonnull
    private static Duration getCurrentThreadUserDuration() {
        return Duration.ofNanos(ManagementFactory.getThreadMXBean().getCurrentThreadUserTime());
    }
}
