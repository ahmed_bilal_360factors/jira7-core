package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.gadgets.dashboard.DashboardId;
import com.atlassian.gadgets.dashboard.spi.DashboardPermissionService;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.portal.PortalPageService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.dashboard.DashboardUtil;
import com.atlassian.jira.plugin.webfragment.JiraWebContext;
import com.atlassian.jira.plugin.webfragment.JiraWebInterfaceManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

/**
 * Checks if the current user is the owner of a given dashboard
 *
 * @since v4.0
 */
public class IsDashboardOwnerCondition implements Condition {
    public static final String CONTEXT_KEY_DASHBOARD_ID = "dashboardId";

    public void init(final Map<String, String> params) throws PluginParseException {
    }

    public boolean shouldDisplay(final Map<String, Object> context) {
        final ApplicationUser appUser = getApplicationUser(context);
        final DashboardId dashboardId = (DashboardId) context.get(CONTEXT_KEY_DASHBOARD_ID);

        if (dashboardId != null && DashboardUtil.toLong(dashboardId) != null && appUser != null) {
            PortalPage portalPage = getPortalPageService().getPortalPage(new JiraServiceContextImpl(appUser), DashboardUtil.toLong(dashboardId));
            return appUser.equals(portalPage.getOwner());
        }
        return false;
    }

    private ApplicationUser getApplicationUser(final Map<String, Object> context) {
        return JiraWebContext.from(context).getUser().orElse(null);
    }

    PortalPageService getPortalPageService() {
        return ComponentAccessor.getComponentOfType(PortalPageService.class);
    }
}