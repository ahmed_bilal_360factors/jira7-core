package com.atlassian.jira.cluster.zdu.analytics;

import com.atlassian.analytics.api.annotations.EventName;

/**
 * @since v7.3
 */
@EventName("zdu.upgrade-state.approve")
public class UpgradeApprovedAnalytics {
    private final Integer nodeCount;
    private final Long fromBuildNumber;
    private final Long toBuildNumber;

    public UpgradeApprovedAnalytics(Integer nodeCount, Long fromBuildNumber, Long toBuildNumber) {
        this.nodeCount = nodeCount;
        this.fromBuildNumber = fromBuildNumber;
        this.toBuildNumber = toBuildNumber;
    }

    public Integer getNodeCount() {
        return nodeCount;
    }

    public Long getFromBuildNumber() {
        return fromBuildNumber;
    }

    public Long getToBuildNumber() {
        return toBuildNumber;
    }
}
