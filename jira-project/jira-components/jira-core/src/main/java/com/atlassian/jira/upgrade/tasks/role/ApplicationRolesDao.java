package com.atlassian.jira.upgrade.tasks.role;

/**
 * A simple application role DAO for renaissance migration. It is only safe to use in migration from JIRA 6.x to
 * JIRA 7.0.
 * <p>
 * NOTE: This class it written to access the database as it was in JIRA 6.x and should not be updated to reflect
 * changes that happen after this point.
 *
 * @since v7.0
 */
abstract class ApplicationRolesDao {
    /**
     * Return the {@link ApplicationRoles} as currently stored in the JIRA database.
     *
     * @return the {@link ApplicationRoles} as currently stored in the JIRA database.
     */
    abstract ApplicationRoles get();

    /**
     * Save the passed {@link ApplicationRoles} to the JIRA databse.
     *
     * @param applicationRoles the roles to save to the database.
     */
    abstract void put(final ApplicationRoles applicationRoles);
}
