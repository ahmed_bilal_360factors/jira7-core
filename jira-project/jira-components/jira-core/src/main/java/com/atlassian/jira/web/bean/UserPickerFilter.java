package com.atlassian.jira.web.bean;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.gzipfilter.org.apache.commons.lang.math.NumberUtils;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.user.search.UserSearchParams;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.LazyLoadingApplicationUser;
import com.atlassian.jira.user.UserFilter;
import com.atlassian.jira.user.UserFilterManager;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

public class UserPickerFilter extends PagerFilter {
    /**
     * When doing user queries involving groups and other criteria, if we get more results than this from the criteria
     * query then do a second crowd query just for groups and intersect results, otherwise just iterate over criteria
     * results and ask crowd to filter each one individually.
     */
    private static final int MEMBERSHIP_CRITERIA_THRESHOLD = 1000;

    private UserFilter filter;

    private String nameFilter;
    private String emailFilter;
    private String group;

    private final FieldConfigManager fieldConfigManager;
    private final JiraServiceContext jiraServiceContext;
    private final PermissionManager permissionManager;
    private final UserFilterManager userFilterManager;
    private final UserSearchService userSearchService;


    // custom fields
    /**
     * id of custom field
     */
    private String element;

    /**
     * field configuration id of custom field in the current context
     */
    private Long fieldConfigId;
    /**
     * project ids of the projects in the current context, to help infer project role related info for custom field
     */
    private Collection<Long> projectIds;

    public UserPickerFilter(final FieldConfigManager fieldConfigManager,
                            final JiraServiceContext jiraServiceContext,
                            final PermissionManager permissionManager,
                            final UserFilterManager userFilterManager,
                            final UserSearchService userSearchService) {
        this.fieldConfigManager = fieldConfigManager;
        this.jiraServiceContext = jiraServiceContext;
        this.permissionManager = permissionManager;
        this.userFilterManager = userFilterManager;
        this.userSearchService = userSearchService;
    }

    @SuppressWarnings("UnusedDeclaration")
    public String getNameFilter() {
        return nameFilter;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setNameFilter(final String nameFilter) {
        this.nameFilter = FilterUtils.verifyString(nameFilter);
    }

    @SuppressWarnings("UnusedDeclaration")
    public String getEmailFilter() {
        return emailFilter;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setEmailFilter(final String emailFilter) {
        this.emailFilter = FilterUtils.verifyString(emailFilter);
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(final String group) {
        this.group = FilterUtils.verifyString(group);
    }

    public String getElement() {
        return element;
    }

    public void setElement(final String element) {
        this.element = element;
    }

    public Long getFieldConfigId() {
        return fieldConfigId;
    }

    public void setFieldConfigId(final Long fieldConfigId) {
        this.fieldConfigId = fieldConfigId;
    }

    public Collection<Long> getProjectIds() {
        return projectIds;
    }

    /**
     * Setter method for BeanUtils to inject projectIds.
     * The name of the multi-valued url query parameter is projectId, so it has to be named as setProjectId().
     *
     * @param projectIds the list of project id's
     */
    public void setProjectId(final String[] projectIds) {
        this.projectIds = Sets.newHashSetWithExpectedSize(projectIds.length);
        for (String projectIdStr : projectIds) {
            long projectId = NumberUtils.toLong(projectIdStr, -1);
            if (projectId != -1) {
                this.projectIds.add(projectId);
            }
        }
    }

    /**
     * Get a list of users based on the parameters of the filter
     **/
    public List<ApplicationUser> getFilteredUsers() throws Exception {
        final UserFilter filter = getFilter();
        final Collection<Long> projectIdSet = CustomFieldUtils.getProjectIdsForUser(
                jiraServiceContext.getLoggedInApplicationUser(), projectIds, permissionManager, filter);
        final Comparator<String> userNameStringComparator = new UserNameStringComparator(jiraServiceContext.getI18nBean().getLocale());
        final UserSearchParams userSearchParams = UserSearchParams.builder()
                .allowEmptyQuery(true)
                .canMatchEmail(true)
                .filter(filter)
                .filterByProjectIds(projectIdSet)
                .sorted(false)
                .build();

        final CrowdService crowdService = ComponentAccessor.getComponentOfType(CrowdService.class);

        //If there is no group filter the query can be done fairly quickly
        List<String> userNamesByFilter = userSearchService.findUserNames(nameFilter, emailFilter, userSearchParams);
        if (group == null) {
            //Returned list may not be modifiable, so make a new one
            userNamesByFilter = new ArrayList<>(userNamesByFilter);

            Collections.sort(userNamesByFilter, userNameStringComparator);
            return makeLazyApplicationUsersFromNames(userNamesByFilter, crowdService);
        } else {
            //The slow path, but how slow depends on the name and e-mail filters
            //If there aren't too many results, it is cheaper to just check each user against Crowd
            //(versus loading the whole group's users up which could take ages)
            if (userNamesByFilter.size() > MEMBERSHIP_CRITERIA_THRESHOLD) {
                // Further filter by group. UserFilter supports OR but not AND of the groups/roles filtering.
                // We might add that support in the future if there are more usage of such AND-ed filtering.
                // For now, we do the AND separately here.
                final UserFilter filterByGroup = new UserFilter(true, null, ImmutableSet.of(group));
                final UserSearchParams userSearchParamsWithGroup = UserSearchParams.builder()
                        .allowEmptyQuery(true)
                        .canMatchEmail(true)
                        .filter(filterByGroup)
                        .build();
                final List<String> usersByGroup = userSearchService.findUserNames(nameFilter, emailFilter, userSearchParamsWithGroup);

                List<String> intersection = intersectLists(userNameStringComparator, userNamesByFilter, usersByGroup);
                return makeLazyApplicationUsersFromNames(intersection, crowdService);
            } else {
                //Just ask each user in the result list for membership and filter them
                List<String> fullMatches = new ArrayList<>();
                for (String criteriaMatchingUser : userNamesByFilter) {
                    if (crowdService.isUserMemberOfGroup(criteriaMatchingUser, group)) {
                        fullMatches.add(criteriaMatchingUser);
                    }
                }
                Collections.sort(fullMatches, userNameStringComparator);
                return makeLazyApplicationUsersFromNames(fullMatches, crowdService);
            }
        }
    }

    /**
     * Intersect the two lists and return the intersection ordered based on {@code userNameComparator}.
     */
    @VisibleForTesting
    <T> List<T> intersectLists(final Comparator<? super T> userNameComparator, final List<T> list1, final List<T> list2) {
        if (list1 == null || list2 == null) {
            return ImmutableList.of();
        }
        // Not really taking an advantage of the fact that the two lists are already sorted.
        Set<T> intersection = new TreeSet<>(userNameComparator);
        intersection.addAll(list1);
        intersection.retainAll(new HashSet<>(list2));

        return ImmutableList.copyOf(intersection);
    }

    private UserFilter getFilter() {
        if (filter == null) {
            if (fieldConfigId == null) {
                filter = UserFilter.DISABLED;
            } else {
                FieldConfig fieldConfig = fieldConfigManager.getFieldConfig(fieldConfigId);
                if (fieldConfig == null) {
                    filter = UserFilter.DISABLED;
                } else {
                    filter = userFilterManager.getFilter(fieldConfig);
                }
            }
        }
        return filter;
    }

    private List<ApplicationUser> makeLazyApplicationUsersFromNames(Collection<String> userNames, CrowdService crowdService) {
        List<ApplicationUser> users = new ArrayList<>(userNames.size());
        for (String userName : userNames) {
            users.add(new LazyLoadingApplicationUser(userName, crowdService));
        }
        return users;
    }

    private static class UserNameStringComparator implements Comparator<String> {
        private final Collator collator;

        public UserNameStringComparator(Locale locale) {
            this.collator = Collator.getInstance(locale);
            // Make this case insensitive
            this.collator.setStrength(Collator.SECONDARY);
        }

        @Override
        public int compare(String name1, String name2) {
            if (name1 == null || name2 == null) {
                throw new RuntimeException("Null user name");
            }

            return collator.compare(name1, name2);
        }
    }
}
