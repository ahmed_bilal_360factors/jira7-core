package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.core.util.Clock;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.license.Jira6xServiceDeskPluginEncodedLicenseSupplier;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.RenaissanceMigrationStatus;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.web.util.ExternalLinkUtil;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * @since 7.0
 */
public final class RenaissanceMigrationImpl implements RenaissanceMigration {
    private final MigrationStateDao migrationStateDao;
    private final List<MigrationTask> migrationTasks;
    private final ExternalLinkUtil externalLinkUtil;
    private final MigrationValidator migrationValidator;
    private final RenaissanceMigrationStatus migrationStatus;
    private final Logger log;
    private final Clock clock;

    @Inject
    public RenaissanceMigrationImpl(ApplicationProperties properties,
                                    JiraLicenseManager licenseManager,
                                    ApplicationRoleManager applicationRoleManager,
                                    ExternalLinkUtil externalLinkUtil,
                                    OfBizDelegator db, CrowdService crowdService,
                                    RenaissanceMigrationStatus migrationStatus,
                                    ApplicationProperties applicationProperties, Clock clock) {
        notNull("crowdService", crowdService);
        notNull("properties", properties);
        notNull("licenseManager", licenseManager);
        notNull("db", db);
        notNull("applicationRoleManager", applicationRoleManager);
        this.clock = notNull("clock", clock);
        log = LoggerFactory.getLogger(MigrationValidatorImpl.class);

        final GlobalPermissionDao globalPermissionDao = new GlobalPermissionDaoImpl(db);
        this.externalLinkUtil = notNull("externalLinkUtil", externalLinkUtil);
        this.migrationStatus = notNull("migrationVersionStore", migrationStatus);
        final LicenseDao licenseDao = new LicenseDaoImpl(properties, db, licenseManager);
        final ApplicationRolesDao rolesDao = new ApplicationRolesDaoImpl(db, applicationRoleManager);
        final UseBasedMigration useBasedMigration = new UseBasedMigrationImpl(globalPermissionDao);
        this.migrationStateDao = new MigrationStateDaoImpl(licenseDao, rolesDao, new MigrationLogDaoImpl(db));
        final Jira6xServiceDeskPluginEncodedLicenseSupplier sdLicenseSupplier =
                new Jira6xServiceDeskPluginEncodedLicenseSupplier(properties);
        final MigrationGroupService migrationGroupService = new MigrationGroupServiceImpl(globalPermissionDao,
                crowdService);

        final Jira6xServiceDeskLicenseProvider jira6xServiceDeskLicenseProvider
                = new Jira6xServiceDeskLicenseProviderImpl(sdLicenseSupplier, licenseDao);

        this.migrationValidator = new MigrationValidatorImpl(globalPermissionDao, jira6xServiceDeskLicenseProvider,
                migrationGroupService);

        //The order of these tasks is very important. Think before adding to this list.
        this.migrationTasks = ImmutableList.of(new Move6xLicenseTo70Store(licenseDao),
                new Move6xServiceDeskLicenseTo70Store(sdLicenseSupplier),
                new Move6xUsePermissionOverToCoreAndSoftwareApplications(useBasedMigration),
                new MoveJira6xServiceDeskPermissions(jira6xServiceDeskLicenseProvider, new ServiceDeskPropertySetDaoImpl(db),
                        new MoveJira6xTBPServiceDeskPermissions(useBasedMigration),
                        new MoveJira6xABPServiceDeskPermissions(migrationGroupService, globalPermissionDao,
                                applicationProperties)));
    }

    @VisibleForTesting
    RenaissanceMigrationImpl(MigrationStateDao migrationStateDao,
                             ExternalLinkUtil externalLinkUtil,
                             Iterable<? extends MigrationTask> migrationTasks,
                             MigrationValidator migrationValidator,
                             RenaissanceMigrationStatus migrationStatus, Clock clock, Logger logger) {
        this.migrationTasks = ImmutableList.copyOf(notNull("migrationTasks", migrationTasks));
        this.migrationStateDao = notNull("migrationStateDao", migrationStateDao);
        this.externalLinkUtil = notNull("externalLinkUtil", externalLinkUtil);
        this.migrationValidator = notNull("migrationValidator", migrationValidator);
        this.migrationStatus = notNull("migrationPredicate", migrationStatus);
        this.clock = notNull("clock", clock);
        this.log = logger;
    }

    @Override
    public MigrationState executeTasksAndValidate() {
        final MigrationState original = migrationStateDao.get();
        //If there are no licenses in the store we don't move them over as we assume the user entered in a license.
        //This is not 100% correct (e.g. maybe a license already exists in the store) but is a reasonable indicator.
        final boolean suppliedByUser = !original.licenses().isEmpty();
        MigrationState migrationState = original;
        Date taskStartTime;
        Date taskEndTime;
        for (final MigrationTask migrationMigrationTask : migrationTasks) {
            taskStartTime = clock.getCurrentDate();
            migrationState = migrationMigrationTask.migrate(migrationState, suppliedByUser);
            taskEndTime = clock.getCurrentDate();
            log.info("Elapsed time of " + (taskEndTime.getTime() - taskStartTime.getTime())
                    + "ms for migration task: " + migrationMigrationTask.getClass().getSimpleName());
        }
        taskStartTime = clock.getCurrentDate();
        migrationValidator.validate(original, migrationState);
        taskEndTime = clock.getCurrentDate();
        log.info("Elapsed time of " + (taskEndTime.getTime() - taskStartTime.getTime())
                + "ms for migration validation task");
        return migrationState;
    }

    @Override
    public void migrate() {
        try {
            MigrationState migrationState = executeTasksAndValidate();
            migrationStateDao.put(migrationState);
            migrationState.afterSaveTasks().forEach(Runnable::run);
            migrationStatus.markDone();
        } catch (MigrationFailedException e) {
            final String supportLink = externalLinkUtil.getProperty("external.link.jira.support.site");
            final String msg = String.format("JIRA migration failed. Please contact Atlassian Support at %s.", supportLink);
            throw new MigrationFailedException(msg, e);
        }
    }
}
