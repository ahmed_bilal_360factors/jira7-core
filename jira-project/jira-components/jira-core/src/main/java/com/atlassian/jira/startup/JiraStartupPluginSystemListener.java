package com.atlassian.jira.startup;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.plugin.JiraPluginSystemListener;
import com.atlassian.plugin.event.events.PluginContainerRefreshedEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginEnablingEvent;
import com.atlassian.plugin.event.events.PluginFrameworkDelayedEvent;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;
import com.atlassian.plugin.event.events.PluginFrameworkStartingEvent;
import com.atlassian.plugin.event.events.PluginFrameworkWarmRestartedEvent;
import com.atlassian.plugin.util.PluginUtils;
import com.google.common.annotations.VisibleForTesting;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * Event listener for internal JIRA events that the JiraStartupChecklist cares about.
 *
 * @see JiraStartupChecklist
 * @see JiraStartupState
 * @since 4.3.1
 */
@SuppressWarnings({"unused", "MethodMayBeStatic"})  // Event listeners are used indirectly
public class JiraStartupPluginSystemListener implements JiraPluginSystemListener {
    static final int PLUGIN_ENABLING_WAIT_PERIOD = PluginUtils.getDefaultEnablingWaitPeriod();

    // Strictly speaking, these don't have to add to 100.  It just makes more sense if they do.
    // The "refreshed" events are what really count, here.  They mean that the OSGi bundle has finished
    // refreshing and the plugin is available except for the actual plugin enabled event, which is usually
    // very fast by comparison.  The "enabling" and "enabled" events come in huge bursts for all of the
    // plugins at once, so while they count towards to overall progress, they are normally only a tiny
    // portion of the loading time.
    private static final int WEIGHT_FOR_ENABLING = 1;
    private static final int WEIGHT_FOR_REFRESHED = 98;
    private static final int WEIGHT_FOR_ENABLED = 1;

    private static final int TOTAL_WEIGHT = WEIGHT_FOR_ENABLING + WEIGHT_FOR_REFRESHED + WEIGHT_FOR_ENABLED;

    /**
     * Portion of the weight to "fudge" by assuming time elapsed implies progress.
     */
    private static final double FUDGE_FACTOR = 0.3;

    /**
     * Scaling factor to reserve room for late startup plugins.
     * This is the relative weighting between the early startup and late startup plugin phases.
     * For example, a factor of 0.9 means that 90% of the budget is allocated to early startup
     * and the remaining 10% is allocated to late startup.
     */
    private static final double EARLY_STARTUP_FACTOR = 0.95;

    private static final AtomicReference<Tracker> TRACKER =
            new AtomicReference<>(new StartupTracker(0.0, EARLY_STARTUP_FACTOR));

    /**
     * Creates a new JiraStartupPluginSystemListener.
     *
     * @param eventPublisher an EventPublisher
     */
    public JiraStartupPluginSystemListener(EventPublisher eventPublisher) {
        eventPublisher.register(this);
    }

    /**
     * Creates a new tracker to monitor the progress of the plugin framework startup.
     *
     * @param event ignored
     */
    @EventListener
    public void onPluginSystemStarting(PluginFrameworkStartingEvent event) {
        TRACKER.set(new StartupTracker(0.0, EARLY_STARTUP_FACTOR));
    }

    /**
     * Dispatches the "Plugin System Loaded" event to the current JIRA startup state.
     *
     * @param event a PluginFrameworkStartedEvent
     */
    @EventListener
    public void onPluginSystemStarted(PluginFrameworkStartedEvent event) {
        JiraStartupChecklist.getInstance().startupState().onPluginSystemStarted();
        TRACKER.set(new DoneTracker());
    }

    /**
     * Dispatches the "Plugin System Shutdown" event to the current JIRA startup state.
     *
     * @param event a PluginFrameworkShutdownEvent
     */
    @EventListener
    public void onPluginSystemShutdown(PluginFrameworkShutdownEvent event) {
        JiraStartupChecklist.getInstance().startupState().onPluginSystemStopped();
    }

    /**
     * Dispatches the "Plugin System Warn Restarting" event to the current JIRA startup state.
     *
     * @param event a PluginFrameworkWarmRestartedEvent
     */
    @EventListener
    public void onPluginSystemRestarted(PluginFrameworkWarmRestartedEvent event) {
        JiraStartupChecklist.getInstance().startupState().onPluginSystemRestarted();
    }

    @EventListener
    public void onPluginSystemDelayed(PluginFrameworkDelayedEvent event) {
        // Moving into late startup phase; reset the progress tracker for the late startup range
        JiraStartupChecklist.getInstance().startupState().onPluginSystemDelayed();
        TRACKER.set(new StartupTracker(EARLY_STARTUP_FACTOR, 1.0 - EARLY_STARTUP_FACTOR));
    }

    @EventListener
    public void onPluginEnabling(PluginEnablingEvent event) {
        TRACKER.get().accept(WEIGHT_FOR_ENABLING);
    }

    @EventListener
    public void onPluginContainerRefreshed(PluginContainerRefreshedEvent event) {
        TRACKER.get().accept(WEIGHT_FOR_REFRESHED);
    }

    @EventListener
    public void onPluginEnabled(PluginEnabledEvent event) {
        TRACKER.get().accept(WEIGHT_FOR_ENABLED);
    }

    public static void addPlugins(int expectedCount) {
        TRACKER.get().expect(expectedCount * TOTAL_WEIGHT);
    }

    public static int getProgress(final int budget) {
        return TRACKER.get().getProgress(budget);
    }


    interface Tracker {
        default void accept(int weight) {
        }

        default void expect(int weight) {
        }

        default int getProgress(int budget) {
            return budget;
        }
    }

    /**
     * Tracker that always reports completion.
     */
    static class DoneTracker implements Tracker {
    }

    static class StartupTracker implements Tracker {
        private final AtomicInteger expected = new AtomicInteger();
        private final AtomicInteger accepted = new AtomicInteger();
        private final long startedAt;
        private final double offset;
        private final double scale;

        StartupTracker(double offset, double scale) {
            this.startedAt = now();
            this.offset = offset;
            this.scale = scale;
        }

        @Override
        public void accept(int weight) {
            accepted.addAndGet(weight);
        }

        @Override
        public void expect(int count) {
            expected.addAndGet(count);
        }

        @Override
        public int getProgress(int budget) {
            // If we have no budget for the calculation, then this is pointless...
            if (budget <= 0) {
                return 0;
            }

            final int accepted = this.accepted.get();
            final int expected = this.expected.get();
            final double ratio = (expected > 0) ? fudge(ratio(accepted, expected)) : 0.0;
            return budget(budget, scale(ratio));
        }

        private double fudge(final double progress) {
            final int seconds = (int) TimeUnit.NANOSECONDS.toSeconds(now() - startedAt);
            final double fudge = ratio(seconds, timeout()) * (1.0 - progress) * FUDGE_FACTOR;
            return normalized(progress + fudge);
        }

        private double scale(final double progress) {
            return normalized(offset + scale * progress);
        }

        @VisibleForTesting
        protected long now() {
            return System.nanoTime();
        }

        @VisibleForTesting
        protected int timeout() {
            return PLUGIN_ENABLING_WAIT_PERIOD;
        }
    }

    static int budget(int budget, double ratio) {
        final int progress = (int) (budget * ratio);
        return max(0, min(progress, budget));
    }

    static double ratio(int portion, int total) {
        return (total == 0) ? 0.0 : normalized((double) portion / total);
    }

    static double normalized(double ratio) {
        return max(0.0, min(ratio, 1.0));
    }
}
