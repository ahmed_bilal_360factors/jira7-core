package com.atlassian.jira.upgrade;

import com.atlassian.jira.config.properties.PropertiesManager;
import com.atlassian.upgrade.core.LegacyUpgradeTaskBuildNumberStorage;
import com.opensymphony.module.propertyset.PropertySet;

import javax.annotation.Nonnull;
import java.util.Optional;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * This is the same basic structure as {@link com.atlassian.sal.jira.pluginsettings.JiraPluginSettings} and was copied
 * in the case that JiraPluginSettings was ever re-written we want this to work in a legacy way.
 */
public class LegacyUpgradeTaskBuildNumberStorageImpl  extends LegacyUpgradeTaskBuildNumberStorage {
    private final PropertiesManager propertiesManager;

    public LegacyUpgradeTaskBuildNumberStorageImpl(final PropertiesManager propertiesManager) {
        this.propertiesManager = propertiesManager;
    }

    @Nonnull
    @Override
    protected Optional<Integer> getIntegerPluginSetting(String key) {
        notNull("key", key);

        PropertySet propertySet = propertiesManager.getPropertySet();

        if (!propertySet.exists(key)) {
            return Optional.empty();
        }

        final String value;
        switch (propertySet.getType(key)) {
            case PropertySet.STRING:
                value = propertySet.getString(key);
                break;
            case PropertySet.TEXT:
                value = propertySet.getText(key);
                break;
            default:
                value = null;
                break;
        }

        return Optional.ofNullable(value).map(Integer::valueOf);
    }

    @Override
    protected void putIntegerPluginSetting(String key, int i) {
        notNull("key", key);

        final PropertySet propertySet = propertiesManager.getPropertySet();

        if (propertySet.exists(key)) {
            propertySet.remove(key);
        }

        propertySet.setString(key, String.valueOf(i));
    }
}

