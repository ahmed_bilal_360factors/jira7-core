package com.atlassian.jira.servermetrics;


/**
 * Put your activity names here to ensure that those are checked against whitelist.
 */
public enum RequestActivities {
    dbRead,
    dbConnected
}