package com.atlassian.jira.issue.statistics;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.project.Project;
import com.atlassian.query.Query;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

public class ComponentStatisticsManagerImpl implements ComponentStatisticsManager {

    private StatisticsManager statisticsManager;
    private ProjectComponentManager projectComponentManager;

    public ComponentStatisticsManagerImpl(StatisticsManager statisticsManager, ProjectComponentManager projectComponentManager) {
        this.statisticsManager = statisticsManager;
        this.projectComponentManager = projectComponentManager;
    }

    /** {@inheritDoc} */
    @Override
    public Map<Project, Map<ProjectComponent, Integer>> getProjectsWithComponentsWithIssueCount(Optional<Query> query) {

        Map<Project, Map<ProjectComponent, Integer>> result = statisticsManager.<ProjectComponent>getProjectsWithItemsWithIssueCount(
                query, component -> component.getProjectId(), FilterStatisticsValuesGenerator.COMPONENTS);

        for (Project project : result.keySet()) {
            Collection<ProjectComponent> componentsInProject = projectComponentManager.findAllForProject(project.getId());
            Map<ProjectComponent, Integer> componentToCount = result.get(project);

            for (ProjectComponent component : componentsInProject) {
                if (!componentToCount.containsKey(component)) {
                    componentToCount.put(component, 0);
                }
            }
        }

        return result;
    }
    
}
