package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import javax.annotation.Nullable;
import java.util.Map;

import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static java.lang.String.format;

/**
 * Represents all application roles for the renaissance migration (i.e. JIRA 6.x single use permission to JIRA 7.x
 * multiple applications).
 *
 * @since 7.0
 */
public final class ApplicationRoles {
    private final Map<ApplicationKey, ApplicationRole> roles;

    private ApplicationRoles(ImmutableMap<ApplicationKey, ApplicationRole> roles) {
        this.roles = roles;
    }

    public ApplicationRoles(Iterable<ApplicationRole> roles) {
        notNull("roles", roles);

        Map<ApplicationKey, ApplicationRole> roleIndex = Maps.newHashMap();
        for (ApplicationRole newRole : roles) {
            final ApplicationRole oldRole = roleIndex.put(newRole.key(), newRole);
            if (oldRole != null) {
                throw new MigrationFailedException(format("Duplicate Application Role %s.", newRole.key()));
            }
        }

        this.roles = ImmutableMap.copyOf(roleIndex);
    }

    Option<ApplicationRole> get(ApplicationKey key) {
        notNull("key", key);
        return Option.option(roles.get(key));
    }

    ApplicationRoles put(ApplicationRole role) {
        notNull("role", role);

        final Map<ApplicationKey, ApplicationRole> roleIndex = Maps.newHashMap(this.roles);
        roleIndex.put(role.key(), role);

        return new ApplicationRoles(ImmutableMap.copyOf(roleIndex));
    }

    Map<ApplicationKey, ApplicationRole> asMap() {
        return roles;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ApplicationRoles that = (ApplicationRoles) o;
        return roles.equals(that.roles);
    }

    @Override
    public int hashCode() {
        return roles.hashCode();
    }

    @Override
    public String toString() {
        return roles.toString();
    }
}
