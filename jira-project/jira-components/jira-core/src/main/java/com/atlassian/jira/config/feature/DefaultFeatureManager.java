package com.atlassian.jira.config.feature;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.cache.Supplier;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.InitializingComponent;
import com.atlassian.jira.cache.request.RequestCache;
import com.atlassian.jira.cache.request.RequestCacheFactory;
import com.atlassian.jira.component.ComponentReference;
import com.atlassian.jira.config.CoreFeatures;
import com.atlassian.jira.config.Feature;
import com.atlassian.jira.config.FeatureDisabledEvent;
import com.atlassian.jira.config.FeatureEnabledEvent;
import com.atlassian.jira.config.FeatureFlag;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.FeatureStore;
import com.atlassian.jira.config.InstanceFeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.plugin.profile.DarkFeatures;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.tenancy.api.TenantAccessor;
import com.atlassian.tenancy.api.event.TenantArrivedEvent;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Set;

import static com.atlassian.jira.component.ComponentAccessor.getComponentReference;

/**
 * Default implementation of {@link com.atlassian.jira.config.FeatureManager}.
 *
 * @since v4.4
 */
public class DefaultFeatureManager implements FeatureManager, InitializingComponent {
    private final FeatureFlagProviderAccessor featureFlagProviderAccessor;
    private final JiraAuthenticationContext authenticationContext;
    private final FeatureStore featureStore;
    private final JiraProperties jiraSystemProperties;
    private final TenantAccessor tenantAccessor;

    private final CachedReference<Set<String>> siteFeatures;
    private final Cache<String, Set<String>> userFeaturesCache;
    private final RequestCache<Boolean, DarkFeatures> darkFeatures;

    private final ComponentReference<EventPublisher> eventPublisherRef = getComponentReference(EventPublisher.class);
    private final ComponentReference<GlobalPermissionManager> permissionManagerRef = getComponentReference(GlobalPermissionManager.class);
    private final InstanceFeatureManager instanceFeatureManager;

    public DefaultFeatureManager(final JiraAuthenticationContext authenticationContext,
                                 final FeatureStore featureStore,
                                 final JiraProperties jiraSystemProperties,
                                 final TenantAccessor tenantAccessor,
                                 final FeatureFlagProviderAccessor featureFlagProviderAccessor,
                                 final CacheManager cacheManager,
                                 final RequestCacheFactory cacheFactory,
                                 final InstanceFeatureManager instanceFeatureManager) {
        this.jiraSystemProperties = jiraSystemProperties;
        this.authenticationContext = authenticationContext;
        this.featureStore = featureStore;
        this.featureFlagProviderAccessor = featureFlagProviderAccessor;
        this.instanceFeatureManager = instanceFeatureManager;
        this.userFeaturesCache = initUserFeatureCache(cacheManager);
        this.siteFeatures = initSiteFeatureCache(cacheManager);
        this.tenantAccessor = tenantAccessor;
        this.darkFeatures = cacheFactory.createRequestCache(DefaultFeatureManager.class.getName() + ".darkFeatures",  this::getDarkFeatures);
    }

    @Override
    public void afterInstantiation() throws Exception {
        final EventPublisher eventPublisher = eventPublisherRef.get();
        eventPublisher.register(this);
    }

    private Cache<String, Set<String>> initUserFeatureCache(final CacheManager cacheManager) {
        return cacheManager.getCache(DefaultFeatureManager.class.getName() + ".userFeaturesCache",
                new UserFeatureCacheLoader());
    }

    private CachedReference<Set<String>> initSiteFeatureCache(final CacheManager cacheManager) {
        return cacheManager.getCachedReference(DefaultFeatureManager.class.getName() + ".siteFeatures",
                new SiteFeaturesSupplier());

    }

    private boolean isEnabled(@Nonnull final String featureKey, final boolean isUserSettable) {
        return instanceFeatureManager.isInstanceFeatureEnabled(featureKey) || getDarkFeatures(isUserSettable).isFeatureEnabled(featureKey);
    }

    @Override
    public boolean isEnabled(FeatureFlag featureFlag) {
        if (featureFlag.isOnByDefault()) {
            //Flag is set to "on", check for presence of (featureKey + ".disabled") to tell us otherwise
            return !isEnabled(featureFlag.disabledFeatureKey(), true);
        } else {
            //Flag is set to "off", remain disabled unless explicitly enabled by (featureKey + ".enabled")
            return isEnabled(featureFlag.enabledFeatureKey(), true);
        }
    }

    @Override
    public boolean isEnabled(final String featureKey) {
        Option<FeatureFlag> featureFlagOption = featureFlagProviderAccessor.findFeatureFlag(featureKey);
        if (featureFlagOption.isDefined()) {
            return isEnabled(featureFlagOption.get());
        } else {
            // assume the feature is user-settable, since we don't know
            return isEnabled(featureKey, true);
        }
    }

    @Override
    public Option<FeatureFlag> getFeatureFlag(String featureKey) {
        return featureFlagProviderAccessor.findFeatureFlag(featureKey);
    }

    @Override
    public boolean isEnabled(final Feature feature) {
        if (feature instanceof CoreFeatures) {
            return isEnabled((CoreFeatures) feature);
        }

        // non-core features are always implicitly user-settable
        return isEnabled(feature.featureKey(), true);
    }

    @Override
    public boolean isEnabledForUser(@Nullable final ApplicationUser user, final String featureKey) {
        return getDarkFeaturesForUser(user).isFeatureEnabled(featureKey);
    }

    @Override
    public boolean isEnabled(final CoreFeatures feature) {
        return isEnabled(feature.featureKey(), feature.isDevFeature());
    }

    @Override
    public Set<FeatureFlag> getRegisteredFlags() {
        return Sets.newHashSet(featureFlagProviderAccessor.getFeatureFlags().values());
    }

    @Override
    public Set<String> getEnabledFeatureKeys() {
        return instanceFeatureManager.getEnabledFeatureKeys();
    }

    @Override
    public DarkFeatures getDarkFeatures() {
        return getDarkFeatures(true);
    }

    private DarkFeatures getDarkFeatures(final boolean includeUserSettable) {
        final boolean cacheKey = includeUserSettable && authenticationContext.isLoggedInUser();
        return darkFeatures.get(cacheKey, () -> fetchDarkFeatures(cacheKey));
    }

    private DarkFeatures fetchDarkFeatures(final boolean includeUserSettable) {
        if (jiraSystemProperties.isDarkFeaturesDisabled()) {
            return new DarkFeatures(Collections.emptySet(), Collections.emptySet(), Collections.emptySet());
        }
        if (includeUserSettable) {
            final ApplicationUser user = authenticationContext.getLoggedInUser();
            return new DarkFeatures(getEnabledFeatureKeys(), getSiteEnabledFeatures(), getUserEnabledFeatures(user));
        } else {
            return new DarkFeatures(getEnabledFeatureKeys(), getSiteEnabledFeatures(), Collections.emptySet());
        }
    }

    @Override
    public DarkFeatures getDarkFeaturesForUser(@Nullable final ApplicationUser user) {
        if (jiraSystemProperties.isDarkFeaturesDisabled()) {
            return new DarkFeatures(Collections.emptySet(), Collections.emptySet(), Collections.emptySet());
        }

        return new DarkFeatures(getEnabledFeatureKeys(), getSiteEnabledFeatures(), getUserEnabledFeatures(user));
    }

    @Override
    public void enableUserDarkFeature(final ApplicationUser user, final String feature) {
        changeUserDarkFeature(user, feature, true);
    }

    @Override
    public void disableUserDarkFeature(final ApplicationUser user, final String feature) {
        changeUserDarkFeature(user, feature, false);
    }

    @Override
    public void enableSiteDarkFeature(final String feature) {
        changeSiteDarkFeature(feature, true);
    }

    @Override
    public void disableSiteDarkFeature(final String feature) {
        changeSiteDarkFeature(feature, false);
    }

    @Override
    public boolean hasSiteEditPermission() {
        final ApplicationUser loggedInUser = authenticationContext.getLoggedInUser();
        return permissionManagerRef.get().hasPermission(GlobalPermissionKey.ADMINISTER, loggedInUser);
    }

    private void changeUserDarkFeature(final ApplicationUser user, final String feature, final boolean enable) {
        // Check 'dark feature' key against CoreFeatures - users should not attempt to
        // enable or disable them.
        final CoreFeatures coreFeature = CoreFeatures.forFeatureKey(feature);
        if (coreFeature != null && !coreFeature.isDevFeature()) {
            throw new IllegalStateException("User cannot set feature '" + feature
                    + "' at runtime. It must be set by an admin via properties.");
        }

        final Set<String> enabledFeatures = getUserEnabledFeatures(user);
        if (enable == enabledFeatures.contains(feature)) {
            // No change to make - feature is already enabled or disabled.
            return;
        }

        if (enable) {
            featureStore.create(feature, user.getKey());
        } else {
            featureStore.delete(feature, user.getKey());
        }
        darkFeatures.removeAll();
        userFeaturesCache.remove(user.getKey());
        eventPublisherRef.get().publish(enable ? new FeatureEnabledEvent(feature, user)
                : new FeatureDisabledEvent(feature, user));
    }

    private void changeSiteDarkFeature(final String feature, final boolean enable) {
        if (!hasSiteEditPermission()) {
            throw new IllegalStateException("User " + authenticationContext.getLoggedInUser()
                    + " does not have permission to change site dark features");
        }

        final Set<String> enabledFeatures = getSiteEnabledFeatures();
        if (enable == enabledFeatures.contains(feature)) {
            // No change to make - feature is already enabled or disabled.
            return;
        }

        if (enable) {
            featureStore.create(feature, null);
            enabledFeatures.add(feature);
        } else {
            featureStore.delete(feature, null);
            enabledFeatures.remove(feature);
        }
        siteFeatures.reset();
        darkFeatures.removeAll();
        eventPublisherRef.get().publish(enable ? new FeatureEnabledEvent(feature) : new FeatureDisabledEvent(feature));
    }

    private Set<String> getUserEnabledFeatures(final ApplicationUser user) {
        if (user != null) {
            return userFeaturesCache.get(user.getKey());
        } else {
            return ImmutableSet.of();
        }
    }

    private Set<String> getUserEnabledFeaturesFromStore(final String userKey) {
        return featureStore.getUserFeatures(userKey);
    }

    private Set<String> getSiteEnabledFeatures() {
        return siteFeatures.get();
    }

    private Set<String> getSiteEnabledFeaturesFromStore() {
        return featureStore.getSiteFeatures();
    }

    @EventListener
    @SuppressWarnings({"UnusedDeclaration"})
    public void onClearCache(final ClearCacheEvent event) {
        invalidateCaches();
    }

    @EventListener
    @SuppressWarnings({"UnusedDeclaration"})
    public void onTenantArrived(final TenantArrivedEvent event) {
        invalidateCaches();
    }

    private void invalidateCaches() {
        userFeaturesCache.removeAll();
        darkFeatures.removeAll();
        siteFeatures.reset();
    }

    private boolean isTenanted() {
        return !Iterables.isEmpty(tenantAccessor.getAvailableTenants());
    }

    private class UserFeatureCacheLoader implements CacheLoader<String, Set<String>> {
        @Override
        @Nonnull
        public Set<String> load(final @Nonnull String key) {
            if (isTenanted()) {
                return getUserEnabledFeaturesFromStore(key);
            }
            // If the instance is not tenanted yet, do not access the DB to load the cache.
            return ImmutableSet.of();
        }
    }

    private class SiteFeaturesSupplier implements Supplier<Set<String>> {
        @Override
        public Set<String> get() {
            if (isTenanted()) {
                return getSiteEnabledFeaturesFromStore();
            }
            // If the instance is not tenanted yet, do not access the DB to load the cache.
            return ImmutableSet.of();
        }
    }
}
