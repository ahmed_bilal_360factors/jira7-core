package com.atlassian.jira.security.login;

import com.atlassian.core.util.Clock;
import com.atlassian.jira.bc.security.login.LoginInfo;
import com.atlassian.jira.bc.security.login.LoginInfoImpl;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.RecoveryMode;
import com.google.common.base.Function;

import java.util.concurrent.atomic.AtomicReference;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * A login {@link com.atlassian.jira.security.login.LoginStore} for the 'recovery admin'. The login information
 * for other users is passed to a delegate to handle.
 *
 * @see com.atlassian.jira.user.util.RecoveryMode
 * @since 7.0
 */
public class RecoveryLoginStore implements LoginStore {
    private final RecoveryMode mode;
    private final LoginStore delegate;
    private final Clock clock;
    private final AtomicReference<LoginInfo> recoveryInfo = new AtomicReference<>();

    public RecoveryLoginStore(final RecoveryMode mode, final LoginStore delegate, final Clock clock) {
        this.mode = notNull("mode", mode);
        this.delegate = notNull("delegate", delegate);
        this.clock = notNull("clock", clock);
    }

    @Override
    public LoginInfo getLoginInfo(final ApplicationUser user) {
        if (mode.isRecoveryUser(user)) {
            LoginInfo info = recoveryInfo.get();
            while (info == null) {
                recoveryInfo.compareAndSet(null, LoginInfoImpl.builder().build());
                info = recoveryInfo.get();
            }
            return info;
        } else {
            return delegate.getLoginInfo(user);
        }
    }

    @Override
    public LoginInfo recordLoginAttempt(final ApplicationUser user, final boolean authenticated) {
        if (mode.isRecoveryUser(user)) {
            return setRecoveryInfo(new Function<LoginInfo, LoginInfo>() {
                @Override
                public LoginInfo apply(final LoginInfo loginInfo) {
                    final LoginInfoImpl.Builder builder = LoginInfoImpl.builder(loginInfo);
                    if (authenticated) {
                        builder.succeededAt(now());
                    } else {
                        builder.failedAt(now());
                    }
                    return builder.build();
                }
            });
        } else {
            return delegate.recordLoginAttempt(user, authenticated);
        }
    }

    @Override
    public long getMaxAuthenticationAttemptsAllowed() {
        return delegate.getMaxAuthenticationAttemptsAllowed();
    }

    @Override
    public void resetFailedLoginCount(final ApplicationUser user) {
        if (mode.isRecoveryUser(user)) {
            setRecoveryInfo(new Function<LoginInfo, LoginInfo>() {
                @Override
                public LoginInfo apply(final LoginInfo loginInfo) {
                    return LoginInfoImpl.builder(loginInfo).setLoginCount(0L).build();
                }
            });
        } else {
            delegate.resetFailedLoginCount(user);
        }
    }

    private LoginInfo setRecoveryInfo(Function<LoginInfo, LoginInfo> transform) {
        LoginInfo oldInfo, newInfo;
        do {
            oldInfo = recoveryInfo.get();
            newInfo = transform.apply(oldInfo);
        }
        while (!recoveryInfo.compareAndSet(oldInfo, newInfo));
        return newInfo;
    }

    private long now() {
        return clock.getCurrentDate().getTime();
    }
}
