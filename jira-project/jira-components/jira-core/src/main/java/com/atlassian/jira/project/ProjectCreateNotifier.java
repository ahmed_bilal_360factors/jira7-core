package com.atlassian.jira.project;

/**
 * Class in charge to send notifications to all {@link ProjectCreateHandler}.
 */
public interface ProjectCreateNotifier {
    /**
     * Notifies all {@link ProjectCreateHandler} objects that a project as been created.
     *
     * @param projectCreatedData Object encapsulating the information about the recently created project.
     * @return False if any of the handlers failed while handling the notification. True if everything went ok.
     */
    boolean notifyAllHandlers(ProjectCreatedData projectCreatedData);
}
