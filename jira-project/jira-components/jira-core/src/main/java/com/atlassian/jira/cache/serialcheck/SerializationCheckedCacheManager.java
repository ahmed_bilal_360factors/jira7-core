package com.atlassian.jira.cache.serialcheck;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettings;
import com.atlassian.cache.CachedReference;
import com.atlassian.cache.ManagedCache;
import com.atlassian.cache.Supplier;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;

/**
 * Every non-local cache's value is checked for ability to be serialized.
 *
 * @since 7.2.0
 */
@ParametersAreNonnullByDefault
public class SerializationCheckedCacheManager implements CacheManager {

    private final CacheManager delegate;
    private final SerializationChecker checker;

    public SerializationCheckedCacheManager(final CacheManager delegate, final SerializationChecker checker) {
        this.delegate = delegate;
        this.checker = checker;
    }

    @Override
    @Deprecated
    @Nonnull
    public Collection<Cache<?, ?>> getCaches() {
        return delegate.getCaches();
    }

    @Override
    @Nonnull
    public Collection<ManagedCache> getManagedCaches() {
        return delegate.getManagedCaches();
    }

    @Override
    public void flushCaches() {
        delegate.flushCaches();
    }

    @Override
    @Nullable
    public ManagedCache getManagedCache(@Nonnull final String name) {
        return delegate.getManagedCache(name);
    }

    @Override
    public void shutdown() {
        delegate.shutdown();
    }

    @Override
    @Nonnull
    public <V> CachedReference<V> getCachedReference(@Nonnull final String name, @Nonnull final Supplier<V> supplier) {
        return delegate.getCachedReference(name, supplier); //Non-local but replicate by invalidate by default
    }

    @Override
    @Nonnull
    public <V> CachedReference<V> getCachedReference(final String name, final Supplier<V> supplier,
                                                     final CacheSettings cacheSettings) {
        final CachedReference<V> ref = delegate.getCachedReference(name, supplier, cacheSettings);
        return isCheckRequired(cacheSettings) ? wrap(name, ref) : ref;
    }

    @Override
    @Nonnull
    public <V> CachedReference<V> getCachedReference(final Class<?> owningClass, final String name,
                                                     final Supplier<V> supplier) {
        return delegate.getCachedReference(owningClass, name, supplier); //Non-local but replicate by invalidate by default
    }

    @Override
    @Nonnull
    public <V> CachedReference<V> getCachedReference(final Class<?> owningClass, final String name,
                                                     final Supplier<V> supplier, final CacheSettings cacheSettings) {
        final CachedReference<V> ref = delegate.getCachedReference(owningClass, name, supplier, cacheSettings);
        return isCheckRequired(cacheSettings) ? wrap(name, ref): ref;
    }

    @Override
    @Nonnull
    public <K, V> Cache<K, V> getCache(final String name) {
        return delegate.getCache(name); //Non-local but replicate by invalidate by default
    }

    @Override
    @Nonnull
    public <K, V> Cache<K, V> getCache(final Class<?> owningClass, final String name) {
        return delegate.getCache(owningClass, name); //Non-local but replicate by invalidate by default
    }

    @Override
    @Nonnull
    public <K, V> Cache<K, V> getCache(final String name, @Nullable final CacheLoader<K, V> cacheLoader) {
        return delegate.getCache(name, cacheLoader); //Non-local but replicate by invalidate by default
    }

    @Override
    @Nonnull
    public <K, V> Cache<K, V> getCache(final String name, @Nullable final CacheLoader<K, V> cacheLoader,
                                       final CacheSettings cacheSettings) {
        final Cache<K, V> cache = delegate.getCache(name, cacheLoader, cacheSettings);
        return isCheckRequired(cacheSettings) ? wrap(cache) : cache;
    }

    @Override
    @Deprecated
    @Nonnull
    public <K, V> Cache<K, V> getCache(final String name, final Class<K> keyClass, final Class<V> valueClass) {
        return delegate.getCache(name, keyClass, valueClass); //Non-local by default
    }

    private <T> CachedReference<T> wrap(String name, CachedReference<T> delegate) {
        return new SerializationCheckedCachedReference<>(checker, name, delegate);
    }

    private <K, V> Cache<K, V> wrap(Cache<K, V> delegate) {
        return new SerializationCheckedCache<>(checker, delegate);
    }

    private static boolean isCheckRequired(CacheSettings settings) {
        //Not local (remote) by default
        //Only check remote caches
        if (Boolean.TRUE.equals(settings.getLocal())) {
            return false;
        }
        //Not replicate by copy (replicate by invalidate) by default
        //Only check remote caches that replicate by copy
        if (!Boolean.TRUE.equals(settings.getReplicateViaCopy())) {
            return false;
        }
        return true;
    }
}
