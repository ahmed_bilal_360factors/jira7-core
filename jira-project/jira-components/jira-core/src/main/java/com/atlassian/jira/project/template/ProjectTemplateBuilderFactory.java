package com.atlassian.jira.project.template;

import com.atlassian.plugin.webresource.WebResourceUrlProvider;

public class ProjectTemplateBuilderFactory {
    private final WebResourceUrlProvider webResourceUrlProvider;

    public ProjectTemplateBuilderFactory(WebResourceUrlProvider webResourceUrlProvider) {
        this.webResourceUrlProvider = webResourceUrlProvider;
    }

    public ProjectTemplateBuilder newBuilder() {
        return new ProjectTemplateBuilder(webResourceUrlProvider);
    }
}
