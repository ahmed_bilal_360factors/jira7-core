package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettings;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.util.Functions;
import com.atlassian.jira.util.Visitor;
import com.google.common.annotations.VisibleForTesting;
import org.ofbiz.core.entity.GenericValue;

import java.util.concurrent.locks.Lock;

/**
 * A Group cache that eagerly loads a full set of all groups.
 *
 * @since v7.0
 */
class EagerOfBizGroupCache extends UserOrGroupCache<OfBizGroup> implements OfBizGroupCache {
    /**
     * The EHCache settings are really coming from ehcache.xml, but we need to set unflushable() here .
     */
    @VisibleForTesting
    static final CacheSettings GROUP_CACHE_SETTINGS = new CacheSettingsBuilder().unflushable().replicateViaCopy().build();

    private static final String LOAD_GROUP_CACHE_LOCK = EagerOfBizGroupCache.class.getName() + ".loadGroupCacheLock";

    private final ClusterLockService clusterLockService;
    private final CacheManager cacheManager;
    private final DirectoryDao directoryDao;
    private final OfBizDelegator ofBizDelegator;

    EagerOfBizGroupCache(final ClusterLockService clusterLockService, final CacheManager cacheManager,
                         final DirectoryDao directoryDao, final OfBizDelegator ofBizDelegator) {
        super(GroupEntity.ENTITY);
        this.clusterLockService = clusterLockService;
        this.cacheManager = cacheManager;
        this.directoryDao = directoryDao;
        this.ofBizDelegator = ofBizDelegator;
    }

    @Override
    public Lock getLock() {
        return clusterLockService.getLockForName(LOAD_GROUP_CACHE_LOCK);
    }

    @Override
    public Cache<DirectoryEntityKey, OfBizGroup> createCache() {
        return cacheManager.getCache(EagerOfBizGroupCache.class.getName() + ".groupCache", null, GROUP_CACHE_SETTINGS);
    }

    @Override
    public long countAllUsingDatabase() {
        // Count by directory to make sure that what we count matches what we would actually visit.
        // We would only get a discrepancy if there are garbage groups with an invalid directory ID, but
        // better safe than sorry...
        long count = 0L;
        for (Directory directory : directoryDao.findAll()) {
            count += Select.id()
                    .from(GroupEntity.ENTITY)
                    .whereEqual(GroupEntity.DIRECTORY_ID, directory.getId())
                    .runWith(ofBizDelegator)
                    .count();
        }
        return count;
    }

    @Override
    public void visitAllUsingDatabase(final Visitor<OfBizGroup> visitor) {
        final Visitor<GenericValue> gvVisitor = Functions.mappedVisitor(OfBizGroupDao.TO_GROUP_FUNCTION, visitor);
        for (Directory directory : directoryDao.findAll()) {
            Select.columns(OfBizGroup.SUPPORTED_FIELDS)
                    .from(GroupEntity.ENTITY)
                    .whereEqual(GroupEntity.DIRECTORY_ID, directory.getId())
                    .runWith(ofBizDelegator)
                    .visitWith(gvVisitor);
        }
    }

}
