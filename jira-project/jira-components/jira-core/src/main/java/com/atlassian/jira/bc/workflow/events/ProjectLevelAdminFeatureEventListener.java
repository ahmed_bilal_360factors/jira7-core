package com.atlassian.jira.bc.workflow.events;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.bc.workflow.DefaultWorkflowService;
import com.atlassian.jira.config.FeatureDisabledEvent;
import com.atlassian.jira.config.FeatureEnabledEvent;

@EventComponent
public class ProjectLevelAdminFeatureEventListener {

    private final EventPublisher eventPublisher;

    public ProjectLevelAdminFeatureEventListener(EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @EventListener
    public void onFeatureEnabled(FeatureEnabledEvent event) {
        if (DefaultWorkflowService.PROJECT_LEVEL_ADMIN_EDIT_WORKFLOW_FEATURE.disabledFeatureKey().equals(event.feature())) {
            eventPublisher.publish(new ProjectLevelAdminEditWorkflowFeatureOff());
        }
    }

    @EventListener
    public void onFeatureEnabled(FeatureDisabledEvent event) {
        if (DefaultWorkflowService.PROJECT_LEVEL_ADMIN_EDIT_WORKFLOW_FEATURE.disabledFeatureKey().equals(event.feature())) {
            eventPublisher.publish(new ProjectLevelAdminEditWorkflowFeatureOn());
        }
    }
}
