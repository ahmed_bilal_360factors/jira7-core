package com.atlassian.jira.issue.fields.config.manager;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.component.ComponentReference;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.issue.field.config.manager.IssueTypeSchemeCreatedEvent;
import com.atlassian.jira.event.issue.field.config.manager.IssueTypeSchemeDeletedEvent;
import com.atlassian.jira.event.issue.field.config.manager.IssueTypeSchemeUpdatedEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.context.IssueContextImpl;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.fields.ConfigurableField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.IssueTypeField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.option.IssueConstantOption;
import com.atlassian.jira.issue.fields.option.Option;
import com.atlassian.jira.issue.fields.option.OptionSetManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.SchemeComparator;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static java.util.Comparator.comparing;
import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.nullsFirst;

/**
 * A manager to manage {@link IssueType}'s unique set of circumstances.
 * That is, it circumvents the scheme system by collapsing the scheme and config.
 */
public class IssueTypeSchemeManagerImpl implements IssueTypeSchemeManager {
    private final FieldConfigSchemeManager configSchemeManager;
    private final OptionSetManager optionSetManager;
    private final GenericConfigManager genericConfigManager;
    private final ConstantsManager constantsManager;

    private final ComponentReference<FieldManager> fieldManagerRef;
    private final ComponentReference<ApplicationProperties> applicationPropertiesRef;

    private EventPublisher eventPublisher;

    public IssueTypeSchemeManagerImpl(FieldConfigSchemeManager configSchemeManager, OptionSetManager optionSetManager,
                                      GenericConfigManager genericConfigManager, ConstantsManager constantsManager,
                                      EventPublisher eventPublisher) {
        this.configSchemeManager = configSchemeManager;
        this.optionSetManager = optionSetManager;
        this.genericConfigManager = genericConfigManager;
        this.constantsManager = constantsManager;
        this.eventPublisher = eventPublisher;

        // breaking circular dependencies
        this.fieldManagerRef = ComponentAccessor.getComponentReference(FieldManager.class);
        this.applicationPropertiesRef = ComponentAccessor.getComponentReference(ApplicationProperties.class);
    }

    public FieldConfigScheme create(String schemeName, String schemeDescription, List<String> optionIds) {
        FieldConfigScheme configScheme = new FieldConfigScheme.Builder()
                .setName(schemeName)
                .setDescription(schemeDescription)
                .toFieldConfigScheme();
        configScheme = configSchemeManager.createFieldConfigScheme(configScheme, null,
                FieldConfigSchemeManager.ALL_ISSUE_TYPES, getIssueTypeField());
        FieldConfig config = configScheme.getOneAndOnlyConfig();
        optionSetManager.createOptionSet(config, optionIds);

        getFieldManager().refresh();
        eventPublisher.publish(new IssueTypeSchemeCreatedEvent(configScheme));

        return configScheme;
    }

    public FieldConfigScheme update(FieldConfigScheme configScheme, Collection<String> optionIds) {
        configScheme = configSchemeManager.updateFieldConfigScheme(configScheme);

        FieldConfig config = configScheme.getOneAndOnlyConfig();
        optionSetManager.updateOptionSet(config, optionIds);

        triggerIssueTypeSchemeUpdated(configScheme);

        return configScheme;
    }

    public FieldConfigScheme getDefaultIssueTypeScheme() {
        final long schemeId = getDefaultIssueTypeSchemeId();
        return configSchemeManager.getFieldConfigScheme(schemeId);
    }

    public boolean isDefaultIssueTypeScheme(FieldConfigScheme configScheme) {
        final Long id = configScheme.getId();
        return id != null && id == getDefaultIssueTypeSchemeId();
    }

    private ConfigurableField<?> getIssueTypeField() {
        return getFieldManager().getConfigurableField(IssueFieldConstants.ISSUE_TYPE);
    }

    private long getDefaultIssueTypeSchemeId() {
        final String s = applicationPropertiesRef.get().getString(APKeys.DEFAULT_ISSUE_TYPE_SCHEME);
        return Long.parseLong(s);
    }

    public void addOptionToDefault(String id) {
        FieldConfigScheme defaultIssueTypeScheme = getDefaultIssueTypeScheme();
        optionSetManager.addOptionToOptionSet(defaultIssueTypeScheme.getOneAndOnlyConfig(), id);
        triggerIssueTypeSchemeUpdated(defaultIssueTypeScheme);
    }

    public Collection<FieldConfigScheme> getAllRelatedSchemes(final String optionId) {
        List<FieldConfigScheme> configs = configSchemeManager.getConfigSchemesForField(getIssueTypeField());
        return Lists.newArrayList(Iterables.filter(configs, new Predicate<FieldConfigScheme>() {
            @Override
            public boolean apply(@Nullable FieldConfigScheme configScheme) {
                Collection<String> optionIds = optionSetManager.getOptionsForConfig(configScheme.getOneAndOnlyConfig()).getOptionIds();
                return optionIds.contains(optionId);
            }
        }));
    }

    public void removeOptionFromAllSchemes(String optionId) {
        Collection<FieldConfigScheme> relatedSchemes = getAllRelatedSchemes(optionId);
        for (FieldConfigScheme configScheme : relatedSchemes) {
            optionSetManager.removeOptionFromOptionSet(configScheme.getOneAndOnlyConfig(), optionId);
            triggerIssueTypeSchemeUpdated(configScheme);
        }
    }

    private void triggerIssueTypeSchemeUpdated(FieldConfigScheme configScheme) {
        getFieldManager().refresh();
        eventPublisher.publish(new IssueTypeSchemeUpdatedEvent(configScheme));
    }

    public void deleteScheme(FieldConfigScheme configScheme) {
        // Note: the following calls are now encapsulated by the FieldConfigSchemeManager#removeFieldConfigScheme() method
        // as that method has greater scope in the application.
        //   * option sets are now deleted when FieldConfigPersister#remove() is called
        //   * instead of calling FieldConfigManager#removeFieldConfig(), we use FieldConfigManager#removeConfigsForConfigScheme()
        // optionSetManager.removeOptionSet(configScheme.getOneAndOnlyConfig());
        // configManager.removeFieldConfig(configScheme.getOneAndOnlyConfig());

        configSchemeManager.removeFieldConfigScheme(configScheme.getId());
        getFieldManager().refresh();
        eventPublisher.publish(new IssueTypeSchemeDeletedEvent(configScheme));
    }

    /**
     * Retrieves all schemes and sorts them.
     * <p>
     * Schemes with the default scheme ID are first
     * Schemes with null names are second
     * the rest are sorted on name
     *
     * @return all schemes sorted on name with default first
     */
    public List<FieldConfigScheme> getAllSchemes() {
        List<FieldConfigScheme> schemes = new ArrayList<>(configSchemeManager.getConfigSchemesForField(getFieldManager().getIssueTypeField()));
        Collections.sort(schemes, schemeComparator());
        return schemes;
    }

    /**
     * Returns the FieldManager to use.
     * We cannot inject this because we would get a cyclic dependency.
     *
     * @return the FieldManager to use.
     */
    private FieldManager getFieldManager() {
        return fieldManagerRef.get();
    }

    public IssueType getDefaultValue(Issue issue) {
        FieldConfig config = configSchemeManager.getRelevantConfig(issue, getIssueTypeField());
        return getDefaultValue(config);
    }

    public IssueType getDefaultValue(FieldConfig config) {
        if (config != null) {
            String issueTypeId = (String) genericConfigManager.retrieve(CustomFieldType.DEFAULT_VALUE_TYPE, config.getId().toString());
            return constantsManager.getIssueType(issueTypeId);
        } else {
            return null;
        }
    }

    @Override
    public IssueType getDefaultValue(GenericValue project) {
        if (project != null) {
            IssueTypeField issueTypeField = getFieldManager().getIssueTypeField();
            FieldConfig relevantConfig = issueTypeField.getRelevantConfig(new IssueContextImpl(project.getLong("id"), null));
            return getDefaultValue(relevantConfig);
        } else {
            return null;
        }
    }

    @Override
    public IssueType getDefaultIssueType(Project project) {
        if (project != null) {
            IssueTypeField issueTypeField = getFieldManager().getIssueTypeField();
            FieldConfig relevantConfig = issueTypeField.getRelevantConfig(new IssueContextImpl(project.getId(), null));
            return getDefaultValue(relevantConfig);
        } else {
            return null;
        }
    }

    public void setDefaultValue(FieldConfig config, String optionId) {
        genericConfigManager.update(CustomFieldType.DEFAULT_VALUE_TYPE, config.getId().toString(), optionId);
        getFieldManager().refresh();
    }

    public FieldConfigScheme getConfigScheme(GenericValue project) {
        return configSchemeManager.getRelevantConfigScheme(new IssueContextImpl(project != null ? project.getLong("id") : null, null), getFieldManager().getIssueTypeField());
    }


    public FieldConfigScheme getConfigScheme(Project project) {
        return configSchemeManager.getRelevantConfigScheme(project, getFieldManager().getIssueTypeField());
    }

    @Nonnull
    public Collection<IssueType> getIssueTypesForProject(GenericValue project) {
        FieldConfigScheme fieldConfigScheme = getConfigScheme(project);
        return getIssueTypesForConfigScheme(fieldConfigScheme, true, true);
    }

    @Nonnull
    public Collection<IssueType> getIssueTypesForProject(Project project) {
        return getIssueTypesForProject(project.getGenericValue());
    }

    @Nonnull
    public Collection<IssueType> getIssueTypesForDefaultScheme() {
        FieldConfigScheme fieldConfigScheme = getDefaultIssueTypeScheme();
        return getIssueTypesForConfigScheme(fieldConfigScheme, true, true);
    }

    @Nonnull
    public Collection<IssueType> getNonSubTaskIssueTypesForProject(Project project) {
        FieldConfigScheme fieldConfigScheme = getConfigScheme(project.getGenericValue());
        return getIssueTypesForConfigScheme(fieldConfigScheme, false, true);
    }

    @Nonnull
    public Collection<IssueType> getSubTaskIssueTypesForProject(@Nonnull Project project) {
        FieldConfigScheme fieldConfigScheme = getConfigScheme(project.getGenericValue());
        return getIssueTypesForConfigScheme(fieldConfigScheme, true, false);
    }

    @Nonnull
    private Collection<IssueType> getIssueTypesForConfigScheme(FieldConfigScheme fieldConfigScheme, boolean includeSubTasks,
                                                               boolean includeNonSubTaskIssueTypes) {
        FieldConfig config = fieldConfigScheme.getOneAndOnlyConfig();
        Collection<Option> options = optionSetManager.getOptionsForConfig(config).getOptions();
        Collection<IssueType> issueTypeObjects = new ArrayList<>();

        for (Option option : options) {
            IssueConstantOption issueTypeOption = (IssueConstantOption) option;
            final IssueType issueType = constantsManager.getIssueType(issueTypeOption.getId());
            final boolean isSubTask = issueType.isSubTask();
            if ((includeSubTasks && isSubTask) || (includeNonSubTaskIssueTypes && !isSubTask)) {
                issueTypeObjects.add(issueType);
            }
        }

        return issueTypeObjects;
    }

    /**
     * Comparator for the getAllSchemes function.
     * <ol>
     * <li>Schemes with the {@link #getDefaultIssueTypeSchemeId() default scheme ID} come first.</li>
     * <li>Schemes with null names follow that with no particular ordering among them..</li>
     * <li>All other schemes are then sorted by name, case-insensitively.</li>
     * </ol>
     */

    private Comparator<FieldConfigScheme> schemeComparator() {
        return defaultIdFirst().thenComparing(BY_NAME_NULLS_FIRST);
    }

    private Comparator<FieldConfigScheme> defaultIdFirst() {
        final Long defaultId = getDefaultIssueTypeSchemeId();
        return comparing(FieldConfigScheme::getId, (id1, id2) -> {
            if (defaultId.equals(id1)) {
                return -1;
            }
            if (defaultId.equals(id2)) {
                return 1;
            }
            return 0;
        });
    }

    private static final Comparator<FieldConfigScheme> BY_NAME_NULLS_FIRST =
            comparing(FieldConfigScheme::getName, nullsFirst(String::compareToIgnoreCase));
}
