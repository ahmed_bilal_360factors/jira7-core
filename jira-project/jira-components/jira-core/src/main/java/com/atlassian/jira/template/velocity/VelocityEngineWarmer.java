package com.atlassian.jira.template.velocity;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.warmer.JiraWarmer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A warmer used to warm up Velocity engine.
 * Creating Velocity engine is costly and we would rather warm it up after JIRA restart rather than lazily initialize it.
 *
 * @since v7.1
 */
public class VelocityEngineWarmer implements JiraWarmer {

    private static final Logger log = LoggerFactory.getLogger(VelocityEngineWarmer.class);

    private final VelocityEngineFactory velocityEngineFactory;
    private final FeatureManager featureManager;

    public VelocityEngineWarmer(final VelocityEngineFactory velocityEngineFactory, final FeatureManager featureManager) {
        this.velocityEngineFactory = velocityEngineFactory;
        this.featureManager = featureManager;
    }

    @Override
    public void run() {
        if (featureManager.isEnabled("jira.velocity.engine.warmup")) {
            log.debug("Warming up velocity.");
            velocityEngineFactory.getEngine();
            log.debug("Warming up velocity done.");
        }
    }
}
