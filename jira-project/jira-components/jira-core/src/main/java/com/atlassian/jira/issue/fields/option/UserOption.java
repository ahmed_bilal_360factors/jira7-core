package com.atlassian.jira.issue.fields.option;

import com.atlassian.jira.user.ApplicationUser;

public class UserOption extends AbstractChildOption implements Option, Comparable {
    // ------------------------------------------------------------------------------------------------------- Constants
    // ------------------------------------------------------------------------------------------------- Type Properties
    private ApplicationUser user;
    private String alternateName;

    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    public UserOption(ApplicationUser user) {
        this.user = user;
    }

    public UserOption(String alternateName) {
        this.alternateName = alternateName;
    }

    public UserOption(ApplicationUser user, String alternateName) {
        this.user = user;
        this.alternateName = alternateName;
    }

    // -------------------------------------------------------------------------------------------------- Public Methods
    public String getName() {
        return alternateName != null ? alternateName : (user != null ? user.getName() : "");
    }

    public ApplicationUser getUser() {
        return user;
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Helper Methods
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserOption)) return false;
        if (!super.equals(o)) return false;

        final UserOption userOption = (UserOption) o;

        String myName = getName();
        String otherName = userOption.getName();

        if (myName == null && otherName == null) {
            return true;
        } else if (myName == null || otherName == null) {
            return false;
        } else {
            return myName.equals(otherName);
        }
    }

    public int hashCode() {
        int result = super.hashCode();
        String name = getName();
        result = 29 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    public int compareTo(Object obj) {
        if (obj == null || !(obj instanceof UserOption))
            return 1;

        String myName = getName();
        String otherName = ((UserOption) obj).getName();

        if (myName != null) {
            if (otherName != null)
                return compareNames(myName, otherName);
            else return 1;
        } else {
            if (otherName != null)
                return -1;
            else
                return 0;
        }
    }

    private int compareNames(String name1, String name2) {
        if (name1 != null) {
            if (name2 != null)
                return name1.compareTo(name2);
            else
                return 1;
        } else {
            if (name2 != null)
                return -1;
            else
                return 0;
        }
    }
}
