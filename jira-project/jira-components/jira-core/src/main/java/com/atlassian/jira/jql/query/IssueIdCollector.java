package com.atlassian.jira.jql.query;

import com.atlassian.jira.issue.index.DocumentConstants;
import com.google.common.collect.ImmutableSortedSet;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.FieldCache;
import org.apache.lucene.search.Scorer;

import java.io.IOException;
import java.util.Set;
import java.util.HashSet;

/**
 * Collect Issue Ids for subquery searchers.
 *
 * @since v6.1
 */
public class IssueIdCollector extends Collector {
    private Set<String> issueIds;
    private String[] docIdToIssueId;

    IssueIdCollector() {
        this.issueIds = new HashSet<>();
    }

    @Override
    public void setScorer(final Scorer scorer) throws IOException {
    }

    @Override
    public void collect(final int docId) throws IOException {
        issueIds.add(docIdToIssueId[docId]);
    }

    @Override
    public void setNextReader(final IndexReader reader, final int docBase) throws IOException {
        docIdToIssueId = FieldCache.DEFAULT.getStrings(reader, DocumentConstants.ISSUE_ID);
    }

    @Override
    public boolean acceptsDocsOutOfOrder() {
        return true;
    }

    public Set<String> getIssueIds() throws IOException {
        return ImmutableSortedSet.copyOf(issueIds);
    }
}
