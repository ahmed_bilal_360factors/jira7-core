package com.atlassian.jira.web.filters.johnson;

import com.atlassian.jira.web.startup.StartupPageSupport;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.filters.AbstractJohnsonFilter;
import com.atlassian.johnson.setup.SetupConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Enhances the base {@code JohnsonFilter} with awareness of the startup page.
 *
 * @since v7.1.0
 */
public class JiraJohnsonFilter extends AbstractJohnsonFilter {
    private static final Logger LOG = LoggerFactory.getLogger(JiraJohnsonFilter.class);
    private static final String ALREADY_FILTERED = JiraJohnsonFilter.class.getName() + "_already_filtered";

    /**
     * Re-publishes {@link #getServletPath(HttpServletRequest)} with {@code public} access.
     * In its infinite wisdom, {@link AbstractJohnsonFilter} restricts this extraordinarily useful method to
     * {@code protected} scope.  This is the wrong place for that useful code to live, but the other choice
     * is to copy-and-paste the code.
     *
     * @param request the servlet request from which to extract the servlet path
     * @return the servlet path, which is the application context-scoped part of the URL.  In other words, for
     * {@code http://localhost:8090/jira/secure/foo.jsp}, the result is {@code /secure/foo.jsp}.
     */
    public static String getServletPathFromRequest(HttpServletRequest request) {
        return getServletPath(request);
    }


    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException {
        boolean intercepted = false;
        if (request.getAttribute(ALREADY_FILTERED) == null) {
            request.setAttribute(ALREADY_FILTERED, Boolean.TRUE);
            intercepted = new Interceptor((HttpServletRequest) request, (HttpServletResponse) response).intercept();
        }

        // Terminate the filter chain if we redirected the request; otherwise we need to pass it along...
        if (!intercepted) {
            filterChain.doFilter(request, response);
        }
    }

    @Override
    protected void handleError(final JohnsonEventContainer container, final HttpServletRequest request,
                               final HttpServletResponse response) throws IOException {
        redirect(request, response, "JIRA currently has application level errors.", config.getErrorPath());
    }

    @Override
    protected void handleNotSetup(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        redirect(request, response, "JIRA has not been configured, yet.", config.getSetupPath());
    }

    static void redirect(HttpServletRequest request, HttpServletResponse response, String message, String targetPath)
            throws IOException {
        final String contextPath = request.getContextPath();
        LOG.debug("{} Redirecting request from '{}' to '{}'", message, getServletPath(request), targetPath);
        response.sendRedirect(contextPath + targetPath);
    }


    /**
     * A helper that determines whether or not the current request needs to be redirected
     * and calls {@link #handleError(JohnsonEventContainer, HttpServletRequest, HttpServletResponse)},
     * {@link StartupPageSupport#redirectToStartupPage(HttpServletRequest, HttpServletResponse)}, or
     * {@link #handleNotSetup(HttpServletRequest, HttpServletResponse)} as appropriate.
     * <p>
     * The conditions are checked in that order; that is, first we look for Johnson events, then for
     * JIRA still starting up, and finally started without errors but not yet configured.
     * </p>
     * <p>
     * This is more or less the same logic as is in
     * {@link AbstractJohnsonFilter#doFilter(ServletRequest, ServletResponse, FilterChain)}, except that:
     * </p>
     * <ol>
     * <li>We check for the startup page in between checking for errors or not being set up.</li>
     * <li>We only check the Johnson whitelist if there are actually a reason to do so (slight optimization).</li>
     * </ol>
     */
    class Interceptor {
        private final HttpServletRequest request;
        private final HttpServletResponse response;
        private final String servletPath;
        private final JohnsonEventContainer container;

        private Boolean filtered;

        Interceptor(final HttpServletRequest request, final HttpServletResponse response) {
            this.request = request;
            this.response = response;
            this.servletPath = getServletPath(request);
            this.container = getContainerAndRunEventChecks(request);
        }

        /**
         * Checks for the need to intercept/redirect the current request.
         *
         * @return {@code true} if the request has been redirected (and the filter chain should be
         * terminated in response); {@code false} if no redirect was needed and the request may
         * proceed as-is.
         * @throws IOException if an I/O error occurs while attempting to send the redirect response
         *                     to the HTTP client.  In most cases, this means that there was a network error or the
         *                     client disconnected before receiving our response.
         */
        boolean intercept() throws IOException {
            if (!shouldBeJohnsonFilteredPage()) {
                LOG.debug("This page should be ignored as specified in johnson-config.xml. Current servletPath = '{}'", servletPath);
                return false;
            }
            return interceptErrors() || interceptStartup() || interceptSetup();
        }

        private boolean interceptStartup() throws IOException {
            if (StartupPageSupport.isLaunched()) {
                return false;
            }

            if (StartupPageSupport.isStartupPage(request)) {
                throw new IllegalStateException("StartupProgressFilter should have terminated the filter chain!");
            }

            StartupPageSupport.redirectToStartupPage(request, response);
            return true;
        }

        private boolean shouldBeJohnsonFilteredPage() {
            if (filtered == null) {
                filtered = !ignoreURI(servletPath);
            }
            return filtered;
        }

        private boolean interceptErrors() throws IOException {
            if (container.hasEvents() && shouldBeJohnsonFilteredPage()) {
                handleError(container, request, response);
                return true;
            }
            return false;
        }

        private boolean interceptSetup() throws IOException {
            final SetupConfig setup = config.getSetupConfig();
            if (setup.isSetup() || setup.isSetupPage(servletPath)) {
                return false;
            }

            handleNotSetup(request, response);
            return true;
        }
    }
}
