package com.atlassian.jira.license;

import com.atlassian.annotations.Internal;
import com.atlassian.extras.api.LicenseException;

import javax.annotation.Nonnull;

/**
 * Provides the ability to extract a LicenseDetails object from a license String.
 *
 * @since 7.0
 */
@Internal
public interface LicenseDetailsFactory {
    /**
     * Converts an encoded license key into a {@link com.atlassian.jira.license.LicenseDetails}. This method never
     * returns null, if the provided license key is undecodable or not for a JIRA Product a {@link
     * com.atlassian.extras.api.LicenseException} is thrown.
     *
     * @param licenseString the encoded license key.
     * @return the corresponding license details.
     * @throws LicenseException if licenseString was null or could not be decoded (see {@link #isDecodeable(String)}.
     */
    @Nonnull
    LicenseDetails getLicense(@Nonnull String licenseString) throws LicenseException;

    /**
     * Determines whether a license string can be decoded by this factory. If this returns true, then getLicense is
     * guaranteed to return a valid non-placeholder LicenseDetails object.
     *
     * @param licenseString the (usually encrypted) license string to test
     * @return true if the license string can be interpreted by this factory, false otherwise
     */
    boolean isDecodeable(@Nonnull String licenseString);
}
