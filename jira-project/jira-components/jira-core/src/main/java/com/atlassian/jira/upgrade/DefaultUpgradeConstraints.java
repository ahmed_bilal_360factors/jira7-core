package com.atlassian.jira.upgrade;

import com.atlassian.jira.util.BuildUtilsInfo;

import javax.inject.Inject;

/**
 * Allows to run every upgrade task (this needs to be fixed as it should only be allowed to run upgrade task up to
 * current build number) and returns {@link BuildUtilsInfo#getApplicationBuildNumber()} as a target database number.
 *
 * @since v7
 */
public class DefaultUpgradeConstraints implements UpgradeConstraints {
    private final BuildUtilsInfo buildUtilsInfo;


    @Inject
    public DefaultUpgradeConstraints(final BuildUtilsInfo buildUtilsInfo) {
        this.buildUtilsInfo = buildUtilsInfo;
    }

    @Override
    public int getTargetDatabaseBuildNumber() {
        return buildUtilsInfo.getApplicationBuildNumber();
    }

    @Override
    public boolean shouldRunTask(final String upgradeTaskNumber) {
        //We always run the task even if the version of the task is higher than application build number
        //this is because we have lots of test that rely on this behaviour. It is not right though and this
        //behaviour needs to be changed.
        return true;
    }
}
