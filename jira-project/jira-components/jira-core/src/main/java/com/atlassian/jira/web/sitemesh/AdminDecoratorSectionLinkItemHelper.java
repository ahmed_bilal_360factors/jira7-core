package com.atlassian.jira.web.sitemesh;

import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.SimpleLink;
import com.atlassian.jira.plugin.webfragment.model.SimpleLinkSection;
import com.atlassian.jira.user.ApplicationUser;

import java.util.Optional;

/**
 * Returns web-item that given web-section declares to use as its link.
 *
 * @since v7.0
 */
@Deprecated
public interface AdminDecoratorSectionLinkItemHelper {

    /**
     * Find the web-item associated with given web-section. This works by adding additional param to the section with
     * name 'webfragments.cloud.link.item' and value equal to location and id of the item.
     *
     * @param currentSection current section
     * @param loggedInUser   logged in user
     * @param jiraHelper     jira helper
     * @return the web-item associated with the section or empty if it's not found
     */
    Optional<SimpleLink> findSectionLink(final SimpleLinkSection currentSection, final ApplicationUser loggedInUser, final JiraHelper jiraHelper);
}
