package com.atlassian.jira.database;

import com.atlassian.jira.model.querydsl.JiraRelationalPathBase;
import com.querydsl.core.QueryFlag;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.SubQueryExpression;
import com.querydsl.sql.SQLTemplates;
import com.querydsl.sql.dml.Mapper;
import com.querydsl.sql.dml.SQLInsertClause;
import org.ofbiz.core.entity.DelegatorInterface;

import javax.annotation.Nonnull;
import java.sql.Connection;

import static java.util.Objects.requireNonNull;

/**
 * Extended SQLInsertClause supporting Generation of OfBiz sequence values.
 *
 * @since v6.4.5
 */
public class IdGeneratingSQLInsertClause extends SQLInsertClause {
    private final JiraRelationalPathBase<?> entity;
    private final DelegatorInterface delegatorInterface;

    public IdGeneratingSQLInsertClause(Connection con, final SQLTemplates dialect,
                                       final JiraRelationalPathBase<?> entity, final DelegatorInterface delegatorInterface) {
        super(con, dialect, entity);
        this.entity = entity;
        this.delegatorInterface = delegatorInterface;
    }

    /**
     * Add an automatically generated "ID" column value to the insert statement. <p>This method uses the Ofbiz sequence
     * generator to generate an Id for the relation being inserted.</p> <p>Users of this method should not manually add
     * an "ID" using another call such as {@link #set(Path, Object)} or {@link #populate(Object)}.</p>
     *
     * @return clause for building the query.
     */
    public IdGeneratingSQLInsertClause withId() {
        checkForNumericId(entity);
        final Long id = delegatorInterface.getNextSeqId(entity.getEntityName());
        super.set(entity.getNumericIdPath(), id);
        return this;
    }

    /**
     * Add an automatically generated "ID" column value to the insert statement and execute it. <p>This method uses the
     * Ofbiz sequence generator to generate an Id for the relation being inserted.</p>
     * <p>
     * <p>
     * Example Usage:
     * <pre>
     *     final Long generatedId = dbConnectionManager.executeQuery(dbConnection -> dbConnection.
     *             .insert(QProject.PROJECT)
     *             .set(QProject.PROJECT.name, "some name")
     *             .executeWithId();
     *     );
     * </pre>
     * <p>
     *
     * @return the id that was populated into the query.
     * @since 7.1
     */
    @Nonnull
    public Long executeWithId() {
        checkForNumericId(entity);
        final Long id = requireNonNull(delegatorInterface.getNextSeqId(entity.getEntityName()), "id");
        super.set(entity.getNumericIdPath(), id);
        super.execute();
        return id;
    }

    private static void checkForNumericId(final JiraRelationalPathBase<?> entity) {
        if (!entity.hasNumericId()) {
            throw new UnsupportedOperationException("Entity '" + entity.getEntityName() + "' does not support automatic ID sequence generation.");
        }
    }

    @Override
    public IdGeneratingSQLInsertClause columns(final Path<?>... columns) {
        super.columns(columns);
        return this;
    }

    @Override
    public IdGeneratingSQLInsertClause select(final SubQueryExpression<?> sq) {
        super.select(sq);
        return this;
    }

    @Override
    public <T> IdGeneratingSQLInsertClause set(final Path<T> path, final T value) {
        super.set(path, value);
        return this;
    }

    @Override
    public <T> IdGeneratingSQLInsertClause set(final Path<T> path, final Expression<? extends T> expression) {
        super.set(path, expression);
        return this;
    }

    @Override
    public <T> IdGeneratingSQLInsertClause setNull(final Path<T> path) {
        super.setNull(path);
        return this;
    }

    @Override
    public IdGeneratingSQLInsertClause values(final Object... v) {
        super.values(v);
        return this;
    }

    @Override
    public IdGeneratingSQLInsertClause populate(final Object bean) {
        super.populate(bean, OrderingMapper.DEFAULT);
        return this;
    }

    @Override
    public <T> IdGeneratingSQLInsertClause populate(final T obj, final Mapper<T> mapper) {
        super.populate(obj, mapper);
        return this;
    }

    @Override
    public IdGeneratingSQLInsertClause addFlag(final QueryFlag.Position position, final Expression<?> flag) {
        super.addFlag(position, flag);
        return this;
    }

    @Override
    public IdGeneratingSQLInsertClause addFlag(final QueryFlag.Position position, final String flag) {
        super.addFlag(position, flag);
        return this;
    }
}
