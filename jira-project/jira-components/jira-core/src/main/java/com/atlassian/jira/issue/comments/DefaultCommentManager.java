package com.atlassian.jira.issue.comments;

import com.atlassian.core.util.collection.EasyList;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.bc.issue.comment.property.CommentPropertyHelper;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.group.GroupConfigurable;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.entity.EntityUtils;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.entity.property.JsonEntityPropertyManager;
import com.atlassian.jira.event.issue.IssueEventSource;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.exception.CommentBodyCharacterLimitExceededException;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.ActionConstants;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.util.IssueUpdateBean;
import com.atlassian.jira.issue.util.IssueUpdater;
import com.atlassian.jira.model.querydsl.QAction;
import com.atlassian.jira.model.querydsl.QIssue;
import com.atlassian.jira.model.querydsl.QProjectRole;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.JiraDateUtils;
import com.atlassian.jira.util.ObjectUtils;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.json.JSONObject;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QTuple;
import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.EntityCondition;
import org.ofbiz.core.entity.EntityFieldMap;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.ofbiz.core.util.UtilDateTime;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.base.Preconditions.checkArgument;

public class DefaultCommentManager implements CommentManager, GroupConfigurable {

    private static final String COMMENT_ID = "id";
    public static final String COMMENT_ENTITY = "Action";

    private static final QTuple COMMENT_PROJECT_ROLE_TUPLE = Projections.tuple(ImmutableList.<Expression<?>>builder()
            .addAll(Lists.newArrayList(QAction.ACTION.all()))
            .addAll(Lists.newArrayList(QProjectRole.PROJECT_ROLE.all()))
            .build());
    private final UserManager userManager;
    private final ProjectRoleManager projectRoleManager;
    private final CommentPermissionManager commentPermissionManager;
    private final OfBizDelegator delegator;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final TextFieldCharacterLengthValidator textFieldCharacterLengthValidator;
    private final JsonEntityPropertyManager jsonEntityPropertyManager;
    private final CommentPropertyHelper commentPropertyHelper;
    private final CommentSearchManager commentSearchManager;
    private final CommentEventPublisher commentEventPublisher;
    private final IssueUpdater issueUpdater;
    private final DbConnectionManager dbConnectionManager;
    private final QueryDSLCommentFactory queryDSLCommentFactory;
    private final QueryDslAccessor queryDslAccessor;

    public DefaultCommentManager(ProjectRoleManager projectRoleManager,
                                 CommentPermissionManager commentPermissionManager,
                                 OfBizDelegator delegator,
                                 JiraAuthenticationContext jiraAuthenticationContext,
                                 TextFieldCharacterLengthValidator textFieldCharacterLengthValidator,
                                 UserManager userManager,
                                 JsonEntityPropertyManager jsonEntityPropertyManager,
                                 CommentPropertyHelper commentPropertyHelper,
                                 CommentSearchManager commentSearchManager,
                                 CommentEventPublisher commentEventPublisher,
                                 IssueUpdater issueUpdater,
                                 DbConnectionManager dbConnectionManager,
                                 QueryDSLCommentFactory queryDSLCommentFactory, QueryDslAccessor queryDslAccessor) {
        this.commentSearchManager = commentSearchManager;
        this.projectRoleManager = projectRoleManager;
        this.commentPermissionManager = commentPermissionManager;
        this.delegator = delegator;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.textFieldCharacterLengthValidator = textFieldCharacterLengthValidator;
        this.userManager = userManager;
        this.jsonEntityPropertyManager = jsonEntityPropertyManager;
        this.commentPropertyHelper = commentPropertyHelper;
        this.commentEventPublisher = commentEventPublisher;
        this.issueUpdater = issueUpdater;
        this.dbConnectionManager = dbConnectionManager;
        this.queryDSLCommentFactory = queryDSLCommentFactory;
        this.queryDslAccessor = queryDslAccessor;
    }

    @Override
    public ProjectRole getProjectRole(Long projectRoleId) {
        return projectRoleManager.getProjectRole(projectRoleId);
    }

    @Override
    public Comment convertToComment(GenericValue gv) {
        return commentSearchManager.convertToComment(gv);
    }

    @Override
    public Comment getCommentById(Long commentId) {
        return commentSearchManager.getCommentById(commentId);
    }

    @Override
    public MutableComment getMutableComment(Long commentId) {
        return commentSearchManager.getMutableComment(commentId);
    }

    @Override
    public List<Comment> getCommentsForUser(Issue issue, ApplicationUser user) {
        return commentSearchManager.getCommentsForUser(issue, user);
    }

    @Override
    public Stream<Comment> streamComments(@Nullable ApplicationUser user, @Nonnull Issue issue) {
        return commentSearchManager.streamComments(user, issue);
    }

    @Override
    public CommentSummary getCommentSummary(@Nullable ApplicationUser user, @Nonnull Issue issue, @Nonnull final Optional<Long> focusedCommentId) {
        return commentSearchManager.getCommentSummary(user, issue, focusedCommentId);
    }

    @Override
    public Comment getLastComment(Issue issue) {
        final GenericValue commentGV = Select.from(COMMENT_ENTITY)
                .whereEqual("issue", issue.getId())
                .andEqual("type", ActionConstants.TYPE_COMMENT)
                .orderBy("created DESC")
                .limit(1)
                .runWith(delegator)
                .singleValue();

        return commentGV == null ? null : convertToComment(commentGV);
    }

    @Override
    @Nonnull
    public List<Comment> getCommentsForUserSince(@Nonnull Issue issue, @Nullable ApplicationUser user, @Nonnull Date since) {
        return commentSearchManager.getCommentsForUserSince(issue, user, since);
    }

    @Override
    public List<Comment> getComments(Issue issue) {
        return commentSearchManager.getComments(issue);
    }

    @Override
    public Comment create(Issue issue, ApplicationUser author, String body, boolean dispatchEvent) {
        return create(issue, author, body, null, null, dispatchEvent);
    }

    @Override
    public Comment create(Issue issue, String author, String body, boolean dispatchEvent) {
        return create(issue, userManager.getUserByKeyEvenWhenUnknown(author), body, dispatchEvent);
    }

    @Override
    public Comment create(Issue issue, ApplicationUser author, String body, String groupLevel, Long roleLevelId, boolean dispatchEvent)
            throws DataAccessException {
        return create(issue, author, body, groupLevel, roleLevelId, new Date(), dispatchEvent);
    }

    @Override
    public Comment create(Issue issue, String author, String body, String groupLevel, Long roleLevelId, boolean dispatchEvent) {
        return create(issue, userManager.getUserByKeyEvenWhenUnknown(author), body, groupLevel, roleLevelId, dispatchEvent);
    }

    @Override
    public Comment create(Issue issue, ApplicationUser author, String body, String groupLevel, Long roleLevelId, Date created, boolean dispatchEvent)
            throws DataAccessException {
        return create(issue, author, author, body, groupLevel, roleLevelId, created, created, dispatchEvent);
    }

    @Override
    public Comment create(Issue issue, ApplicationUser author, String body, String groupLevel, Long roleLevelId, Date created, Map<String, JSONObject> commentProperties, boolean dispatchEvent) {
        return create(issue, author, author, body, groupLevel, roleLevelId, created, created, commentProperties, dispatchEvent, true);
    }

    @Override
    public Comment create(Issue issue, String author, String body, String groupLevel, Long roleLevelId, Date created, boolean dispatchEvent) {
        return create(issue, userManager.getUserByKeyEvenWhenUnknown(author), body, groupLevel, roleLevelId, created, dispatchEvent);
    }

    @Override
    public Comment create(Issue issue, ApplicationUser author, ApplicationUser updateAuthor, String body, String groupLevel, Long roleLevelId, Date created, Date updated, boolean dispatchEvent)
            throws DataAccessException {
        return create(issue, author, updateAuthor, body, groupLevel, roleLevelId, created, updated, dispatchEvent, true);
    }

    @Override
    public Comment create(Issue issue, String author, String updateAuthor, String body, String groupLevel, Long roleLevelId, Date created, Date updated, boolean dispatchEvent) {
        return create(issue, userManager.getUserByKeyEvenWhenUnknown(author), userManager.getUserByKeyEvenWhenUnknown(updateAuthor), body, groupLevel, roleLevelId, created, updated, dispatchEvent);
    }

    @Override
    public Comment create(Issue issue, ApplicationUser author, ApplicationUser updateAuthor, String body, String groupLevel, Long roleLevelId, Date created, Date updated, boolean dispatchEvent, boolean modifyIssueUpdateDate) {
        return create(issue, author, updateAuthor, body, groupLevel, roleLevelId, created, updated, Collections.emptyMap(), dispatchEvent, modifyIssueUpdateDate);
    }

    public Comment create(Issue issue, ApplicationUser author, ApplicationUser updateAuthor, String body, String groupLevel, Long roleLevelId, Date created, Date updated, Map<String, JSONObject> commentProperties, boolean dispatchEvent, boolean modifyIssueUpdateDate) {

        validateCommentBodyLength(body);

        // create new instance of comment
        CommentImpl comment = new CommentImpl(projectRoleManager, author, updateAuthor, body, groupLevel, roleLevelId, created, updated, issue);

        // create persistable generic value
        Map<String, Object> fields = new HashMap<>();
        fields.put("issue", issue.getId());
        fields.put("type", ActionConstants.TYPE_COMMENT);

        ApplicationUser commentAuthor = comment.getAuthorApplicationUser();
        ApplicationUser commentUpdateAuthor = comment.getUpdateAuthorApplicationUser();
        fields.put("author", commentAuthor == null ? null : commentAuthor.getKey());
        fields.put("updateauthor", commentUpdateAuthor == null ? null : commentUpdateAuthor.getKey());
        fields.put("body", comment.getBody());
        fields.put("level", comment.getGroupLevel());
        fields.put("rolelevel", comment.getRoleLevelId());
        fields.put("created", new Timestamp(comment.getCreated().getTime()));
        fields.put("updated", new Timestamp(comment.getUpdated().getTime()));

        GenericValue commentGV = EntityUtils.createValue(COMMENT_ENTITY, fields);
        // set the ID on comment object
        comment.setId(commentGV.getLong(COMMENT_ID));

        // Update the issue object if required
        if (modifyIssueUpdateDate) {
            queryDslAccessor.execute(dbConnection -> {
                Timestamp commentDate = new Timestamp(comment.getUpdated().getTime());
                dbConnection.update(QIssue.ISSUE)
                        .set(QIssue.ISSUE.updated, commentDate)
                        .where(QIssue.ISSUE.updated.loe(commentDate)) // JRA-36334: Only modify the Issue updated date if it would move forward - we don't want it to go back in time
                        .where(QIssue.ISSUE.id.eq(issue.getId()))
                        .execute();
            });
        }

        if (commentProperties != null) {
            setProperties(author, comment, commentProperties);
        }

        // Dispatch an event if required
        if (dispatchEvent) {
            Map<String, Object> params = new HashMap<>();
            params.put("eventsource", IssueEventSource.ACTION);
            commentEventPublisher.publishCommentCreatedEvent(comment, params);
        }
        return comment;
    }

    private void validateCommentBodyLength(final String body) {
        if (textFieldCharacterLengthValidator.isTextTooLong(body)) {
            throw new CommentBodyCharacterLimitExceededException(textFieldCharacterLengthValidator.getMaximumNumberOfCharacters());
        }
    }

    @Override
    public Comment create(Issue issue, String author, String updateAuthor, String body, String groupLevel, Long roleLevelId, Date created, Date updated, boolean dispatchEvent, boolean modifyIssueUpdateDate) {
        return create(issue, userManager.getUserByKeyEvenWhenUnknown(author), userManager.getUserByKeyEvenWhenUnknown(updateAuthor), body, groupLevel, roleLevelId, created, updated, dispatchEvent, modifyIssueUpdateDate);
    }

    @Override
    public void update(Comment comment, boolean dispatchEvent) {
        update(comment, Collections.emptyMap(), dispatchEvent);
    }

    @Override
    public void update(Comment comment, Map<String, JSONObject> commentProperties, boolean dispatchEvent) {
        if (comment == null) {
            throw new IllegalArgumentException("Comment must not be null");
        }
        if (comment.getId() == null) {
            throw new IllegalArgumentException("Comment ID must not be null");
        }
        validateCommentBodyLength(comment.getBody());

        // We need an in-memory copy of the old comment so we can pass it through in the fired event and to make sure
        // that some fields have changed.
        Comment originalComment = getCommentById(comment.getId());
        if (originalComment == null) {
            throw new IllegalArgumentException("Can not find a comment in the datastore with id: " + comment.getId());
        }

        // Make sure that either the comment body or visibility data has changed, otherwise do not update the datastore
        if (!areCommentsEquivalent(originalComment, comment)) {
            try {
                // create persistable generic value
                final GenericValue commentGV = delegator.findById(COMMENT_ENTITY, comment.getId());
                populateGenericValueFromComment(comment, commentGV);
                commentGV.store();
            } catch (GenericEntityException e) {
                throw new DataAccessException(e);
            }

            // Update the issue object
            IssueFactory issueFactory = ComponentAccessor.getComponentOfType(IssueFactory.class);
            GenericValue issueGV = comment.getIssue().getGenericValue();
            MutableIssue mutableIssue = issueFactory.getIssue(issueGV);
            mutableIssue.setUpdated(UtilDateTime.nowTimestamp());
            mutableIssue.store();
        }

        // Update comment properties
        if (commentProperties != null) {
            setProperties(comment.getAuthorApplicationUser(), comment, commentProperties);
        }

        // Dispatch an event if required
        if (dispatchEvent) {
            final Map<String, Object> parameters =
                    MapBuilder.build("eventsource", IssueEventSource.ACTION, EVENT_ORIGINAL_COMMENT_PARAMETER, originalComment);
            commentEventPublisher.publishCommentUpdatedEvent(comment, parameters);
        }
    }

    @Override
    public ChangeItemBean delete(Comment comment) {
        return doDelete(comment, true, jiraAuthenticationContext.getLoggedInUser());
    }

    @Override
    public void delete(Comment comment, boolean dispatchEvent, ApplicationUser user) {
        doDelete(comment, dispatchEvent, user);
    }

    private ChangeItemBean doDelete(Comment comment, boolean dispatchEvent, ApplicationUser user) {
        ChangeItemBean changeItemBean = constructChangeItemBeanForCommentDelete(comment);

        delegator.removeByAnd("Action", FieldMap.build("id", comment.getId(), "type", ActionConstants.TYPE_COMMENT));
        jsonEntityPropertyManager.deleteByEntity(EntityPropertyType.COMMENT_PROPERTY.getDbEntityName(), comment.getId());

        IssueUpdateBean issueUpdateBean =
                new IssueUpdateBean(comment.getIssue(), comment.getIssue(), EventType.ISSUE_COMMENT_DELETED_ID, user);

        issueUpdateBean.setChangeItems(Lists.newArrayList(changeItemBean));

        if (dispatchEvent) {
            issueUpdateBean.setDispatchEvent(true);
            commentEventPublisher.publishCommentDeletedEvent(comment);
        }

        issueUpdater.doUpdate(issueUpdateBean, false);

        return changeItemBean;
    }

    @Override
    public void deleteCommentsForIssue(Issue issue) {
        checkArgument(issue != null, "Cannot remove comments for not specified issue.");

        final List<Comment> commentForIssue = dbConnectionManager.executeQuery(dbConnection -> dbConnection.newSqlQuery()
                .select(COMMENT_PROJECT_ROLE_TUPLE)
                .from(QAction.ACTION)
                .leftJoin(QProjectRole.PROJECT_ROLE)
                .on(QProjectRole.PROJECT_ROLE.id.eq(QAction.ACTION.rolelevel))
                .where(QAction.ACTION.issue.eq(issue.getId())
                        .and(QAction.ACTION.type.eq(ActionConstants.TYPE_COMMENT))
                )
                .fetch())
                .stream()
                .map(tuple -> queryDSLCommentFactory.createComment(issue, tuple))
                .collect(Collectors.toList());

        List<Long> commentIds = commentForIssue.stream().map(Comment::getId).collect(Collectors.toList());

        if (commentIds.size() > 0) {
            dbConnectionManager.execute(dbConnection -> dbConnection.delete(QAction.ACTION)
                    .where(QAction.ACTION.id.in(commentIds))
                    .execute());

            jsonEntityPropertyManager.deleteByEntityNameAndEntityIds(EntityPropertyType.COMMENT_PROPERTY.getDbEntityName(), commentIds);
            commentForIssue.stream().forEach(commentEventPublisher::publishCommentDeletedEvent);
        }
    }

    @Override
    public boolean isUserCommentAuthor(ApplicationUser user, Comment comment) {
        return commentPermissionManager.isUserCommentAuthor(user, comment);
    }

    @Override
    public int swapCommentGroupRestriction(String groupName, String swapGroup) {
        return swapCommentFieldRestriction(RestrictionColumn.group, groupName, swapGroup);
    }

    @Override
    public int swapCommentRoleRestriction(Long roleId, Long swapRoleId) {
        return swapCommentFieldRestriction(RestrictionColumn.role, roleId, swapRoleId);
    }

    @Override
    public long getCountForCommentsRestrictedByGroup(String groupName) {
        return getCountForCommentsRestrictedByField(RestrictionColumn.group, groupName);
    }

    @Override
    public long getCountForCommentsRestrictedByRole(Long roleId) {
        return getCountForCommentsRestrictedByField(RestrictionColumn.role, roleId);
    }

    private <T> int swapCommentFieldRestriction(RestrictionColumn column, T fieldValue, T swapFieldValue) {
        if (fieldValue == null) {
            throw new IllegalArgumentException("You must provide a non null " + column.column + " value.");
        }

        if (swapFieldValue == null) {
            throw new IllegalArgumentException("You must provide a non null swap " + column.column + " value.");
        }
        return delegator.bulkUpdateByAnd("Action",
                FieldMap.build(column.column, swapFieldValue),
                FieldMap.build(column.column, fieldValue, "type", ActionConstants.TYPE_COMMENT));
    }

    private <T> long getCountForCommentsRestrictedByField(RestrictionColumn column, T fieldValue) {
        if (fieldValue == null) {
            throw new IllegalArgumentException("You must provide a non null field value.");
        }

        EntityCondition condition = new EntityFieldMap(FieldMap.build(column.column, fieldValue, "type", ActionConstants.TYPE_COMMENT), EntityOperator.AND);
        List commentCount = delegator.findByCondition("ActionCount", condition, EasyList.build("count"), Collections.emptyList());
        if (commentCount != null && commentCount.size() == 1) {
            GenericValue commentCountGV = (GenericValue) commentCount.get(0);
            return commentCountGV.getLong("count");
        } else {
            throw new DataAccessException("Unable to access the count for the Action table");
        }
    }

    private enum RestrictionColumn {
        role("rolelevel"), group("level");

        private final String column;

        RestrictionColumn(final String column) {
            this.column = column;
        }
    }

    /**
     * Constructs an issue update bean for a comment delete. The comment text will be masked if the security levels are
     * set
     */
    ChangeItemBean constructChangeItemBeanForCommentDelete(Comment comment) {
        // Check the level of the comment, if the level is not null we need to override the comment
        // This is necessary as part of JRA-9394 to remove comment text from the change history for security (or lack thereof)
        String message;
        final String groupLevel = comment.getGroupLevel();
        final String roleLevel = (comment.getRoleLevel() == null) ? null : comment.getRoleLevel().getName();
        final String actionLevel = groupLevel == null ? roleLevel : groupLevel;
        if (actionLevel != null) {
            message = getText("comment.manager.deleted.comment.with.restricted.level", actionLevel);
        } else {
            message = comment.getBody();
        }

        return new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "Comment", message, null);
    }

    private String getText(String key, String param) {
        return jiraAuthenticationContext.getI18nHelper().getText(key, param);
    }

    private void populateGenericValueFromComment(Comment updatedComment, GenericValue commentGV) {
        ApplicationUser updateAuthor = updatedComment.getUpdateAuthorApplicationUser();
        final String groupLevel = updatedComment.getGroupLevel();
        final Long roleLevelId = updatedComment.getRoleLevelId();
        if (StringUtils.isNotBlank(groupLevel) && (roleLevelId != null)) {
            throw new IllegalArgumentException("Cannot specify both grouplevel and rolelevel comment visibility");
        }
        commentGV.setString("updateauthor", updateAuthor == null ? null : updateAuthor.getKey());
        commentGV.setString("body", updatedComment.getBody());
        commentGV.setString("level", groupLevel);
        commentGV.set("rolelevel", roleLevelId);
        commentGV.set("updated", JiraDateUtils.copyOrCreateTimestampNullsafe(updatedComment.getUpdated()));
    }

    private void setProperties(final ApplicationUser applicationUser, final Comment comment, Map<String, JSONObject> properties) {
        for (Map.Entry<String, JSONObject> property : properties.entrySet()) {
            jsonEntityPropertyManager.put(applicationUser, commentPropertyHelper.getEntityPropertyType().getDbEntityName(),
                    comment.getId(), property.getKey(), property.getValue().toString(), commentPropertyHelper.createSetPropertyEventFunction(), true);
        }
    }

    /**
     * Returns true if both comments have equal bodies, group levels and role level ids, false otherwise.
     *
     * @param comment1 comment to compare
     * @param comment2 comment to compare
     * @return true if both comments have equal bodies, group levels and role level ids, false otherwise
     */
    private boolean areCommentsEquivalent(Comment comment1, Comment comment2) {
        return ObjectUtils.equalsNullSafe(comment1.getBody(), comment2.getBody())
                && ObjectUtils.equalsNullSafe(comment1.getGroupLevel(), comment2.getGroupLevel())
                && ObjectUtils.equalsNullSafe(comment1.getRoleLevelId(), comment2.getRoleLevelId());
    }

    @Override
    public boolean isGroupUsed(@Nonnull final Group group) {
        return getCountForCommentsRestrictedByGroup(group.getName()) > 0;
    }
}
