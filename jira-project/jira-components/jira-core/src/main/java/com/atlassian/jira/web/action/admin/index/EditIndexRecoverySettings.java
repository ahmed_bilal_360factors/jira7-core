package com.atlassian.jira.web.action.admin.index;

import com.atlassian.fugue.Option;
import com.atlassian.jira.index.ha.IndexRecoveryService;
import com.atlassian.jira.issue.fields.option.TextOption;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.scheduler.cron.CronValidator;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.atlassian.jira.web.component.cron.CronEditorBean;
import com.atlassian.jira.web.component.cron.CronEditorWebComponent;
import com.atlassian.jira.web.component.cron.generator.CronExpressionGenerator;
import com.atlassian.jira.web.component.cron.parser.CronExpressionParser;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.atlassian.scheduler.SchedulerService;
import webwork.action.ActionContext;

import java.util.Collection;

@WebSudoRequired
public class EditIndexRecoverySettings extends ProjectActionSupport {
    private final IndexRecoveryService indexRecoveryService;
    private final SchedulerService schedulerService;

    // options
    private boolean recoveryEnabled;

    private IndexRecoveryUtil.Interval snapshotInterval;
    private CronEditorBean cronEditorBean;

    public EditIndexRecoverySettings(ProjectManager projectManager, PermissionManager permissionManager,
                                     final IndexRecoveryService indexRecoveryService, final SchedulerService schedulerService) {
        super(projectManager, permissionManager);
        this.indexRecoveryService = indexRecoveryService;
        this.schedulerService = schedulerService;
    }

    public String doDefault() throws Exception {
        recoveryEnabled = indexRecoveryService.isRecoveryEnabled(getLoggedInUser());
        if (recoveryEnabled) {
            String cronExpression = indexRecoveryService.getSnapshotCronExpression(getLoggedInUser());
            try {
                cronEditorBean = new CronExpressionParser(cronExpression).getCronEditorBean();
            } catch (IllegalArgumentException e) {
                //In case wrong cron expression is stored in database already
                cronEditorBean = new CronExpressionParser().getCronEditorBean();
                recoveryEnabled = false;
            }
        } else {
            recoveryEnabled = false;
            cronEditorBean = new CronExpressionParser().getCronEditorBean();
        }
        return INPUT;
    }

    protected void doValidation() {
        cronEditorBean = new CronEditorBean("service.schedule", ActionContext.getParameters());
        CronEditorWebComponent component = new CronEditorWebComponent();
        addErrorCollection(component.validateInput(cronEditorBean, "cron.editor.name"));
        if (!hasAnyErrors()) {
            final Option<String> cronError = new CronValidator(getI18nHelper(), schedulerService)
                    .validateCron(component.getCronExpressionFromInput(cronEditorBean));
            if (cronError.isDefined()) {
                addError("cron.editor.name", cronError.get());
            }
        }
        super.doValidation();
    }

    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        String cronExpression = new CronExpressionGenerator().getCronExpressionFromInput(cronEditorBean);
        indexRecoveryService.updateRecoverySettings(getLoggedInUser(), recoveryEnabled, cronExpression);
        return returnComplete("IndexAdmin.jspa");
    }

    public CronEditorBean getCronEditorBean() {
        return cronEditorBean;
    }

    public boolean isRecoveryEnabled() {
        return recoveryEnabled;
    }

    public void setRecoveryEnabled(final boolean recoveryEnabled) {
        this.recoveryEnabled = recoveryEnabled;
    }

    public String getSnapshotInterval() {
        return snapshotInterval.name();
    }

    public void setSnapshotInterval(final String snapshotInterval) {
        this.snapshotInterval = IndexRecoveryUtil.Interval.valueOf(snapshotInterval);
    }

    public Collection<TextOption> getIntervalOptions() {
        return IndexRecoveryUtil.getIntervalOptions(getI18nHelper());
    }


}
