package com.atlassian.jira.model.querydsl;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Generated;

/**
 * Data Transfer Object for the FieldScreenScheme entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 *
 * @see QFieldScreenScheme
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class FieldScreenSchemeDTO implements DTO {
    private final Long id;
    private final String name;
    private final String description;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public FieldScreenSchemeDTO(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    /**
     * Creates a GenericValue object from the values in this Data Transfer Object.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
     * @return a GenericValue object constructed from the values in this Data Transfer Object.
     */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator) {
        return ofBizDelegator.makeValue("FieldScreenScheme", new FieldMap()
                .add("id", id)
                .add("name", name)
                .add("description", description)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */
    public static FieldScreenSchemeDTO fromGenericValue(GenericValue gv) {
        return new FieldScreenSchemeDTO(
                gv.getLong("id"),
                gv.getString("name"),
                gv.getString("description")
        );
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(FieldScreenSchemeDTO fieldScreenSchemeDTO) {
        return new Builder(fieldScreenSchemeDTO);
    }

    public static class Builder {
        private Long id;
        private String name;
        private String description;

        public Builder() {
        }

        public Builder(FieldScreenSchemeDTO fieldScreenSchemeDTO) {
            this.id = fieldScreenSchemeDTO.id;
            this.name = fieldScreenSchemeDTO.name;
            this.description = fieldScreenSchemeDTO.description;
        }

        public FieldScreenSchemeDTO build() {
            return new FieldScreenSchemeDTO(id, name, description);
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }
        public Builder name(String name) {
            this.name = name;
            return this;
        }
        public Builder description(String description) {
            this.description = description;
            return this;
        }
    }
}