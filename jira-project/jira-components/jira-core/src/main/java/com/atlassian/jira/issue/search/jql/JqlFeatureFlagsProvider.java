package com.atlassian.jira.issue.search.jql;

import com.atlassian.jira.config.FeatureFlag;
import com.atlassian.jira.config.FeatureFlagProvider;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

/**
 * Feature flag provider to give user a possibility to gracefully shut down experimental JQL features
 *
 * @since v7.3
 */
public class JqlFeatureFlagsProvider implements FeatureFlagProvider {

    public static final String JQL_AUTOSELECTFIRST_FEATURE_NAME = "jira.jql.autoselectfirst";
    private final FeatureFlag jqlAutoselectFeature;

    public JqlFeatureFlagsProvider() {
        this.jqlAutoselectFeature = FeatureFlag.featureFlag(JQL_AUTOSELECTFIRST_FEATURE_NAME).defaultedTo(true);
    }

    @Override
    public Set<FeatureFlag> getFeatureFlags() {
        return ImmutableSet.of(jqlAutoselectFeature);
    }
}
