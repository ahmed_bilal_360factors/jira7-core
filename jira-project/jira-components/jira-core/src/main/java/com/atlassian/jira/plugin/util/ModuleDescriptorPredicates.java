package com.atlassian.jira.plugin.util;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.elements.ResourceDescriptor;

import javax.annotation.Nonnull;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Predicates for module descriptors.
 *
 * @since v6.5
 */
public final class ModuleDescriptorPredicates {
    private ModuleDescriptorPredicates() {
    }

    public static Predicate<Plugin> isPluginWithModuleDescriptor(@Nonnull final Class<? extends ModuleDescriptor> targetModuleClass) {
        return (Plugin plugin) -> plugin.getModuleDescriptors().stream().
                map(ModuleDescriptor::getClass).anyMatch(targetModuleClass::isAssignableFrom);
    }

    public static Predicate<Plugin> isPluginWithResourceType(@Nonnull final String pluginResourceType) {
        return (Plugin plugin) -> {
            final Stream<ResourceDescriptor> resourceDescriptors = Stream.concat(plugin.getResourceDescriptors().stream(),
                    plugin.getModuleDescriptors().stream().flatMap(module -> module.getResourceDescriptors().stream()));
            return resourceDescriptors.map(ResourceDescriptor::getType).anyMatch(pluginResourceType::equals);
        };
    }
}
