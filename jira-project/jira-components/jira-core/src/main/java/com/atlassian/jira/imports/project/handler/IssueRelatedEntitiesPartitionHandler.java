package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.external.beans.ExternalChangeGroup;
import com.atlassian.jira.external.beans.ExternalChangeItem;
import com.atlassian.jira.external.beans.ExternalComment;
import com.atlassian.jira.external.beans.ExternalEntityProperty;
import com.atlassian.jira.external.beans.ExternalLink;
import com.atlassian.jira.external.beans.ExternalNodeAssociation;
import com.atlassian.jira.external.beans.ExternalVoter;
import com.atlassian.jira.external.beans.ExternalWatcher;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.parser.ChangeGroupParser;
import com.atlassian.jira.imports.project.parser.ChangeGroupParserImpl;
import com.atlassian.jira.imports.project.parser.ChangeItemParser;
import com.atlassian.jira.imports.project.parser.ChangeItemParserImpl;
import com.atlassian.jira.imports.project.parser.CommentParser;
import com.atlassian.jira.imports.project.parser.CommentParserImpl;
import com.atlassian.jira.imports.project.parser.EntityPropertyParser;
import com.atlassian.jira.imports.project.parser.EntityPropertyParserImpl;
import com.atlassian.jira.imports.project.parser.IssueLinkParser;
import com.atlassian.jira.imports.project.parser.IssueLinkParserImpl;
import com.atlassian.jira.imports.project.parser.IssueParser;
import com.atlassian.jira.imports.project.parser.NodeAssociationParser;
import com.atlassian.jira.imports.project.parser.NodeAssociationParserImpl;
import com.atlassian.jira.imports.project.parser.UserAssociationParser;
import com.atlassian.jira.imports.project.parser.UserAssociationParserImpl;
import com.atlassian.jira.imports.project.util.ProjectImportTemporaryFiles;
import com.atlassian.jira.issue.IssueRelationConstants;
import org.ofbiz.core.entity.DelegatorInterface;
import org.ofbiz.core.entity.GenericDelegator;
import org.ofbiz.core.entity.GenericEntity;
import org.ofbiz.core.entity.model.ModelEntity;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Parses an XML import file and writes a smaller "partition" containing just the values for certain issue-related
 * entities that are valid for the project we are importing.
 *
 * @since v3.13
 */
public class IssueRelatedEntitiesPartitionHandler implements ImportOfBizEntityHandler {

    private final BackupProject backupProject;
    private final ProjectImportTemporaryFiles projectImportTemporaryFiles;
    private final Map<String, ModelEntity> modelEntityMap;
    private final GenericDelegator delegator;
    private final Set<Long> changeGroupIds;
    private final Set<Long> commentIds;
    private ChangeGroupParser changeGroupParser;
    private ChangeItemParser changeItemParser;
    private UserAssociationParser userAssociationParser;
    private EntityPropertyParser entityPropertyParser;
    private NodeAssociationParser nodeAssocationParser;
    private IssueLinkParser issueLinkParser;
    private CommentParser commentParser;
    private int entityCount = 0;
    private int secondDegreeEntityCount = 0;
    private String currentEntity;
    private PrintWriter printWriter;

    /**
     * @param backupProject               contains the issue id's that we are interested in partitioning.
     * @param projectImportTemporaryFiles the interface to the temporary import files.
     * @param modelEntities               a List of {@link org.ofbiz.core.entity.model.ModelEntity}'s that the partitioner should be
     *                                    interested in.
     * @param delegatorInterface          required for persistence
     */
    public IssueRelatedEntitiesPartitionHandler(final BackupProject backupProject, final ProjectImportTemporaryFiles projectImportTemporaryFiles,
                                                final List<ModelEntity> modelEntities, final DelegatorInterface delegatorInterface) {
        this.backupProject = backupProject;
        this.projectImportTemporaryFiles = projectImportTemporaryFiles;
        this.delegator = GenericDelegator.getGenericDelegator(delegatorInterface.getDelegatorName());
        modelEntityMap = new HashMap<String, ModelEntity>();
        changeGroupIds = new HashSet<Long>();
        commentIds = new HashSet<Long>();
        for (final ModelEntity modelEntity : modelEntities) {
            final String entityName = modelEntity.getEntityName();
            modelEntityMap.put(entityName, modelEntity);
        }
    }

    public void handleEntity(final String entityName, final Map<String, String> attributes) throws ParseException {
        final ModelEntity modelEntity = modelEntityMap.get(entityName);
        if (modelEntity == null) {
            return;
        }
        boolean saveEntity;
        // A couple of entity types store the issueID in a non-standard way
        if (NodeAssociationParser.NODE_ASSOCIATION_ENTITY_NAME.equals(entityName)) {
            saveEntity = handleNodeAssociation(attributes);
        } else if (IssueLinkParser.ISSUE_LINK_ENTITY_NAME.equals(entityName)) {
            saveEntity = handleIssueLink(attributes);
        } else if (ChangeItemParser.CHANGE_ITEM_ENTITY_NAME.equals(entityName)) {
            // This is a special case since we write it to a different XML file
            saveEntity = false;
            if (handleChangeItem(attributes)) {
                saveSecondDegreeEntity(modelEntity, attributes);
            }
        } else if (UserAssociationParser.USER_ASSOCIATION_ENTITY_NAME.equals(entityName)) {
            saveEntity = handleVotersAndWatchers(attributes);
        } else if (EntityPropertyParser.ENTITY_PROPERTY_ENTITY_NAME.equals(entityName)) {
            saveEntity = handleEntityProperty(modelEntity, attributes);
        } else {
            final String issueId = attributes.get("issue");
            saveEntity = backupProject.containsIssue(issueId);
        }

        if (saveEntity) {
            // If we are inspecting a changeGroup then save its id
            handleChangeGroup(entityName, attributes);
            handleComment(entityName, attributes);
            writeXmlText(entityName, attributes, modelEntity);
            // Count the entities as we go, we use the count later for our task progress bar.
            entityCount++;
        }
    }

    private void saveSecondDegreeEntity(final ModelEntity modelEntity, final Map<String, String> attributes)
            throws ParseException {
        final String entityName = modelEntity.getEntityName();
        writeXmlText(entityName, attributes, modelEntity);
        // Count the entities as we go, we use the count later for our task progress bar.
        secondDegreeEntityCount++;
    }

    private void writeXmlText(final String entityName, final Map<String, String> attributes, final ModelEntity modelEntity)
            throws ParseException {
        if (!entityName.equals(currentEntity)) {
            endDocument();
        }
        if (printWriter == null) {
            try {
                currentEntity = entityName;
                printWriter = projectImportTemporaryFiles.getWriter(entityName);
            } catch (IOException e) {
                throw new ParseException(e.getMessage());
            }
        }
        // Create a GenericEntity
        final GenericEntity genericEntity = new GenericEntity(delegator, modelEntity, attributes);
        genericEntity.writeXmlText(printWriter, null);
    }

    private boolean handleEntityProperty(final ModelEntity modelEntity, final Map<String, String> attributes) throws ParseException {
        final ExternalEntityProperty entityProperty = getEntityPropertyParser().parse(attributes);
        if (EntityPropertyType.ISSUE_PROPERTY.getDbEntityName().equals(entityProperty.getEntityName())) {
            return backupProject.containsIssue(String.valueOf(entityProperty.getEntityId()));
        } else if (EntityPropertyType.COMMENT_PROPERTY.getDbEntityName().equals(entityProperty.getEntityName()) && commentIds.contains(entityProperty.getEntityId())) {
            saveSecondDegreeEntity(modelEntity, attributes);
            return false;
        } else if (EntityPropertyType.CHANGE_HISTORY_PROPERTY.getDbEntityName().equals(entityProperty.getEntityName()) && changeGroupIds.contains(entityProperty.getEntityId())) {
            saveSecondDegreeEntity(modelEntity, attributes);
            return false;
        }
        return false;

    }

    public int getEntityCount() {
        return entityCount;
    }

    public int getSecondDegreeEntityCount() {
        return secondDegreeEntityCount;
    }

    private boolean handleVotersAndWatchers(final Map<String, String> attributes) throws ParseException {
        final ExternalVoter externalVoter = getUserAssociationParser().parseVoter(attributes);
        if (externalVoter != null) {
            return backupProject.containsIssue(externalVoter.getIssueId());
        }
        final ExternalWatcher externalWatcher = getUserAssociationParser().parseWatcher(attributes);
        if (externalWatcher != null) {
            return backupProject.containsIssue(externalWatcher.getIssueId());
        }
        return false;
    }

    private boolean handleChangeItem(final Map<String, String> attributes) throws ParseException {
        final ExternalChangeItem externalChangeItem = getChangeItemParser().parse(attributes);
        try {
            final Long groupId = new Long(externalChangeItem.getChangeGroupId());
            return changeGroupIds.contains(groupId);
        } catch (final NumberFormatException e) {
            throw new ParseException("Unable to parse the changeGroup id'" + externalChangeItem.getChangeGroupId() + "' for change item.");
        }
    }

    public void startDocument() {
        // NOOP - document header created lazily on first element write
    }

    public void endDocument() {
        printWriter = null;
    }

    ///CLOVER:OFF - NOTE: this is mainly here for testing purposes
    public Map<String, ModelEntity> getRegisteredHandlers() {
        return Collections.unmodifiableMap(modelEntityMap);
    }

    ///CLOVER:ON

    private void handleComment(final String entityName, final Map<String, String> attributes) throws ParseException {
        if (CommentParser.COMMENT_ENTITY_NAME.equals(entityName)) {
            final ExternalComment externalComment = getCommentParser().parse(attributes);
            if (externalComment != null) {
                final String externalCommentId = externalComment.getId();
                if (externalCommentId != null) {
                    try {
                        final Long id = new Long(externalCommentId);
                        commentIds.add(id);
                    } catch (final NumberFormatException e) {
                        throw new ParseException("Unable to parse the id for changeGroup '" + externalCommentId + "'");
                    }
                } else {
                    throw new ParseException("Encountered a Comment entry without an id, this should not happen");
                }
            }
        }
    }

    private void handleChangeGroup(final String entityName, final Map<String, String> attributes) throws ParseException {
        // keep the change group id's so we can correctly store the changeItems
        if (ChangeGroupParser.CHANGE_GROUP_ENTITY_NAME.equals(entityName)) {
            final ExternalChangeGroup externalChangeGroup = getChangeGroupParser().parse(attributes);
            final String idStr = externalChangeGroup.getId();
            if (idStr != null) {
                try {
                    final Long id = new Long(idStr);
                    changeGroupIds.add(id);
                } catch (final NumberFormatException e) {
                    throw new ParseException("Unable to parse the id for changeGroup '" + idStr + "'");
                }
            } else {
                throw new ParseException("Encountered a ChangeGroup entry without an id, this should not happen.");
            }
        }
    }

    private boolean handleIssueLink(final Map<String, String> attributes) throws ParseException {
        // Let the IssueLink Parser parse the xml:
        final ExternalLink externalLink = getIssueLinkParser().parse(attributes);

        return backupProject.containsIssue(externalLink.getSourceId()) || backupProject.containsIssue(externalLink.getDestinationId());
    }

    private boolean handleNodeAssociation(final Map<String, String> attributes) throws ParseException {
        final ExternalNodeAssociation externalNodeAssociation = getNodeAssociationParser().parse(attributes);
        if (IssueParser.ISSUE_ENTITY_NAME.equals(externalNodeAssociation.getSourceNodeEntity()) && (IssueRelationConstants.VERSION.equals(externalNodeAssociation.getAssociationType()) || IssueRelationConstants.FIX_VERSION.equals(externalNodeAssociation.getAssociationType()) || IssueRelationConstants.COMPONENT.equals(externalNodeAssociation.getAssociationType()))) {
            return backupProject.containsIssue(externalNodeAssociation.getSourceNodeId());
        }
        return false;
    }

    ///CLOVER:OFF
    private NodeAssociationParser getNodeAssociationParser() {
        if (nodeAssocationParser == null) {
            nodeAssocationParser = new NodeAssociationParserImpl();
        }
        return nodeAssocationParser;
    }

    ///CLOVER:ON

    ///CLOVER:OFF
    private IssueLinkParser getIssueLinkParser() {
        if (issueLinkParser == null) {
            issueLinkParser = new IssueLinkParserImpl();
        }
        return issueLinkParser;
    }

    ///CLOVER:ON

    ///CLOVER:OFF
    private ChangeGroupParser getChangeGroupParser() {
        if (changeGroupParser == null) {
            changeGroupParser = new ChangeGroupParserImpl();
        }
        return changeGroupParser;
    }

    ///CLOVER:ON

    ///CLOVER:OFF
    private ChangeItemParser getChangeItemParser() {
        if (changeItemParser == null) {
            changeItemParser = new ChangeItemParserImpl();
        }
        return changeItemParser;
    }

    ///CLOVER:ON

    ///CLOVER:OFF
    UserAssociationParser getUserAssociationParser() {
        if (userAssociationParser == null) {
            userAssociationParser = new UserAssociationParserImpl();
        }
        return userAssociationParser;
    }
    ///CLOVER:ON

    //CLOVER:OFF
    EntityPropertyParser getEntityPropertyParser() {
        if (entityPropertyParser == null) {
            entityPropertyParser = new EntityPropertyParserImpl();
        }
        return entityPropertyParser;
    }
    ///CLOVER:ON

    CommentParser getCommentParser() {
        if (commentParser == null) {
            commentParser = new CommentParserImpl();
        }
        return commentParser;
    }
}