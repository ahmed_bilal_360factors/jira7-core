package com.atlassian.jira.config.feature;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.soy.renderer.JsExpression;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

public class FeatureFlagFunction implements SoyServerFunction<Boolean>, SoyClientFunction {
    private FeatureManager jiraFeatureManager;

    public FeatureFlagFunction(FeatureManager jiraFeatureManager) {
        this.jiraFeatureManager = jiraFeatureManager;
    }

    //Client-side
    @Override
    public JsExpression generate(JsExpression... args) {
        Preconditions.checkArgument(args.length == 1, "Wrong number of arguments (1 expected)");

        return new JsExpression("require(\"jira/featureflags/feature-manager\").isFeatureEnabled(" + args[0].getText() + ")");
    }

    @Override
    public String getName() {
        return "isFeatureFlagEnabled";
    }

    //Server-side
    @Override
    public Boolean apply(Object... args) {
        Preconditions.checkArgument(args.length == 1, "Wrong number of arguments (1 expected)");
        Preconditions.checkArgument(args[0] instanceof String, "The first argument, the feature flag, should be a string");

        String featureKey = (String) args[0];
        return isFeatureEnabled(featureKey);
    }

    //Server-side only
    @VisibleForTesting
    private boolean isFeatureEnabled(String featureKey) {
        return jiraFeatureManager.isEnabled(featureKey);
    }

    @Override
    public Set<Integer> validArgSizes() {
        return ImmutableSet.of(1);
    }
}
