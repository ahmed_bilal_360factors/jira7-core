package com.atlassian.jira.servermetrics;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Converts timing information to analytics event.
 */
public class TimingInformationToEvent {

    public static final String B3_TRACE_ID = "B3-TraceId";

    public RequestMetricsEvent createStatEvent(TimingInformation input, HttpServletRequest httpServletRequest, Optional<String> requestKey) {
        return new RequestMetricsEvent(
                RequestMetricsDispatcher.getRequestPath(httpServletRequest),
                requestKey,
                input.getTotalTime().toMillis(),
                input.getUserTime().toMillis(),
                input.getCpuTime().toMillis(),
                input.getGarbageCollectionTime().toMillis(),
                input.getGarbageCollectionCount(),
                extractCorrelationID(httpServletRequest),
                input.getTimingEventList(),
                input.getActivityDurations());
    }

    protected static Optional<String> extractCorrelationID(HttpServletRequest httpServletRequest) {
        return Optional.ofNullable((String) httpServletRequest.getAttribute(B3_TRACE_ID));
    }
}