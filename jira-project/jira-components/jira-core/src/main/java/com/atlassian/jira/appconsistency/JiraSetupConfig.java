package com.atlassian.jira.appconsistency;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.johnson.setup.SetupConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

/**
 * Determines whether or not JIRA is setup
 */
public class JiraSetupConfig implements SetupConfig {
    private static final Logger log = LoggerFactory.getLogger(JiraSetupConfig.class);

    /**
     * Determines if JIRA is set up.
     *
     * @param uri The uri of the current page
     * @return returns {@code true} if this is one of JIRA's setup pages (or looks like it could be);
     * {@code false} otherwise
     */
    @Override
    public boolean isSetupPage(@Nonnull final String uri) {
        return uri.startsWith("/secure/Setup");
    }

    /**
     * Returns {@code true} if JIRA is set up; {@code false} otherwise.
     * JIRA Cloud always reports itself as being set up.
     *
     * @return {@code true} if JIRA is set up; {@code false} otherwise.
     */
    @Override
    public boolean isSetup() {
        try {
            final ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();
            return applicationProperties.getString(APKeys.JIRA_SETUP) != null;
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }
}
