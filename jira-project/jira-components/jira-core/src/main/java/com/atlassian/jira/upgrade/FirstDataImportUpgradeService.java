package com.atlassian.jira.upgrade;

import com.atlassian.jira.index.request.ReindexRequestTypes;

import static com.atlassian.upgrade.api.UpgradeContext.UpgradeTrigger.PROVISIONING;

/**
 * Upgrade service for the first data import. It runs upgrades without triggering Lucene indexing and also
 * informs upgrade tasks that they are run on clean database.
 */
public class FirstDataImportUpgradeService implements UpgradeService {

    private final LoggingUpgradeService loggingUpgradeService;

    public FirstDataImportUpgradeService(final LoggingUpgradeService loggingUpgradeService) {
        this.loggingUpgradeService = loggingUpgradeService;
    }

    @Override
    public UpgradeResult runUpgrades() {
        return loggingUpgradeService.runUpgradesWithLogging(
                ReindexRequestTypes.noneAllowed(),
                () -> PROVISIONING,
                "run upgrades for first data import"
        );
    }

    @Override
    public boolean areUpgradesRunning() {
        return loggingUpgradeService.areUpgradesRunning();
    }
}
