package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.conditions.cache.ConditionCacheKeys;
import com.atlassian.jira.plugin.webfragment.conditions.cache.RequestCachingConditionHelper;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Condition to check a permission against a given issue for the current user.
 * <p>
 * An issue must be in the JiraHelper context params.
 *
 * @since v4.1
 */
public class HasIssuePermissionCondition extends AbstractProjectPermissionCondition {
    private static final Logger log = LoggerFactory.getLogger(HasIssuePermissionCondition.class);

    private final PermissionManager permissionManager;

    public HasIssuePermissionCondition(PermissionManager permissionManager) {
        this.permissionManager = permissionManager;
    }

    @Override
    protected boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper, ProjectPermissionKey permissionKey) {
        final Map<String, Object> params = jiraHelper.getContextParams();
        final Issue issue = (Issue) params.get("issue");

        if (issue == null) {
            log.warn("Trying to run permission condition on an issue, but no issue exists");
            return false;
        }

        return RequestCachingConditionHelper.cacheConditionResultInRequest(
                ConditionCacheKeys.permission(permissionKey, user, issue),
                () -> permissionManager.hasPermission(permissionKey, issue, user));
    }
}
