package com.atlassian.jira.i18n;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.LocaleManager;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.warmer.JiraWarmer;
import com.google.common.collect.ImmutableSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * This initializes CachingI18NFactory when JIRA is initialized.
 *
 * @since v7.1
 */
public class I18nWarmer implements JiraWarmer {
    private static final Logger log = LoggerFactory.getLogger(I18nWarmer.class);
    private final LocaleManager localeManager;
    private final I18nHelper.BeanFactory i18nBeanFactory;

    public I18nWarmer(final LocaleManager localeManager, final I18nHelper.BeanFactory i18nBeanFactory) {
        this.localeManager = localeManager;
        this.i18nBeanFactory = i18nBeanFactory;
    }

    @Override
    public void run() {
        final long startTime = System.currentTimeMillis();
        log.info("Init i18n cache");
        try {
            getLocalesToWarm().forEach(i18nBeanFactory::getInstance);
        } catch (final Exception e) {
            log.warn("Cannot eager init i18n cache failed", e);
        }
        final long endTime = System.currentTimeMillis();
        log.info("Initialised i18n cache in {}", endTime - startTime);
    }

    private Set<Locale> getLocalesToWarm() {
        return ImmutableSet.<Locale>builder()
                .addAll(localeManager.getInstalledLocales())
                .add(Locale.ROOT)
                .build();
    }
}
