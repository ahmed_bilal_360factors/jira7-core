package com.atlassian.jira.servermetrics;

import javax.annotation.concurrent.NotThreadSafe;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * When there are nested requests (via forwarding) request keys obtained from
 * inner request can overwrite outer request id.
 * In the end outer request sees request id from inner request.
 */
@NotThreadSafe
public class NestedRequestKeyResolver {
    private Optional<String> requestKey = Optional.empty();
    private int referenceCount;
    private final RequestKeyResolver requestKeyResolver;

    public NestedRequestKeyResolver(RequestKeyResolver requestKeyResolver) {
        this.requestKeyResolver = requestKeyResolver;
    }

    public void requestStarted(HttpServletRequest httpServletRequest) {
        ++referenceCount;
    }

    public Optional<String> requestFinished(HttpServletRequest httpServletRequest) {
        if (!requestKey.isPresent()) {
            requestKey = guessRequestKey(httpServletRequest);
        }

        Optional<String> result = requestKey;
        --referenceCount;
        if (referenceCount <= 0) {
            referenceCount = 0;
            requestKey = Optional.empty();
        }

        return result;
    }

    private Optional<String> guessRequestKey(HttpServletRequest httpServletRequest) {
        return requestKeyResolver.getRequestId(httpServletRequest);
    }
}
