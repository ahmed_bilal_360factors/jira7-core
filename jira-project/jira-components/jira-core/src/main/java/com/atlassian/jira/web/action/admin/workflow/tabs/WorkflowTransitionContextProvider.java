package com.atlassian.jira.web.action.admin.workflow.tabs;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

import static com.atlassian.jira.web.action.admin.workflow.tabs.WorkflowTransitionContextUtils.COUNT_KEY;
import static com.google.common.collect.Maps.filterEntries;

public abstract class WorkflowTransitionContextProvider implements ContextProvider {

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {
    }

    @Override
    public Map<String, Object> getContextMap(final Map<String, Object> context) {
        return ImmutableMap.<String, Object>builder()
                .putAll(filterEntries(context, input -> input.getKey() != null && input.getValue() != null))
                .put(COUNT_KEY, getCount(context))
                .build();
    }

    protected abstract int getCount(final Map<String, Object> context);
}
