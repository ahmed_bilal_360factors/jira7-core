package com.atlassian.jira.web.filters.steps.instrumentation;

import com.atlassian.instrumentation.Gauge;
import com.atlassian.instrumentation.InstrumentRegistry;
import com.atlassian.instrumentation.operations.OpSnapshot;
import com.atlassian.instrumentation.operations.OpTimer;
import com.atlassian.instrumentation.operations.OpTimerFactory;
import com.atlassian.jira.instrumentation.DefaultInstrumentationListenerManager;
import com.atlassian.jira.instrumentation.Instrumentation;
import com.atlassian.jira.instrumentation.InstrumentationName;
import com.atlassian.jira.web.filters.steps.FilterCallContext;
import com.atlassian.jira.web.filters.steps.FilterStep;
import com.atlassian.jira.web.filters.steps.NullFilterStep;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static com.atlassian.jira.component.ComponentAccessor.getComponentSafely;

/**
 * Instruments at a top level a single JIRA HTTP request. This class can occasionally be called during periods
 * when the DI container is empty, so counters etc need to be tolerant of this fact.
 *
 * @since v4.4
 */
public class InstrumentationStep implements FilterStep {
    private static final String X_B3_TRACE_ID = "X-B3-TraceId";
    private static final String X_B3_SPAN_ID = "X-B3-SpanId";
    private static final String X_B3_PARENT_SPAN_ID = "X-B3-ParentSpanId";

    private final InstrumentRegistry registry;
    private final Gauge concurrentRequestsGauge;

    private OpTimer requestTimer;

    public static FilterStep create() {
        // This filter step no-ops itself if Pico isn't ready
        return getComponentSafely(InstrumentRegistry.class)
                .<FilterStep>map(InstrumentationStep::new)
                .orElseGet(NullFilterStep::new);
    }

    private InstrumentationStep(InstrumentRegistry registry) {
        this.registry = registry;
        this.concurrentRequestsGauge = registry.pullGauge(InstrumentationName.CONCURRENT_REQUESTS.getInstrumentName());
    }

    @Override
    public FilterCallContext beforeDoFilter(FilterCallContext callContext) {
        requestTimer = registry.pullTimer(InstrumentationName.WEB_REQUESTS.getInstrumentName());
        concurrentRequestsGauge.incrementAndGet();

        // Add the GUID if there is one there... Use Zipkin header just in case... If it is null we will grab a GUID.
        final String traceId = callContext.getHttpServletRequest().getHeader(X_B3_TRACE_ID);
        final String spanId = callContext.getHttpServletRequest().getHeader(X_B3_SPAN_ID);
        final String parentSpanId = callContext.getHttpServletRequest().getHeader(X_B3_PARENT_SPAN_ID);
        final String queryString = callContext.getHttpServletRequest().getQueryString();
        DefaultInstrumentationListenerManager.startContext(callContext.getHttpServletRequest().getRequestURI(), traceId,
                spanId, parentSpanId, queryString, System.nanoTime());
        return callContext;
    }

    @Override
    public FilterCallContext finallyAfterDoFilter(FilterCallContext callContext) {
        concurrentRequestsGauge.decrementAndGet();
        OpSnapshot time = requestTimer.end();
        DefaultInstrumentationListenerManager.endContext(Optional.of(time.getElapsedTotalTime(TimeUnit.NANOSECONDS)));

        /*
         * Web request that generate markup end up calling this and hence get access to the thread local
         * version of the instrument values.  However other requests dont go through AccessLogImprinter and
         * hence we have to make double sure we clear that thread local storage otherwise we double count
         * on the next request
         */
        getComponentSafely(OpTimerFactory.class).ifPresent(Instrumentation::snapshotThreadLocalOperationsAndClear);
        return callContext;
    }
}
