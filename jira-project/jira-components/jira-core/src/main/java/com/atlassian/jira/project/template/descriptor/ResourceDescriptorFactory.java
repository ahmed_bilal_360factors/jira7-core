package com.atlassian.jira.project.template.descriptor;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.elements.ResourceDescriptor;
import org.apache.commons.io.IOUtils;
import org.dom4j.Element;
import org.dom4j.dom.DOMElement;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.Optional;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class ResourceDescriptorFactory {
    /**
     * Creates a ResourceDescriptor for the file at {@code location} within the given Plugin.
     *
     * @param plugin       the Plugin to load the resource from
     * @param resourceName the resource name with the plugin
     * @param location     the resource location within the plugin
     * @return a ResourceDescriptor
     * @throws FileNotFoundException if there is no file at {@code location}
     */
    public ResourceDescriptor createResource(Plugin plugin, String resourceName, String location, Optional<String> contentType)
            throws FileNotFoundException {
        InputStream inputStream = plugin.getResourceAsStream(location);
        if (inputStream == null) {
            throw new FileNotFoundException(location);
        }

        try {
            inputStream = new BufferedInputStream(inputStream);
            // add a <param> for the content type
            String contentTypeString = contentType.orElse(guessContentType(inputStream));
            return makeResourceDescriptorNode(resourceName, location, contentTypeString);
        } catch (IOException e) {
            throw new RuntimeException("Error trying to determine content type of " + inputStream, e);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }

    /**
     * Creates {@link ResourceDescriptor} based on given input. Does not any perform data validation.
     * @param resourceName user-friendly resource name
     * @param location resource location
     * @param contentType resource content type
     * @return
     */
    public ResourceDescriptor makeResourceDescriptorNode(String resourceName, String location, String contentType) {
        // <resource>
        Element resource = new DOMElement("resource")
                .addAttribute("name", resourceName)
                .addAttribute("type", "download")
                .addAttribute("location", location);

        if (isNotBlank(contentType)) {
            resource.addElement("param")
                    .addAttribute("name", "content-type")
                    .addAttribute("value", contentType);
        }

        return new ResourceDescriptor(resource);
    }

    private String guessContentType(final InputStream inputStream) throws IOException {
        return URLConnection.guessContentTypeFromStream(inputStream) + "; charset=binary";
    }
}
