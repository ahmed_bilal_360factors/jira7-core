package com.atlassian.jira.ofbiz;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.ofbiz.core.entity.jdbc.interceptors.connection.ConnectionPoolState;
import org.ofbiz.core.entity.jdbc.interceptors.connection.SQLConnectionInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.function.Consumer;

/**
 * A {@link org.ofbiz.core.entity.jdbc.interceptors.SQLInterceptor} that can chain together multiple
 * SQLInterceptors.
 * <p>
 * It will call them in an enveloping order, and any runtime exceptions thrown by one of the chained
 * interceptors will be caught and logged but otherwise ignored.
 * </p>
 * <p>
 * For example, if we add 'one' and 'two', then it will call:
 * </p>
 * <pre>
 * one.beforeExecution(...)
 * two.beforeExecution(...)
 *
 * two.afterSuccessfulExecution(...)
 * one.afterSuccessfulExecution(...)
 * </pre>
 *
 * @since v4.0
 */
public class ChainedSQLInterceptor implements SQLConnectionInterceptor {
    private static final Logger LOG = LoggerFactory.getLogger(ChainedSQLInterceptor.class);

    private final List<SQLConnectionInterceptor> forward;
    private final List<SQLConnectionInterceptor> reverse;

    public static class Builder {
        private ImmutableList.Builder<SQLConnectionInterceptor> interceptorsList = ImmutableList.builder();

        public Builder add(SQLConnectionInterceptor sqlInterceptor) {
            interceptorsList.add(sqlInterceptor);
            return this;
        }

        public ChainedSQLInterceptor build() {
            return new ChainedSQLInterceptor(interceptorsList.build());
        }
    }

    private ChainedSQLInterceptor(final List<SQLConnectionInterceptor> interceptorsList) {
        this.forward = interceptorsList;
        this.reverse = Lists.reverse(interceptorsList);
    }

    @Override
    public void onConnectionTaken(Connection connection, ConnectionPoolState connectionPoolState) {
        forEach(forward, interceptor ->
                interceptor.onConnectionTaken(connection, connectionPoolState));
    }

    @Override
    public void onConnectionReplaced(Connection connection, ConnectionPoolState connectionPoolState) {
        forEach(reverse, interceptor ->
                interceptor.onConnectionReplaced(connection, connectionPoolState));
    }

    public void beforeExecution(final String sqlString, final List<String> parameterValues, final Statement statement) {
        forEach(forward, interceptor ->
                interceptor.beforeExecution(sqlString, parameterValues, statement));
    }

    public void afterSuccessfulExecution(final String sqlString, final List<String> parameterValues,
                                         final Statement statement, final ResultSet resultSet, final int rowsUpdated) {
        forEach(reverse, interceptor ->
                interceptor.afterSuccessfulExecution(sqlString, parameterValues, statement, resultSet, rowsUpdated));
    }

    public void onException(final String sqlString, final List<String> parameterValues, final Statement statement,
                            final SQLException sqlException) {
        forEach(reverse, interceptor ->
                interceptor.onException(sqlString, parameterValues, statement, sqlException));
    }

    private static void forEach(List<SQLConnectionInterceptor> interceptors, Consumer<SQLConnectionInterceptor> callback) {
        for (SQLConnectionInterceptor interceptor : interceptors) {
            try {
                callback.accept(interceptor);
            } catch (RuntimeException e) {
                LOG.error("Unexpected exception in {}", interceptor, e);
            }
        }
    }
}
