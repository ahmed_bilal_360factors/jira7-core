package com.atlassian.jira.upgrade;

import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.license.LicenseJohnsonEventRaiser;
import com.atlassian.jira.license.MultiLicenseStore;
import com.atlassian.jira.startup.JiraLauncher;
import com.atlassian.jira.startup.JiraStartupChecklist;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.jira.web.ServletContextProvider;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;

/**
 * Tests if an upgrade is necessary, and performs it.  Note that upgrades are performed on the same thread
 */
public class UpgradeLauncher implements JiraLauncher {
    private static final Logger log = LoggerFactory.getLogger(UpgradeLauncher.class);
    private static final String NO_UPGRADE_MESSAGE = "No upgrade is being performed due to detected inconsistencies.";
    private static final String LS = JiraSystemProperties.getInstance().getProperty("line.separator");

    private final JohnsonProvider johnsonProvider;

    public UpgradeLauncher(final JohnsonProvider johnsonProvider) {
        this.johnsonProvider = johnsonProvider;
    }

    /**
     * The upgrade runner loads the Upgrade Manager, which then performs any necessary upgrades.
     */
    @Override
    public void start() {
        try {
            ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();
            if ("true".equals(applicationProperties.getString(APKeys.JIRA_SETUP))) {
                checkIfUpgradeNeeded(ServletContextProvider.getServletContext(), johnsonProvider);
            }
        } catch (RuntimeException rte) {
            log.error("A RuntimeException occurred during UpgradeLauncher servlet context initialisation - " + rte.getMessage() + ".", rte);
            throw rte;
        } catch (Error error) {
            log.error("An Error occurred during UpgradeLauncher servlet context initialisation - " + error.getMessage() + ".", error);
            throw error;
        }
    }

    @Override
    public void stop() {
        // do nothing
    }

    /**
     * This will invoke the {@link com.atlassian.jira.upgrade.UpgradeService} to see if an upgrade is needed.
     * <p>
     * This is run at JIRA startup time and can be invoked later if you need to restart JIRA.
     *
     * @param servletContext this is need to put up Johnson events as the upgrade happens and also if the upgrade fails
     */
    public static void checkIfUpgradeNeeded(final ServletContext servletContext, final JohnsonProvider johnsonProvider) {
        ensureLicenseNotCorrupted();
        if (JiraStartupChecklist.startupOK()) {
            final JohnsonEventContainer eventContainer = johnsonProvider.getContainer();

            /**
             * This will check that the database containers a valid 2.0 style license and also that
             * the license is valid for this build and .  A Johnson event will be raised if not all kosher.
             */
            if (checkLicenseIsValid()) {
                //Add a warning that an upgrade is in progress
                final Event upgradingEvent = new Event(EventType.get("upgrade"), "JIRA is currently being upgraded",
                        EventLevel.get(EventLevel.WARNING));

                eventContainer.addEvent(upgradingEvent);

                try {
                    final UpgradeService upgradeService = ComponentAccessor.getComponent(UpgradeService.class);
                    final UpgradeResult result = upgradeService.runUpgrades();
                    addEventsForErrors(eventContainer, result);
                } catch (Exception upgradeException) {
                    log.error("An error occurred whilst trying to upgrade JIRA.", upgradeException);

                    final Event errorEvent = new Event(EventType.get("upgrade"), "An error occurred performing JIRA upgrade", upgradeException.getMessage(),
                            EventLevel.get(EventLevel.ERROR));
                    eventContainer.addEvent(errorEvent);
                } finally {
                    // upgrade completed
                    eventContainer.removeEvent(upgradingEvent);
                }
            } else {
                log.error(constructErrorMessage(eventContainer));
            }
        } else {
            log.error("Skipping, JIRA is locked.");
        }
    }

    /**
     * <p>
     * A corrupted license results in the LicenseService throwing an exception. We want to pre-emptively check for that
     * case and remove the corrupted license from the store with an appropriate log message.
     * <p>
     * This will allow JIRA to start up and the instance admin to fix the problem by providing a valid license.
     */
    private static void ensureLicenseNotCorrupted() {
        try {
            getJiraLicenseService().getLicenses();
        } catch (Exception e) {
            log.error(String.format("This instance contains one or more corrupt licenses. "
                    + "All licenses will be removed and licenses will need to be re-entered."));
            getLicenseStore().clear();
        }
    }


    private static boolean checkLicenseIsValid() {
        boolean invalid = false;
        final JiraLicenseService licenseService = getJiraLicenseService();

        if (licenseService.isLicenseSet()) {
            final LicenseJohnsonEventRaiser licenseJohnsonEventRaiser = getLicenseJohnsonEventRaiser();

            // check that if the current build is not newer than the license by more than 366 days
            invalid = licenseJohnsonEventRaiser.checkLicenseIsTooOldForBuild();
        }
        return !invalid;
    }

    private static void addEventsForErrors(final JohnsonEventContainer cont, final UpgradeResult result) {
        if (cont != null && result != null && !result.successful()) {
            for (final String exception : result.getErrors()) {
                final Event errorEvent = new Event(EventType.get("upgrade"), "An error occurred performing JIRA upgrade", exception,
                        EventLevel.get(EventLevel.ERROR));
                cont.addEvent(errorEvent);
            }
        }
    }

    private static String constructErrorMessage(final JohnsonEventContainer cont) {
        final StringBuilder errMsg = new StringBuilder(NO_UPGRADE_MESSAGE).append(" ");

        // use the Johnson event itself as the log message
        for (final Object element : cont.getEvents()) {
            final Event errEvent = (Event) element;
            errMsg.append(errEvent.getDesc()).append(LS);
        }

        return errMsg.toString();
    }


    private static JiraLicenseService getJiraLicenseService() {
        return ComponentAccessor.getComponent(JiraLicenseService.class);
    }

    private static MultiLicenseStore getLicenseStore() {
        return ComponentAccessor.getComponent(MultiLicenseStore.class);
    }

    private static LicenseJohnsonEventRaiser getLicenseJohnsonEventRaiser() {
        return ComponentAccessor.getComponent(LicenseJohnsonEventRaiser.class);
    }
}
