package com.atlassian.jira.security.roles;

import com.atlassian.jira.model.querydsl.QProjectRole;
import com.querydsl.core.Tuple;

/**
 * Creates {@link com.atlassian.jira.security.roles.ProjectRole} from QueryDSL tuple.
 *
 * @since 7.0
 */
public class QueryDSLProjectRoleFactory {
    public ProjectRole createProjectRole(Tuple tuple) {
        return new ProjectRoleImpl(tuple.get(QProjectRole.PROJECT_ROLE.id),
                tuple.get(QProjectRole.PROJECT_ROLE.name),
                tuple.get(QProjectRole.PROJECT_ROLE.description));
    }
}
