package com.atlassian.jira.web.bean;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.UserQuery;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.MatchMode;
import com.atlassian.crowd.search.query.entity.restriction.TermRestriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.crowd.embedded.ofbiz.UnsortedNullRestriction;
import com.atlassian.jira.issue.comparator.UserNameComparator;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.LazyLoadingApplicationUser;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.function.Predicate;

import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Sets.intersection;

public class UserBrowserFilter extends PagerFilter<ApplicationUser> {
    public static final String ANY_APPLICATION_ACCESS_FILTER = "-1";
    public static final String NO_APPLICATION_ACCESS_FILTER = "-2";

    /**
     * Free text search over: username, full name and email address
     */
    public String userSearchFilter = null;
    private Option<Boolean> activeFilter = Option.none();
    public String group = null;
    private final Locale userLocale;
    private final ApplicationRoleManager roleManager;
    private AccessFilterType applicationFilterType = AccessFilterType.IGNORE;
    private ApplicationKey applicationFilter = null;

    private enum AccessFilterType {
        SPECIAL_ANY, SPECIAL_NONE, REGULAR, IGNORE
    }

    public UserBrowserFilter(final Locale userLocale, final ApplicationRoleManager roleManager) {
        this.userLocale = userLocale;
        this.roleManager = roleManager;
    }

    public String getActiveFilter() {
        return activeFilter.map(Object::toString).getOrElse("");
    }

    public void setActiveFilter(String activeFilter) {
        this.activeFilter = Strings.isNullOrEmpty(activeFilter) ? Option.none()
                : Option.option(Boolean.valueOf(activeFilter));
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(final String group) {
        this.group = FilterUtils.verifyString(group);
    }

    public String getUserSearchFilter() {
        return userSearchFilter;
    }

    public void setUserSearchFilter(final String userSearchFilter) {
        this.userSearchFilter = userSearchFilter;
    }

    public void setApplicationFilter(final String applicationFilter) {
        applicationFilterType = AccessFilterType.IGNORE;
        this.applicationFilter = null;
        switch (applicationFilter) {
            case ANY_APPLICATION_ACCESS_FILTER:
                applicationFilterType = AccessFilterType.SPECIAL_ANY;
                break;
            case NO_APPLICATION_ACCESS_FILTER:
                applicationFilterType = AccessFilterType.SPECIAL_NONE;
                break;
            default:
                try {
                    final Option<ApplicationRole> role = roleManager.getRole(ApplicationKey.valueOf(applicationFilter));
                    if (role.isDefined()) {
                        applicationFilterType = AccessFilterType.REGULAR;
                        this.applicationFilter = role.get().getKey();
                    }
                } catch (IllegalArgumentException ignored) {
                }
                break;
        }
    }

    public String getApplicationFilter() {
        switch (applicationFilterType) {
            case SPECIAL_ANY:
                return ANY_APPLICATION_ACCESS_FILTER;
            case SPECIAL_NONE:
                return NO_APPLICATION_ACCESS_FILTER;
            case REGULAR:
                return applicationFilter.value();
            case IGNORE:
            default:
                return null;
        }
    }

    public List<ApplicationUser> getFilteredUsers() throws Exception {
        // get list of filtered users
        Iterable<ApplicationUser> users = getUsersFilteredByGroupAndCriteria();

        // Apply access filter if needed (this might be slow)
        if (applicationFilterType != AccessFilterType.IGNORE) {
            users = Iterables.filter(users, this::includeBasedOnApplicationAccess);
        }

        // JRA-15309 - sort the users based on a Locale sensitive comparator
        final List<ApplicationUser> sortedUsers = Lists.newArrayList(users);
        Collections.sort(sortedUsers, new UserNameComparator(userLocale));
        return sortedUsers;
    }

    private Iterable<ApplicationUser> getUsersFilteredByGroupAndCriteria() {
        final CrowdService crowdService = ComponentAccessor.getComponentOfType(CrowdService.class);
        final Iterable<String> userNames = getUserNamesFilteredByCriteria();

        if (group != null) {
            final Set<String> userNamesSet = userNames instanceof Set ? (Set<String>) userNames : ImmutableSet.copyOf(userNames);
            return makeLazyApplicationUsersFromNames(
                    intersection(userNamesSet, ImmutableSet.copyOf(getUsersFilteredByGroup())),
                    crowdService);
        }
        return makeLazyApplicationUsersFromNames(userNames, crowdService);
    }

    private Iterable<String> getUsersFilteredByGroup() {
        final CrowdService crowdService = ComponentAccessor.getComponentOfType(CrowdService.class);
        final MembershipQuery<String> membershipQuery =
                QueryBuilder.queryFor(String.class, EntityDescriptor.user())
                        .childrenOf(EntityDescriptor.group())
                        .withName(group)
                        .returningAtMost(EntityQuery.ALL_RESULTS);
        return crowdService.search(membershipQuery);
    }

    @VisibleForTesting
    Iterable<String> getUserNamesFilteredByCriteria() {
        final CrowdService crowdService = ComponentAccessor.getComponentOfType(CrowdService.class);

        final UserQuery<String> query = new UserQuery<>(String.class, makeCrowdRestriction(userSearchFilter), 0, EntityQuery.ALL_RESULTS);
        final Iterable<String> userNames = crowdService.search(query);

        if (activeFilter.isDefined()) {
            final Predicate<User> usersMatchingActivityFilter = user -> user.isActive() == activeFilter.get();
            final List<String> userNamesList = userNames instanceof List ? (List<String>) userNames : ImmutableList.copyOf(userNames);
            return () -> userNamesList.stream().map(crowdService::getUser)
                    .filter(usersMatchingActivityFilter)
                    .map(User::getName)
                    .iterator();
        }
        return userNames;
    }

    private Iterable<ApplicationUser> makeLazyApplicationUsersFromNames(Iterable<String> userNames, CrowdService crowdService) {
        return transform(userNames, userName -> new LazyLoadingApplicationUser(userName, crowdService));
    }

    private Option<SearchRestriction> makeUserSearchRestrictions(String searchFilter) {
        return Strings.isNullOrEmpty(searchFilter) ? Option.none() :
                Option.option(new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR,
                        ImmutableList.of(new TermRestriction<>(UserTermKeys.USERNAME, MatchMode.CONTAINS, searchFilter),
                                new TermRestriction<>(UserTermKeys.DISPLAY_NAME, MatchMode.CONTAINS, searchFilter),
                                new TermRestriction<>(UserTermKeys.EMAIL, MatchMode.CONTAINS, searchFilter))));
    }

    private SearchRestriction makeCrowdRestriction(String userSearchFilter) {
        final Option<SearchRestriction> userSearchRestrictions = makeUserSearchRestrictions(userSearchFilter);
        return userSearchRestrictions.getOrElse(UnsortedNullRestriction.INSTANCE);
    }

    private boolean includeBasedOnApplicationAccess(final ApplicationUser user) {
        switch (applicationFilterType) {
            case REGULAR:
                return roleManager.userOccupiesRole(user, applicationFilter);
            case SPECIAL_NONE:
                return !roleManager.hasAnyRole(user);
            case SPECIAL_ANY:
                return roleManager.hasAnyRole(user);
            case IGNORE:
            default:
                return true;
        }
    }
}
