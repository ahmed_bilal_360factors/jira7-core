package com.atlassian.jira.bc.dataimport;

import com.atlassian.jira.upgrade.UpgradeConstraints;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.BuildUtilsInfoImpl;

import java.util.Collection;
import java.util.Date;
import java.util.Locale;

/**
 * Temporary class used during transition period (will be deleted afterwards). In dark ages mode we do not want to
 * allow importing data that was exported from renaissance. As {@link DefaultDataImportService} uses
 * {@link BuildUtilsInfoImpl} class to determine if data can be imported, this class returns
 * {@link com.atlassian.jira.util.BuildUtils#CLOUD_DARK_AGES_BUILD_NUMBER} as build number which would prevent import
 * from renaissance happening (as downgrade from {@link com.atlassian.jira.upgrade.tasks.UpgradeTask_Build70100} is
 * not possible).
 * <p>
 * NOTE: this class will change default JIRA behaviour only if roles are not enabled (dark ages) and instance is running
 * in Cloud. In any other case it will work as before.
 *
 * @since 7.0
 */
public class ConstrainedBuildUtilsInfo implements BuildUtilsInfo {
    private final UpgradeConstraints constraints;
    private final BuildUtilsInfo buildUtilsInfo;

    public ConstrainedBuildUtilsInfo(final UpgradeConstraints constraints, final BuildUtilsInfo buildUtilsInfo) {
        this.constraints = constraints;
        this.buildUtilsInfo = buildUtilsInfo;
    }

    @Override
    public int getApplicationBuildNumber() {
        return constraints.getTargetDatabaseBuildNumber();
    }

    @Override
    public String getCurrentBuildNumber() {
        return Integer.toString(constraints.getTargetDatabaseBuildNumber());
    }

    @Override
    public int getDatabaseBuildNumber() {
        return buildUtilsInfo.getDatabaseBuildNumber();
    }

    @Override
    public String getMinimumUpgradableBuildNumber() {
        return buildUtilsInfo.getMinimumUpgradableBuildNumber();
    }

    @Override
    public Date getCurrentBuildDate() {
        return buildUtilsInfo.getCurrentBuildDate();
    }

    @Override
    public String getBuildPartnerName() {
        return buildUtilsInfo.getBuildPartnerName();
    }

    @Override
    public String getBuildInformation() {
        return buildUtilsInfo.getBuildInformation();
    }

    @Override
    @Deprecated
    public String getSvnRevision() {
        return buildUtilsInfo.getSvnRevision();
    }

    @Override
    public String getCommitId() {
        return buildUtilsInfo.getCommitId();
    }

    @Override
    public String getMinimumUpgradableVersion() {
        return buildUtilsInfo.getMinimumUpgradableVersion();
    }

    @Override
    public Collection<Locale> getUnavailableLocales() {
        return buildUtilsInfo.getUnavailableLocales();
    }

    @Override
    public String getSalVersion() {
        return buildUtilsInfo.getSalVersion();
    }

    @Override
    public String getApplinksVersion() {
        return buildUtilsInfo.getApplinksVersion();
    }

    @Override
    public String getLuceneVersion() {
        return buildUtilsInfo.getLuceneVersion();
    }

    @Override
    public String getGuavaOsgiVersion() {
        return buildUtilsInfo.getGuavaOsgiVersion();
    }

    @Override
    public String getBuildProperty(final String key) {
        return buildUtilsInfo.getBuildProperty(key);
    }

    @Override
    public boolean isBeta() {
        return buildUtilsInfo.isBeta();
    }

    @Override
    public boolean isRc() {
        return buildUtilsInfo.isRc();
    }

    @Override
    public boolean isSnapshot() {
        return buildUtilsInfo.isSnapshot();
    }

    @Override
    public boolean isMilestone() {
        return buildUtilsInfo.isMilestone();
    }

    @Override
    public String getVersion() {
        return buildUtilsInfo.getVersion();
    }

    @Override
    public String getDocVersion() {
        return buildUtilsInfo.getDocVersion();
    }

    @Override
    public int[] getVersionNumbers() {
        return buildUtilsInfo.getVersionNumbers();
    }
}
