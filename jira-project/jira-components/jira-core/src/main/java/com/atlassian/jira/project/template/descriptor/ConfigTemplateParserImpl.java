package com.atlassian.jira.project.template.descriptor;

import com.atlassian.jira.project.template.hook.ConfigTemplate;
import com.atlassian.jira.project.template.hook.ConfigTemplateImpl;
import com.atlassian.plugin.Plugin;
import org.codehaus.jackson.map.InjectableValues;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import static org.apache.commons.io.IOUtils.closeQuietly;

public class ConfigTemplateParserImpl implements ConfigTemplateParser {
    public ConfigTemplate parse(String configFile, Plugin plugin) {
        URL configUrl = plugin.getResource(configFile);
        if (configUrl == null) {
            throw new IllegalArgumentException("No configuration file: " + configFile);
        }

        InputStream inputStream = null;
        try {
            inputStream = configUrl.openStream();
            return parseInput(inputStream, plugin);
        } catch (IOException e) {
            throw new IllegalArgumentException("Error while reading configuration file: " + configUrl, e);
        } finally {
            closeQuietly(inputStream);
        }
    }

    private static ConfigTemplate parseInput(InputStream inputStream, Plugin plugin) {
        ObjectMapper mapper = new ObjectMapper();
        InjectableValues inject = new InjectableValues.Std()
                .addValue("plugin", plugin);

        try {
            return mapper.reader(ConfigTemplateImpl.class).withInjectableValues(inject).readValue(inputStream);
        } catch (IOException e) {
            throw new IllegalArgumentException("Error while parsing configuration file.", e);
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("Invalid configuration file.", e);
        }
    }
}
