package com.atlassian.jira.startup;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.cache.JiraVCacheRequestContextSupplier;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.component.ComponentAccessorWorker;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.instrumentation.jdbc.InstantLogJdbcStatsCollector;
import com.atlassian.jira.studio.startup.StudioStartupHooks;
import com.atlassian.jira.studio.startup.StudioStartupHooksLocator;
import com.atlassian.jira.util.MemoryPools;
import com.atlassian.jira.util.johnson.DefaultJohnsonProvider;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.jira.web.startup.StartupPageSupport;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static com.atlassian.jira.cluster.ClusterNodePropertiesImpl.JIRA_CLUSTER_CONFIG_PROPERTIES;
import static com.atlassian.jira.util.concurrent.ThreadFactories.namedThreadFactory;
import static com.google.common.collect.Lists.newArrayList;
import static java.lang.Integer.MAX_VALUE;
import static java.lang.String.format;
import static java.util.concurrent.Executors.newSingleThreadScheduledExecutor;
import static org.apache.commons.lang3.StringUtils.trim;

/**
 * This class is the entry point for JIRA.  It takes care of initialising the application through the use of various
 * {@link com.atlassian.jira.startup.JiraLauncher}s.  Additionally this class will also launch a deadlock detector
 * thread to check for deadlocks during startup.
 *
 * @since v4.3
 */
public class LauncherContextListener implements ServletContextListener {
    private static final Logger log = LoggerFactory.getLogger(LauncherContextListener.class);

    private static final String PROPERTY_LOG_JDBC_ON_STARTUP = "jira.jdbc.startup.logging";
    private static final String JDBC_STARTUP_LOGGER_NAME = "jdbc.startup.log";
    private static final String SYNCHRONOUS = LauncherContextListener.class.getName() + ".SYNCHRONOUS";
    public static final String STARTUP_UNEXPECTED = "startup-unexpected";
    private static final String LOG4J = "log4j.properties";
    private static final int DEADLOCK_DETECTION_PERIOD = 5;

    private final ScheduledExecutorService deadlockDetectionService =
            newSingleThreadScheduledExecutor(namedThreadFactory("DeadlockDetection"));
    private final StudioStartupHooks startupHooks = StudioStartupHooksLocator.getStudioStartupHooks();
    private final JohnsonProvider johnsonProvider;

    private volatile Thread bootstrap;
    private volatile JiraLauncher launcher;
    private InstantLogJdbcStatsCollector jdbcStatsCollector;

    public LauncherContextListener() {
        this.johnsonProvider = new DefaultJohnsonProvider();
    }

    public LauncherContextListener(final JohnsonProvider johnsonProvider) {
        this.johnsonProvider = johnsonProvider;
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        if (bootstrap != null || launcher != null) {
            throw new IllegalStateException("JIRA cannot be initialized twice!");
        }
        try {
            //JVC-205 - Line below will be removed once JIRA will not refer to tenanted data during startup
            //We cannot just call JiraVCacheInitialisationUtils.initVCache as pico is not available yet and this call will be NOOP
            JiraVCacheRequestContextSupplier.initStaticContext("staticStartupContext");
            initStartupJdbcLogging();

            initFastStuff();
            initSlowStuffInBackground();
        } catch (Exception e) {
            fatalJohnson(e);
        } catch (Error e) {
            log.error("Unable to start JIRA due to Java Error", e);
            throw e;
        } finally {
            JiraVCacheRequestContextSupplier.clearStaticContext();
            finishStartupJdbcLogging();
        }
    }

    private void initStartupJdbcLogging() {
        final boolean shouldLog = JiraSystemProperties.getInstance().getBoolean(PROPERTY_LOG_JDBC_ON_STARTUP);
        if (shouldLog) {
            try {
                Class.forName("com.atlassian.instrumentation.driver.Instrumentation");
                final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(JDBC_STARTUP_LOGGER_NAME);
                jdbcStatsCollector = new InstantLogJdbcStatsCollector(logger);
                jdbcStatsCollector.register();
            } catch (ClassNotFoundException e) {
                log.debug("No metrics driver present - startup jdbc logging will be disabled");
                //If no metrics driver is present it is fine.
            }
        }
    }

    private void finishStartupJdbcLogging() {
        if (jdbcStatsCollector != null) {
            jdbcStatsCollector.unregister();
            jdbcStatsCollector = null;
        }
    }

    private void initFastStuff() {
        configureLog4j(startupHooks);
        log.debug("Launching JIRA");
        startupHooks.beforeJiraStart();
        ComponentAccessor.initialiseWorker(new ComponentAccessorWorker());
    }

    private void initSlowStuffInBackground() {
        if (isSynchronousStartup()) {
            initSlowStuff();
        } else {
            final Thread bootstrap = new Thread(this::initSlowStuff, "JIRA-Bootstrap");
            this.bootstrap = bootstrap;
            bootstrap.start();
        }
    }

    private void initSlowStuff() {
        log.debug("Startup deadlock detector launched...");
        final ScheduledFuture<?> deadLockDetector = deadlockDetectionService.scheduleAtFixedRate(
                new DeadlockDetector(), 0, DEADLOCK_DETECTION_PERIOD, TimeUnit.SECONDS);

        try {
            launcher = new DefaultJiraLauncher(johnsonProvider);
            launcher.start();
            startupHooks.afterJiraStart();
        } catch (Exception e) {
            fatalJohnson(e);
        } catch (Error e) {
            fatalJohnson(e);
            throw e;
        } finally {
            deadLockDetector.cancel(false);
            deadlockDetectionService.shutdown();
            log.debug("Startup deadlock detector finished.");

            // initDone will do one last check, then set the flag that opens the door for normal HTTP traffic
            initDone();
        }

        // Log memory usage as the very last thing done during startup because we want to ensure accuracy.
        log.info("Memory Usage:\n" + MemoryPools.memoryPoolsDump(false));
    }

    private void fatalJohnson(final Throwable e) {
        log.error("Unable to start JIRA.", e);
        johnsonProvider.getContainer().addEvent(new Event(
                EventType.get(STARTUP_UNEXPECTED), "Unexpected exception during JIRA startup. This JIRA instance "
                + "will not be able to recover. Please check the logs for details",
                EventLevel.get("fatal")));
    }

    private void initDone() {
        // Failsafe.  If we get here, we aren't started, and there are no johnson events, then make one.
        if (!ComponentManager.getInstance().getState().isStarted() && !johnsonProvider.getContainer().hasEvents()) {
            fatalJohnson(new IllegalStateException("Abnormal system startup detected"));
        }

        // Whether we are johnsoned or not, it's time to stop sending people to the startup page.
        // If we johnsoned, then they'll get errors.jsp; otherwise, this is what unlocks normal HTTP traffic.
        StartupPageSupport.setLaunched(true);
        bootstrap = null;
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        interruptBootstrap();
        if (launcher == null) {
            throw new IllegalStateException("Context destroyed without being initialized first. JIRA launcher is confused.");
        } else {
            launcher.stop();
            launcher = null;
        }
    }

    private void interruptBootstrap() {
        final Thread bootstrap = this.bootstrap;
        if (bootstrap == null) {
            return;
        }

        this.bootstrap = null;
        bootstrap.interrupt();
        try {
            bootstrap.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private void configureLog4j(StudioStartupHooks startupHooks) {
        Properties properties = new Properties();
        InputStream resource = this.getClass().getClassLoader().getResourceAsStream(LOG4J);
        if (resource != null) {
            try {
                properties.load(resource);
            } catch (IOException e) {
                log.warn("Unable read current log4j configuration. Assuming blank configuration.", e);
            } finally {
                IOUtils.closeQuietly(resource);
            }
        } else {
            log.warn("Unable to find '" + LOG4J + "' on class path.");
        }

        Properties newConfig = startupHooks.getLog4jConfiguration(properties);
        if (newConfig != null) {
            PropertyConfigurator.configure(newConfig);
        }
    }

    private static boolean isClustered() {
        return JiraHomeStartupCheck.getInstance().isOk() &&
                new File(JiraHomeStartupCheck.getInstance().getJiraHomeDirectory(), JIRA_CLUSTER_CONFIG_PROPERTIES).exists();
    }

    private static boolean isSynchronousStartup() {
        return JiraSystemProperties.getInstance().getBoolean(SYNCHRONOUS) || isClustered();
    }

    private static class DeadlockDetector implements Runnable {
        private static final String DEAD_LOCK_DETECTOR_KB_URL =
                "https://confluence.atlassian.com/display/JIRAKB/Deadlock+detected+on+startup+error+in+logfile";

        private final ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();

        @Override
        public void run() {
            long[] threadIds = threadMXBean.findDeadlockedThreads();
            if (threadIds != null) {
                final List<String> threadInfoStrings = newArrayList();
                for (ThreadInfo threadInfo : threadMXBean.getThreadInfo(threadIds, 0)) {
                    threadInfoStrings.add(trim(threadInfo.toString()));
                }
                log.error(format("A deadlock has been detected on JIRA startup for the following threads: %s", threadInfoStrings));

                for (ThreadInfo threadInfo : threadMXBean.getThreadInfo(threadIds, MAX_VALUE)) {
                    log.error(generateStackTrace(threadInfo));
                }
                log.error(format("Further troubleshooting information about this issue is available in the KB article at: %s", DEAD_LOCK_DETECTOR_KB_URL));

                throw new DeadlockDetectedException();  //stops this thread from being executed by the ScheduledExecutorService
            }
        }

        private static String generateStackTrace(final ThreadInfo threadInfo) {
            final StringBuilder stackTraceString = new StringBuilder();
            stackTraceString.append(trim(threadInfo.toString())).append(":\n");
            final StackTraceElement[] stackTrace = threadInfo.getStackTrace();
            for (StackTraceElement stackTraceElement : stackTrace) {
                stackTraceString.append('\t').append(stackTraceElement.toString()).append('\n');
            }
            return stackTraceString.toString();
        }
    }

    private final static class DeadlockDetectedException extends RuntimeException {
    }

}
