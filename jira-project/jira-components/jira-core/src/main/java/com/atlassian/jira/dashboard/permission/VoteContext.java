package com.atlassian.jira.dashboard.permission;


import com.atlassian.fugue.Option;
import com.atlassian.gadgets.Vote;
import com.atlassian.jira.dashboard.analytics.GadgetHiddenEvent;
import com.atlassian.plugin.web.Condition;

import static com.atlassian.gadgets.Vote.DENY;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

class VoteContext {
    private final Vote vote;
    private Option<GadgetHiddenEvent.Reason> reason;
    private Option<Condition> conditionClass;

    public VoteContext(final Vote vote) {
        this.reason = Option.none();
        this.conditionClass = Option.none();
        this.vote = vote;
    }

    public VoteContext withReason(final GadgetHiddenEvent.Reason reason) {
        notNull(reason);
        this.reason = Option.some(reason);
        return this;
    }

    public VoteContext withCondition(final Condition conditionClass) {
        notNull(conditionClass);
        this.conditionClass = Option.some(conditionClass);
        return this;
    }

    public Vote getVote() {
        return vote;
    }

    public boolean isDeny() {
        return vote == DENY;
    }

    public Option<GadgetHiddenEvent.Reason> getReason() {
        return reason;
    }

    public Option<Condition> getConditionClass() {
        return conditionClass;
    }
}