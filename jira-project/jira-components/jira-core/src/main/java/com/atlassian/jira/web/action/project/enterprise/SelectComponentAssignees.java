package com.atlassian.jira.web.action.project.enterprise;

import com.atlassian.jira.action.component.ComponentUtils;
import com.atlassian.jira.action.component.SelectComponentAssigneesUtil;
import com.atlassian.jira.bc.EntityNotFoundException;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.project.ComponentAssigneeTypes;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ParameterUtils;
import com.atlassian.jira.web.action.project.AbstractProjectAction;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import webwork.action.ActionContext;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SelectComponentAssignees extends AbstractProjectAction {
    private static final String SECURITY_BREACH = "securitybreach";
    private static final String FIELD_PREFIX = "component_";

    private Long projectId;
    private final SelectComponentAssigneesUtil selectComponentAssigneesUtil;
    private final ProjectComponentManager projectComponentManager;

    private final ProjectManager projectManager;

    public SelectComponentAssignees(final ProjectManager projectManager, final SelectComponentAssigneesUtil selectComponentAssigneesUtil, final ProjectComponentManager projectComponentManager) {
        this.projectManager = projectManager;
        this.selectComponentAssigneesUtil = selectComponentAssigneesUtil;
        this.projectComponentManager = projectComponentManager;
    }

    protected void doValidation() {
        Map<ProjectComponent, Long> components = null;
        try {
            components = getUpdateComponentAssigneeTypes();
        } catch (EntityNotFoundException e) {
            throw new RuntimeException(e);
        }
        if (components != null) {
            selectComponentAssigneesUtil.setComponentAssigneeTypes(components);
            selectComponentAssigneesUtil.setFieldPrefix(getFieldPrefix());
            ErrorCollection errors = selectComponentAssigneesUtil.validate();
            if (errors != null && errors.hasAnyErrors()) {
                addErrorCollection(errors);
            }
        }
    }

    @Override
    public String doDefault() throws Exception {
        if (!selectComponentAssigneesUtil.hasPermission(getProject(), getLoggedInUser())) {
            return SECURITY_BREACH;
        }

        return super.doDefault();
    }

    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        if (!selectComponentAssigneesUtil.hasPermission(getProject(), getLoggedInUser())) {
            return SECURITY_BREACH;
        }

        selectComponentAssigneesUtil.setComponentAssigneeTypes(getUpdateComponentAssigneeTypes());
        addErrorCollection(selectComponentAssigneesUtil.execute(getLoggedInUser()));

        return getRedirect("/plugins/servlet/project-config/" + getProject().getKey() + "/summary");
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Collection<ProjectComponent> getComponents() {
        try {
            return projectComponentManager.findAllForProject(getProject().getId());
        } catch (GenericEntityException e) {
            return Collections.emptyList();
        }
    }

    public String getComponentFieldName(GenericValue component) {
        return getFieldPrefix() + component.getLong("id");
    }

    public long getComponentAssigneeType(GenericValue component) {
        return ComponentUtils.getComponentAssigneeType(component);
    }

    public Map getComponentAssigneeTypes(GenericValue component) {
        return ComponentAssigneeTypes.getAssigneeTypes(component);
    }

    public static String getFieldPrefix() {
        return FIELD_PREFIX;
    }

    public Project getProject() throws GenericEntityException {
        return projectManager.getProjectObj(getProjectId());
    }

    private Map<ProjectComponent, Long> getUpdateComponentAssigneeTypes() throws EntityNotFoundException {
        Map returnedMap = new HashMap();

        Map parameters = ActionContext.getParameters();
        Set keys = parameters.keySet();
        for (final Object key1 : keys) {
            String key = (String) key1;
            if (key.startsWith(getFieldPrefix())) {
                Long componentId = new Long(key.substring(getFieldPrefix().length()));

                ProjectComponent component = projectComponentManager.find(componentId);
                returnedMap.put(component, ParameterUtils.getLongParam(parameters, key));
            }
        }
        return returnedMap;
    }

    public String getAvatarUrl() {
        return ActionContext.getRequest().getContextPath() + "/secure/projectavatar?pid=" + getProjectId() + "&avatarId=" + getAvatarId();
    }
}
