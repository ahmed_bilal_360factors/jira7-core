package com.atlassian.jira.mail;

import com.atlassian.fugue.Option;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.avatar.AvatarTranscoder;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.thumbnail.ThumbnailManager;
import com.atlassian.jira.mail.util.MailAttachment;
import com.atlassian.jira.mail.util.MailAttachments;
import com.atlassian.jira.mail.util.MailAttachmentsManager;
import com.atlassian.jira.mail.util.MailAttachmentsManagerImpl;
import com.atlassian.jira.user.util.UserManager;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import static com.atlassian.fugue.Iterables.findFirst;
import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;
import static com.atlassian.fugue.Option.some;
import static com.google.common.collect.Maps.newHashMap;
import static java.util.Collections.emptyList;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.isNotBlank;
import static org.apache.commons.lang.StringUtils.startsWithIgnoreCase;
import static org.apache.commons.lang.StringUtils.substringAfterLast;
import static org.apache.commons.lang.StringUtils.substringBetween;

@ParametersAreNonnullByDefault
public class MailImageInlinerImpl implements MailImageInliner {

    private static final Logger log = LoggerFactory.getLogger(MailImageInlinerImpl.class);

    private static final String HTML_LINK = "a";
    private static final String HTML_IMG = "img";
    private static final String HTML_SRC = "src";
    private static final String HTML_ID = "id";
    private static final String CONTENT_ID_PREFIX = "cid:";
    private static final String THUMBNAIL_PATH = "/secure/thumbnail";
    private static final String THUMBNAIL_ID = "_thumb";
    private static final String ATTACHMENT_PATH = "/secure/attachment";

    private static final BiFunction<String, String, Option<String>> substringAfterToOptionFunction = (imageSource, find) -> {
        final String s = substringAfterLast(imageSource, find);
        if (isNotBlank(s)) {
            return some(s);
        }
        return none();
    };

    private final AvatarService avatarService;
    private final AvatarTranscoder avatarTranscoder;
    private final UserManager userManager;
    private final AvatarManager avatarManager;
    private final ApplicationProperties applicationProperties;
    private final AttachmentManager attachmentManager;
    private final ThumbnailManager thumbnailManager;

    public MailImageInlinerImpl(final ApplicationProperties applicationProperties,
                                final AvatarService avatarService,
                                final AvatarTranscoder avatarTranscoder,
                                final UserManager userManager,
                                final AvatarManager avatarManager,
                                final AttachmentManager attachmentManager,
                                final ThumbnailManager thumbnailManager) {
        this.applicationProperties = applicationProperties;
        this.avatarService = avatarService;
        this.avatarTranscoder = avatarTranscoder;
        this.userManager = userManager;
        this.avatarManager = avatarManager;
        this.attachmentManager = attachmentManager;
        this.thumbnailManager = thumbnailManager;
    }

    @Nonnull
    @Override
    public InlinedEmailBody inlineImages(String html) {
        return doInlineImages(html, emptyList());
    }

    @Nonnull
    @Override
    public InlinedEmailBody inlineImages(final String html, final Issue issue) {
        final Collection<Attachment> attachments = issue.getAttachments() != null ? issue.getAttachments() : emptyList();
        return doInlineImages(html, attachments);
    }

    private InlinedEmailBody doInlineImages(final String html, final Collection<Attachment> issueAttachments) {
        final MailAttachmentsManager mailAttachmentsManager = new MailAttachmentsManagerImpl(avatarService, avatarTranscoder, userManager, avatarManager, applicationProperties);

        // use the existing MailAttachmentsManager method to inline system images included in html
        final String updatedHtml = mailAttachmentsManager.inlineImages(html);

        final Document document = Jsoup.parse(updatedHtml);

        if (!issueAttachments.isEmpty()) {
            final Predicate<String> isLinkOnInstancePredicate = link -> !link.equals(mailAttachmentsManager.removeBaseUrl(link));
            final Map<String, MailAttachment> mailAttachmentCache = newHashMap();
            for (final Element imageElement : document.body().getElementsByTag(HTML_IMG)) {
                constructMailAttachment(imageElement, issueAttachments, mailAttachmentCache, isLinkOnInstancePredicate)
                        .foreach(mailAttachment -> {
                            final String cid = mailAttachmentsManager.addAttachmentAndReturnCid(mailAttachment);
                            imageElement.attr(HTML_SRC, cid);
                        });
            }
        }

        return new InlinedEmailBody(document.outerHtml(), mailAttachmentsManager.buildAttachmentsBodyParts());
    }

    private Option<MailAttachment> constructMailAttachment(final Element imageElement, final Collection<Attachment> issueAttachments, final Map<String, MailAttachment> mailAttachmentCache, final Predicate<String> isLinkOnInstancePredicate) {
        final String imageSource = imageElement.attr(HTML_SRC);

        // if it is somehow blank or already been inlined, nothing to do here ...
        if (isBlank(imageSource) || startsWithIgnoreCase(imageSource, CONTENT_ID_PREFIX)) {
            return none();
        }

        // save some time, and assure that if the same image is included more than once, it has same body part and cid
        if (mailAttachmentCache.containsKey(imageSource)) {
            final MailAttachment cachedMailAttachment = mailAttachmentCache.get(imageSource);
            if (cachedMailAttachment != null) {
                return some(cachedMailAttachment);
            }
        }

        // if the link is not for our instance, then nothing to inline
        if (!isLinkOnInstancePredicate.test(imageSource)) {
            return none();
        }

        final Long id =
                substringAfterToOptionFunction.apply(imageSource, ATTACHMENT_PATH)
                        .orElse(() -> substringAfterToOptionFunction.apply(imageSource, THUMBNAIL_PATH))
                        .flatMap(p -> option(substringBetween(p, "/")))
                        .flatMap(imId -> {
                            try {
                                return option(Long.parseLong(imId));
                            } catch (NumberFormatException nfe) {
                                log.warn("{} can not be converted to a Long - the image id appears invalid", imId);
                                return none();
                            }
                        }).getOrNull();

        // if no id was found, then it must be an invalid image source or something very new??
        if (id == null) {
            log.debug("No id was able to be extracted from the HTML image source of {}", imageSource);
            return none();
        }

        final Option<Attachment> attachment = findFirst(issueAttachments, att -> id.equals(att.getId()));
        if (attachment.isEmpty()) {
            log.debug("No attachment was found with id {} from HTML image source of {}", id, imageSource);
            return none();
        }

        // is it supposed to be a thumbnail inlined?
        final boolean thumbnail =
                imageSource.contains(THUMBNAIL_PATH) ||
                        option(imageElement.parent())
                                .filter(el -> HTML_LINK.equals(el.tagName()))
                                .flatMap(el -> option(el.attr(HTML_ID)))
                                .exists(thId -> thId.endsWith(THUMBNAIL_ID));

        final MailAttachment mailAttachment = thumbnail
                ? MailAttachments.newMailAttachmentByStreamingFromThumbnailManager(attachment.get(), thumbnailManager)
                : MailAttachments.newMailAttachmentByStreamingFromAttachmentManager(attachment.get(), attachmentManager);

        // add to the cache
        mailAttachmentCache.put(imageSource, mailAttachment);

        return option(mailAttachment);
    }
}
