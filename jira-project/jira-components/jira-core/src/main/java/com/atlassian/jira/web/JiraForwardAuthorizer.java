package com.atlassian.jira.web;

import com.atlassian.web.servlet.api.ForwardAuthorizer;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.Optional;
import java.util.regex.Pattern;

public class JiraForwardAuthorizer implements ForwardAuthorizer {
    private static Pattern dashboardPage = Pattern.compile("[^?#]*/Dashboard\\.jspa\\??.*");

    @Override
    public Optional<Boolean> authorizeForward(HttpServletRequest request, URI targetURI) {
        if (dashboardPage.matcher(targetURI.toASCIIString()).matches()) {
            return Optional.of(true);
        }

        return Optional.empty();
    }
}
