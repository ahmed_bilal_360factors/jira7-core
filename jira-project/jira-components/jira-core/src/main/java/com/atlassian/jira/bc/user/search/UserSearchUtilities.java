package com.atlassian.jira.bc.user.search;

import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.search.query.entity.restriction.MatchMode;
import com.atlassian.crowd.search.query.entity.restriction.Property;
import com.atlassian.crowd.search.query.entity.restriction.TermRestriction;
import com.google.common.collect.ImmutableList;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Predicate;

import static com.atlassian.jira.util.dbc.Assertions.containsNoNulls;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.collect.Sets.newLinkedHashSet;
import static org.apache.commons.lang.StringUtils.join;
import static org.apache.commons.lang.StringUtils.splitPreserveAllTokens;

/**
 * Provide a centralised location for accessing functions and other utilities that help to produce the queries needed to
 * perform user searches in both Crowd and in-memory Java
 *
 * @since v7.0
 */
public class UserSearchUtilities {

    // FYI: Currently _ is broken as a separator as not escaped in crowd (https://jira.atlassian.com/browse/CWD-3415)
    // If used, it will basically do a straight contains of the query
    static final Collection<String> SEPARATORS = ImmutableList.of(" ", "@", ".", "-", "\"", ",", "'", "(");
    public static final String SEPARATORS_STRING = join(SEPARATORS.iterator(), "");

    /**
     * Will assist with creating a Crowd Search Query to search using the provided query and search fields.
     * <p>
     * Returned will be an appropriate collection of {@link SearchRestriction} that can then be combined as required
     * into a query.
     * <p>
     * Each search field will be compared with a starts with.
     *
     * @param query        The inputted query
     * @param searchFields The search fields to be compared against
     * @return A collection of restrictions from which a query can be built
     */
    static Collection<SearchRestriction> userSearchTermRestrictions(final String query, final Iterable<Property<String>> searchFields) {
        notNull("query", query);
        containsNoNulls("searchFields", searchFields);
        final Collection<SearchRestriction> terms = newLinkedHashSet();
        searchFields.forEach(key -> terms.add(new TermRestriction<>(key, MatchMode.STARTS_WITH, query)));
        return terms;

    }

    /**
     * Create a predicate that will assist with searching user fields (username, email, display name) against the given
     * query String.
     * <p>
     * Each user field is tokenised (on spaces, '.', '@' etc) and the tokens are compared to the query String.
     *
     * @param query The inputted query
     * @return A predicate that accepts a user field value and tests if it matches the given query String.
     */
    static Predicate<String> createUserMatchPredicate(final String query) {
        notNull("query", query);

        final String lowercasedQuery = query.toLowerCase();

        if (lowercasedQuery.isEmpty()) {
            return Objects::nonNull;
        } else {
            final char firstLowercaseQueryChar = lowercasedQuery.charAt(0);

            return (userPart) -> {
                String currentPart = userPart;
                while (currentPart != null) {
                    // We try and delay any lower case conversion in this class to as little and as later as possible.
                    if (lowercasedQuery.length() > currentPart.length()) {
                        return false;
                    }

                    // > 90% of searches will typically miss on the first character.
                    if (currentPart.substring(0, 1).toLowerCase().charAt(0) == firstLowercaseQueryChar
                            && lowercasedQuery.equals(currentPart.substring(0, lowercasedQuery.length()).toLowerCase())) {
                        return true;
                    }

                    // FYI: To be consistent with crowd query.
                    //
                    // For the following token mdravis@atlassian.com, a query of atlassian.com would match, but a query
                    // of @atlassian.com will not match.
                    //
                    // If the token was mdravis-@atlassian.com (another separator before the @ separator), then the query
                    // of @atlassian.com would actually match.
                    //
                    // This is expected behaviour
                    final String[] tokens = splitPreserveAllTokens(currentPart, SEPARATORS_STRING, 2);

                    currentPart = tokens.length == 2 ? tokens[1] : null;
                }

                return false;
            };
        }
    }

}
