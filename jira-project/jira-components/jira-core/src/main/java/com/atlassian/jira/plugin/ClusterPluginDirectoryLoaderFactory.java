package com.atlassian.jira.plugin;

import com.atlassian.jira.cluster.ClusterInfo;
import com.atlassian.jira.cluster.zdu.ClusterUpgradePluginLoaderFactory;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.loaders.DirectoryScanner;
import com.atlassian.plugin.loaders.PluginLoader;

import java.util.List;

/**
 * @since v7.3
 */
public class ClusterPluginDirectoryLoaderFactory implements PluginDirectoryLoaderFactory {
    private final BootstrapPluginDirectoryLoaderFactory bootstrapPluginDirectoryLoaderFactory;
    private final PluginPath pathFactory;
    private final ClusterInfo clusterInfo;
    private final ClusterUpgradePluginLoaderFactory clusterUpgradePluginLoaderFactory;

    public ClusterPluginDirectoryLoaderFactory(final PluginPath pathFactory,
                                               final ClusterInfo clusterInfo,
                                               final PluginEventManager pluginEventManager,
                                               final ClusterUpgradePluginLoaderFactory clusterUpgradePluginLoaderFactory) {
        this.bootstrapPluginDirectoryLoaderFactory = new BootstrapPluginDirectoryLoaderFactory(pathFactory, pluginEventManager);
        this.pathFactory = pathFactory;
        this.clusterInfo = clusterInfo;
        this.clusterUpgradePluginLoaderFactory = clusterUpgradePluginLoaderFactory;
    }

    @Override
    public PluginLoader getDirectoryPluginLoader(final List<PluginFactory> pluginFactories) {
        if (clusterInfo.isClustered()) {
            return clusterUpgradePluginLoaderFactory.create(
                    new DirectoryScanner(pathFactory.getInstalledPluginsDirectory()),
                    pathFactory.getInstalledPluginsDirectory(),
                    pathFactory.getPluginsFreezeFile(),
                    pluginFactories);
        }
        return bootstrapPluginDirectoryLoaderFactory.getDirectoryPluginLoader(pluginFactories);
    }
}
