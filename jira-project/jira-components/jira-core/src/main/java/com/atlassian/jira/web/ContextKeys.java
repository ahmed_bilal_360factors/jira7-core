package com.atlassian.jira.web;

public interface ContextKeys {
    public static final String STARTUP_TIME = "jira_startup_time";
}
