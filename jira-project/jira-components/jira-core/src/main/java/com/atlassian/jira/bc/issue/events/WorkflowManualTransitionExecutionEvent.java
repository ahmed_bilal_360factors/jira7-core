package com.atlassian.jira.bc.issue.events;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.analytics.api.annotations.PrivacyPolicySafe;

@PrivacyPolicySafe
public class WorkflowManualTransitionExecutionEvent {
    private static final String BASE_EVENT_NAME = "workflow.manual.issue.transition.execution";

    @PrivacyPolicySafe(true)
    private final int actionId;

    @PrivacyPolicySafe(true)
    private final Long issueId;

    @PrivacyPolicySafe(true)
    private final Long projectId;

    @PrivacyPolicySafe(true)
    private final String issueTypeId;

    @PrivacyPolicySafe(true)
    private final String projectTypeId;

    private final boolean successful;

    public WorkflowManualTransitionExecutionEvent(final int actionId,
                                                  final Long issueId, final Long projectId,
                                                  final String issueTypeId, final String projectTypeId,
                                                  final boolean successful) {
        this.actionId = actionId;
        this.issueId = issueId;
        this.projectId = projectId;
        this.projectTypeId = projectTypeId;
        this.issueTypeId = issueTypeId;
        this.successful = successful;
    }

    public int getActionId() {
        return actionId;
    }

    public Long getIssueId() {
        return issueId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public String getIssueTypeId() {
        return issueTypeId;
    }

    public String getProjectTypeId() {
        return projectTypeId;
    }

    @EventName
    public String buildEventName() {
        return String.format("%s.%s", BASE_EVENT_NAME, (successful ? "successful" : "failed"));
    }
}
