package com.atlassian.jira.config;

import com.atlassian.core.util.collection.EasyList;
import com.atlassian.jira.bean.SubTaskBean;
import com.atlassian.jira.bean.SubTaskBeanImpl;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.link.SequenceIssueLinkComparator;
import com.atlassian.jira.issue.util.IssueUpdateBean;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.CollectionReorderer;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class DefaultSubTaskManager implements SubTaskManager {
    private static final Logger log = LoggerFactory.getLogger(DefaultSubTaskManager.class);

    private static final String ISSUE_TYPE_NAME = "IssueType";

    private static final String DEFAULT_SUB_TASK_ISSUE_TYPE_NAME = "Sub-task";
    private static final long DEFAULT_SUB_TASK_ISSUE_TYPE_SEQUENCE = 0L;
    private static final String DEFAULT_SUB_TASK_ISSUE_TYPE_DESCRIPTION = "The sub-task of the issue";
    private static final String DEFAULT_SUB_TASK_ISSUE_TYPE_ICON_URL = "/images/icons/issuetypes/subtask_alternate.png";

    private final IssueLinkTypeManager issueLinkTypeManager;
    private final IssueLinkManager issueLinkManager;
    private final PermissionManager permissionManager;
    private final ApplicationProperties applicationProperties;
    private final ConstantsManager constantsManager;
    private final IssueTypeSchemeManager issueTypeSchemeManager;
    private final IssueManager issueManager;

    public DefaultSubTaskManager(ConstantsManager constantsManager, IssueLinkTypeManager issueLinkTypeManager,
                                 IssueLinkManager issueLinkManager, PermissionManager permissionManager,
                                 ApplicationProperties applicationProperties,
                                 IssueTypeSchemeManager issueTypeSchemeManager,
                                 IssueManager issueManager) {
        this.constantsManager = constantsManager;
        this.issueLinkTypeManager = issueLinkTypeManager;
        this.issueLinkManager = issueLinkManager;
        this.permissionManager = permissionManager;
        this.applicationProperties = applicationProperties;
        this.issueTypeSchemeManager = issueTypeSchemeManager;
        this.issueManager = issueManager;
    }

    /**
     * Turn on sub-tasks by creating a sub-task issue link type
     * and a default sub-task issue type
     */
    @Override
    public void enableSubTasks() throws CreateException {
        enableSubTasks(true);
    }

    @Override
    public void enableSubTasks(boolean createDefaultIfMissing) throws CreateException {
        // Check that the sub-task issue link does not exist
        final Collection subTaskIssueLinks = getSubTaskIssueLinkTypes();
        if (subTaskIssueLinks == null || subTaskIssueLinks.isEmpty()) {
            // Create default sub-task issue link
            issueLinkTypeManager.createIssueLinkType(SUB_TASK_LINK_TYPE_NAME, SUB_TASK_LINK_TYPE_OUTWARD_NAME, SUB_TASK_LINK_TYPE_INWARD_NAME, SUB_TASK_LINK_TYPE_STYLE);
        }

        // Check if sub-task already exists
        if (createDefaultIfMissing) {
            final Collection<IssueType> subTaskIssueTypes = constantsManager.getSubTaskIssueTypeObjects();
            if (subTaskIssueTypes == null || subTaskIssueTypes.isEmpty()) {
                // If not, create default sub-task issue type
                insertSubTaskIssueType(DEFAULT_SUB_TASK_ISSUE_TYPE_NAME, DEFAULT_SUB_TASK_ISSUE_TYPE_SEQUENCE, DEFAULT_SUB_TASK_ISSUE_TYPE_DESCRIPTION, DEFAULT_SUB_TASK_ISSUE_TYPE_ICON_URL);
            }
        }

        // Update the application property
        applicationProperties.setOption(APKeys.JIRA_OPTION_ALLOWSUBTASKS, true);
    }

    @Override
    public void disableSubTasks() {
        // Just set the property to false - do not remove any entities so that nothing breaks
        applicationProperties.setOption(APKeys.JIRA_OPTION_ALLOWSUBTASKS, false);
    }


    @Override
    public Collection<IssueType> getSubTaskIssueTypeObjects() {
        return constantsManager.getSubTaskIssueTypeObjects();
    }

    @Override
    public boolean isSubTasksEnabled() {
        return applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWSUBTASKS);
    }

    @Override
    public IssueType insertSubTaskIssueType(String name, Long sequence, String description, String iconurl)
            throws CreateException {
        // Check that an issue type of that name does not already exist.
        if (issueTypeExistsByName(name)) {
            throw new CreateException("Issue Type with name '" + name + "' already exists.");
        }

        // Insert the record into the database
        final IssueType subTaskIssueType = constantsManager.insertIssueType(name, sequence, SUB_TASK_ISSUE_TYPE_STYLE, description, iconurl);

        // Add to default scheme
        issueTypeSchemeManager.addOptionToDefault(subTaskIssueType.getId());

        return subTaskIssueType;
    }

    public IssueType insertSubTaskIssueType(String name, Long sequence, String description, Long avatarId) throws CreateException {
        // Check that an issue type of that name does not already exist.
        if (issueTypeExistsByName(name)) {
            throw new CreateException("Issue Type with name '" + name + "' already exists.");
        }

        // Insert the record into the database
        final IssueType subTaskIssueType = constantsManager.insertIssueType(name, sequence, SUB_TASK_ISSUE_TYPE_STYLE, description, avatarId);

        // Add to default scheme
        issueTypeSchemeManager.addOptionToDefault(subTaskIssueType.getId());

        return subTaskIssueType;

    }

    @Override
    public void updateSubTaskIssueType(String id, String name, Long sequence, String description, String iconurl)
            throws DataAccessException {
        constantsManager.updateIssueType(id, name, sequence, SUB_TASK_ISSUE_TYPE_STYLE, description, iconurl);
    }

    @Override
    public void updateSubTaskIssueType(String id, String name, Long sequence, String description, Long avatarId)
            throws DataAccessException {
        constantsManager.updateIssueType(id, name, sequence, SUB_TASK_ISSUE_TYPE_STYLE, description, avatarId);
    }

    private Collection getSubTaskIssueLinkTypes() {
        return issueLinkTypeManager.getIssueLinkTypesByStyle(SUB_TASK_LINK_TYPE_STYLE);
    }

    @Override
    public boolean issueTypeExistsByName(String name) {
        return constantsManager.constantExists(ISSUE_TYPE_NAME, name);
    }

    @Override
    public void moveSubTaskIssueTypeUp(String id) throws DataAccessException {
        if (id == null) {
            throw new IllegalArgumentException("Id cannot be null.");
        }

        ArrayList<IssueType> subTasksIssueTypes = new ArrayList<IssueType>(constantsManager.getEditableSubTaskIssueTypes());
        final IssueType issueType = getSubTaskIssueTypeById(id);
        CollectionReorderer.increasePosition(subTasksIssueTypes, issueType);

        recalculateSequencesAndStore(subTasksIssueTypes);
    }

    @Override
    public void moveSubTaskIssueTypeDown(String id) throws DataAccessException {
        if (id == null) {
            throw new IllegalArgumentException("Id cannot be null.");
        }

        final List<IssueType> subTasksIssueTypes = new ArrayList<IssueType>(constantsManager.getEditableSubTaskIssueTypes());
        final IssueType issueType = constantsManager.getIssueType(id);
        CollectionReorderer.decreasePosition(subTasksIssueTypes, issueType);

        recalculateSequencesAndStore(subTasksIssueTypes);
    }

    @Override
    public IssueType getSubTaskIssueTypeById(String id) {
        if (id == null) {
            throw new IllegalArgumentException("Id cannot be null.");
        }

        final IssueType issueType = constantsManager.getIssueType(id);
        if (issueType != null) {
            // Ensure that the issue type is a sub-task issue type
            if (!issueType.isSubTask()) {
                throw new IllegalArgumentException("The issue type with id '" + id + "' is not a sub-task issue type.");
            }
        }

        return issueType;
    }

    @Override
    public IssueType getSubTaskIssueType(String id) {
        if (id == null) {
            throw new IllegalArgumentException("Id cannot be null.");
        }

        final IssueType issueType = constantsManager.getIssueTypeObject(id);
        if (issueType == null) {
            return null;
        } else if (issueType.isSubTask()) {
            return issueType;
        } else {
            throw new IllegalArgumentException("The issue type with id '" + id + "' is not a sub-task issue type.");
        }
    }

    private void recalculateSequencesAndStore(List<IssueType> issueTypes) throws DataAccessException {
        constantsManager.recalculateIssueTypeSequencesAndStore(issueTypes);
    }

    @Override
    public void removeSubTaskIssueType(String name) throws RemoveException {
        // Ensure that an issue type with that name exists
        final IssueType issueType = (IssueType) constantsManager.getIssueConstantByName(ISSUE_TYPE_NAME, name);
        if (issueType != null) {
            // Ensure that the issue type is a sub-task
            if (issueType.isSubTask()) {
                // Remove the record from the database
                constantsManager.removeIssueType(issueType.getId());
            } else {
                throw new RemoveException("Issue Type with name '" + name + "' is not a sub-task issue type.");
            }
        } else {
            throw new RemoveException("Issue Type with name '" + name + "' does not exist.");
        }
    }

    @Override
    public boolean issueTypeExistsById(String id) {
        return (getSubTaskIssueTypeById(id) != null);
    }

    @Override
    public boolean isSubTask(Issue issue) {
        return (getParentIssueId(issue) != null);
    }

    @Override
    @Nullable
    public Long getParentIssueId(GenericValue issue) {
        ensureIssueNotNull(issue);
        return getParentIssueId(issue.getLong("id"));
    }

    @Override
    @Nullable
    public Long getParentIssueId(Issue issue) {
        return getParentIssueId(issue.getId());
    }

    @Override
    @Nullable
    public Long getParentIssueId(Long issueId) {
        if (issueId == null) return null;

        // Check if we have any incoming sub-task issue links
        final List<IssueLink> inwardLinks = issueLinkManager.getInwardLinks(issueId);
        for (final IssueLink inwardLink : inwardLinks) {
            if (inwardLink.getIssueLinkType().isSubTaskLinkType()) {
                return inwardLink.getSourceId();
            }
        }
        return null;
    }

    @Override
    public SubTaskBean getSubTaskBean(Issue issue, ApplicationUser remoteUser) {
        SubTaskBeanImpl subTaskBean = new SubTaskBeanImpl();

        final Collection<IssueLink> subTaskIssueLinks = getSubTaskIssueLinks(issue.getId());
        final Map<Long, Long> sequenceMapping = new HashMap<>(subTaskIssueLinks.size());
        for (IssueLink subTaskIssueLink: subTaskIssueLinks) {
            sequenceMapping.put(subTaskIssueLink.getDestinationId(), subTaskIssueLink.getSequence());
        }

        for (final Issue subTaskIssue : bulkLoadDestinationIssues(subTaskIssueLinks)) {
            if (subTaskIssue instanceof MutableIssue) {
                // Set the parent object explicitly so it doesn't need to be lazy loaded from the database.
                ((MutableIssue) subTaskIssue).setParentObject(issue);
            }
            // Check that the remote user has the permissions to actually see the sub-task due to issue level security.
            // Even though we keep the issue security level the same on sub-tasks and parent issues
            // due to things like 'assignee' and 'reporter' permissions, it is possible to have
            // a situation where a user can see the parent issue but not its sub task, or vice versa
            if (permissionManager.hasPermission(Permissions.BROWSE, subTaskIssue, remoteUser)) {
                subTaskBean.addSubTask(sequenceMapping.get(subTaskIssue.getId()), subTaskIssue, issue);
            }
        }

        return subTaskBean;
    }

    @Override
    public void moveSubTask(Issue parentIssue, Long currentSequence, Long sequence) {
        final List<IssueLink> subTaskIssueLinks = getSubTaskIssueLinks(parentIssue.getId());
        issueLinkManager.moveIssueLink(subTaskIssueLinks, currentSequence, sequence);
    }

    @Override
    public void resetSequences(Issue issue) {
        resetSequences(issue.getId());
    }

    private void resetSequences(Long issueId) {
        final List<IssueLink> subTaskIssueLinks = getSubTaskIssueLinks(issueId);
        issueLinkManager.resetSequences(subTaskIssueLinks);
    }

    /**
     * Retrieves ids of all sub-task issues in the system.
     */
    @Override
    public Collection<Long> getAllSubTaskIssueIds() {
        // Get the sub-task issue link type
        final IssueLinkType subTaskIssueLinkType = getSubTaskIssueLinkType();

        // Find all sub-task links
        final Collection<IssueLink> issueLinks = issueLinkManager.getIssueLinks(subTaskIssueLinkType.getId());

        // Theorietically we should be able to return the count of issue links (as a sub-task should cannot be a sub-task of
        // more than one issue). However if this ever changes, this will likely cause a bug. Hence, this code is in place.
        // As it is not expected to be executed often, the safe approach is taken.
        Set<Long> subTaskIssueIds = new HashSet<Long>();
        for (final IssueLink issueLink : issueLinks) {
            subTaskIssueIds.add(issueLink.getDestinationId());
        }

        return subTaskIssueIds;
    }

    private void ensureIssueNotNull(GenericValue issue) {
        if (issue == null) {
            throw new IllegalArgumentException("Issue cannot be null.");
        } else if (!"Issue".equals(issue.getEntityName())) {
            throw new IllegalArgumentException("The argument must be an issue.");
        }
    }

    @Override
    public List<IssueLink> getSubTaskIssueLinks(final Long issueId) {
        List<IssueLink> subTasks = new ArrayList<IssueLink>();
        for (IssueLink link : issueLinkManager.getOutwardLinks(issueId)) {
            if (link.getIssueLinkType().isSubTaskLinkType()) {
                subTasks.add(link);
            }
        }
        // Sort the sub-task issue links on sequence
        Collections.sort(subTasks, new SequenceIssueLinkComparator());
        return subTasks;
    }

    /**
     * Get an issue's subtasks.
     *
     * @return A collection of {@link Issue}s.
     */
    @Override
    public Collection<Issue> getSubTaskObjects(Issue parentIssue) {
        return bulkLoadDestinationIssues(getSubTaskIssueLinks(parentIssue.getId()));
    }

    /**
     * Bulk load the destination issue object from a collection of issue links.
     *
     * @param issuesToLoad the issue links to load the destination object of.
     * @return A collection of {@link Issue}s
     */
    private Collection<Issue> bulkLoadDestinationIssues(Collection<IssueLink> issuesToLoad) {
        Collection<Long> subTaskIds = new LinkedList<>();
        // Collect the ids of the sub tasks to bulk load them.
        subTaskIds.addAll(issuesToLoad.stream()
                .map(IssueLink::getDestinationId)
                .collect(Collectors.toList()));
        // Bulk load the sub-tasks to avoid a n+1 problem and improve scalability.
        return issueManager.getIssueObjects(subTaskIds);
    }

    @Override
    public void createSubTaskIssueLink(Issue parentIssue, Issue subTaskIssue, ApplicationUser remoteUser) throws CreateException {
        if (parentIssue == null) {
            throw new IllegalArgumentException("Parent Issue cannot be null.");
        } else if (subTaskIssue == null) {
            throw new IllegalArgumentException("Sub-Task Issue cannot be null.");
        }

        // Determine the next sequence of the issue
        final Collection subTaskIssueLinks = getSubTaskIssueLinks(parentIssue.getId());

        // Determine the sequence of the new sub-task link
        final long sequence = subTaskIssueLinks == null ? 0 : subTaskIssueLinks.size();
        issueLinkManager.createIssueLink(parentIssue.getId(), subTaskIssue.getId(), getSubTaskIssueLinkType().getId(), sequence, remoteUser);
    }

    private IssueLinkType getSubTaskIssueLinkType() {
        final Collection subTaskIssueLinkTypes = getSubTaskIssueLinkTypes();
        if (subTaskIssueLinkTypes != null && !subTaskIssueLinkTypes.isEmpty()) {
            if (subTaskIssueLinkTypes.size() > 1) {
                log.warn("Found '" + subTaskIssueLinkTypes.size() + "' sub-task issue link types. Returning first one.");
            }

            return (IssueLinkType) subTaskIssueLinkTypes.iterator().next();
        } else {
            return null;
        }
    }

    @Override
    public IssueUpdateBean changeParent(Issue subTask, Issue newParentIssue, ApplicationUser currentUser)
            throws RemoveException, CreateException {
        final Issue oldParentIssue = subTask.getParentObject();
        Collection<IssueLink> inwardLinks = issueLinkManager.getInwardLinks(subTask.getId());
        for (final IssueLink issueLink : inwardLinks) {
            if (issueLink.getIssueLinkType().isSubTaskLinkType()) {
                issueLinkManager.removeIssueLink(issueLink, currentUser);
            }
        }

        // Create new Link
        createSubTaskIssueLink(newParentIssue, subTask, currentUser);
        // Reorder new parent subTask
        resetSequences(newParentIssue.getId());
        // Reorder old parent subTask
        resetSequences(oldParentIssue.getId());

        // Create change item for subtask
        ChangeItemBean cibParent = new ChangeItemBean(ChangeItemBean.CUSTOM_FIELD, "Parent Issue",
                oldParentIssue.getKey(), oldParentIssue.getKey(),
                newParentIssue.getKey(), newParentIssue.getKey());

        //JRA-10546 - the sub task must have the same security level as its parent
        //Note, the IssueUpdater takes care of generating the change history (by checking which fields
        //have changed from the old to the new issue)
        GenericValue newSubTask = (GenericValue) subTask.getGenericValue().clone();
        newSubTask.set(IssueFieldConstants.SECURITY, newParentIssue.getSecurityLevelId());

        IssueUpdateBean issueUpdateBean = new IssueUpdateBean(newSubTask, subTask.getGenericValue(), EventType.ISSUE_UPDATED_ID, currentUser);
        issueUpdateBean.setChangeItems(EasyList.build(cibParent));
        return issueUpdateBean;
    }
}
