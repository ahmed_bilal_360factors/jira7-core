package com.atlassian.jira.event.issue;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Nonnull;

import com.atlassian.jira.event.comment.CommentCreatedEvent;
import com.atlassian.jira.event.comment.CommentUpdatedEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.managers.DefaultIssueDeleteHelper;
import com.atlassian.jira.issue.util.IssueUpdateBean;
import com.atlassian.jira.user.ApplicationUser;

import com.google.common.collect.ImmutableList;

import org.ofbiz.core.entity.GenericValue;

public class IssueEventBundleFactoryImpl implements IssueEventBundleFactory {
    private final IssueEventParamsTransformer paramsTransformer;
    private final EventTypesForIssueChange eventsForIssueChange;

    public IssueEventBundleFactoryImpl(IssueEventParamsTransformer paramsTransformer,
                                       EventTypesForIssueChange eventsForIssueChange) {
        this.paramsTransformer = paramsTransformer;
        this.eventsForIssueChange = eventsForIssueChange;
    }

    /**
     * This method uses {@link com.atlassian.jira.event.issue.EventTypesForIssueChange} to raise events during update.
     */
    @Override
    public IssueEventBundle createIssueUpdateEventBundle(Issue issue, GenericValue changeGroup, IssueUpdateBean iub,
                                                         ApplicationUser user) {
        // Sometimes an email handler will call this even though it's not really an issue update event. Therefore, we
        // check if the event type is issue_commented so incoming mails that are meant to only trigger a comment
        // notification (not generic issue update) will be handled properly :-( See JRA-41226
        if (EventType.ISSUE_COMMENTED_ID.equals(iub.getEventTypeId())) {
            return this.createCommentAddedBundle(issue, user, iub.getComment(), iub.getParams());
        } else {
            List<Long> eventIds = eventsForIssueChange.getEventTypeIdsForIssueUpdate(iub);

            Collection<JiraIssueEvent> events = toJiraIssueEvents(eventIds.stream()
                    .map(eventId -> new IssueEvent(issue,
                            user,
                            iub.getComment(),
                            iub.getWorklog(),
                            changeGroup,
                            transformParams(iub.getParams()),
                            eventId,
                            iub.isSendMail(),
                            iub.isSubtasksUpdated())));

            if (iub.getComment() != null) {
                return DefaultIssueEventBundle.create(ImmutableList.<JiraIssueEvent>builder()
                        .addAll(events)
                        .add(new CommentCreatedEvent(iub.getComment()))
                        .build());
            } else {
                return DefaultIssueEventBundle.create(events);
            }
        }
    }

    @Override
    public IssueEventBundle createWorklogEventBundle(final Issue issue, final GenericValue changeGroup, final IssueUpdateBean iub, final ApplicationUser user) {
        IssueEvent issueEvent = new IssueEvent(
                issue,
                user,
                iub.getComment(),
                iub.getWorklog(),
                changeGroup,
                transformParams(iub.getParams()),
                iub.getEventTypeId(),
                iub.isSendMail(),
                iub.isSubtasksUpdated()
        );
        return wrapInBundle(issueEvent);
    }

    @Override
    public IssueEventBundle createIssueDeleteEventBundle(Issue issue,
                                                         DefaultIssueDeleteHelper.DeletedIssueEventData deletedIssueEventData, ApplicationUser user) {
        IssueEvent issueEvent = new IssueEvent(
                issue,
                transformParams(deletedIssueEventData.paramsMap()),
                user,
                EventType.ISSUE_DELETED_ID,
                deletedIssueEventData.isSendMail()
        );
        return wrapInBundle(issueEvent);
    }

    @Override
    public IssueEventBundle createCommentAddedBundle(Issue issue, ApplicationUser user, Comment comment,
                                                     Map<String, Object> params) {
        IssueEvent issueEvent = new IssueEvent(
                issue,
                user,
                comment,
                null,
                null,
                transformParams(params),
                EventType.ISSUE_COMMENTED_ID
        );

        final CommentCreatedEvent commentCreatedEvent = new CommentCreatedEvent(comment);

        return DefaultIssueEventBundle.create(ImmutableList.of(new IssueEventWrapper(issueEvent), commentCreatedEvent));
    }

    @Override
    public IssueEventBundle createCommentEditedBundle(Issue issue, ApplicationUser user, Comment comment,
                                                      Map<String, Object> params) {
        IssueEvent issueEvent = new IssueEvent(
                issue,
                user,
                comment,
                null,
                null,
                transformParams(params),
                EventType.ISSUE_COMMENT_EDITED_ID
        );

        final CommentUpdatedEvent commentUpdatedEvent = new CommentUpdatedEvent(comment);

        return DefaultIssueEventBundle.create(ImmutableList.of(new IssueEventWrapper(issueEvent), commentUpdatedEvent));
    }

    @Override
    public IssueEventBundle createWorkflowEventBundle(Long eventType, Issue issue, ApplicationUser user,
                                                      Comment comment, GenericValue changeGroup, Map<String, Object> params, boolean sendMail,
                                                      String originalAssigneeId) {
        IssueEvent workflowEvent = new IssueEvent(
                issue,
                user,
                comment,
                null,
                changeGroup,
                transformParams(params),
                eventType,
                sendMail
        );

        if (issue.getAssigneeId() == null || issue.getAssigneeId().equals(originalAssigneeId) || originalAssigneeId!=null) {
            return wrapInBundle(workflowEvent);
        }

        IssueEvent assigneeChangedEvent = new IssueEvent(
                issue,
                user,
                comment,
                null,
                changeGroup,
                transformParams(params),
                EventType.ISSUE_ASSIGNED_ID,
                sendMail
        );

        return wrapInBundle(workflowEvent, assigneeChangedEvent);
    }

    @Override
    public IssueEventBundle wrapInBundle(IssueEvent issueEvent) {
        return wrapInBundle(new IssueEvent[]{issueEvent});
    }

    private IssueEventBundle wrapInBundle(IssueEvent... issueEvents) {
        return DefaultIssueEventBundle.create(toJiraIssueEvents(issueEvents));
    }

    private Collection<JiraIssueEvent> toJiraIssueEvents(IssueEvent... issueEvents) {
        return toJiraIssueEvents(Stream.of(issueEvents));
    }

    private Collection<JiraIssueEvent> toJiraIssueEvents(final Stream<IssueEvent> issueEvents)
    {
        return issueEvents
                .map(IssueEventWrapper::new)
                .collect(Collectors.toList());
    }

    private Map<String, Object> transformParams(Map<String, Object> params) {
        return paramsTransformer.transformParams(params);
    }

    private static class IssueEventWrapper implements DelegatingJiraIssueEvent {
        private final IssueEvent issueEvent;

        private IssueEventWrapper(IssueEvent issueEvent) {
            this.issueEvent = issueEvent;
        }

        @Nonnull
        @Override
        public IssueEvent asIssueEvent() {
            return issueEvent;
        }
    }
}
