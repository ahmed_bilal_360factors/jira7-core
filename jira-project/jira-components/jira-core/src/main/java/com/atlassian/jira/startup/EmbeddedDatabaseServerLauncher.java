package com.atlassian.jira.startup;

import com.atlassian.jira.config.properties.JiraSystemProperties;
import org.apache.log4j.Logger;
import org.h2.tools.Server;

import java.sql.SQLException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class EmbeddedDatabaseServerLauncher implements JiraLauncher {
    private static final Logger log = Logger.getLogger(EmbeddedDatabaseServerLauncher.class);

    private static final Integer port = JiraSystemProperties.getInstance().getInteger("jira.embedded.h2.server.port");

    private Server h2Server;

    private final Lock h2ServerLock = new ReentrantLock();

    /**
     * True if the server is enabled i.e. jira.embedded.h2.server.port system property is set.
     */
    public static boolean isEnabled() {
        return port != null;
    }

    /**
     * Server port i.e. jira.embedded.h2.server.port system property or null if not set.
     */
    public static Integer getPort() {
        return port;
    }

    /**
     * Start an H2 in-process server, if it's not already started.
     * <p>
     * jira.embedded.h2.server.port system property must be set for the server to start.
     */
    @Override
    public void start() {
        h2ServerLock.lock();
        try {
            if (h2Server == null && port != null) {
                log.info("starting embedded H2 server on port " + port);
                try {
                    h2Server = Server.createTcpServer("-tcpPort", port.toString()).start();
                } catch (SQLException e) {
                    h2Server = null;
                    throw new RuntimeException("unable to start embedded H2 server", e);
                }
            }
        } finally {
            h2ServerLock.unlock();
        }
    }

    /**
     * Stop the H2 in-process server, if it's started.
     */
    @Override
    public void stop() {
        h2ServerLock.lock();
        try {
            if (h2Server != null) {
                log.info("stopping embedded H2 server on port " + port);
                h2Server.stop();
            }
        } finally {
            h2ServerLock.unlock();
        }
    }
}
