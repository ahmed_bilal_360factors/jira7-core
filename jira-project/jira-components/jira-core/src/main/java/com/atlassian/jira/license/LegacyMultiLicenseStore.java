package com.atlassian.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.extras.api.LicenseException;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.util.dbc.Assertions.containsNoNulls;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.collect.Iterables.any;
import static com.google.common.collect.Iterables.transform;
import static java.lang.String.format;

/**
 * This class reads licenses from legacy locations within JIRA. In JIRA 6.4 we moved the location JIRA licenses. In JIRA
 * 7.0 we also read the SD license from the UPM store. We have a migration task that moves the licenses into the
 * right place, however, we have some code in JIRA that needs to access the licenses before JIRA migration is run
 * (e.g. checking if its possible to run the upgrade {@link BuildVersionLicenseCheck}). This class allows this code to
 * read and remove the licenses before they have been moved completely by the migration.
 *
 * @since v7.0
 */
public class LegacyMultiLicenseStore implements MultiLicenseStore {
    private final MultiLicenseStore delegate;
    private final ApplicationProperties applicationProperties;
    private final Jira6xServiceDeskPluginEncodedLicenseSupplier sdSupplier;

    @Inject
    public LegacyMultiLicenseStore(final MultiLicenseStoreImpl delegate,
                                   final ApplicationProperties applicationProperties) {
        this((MultiLicenseStore) delegate, applicationProperties);
    }

    @VisibleForTesting
    LegacyMultiLicenseStore(final MultiLicenseStore delegate,
                            final ApplicationProperties applicationProperties) {
        this.applicationProperties = notNull("applicationProperties", applicationProperties);
        this.delegate = notNull("delegate", delegate);
        this.sdSupplier = new Jira6xServiceDeskPluginEncodedLicenseSupplier(applicationProperties);
    }

    @Override
    @Nonnull
    public Iterable<String> retrieve() {
        try {
            Licenses parsedLicenses = new Licenses(transform(delegate.retrieve(), License::new));
            //If the table is empty then we try to old store.
            if (parsedLicenses.isEmpty()) {
                parsedLicenses = parsedLicenses.addLicense(retrieveOldLicense());
            }
            //Merge in the SD license if it exists.
            return parsedLicenses.addLicense(retrieveOldSdLicense()).toLicenseStrings();
        } catch (IllegalArgumentException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    @Override
    public void store(@Nonnull final Iterable<String> licenses) {
        if (Iterables.isEmpty(licenses)) {
            throw new IllegalArgumentException("You must store at least one license.");
        }

        if (any(licenses, Predicates.isNull())) {
            throw new IllegalArgumentException("You cannot store null licenses - no changes have been made to licenses.");
        }

        //We need to keep the Service Desk license around for migration. We will be moving the license
        //over to the multi-store so no need to keep in its old location.
        sdSupplier.moveToUpgradeStore();
        //Old licenses get moved over to the new store on save since 6.4. We also move over the SD license so that
        //we don't end up with no licenses in the delegate store (which is not allowed).
        clearOldLicense();
        delegate.store(licenses);
    }

    @Override
    public String retrieveServerId() {
        return delegate.retrieveServerId();
    }

    @Override
    public void storeServerId(final String serverId) {
        delegate.storeServerId(serverId);
    }

    @Override
    public void resetOldBuildConfirmation() {
        delegate.resetOldBuildConfirmation();
    }

    @Override
    public void confirmProceedUnderEvaluationTerms(final String userName) {
        delegate.confirmProceedUnderEvaluationTerms(userName);
    }

    @Override
    public void clear() {
        sdSupplier.moveToUpgradeStore();
        clearOldLicense();
        delegate.clear();
    }

    private void clearOldLicense() {
        applicationProperties.setText(APKeys.JIRA_LICENSE, null);
    }

    /**
     * Return the old JIRA < 6.3.x stored license. Since 6.4 the location of the licenses has moved.
     *
     * @return the license stored in the < 6.3.x location or {@link Option#none()} if no such license exists.
     */
    private Option<License> retrieveOldLicense() {
        return retrieveOldLicenseString().map(License::new);
    }

    /**
     * Return the old JIRA < 6.3.x stored license. Since 6.4 the location of the licenses has moved.
     *
     * @return the license stored in the < 6.3.x location or {@link Option#none()} if no such license exists.
     */
    private Option<String> retrieveOldLicenseString() {
        return Option.option(applicationProperties.getText(APKeys.JIRA_LICENSE));
    }

    /**
     * Return the Service Desk license stored in the UPM plugin store.
     *
     * @return the old Service Desk license from the UPM plugin store or {@link Option#none()}.
     */
    private Option<License> retrieveOldSdLicense() {
        return sdSupplier.get()
                .map(License::new)
                .filter(license -> license.isLicensed(ApplicationKeys.SERVICE_DESK));
    }

    private final static class License {
        private static final LicenseDetailsFactoryImpl.JiraProductLicenseManager FACTORY
                = LicenseDetailsFactoryImpl.JiraProductLicenseManager.INSTANCE;

        private final JiraProductLicense productLicense;
        private final String licenseString;

        private License(final String licenseString) {
            this.licenseString = notNull("licenseString", licenseString);
            try {
                this.productLicense = FACTORY.getProductLicense(licenseString);
            } catch (LicenseException e) {
                String msg = format("Unable to parse license: %s", abbreviate(licenseString));
                throw new IllegalArgumentException(msg, e);
            }
        }

        private String licenseString() {
            return licenseString;
        }

        private boolean isLicensed(ApplicationKey key) {
            return productLicense.getApplications().getKeys().contains(key);
        }

        private Set<ApplicationKey> applicationKeys() {
            return productLicense.getApplications().getKeys();
        }

        private String abbreviatedLicenseString() {
            return abbreviate(licenseString);
        }

        @Override
        public boolean equals(@Nullable final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final License license = (License) o;
            return licenseString.equals(license.licenseString);
        }

        @Override
        public int hashCode() {
            return licenseString.hashCode();
        }

        @Override
        public String toString() {
            return format("License for %s: %s.", productLicense.getApplications().getKeys(),
                    abbreviatedLicenseString());
        }

        private static String abbreviate(final String licenseString) {
            return StringUtils.abbreviate(licenseString, 40);
        }
    }

    private static class Licenses {
        private final ImmutableSet<License> licenses;
        private final ImmutableSet<ApplicationKey> applicationKeys;

        Licenses(Iterable<? extends License> licenses) {
            containsNoNulls("licenses", licenses);

            final Map<ApplicationKey, License> index = new HashMap<>();
            for (License newLicense : licenses) {
                for (ApplicationKey applicationKey : newLicense.applicationKeys()) {
                    final License oldLicense = index.put(applicationKey, newLicense);
                    if (oldLicense != null && !oldLicense.equals(newLicense)) {
                        throw new IllegalArgumentException(format("More than once license contains access to '%s'. "
                                + "The licenses are: %s and %s.", applicationKey, oldLicense, newLicense));

                    }
                }
            }

            this.applicationKeys = ImmutableSet.copyOf(index.keySet());
            this.licenses = ImmutableSet.copyOf(licenses);
        }

        private Licenses(ImmutableSet<License> licenses, ImmutableSet<ApplicationKey> applicationKeys) {
            this.licenses = licenses;
            this.applicationKeys = applicationKeys;
        }

        private Licenses addLicense(Option<License> licenseOption) {
            notNull("licenseOption", licenseOption);

            if (licenseOption.isEmpty()) {
                return this;
            }

            final License license = licenseOption.get();
            if (!Sets.intersection(license.applicationKeys(), applicationKeys).isEmpty()) {
                return this;
            }

            final ImmutableSet<License> licenses = ImmutableSet.<License>builder()
                    .addAll(this.licenses)
                    .add(license)
                    .build();

            final ImmutableSet<ApplicationKey> keys = ImmutableSet.<ApplicationKey>builder()
                    .addAll(this.applicationKeys)
                    .addAll(license.applicationKeys())
                    .build();

            return new Licenses(licenses, keys);
        }

        private List<String> toLicenseStrings() {
            return licenses.stream().map(License::licenseString).collect(CollectorsUtil.toImmutableList());
        }

        private boolean isEmpty() {
            return licenses.isEmpty();
        }

        @Override
        public String toString() {
            return licenses.toString();
        }
    }
}
