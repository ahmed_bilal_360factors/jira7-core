package com.atlassian.jira.security.type;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public abstract class AbstractIssueFieldSecurityType extends AbstractSecurityType {
    private static final Logger log = LoggerFactory.getLogger(AbstractIssueFieldSecurityType.class);

    /**
     * Get the field name for this Issue Field-based Security Type given the parameter in the saved configuration.
     * <p>
     * Some Field based permissions are based on hard-coded fields like assignee and reporter in which case the parameter is not used.
     * Other types use the parameter to name the particular User or Group custom field that is used for the permission.
     *
     * @param parameter the parameter as saved in the config
     * @return the field name for this Issue Field-based Security Type.
     */
    protected abstract String getFieldName(String parameter);

    @Override
    public Query getQuery(ApplicationUser searcher, Project project, String parameter) {
        if (project == null) {
            return null;
        }

        PermissionSchemeManager permissionSchemeManager = ComponentAccessor.getPermissionSchemeManager();
        Long schemeId = permissionSchemeManager.getSchemeIdFor(project);
        if (schemeId == null) {
            throw new IllegalStateException("No permission scheme associated with project " + project.getKey());
        }
        if (permissionSchemeManager.getPermissionSchemeEntries(schemeId, ProjectPermissions.BROWSE_PROJECTS, getType()).size() > 0) {
            BooleanQuery projectAndUserQuery = getQueryForProject(project, searcher, getFieldName(parameter));
            if (projectAndUserQuery != null) {
                BooleanQuery query = new BooleanQuery();
                query.add(projectAndUserQuery, BooleanClause.Occur.SHOULD);
                return query;
            }
        }
        return null;
    }

    @Override
    public Query getQuery(ApplicationUser searcher, Project project, IssueSecurityLevel securityLevel, String parameter) {
        BooleanQuery queryForSecurityLevel = getQueryForSecurityLevel(securityLevel, searcher, getFieldName(parameter));
        if (queryForSecurityLevel == null) {
            return null;
        } else {
            BooleanQuery query = new BooleanQuery();
            query.add(queryForSecurityLevel, BooleanClause.Occur.MUST);
            return query;
        }
    }

    /**
     * Gets called to produce the Lucene query for a project
     *
     * @param project  The project for which to construct a query
     * @param searcher The user who is searching to add to the query
     * @return A BooleanQuery with the project and searcher terms, or {@code null} if the searcher is either
     * {@code null} or not a known user
     */
    @Nullable
    protected BooleanQuery getQueryForProject(@Nonnull Project project, @Nullable ApplicationUser searcher, @Nonnull String fieldName) {
        BooleanQuery projectAndUserQuery = null;
        if (searcher != null) {
            projectAndUserQuery = new BooleanQuery();
            final Query projectQuery = new TermQuery(new Term(DocumentConstants.PROJECT_ID, project.getId().toString()));
            final Query userQuery = new TermQuery(new Term(fieldName, searcher.getKey()));
            projectAndUserQuery.add(projectQuery, BooleanClause.Occur.MUST);
            projectAndUserQuery.add(userQuery, BooleanClause.Occur.MUST);
        }
        return projectAndUserQuery;
    }

    /**
     * Produces a Lucene query for a given issue security type such that documents
     * match the query only when the given user is defined for the issue by this
     * custom field in the given security.
     *
     * @param issueSecurity the security defined by this IssueFieldSecurityType instance.
     * @param searcher      the user.
     * @return a query to constrain to the given issue security for the given user or {@code null} if user is either
     * {@code null} or not a known user
     */
    @Nullable
    protected BooleanQuery getQueryForSecurityLevel(@Nonnull IssueSecurityLevel issueSecurity, @Nullable ApplicationUser searcher,
                                                    @Nonnull String fieldName) {
        BooleanQuery issueLevelAndUserQuery = null;
        if (searcher != null) {
            issueLevelAndUserQuery = new BooleanQuery();
            // We wish to ensure that the search has the value of the field
            Term securityLevelIsSet = new Term(DocumentConstants.ISSUE_SECURITY_LEVEL, issueSecurity.getId().toString());
            Term customFieldSpecifiesUser = new Term(fieldName, searcher.getKey());
            issueLevelAndUserQuery.add(new TermQuery(securityLevelIsSet), BooleanClause.Occur.MUST);
            issueLevelAndUserQuery.add(new TermQuery(customFieldSpecifiesUser), BooleanClause.Occur.MUST);
        }
        return issueLevelAndUserQuery;
    }

    @Override
    public boolean hasPermission(Issue issue, String argument) {
        return false;
    }

    @Override
    public boolean hasPermission(Project project, String argument) {
        return false;
    }

    @Override
    public boolean hasPermission(Issue issue, String parameter, ApplicationUser user, boolean issueCreation) {

        if (user == null || issue == null) {
            return false;
        } else {
            return hasIssuePermission(user, issueCreation, issue, parameter);
        }
    }

    @Override
    public boolean hasPermission(Project project, String parameter, ApplicationUser user, boolean issueCreation) {

        if (user == null || project == null) {
            return false;
        } else {
            return hasProjectPermission(user, issueCreation, project);
        }
    }

    protected abstract boolean hasIssuePermission(ApplicationUser user, boolean issueCreation, Issue issue, String parameter);

    protected abstract boolean hasProjectPermission(ApplicationUser user, boolean issueCreation, Project project);
}
