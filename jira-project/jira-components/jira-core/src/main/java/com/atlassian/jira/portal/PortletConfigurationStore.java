package com.atlassian.jira.portal;

import com.atlassian.fugue.Option;
import com.atlassian.gadgets.dashboard.Color;
import com.atlassian.jira.util.collect.EnclosedIterable;
import com.atlassian.plugin.ModuleCompleteKey;

import javax.annotation.Nullable;
import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * Store for the {@link com.atlassian.jira.portal.PortletConfiguration} domain object.
 */
public interface PortletConfigurationStore {
    /**
     * Get all {@link com.atlassian.jira.portal.PortletConfiguration} objects for a given {@link
     * com.atlassian.jira.portal.PortalPage} id.
     *
     * @param portalPageId The id of the page to retreive all configurations for.
     * @return The configurations associated with the given page.
     */
    public List<PortletConfiguration> getByPortalPage(Long portalPageId);

    /**
     * Gall a {@link com.atlassian.jira.portal.PortletConfiguration} by its id.
     *
     * @param portletId The id of the portlet configuration.
     * @return The configuration of the given id or null.
     */
    @Nullable
    public PortletConfiguration getByPortletId(Long portletId);

    /**
     * Deletes the given {@link com.atlassian.jira.portal.PortletConfiguration}.
     *
     * @param pc The PortletConfiguration to delete.
     */
    public void delete(PortletConfiguration pc);

    /**
     * Saves the given {@link com.atlassian.jira.portal.PortletConfiguration}.
     *
     * @param pc The PortletConfiguration to save.
     */
    public void store(PortletConfiguration pc);

    /**
     * Given a gadget, this method will update it's row, column and parent dashboard id.
     *
     * @param gadgetId    The id of the gadget being updated
     * @param row         The new row value for this gadget
     * @param column      The new column value for this gadget
     * @param dashboardId The new parent dashboard id value for this gadget
     */
    void updateGadgetPosition(Long gadgetId, int row, int column, Long dashboardId);

    /**
     * Given a gadget, this method will update the color value for this gadget.
     *
     * @param gadgetId The id of the gadget being updated
     * @param color    The new color value for this gadget
     */
    void updateGadgetColor(Long gadgetId, Color color);

    /**
     * Given a gadget, this method updates all userprefs for this gadget.
     *
     * @param gadgetId  The id of the gadget being updated
     * @param userPrefs The new userprefs to set for this gadget.
     */
    void updateUserPrefs(Long gadgetId, Map<String, String> userPrefs);

    /**
     * Creates and adds a new {@link com.atlassian.jira.portal.PortletConfiguration} to given PortalPage.  This should
     * be used to add a gadget.
     *
     * @param pageId                 The id of the page to add the configuration to.
     * @param portletConfigurationId The id to use for adding the gadget. May be null for a generated id.
     * @param column                 The column position of the portlet.
     * @param row                    The row position of the portlet
     * @param gadgetXml              A URI specifying the location of the gadget XML.  May be null if this is a legacy portlet.
     * @param color                  The chrome color for the gadget.
     * @param userPreferences        A map of key -> value user preference pairs used to store gadget configuration.
     * @return The new PortletConfiguration with the id set.
     * deprecated Use {@link #addDashboardItem(Long, Long, Integer, Integer, com.atlassian.fugue.Option, com.atlassian.gadgets.dashboard.Color, java.util.Map, com.atlassian.fugue.Option)} instead. Since v6.4
     */
    @Deprecated
    PortletConfiguration addGadget(Long pageId, Long portletConfigurationId, Integer column, Integer row, URI gadgetXml, Color color, Map<String, String> userPreferences);

    /**
     * Creates and adds a new {@link com.atlassian.jira.portal.PortletConfiguration} to given PortalPage.  This should
     * be used to add a gadget.
     *
     * @param pageId                 The id of the page to add the configuration to.
     * @param portletConfigurationId The id to use for adding the gadget. May be null for a generated id.
     * @param column                 The column position of the portlet.
     * @param row                    The row position of the portlet
     * @param openSocialSpecUri      A URI specifying the location of the gadget XML.  May be an empty option if this is
     *                               a legacy portlet or dashboard item without uri replacement.
     * @param color                  The chrome color for the gadget.
     * @param userPreferences        A map of key -> value user preference pairs used to store gadget configuration.
     * @param moduleKey              A complete module key of dashboard items.
     * @return The new PortletConfiguration with the id set.
     * @since 6.4
     */
    PortletConfiguration addDashboardItem(Long pageId, Long portletConfigurationId, Integer column, Integer row,
                                          Option<URI> openSocialSpecUri, Color color, Map<String, String> userPreferences, Option<ModuleCompleteKey> moduleKey);

    /**
     * Returns an iterable over all PortletConfigurations available in the database.
     *
     * @return iterable over all PortletConfigurations available in the database
     */
    EnclosedIterable<PortletConfiguration> getAllPortletConfigurations();
}
