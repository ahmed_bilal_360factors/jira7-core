package com.atlassian.jira.plugin.freeze;

import com.atlassian.event.api.EventPublisher;

import java.io.File;

public class DefaultFreezeFileManagerFactory implements FreezeFileManagerFactory {
    private final EventPublisher eventPublisher;

    public DefaultFreezeFileManagerFactory(final EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @Override
    public FreezeFileManager create(final File freezeFile, final File installedPluginsDirectory) {
        return new FreezeFileManager(eventPublisher, freezeFile, installedPluginsDirectory);
    }
}
