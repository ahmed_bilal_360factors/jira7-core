package com.atlassian.jira.user.util;

import com.atlassian.fugue.Option;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Abstraction for interacting with recovery mode. In recovery mode a special 'recovery admin' is able to login with a
 * password specified at the command line. This gives customers who lock themselves out the ability to login and fix any
 * problems.
 *
 * @since 7.0
 */
public interface RecoveryMode {
    /**
     * Tells the caller if JIRA is in recovery mode.
     *
     * @return {@code true} JIRA is in recovery mode or false otherwise.
     */
    boolean isRecoveryModeOn();

    /**
     * Tells if the caller if the passed {@code ApplicationUser} is the 'recovery admin'.
     *
     * @return {@code true} if the passed {@code ApplicationUser} is the 'recovery admin' and recovery mode is enabled.
     */
    boolean isRecoveryUser(ApplicationUser user);

    /**
     * Tells the caller if the passed username is associated with the 'recovery admin'.
     *
     * @param username the username to check.
     * @return {@code true} if the passed {@code name} is the 'recovery admin' and recovery mode is enabled.
     */
    boolean isRecoveryUsername(String username);

    /**
     * Tells the caller the name of the 'recovery admin'.
     *
     * @return the name of the user who is considered to be the 'recovery admin' or
     * {@link com.atlassian.fugue.Option#none()} if there is no 'recovery admin' active.
     */
    Option<String> getRecoveryUsername();
}
