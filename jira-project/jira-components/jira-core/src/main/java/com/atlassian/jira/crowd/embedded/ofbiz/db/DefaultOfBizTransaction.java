package com.atlassian.jira.crowd.embedded.ofbiz.db;

import com.atlassian.core.ofbiz.util.CoreTransactionUtil;
import com.google.common.annotations.VisibleForTesting;
import org.ofbiz.core.entity.GenericTransactionException;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Provides a simpler way of executing code within an OfBiz transaction.
 *
 * @since v7.0
 */
public class DefaultOfBizTransaction implements OfBizTransaction {
    private final boolean inTransaction;
    private final OfBizTransactionUtil transactionUtil;
    private Status status = Status.UNPROCESSED;

    @VisibleForTesting
    DefaultOfBizTransaction(boolean inTransaction, OfBizTransactionUtil transactionUtil) {
        this.inTransaction = inTransaction;
        this.transactionUtil = transactionUtil;
    }

    @VisibleForTesting
    static <R> R withTransaction(OfBizTransactionUtil transactionUtil, Function<? super DefaultOfBizTransaction, R> transactionUsingFunction) {
        boolean inTransaction;
        try {
            inTransaction = transactionUtil.begin();
        } catch (GenericTransactionException e) {
            throw new TransactionException("Error starting transaction: " + e.getMessage(), e);
        }

        DefaultOfBizTransaction transaction = new DefaultOfBizTransaction(inTransaction, transactionUtil);
        try {
            R result = transactionUsingFunction.apply(transaction);
            transaction.finish();
            return result;
        } catch (Throwable t) {
            try {
                transaction.rollback();
            } catch (Throwable rbt) {
                t.addSuppressed(rbt);
            }
            throw t;
        }
    }

    static <R> R withTransaction(Function<? super DefaultOfBizTransaction, R> transactionUsingFunction) {
        return withTransaction(new OfBizTransactionUtil(), transactionUsingFunction);
    }

    static void withTransaction(Consumer<? super DefaultOfBizTransaction> transactionUsingFunction) {
        withTransaction(t ->
        {
            transactionUsingFunction.accept(t);
            return null;
        });
    }

    @VisibleForTesting
    Status getStatus() {
        return status;
    }

    @Override
    public boolean isProcessed() {
        return status != Status.UNPROCESSED;
    }

    @Override
    public void commit() {
        try {
            transactionUtil.commit(inTransaction);
            status = Status.COMMITTED;
        } catch (GenericTransactionException e) {
            throw new TransactionException("Error committing transaction: " + e.getMessage(), e);
        }
    }

    /**
     * Roll back the transaction
     */
    @Override
    public void rollback() {
        try {
            transactionUtil.rollback(inTransaction);
            status = Status.ROLLED_BACK;
        } catch (GenericTransactionException e) {
            throw new TransactionException("Error rolling back transaction: " + e.getMessage(), e);
        }
    }

    /**
     * Commits the transaction if it hasn't already been committed or rolled back.
     */
    private void finish() {
        if (!isProcessed()) {
            commit();
        }
    }

    @VisibleForTesting
    enum Status {
        /**
         * Has not yet been successfully committed or rolled back.
         */
        UNPROCESSED,

        /**
         * Successfully committed.
         */
        COMMITTED,

        /**
         * Rolled back.
         */
        ROLLED_BACK
    }

    /**
     * Wrapper for CoreTransactionUtil so we can mock it out for testing purposes.
     */
    @VisibleForTesting
    static class OfBizTransactionUtil {
        public boolean begin() throws GenericTransactionException {
            return CoreTransactionUtil.begin();
        }

        public void rollback(boolean began) throws GenericTransactionException {
            CoreTransactionUtil.rollback(began);
        }

        public void commit(boolean began) throws GenericTransactionException {
            CoreTransactionUtil.commit(began);
        }
    }
}
