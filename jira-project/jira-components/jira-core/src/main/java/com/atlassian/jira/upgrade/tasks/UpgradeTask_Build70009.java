package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.entity.Update;
import com.atlassian.jira.service.ServiceManager;
import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;
import com.atlassian.jira.upgrade.tasks.util.CronExpressionFixer;
import com.atlassian.jira.upgrade.tasks.util.CronExpressionFixer.Result;
import com.google.common.collect.Maps;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityOperator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.function.BiConsumer;

/**
 * Repair any malformed cron expressions in the {@code serviceconfig} and {@code clusteredjob} tables.
 *
 * @since v7.0.0
 */
public class UpgradeTask_Build70009 extends AbstractImmediateUpgradeTask {
    private static final Logger LOG = LoggerFactory.getLogger(UpgradeTask_Build70009.class);

    private static final String SERVICE_CONFIG = "ServiceConfig";
    private static final String ID = "id";
    private static final String CRON_EXPRESSION = "cronExpression";

    private final EntityEngine entityEngine;
    private final ServiceManager serviceManager;

    public UpgradeTask_Build70009(final EntityEngine entityEngine, final ServiceManager serviceManager) {
        this.entityEngine = entityEngine;
        this.serviceManager = serviceManager;
    }

    @Override
    public int getBuildNumber() {
        return 70009;
    }

    @Override
    public String getShortDescription() {
        return "Repair any malformed cron expressions in the 'serviceconfig' table";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception {
        repairServiceConfigs();
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        return false;
    }

    private void repairServiceConfigs() {
        final Map<Long, String> repairs = getRepairs();
        if (repairs.isEmpty()) {
            return;
        }

        repairs.forEach((id, cronExpression) -> Update.into(SERVICE_CONFIG)
                .set(CRON_EXPRESSION, cronExpression)
                .whereIdEquals(id)
                .execute(entityEngine));

        serviceManager.refreshAll();
    }

    private Map<Long, String> getRepairs() {
        final Map<Long, String> existing = findServiceConfigs();
        final Map<Long, String> repairs = Maps.newHashMap();
        existing.forEach((id, oldCron) -> repairCronExpression(id, oldCron, repairs::put));
        return repairs;
    }

    private static void repairCronExpression(Long id, String oldCron, BiConsumer<Long, String> put) {
        final Result result = CronExpressionFixer.repairCronExpression(oldCron);

        result.getNewCronExpression().ifPresent(newCron ->
        {
            LOG.warn("Repairing cron expression for service {}: '{}' => '{}'", id, oldCron, newCron);
            put.accept(id, newCron);
        });

        result.getCronSyntaxException().ifPresent(exception -> LOG.error(
                "Unable to repair cron expression for service id {}: '{}': {}",
                id, oldCron, exception.toString()));
    }

    private Map<Long, String> findServiceConfigs() {
        return Select.columns(ID, CRON_EXPRESSION)
                .from(SERVICE_CONFIG)
                .whereCondition(new EntityExpr(CRON_EXPRESSION, EntityOperator.NOT_EQUAL, null))
                .runWith(entityEngine)
                .asMap(gv -> gv.getLong(ID), gv -> gv.getString(CRON_EXPRESSION));
    }
}
