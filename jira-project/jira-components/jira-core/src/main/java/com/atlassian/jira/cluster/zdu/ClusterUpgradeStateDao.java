package com.atlassian.jira.cluster.zdu;

import com.atlassian.jira.model.querydsl.ClusterUpgradeStateDTO;

import java.util.Optional;

public interface ClusterUpgradeStateDao {
    Optional<ClusterUpgradeStateDTO> getCurrent();
    void writeState(NodeBuildInfo nodeBuildInfo, UpgradeState state);
}
