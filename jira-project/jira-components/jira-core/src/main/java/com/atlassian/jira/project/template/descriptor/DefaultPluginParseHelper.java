package com.atlassian.jira.project.template.descriptor;

import com.atlassian.plugin.PluginParseException;
import org.dom4j.Attribute;
import org.dom4j.Element;
import org.dom4j.Node;

import java.util.Optional;

public class DefaultPluginParseHelper implements PluginParseHelper {
    private final Element element;
    private final String xpath;

    public DefaultPluginParseHelper(Element element, String xpath) {
        this.element = element;
        this.xpath = xpath;
    }

    @Override
    public PluginParseHelper element(String name) {
        return element(name, true);
    }

    @Override
    public Optional<PluginParseHelper> optElement(String name) {
        return Optional.ofNullable(element(name, false));
    }

    @Override
    public String attribute(String name) {
        return attribute(name, true);
    }

    @Override
    public String optAttribute(String name) {
        return attribute(name, false);
    }

    @Override
    public String text() {
        return element.getText();
    }

    public Element rawElement() {
        return element;
    }

    private PluginParseHelper element(String name, boolean required) {
        String xpath = String.format("%s/%s", this.xpath, name);
        Node node = element.selectSingleNode(xpath);
        if (node == null) {
            if (required) {
                throw new PluginParseException("Missing expected node: " + xpath);
            }

            return null;
        }

        if (node instanceof Element) {
            return new DefaultPluginParseHelper((Element) node, xpath);
        }

        throw new PluginParseException(String.format("Got %s at %s when expecting an Element", node.getClass(), xpath));
    }

    private String attribute(String name, boolean required) {
        Attribute attribute = element.attribute(name);
        if (attribute == null) {
            if (required) {
                throw new PluginParseException(String.format("Missing expected attribute %s at: %s", name, xpath));
            }

            return null;
        }

        return attribute.getText();
    }
}
