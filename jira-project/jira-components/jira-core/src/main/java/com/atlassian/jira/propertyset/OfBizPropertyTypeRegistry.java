package com.atlassian.jira.propertyset;

import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.model.querydsl.JiraRelationalPathBase;
import com.google.common.collect.ImmutableMap;
import com.opensymphony.module.propertyset.PropertyImplementationException;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.ofbiz.DatePropertyHandler;
import com.opensymphony.module.propertyset.ofbiz.DecimalPropertyHandler;
import com.opensymphony.module.propertyset.ofbiz.NumberPropertyHandler;
import com.opensymphony.module.propertyset.ofbiz.PropertyHandler;
import com.opensymphony.module.propertyset.ofbiz.StringPropertyHandler;
import com.querydsl.core.types.Path;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.Map;

import static com.atlassian.jira.model.querydsl.QOSPropertyDate.O_S_PROPERTY_DATE;
import static com.atlassian.jira.model.querydsl.QOSPropertyDecimal.O_S_PROPERTY_DECIMAL;
import static com.atlassian.jira.model.querydsl.QOSPropertyNumber.O_S_PROPERTY_NUMBER;
import static com.atlassian.jira.model.querydsl.QOSPropertyString.O_S_PROPERTY_STRING;
import static com.atlassian.jira.model.querydsl.QOSPropertyText.O_S_PROPERTY_TEXT;

/**
 * Maps the int property value types as defined in {@link PropertySet} to the appropriate
 * handling information for them.  The property handlers have been enhanced such that those
 * whose {@link PropertyHandler#processGet(int, Object) processGet} methods return mutable
 * data (specifically for the {@code DATE} type) return a safe copy of the value instead of
 * the original so that they will be safe to use inside of the {@link CachingOfBizPropertySet}
 * implementation.
 *
 * @since v6.2
 */
class OfBizPropertyTypeRegistry {
    private static final Map<Integer, TypeMapper> TYPE_MAPPER;

    static {
        final StringPropertyHandler stringPropertyHandler = new StringPropertyHandler();

        final TypeMapper propString = new TypeMapper(O_S_PROPERTY_STRING, O_S_PROPERTY_STRING.value,
                stringPropertyHandler);
        final TypeMapper propText = new TypeMapper(O_S_PROPERTY_TEXT, O_S_PROPERTY_TEXT.value,
                stringPropertyHandler);
        final TypeMapper propDate = new TypeMapper(O_S_PROPERTY_DATE, O_S_PROPERTY_DATE.value,
                new SafeDatePropertyHandler());
        final TypeMapper propNumber = new TypeMapper(O_S_PROPERTY_NUMBER, O_S_PROPERTY_NUMBER.value,
                new NumberPropertyHandler());
        final TypeMapper propDecimal = new TypeMapper(O_S_PROPERTY_DECIMAL, O_S_PROPERTY_DECIMAL.value,
                new DecimalPropertyHandler());

        TYPE_MAPPER = ImmutableMap.<Integer, TypeMapper>builder()
                .put(PropertySet.BOOLEAN, propNumber)
                .put(PropertySet.INT, propNumber)
                .put(PropertySet.LONG, propNumber)
                .put(PropertySet.DOUBLE, propDecimal)
                .put(PropertySet.STRING, propString)
                .put(PropertySet.TEXT, propText)
                .put(PropertySet.DATE, propDate)
                .build();
    }

    @Nonnull
    static TypeMapper mapper(final int type) {
        return mapper(Integer.valueOf(type));
    }

    @Nonnull
    static TypeMapper mapper(final Integer type) {
        final TypeMapper mapper = TYPE_MAPPER.get(type);
        if (mapper == null) {
            throw new PropertyImplementationException("Invalid property type: " + type);
        }
        return mapper;
    }

    static class SafeDatePropertyHandler extends DatePropertyHandler {
        @Override
        public Object processGet(final int type, final Object input) {
            final Object result = super.processGet(type, input);
            return result instanceof Date ? ((Date) result).clone() : result;
        }
    }

    static class TypeMapper {
        private final JiraRelationalPathBase<?> valueTable;
        private final Path<?> valuePath;
        private final PropertyHandler handler;

        public TypeMapper(JiraRelationalPathBase<?> valueTable, Path<?> valuePath, PropertyHandler handler) {
            this.valueTable = valueTable;
            this.valuePath = valuePath;
            this.handler = handler;
        }

        public String getValueEntity() {
            return valueTable.getEntityName();
        }

        public JiraRelationalPathBase<?> getValueTable() {
            return valueTable;
        }

        public Path<?> getValuePath() {
            return valuePath;
        }

        public PropertyHandler getHandler() {
            return handler;
        }

        boolean upsert(DbConnection db, long id, Object value) {
            if (update(db, id, value) <= 0L) {
                insert(db, id, value);
            }
            return true;
        }

        @SuppressWarnings("unchecked")  // Type punning on valuePath is not easily avoidable, here
        long insert(DbConnection db, long id, Object value) {
            return db.insert(valueTable)
                    .set(valueTable.getNumericIdPath(), id)
                    .set((Path<Object>)valuePath, value)
                    .execute();
        }

        @SuppressWarnings("unchecked")  // Type punning on valuePath is not easily avoidable, here
        long update(DbConnection db, long id, Object value) {
            return db.update(valueTable)
                    .set((Path<Object>)valuePath, value)
                    .where(valueTable.getNumericIdPath().eq(id))
                    .execute();
        }

        long delete(DbConnection db, long id) {
            return db.delete(valueTable)
                    .where(valueTable.getNumericIdPath().eq(id))
                    .execute();
        }

        public boolean hasSameEntityName(@Nonnull TypeMapper otherMapper) {
            return valueTable.getEntityName().equals(otherMapper.valueTable.getEntityName());
        }

        @Override
        public String toString() {
            return "TypeMapper[handler=" + handler.getClass().getSimpleName() +
                    ",valueEntity=" + getValueEntity() +
                    ']';
        }
    }
}

