package com.atlassian.jira.bc.project;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.task.TaskContext;

/**
 * Context for project disrupting operations. There can be only one such operation
 * per project at any given time.
 *
 * @since v7.1.1
 */
public abstract class ProjectTaskContext implements TaskContext {

    protected final Long projectId;

    public ProjectTaskContext(Project project) {
        projectId = project.getId();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProjectTaskContext)) {
            return false;
        }

        final ProjectTaskContext that = (ProjectTaskContext) o;

        if (!projectId.equals(that.projectId)) {
            return false;
        }

        return projectId.equals(that.projectId);
    }

    @Override
    public int hashCode() {
        return projectId.hashCode();
    }
}
