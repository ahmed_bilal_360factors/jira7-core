package com.atlassian.jira.upgrade;

import com.atlassian.jira.index.request.ReindexRequestTypes;

import static com.atlassian.upgrade.api.UpgradeContext.UpgradeTrigger.PROVISIONING;

/**
 * Upgrade service used in setup process. It triggers lucene indexing only if immediate upgrade tasks were executed.
 */
public class SetupUpgradeService implements UpgradeService {

    private final LoggingUpgradeService loggingUpgradeService;

    public SetupUpgradeService(final LoggingUpgradeService loggingUpgradeService) {
        this.loggingUpgradeService = loggingUpgradeService;
    }

    @Override
    public UpgradeResult runUpgrades() {
        return loggingUpgradeService.runUpgradesWithLogging(
                ReindexRequestTypes.onlyImmediate(),
                () -> PROVISIONING,
                "run upgrades for setup"
        );
    }

    @Override
    public boolean areUpgradesRunning() {
        return loggingUpgradeService.areUpgradesRunning();
    }
}
