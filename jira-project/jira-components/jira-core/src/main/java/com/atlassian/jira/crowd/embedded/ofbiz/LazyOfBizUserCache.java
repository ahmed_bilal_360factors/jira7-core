package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.util.Visitor;
import com.atlassian.jira.util.map.CacheObject;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.atlassian.jira.crowd.embedded.ofbiz.DirectoryEntityKey.getKeyLowerCase;

/**
 * A lazy loading User Cache.
 *
 * @since v7.0
 */
class LazyOfBizUserCache implements OfBizUserCache {
    private final Cache<DirectoryEntityKey, CacheObject<OfBizUser>> cache;
    private OfBizDelegator ofBizDelegator;

    public LazyOfBizUserCache(final CacheManager cacheManager, final OfBizDelegator ofBizDelegator) {
        this.ofBizDelegator = ofBizDelegator;
        this.cache = cacheManager.getCache(LazyOfBizUserCache.class.getName() + ".userCache",
                new UserCacheLoader(),
                new CacheSettingsBuilder()
                        .expireAfterAccess(30, TimeUnit.MINUTES).flushable().build());
    }

    @Override
    public boolean isCacheInitialized() {
        //The lazy cache is always ready for action
        return true;
    }

    @Override
    public void refresh() {
        cache.removeAll();
    }

    @Override
    public void visitAllUserIdsInDirectory(long directoryId, Visitor<String> visitor) {
        Select.stringColumn(UserEntity.EXTERNAL_ID).from(UserEntity.ENTITY).whereEqual(UserEntity.DIRECTORY_ID, directoryId).runWith(ofBizDelegator)
                .visitWith(visitor);
    }

    @Override
    public OfBizUser getCaseInsensitive(long directoryId, String name) {
        CacheObject<OfBizUser> cacheValue = cache.get(getKeyLowerCase(directoryId, name));
        if (cacheValue == null) {
            return null;
        }
        return cacheValue.getValue();
    }

    @Override
    public List<OfBizUser> getAllCaseInsensitive(long directoryId, Collection<String> userIds) {
        final List<OfBizUser> users = new ArrayList<OfBizUser>(userIds.size());

        //Do this query in batches
        //For large amounts of users, doing less database calls pays off considerably
        //But if it's too big some databases choke on it - 500 should be safe
        final int batchSize = 500;
        final List<String> userIdBatch = new ArrayList<String>(batchSize);

        for (String userId : userIds) {
            userIdBatch.add(userId);
            if (userIdBatch.size() == batchSize) {
                readUsers(directoryId, userIdBatch, users);
                userIdBatch.clear();
            }
        }

        //Final batch
        if (!userIdBatch.isEmpty()) {
            readUsers(directoryId, userIdBatch, users);
        }

        return users;

    }

    @Override
    public DirectoryEntityKey refresh(OfBizUser user) {
        //Remove entry from lazy cache so it is re-read from the database on next access
        DirectoryEntityKey key = getKeyLowerCase(user);
        cache.remove(key);
        return key;
    }

    @Override
    public void remove(long directoryId, String name) {
        cache.remove(getKeyLowerCase(directoryId, name));
    }

    @Override
    public void remove(DirectoryEntityKey key) {
        cache.remove(key);
    }

    private void readUsers(long directoryId, List<String> userNames, final List<OfBizUser> readIntoList) {
        Select.from(UserEntity.ENTITY)
                .whereEqual(UserEntity.DIRECTORY_ID, directoryId)
                .andCondition(new EntityExpr(UserEntity.LOWER_USER_NAME, EntityOperator.IN, userNames))
                .runWith(ofBizDelegator)
                .visitWith(new Visitor<GenericValue>() {
                    @Override
                    public void visit(GenericValue element) {
                        readIntoList.add(OfBizUser.from(element));
                    }
                });
    }

    private class UserCacheLoader implements CacheLoader<DirectoryEntityKey, CacheObject<OfBizUser>> {
        @Nonnull
        @Override
        public CacheObject<OfBizUser> load(@Nonnull final DirectoryEntityKey key) {
            final GenericValue user = Select.columns(OfBizUser.SUPPORTED_FIELDS)
                    .from(UserEntity.ENTITY)
                    .whereEqual(UserEntity.DIRECTORY_ID, key.getDirectoryId())
                    .andEqual(UserEntity.LOWER_USER_NAME, key.getName())
                    .runWith(ofBizDelegator)
                    .singleValue();

            if (user == null) {
                return CacheObject.NULL();
            } else {
                return CacheObject.wrap(OfBizUser.from(user));
            }
        }
    }

}
