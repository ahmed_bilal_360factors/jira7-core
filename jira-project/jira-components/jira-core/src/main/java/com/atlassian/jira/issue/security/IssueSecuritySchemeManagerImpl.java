package com.atlassian.jira.issue.security;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.association.NodeAssocationType;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.group.GroupConfigurable;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.entity.EntityUtils;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.event.issue.security.IssueSecurityLevelAddedEvent;
import com.atlassian.jira.event.issue.security.IssueSecurityLevelDeletedEvent;
import com.atlassian.jira.event.issue.security.IssueSecuritySchemeAddedToProjectEvent;
import com.atlassian.jira.event.issue.security.IssueSecuritySchemeCopiedEvent;
import com.atlassian.jira.event.issue.security.IssueSecuritySchemeCreatedEvent;
import com.atlassian.jira.event.issue.security.IssueSecuritySchemeDeletedEvent;
import com.atlassian.jira.event.issue.security.IssueSecuritySchemeRemovedFromProjectEvent;
import com.atlassian.jira.event.issue.security.IssueSecuritySchemeUpdatedEvent;
import com.atlassian.jira.event.scheme.AbstractSchemeAddedToProjectEvent;
import com.atlassian.jira.event.scheme.AbstractSchemeCopiedEvent;
import com.atlassian.jira.event.scheme.AbstractSchemeEvent;
import com.atlassian.jira.event.scheme.AbstractSchemeRemovedFromProjectEvent;
import com.atlassian.jira.event.scheme.AbstractSchemeUpdatedEvent;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.permission.PermissionContextFactory;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.scheme.AbstractSchemeManager;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.scheme.SchemeFactory;
import com.atlassian.jira.security.SecurityTypeManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.type.GroupDropdown;
import com.atlassian.jira.security.type.SecurityType;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.tenancy.TenantAware;
import com.atlassian.jira.tenancy.TenantInfo;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NameComparator;


import com.google.common.collect.ImmutableList;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@TenantInfo(TenantAware.UNRESOLVED)
public class IssueSecuritySchemeManagerImpl extends AbstractSchemeManager implements IssueSecuritySchemeManager, Startable, GroupConfigurable {
    private static final Logger log = LoggerFactory.getLogger(IssueSecuritySchemeManagerImpl.class);

    private static final String SCHEME_ENTITY_NAME = "IssueSecurityScheme";
    private static final String ISSUE_SECURITY_ENTITY_NAME = "SchemeIssueSecurities";
    private static final String SCHEME_DESC = "Issue Security";
    private static final String DEFAULT_NAME_KEY = "admin.schemes.security.default";
    private static final String DEFAULT_DESC_KEY = "admin.schemes.security.default.desc";
    private static final String PERM_TYPE = "type";
    private static final String PERM_PARAMETER = "parameter";
    private static final NodeAssocationType ISSUE_SECURITY_SCHEME_ASSOCIATION = new NodeAssocationType("ProjectScheme", "Project", "IssueSecurityScheme");
    private static final String SCHEME = "scheme";

    private final Cache<CacheKey, List<GenericValue>> schemeIdToSecuritiesCache;
    private final Cache<Long, List<IssueSecurityLevelPermission>> securityLevelToPermissionsCache;
    private final IssueIndexingService issueIndexingService;
    private final IssueManager issueManager;
    private final TaskManager taskManager;
    private final QueryDslAccessor queryDslAccessor;
    private final I18nHelper i18nHelper;

    private final ProjectManager projectManager;
    private final OfBizDelegator ofBizDelegator;
    private final NodeAssociationStore nodeAssociationStore;

    public IssueSecuritySchemeManagerImpl(ProjectManager projectManager, SecurityTypeManager securityTypeManager,
                                          PermissionContextFactory permissionContextFactory, SchemeFactory schemeFactory, EventPublisher eventPublisher,
                                          final OfBizDelegator ofBizDelegator, final GroupManager groupManager, NodeAssociationStore nodeAssociationStore, CacheManager cacheManager,
                                          IssueIndexingService issueIndexingService, IssueManager issueManager, TaskManager taskManager, QueryDslAccessor queryDslAccessor, I18nHelper i18nHelper) {
        super(projectManager, securityTypeManager, permissionContextFactory, schemeFactory,
                nodeAssociationStore, ofBizDelegator, groupManager, eventPublisher, cacheManager);
        this.projectManager = projectManager;
        this.nodeAssociationStore = nodeAssociationStore;
        this.ofBizDelegator = ofBizDelegator;
        this.issueIndexingService = issueIndexingService;
        this.issueManager = issueManager;
        this.taskManager = taskManager;
        this.queryDslAccessor = queryDslAccessor;
        this.i18nHelper = i18nHelper;

        this.schemeIdToSecuritiesCache = cacheManager.getCache(IssueSecuritySchemeManagerImpl.class.getName() + ".schemeIdToSecuritiesCache",
                this::loadPermissionsBySecurityLevel,
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).build());

        this.securityLevelToPermissionsCache = cacheManager.getCache(IssueSecuritySchemeManagerImpl.class.getName() + "securityLevelToPermissionsCache",
                this::loadSecuritiesBySchemeId,
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).build());
    }

    @Override
    public void start() throws Exception {
        eventPublisher.register(this);
    }

    @EventListener
    @Override
    public void onClearCache(final ClearCacheEvent event) {
        super.onClearCache(event);
        clearCache();
    }

    @Override
    public String getSchemeEntityName() {
        return SCHEME_ENTITY_NAME;
    }

    @Override
    public String getEntityName() {
        return ISSUE_SECURITY_ENTITY_NAME;
    }

    @Override
    public String getSchemeDesc() {
        return SCHEME_DESC;
    }

    @Override
    public String getDefaultNameKey() {
        return DEFAULT_NAME_KEY;
    }

    @Override
    public String getDefaultDescriptionKey() {
        return DEFAULT_DESC_KEY;
    }

    private void clearCache() {
        if (log.isDebugEnabled()) {
            log.debug("Clearing issue security scheme cache.");
        }
        schemeIdToSecuritiesCache.removeAll();
        securityLevelToPermissionsCache.removeAll();
    }

    @Override
    public List<GenericValue> getEntities(GenericValue scheme) {
        final CacheKey id = new CacheKey(scheme.getLong("id"));
        return schemeIdToSecuritiesCache.get(id);
    }

    @Override
    public List<GenericValue> getEntities(GenericValue scheme, Long securityLevelId) throws GenericEntityException {
        final List<GenericValue> securities = getEntitiesBySecurityLevel(securityLevelId);
        return (securities.isEmpty() || scheme.getLong("id").equals(securities.get(0).getLong("scheme")))
                ? securities
                : Collections.<GenericValue>emptyList();
    }

    @Override
    public IssueSecurityLevelScheme getIssueSecurityLevelScheme(Long issueSecuritySchemeId) {
        // TODO: We should probably cache this, but not cached in old code.
        return Select.from(Entity.ISSUE_SECURITY_LEVEL_SCHEME).whereEqual("id", issueSecuritySchemeId).runWith(ofBizDelegator).singleValue();
    }

    @Override
    public Collection<IssueSecurityLevelScheme> getIssueSecurityLevelSchemes() {
        return Select.from(Entity.ISSUE_SECURITY_LEVEL_SCHEME).runWith(ofBizDelegator).asList();
    }

    @Override
    public List<GenericValue> getEntitiesBySecurityLevel(Long securityLevelId) {
        return EntityUtils.convertToGenericValues(Entity.ISSUE_SECURITY_LEVEL_PERMISSION, getPermissionsBySecurityLevel(securityLevelId));
    }

    @Override
    public List<IssueSecurityLevelPermission> getPermissionsBySecurityLevel(Long securityLevelId) {
        return securityLevelToPermissionsCache.get(securityLevelId);
    }

    @Override
    public Collection<GenericValue> getSchemesContainingEntity(String type, String parameter) {
        final Collection<GenericValue> entities = ofBizDelegator.findByAnd(ISSUE_SECURITY_ENTITY_NAME, FieldMap.build(
                PERM_TYPE, type,
                PERM_PARAMETER, parameter));
        if (entities.isEmpty()) {
            return Collections.emptyList();
        }

        final Set<Long> schemeIds = entities.stream()
                .map(schemeEntity -> schemeEntity.getLong("scheme")).collect(Collectors.toSet());
        // This is not needed if we can do a distinct select
        return getSchemesByIds(schemeIds);
    }

    @Override
    public void setSchemeForProject(Project project, Long schemeId) {
        // remove any existing association
        nodeAssociationStore.removeAssociationsFromSource(ISSUE_SECURITY_SCHEME_ASSOCIATION, project.getId());
        // Add new association if not null
        if (schemeId != null)
            nodeAssociationStore.createAssociation(ISSUE_SECURITY_SCHEME_ASSOCIATION, project.getId(), schemeId);
        flushProjectSchemes();
    }

    @Override
    public String assignSchemeToProject(@Nonnull Project project, Long newSchemeId, Map<Long, Long> oldToNewSecurityLevelMappings) {
        final String taskName = i18nHelper.getText("admin.iss.associate.security.scheme", project.getName());
        Callable<AssignIssueSecuritySchemeCommand.AssignSecurityLevelResult> indexCallable =
                new AssignIssueSecuritySchemeCommand(project, newSchemeId, oldToNewSecurityLevelMappings,
                        queryDslAccessor, this, issueIndexingService, issueManager, log,
                        i18nHelper);
        return taskManager.submitTask(indexCallable, taskName,
                new AssignIssueSecuritySchemeTaskContext(project), true).getProgressURL();
    }

    @Override
    public List<Project> getProjectsUsingScheme(long schemeId) {
        return nodeAssociationStore.getSourceIdsFromSink(ISSUE_SECURITY_SCHEME_ASSOCIATION, schemeId).stream()
                .map(projectManager::getProjectObj)
                .sorted(NameComparator.COMPARATOR)
                .collect(Collectors.toList());
    }

    private Collection<GenericValue> getSchemesByIds(Set<Long> schemeIds) {
        List<EntityExpr> entityConditions = schemeIds.stream()
                .map(schemeId -> new EntityExpr("id", EntityOperator.EQUALS, schemeId))
                .collect(Collectors.toList());
        return entityConditions.isEmpty()
                ? Collections.<GenericValue>emptyList()
                : ofBizDelegator.findByOr(SCHEME_ENTITY_NAME, entityConditions, Collections.emptyList());
    }


    /**
     * Get all Generic Value permission records for a particular scheme and permission Id
     *
     * @param scheme       The scheme that the permissions belong to
     * @param schemeTypeId The security level Id
     * @param parameter    The permission parameter (group name etc)
     * @return List of (GenericValue) permissions
     * @throws GenericEntityException
     */
    @Override
    public List<GenericValue> getEntities(GenericValue scheme, Long schemeTypeId, String parameter) throws GenericEntityException {
        final List<GenericValue> securities = getEntities(scheme, schemeTypeId);
        return securities.isEmpty() ? securities : filter(securities, withField(PERM_PARAMETER, parameter));
    }

    //This one is for Workflows as the entity type is a string
    @Override
    public List<GenericValue> getEntities(GenericValue scheme, String entityTypeId) throws GenericEntityException {
        throw new IllegalArgumentException("Issue Security scheme IDs must be Long values.");
    }

    /**
     * Get all Generic Value issue security records for a particular scheme, type and Id
     *
     * @param scheme       The scheme that the permissions belong to
     * @param type         The type of the permission(Group, Current Reporter etc)
     * @param schemeTypeId The security level Id
     * @return List of (GenericValue) permissions
     * @throws GenericEntityException
     */
    @Override
    public List<GenericValue> getEntities(GenericValue scheme, String type, Long schemeTypeId) throws GenericEntityException {
        final List<GenericValue> securities = getEntities(scheme, schemeTypeId);
        return securities.isEmpty() ? securities : filter(securities, withField(PERM_TYPE, type));
    }

    public GenericValue createSchemeEntity(GenericValue scheme, SchemeEntity schemeEntity) throws GenericEntityException {
        final GenericValue result = createSchemeEntityNoEvent(scheme, schemeEntity);
        eventPublisher.publish(new IssueSecurityLevelAddedEvent((scheme == null) ? null : scheme.getLong("id"), schemeEntity));
        return result;
    }

    @Override
    protected GenericValue createSchemeEntityNoEvent(GenericValue scheme, SchemeEntity schemeEntity) throws GenericEntityException {
        if (!(schemeEntity.getEntityTypeId() instanceof Long)) {
            throw new IllegalArgumentException("Issue Security Level IDs must be a long value.");
        }

        try {
            final Long schemeId = (scheme == null) ? null : scheme.getLong("id");
            return EntityUtils.createValue(ISSUE_SECURITY_ENTITY_NAME, FieldMap.build(
                    "scheme", schemeId,
                    "security", schemeEntity.getEntityTypeId(),
                    PERM_TYPE, schemeEntity.getType(),
                    PERM_PARAMETER, schemeEntity.getParameter()));
        } finally {
            clearCache();
        }
    }

    /**
     * This method overrides the AbstractSchemeManager because within Issue Security schemes there is an extra level, which
     * is the table that holds the Security Levels for that Scheme. This is because with Issue Security schemes you can add and delete
     * the different levels of security. With other schemes this is not possible
     */
    @Override
    public GenericValue copyScheme(GenericValue oldScheme) throws GenericEntityException {
        if (oldScheme == null) {
            return null;
        }

        try {
            final String name = getNameForCopy(oldScheme.getString("name"), null);

            //Copy the original scheme
            final GenericValue newScheme = createSchemeNoEvent(name, oldScheme.getString("description"));

            //for the scheme copy all security levels
            copySecurityLevels(newScheme, oldScheme);

            eventPublisher.publish(createSchemeCopiedEvent(schemeFactory.getScheme(oldScheme), schemeFactory.getScheme(newScheme)));

            return newScheme;
        } finally {
            clearCache();
        }
    }

    @Nonnull
    @Override
    protected AbstractSchemeCopiedEvent createSchemeCopiedEvent(@Nonnull final Scheme oldScheme, @Nonnull final Scheme newScheme) {
        return new IssueSecuritySchemeCopiedEvent(oldScheme, newScheme);
    }

    private void copySecurityLevels(GenericValue scheme, GenericValue oldScheme) throws GenericEntityException {
        //get all the security levels for this scheme
        List<GenericValue> levels = ofBizDelegator.findByAnd("SchemeIssueSecurityLevels", FieldMap.build("scheme", oldScheme.getLong("id")));

        //create the security levels for the new scheme
        for (GenericValue level : levels) {
            IssueSecurityLevel newSecurityLevel = ComponentAccessor.getIssueSecurityLevelManager().createIssueSecurityLevel(
                    scheme.getLong("id"),
                    level.getString("name"),
                    level.getString("description"));

            //if this level is the default level for the old scheme then make it the default level for the new scheme also
            if (level.getLong("id").equals(oldScheme.getLong("defaultlevel"))) {
                scheme.set("defaultlevel", newSecurityLevel.getId());
                scheme.store();
            }

            //copy all the securities for this security level and scheme and copy them also
            List<GenericValue> securities = ofBizDelegator.findByAnd(getEntityName(), FieldMap.build(
                    "scheme", oldScheme.getLong("id"),
                    "security", level.getLong("id")));

            for (GenericValue security : securities) {
                createSchemeEntity(scheme, new SchemeEntity(security.getString(PERM_TYPE), security.getString(PERM_PARAMETER), newSecurityLevel.getId()));
            }
        }
    }

    @Override
    public boolean hasSecurityLevelAccess(@Nonnull Issue issue, @Nullable ApplicationUser user) {
        final Long securityLevelId = issue.getSecurityLevelId();
        if (securityLevelId == null) {
            // no security set on the issue
            return true;
        }
		
		//Predict code
		System.out.println("This is predict code ..................................");
		System.out.println("hasSecurityLevelAccess");
		WatcherManager wm = ComponentAccessor.getWatcherManager();
		List<String> watchers=wm.getCurrentWatcherUsernames(issue);
		if(watchers.contains(user.getUsername())) {
			return true;
		}
		

        try {
            //Get the issue security scheme associated to the project for this project
            Scheme scheme = getSchemeFor(issue.getProjectObject());
            if (scheme != null) {
                //try each type of Issue Security Type
                return hasPermission(issue, user, getEntities(getScheme(scheme.getId()), securityLevelId));
            }
        } catch (GenericEntityException e) {
            log.error("Could not retrieve entities from the database", e);
        }
        return false;
    }

    //Check to see if the permission exists
    private boolean hasPermission(Issue issue, ApplicationUser user, List<GenericValue> entities) {
        if (entities.isEmpty()) {
            return false;
        }

        //loop through each issue security type to see if the user has access to the issue.
        //Once the permission is reached then we return right away
        for (SecurityType type : securityTypeManager.getTypes().values()) {
            for (GenericValue perm : filter(entities, withField(PERM_TYPE, type.getType()))) {
                if (perm != null && hasPermission(issue, user, type, perm)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean hasPermission(Issue issue, ApplicationUser user, SecurityType type, GenericValue perm) {
        return (user == null)
                ? type.hasPermission(issue, perm.getString(PERM_PARAMETER))
                : type.hasPermission(issue, perm.getString(PERM_PARAMETER), user, false);
    }

    @Override
    protected AbstractSchemeUpdatedEvent createSchemeUpdatedEvent(final Scheme scheme, final Scheme originalScheme) {
        return new IssueSecuritySchemeUpdatedEvent(scheme, originalScheme);
    }

    /**
     * Deletes a scheme from the database
     *
     * @param id Id of the scheme to be deleted
     * @throws GenericEntityException
     */
    @Override
    public void deleteScheme(Long id) throws GenericEntityException {
        try {
            GenericValue scheme = getScheme(id);
            nodeAssociationStore.removeAssociationsFromSink(scheme);
            scheme.removeRelated("Child" + getEntityName());
            // Cache for this gets reset by the IssueSecuritySchemeDeletedEvent fired below.
            scheme.removeRelated("Child" + "SchemeIssueSecurityLevels");
            scheme.remove();

            eventPublisher.publish(new IssueSecuritySchemeDeletedEvent(id));
        } finally {
            clearCache();
        }
    }

    @Override
    public void deleteEntity(Long id) throws DataAccessException {
        deleteEntities(Collections.singleton(id));
    }

    @Override
    public void deleteEntities(final Iterable<Long> ids) {
        try {
            super.deleteEntities(ids);
            for (Long id : ids) {
                eventPublisher.publish(new IssueSecurityLevelDeletedEvent(id));
            }
        } finally {
            clearCache();
        }
    }

    @Override
    protected SchemeEntity makeSchemeEntity(final GenericValue entity) {
        return new SchemeEntity(entity.getString(PERM_TYPE), entity.getString(PERM_PARAMETER), entity.getLong("security"));
    }

    @Override
    protected Object createSchemeEntityDeletedEvent(final GenericValue entity) {
        return null;
    }

    @Override
    public boolean removeEntities(GenericValue scheme, Long entityTypeId) throws RemoveException {
        try {
            return super.removeEntities(scheme, entityTypeId);
        } finally {
            clearCache();
        }
    }

    @Override
    public GenericValue createScheme(String name, String description) throws GenericEntityException {
        try {
            return super.createScheme(name, description);
        } finally {
            clearCache();
        }
    }

    @Override
    protected AbstractSchemeEvent createSchemeCreatedEvent(final Scheme scheme) {
        return new IssueSecuritySchemeCreatedEvent(scheme);
    }

    @Override
    protected void flushProjectSchemes() {
        try {
            super.flushProjectSchemes();
        } finally {
            clearCache();
        }
    }

    /**
     * This method overrides the super implemntation in order to clear cache.
     *
     * @param type      type
     * @param parameter parameter
     * @return the original result of the call to super method
     * @throws RemoveException if super method throws it
     */
    @Override
    public boolean removeEntities(String type, String parameter) throws RemoveException {
        try {
            return super.removeEntities(type, parameter);
        } finally {
            clearCache();
        }
    }

    @Override
    @Nonnull
    protected AbstractSchemeAddedToProjectEvent createSchemeAddedToProjectEvent(final Scheme scheme, final Project project) {
        return new IssueSecuritySchemeAddedToProjectEvent(scheme, project);
    }

    @Override
    @Nonnull
    protected AbstractSchemeRemovedFromProjectEvent createSchemeRemovedFromProjectEvent(final Scheme scheme, final Project project) {
        return new IssueSecuritySchemeRemovedFromProjectEvent(scheme, project);
    }

    private static List<GenericValue> filter(List<GenericValue> list, Predicate<GenericValue> predicate) {
        return ImmutableList.copyOf(list.stream().filter(predicate).collect(Collectors.toList()));
    }

    private static Predicate<GenericValue> withField(final String field, final String value) {
        if (value == null) {
            return input -> input.getString(field) == null;
        }
        return input -> value.equals(input.getString(field));
    }

    @Override
    public boolean isGroupUsed(@Nonnull final Group group) {
        final Collection<GenericValue> entities = ofBizDelegator.findByAnd(ISSUE_SECURITY_ENTITY_NAME, FieldMap.build(
                PERM_TYPE, GroupDropdown.DESC,
                PERM_PARAMETER, group.getName()));
        return entities.size() > 0;
    }

    private List<GenericValue> loadPermissionsBySecurityLevel(final CacheKey cacheKey) {
        return ofBizDelegator.findByAnd(getEntityName(), FieldMap.build(SCHEME, cacheKey.id));
    }

    private List<IssueSecurityLevelPermission> loadSecuritiesBySchemeId(final Long cacheKey) {
        return Select.from(Entity.ISSUE_SECURITY_LEVEL_PERMISSION).whereEqual("security", cacheKey).runWith(ofBizDelegator).asList();
    }

    static class CacheKey implements Serializable {
        final Long id;

        CacheKey(final Long id) {
            this.id = id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final CacheKey other = (CacheKey) o;
            return (id == null) ? (other.id == null) : id.equals(other.id);
        }

        @Override
        public int hashCode() {
            return (id != null) ? id.hashCode() : 0;
        }
    }
}

