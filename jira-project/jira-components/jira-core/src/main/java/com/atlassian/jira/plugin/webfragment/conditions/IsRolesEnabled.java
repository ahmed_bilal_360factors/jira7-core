package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.annotations.Internal;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

/**
 * A condition that determines whether application roles are enabled.
 * Will return shouldDisplay of true if roles are enabled.
 * <pre>
 * *******************************
 * This is in the DMZ; search for usages before removing it. ProjectConfigPlugin is one known user.
 * *******************************
 * </pre>
 * @since 7.0
 * @deprecated this always returns true, so there's no need to use it, since 7.0.1
 */
@Deprecated
@Internal
public class IsRolesEnabled implements Condition {

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {
    }

    @Override
    public boolean shouldDisplay(final Map<String, Object> context) {
        return true;
    }
}
