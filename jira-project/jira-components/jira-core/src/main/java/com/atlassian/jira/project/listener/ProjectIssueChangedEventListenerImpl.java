package com.atlassian.jira.project.listener;

import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.project.ProjectChangedTimeStore;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import static com.atlassian.jira.event.type.EventType.ISSUE_MOVED_ID;

//@EventComponent
public class ProjectIssueChangedEventListenerImpl implements ProjectIssueChangedEventListener {
    private static final String PROJECT_FIELD_NAME = "project";

    private final ProjectChangedTimeStore projectChangedTimeStore;
    private final ChangeHistoryManager changeHistoryManager;

    public ProjectIssueChangedEventListenerImpl(
            ProjectChangedTimeStore projectChangedTimeStore,
            ChangeHistoryManager changeHistoryManager
    ) {
        this.projectChangedTimeStore = projectChangedTimeStore;
        this.changeHistoryManager = changeHistoryManager;
    }

    @Override
    /*
    This causes a deadlock when parent issue creation thread spawns child threads for creation of more issues.
    Will have to re-add this with only delete and moved as the allowed event types.
    JCB-210 is tracking this issue.
    @EventListener
    */
    public void listenForIssueChangedEvent(IssueEvent issueEvent) {
        Long issueEventType = issueEvent.getEventTypeId();
        if (isAllowedIssueChangedEvent(issueEventType)) {
            long projectId = issueEvent.getProject().getId();
            Timestamp issueChangedTime = getIssueChangedTime(issueEvent);
            projectChangedTimeStore.updateOrAddIssueChangedTime(projectId, issueChangedTime);

            if (isIssueMoveEvent(issueEventType)) {
                getSourceProjectId(issueEvent).ifPresent(
                        sourceProjectId -> projectChangedTimeStore.updateOrAddIssueChangedTime(sourceProjectId, issueChangedTime)
                );
            }
        }
    }

    private Timestamp getIssueChangedTime(IssueEvent issueEvent) {
        return new Timestamp(issueEvent.getTime().getTime());
    }

    private boolean isAllowedIssueChangedEvent(Long eventTypeId) {
        return ProjectIssueChangedEventAllowedTypes.eventTypes.contains(eventTypeId);
    }

    private Optional<Long> getSourceProjectId(IssueEvent moveIssueEvent) {
        List<ChangeItemBean> changesToProjectField = changeHistoryManager.getChangeItemsForField(moveIssueEvent.getIssue(), PROJECT_FIELD_NAME);

        return getLast(changesToProjectField)
                .map(projectFieldChange -> Long.valueOf(projectFieldChange.getFrom()));
    }

    private <T> Optional<T> getLast(List<T> list) {
        return list.isEmpty() ? Optional.empty() : Optional.of(list.get(list.size() - 1));
    }

    private boolean isIssueMoveEvent(Long eventTypeId) {
        return eventTypeId.equals(ISSUE_MOVED_ID);
    }
}
