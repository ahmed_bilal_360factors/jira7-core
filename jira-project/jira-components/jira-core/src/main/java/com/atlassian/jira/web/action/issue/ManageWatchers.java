package com.atlassian.jira.web.action.issue;

import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.exception.IssueNotFoundException;
import com.atlassian.jira.exception.IssuePermissionException;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.web.component.multiuserpicker.UserPickerLayoutBean;
import com.atlassian.jira.web.component.multiuserpicker.UserPickerWebComponent;
import org.ofbiz.core.entity.GenericEntityException;
import webwork.action.ParameterAware;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.security.Permissions.MANAGE_WATCHER_LIST;
import static com.atlassian.jira.security.Permissions.VIEW_VOTERS_AND_WATCHERS;
import static org.apache.commons.lang.StringUtils.isBlank;

public class ManageWatchers extends AbstractIssueSelectAction implements ParameterAware {
    private final WatcherManager watcherManager;
    private final VelocityTemplatingEngine templatingEngine;
    private final UserSearchService searchService;
    private final PermissionManager permissionManager;
    private final UserUtil userUtil;

    private static final String NOWATCHING = "watchingnotenabled";
    public static final String REMOVE_WATCHERS_PREFIX = "stopwatch_";

    private String userNames;
    private Map params;

    public ManageWatchers(final WatcherManager watcherManager, final VelocityTemplatingEngine templatingEngine,
                          final UserSearchService searchService, final PermissionManager permissionManager,
                          final UserUtil userUtil) {
        this.watcherManager = watcherManager;
        this.templatingEngine = templatingEngine;
        this.searchService = searchService;
        this.permissionManager = permissionManager;
        this.userUtil = userUtil;
    }

    public String doDefault() throws Exception {
        if (!isWatchingEnabled()) {
            return NOWATCHING;
        }

        try {
            if (!isCanViewWatcherList()) {
                return "securitybreach";
            }
        } catch (IssueNotFoundException e) {
            // Error is added above
            return ISSUE_PERMISSION_ERROR;
        } catch (IssuePermissionException e) {
            return ISSUE_PERMISSION_ERROR;
        }

        return super.doDefault();
    }

    public String getUserPickerHtml() throws GenericEntityException {
        final UserPickerLayoutBean layout = getManageWatchersLayout();
        final boolean canEdit = isCanEditWatcherList();
        final Long issueId = getIssueObject().getId();
        final List<String> usernames = watcherManager.getCurrentWatcherUsernames(getIssueObject());
        return new UserPickerWebComponent(templatingEngine, getApplicationProperties(), searchService)
                .getHtmlForUsernames(layout, usernames, canEdit, issueId);
    }

    public boolean isWatchingEnabled() {
        return getApplicationProperties().getOption(APKeys.JIRA_OPTION_WATCHING);
    }

    public boolean isWatching() {
        return watcherManager.isWatching(getLoggedInUser(), getIssueObject());
    }

    // Start the current user watching this issue
    @RequiresXsrfCheck
    public String doStartWatching() throws GenericEntityException {
        try {
            watcherManager.startWatching(getLoggedInUser(), getIssueObject());
        } catch (IssueNotFoundException e) {
            return ISSUE_PERMISSION_ERROR;
        } catch (IssuePermissionException e) {
            return ISSUE_PERMISSION_ERROR;
        }
        return getRedirect("ManageWatchers!default.jspa?id=" + getId());
    }

    // Stop the current user watching this issue
    @RequiresXsrfCheck
    public String doStopWatching() throws GenericEntityException {
        try {
            setIssueObject(watcherManager.stopWatching(getLoggedInUser(), getIssueObject()));
        } catch (IssueNotFoundException e) {
            return ISSUE_PERMISSION_ERROR;
        } catch (IssuePermissionException e) {
            return ISSUE_PERMISSION_ERROR;
        }
        return getRedirect("ManageWatchers!default.jspa?id=" + getId());
    }

    // Stop the user with the specified username watching this issue
    private String stopUserWatching(String username) {
        if (isBlank(username)) {
            addErrorMessage(getText("watcher.error.selectuser"));
            return ERROR;
        }
        setIssueObject(watcherManager.stopWatching(userUtil.getUserByName(username), getIssueObject()));
        userNames = null;
        return INPUT;

    }

    // Stop the specified users watching this issue
    @RequiresXsrfCheck
    public String doStopWatchers() throws GenericEntityException {
        try {
            // Require the MANAGE_WATCHER_LIST permission to remove other users from the watch list
            if (!isCanEditWatcherList()) {
                return "securitybreach";
            }
        } catch (IssueNotFoundException e) {
            return ISSUE_PERMISSION_ERROR;
        } catch (IssuePermissionException e) {
            return ISSUE_PERMISSION_ERROR;
        }

        final Collection<String> userNames = UserPickerWebComponent.getUserNamesToRemove(params, REMOVE_WATCHERS_PREFIX);
        for (final String userName : userNames) {
            stopUserWatching(userName);
        }
        return getRedirect("ManageWatchers!default.jspa?id=" + getId());
    }

    // Start the specified users watching this issue
    @RequiresXsrfCheck
    public String doStartWatchers() throws GenericEntityException {
        try {
            // Require the MANAGE_WATCHER_LIST permission to add other users to the watch list
            if (!isCanEditWatcherList()) {
                return "securitybreach";
            }
        } catch (IssueNotFoundException e) {
            return ISSUE_PERMISSION_ERROR;
        } catch (IssuePermissionException e) {
            return ISSUE_PERMISSION_ERROR;
        }

        final Collection<String> userNames = UserPickerWebComponent.getUserNamesToAdd(getUserNames());
        if (userNames.isEmpty()) {
            addErrorMessage(getText("watcher.error.selectuser"));
            return ERROR;
        }

        boolean badUsersFound = false;
        for (final String userName : userNames) {
            final ApplicationUser user = getUser(userName);
            if (user != null) {
                if (isUserPermittedToSeeIssue(user)) {
                    setIssueObject(watcherManager.startWatching(user, getIssueObject()));
                } else {
                    badUsersFound = true;
                    addErrorMessage(getText("watcher.error.user.cant.see.issue", userName));
                }
            } else {
                badUsersFound = true;
                addErrorMessage(getText("watcher.error.usernotfound", userName));
            }
        }

        if (badUsersFound) {
            setUserNames(null);
            return ERROR;
        } else {
            return getRedirect("ManageWatchers!default.jspa?id=" + getId());
        }
    }

    private boolean isUserPermittedToSeeIssue(final ApplicationUser user) {
        System.out.println("This is predict code ..................................");
		System.out.println("isUserPermittedToSeeIssue");
		return permissionManager.hasPermission(Permissions.BROWSE, getIssueObject().getProjectObject(), user);
    }

    private ApplicationUser getUser(String userName) {
        ApplicationUser user = userUtil.getUserByName(userName);
        if (user == null) {
            log.info("Unable to retrieve the user '" + userName + "' to add to watch list.");
        }
        return user;
    }

    public UserPickerLayoutBean getManageWatchersLayout() {
        return new UserPickerLayoutBean("watcher.manage", REMOVE_WATCHERS_PREFIX, "ManageWatchers!stopWatchers.jspa", "ManageWatchers!startWatchers.jspa");
    }

    public String getUserNames() {
        return userNames;
    }

    public void setUserNames(String userNames) {
        this.userNames = userNames;
    }

    // Used to name the checkboxes to determine the users to remove from the watch list
    public String getCheckboxName(ApplicationUser user) {
        return REMOVE_WATCHERS_PREFIX + user.getName();
    }

    public void setParameters(Map params) {
        this.params = params;
    }

    public Map getParams() {
        return params;
    }

    // Check permission to edit watcher list
    public boolean isCanEditWatcherList() throws GenericEntityException {
        return (permissionManager.hasPermission(MANAGE_WATCHER_LIST, getIssueObject(), getLoggedInUser()));
    }

    // Check permission to view watcher list
    public boolean isCanViewWatcherList() throws GenericEntityException {
        return (permissionManager.hasPermission(VIEW_VOTERS_AND_WATCHERS, getIssueObject(), getLoggedInUser())
                || isCanEditWatcherList());
    }

    public boolean isCanStartWatching() {
        return isWatchingEnabled() && getLoggedInUser() != null && !isWatching();
    }

    public boolean isCanStopWatching() {
        return isWatchingEnabled() && getLoggedInUser() != null && isWatching();
    }
}
