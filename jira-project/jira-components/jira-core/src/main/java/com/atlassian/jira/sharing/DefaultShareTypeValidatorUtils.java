package com.atlassian.jira.sharing;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.sharing.SharedEntity.SharePermissions;
import com.atlassian.jira.sharing.search.ShareTypeSearchParameter;
import com.atlassian.jira.sharing.type.ShareType;
import com.atlassian.jira.sharing.type.ShareTypeFactory;
import com.atlassian.jira.sharing.type.ShareTypeValidator;
import com.atlassian.jira.util.dbc.Assertions;

/**
 * Default implementation of {@link ShareTypeValidatorUtils}.
 *
 * @since v3.13
 */
public class DefaultShareTypeValidatorUtils implements ShareTypeValidatorUtils {
    private final ShareTypeFactory shareTypeFactory;
    private final PermissionManager permissionManager;

    public DefaultShareTypeValidatorUtils(final ShareTypeFactory shareTypeFactory, final PermissionManager permissionManager) {
        Assertions.notNull("shareTypeFactory", shareTypeFactory);
        Assertions.notNull("permissionManager", permissionManager);

        this.permissionManager = permissionManager;
        this.shareTypeFactory = shareTypeFactory;
    }

    @Deprecated
    public boolean isValidSharePermission(final JiraServiceContext context, final SharedEntity entity) {
        return hasValidSharePermissions(context, entity);
    }

    @Override
    public boolean hasValidSharePermissions(JiraServiceContext context, SharedEntity entity) {
        Assertions.notNull("entity", entity);
        final SharePermissions permissions = entity.getPermissions();
        Assertions.notNull("permissions", permissions);

        if (!permissions.isPrivate()) {
            final boolean hasPermission = permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, context.getLoggedInUser());
            if (!hasPermission) {
                final String userName = context.getLoggedInUser() != null ? context.getLoggedInUser().getDisplayName() : context.getI18nBean().getText("common.words.Anonymous");
                addErrorToContext(context, ShareTypeValidator.ERROR_KEY, "common.sharing.exception.no.share.permission");
                addErrorToContext(context, ShareTypeValidator.DELEGATED_ERROR_KEY, "common.sharing.exception.delegated.user.no.share.permission", userName);
                return false;
            }

            for (final SharePermission sharePermission : permissions) {
                isValidSharePermission(context, sharePermission, permissions.size());
            }
        }

        return !context.getErrorCollection().hasAnyErrors();
    }

    private boolean isValidSharePermission(JiraServiceContext context, SharePermission sharePermission, long entityPermissionCount) {
        final ShareType type = shareTypeFactory.getShareType(sharePermission.getType());
        if (type == null) {
            addErrorToContext(context, ShareTypeValidator.ERROR_KEY, "common.sharing.exception.unknown.type", sharePermission.getType());
        } else {
            if (type.isSingleton() && (entityPermissionCount > 1)) {
                addErrorToContext(context, ShareTypeValidator.ERROR_KEY, "common.sharing.exception.singleton", sharePermission.getType());
            }
            type.getValidator().checkSharePermission(context, sharePermission);
        }
        return !context.getErrorCollection().hasAnyErrors();
    }

    public boolean isValidSearchParameter(final JiraServiceContext context, final ShareTypeSearchParameter searchParameter) {
        Assertions.notNull("context", context);
        Assertions.notNull("searchParameter", searchParameter);

        final ShareType.Name shareType = searchParameter.getType();
        final ShareType type = shareTypeFactory.getShareType(shareType);
        if (type == null) {
            addErrorToContext(context, ShareTypeValidator.ERROR_KEY, "common.sharing.exception.unknown.type", shareType);
            return false;
        }
        return type.getValidator().checkSearchParameter(context, searchParameter);
    }

    private void addErrorToContext(JiraServiceContext context, String field, String message, Object... params) {
        String text = context.getI18nBean().getText(message, params);
        context.getErrorCollection().addError(field, text);
    }
}
