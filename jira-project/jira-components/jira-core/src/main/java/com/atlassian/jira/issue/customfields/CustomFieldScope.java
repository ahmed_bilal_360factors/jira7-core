package com.atlassian.jira.issue.customfields;

public class CustomFieldScope {
    public static final String GLOBAL = "global";
    public static final String ISSUETYPE = "issuetype";
    public static final String PROJECT = "project";
}
