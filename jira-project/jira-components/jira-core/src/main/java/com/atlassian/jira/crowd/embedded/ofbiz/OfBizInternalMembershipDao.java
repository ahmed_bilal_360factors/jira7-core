package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.event.migration.XMLRestoreFinishedEvent;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizListIterator;
import com.atlassian.jira.util.cache.WeakInterner;
import com.google.common.collect.ImmutableList;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.StringPath;
import org.ofbiz.core.entity.EntityCondition;
import org.ofbiz.core.entity.EntityConditionList;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static com.atlassian.crowd.model.membership.MembershipType.GROUP_GROUP;
import static com.atlassian.crowd.model.membership.MembershipType.GROUP_USER;
import static com.atlassian.jira.crowd.embedded.ofbiz.PrimitiveMap.builder;
import static com.atlassian.jira.model.querydsl.QMembership.MEMBERSHIP;
import static com.atlassian.jira.util.cache.WeakInterner.newWeakInterner;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.querydsl.core.types.ExpressionUtils.allOf;
import static com.querydsl.core.types.ExpressionUtils.anyOf;
import static org.ofbiz.core.entity.EntityFindOptions.findOptions;
import static org.ofbiz.core.entity.EntityUtil.getOnly;


public class OfBizInternalMembershipDao implements InternalMembershipDao {
    private static final ImmutableList<String> FIELD_LIST_LOWERCHILDNAME = ImmutableList.of(MembershipEntity.LOWER_CHILD_NAME);
    private static final ImmutableList<String> FIELD_LIST_LOWERPARENTNAME = ImmutableList.of(MembershipEntity.LOWER_PARENT_NAME);
    private static final LinkedHashSet<String> EMPTY = new LinkedHashSet<>();

    private final OfBizDelegator ofBizDelegator;
    private final QueryDslAccessor queryDslAccessor;
    // The groups that a user belongs to. Iteration over LinkedHasSet is naturally sorted for this cache
    private final Cache<MembershipKey, LinkedHashSet<String>> parentsCache;
    // The users that belongs to a group. Iteration over LinkedHasSet is naturally sorted for this cache
    private final Cache<MembershipKey, LinkedHashSet<String>> childrenCache;
    private WeakInterner<String> myInterner = newWeakInterner();

    public OfBizInternalMembershipDao(final OfBizDelegator ofBizDelegator, final QueryDslAccessor queryDslAccessor, final CacheManager cacheManager) {
        this.ofBizDelegator = ofBizDelegator;
        this.queryDslAccessor = queryDslAccessor;
        parentsCache = cacheManager.getCache(OfBizInternalMembershipDao.class.getName() + ".parentsCache",
                new ParentsLoader(),
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).flushable().build());
        childrenCache = cacheManager.getCache(OfBizInternalMembershipDao.class.getName() + ".childrenCache",
                new ChildrenLoader(),
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).flushable().build());
    }

    /**
     * Test if this query can be satisfied from the cache.
     *
     * @param query Query
     * @return True if can be satisfied from the cache
     */
    private static boolean canUseCacheSearch(final MembershipQuery<?> query) {
        // We can use the cache if there are no limits on the search
        return query.getStartIndex() == 0 && query.getMaxResults() == EntityQuery.ALL_RESULTS;
    }

    public boolean isUserDirectMember(final long directoryId, final String userName, final String groupName) {
        return isDirectMember(directoryId, GROUP_USER, groupName, userName);
    }

    public boolean isGroupDirectMember(final long directoryId, final String childGroup, final String parentGroup) {
        return isDirectMember(directoryId, GROUP_GROUP, parentGroup, childGroup);
    }

    private boolean isDirectMember(final long directoryId, final MembershipType membershipType, final String parentName, final String childName) {
        // We look in the parent cache because typically results in a much shorter list than the looking via the children cache.
        Set<String> parents = parentsCache.get(MembershipKey.getKey(directoryId, childName, membershipType));
        // Look first for the case we have.  Then try for the lower case.
        // This is an optimisation that should work in most JIRA instances where group and user names are really all lower case.
        return parents != null && (parents.contains(parentName) || parents.contains(toLowerCase(parentName)));
    }

    public void addUserToGroup(final long directoryId, final UserOrGroupStub user, final UserOrGroupStub group) throws MembershipAlreadyExistsException {
        if (isDirectMember(directoryId, GROUP_USER, group.getName(), user.getName())) {
            throw new MembershipAlreadyExistsException(directoryId, user.getName(), group.getName());
        }

        try {
            createMembership(directoryId, GROUP_USER, group, user);
        } catch (DataAccessException e) {
            // If we have a race we could end up with a duplicate key exception, exposed here.
            if (isDirectMember(directoryId, GROUP_USER, group.getName(), user.getName())) {
                throw new MembershipAlreadyExistsException(directoryId, user.getName(), group.getName());
            }
            throw e;
        }
        invalidateChildrenCacheEntry(directoryId, group.getName(), MembershipType.GROUP_USER);
        invalidateParentsCacheEntry(directoryId, user.getName(), MembershipType.GROUP_USER);
    }

    public BatchResult<String> addAllUsersToGroup(long directoryId, Collection<UserOrGroupStub> users, UserOrGroupStub group) {
        // We get the list of children this way because of a seen performance problem when doing initial full
        // synchronizations of Remote Directories, rather than using the isDirectMemberOf methods which cause cache thrashing.
        Set<String> currentUsers = getChildrenOfGroupFromCache(directoryId, group.getName(), MembershipType.GROUP_USER);

        BatchResult<String> result = new BatchResult<>(users.size());
        boolean groupIsDirty = false;

        for (UserOrGroupStub user : users) {
            try {
                if (currentUsers.contains(user.getLowerName())) {
                    result.addFailure(user.getName());
                } else {
                    createMembership(directoryId, GROUP_USER, group, user);
                    invalidateParentsCacheEntry(directoryId, user.getName(), MembershipType.GROUP_USER);
                    groupIsDirty = true;
                }
                result.addSuccess(user.getName());
            } catch (DataAccessException e) {
                // If we come across any database errors we want to continue processing all other users
                result.addFailure(user.getName());
            }
        }

        if (groupIsDirty) {
            invalidateChildrenCacheEntry(directoryId, group.getName(), MembershipType.GROUP_USER);
        }
        return result;
    }

    private void createMembership(final long directoryId, final MembershipType membershipType, final UserOrGroupStub parent, final UserOrGroupStub child) {
        ofBizDelegator.createValue("Membership", builder()
                .put("directoryId", directoryId)
                .put("childId", child.getId())
                .put("childName", child.getName())
                .put("lowerChildName", child.getLowerName())
                .put("parentId", parent.getId())
                .put("parentName", parent.getName())
                .put("lowerParentName", parent.getLowerName())
                .put("membershipType", membershipType.name())
                .build());
    }

    public void addGroupToGroup(final long directoryId, final UserOrGroupStub child, final UserOrGroupStub parent) {
        if (!isDirectMember(directoryId, GROUP_GROUP, parent.getName(), child.getName())) {
            createMembership(directoryId, GROUP_GROUP, parent, child);
            invalidateChildrenCacheEntry(directoryId, parent.getName(), MembershipType.GROUP_GROUP);
            invalidateParentsCacheEntry(directoryId, child.getName(), MembershipType.GROUP_GROUP);
        }
    }

    public void removeUserFromGroup(final long directoryId, final UserOrGroupStub user, final UserOrGroupStub group)
            throws MembershipNotFoundException {
        removeMembership(directoryId, GROUP_USER, group, user);
        invalidateChildrenCacheEntry(directoryId, group.getName(), MembershipType.GROUP_USER);
        invalidateParentsCacheEntry(directoryId, user.getName(), MembershipType.GROUP_USER);
    }

    private void removeMembership(final long directoryId, final MembershipType membershipType, final UserOrGroupStub parent, final UserOrGroupStub child)
            throws MembershipNotFoundException {
        final GenericValue membershipGenericValue = getOnly(ofBizDelegator.findByAnd(MembershipEntity.ENTITY, builder()
                .put("directoryId", directoryId)
                .put("childId", child.getId())
                .put("parentId", parent.getId())
                .put("membershipType", membershipType.name())
                .build()));

        if (membershipGenericValue == null) {
            throw new MembershipNotFoundException(child.getName(), parent.getName());
        }
        ofBizDelegator.removeValue(membershipGenericValue);
    }

    public void removeGroupFromGroup(final long directoryId, final UserOrGroupStub childGroup, final UserOrGroupStub parentGroup)
            throws MembershipNotFoundException {
        try {
            removeMembership(directoryId, GROUP_GROUP, parentGroup, childGroup);
        } finally {
            invalidateChildrenCacheEntry(directoryId, parentGroup.getName(), MembershipType.GROUP_GROUP);
            invalidateParentsCacheEntry(directoryId, childGroup.getName(), MembershipType.GROUP_GROUP);
        }
    }

    @Override
    public long countDirectMembersOfGroup(long directoryId, String groupName, MembershipType type) {
        return Select.from(MembershipEntity.ENTITY)
                .whereEqual(MembershipEntity.DIRECTORY_ID, directoryId)
                .andEqual(MembershipEntity.LOWER_PARENT_NAME, toLowerCase(groupName))
                .andEqual(MembershipEntity.MEMBERSHIP_TYPE, type.name())
                .runWith(ofBizDelegator)
                .count();
    }

    public void removeAllMembersFromGroup(final Group group) {
        //remove group parent from all caches
        Stream<String> users = getChildrenOfGroupFromCache(group.getDirectoryId(), group.getName(), MembershipType.GROUP_USER).stream();
        Stream<String> childGroups = getChildrenOfGroupFromCache(group.getDirectoryId(), group.getName(), MembershipType.GROUP_GROUP).stream();

        try {
            ofBizDelegator.removeByAnd(MembershipEntity.ENTITY, builder()
                    .put(MembershipEntity.DIRECTORY_ID, group.getDirectoryId())
                    .put(MembershipEntity.PARENT_NAME, group.getName())
                    .build());
        } finally {
            users.forEach(userName -> {
                invalidateChildrenCacheEntry(group.getDirectoryId(), group.getName(), MembershipType.GROUP_USER);
                invalidateParentsCacheEntry(group.getDirectoryId(), userName, MembershipType.GROUP_USER);
            });
            childGroups.forEach(childName -> {
                invalidateChildrenCacheEntry(group.getDirectoryId(), group.getName(), MembershipType.GROUP_GROUP);
                invalidateParentsCacheEntry(group.getDirectoryId(), childName, MembershipType.GROUP_GROUP);
            });
        }
    }

    public void removeAllGroupMemberships(final Group group) {
        Stream<String> parentGroups = getParentsForMemberFromCache(group.getDirectoryId(), group.getName(), MembershipType.GROUP_GROUP).stream();

        try {
            ofBizDelegator.removeByAnd(MembershipEntity.ENTITY, builder()
                    .put(MembershipEntity.DIRECTORY_ID, group.getDirectoryId())
                    .put(MembershipEntity.MEMBERSHIP_TYPE, GROUP_GROUP.name())
                    .put(MembershipEntity.CHILD_NAME, group.getName())
                    .build());
        } finally {
            parentGroups.forEach(parentName -> {
                invalidateChildrenCacheEntry(group.getDirectoryId(), parentName, MembershipType.GROUP_GROUP);
                invalidateParentsCacheEntry(group.getDirectoryId(), group.getName(), MembershipType.GROUP_GROUP);
            });
        }
    }

    @Override
    public void removeAllUserMemberships(final User user) {
        removeAllUserMemberships(user.getDirectoryId(), user.getName());
    }

    @Override
    public void removeAllUserMemberships(final long directoryId, final String username) {
        Stream<String> groups = getParentsForMemberFromCache(directoryId, username, MembershipType.GROUP_USER).stream();

        try {
            ofBizDelegator.removeByAnd(MembershipEntity.ENTITY, builder()
                    .put(MembershipEntity.DIRECTORY_ID, directoryId)
                    .put(MembershipEntity.MEMBERSHIP_TYPE, GROUP_USER.name())
                    .put(MembershipEntity.CHILD_NAME, username)
                    .build());
        } finally {
            groups.forEach(groupName -> {
                invalidateChildrenCacheEntry(directoryId, groupName, MembershipType.GROUP_USER);
                invalidateParentsCacheEntry(directoryId, username, MembershipType.GROUP_USER);
            });
        }
    }

    @Override
    public List<String> findGroupChildrenOfGroups(final long directoryId, final Collection<String> groupNames) {
        return findChildrenOfGroupsGeneric(directoryId, groupNames, MembershipType.GROUP_GROUP);
    }

    @Override
    public List<String> findUserChildrenOfGroups(final long directoryId, final Collection<String> groupNames) {
        return findChildrenOfGroupsGeneric(directoryId, groupNames, MembershipType.GROUP_USER);
    }

    private List<String> findChildrenOfGroupsGeneric(final long directoryId, final Collection<String> groupNames, final MembershipType relationship) {
        //No groups to read in this directory, don't have to go to the database for that
        if (groupNames.isEmpty()) {
            return Collections.emptyList();
        }

        List<String> adjustedGroupNames = groupNames.stream().map(IdentifierUtils::toLowerCase).collect(Collectors.toList());

        EntityExpr directoryCondition = new EntityExpr(MembershipEntity.DIRECTORY_ID, EntityOperator.EQUALS, directoryId);
        EntityExpr parentCondition = new EntityExpr(MembershipEntity.LOWER_PARENT_NAME, EntityOperator.IN, adjustedGroupNames);
        EntityExpr typeCondition = new EntityExpr(MembershipEntity.MEMBERSHIP_TYPE, EntityOperator.EQUALS, relationship.name());

        EntityCondition condition = new EntityConditionList(ImmutableList.of(directoryCondition, parentCondition, typeCondition), EntityOperator.AND);

        OfBizListIterator memberships = ofBizDelegator.findListIteratorByCondition(MembershipEntity.ENTITY, condition,
                null, FIELD_LIST_LOWERCHILDNAME, FIELD_LIST_LOWERCHILDNAME, findOptions().distinct());
        try {
            List<String> children = new ArrayList<>(4096);
            GenericValue membership = memberships.next();
            while (membership != null) {
                final String child = membership.getString(MembershipEntity.LOWER_CHILD_NAME);
                children.add(myInterner.intern(child));
                membership = memberships.next();
            }
            return children;
        } finally {
            memberships.close();
        }
    }

    public <T> List<String> search(final long directoryId, final MembershipQuery<T> query) {
        // Optimisation to use the cache if we can.  This is possible if there are no bounding restrictions
        if (canUseCacheSearch(query)) {
            return searchCache(directoryId, query);
        }


        final ImmutableList.Builder<Predicate> andFilter = ImmutableList.builder();
        final ImmutableList.Builder<Predicate> orFilter = ImmutableList.builder();

//        andFilter.put("directoryId", Long.toString(directoryId));
        andFilter.add(MEMBERSHIP.directoryId.eq(directoryId));
        if (query.getEntityToReturn().equals(EntityDescriptor.user()) || query.getEntityToMatch().equals(EntityDescriptor.user())) {
            andFilter.add(MEMBERSHIP.membershipType.eq(GROUP_USER.name()));
        } else {
            andFilter.add(MEMBERSHIP.membershipType.eq(GROUP_GROUP.name()));
        }

        final StringPath column;
        final StringPath constraintName;
        if (query.isFindChildren()) {
            constraintName = MEMBERSHIP.lowerParentName;
            column = MEMBERSHIP.lowerChildName;
        } else {
            constraintName = MEMBERSHIP.lowerChildName;
            column = MEMBERSHIP.lowerParentName;
        }
        for (String name : query.getEntityNamesToMatch()) {
            orFilter.add(constraintName.eq(IdentifierUtils.toLowerCase(name)));
        }

        return findMemberships(column, andFilter.build(), orFilter.build(), query.getStartIndex(), query.getMaxResults());
    }

    /**
     * Search the cache to satisfy ths query.
     * We assume the caller has established this is valid to do.
     *
     * @param directoryId Directory
     * @param query       Query
     * @return List of results
     */
    private List<String> searchCache(final long directoryId, final MembershipQuery<?> query) {
        final MembershipType type;
        if (query.getEntityToReturn().equals(EntityDescriptor.user()) || query.getEntityToMatch().equals(EntityDescriptor.user())) {
            type = MembershipType.GROUP_USER;
        } else {
            type = MembershipType.GROUP_GROUP;
        }

        final Function<String, Stream<? extends String>> generator;
        if (query.isFindChildren()) {
            // This is get members of group
            generator = entityName -> getChildrenOfGroupFromCache(directoryId, entityName, type).stream();

        } else {
            // This is get groups a user is a member of
            generator = entityName -> getParentsForMemberFromCache(directoryId, entityName, type).stream();
        }

        final Set<String> entityNamesToMatch = query.getEntityNamesToMatch();
        if (entityNamesToMatch.size() == 1) {
            return generator.apply(getOnlyElement(entityNamesToMatch)).collect(Collectors.toList());
        } else {
            return entityNamesToMatch.stream()
                    .sorted()
                    .distinct()
                    .collect(Collectors.toList());
        }
    }

    /**
     * Find the groups a user is a member of from the cache.
     *
     * @param directoryId The directory Id
     * @param childName   The child name (either a user or a sub-group)
     * @param type        The kind of membership to check
     * @return A list of lower-case Group names
     */
    private LinkedHashSet<String> getParentsForMemberFromCache(final long directoryId, final String childName, final MembershipType type) {
        return getFromCache(parentsCache, directoryId, childName, type);
    }

    /**
     * Get the Members of a Group from the cache.
     *
     * @param directoryId The directory Id
     * @param groupName   The group name
     * @param type        The kind of membership to check
     * @return A list of lower-case member names.
     */
    private LinkedHashSet<String> getChildrenOfGroupFromCache(final long directoryId, final String groupName, final MembershipType type) {
        return getFromCache(childrenCache, directoryId, groupName, type);
    }

    private LinkedHashSet<String> getFromCache(final Cache<MembershipKey, LinkedHashSet<String>> cache, final long directoryId, final String name, final MembershipType type) {
        final LinkedHashSet<String> values = cache.get(MembershipKey.getKey(directoryId, name, type));
        if (values == null) {
            return EMPTY;
        }

        return values;
    }

    private List<String> findMemberships(final StringPath column, final List<Predicate> andFilters, final List<Predicate> orFilters, final int offset, final int maxResults) {
        return queryDslAccessor.executeQuery(dbConnection -> dbConnection.newSqlQuery()
                .select(column)
                .from(MEMBERSHIP)
                .where(allOf(
                        allOf(andFilters),
                        anyOf(orFilters))
                )
                .orderBy(column.asc())
                .distinct()
                .offset(offset)
                .limit(maxResults)
                .fetch());

    }

    private LinkedHashSet<String> findChildren(final long directoryId, final MembershipType membershipType, final String parent) {
        EntityExpr directoryCondition = new EntityExpr(MembershipEntity.DIRECTORY_ID, EntityOperator.EQUALS, directoryId);
        EntityExpr parentCondition = new EntityExpr(MembershipEntity.LOWER_PARENT_NAME, EntityOperator.EQUALS, parent);
        EntityExpr typeCondition = new EntityExpr(MembershipEntity.MEMBERSHIP_TYPE, EntityOperator.EQUALS, membershipType.name());

        EntityCondition condition = new EntityConditionList(ImmutableList.of(directoryCondition, parentCondition, typeCondition), EntityOperator.AND);

        OfBizListIterator memberships = ofBizDelegator.findListIteratorByCondition(MembershipEntity.ENTITY, condition,
                null, FIELD_LIST_LOWERCHILDNAME, FIELD_LIST_LOWERCHILDNAME, findOptions().distinct());
        try {
            // The large size hint is to prevent multiple rebuilds of the collection for large result sets.  The
            // collection is ephemeral, so the memory it temporarily wastes shouldn't matter.
            List<String> children = new ArrayList<>(4096);
            GenericValue membership = memberships.next();
            while (membership != null) {
                final String child = membership.getString(MembershipEntity.LOWER_CHILD_NAME);
                children.add(myInterner.intern(child));
                membership = memberships.next();
            }
            return new LinkedHashSet<>(children);
        } finally {
            memberships.close();
        }
    }

    private LinkedHashSet<String> findParents(final long directoryId, final MembershipType membershipType, final String child) {
        EntityExpr directoryCondition = new EntityExpr(MembershipEntity.DIRECTORY_ID, EntityOperator.EQUALS, directoryId);
        EntityExpr childCondition = new EntityExpr(MembershipEntity.LOWER_CHILD_NAME, EntityOperator.EQUALS, child);
        EntityExpr typeCondition = new EntityExpr(MembershipEntity.MEMBERSHIP_TYPE, EntityOperator.EQUALS, membershipType.name());

        EntityCondition condition = new EntityConditionList(ImmutableList.of(directoryCondition, childCondition, typeCondition), EntityOperator.AND);

        OfBizListIterator memberships = ofBizDelegator.findListIteratorByCondition(MembershipEntity.ENTITY, condition,
                null, FIELD_LIST_LOWERPARENTNAME, FIELD_LIST_LOWERPARENTNAME, findOptions().distinct());
        try {
            // The collection is ephemeral, so the memory it temporarily wastes shouldn't matter, but it would
            // be unusual to have a lot of parents unless using making very heavy use of nested groups, anyway.
            List<String> parents = new ArrayList<>(64);
            GenericValue membership = memberships.next();
            while (membership != null) {
                final String parent = membership.getString(MembershipEntity.LOWER_PARENT_NAME);
                parents.add(myInterner.intern(parent));
                membership = memberships.next();
            }
            return new LinkedHashSet<>(parents);
        } finally {
            memberships.close();
        }
    }

    /**
     * Invoked by {@link OfBizCacheFlushingManager} to ensure caches are being flushed in the right order on
     * {@link XMLRestoreFinishedEvent}
     */
    public void flushCache() {
        parentsCache.removeAll();
        childrenCache.removeAll();
    }

    private void invalidateChildrenCacheEntry(final Long directoryId, final String parentName, final MembershipType type) {
        MembershipKey key = MembershipKey.getKey(directoryId, parentName, type);
        childrenCache.remove(key);
    }

    private void invalidateParentsCacheEntry(final Long directoryId, final String childName, final MembershipType type) {
        MembershipKey key = MembershipKey.getKey(directoryId, childName, type);
        parentsCache.remove(key);
    }

    private class ParentsLoader implements CacheLoader<MembershipKey, LinkedHashSet<String>> {
        @Override
        @Nonnull
        public LinkedHashSet<String> load(@Nonnull final MembershipKey key) {
            return findParents(key.getDirectoryId(), key.getType(), key.getName());
        }
    }

    private class ChildrenLoader implements CacheLoader<MembershipKey, LinkedHashSet<String>> {
        @Override
        @Nonnull
        public LinkedHashSet<String> load(@Nonnull final MembershipKey key) {
            return findChildren(key.getDirectoryId(), key.getType(), key.getName());
        }
    }
}
