package com.atlassian.jira.mention;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserIssueRelevance;

import java.util.List;

/**
 * Responsible for finding users that can mentioned on an issue, by role or name.
 * Split from {@link com.atlassian.jira.mention.MentionService} to resolve circular dependencies.
 *
 * @since v7.2
 */
public interface MentionableUserSearcher {
    /**
     * Calculates a list of users that are relevant to the issue so that they can be mentioned
     *
     * @param searchString the filter string to match the user, or role, against
     * @param issue the issue we are looking for users to mention on (to determine relevance)
     * @param requestingUser what user is making the request, for authorisation
     * @param maxResults the maximum number of results that will be returned. We assume it has been validated before reaching here.
     * @return A list of users, ordered by relevance to the issue.
     */
    List<UserIssueRelevance> findRelatedUsersToMention(String searchString, Issue issue, ApplicationUser requestingUser, int maxResults);
}
