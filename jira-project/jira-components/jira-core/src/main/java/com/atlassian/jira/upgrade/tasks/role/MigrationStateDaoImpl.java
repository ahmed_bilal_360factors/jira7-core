package com.atlassian.jira.upgrade.tasks.role;

import com.google.common.collect.ImmutableList;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * @since 7.0
 */
final class MigrationStateDaoImpl extends MigrationStateDao {
    private final LicenseDao licenseDao;
    private final ApplicationRolesDao rolesDao;
    private final MigrationLogDao migrationLogDao;

    MigrationStateDaoImpl(final LicenseDao licenseDao, final ApplicationRolesDao rolesDao,
                          final MigrationLogDao migrationLogDao) {
        this.migrationLogDao = migrationLogDao;
        this.rolesDao = notNull("rolesDao", rolesDao);
        this.licenseDao = notNull("licenseDao", licenseDao);
    }

    @Override
    MigrationState get() {
        return new MigrationState(licenseDao.getLicenses(), rolesDao.get(), ImmutableList.<Runnable>of(),
                new MigrationLogImpl());
    }

    @Override
    void put(final MigrationState migrationState) {
        licenseDao.setLicenses(migrationState.licenses());
        rolesDao.put(migrationState.applicationRoles());
        migrationLogDao.write(migrationState.log());
    }
}
