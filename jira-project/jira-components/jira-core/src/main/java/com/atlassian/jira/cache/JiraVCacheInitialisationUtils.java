package com.atlassian.jira.cache;

import com.atlassian.fugue.Option;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.tenancy.JiraTenantContext;
import com.atlassian.jira.tenancy.JiraTenantImpl;
import com.atlassian.tenancy.api.TenantContext;
import com.atlassian.vcache.internal.VCacheLifecycleManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Optional;

/**
 * Initialisation utilities for VCache.
 *
 * @since v7.1
 */
@ParametersAreNonnullByDefault
public final class JiraVCacheInitialisationUtils {
    private static final Logger log = LoggerFactory.getLogger(JiraVCacheInitialisationUtils.class);

    private JiraVCacheInitialisationUtils() {
    }

    /**
     * Initialises thread local request context for VCache with the tenant supplied by {@link TenantContext}.
     * @return true if context was initialised successfully, false otherwise.
     */
    public static boolean initVCache() {

        final Optional<TenantContext> tenantContext = ComponentAccessor.getComponentSafely(TenantContext.class);

        if (tenantContext.isPresent()) {
            //FIXME: tenant context is not properly initialised in JIRA (it is always null)
            //Issue https://jdog.jira-dev.com/browse/JVC-18 should address that
            ((JiraTenantContext) tenantContext.get()).setCurrentTenant(getFakeTenant());

            //In JIRA we will always get JiraTenantImpl
            final JiraTenantImpl currentTenant = (JiraTenantImpl) tenantContext.get().getCurrentTenant();
            if (currentTenant != null) {
                return initVCache(currentTenant);
            } else {
                //Here we may initialise it with a fake tenant, but even if this is warm instance
                //we should get 'system' tenant so it should not be needed.
                log.debug("Unable to initialize VCache request context supplier - cannot determine current tenant.");
            }
        } else {
            log.debug("Unable to initialize VCache request context supplier - cannot get components from the container.");
        }
        return false;
    }

    /**
     * Initialises thread local request context for VCache with the tenant provided as a parameter.
     *
     * @param currentTenant tenant with which request context will be initialised.
     * @return true if context was initialised successfully, false otherwise
     */
    public static boolean initVCache(JiraTenantImpl currentTenant) {
        final Optional<JiraVCacheRequestContextSupplier> requestContextSupplier = ComponentAccessor
                .getComponentSafely(JiraVCacheRequestContextSupplier.class);

        if (requestContextSupplier.isPresent()) {
            if (requestContextSupplier.get().isInitilised()) {
                return false;
            }

            requestContextSupplier.get().initThread(currentTenant.getId());
            return true;
        } else {
            log.debug("Unable to initialize VCache request context supplier - cannot get components from the container.");
        }
        return false;
    }

    /**
     * Executes any transactions associated with current request context and cleans it up.
     */
    public static void cleanupVCache() {
        final JiraVCacheRequestContextSupplier requestContextSupplier = ComponentAccessor
                .getComponentSafely(JiraVCacheRequestContextSupplier.class)
                .orElse(null);

        if (requestContextSupplier != null && requestContextSupplier.isInitilised()) {
            try {
                Option.option(ComponentAccessor.getComponent(VCacheLifecycleManager.class))
                        .forEach(service -> service.transactionSync(requestContextSupplier.get()));
            } catch (IllegalStateException e) {
                log.warn("Error while cleaning up vcache.", e);
            } finally {
                requestContextSupplier.clearThread();
            }
        }
    }

    /**
     * Temporary method to get current tenant as {@link TenantContext} is not implemented correctly in JIRA. For the
     * time being it should be called by anything that needs current tenant.
     *
     * NOTE: This method will be removed once JVC-18 is fixed.
     *
     * @return static fake tenant.
     */
    public static JiraTenantImpl getFakeTenant() {
        return new JiraTenantImpl("fakeTenantId");
    }
}
