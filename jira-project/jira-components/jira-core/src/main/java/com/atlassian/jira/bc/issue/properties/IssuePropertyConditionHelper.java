package com.atlassian.jira.bc.issue.properties;

import com.atlassian.jira.entity.property.AbstractEntityPropertyConditionHelper;
import com.atlassian.jira.issue.Issue;

public final class IssuePropertyConditionHelper extends AbstractEntityPropertyConditionHelper<Issue> {

    public IssuePropertyConditionHelper(IssuePropertyService propertyService) {
        super(propertyService, Issue.class, "issue");
    }
}
