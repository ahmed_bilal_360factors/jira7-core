package com.atlassian.jira.model.querydsl;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Generated;

/**
 * Data Transfer Object for the RunDetails entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 *
 * @see QRunDetails
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class RunDetailsDTO implements DTO {
    private final Long id;
    private final String jobId;
    private final java.sql.Timestamp startTime;
    private final Long runDuration;
    private final String runOutcome;
    private final String infoMessage;

    public Long getId() {
        return id;
    }

    public String getJobId() {
        return jobId;
    }

    public java.sql.Timestamp getStartTime() {
        return startTime;
    }

    public Long getRunDuration() {
        return runDuration;
    }

    public String getRunOutcome() {
        return runOutcome;
    }

    public String getInfoMessage() {
        return infoMessage;
    }

    public RunDetailsDTO(Long id, String jobId, java.sql.Timestamp startTime, Long runDuration, String runOutcome, String infoMessage) {
        this.id = id;
        this.jobId = jobId;
        this.startTime = startTime;
        this.runDuration = runDuration;
        this.runOutcome = runOutcome;
        this.infoMessage = infoMessage;
    }

    /**
     * Creates a GenericValue object from the values in this Data Transfer Object.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
     * @return a GenericValue object constructed from the values in this Data Transfer Object.
     */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator) {
        return ofBizDelegator.makeValue("RunDetails", new FieldMap()
                .add("id", id)
                .add("jobId", jobId)
                .add("startTime", startTime)
                .add("runDuration", runDuration)
                .add("runOutcome", runOutcome)
                .add("infoMessage", infoMessage)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */
    public static RunDetailsDTO fromGenericValue(GenericValue gv) {
        return new RunDetailsDTO(
                gv.getLong("id"),
                gv.getString("jobId"),
                gv.getTimestamp("startTime"),
                gv.getLong("runDuration"),
                gv.getString("runOutcome"),
                gv.getString("infoMessage")
        );
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(RunDetailsDTO runDetailsDTO) {
        return new Builder(runDetailsDTO);
    }

    public static class Builder {
        private Long id;
        private String jobId;
        private java.sql.Timestamp startTime;
        private Long runDuration;
        private String runOutcome;
        private String infoMessage;

        public Builder() {
        }

        public Builder(RunDetailsDTO runDetailsDTO) {
            this.id = runDetailsDTO.id;
            this.jobId = runDetailsDTO.jobId;
            this.startTime = runDetailsDTO.startTime;
            this.runDuration = runDetailsDTO.runDuration;
            this.runOutcome = runDetailsDTO.runOutcome;
            this.infoMessage = runDetailsDTO.infoMessage;
        }

        public RunDetailsDTO build() {
            return new RunDetailsDTO(id, jobId, startTime, runDuration, runOutcome, infoMessage);
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }
        public Builder jobId(String jobId) {
            this.jobId = jobId;
            return this;
        }
        public Builder startTime(java.sql.Timestamp startTime) {
            this.startTime = startTime;
            return this;
        }
        public Builder runDuration(Long runDuration) {
            this.runDuration = runDuration;
            return this;
        }
        public Builder runOutcome(String runOutcome) {
            this.runOutcome = runOutcome;
            return this;
        }
        public Builder infoMessage(String infoMessage) {
            this.infoMessage = infoMessage;
            return this;
        }
    }
}