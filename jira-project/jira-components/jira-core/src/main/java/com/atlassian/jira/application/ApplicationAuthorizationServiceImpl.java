package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.LicenseMaintenancePredicate;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import java.util.function.Predicate;

import static com.atlassian.application.api.ApplicationAccess.AccessError;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

public class ApplicationAuthorizationServiceImpl implements ApplicationAuthorizationService {
    private final ApplicationRoleManager applicationRoleManager;
    private final JiraLicenseManager jiraLicenseManager;
    private final Predicate<LicenseDetails> maintenanceCheck;

    public ApplicationAuthorizationServiceImpl(final ApplicationRoleManager applicationRoleManager,
                                               final JiraLicenseManager jiraLicenseManager, final LicenseMaintenancePredicate licenseMaintenancePredicate) {
        this.applicationRoleManager = applicationRoleManager;
        this.jiraLicenseManager = jiraLicenseManager;
        this.maintenanceCheck = licenseMaintenancePredicate;
    }

    @Override
    public boolean isApplicationInstalledAndLicensed(@Nonnull final ApplicationKey key) {
        return applicationRoleManager.isRoleInstalledAndLicensed(notNull("key", key));
    }

    @Override
    public boolean isAnyRoleLimitExceeded() {
        return applicationRoleManager.isAnyRoleLimitExceeded();
    }

    @Override
    public boolean isExceeded(@Nonnull final ApplicationKey key) {
        return applicationRoleManager.isRoleLimitExceeded(notNull("key", key));
    }

    @Override
    public boolean canUseApplication(@Nullable final ApplicationUser user, @Nonnull final ApplicationKey key) {
        return getAccessErrors(user, key).isEmpty();
    }

    @Override
    public Set<AccessError> getAccessErrors(@Nullable ApplicationUser user, @Nonnull ApplicationKey key) {
        notNull("key", key);

        Set<AccessError> errors = getAccessErrorsExact(user, key);

        if (!errors.isEmpty() && key.equals(ApplicationKeys.CORE)) {
            for (ApplicationKey appKey : jiraLicenseManager.getAllLicensedApplicationKeys()) {
                Set<AccessError> otherAppErrors = getAccessErrorsExact(user, appKey);
                if (otherAppErrors.isEmpty()) {
                    return Collections.unmodifiableSet(otherAppErrors);
                }
            }
        }

        return Collections.unmodifiableSet(errors);
    }

    @Override
    public boolean hasNoLicensingAccessErrors(@Nonnull ApplicationKey key) {
        return getLicensingAccessErrors(key).isEmpty();
    }

    @Override
    public Set<AccessError> getLicensingAccessErrors(@Nonnull ApplicationKey key) {
        notNull("key", key);
        Set<AccessError> errors = getLicensingExactErrors(key);

        if (!errors.isEmpty() && key.equals(ApplicationKeys.CORE)) {
            Set<AccessError> otherAppErrors;
            for (ApplicationKey appKey : jiraLicenseManager.getAllLicensedApplicationKeys()) {
                otherAppErrors = getLicensingExactErrors(appKey);
                if (otherAppErrors.isEmpty()) {
                    return Collections.unmodifiableSet(otherAppErrors);
                }
            }
        }

        return Collections.unmodifiableSet(errors);
    }

    @Override
    public int getUserCount(@Nonnull final ApplicationKey key) {
        return applicationRoleManager.getUserCount(notNull("key", key));
    }

    @Override
    public boolean rolesEnabled() {
        return true;
    }

    private Set<AccessError> getAccessErrorsExact(@Nullable ApplicationUser user, ApplicationKey key) {
        Set<AccessError> errors = getLicensingExactErrors(key);

        if (!applicationRoleManager.userHasRole(user, key)) {
            errors.add(AccessError.NO_ACCESS);
        }

        return errors;
    }

    private Set<AccessError> getLicensingExactErrors(ApplicationKey key) {
        final Option<LicenseDetails> licenseForApplication = jiraLicenseManager.getLicense(key);

        // Check if licensed
        if (licenseForApplication.isEmpty()) {
            // If unlicensed then no need to perform any further license checks.
            return EnumSet.of(AccessError.UNLICENSED);
        }

        EnumSet<AccessError> errors = EnumSet.noneOf(AccessError.class);

        // Checks subscription license date
        final LicenseDetails licenseDetails = licenseForApplication.get();
        if (licenseDetails.isExpired()) {
            errors.add(AccessError.EXPIRED);
        }

        // Check maintenance license dates
        if (!maintenanceCheck.test(licenseDetails)) {
            errors.add(AccessError.VERSION_MISMATCH);
        }

        if (applicationRoleManager.isRoleLimitExceeded(key)) {
            errors.add(AccessError.USERS_EXCEEDED);
        }

        return errors;
    }
}
