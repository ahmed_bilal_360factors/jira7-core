package com.atlassian.jira.util;

/**
 * This class is not intended to be used outside of the setup process.
 *
 * @since 7.0
 */
public interface JiraProductInformation {
    enum Keys {
        LICENSE_PRODUCT_KEY("setup.hams.license.product.key", "jira"),
        SETUP_HELP_APPLICATION_KEY("setup.help.application.key", "jira-core"),
        SETUP_HELP_APPLICATION_SPACE("setup.help.application.space", "jcore-docs-" + (new BuildUtilsInfoImpl()).getDocVersion());

        private final String key;
        private final String defaultValue;

        Keys(final String key, final String defaultValue) {
            this.key = key;
            this.defaultValue = defaultValue;
        }

        public String getKey() {
            return key;
        }

        public String getDefaultValue() {
            return defaultValue;
        }
    }


    /**
     * Returns the product name needed to obtain proper evaluation license from HAMS.
     */
    default String getLicenseProductKey() {
        return getValueForKey(Keys.LICENSE_PRODUCT_KEY);
    }

    /**
     * Returns the application help space to point core help base urls to during setup.
     */
    default String getSetupHelpApplicationHelpSpace() {
        return getValueForKey(Keys.SETUP_HELP_APPLICATION_SPACE);
    }

    /**
     * Returns an application key for loading help during db setup,
     */
    default String getSetupHelpApplicationKey() {
        return getValueForKey(Keys.SETUP_HELP_APPLICATION_KEY);
    }

    String getValueForKey(JiraProductInformation.Keys key);
}