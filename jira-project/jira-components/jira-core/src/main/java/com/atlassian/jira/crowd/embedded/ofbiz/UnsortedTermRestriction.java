package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.search.query.entity.restriction.MatchMode;
import com.atlassian.crowd.search.query.entity.restriction.Property;
import com.atlassian.crowd.search.query.entity.restriction.PropertyRestriction;
import com.atlassian.crowd.search.query.entity.restriction.TermRestriction;

/**
 * Internal flagging restriction type that behaves like a normal TermRestriction but also indicates results may be
 * unsorted.  Can be used for increased performance for queries with large numbers of results.
 */
public class UnsortedTermRestriction<T> extends TermRestriction<T> implements UnsortedRestriction {
    public UnsortedTermRestriction(final Property<T> property, final MatchMode matchMode, final T value) {
        super(property, matchMode, value);
    }

    public static <T> UnsortedTermRestriction<T> from(PropertyRestriction<T> pr) {
        return new UnsortedTermRestriction<>(pr.getProperty(), pr.getMatchMode(), pr.getValue());
    }
}

