package com.atlassian.jira.web.action;

import com.atlassian.jira.plugin.myjirahome.MyJiraHomeLinker;
import com.atlassian.jira.security.login.LoginManagerImpl;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.landingpage.LandingPageRedirectManager;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Redirects or forwards to the current My JIRA Home.
 *
 * @since 5.1
 */
public class MyJiraHome extends JiraWebActionSupport {
    private final MyJiraHomeLinker myJiraHomeLinker;
    private final UserManager userManager;
    private final LandingPageRedirectManager landingPageRedirectManager;

    public MyJiraHome(@Nonnull final MyJiraHomeLinker myJiraHomeLinker, @Nonnull final UserManager userManager,


                      @Nonnull final LandingPageRedirectManager landingPageRedirectManager) {
        this.myJiraHomeLinker = myJiraHomeLinker;
        this.userManager = userManager;
        this.landingPageRedirectManager = landingPageRedirectManager;
    }

    @Override
    protected String doExecute() throws Exception {
        return getRedirect(findMyHome());
    }

    private String findMyHome() {
        final ApplicationUser loggedInUser = getLoggedInUser();

        Optional<String> redirectUrl = landingPageRedirectManager.redirectUrl(loggedInUser);
        if (redirectUrl.isPresent()) {
            return redirectUrl.get();
        }

        if (loggedInUser != null) {
            return myJiraHomeLinker.getHomeLink(loggedInUser);
        } else if (isKnownButUnauthorised()) {
            //If the user is authenticated but not authorised to access JIRA, send them to the default home
            return myJiraHomeLinker.getDefaultUserHome();
        } else {
            return myJiraHomeLinker.getHomeLink((ApplicationUser) null);
        }
    }

    private boolean isKnownButUnauthorised() {
        final HttpServletRequest httpRequest = getHttpRequest();
        if (Boolean.TRUE.equals(httpRequest.getAttribute(LoginManagerImpl.AUTHORISED_FAILURE))) {
            Object userKey = httpRequest.getAttribute(LoginManagerImpl.AUTHORISING_USER_KEY);
            if (userKey instanceof String) {
                String userKeyStr = (String) userKey;
                final ApplicationUser user = userManager.getUserByKey(userKeyStr);

                if (user != null) {
                    return true;
                }
            }
        }
        return false;
    }

}
