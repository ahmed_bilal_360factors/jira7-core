package com.atlassian.jira.event.type;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.model.querydsl.EventTypeDTO;
import com.atlassian.jira.model.querydsl.QEventType;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.util.EventTypeOrderTransformer;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.google.common.collect.ImmutableMap;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.comparators.TransformingComparator;
import org.apache.commons.collections.map.MultiValueMap;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@EventComponent
public class DefaultEventTypeManager implements EventTypeManager {
    public static final String EVENT_TYPE_ID = "eventTypeId";

    private final QueryDslAccessor accessor;
    private final OfBizDelegator delegator;
    private final WorkflowManager workflowManager;
    private final NotificationSchemeManager notificationSchemeManager;
    private final Comparator<EventType> eventTypeComparator = new TransformingComparator(new EventTypeOrderTransformer());

    private final CachedReference<Map<Long, EventType>> eventTypesMapRef;

    public DefaultEventTypeManager(OfBizDelegator delegator, QueryDslAccessor accessor,
                                   WorkflowManager workflowManager, NotificationSchemeManager notificationSchemeManager,
                                   CacheManager cacheFactory) {
        this.delegator = delegator;
        this.accessor = accessor;
        this.workflowManager = workflowManager;
        this.notificationSchemeManager = notificationSchemeManager;

        eventTypesMapRef = cacheFactory.getCachedReference(getClass().getName() +  ".eventTypesMapRef",
                this::loadEventTypesMap);
    }

    @EventListener
    public void onClearCache(final ClearCacheEvent event) {
        clearCache();
    }

    // ---- Retrieval methods ------------------------------------------------------------------------------------------

    public Collection<EventType> getEventTypes() {
        return getEventTypesMap().values();
    }

    public Map<Long, EventType> getEventTypesMap() {
        return eventTypesMapRef.get();
    }

    public EventType getEventType(Long id) {
        EventType eventType = getEventTypesMap().get(id);
        if (eventType == null) {
            EventTypeDTO issueEventTypeGV = retrieveEntityByPrimaryKey(id);
            if (issueEventTypeGV == null) {
                throw new IllegalArgumentException("No event type with id " + id);
            }
            eventType = new EventType(issueEventTypeGV.toGenericValue(delegator));
        }
        return eventType;
    }

    // ---- Event Type specific methods --------------------------------------------------------------------------------

    public boolean isActive(EventType eventType) {
        return !(getAssociatedWorkflows(eventType, true).isEmpty() && getAssociatedNotificationSchemes(eventType).isEmpty());
    }

    public MultiMap getAssociatedWorkflows(EventType eventType, boolean statusCheck) {
        MultiMap workflowTransitionMap = new MultiValueMap();

        Collection<JiraWorkflow> workflows = workflowManager.getWorkflows();
        Long eventTypeId = eventType.getId();

        for (final JiraWorkflow workflow : workflows) {
            Map<ActionDescriptor, Collection<FunctionDescriptor>> transitionPostFunctionMap = workflowManager.getPostFunctionsForWorkflow(workflow);

            Collection<ActionDescriptor> keys = transitionPostFunctionMap.keySet();

            for (final ActionDescriptor actionDescriptor : keys) {
                Collection<FunctionDescriptor> postFunctions = transitionPostFunctionMap.get(actionDescriptor);

                for (final FunctionDescriptor functionDescriptor : postFunctions) {
                    if (functionDescriptor.getArgs().containsKey(EVENT_TYPE_ID) &&
                            eventTypeId.equals(new Long((String) functionDescriptor.getArgs().get(EVENT_TYPE_ID)))) {
                        workflowTransitionMap.put(workflow.getName(), actionDescriptor);

                        // Exit now as we only need one association for a status check
                        if (statusCheck) {
                            return workflowTransitionMap;
                        }
                    }
                }
            }
        }

        return workflowTransitionMap;
    }

    public Map<Long, String> getAssociatedNotificationSchemes(EventType eventType) {
        return notificationSchemeManager.getSchemesMapByConditions(FieldMap.build(EVENT_TYPE_ID, eventType.getId()));
    }

    // ---- Add, Edit, Delete methods ----------------------------------------------------------------------------------

    public void addEventType(EventType eventType) {
        accessor.withNewConnection()
                .execute(connection -> connection
                    .insert(QEventType.EVENT_TYPE)
                    .set(QEventType.EVENT_TYPE.name, eventType.getName())
                    .set(QEventType.EVENT_TYPE.description, eventType.getDescription())
                    .set(QEventType.EVENT_TYPE.templateId, eventType.getTemplateId())
                    .executeWithId());

        clearCache();
    }

    public void editEventType(Long eventTypeId, String name, String description, Long templateId) {
        accessor.withNewConnection()
                .execute(connection -> connection
                    .update(QEventType.EVENT_TYPE)
                    .set(QEventType.EVENT_TYPE.name, name)
                    .set(QEventType.EVENT_TYPE.description, description)
                    .set(QEventType.EVENT_TYPE.templateId, templateId)
                    .where(QEventType.EVENT_TYPE.id.eq(eventTypeId))
                    .execute());

        clearCache();
    }

    public void deleteEventType(Long eventTypeId) {
        accessor.withNewConnection()
                .execute(connection -> connection
                        .delete(QEventType.EVENT_TYPE)
                        .where(QEventType.EVENT_TYPE.id.eq(eventTypeId))
                        .execute());

        clearCache();
    }

    // ---- Validation methods -----------------------------------------------------------------------------------------

    public boolean isEventTypeExists(String issueEventTypeName) {
        if (issueEventTypeName == null) {
            throw new IllegalArgumentException("EventTypeName must not be null.");
        }

        for (EventType eventType : getEventTypes()) {
            if (issueEventTypeName.equals(eventType.getName())) {
                return true;
            }
        }
        return false;
    }

    public boolean isEventTypeExists(Long eventTypeId) {
        if (eventTypeId == null) {
            throw new IllegalArgumentException("EventTypeId must not be null.");
        }
        return getEventTypesMap().containsKey(eventTypeId);
    }

    private Map<Long, EventType> loadEventTypesMap() {
        final ImmutableMap.Builder<Long, EventType> eventTypeMap = ImmutableMap.builder();
        for (EventType eventType : retrieveAllEntities()) {
            eventTypeMap.put(eventType.getId(), eventType);
        }
        return eventTypeMap.build();
    }

    // ---- Database methods -------------------------------------------------------------------------------------------

    /**
     * Return a list of {@link com.atlassian.jira.event.type.EventType}s extracted from the database using the specified params.
     *
     * @return Collection   all event types within the system.
     */
    private List<EventType> retrieveAllEntities() {
        List<EventTypeDTO> dtos = this.accessor.withNewConnection().executeQuery(dbConnection ->
                dbConnection.newSqlQuery()
                        .select(QEventType.EVENT_TYPE)
                        .from(QEventType.EVENT_TYPE)
                        .fetch());

        List<EventType> eventTypes = dtos.stream()
                .map(eventTypeDTO -> new EventType(eventTypeDTO.toGenericValue(delegator)))
                .collect(Collectors.toList());

        Collections.sort(eventTypes, eventTypeComparator);
        return eventTypes;
    }

    private EventTypeDTO retrieveEntityByPrimaryKey(long id) {
        return this.accessor.withNewConnection().executeQuery(dbConnection ->
                dbConnection.newSqlQuery()
                            .select(QEventType.EVENT_TYPE)
                            .from(QEventType.EVENT_TYPE)
                            .where(QEventType.EVENT_TYPE.id.eq(id))
                            .fetchFirst());
    }

    // ---- Helper methods ---------------------------------------------------------------------------------------------

    public void clearCache() {
        eventTypesMapRef.reset();
    }
}
