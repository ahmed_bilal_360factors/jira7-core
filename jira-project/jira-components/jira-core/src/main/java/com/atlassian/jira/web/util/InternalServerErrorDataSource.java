package com.atlassian.jira.web.util;

import com.atlassian.annotations.Internal;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.core.logging.DatedLoggingEvent;
import com.atlassian.core.logging.ThreadLocalErrorCollection;
import com.atlassian.core.ofbiz.util.OFBizPropertyUtils;
import com.atlassian.jira.config.LocaleManager;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.EnumerationIterator;
import com.atlassian.jira.util.system.ExtendedSystemInfoUtils;
import com.atlassian.logging.log4j.StackTraceInfo;
import com.atlassian.plugin.PluginInformation;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.opensymphony.module.propertyset.PropertySet;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.ThrowableInformation;

import javax.annotation.Nullable;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

import static com.google.common.collect.Maps.transformEntries;

@Internal
public class InternalServerErrorDataSource {
    public static final String JAVAX_SERVLET_ERROR_EXCEPTION = "javax.servlet.error.exception";
    public static final String JAVAX_SERVLET_ERROR_MESSAGE = "javax.servlet.error.message";
    public static final String UNKNOWN = "Unknown";

    private final I18nHelper i18n;

    @Nullable
    private final ExtendedSystemInfoUtils extendedSystemInfoUtils;

    private final ServletContext servletContext;
    private final LocaleManager localeManager;

    private final HttpServletRequest request;

    public InternalServerErrorDataSource(final I18nHelper i18n, final ExtendedSystemInfoUtils extendedSystemInfoUtils,
                                         final ServletContext servletContext, final LocaleManager localeManager, final HttpServletRequest req) {
        this.i18n = i18n;
        this.extendedSystemInfoUtils = extendedSystemInfoUtils;
        this.servletContext = servletContext;
        this.localeManager = localeManager;
        this.request = req;
    }

    public ImmutableMap.Builder<String, Object> appendFullMessageData(ImmutableMap.Builder<String, Object> map, boolean isSysadmin) {
        appendSimpleMessageData(map);

        map.put("serverInfo", soyMap(translateKeys(getServerInfo())));
        map.put("requestInfo", soyMap(translateKeys(getRequestInfo())));
        map.put("requestAttributes", soyMap(translateKeys(getRequestAttributes())));
        map.put("loggingEvents", getLoggingEvents());

        if (extendedSystemInfoUtils != null) {
            if (isSysadmin) {
                map.put("filePaths", soyMap(translateKeys(getFilePaths())));
                map.put("sysInfo", soyMap(extendedSystemInfoUtils.getProps()));
            } else {
                Map<String, String> sysProps = extendedSystemInfoUtils.getProps();
                Set<String> sysadminOnlyProps = ImmutableSet.of(
                        i18n.getText("admin.systeminfo.system.cwd"),
                        i18n.getText("admin.systeminfo.jvm.input.arguments"));
                map.put("sysInfo", soyMap(filterKeys(sysProps, sysadminOnlyProps, i18n.getText("system.error.property.not.sysadmin"))));
            }
            map.put("languageInfo", translateKeys(getLanguageInfo()));
            map.put("listeners", getListeners());
            map.put("services", getServices());
            map.put("buildInfoData", soyMap(extendedSystemInfoUtils.getBuildStats()));
            map.put("memInfo", soyMap(extendedSystemInfoUtils.getJvmStats()));
            map.put("plugins", getPlugins());
        }
        return map;
    }

    public void appendSimpleMessageData(ImmutableMap.Builder<String, Object> map) {
        map.put("generalInfo", getGeneralInfo());
    }

    public Map<String, Object> getGeneralInfo() {
        final ImmutableMap.Builder<String, Object> map = ImmutableMap.builder();
        InternalServerErrorExceptionDataSource exceptionInfo = new InternalServerErrorExceptionDataSource((Throwable) request.getAttribute(JAVAX_SERVLET_ERROR_EXCEPTION), extendedSystemInfoUtils);
        map.put("interpretedMsg", exceptionInfo.getInterpretedMessage());
        map.put("cause", exceptionInfo.getRootCause());
        map.put("stacktrace", exceptionInfo.getStacktrace());
        map.put("referer", request.getHeader("Referer") != null ? request.getHeader("Referer") : UNKNOWN);
        map.put("servletErrorMessage", request.getAttribute(JAVAX_SERVLET_ERROR_MESSAGE) != null ? request.getAttribute(JAVAX_SERVLET_ERROR_MESSAGE) : "");
        return map.build();
    }

    private Map<String, Object> getLanguageInfo() {
        String defaultLang = extendedSystemInfoUtils.getDefaultLanguage();

        if (extendedSystemInfoUtils.isUsingSystemLocale()) {
            defaultLang = defaultLang + " - " + i18n.getText("admin.systeminfo.system.default.locale");
        }

        return ImmutableMap.<String, Object>of(
                "admin.generalconfiguration.installed.languages", getInstalledLocales(),
                "admin.generalconfiguration.default.language", defaultLang
        );
    }

    private List<String> getInstalledLocales() {
        if (localeManager != null) {
            return localeManager.getInstalledLocales().stream()
                    .map(locale -> locale.getDisplayName(i18n.getLocale()))
                    .collect(CollectorsUtil.toImmutableList());
        }
        return ImmutableList.of();
    }


    private Map<String, String> getServerInfo() {
        return ImmutableMap.of(
                "system.error.application.server", servletContext.getServerInfo(),
                "system.error.servlet.version", servletContext.getMajorVersion() + "." + servletContext.getMinorVersion());
    }

    private Map<String, String> getFilePaths() {
        return ImmutableMap.of(
                "system.error.location.of.log", extendedSystemInfoUtils.getLogPath(),
                "system.error.location.of.entityengine", extendedSystemInfoUtils.getEntityEngineXmlPath());
    }


    private Map<String, String> getRequestInfo() {
        Map<String, String> map = Maps.newHashMapWithExpectedSize(10);
        map.put("system.error.request.url", request.getRequestURL().toString());
        map.put("system.error.scheme", request.getScheme());
        map.put("system.error.server", request.getServerName());
        map.put("system.error.port", Integer.toString(request.getServerPort()));
        map.put("system.error.uri", request.getRequestURI());
        map.put("system.error.context.path", request.getContextPath());
        map.put("system.error.servlet.path", request.getServletPath());
        map.put("system.error.path.info", request.getPathInfo());
        map.put("system.error.query.string", request.getQueryString());
        return map;
    }

    private Map<String, String> getRequestAttributes() {
        ImmutableMap.Builder<String, String> map = ImmutableMap.builder();
        EnumerationIterator.fromEnumeration(request.getAttributeNames())
                .forEachRemaining(attr -> map.put(attr, String.valueOf(request.getAttribute(attr))));
        return map.build();
    }

    @SuppressWarnings("unchecked")
    private static List<Map<String, Object>> getLoggingEvents() {
        final List<DatedLoggingEvent> events = ThreadLocalErrorCollection.getList();
        return convert(events, (datedLoggingEvent, map) ->
        {
            LoggingEvent event = datedLoggingEvent.getEvent();
            ThrowableInformation throwableInformation = event.getThrowableInformation();
            Throwable throwable = throwableInformation == null ? null : throwableInformation.getThrowable();
            map.put("loggerName", event.getLoggerName());
            map.put("level", event.getLevel());
            map.put("date", datedLoggingEvent.getDate().toString());
            map.put("message", event.getRenderedMessage());
            map.put("throwableStrRep", throwable != null ? StackTraceInfo.asLines(throwable) : Collections.emptyList());
        });
    }

    private List<Map<String, Object>> getListeners() {
        return convert(extendedSystemInfoUtils.getListeners(), (listener, map) ->
        {
            map.put("name", listener.getString("name"));
            map.put("clazz", listener.getString("clazz"));

            PropertySet propset = OFBizPropertyUtils.getPropertySet(listener);
            Collection<String> keys = propset.getKeys(PropertySet.STRING);
            if (keys == null) {
                keys = Collections.emptyList();
            }

            Map<String, String> values = Maps.newHashMapWithExpectedSize(keys.size());
            for (String key : keys) {
                values.put(key, propset.getString(key));
            }
            map.put("properties", soyMap(values));
        });
    }


    private List<Map<String, Object>> getServices() {
        return convert(extendedSystemInfoUtils.getServices(), (service, map) -> {
            map.put("name", service.getName());
            map.put("class", service.getServiceClass());
            map.put("schedule", service.getCronExpression());
            map.put("properties", soyMap(translateValues(extendedSystemInfoUtils.getServicePropertyMap(service))));
        });
    }

    private List<Map<String, Object>> getPlugins() {
        return convert(extendedSystemInfoUtils.getPlugins(), (plugin, map) ->
        {
            PluginInformation info = plugin.getPluginInformation();
            map.put("name", plugin.getName());
            map.put("version", info.getVersion());
            map.put("vendor", info.getVendorName());
            map.put("enabled", extendedSystemInfoUtils.isPluginEnabled(plugin));
            map.put("parameters", soyMap(info.getParameters()));
        });
    }

    //###### HELPERS

    private static Map<String, String> filterKeys(Map<String, String> sourceMap, final Set<String> keysToRemove, final String replacementValue) {
        if (replacementValue == null) {
            return Maps.filterKeys(sourceMap, input -> !keysToRemove.contains(input));
        } else {
            return transformEntries(sourceMap, (key, value) -> keysToRemove.contains(key) ? replacementValue : value);
        }

    }

    private <T> Map<String, T> translateKeys(Map<String, T> sourceMap) {
        Map<String, T> resultMap = Maps.newHashMapWithExpectedSize(sourceMap.size());
        sourceMap.forEach((key, value) -> resultMap.put(i18n.getText(key), value));
        return resultMap;
    }

    private Map<String, String> translateValues(Map<String, String> sourceMap) {
        Map<String, String> resultMap = Maps.newHashMapWithExpectedSize(sourceMap.size());
        sourceMap.forEach((key, value) -> resultMap.put(key, i18n.getText(value)));
        return resultMap;
    }

    /**
     * Transforms the provided map to a list of maps that have entries for the key and value
     *
     * @param map the map to transform
     * @param <T> the inferred type of the values held by the map
     * @return the result of {@link #listifyMap(Map)}
     */
    private <T> Object soyMap(Map<String, T> map) {
        return listifyMap(map);
    }

    /**
     * Transforms the map for presentation in soy.
     * <p>
     * Transforms the map provided, for example:
     * </p>
     * <pre><code>
     * {
     *     "K1" : "V1",
     *     "K2" : "V2"
     * }
     * </code></pre>
     * <p>
     * Into a list of map entries, themselves expressed as maps, in the original iteration order.  For example:
     * </p>
     * <pre><code>
     * [
     *     {
     *         "key" : "K1",
     *         "value" : "V1"
     *     },
     *     {
     *         "key" : "K2",
     *         "value" : "V2"
     *     }
     * ]
     * </code></pre>
     * <p>
     * This restructuring is necessary for the soy templates that render the page, but it makes the JSON harder to
     * read in the logs, so we skip it if we can.
     * </p>
     *
     * @param map the data to be transformed
     * @param <T> the inferred value type of the map
     * @return a list of the transformed map entries, in the map's original iteration order
     */
    private static <T> List<Map<String, Object>> listifyMap(Map<String, T> map) {
        final ImmutableList.Builder<Map<String, Object>> list = ImmutableList.builder();
        map.forEach((key, value) -> list.add(FieldMap.build("key", key, "value", value)));
        return list.build();
    }

    private static <T> List<Map<String, Object>> convert(Iterable<T> objects, BiConsumer<T, Map<String, Object>> mapper) {
        if (objects == null) {
            return ImmutableList.of();
        }

        final ImmutableList.Builder<Map<String, Object>> result = ImmutableList.builder();
        objects.forEach(obj ->
        {
            Map<String, Object> map = Maps.newHashMap();
            mapper.accept(obj, map);
            result.add(map);
        });
        return result.build();
    }
}
