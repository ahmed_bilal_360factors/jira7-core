package com.atlassian.jira.issue.worklog;

import com.atlassian.jira.bc.issue.worklog.DeletedWorklog;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.model.querydsl.QChangeGroup;
import com.atlassian.jira.model.querydsl.QChangeItem;
import com.atlassian.jira.model.querydsl.QProjectRole;
import com.atlassian.jira.model.querydsl.QWorklog;
import com.atlassian.jira.model.querydsl.WorklogDTO;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import com.querydsl.core.Tuple;
import com.querydsl.core.dml.StoreClause;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QTuple;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.NumberExpression;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.SQLExpressions;
import org.joda.time.DateTime;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

import static com.atlassian.jira.model.querydsl.QWorklog.WORKLOG;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * QueryDSL based implementation of {@link com.atlassian.jira.issue.worklog.WorklogStore}.
 */
public class DatabaseWorklogStore implements WorklogStore {
    public static final String WORKLOG_ENTITY = "Worklog";

    private static final QTuple WORKLOG_PROJECT_ROLE_TUPLE =  Projections.tuple(ImmutableList.<Expression<?>>builder()
            .addAll(Lists.newArrayList(QWorklog.WORKLOG.all()))
            .addAll(Lists.newArrayList(QProjectRole.PROJECT_ROLE.all()))
            .build());

    private static final QTuple WORKLOG_ALIASED_PROJECT_ROLE_TUPLE = Projections.tuple(ImmutableList.<Expression<?>>builder()
            .addAll(worklogColumnsWithAliasedId())
            .addAll(Lists.newArrayList(QProjectRole.PROJECT_ROLE.all()))
            .build());

    private static List<Expression<?>> worklogColumnsWithAliasedId() {
        List<Expression<?>> paths = Lists.newArrayList(WORKLOG.all());
        paths.replaceAll(new UnaryOperator<Expression<?>>() {
            @Override
            public Expression<?> apply(final Expression<?> expression) {
                if (expression.equals(WORKLOG.id)) {
                    return WORKLOG.id.as("WORKLOG_ID");
                }
                return expression;
            }
        });
        return paths;
    }

    private final OfBizDelegator delegator;
    private final QueryDslAccessor queryDslAccessor;
    private final QueryDSLWorklogFactory queryDSLWorklogFactory;
    private final DatabaseConfigurationManager databaseConfigurationManager;

    public DatabaseWorklogStore(final OfBizDelegator delegator, QueryDslAccessor queryDslAccessor, final QueryDSLWorklogFactory queryDSLWorklogFactory, final DatabaseConfigurationManager databaseConfigurationManager) {
        this.delegator = delegator;
        this.queryDslAccessor = queryDslAccessor;
        this.queryDSLWorklogFactory = queryDSLWorklogFactory;
        this.databaseConfigurationManager = databaseConfigurationManager;
    }

    @Override
    public Worklog update(final Worklog worklog) {
        final WorklogDTO worklogDTO = queryDslAccessor.executeQuery(dbConnection -> dbConnection.newSqlQuery()
                .select(WORKLOG)
                .from(WORKLOG)
                .where(WORKLOG.id.eq(worklog.getId()))
                .limit(1)
                .fetchOne());

        if (worklogDTO != null) {
            queryDslAccessor.execute(dbConnection -> {
                setWorklogFields(dbConnection.update(WORKLOG), worklog)
                        .where(WORKLOG.id.eq(worklog.getId()))
                        .execute();
            });
        } else {
            throw new IllegalArgumentException("Could not find original worklog entity to update.");
        }

        return getById(worklog.getId());
    }

    @Override
    public Worklog create(final Worklog worklog) {
        final Long nextId = delegator.getDelegatorInterface().getNextSeqId(WORKLOG.getEntityName());
        queryDslAccessor.executeQuery(dbConnection ->
                        setWorklogFields(dbConnection.insert(WORKLOG), worklog)
                                .set(WORKLOG.id, nextId)
                                .execute()
        );

        return new WorklogImpl2(worklog.getIssue(),
                nextId,
                worklog.getAuthorKey(),
                worklog.getComment(),
                worklog.getStartDate(),
                worklog.getGroupLevel(),
                worklog.getRoleLevelId(),
                worklog.getTimeSpent(),
                worklog.getUpdateAuthorKey(),
                worklog.getCreated(),
                worklog.getUpdated(),
                worklog.getRoleLevel());
    }

    private <T extends StoreClause<T>> T setWorklogFields(final T storeClause, final Worklog worklog) {
        checkArgument(worklog != null, "Cannot store a null worklog.");
        checkArgument(worklog.getIssue() != null, "Cannot store a worklog against a null issue.");

        return storeClause.set(WORKLOG.issue, worklog.getIssue().getId())
                .set(WORKLOG.author, worklog.getAuthorKey())
                .set(WORKLOG.updateauthor, worklog.getUpdateAuthorKey())
                .set(WORKLOG.body, worklog.getComment())
                .set(WORKLOG.grouplevel, worklog.getGroupLevel())
                .set(WORKLOG.rolelevel, worklog.getRoleLevelId())
                .set(WORKLOG.timeworked, worklog.getTimeSpent())
                .set(WORKLOG.startdate, new Timestamp(worklog.getStartDate().getTime()))
                .set(WORKLOG.created, new Timestamp(worklog.getCreated().getTime()))
                .set(WORKLOG.updated, new Timestamp(worklog.getUpdated().getTime()));
    }

    @Override
    public boolean delete(final Long worklogId) {
        checkArgument(worklogId != null, "Cannot remove a worklog with id null.");

        return queryDslAccessor.executeQuery(dbConnection -> dbConnection
                .delete(WORKLOG)
                .where(WORKLOG.id.eq(worklogId))
                .execute()) == 1;
    }

    @Override
    public long deleteWorklogsForIssue(Issue issue) {
        checkArgument(issue != null, "Cannot remove worklogs for null issue");
        checkArgument(issue.getId() != null, "Cannot remove worklogs for null issue id");

        return queryDslAccessor.executeQuery(dbConnection -> dbConnection.delete(WORKLOG)
                .where(WORKLOG.issue.eq(issue.getId()))
                .execute());
    }

    @Override
    public Worklog getById(final Long id) {
        if (id == null) {
            return null;
        }

        final Tuple worklogTuple = queryDslAccessor.executeQuery(dc -> dc.newSqlQuery()
                .select(WORKLOG_ALIASED_PROJECT_ROLE_TUPLE)
                .from(WORKLOG)
                .leftJoin(QProjectRole.PROJECT_ROLE)
                .on(QProjectRole.PROJECT_ROLE.id.eq(WORKLOG.rolelevel))
                .where(WORKLOG.id.eq(id))
                .limit(1)
                .fetchOne());

        return queryDSLWorklogFactory.createWorklogWithAliasedIdColumn(worklogTuple);
    }

    @Override
    public List<Worklog> getByIssue(final Issue issue) {
        checkArgument(issue != null, "Cannot resolve worklogs for null issue.");

        final BooleanExpression query = issue.getId() != null ?
                WORKLOG.issue.eq(issue.getId()) : WORKLOG.issue.isNull();

        return queryDslAccessor.executeQuery(dc -> dc.newSqlQuery()
                        .select(WORKLOG_PROJECT_ROLE_TUPLE)
                        .from(WORKLOG)
                        .leftJoin(QProjectRole.PROJECT_ROLE)
                        .on(QProjectRole.PROJECT_ROLE.id.eq(WORKLOG.rolelevel))
                        .where(query)
                        .orderBy(WORKLOG.created.asc())
                        .fetch()
        ).stream()
                .map(t -> queryDSLWorklogFactory.createWorklog(issue, t))
                .collect(Collectors.toList());
    }

    @Override
    public int swapWorklogGroupRestriction(final String groupName, final String swapGroup) {
        checkArgument(groupName != null, "You must provide a non null group name.");
        checkArgument(swapGroup != null, "You must provide a non null swap group name.");

        return Ints.checkedCast(queryDslAccessor.executeQuery(dc -> dc.update(WORKLOG)
                .set(WORKLOG.grouplevel, swapGroup)
                .where(WORKLOG.grouplevel.eq(groupName))
                .execute()));

    }

    @Override
    public long getCountForWorklogsRestrictedByGroup(final String groupName) {
        checkArgument(groupName != null, "You must provide a non null group name.");

        return queryDslAccessor.executeQuery(dc -> dc.newSqlQuery()
                .from(WORKLOG)
                .where(WORKLOG.grouplevel.eq(groupName))
                .fetchCount());
    }

    @Override
    public long getCountForWorklogsRestrictedByRole(final Long roleId) {
        checkNotNull(roleId, "You must provide a non null role id.");

        return queryDslAccessor.executeQuery(dc -> dc.newSqlQuery()
                .from(WORKLOG)
                .where(WORKLOG.rolelevel.eq(roleId))
                .fetchCount());
    }

    @Override
    public int swapWorklogRoleRestriction(final Long roleId, final Long swapRoleId) {
        checkNotNull(roleId, "You must provide a non null role id.");
        checkNotNull(swapRoleId, "You must provide a non null swap role id.");

        return Ints.checkedCast(queryDslAccessor.executeQuery(dc -> dc.update(WORKLOG)
                .set(WORKLOG.rolelevel, swapRoleId)
                .where(WORKLOG.rolelevel.eq(roleId))
                .execute()));
    }

    @Override
    public List<Worklog> getWorklogsUpdateSince(final Long sinceInMiliseconds, final int maxResults) {
        return queryDslAccessor.executeQuery(dbConnection -> dbConnection.newSqlQuery()
                        .select(WORKLOG_ALIASED_PROJECT_ROLE_TUPLE)
                        .from(WORKLOG)
                        .leftJoin(QProjectRole.PROJECT_ROLE)
                        .on(QProjectRole.PROJECT_ROLE.id.eq(WORKLOG.rolelevel))
                        .where(WORKLOG.updated.goe(new Timestamp(sinceInMiliseconds))
                                .and(WORKLOG.updated.loe(new Timestamp(minuteAgo()))))
                        .orderBy(WORKLOG.updated.asc())
                        .limit(maxResults)
                        .fetch()
        ).stream()
                .map(queryDSLWorklogFactory::createWorklogWithAliasedIdColumn)
                .collect(Collectors.toList());
    }

    @Override
    public List<DeletedWorklog> getWorklogsDeletedSince(final Long sinceInMilliseconds, final int maxResults) {
        final QChangeItem ci = QChangeItem.CHANGE_ITEM;
        final QChangeGroup cg = QChangeGroup.CHANGE_GROUP;
        final QWorklog w = WORKLOG;

        NumberExpression<Long> oldValueExpression = isOracle() ? convertToNumber(ci.oldvalue) : ci.oldvalue.castToNum(Long.class);

        List<Tuple> removedWorklogs = getRemovedWorklogs(sinceInMilliseconds, maxResults, ci, cg, w, oldValueExpression);

        return removedWorklogs
                .stream()
                .map(t -> new DeletedWorklog(t.get(oldValueExpression), new Date(t.get(cg.created.max()).getTime())))
                .collect(Collectors.toList());
    }

    @Override
    public Set<Worklog> getWorklogsForIds(final Set<Long> worklogIds, final int maxResults) {
        return queryDslAccessor.executeQuery(dbConnection -> dbConnection.newSqlQuery()
                        .select(WORKLOG_ALIASED_PROJECT_ROLE_TUPLE)
                        .from(WORKLOG)
                        .leftJoin(QProjectRole.PROJECT_ROLE)
                        .on(QProjectRole.PROJECT_ROLE.id.eq(WORKLOG.rolelevel))
                        .where(WORKLOG.id.in(worklogIds))
                        .limit(maxResults)
                        .fetch()
        ).stream()
                .map(queryDSLWorklogFactory::createWorklogWithAliasedIdColumn)
                .collect(Collectors.toSet());
    }

    private long minuteAgo() {
        return new DateTime(System.currentTimeMillis()).minusMinutes(1).getMillis();
    }

    private boolean isOracle() {
        return databaseConfigurationManager.getDatabaseConfiguration().isOracle();
    }

    private List<Tuple> getRemovedWorklogs(final Long sinceInMilliseconds, final int maxResults, final QChangeItem ci, final QChangeGroup cg, final QWorklog w, final NumberExpression<Long> oldValueExpression) {
        return queryDslAccessor.executeQuery(dbConnection ->
                dbConnection.newSqlQuery()
                        .select(oldValueExpression, cg.created.max())
                        .from(ci)
                        .leftJoin(cg)
                        .on(ci.group.eq(cg.id))
                        .where(ci.field.eq(IssueFieldConstants.WORKLOG_ID)
                                        .and(cg.created.goe(new Timestamp(sinceInMilliseconds))
                                                .and(cg.created.loe(new Timestamp(minuteAgo()))))
                                        .and(oldValueExpression.notIn(SQLExpressions.select(w.id)
                                                        .from(w))

                                        )
                        )
                        .groupBy(oldValueExpression)
                        .orderBy(cg.created.max().asc())
                        .limit(maxResults)
                        .fetch());
    }

    private NumberExpression<Long> convertToNumber(final StringPath value) {
        return Expressions.numberTemplate(Long.class, "TO_NUMBER({0})", value);
    }
}
