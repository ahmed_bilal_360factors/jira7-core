package com.atlassian.jira.user;

import com.atlassian.core.AtlassianCoreException;
import com.atlassian.jira.issue.issuetype.IssueTypeId;
import com.atlassian.jira.user.preferences.ExtendedPreferences;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Optional;

/**
 * Stores last used issue type in user preferences.
 *
 * @see UserIssueTypeManager
 */
public class UserIssueTypeManagerImpl implements UserIssueTypeManager {
    private static final Logger log = LoggerFactory.getLogger(UserIssueTypeManagerImpl.class);

    private static final String LAST_USED_ISSUE_TYPE_KEY = "user.last.issue.type.id";
    private static final String LAST_USED_SUBTASK_ISSUE_TYPE_KEY = "user.last.subtask.issue.type.id";

    private final UserPreferencesManager userPreferencesManager;

    public UserIssueTypeManagerImpl(final UserPreferencesManager userPreferencesManager) {
        this.userPreferencesManager = userPreferencesManager;
    }

    @Override
    public void setLastUsedIssueTypeId(@Nullable ApplicationUser user, IssueTypeId lastUsedIssueTypeId) {
        setIssueType(user, LAST_USED_ISSUE_TYPE_KEY, lastUsedIssueTypeId.getId());
    }

    @Override
    public void setLastUsedSubtaskIssueTypeId(@Nullable ApplicationUser user, IssueTypeId lastUsedSubtaskIssueTypeId) {
        setIssueType(user, LAST_USED_SUBTASK_ISSUE_TYPE_KEY, lastUsedSubtaskIssueTypeId.getId());
    }

    @Override
    public Optional<IssueTypeId> getLastUsedIssueTypeId(@Nullable ApplicationUser user) {
        return getIssueTypeId(user, LAST_USED_ISSUE_TYPE_KEY);
    }

    @Override
    public Optional<IssueTypeId> getLastUsedSubtaskIssueTypeId(@Nullable ApplicationUser user) {
        return getIssueTypeId(user, LAST_USED_SUBTASK_ISSUE_TYPE_KEY);
    }

    private Optional<IssueTypeId> getIssueTypeId(ApplicationUser user, String prefKey) {
        if (user == null) {
            return Optional.empty();
        }

        final ExtendedPreferences prefs = userPreferencesManager.getExtendedPreferences(user);
        return Optional.ofNullable(prefs.getString(prefKey)).map(IssueTypeId::new);
    }

    private void setIssueType(ApplicationUser user, String prefKey, String issueTypeId) {
        if (user == null) {
            return;
        }

        final ExtendedPreferences prefs = userPreferencesManager.getExtendedPreferences(user);
        try {
            prefs.setString(prefKey, issueTypeId);
        } catch (AtlassianCoreException e) {
            log.warn("Unable to store last used issue type for user.", e);
        }
    }
}
