/*
 * Created by IntelliJ IDEA.
 * User: Mike
 * Date: Mar 23, 2004
 * Time: 4:02:21 PM
 */
package com.atlassian.jira.web.action.admin.workflow;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.workflow.WorkflowService;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.util.CollectionReorderer;
import com.atlassian.jira.web.action.admin.workflow.analytics.WorkflowTransitionTabEvent;
import com.atlassian.jira.web.action.util.workflow.WorkflowEditorTransitionConditionUtil;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowActionsBean;
import com.atlassian.jira.workflow.tabs.WorkflowTransitionTabProvider;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.collect.ImmutableList;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.ConditionalResultDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@WebSudoRequired
public class ViewWorkflowTransition extends AbstractWorkflowTransitionAction {
    private final ConstantsManager constantsManager;
    private final WorkflowActionsBean workflowActionsBean;
    private final WorkflowTransitionTabProvider workflowTransitionTabProvider;
    private final EventPublisher eventPublisher;

    private int up;
    private int down;

    private String count;
    private String currentCount;

    public ViewWorkflowTransition(JiraWorkflow workflow, StepDescriptor step, ActionDescriptor transition,
                                  PluginAccessor pluginAccessor, ConstantsManager constantsManager,
                                  WorkflowService workflowService, WorkflowTransitionTabProvider workflowTransitionTabProvider, final EventPublisher eventPublisher) {
        super(workflow, step, transition, pluginAccessor, workflowService);
        this.constantsManager = constantsManager;
        this.eventPublisher = eventPublisher;
        this.workflowActionsBean = new WorkflowActionsBean();
        this.workflowTransitionTabProvider = workflowTransitionTabProvider;
    }

    public ViewWorkflowTransition(JiraWorkflow workflow, ActionDescriptor transition, PluginAccessor pluginAccessor,
                                  ConstantsManager constantsManager, WorkflowService workflowService,
                                  WorkflowTransitionTabProvider workflowTransitionTabProvider, final EventPublisher eventPublisher) {
        // Used for working with global actions 
        super(workflow, transition, pluginAccessor, workflowService);
        this.constantsManager = constantsManager;
        this.eventPublisher = eventPublisher;
        this.workflowActionsBean = new WorkflowActionsBean();
        this.workflowTransitionTabProvider = workflowTransitionTabProvider;
    }

    public StepDescriptor getStepDescriptor(ConditionalResultDescriptor conditionalResultDescriptor) {
        final int targetStepId = conditionalResultDescriptor.getStep();
        return getWorkflow().getDescriptor().getStep(targetStepId);
    }

    public Status getStatus(String id) {
        return constantsManager.getStatus(id);
    }


    public Status getStatusObject(String id) {
        return constantsManager.getStatusObject(id);
    }

    @RequiresXsrfCheck
    public String doMoveWorkflowFunctionUp() throws Exception {
        final List postFunctions = getTransition().getUnconditionalResult().getPostFunctions();

        if (up <= 0 || up >= postFunctions.size()) {
            addErrorMessage(getText("admin.errors.workflows.invalid.index", "" + up));
        } else {
            Object toMove = postFunctions.get(up);
            CollectionReorderer.increasePosition(postFunctions, toMove);
            workflowService.updateWorkflow(getJiraServiceContext(), getWorkflow());
        }

        return getViewRedirect("&currentCount=workflow-function" + (up));
    }

    @RequiresXsrfCheck
    public String doMoveWorkflowFunctionDown() throws Exception {
        final List postFunctions = getTransition().getUnconditionalResult().getPostFunctions();

        if (down < 0 || down >= (postFunctions.size() - 1)) {
            addErrorMessage(getText("admin.errors.workflows.invalid.index", "" + down));
        } else {
            Object toMove = postFunctions.get(down);
            CollectionReorderer.decreasePosition(postFunctions, toMove);
            workflowService.updateWorkflow(getJiraServiceContext(), getWorkflow());
        }

        return getViewRedirect("&currentCount=workflow-function" + (down + 2));
    }

    @RequiresXsrfCheck
    public String doChangeLogicOperator() throws Exception {
        WorkflowEditorTransitionConditionUtil wetcu = new WorkflowEditorTransitionConditionUtil();
        wetcu.changeLogicOperator(getTransition(), getCount());

        workflowService.updateWorkflow(getJiraServiceContext(), getWorkflow());

        return getViewRedirect("");
    }

    protected String getViewRedirect(String postfix) {
        if (getStep() == null) {
            return getRedirect("ViewWorkflowTransition.jspa" + getBasicWorkflowParameters() +
                    "&workflowTransition=" + getTransition().getId() + postfix);
        } else {
            return getRedirect("ViewWorkflowTransition.jspa" + getBasicWorkflowParameters() +
                    "&workflowStep=" + getStep().getId() +
                    "&workflowTransition=" + getTransition().getId() + postfix);
        }
    }

    public int getUp() {
        return up;
    }

    public void setUp(int up) {
        this.up = up;
    }

    public int getDown() {
        return down;
    }

    public void setDown(int down) {
        this.down = down;
    }

    public Collection getStepsForTransition() {
        return getWorkflow().getStepsForTransition(getTransition());
    }

    public boolean isInitial() {
        return getWorkflow().isInitialAction(getTransition());
    }

    public boolean isGlobal() {
        return getWorkflow().isGlobalAction(getTransition());
    }

    public boolean isCommon() {
        return getWorkflow().isCommonAction(getTransition());
    }

    public boolean isTransitionWithoutStepChange() {
        return getTransition().getUnconditionalResult().getStep() == JiraWorkflow.ACTION_ORIGIN_STEP_ID;
    }

    @Override
    public String getDescriptorTab() {
        final ImmutableList<WorkflowTransitionTabProvider.WorkflowTransitionTab> tabPanels = ImmutableList.copyOf(getTabPanels());
        final Optional<WorkflowTransitionTabProvider.WorkflowTransitionTab> firstTab = tabPanels.stream().findFirst();

        //If there are no tab panels available reset the descriptor tab
        if (!firstTab.isPresent()) {
            super.setDescriptorTab("");
        } else {
            final boolean noTabForDescriptor = tabPanels.stream()
                    .noneMatch(workflowTransitionTab -> workflowTransitionTab.getModule().getKey().equals(super.getDescriptorTab()));
            //If the specified descriptor tab does not have a panel then reset to the first panel
            if (noTabForDescriptor) {
                super.setDescriptorTab(firstTab.get().getModule().getKey());
            }
        }

        return super.getDescriptorTab();
    }

    @Override
    public void setDescriptorTab(String descriptorTab) {
        super.setDescriptorTab(descriptorTab);

        eventPublisher.publish(new WorkflowTransitionTabEvent(descriptorTab));
    }

    public FieldScreen getFieldScreen() {
        return workflowActionsBean.getFieldScreenForView(getTransition());
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getCurrentCount() {
        return currentCount;
    }

    public void setCurrentCount(String currentCount) {
        this.currentCount = currentCount;
    }

    public Iterator<WorkflowTransitionTabProvider.WorkflowTransitionTab> getTabPanels() {
        return workflowTransitionTabProvider.getTabs(getTransition(), getWorkflow()).iterator();
    }

    public String getTabPanelContent() {
        return workflowTransitionTabProvider.getTabContentHtml(getDescriptorTab(), getTransition(), getWorkflow());
    }
}