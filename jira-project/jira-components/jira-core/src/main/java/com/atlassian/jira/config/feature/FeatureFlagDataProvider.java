package com.atlassian.jira.config.feature;

import com.atlassian.jira.config.FeatureFlag;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.collect.Maps;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;
import java.util.Set;

public class FeatureFlagDataProvider implements WebResourceDataProvider {
    private final FeatureManager featureManager;
    private final ObjectMapper configuredObjectMapper;

    public FeatureFlagDataProvider(FeatureManager featureManager) {
        this.featureManager = featureManager;
        this.configuredObjectMapper = new ObjectMapper();
        configuredObjectMapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
    }

    @Override
    public Jsonable get() {
        Set<FeatureFlag> registeredFlags = featureManager.getRegisteredFlags();
        Map<String, Boolean> featureFlagData = Maps.newHashMap();

        for (FeatureFlag featureFlag : registeredFlags) {
            featureFlagData.put(featureFlag.featureKey(), featureFlag.isOnByDefault());
        }

        // FeatureManager.getEnabledFeatureKeys() only returns system property dark features, not site-wide ones
        // so we don't use it here
        Set<String> allJiraEnabledFeatures = featureManager.getDarkFeatures().getAllEnabledFeatures();

        final Map<String, Object> topLevelMap = Maps.newHashMap();
        topLevelMap.put("enabled-feature-keys", allJiraEnabledFeatures);
        topLevelMap.put("feature-flag-states", featureFlagData);

        return new Jsonable() {
            @Override
            public void write(Writer writer) throws IOException {
                configuredObjectMapper.writeValue(writer, topLevelMap);
            }
        };
    }
}
