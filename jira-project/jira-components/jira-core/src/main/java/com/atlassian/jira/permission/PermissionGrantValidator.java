package com.atlassian.jira.permission;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.scheme.SchemeType;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.security.plugin.ProjectPermissionTypesManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;

import java.util.Collections;
import java.util.Map;

import static com.atlassian.jira.util.ErrorCollections.create;
import static com.atlassian.jira.util.ErrorCollections.empty;
import static com.atlassian.jira.util.ErrorCollections.validationError;
import static com.atlassian.jira.util.lang.JiraStringUtils.toLong;

public class PermissionGrantValidator {
    private final PermissionTypeManager permissionTypeManager;
    private final ProjectPermissionTypesManager projectPermissionsManager;
    private final I18nHelper i18n;
    private final GroupManager groupManager;
    private final CustomFieldManager customFieldManager;
    private final ProjectRoleManager projectRoleManager;
    private final UserManager userManager;

    public PermissionGrantValidator(
            final PermissionTypeManager permissionTypeManager,
            final ProjectPermissionTypesManager projectPermissionsManager,
            final I18nHelper i18nHelper,
            final GroupManager groupManager,
            final CustomFieldManager customFieldManager,
            final ProjectRoleManager projectRoleManager,
            final UserManager userManager) {
        this.permissionTypeManager = permissionTypeManager;
        this.projectPermissionsManager = projectPermissionsManager;
        this.i18n = i18nHelper;
        this.groupManager = groupManager;
        this.customFieldManager = customFieldManager;
        this.projectRoleManager = projectRoleManager;
        this.userManager = userManager;
    }

    public ErrorCollection validateGrants(ApplicationUser user, Iterable<PermissionGrantInput> grants) {
        ErrorCollection result = empty();
        for (PermissionGrantInput grant : grants) {
            result.addErrorCollection(validateGrant(user, grant));
        }
        return result;
    }

    public ErrorCollection validateGrant(final ApplicationUser user, PermissionGrantInput grants) {
        ErrorCollection errorCollection = initialValidation(grants);
        return errorCollection.hasAnyErrors() ?
                errorCollection :
                validationImplementedByTypes(user, grants);
    }

    private ErrorCollection initialValidation(PermissionGrantInput grant) {
        ErrorCollection result = empty();
        result.addErrorCollection(validatePermission(grant.getPermission()));
        result.addErrorCollection(validateHolder(grant.getHolder()));
        return result;
    }

    private ErrorCollection validatePermission(final ProjectPermissionKey permission) {
        if (!projectPermissionsManager.exists(permission)) {
            return validationError("permission", i18n.getText("admin.schemes.permissions.validation.permission.unrecognized", permission));
        } else {
            return empty();
        }
    }

    private ErrorCollection validateHolder(final PermissionHolder holder) {
        Option<JiraPermissionHolderType> holderType = JiraPermissionHolderType.fromKey(holder.getType().getKey(), holder.getParameter().getOrNull());
        if (holderType.isDefined()) {
            switch (holderType.get()) {
                case GROUP:
                    if (!groupManager.groupExists(holder.getParameter().get())) {
                        return validationError("holder.parameter", i18n.getText("admin.schemes.permissions.validation.group.does.not.exist", holder.getParameter().get()));
                    }
                    break;
                case USER_CUSTOM_FIELD:
                case GROUP_CUSTOM_FIELD:
                    return validateCustomField(holder.getParameter().get());
                case PROJECT_ROLE:
                    Option<Long> roleId = toLong(holder.getParameter().get());
                    if (roleId.isEmpty()) {
                        return validationError("holder.parameter", i18n.getText("admin.schemes.permissions.validation.project.role.id.must.be.number"));
                    } else if (projectRoleManager.getProjectRole(roleId.get()) == null) {
                        return validationError("holder.parameter", i18n.getText("admin.schemes.permissions.validation.project.role.does.not.exist", holder.getParameter().get()));
                    }
                    break;
                case USER:
                    if (userManager.getUserByName(holder.getParameter().get()) == null) {
                        return validationError("holder.parameter", i18n.getText("admin.schemes.permissions.validation.user.does.not.exist", holder.getParameter().get()));
                    }
                    break;
                default:
                    return empty();
            }
        }
        return empty();
    }

    private ErrorCollection validateCustomField(final String customField) {
        if (!customFieldManager.exists(customField)) {
            return validationError("holder.parameter", i18n.getText("admin.schemes.permissions.validation.cf.does.not.exist", customField));
        }
        return empty();
    }

    /**
     * This method returns a parameter map accepted by {@link com.atlassian.jira.scheme.SchemeType} implementations.
     * It has a single entry of the form {@code holderTypeKey -> parameterValue}, e.g. {@code "group" -> "jira-developers"}.
     */
    private Map<String, String> getParameters(final PermissionHolder holder) {
        return holder.getParameter().map(new Function<String, Map<String, String>>() {
            @Override
            public Map<String, String> apply(final String parameter) {
                return ImmutableMap.of(holder.getType().getKey(), parameter);
            }
        }).getOrElse(Collections.<String, String>emptyMap());
    }


    private ErrorCollection validationImplementedByTypes(ApplicationUser user, PermissionGrantInput entity) {
        JiraServiceContext context = new JiraServiceContextImpl(user, empty(), i18n);
        String key = entity.getHolder().getType().getKey();
        SchemeType type = permissionTypeManager.getSchemeType(key);

        if (type == null) {
            return validationError("holder.type", i18n.getText("admin.schemes.permissions.validation.holder.type.unrecognized", key));
        }

        if (!type.isValidForPermission(entity.getPermission())) {
            return create(i18n.getText("admin.permissions.errors.invalid.combination", entity.getPermission().permissionKey(), key), ErrorCollection.Reason.VALIDATION_FAILED);
        }

        type.doValidation(key, getParameters(entity.getHolder()), context);
        return context.getErrorCollection();
    }
}
