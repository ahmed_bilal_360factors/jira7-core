package com.atlassian.jira.bc.project.index;

import com.atlassian.jira.config.IndexTask;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.I18nHelper;

/**
 * Context for project index operations. There can be only one such operation
 * per project at any given time.
 *
 * @since v6.1
 */
public class ProjectIndexTaskContext implements IndexTask {
    private static final long serialVersionUID = -1875765202444481474L;

    private final Long projectId;
    private final String projectName;
    private final String nodeId;

    public ProjectIndexTaskContext(final Project project, final String nodeId) {
        this.nodeId = nodeId;
        this.projectId = project.getId();
        this.projectName = project.getName();
    }

    @Override
    public String getTaskInProgressMessage(final I18nHelper i18n) {
        return i18n.getText("admin.notifications.reindex.in.progress.project", projectName);
    }

    @Override
    public String buildProgressURL(final Long taskId) {
        return "/secure/project/IndexProjectProgress.jspa?pid=" + projectId + "&taskId=" + taskId;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProjectIndexTaskContext)) {
            return false;
        }

        final ProjectIndexTaskContext that = (ProjectIndexTaskContext) o;

        if (!projectId.equals(that.projectId)) {
            return false;
        }

        if (nodeId == null) {
            return that.nodeId == null;
        }
        return nodeId.equals(that.nodeId);
    }

    @Override
    public int hashCode() {
        int result = projectId.hashCode();
        result = 31 * result + (nodeId != null ? nodeId.hashCode() : 0);
        return result;
    }
}
