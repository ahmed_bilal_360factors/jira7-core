package com.atlassian.jira.model.querydsl;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Generated;

/**
 * Data Transfer Object for the BoardProject entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 *
 * @see QBoardProject
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class BoardProjectDTO implements DTO {
    private final Long boardId;
    private final Long projectId;

    public Long getBoardId() {
        return boardId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public BoardProjectDTO(Long boardId, Long projectId) {
        this.boardId = boardId;
        this.projectId = projectId;
    }

    /**
     * Creates a GenericValue object from the values in this Data Transfer Object.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
     * @return a GenericValue object constructed from the values in this Data Transfer Object.
     */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator) {
        return ofBizDelegator.makeValue("BoardProject", new FieldMap()
                .add("boardId", boardId)
                .add("projectId", projectId)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */
    public static BoardProjectDTO fromGenericValue(GenericValue gv) {
        return new BoardProjectDTO(
                gv.getLong("boardId"),
                gv.getLong("projectId")
        );
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(BoardProjectDTO boardProjectDTO) {
        return new Builder(boardProjectDTO);
    }

    public static class Builder {
        private Long boardId;
        private Long projectId;

        public Builder() {
        }

        public Builder(BoardProjectDTO boardProjectDTO) {
            this.boardId = boardProjectDTO.boardId;
            this.projectId = boardProjectDTO.projectId;
        }

        public BoardProjectDTO build() {
            return new BoardProjectDTO(boardId, projectId);
        }

        public Builder boardId(Long boardId) {
            this.boardId = boardId;
            return this;
        }
        public Builder projectId(Long projectId) {
            this.projectId = projectId;
            return this;
        }
    }
}