package com.atlassian.jira.plugin.icon;

import com.atlassian.jira.icon.IconType;
import com.atlassian.ozymandias.PluginPointVisitor;
import com.atlassian.ozymandias.SafeAccessViaPluginAccessor;
import com.atlassian.ozymandias.SafePluginPointAccess;
import com.atlassian.plugin.PluginAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

@Component
public class IconTypeDefinitionFactoryImpl implements IconTypeDefinitionFactory {
    private static final Logger log = LoggerFactory.getLogger(IconTypeDefinitionFactoryImpl.class);

    private PluginAccessor pluginAccessor;

    public IconTypeDefinitionFactoryImpl(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    private IconTypeNameCollector collectIconTypes() {
        IconTypeNameCollector collector = new IconTypeNameCollector();

        SafeAccessViaPluginAccessor safe =  SafePluginPointAccess.to(pluginAccessor);

        safe.forType(IconTypeModuleDescriptor.class, collector);
        return collector;
    }

    @Override
    public IconTypeDefinition getDefinition(IconType iconType) {
        return collectIconTypes().getDefinition(iconType);
    }

    @Override
    @Nonnull
    public String getDefaultSystemIconFilename(IconType iconType) {
        IconTypeModuleDescriptor moduleDescriptor = collectIconTypes().getModule(iconType);
        if (moduleDescriptor == null) {
            throw new IllegalArgumentException("Unknown icon type '" + iconType + "'");
        } else {
            return moduleDescriptor.getDefaultFilename();
        }
    }

    public class IconTypeNameCollector implements PluginPointVisitor<IconTypeModuleDescriptor, IconTypeDefinition> {
        private Map<String, IconTypeDefinition> nameToDefinitionMap = new HashMap<>();
        private Map<String, IconTypeModuleDescriptor> nameToModuleMap = new HashMap<>();

        @Override
        public void visit(IconTypeModuleDescriptor moduleDescriptor, IconTypeDefinition module) {
            if (module.getKey() == null) {
                log.warn("IconType class " + module.getClass().getName() + " supplied a null name. This IconType will be ignored");
            } else if (nameToDefinitionMap.containsKey(module.getKey())) {
                log.warn("IconType calss " + module.getClass().getName() + " supplied a key (" + module.getKey() +
                        ") that is already used by the IconType " + nameToDefinitionMap.get(module.getKey()).getClass().getName() +
                        ". This IconType will be ignored");
            } else {
                nameToDefinitionMap.put(module.getKey(), module);
                nameToModuleMap.put(module.getKey(), moduleDescriptor);
            }
        }

        public IconTypeDefinition getDefinition(IconType iconType) {
            return nameToDefinitionMap.get(iconType.getKey());
        }

        public IconTypeModuleDescriptor getModule(IconType iconType) {
            return nameToModuleMap.get(iconType.getKey());
        }
    }
}
