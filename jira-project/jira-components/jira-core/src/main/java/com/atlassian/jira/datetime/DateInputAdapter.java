package com.atlassian.jira.datetime;

import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public enum DateInputAdapter {

    INSTANCE;

    private final List<Rule> rules = ImmutableList.of(
            Rule.replace("Mrz", "M\u00e4r").in(Locale.GERMANY),
            Rule.replace("Marz", "M\u00e4r").in(Locale.GERMANY),
            Rule.replace("Mar", "M\u00e4r").in(Locale.GERMANY)
    );

    /**
     * Transforms date input to cover formats not supported in Java.
     *
     * <p>
     * Some rules for formatting dates have changed across Java versions.
     * Certain input values which were working in JIRA before, don't work anymore.
     * We could tell customers to HDFU but let's be friendly and nice instead.
     *
     * <p>
     *     For example, this will change "1/Mrz/2017" to "1/Mär/2017".
     *     Only the latter is supported since Java 8, but some people
     *     got used to using the former.
     */
    public String adapt(String date, Locale locale) {
        for (Rule rule : rules) {
            date = rule.apply(date, locale);
        }
        return date;
    }

    private final static class Rule {
        private final Locale locale;
        private final Pattern pattern;
        private final String replacement;

        public static Rule replace(String original, String replacement) {
            Pattern pattern = Pattern.compile("([^\\w]|^)" + original + "([^\\w]|$)", Pattern.CASE_INSENSITIVE);
            return new Rule(null, pattern, "$1" + replacement + "$2");
        }

        public Rule in(Locale locale) {
            return new Rule(locale, pattern, replacement);
        }

        private Rule(Locale locale, Pattern pattern, String replacement) {
            this.locale = locale;
            this.pattern = pattern;
            this.replacement = replacement;
        }

        public String apply(String text, Locale locale) {
            if (text != null && (this.locale == null || this.locale.equals(locale))) {
                return pattern.matcher(text).replaceAll(replacement);
            } else {
                return text;
            }
        }
    }
}
