package com.atlassian.jira.cluster.zdu;

import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.loaders.PluginLoader;
import com.atlassian.plugin.loaders.classloading.Scanner;

import java.io.File;
import java.util.List;

/**
 * @since v7.3
 */
public interface ClusterUpgradePluginLoaderFactory {
    PluginLoader create(Scanner installedPluginsScanner, File installedPluginsDirectory, File freezeFile, List<PluginFactory> pluginFactories);
}
