package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.admin.ProjectAdminSidebarFeature;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

/**
 * @since 7.1
 * Determines when the project-centric sidebar is present in the project administration
 */
public class IsAdminSidebarPresentCondition extends AbstractWebCondition {
    @Override
    public boolean shouldDisplay(final ApplicationUser user, final JiraHelper jiraHelper) {
        return ComponentAccessor.getComponent(ProjectAdminSidebarFeature.class).shouldDisplay();
    }
}
