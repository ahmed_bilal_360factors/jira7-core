package com.atlassian.jira.soy;

import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.soy.renderer.JsExpression;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang.StringEscapeUtils;

import javax.annotation.Nonnull;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.jira.util.dbc.Assertions.is;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Abstract Soy function that returns help information for a given key.
 *
 * @since v7.0
 */
public abstract class AbstractHelpFunction implements SoyServerFunction<String>, SoyClientFunction {

    private static final ImmutableSet<Integer> ARGUMENTS_SIZE = ImmutableSet.of(1);
    private static final Pattern SOY_STRING_PATTERN = Pattern.compile("^'(.*)'$");
    final String MSG_INVALID_ARGS = "Wrong number of arguments (1 expected)";

    private HelpUrls helpUrls;

    public AbstractHelpFunction(HelpUrls helpUrls) {
        this.helpUrls = notNull("helpUrls", helpUrls);
    }

    @Nonnull
    abstract String getHelpValue(HelpUrl helpUrl);

    //Client-side
    @Override
    public JsExpression generate(JsExpression... args) {
        is(MSG_INVALID_ARGS, args.length == 1);
        Matcher matcher = SOY_STRING_PATTERN.matcher(args[0].getText());
        is("The help key should be a string literal", matcher.matches());
        final String helpUrlKey = matcher.group(1);
        return new JsExpression('"' + StringEscapeUtils.escapeJavaScript(getDisplayValueForHelpUrl(helpUrlKey)) + '"');
    }

    //Server-side
    @Override
    public String apply(Object... args) {
        is(MSG_INVALID_ARGS, args.length == 1);
        is("The help key should be a String", args[0] instanceof String);
        final String helpUrlKey = (String) args[0];
        return getDisplayValueForHelpUrl(helpUrlKey);
    }

    @Nonnull
    private String getDisplayValueForHelpUrl(@Nonnull final String key) {
        return getHelpValue(getHelpUrl(key));
    }

    @Nonnull
    private HelpUrl getHelpUrl(@Nonnull final String key) {
        return helpUrls.getUrl(key);
    }

    @Override
    public Set<Integer> validArgSizes() {
        return ARGUMENTS_SIZE;
    }
}
