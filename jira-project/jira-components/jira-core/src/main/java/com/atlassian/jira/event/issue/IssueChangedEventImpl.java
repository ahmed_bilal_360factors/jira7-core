package com.atlassian.jira.event.issue;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

public class IssueChangedEventImpl implements IssueChangedEvent {

    private final Collection<ChangeItemBean> changeItems;
    private final Optional<ApplicationUser> author;
    private final Optional<Comment> comment;
    private final Issue issue;
    private final Date eventTime;

    public IssueChangedEventImpl(
            @Nonnull Issue issue,
            @Nonnull Optional<ApplicationUser> author,
            @Nonnull Collection<ChangeItemBean> changeItems,
            @Nonnull Optional<Comment> comment, Date eventTime) {
        this.changeItems = changeItems;
        this.author = author;
        this.comment = comment;
        this.issue = issue;
        this.eventTime = eventTime;
    }

    @Nonnull
    @Override
    public Collection<ChangeItemBean> getChangeItems() {
        return changeItems;
    }

    @Nonnull
    @Override
    public Optional<ChangeItemBean> getChangeItemForField(@Nonnull String fieldName) {
        return ChangeItemBeanKit.getFieldChange(fieldName, changeItems);
    }

    @Nonnull
    @Override
    public Optional<ApplicationUser> getAuthor() {
        return author;
    }

    @Nonnull
    @Override
    public Optional<Comment> getComment() {
        return comment;
    }

    @Override
    public Issue getIssue() {
        return issue;
    }

    @Override
    public Date getTime() {
        return eventTime;
    }

    @Override
    public Map<String, Object> getParams() {
        return Collections.emptyMap();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("changeItems", changeItems)
                .add("author", author)
                .add("comment", comment)
                .add("issue", issue)
                .add("eventTime", eventTime)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IssueChangedEventImpl that = (IssueChangedEventImpl) o;
        return Objects.equal(changeItems, that.changeItems) &&
                Objects.equal(author, that.author) &&
                Objects.equal(comment, that.comment) &&
                Objects.equal(issue, that.issue) &&
                Objects.equal(eventTime, that.eventTime);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(changeItems, author, comment, issue, eventTime);
    }
}
