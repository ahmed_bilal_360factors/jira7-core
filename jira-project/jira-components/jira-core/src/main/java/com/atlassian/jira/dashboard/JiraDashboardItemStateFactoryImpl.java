package com.atlassian.jira.dashboard;

import com.atlassian.gadgets.DashboardItemModuleId;
import com.atlassian.gadgets.DashboardItemModuleIdVisitor;
import com.atlassian.gadgets.DashboardItemState;
import com.atlassian.gadgets.GadgetId;
import com.atlassian.gadgets.GadgetState;
import com.atlassian.gadgets.LocalDashboardItemModuleId;
import com.atlassian.gadgets.LocalDashboardItemState;
import com.atlassian.gadgets.OpenSocialDashboardItemModuleId;
import com.atlassian.gadgets.dashboard.Color;
import com.atlassian.gadgets.dashboard.spi.DashboardItemStateFactory;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.portal.OfbizPortletConfigurationStore;
import org.ofbiz.core.entity.DelegatorInterface;

import java.net.URI;

public class JiraDashboardItemStateFactoryImpl implements DashboardItemStateFactory {
    private static final Color DEFAULT_JIRA_DASHBOARD_ITEM_CHROME_COLOR = Color.color1;

    @Override
    public DashboardItemState createDashboardItemState(final DashboardItemModuleId moduleId) {
        return moduleId.accept(new DashboardItemModuleIdVisitor<DashboardItemState>() {
            @Override
            public DashboardItemState visit(final OpenSocialDashboardItemModuleId openSocialModuleId) {
                return createGadgetState(openSocialModuleId);
            }

            @Override
            public DashboardItemState visit(final LocalDashboardItemModuleId dashboardItemModuleId) {
                return createLocalDashboardState(dashboardItemModuleId);
            }
        });
    }

    @Override
    public LocalDashboardItemState createLocalDashboardState(final LocalDashboardItemModuleId dashboardItemModuleId) {
        return LocalDashboardItemState.builder()
                .gadgetId(getNewGadgetId())
                .dashboardItemModuleId(dashboardItemModuleId)
                .color(DEFAULT_JIRA_DASHBOARD_ITEM_CHROME_COLOR)
                .build();
    }

    @Override
    public GadgetState createGadgetState(final OpenSocialDashboardItemModuleId openSocialDashboardItemModuleId) {
        final URI uri = openSocialDashboardItemModuleId.getSpecUri();
        return GadgetState.gadget((getNewGadgetId())).specUri(uri).color(DEFAULT_JIRA_DASHBOARD_ITEM_CHROME_COLOR).build();
    }

    private GadgetId getNewGadgetId() {
        final Long nextSeqId = ComponentAccessor.getComponent(DelegatorInterface.class).getNextSeqId(OfbizPortletConfigurationStore.TABLE);
        return GadgetId.valueOf(nextSeqId.toString());
    }
}
