package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.entity.Delete;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.issue.subscription.DefaultSubscriptionManager;
import com.atlassian.jira.scheduler.ClusteredJobFactory;
import com.atlassian.jira.scheduler.JiraParameterMapSerializer;
import com.atlassian.jira.scheduler.OfBizClusteredJob;
import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;
import com.atlassian.jira.upgrade.tasks.util.CronExpressionFixer;
import com.atlassian.jira.upgrade.tasks.util.CronExpressionFixer.Result;
import com.atlassian.jira.web.component.cron.parser.CronExpressionParser;
import com.atlassian.scheduler.SchedulerRuntimeException;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.caesium.impl.ImmutableClusteredJob;
import com.atlassian.scheduler.caesium.spi.ClusteredJob;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import com.google.common.collect.ImmutableMap;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Date;
import java.util.List;

import static com.atlassian.jira.web.component.cron.parser.CronExpressionParser.DEFAULT_CRONSTRING;

/**
 * Upgrade the Filter Subscription schedule.
 * <p>
 * Historical note: This upgrade task was originally numbered 6303, but it used the wrong ID column when
 * resolving the Quartz triggers, leading to incorrect results.  The fixed upgrade task was remapped first
 * to 6307 in the 6.3-OD-1 release and 6317 for development systems.  Although the upgrade task is idempotent,
 * it is based on the old quartz tables, which will not be updated once the data has migrated to the
 * new ones.  Therefore, we should not do anything as 6317 if the Quartz data has already been fixed in
 * build 6307.
 * </p>
 * <p>
 * To further complicate matters, in JIRA 7.0, we are changing the scheduler implementation again, which means
 * that we need to change how this upgrade task works.  If 6317 was previously run, then the subscriptions have
 * already been migrated from the QRTZ tables to the JQUARTZ tables.  If not, then we will need to run this
 * migration *after* the JQUARTZ to clusteredjob migration, as otherwise that would clobber the subscriptions
 * when it deletes everything from clusteredjob for idempotency.
 * </p>
 *
 * @since v6.3 (as UpgradeTask_Build6317)
 */
public class UpgradeTask_Build70011 extends AbstractImmediateUpgradeTask {
    private static final Logger LOG = LoggerFactory.getLogger(UpgradeTask_Build70011.class);

    static final String JOB_RUNNER_KEY = DefaultSubscriptionManager.class.getName();
    static final String SUBSCRIPTION_PREFIX = DefaultSubscriptionManager.class.getName();
    static final String SUBSCRIPTION_IDENTIFIER = "SUBSCRIPTION_ID";

    static final String FILTER_GROUP_NAME = "SEND_SUBSCRIPTION";
    static final String FILTER_NAME_PREFIX = "SUBSCRIPTION_";

    private final EntityEngine entityEngine;
    private final SchedulerService schedulerService;
    private final JiraParameterMapSerializer jiraParameterMapSerializer;

    public UpgradeTask_Build70011(final EntityEngine entityEngine, final SchedulerService schedulerService, final JiraParameterMapSerializer jiraParameterMapSerializer) {
        super();
        this.entityEngine = entityEngine;
        this.schedulerService = schedulerService;
        this.jiraParameterMapSerializer = jiraParameterMapSerializer;
    }

    @Override
    public int getBuildNumber() {
        return 70011;
    }

    @Override
    public String getShortDescription() {
        return "Adding filter subscriptions to atlassian-scheduler";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception {
        if (hasRun("com.atlassian.jira.upgrade.tasks.UpgradeTask_Build6307")) {
            LOG.info("This upgrade task already ran as 6307; skipping it...");
        } else if (hasRun("com.atlassian.jira.upgrade.tasks.UpgradeTask_Build6317")) {
            LOG.info("This upgrade task already ran as 6317; skipping it...");
        } else {
            upgradeFilterSubscriptionSchedules();
        }
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        return false;
    }

    private boolean hasRun(String upgradeClassName) {
        return Select.id()
                .from("UpgradeHistory")
                .whereEqual("upgradeclass", upgradeClassName)
                .runWith(entityEngine)
                .count() > 0;
    }

    void upgradeFilterSubscriptionSchedules() throws GenericEntityException, SchedulerServiceException {
        final List<GenericValue> jobTriggers = Select.from("QRTZTriggers")
                .whereEqual("triggerGroup", FILTER_GROUP_NAME)
                .runWith(entityEngine)
                .asList();

        for (GenericValue jobTrigger : jobTriggers) {
            final String triggerName = jobTrigger.getString("triggerName");
            final Long id = Long.parseLong(triggerName.substring(FILTER_NAME_PREFIX.length()));
            final Schedule schedule = getSchedule(jobTrigger, triggerName);
            if (schedule != null) {
                createFilterSubscriptionSchedule(id, schedule);
            }
        }
    }

    private void createFilterSubscriptionSchedule(final Long id, final Schedule schedule)
            throws SchedulerServiceException {
        final JobId jobId = toJobId(id);
        final Date nextRunTime = schedulerService.calculateNextRunTime(schedule);
        final ClusteredJob clusteredJob = ImmutableClusteredJob.builder()
                .jobId(jobId)
                .jobRunnerKey(JobRunnerKey.of(JOB_RUNNER_KEY))
                .nextRunTime(nextRunTime)
                .schedule(schedule)
                .parameters(toRawParameters(id))
                .build();
        Delete.from(Entity.CLUSTERED_JOB)
                .whereEqual(ClusteredJobFactory.JOB_ID, jobId.toString())
                .execute(entityEngine);
        entityEngine.createValue(Entity.CLUSTERED_JOB, new OfBizClusteredJob(null, clusteredJob));
    }

    private Schedule getSchedule(final GenericValue jobTrigger, final String triggerName) {
        // Get the related trigger (cron or simple)
        final String triggerType = jobTrigger.getString("triggerType");

        if ("SIMPLE".equals(triggerType)) {
            return getSimpleTriggerSchedule(jobTrigger, triggerName);
        }

        if ("CRON".equals(triggerType)) {
            return getCronTriggerSchedule(jobTrigger, triggerName);
        }

        LOG.error("Unable to migrate filter subscription trigger '" + triggerName +
                "'; unsupported trigger type '" + triggerType + '\'');
        return null;
    }

    private Schedule getSimpleTriggerSchedule(final GenericValue jobTrigger, final String triggerName) {
        GenericValue simpleTrigger = getTriggerDetails("QRTZSimpleTriggers", jobTrigger);
        if (simpleTrigger == null) {
            LOG.error("Missing simple trigger data for filter subscription trigger '" + triggerName + '\'');
            return null;
        }

        final Long repeatInterval = simpleTrigger.getLong("repeatInterval");
        if (repeatInterval == null) {
            LOG.error("Missing repeatInterval for filter subscription simple trigger '" + triggerName + '\'');
            return null;
        }

        return Schedule.forInterval(repeatInterval, null);
    }

    private Schedule getCronTriggerSchedule(final GenericValue jobTrigger, final String triggerName) {
        final GenericValue cronTrigger = getTriggerDetails("QRTZCronTriggers", jobTrigger);
        if (cronTrigger == null) {
            LOG.error("Missing cron trigger data for filter subscription trigger '" + triggerName + '\'');
            return null;
        }

        // Note: the column is misspelled as "cronexperssion", but the field name is correct.
        final String cronExpression = cronTrigger.getString("cronExpression");
        if (cronExpression == null) {
            LOG.error("Missing cronExpression for filter subscription cron trigger '" + triggerName + '\'');
            return null;
        }

        return Schedule.forCronExpression(repairCronExpression(cronExpression, triggerName));
    }

    @Nullable
    private GenericValue getTriggerDetails(String entityName, GenericValue trigger) {
        final List<GenericValue> triggerDetails = Select.from(entityName)
                .whereEqual("trigger", trigger.getLong("id"))
                .runWith(entityEngine)
                .asList();
        return triggerDetails.isEmpty() ? null : triggerDetails.get(0);
    }

    private byte[] toRawParameters(Long subscriptionId) {
        try {
            return jiraParameterMapSerializer.serializeParameters(ImmutableMap.of(SUBSCRIPTION_IDENTIFIER, subscriptionId));
        } catch (SchedulerServiceException sse) {
            throw new SchedulerRuntimeException("Unable to serialize parameter map", sse);
        }
    }

    private static JobId toJobId(final Long subId) {
        return JobId.of(SUBSCRIPTION_PREFIX + ':' + subId);
    }


    @Nullable
    @Override
    public Integer dependsUpon() {
        return 70010;
    }

    private static String repairCronExpression(String cronExpression, String triggerName) {
        final Result result = CronExpressionFixer.repairCronExpression(cronExpression);
        final String repaired = result.getNewCronExpression().orElse(null);
        if (repaired != null) {
            LOG.warn("Repairing cron expression for filter subscription '{}': '{}' => '{}'",
                    triggerName, cronExpression, repaired);
            return repaired;
        }

        return result.getCronSyntaxException()
                .map(cse -> handleFailedRepair(triggerName, cronExpression, cse))
                .orElse(cronExpression);
    }

    private static String handleFailedRepair(String triggerName, String cronExpression, Exception cse) {
        LOG.warn("Reverting filter subscription '{}' to default schedule: '{}' => '{}': {}",
                triggerName, cronExpression, DEFAULT_CRONSTRING, cse.toString());
        return DEFAULT_CRONSTRING;
    }
}
