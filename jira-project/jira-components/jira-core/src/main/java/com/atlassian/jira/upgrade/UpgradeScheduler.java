package com.atlassian.jira.upgrade;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.JiraDelayedUpgradeCompletedEvent;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

/**
 * Used for scheduling the execution of delayed upgrade tasks.
 */
public class UpgradeScheduler {
    public static final JobRunnerKey JOB_RUNNER_KEY = JobRunnerKey.of(UpgradeService.class.getName());
    public static final JobId JOB_ID = JobId.of(UpgradeService.class.getName());

    private static final Logger LOGGER = LoggerFactory.getLogger(UpgradeScheduler.class);

    private final IndexingUpgradeService indexingUpgradeService;
    private final SchedulerService schedulerService;
    private final EventPublisher eventPublisher;
    private final BuildUtilsInfo buildUtilsInfo;

    public UpgradeScheduler(
            final IndexingUpgradeService indexingUpgradeService,
            final SchedulerService schedulerService,
            final EventPublisher eventPublisher,
            final BuildUtilsInfo buildUtilsInfo
    ) {
        this.indexingUpgradeService = indexingUpgradeService;
        this.schedulerService = schedulerService;
        this.eventPublisher = eventPublisher;
        this.buildUtilsInfo = buildUtilsInfo;

        this.schedulerService.registerJobRunner(JOB_RUNNER_KEY, this::runUpgrades);
    }

    private JobRunnerResponse runUpgrades(final JobRunnerRequest jobRunnerRequest) {
        LOGGER.info("Running schedules upgrades");
        final UpgradeResult upgradeResult = indexingUpgradeService.runUpgrades();

        if (upgradeResult.successful()) {
            LOGGER.info("Schedules upgrades completed successfully");
            eventPublisher.publish(new JiraDelayedUpgradeCompletedEvent(buildUtilsInfo.getCurrentBuildNumber()));
            return JobRunnerResponse.success();
        } else {
            LOGGER.info("Scheduled upgrades completed with an error");
            return JobRunnerResponse.failed("An error occurred running the scheduled upgrade tasks");
        }
    }

    public UpgradeResult scheduleUpgrades(final int delayInMinutes) {
        final JobConfig jobConfig = JobConfig
                .forJobRunnerKey(JOB_RUNNER_KEY)
                .withSchedule(Schedule.runOnce(DateTime.now().plusMinutes(delayInMinutes).toDate()));
        try {
            LOGGER.info("Scheduling upgrades to run in {} minute(s)", delayInMinutes);

            schedulerService.scheduleJob(JOB_ID, jobConfig);
            return UpgradeResult.OK;
        } catch (SchedulerServiceException e) {
            LOGGER.error("Unable to schedule upgrades for future execution :" + e, e);

            return new UpgradeResult(createErrorMessages(e));
        }
    }

    private Collection<String> createErrorMessages(final Exception e) {
        return ImmutableList.of(
                "Unable to schedule upgrade tasks for future execution: " + e.getMessage() + '\n' +
                        ExceptionUtils.getStackTrace(e)
        );
    }
}
