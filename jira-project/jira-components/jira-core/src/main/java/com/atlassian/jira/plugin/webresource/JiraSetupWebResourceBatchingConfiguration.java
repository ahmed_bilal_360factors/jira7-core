package com.atlassian.jira.plugin.webresource;

import com.atlassian.jira.config.InstanceFeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Determines which resources are included superbatched during setup only!
 *
 * @since v7.3
 */
public class JiraSetupWebResourceBatchingConfiguration extends JiraWebResourceBatchingConfiguration {

    private final List<String> setupResources;

    public JiraSetupWebResourceBatchingConfiguration(final JiraProperties jiraSystemProperties,
                                                     final InstanceFeatureManager featureManager) {
        super(jiraSystemProperties, featureManager);
        this.setupResources = Lists.newArrayList();
    }

    @Override
    public List<String> getSuperBatchModuleCompleteKeys() {
        return setupResources;
    }
}
