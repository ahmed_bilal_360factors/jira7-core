package com.atlassian.jira.project.template.module;

import com.atlassian.plugin.Plugin;

import java.util.Optional;

public class DemoProjectModuleBuilder {
    private String key;
    private Integer weight;
    private String labelKey;
    private String descriptionKey;
    private Optional<String> longDescriptionKey;
    private String importFile;
    private Optional<String> projectTemplateKey;
    private Optional<String> projectTypeKey;
    private Icon icon;
    private Optional<Icon> backgroundIcon;
    private Plugin modulePlugin;


    public DemoProjectModuleBuilder setKey(String key) {
        this.key = key;
        return this;
    }

    public DemoProjectModuleBuilder setWeight(Integer weight) {
        this.weight = weight;
        return this;
    }

    public DemoProjectModuleBuilder setLabelKey(String labelKey) {
        this.labelKey = labelKey;
        return this;
    }

    public DemoProjectModuleBuilder setDescriptionKey(String descriptionKey) {
        this.descriptionKey = descriptionKey;
        return this;
    }

    public DemoProjectModuleBuilder setLongDescriptionKey(Optional<String> longDescriptionKey) {
        this.longDescriptionKey = longDescriptionKey;
        return this;
    }

    public DemoProjectModuleBuilder setImportFile(String importFile) {
        this.importFile = importFile;
        return this;
    }

    public DemoProjectModuleBuilder setProjectTemplateKey(Optional<String> projectTemplateKey) {
        this.projectTemplateKey = projectTemplateKey;
        return this;
    }

    public DemoProjectModuleBuilder setProjectTypeKey(Optional<String> projectTypeKey) {
        this.projectTypeKey = projectTypeKey;
        return this;
    }

    public DemoProjectModuleBuilder setIcon(Icon icon) {
        this.icon = icon;
        return this;
    }

    public DemoProjectModuleBuilder setBackgroundIcon(Optional<Icon> backgroundIcon) {
        this.backgroundIcon = backgroundIcon;
        return this;
    }

    public DemoProjectModuleBuilder setModulePlugin(Plugin modulePlugin) {
        this.modulePlugin = modulePlugin;
        return this;
    }

    public DemoProjectModule createDemoProjectModule() {
        return new DefaultDemoProjectModule(key, weight, labelKey, descriptionKey, longDescriptionKey, importFile, projectTemplateKey, projectTypeKey, icon, backgroundIcon, modulePlugin);
    }
}