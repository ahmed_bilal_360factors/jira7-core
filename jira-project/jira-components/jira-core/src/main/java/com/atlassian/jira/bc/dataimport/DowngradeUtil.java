package com.atlassian.jira.bc.dataimport;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.model.querydsl.UpgradeHistoryDTO;
import com.atlassian.jira.upgrade.DowngradeException;
import com.atlassian.jira.upgrade.DowngradeTask;
import com.atlassian.jira.upgrade.DowngradeTaskFileParser;
import com.atlassian.jira.upgrade.MissingDowngradeTaskException;
import com.atlassian.jira.upgrade.ReindexRequirement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Static utility methods for running a database downgrade.
 * <p>
 * This is used in two separate places:
 * <ul>
 * <li>First we get Upgrade History items from the import XML file and use this class to calculate if a
 * downgrade is even possible (else we don't write to the DB).
 * </li>
 * <li>Second we use it after the import data is in the DB and we want to run the actual downgrade.
 * </li>
 * </ul>
 *
 * @since v6.4.6
 */
public class DowngradeUtil {
    private static final Logger log = LoggerFactory.getLogger(DowngradeUtil.class);
    private static final String AP_KEY_REINDEX_REQUIREMENT = "jira.downgrade.ReindexRequirement";

    // Status for an upgrade task that is complete
    private static final String STATUS_COMPLETE = "complete";
    // Status for a pending background upgrade task
    private static final String STATUS_PENDING = "pending";
    private static final String DOWNGRADE_FILENAME = "downgrades.xml";

    /**
     * Given a list of all upgrade History items, determine if a Downgrade should be successful.
     *
     * @param upgradeHistoryItems History of upgrades that have already run (usually comes from an import file)
     * @param targetBuildNumber   target build number
     * @return true if a Downgrade should be successful.
     */
    public static boolean canDowngrade(final List<UpgradeHistoryDTO> upgradeHistoryItems, final int targetBuildNumber) {
        try {
            final List<Integer> downgradeTaskNumbers = findDowngradeTasksToRun(upgradeHistoryItems, targetBuildNumber);
            final Map<Integer, DowngradeTask> downgradeTaskMap = loadDowngradeTasks();
            verifyTasksExist(downgradeTaskNumbers, downgradeTaskMap);
            return true;
        } catch (DowngradeException ex) {
            log.error("Downgrade cannot proceed: " + ex.getMessage());
            return false;
        }
    }

    /**
     * Finds downgrade tasks that are needed to be run in order to downgrade version of JIRA to targetBuildNumber.
     *
     * @param upgradeHistoryItems History of upgrades that have already run (usually comes from an import file)
     * @param targetBuildNumber   build number to downgrade to
     * @return List of downgrade task numbers
     */
    @Nonnull
    public static List<Integer> findDowngradeTasksToRun(List<UpgradeHistoryDTO> upgradeHistoryItems,
                                                        int targetBuildNumber) throws DowngradeException {
        // The build number in the code. For a downgrade this is lower than the build number in the DB
        Set<Integer> uniqueBuildNumbers = new HashSet<>();
        boolean oldUpgradeTaskFound = false;
        // analyse the upgrade tasks
        for (UpgradeHistoryDTO upgradeHistoryItem : upgradeHistoryItems) {
            int buildNumber = parseBuildNumber(upgradeHistoryItem.getTargetbuild());
            if (buildNumber > targetBuildNumber) {
                if (requiresDowngradeTask(upgradeHistoryItem)) {
                    uniqueBuildNumbers.add(buildNumber);
                }
            } else {
                // Good - we expect at least one upgrade task that is older than the current build number.
                oldUpgradeTaskFound = true;
            }
        }
        if (!oldUpgradeTaskFound) {
            // We should never actually get into this state because even the bootstrap data for a fresh install includes
            // upgrade history ... however someone may get clever and think it is a good idea to clean this upgrade
            // history out of our initial data - this is a safety mechanism in case that happens.
            throw new DowngradeException("Unable to downgrade data to build number " + targetBuildNumber +
                    " because it does not have sufficient upgrade task history.");
        }

        // Now reverse sort the unique build numbers
        final List<Integer> downgradeTasks = new ArrayList<Integer>(uniqueBuildNumbers);
        Collections.sort(downgradeTasks);
        Collections.reverse(downgradeTasks);

        return downgradeTasks;
    }

    private static boolean requiresDowngradeTask(final UpgradeHistoryDTO upgradeHistoryItem) throws DowngradeException {
        // Upgrade tasks tha ran prior to JIRA 7.0 will have NULL for downgrade_task_required
        // Only treat an explicit "N" as a no-op downgrade
        if ("N".equals(upgradeHistoryItem.getDowngradetaskrequired())) {
            return false;
        }

        if (STATUS_COMPLETE.equals(upgradeHistoryItem.getStatus())) {
            // the upgrade task has actually run
            return true;
        } else if (STATUS_PENDING.equals(upgradeHistoryItem.getStatus())) {
            // we can ignore any "pending" background tasks - they have not actually run
            return false;
        } else {
            throw new DowngradeException("Unable to downgrade data because upgrade " + upgradeHistoryItem.getTargetbuild() +
                    " is in an unknown state '" + upgradeHistoryItem.getStatus() + "'.");
        }
    }

    /**
     * Reads a downgrade XML file to get a list of all the downgrades specified in this file.
     */
    public static Map<Integer, DowngradeTask> loadDowngradeTasks() throws DowngradeException {
        final InputStream is = ClassLoaderUtils.getResourceAsStream(DOWNGRADE_FILENAME, DowngradeUtil.class);
        try {
            final Collection<String> classNames = new DowngradeTaskFileParser().parse(is);
            final Map<Integer, DowngradeTask> downgradeTaskMap = new HashMap<Integer, DowngradeTask>(classNames.size());
            for (String className : classNames) {
                DowngradeTask downgradeTask = constructNewInstance(className);
                downgradeTaskMap.put(downgradeTask.getBuildNumber(), downgradeTask);
            }
            return downgradeTaskMap;
        } catch (Exception ex) {
            // Multiple things can go wrong parsing the XML including Runtime exceptions like NPE
            // But only if the downgrades.xml is invalid.
            log.error("Unable to parse the Downgrade config file " + DOWNGRADE_FILENAME + ".", ex);
            throw new DowngradeException("An unexpected error occurred while parsing the Downgrade config file.");
        }
    }

    private static DowngradeTask constructNewInstance(final String className)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class clazz = Class.forName(className);
        try {
            return (DowngradeTask) clazz.newInstance();
        } catch (InstantiationException ex) {
            log.error("Unable to construct class '" + className + "' - Downgrade Tasks must provide a no arg constructor.");
            throw ex;
        }
    }

    private static int parseBuildNumber(final String targetbuild) throws DowngradeException {
        // Really old upgrades did not fill in the target build. eg UpgradeTask_Build403
        if (targetbuild == null) {
            return -1;
        }

        try {
            return Integer.parseInt(targetbuild);
        } catch (NumberFormatException e) {
            throw new DowngradeException("Unable to parse build number '" + targetbuild + "' in upgrade history.");
        }
    }

    /**
     * Given a list of all downgrade tasks to run, and a Map of Tasks we have, verify that all required tasks are known.
     *
     * @param downgradeTaskNumbers the upgrade task build numbers that we need to revert with an explicit downgrade task
     * @param downgradeTaskMap     map of known Downgrade tasks by build number
     * @throws MissingDowngradeTaskException if there are upgrade tasks that we can't revert
     */
    public static void verifyTasksExist(final List<Integer> downgradeTaskNumbers, final Map<Integer, DowngradeTask> downgradeTaskMap)
            throws MissingDowngradeTaskException {
        for (Integer downgradeTaskNumber : downgradeTaskNumbers) {
            if (!downgradeTaskMap.containsKey(downgradeTaskNumber)) {
                throw new MissingDowngradeTaskException("Cannot downgrade data - missing downgrade task " + downgradeTaskNumber);
            }
        }
    }

    @Nonnull
    public static ReindexRequirement getReindexRequirement(final ApplicationProperties applicationProperties) {
        String strValue = applicationProperties.getString(AP_KEY_REINDEX_REQUIREMENT);
        return strValue == null ? ReindexRequirement.NONE : ReindexRequirement.valueOf(strValue);
    }

    public static void setReindexRequirement(final ApplicationProperties applicationProperties, final ReindexRequirement reindexRequirement) {
        applicationProperties.setString(AP_KEY_REINDEX_REQUIREMENT, reindexRequirement.name());
    }
}
