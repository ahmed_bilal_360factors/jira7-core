package com.atlassian.jira.util;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Pair;
import com.atlassian.jira.util.OrderByRequest.Order;
import com.google.common.base.Strings;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.fugue.Pair.pair;
import static com.google.common.base.Strings.nullToEmpty;

public class OrderByRequestParserImpl implements OrderByRequestParser {
    private static final Pattern PREFIX_FIELD_PATTERN = Pattern.compile("^([+\\-]?)\\s*(.*)$");

    private final I18nHelper i18n;

    public OrderByRequestParserImpl(final I18nHelper i18n) {
        this.i18n = i18n;
    }

    public <T extends Enum<T>> Either<ErrorCollection, OrderByRequest<T>> parse(String orderByValue, Class<T> fields) {
        Pair<Order, String> orderAndField = extractOrderAndField(orderByValue);
        Order order = orderAndField.left();
        String value = orderAndField.right();

        for (T field : fields.getEnumConstants()) {
            if (field.toString().equalsIgnoreCase(value)) {
                return Either.right(new OrderByRequest<T>(field, order));
            }
        }
        return Either.<ErrorCollection, OrderByRequest<T>>left(new SimpleErrorCollection(i18n.getText("util.errors.order.query.parse",
                value, Arrays.toString(fields.getEnumConstants())), ErrorCollection.Reason.VALIDATION_FAILED));
    }

    private Pair<Order, String> extractOrderAndField(String orderQuery) {
        Matcher matcher = PREFIX_FIELD_PATTERN.matcher(nullToEmpty(orderQuery).trim());
        matcher.find();
        String prefix = matcher.group(1);
        Order order = Strings.isNullOrEmpty(prefix) ? Order.ASC : (prefix.equals("+") ? Order.ASC : Order.DESC);

        return pair(order, matcher.group(2));
    }
}
