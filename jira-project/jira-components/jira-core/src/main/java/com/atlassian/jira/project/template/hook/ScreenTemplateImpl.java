package com.atlassian.jira.project.template.hook;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.nullToEmpty;
import static java.util.Collections.unmodifiableList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ScreenTemplateImpl implements ScreenTemplate {
    private final String key;
    private final String name;
    private final String description;
    private final List<? extends ScreenTabTemplate> tabs;

    public ScreenTemplateImpl(
            @JsonProperty("key") String key,
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("tabs") List<ScreenTabTemplateImpl> tabs) {
        this.key = checkNotNull(key).toUpperCase();
        this.name = checkNotNull(name);
        this.description = nullToEmpty(description);
        this.tabs = checkNotNull(tabs);
    }

    @Override
    public String key() {
        return key;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String description() {
        return description;
    }

    @Override
    public List<ScreenTabTemplate> tabs() {
        return unmodifiableList(tabs);
    }
}
