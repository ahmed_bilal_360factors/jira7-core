package com.atlassian.jira.issue.export.customfield;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.export.ExportableSystemField;
import com.atlassian.jira.issue.export.FieldExportPart;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.export.FieldExportPartsBuilder;
import com.atlassian.jira.issue.export.customfield.layout.IssueSearchCsvExportLayout;
import com.atlassian.jira.issue.export.customfield.layout.IssueSearchCsvExportLayoutBuilder;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.statistics.util.DocumentHitCollector;
import com.atlassian.jira.issue.views.util.IssueWriterHitCollector;
import com.atlassian.jira.issue.views.util.SearchRequestViewBodyWriterUtil;
import com.atlassian.jira.plugin.searchrequestview.SearchRequestParams;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.component.TableLayoutFactory;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.IndexSearcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Stream.concat;
import static java.util.stream.Stream.empty;
import static java.util.stream.Stream.generate;

/**
 * @since 7.2.0
 */
public class DefaultCsvIssueExporter implements CsvIssueExporter {
    private static final Logger log = LoggerFactory.getLogger(DefaultCsvIssueExporter.class);
    protected final JiraAuthenticationContext authenticationContext;
    protected final TableLayoutFactory tableLayoutFactory;
    private final SearchProviderFactory searchProviderFactory;
    private final IssueFactory issueFactory;
    private final SearchRequestViewBodyWriterUtil searchRequestViewBodyWriterUtil;

    public DefaultCsvIssueExporter(JiraAuthenticationContext authenticationContext, TableLayoutFactory tableLayoutFactory,
                                   SearchProviderFactory searchProviderFactory, IssueFactory issueFactory,
                                   SearchRequestViewBodyWriterUtil searchRequestViewBodyWriterUtil) {
        this.authenticationContext = authenticationContext;
        this.tableLayoutFactory = tableLayoutFactory;
        this.searchProviderFactory = searchProviderFactory;
        this.issueFactory = issueFactory;
        this.searchRequestViewBodyWriterUtil = searchRequestViewBodyWriterUtil;
    }

    @Override
    public void export(Writer writer, SearchRequest searchRequest, SearchRequestParams searchRequestParams, List<Field> fields) throws IOException, SearchException {
        IssueSearchCsvExportLayout layout = getLayout(searchRequest, searchRequestParams, fields);

        writeHeader(writer, layout);

        writeBody(writer, layout, searchRequest, searchRequestParams);
    }

    /**
     * Get the {@link IssueSearchCsvExportLayout} for this export. This layout will determine how the fields should be
     * displayed and what order.
     *
     * @param searchRequest to find issues for
     * @param searchRequestParams context for the searcher
     * @param fields to get values for
     * @return Layout to export the Csv as.
     * @throws SearchException this is thrown when the searching of issues was not able to be completed.
     */
    private IssueSearchCsvExportLayout getLayout(SearchRequest searchRequest, SearchRequestParams searchRequestParams, List<Field> fields) throws SearchException {
        final IssueSearchCsvExportLayoutBuilder builder = new IssueSearchCsvExportLayoutBuilder();

        final IndexSearcher searcher = searchProviderFactory.getSearcher(SearchProviderFactory.ISSUE_INDEX);

        final Collector collector = new DocumentHitCollector(searcher) {
            @Override
            public void collect(Document d) {
                Issue issue = issueFactory.getIssue(d);
                for (Field field : fields) {

                    FieldExportParts fieldRepresentation = getFieldExportRepresentation(issue, field);
                    if (fieldRepresentation != null) {
                        builder.registerFieldRepresentation(field, fieldRepresentation);
                    }
                }
            }
        };

        searchRequestViewBodyWriterUtil.searchAndSort(searchRequest, collector, searchRequestParams.getPagerFilter());

        return builder.build();
    }

    /**
     * Given a field it gets the export representation for this issue. This deals with determine if it is a system
     * field or a custom field and converting it to the correct format.
     *
     * @param issue to get field value from
     * @param field to get value for
     * @return FieldExportRepresentation of the field.
     */
    @Nullable
    private FieldExportParts getFieldExportRepresentation(Issue issue, Field field) {
        if (field instanceof ExportableSystemField) {
            return ((ExportableSystemField)field).getRepresentationFromIssue(issue);
        } else if (field instanceof CustomField) {
            return getFieldExportRepresentationForCustomField(issue, (CustomField) field);
        } else {
            log.warn("Trying to export a field which is not exportable; ignoring", field);
            return null;
        }
    }

    /**
     * Convert the custom field to a FieldExportRepresentation for use by the exporter. If a custom field does not
     * implement the field it outputs a default implementation.
     * @param issue to get field value from
     * @param customField to get value for
     * @return FieldExportRepresentation of the field.
     */
    private FieldExportParts getFieldExportRepresentationForCustomField(Issue issue, CustomField customField) {
        final CustomFieldType customFieldType = customField.getCustomFieldType();

        final CustomFieldExportContext context = new DefaultCustomFieldExportContext(customField, authenticationContext.getI18nHelper(), "admin.issue.export.field.custom.field");
        FieldExportParts fieldRepresentation;

        if (customFieldType instanceof ExportableCustomFieldType) {
            fieldRepresentation = ((ExportableCustomFieldType) customFieldType).getRepresentationFromIssue(issue, context);
        } else {
            //Default representation for the field the field type hasn't implemented the CsvAwareCustomFieldType
            final Stream<String> values;
            Object value = customFieldType.getValueFromIssue(customField, issue);
            if (value == null) {
                values = Stream.of("");
            } else if (value instanceof Collection) {
                values = ((Collection<?>) value).stream().map(String::valueOf);
            } else {
                values = Stream.of(value.toString());
            }

            fieldRepresentation = FieldExportPartsBuilder.buildSinglePartRepresentation(customField.getId(), context.getDefaultColumnHeader(), values);
        }

        return fieldRepresentation;
    }

    /**
     * This deals with writing the first row, the header row, into the writer.
     * @param writer to output the header to
     * @param layout to display the fields in a certain order
     */
    private void writeHeader(Writer writer, IssueSearchCsvExportLayout layout) throws IOException {
        final CsvWriter csv = new CsvWriter(writer);

        for (IssueSearchCsvExportLayout.FieldLayout field : layout.getFieldLayouts()) {
            for (IssueSearchCsvExportLayout.SubFieldLayout subField : field.getSubFields()) {
                final String columnHeader = subField.getColumnHeader();
                for (Integer i = 0; i < subField.getColumnCount(); ++i) {
                    csv.writeColumn(columnHeader);
                }
            }
        }
        csv.writeRow();
    }

    /**
     * This deals with writing the the data for the rest of the CSV, each row would be an issue.
     * @param writer to output the header to
     * @param layout to display the fields in a certain order
     * @param searchRequest to find issues for
     * @param searchRequestParams context for the searcher
     */
    private void writeBody(Writer writer, IssueSearchCsvExportLayout layout, SearchRequest searchRequest, SearchRequestParams searchRequestParams) throws SearchException {
        final IndexSearcher searcher = searchProviderFactory.getSearcher(SearchProviderFactory.ISSUE_INDEX);

        final CsvWriter csv = new CsvWriter(writer);

        /**
         * This takes each field column for the layout and writes a comma separated string for each.
         *
         * For example, an example of a field with multiple columns is comments.
         */
        final Collector collector = new IssueWriterHitCollector(searcher, writer, issueFactory) {
            @Override
            protected void writeIssue(Issue issue, Writer writer) throws IOException {

                for (IssueSearchCsvExportLayout.FieldLayout fieldLayout : layout.getFieldLayouts()) {
                    final Field field = fieldLayout.getField();
                    FieldExportParts fieldRepresentation = getFieldExportRepresentation(issue, field);
                    if (fieldRepresentation != null) {
                        try (Stream<String> fieldColumns = getFieldColumns(fieldRepresentation, fieldLayout)) {
                            for (String content : (Iterable<String>) fieldColumns::iterator) {
                                csv.writeColumn(content);
                            }
                        }
                    }
                }

                csv.writeRow();
            }
        };

        searchRequestViewBodyWriterUtil.searchAndSort(searchRequest, collector, searchRequestParams.getPagerFilter());
    }

    /**
     * This deals with getting the column values for a field given the layout. For example, the field could have a sub
     * item with 7 values but 9 available columns so would need to output two extra empty strings. If it has 9 values
     * but only 7 available columns the last two values would be dropped. An example of this occurring is if between the
     * time that the layout was being calculated and outputting the result, more data was added.
     * @param fieldExportParts for a certain issue for a given field
     * @param fieldLayout representing the layout for this field. This stores the number of columns, etc.
     * @return stream of strings each being a value in the column.
     */
    private Stream<String> getFieldColumns(final FieldExportParts fieldExportParts, final IssueSearchCsvExportLayout.FieldLayout fieldLayout) {
        Stream<String> ret = empty();

        for (IssueSearchCsvExportLayout.SubFieldLayout subFieldLayout : fieldLayout.getSubFields()) {
            Integer columnsToPrint = subFieldLayout.getColumnCount();
            Optional<FieldExportPart> possiblePart = fieldExportParts.getPartWithId(subFieldLayout.getFieldItemId());

            Stream<String> partValues = possiblePart.map(FieldExportPart::getValues).orElse(empty());
            Stream<String> filledPartValues = concat(partValues, generate(() -> "")).limit(columnsToPrint);
            ret = concat(ret, filledPartValues);
        }

        return ret;
    }

    private static final class CsvWriter {
        private final Writer writer;

        private boolean newLine = true;

        CsvWriter(final Writer writer) {
            this.writer = writer;
        }

        void writeRow() throws IOException {
            writer.write("\n");
            newLine = true;
        }

        void writeColumn(final String content) throws IOException {
            if (newLine) {
                newLine = false;
            } else {
                writer.write(",");
            }
            StringEscapeUtils.ESCAPE_CSV.translate(content, writer);
        }
    }
}
