package com.atlassian.jira.issue.subscription;

import javax.annotation.Nullable;
import java.util.Date;

/**
 * A Filter Subscription bean
 *
 * @since v6.2
 */
public class DefaultFilterSubscription implements FilterSubscription {
    private boolean emailOnEmpty;
    private Long id;
    private Long filterId;
    private String userKey;
    private String groupName;
    private Date lastRunTime;

    public DefaultFilterSubscription(final Long id, final Long filterId, final String userKey, final String groupName, final Date lastRunTime, final boolean emailOnEmpty) {
        this.id = id;
        this.filterId = filterId;
        this.userKey = userKey;
        this.groupName = groupName;
        this.lastRunTime = lastRunTime;
        this.emailOnEmpty = emailOnEmpty;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Long getFilterId() {
        return filterId;
    }

    @Override
    public String getUserKey() {
        return userKey;
    }

    @Nullable
    @Override
    public String getGroupName() {
        return groupName;
    }

    @Nullable
    @Override
    public Date getLastRunTime() {
        return lastRunTime;
    }

    @Override
    public boolean isEmailOnEmpty() {
        return emailOnEmpty;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DefaultFilterSubscription)) {
            return false;
        }

        final DefaultFilterSubscription that = (DefaultFilterSubscription) o;

        if (emailOnEmpty != that.emailOnEmpty) {
            return false;
        }
        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (filterId != null ? !filterId.equals(that.filterId) : that.filterId != null) {
            return false;
        }
        if (userKey != null ? !userKey.equals(that.userKey) : that.userKey != null) {
            return false;
        }
        if (groupName != null ? !groupName.equals(that.groupName) : that.groupName != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = (emailOnEmpty ? 1 : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (filterId != null ? filterId.hashCode() : 0);
        result = 31 * result + (userKey != null ? userKey.hashCode() : 0);
        result = 31 * result + (groupName != null ? groupName.hashCode() : 0);
        return result;
    }
}
