package com.atlassian.jira.issue.export;

import com.atlassian.jira.issue.Issue;

/**
 * Represents a field that is able to be exported, e.g. to a CSV output, etc.
 *
 * @since 7.2.0
 */
public interface ExportableSystemField {
    /**
     * For the given issue get an exportable representation for the field
     * @param issue to get representation for the field
     * @return the fields exportable representation
     */
    FieldExportParts getRepresentationFromIssue(Issue issue);
}
