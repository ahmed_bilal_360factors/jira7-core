package com.atlassian.jira.bc.user.search;

import com.atlassian.collectors.CollectorsUtil;
import org.apache.lucene.analysis.CharTokenizer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.ReusableAnalyzerBase;
import org.apache.lucene.util.Version;

import java.io.Reader;
import java.util.Set;

public class UserNameAnalyzer extends ReusableAnalyzerBase {

    private static final Set<Integer> SEPARATOR_CODE_POINTS = UserSearchUtilities.SEPARATORS
            .stream()
            .map(separator -> Character.codePointAt(separator, 0))
            .collect(CollectorsUtil.toImmutableSet());

    @Override
    protected TokenStreamComponents createComponents(String fieldName, Reader aReader) {
        CharTokenizer source = new CharTokenizer(Version.LUCENE_33, aReader) {

            @Override
            protected boolean isTokenChar(int c) {
                return !SEPARATOR_CODE_POINTS.contains(c);
            }
        };
        return new TokenStreamComponents(source, new LowerCaseFilter(Version.LUCENE_33, source));
    }
}
