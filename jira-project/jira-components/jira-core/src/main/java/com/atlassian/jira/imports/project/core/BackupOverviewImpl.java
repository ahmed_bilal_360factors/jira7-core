package com.atlassian.jira.imports.project.core;

import com.atlassian.jira.util.dbc.Null;
import org.apache.commons.collections.map.ListOrderedMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @since v3.13
 */
public class BackupOverviewImpl implements BackupOverview {
    private static final long serialVersionUID = -8149242078577537996L;

    private final transient Map<String, BackupProject> fullProjectsByKey;
    private final BackupSystemInformation backupSystemInformation;

    public BackupOverviewImpl(final BackupSystemInformation backupSystemInformation, final List<? extends BackupProject> backupProjects) {
        Null.not("backupSystemInformation", backupSystemInformation);
        Null.not("backupProjects", backupProjects);

        this.backupSystemInformation = backupSystemInformation;

        fullProjectsByKey = new ListOrderedMap();

        // Sort the projects by name so they are ordered
        Collections.sort(backupProjects, new BackupProjectNameComparator());
        for (final BackupProject backupProject : backupProjects) {
            fullProjectsByKey.put(backupProject.getProject().getKey(), backupProject);
        }
    }

    public BackupProject getProject(final String projectKey) {
        return fullProjectsByKey.get(projectKey);
    }

    public List /*<BackupProject>*/getProjects() {
        return new ArrayList<BackupProject>(fullProjectsByKey.values());
    }

    public BackupSystemInformation getBackupSystemInformation() {
        return backupSystemInformation;
    }
}
