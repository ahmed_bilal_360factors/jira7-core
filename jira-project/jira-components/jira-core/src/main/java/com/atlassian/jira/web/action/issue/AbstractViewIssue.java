package com.atlassian.jira.web.action.issue;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.AttachmentFileNameCreationDateComparator;
import com.atlassian.jira.issue.link.LinkCollection;
import com.atlassian.jira.project.VersionProxy;

import java.util.Collection;
import java.util.List;

public class AbstractViewIssue extends AbstractIssueSelectAction {
    private LinkCollection linkCollection;
    private List<Attachment> attachments;

    public AbstractViewIssue(final SubTaskManager subTaskManager) {
        super(subTaskManager);
    }

    public Collection<ProjectComponent> getPossibleComponents() throws Exception {
        return ComponentAccessor.getProjectComponentManager().findAllForProject(getProjectObject() != null ? getProjectObject().getId() : null);
    }

    public Collection<VersionProxy> getPossibleVersions() throws Exception {
        return getPossibleVersions(getProjectObject());
    }

    public Collection<VersionProxy> getPossibleVersionsReleasedFirst() throws Exception {
        return getPossibleVersionsReleasedFirst(getProjectObject());
    }

    public Collection<Attachment> getAttachments() throws Exception {
        if (attachments == null) {
            attachments = attachmentManager.getAttachments(getIssueObject(),
                    new AttachmentFileNameCreationDateComparator(getLocale()));
        }
        return attachments;
    }
}
