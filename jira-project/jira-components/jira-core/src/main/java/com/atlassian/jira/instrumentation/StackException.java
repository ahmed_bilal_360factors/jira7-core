package com.atlassian.jira.instrumentation;

/**
 * An exception to use when all you really want to do is create a stack trece.
 *
 * @since v7.1
 */
public class StackException extends Exception {
    public StackException(String message) {
        super(message);
    }

    public String toString() {
        String s = "Stack Trace Generated ";
        String message = getLocalizedMessage();
        return (message != null) ? (s + ": " + message) : s;
    }
}
