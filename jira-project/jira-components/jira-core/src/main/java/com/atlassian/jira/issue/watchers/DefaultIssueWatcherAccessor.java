package com.atlassian.jira.issue.watchers;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Locale;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

public class DefaultIssueWatcherAccessor implements IssueWatcherAccessor {
    private final WatcherManager watcherManager;

    public DefaultIssueWatcherAccessor(final WatcherManager watcherManager) {
        this.watcherManager = notNull("voteManager", watcherManager);
    }

    @Override
    public Iterable<ApplicationUser> getWatchers(@Nonnull Issue issue, @Nonnull Locale displayLocale) {
        return watcherManager.getWatchers(issue, displayLocale);
    }

    @Override
    public boolean isWatchingEnabled() {
        return watcherManager.isWatchingEnabled();
    }

    @Override
    public Iterable<String> getWatcherNames(final @Nonnull Issue issue) {
        return watcherManager.getCurrentWatcherUsernames(issue);
    }

    @Override
    public Collection<String> getWatcherKeys(Issue issue) {
        return watcherManager.getWatcherUserKeys(issue);
    }
}
