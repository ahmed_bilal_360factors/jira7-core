package com.atlassian.jira.web.filters;

import com.atlassian.core.filters.AbstractHttpFilter;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.component.ComponentReference;
import com.atlassian.jira.config.InstanceFeatureManager;
import com.atlassian.jira.event.mau.LastSentKey;
import com.atlassian.jira.event.mau.MauApplicationKey;
import com.atlassian.jira.event.mau.MauEventService;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;


/**
 * Servlet filter to trigger eyeball events to track monthly active users.  This filter will call {@link
 * MauEventService#triggerEvent()} at the end of the filterchain assuming that some code in the current request
 * has set an application correctly using {@link MauEventService#setApplicationForThread(MauApplicationKey)}.
 *
 * @since v7.0.1
 */
public class MauEventFilter extends AbstractHttpFilter {

    public static final String MAU_FEATURE_LOGGING = "jira.event.mau.logging";

    private static final String MAU_IGNORE_HEADER = "x-atlassian-mau-ignore";

    private static final Logger LOGGER = LoggerFactory.getLogger(MauEventFilter.class);

    /**
     * This filter logs MAU events, except where the header is set to ignore.
     * The services used may not be available during bootstrapping which is why they
     * are wrapped to ensure we fail quietly.
     *
     * The dark feature flag used here will disable logging of the MAU information,
     * which is primarily for JIRA development.
     */
    @Override
    protected void doFilter(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException {
        try {
            filterChain.doFilter(request, response);
        } finally {
            final boolean ignoreMauEvent = request.getHeader(MAU_IGNORE_HEADER) != null;
            if (!ignoreMauEvent) {
                getMauEventService().ifPresent(service -> {
                    service.triggerEvent();

                    final boolean shouldLogMau = getFeatureManager()
                            .map(featureManager -> featureManager.isInstanceFeatureEnabled(MAU_FEATURE_LOGGING))
                            .orElse(false);

                    final LastSentKey key = service.getKeyWithCurrentApplication();
                    if (shouldLogMau && key != null) {
                        if (LOGGER.isInfoEnabled()) {
                            LOGGER.info("MAU registered to '{}' by '{}'", key.getApplicationKey().getKey(), key.getUserId());
                        }
                    }
                });
            }
        }
    }

    @VisibleForTesting
    Optional<MauEventService> getMauEventService() {
        return ComponentAccessor.getComponentSafely(MauEventService.class);
    }

    @VisibleForTesting
    Optional<InstanceFeatureManager> getFeatureManager() {
        return ComponentAccessor.getComponentSafely(InstanceFeatureManager.class);
    }
}
