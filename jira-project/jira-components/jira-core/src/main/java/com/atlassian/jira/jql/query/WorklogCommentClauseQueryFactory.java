package com.atlassian.jira.jql.query;

import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.search.util.LuceneQueryModifier;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.query.clause.TerminalClause;
import com.google.common.collect.ImmutableList;
import org.apache.lucene.search.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class WorklogCommentClauseQueryFactory extends WorklogClauseQueryFactory {
    private static final Logger log = LoggerFactory.getLogger(WorklogCommentClauseQueryFactory.class);

    private final LuceneQueryModifier luceneQueryModifier;
    private final ClauseQueryFactory delegateClauseQueryFactory;

    public WorklogCommentClauseQueryFactory(final IssueIdFilterQueryFactory issueIdFilterQueryFactory,
                                            final QueryProjectRoleAndGroupPermissionsDecorator queryPermissionsDecorator,
                                            final LuceneQueryModifier luceneQueryModifier,
                                            final JqlOperandResolver operandResolver) {
        super(issueIdFilterQueryFactory, queryPermissionsDecorator);
        this.luceneQueryModifier = luceneQueryModifier;
        this.delegateClauseQueryFactory = new GenericClauseQueryFactory(DocumentConstants.WORKLOG_COMMENT, ImmutableList.of(new LikeQueryFactory(false)), operandResolver);
    }

    @Override
    public Query getWorklogQuery(final QueryCreationContext queryCreationContext, final TerminalClause terminalClause) {
        return luceneQueryModifier.getModifiedQuery(delegateClauseQueryFactory.getQuery(queryCreationContext, terminalClause).getLuceneQuery());
    }
}
