package com.atlassian.velocity;

import com.atlassian.jira.cluster.ClusterSafe;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.tenancy.TenantAware;
import com.atlassian.jira.tenancy.TenantInfo;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.velocity.runtime.log.Log;
import org.apache.velocity.util.introspection.ClassMap;
import org.apache.velocity.util.introspection.IntrospectorCache;

import javax.annotation.Nonnull;
import java.util.concurrent.TimeUnit;

/**
 * JIRA velocity introspector cache that uses Guava to store loaded classes.
 *
 * @since v6.4
 */
@ClusterSafe
@TenantInfo(value = TenantAware.TENANTLESS, comment = "Purely a local class/method resolution concern")
public class JiraIntrospectorCache implements IntrospectorCache {
    private static final String NO_EXPIRY_PROPERTY = "atlassian.cache.velocity.noexpiry";

    // Note: This class should *not* use atlassian-cache or VCache because it gets used by the
    // emergency velocity engine used for catastrophic errors, such as JiraLockedError, and the
    // CacheManager/VCacheFactory may not even exist.
    private final LoadingCache<Class<?>, ClassMap> classMapCache;

    @SuppressWarnings("UnusedParameters")
    public JiraIntrospectorCache(final Log log) {
        final CacheBuilder<Object, Object> builder = CacheBuilder.newBuilder();
        if (!JiraSystemProperties.getInstance().getBoolean(NO_EXPIRY_PROPERTY)) {
            builder.expireAfterAccess(30, TimeUnit.MINUTES);
        }
        this.classMapCache = builder.build(new CacheLoader<Class<?>, ClassMap>() {
            @Override
            public ClassMap load(@Nonnull Class<?> key) throws Exception {
                return new CachingJiraClassMap(new JiraClassMap(key));
            }
        });
    }

    @Override
    public void clear() {
        classMapCache.invalidateAll();
    }

    @Override
    @SuppressWarnings("rawtypes")  // Forced by IntrospectorCache interface
    public ClassMap get(final Class clazz) {
        return classMapCache.getUnchecked(clazz);
    }

    @Override
    @SuppressWarnings("rawtypes")  // Forced by IntrospectorCache interface
    public ClassMap put(final Class clazz) {
        return classMapCache.getUnchecked(clazz);
    }
}
