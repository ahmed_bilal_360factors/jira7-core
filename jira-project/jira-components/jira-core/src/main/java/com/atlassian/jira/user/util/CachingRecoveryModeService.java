package com.atlassian.jira.user.util;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.recovery.RecoveryModeService;
import com.atlassian.util.concurrent.LazyReference;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * JIRA does not want to be able to enable 'recovery mode' dynamically so for efficiency we cache the load time
 * results.
 * <p>
 * {@link com.atlassian.jira.user.util.RecoveryMode}
 *
 * @since 7.0
 */
public final class CachingRecoveryModeService implements RecoveryModeService {
    private final RecoveryModeService delegate;
    private final LazyReference<State> state = new LazyReference<State>() {
        @Override
        protected State create() throws Exception {
            return new State(delegate);
        }
    };

    public CachingRecoveryModeService(RecoveryModeService delegate) {
        this.delegate = notNull("delegate", delegate);
    }

    @Override
    public boolean isRecoveryModeOn() {
        //noinspection ConstantConditions
        return state.get().isEnabled();
    }

    @Override
    public Directory getRecoveryDirectory() {
        //noinspection ConstantConditions
        return state.get().getDirectory();
    }

    @Override
    public String getRecoveryUsername() {
        //noinspection ConstantConditions
        return state.get().getUserName();
    }

    private static class State {
        private final boolean enabled;
        private final Directory directory;
        private final String userName;

        private State(RecoveryModeService service) {
            this.enabled = service.isRecoveryModeOn();
            if (this.enabled) {
                this.directory = service.getRecoveryDirectory();
                this.userName = service.getRecoveryUsername();
            } else {
                this.directory = null;
                this.userName = null;
            }
        }

        private boolean isEnabled() {
            return enabled;
        }

        private Directory getDirectory() {
            assertEnabled();
            return directory;
        }

        private String getUserName() {
            assertEnabled();
            return userName;
        }

        private void assertEnabled() {
            if (!isEnabled()) {
                throw new IllegalStateException("Recovery Mode is not enabled.");
            }
        }
    }
}
