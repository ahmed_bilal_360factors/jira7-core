package com.atlassian.jira.cluster.distribution;

import java.util.function.Supplier;

class ClassLoaderSwitchingSupplier<T> implements Supplier<T> {
    private final Supplier<T> supplier;
    private final ClassLoader taskClassLoader;

    ClassLoaderSwitchingSupplier(final Supplier<T> supplier) {
        this.supplier = supplier;

        taskClassLoader = Thread.currentThread().getContextClassLoader();
    }

    @Override
    public T get() {
        final ClassLoader threadClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(taskClassLoader);
            return supplier.get();
        } finally {
            Thread.currentThread().setContextClassLoader(threadClassLoader);
        }
    }
}
