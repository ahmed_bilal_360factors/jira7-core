package com.atlassian.jira.auditing.handlers;

import com.atlassian.jira.auditing.AuditingCategory;
import com.atlassian.jira.auditing.ChangedValue;
import com.atlassian.jira.auditing.RecordRequest;
import com.atlassian.jira.cluster.zdu.JiraUpgradeApprovedEvent;
import com.atlassian.jira.cluster.zdu.JiraUpgradeCancelledEvent;
import com.atlassian.jira.cluster.zdu.JiraUpgradeFinishedEvent;
import com.atlassian.jira.cluster.zdu.JiraUpgradeStartedEvent;
import com.atlassian.jira.cluster.zdu.NodeBuildInfo;
import com.atlassian.jira.util.I18nHelper;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Locale;

/**
 * @since v7.3
 */
public class JiraUpgradeEventHandlerImpl implements JiraUpgradeEventHandler {

    private final I18nHelper.BeanFactory i18nHelper;

    public JiraUpgradeEventHandlerImpl(I18nHelper.BeanFactory i18nHelper) {
        this.i18nHelper = i18nHelper;
    }

    @Override
    public RecordRequest onStartedEvent(final JiraUpgradeStartedEvent event) {
        List<ChangedValue> changedValues = getVersionInfoValues(null, event.getNodeBuildInfo());
        return new RecordRequest(AuditingCategory.SYSTEM, "jira.auditing.upgrade.started")
                .withDescription(getI18n().getText("jira.auditing.upgrade.started.description"))
                .withChangedValues(changedValues);
    }

    @Override
    public RecordRequest onCancelledEvent(final JiraUpgradeCancelledEvent event) {
        List<ChangedValue> changedValues = getVersionInfoValues(null, event.getNodeBuildInfo());
        return new RecordRequest(AuditingCategory.SYSTEM, "jira.auditing.upgrade.cancelled")
                .withDescription(getI18n().getText("jira.auditing.upgrade.cancelled.description"))
                .withChangedValues(changedValues);
    }

    @Override
    public RecordRequest onApprovedEvent(final JiraUpgradeApprovedEvent event) {
        List<ChangedValue> changedValues = getVersionInfoValues(event.getFromVersion(), event.getToVersion());
        return new RecordRequest(AuditingCategory.SYSTEM, "jira.auditing.upgrade.approved")
                .withDescription(getI18n().getText("jira.auditing.upgrade.approved.description"))
                .withChangedValues(changedValues);
    }

    @Override
    public RecordRequest onFinishedEvent(final JiraUpgradeFinishedEvent event) {
        List<ChangedValue> changedValues = getVersionInfoValues(event.getFromVersion(), event.getToVersion());
        return new RecordRequest(AuditingCategory.SYSTEM, "jira.auditing.upgrade.finished")
                .withDescription(getI18n().getText("jira.auditing.upgrade.finished.description"))
                .withChangedValues(changedValues);
    }

    private List<ChangedValue> getVersionInfoValues(@Nullable final NodeBuildInfo previous,
                                                    final NodeBuildInfo current) {
        return new ChangedValuesBuilder()
                .addIfDifferent("admin.systeminfo.version", previous != null ? previous.getVersion() : null, current.getVersion())
                .addIfDifferent("admin.systeminfo.build.number", previous != null ? String.valueOf(previous.getBuildNumber()) : null, String.valueOf(current.getBuildNumber()))
                .build();
    }

    private I18nHelper getI18n() {
        return i18nHelper.getInstance(Locale.ENGLISH);
    }
}
