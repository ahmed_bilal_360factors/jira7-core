package com.atlassian.jira.web.action.admin;

import com.atlassian.fugue.Option;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

/**
 * Gets the admin permissions of the logged in user.
 *
 * @since 7.2
 */
public class AdminUserDataProvider implements WebResourceDataProvider {
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final GlobalPermissionManager globalPermissionManager;

    private static final String IS_ADMIN = "isAdmin";
    private static final String IS_SYS_ADMIN = "isSysAdmin";

    /**
     * NOTE: These objects are not dependency injected because this class is sometimes loaded (instance setup) when the
     * objects are not available causing a stack trace. Safely trying to access these objects stops these failures.
     */
    public AdminUserDataProvider() {
        this.jiraAuthenticationContext = getJiraAuthenticationContext();
        this.globalPermissionManager = getGlobalPermissionManager();
    }

    @Override
    public Jsonable get() {
        return writer -> {
            try {
                getJsonData().write(writer);
            } catch (JSONException e) {
                throw new Jsonable.JsonMappingException(e);
            }
        };
    }

    private JSONObject getJsonData() throws JSONException {
        final JSONObject values = new JSONObject();

        if (canAccessComponents()) {
            final ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();

            values.put(IS_SYS_ADMIN, globalPermissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, user));
            values.put(IS_ADMIN, globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user));
        }

        return values;
    }

    private boolean canAccessComponents() {
        return (jiraAuthenticationContext != null && globalPermissionManager != null);
    }

    private GlobalPermissionManager getGlobalPermissionManager() {
        return getComponentOrNull(GlobalPermissionManager.class);
    }

    private JiraAuthenticationContext getJiraAuthenticationContext() {
        return getComponentOrNull(JiraAuthenticationContext.class);
    }

    private static <T> T getComponentOrNull(Class<T> clazz) {
        Option<T> componentOption = Option.option(ComponentAccessor.getComponentSafely(clazz).orElse(null));

        return componentOption.getOrNull();
    }
}
