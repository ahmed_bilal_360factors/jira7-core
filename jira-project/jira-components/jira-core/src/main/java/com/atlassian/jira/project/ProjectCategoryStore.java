package com.atlassian.jira.project;

import java.util.List;

/**
 * Does DB operations for ProjectCategory.
 *
 * @since v4.4
 */
public interface ProjectCategoryStore {
    ProjectCategory createProjectCategory(String name, String description);

    void removeProjectCategory(Long id);

    /**
     * Returns {@link ProjectCategory} for the given id.
     * @param id project category id
     * @return the {@link ProjectCategory} for the provided id if it was found or else null
     */
    ProjectCategory getProjectCategory(Long id);

    /**
     * Returns all ProjectCategories, ordered by name.
     *
     * @return all ProjectCategories, ordered by name.
     */
    List<ProjectCategory> getAllProjectCategories();

    void updateProjectCategory(ProjectCategory projectCategory);
}
