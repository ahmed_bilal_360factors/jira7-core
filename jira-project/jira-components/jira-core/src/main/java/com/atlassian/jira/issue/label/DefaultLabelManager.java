package com.atlassian.jira.issue.label;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.label.suggestions.LabelSuggester;
import com.atlassian.jira.issue.util.IssueUpdateBean;
import com.atlassian.jira.issue.util.IssueUpdater;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Manager responsible for label operations.
 *
 * @since v4.2
 */
public class DefaultLabelManager implements LabelManager {
    private final LabelStore labelStore;
    private final IssueManager issueManager;
    private final IssueUpdater issueUpdater;
    private final LabelSuggester labelSuggester;

    public DefaultLabelManager(final LabelStore labelStore, final IssueManager issueManager,
                               final IssueUpdater issueUpdater, final LabelSuggester labelSuggester) {
        this.labelStore = labelStore;
        this.issueManager = issueManager;
        this.issueUpdater = issueUpdater;
        this.labelSuggester = labelSuggester;
    }

    protected CustomFieldManager getFieldManager() {
        return ComponentAccessor.getComponentOfType(CustomFieldManager.class);
    }

    @Override
    public Set<Label> getLabels(final Long issueId) {
        notNull("issueId", issueId);

        return labelStore.getLabels(issueId, null);
    }

    @Override
    public Set<Label> setLabels(final ApplicationUser remoteUser, final Long issueId, final Set<String> labels, final boolean sendNotification, final boolean causesChangeNotification) {
        notNull("issueId", issueId);
        notNull("labels", labels);

        validateLabels(labels);
        final Issue issue = findIssue(issueId);

        return setIfNotEqual(issue, labels, null, remoteUser, sendNotification, causesChangeNotification);
    }

    @Override
    public Set<Label> getLabels(final Long issueId, final Long customFieldId) {
        notNull("issueId", issueId);
        notNull("customFieldId", customFieldId);
        return labelStore.getLabels(issueId, customFieldId);
    }

    @Override
    public Set<Label> setLabels(final ApplicationUser remoteUser, final Long issueId, final Long customFieldId,
                                final Set<String> labels, final boolean sendNotification, final boolean causesChangeNotification) {
        notNull("issueId", issueId);
        notNull("customFieldId", customFieldId);
        notNull("labels", labels);

        validateLabels(labels);
        final Issue issue = findIssue(issueId);
        return setIfNotEqual(issue, labels, customFieldId, remoteUser, sendNotification, causesChangeNotification);
    }

    @Override
    public Label addLabel(final ApplicationUser remoteUser, final Long issueId, final String label, final boolean sendNotification) {
        notNull("issueId", issueId);
        notNull("label", label);

        validateSingleLabel(label);
        final Issue issue = findIssue(issueId);

        return addIfNotContains(issue, label, null, remoteUser, sendNotification);
    }

    @Override
    public Label addLabel(final ApplicationUser remoteUser, final Long issueId, final Long customFieldId, final String label,
                          final boolean sendNotification) {
        notNull("issueId", issueId);
        notNull("label", label);
        notNull("customFieldId", customFieldId);

        validateSingleLabel(label);
        return addIfNotContains(findIssue(issueId), label, customFieldId, remoteUser, sendNotification);
    }

    private Set<Label> setIfNotEqual(final Issue issue, final Set<String> labels, final Long customFieldId,
                                     final ApplicationUser remoteUser, final boolean sendNotification, final boolean causesChangeNotification) {
        Set<Label> oldLabels = labelStore.getLabels(issue.getId(), customFieldId);
        if (differentLabels(labels, oldLabels)) {
            final Set<Label> newLabels = labelStore.setLabels(issue.getId(), customFieldId, labels);
            // JRADEV-2126 Only create a History entry if you are updating labels after  creating the issue
            // note that if causesChangeNotification is false then the labels manager does not fire any
            // update events at all, and the index is not reindexed
            if (causesChangeNotification) {
                issueUpdated(newLabels, oldLabels, issue, customFieldId, remoteUser, sendNotification);
            }
            return newLabels;
        } else {
            return oldLabels;
        }
    }

    private Label addIfNotContains(final Issue issue, final String label, Long customFieldId, final ApplicationUser remoteUser,
                                   final boolean sendNotification) {
        Set<Label> oldLabels = labelStore.getLabels(issue.getId(), customFieldId);
        if (containsLabel(label, oldLabels)) {
            return getLabel(label, oldLabels);
        } else {
            final Label newLabel = labelStore.addLabel(issue.getId(), customFieldId, label);
            issueUpdated(newLabel, oldLabels, issue, customFieldId, remoteUser, sendNotification);
            return newLabel;
        }
    }

    @Override
    public Set<Long> removeLabelsForCustomField(final Long customFieldId) {
        notNull("customFieldId", customFieldId);

        return labelStore.removeLabelsForCustomField(customFieldId);
    }

    @Override
    public Set<String> getSuggestedLabels(final ApplicationUser user, final Long issueId, final String token) {
        final Set<Label> issueLabels = issueId != null ? getLabels(issueId) : Collections.emptySet();
        return labelSuggester.getSuggestedLabels(token, issueId, issueLabels, user);
    }

    @Override
    public Set<String> getSuggestedLabels(final ApplicationUser user, final Long issueId, final Long customFieldId, final String token) {
        notNull("customFieldId", customFieldId);
        final Set<Label> issueLabels = issueId != null ? getLabels(issueId) : Collections.emptySet();
        return labelSuggester.getSuggestedLabels(token, issueId, customFieldId, issueLabels, user);
    }

    private Issue findIssue(final Long issueId) {
        final Issue issue = issueManager.getIssueObject(issueId);
        if (issue == null) {
            throw new IllegalArgumentException("Issue with id '" + issueId + "' no longer exists!");
        }
        return issue;
    }

    private boolean differentLabels(final Set<String> labels, final Set<Label> existingLabels) {
        if (labels.size() != existingLabels.size()) {
            return true;
        }
        return !labels.containsAll(asSortedStringLabels(existingLabels));
    }

    private boolean containsLabel(final String label, final Set<Label> labels) {
        for (Label issueLabel : labels) {
            if (issueLabel.getLabel().equals(label)) {
                return true;
            }
        }
        return false;
    }

    private Label getLabel(final String label, final Set<Label> labels) {
        for (Label issueLabel : labels) {
            if (issueLabel.getLabel().equals(label)) {
                return issueLabel;
            }
        }
        throw new IllegalArgumentException("The label <" + label + "> not found in the issue");
    }

    private void issueUpdated(final Label label, final Set<Label> oldLabels, final Issue originalIssue,
                              final Long customFieldId, final ApplicationUser remoteUser, final boolean sendNotification) {
        issueUpdated(joinLabels(label, oldLabels), oldLabels, originalIssue, customFieldId, remoteUser, sendNotification);
    }

    private Set<Label> joinLabels(Label label, Set<Label> oldLabels) {
        Set<Label> answer = new HashSet<Label>(oldLabels);
        answer.add(label);
        return answer;
    }

    private void issueUpdated(Set<Label> newLabels, Set<Label> oldLabels, Issue originalIssue, final Long customFieldId,
                              ApplicationUser remoteUser, boolean sendNotification) {
        IssueUpdateBean updateBean = createIssueUpdateBean(remoteUser, originalIssue, sendNotification);
        updateBean.setChangeItems(Lists.newArrayList(createLabelsChangeItem(newLabels, oldLabels, customFieldId)));
        doUpdate(updateBean);
    }

    private IssueUpdateBean createIssueUpdateBean(final ApplicationUser remoteUser, final Issue originalIssue,
                                                  final boolean sendNotification) {
        Issue updated = issueManager.getIssueObject(originalIssue.getId());
        return new IssueUpdateBean(updated, originalIssue, EventType.ISSUE_UPDATED_ID, remoteUser,
                sendNotification, false);
    }

    private ChangeItemBean createLabelsChangeItem(Set<Label> newLabels, Set<Label> oldLabels, Long customFieldId) {
        return new ChangeItemBean(resolveFieldType(customFieldId), resolveFieldName(customFieldId),
                null, toString(oldLabels), null, toString(newLabels));
    }

    private String resolveFieldType(Long customFieldId) {
        if (customFieldId == null) {
            return ChangeItemBean.STATIC_FIELD;
        } else {
            return ChangeItemBean.CUSTOM_FIELD;
        }
    }

    private String resolveFieldName(Long customFieldId) {
        if (customFieldId == null) {
            return IssueFieldConstants.LABELS;
        } else {
            return getFromCustomField(customFieldId);
        }
    }

    private String getFromCustomField(final Long customFieldId) {
        CustomField field = getFieldManager().getCustomFieldObject(customFieldId);
        if (field == null) {
            throw new IllegalArgumentException("No custom field with ID [" + customFieldId + "] found");
        }
        return field.getName();
    }

    private String toString(final Set<Label> labels) {
        if (labels.isEmpty()) {
            return "";
        }
        StringBuilder accumulator = new StringBuilder(labels.size() * 8);
        for (String label : asSortedStringLabels(labels)) {
            accumulator.append(label).append(" ");
        }
        return accumulator.deleteCharAt(accumulator.length() - 1).toString();
    }

    private Set<String> asSortedStringLabels(Set<Label> labels) {
        Set<String> strings = new TreeSet<>();
        for (Label label : labels) {
            strings.add(label.getLabel());
        }
        return strings;
    }

    private void doUpdate(final IssueUpdateBean updateBean) {
        issueUpdater.doUpdate(updateBean, false);
    }

    private void validateLabels(final Set<String> labels) {
        for (String theLabel : labels) {
            validateSingleLabel(theLabel);
        }
    }

    private void validateSingleLabel(final String theLabel) {
        final String label = theLabel.trim();
        if (StringUtils.isBlank(label)) {
            throw new IllegalArgumentException("Labels cannot be blank!");
        }
        if (!LabelParser.isValidLabelName(label)) {
            throw new IllegalArgumentException("The label '" + label +
                    "' contained spaces which is invalid.");
        }
        if (label.length() > LabelParser.MAX_LABEL_LENGTH) {
            throw new IllegalArgumentException("The label '" + label +
                    "' exceeds the maximum length allowed for labels of " + LabelParser.MAX_LABEL_LENGTH + " characters.");
        }
    }
}
