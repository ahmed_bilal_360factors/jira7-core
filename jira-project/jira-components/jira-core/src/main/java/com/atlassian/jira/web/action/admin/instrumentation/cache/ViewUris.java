package com.atlassian.jira.web.action.admin.instrumentation.cache;

import com.atlassian.jira.instrumentation.InstrumentationLogger;
import com.atlassian.jira.instrumentation.LogEntry;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * Displays cache usage by URI.
 *
 * @since v7.1
 */
@WebSudoRequired
public class ViewUris extends JiraWebActionSupport {
    private final InstrumentationLogger instrumentationLogger;

    public ViewUris(InstrumentationLogger instrumentationLogger) {
        this.instrumentationLogger = instrumentationLogger;
    }

    public Collection<UriDisplayBean> getAccessedUris() {
        return instrumentationLogger.getLogEntriesFromBuffer().stream()
                // Count the number of times each URL appears in the list
                .collect(Collectors.groupingByConcurrent(LogEntry::getPath, Collectors.counting()))
                .entrySet().stream()
                        // Get rid of anything called other.
                .filter(e -> !e.getKey().equals("Other"))
                        // Convert the map or URL/Count into a list of UriDisplayBeans.
                .map(e -> new UriDisplayBean(e.getKey(), e.getValue().intValue()))
                .collect(Collectors.toList());
    }

    public class UriDisplayBean {
        private String uri;
        private int count;

        public UriDisplayBean(@Nonnull String uri, int count) {
            this.uri = requireNonNull(uri);
            this.count = count;
        }

        public String getUri() {
            return uri;
        }

        public void setUri(final String uri) {
            this.uri = uri;
        }

        public int getCount() {
            return count;
        }

        public void setCount(final int count) {
            this.count = count;
        }
    }
}
