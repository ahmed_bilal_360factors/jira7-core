package com.atlassian.jira.bc.filter;

import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Maps;

import java.util.HashMap;
import java.util.List;

import static com.google.common.collect.ImmutableMap.of;
import static java.util.stream.Collectors.toList;

public class FilterDeletionWarningViewProviderImpl implements FilterDeletionWarningViewProvider {
    public static final String CONTEXT_KEY_USER = "user";
    public static final String CONTEXT_KEY_SEARCH_REQUEST = "searchRequest";
    
    @VisibleForTesting
    static final String FILTER_DELETION_WARNING_PANEL_LOCATION = "atl.jira.filter.delete.warning";

    @VisibleForTesting
    static final String TEMPLATES_RESOURCE_KEY = "jira.filter.deletion.warning:soy-templates";
    
    @VisibleForTesting
    static final String TEMPLATE_NAME = "JIRA.Templates.Issues.Filters.Warnings.dialogBody";
    
    private static final String STYLES_WEB_RESOURCE_KEY = "jira.filter.deletion.warning:styles";

    private final WebInterfaceManager webInterfaceManager;
    private final SoyTemplateRendererProvider soyTemplateRendererProvider;
    private final JiraAuthenticationContext jiraAuthenticationContext;


    public FilterDeletionWarningViewProviderImpl(final WebInterfaceManager webInterfaceManager,
                                                 final SoyTemplateRendererProvider soyTemplateRendererProvider,
                                                 final JiraAuthenticationContext jiraAuthenticationContext) {
        this.webInterfaceManager = webInterfaceManager;
        this.soyTemplateRendererProvider = soyTemplateRendererProvider;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }
    
    @Override
    public String getWarningHtml(SearchRequest searchRequest) {

        ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
        List<String> warningMessages = webInterfaceManager.getDisplayableWebPanels(FILTER_DELETION_WARNING_PANEL_LOCATION, createContext(searchRequest, user)).stream()
                .map(webPanel -> webPanel.getHtml(createContext(searchRequest, user)))
                .filter(html -> !html.isEmpty())
                .collect(toList());
        return soyTemplateRendererProvider.getRenderer().render(TEMPLATES_RESOURCE_KEY, TEMPLATE_NAME, of("warningMessages", warningMessages));
    }

    @Override
    public void requireDefaultStyle(final RequiredResources requiredResources) {
        requiredResources.requireWebResource(STYLES_WEB_RESOURCE_KEY);
    }

    private HashMap<String, Object> createContext(final SearchRequest searchRequest, ApplicationUser user) {

        HashMap<String, Object> context = Maps.newHashMap();
        context.put(CONTEXT_KEY_SEARCH_REQUEST, searchRequest);
        context.put(CONTEXT_KEY_USER, user);
        return context;
    }
}
