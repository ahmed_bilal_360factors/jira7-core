package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;

import javax.annotation.Nonnull;
import java.util.Collections;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * A {@link com.atlassian.jira.application.ApplicationRole} uses the data in an {@link ApplicationKey}
 * to implement {@link #getKey()} and {@link #getName()}.
 *
 * @since 7.0
 */
final class IdApplicationRole extends AbstractApplicationRole {
    private final ApplicationKey key;

    private static final boolean UNDEFINED = false;

    static IdApplicationRole empty(final ApplicationKey key) {
        return new IdApplicationRole(key, Collections.emptyList(), Collections.emptyList(), 0, false);
    }

    IdApplicationRole(final ApplicationKey key, final Iterable<Group> groups, final Iterable<Group> defaultGroups,
                      int numberOfLicenseSeats, final boolean selectedByDefault) {
        super(groups, defaultGroups, numberOfLicenseSeats, selectedByDefault); // The application/product is never defined at this point.
        this.key = notNull("id", key);
    }

    @Nonnull
    @Override
    public ApplicationKey getKey() {
        return key;
    }

    @Nonnull
    @Override
    public String getName() {
        return key.value();
    }

    @Nonnull
    @Override
    public ApplicationRole withGroups(@Nonnull final Iterable<Group> groups, @Nonnull final Iterable<Group> defaultGroups) {
        return new IdApplicationRole(key, groups, defaultGroups, getNumberOfSeats(), isSelectedByDefault());
    }

    @Override
    public ApplicationRole withSelectedByDefault(final boolean selectedByDefault) {
        return new IdApplicationRole(key, getGroups(), getDefaultGroups(), getNumberOfSeats(), selectedByDefault);
    }

    @Override
    public boolean isPlatform() {
        return false;
    }

    @Override
    public boolean isDefined() {
        // The application/product is never defined at this point.
        return UNDEFINED;
    }
}