package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationAccess;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.host.ApplicationAccessFactory;
import com.atlassian.application.host.NotLicensedAccess;
import com.atlassian.fugue.Option;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.user.util.UserManager;
import org.joda.time.DateTime;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * JIRA's implementation of {@link com.atlassian.application.host.ApplicationAccessFactory}.
 *
 * @since v7.0
 */
public class JiraApplicationAccessFactory implements ApplicationAccessFactory {
    private final ApplicationRoleManager applicationRoleManager;
    private final UserManager userManager;
    private final JiraLicenseManager manager;

    public JiraApplicationAccessFactory(final ApplicationRoleManager applicationRoleManager,
                                        final UserManager userManager, final JiraLicenseManager manager) {
        this.applicationRoleManager = notNull("applicationRoleManager", applicationRoleManager);
        this.userManager = notNull("userManager", userManager);
        this.manager = notNull("manager", manager);
    }

    private static ApplicationAccess createNoLicensedAccess(final ApplicationKey key) {
        return new NotLicensedAccess(key, JiraApplicationAccess.getManagementPageForRole(key));
    }

    @Override
    public ApplicationAccess access(final ApplicationKey key, final DateTime buildDate) {
        notNull("key", key);
        notNull("buildDate", buildDate);

        final Option<ApplicationRole> role = applicationRoleManager.getRole(key);
        if (role.isEmpty()) {
            return createNoLicensedAccess(key);
        } else {
            final Option<LicenseDetails> licenseFor = manager.getLicense(key);
            if (licenseFor.isEmpty()) {
                return createNoLicensedAccess(key);
            } else {
                return new JiraApplicationAccess(role.get(),
                        ComponentAccessor.getComponent(ApplicationAuthorizationService.class), userManager);
            }
        }
    }
}
