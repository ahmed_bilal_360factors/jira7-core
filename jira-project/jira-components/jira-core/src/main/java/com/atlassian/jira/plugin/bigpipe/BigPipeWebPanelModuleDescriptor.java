package com.atlassian.jira.plugin.bigpipe;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.plugin.PluginInjector;
import com.atlassian.jira.servermetrics.ServerMetricsDetailCollector;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.DefaultWebPanelModuleDescriptor;
import com.atlassian.plugin.web.model.EmbeddedTemplateWebPanel;
import com.atlassian.plugin.web.model.ResourceTemplateWebPanel;
import com.atlassian.plugin.web.model.WebPanel;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import org.dom4j.Element;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static java.lang.Boolean.TRUE;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.isEmpty;
import static org.apache.commons.lang.StringUtils.isNotEmpty;

/**
 * Extension of {@link DefaultWebPanelModuleDescriptor} that allows content to be loaded through BigPipe. Content
 * rendering of BigPipe enabled panel is deferred to the end of the request (or to another thread), allowing user to see
 * intermediate result faster. Until actual content is rendered and sent to browser user will see temporary placeholder
 * (which should be static so its generation is very fast). In order to enable BigPipe for web-panel resource
 * named bigPipe must added like so:
 * <pre>
 *     &lt;web-panel key="myPanel" location="general">
 *          [...]
 *         &lt;resource name="bigPipe" type="static">
 *          &lt;param name="bigPipeId" value="activity-panel-pipe-id" &#47;>
 *          &lt;![CDATA[
 *                    &lt;big-pipe data-id="activity-panel-pipe-id">&lt;div>Placeholder&lt;/div>&lt;/big-pipe>
 *          ]]>
 *         &lt;/resource>
 *     &lt;/web-panel>
 * </pre>
 *
 * And com.atlassian.jira.config.BIG_PIPE dark feature must be enabled as well. To do that using jmake just pass
 * the following parameter:
 * <pre>-J-Datlassian.darkfeature.com.atlassian.jira.config.BIG_PIPE=true</pre>
 *
 * @since v7.1
 */
public class BigPipeWebPanelModuleDescriptor extends DefaultWebPanelModuleDescriptor {
    private static final String FEATURE_BIG_PIPE_KILLSWITCH = "com.atlassian.jira.config.BIG_PIPE_KILLSWITCH";
    static final String FEATURE_WEB_PANEL_METRICS = "com.atlassian.jira.config.WEB_PANEL_METRICS";
    private static final String PARAM_BIG_PIPE_ID = "bigPipeId";
    static final String PARAM_BIG_PIPE_PRIORITY = "bigPipePriority";
    private static final String PARM_BIG_PIPE_PRIORITY_CONFIGURER = "bigPipePriorityConfigurer";
    private static final String NAME_BIG_PIPE = "bigPipe";

    private final BigPipeService bigPipeService;

    final HostContainer hostContainer;
    private Optional<ResourceDescriptor> bigPipeResource;
    private final FeatureManager featureManager;
    private final ServerMetricsDetailCollector detailCollector;

    public BigPipeWebPanelModuleDescriptor(final HostContainer hostContainer, final ModuleFactory moduleClassFactory,
                                           final WebInterfaceManager webInterfaceManager, final BigPipeService bigPipeService,
                                           final FeatureManager featureManager, final ServerMetricsDetailCollector detailCollector) {
        super(hostContainer, moduleClassFactory, webInterfaceManager);
        this.bigPipeService = bigPipeService;
        this.hostContainer = hostContainer;
        this.featureManager = featureManager;
        this.detailCollector = detailCollector;
    }

    @Override
    public void init(final Plugin plugin, final Element element) throws PluginParseException {
        super.init(plugin, element);
        bigPipeResource = getBigPipeResource();
    }

    @Override
    public WebPanel getModule() {
        if (bigPipeResource.isPresent() && bigPipeService.isBigPipeEnabled()) {
            final String bigPipeId = bigPipeResource.get().getParameter(PARAM_BIG_PIPE_ID);
            String configurerClassName = bigPipeResource.get().getParameter(PARM_BIG_PIPE_PRIORITY_CONFIGURER);
            if (Strings.isNullOrEmpty(configurerClassName)) {
                //Default if not configured
                configurerClassName = GlobalPriorityConfigurer.class.getName();
            }

            final BigPipePriorityConfigurer priorityConfigurer = loadPriorityConfigurer(configurerClassName);
            final WebPanel placeholderBigPipePanel = metricsRecordingPlaceholderPanel(placeholderBigPipePanel(bigPipeResource.get()));
            return new BigPipeDelegatingWebPanel(bigPipeId, priorityConfigurer, bigPipeResource.get(), metricsRecordingBigPipeContentPanel(super.getModule()), placeholderBigPipePanel);
        } else {
            return metricsRecordingStandardPanel(super.getModule());
        }
    }

    private WebPanel metricsRecordingStandardPanel(WebPanel base) {
        if (featureManager.isEnabled(FEATURE_WEB_PANEL_METRICS)) {
            return new MetricsRecordingWebPanel(base, "panel." + getKey());
        } else {
            return base;
        }
    }

    private WebPanel metricsRecordingPlaceholderPanel(WebPanel base) {
        if (featureManager.isEnabled(FEATURE_WEB_PANEL_METRICS)) {
            return new MetricsRecordingWebPanel(base, "placeholder.panel." + getKey());
        } else {
            return base;
        }
    }

    private WebPanel metricsRecordingBigPipeContentPanel(WebPanel base) {
        if (featureManager.isEnabled(FEATURE_WEB_PANEL_METRICS)) {
            return new MetricsRecordingWebPanel(base, "content.panel." + getKey());
        } else {
            return base;
        }
    }

    private BigPipePriorityConfigurer loadPriorityConfigurer(final String className) {
        try {
            Class<? extends BigPipePriorityConfigurer> providerClass;
            try {
                providerClass = plugin.loadClass(className, getClass());
            } catch (ClassNotFoundException e) {
                //Use JIRA container instead as fallback
                try {
                    providerClass = Class.forName(className, true, BigPipeWebPanelModuleDescriptor.class.getClassLoader()).asSubclass(BigPipePriorityConfigurer.class);
                } catch (ClassNotFoundException ex) {
                    //Throw original exception as it will be clearer to plugin developer
                    e.addSuppressed(ex);
                    throw e;
                }
            }
            return PluginInjector.newInstance(providerClass, plugin);
        } catch (Exception e) {
            throw new RuntimeException("Could not load bigpipe priority configurer '" + className + "' in plugin " + plugin, e);
        }
    }

    /**
     * Creates webPanel that will render placeholder until actual content is streamed through BigPipe.
     */
    private WebPanel placeholderBigPipePanel(ResourceDescriptor bigPipeResource) {
        final String filename = bigPipeResource.getLocation();
        if (isNotEmpty(filename)) {
            final ResourceTemplateWebPanel panel = hostContainer.create(ResourceTemplateWebPanel.class);
            panel.setResourceFilename(filename);
            panel.setResourceType(getRequiredResourceType(bigPipeResource));
            panel.setPlugin(getPlugin());
            return panel;
        }

        // If no resource file is specified the panel template must be
        // embedded.
        final String body = Preconditions.checkNotNull(bigPipeResource.getContent());

        final EmbeddedTemplateWebPanel panel = hostContainer.create(EmbeddedTemplateWebPanel.class);
        panel.setTemplateBody(body);
        panel.setResourceType(getRequiredResourceType(bigPipeResource));
        panel.setPlugin(getPlugin());
        return panel;
    }

    /**
     * @return the (first) resource with attribute <code>name="bigPipe"</code>
     * @throws PluginParseException if bigPipe resource does not have bigPipeId parameter.
     */
    private Optional<ResourceDescriptor> getBigPipeResource() {
        return getResourceDescriptors().stream()
                .filter(r -> NAME_BIG_PIPE.equals(r.getName()))
                .findFirst()
                .map(r -> {
                    if (isBlank(r.getParameter(PARAM_BIG_PIPE_ID))) {
                        throw new PluginParseException("Resource element with name 'bigPipe' is lacking a "
                                + "bigPipeId parameter.");
                    }
                    return r;
                });
    }

    private String getRequiredResourceType(final ResourceDescriptor resource) {
        final String type = resource.getType();
        if (isEmpty(type)) {
            throw new PluginParseException("Resource element is lacking a type attribute.");
        } else {
            return type;
        }
    }

    private class MetricsRecordingWebPanel implements WebPanel {
        private final String metricName;
        private final WebPanel delegate;

        public MetricsRecordingWebPanel(WebPanel delegate, String metricName) {
            this.delegate = delegate;
            this.metricName = metricName;
        }

        @Override
        public String getHtml(Map<String, Object> context) {
            detailCollector.checkpointReached(metricName + ".start");
            String html = delegate.getHtml(context);
            detailCollector.checkpointReached(metricName + ".finish");
            return html;
        }

        @Override
        public void writeHtml(Writer writer, Map<String, Object> context) throws IOException {
            detailCollector.checkpointReached(metricName + ".start");
            delegate.writeHtml(writer, context);
            detailCollector.checkpointReached(metricName + ".finish");
        }
    }

    private class BigPipeDelegatingWebPanel implements WebPanel {
        private final String resourceId;
        private final BigPipePriorityConfigurer priorityConfigurer;
        private final ResourceDescriptor resourceDescriptor;
        private final WebPanel delegate;
        private final WebPanel placeholderBigPipePanel;

        private BigPipeDelegatingWebPanel(final String resourceId, final BigPipePriorityConfigurer priorityConfigurer,
                                          final ResourceDescriptor resourceDescriptor, final WebPanel delegate,
                                          final WebPanel placeholderBigPipePanel) {
            this.resourceId = resourceId;
            this.priorityConfigurer = priorityConfigurer;
            this.resourceDescriptor = resourceDescriptor;
            this.delegate = delegate;
            this.placeholderBigPipePanel = placeholderBigPipePanel;
        }

        public String getHtml(final Map<String, Object> context) {
            if (useBigPipe()) {
                Integer priority = priorityConfigurer.calculatePriority(resourceDescriptor, Collections.unmodifiableMap(context));
                bigPipeService.pipeContent(resourceId, priority, () -> delegate.getHtml(context));
                return placeholderBigPipePanel.getHtml(context);
            }
            return delegate.getHtml(getContextProvider().getContextMap(Maps.newHashMap(context)));
        }

        public void writeHtml(Writer writer, Map<String, Object> context) throws IOException {
            if (useBigPipe()) {
                Integer priority = priorityConfigurer.calculatePriority(resourceDescriptor, Collections.unmodifiableMap(context));
                bigPipeService.pipeContent(resourceId, priority, () -> {
                    try {
                        //FIXME: should we have a way of doing it through writer in bigPipe?
                        StringWriter stringWriter = new StringWriter();
                        delegate.writeHtml(stringWriter, context);
                        return stringWriter.toString();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
                placeholderBigPipePanel.writeHtml(writer, context);
            } else {
                delegate.writeHtml(writer, getContextProvider().getContextMap(Maps.newHashMap(context)));
            }
        }

        private boolean useBigPipe() {
            HttpServletRequest request = ExecutingHttpRequest.get();
            if ("xmlhttprequest".equalsIgnoreCase(request.getHeader("X-Requested-With"))) {
                return false;
            }
            if (TRUE.equals(request.getAttribute("is_rest_request"))) {
                return false;
            }
            if (featureManager.isEnabled(FEATURE_BIG_PIPE_KILLSWITCH)) {
                return false;
            }

            return true;
        }
    }
}
