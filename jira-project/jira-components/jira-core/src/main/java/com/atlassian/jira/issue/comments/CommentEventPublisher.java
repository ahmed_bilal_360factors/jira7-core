package com.atlassian.jira.issue.comments;

import java.util.Map;

/**
 * Handles {@link com.atlassian.jira.event.comment.CommentEvent} publication.
 *
 * @since 7.1
 */
public interface CommentEventPublisher {

    /**
     * Publishes events when a comment is created. This method publishes {@link com.atlassian.jira.event.comment.CommentCreatedEvent}
     * and {@link com.atlassian.jira.event.issue.IssueEvent} with event type {@link com.atlassian.jira.event.type.EventType#ISSUE_COMMENTED_ID}.
     *
     * @param comment the comment which was created.
     * @param parameters parameters of the newly created comment.
     */
    void publishCommentUpdatedEvent(Comment comment, Map<String, Object> parameters);

    /**
     * Publishes events when a comment is updated. This method publishes {@link com.atlassian.jira.event.comment.CommentUpdatedEvent}
     * and {@link com.atlassian.jira.event.issue.IssueEvent} with event type {@link com.atlassian.jira.event.type.EventType#ISSUE_COMMENT_EDITED_ID}.
     *
     * @param comment the comment which was updated.
     * @param parameters parameters of the updated comment.
     */
    void publishCommentCreatedEvent(Comment comment, Map<String, Object> parameters);

    /**
     * Publishes events when a comment is deleted. Unlike {@link #publishCommentCreatedEvent(Comment, Map)}
     * and {@link #publishCommentUpdatedEvent(Comment, Map)} this method only publishes {@link com.atlassian.jira.event.comment.CommentDeletedEvent}.
     * {@link com.atlassian.jira.event.issue.IssueEvent} with {@link com.atlassian.jira.event.type.EventType#ISSUE_COMMENT_DELETED_ID} are published by
     * {@link com.atlassian.jira.issue.util.IssueUpdater}.
     *
     * @param comment the comment which was deleted.
     */
    void publishCommentDeletedEvent(Comment comment);
}
