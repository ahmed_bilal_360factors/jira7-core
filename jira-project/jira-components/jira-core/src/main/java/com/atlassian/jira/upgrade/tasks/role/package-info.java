/**
 * Package that provides migration for use permission and licenses from dark ages (JIRA 6.x) to renaissance (JIRA 7.x).
 *
 * @since 7.0
 */
@ParametersAreNonnullByDefault package com.atlassian.jira.upgrade.tasks.role;

import javax.annotation.ParametersAreNonnullByDefault;