package com.atlassian.jira.model.querydsl;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Generated;

/**
 * Data Transfer Object for the ProjectKey entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 *
 * @see QProjectKey
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class ProjectKeyDTO implements DTO {
    private final Long id;
    private final Long projectId;
    private final String projectKey;

    public Long getId() {
        return id;
    }

    public Long getProjectId() {
        return projectId;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public ProjectKeyDTO(Long id, Long projectId, String projectKey) {
        this.id = id;
        this.projectId = projectId;
        this.projectKey = projectKey;
    }

    /**
     * Creates a GenericValue object from the values in this Data Transfer Object.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
     * @return a GenericValue object constructed from the values in this Data Transfer Object.
     */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator) {
        return ofBizDelegator.makeValue("ProjectKey", new FieldMap()
                .add("id", id)
                .add("projectId", projectId)
                .add("projectKey", projectKey)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */
    public static ProjectKeyDTO fromGenericValue(GenericValue gv) {
        return new ProjectKeyDTO(
                gv.getLong("id"),
                gv.getLong("projectId"),
                gv.getString("projectKey")
        );
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(ProjectKeyDTO projectKeyDTO) {
        return new Builder(projectKeyDTO);
    }

    public static class Builder {
        private Long id;
        private Long projectId;
        private String projectKey;

        public Builder() {
        }

        public Builder(ProjectKeyDTO projectKeyDTO) {
            this.id = projectKeyDTO.id;
            this.projectId = projectKeyDTO.projectId;
            this.projectKey = projectKeyDTO.projectKey;
        }

        public ProjectKeyDTO build() {
            return new ProjectKeyDTO(id, projectId, projectKey);
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }
        public Builder projectId(Long projectId) {
            this.projectId = projectId;
            return this;
        }
        public Builder projectKey(String projectKey) {
            this.projectKey = projectKey;
            return this;
        }
    }
}