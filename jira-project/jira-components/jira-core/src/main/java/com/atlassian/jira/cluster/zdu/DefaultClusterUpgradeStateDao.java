package com.atlassian.jira.cluster.zdu;

import com.atlassian.jira.database.DatabaseSystemTimeReader;
import com.atlassian.jira.database.DatabaseSystemTimeReaderFactory;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.model.querydsl.ClusterUpgradeStateDTO;
import com.atlassian.jira.model.querydsl.QClusterUpgradeState;

import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;
import java.sql.SQLException;
import java.util.Optional;

import static com.atlassian.jira.model.querydsl.QClusterUpgradeState.CLUSTER_UPGRADE_STATE;
import static com.querydsl.sql.SQLExpressions.select;


public class DefaultClusterUpgradeStateDao implements ClusterUpgradeStateDao {

    private final DatabaseSystemTimeReader databaseSystemTimeReader;
    private final QueryDslAccessor queryDslAccessor;

    public DefaultClusterUpgradeStateDao(DatabaseSystemTimeReaderFactory databaseSystemTimeReaderFactory,
                                         QueryDslAccessor queryDslAccessor) {
        this.databaseSystemTimeReader = databaseSystemTimeReaderFactory.getReader();
        this.queryDslAccessor = queryDslAccessor;
    }

    @Override
    @Nullable
    @GuardedBy("ClusterLock in DefaultClusterUpgradeStateManager#lock")
    public Optional<ClusterUpgradeStateDTO> getCurrent() {
        return Optional.ofNullable(queryDslAccessor.executeQuery(dbConnection ->
                dbConnection.newSqlQuery()
                        .select(CLUSTER_UPGRADE_STATE)
                        .from(CLUSTER_UPGRADE_STATE)
                        .where(CLUSTER_UPGRADE_STATE.clusterVersion.isNotNull()
                                .and(CLUSTER_UPGRADE_STATE.clusterBuildNumber.isNotNull()))
                        .orderBy(CLUSTER_UPGRADE_STATE.orderNumber.coalesce(0L).desc(), CLUSTER_UPGRADE_STATE.databaseTime.desc())
                        .limit(1)
                        .fetchFirst()
        ));
    }

    @Override
    @GuardedBy("ClusterLock in DefaultClusterUpgradeStateManager#lock")
    public void writeState(NodeBuildInfo nodeBuildInfo, UpgradeState state) {
        QClusterUpgradeState clusterUpgradeStateAlias = new QClusterUpgradeState("c");
        queryDslAccessor.execute(connection ->
                connection.insert(CLUSTER_UPGRADE_STATE)
                        .withId()
                        .set(CLUSTER_UPGRADE_STATE.databaseTime, getDatabaseTime())
                        .set(CLUSTER_UPGRADE_STATE.clusterVersion, nodeBuildInfo.getVersion())
                        .set(CLUSTER_UPGRADE_STATE.clusterBuildNumber, nodeBuildInfo.getBuildNumber())
                        .set(CLUSTER_UPGRADE_STATE.state, state.name())
                        .set(CLUSTER_UPGRADE_STATE.orderNumber,
                                select(clusterUpgradeStateAlias.orderNumber.max().add(1).coalesce(1L))
                                        .from(clusterUpgradeStateAlias))
                        .execute());
    }

    private long getDatabaseTime() {
        try {
            return databaseSystemTimeReader.getDatabaseSystemTimeMillis();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
