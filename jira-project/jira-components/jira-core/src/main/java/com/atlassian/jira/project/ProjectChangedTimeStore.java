package com.atlassian.jira.project;

import java.sql.Timestamp;
import java.util.Optional;

/**
 * Provide access to ProjectChangedTime in database.
 *
 * @since v7.2
 */
public interface ProjectChangedTimeStore {
    /**
     * Fetches ProjectChangedTime for the given projectId.
     *
     * @param projectId
     * @return If no record found, returns empty Optional. Otherwise, returns the projectUpdatedTime.
     */
    Optional<ProjectChangedTime> getProjectChangedTime(long projectId);

    /**
     * Updates if an entry for the given project exists, otherwise adds a new entry with the issue updated time
     *
     * @param projectId
     * @param issueUpdatedTime
     */
    void updateOrAddIssueChangedTime(long projectId, Timestamp issueUpdatedTime);
}
