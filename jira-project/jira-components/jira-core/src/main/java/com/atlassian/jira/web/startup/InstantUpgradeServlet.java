package com.atlassian.jira.web.startup;

import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.startup.InstantUpgradeManager;
import com.google.common.collect.ImmutableMap;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.MappingJsonFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.Optional;

import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_SERVICE_UNAVAILABLE;

/**
 * This endpoint is used by the instant upgrade feature.
 * GET returns the state of the system.
 * PUT activates the instance.
 * @since v7.1.0
 */
public class InstantUpgradeServlet  extends HttpServlet {
    static final JsonFactory JSON = new MappingJsonFactory();
    static final String INSTANCE_STATE = "state";
    static final String MESSAGE = "message";

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        Map<String, String> responseJson;
        Optional<InstantUpgradeManager> maybeInstantUpgradeManager = ComponentAccessor.getComponentSafely(InstantUpgradeManager.class);
        
        response.setContentType("application/json");
        if (!maybeInstantUpgradeManager.isPresent()) {
            responseJson = ImmutableMap.of(
                    MESSAGE, "instant upgrade manager is not available"
            );
            response.setStatus(SC_SERVICE_UNAVAILABLE);
        } else {
            responseJson = ImmutableMap.of(
                    INSTANCE_STATE, maybeInstantUpgradeManager.get().getState().name()
            );
            response.setStatus(SC_OK);
        }
        
        try (JsonGenerator jsonGenerator = JSON.createJsonGenerator(response.getWriter())) {
            jsonGenerator.writeObject(responseJson);
        }
    }

    @Override
    protected void doPut(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        Optional<Serializable> responseJson;
        response.setContentType("application/json");

        Optional<InstantUpgradeManager> maybeInstantUpgradeManager = ComponentAccessor.getComponentSafely(InstantUpgradeManager.class);
        if (!maybeInstantUpgradeManager.isPresent()) {
            responseJson = Optional.of(ImmutableMap.of(
                    MESSAGE, "instant upgrade manager is not available"
            ));
            response.setStatus(SC_SERVICE_UNAVAILABLE);
        } else {
            ServiceResult activateInstanceResult = maybeInstantUpgradeManager.get().activateInstance();
            if (activateInstanceResult.isValid()) {
                response.setStatus(SC_OK);
                responseJson = Optional.empty();
            } else {
                responseJson = Optional.of(ImmutableMap.of(
                        MESSAGE, activateInstanceResult.getErrorCollection().getErrorMessages()
                ));
                response.setStatus(SC_INTERNAL_SERVER_ERROR);
            }
        }
        
        if (responseJson.isPresent()) {
            try (JsonGenerator jsonGenerator = JSON.createJsonGenerator(response.getWriter())) {
                jsonGenerator.writeObject(responseJson.get());
            }
        }
    }

}
