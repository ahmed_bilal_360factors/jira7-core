package com.atlassian.jira.dashboard.analytics;

import com.atlassian.analytics.api.annotations.EventName;

/**
 * Event raised when a gadget on a dashboard is hidden for any reason.
 */
@EventName("jira.dashboard.gadget.hidden")
public class GadgetHiddenEvent {
    public enum Reason {
        ENABLED_CONDITION_FAILED,
        LOCAL_CONDITION_FAILED,
        PERMISSIONS_FAILED
    }

    private final String gadgetName;
    private final String conditionClass;
    private final Reason reason;
    private final boolean isAnonymousUser;

    public GadgetHiddenEvent(final String gadgetName, final String conditionClass, final Reason reason, final boolean isAnonymousUser) {
        this.gadgetName = gadgetName;
        this.conditionClass = conditionClass;
        this.reason = reason;
        this.isAnonymousUser = isAnonymousUser;
    }

    public String getGadgetName() {
        return gadgetName;
    }

    public String getConditionClass() {
        return conditionClass;
    }

    public String getReason() {
        return reason.name();
    }

    public String getIsAnonymousUser() {
        return Boolean.toString(isAnonymousUser);
    }
}