package com.atlassian.jira.web.action.admin.issuesecurity;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.issue.security.AssignIssueSecuritySchemeCommand;
import com.atlassian.jira.issue.security.AssignIssueSecuritySchemeTaskContext;
import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.issue.security.IssueSecurityLevelScheme;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.issue.security.IssueSecuritySchemeService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.scheme.AbstractSchemeAwareAction;
import com.atlassian.jira.scheme.SchemeManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.task.TaskContext;
import com.atlassian.jira.task.TaskDescriptor;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.util.ParameterUtils;
import com.atlassian.jira.web.action.admin.notification.ProjectAware;
import com.atlassian.jira.web.bean.TaskDescriptorBean;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;
import org.ofbiz.core.entity.GenericEntityException;
import webwork.action.ActionContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

@WebSudoRequired
public class SelectProjectSecuritySchemeStep2 extends AbstractSchemeAwareAction implements ProjectAware {
    //private List affectedIssues;
    private Long origSchemeId;
    private Long newSchemeId;
    private Long projectId;
    private Project project;
    private Map<Long, Long> levels = null;
    private static final String LEVEL_PREFIX = "level_";

    private final TaskManager taskManager;
    private final TaskDescriptorBean.Factory taskBeanFactory;
    private final IssueSecuritySchemeManager issueSecuritySchemeManager;
    private final IssueSecuritySchemeService issueSecuritySchemeService;
    private final ProjectManager projectManager;

    private final IssueSecurityLevelManager issueSecurityLevelManager;
    private Long taskId;
    private TaskDescriptor<AssignIssueSecuritySchemeCommand.AssignSecurityLevelResult> currentTaskDescriptor;
    private TaskDescriptorBean<AssignIssueSecuritySchemeCommand.AssignSecurityLevelResult> currentTask;
    /**
     * Final destination to go to after progress is acknowledged.
     */
    private String destinationURL;

    public SelectProjectSecuritySchemeStep2(TaskManager taskManager, TaskDescriptorBean.Factory taskBeanFactory,
                                            IssueSecuritySchemeManager issueSecuritySchemeManager, IssueSecuritySchemeService issueSecuritySchemeService, ProjectManager projectManager, IssueSecurityLevelManager issueSecurityLevelManager) {
        this.taskManager = taskManager;
        this.taskBeanFactory = taskBeanFactory;
        this.issueSecuritySchemeManager = issueSecuritySchemeManager;
        this.issueSecuritySchemeService = issueSecuritySchemeService;
        this.projectManager = projectManager;
        this.issueSecurityLevelManager = issueSecurityLevelManager;
    }

    @Override
    public String doDefault() throws Exception {
        final TaskDescriptor<AssignIssueSecuritySchemeCommand.AssignSecurityLevelResult> taskDescriptor = getCurrentTaskDescriptor();
        if (taskDescriptor != null) {
            return getRedirect(taskDescriptor.getProgressURL());
        }
        return super.doDefault();
    }

    @Override
    protected void doValidation() {
        if (origSchemeId != null && origSchemeId.equals(newSchemeId)) {
            addErrorMessage(getText("admin.errors.project.already.associated"));
        }
    }

    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        setDestinationURL(StringUtils.isNotBlank(getReturnUrl()) ? getReturnUrl() : getDefaultDestinationURL());
        setReturnUrl(null);

        final TaskDescriptor<AssignIssueSecuritySchemeCommand.AssignSecurityLevelResult> taskDescriptor = getCurrentTaskDescriptor();
        if (taskDescriptor != null) {
            return getRedirect(taskDescriptor.getProgressURL());
        } else {
            ServiceOutcome<String> serviceOutcome = issueSecuritySchemeService.assignSchemeToProject(getLoggedInUser(), project.getId(), newSchemeId, getLevels());
            if (!serviceOutcome.isValid()) {
                addErrorMessages(serviceOutcome.getErrorCollection().getErrorMessages());
                addErrors(serviceOutcome.getErrorCollection().getErrors());
                return ERROR;
            }
            String redirectUrl = serviceOutcome.get();
            return getRedirect(redirectUrl + "&destinationURL=" + getDestinationURL());
        }
    }

    public String doProgress() throws ExecutionException, InterruptedException {
        if (taskId == null) {
            addErrorMessage(getText("admin.indexing.no.task.id"));
            return ERROR;
        }
        currentTaskDescriptor = taskManager.getTask(taskId);
        if (currentTaskDescriptor == null) {
            addErrorMessage(getText("admin.indexing.no.task.found"));
            return ERROR;
        }
        final TaskContext context = currentTaskDescriptor.getTaskContext();
        if (!(context instanceof AssignIssueSecuritySchemeTaskContext)) {
            addErrorMessage(getText("common.tasks.wrong.task.context", AssignIssueSecuritySchemeTaskContext.class.getName(), context.getClass().getName()));
            return ERROR;
        }

        currentTask = taskBeanFactory.create(currentTaskDescriptor);
        if (currentTaskDescriptor.isFinished() && !currentTaskDescriptor.isCancelled()) {
            final AssignIssueSecuritySchemeCommand.AssignSecurityLevelResult result = currentTaskDescriptor.getResult();
            addErrorCollection(result.getErrorCollection());
        }
        return "progress";
    }

    private Map<Long, Long> getLevels() {
        if (levels == null) {
            levels = new HashMap<>();

            // retrieve the list of levels from the parameters map
            final Map parameters = ActionContext.getParameters();
            final Set keys = parameters.keySet();
            for (final Object key1 : keys) {
                String key = (String) key1;
                if (key.startsWith(LEVEL_PREFIX)) {
                    final Long longParam = ParameterUtils.getLongParam(parameters, key);
                    if (longParam != null) {
                        levels.put(new Long(key.substring(LEVEL_PREFIX.length())), longParam);
                    }
                }
            }
        }
        return levels;
    }

    /**
     * Get count of issues that are part of this project and have this security level
     *
     * @param levelId The security level
     * @return A List containing all affected issues
     */
    public long getAffectedIssues(Long levelId) {
        Long realLevelId = null;
        if (levelId != null && levelId != -1) {
            realLevelId = levelId;
        }
        return issueSecurityLevelManager.getIssueCount(realLevelId, projectId);
    }

    /**
     * Get count of all issues that are part of this project and have security set on them
     *
     * @return A List containing all affected issues
     */
    public long getTotalAffectedIssues() {
        long affectedIssues = 0;
        try {
            for (final IssueSecurityLevel level : getOriginalSecurityLevels()) {
                //add all affected issues for this level to the total list
                affectedIssues += getAffectedIssues(level.getId());
            }
        } catch (Exception e) {
            addErrorMessage(getText("admin.errors.exception") + " " + e);
        }
        return affectedIssues;
    }

    /**
     * Get the list of Security Levels for the original scheme
     *
     * @return A Map containing the levels
     */
    public List<IssueSecurityLevel> getOriginalSecurityLevels() {
        return getSecurityLevels(origSchemeId);
    }

    /**
     * Get the list of Security Levels for the new scheme
     *
     * @return A Map containing the levels
     */
    public List<IssueSecurityLevel> getNewSecurityLevels() {
        return getSecurityLevels(newSchemeId);
    }

    private ImmutableList<IssueSecurityLevel> getSecurityLevels(final Long schemeId) {
        if (schemeId == null) {
            return ImmutableList.of();
        }
        return ImmutableList.<IssueSecurityLevel>builder()
                .add(getNullIssueSecurityLevel(schemeId))
                .addAll(issueSecurityLevelManager.getIssueSecurityLevels(schemeId))
                .build();
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getOrigSchemeId() {
        return origSchemeId;
    }

    public void setOrigSchemeId(Long origSchemeId) {
        if (origSchemeId == null || origSchemeId.equals(new Long(-1)))
            this.origSchemeId = null;
        else
            this.origSchemeId = origSchemeId;
    }

    public Long getNewSchemeId() {
        return newSchemeId;
    }

    public void setNewSchemeId(Long newSchemeId) {
        if (newSchemeId == null || newSchemeId.equals(new Long(-1)))
            this.newSchemeId = null;
        else
            this.newSchemeId = newSchemeId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Project getProject() {
        if (project == null) {
            project = projectManager.getProjectObj(getProjectId());
        }
        return project;
    }

    public SchemeManager getSchemeManager() {
        return issueSecuritySchemeManager;
    }

    public String getRedirectURL() throws GenericEntityException {
        return "/plugins/servlet/project-config/" + getProject().getKey() + "/issuesecurity";
    }

    public IssueSecurityLevelScheme getSecurityScheme(Long schemeId) throws GenericEntityException {
        return issueSecuritySchemeManager.getIssueSecurityLevelScheme(schemeId);
    }

    public static String getLevelPrefix() {
        return LEVEL_PREFIX;
    }

    public String getDestinationURL() {
        return destinationURL;
    }

    public void setDestinationURL(String destinationURL) {
        this.destinationURL = destinationURL;
    }

    public TaskDescriptorBean<AssignIssueSecuritySchemeCommand.AssignSecurityLevelResult> getCurrentTask() {
        if (currentTask == null) {
            final TaskDescriptor<AssignIssueSecuritySchemeCommand.AssignSecurityLevelResult> taskDescriptor = getCurrentTaskDescriptor();
            if (taskDescriptor != null) {
                currentTask = taskBeanFactory.create(taskDescriptor);
            }
        }
        return currentTask;
    }

    private TaskDescriptor<AssignIssueSecuritySchemeCommand.AssignSecurityLevelResult> getCurrentTaskDescriptor() {
        if (currentTaskDescriptor == null) {
            currentTaskDescriptor = taskManager.getLiveTask(new AssignIssueSecuritySchemeTaskContext(getProject()));
        }
        return currentTaskDescriptor;
    }

    public String getDefaultDestinationURL() {
        final Project project = getProject();
        if (project != null) {
            return "/plugins/servlet/project-config/" + project.getKey() + "/issuesecurity";
        }
        return "/secure/project/ViewProjects.jspa";
    }

    private IssueSecurityLevel getNullIssueSecurityLevel(Long schemeId) {
        return new IssueSecurityLevel() {
            @Override
            public Long getId() {
                return -1L;
            }

            @Override
            public String getName() {
                return getI18nHelper().getText("common.words.none");
            }

            @Override
            public String getDescription() {
                return getI18nHelper().getText("common.words.none");
            }

            @Override
            public Long getSchemeId() {
                return schemeId;
            }
        };
    }
}
