package com.atlassian.jira.project.template.descriptor;

import com.atlassian.plugin.PluginParseException;
import org.dom4j.Element;

import java.util.Optional;

public interface PluginParseHelper {
    /**
     * Returns a PluginParseHelper for the element with the given name.
     *
     * @param name an element name
     * @return a PluginParseHelper
     */
    PluginParseHelper element(String name);

    /**
     * Returns a PluginParseHelper for the element with the given name if it exists, Option.none() if it doesn't.
     *
     * @param name an element name
     * @return an Option&lt;PluginParseHelper&gt;
     */
    Optional<PluginParseHelper> optElement(String name);

    /**
     * Returns the value of a the attribute with the given name.
     *
     * @param name the attribute name
     * @return the attribute value
     * @throws PluginParseException if the attribute is not defined
     */
    String attribute(String name) throws PluginParseException;

    /**
     * Returns the value of the attribute with the given name, or null.
     *
     * @param name the attribute name
     * @return the attribute value
     */
    String optAttribute(String name);

    /**
     * Returns the text within the current element.
     *
     * @return the text
     */
    String text();

    /**
     * Returns the raw DOM4J element.
     *
     * @return the raw Element
     */
    Element rawElement();

    /**
     * Null PluginParseHelper that just returns null everywhere.
     */
    final PluginParseHelper NULL_PARSE_HELPER = new PluginParseHelper() {
        @Override
        public PluginParseHelper element(String name) {
            return null;
        }

        @Override
        public Optional<PluginParseHelper> optElement(String name) {
            return Optional.ofNullable(NULL_PARSE_HELPER);
        }

        @Override
        public String attribute(String name) {
            return null;
        }

        @Override
        public String optAttribute(String name) {
            return null;
        }

        @Override
        public String text() {
            return null;
        }

        @Override
        public Element rawElement() {
            return null;
        }
    };
}
