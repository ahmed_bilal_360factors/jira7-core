package com.atlassian.jira.web.component.admin.group;

import com.atlassian.crowd.embedded.api.Group;

import java.util.List;
import java.util.Optional;

/**
 * Service that returns list for labels for a given {@link Group}
 *
 * @since v7.0
 */
public interface GroupLabelsService {
    List<GroupLabelView> getGroupLabels(Group group, Optional<Long> directory);
}
