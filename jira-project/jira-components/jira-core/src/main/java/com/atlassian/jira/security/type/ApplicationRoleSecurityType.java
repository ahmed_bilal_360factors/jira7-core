package com.atlassian.jira.security.type;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationAuthorizationService;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.permission.PermissionContext;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Represents a security type for application roles to be configured on project
 * permission schemes.
 *
 * Admins will be able to select licensed applications or 'Any logged in user'. Note, the 'Any logged in user' option
 * gives permission to any user that belongs to a group associated with an application role.
 *
 * @since 7.0
 */
public class ApplicationRoleSecurityType extends AbstractProjectsSecurityType {
    private static final Logger log = LoggerFactory.getLogger(ApplicationRoleSecurityType.class);

    public static final String ID = "applicationRole";

    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final ApplicationAuthorizationService applicationAuthorizationService;
    private final ApplicationRoleManager applicationRoleManager;
    private final UserUtil userUtil;

    public ApplicationRoleSecurityType(
            final JiraAuthenticationContext jiraAuthenticationContext,
            final ApplicationAuthorizationService applicationAuthorizationService,
            final ApplicationRoleManager applicationRoleManager,
            final UserUtil userUtil) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.applicationAuthorizationService = applicationAuthorizationService;
        this.applicationRoleManager = applicationRoleManager;
        this.userUtil = userUtil;
    }

    @Override
    public String getDisplayName() {
        return jiraAuthenticationContext.getI18nHelper().getText("admin.permission.types.application.role");
    }

    @Override
    public String getArgumentDisplay(String argument) {
        if (isBlank(argument)) {
            return jiraAuthenticationContext.getI18nHelper().getText("admin.permission.types.application.role.any");
        }

        final Option<ApplicationRole> role = toApplicationRole(argument);
        if (role.isDefined()) {
            return role.get().getName();
        }
        return argument;
    }

    @Override
    public String getType() {
        return ID;
    }

    @Override
    public boolean hasPermission(Project project, String applicationRole) {
        // Only *actual* users can have an application role
        return false;
    }

    @Override
    public boolean hasPermission(Issue issue, String parameter) {
        // Only *actual* users can have an application role
        return false;
    }

    @Override
    public boolean hasPermission(Project project, String parameter, ApplicationUser user, boolean issueCreation) {
        return hasPermission(parameter, user);
    }

    @Override
    public boolean hasPermission(Issue issue, String parameter, ApplicationUser user, boolean issueCreation) {
        return hasPermission(parameter, user);
    }

    private boolean hasPermission(String applicationId, ApplicationUser user) {
        if (user == null) {
            throw new IllegalArgumentException("User passed must not be null");
        }
        // If there is no applicationRole then it is "Any", which means as long as there's a user we allow them access
        if (isBlank(applicationId)) {
            return true;
        }
        Option<ApplicationKey> applicationKey = toApplicationKey(applicationId);
        return applicationKey.isDefined() &&
            applicationAuthorizationService.canUseApplication(user, applicationKey.get());
    }

    @Override
    public void doValidation(String key, Map parameters, JiraServiceContext jiraServiceContext) {
        final String applicationId = (String) parameters.get(ID);
        if (isNotBlank(applicationId) && toApplicationRole(applicationId).isEmpty()) {
            final String errorMsg = jiraServiceContext.getI18nBean().getText("admin.permissions.errors.please.select.application.role");
            jiraServiceContext.getErrorCollection().addErrorMessage(errorMsg);
        }
    }

    public Set<ApplicationRole> getApplicationRoles() {
        return applicationRoleManager.getRoles();
    }

    @Override
    public Set<ApplicationUser> getUsers(PermissionContext ctx, String applicationId) {
        if (isBlank(applicationId)) {
            return Sets.newHashSet(userUtil.getAllApplicationUsers());
        }

        Option<ApplicationRole> role = toApplicationRole(applicationId);
        if (role.isDefined()) {
            final Set<Group> groups = Sets.newHashSet(role.get().getGroups());
            return userUtil.getAllUsersInGroups(groups);
        }
        return Collections.emptySet();
    }

    private Option<ApplicationRole> toApplicationRole(final String applicationId) {
        return toApplicationKey(applicationId).flatMap(applicationRoleManager::getRole);
    }

    private Option<ApplicationKey> toApplicationKey(String applicationIdString) {
        try {
            return option(ApplicationKey.valueOf(applicationIdString));
        } catch (IllegalArgumentException e) {
            log.error("Invalid Application ID supplied: '{}'", applicationIdString, e);
            return none();
        }
    }
}
