package com.atlassian.jira.plugin.webpanel.notification;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.websudo.InternalWebSudoManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.HttpServletVariables;
import com.atlassian.plugin.web.Condition;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Does the current request have valid WebSudo credentials.
 *
 * @since 7.0
 */
public final class IsWebSudoAuthenticated implements Condition {
    private final InternalWebSudoManager internalWebSudoManager;
    private final JiraAuthenticationContext context;
    private final HttpServletVariables servlet;

    public IsWebSudoAuthenticated(final InternalWebSudoManager internalWebSudoManager,
                                  final JiraAuthenticationContext context, HttpServletVariables request) {
        this.internalWebSudoManager = notNull("internalWebSudoManager", internalWebSudoManager);
        this.context = notNull("context", context);
        this.servlet = notNull("request", request);
    }

    @Override
    public void init(final Map<String, String> map) {
    }

    @Override
    public boolean shouldDisplay(final Map<String, Object> map) {
        if (!internalWebSudoManager.isEnabled()) {
            //Can't be websudo authenticated.
            return false;
        }

        final ApplicationUser user = context.getUser();
        if (user == null) {
            //Anonymous can't have websudo session.
            return false;
        }

        final HttpServletRequest request = servlet.getHttpRequest();
        return internalWebSudoManager.hasValidSession(request.getSession(false));
    }
}
