package com.atlassian.jira.project.version;

import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.util.Function;

import java.util.Collection;

import static com.atlassian.jira.project.version.VersionCollectionManipulators.updateValueIfChangedAfterTranformation;

/**
 * Takes issue and performs transformation on choosen version custom field.
 */
class CustomFieldVersionTransformation {
    private final CustomField customField;
    private final Function<Collection<Version>, Collection<Version>> transformation;

    public CustomFieldVersionTransformation(CustomField customField, Function<Collection<Version>, Collection<Version>> transformation) {
        this.customField = customField;
        this.transformation = transformation;
    }

    public void updateVersionFieldInIssue(MutableIssue newIssue) {
        updateValueIfChangedAfterTranformation(
                (Collection<Version>) newIssue.getCustomFieldValue(customField),
                transformation,
                (version) -> newIssue.setCustomFieldValue(customField, version)
        );
    }
}
