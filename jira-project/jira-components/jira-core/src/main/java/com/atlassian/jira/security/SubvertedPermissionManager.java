package com.atlassian.jira.security;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.permission.ProjectPermissionCategory;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.security.plugin.ProjectPermissionTypesManager;
import com.atlassian.jira.user.ApplicationUser;
import com.opensymphony.workflow.loader.ActionDescriptor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;

import static com.atlassian.jira.component.ComponentAccessor.getComponent;
import static com.atlassian.jira.component.ComponentAccessor.getProjectManager;

/**
 * The purpose of this class is to provide a temporary access-all-areas pass
 * and is a (partial) implementation of PermissionManager (subverting the
 * stored permissions). Operations that attempt to specify a change to stored
 * permissions like adding or removing permissions and the getAllGroups() method
 * throw an UnsupportedOperationException.
 */
public class SubvertedPermissionManager implements PermissionManager {
    @Override
    public Collection<ProjectPermission> getAllProjectPermissions() {
        return getComponent(ProjectPermissionTypesManager.class).all();
    }

    @Override
    public Collection<ProjectPermission> getProjectPermissions(@Nonnull ProjectPermissionCategory category) {
        return getComponent(ProjectPermissionTypesManager.class).withCategory(category);
    }

    @Override
    public Option<ProjectPermission> getProjectPermission(@Nonnull ProjectPermissionKey permissionKey) {
        return getComponent(ProjectPermissionTypesManager.class).withKey(permissionKey);
    }

    @Override
    public Collection<Project> getProjects(int permissionId, ApplicationUser user) {
        return getAllProjects();
    }

    @Override
    public Collection<Project> getProjects(@Nonnull ProjectPermissionKey permissionKey, ApplicationUser user) {
        return getAllProjects();
    }

    private Collection<Project> getAllProjects() {
        return getProjectManager().getProjectObjects();
    }

    @Override
    public Collection<Project> getProjects(int permissionId, ApplicationUser user, ProjectCategory projectCategory) {
        return getProjectsWithCategory(projectCategory);
    }

    @Override
    public Collection<Project> getProjects(@Nonnull ProjectPermissionKey permissionKey, @Nullable ApplicationUser user, @Nullable ProjectCategory projectCategory) {
        return getProjectsWithCategory(projectCategory);
    }

    @Override
    public void flushCache() {
        // no-op
    }

    private Collection<Project> getProjectsWithCategory(ProjectCategory category) {
        if (category == null) {
            return getProjectManager().getProjectObjectsWithNoCategory();
        } else {
            return getProjectManager().getProjectsFromProjectCategory(category);
        }
    }

    @Override
    public boolean hasProjects(int permissionId, ApplicationUser user) {
        return hasProjects();
    }

    @Override
    public boolean hasProjects(@Nonnull ProjectPermissionKey permissionKey, ApplicationUser user) {
        return hasProjects();
    }

    private boolean hasProjects() {
        return !getAllProjects().isEmpty();
    }

    /**
     * Not implemented.
     */
    public void removeGroupPermissions(String group) {
        throw new UnsupportedOperationException("removeGroupPermissions() not implemented in " + this.getClass().getName());
    }

    /**
     * Not implemented.
     */
    @Override
    public void removeUserPermissions(final ApplicationUser user) throws RemoveException {
        throw new UnsupportedOperationException("removeUserPermissions() not implemented in " + this.getClass().getName());
    }

    /**
     * Not implemented.
     */
    public Collection<Group> getAllGroups(int permType, Project project) {
        throw new UnsupportedOperationException("getAllGroups() not implemented in " + this.getClass().getName());
    }

    @Override
    public boolean hasPermission(int permissionsId, ApplicationUser user) {
        return true;
    }

    @Override
    public boolean hasPermission(int permissionsId, Issue issue, ApplicationUser user) {
        return true;
    }

    @Override
    public boolean hasPermission(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Issue issue, ApplicationUser user) {
        return true;
    }

    @Override
    public boolean hasPermission(@Nonnull final ProjectPermissionKey permissionKey, @Nonnull final Issue issue, @Nullable final ApplicationUser user, @Nullable final ActionDescriptor actionDescriptor) {
        return true;
    }

    @Override
    public boolean hasPermission(@Nonnull final ProjectPermissionKey permissionKey, @Nonnull final Issue issue, @Nullable final ApplicationUser user, @Nonnull final Status status) {
        return true;
    }

    @Override
    public boolean hasPermission(int permissionsId, Project project, ApplicationUser user) {
        return true;
    }

    @Override
    public boolean hasPermission(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Project project, @Nullable ApplicationUser user) {
        return true;
    }

    @Nonnull
    @Override
    public ProjectWidePermission hasProjectWidePermission(@Nonnull final ProjectPermissionKey permissionKey,
                                                          @Nonnull final Project project, @Nullable final ApplicationUser user) {
        return ProjectWidePermission.ALL_ISSUES;
    }

    /**
     * Always return true.
     *
     * @param permissionsId ignored
     * @param project       ignored
     * @param user          ignored
     * @param issueCreation ignored
     * @return true
     */
    @Override
    public boolean hasPermission(int permissionsId, Project project, ApplicationUser user, boolean issueCreation) {
        return true;
    }

    @Override
    public boolean hasPermission(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Project project, ApplicationUser user, boolean issueCreation) {
        return true;
    }
}
