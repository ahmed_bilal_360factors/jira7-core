package com.atlassian.jira.bc.group;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.google.common.collect.ImmutableSet;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @since v7.0
 */
public class GroupAccessLabelsManager {
    private final GroupsToApplicationsSeatingHelper seatingHelper;
    private final GlobalPermissionManager globalPermissionManager;
    private final GroupRelationshipChecker relationshipChecker;

    public GroupAccessLabelsManager(final GroupsToApplicationsSeatingHelper seatingHelper, final GlobalPermissionManager globalPermissionManager, final GroupRelationshipChecker relationshipChecker) {
        this.seatingHelper = seatingHelper;
        this.globalPermissionManager = globalPermissionManager;
        this.relationshipChecker = relationshipChecker;
    }

    public GroupAccessLabels getForGroup(Group group, Optional<Long> directoryId) {
        final Set<ApplicationRole> applications = seatingHelper.findEffectiveApplicationsByGroups(directoryId, ImmutableSet.of(group.getName()));
        return new GroupAccessLabels(isGroupAdminGroup(group, directoryId), applications);
    }

    private boolean isGroupAdminGroup(Group group, Optional<Long> directoryId) {
        final Collection<Group> adminGroups = globalPermissionManager.getGroupsWithPermission(GlobalPermissionKey.ADMINISTER);
        final Collection<Group> sysadminGroups = globalPermissionManager.getGroupsWithPermission(GlobalPermissionKey.SYSTEM_ADMIN);
        final Set<Group> groups = Stream.concat(adminGroups.stream(), sysadminGroups.stream()).collect(CollectorsUtil.toImmutableSet());
        return groups.stream()
                .map(Group::getName)
                .anyMatch(adminGroup -> relationshipChecker.isGroupEqualOrNested(directoryId, group.getName(), adminGroup));
    }

    public static class GroupAccessLabels {
        private final boolean isAdmin;
        private final Set<ApplicationRole> accessibleApplications;

        public GroupAccessLabels(final boolean isAdmin, final Set<ApplicationRole> accessibleApplications) {
            this.isAdmin = isAdmin;
            this.accessibleApplications = accessibleApplications;
        }

        public boolean isAdmin() {
            return isAdmin;
        }

        public Set<ApplicationRole> getAccessibleApplications() {
            return accessibleApplications;
        }
    }
}
