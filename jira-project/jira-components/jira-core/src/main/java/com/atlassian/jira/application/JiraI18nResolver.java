package com.atlassian.jira.application;

import com.atlassian.application.host.i18n.I18nResolver;
import com.atlassian.jira.security.JiraAuthenticationContext;

import java.io.Serializable;

/**
 * JIRA's implementation of {@code I18nResolver} for {@code atlassian-application}.
 *
 * @since v7.0
 */
final class JiraI18nResolver implements I18nResolver {
    private final JiraAuthenticationContext ctx;

    JiraI18nResolver(final JiraAuthenticationContext ctx) {
        this.ctx = ctx;
    }

    @Override
    public String getText(final String key) {
        return ctx.getI18nHelper().getText(key);
    }

    @Override
    public String getText(final String key, final Serializable... args) {
        return ctx.getI18nHelper().getText(key, args);
    }
}
