package com.atlassian.jira.io;

import org.apache.commons.compress.utils.IOUtils;

import java.io.InputStream;

public class CloseableResourceData implements AutoCloseable{

    private final ResourceData resourceData;

    public CloseableResourceData(final InputStream inputStream, final String contentType) {
        this.resourceData = new ResourceData(inputStream, contentType);
    }

    public InputStream getInputStream() {
        return resourceData.getInputStream();
    }

    public String getContentType() {
        return resourceData.getContentType();
    }

    @Override
    public void close() {
        IOUtils.closeQuietly(getInputStream());
    }
}
