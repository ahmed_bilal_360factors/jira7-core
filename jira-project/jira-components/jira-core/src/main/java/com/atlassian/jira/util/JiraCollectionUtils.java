package com.atlassian.jira.util;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.toList;

public class JiraCollectionUtils {
    /**
     * Converts a {@link String} collection into a {@link String} array.
     *
     * @param allValues The collection to convert
     * @return An array holding all the values of the supplied collection
     */
    public static String[] stringCollectionToStringArray(Collection<String> allValues) {
        return allValues.toArray(new String[allValues.size()]);
    }

    /**
     * Converts an iterable to a new collection if it isn't a collection already. If it is already a collection then the
     * same instance is returned.
     *
     * @param iterable The iterable to be converted
     * @param <T>      The type being iterated over
     * @return A collection of {@link T}
     */
    public static <T> Collection<T> convertIterableToCollection(Iterable<T> iterable) {
        if (iterable instanceof Collection) {
            return (Collection<T>) iterable;
        }

        return StreamSupport.stream(iterable.spliterator(), false).collect(toList());
    }

    /**
     * Turns an Optional<T> into a Stream<T> of length zero or one depending upon
     * whether a value is present.
     */
    public static <T> Stream<T> streamFromOptional(final Optional<T> opt) {
        if (opt.isPresent())
            return Stream.of(opt.get());
        else
            return Stream.empty();
    }
}
