package com.atlassian.jira.issue.fields.option;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.IssueConstant;
import com.google.common.collect.Collections2;

import java.util.Collection;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class IssueConstantOptionSet implements OptionSet {
    private final ConstantsManager constantsManager;
    private final Queue<Option> options;

    IssueConstantOptionSet(ConstantsManager constantsManager, List<IssueConstant> constants) {
        this.constantsManager = constantsManager;
        this.options = new ConcurrentLinkedQueue<>(toOptions(constants));
    }

    public Collection<Option> getOptions() {
        return options;
    }

    public Collection<String> getOptionIds() {
        // Note: Must be a live collection so remove can work :(
        return Collections2.transform(options, Option::getId);
    }

    public void addOption(String constantType, String constantId) {
        final IssueConstant constant = constantsManager.getConstantObject(constantType, constantId);
        if (constant == null) {
            throw new IllegalArgumentException("Not a valid constant: type=" + constantType +
                    "; constantId='" + constantId + '\'');
        }
        options.add(new IssueConstantOption(constant));
    }

    private static Collection<Option> toOptions(Collection<IssueConstant> constants) {
        return constants.stream()
                .map(IssueConstantOption::new)
                .collect(CollectorsUtil.toNewArrayListWithSizeOf(constants));
    }
}

