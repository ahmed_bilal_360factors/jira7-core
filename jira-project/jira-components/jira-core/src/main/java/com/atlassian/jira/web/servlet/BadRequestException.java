package com.atlassian.jira.web.servlet;

/**
 * Thrown to represent a "400 Bad Request" status code.
 * <p>
 *     The request could not be understood by the server due to malformed syntax.
 * </p>
 *
 * @since v7.2
 */
public class BadRequestException extends Exception {
    public BadRequestException(String message) {
        super(message);
    }
}
