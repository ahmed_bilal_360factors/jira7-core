package com.atlassian.jira.plugin.webfragment.contextproviders;

import com.atlassian.jira.application.JiraApplicationManager;
import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.util.HelpUtil;
import com.google.common.collect.Maps;

import java.util.Map;

public class HelpContextProvider extends AbstractJiraContextProvider {
    private final JiraAuthenticationContext authenticationContext;
    private final HelpUrls helpUrls;
    private final JiraApplicationManager applicationManager;

    public HelpContextProvider(
            JiraAuthenticationContext authenticationContext,
            HelpUrls helpUrls,
            JiraApplicationManager applicationManager) {
        this.authenticationContext = authenticationContext;
        this.helpUrls = helpUrls;
        this.applicationManager = applicationManager;
    }

    @Override
    public Map getContextMap(ApplicationUser user, JiraHelper jiraHelper) {
        final Map<String, Object> context = Maps.newHashMap();
        context.put("i18n", authenticationContext.getI18nHelper());
        context.put("helpUrls", helpUrls);
        final HelpUrl coreHelp = helpUrls.getUrlForApplication(applicationManager.getPlatform().getKey(), "default");
        context.put("coreHelpUrl", coreHelp.getUrl());

        //This is here for backwards compatibility in case this context provider is used in plugins.
        //We should probably remove this post JIRA 7.0 - right now it's too risky.
        context.put("helpUtil", HelpUtil.getInstance());

        return context;
    }
}
