package com.atlassian.jira.instrumentation;

/**
 * Types of common metrics supported by caches.
 */
public enum CacheMetricsKeys {
    HITS("hits"),
    MISSES("misses"),
    LOAD_TIME("loadTime"),
    PUT_TIME("putTime"),
    GET_TIME("getTime");

    private final String name;

    CacheMetricsKeys(String name) {
        this.name = name;
    }

    public String key() {
        return name;
    }
}
