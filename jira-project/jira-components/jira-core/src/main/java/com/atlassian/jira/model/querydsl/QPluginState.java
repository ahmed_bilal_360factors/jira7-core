package com.atlassian.jira.model.querydsl;

import com.querydsl.core.types.dsl.*;
import com.querydsl.sql.ColumnMetadata;

import javax.annotation.Generated;
import java.sql.Types;

/**
 * QPluginState is a Querydsl query object.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QPluginState extends JiraRelationalPathBase<PluginStateDTO> {
    public static final QPluginState PLUGIN_STATE = new QPluginState("PLUGIN_STATE");

    public final StringPath key = createString("key");
    public final StringPath enabled = createString("enabled");

    public QPluginState(String alias) {
        super(PluginStateDTO.class, alias, "pluginstate");
        addMetadata();
    }

    private void addMetadata() {
        addMetadata(key, ColumnMetadata.named("pluginkey").withIndex(1).ofType(Types.VARCHAR).withSize(255));
        addMetadata(enabled, ColumnMetadata.named("pluginenabled").withIndex(2).ofType(Types.VARCHAR).withSize(60));
    }

    /**
     * Returns the ofbiz model's entity name for this query object.
     *
     * @return the ofbiz model's entity name for this query object.
     */
    @Override
    public String getEntityName() {
        return "PluginState";
    }

}

