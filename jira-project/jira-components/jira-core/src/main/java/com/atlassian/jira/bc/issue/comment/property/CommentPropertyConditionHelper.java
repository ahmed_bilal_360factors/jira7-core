package com.atlassian.jira.bc.issue.comment.property;

import com.atlassian.jira.entity.property.AbstractEntityPropertyConditionHelper;
import com.atlassian.jira.issue.comments.Comment;

public final class CommentPropertyConditionHelper extends AbstractEntityPropertyConditionHelper<Comment> {

    public CommentPropertyConditionHelper(CommentPropertyService propertyService) {
        super(propertyService, Comment.class, "comment");
    }

}
