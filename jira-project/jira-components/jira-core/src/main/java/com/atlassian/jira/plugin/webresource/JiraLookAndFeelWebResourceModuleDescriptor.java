package com.atlassian.jira.plugin.webresource;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

/**
 * Encapsulates logic specific to declaring, compiling and outputting
 * web-resources that contain Look and Feel affected values
 * (i.e., LESS files that depend on the result of
 * {@link com.atlassian.jira.web.less.LookAndFeelLessProvider#makeLookAndFeelLess})
 * <p/>
 * See:
 * <ul>
 *     <li><a href="https://extranet.atlassian.com/jira/browse/APDEX-863">APDEX-863</a></li>
 *     <li><a href="https://extranet.atlassian.com/jira/browse/APDEX-891">APDEX-891</a></li>
 *     <li><a href="https://extranet.atlassian.com/jira/browse/APDEX-1221">APDEX-1221</a></li>
 * </ul>
 *
 * @since v7.2
 */
public class JiraLookAndFeelWebResourceModuleDescriptor extends WebResourceModuleDescriptor {
    // <transformation extension='less'> <transformer key='lessTransformer'/> </transformation>
    private static Element LESS_TRANSFORMER_ELEMENT = DocumentHelper.createElement("transformation");

    // <context>jira.global.look-and-feel</context>
    private static Element RESOURCE_CONTEXT_ELEMENT = DocumentHelper.createElement("context");

    static {
        LESS_TRANSFORMER_ELEMENT.addAttribute("extension", "less");
        LESS_TRANSFORMER_ELEMENT.add(DocumentHelper.createElement("transformer").addAttribute("key", "lessTransformer"));
        RESOURCE_CONTEXT_ELEMENT.addText("jira.global.look-and-feel");
    }

    public JiraLookAndFeelWebResourceModuleDescriptor(ModuleFactory moduleFactory, final HostContainer hostContainer) {
        super(moduleFactory, hostContainer);
    }

    @Override
    public void init(Plugin plugin, Element element) throws PluginParseException {
        element.add(LESS_TRANSFORMER_ELEMENT.createCopy());
        element.add(RESOURCE_CONTEXT_ELEMENT.createCopy());
        super.init(plugin, element);
    }
}
