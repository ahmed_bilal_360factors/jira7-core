package com.atlassian.jira.model.querydsl;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Generated;

/**
 * Data Transfer Object for the FieldLayoutSchemeEntity entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 *
 * @see QFieldLayoutSchemeEntity
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class FieldLayoutSchemeEntityDTO implements DTO {
    private final Long id;
    private final Long scheme;
    private final String issuetype;
    private final Long fieldlayout;

    public Long getId() {
        return id;
    }

    public Long getScheme() {
        return scheme;
    }

    public String getIssuetype() {
        return issuetype;
    }

    public Long getFieldlayout() {
        return fieldlayout;
    }

    public FieldLayoutSchemeEntityDTO(Long id, Long scheme, String issuetype, Long fieldlayout) {
        this.id = id;
        this.scheme = scheme;
        this.issuetype = issuetype;
        this.fieldlayout = fieldlayout;
    }

    /**
     * Creates a GenericValue object from the values in this Data Transfer Object.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
     * @return a GenericValue object constructed from the values in this Data Transfer Object.
     */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator) {
        return ofBizDelegator.makeValue("FieldLayoutSchemeEntity", new FieldMap()
                .add("id", id)
                .add("scheme", scheme)
                .add("issuetype", issuetype)
                .add("fieldlayout", fieldlayout)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */
    public static FieldLayoutSchemeEntityDTO fromGenericValue(GenericValue gv) {
        return new FieldLayoutSchemeEntityDTO(
                gv.getLong("id"),
                gv.getLong("scheme"),
                gv.getString("issuetype"),
                gv.getLong("fieldlayout")
        );
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(FieldLayoutSchemeEntityDTO fieldLayoutSchemeEntityDTO) {
        return new Builder(fieldLayoutSchemeEntityDTO);
    }

    public static class Builder {
        private Long id;
        private Long scheme;
        private String issuetype;
        private Long fieldlayout;

        public Builder() {
        }

        public Builder(FieldLayoutSchemeEntityDTO fieldLayoutSchemeEntityDTO) {
            this.id = fieldLayoutSchemeEntityDTO.id;
            this.scheme = fieldLayoutSchemeEntityDTO.scheme;
            this.issuetype = fieldLayoutSchemeEntityDTO.issuetype;
            this.fieldlayout = fieldLayoutSchemeEntityDTO.fieldlayout;
        }

        public FieldLayoutSchemeEntityDTO build() {
            return new FieldLayoutSchemeEntityDTO(id, scheme, issuetype, fieldlayout);
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }
        public Builder scheme(Long scheme) {
            this.scheme = scheme;
            return this;
        }
        public Builder issuetype(String issuetype) {
            this.issuetype = issuetype;
            return this;
        }
        public Builder fieldlayout(Long fieldlayout) {
            this.fieldlayout = fieldlayout;
            return this;
        }
    }
}