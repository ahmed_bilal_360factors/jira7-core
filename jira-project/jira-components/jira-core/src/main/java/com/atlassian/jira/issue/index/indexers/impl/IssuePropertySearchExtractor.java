package com.atlassian.jira.issue.index.indexers.impl;

import com.atlassian.fugue.Option;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.entity.property.JsonEntityPropertyManager;
import com.atlassian.jira.index.IndexDocumentConfiguration;
import com.atlassian.jira.index.IndexDocumentConfiguration.ExtractConfiguration;
import com.atlassian.jira.index.IndexDocumentConfiguration.KeyConfiguration;
import com.atlassian.jira.index.IssueSearchExtractor;
import com.atlassian.jira.index.SearchExtractorRegistrationManager;
import com.atlassian.jira.index.property.PluginIndexConfiguration;
import com.atlassian.jira.index.property.PluginIndexConfigurationManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.DoubleConverter;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.util.LuceneUtils;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.util.json.JSONTokener;
import com.atlassian.query.clause.Property;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Fieldable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static com.atlassian.jira.datetime.DateTimeStyle.COMPLETE;
import static com.atlassian.jira.datetime.DateTimeStyle.DATE;
import static com.atlassian.jira.datetime.DateTimeStyle.DATE_PICKER;
import static com.atlassian.jira.datetime.DateTimeStyle.DATE_TIME_PICKER;
import static com.atlassian.jira.datetime.DateTimeStyle.ISO_8601_DATE;
import static com.atlassian.jira.datetime.DateTimeStyle.ISO_8601_DATE_TIME;
import static com.atlassian.jira.datetime.DateTimeStyle.RSS_RFC822_DATE_TIME;
import static com.atlassian.jira.issue.index.DocumentConstants.ISSUE_PROPERTY_VALUE_PREFIX;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toSet;

/**
 * Search extractor that constructs lucene document fields based on entities assigned to particular issue and their
 * configuration as defined by {@link com.atlassian.jira.index.IndexDocumentConfiguration}
 *
 * @since v6.2
 */
public class IssuePropertySearchExtractor implements IssueSearchExtractor {
    private static final Logger LOG = LoggerFactory.getLogger(IssuePropertySearchExtractor.class);

    private final JsonEntityPropertyManager jsonEntityPropertyManager;
    private final PluginIndexConfigurationManager entityPropertyIndexDocumentManager;
    private final DoubleConverter doubleConverter;
    private final Set<DateTimeFormatter> dateTimeFormatters;
    private final Set<DateTimeFormatter> dateOnlyFormatters;

    public IssuePropertySearchExtractor(final JsonEntityPropertyManager jsonEntityPropertyManager,
                                        final PluginIndexConfigurationManager entityPropertyIndexDocumentManager,
                                        final DateTimeFormatterFactory dateTimeFormatterFactory,
                                        final SearchExtractorRegistrationManager searchExtractorRegistrationManager,
                                        final DoubleConverter doubleConverter) {
        this.jsonEntityPropertyManager = jsonEntityPropertyManager;
        this.entityPropertyIndexDocumentManager = entityPropertyIndexDocumentManager;
        this.doubleConverter = doubleConverter;
        dateOnlyFormatters = ImmutableSet.of(
                dateTimeFormatterFactory.formatter().withStyle(DATE).withDefaultLocale(),
                dateTimeFormatterFactory.formatter().withStyle(DATE_PICKER).withDefaultLocale(),
                dateTimeFormatterFactory.formatter().withStyle(ISO_8601_DATE).withDefaultLocale()
        );
        dateTimeFormatters = ImmutableSet.of(
                dateTimeFormatterFactory.formatter().withStyle(COMPLETE).withDefaultLocale(),
                dateTimeFormatterFactory.formatter().withStyle(DATE_TIME_PICKER).withDefaultLocale(),
                dateTimeFormatterFactory.formatter().withStyle(ISO_8601_DATE_TIME).withDefaultLocale(),
                dateTimeFormatterFactory.formatter().withStyle(RSS_RFC822_DATE_TIME).withDefaultLocale()
        );
        searchExtractorRegistrationManager.register(this, Issue.class);
    }

    @Override
    public Set<String> indexEntity(final Context<Issue> ctx, final Document doc) {
        final Iterable<PluginIndexConfiguration> configurations = entityPropertyIndexDocumentManager.getDocumentsForEntity(EntityPropertyType.ISSUE_PROPERTY.getDbEntityName());

        List<IndexDocumentConfiguration> indexDocumentConfigurations = asStream(configurations)
                .map(PluginIndexConfiguration::getIndexDocumentConfiguration)
                .collect(toImmutableList());

        indexDocumentConfigurations.stream()
                .flatMap(conf -> documentConfigurationToFieldables(ctx.getEntity(), conf).stream())
                .forEach(doc::add);

        return indexDocumentConfigurations.stream()
                .flatMap(i -> i.getKeyConfigurations().stream())
                .flatMap(conf -> takeFieldIds(conf).stream())
                .collect(toSet());
    }

    public List<Fieldable> documentConfigurationToFieldables(final Issue issue, final IndexDocumentConfiguration indexDocumentConfiguration) {
        // Grab all the keys so we can optimise our database call.
        final List<String> keys = indexDocumentConfiguration.getKeyConfigurations().stream()
                .map(KeyConfiguration::getPropertyKey)
                .collect(toImmutableList());

        Long issueId = issue.getId();

        Map<String, EntityProperty> entityProperties = jsonEntityPropertyManager.get(
                EntityPropertyType.ISSUE_PROPERTY.getDbEntityName(), issueId, keys);

        return indexDocumentConfiguration.getKeyConfigurations().stream()
                .flatMap(conf -> keyConfigurationToFieldables(issueId, entityProperties, conf).stream())
                .collect(toImmutableList());
    }

    private Set<String> takeFieldIds(KeyConfiguration conf) {
        return conf.getExtractorConfigurations().stream()
                .map(c -> fieldNameByKeyAndPath(conf.getPropertyKey(), c.getPath()))
                .collect(toSet());
    }

    public List<Fieldable> keyConfigurationToFieldables(Long issueId, Map<String, EntityProperty> entityProperties, final KeyConfiguration keyConfiguration) {
        final EntityProperty entityProperty = entityProperties.get(keyConfiguration.getPropertyKey());

        if (entityProperty == null) {
            return emptyList();
        }
        final Option<Object> jsonEntityProperty = getJSON(entityProperty.getValue(), issueId, keyConfiguration.getPropertyKey());
        if (jsonEntityProperty.isEmpty()) {
            return emptyList();
        }

        return keyConfiguration.getExtractorConfigurations().stream()
                .flatMap(conf -> extractConfigurationToFieldables(jsonEntityProperty.get(), keyConfiguration.getPropertyKey(), conf).stream())
                .collect(toImmutableList());
    }

    public List<Fieldable> extractConfigurationToFieldables(final Object jsonEntityProperty, String key, final ExtractConfiguration extractConfiguration) {

        final String path = extractConfiguration.getPath();
        final Option<Object> value = getValueForPath(jsonEntityProperty, path);
        if (value.isEmpty()) {
            return emptyList();
        }

        final Object jsonValue = value.get();
        if (jsonValue instanceof JSONArray) {
            final JSONArray array = (JSONArray) jsonValue;
            final ImmutableList.Builder<Fieldable> fieldsForArray = ImmutableList.builder();
            for (int i = 0; i < array.length(); i++) {
                final Object arrayElement = array.opt(i);
                if (arrayElement != null) {
                    fieldsForArray.addAll(getValueFromJsonObject(arrayElement, path, key, extractConfiguration.getType()));
                }
            }
            return fieldsForArray.build();
        } else {
            return getValueFromJsonObject(jsonValue, path, key, extractConfiguration.getType());
        }
    }

    private Option<Object> getJSON(final String jsonString, final Long issueId, final String entityKey) {
        try {
            final JSONTokener jsonTokener = new JSONTokener(jsonString);
            final Object value = jsonTokener.nextValue();
            if (jsonTokener.more()) {
                //if there is something left after reading and object then we don't parse this
                return Option.none();
            }
            return Option.some(value);
        } catch (final JSONException e) {
            //This should never happen
            final String message = MessageFormat.format("JSON stored in jsonEntityPropertyManagers is not valid for entityId='{'0'}' , entityName='{'1'}', entityKey='{'3'}'{0}",
                    issueId.toString(), EntityPropertyType.ISSUE_PROPERTY.getDbEntityName(), entityKey);
            LOG.debug(message, e);
            return Option.none();
        }
    }

    private String fieldNameByKeyAndPath(String key, String path) {
        return ISSUE_PROPERTY_VALUE_PREFIX + new Property(ImmutableList.of(key), ImmutableList.of(path)).getAsPropertyString();
    }

    private List<Fieldable> getValueFromJsonObject(final Object value, final String path, final String key, final IndexDocumentConfiguration.Type type) {

        if (value instanceof JSONObject || value instanceof JSONArray || value == null) {
            return emptyList();
        }
        final String fieldName = fieldNameByKeyAndPath(key, path);
        String fieldValue = value.toString();
        Field.Index analyze = Field.Index.NOT_ANALYZED_NO_NORMS;
        switch (type) {
            case NUMBER:
                try {
                    fieldValue = doubleConverter.getStringForLucene(fieldValue);
                } catch (final FieldValidationException e) {
                    LOG.debug(MessageFormat.format("Not adding field with name {0}, value {1} is invalid message {3}", fieldName, value, e.getMessage()));
                    return emptyList();
                }
                break;
            case DATE:
                final Option<String> dateOption = getDateValue(fieldValue);
                if (dateOption.isEmpty()) {
                    LOG.debug(MessageFormat.format("Not adding field with name {0}, value {1} cannot be parsed as date", fieldName, value));
                    return emptyList();
                }
                fieldValue = dateOption.get();
                break;
            case TEXT:
                analyze = Field.Index.ANALYZED;
                break;
            case STRING:
                break;

        }
        return singletonList(new Field(fieldName, false, fieldValue, Field.Store.NO, analyze, Field.TermVector.NO));
    }

    private Option<String> getDateValue(final String value) {
        final Option<String> parsedDate = parseDateWithFormatter(value, dateTimeFormatters);
        if (parsedDate.isDefined()) {
            return parsedDate;
        }
        return parseDateWithFormatter(value, dateOnlyFormatters);
    }

    private Option<String> parseDateWithFormatter(String value, Iterable<DateTimeFormatter> formatters) {
        for (final DateTimeFormatter formatter : formatters) {
            try {
                final Date parsedDate = formatter.parse(value);
                if (parsedDate != null) {
                    return Option.some(LuceneUtils.dateToString(parsedDate));
                }
            } catch (final IllegalArgumentException ignore) {

            }
        }
        return Option.none();
    }

    private Option<Object> getValueForPath(final Object jsonEntityProperty, final String path) {
        final String[] split = StringUtils.split(path, '.');
        Object value = jsonEntityProperty;
        for (final String currentKey : split) {
            if (value == null || !(value instanceof JSONObject)) {
                return Option.none();
            } else {
                value = ((JSONObject) value).opt(currentKey);
            }
        }
        return Option.option(value);
    }

    private <T> Stream<T> asStream(Iterable<T> configurations) {
        return StreamSupport.stream(configurations.spliterator(), false);
    }
}
