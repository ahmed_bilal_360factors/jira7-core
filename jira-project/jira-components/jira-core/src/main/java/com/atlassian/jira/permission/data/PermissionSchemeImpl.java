package com.atlassian.jira.permission.data;

import com.atlassian.jira.permission.PermissionGrant;
import com.atlassian.jira.permission.PermissionScheme;
import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

@Immutable
@ParametersAreNonnullByDefault
public final class PermissionSchemeImpl implements PermissionScheme {
    private final Long id;
    private final String name;
    private final String description;
    private final List<PermissionGrant> permissions;

    public PermissionSchemeImpl(final Long id, final String name, @Nullable final String description) {
        this(id, name, description, Collections.<PermissionGrant>emptyList());
    }

    public PermissionSchemeImpl(final Long id, final String name, @Nullable final String description, final Iterable<PermissionGrant> permissions) {
        this.id = checkNotNull(id);
        this.name = checkNotNull(name);
        this.description = Strings.nullToEmpty(description);
        this.permissions = ImmutableList.copyOf(checkNotNull(permissions));
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List<PermissionGrant> getPermissions() {
        return permissions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PermissionSchemeImpl that = (PermissionSchemeImpl) o;

        return Objects.equal(this.id, that.id) &&
                Objects.equal(this.name, that.name) &&
                Objects.equal(this.description, that.description) &&
                Objects.equal(this.permissions, that.permissions);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, name, description, permissions);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("description", description)
                .add("permissions", permissions)
                .toString();
    }
}
