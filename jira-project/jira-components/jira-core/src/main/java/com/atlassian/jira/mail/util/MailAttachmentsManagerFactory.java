package com.atlassian.jira.mail.util;

import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.avatar.AvatarTranscoder;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.InjectableComponent;

/**
 * A factory that should be used for creating {@link MailAttachmentsManager}.
 */
@InjectableComponent
public class MailAttachmentsManagerFactory {

    private final AvatarManager avatarManager;
    private final AvatarService avatarService;
    private final AvatarTranscoder avatarTranscoder;
    private final UserManager userManager;
    private final ApplicationProperties applicationProperties;

    public MailAttachmentsManagerFactory(
            final AvatarManager avatarManager,
            final AvatarService avatarService,
            final AvatarTranscoder avatarTranscoder,
            final UserManager userManager,
            final ApplicationProperties applicationProperties
    ) {
        this.avatarManager = avatarManager;
        this.avatarService = avatarService;
        this.avatarTranscoder = avatarTranscoder;
        this.userManager = userManager;
        this.applicationProperties = applicationProperties;
    }

    public MailAttachmentsManager createAttachmentsManager() {
        return new MailAttachmentsManagerImpl(avatarService, avatarTranscoder, userManager, avatarManager, applicationProperties);
    }
}
