package com.atlassian.jira.model.querydsl;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Generated;

/**
 * Data Transfer Object for the QRTZFiredTriggers entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 *
 * @see QQRTZFiredTriggers
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QRTZFiredTriggersDTO implements DTO {
    private final Long id;
    private final String entryId;
    private final Long trigger;
    private final String triggerListener;
    private final java.sql.Timestamp firedTime;
    private final String triggerState;

    public Long getId() {
        return id;
    }

    public String getEntryId() {
        return entryId;
    }

    public Long getTrigger() {
        return trigger;
    }

    public String getTriggerListener() {
        return triggerListener;
    }

    public java.sql.Timestamp getFiredTime() {
        return firedTime;
    }

    public String getTriggerState() {
        return triggerState;
    }

    public QRTZFiredTriggersDTO(Long id, String entryId, Long trigger, String triggerListener, java.sql.Timestamp firedTime, String triggerState) {
        this.id = id;
        this.entryId = entryId;
        this.trigger = trigger;
        this.triggerListener = triggerListener;
        this.firedTime = firedTime;
        this.triggerState = triggerState;
    }

    /**
     * Creates a GenericValue object from the values in this Data Transfer Object.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
     * @return a GenericValue object constructed from the values in this Data Transfer Object.
     */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator) {
        return ofBizDelegator.makeValue("QRTZFiredTriggers", new FieldMap()
                .add("id", id)
                .add("entryId", entryId)
                .add("trigger", trigger)
                .add("triggerListener", triggerListener)
                .add("firedTime", firedTime)
                .add("triggerState", triggerState)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */
    public static QRTZFiredTriggersDTO fromGenericValue(GenericValue gv) {
        return new QRTZFiredTriggersDTO(
                gv.getLong("id"),
                gv.getString("entryId"),
                gv.getLong("trigger"),
                gv.getString("triggerListener"),
                gv.getTimestamp("firedTime"),
                gv.getString("triggerState")
        );
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(QRTZFiredTriggersDTO qRTZFiredTriggersDTO) {
        return new Builder(qRTZFiredTriggersDTO);
    }

    public static class Builder {
        private Long id;
        private String entryId;
        private Long trigger;
        private String triggerListener;
        private java.sql.Timestamp firedTime;
        private String triggerState;

        public Builder() {
        }

        public Builder(QRTZFiredTriggersDTO qRTZFiredTriggersDTO) {
            this.id = qRTZFiredTriggersDTO.id;
            this.entryId = qRTZFiredTriggersDTO.entryId;
            this.trigger = qRTZFiredTriggersDTO.trigger;
            this.triggerListener = qRTZFiredTriggersDTO.triggerListener;
            this.firedTime = qRTZFiredTriggersDTO.firedTime;
            this.triggerState = qRTZFiredTriggersDTO.triggerState;
        }

        public QRTZFiredTriggersDTO build() {
            return new QRTZFiredTriggersDTO(id, entryId, trigger, triggerListener, firedTime, triggerState);
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }
        public Builder entryId(String entryId) {
            this.entryId = entryId;
            return this;
        }
        public Builder trigger(Long trigger) {
            this.trigger = trigger;
            return this;
        }
        public Builder triggerListener(String triggerListener) {
            this.triggerListener = triggerListener;
            return this;
        }
        public Builder firedTime(java.sql.Timestamp firedTime) {
            this.firedTime = firedTime;
            return this;
        }
        public Builder triggerState(String triggerState) {
            this.triggerState = triggerState;
            return this;
        }
    }
}