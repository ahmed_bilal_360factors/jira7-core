package com.atlassian.jira.dashboard;

import com.atlassian.fugue.Option;
import com.atlassian.gadgets.DashboardItemState;
import com.atlassian.gadgets.DashboardItemStateVisitor;
import com.atlassian.gadgets.GadgetId;
import com.atlassian.gadgets.GadgetState;
import com.atlassian.gadgets.LocalDashboardItemState;
import com.atlassian.gadgets.OpenSocialDashboardItemModuleId;
import com.atlassian.gadgets.dashboard.DashboardState;
import com.atlassian.gadgets.dashboard.spi.DashboardStateStoreException;
import com.atlassian.gadgets.dashboard.spi.GadgetLayout;
import com.atlassian.gadgets.dashboard.spi.changes.AddGadgetChange;
import com.atlassian.gadgets.dashboard.spi.changes.DashboardChange;
import com.atlassian.gadgets.dashboard.spi.changes.GadgetColorChange;
import com.atlassian.gadgets.dashboard.spi.changes.RemoveGadgetChange;
import com.atlassian.gadgets.dashboard.spi.changes.UpdateGadgetUserPrefsChange;
import com.atlassian.gadgets.dashboard.spi.changes.UpdateLayoutChange;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.portal.PortalPageStore;
import com.atlassian.jira.portal.PortletConfiguration;
import com.atlassian.jira.portal.PortletConfigurationImpl;
import com.atlassian.jira.portal.PortletConfigurationStore;
import com.atlassian.plugin.ModuleCompleteKey;
import com.google.common.base.Function;

import java.net.URI;
import java.util.Collections;

import static com.atlassian.fugue.Iterables.drop;
import static com.atlassian.jira.dashboard.DashboardUtil.toLong;

/**
 * Implements the Dashboard plugin DashboardChange.Visitor.  This class essentially handles fine grained update
 * operations of the dashboard, such that the entire dashboard state doesn't have to be rewritten to the database on
 * every update.
 *
 * @since v4.0
 */
class JiraDashboardChangeHandler implements DashboardChange.Visitor {
    private final DashboardState updateDashboardState;
    private final PortletConfigurationStore portletConfigurationStore;
    private final PortalPageStore portalPageStore;

    JiraDashboardChangeHandler(final DashboardState updateDashboardState,
                               final PortletConfigurationStore portletConfigurationStore,
                               final PortalPageStore portalPageStore) {
        this.updateDashboardState = updateDashboardState;
        this.portletConfigurationStore = portletConfigurationStore;
        this.portalPageStore = portalPageStore;
    }

    /**
     * This method can be used to submit an iterable of {@link com.atlassian.gadgets.dashboard.spi.changes.DashboardChange} to
     * be visited by 'this' visitor, rather than having to iterate over them manually.
     *
     * @param changes A list of dashboardchanges to visit by this visitor.
     */
    public void accept(Iterable<DashboardChange> changes) {
        for (DashboardChange change : changes) {
            change.accept(this);
        }
    }

    public void visit(final AddGadgetChange addGadgetChange) {
        final DashboardState.ColumnIndex column = addGadgetChange.getColumnIndex();
        final Iterable<DashboardItemState> dashboardItems = updateDashboardState.getDashboardColumns().getItemsInColumn(column);
        final DashboardItemState newItemState = addGadgetChange.getDashboardItemState();

        final Long portalPageId = toLong(updateDashboardState.getId());
        final PortletConfiguration pc = toPortletConfiguration(newItemState, portalPageId, column.index(), addGadgetChange.getRowIndex());

        portletConfigurationStore.addDashboardItem(pc.getDashboardPageId(), pc.getId(), pc.getColumn(), pc.getRow(),
                pc.getOpenSocialSpecUri(), pc.getColor(), pc.getUserPrefs(), pc.getCompleteModuleKey());

        moveDashboardItemsOnePositionDown(dashboardItems, addGadgetChange.getRowIndex() + 1, column.index());
    }

    public void visit(final GadgetColorChange gadgetColorChange) {
        final Long gadgetId = toLong(gadgetColorChange.getGadgetId());
        final PortletConfiguration portletConfiguration = portletConfigurationStore.getByPortletId(gadgetId);
        if (portletConfiguration != null) {
            portletConfigurationStore.updateGadgetColor(gadgetId, gadgetColorChange.getColor());
        } else {
            throw new DashboardStateStoreException("Gadget with id '" + gadgetColorChange.getGadgetId() + "' not found for color change.");
        }
    }

    public void visit(final RemoveGadgetChange removeGadgetChange) {
        final PortletConfiguration gadgetToRemove = portletConfigurationStore.getByPortletId(toLong(removeGadgetChange.getGadgetId()));
        final Iterable<DashboardItemState> gadgets = updateDashboardState.getDashboardColumns().getItemsInColumn(DashboardState.ColumnIndex.from(gadgetToRemove.getColumn()));
        portletConfigurationStore.delete(gadgetToRemove);
        int row = 0;
        for (DashboardItemState gadget : gadgets) {
            portletConfigurationStore.updateGadgetPosition(toLong(gadget.getId()), row, gadgetToRemove.getColumn(), toLong(updateDashboardState.getId()));
            row++;
        }
    }

    public void visit(final UpdateGadgetUserPrefsChange updateGadgetUserPrefsChange) {
        portletConfigurationStore.updateUserPrefs(toLong(updateGadgetUserPrefsChange.getGadgetId()), updateGadgetUserPrefsChange.getPrefValues());
    }

    public void visit(final UpdateLayoutChange updateLayoutChange) {
        final Long dashboardId = toLong(updateDashboardState.getId());
        final PortalPage portalPage = portalPageStore.getPortalPage(dashboardId);
        if (!portalPage.getLayout().equals(updateLayoutChange.getLayout())) {
            portalPageStore.update(PortalPage.portalPage(portalPage).layout(updateLayoutChange.getLayout()).build());
        }

        final GadgetLayout newLayout = updateLayoutChange.getGadgetLayout();
        for (int column = 0; column < newLayout.getNumberOfColumns(); column++) {
            int row = 0;
            final Iterable<GadgetId> gadgets = newLayout.getGadgetsInColumn(column);
            for (GadgetId gadgetId : gadgets) {
                final Long gadgetIdLong = toLong(gadgetId);
                final PortletConfiguration portletConfiguration = portletConfigurationStore.getByPortletId(gadgetIdLong);
                if (portletConfiguration.getColumn() != column || portletConfiguration.getRow() != row) {
                    portletConfigurationStore.updateGadgetPosition(gadgetIdLong, row, column, dashboardId);
                }
                row += 1;
            }
        }
    }

    private PortletConfiguration toPortletConfiguration(final DashboardItemState dashboardItemState, final Long page, final Integer column, final Integer row) {
        final long itemId = toLong(dashboardItemState.getId());
        return dashboardItemState.accept(new DashboardItemStateVisitor<PortletConfiguration>() {
            @Override
            public PortletConfiguration visit(final GadgetState state) {
                return new PortletConfigurationImpl(itemId, page, column, row, Option.some(state.getGadgetSpecUri()),
                        state.getColor(), state.getUserPrefs(), Option.<ModuleCompleteKey>none());
            }

            @Override
            public PortletConfiguration visit(final LocalDashboardItemState state) {
                final Option<URI> openSocialSpecUri = state.getDashboardItemModuleId().getReplacedGadgetId().map(new Function<OpenSocialDashboardItemModuleId, URI>() {
                    @Override
                    public URI apply(final OpenSocialDashboardItemModuleId input) {
                        return input.getSpecUri();
                    }
                });
                final Option<ModuleCompleteKey> moduleKey = Option.some(state.getDashboardItemModuleId().getFullModuleKey());
                return new PortletConfigurationImpl(itemId, page, column, row, openSocialSpecUri,
                        state.getColor(), Collections.<String, String>emptyMap(), moduleKey);
            }
        });
    }

    /**
     * Moves all items one position down.
     *
     * @param dashboardItems dashboard items which are moved one position down.
     * @param firstNewRow    the first row, starting from which the items are moved down.
     * @param column         in which the items are going to be moved.
     */
    private void moveDashboardItemsOnePositionDown(final Iterable<DashboardItemState> dashboardItems,
                                                   final int firstNewRow,
                                                   final int column) {
        final Long dashboardId = toLong(updateDashboardState.getId());
        final Iterable<DashboardItemState> itemsBelowNewItem = drop(firstNewRow, dashboardItems);

        int row = firstNewRow;
        for (DashboardItemState dashboardItemState : itemsBelowNewItem) {
            portletConfigurationStore.updateGadgetPosition(toLong(dashboardItemState.getId()), row, column, dashboardId);
            row += 1;
        }
    }
}