package com.atlassian.jira.issue.managers;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.event.issue.field.CustomFieldCreatedEvent;
import com.atlassian.jira.event.issue.field.CustomFieldUpdatedEvent;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comparator.CustomFieldComparators;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.context.persistence.FieldConfigContextPersister;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.CustomFieldFactory;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.config.persistence.FieldConfigPersister;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.index.managers.FieldIndexerManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.managers.IssueSearcherManager;
import com.atlassian.jira.model.querydsl.CustomFieldDTO;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.plugin.customfield.CustomFieldSearcherModuleDescriptor;
import com.atlassian.jira.plugin.customfield.CustomFieldSearcherModuleDescriptors;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptors;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.ObjectUtils;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static com.atlassian.jira.model.querydsl.QColumnLayoutItem.COLUMN_LAYOUT_ITEM;
import static com.atlassian.jira.model.querydsl.QCustomField.CUSTOM_FIELD;
import static com.atlassian.jira.model.querydsl.QCustomFieldValue.CUSTOM_FIELD_VALUE;
import static com.atlassian.jira.model.querydsl.QFieldLayoutItem.FIELD_LAYOUT_ITEM;
import static com.atlassian.util.concurrent.Assertions.notNull;
import static io.atlassian.fugue.Option.option;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

public class CachingCustomFieldManager implements CustomFieldManager {
    private static final Logger log = LoggerFactory.getLogger(CachingCustomFieldManager.class);
    private static final String ID = "id";

    private final PluginAccessor pluginAccessor;
    private final QueryDslAccessor dbConnectionManager;
    private final FieldConfigSchemeManager fieldConfigSchemeManager;
    private final ConstantsManager constantsManager;
    private final ProjectManager projectManager;
    private final FieldConfigContextPersister contextPersister;
    private final FieldScreenManager fieldScreenManager;
    private final CustomFieldValuePersister customFieldValuePersister;
    private final NotificationSchemeManager notificationSchemeManager;
    private final FieldManager fieldManager;
    private final EventPublisher eventPublisher;
    private final CustomFieldFactory customFieldFactory;
    private final CustomFieldTypeModuleDescriptors customFieldTypeModuleDescriptors;
    private final CustomFieldSearcherModuleDescriptors customFieldSearcherModuleDescriptors;
    private final CustomFieldSearcherManager customFieldSearcherManager;

    private final CachedReference<CustomFieldInMemoryStore> cache;

    public CachingCustomFieldManager(
            final PluginAccessor pluginAccessor,
            final QueryDslAccessor dbConnectionManager,
            final FieldConfigSchemeManager fieldConfigSchemeManager,
            final ConstantsManager constantsManager,
            final ProjectManager projectManager,
            final FieldConfigContextPersister contextPersister,
            final FieldScreenManager fieldScreenManager,
            final CustomFieldValuePersister customFieldValuePersister,
            final NotificationSchemeManager notificationSchemeManager,
            final FieldManager fieldManager,
            final EventPublisher eventPublisher,
            final CustomFieldFactory customFieldFactory,
            final CustomFieldTypeModuleDescriptors customFieldTypeModuleDescriptors,
            final CustomFieldSearcherModuleDescriptors customFieldSearcherModuleDescriptors,
            final CustomFieldSearcherManager customFieldSearcherManager,
            final CacheManager cacheManager) {
        this.pluginAccessor = pluginAccessor;
        this.dbConnectionManager = dbConnectionManager;
        this.fieldConfigSchemeManager = fieldConfigSchemeManager;
        this.constantsManager = constantsManager;
        this.projectManager = projectManager;
        this.contextPersister = contextPersister;
        this.fieldScreenManager = fieldScreenManager;
        this.customFieldValuePersister = customFieldValuePersister;
        this.notificationSchemeManager = notificationSchemeManager;
        this.fieldManager = fieldManager;
        this.eventPublisher = eventPublisher;
        this.customFieldFactory = customFieldFactory;
        this.customFieldTypeModuleDescriptors = customFieldTypeModuleDescriptors;
        this.customFieldSearcherModuleDescriptors = customFieldSearcherModuleDescriptors;
        this.customFieldSearcherManager = customFieldSearcherManager;

        this.cache =  cacheManager.getCachedReference(getClass().getName() + ".cache",
                this::loadAllCustomFields);

        eventPublisher.register(this);
    }

    private CustomFieldInMemoryStore loadAllCustomFields() {
        return new CustomFieldInMemoryStore(dbConnectionManager.withNewConnection().executeQuery(callback ->
                callback.newSqlQuery()
                        .select(CUSTOM_FIELD)
                        .from(CUSTOM_FIELD)
                        .fetch())
                .stream()
                .map(customFieldFactory::create)
                // Don't add if the customfield is invalid (check type later, to avoid calling it too early)
                .filter(Objects::nonNull)
                .collect(toList()));
    }

    @Override
    public CustomField createCustomField(final String fieldName, final String description, final CustomFieldType fieldType,
                                         final CustomFieldSearcher customFieldSearcher, final List<JiraContextNode> contexts, final List<IssueType> issueTypes) throws GenericEntityException {
        final String customFieldName = StringUtils.abbreviate(fieldName, FieldConfigPersister.ENTITY_LONG_TEXT_LENGTH);
        final Optional<String> customFieldDescription = ofNullable(description)
                .filter(s -> !s.isEmpty());
        final Optional<String> customFieldSearcherKey = ofNullable(customFieldSearcher)
                .map(searcher -> customFieldSearcher.getDescriptor().getCompleteKey());

        final CustomFieldDTO initialCustomField = CustomFieldDTO.builder()
                .name(customFieldName)
                .customfieldtypekey(fieldType.getKey())
                .description(customFieldDescription.orElse(null))
                .customfieldsearcherkey(customFieldSearcherKey.orElse(null))
                .build();

        // We need to have an id for the CustomFieldDTO first before we can create a CustomField in the factory
        final Long createdCustomFieldId = dbConnectionManager.withNewConnection().executeQuery(connection -> connection
                .insert(CUSTOM_FIELD)
                .populate(initialCustomField)
                .executeWithId());

        // We can use the original CustomFieldDTO object to create a new one with the id added to insert
        final CustomFieldDTO customFieldToInsert = CustomFieldDTO.builder(initialCustomField)
                .id(createdCustomFieldId)
                .build();

        final CustomField customField = customFieldFactory.create(customFieldToInsert);

        associateCustomFieldContext(customField, contexts, issueTypes);

        refreshConfiguration();

        // The custom field created event should be the last thing that's fired to make sure everything in the creation
        // of the custom field is complete and the custom field is ready to be used.
        eventPublisher.publish(new CustomFieldCreatedEvent(customField));

        return getCustomFieldObject(createdCustomFieldId);
    }

    private void associateCustomFieldContext(final CustomField customField, final List<JiraContextNode> contexts, final List<IssueType> issueTypes) {
        if (CollectionUtils.isNotEmpty(contexts)) {
            fieldConfigSchemeManager.createDefaultScheme(customField, contexts, issueTypes);
        }
    }

    @Override
    @Nonnull
    public List<CustomFieldType<?, ?>> getCustomFieldTypes() {
        return customFieldTypeModuleDescriptors.getCustomFieldTypes();
    }

    @Override
    public CustomFieldType getCustomFieldType(final String key) {
        return customFieldTypeModuleDescriptors.getCustomFieldType(key).getOrNull();
    }

    @Override
    @Nonnull
    public List<CustomFieldSearcher> getCustomFieldSearchers(final CustomFieldType customFieldType) {
        return customFieldSearcherManager.getSearchersValidFor(customFieldType);
    }

    @Override
    public CustomFieldSearcher getCustomFieldSearcher(final String key) {
        return customFieldSearcherModuleDescriptors.getCustomFieldSearcher(key).getOrNull();
    }

    @Nullable
    @Override
    public CustomFieldSearcher getDefaultSearcher(@Nonnull final CustomFieldType<?, ?> type) {
        // noinspection ConstantConditions
        return getCustomFieldSearchers(notNull("type", type)).stream().findFirst().orElse(null);
    }

    @Override
    public Class<? extends CustomFieldSearcher> getCustomFieldSearcherClass(final String key) {
        if (!ObjectUtils.isValueSelected(key)) {
            return null;
        }

        final ModuleDescriptor<?> module = pluginAccessor.getEnabledPluginModule(key);

        if (module instanceof CustomFieldSearcherModuleDescriptor) {
            return ((CustomFieldSearcherModuleDescriptor) module).getModuleClass();
        } else {
            log.warn("Custom field searcher module: " + key + " is invalid. Null being returned.");
            return null;
        }
    }

    /**
     * Get all {@link CustomField}s in scope for this issue's project/type.
     */
    @Override
    public List<CustomField> getCustomFieldObjects(final Issue issue) {
        return getCustomFieldObjects(issue.getProjectId(), issue.getIssueTypeId());
    }

    /**
     * @deprecated Use {@link #getCustomFieldObjects(com.atlassian.jira.issue.Issue)}
     */
    @Override
    public List<CustomField> getCustomFieldObjects(final GenericValue issue) {
        return getCustomFieldObjects(issue.getLong("project"), issue.getString("type"));
    }

    @Override
    public List<CustomField> getCustomFieldObjects(final Long projectId, final String issueTypeId) {
        return getCustomFieldObjects(projectId, issueTypeId == null ? null : Lists.newArrayList(issueTypeId));
    }

    @Override
    public List<CustomField> getCustomFieldObjects(final Long projectId, final List<String> issueTypeIds) {
        final Project project = projectManager.getProjectObj(projectId);
        final List<String> expandedIssueTypeIds = constantsManager.expandIssueTypeIds(issueTypeIds);

        return getCustomFieldObjects().stream()
                .filter(customField -> customField.isInScopeForSearch(project, expandedIssueTypeIds))
                .collect(toList());
    }

    @Override
    public List<CustomField> getCustomFieldObjects(final SearchContext searchContext) {
        return getCustomFieldObjects().stream()
                .filter(customField -> customField.isInScope(searchContext))
                .collect(toList());
    }

    @Override
    @Nullable
    public CustomField getCustomFieldObject(final Long id) {
        return cache.get().get(id);
    }

    private Optional<CustomField> getCustomFieldOptional(final Long id) {
        return Optional.ofNullable(getCustomFieldObject(id));
    }

    @Override
    @Nullable
    public CustomField getCustomFieldObject(final String key) {
        final Optional<Long> id = ofNullable(CustomFieldUtils.getCustomFieldId(key));
        return id.map(this::getCustomFieldObject).orElse(null);
    }

    @Override
    public boolean isCustomField(final String id) {
        return id != null && id.startsWith(CustomFieldUtils.CUSTOM_FIELD_PREFIX);
    }

    @Override
    public boolean exists(final String key) {
        return getCustomFieldObject(key) != null;
    }

    @Override
    @Nullable
    public CustomField getCustomFieldObjectByName(final String customFieldName) {
        final Collection<CustomField> values = getCustomFieldObjectsByName(customFieldName);

        // Should have called getCustomFieldObjectsByName instead?
        if (values.size() > 1) {
            logReturningFirstCustomFieldWarning(values.size(), customFieldName);
        }

        return values.stream()
                .findFirst()
                .orElse(null);
    }

    /**
     * Only dump the stack trace if debug logging is enabled - otherwise the log file can get full of rubbish.
     * Some 3rd party plugins are known to call this a lot.
     */
    private void logReturningFirstCustomFieldWarning(final int size, final String customFieldName) {
        final String msg = "Warning: returning 1 of " + size + " custom fields named '" + customFieldName + '\'';
        if (log.isDebugEnabled()) {
            log.warn(msg, new Throwable());
        } else {
            log.warn(msg);
        }
    }

    @Override
    public Collection<CustomField> getCustomFieldObjectsByName(final String customFieldName) {
        return cache.get().getAllByName(customFieldName);
    }

    @Override
    public List<CustomField> getCustomFieldObjects() {
        return cache.get().getAllSorted();
    }

    @Override
    public List<CustomField> getGlobalCustomFieldObjects() {
        return getCustomFieldObjects().stream()
                .filter(CustomField::isGlobal)
                .collect(toList());
    }

    @Override
    public void refresh() {
        refreshConfigurationSchemes(null);
        refreshSearchersAndIndexers();
    }

    @Override
    public void refreshConfigurationSchemes(final Long customFieldId) {
        fieldConfigSchemeManager.init();
        cache.reset();
    }

    public void refreshConfiguration() {
        fieldManager.refresh();
        cache.reset();
    }

    private void refreshSearchersAndIndexers() {
        // This must be statically called since otherwise a cyclic dependency will occur.
        // TODO There really needs to be a CacheManager that handles all these dependent caches.
        final IssueSearcherManager issueSearcherManager = ComponentAccessor.getComponent(IssueSearcherManager.class);
        issueSearcherManager.refresh();

        final FieldIndexerManager fieldIndexerManager = ComponentAccessor.getComponent(FieldIndexerManager.class);
        fieldIndexerManager.refresh();
    }

    @Override
    public void clear() {
        cache.reset();
    }

    @Override
    public void removeCustomFieldPossiblyLeavingOrphanedData(final Long customFieldId) throws RemoveException {
        final CustomField originalCustomField = getCustomFieldObject(notNull(ID, customFieldId));
        if (originalCustomField != null) {
            removeCustomField(originalCustomField);
        } else {
            removeCustomFieldDirectlyFromDb(customFieldId);
        }
    }

    private void removeCustomFieldDirectlyFromDb(final Long customFieldId) throws RemoveException {
        log.debug("Couldn't load customfield object for id '" + customFieldId + "'.  Trying to lookup field directly via the db."
                + "  Please note that deleting a custom field this way may leave some custom field data behind.");

        ofNullable(
                dbConnectionManager.withNewConnection().executeQuery(callback ->
                        callback.newSqlQuery()
                                .from(CUSTOM_FIELD)
                                .select(CUSTOM_FIELD)
                                .where(CUSTOM_FIELD.id.eq(customFieldId))
                                .limit(1)
                                .fetchOne())
        ).orElseThrow(() -> new IllegalArgumentException("Tried to remove custom field with id '" + customFieldId + "' that doesn't exist!"));

        log.debug("Customfield with id '" + customFieldId + "' retrieved successfully via the db.");

        final String customFieldStringId = FieldManager.CUSTOM_FIELD_PREFIX + customFieldId;
        removeCustomFieldAssociations(customFieldStringId);

        customFieldValuePersister.removeAllValues(customFieldStringId);

        dbConnectionManager.withNewConnection().execute(connection -> connection
                .delete(CUSTOM_FIELD)
                .where(CUSTOM_FIELD.id.eq(customFieldId))
                .execute());

        // It's not in the manager, no need to refresh the cache.
        fieldManager.refresh();
    }

    @Override
    public void removeCustomField(final CustomField customField) throws RemoveException {
        removeCustomFieldAssociations(customField.getId());
        // noinspection deprecation
        customField.remove();
        refreshConfiguration();
    }

    private void removeCustomFieldAssociations(final String customFieldId) throws RemoveException {
        // Remove and field screen layout items of this custom field.
        fieldScreenManager.removeFieldScreenItems(customFieldId);

        dbConnectionManager.withNewConnection().execute(connection -> {
                    connection
                            .delete(COLUMN_LAYOUT_ITEM)
                            .where(COLUMN_LAYOUT_ITEM.fieldidentifier.eq(customFieldId))
                            .execute();

                    connection
                            .delete(FIELD_LAYOUT_ITEM)
                            .where(FIELD_LAYOUT_ITEM.fieldidentifier.eq(customFieldId))
                            .execute();
                }
        );

        fieldConfigSchemeManager.removeInvalidFieldConfigSchemesForCustomField(customFieldId);

        // This should be triggered via an event system but until then is done explicitly.
        notificationSchemeManager.removeSchemeEntitiesForField(customFieldId);
    }

    @Override
    public void removeCustomFieldValues(final GenericValue issue) throws GenericEntityException {
        dbConnectionManager.withNewConnection().execute(connection -> connection
                .delete(CUSTOM_FIELD_VALUE)
                .where(CUSTOM_FIELD_VALUE.issue.eq(issue.getLong(ID)))
                .execute()
        );
    }

    @Override
    public void updateCustomField(final Long id, final String name, final String description, final CustomFieldSearcher searcher) {
        // Get a copy of the old value - we will use this for the event we publish and clearing caches.
        final CustomField oldCustomField = getCustomFieldOptional(id)
                .orElseThrow(() -> new DataAccessException("Cannot update custom field that does not exist"));

        final String searcherKey = ofNullable(searcher)
                .map(s -> s.getDescriptor().getCompleteKey())
                .orElse(null);

        dbConnectionManager.withNewConnection().execute(connection -> connection
                .update(CUSTOM_FIELD)
                .set(CUSTOM_FIELD.name, name)
                .set(CUSTOM_FIELD.description, description)
                .set(CUSTOM_FIELD.customfieldsearcherkey, searcherKey)
                .where(CUSTOM_FIELD.id.eq(id))
                .execute());

        cache.reset();

        final CustomField newCustomField = getCustomFieldObject(id);

        // New CustomField was just created.
        // noinspection ConstantConditions
        eventPublisher.publish(new CustomFieldUpdatedEvent(newCustomField, oldCustomField));
        if (!areConfigSchemesEqual(newCustomField.getConfigurationSchemes(), oldCustomField.getConfigurationSchemes())) {
            fieldManager.refresh();
        } else if (searcher != oldCustomField.getCustomFieldSearcher()) {
            //we assume that fieldManager.refresh() also refreshes searchers and indexers, so we don't need to do it twice
            refreshSearchersAndIndexers();
        }
    }

    protected boolean areConfigSchemesEqual(final List<FieldConfigScheme> schemes, final List<FieldConfigScheme> otherSchemes) {
        if (schemes != null && otherSchemes != null) {
            return HashMultiset.create(schemes).equals(HashMultiset.create(otherSchemes));
        }
        return schemes == null && otherSchemes == null;
    }

    @Override
    public CustomField getCustomFieldInstance(final GenericValue customFieldGv) {
        return customFieldFactory.create(customFieldGv);
    }

    @Override
    public void removeProjectAssociations(final Project project) {
        contextPersister.removeContextsForProject(project);
        refresh();
    }

    @EventListener
    public void onClearCache(final ClearCacheEvent event) {
        refresh();
    }

    private final class CustomFieldInMemoryStore {
        private final Map<Long, CustomField> byId;
        private final Multimap<String, CustomField> byName;
        private final List<CustomField> sortedFields;

        // The valid CustomFieldType check was previously being done in loadAllCustomFields, however if the plugins
        // that should provide the module are not yet all started when the cache is built, they will be filtered out
        // of the collection passed into this constructor, and hence be regarded as not existing.
        // Instead delay those checks until they are actually requested, as the getCustomFieldType is also cached
        // on the Custom Field side
        private CustomFieldInMemoryStore(final Collection<CustomField> customFields) {
            byId = Maps.uniqueIndex(customFields, CustomField::getIdAsLong);
            byName = Multimaps.index(customFields, CustomField::getName);

            sortedFields = byId.values().stream()
                    .sorted(CustomFieldComparators.byName())
                    .collect(toImmutableList());
        }

        @Nullable
        private CustomField get(final Long id) {
            return option(byId.get(id))
                    .filter(customField -> customField.getCustomFieldType() != null)
                    .getOrNull();
        }

        @Nonnull
        private Collection<CustomField> getAllByName(final String name) {
            // Multimap.get() returns empty collection, not null.
            return byName.get(name)
                    .stream()
                    .filter(customField -> customField.getCustomFieldType() != null)
                    .collect(toImmutableList());
        }

        @Nonnull
        private List<CustomField> getAllSorted() {
            return sortedFields
                    .stream()
                    .filter(customField -> customField.getCustomFieldType() != null)
                    .collect(toImmutableList());
        }

    }
}
