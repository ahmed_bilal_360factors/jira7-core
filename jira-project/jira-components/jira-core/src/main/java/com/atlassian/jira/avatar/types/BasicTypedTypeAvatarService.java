package com.atlassian.jira.avatar.types;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarImageDataProvider;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.SystemAndCustomAvatars;
import com.atlassian.jira.avatar.TypeAvatarService;
import com.atlassian.jira.icon.IconOwningObjectId;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * To be honest, you don't need this class very much if you already have the {@link IconType} and the
 * {@link AvatarManager}.
 */
public class BasicTypedTypeAvatarService implements TypeAvatarService {
    private final IconType iconType;
    private final AvatarManager avatarManager;

    public BasicTypedTypeAvatarService(final IconType iconType, final AvatarManager avatarManager) {
        this.iconType = iconType;
        this.avatarManager = avatarManager;
    }

    @Override
    final public SystemAndCustomAvatars getAvatars(final ApplicationUser remoteUser, final String owningObjectId) {
        final List<Avatar> systemAvatars = avatarManager.getAllSystemAvatars(iconType);
        final List<Avatar> avatarsForOwner = avatarManager.getCustomAvatarsForOwner(iconType, owningObjectId);

        return new SystemAndCustomAvatars(systemAvatars, avatarsForOwner);
    }

    @Override
    public Avatar getAvatar(final ApplicationUser remoteUser, final long avatarId) {
        final Avatar currentAvatar = avatarManager.getById(avatarId);
        final boolean userCanViewAvatar =
                currentAvatar != null ?
                        avatarManager.userCanView(remoteUser, currentAvatar) :
                        false;

        return userCanViewAvatar ? currentAvatar : null;
    }

    @Override
    public boolean canUserCreateAvatar(final ApplicationUser remoteUser, final String owningObjectId) {
        return avatarManager.userCanCreateFor(remoteUser, iconType, new IconOwningObjectId(owningObjectId));
    }

    @Override
    public Avatar createAvatar(final ApplicationUser remoteUser, final String owningObjectId, final AvatarImageDataProvider imageDataProvider)
            throws IllegalAccessException, IOException {
        if (!avatarManager.userCanCreateFor(remoteUser, iconType, new IconOwningObjectId(owningObjectId))) {
            throw new IllegalAccessException();
        }
        return avatarManager.create(this.iconType, new IconOwningObjectId(owningObjectId), imageDataProvider);
    }

    @Nonnull
    @Override
    public Avatar getDefaultAvatar() {
        final Long defaultAvatarId = avatarManager.getDefaultAvatarId(iconType);
        if (null == defaultAvatarId) {
            throw new NoSuchElementException("No default avatar id for " + iconType);
        }
        final Avatar avatar = avatarManager.getById(defaultAvatarId);
        if (null == avatar) {
            throw new NoSuchElementException("No default avatar for " + iconType);
        }
        return avatar;
    }
}
