package com.atlassian.jira.cluster.zdu.analytics;

import com.atlassian.analytics.api.annotations.EventName;

/**
 * @since v7.3
 */
@EventName("zdu.node-state.down")
public class NodeStoppedAnalytics {
    /**
     * This can be identifiable to the customer - make sure it's hashed!
     */
    private final String nodeId;
    private final Integer databaseBuildNumber;
    private final Long nodeBuildNumber;
    private final String clusterState;

    public NodeStoppedAnalytics(String nodeId, Integer databaseBuildNumber, Long nodeBuildNumber, String clusterState) {
        this.nodeId = nodeId;
        this.databaseBuildNumber = databaseBuildNumber;
        this.nodeBuildNumber = nodeBuildNumber;
        this.clusterState = clusterState;
    }

    public String getNodeId() {
        return nodeId;
    }

    public Integer getDatabaseBuildNumber() {
        return databaseBuildNumber;
    }

    public Long getNodeBuildNumber() {
        return nodeBuildNumber;
    }

    public String getClusterState() {
        return clusterState;
    }
}
