package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.model.DirectoryEntity;

import javax.annotation.Nonnull;
import java.io.Serializable;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

/**
 * A composite Key of directoryId and name that is used to cache Users and Groups.
 */
final class DirectoryEntityKey implements Serializable {
    @Nonnull
    public static DirectoryEntityKey getKeyPreserveCase(long directoryId, String name) {
        return new DirectoryEntityKey(directoryId, name);
    }

    @Nonnull
    public static DirectoryEntityKey getKeyLowerCase(long directoryId, String name) {
        return new DirectoryEntityKey(directoryId, toLowerCase(name));
    }

    @Nonnull
    public static DirectoryEntityKey getKeyLowerCase(@Nonnull DirectoryEntity entity) {
        if (entity instanceof UserOrGroupStub) {
            return getKeyFor((UserOrGroupStub) entity);
        }
        return new DirectoryEntityKey(entity.getDirectoryId(), toLowerCase(entity.getName()));
    }

    public static DirectoryEntityKey getKeyFor(@Nonnull UserOrGroupStub stub) {
        return new DirectoryEntityKey(stub.getDirectoryId(), stub.getLowerName());
    }

    private final long directoryId;
    private final
    @Nonnull
    String name;

    private DirectoryEntityKey(long directoryId, @Nonnull String name) {
        this.directoryId = directoryId;
        this.name = name;
    }

    public long getDirectoryId() {
        return directoryId;
    }

    @Nonnull
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof DirectoryEntityKey && equals((DirectoryEntityKey) o));
    }

    private boolean equals(@Nonnull final DirectoryEntityKey other) {
        return directoryId == other.directoryId && name.equals(other.name);
    }

    @Override
    public int hashCode() {
        int hash = (int) (directoryId ^ (directoryId >>> 32));
        return 31 * hash + name.hashCode();
    }

    @Override
    public String toString() {
        return "{" + directoryId + ',' + name + '}';
    }
}
