package com.atlassian.jira.io;

import com.atlassian.fugue.Option;
import com.atlassian.plugin.Plugin;

import javax.annotation.ParametersAreNonnullByDefault;
import java.io.InputStream;
import java.net.URL;

/**
 * Resources Loader that is based on class loader.
 *
 * @since v6.5
 */
@ParametersAreNonnullByDefault
public class ClassPathResourceLoader implements ResourceLoader {
    @Override
    public Option<InputStream> getResourceAsStream(final String resourcePath) {
        return Option.option(ClassPathResourceLoader.class.getResourceAsStream(resourcePath));
    }

    @Override
    public Option<URL> getResource(final String resourcePath) {
        return Option.option(ClassPathResourceLoader.class.getResource(resourcePath));
    }

    @Override
    public Option<InputStream> getResourceAsStream(final Plugin plugin, final String resourcePath) {
        return Option.option(plugin.getResourceAsStream(resourcePath));
    }

    @Override
    public Option<URL> getResource(final Plugin plugin, final String resourcePath) {
        return Option.option(plugin.getResource(resourcePath));
    }
}
