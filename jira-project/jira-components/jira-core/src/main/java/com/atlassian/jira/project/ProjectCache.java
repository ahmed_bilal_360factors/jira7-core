package com.atlassian.jira.project;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.association.NodeAssocationType;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.comparator.OfBizComparators;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.util.ProjectKeyStore;
import com.atlassian.jira.util.Named;
import com.atlassian.util.profiling.UtilTimerStack;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.sort;

/**
 * This is a very basic cache that stores projects and project categories
 * <p>
 * When constructed, or when you call refresh() - it will find and cache all projects and project categories
 */
public class ProjectCache {
    private static final Logger LOG = LoggerFactory.getLogger(ProjectCache.class);

    private final OfBizDelegator delegator = ComponentAccessor.getOfBizDelegator();
    private final ProjectManager projectManager;
    private final NodeAssociationStore nodeAssociationStore;

    private final ProjectKeyStore projectKeyStore;
    // maps of projects by ID and key
    private volatile ImmutableMap<Long, Project> projectsById;
    private volatile ImmutableMap<String, Project> projectsByCurrentKey;

    private volatile ImmutableSortedMap<String, Project> projectsByCurrentKeyIgnoreCase;

    // list of projectCategories
    private volatile ImmutableMap<Long, ProjectCategory> projectCategories;

    /**
     * Map of Project ID to Project Category ID
     */
    private volatile ImmutableMap<Long, Long> projectIdToProjectCategoryId;

    // Map of projectCategory ID to List of Project IDs.
    private volatile ImmutableMap<Long, List<Long>> projectCategoriesToProjects;

    private volatile ImmutableList<Long> projectsWithNoCategory;

    // List of all Projects
    private volatile ImmutableList<Project> allProjectObjects;

    public ProjectCache(ProjectManager projectManager, ProjectKeyStore projectKeyStore, final NodeAssociationStore nodeAssociationStore) {
        this.projectManager = projectManager;
        this.projectKeyStore = projectKeyStore;
        this.nodeAssociationStore = nodeAssociationStore;
        init();
    }

    private void init() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("ProjectCache.refresh");
        }

        final long start = System.currentTimeMillis();
        UtilTimerStack.push("ProjectCache.refresh");
        try {
            refreshProjectList();
            refreshProjectCategories();
            refreshCategoryProjectMappings(projectsById);
            refreshProjectsWithNoCategory();
        } finally {
            UtilTimerStack.pop("ProjectCache.refresh");
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("ProjectCache.refresh took " + (System.currentTimeMillis() - start));
        }
    }

    /**
     * Refresh the list of projects
     * <p>
     * IMPACT: Should perform only one SQL select statement
     */
    private void refreshProjectList() {
        final List<Project> dbProjects = projectManager.getProjectObjects();

        final Map<Long, Project> tmpById = Maps.newLinkedHashMap();
        final Map<String, Project> tmpByCurrentKey = Maps.newLinkedHashMap();

        for (Project project : dbProjects) {
            // cache by ID
            tmpById.put(project.getId(), project);
            // cache by Project Key
            tmpByCurrentKey.put(project.getKey(), project);
        }

        // the caches are immutable, we recreate from scratch on refresh.
        projectsById = ImmutableMap.copyOf(tmpById);
        projectsByCurrentKey = ImmutableMap.copyOf(tmpByCurrentKey);
        projectsByCurrentKeyIgnoreCase = ImmutableSortedMap.copyOf(projectsByCurrentKey, String.CASE_INSENSITIVE_ORDER);
        allProjectObjects = ImmutableList.copyOf(dbProjects);
    }

    protected void refreshProjectCategories() {
        List<GenericValue> dbCategories = delegator.findAll("ProjectCategory");
        sort(dbCategories, OfBizComparators.NAME_COMPARATOR);
        Map<Long, ProjectCategory> tmpById = new LinkedHashMap<>(dbCategories.size() * 2);
        for (final GenericValue projectCategory : dbCategories) {
            tmpById.put(projectCategory.getLong("id"), Entity.PROJECT_CATEGORY.build(projectCategory));
        }

        projectCategories = ImmutableMap.copyOf(tmpById);
    }

    // PROJECT CACHING --------------------------------
    public Project getProject(Long id) {
        return projectsById.get(id);
    }

    @Nullable
    public Project getProjectByName(String name) {
        for (final Project project : getProjects()) {
            if (project.getName().equalsIgnoreCase(name)) {
                return project;
            }
        }

        return null;
    }

    @Nullable
    public Project getProjectByKey(String key) {
        // JDEV-24899 we want to avoid potential NPE if ProjectKey table contains keys referring to non-existent projects
        final Long projectId = projectKeyStore.getProjectId(key);
        return projectId != null ? getProject(projectId) : null;
    }

    public Collection<Project> getProjects() {
        return projectsById.values();
    }

    /**
     * Returns a list of all Projects ordered by name.
     *
     * @return a list of all Projects ordered by name.
     */
    public List<Project> getProjectObjects() {
        return allProjectObjects;
    }

    public Collection<ProjectCategory> getProjectCategories() {
        return projectCategories.values();
    }

    @Nullable
    public ProjectCategory getProjectCategory(Long id) {
        return projectCategories.get(id);
    }

    private void refreshCategoryProjectMappings(final Map<Long, Project> projectsById) {
        final Collection<ProjectCategory> categories = getProjectCategories();
        final Map<Long, List<Long>> tmpProjectCategoriesToProjects = Maps.newHashMapWithExpectedSize(categories.size());
        final Map<Long, Long> tmpProjectIdToProjectCategoryId = new HashMap<>(64);
        for (final ProjectCategory category : categories) {
            try {
                final List<Long> projectIds = nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.PROJECT_TO_CATEGORY, category.getId());

                // Sort the projects by name - the entire cache is rebuilt on project rename :/
                final List<Long> sortedProjectIds = new ArrayList<>(projectIds.size());
                projectIds.stream()
                        // get project object from map
                        .map(projectsById::get)
                                // sort by name
                        .sorted(Named.NAME_COMPARATOR)
                                // get the ID back out :)
                        .forEach(project -> sortedProjectIds.add(project.getId()));

                tmpProjectCategoriesToProjects.put(category.getId(), sortedProjectIds);
                for (final Long projectId : projectIds) {
                    tmpProjectIdToProjectCategoryId.put(projectId, category.getId());
                }
            } catch (DataAccessException ex) {
                LOG.error("Error getting projects for category " + category + ": " + ex, ex); //TODO: What should this really do?
            }
        }

        // Cached values are Immutable. We overwrite with a new cache on update.
        projectIdToProjectCategoryId = ImmutableMap.copyOf(tmpProjectIdToProjectCategoryId);
        projectCategoriesToProjects = ImmutableMap.copyOf(tmpProjectCategoriesToProjects);
    }

    private static List<Long> getIdsFromProjects(Collection<Project> projectObjects) {
        if (projectObjects == null) {
            return Collections.emptyList();
        }
        return newArrayList(transform(projectObjects, new Function<Project, Long>() {
            @Nullable
            @Override
            public Long apply(@Nullable final Project project) {
                return (project != null) ? project.getId() : null;
            }
        }));
    }

    private List<Project> getProjectsFromProjectIds(Collection<Long> projectIds) {
        if (projectIds == null) {
            return Collections.emptyList();
        }
        return projectIds.stream().map(this::getProject).collect(CollectorsUtil.toNewArrayListWithSizeOf(projectIds));
    }

    public Collection<Project> getProjectsFromProjectCategory(Long projectCategoryId) {
        if (projectCategoryId != null) {
            final List<Long> projectIds = projectCategoriesToProjects.get(projectCategoryId);
            if (projectIds != null) {
                return getProjectsFromProjectIds(projectIds);
            }
        }
        return Collections.emptyList();
    }

    @Nullable
    public ProjectCategory getProjectCategoryForProject(Project project) {
        return (project != null) ? getProjectCategoryForProjectId(project.getId()) : null;
    }

    @Nullable
    public ProjectCategory getProjectCategoryFromProject(Project project) {
        return (project != null) ? getProjectCategoryForProjectId(project.getId()) : null;
    }

    @Nullable
    private ProjectCategory getProjectCategoryForProjectId(Long projectId) {
        final Long projectCategoryId = projectIdToProjectCategoryId.get(projectId);
        return (projectCategoryId != null) ? getProjectCategory(projectCategoryId) : null;
    }

    public Collection<Project> getProjectsWithNoCategory() {
        return getProjectsFromProjectIds(projectsWithNoCategory);
    }

    protected void refreshProjectsWithNoCategory() {
        final Collection<Project> projects = getProjects();
        final List<Project> projectObjectsWithNoCategory = Lists.newArrayListWithCapacity(projects.size());
        projectObjectsWithNoCategory.addAll(projects.stream().filter(project -> getProjectCategoryFromProject(project) == null).collect(Collectors.toList()));

        //alphabetic order on the project name
        sort(projectObjectsWithNoCategory, Named.NAME_COMPARATOR);
        projectsWithNoCategory = ImmutableList.copyOf(getIdsFromProjects(projectObjectsWithNoCategory));
    }

    public Project getProjectByCurrentKeyIgnoreCase(final String projectKey) {
        return projectsByCurrentKeyIgnoreCase.get(projectKey);
    }

    @Nullable
    public Project getProjectByKeyIgnoreCase(final String projectKey) {
        // JDEV-24899 we want to avoid potential NPE if ProjectKey table contains keys referring to non-existent projects
        final Long projectId = projectKeyStore.getProjectIdByKeyIgnoreCase(projectKey);
        return projectId != null ? getProject(projectId) : null;
    }

    public Project getProjectByCurrentKey(final String projectKey) {
        return projectsByCurrentKey.get(projectKey);
    }

    public Set<String> getAllProjectKeys(final Long projectId) {
        return projectKeyStore.getProjectKeys(projectId);
    }
}
