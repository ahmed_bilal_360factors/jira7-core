package com.atlassian.jira.web.action.admin;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.application.JiraApplication;
import com.atlassian.jira.project.type.JiraApplicationAdapter;
import com.atlassian.jira.project.type.ProjectType;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.web.action.ActionViewData;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.web.landingpage.LandingPageRedirectManager;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.StreamSupport.stream;

@WebSudoRequired
public class LandingPage extends JiraWebActionSupport {
    private static final Logger log = LoggerFactory.getLogger(LandingPage.class);

    @Nonnull
    private final LandingPageRedirectManager landingPageRedirectManager;
    @Nonnull
    private final PageBuilderService pageBuilderService;
    @Nonnull
    private final JiraApplicationAdapter jiraApplicationAdapter;

    @Nonnull
    private Optional<String> product = Optional.empty();

    private String projectTypeKey;
    private String productName;


    public LandingPage(@Nonnull final LandingPageRedirectManager landingPageRedirectManager,
                       @Nonnull final PageBuilderService pageBuilderService,
                       @Nonnull final JiraApplicationAdapter jiraApplicationAdapter) {
        this.landingPageRedirectManager = landingPageRedirectManager;
        this.pageBuilderService = pageBuilderService;
        this.jiraApplicationAdapter = jiraApplicationAdapter;
    }

    @Override
    protected void doValidation() {
        product.filter(key -> !ApplicationKey.isValid(key))
                .ifPresent(key -> log.warn("Incorrect application key: " + key));

        product.filter(ApplicationKey::isValid)
                .filter(key -> !getForApplicationKey(key).isPresent())
                .ifPresent(key -> log.warn("Did not found application for key: " + key));
    }

    @Override
    protected String doExecute() throws Exception {
        handleSelectedProduct();

        pageBuilderService.assembler().resources()
                .requireContext("atl.general")
                .requireContext("atl.admin")
                .requireWebResource("jira.webresources:landingpage");

        return landingPageRedirectManager.redirectUrl(getLoggedInUser())
                .map(this::getRedirect)
                .orElse(SUCCESS);
    }

    @VisibleForTesting
    void handleSelectedProduct() {
        final Optional<JiraApplication> selectedProduct = product.flatMap(this::getForApplicationKey);

        projectTypeKey = selectedProduct
                .map(application -> stream(application.getProjectTypes().spliterator(), false))
                .flatMap(Stream::findFirst)
                .map(ProjectType::getKey)
                .map(ProjectTypeKey::getKey)
                .orElse("");

        productName = selectedProduct
                .map(JiraApplication::getName)
                .orElse("JIRA");
    }

    public void setProduct(@Nullable final String product) {
        this.product = Optional.ofNullable(product);
    }

    @ActionViewData
    public String getProjectTypeKey() throws Exception {
        return projectTypeKey;
    }

    @ActionViewData
    public String getProductName() throws Exception {
        return productName;
    }

    private Optional<JiraApplication> getForApplicationKey(final String applicationKey) {
        return Optional.of(applicationKey).filter(ApplicationKey::isValid)
                .map(ApplicationKey::valueOf)
                .flatMap(this::getApplication);
    }

    private Optional<JiraApplication> getApplication(final ApplicationKey applicationKey) {
        return stream(jiraApplicationAdapter.getAccessibleJiraApplications().spliterator(), false)
                .filter(application -> applicationKey.equals(application.getKey()))
                .findFirst();
    }
}
