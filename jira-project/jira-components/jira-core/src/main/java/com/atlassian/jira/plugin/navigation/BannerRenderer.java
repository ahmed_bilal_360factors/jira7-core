package com.atlassian.jira.plugin.navigation;

import com.atlassian.fugue.Iterables;
import com.atlassian.fugue.Option;
import com.atlassian.jira.plugin.webfragment.DefaultWebFragmentContext;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Renders (some of) the banners that display at the top of the JIRA page.
 *
 * @since 7.0
 */
public final class BannerRenderer {
    private static final Logger LOG = LoggerFactory.getLogger(BannerRenderer.class);
    private static final String LOCATION = "jira-banner";

    private final WebInterfaceManager webInterfaceManager;
    private final Supplier<Map<String, Object>> contextSupplier;

    @Inject
    public BannerRenderer(final WebInterfaceManager webInterfaceManager) {
        this(webInterfaceManager, DefaultWebFragmentContext::get);
    }

    @VisibleForTesting
    BannerRenderer(final WebInterfaceManager webInterfaceManager, final Supplier<Map<String, Object>> contextSupplier) {
        this.webInterfaceManager = webInterfaceManager;
        this.contextSupplier = contextSupplier;
    }

    public void writeBanners(Writer writer) {
        final Map<String, Object> context = contextSupplier.get();
        final Option<WebPanelModuleDescriptor> displayableWebPanelDescriptors =
                Iterables.first(webInterfaceManager.getDisplayableWebPanelDescriptors(LOCATION, context));
        for (WebPanelModuleDescriptor descriptor : displayableWebPanelDescriptors) {
            try {
                descriptor.getModule().writeHtml(writer, context);
            } catch (IOException | RuntimeException e) {
                LOG.debug(String.format("Unable to render banner '%s'.", descriptor.getCompleteKey()), e);
            }
        }
    }
}
