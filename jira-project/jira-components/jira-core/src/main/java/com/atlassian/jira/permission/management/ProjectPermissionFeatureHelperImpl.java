package com.atlassian.jira.permission.management;

import com.atlassian.jira.config.FeatureManager;

public class ProjectPermissionFeatureHelperImpl implements ProjectPermissionFeatureHelper {

    private final FeatureManager featureManager;

    private static final String DISABLE_NEW_PERMISSION_SCHEME_FLAG = "com.atlassian.jira.permission-schemes.single-page-ui.disabled";

    private static final String OLD_PROJECT_PERMISSIONS_PAGE = "OldEditPermissions!default.jspa?schemeId=";
    private static final String NEW_PROJECT_PERMISSIONS_PAGE = "EditPermissions!default.jspa?schemeId=";


    public ProjectPermissionFeatureHelperImpl(FeatureManager featureManager) {
        this.featureManager = featureManager;
    }

    @Override
    public Boolean useOldProjectPermissionPage() {
        return featureManager.isEnabled(DISABLE_NEW_PERMISSION_SCHEME_FLAG);
    }

    @Override
    public String getOldEditPermissionUrl(Long schemeId) {
        return OLD_PROJECT_PERMISSIONS_PAGE + schemeId;
    }

    @Override
    public String getNewEditPermissionUrl(Long schemeId) {
        return NEW_PROJECT_PERMISSIONS_PAGE + schemeId;
    }
}
