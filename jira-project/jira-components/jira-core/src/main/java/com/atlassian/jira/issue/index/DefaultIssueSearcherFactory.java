package com.atlassian.jira.issue.index;

import com.atlassian.jira.config.util.IndexingConfiguration;
import org.apache.lucene.search.IndexSearcher;

import javax.annotation.Nonnull;

import static com.atlassian.jira.issue.index.IndexDirectoryFactory.Name;

/**
 * @since v6.4
 */
public class DefaultIssueSearcherFactory implements IssueSearcherFactory {
    private final IssueIndexer issueIndexer;
    private final IndexingConfiguration indexingConfiguration;

    public DefaultIssueSearcherFactory(final IssueIndexer issueIndexer, final IndexingConfiguration indexingConfiguration) {
        this.issueIndexer = issueIndexer;
        this.indexingConfiguration = indexingConfiguration;
    }

    @Nonnull
    @Override
    public IndexSearcher getEntitySearcher(final Name index) {
        return SearcherCache.getThreadLocalCache().retrieveEntitySearcher(issueIndexer, indexingConfiguration, index);
    }
}
