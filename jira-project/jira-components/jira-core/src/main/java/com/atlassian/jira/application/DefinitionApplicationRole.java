package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;

import javax.annotation.Nonnull;

import static com.atlassian.jira.application.ApplicationRoleDefinitions.ApplicationRoleDefinition;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * A {@link com.atlassian.jira.application.ApplicationRole} defined in a plugin.
 *
 * @since v7.0
 */
final class DefinitionApplicationRole extends AbstractApplicationRole {
    private final ApplicationRoleDefinition data;
    private final ApplicationRoleDefinitions definitions;
    private Boolean defined = null;

    DefinitionApplicationRole(final ApplicationRoleDefinitions definitions, final ApplicationRoleDefinition data, final Iterable<Group> groups,
                              final Iterable<Group> defaultGroups, final int numberOfLicenseSeats, final boolean selectedByDefault) {
        super(groups, defaultGroups, numberOfLicenseSeats, selectedByDefault);

        this.definitions = definitions;
        this.data = notNull("data", data);
    }

    @Nonnull
    @Override
    public ApplicationKey getKey() {
        return data.key();
    }

    @Nonnull
    @Override
    public String getName() {
        return data.name();
    }

    @Nonnull
    @Override
    public ApplicationRole withGroups(@Nonnull final Iterable<Group> groups, @Nonnull final Iterable<Group> defaultGroups) {
        return new DefinitionApplicationRole(definitions, data, groups, defaultGroups, getNumberOfSeats(), isSelectedByDefault());
    }

    @Override
    public ApplicationRole withSelectedByDefault(final boolean selectedByDefault) {
        return new DefinitionApplicationRole(definitions, data, getGroups(), getDefaultGroups(), getNumberOfSeats(), selectedByDefault);
    }

    @Override
    public boolean isPlatform() {
        return data instanceof DefaultApplicationRoleDefinitions.CoreRoleDefinition;
    }

    @Override
    public boolean isDefined() {
        if (defined == null) {
            // HIROL-1288: Lazy load `defined` so that it not cached with incorrect data
            // at the time `DefaultApplicationRoleManager` 's cache loaded
            defined = Boolean.valueOf(definitions.isDefined(data.key()));
        }
        return defined.booleanValue();
    }
}