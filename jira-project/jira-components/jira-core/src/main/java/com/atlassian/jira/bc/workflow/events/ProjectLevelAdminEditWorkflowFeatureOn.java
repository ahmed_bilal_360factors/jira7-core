package com.atlassian.jira.bc.workflow.events;

import com.atlassian.analytics.api.annotations.EventName;

@EventName("project.level.admin.edit_workflow.feature.on")
public class ProjectLevelAdminEditWorkflowFeatureOn {
}
