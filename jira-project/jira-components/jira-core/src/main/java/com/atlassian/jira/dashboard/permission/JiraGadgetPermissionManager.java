package com.atlassian.jira.dashboard.permission;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.gadgets.DashboardItemState;
import com.atlassian.gadgets.DashboardItemStateVisitor;
import com.atlassian.gadgets.GadgetState;
import com.atlassian.gadgets.LocalDashboardItemState;
import com.atlassian.gadgets.Vote;
import com.atlassian.gadgets.dashboard.DashboardState;
import com.atlassian.gadgets.dashboard.spi.DashboardPermissionService;
import com.atlassian.gadgets.plugins.GadgetLocationTranslator;
import com.atlassian.gadgets.plugins.PluginGadgetSpec;
import com.atlassian.jira.dashboard.analytics.GadgetHiddenEvent;
import com.atlassian.jira.plugin.webfragment.DefaultWebFragmentContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.Users;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.Condition;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.gadgets.Vote.ALLOW;
import static com.atlassian.gadgets.Vote.DENY;
import static com.atlassian.gadgets.Vote.PASS;
import static com.atlassian.jira.component.ComponentAccessor.getOSGiComponentInstanceOfType;
import static com.atlassian.jira.dashboard.analytics.GadgetHiddenEvent.Reason;
import static com.atlassian.jira.dashboard.analytics.GadgetHiddenEvent.Reason.ENABLED_CONDITION_FAILED;
import static com.atlassian.jira.dashboard.analytics.GadgetHiddenEvent.Reason.LOCAL_CONDITION_FAILED;
import static com.atlassian.jira.dashboard.analytics.GadgetHiddenEvent.Reason.PERMISSIONS_FAILED;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.collect.Iterables.transform;

public class JiraGadgetPermissionManager implements GadgetPermissionManager {
    private static final Logger log = LoggerFactory.getLogger(JiraGadgetPermissionManager.class);

    //match two groups in a string starting with anything, then 'rest/gadgets/' then anything then '/g/' then match all except '/' then ':' then match all except '/' then anything.
    private static final Pattern PLUGIN_KEY_PATTERN = Pattern.compile(".*rest\\/gadgets\\/.*\\/g\\/([^\\/]+):([^\\/]+).*", Pattern.CASE_INSENSITIVE);

    private final PermissionManager permissionManager;
    private final PluginAccessor pluginAccessor;
    private final DashboardPermissionService permissionService;
    private final EventPublisher eventPublisher;

    public JiraGadgetPermissionManager(
            final PermissionManager permissionManager,
            final PluginAccessor pluginAccessor,
            final DashboardPermissionService permissionService,
            final EventPublisher eventPublisher) {
        this.permissionManager = permissionManager;
        this.pluginAccessor = pluginAccessor;
        this.permissionService = permissionService;
        this.eventPublisher = eventPublisher;
    }

    @Override
    public Vote voteOn(final PluginGadgetSpec pluginGadgetSpec, final ApplicationUser remoteUser) {
        return voteOnWithReason(pluginGadgetSpec, remoteUser).getVote();
    }

    private VoteContext voteOnWithReason(final PluginGadgetSpec pluginGadgetSpec, final ApplicationUser remoteUser) {
        notNull("pluginGadgetSpec", pluginGadgetSpec);

        Map<String, Object> context = DefaultWebFragmentContext.get();

        if (!pluginGadgetSpec.getEnabledCondition().shouldDisplay(context)) {
            return new VoteContext(DENY).withReason(ENABLED_CONDITION_FAILED).withCondition(pluginGadgetSpec.getEnabledCondition());
        }

        if (!pluginGadgetSpec.getLocalCondition().shouldDisplay(context)) {
            return new VoteContext(DENY).withReason(LOCAL_CONDITION_FAILED).withCondition(pluginGadgetSpec.getLocalCondition());
        }

        final String roleString = pluginGadgetSpec.getParameter("roles-required");
        if (StringUtils.isBlank(roleString)) {
            return new VoteContext(ALLOW);
        }

        //admins get to see all gadgets, so that they'll show up in the 'Default Dashboard' section in the admin section
        if (permissionManager.hasPermission(Permissions.ADMINISTER, remoteUser)) {
            return new VoteContext(ALLOW);
        }

        final String[] roles = StringUtils.split(roleString);
        for (String role : roles) {
            final int permission = Permissions.getType(role);
            if (permission == -1) {
                log.warn("Invalid role-required specified for gadget '" + pluginGadgetSpec.getKey() + "': '" + role + "'");
                return new VoteContext(PASS);
            }
            if (Permissions.isGlobalPermission(permission)) {
                if (!permissionManager.hasPermission(permission, remoteUser)) {
                    return new VoteContext(DENY).withReason(PERMISSIONS_FAILED);
                }
            } else {
                if (!hasProjectsPermission(permission, remoteUser)) {
                    return new VoteContext(DENY).withReason(PERMISSIONS_FAILED);
                }
            }
        }

        return new VoteContext(ALLOW);
    }

    @Override
    public DashboardState filterGadgets(final DashboardState dashboardState, final ApplicationUser remoteUser) {
        notNull("dashboardState", dashboardState);

        boolean isWritable = permissionService.isWritableBy(dashboardState.getId(), remoteUser == null ? null : remoteUser.getName());
        if (isWritable) {
            return dashboardState;
        }

        final Iterable<Iterable<DashboardItemState>> columns = filterGadgetsThatUserCanNotSee(dashboardState.getDashboardColumns().getColumns(), allowedDashboardItemPredicate(remoteUser));
        return DashboardState.dashboard(dashboardState).dashboardColumns(columns).build();
    }

    @Override
    public Option<PluginGadgetSpec> getPluginGadgetSpec(URI gadgetUri) {
        if (gadgetUri == null) {
            return Option.none();
        }

        URI gadgetSpecUri = getGadgetLocationTranslator().translate(gadgetUri);
        String moduleKey = extractModuleKey(gadgetSpecUri.toASCIIString());
        return moduleKey == null ? Option.<PluginGadgetSpec>none() : getPluginGadgetSpecFromModuleKey(moduleKey);
    }

    @VisibleForTesting
    String extractModuleKey(final String gadgetUri) {
        final Matcher matcher = PLUGIN_KEY_PATTERN.matcher(gadgetUri);
        if (matcher.matches() && matcher.groupCount() == 2) {
            return matcher.group(1) + ":" + matcher.group(2);
        }
        return null;
    }

    private Option<PluginGadgetSpec> getPluginGadgetSpecFromModuleKey(String completeGadgetModuleKey) {
        final ModuleDescriptor<?> moduleDescriptor = pluginAccessor.getEnabledPluginModule(completeGadgetModuleKey);
        if (moduleDescriptor == null) {
            return Option.none();
        }

        Object module = moduleDescriptor.getModule();
        if (module instanceof PluginGadgetSpec) {
            return Option.some((PluginGadgetSpec) module);
        }
        return Option.none();
    }

    private boolean hasProjectsPermission(int permission, ApplicationUser user) {
        try {
            return permissionManager.hasProjects(new ProjectPermissionKey(permission), user);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private GadgetLocationTranslator getGadgetLocationTranslator() {
        return getOSGiComponentInstanceOfType(GadgetLocationTranslator.class);
    }


    private Predicate<DashboardItemState> allowedDashboardItemPredicate(final ApplicationUser remoteUser) {
        return new Predicate<DashboardItemState>() {
            @Override
            public boolean apply(final DashboardItemState dashboardItemState) {
                VoteContext voteResult = dashboardItemState.accept(new DashboardItemStateVisitor<VoteContext>() {
                    @Override
                    public VoteContext visit(final GadgetState gadgetState) {
                        final Option<PluginGadgetSpec> gadgetSpecResult = getPluginGadgetSpec(gadgetState.getGadgetSpecUri());
                        if (!gadgetSpecResult.isDefined()) {
                            return new VoteContext(ALLOW);
                        }

                        return voteOnWithReason(gadgetSpecResult.get(), remoteUser);
                    }

                    @Override
                    public VoteContext visit(final LocalDashboardItemState localDashboardItemState) {
                        // We don't need to filter local dashboard items, they wont be displayed if user does not have permission to see them.
                        return new VoteContext(ALLOW);
                    }
                });

                raiseAnalyticEvents(dashboardItemState, voteResult, Users.isAnonymous(remoteUser));
                return !voteResult.isDeny();
            }
        };
    }


    /**
     * Filters unwanted items from a 2D array (Iterable of Iterable) of Dashboard Items.
     * The outer array is iterated and each inner array is filtered using the supplied Predicate.
     */
    private Iterable<Iterable<DashboardItemState>> filterGadgetsThatUserCanNotSee(Iterable<? extends Iterable<DashboardItemState>> columns, Predicate allowedDashboardItemPredicate) {
        return transform(columns, new Function<Iterable<DashboardItemState>, Iterable<DashboardItemState>>() {
            @Override
            public Iterable apply(final Iterable rows) {
                return Iterables.filter(rows, allowedDashboardItemPredicate);
            }
        });
    }

    private void raiseAnalyticEvents(final DashboardItemState dashboardItemState, final VoteContext voteContext, final boolean isAnonymousUser) {
        if (voteContext.isDeny() && voteContext.getReason().isDefined()) {
            // raise events for hidden gadgets only

            final String gadgetName = dashboardItemState.accept(new DashboardItemStateVisitor<String>() {
                @Override
                public String visit(final GadgetState gadgetState) {
                    return gadgetState.getGadgetSpecUri().toString();
                }

                @Override
                public String visit(final LocalDashboardItemState localDashboardItemState) {
                    return localDashboardItemState.getDashboardItemModuleId().getFullModuleKey().getCompleteKey();
                }
            });

            final Reason reason = voteContext.getReason().get();
            final Option<Condition> conditionClass = voteContext.getConditionClass();
            final String conditionClassName = conditionClass.isDefined() ? conditionClass.get().getClass().getSimpleName() : null;
            eventPublisher.publish(new GadgetHiddenEvent(gadgetName, conditionClassName, reason, isAnonymousUser));
        }
    }
}
