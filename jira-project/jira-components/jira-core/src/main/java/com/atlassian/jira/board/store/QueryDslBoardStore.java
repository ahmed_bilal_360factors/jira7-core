package com.atlassian.jira.board.store;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.board.Board;
import com.atlassian.jira.board.BoardCreationData;
import com.atlassian.jira.board.BoardId;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.model.querydsl.BoardDTO;
import com.atlassian.jira.model.querydsl.ProjectChangedTimeDTO;
import com.atlassian.jira.model.querydsl.QBoard;
import com.atlassian.jira.ofbiz.OfBizDelegator;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.atlassian.jira.model.querydsl.QBoardProject.BOARD_PROJECT;
import static com.atlassian.jira.model.querydsl.QProjectChangedTime.PROJECT_CHANGED_TIME;

public class QueryDslBoardStore implements BoardStore {
    private static final String BOARD_ENTITY_NAME = "Board";
    private static final QBoard BOARD = new QBoard(BOARD_ENTITY_NAME);

    private final OfBizDelegator ofBizDelegator;
    private final QueryDslAccessor queryDslAccessor;

    public QueryDslBoardStore(
            final OfBizDelegator ofBizDelegator,
            final QueryDslAccessor queryDslAccessor) {
        this.ofBizDelegator = ofBizDelegator;
        this.queryDslAccessor = queryDslAccessor;
    }

    @Override
    public Board createBoard(final BoardCreationData boardCreationData) {
        final Long nextSeqId = ofBizDelegator.getDelegatorInterface().getNextSeqId(BOARD_ENTITY_NAME);
        queryDslAccessor.execute(dbConnection -> {
                dbConnection.insert(BOARD)
                        .set(BOARD.id, nextSeqId)
                        .set(BOARD.jql, boardCreationData.getJql())
                        .execute();

                dbConnection.insert(BOARD_PROJECT)
                        .set(BOARD_PROJECT.boardId, nextSeqId)
                        .set(BOARD_PROJECT.projectId, boardCreationData.getProjectId())
                        .execute();
        });

        return getBoard(new BoardId(nextSeqId)).get();
    }

    @Override
    public Optional<Board> getBoard(final BoardId boardId) {
        final BoardDTO boardDto = queryDslAccessor.executeQuery(dbConnection -> dbConnection.newSqlQuery()
                .select(BOARD)
                .from(BOARD)
                .where(BOARD.id.eq(boardId.getId()))
                .limit(1)
                .fetchOne());

        if (boardDto == null) {
            return Optional.empty();
        }

        return Optional.of(new Board(new BoardId(boardDto.getId()), boardDto.getJql()));
    }

    @Override
    public boolean deleteBoard(final BoardId boardId) {
        long deletedRecords = queryDslAccessor.executeQuery(dbConnection -> {
            long count = dbConnection
                    .delete(BOARD)
                    .where(BOARD.id.eq(boardId.getId()))
                    .execute();
            dbConnection
                    .delete(BOARD_PROJECT)
                    .where(BOARD_PROJECT.boardId.eq(boardId.getId()))
                    .execute();
            return count;
        });
        return deletedRecords == 1;

    }

    @Override
    public List<Board> getBoardsForProject(long projectId) {
        final List<BoardDTO> boardDtos = queryDslAccessor.executeQuery(dbConnection -> dbConnection.newSqlQuery()
                .select(BOARD)
                .from(BOARD)
                .innerJoin(BOARD_PROJECT)
                .on(BOARD.id.eq(BOARD_PROJECT.boardId))
                .where(BOARD_PROJECT.projectId.eq(projectId))
                .fetch());
        return boardDtos.stream().map(boardDto -> new Board(new BoardId(boardDto.getId()), boardDto.getJql()))
                .collect(CollectorsUtil.toImmutableList());
    }

    @Override
    public boolean hasBoardForProject(long projectId) {
        long count = queryDslAccessor.executeQuery(dbConnection -> dbConnection.newSqlQuery()
                .select(BOARD_PROJECT.projectId)
                .from(BOARD_PROJECT)
                .where(BOARD_PROJECT.projectId.eq(projectId))
                .fetchCount());
        return count > 0;
    }

    @Override
    public Optional<Date> getBoardDataChangedTime(BoardId boardId) {
        final ProjectChangedTimeDTO projectChangedTimeDTO = queryDslAccessor.executeQuery(
                dbConnection -> dbConnection.newSqlQuery()
                        .select(PROJECT_CHANGED_TIME)
                        .from(PROJECT_CHANGED_TIME)
                        .innerJoin(BOARD_PROJECT)
                        .on(PROJECT_CHANGED_TIME.projectId.eq(BOARD_PROJECT.projectId))
                        .where(BOARD_PROJECT.boardId.eq(boardId.getId()))
                        .fetchFirst());

        if (projectChangedTimeDTO == null || projectChangedTimeDTO.getIssueChangedTime() == null) {
            return Optional.empty();
        }
        return Optional.of(new Date(projectChangedTimeDTO.getIssueChangedTime().getTime()));
    }
}
