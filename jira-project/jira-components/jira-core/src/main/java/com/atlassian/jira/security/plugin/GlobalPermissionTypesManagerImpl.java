package com.atlassian.jira.security.plugin;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.event.api.EventListener;
import com.atlassian.fugue.Option;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.GlobalPermissionType;
import com.atlassian.jira.plugin.permission.GlobalPermissionModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.tracker.DefaultPluginModuleTracker;
import com.atlassian.plugin.tracker.PluginModuleTracker;
import com.atlassian.util.concurrent.ResettableLazyReference;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Maps;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Map;


@EventComponent
public class GlobalPermissionTypesManagerImpl implements Startable, GlobalPermissionTypesManager {
    private final DefaultPluginModuleTracker<Void, GlobalPermissionModuleDescriptor> pluginModuleTracker;

    private final ResettableLazyReference<Map<String, GlobalPermissionType>> globalPermissions;

    private final ApplicationRoleManager applicationRoleManager;

    public GlobalPermissionTypesManagerImpl(@Nonnull final PluginAccessor pluginAccessor, @Nonnull final PluginEventManager pluginEventManager,
                                            @Nonnull final ApplicationRoleManager applicationRoleManager) {
        this.applicationRoleManager = applicationRoleManager;
        this.pluginModuleTracker = new DefaultPluginModuleTracker<>(pluginAccessor, pluginEventManager, GlobalPermissionModuleDescriptor.class, new PluginModuleTracker.Customizer<Void, GlobalPermissionModuleDescriptor>() {
            @Override
            public void removed(final GlobalPermissionModuleDescriptor descriptor) {
                globalPermissions.reset();
            }

            @Override
            public GlobalPermissionModuleDescriptor adding(final GlobalPermissionModuleDescriptor descriptor) {
                globalPermissions.reset();
                return descriptor;
            }
        });
        this.globalPermissions = new GlobalPermissionsResettableLazyReference(pluginModuleTracker.getModuleDescriptors());
    }

    @EventListener
    public void onClearCache(final ClearCacheEvent event) {
        globalPermissions.reset();
    }

    @Override
    public void start() throws Exception {
        globalPermissions.reset();
    }

    @Override
    public Collection<GlobalPermissionType> getAll() {
        return globalPermissions.get().values().stream()
                .filter(gpt -> !gpt.getKey().equals(GlobalPermissionKey.USE.getKey()))
                .collect(CollectorsUtil.toImmutableSet());
    }

    @Override
    public Option<GlobalPermissionType> getGlobalPermission(@Nonnull String permissionKey) {
        return Option.option(globalPermissions.get().get(permissionKey));
    }

    @Override
    public Option<GlobalPermissionType> getGlobalPermission(@Nonnull final GlobalPermissionKey permissionKey) {
        return getGlobalPermission(permissionKey.getKey());
    }

    @VisibleForTesting
    DefaultPluginModuleTracker<Void, GlobalPermissionModuleDescriptor> getPluginModuleTracker() {
        return pluginModuleTracker;
    }

    private static class GlobalPermissionsResettableLazyReference extends ResettableLazyReference<Map<String, GlobalPermissionType>> {
        private final Iterable<GlobalPermissionModuleDescriptor> moduleDescriptors;

        private GlobalPermissionsResettableLazyReference(final Iterable<GlobalPermissionModuleDescriptor> moduleDescriptors) {
            this.moduleDescriptors = moduleDescriptors;
        }

        @Override
        protected Map<String, GlobalPermissionType> create() {
            Map<String, GlobalPermissionType> permissions = Maps.newHashMap();
            for (final GlobalPermissionModuleDescriptor moduleDescriptor : moduleDescriptors) {
                GlobalPermissionType globalPermissionType = createGlobalPermission(moduleDescriptor);
                final String key = moduleDescriptor.getKey();
                permissions.put(key, globalPermissionType);
            }
            return permissions;
        }

        private GlobalPermissionType createGlobalPermission(GlobalPermissionModuleDescriptor descriptor) {
            return new GlobalPermissionType(descriptor.getKey(),
                    descriptor.getI18nNameKey(),
                    descriptor.getDescriptionI18nKey(),
                    descriptor.isAnonymousAllowed()
            );
        }
    }
}
