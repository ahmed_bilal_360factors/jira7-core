package com.atlassian.jira.issue.subscription;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.config.group.GroupConfigurable;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.entity.Delete;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.entity.Update;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.mail.MailingListCompiler;
import com.atlassian.jira.mail.SubscriptionMailQueueItemFactory;
import com.atlassian.jira.scheduler.cron.SimpleToCronTriggerConverter;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.template.TemplateManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.mail.queue.MailQueue;
import com.atlassian.mail.queue.MailQueueItem;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.status.JobDetails;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.ofbiz.core.entity.EntityCondition;
import org.ofbiz.core.entity.EntityConditionList;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericEntityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static com.atlassian.jira.issue.subscription.FilterSubscriptionFactory.EMAIL_ON_EMPTY;
import static com.atlassian.jira.issue.subscription.FilterSubscriptionFactory.FILTER_ID;
import static com.atlassian.jira.issue.subscription.FilterSubscriptionFactory.GROUP;
import static com.atlassian.jira.issue.subscription.FilterSubscriptionFactory.ID;
import static com.atlassian.jira.issue.subscription.FilterSubscriptionFactory.LAST_RUN_TIME;
import static com.atlassian.jira.issue.subscription.FilterSubscriptionFactory.USER_KEY;
import static com.atlassian.jira.user.ApplicationUsers.getKeyFor;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

public class DefaultSubscriptionManager extends MailingListCompiler implements SubscriptionManager, GroupConfigurable {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultSubscriptionManager.class);
    public static final String SUBSCRIPTION_PREFIX = DefaultSubscriptionManager.class.getName();
    public static final String JOB_RUNNER_KEY = DefaultSubscriptionManager.class.getName();

    private static final String ENTITY_NAME = "FilterSubscription";

    private final MailQueue mailQueue;
    private final SubscriptionMailQueueItemFactory subscriptionMailQueueItemFactory;
    private final GroupManager groupManager;
    private final SchedulerService schedulerService;
    private final EntityEngine entityEngine;
    private final UserManager userManager;

    public DefaultSubscriptionManager(final MailQueue mailQueue,
                                      final TemplateManager templateManager,
                                      final SubscriptionMailQueueItemFactory subscriptionMailQueueItemFactory,
                                      final ProjectRoleManager projectRoleManager, final GroupManager groupManager,
                                      SchedulerService schedulerService, final EntityEngine entityEngine,
                                      DateTimeFormatterFactory dateTimeFormatterFactory,
                                      UserManager userManager) {
        super(templateManager, projectRoleManager, dateTimeFormatterFactory);
        this.mailQueue = mailQueue;
        this.subscriptionMailQueueItemFactory = subscriptionMailQueueItemFactory;
        this.groupManager = groupManager;
        this.schedulerService = schedulerService;
        this.entityEngine = entityEngine;
        this.userManager = userManager;
        schedulerService.registerJobRunner(JobRunnerKey.of(JOB_RUNNER_KEY), new SendFilterJob());
    }

    @Override
    public boolean hasSubscription(final ApplicationUser user, final Long filterId) throws GenericEntityException {
        return !getFilterSubscriptions(user, filterId).isEmpty();
    }

    @Override
    public FilterSubscription getFilterSubscription(final Long subId) throws GenericEntityException {
        return Select.from(Entity.FILTER_SUBSCRIPTION).byId(subId).runWith(entityEngine).singleValue();
    }

    @Override
    public FilterSubscription getFilterSubscription(final ApplicationUser user, final Long subId)
            throws GenericEntityException {
        return Select.from(Entity.FILTER_SUBSCRIPTION)
                .byId(subId).whereEqual(USER_KEY, getKeyFor(user))
                .runWith(entityEngine).singleValue();
    }

    private EntityCondition getFilterByUserCondition(final ApplicationUser user, final Long filterId) {
        final List<EntityExpr> entityExpressions = Lists.newArrayList();
        // Retrieve all subscriptions created by the user
        entityExpressions.add(new EntityExpr(USER_KEY, EntityOperator.EQUALS, getKeyFor(user)));

        // Group shared subscriptions
        final Iterable<String> groups = groupManager.getGroupNamesForUser(user);
        for (final String group : groups) {
            // That's the whole point...
            //noinspection ObjectAllocationInLoop
            entityExpressions.add(new EntityExpr(GROUP, EntityOperator.EQUALS, group));
        }

        // Get the expression which will return everything owned by the user or shared to one of user's groups
        final EntityCondition ownershipCondition = new EntityConditionList(entityExpressions, EntityOperator.OR);

        final EntityCondition filterCondition = new EntityExpr(FILTER_ID, EntityOperator.EQUALS, filterId);

        return new EntityConditionList(ImmutableList.of(ownershipCondition, filterCondition), EntityOperator.AND);
    }

    @Override
    public List<FilterSubscription> getFilterSubscriptions(final ApplicationUser user, final Long filterId)
            throws GenericEntityException {
        final EntityCondition filterCondition = getFilterByUserCondition(user, filterId);

        return Select.from(Entity.FILTER_SUBSCRIPTION).whereCondition(filterCondition).runWith(entityEngine).asList();
    }

    @Nullable
    @Override
    public String getCronExpressionForSubscription(final FilterSubscription subscription) {
        return getCronExpression(subscription.getId());
    }

    @Nullable
    private String getCronExpression(final long subscriptionId) {
        final JobDetails jobDetails = schedulerService.getJobDetails(toJobId(subscriptionId));
        if (jobDetails == null) {
            return null;
        }
        return getCronExpression(jobDetails.getSchedule());
    }

    @Nullable
    private static String getCronExpression(final Schedule schedule) {
        final String cronExpression;
        if (schedule.getCronScheduleInfo() != null) {
            cronExpression = schedule.getCronScheduleInfo().getCronExpression();
        } else if (schedule.getIntervalScheduleInfo() != null) {
            final long interval = schedule.getIntervalScheduleInfo().getIntervalInMillis();
            cronExpression = new SimpleToCronTriggerConverter().convertToCronString(new Date(), interval).cronString;
        } else {
            cronExpression = null;
        }
        return cronExpression;
    }

    @Override
    public void updateSubscription(final ApplicationUser user, final Long subId, final String groupName, final String cronExpression, final Boolean emailOnEmpty) throws DataAccessException {
        Schedule schedule = Schedule.forCronExpression(cronExpression);
        try {
            schedulerService.unscheduleJob(toJobId(subId));

            Update.into(ENTITY_NAME).set(USER_KEY, getKeyFor(user)).set(GROUP, groupName).set(EMAIL_ON_EMPTY, emailOnEmpty.toString())
                    .whereEqual(ID, subId).execute(entityEngine);

            JobConfig config = getJobConfig(subId, schedule);
            schedulerService.scheduleJob(toJobId(subId), config);
        } catch (final SchedulerServiceException e) {
            throw new DataAccessException(e);
        }
    }

    private static JobId toJobId(final Long subId) {
        return JobId.of(SUBSCRIPTION_PREFIX + ':' + subId);
    }

    private static JobConfig getJobConfig(final Long subscriptionId, final Schedule schedule) {
        return JobConfig.forJobRunnerKey(JobRunnerKey.of(JOB_RUNNER_KEY))
                .withSchedule(schedule)
                .withParameters(ImmutableMap.<String, Serializable>of(SUBSCRIPTION_IDENTIFIER, subscriptionId));
    }

    @Override
    public List<FilterSubscription> getAllFilterSubscriptions(final Long filterId) {
        return Select.from(Entity.FILTER_SUBSCRIPTION).whereEqual(FILTER_ID, filterId).runWith(entityEngine).asList();
    }

    @Override
    public List<FilterSubscription> getAllFilterSubscriptions() {
        return Select.from(Entity.FILTER_SUBSCRIPTION).runWith(entityEngine).asList();
    }

    @Override
    public FilterSubscription createSubscription(final ApplicationUser user, final Long filterId, String groupName, final String cronExpression, final Boolean emailOnEmpty) {
        Schedule schedule = Schedule.forCronExpression(cronExpression);
        return createSubscription(user, filterId, groupName, emailOnEmpty, schedule);
    }

    @Nullable
    @Override
    public Date getNextSendTime(@Nonnull final FilterSubscription sub) {
        final JobDetails jobDetails = schedulerService.getJobDetails(toJobId(sub.getId()));
        return jobDetails == null ? null : jobDetails.getNextRunTime();
    }

    private FilterSubscription createSubscription(final ApplicationUser user, final Long filterId, String groupName, final Boolean emailOnEmpty, final Schedule schedule) {
        final FilterSubscription newSubscription = new DefaultFilterSubscription(null, filterId, user.getKey(), groupName, null, emailOnEmpty);

        try {
            final FilterSubscription subscription = entityEngine.createValue(Entity.FILTER_SUBSCRIPTION, newSubscription);
            JobConfig config = getJobConfig(subscription.getId(), schedule);
            schedulerService.scheduleJob(toJobId(subscription.getId()), config);
            return subscription;
        } catch (final SchedulerServiceException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public void deleteSubscription(final Long subId) throws GenericEntityException {
        final FilterSubscription subscription = getFilterSubscription(subId);
        if (subscription == null) {
            LOG.debug("Unable to find subscription for id : " + subId);
        }

        if (schedulerService.getJobDetails(toJobId(subId)) != null) {
            schedulerService.unscheduleJob(toJobId(subId));
        } else {
            LOG.debug("Unable to find a scheduled job for the subscription: " + subId + "; removing the subscription anyway.");
        }
        Delete.from(Entity.FILTER_SUBSCRIPTION).whereIdEquals(subId).execute(entityEngine);
    }

    @Override
    public void deleteSubscriptionsForUser(@Nonnull final ApplicationUser user)
            throws GenericEntityException {
        final String userKey = notNull("user", user).getKey();
        Delete.from(Entity.FILTER_SUBSCRIPTION).whereEqual(USER_KEY, userKey).execute(entityEngine);
    }

    private void runSubscription(final FilterSubscription sub) throws GenericEntityException {
        //Update the timestamps for the subscription so if it fails it won't get run every minute
        final Timestamp ts = new Timestamp(new Date().getTime());
        Update.into(ENTITY_NAME).set(LAST_RUN_TIME, ts).whereEqual(ID, sub.getId()).execute(entityEngine);

        if (userManager.getUserByKey(sub.getUserKey()).isActive()) {
            final MailQueueItem item = subscriptionMailQueueItemFactory.getSubscriptionMailQueueItem(sub);
            mailQueue.addItem(item);
        }
    }

    @Override
    public void runSubscription(final Long subId) throws GenericEntityException {
        runSubscription(getFilterSubscription(subId));
    }

    @Override
    public void runSubscription(final ApplicationUser user, final Long subId) throws GenericEntityException {
        runSubscription(getFilterSubscription(user, subId));
    }

    @Override
    public void deleteSubscriptionsForGroup(final Group group) throws GenericEntityException {
        notNull("group", group);
        final List<FilterSubscription> subscriptions = getGroupSubscriptions(group);
        for (FilterSubscription subscription : subscriptions) {
            deleteSubscription(subscription.getId());
        }

    }

    private List<FilterSubscription> getGroupSubscriptions(final Group group) {
        return Select.from(Entity.FILTER_SUBSCRIPTION).whereEqual(GROUP, group.getName()).runWith(entityEngine).asList();
    }

    @Override
    public boolean isGroupUsed(@Nonnull final Group group) {
        return getGroupSubscriptions(group).size() > 0;
    }
}
