package com.atlassian.jira.database;

import com.atlassian.jira.model.querydsl.JiraRelationalPathBase;
import com.querydsl.sql.RelationalPath;
import com.querydsl.sql.SQLQuery;
import com.querydsl.sql.dml.SQLDeleteClause;
import com.querydsl.sql.dml.SQLUpdateClause;

import java.sql.Connection;

/**
 * A handle to a Database Connection obtained from JIRA's connection pool.
 *
 * @see com.atlassian.jira.database.DbConnectionManager
 * @since v6.4
 */
public interface DbConnection {
    /**
     * Returns the JDBC connection wrapped by this object.
     *
     * @return the JDBC connection wrapped by this object.
     */
    Connection getJdbcConnection();

    /**
     * Starts a SELECT statement on this connection.
     * <p>
     * Example usage:
     * <pre>
     *     QVersion v = new QVersion("v");
     *
     *     final List<VersionDTO> versions =
     *             dbConnection.newSqlQuery()
     *                     .select(v)
     *                     .from(v)
     *                     .where(v.project.eq(projectId))
     *                     .orderBy(v.sequence.asc())
     *                     .fetch();
     * </pre>
     *
     * @return the new Query builder.
     */
    SQLQuery<?> newSqlQuery();

    /**
     * Starts an insert statement on the given DB Table.
     * <p>
     * Example 1 usage:
     * <pre>
     *        dbConnection.insert(QIssueLink.ISSUE_LINK)
     *                .set(QIssueLink.ISSUE_LINK.linktype, newIssueLinkTypeId)
     *                .set(QIssueLink.ISSUE_LINK.sequence, sequence)
     *                .execute();
     * </pre>
     * <p>
     * Example 2 usage:
     * <pre>
     *        dbConnection.insert(QIssueLink.ISSUE_LINK)
     *                .populate(issueLinkDTO)
     *                .execute();
     * </pre>
     *
     * @param entity The DB entity you want to insert into eg {@link com.atlassian.jira.model.querydsl.QIssue#ISSUE}
     * @return a builder to create your insert statement.
     */
    IdGeneratingSQLInsertClause insert(JiraRelationalPathBase<?> entity);

    /**
     * Starts an update statement on the given DB Table.
     * <p>
     * Example usage:
     * <pre>
     *        dbConnection.update(QIssueLink.ISSUE_LINK)
     *                .set(QIssueLink.ISSUE_LINK.linktype, newIssueLinkTypeId)
     *                .where(QIssueLink.ISSUE_LINK.id.eq(issueLink.getId()))
     *                .execute();
     * </pre>
     *
     * @param entity The DB entity you want to update eg {@link com.atlassian.jira.model.querydsl.QIssue#ISSUE}
     * @return a builder to create your update statement.
     */
    SQLUpdateClause update(RelationalPath<?> entity);

    /**
     * Starts an delete statement on the given DB Table.
     * <p>
     * Example usage:
     * <pre>
     *        dbConnection.delete(QIssueLink.ISSUE_LINK)
     *                .where(QIssueLink.ISSUE_LINK.id.eq(issueLink.getId()))
     *                .execute();
     * </pre>
     *
     * @param entity The DB entity you want to delete from eg {@link com.atlassian.jira.model.querydsl.QIssue#ISSUE}
     * @return a builder to create your delete statement.
     */
    SQLDeleteClause delete(RelationalPath<?> entity);

    void setAutoCommit(boolean autoCommit);

    void commit();

    void rollback();
}
