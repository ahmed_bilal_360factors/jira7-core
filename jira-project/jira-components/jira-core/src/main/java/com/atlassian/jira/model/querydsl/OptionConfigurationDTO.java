package com.atlassian.jira.model.querydsl;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Generated;

/**
 * Data Transfer Object for the OptionConfiguration entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 *
 * @see QOptionConfiguration
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class OptionConfigurationDTO implements DTO {
    private final Long id;
    private final String fieldid;
    private final String optionid;
    private final Long fieldconfig;
    private final Long sequence;

    public Long getId() {
        return id;
    }

    public String getFieldid() {
        return fieldid;
    }

    public String getOptionid() {
        return optionid;
    }

    public Long getFieldconfig() {
        return fieldconfig;
    }

    public Long getSequence() {
        return sequence;
    }

    public OptionConfigurationDTO(Long id, String fieldid, String optionid, Long fieldconfig, Long sequence) {
        this.id = id;
        this.fieldid = fieldid;
        this.optionid = optionid;
        this.fieldconfig = fieldconfig;
        this.sequence = sequence;
    }

    /**
     * Creates a GenericValue object from the values in this Data Transfer Object.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
     * @return a GenericValue object constructed from the values in this Data Transfer Object.
     */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator) {
        return ofBizDelegator.makeValue("OptionConfiguration", new FieldMap()
                .add("id", id)
                .add("fieldid", fieldid)
                .add("optionid", optionid)
                .add("fieldconfig", fieldconfig)
                .add("sequence", sequence)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */
    public static OptionConfigurationDTO fromGenericValue(GenericValue gv) {
        return new OptionConfigurationDTO(
                gv.getLong("id"),
                gv.getString("fieldid"),
                gv.getString("optionid"),
                gv.getLong("fieldconfig"),
                gv.getLong("sequence")
        );
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(OptionConfigurationDTO optionConfigurationDTO) {
        return new Builder(optionConfigurationDTO);
    }

    public static class Builder {
        private Long id;
        private String fieldid;
        private String optionid;
        private Long fieldconfig;
        private Long sequence;

        public Builder() {
        }

        public Builder(OptionConfigurationDTO optionConfigurationDTO) {
            this.id = optionConfigurationDTO.id;
            this.fieldid = optionConfigurationDTO.fieldid;
            this.optionid = optionConfigurationDTO.optionid;
            this.fieldconfig = optionConfigurationDTO.fieldconfig;
            this.sequence = optionConfigurationDTO.sequence;
        }

        public OptionConfigurationDTO build() {
            return new OptionConfigurationDTO(id, fieldid, optionid, fieldconfig, sequence);
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }
        public Builder fieldid(String fieldid) {
            this.fieldid = fieldid;
            return this;
        }
        public Builder optionid(String optionid) {
            this.optionid = optionid;
            return this;
        }
        public Builder fieldconfig(Long fieldconfig) {
            this.fieldconfig = fieldconfig;
            return this;
        }
        public Builder sequence(Long sequence) {
            this.sequence = sequence;
            return this;
        }
    }
}