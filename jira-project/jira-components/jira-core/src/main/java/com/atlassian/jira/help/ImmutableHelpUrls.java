package com.atlassian.jira.help;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.util.dbc.Assertions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;

import javax.annotation.Nonnull;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @since v6.2.4
 */
class ImmutableHelpUrls implements HelpUrls {
    private final Map<String, HelpUrl> urls;
    private final HelpUrl defaultUrl;

    ImmutableHelpUrls(HelpUrl defaultUrl, Iterable<? extends HelpUrl> urls) {
        Assertions.notNull(urls);

        this.defaultUrl = Assertions.notNull(defaultUrl);

        final Map<String, HelpUrl> index = Maps.newHashMap();
        for (HelpUrl url : urls) {
            index.put(url.getKey(), url);
        }
        index.put(defaultUrl.getKey(), defaultUrl);

        this.urls = ImmutableMap.copyOf(index);
    }

    @Nonnull
    @Override
    public HelpUrl getUrl(@Nonnull final String key) {
        final HelpUrl helpUrl = urls.get(Assertions.notNull("key", key));
        return helpUrl == null ? defaultUrl : helpUrl;
    }

    @Nonnull
    @Override
    public HelpUrl getDefaultUrl() {
        return defaultUrl;
    }

    @Nonnull
    @Override
    public Set<String> getUrlKeys() {
        return urls.keySet();
    }

    @Nonnull
    @Override
    public HelpUrl getUrlForApplication(@Nonnull final ApplicationKey applicationKey, @Nonnull final String key) {
        return getUrl(key);
    }

    @Override
    public Iterator<HelpUrl> iterator() {
        return urls.values().iterator();
    }

    /**
     * Merges two {@code ImmutableHelpUrl}'s objects into one by flattening {@code HelpUrl}'s form first and then
     * from second object into single {@code ImmutableHelpUrls}. This results in {@code HelpUrl} from second object
     * overwriting {@code HelpUrl} from the first object in case they have identical key.
     * <p>
     * Default link is taken from first object.
     *
     * @param first  help urls object
     * @param second help urls object
     * @return merged help urls object
     */
    @Nonnull
    public static ImmutableHelpUrls merge(@Nonnull final ImmutableHelpUrls first, @Nonnull final ImmutableHelpUrls second) {
        return new ImmutableHelpUrls(first.getDefaultUrl(), Iterables.concat(first.urls.values(), second.urls.values()));
    }
}
