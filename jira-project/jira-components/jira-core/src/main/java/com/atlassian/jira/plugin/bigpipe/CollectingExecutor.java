package com.atlassian.jira.plugin.bigpipe;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Only collects runnable submitted by execute method.
 *
 * @since v7.1
 */
public class CollectingExecutor implements Executor {
    /**
     * The priority used for ordinary runnables that don't specify a priority themselves.
     *
     * @see PrioritizedRunnable
     */
    public static final int DEFAULT_PRIORITY = 100;

    private final Queue<PrioritizedRunnable> queue = new PriorityBlockingQueue<>();
    private volatile boolean closed;

    @Override
    public void execute(final @Nonnull Runnable command) {
        if (closed) {
            throw new IllegalStateException("Cannot execute new commands - executor is closed");
        }

        PrioritizedRunnable prioritizedCommand;
        if (command instanceof PrioritizedRunnable) {
            prioritizedCommand = (PrioritizedRunnable) command;
        } else {
            prioritizedCommand = new PrioritizedRunnable(DEFAULT_PRIORITY, command);
        }

        queue.add(prioritizedCommand);
    }

    public void close() {
        closed = true;
    }

    public Optional<Runnable> pop() {
        return Optional.ofNullable(queue.poll());
    }

    /**
     * @return an executor wrapping this one, where all runnables are assigned the specified priority.  If priority
     * is null, the {@linkplain #DEFAULT_PRIORITY default} is used.
     */
    public Executor prioritized(@Nullable Integer priority) {
        if (priority == null) {
            priority = DEFAULT_PRIORITY;
        }
        return new PrioritizedExecutor(priority);
    }

    private class PrioritizedExecutor implements Executor {
        private final int priority;

        public PrioritizedExecutor(int priority) {
            this.priority = priority;
        }

        @Override
        public void execute(@Nonnull Runnable command) {
            CollectingExecutor.this.execute(new PrioritizedRunnable(priority, command));
        }
    }
}
