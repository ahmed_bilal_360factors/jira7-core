package com.atlassian.jira.issue.worklog;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.issue.worklog.DeletedWorklog;
import com.atlassian.jira.config.group.GroupConfigurable;
import com.atlassian.jira.entity.EntityPagedList;
import com.atlassian.jira.entity.WorklogEntityFactory;
import com.atlassian.jira.event.worklog.WorklogDeletedEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.transaction.Transaction;
import com.atlassian.jira.transaction.Txn;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.PagedList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.ofbiz.core.entity.EntityCondition;
import org.ofbiz.core.entity.EntityFieldMap;
import org.ofbiz.core.entity.EntityOperator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Set;

import static com.google.common.base.Preconditions.checkArgument;

public class DefaultWorklogManager implements WorklogManager, GroupConfigurable {

    private static final Logger log = LoggerFactory.getLogger(DefaultWorklogManager.class);

    private final ProjectRoleManager projectRoleManager;
    private final WorklogStore worklogStore;
    private final TimeTrackingIssueUpdater timeTrackingIssueUpdater;
    private final EventPublisher eventPublisher;

    public DefaultWorklogManager(ProjectRoleManager projectRoleManager,
                                 WorklogStore worklogStore,
                                 TimeTrackingIssueUpdater timeTrackingIssueUpdater,
                                 EventPublisher eventPublisher) {
        this.projectRoleManager = projectRoleManager;
        this.worklogStore = worklogStore;
        this.timeTrackingIssueUpdater = timeTrackingIssueUpdater;
        this.eventPublisher = eventPublisher;
    }

    @Override
    public boolean delete(ApplicationUser user, Worklog worklog, Long newEstimate, boolean dispatchEvent) {
        validateWorklog(worklog, false);
        final boolean isWorklogDeleted = worklogStore.delete(worklog.getId());
        if (isWorklogDeleted) {
            timeTrackingIssueUpdater.updateIssueOnWorklogDelete(user, worklog, newEstimate, dispatchEvent);
        }

        return isWorklogDeleted;
    }

    @Override
    public Worklog create(ApplicationUser user, Worklog worklog, Long newEstimate, boolean dispatchEvent) {
        validateWorklog(worklog, true);

        Worklog newWorklog = worklogStore.create(worklog);

        // Update the issues time tracking fields
        timeTrackingIssueUpdater.updateIssueOnWorklogCreate(user, newWorklog, newEstimate, dispatchEvent);

        return newWorklog;
    }

    @Override
    public Worklog update(ApplicationUser user, Worklog worklog, Long newEstimate, boolean dispatchEvent) {
        validateWorklog(worklog, false);

        // We need to lookup the value as stored in the DB so that we can determine the original value of the
        // timeSpent so that we can correctly recalculate the issues total time spent field.
        Worklog originalWorklog = getById(worklog.getId());
        if (originalWorklog == null) {
            throw new IllegalArgumentException("Unable to find a worklog in the datastore for the provided id: '" + worklog.getId() + "'");
        }

        Long originalTimeSpent = originalWorklog.getTimeSpent();
        Worklog newWorklog = worklogStore.update(worklog);

        // Update the issues time tracking fields
        timeTrackingIssueUpdater.updateIssueOnWorklogUpdate(user, originalWorklog, newWorklog, originalTimeSpent, newEstimate, dispatchEvent);

        return newWorklog;
    }

    @Override
    public Worklog getById(Long id) {
        return worklogStore.getById(id);
    }

    @Override
    public List<Worklog> getByIssue(Issue issue) {
        if (issue == null) {
            throw new IllegalArgumentException("Cannot resolve worklogs for null issue.");
        }
        return worklogStore.getByIssue(issue);
    }

    @Override
    public PagedList<Worklog> getByIssue(final Issue issue, final int pageSize) {
        final WorklogEntityFactory worklogEntityFactory = new WorklogEntityFactory(issue, projectRoleManager);
        EntityCondition condition = new EntityFieldMap(ImmutableMap.<String, Object>of("issue", issue.getId()), EntityOperator.EQUALS);
        List<String> orderBy = Lists.newArrayList("created");
        return new EntityPagedList<>(pageSize, worklogEntityFactory, condition, orderBy);
    }

    @Override
    public int swapWorklogGroupRestriction(String groupName, String swapGroup) {
        if (groupName == null) {
            throw new IllegalArgumentException("You must provide a non null group name.");
        }

        if (swapGroup == null) {
            throw new IllegalArgumentException("You must provide a non null swap group name.");
        }

        return worklogStore.swapWorklogGroupRestriction(groupName, swapGroup);
    }

    @Override
    public long getCountForWorklogsRestrictedByGroup(String groupName) {
        if (groupName == null) {
            throw new IllegalArgumentException("You must provide a non null group name.");
        }
        return worklogStore.getCountForWorklogsRestrictedByGroup(groupName);
    }

    @Override
    public long getCountForWorklogsRestrictedByRole(Long roleId) {
        if (roleId == null) {
            throw new IllegalArgumentException("You must provide a non null role id.");
        }
        return worklogStore.getCountForWorklogsRestrictedByRole(roleId);
    }

    @Override
    public int swapWorklogRoleRestriction(Long roleId, Long swapRoleId) {
        if (roleId == null) {
            throw new IllegalArgumentException("You must provide a non null role id.");
        }

        if (swapRoleId == null) {
            throw new IllegalArgumentException("You must provide a non null swap role id.");
        }

        return worklogStore.swapWorklogRoleRestriction(roleId, swapRoleId);
    }

    @Override
    public ProjectRole getProjectRole(Long projectRoleId) {
        return projectRoleManager.getProjectRole(projectRoleId);
    }

    @Override
    public List<Worklog> getWorklogsUpdatedSince(final Long sinceInMilliseconds) {
        checkArgument(sinceInMilliseconds != null, "The time in milliseconds must not be null.");
        return worklogStore.getWorklogsUpdateSince(sinceInMilliseconds, WORKLOG_UPDATE_DATA_PAGE_SIZE);
    }

    @Override
    public List<DeletedWorklog> getWorklogsDeletedSince(final Long sinceInMilliseconds) {
        checkArgument(sinceInMilliseconds != null, "The time in milliseconds must not be null.");
        return worklogStore.getWorklogsDeletedSince(sinceInMilliseconds, WORKLOG_UPDATE_DATA_PAGE_SIZE);
    }

    @Override
    public Set<Worklog> getWorklogsForIds(final Set<Long> idsOfWorklogs) {
        checkArgument(idsOfWorklogs != null, "Ids of worklogs must not be null.");
        checkArgument(idsOfWorklogs.size() <= WORKLOG_UPDATE_DATA_PAGE_SIZE, "The size of set with ids must less than " + WORKLOG_UPDATE_DATA_PAGE_SIZE);

        return worklogStore.getWorklogsForIds(idsOfWorklogs, WORKLOG_UPDATE_DATA_PAGE_SIZE);
    }

    @Override
    public void deleteWorklogsForIssue(final Issue issue) {
        // There should be a transaction already opened by IssueDeleteHelper. If the transaction is already opened
        // this will simply use the existing transaction. However, being future proof, if someone uses
        // this method outside of the transaction from IssueDeleteHelper, we open a new one.
        // Going one, the purpose of this transaction is to ensure, that we will publish events for all worklogs
        // which are removed. We don't want a worklog to be added between the call to getByIssue and deleteWorklogsForIssue.
        final Transaction transaction = Txn.begin();
        final List<Worklog> worklogsForIssue;
        try {
            worklogsForIssue = worklogStore.getByIssue(issue);
            worklogStore.deleteWorklogsForIssue(issue);

            transaction.commit();

            worklogsForIssue.stream()
                    .map(WorklogDeletedEvent::new)
                    .forEach(eventPublisher::publish);
        } catch (Exception e) {
            log.error("Error when deleting worklogs for issue " + issue.getId(), e);
        } finally {
            transaction.finallyRollbackIfNotCommitted();
        }
    }

    void validateWorklog(Worklog worklog, boolean create) {
        if (worklog == null) {
            throw new IllegalArgumentException("Worklog must not be null.");
        }

        if (worklog.getIssue() == null) {
            throw new IllegalArgumentException("The worklogs issue must not be null.");
        }

        if (!create && worklog.getId() == null) {
            throw new IllegalArgumentException("Can not modify a worklog with a null id.");
        }
    }

    @Override
    public boolean isGroupUsed(@Nonnull final Group group) {
        return worklogStore.getCountForWorklogsRestrictedByGroup(group.getName()) > 0;
    }
}
