package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.util.dbc.Assertions.containsNoNulls;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Represents the collection of licenses in renaissance migration (i.e. JIRA 6.x single use permission to JIRA 7.x
 * multiple application)
 *
 * @since v7.0
 */
public final class Licenses {
    private final ImmutableMap<ApplicationKey, License> index;
    private final ImmutableSet<License> licenses;

    public Licenses(Iterable<? extends License> licenses) {
        containsNoNulls("licenses", licenses);

        final Map<ApplicationKey, License> index = new HashMap<>();
        for (License newLicense : licenses) {
            for (ApplicationKey applicationKey : newLicense.applicationKeys()) {
                final License oldLicense = index.put(applicationKey, newLicense);
                if (oldLicense != null && !oldLicense.equals(newLicense)) {
                    throw new MigrationFailedException(String.format("More than once license contains access to '%s'. "
                            + "The licenses are: %s and %s.", applicationKey, oldLicense, newLicense));

                }
            }
        }

        this.licenses = ImmutableSet.copyOf(licenses);
        this.index = ImmutableMap.copyOf(index);
    }

    private Licenses(ImmutableSet<License> licenses, ImmutableMap<ApplicationKey, License> index) {
        this.licenses = licenses;
        this.index = index;
    }

    public boolean isEmpty() {
        return licenses.isEmpty();
    }

    Set<License> get() {
        return licenses;
    }

    Set<ApplicationKey> keys() {
        return index.keySet();
    }

    Option<License> license(ApplicationKey applicationKey) {
        return Option.option(index.get(applicationKey));
    }

    boolean canAdd(final License license) {
        notNull("license", license);

        return Collections.disjoint(license.applicationKeys(), keys());
    }

    /**
     * Add a license. This method will throw an exception if any of the applications in the passed license are already
     * licensed.
     *
     * @param license the license to add.
     * @return a new {@link Licenses} object containing the passed license.
     * @throws MigrationFailedException if the passed license is already licensed.
     */
    public Licenses addLicense(License license) {
        notNull("license", license);

        if (licenses.contains(license)) {
            return this;
        }

        final ImmutableMap.Builder<ApplicationKey, License> newIndex = ImmutableMap.<ApplicationKey, License>builder()
                .putAll(this.index);

        for (final ApplicationKey applicationKey : license.applicationKeys()) {
            final License oldLicense = index.get(applicationKey);
            if (oldLicense != null) {
                throw new MigrationFailedException(String.format("Current license already contains access to '%s'. "
                        + "The old license is: %s", applicationKey, oldLicense));
            }
            newIndex.put(applicationKey, license);
        }

        final ImmutableSet<License> licenses = ImmutableSet.<License>builder()
                .addAll(this.licenses)
                .add(license)
                .build();

        return new Licenses(licenses, newIndex.build());
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Licenses otherLicenses = (Licenses) o;
        return licenses.equals(otherLicenses.licenses);

    }

    @Override
    public int hashCode() {
        return licenses.hashCode();
    }

    @Override
    public String toString() {
        return licenses.toString();
    }
}
