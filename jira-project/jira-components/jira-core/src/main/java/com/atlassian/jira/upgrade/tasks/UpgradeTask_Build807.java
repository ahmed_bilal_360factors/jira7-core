package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.upgrade.AbstractUpgradeTask;
import com.atlassian.jira.upgrade.UpgradeTask;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;

/**
 * Remove any only draft workflows schemes that might be around after a downgrade from OnDemand 5.2
 * to BTF 5.1.x.
 *
 * @since v5.2
 */
public class UpgradeTask_Build807 extends AbstractUpgradeTask {
    private final OfBizDelegator entityEngine;

    public UpgradeTask_Build807(OfBizDelegator entityEngine) {
        this.entityEngine = entityEngine;
    }

    @Override
    public int getBuildNumber() {
        return 807;
    }

    @Override
    public String getShortDescription() {
        return "Removing old Draft Workflow Schemes";
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        // Flagged as not downgradable because it was created before the Downgrade Task framework was built.
        return true;
    }

    @Override
    public void doUpgrade(boolean setupMode) {
        entityEngine.removeByAnd("DraftWorkflowSchemeEntity", Collections.<String, String>emptyMap());
        entityEngine.removeByAnd("DraftWorkflowScheme", Collections.<String, String>emptyMap());
    }

    @Override
    public Collection<String> getErrors() {
        return Collections.emptyList();
    }

    @Nullable
    @Override
    public Integer dependsUpon() {
        return 802;
    }


    @Override
    public ScheduleOption getScheduleOption() {
        return ScheduleOption.BEFORE_JIRA_STARTED;
    }

}
