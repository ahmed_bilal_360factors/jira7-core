package com.atlassian.jira.web.action.filter;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.filter.FilterSubscriptionService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.issue.search.managers.IssueSearcherManager;
import com.atlassian.jira.issue.subscription.FilterSubscription;
import com.atlassian.jira.issue.subscription.SubscriptionManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.bean.FilterUtils;
import com.atlassian.jira.web.component.cron.CronEditorBean;
import com.atlassian.jira.web.component.cron.CronEditorWebComponent;
import com.atlassian.jira.web.component.cron.generator.CronExpressionGenerator;
import com.atlassian.jira.web.component.cron.parser.CronExpressionParser;
import com.opensymphony.util.TextUtils;
import org.ofbiz.core.entity.GenericEntityException;
import webwork.action.ActionContext;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;

import static com.atlassian.jira.security.Permissions.MANAGE_GROUP_FILTER_SUBSCRIPTIONS;

/**
 * Action for CRUD of a scheduled email subscription to search filter results.
 */
public class EditSubscription extends AbstractFilterAction implements FilterOperationsAction {
    private Long subId = null;
    private String groupName;
    private Boolean emailOnEmpty = Boolean.FALSE;
    private Date lastRun;
    private Date nextRun;
    private SubscriptionManager subscriptionManager = ComponentAccessor.getSubscriptionManager();
    private CronEditorBean cronEditorBean;
    private final FilterSubscriptionService filterSubscriptionService;


    public EditSubscription(IssueSearcherManager issueSearcherManager, FilterSubscriptionService filterSubscriptionService,
                            SearchService searchService) {
        super(issueSearcherManager, searchService);
        this.filterSubscriptionService = filterSubscriptionService;
    }

    public String doDefault() throws Exception {
        if (subId == null && getFilterId() == null) {
            addErrorMessage(getText("filtersubscription.please.select.a.subscription.or.filter"));
            return ERROR;
        } else {
            if (subId != null) {
                FilterSubscription subscription = subscriptionManager.getFilterSubscription(getLoggedInUser(), subId);
                if (subscription == null) {
                    return PERMISSION_VIOLATION_RESULT;
                } else {
                    groupName = subscription.getGroupName();
                    emailOnEmpty = subscription.isEmailOnEmpty();
                    lastRun = subscription.getLastRunTime();

                    //Get from the trigger
                    String cronExpression = subscriptionManager.getCronExpressionForSubscription(subscription);
                    CronExpressionParser cronExpresionParser = new CronExpressionParser(cronExpression);
                    cronEditorBean = cronExpresionParser.getCronEditorBean();

                    nextRun = null;// TODO new Timestamp(trigger.getNextFireTime().getTime());
                }
            }

            if (getFilterId() != null) {
                if (getFilter() == null) {
                    return ERROR;
                }
            }
        }
        return super.doDefault();
    }


    public void doValidation() {
        cronEditorBean = new CronEditorBean("filter.subscription.prefix", ActionContext.getParameters());
        CronEditorWebComponent component = new CronEditorWebComponent();
        addErrorCollection(component.validateInput(cronEditorBean, "cron.editor.name"));
        if (!hasAnyErrors()) {
            JiraServiceContext serviceContext = getJiraServiceContext();
            String cronString = component.getCronExpressionFromInput(cronEditorBean);
            filterSubscriptionService.validateCronExpression(serviceContext, cronString);
        }
    }

    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        String cronExpression = new CronExpressionGenerator().getCronExpressionFromInput(cronEditorBean);
        if (subId != null) {
            // we have a subscription id so we are editing it
            FilterSubscription subscription = subscriptionManager.getFilterSubscription(getLoggedInUser(), subId);
            if (subscription == null) {
                return PERMISSION_VIOLATION_RESULT;
            }


            filterSubscriptionService.updateSubscription(getJiraServiceContext(), subId, getGroupName(), cronExpression, emailOnEmpty);
        } else {
            // no subscription id so we are creating a new one
            if (getFilter() == null) {
                return "securitybreach";
            }
            filterSubscriptionService.storeSubscription(getJiraServiceContext(), getFilterId(), getGroupName(), cronExpression, getEmailOnEmpty());
        }
        if (isInlineDialogMode()) {
            return returnComplete(getReturnUrl());
        } else {
            return (getReturnUrl() == null ?
                    getRedirect("ViewSubscriptions.jspa?filterId=" + getFilterId()) :
                    getRedirect(getReturnUrl()));
        }
    }

    @RequiresXsrfCheck
    public String doDelete() throws Exception {
        FilterSubscription subscription = subscriptionManager.getFilterSubscription(getLoggedInUser(), subId);

        if (subscription == null) {
            addErrorMessage(getText("subscriptions.error.delete.subscriptiondoesnotexist"));
            return ERROR;
        }
        ComponentAccessor.getSubscriptionManager().deleteSubscription(subId);
        return getRedirect("ViewSubscriptions.jspa?filterId=" + getFilterId());
    }

    @RequiresXsrfCheck
    public String doRunNow() throws Exception {
        FilterSubscription subscription = subscriptionManager.getFilterSubscription(getLoggedInUser(), subId);

        if (subscription == null) {
            addErrorMessage(getText("subscriptions.error.runnow.subscriptiondoesnotexist"));
            return ERROR;
        }

        ComponentAccessor.getSubscriptionManager().runSubscription(getLoggedInUser(), subId);
        return getRedirect("ViewSubscriptions.jspa?filterId=" + getFilterId());
    }

    public boolean hasGroupPermission() throws GenericEntityException {
        return ComponentAccessor.getPermissionManager().hasPermission(MANAGE_GROUP_FILTER_SUBSCRIPTIONS, getLoggedInUser());
    }

    public String getSubmitName() throws GenericEntityException {
        if (subId == null) {
            return getText("filtersubscription.subscribe");
        } else {
            return getText("common.forms.update");
        }
    }

    public String getCancelStr() throws GenericEntityException {
        if (getReturnUrl() != null) {
            return getReturnUrl();
        } else if (subId == null) {
            return "ManageFilters.jspa";
        } else {
            return "ViewSubscriptions.jspa?filterId=" + getFilterId();
        }
    }

    public String getSubId() {
        if (subId == null) {
            return null;
        } else {
            return subId.toString();
        }
    }

    public void setSubId(String subId) {
        if (TextUtils.stringSet(subId)) {
            this.subId = new Long(subId);
        }
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Boolean getEmailOnEmpty() {
        return emailOnEmpty;
    }

    public void setEmailOnEmpty(Boolean emailOnEmpty) {
        this.emailOnEmpty = emailOnEmpty;
    }

    public String getLastRun() {
        if (lastRun == null) {
            return null;
        } else {
            return String.valueOf(lastRun.getTime());
        }
    }

    public void setLastRun(String lastRun) {
        if (TextUtils.stringSet(lastRun)) {
            this.lastRun = new Timestamp(Long.parseLong(lastRun));
        }
    }

    public String getNextRun() {
        if (nextRun == null) {
            return null;
        } else {
            return String.valueOf(nextRun.getTime());
        }
    }

    public void setNextRun(String nextRun) {
        if (TextUtils.stringSet(nextRun)) {
            this.nextRun = new Timestamp(Long.parseLong(nextRun));
        }
    }

    public String getLastRunStr() {
        return formatDate(lastRun);
    }

    public String getNextRunStr() {
        return formatDate(nextRun);
    }

    private String formatDate(Date date) {
        if (date == null) {
            return null;
        }
        return ComponentAccessor.getComponent(DateTimeFormatterFactory.class).formatter().withLocale(getLocale())
                .withStyle(DateTimeStyle.COMPLETE).format(date);
    }

    public Collection getGroups() {
        return FilterUtils.getGroups(getLoggedInUser());
    }

    public CronEditorBean getCronEditorBean() {
        if (cronEditorBean == null) {
            cronEditorBean = new CronExpressionParser().getCronEditorBean();
        }
        return cronEditorBean;
    }

    public String getHelpLink() {
        return "issue_filters_subscribing";
    }

}
