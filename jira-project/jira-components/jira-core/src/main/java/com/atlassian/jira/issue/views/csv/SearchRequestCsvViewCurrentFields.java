package com.atlassian.jira.issue.views.csv;

import java.util.List;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.export.customfield.CsvIssueExporter;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.component.TableLayoutFactory;

/**
 * Export all of the fields currently selected by the current user in their search.
 *
 * @since 7.2.0
 */
public class SearchRequestCsvViewCurrentFields extends AbstractCsvSearchRequestView {
    private final JiraAuthenticationContext authenticationContext;
    private final TableLayoutFactory tableLayoutFactory;

    public SearchRequestCsvViewCurrentFields(final ApplicationProperties applicationProperties,
                                             final CsvIssueExporter csvIssueExporter,
                                             final JiraAuthenticationContext authenticationContext,
                                             final TableLayoutFactory tableLayoutFactory) {
        super(applicationProperties, csvIssueExporter);
        this.authenticationContext = authenticationContext;
        this.tableLayoutFactory = tableLayoutFactory;
    }

    @Override
    protected List<Field> getFieldsToBeExported(SearchRequest searchRequest) {
        return tableLayoutFactory.getCurrentUserCsvColumnsFields(searchRequest, authenticationContext.getLoggedInUser());
    }
}
