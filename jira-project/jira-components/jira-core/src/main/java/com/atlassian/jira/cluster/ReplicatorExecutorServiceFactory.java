package com.atlassian.jira.cluster;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.config.properties.SystemPropertyKeys;
import com.google.common.util.concurrent.RateLimiter;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ReplicatorExecutorServiceFactory {
    private static final Logger LOG = LoggerFactory.getLogger(ReplicatorExecutorServiceFactory.class);

    private static final String THREAD_NAMING_FORMAT = "ehcache-replicator-%s";
    private static final int DEFAULT_REPLICATION_THREAD_COUNT = 20;

    private static ExecutorService executorService;

    public static synchronized void init() {
        LOG.info("EhCache replication thread pool initialization");
        final Integer threadCount = getThreadCount();

        executorService = new ThreadPoolExecutor(
                threadCount,
                threadCount,
                0L,
                TimeUnit.MILLISECONDS,
                new SynchronousQueue<>(),
                new ThreadFactoryBuilder().setNameFormat(THREAD_NAMING_FORMAT).build(),
                new RunInCallingThread()
        );
    }

    public static synchronized void shutdown() {
        if (executorService != null) {
            LOG.info("EhCache replication thread pool shutdown");
            executorService.shutdown();
        }
    }

    public static ExecutorService getExecutorService() {
        return executorService;
    }

    private static Integer getThreadCount() {
        final JiraProperties jiraProperties = JiraSystemProperties.getInstance();
        return jiraProperties.getInteger(SystemPropertyKeys.CACHE_PARALLEL_REPLICATION_THREAD_COUNT,
                DEFAULT_REPLICATION_THREAD_COUNT);
    }

    private static class RunInCallingThread implements RejectedExecutionHandler {
        private static final String WARNING_MESSAGE = "Cache replication thread pool is too small. " +
                "Try increasing number of threads by setting system property {}";

        private final RateLimiter limiter = RateLimiter.create(0.1d);

        @Override
        public void rejectedExecution(final Runnable r, final ThreadPoolExecutor executor) {
            logWarning();
            r.run();
        }

        private void logWarning() {
            if (LOG.isDebugEnabled()) {
                LOG.debug(WARNING_MESSAGE, SystemPropertyKeys.CACHE_PARALLEL_REPLICATION_THREAD_COUNT, new Throwable());
            } else if (limiter.tryAcquire()) {
                LOG.warn(WARNING_MESSAGE, SystemPropertyKeys.CACHE_PARALLEL_REPLICATION_THREAD_COUNT);
            }
        }
    }
}
