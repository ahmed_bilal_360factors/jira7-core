package com.atlassian.jira.plugin.bigpipe;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.google.common.base.Strings;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

/**
 * Standard bigpipe priority configurer that uses the
 * <code>{@value BigPipeWebPanelModuleDescriptor#PARAM_BIG_PIPE_PRIORITY}</code> parameter from the resource descriptor
 * to configure the priority.
 *
 * @since v7.1
 */
public class GlobalPriorityConfigurer implements BigPipePriorityConfigurer {
    @Override
    @Nullable
    public Integer calculatePriority(@Nonnull ResourceDescriptor resource, @Nonnull Map<String, ?> context) {
        final String priorityStr = resource.getParameter(BigPipeWebPanelModuleDescriptor.PARAM_BIG_PIPE_PRIORITY);
        final Integer priority;
        if (Strings.isNullOrEmpty(priorityStr)) {
            priority = null;
        } else {
            try {
                priority = Integer.parseInt(priorityStr);
            } catch (NumberFormatException e) {
                throw new PluginParseException("Invalid " + BigPipeWebPanelModuleDescriptor.PARAM_BIG_PIPE_PRIORITY + ": " + priorityStr, e);
            }
        }

        return priority;
    }
}
