package com.atlassian.jira.action.component;

import com.atlassian.jira.bc.project.component.MutableProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentService;
import com.atlassian.jira.project.ComponentAssigneeTypes;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import org.ofbiz.core.entity.GenericEntityException;

import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;

public class SelectComponentAssigneesUtilImpl implements SelectComponentAssigneesUtil {
    private Map<ProjectComponent, Long> componentAssigneeTypes;
    private String fieldPrefix;
    private final JiraAuthenticationContext authenticationContext;
    private final ProjectComponentService projectComponentService;
    private PermissionManager permissionManager;

    public SelectComponentAssigneesUtilImpl(final JiraAuthenticationContext authenticationContext,
                                            final ProjectComponentService projectComponentService, final PermissionManager permissionManager) {
        this.authenticationContext = authenticationContext;
        this.projectComponentService = projectComponentService;
        this.permissionManager = permissionManager;
    }

    public ErrorCollection validate() {
        ErrorCollection errorCollection = new SimpleErrorCollection();

        final Set<ProjectComponent> components = componentAssigneeTypes.keySet();
        Long assigneeType;
        for (ProjectComponent component : components) {
            assigneeType = componentAssigneeTypes.get(component);
            if (!isAssigneeTypeValid(component, assigneeType)) {
                errorCollection.addError(fieldPrefix + component.getId(),
                        authenticationContext.getI18nHelper().getText("admin.errors.invalid.default.assignee"));
            }
        }
        return errorCollection;
    }

    @Override
    public boolean hasPermission(Project project, ApplicationUser user) {
        return permissionManager.hasPermission(Permissions.ADMINISTER, user) ||
                permissionManager.hasPermission(ADMINISTER_PROJECTS, project, user);

    }

    public ErrorCollection execute(ApplicationUser user) throws GenericEntityException {
        ErrorCollection errorCollection = new SimpleErrorCollection();

        final Set<Map.Entry<ProjectComponent, Long>> componentEntries = componentAssigneeTypes.entrySet();
        for (final Map.Entry<ProjectComponent, Long> entry : componentEntries) {
            final Long assigneeType = entry.getValue();
            final Long componentId = entry.getKey().getId();
            MutableProjectComponent projectComponent = MutableProjectComponent.copy(projectComponentService.find(user, errorCollection, componentId));
            projectComponent.setAssigneeType(assigneeType);
            projectComponentService.update(user, errorCollection, projectComponent);
        }

        return errorCollection;
    }

    private boolean isAssigneeTypeValid(ProjectComponent component, Long assigneeType) {
        return ComponentAssigneeTypes.isAssigneeTypeValid(component.getGenericValue(), assigneeType);
    }

    public Map getComponentAssigneeTypes() {
        return componentAssigneeTypes;
    }

    public void setComponentAssigneeTypes(Map<ProjectComponent, Long> componentAssigneeTypes) {
        this.componentAssigneeTypes = componentAssigneeTypes;
    }

    public String getFieldPrefix() {
        return fieldPrefix;
    }

    public void setFieldPrefix(String fieldPrefix) {
        this.fieldPrefix = fieldPrefix;
    }
}
