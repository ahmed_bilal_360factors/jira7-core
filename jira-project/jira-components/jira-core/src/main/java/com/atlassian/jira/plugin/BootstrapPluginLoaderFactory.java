package com.atlassian.jira.plugin;

import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.loaders.PluginLoader;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * This is the bootstrap plugin loader factory of JIRA.  Its similar to {@link DefaultPluginLoaderFactory}
 * but only allows certain bundled plugin through to be loaded.  Just enough to get JIRA bootstrapped and
 * no more.
 */
public class BootstrapPluginLoaderFactory implements PluginLoaderFactory {
    private static final Logger log = LoggerFactory.getLogger(BootstrapPluginLoaderFactory.class);

    private final PluginFactoryAndLoaderRegistrar pluginFactoryAndLoaderRegistrar;
    private final BootstrapPluginDirectoryLoaderFactory bootstrapPluginDirectoryLoaderFactory;

    public BootstrapPluginLoaderFactory(final PluginFactoryAndLoaderRegistrar pluginFactoryAndLoaderRegistrar, final BootstrapPluginDirectoryLoaderFactory bootstrapPluginDirectoryLoaderFactory) {
        this.pluginFactoryAndLoaderRegistrar = pluginFactoryAndLoaderRegistrar;
        this.bootstrapPluginDirectoryLoaderFactory = bootstrapPluginDirectoryLoaderFactory;
    }

    public List<PluginLoader> getPluginLoaders() {
        //
        // we only allow the aui plugin bundled plugin through at this stage and language packs
        // you must make sure these plugins are always available in Phase 1
        ArrayList<Pattern> pluginWhitelist = Lists.newArrayList(
                Pattern.compile("jquery-[0-9]+(?:\\.[0-9])+\\.jar"),
                Pattern.compile("auiplugin-[0-9.]+.*\\.jar"),
                Pattern.compile("jira-(?:core-)?language-pack-.*\\.jar"),
                Pattern.compile("jira-languages-.*\\.jar"),
                Pattern.compile("org.apache.servicemix.bundles.javax-inject-.*\\.jar"),
                Pattern.compile("soy-template-plugin-.*\\.jar"),
                Pattern.compile("issue-status-plugin-.*\\.jar"),
                Pattern.compile("less-transformer-plugin-.*\\.jar"),
                Pattern.compile("jira-less-integration-.*\\.jar"),
                Pattern.compile("jslibs-.*\\.jar")
                // spring-scanner plugins should eventually go here once SCANNER-36 is fixed.
                // Hence plugins in bootstrap cannot use scanner 2 'provided' scope, but must embed scanner 1.2 'runtime'.
                // Pattern.compile("atlassian-spring-scanner-annotation-.*\\.jar"),
                // Pattern.compile("atlassian-spring-scanner-runtime-.*\\.jar")
        );

        final List<PluginFactory> pluginFactories = pluginFactoryAndLoaderRegistrar.getDefaultPluginFactories(pluginWhitelist);

        final CollectionBuilder<PluginLoader> pluginLoaderBuilder = CollectionBuilder.newBuilder();

        //
        // plugin loaders for our system-xxx plugins which is basically web-resources at this stage
        pluginLoaderBuilder.addAll(pluginFactoryAndLoaderRegistrar.getBootstrapSystemPluginLoaders());

        //
        // for loading plugin1 plugins the old way (ie. via WEB-INF/lib) which for bootstrapping
        // we dont want.  I have left the code here commented out so you know how it differs to normal
        // plugins loading
        //
        // pluginLoaderBuilder.add(new ClassPathPluginLoader());

        if (JiraSystemProperties.isBundledPluginsDisabled()) {
            log.warn("Bundled plugins have been disabled. Removing bundled plugin loader.");
        } else {
            pluginLoaderBuilder.add(pluginFactoryAndLoaderRegistrar.getBundledPluginsLoader(pluginFactories));
        }

        pluginLoaderBuilder.add(bootstrapPluginDirectoryLoaderFactory.getDirectoryPluginLoader(pluginFactories));

        return pluginLoaderBuilder.asList();
    }

}
