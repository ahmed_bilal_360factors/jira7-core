package com.atlassian.jira.security.groups;

import com.atlassian.jira.model.querydsl.QUser;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.sql.SQLExpressions;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.function.Function;

/**
 * This class builds {@link BooleanExpression} instances that can be used to filter
 * a QueryDSL query based on user shadowing rules and a collection of directory ids.
 */
public class UserIsNotShadowedPredicate implements Function<QUser, BooleanExpression> {
    /**
     * Higher priority directory IDs
     */
    private final Collection<Long> higherPriorityDirectoryIds;

    public UserIsNotShadowedPredicate(@Nonnull final Collection<Long> higherPriorityDirectoryIds) {
        this.higherPriorityDirectoryIds = higherPriorityDirectoryIds;
    }

    /**
     * Build a predicate that will only include users if this directory is their canonical directory.
     * i.e. ignore users that are shadowed by a higher priority directory.
     * <p>
     * This method can return null if a predicate is not necessary. QueryDSL will gracefully ignore the null predicate.
     *
     * @param u the user table
     * @return the predicate, or null if there are no higher priority directories.
     */
    @Nullable
    public BooleanExpression apply(final QUser u) {
        if (higherPriorityDirectoryIds.isEmpty()) {
            return null;
        }
        QUser u2 = new QUser("u2");
        return SQLExpressions
                .select(u2)
                .from(u2)
                .where(u2.lowerUserName.eq(u.lowerUserName)
                        .and(u2.directoryId.in(higherPriorityDirectoryIds)))
                .notExists();
    }

}
