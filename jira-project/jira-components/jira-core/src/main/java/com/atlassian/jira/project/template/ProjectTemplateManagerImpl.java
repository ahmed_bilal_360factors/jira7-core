package com.atlassian.jira.project.template;

import com.atlassian.jira.plugin.webfragment.JiraWebInterfaceManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.project.template.descriptor.ProjectTemplateModuleDescriptor;
import com.atlassian.jira.project.template.module.ProjectTemplateModule;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class ProjectTemplateManagerImpl implements ProjectTemplateManager {
    static final ProjectTemplateKey JIRA_DEFAULT_TEMPLATE_KEY_POST_7_0 = new ProjectTemplateKey("com.atlassian.jira-core-project-templates:jira-core-project-management");

    private final PluginAccessor pluginAccessor;
    private final JiraAuthenticationContext authenticationContext;
    private final ProjectTemplateBuilderFactory builderFactory;

    public ProjectTemplateManagerImpl(
            PluginAccessor pluginAccessor,
            JiraAuthenticationContext authenticationContext,
            ProjectTemplateBuilderFactory builderFactory) {
        this.pluginAccessor = pluginAccessor;
        this.authenticationContext = authenticationContext;
        this.builderFactory = builderFactory;
    }

    @Override
    public List<ProjectTemplate> getProjectTemplates() {
        return findAllTemplateModules()
                .filter(this::isDisplayable)
                .map(this::toProjectTemplate)
                .sorted()
                .collect(toImmutableList());
    }

    @Override
    public Optional<ProjectTemplate> getProjectTemplate(ProjectTemplateKey projectTemplateKey) {
        return findByKeyVisible(projectTemplateKey);
    }

    @Override
    public ProjectTemplate getDefaultTemplate() {
        final ProjectTemplateKey key = JIRA_DEFAULT_TEMPLATE_KEY_POST_7_0;
        return getProjectTemplate(key).orElseThrow(
                () -> new IllegalStateException("The JIRA default schemes project template is not defined on this instance")
        );
    }

    private Stream<ProjectTemplateModuleDescriptor> findAllTemplateModules() {
        return pluginAccessor.getEnabledModuleDescriptorsByClass(ProjectTemplateModuleDescriptor.class).stream();
    }

    private boolean isDisplayable(ProjectTemplateModuleDescriptor moduleDescriptor) {
        //noinspection deprecation
        return moduleDescriptor.getCondition().shouldDisplay(ImmutableMap.of(
                JiraWebInterfaceManager.CONTEXT_KEY_HELPER, new JiraHelper(ExecutingHttpRequest.get()),
                JiraWebInterfaceManager.CONTEXT_KEY_USER, authenticationContext.getLoggedInUser()
        ));
    }

    private ProjectTemplate toProjectTemplate(ProjectTemplateModuleDescriptor templateModuleDescriptor) {
        I18nHelper i18nHelper = authenticationContext.getI18nHelper();

        ProjectTemplateModule template = templateModuleDescriptor.getModule();
        ProjectTemplateBuilder builder = builderFactory.newBuilder()
                .key(isBlank(template.key()) ? "" : template.key())
                .weight(template.weight())
                .name(i18nHelper.getText(template.labelKey()))
                .description(i18nHelper.getText(template.descriptionKey()))
                .iconUrl(template.icon().url())
                .backgroundIconUrl(template.backgroundIcon().url())
                .infoSoyPath(template.getInfoSoyPath())
                .projectTypeKey(template.projectTypeKey());

        template.longDescriptionKey().ifPresent(key -> builder.longDescriptionContent(i18nHelper.getText(key)));

        if (template.hasAddProjectModule()) {
            builder.addProjectModule(template.addProjectModule());
        }

        return builder.build();
    }

    private Optional<ProjectTemplate> findByKeyVisible(ProjectTemplateKey key) {
        if (key == null || key.getKey() == null) {
            return Optional.empty();
        }

        ModuleDescriptor<?> module = pluginAccessor.getEnabledPluginModule(key.getKey());
        if (module == null) {
            return Optional.empty();
        }

        if (module instanceof ProjectTemplateModuleDescriptor) {

            ProjectTemplateModuleDescriptor template = (ProjectTemplateModuleDescriptor) module;
            return Optional.ofNullable(isDisplayable(template) ? toProjectTemplate(template) : null);
        }

        return Optional.empty();
    }
}
