package com.atlassian.jira.permission.management.events;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.jira.event.AbstractEvent;
import com.google.common.base.Objects;

/**
 * Event fired when a grant operation was successfully executed
 */
@EventName("jira.projectpermissions.grant.success")
public final class SuccessfulPermissionSchemeGrantEvent extends AbstractEvent {
    private final Long schemeId;
    private final String securityType; // will be a comma,separated,list if multiple securityTypes were sent
    private final Integer numberOfPermissions;

    public SuccessfulPermissionSchemeGrantEvent(
            final Long schemeId,
            final String securityType,
            final Integer numberOfPermissions
    ) {
        this.schemeId = schemeId;
        this.securityType = securityType;
        this.numberOfPermissions = numberOfPermissions;
    }

    public Long getSchemeId() {
        return schemeId;
    }

    public String getSecurityType() {
        return securityType;
    }

    public Integer getNumberOfPermissions() {
        return numberOfPermissions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SuccessfulPermissionSchemeGrantEvent that = (SuccessfulPermissionSchemeGrantEvent) o;
        return Objects.equal(schemeId, that.schemeId) &&
                Objects.equal(numberOfPermissions, that.numberOfPermissions) &&
                Objects.equal(securityType, that.securityType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), schemeId, numberOfPermissions, securityType);
    }

    @Override
    public String toString() {
        return "SuccessfulPermissionSchemeGrantEvent{" +
                "schemeId=" + schemeId +
                ", securityType='" + securityType + '\'' +
                ", numberOfPermissions=" + numberOfPermissions +
                '}';
    }
}
