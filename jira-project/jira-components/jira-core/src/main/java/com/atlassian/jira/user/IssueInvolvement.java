package com.atlassian.jira.user;

/**
 * The ways a user can be involved in an issue.
 *
 * @since v7.2
 */
public enum IssueInvolvement implements Comparable<IssueInvolvement> {
    ASSIGNEE("assignee", "rest.mentionableuser.assignee"),
    REPORTER("reporter", "rest.mentionableuser.reporter"),
    COMMENTER("commenter", "rest.mentionableuser.commenter"),
    WATCHER("watcher", "rest.mentionableuser.watcher"),
    VOTER("voter", "rest.mentionableuser.voter");

    private final String id;
    private final String i18nKey;

    IssueInvolvement(String id, String i18nKey) {
        this.id = id;
        this.i18nKey = i18nKey;
    }

    /**
     * Returns the ID for the involvement type.
     *
     * @return The ID
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the i18n key for the involvement type. Useful for displaying on the front-end.
     *
     * @return The i18n key
     */
    public String getI18nKey() {
        return i18nKey;
    }
}