package com.atlassian.jira.config;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nullable;

public class DefaultIssueTypePermissionService implements IssueTypePermissionService {
    private final IssueTypeService issueTypeService;
    private final GlobalPermissionManager globalPermissionManager;

    public DefaultIssueTypePermissionService(
            IssueTypeService issueTypeService,
            GlobalPermissionManager globalPermissionManager) {
        this.issueTypeService = issueTypeService;
        this.globalPermissionManager = globalPermissionManager;
    }

    @Override
    public boolean hasPermissionToViewIssueType(final ApplicationUser user, final String id) {
        //can be made faster by getting all schemes with given issue type and checking if the scheme is used in a project where the user has browse permission.
        return issueTypeService.getIssueType(user, id).isDefined();
    }

    @Override
    public boolean hasPermissionToEditIssueType(@Nullable final ApplicationUser user) {
        return globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user);
    }
}
