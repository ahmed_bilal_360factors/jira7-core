package com.atlassian.jira.license;

import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.Users;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.JiraContactHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @since 7.0
 */
public class CreateIssueLicenseCheckImpl implements CreateIssueLicenseCheck {
    private final JiraLicenseManager licenseManager;
    private final ApplicationRoleManager roleManager;
    private final JiraAuthenticationContext authenticationContext;
    private final JiraContactHelper jiraContactHelper;
    private final I18nHelper i18n;

    public CreateIssueLicenseCheckImpl(JiraLicenseManager licenseManager, ApplicationRoleManager roleManager,
                                       JiraAuthenticationContext authenticationContext, JiraContactHelper jiraContactHelper, I18nHelper i18n) {
        this.licenseManager = licenseManager;
        this.roleManager = roleManager;
        this.authenticationContext = authenticationContext;
        this.jiraContactHelper = jiraContactHelper;
        this.i18n = i18n;
    }

    @Override
    public Result evaluate() {
        return evaluateWithUser(authenticationContext.getLoggedInUser());
    }

    @Override
    public Result evaluateWithUser(ApplicationUser user) {
        // license count check
        if (!licenseManager.isLicenseSet()) {
            return new Failure(Collections.emptyList(),
                    i18n.getText("createissue.error.invalid.license", getContactLink()));
        }

        // expiry check
        List<LicenseDetails> expiredLicenses = getExpiredLicenses();
        if (!expiredLicenses.isEmpty()) {
            String i18nKey = instanceIsEnterprise()
                    ? "createissue.error.enterprise.license.expired"
                    : "createissue.error.license.expired";

            return new Failure(expiredLicenses, i18n.getText(i18nKey, getContactLink()));
        }

        return checkApplicationAccessAndUserLimits(user);
    }

    private Result checkApplicationAccessAndUserLimits(ApplicationUser user) {
        if (Users.isAnonymous(user)) {
            if (roleManager.isAnyRoleLimitExceeded()) {
                return new Failure(Collections.emptyList(),
                        i18n.getText("createissue.error.license.user.limit.exceeded.anonymous", getCapitalizedContactLink()));
            }
        } else {
            if (!roleManager.hasAnyRole(user)) {
                return new Failure(Collections.emptyList(), i18n.getText("createissue.error.license.user.no.application.role"));
            }

            if (roleManager.hasExceededAllRoles(user)) {
                return new Failure(Collections.emptyList(),
                        i18n.getText("createissue.error.license.user.limit.exceeded", getCapitalizedContactLink()));
            }
        }

        return PASS;
    }

    private List<LicenseDetails> getExpiredLicenses() {
        List<LicenseDetails> expired = new ArrayList<>();
        for (LicenseDetails license : licenseManager.getLicenses()) {
            if (license.isExpired()) {
                expired.add(license);
            }
        }

        return expired;
    }

    private boolean instanceIsEnterprise() {
        for (LicenseDetails license : licenseManager.getLicenses()) {
            if (license.isDataCenter() || license.isEnterpriseLicenseAgreement()) {
                return true;
            }
        }

        return false;
    }

    private String getCapitalizedContactLink() {
        String contact = getContactLink();
        return contact.substring(0, 1).toUpperCase() + contact.substring(1);
    }

    private String getContactLink() {
        return jiraContactHelper.getAdministratorContactMessage(i18n);
    }
}
