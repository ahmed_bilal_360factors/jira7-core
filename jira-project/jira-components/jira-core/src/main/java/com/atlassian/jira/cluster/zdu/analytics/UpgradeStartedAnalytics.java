package com.atlassian.jira.cluster.zdu.analytics;

import com.atlassian.analytics.api.annotations.EventName;

/**
 * @since v7.3
 */
@EventName("zdu.upgrade-state.start")
public class UpgradeStartedAnalytics {
    private final Integer nodeCount;
    private final Long nodeBuildNumber;

    public UpgradeStartedAnalytics(Integer nodeCount, Long nodeBuildNumber) {
        this.nodeCount = nodeCount;
        this.nodeBuildNumber = nodeBuildNumber;
    }

    public Integer getNodeCount() {
        return nodeCount;
    }

    public Long getNodeBuildNumber() {
        return nodeBuildNumber;
    }
}
