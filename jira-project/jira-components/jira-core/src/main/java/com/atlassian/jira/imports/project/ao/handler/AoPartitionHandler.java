package com.atlassian.jira.imports.project.ao.handler;

import com.atlassian.jira.action.admin.export.DefaultSaxEntitiesExporter;
import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.ProjectImportResults;
import com.atlassian.jira.imports.project.handler.AbortImportException;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.util.AoImportTemporaryFiles;
import com.atlassian.jira.imports.project.util.XMLEscapeUtil;
import org.ofbiz.core.util.UtilFormatOut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * Divide the AO file into separate files for each entity.
 *
 * @since v6.5
 */
public class AoPartitionHandler implements PluggableImportAoEntityHandler {
    private static final Logger logger = LoggerFactory.getLogger(AoPartitionHandler.class);

    private final AoImportTemporaryFiles aoImportTemporaryFiles;
    private final String encoding;

    PrintWriter entityWriter;
    private String currentEntityName;
    private List<String> columnNames;

    public AoPartitionHandler(final AoImportTemporaryFiles aoImportTemporaryFiles, final String encoding) {
        this.aoImportTemporaryFiles = aoImportTemporaryFiles;
        this.encoding = encoding;
    }

    @Override
    public boolean handlesEntity(final String entityName) {
        return true;
    }

    @Override
    public Long getEntityWeight(final String entityName) {
        return WEIGHT_NONE;
    }

    @Override
    public void handleEntity(final String entityName, final Map<String, Object> attributes)
            throws ParseException, AbortImportException {
        if (entityName != currentEntityName) {
            // Close of any previous file if it is still open
            closeTable();
            columnNames = new ArrayList(attributes.keySet());
            openTable(entityName, columnNames);
            currentEntityName = entityName;
        }

        // Output the row
        entityWriter.println("<row>");
        // Attributes must be written in the same  order as the column names
        for (String columnName : columnNames) {
            Object value = attributes.get(columnName);
            if (value == null) {
                // Always Encoding this a string is really a bit broken. but for Project Import should be OK
                writeStringValue("");
            } else if (value instanceof String) {
                writeStringValue((String) value);
            } else if (value instanceof Long) {
                writeLongValue((Long) value);
            } else if (value instanceof Double) {
                writeDoubleValue((Double) value);
            } else if (value instanceof Boolean) {
                writeBooleanValue((Boolean) value);
            } else if (value instanceof Date) {
                writeDateValue((Date) value);
            }
        }
        entityWriter.println("</row>");

    }

    private void writeStringValue(final String s) {
        entityWriter.println("<" + ChainedAoSaxHandler.STRING + ">" + encode(s) + "</" + ChainedAoSaxHandler.STRING + ">");
    }

    private void writeLongValue(final Long value) {
        entityWriter.println("<" + ChainedAoSaxHandler.INTEGER + ">" + encode(value.toString()) + "</" + ChainedAoSaxHandler.INTEGER + ">");
    }

    private void writeDoubleValue(final Double value) {
        entityWriter.println("<" + ChainedAoSaxHandler.DOUBLE + ">" + encode(value.toString()) + "</" + ChainedAoSaxHandler.DOUBLE + ">");
    }

    private void writeBooleanValue(final Boolean value) {
        entityWriter.println("<" + ChainedAoSaxHandler.BOOLEAN + ">" + encode(value.toString()) + "</" + ChainedAoSaxHandler.BOOLEAN + ">");
    }

    private void writeDateValue(final Date value) {
        entityWriter.println("<" + ChainedAoSaxHandler.DATE + ">" + encode(value) + "</" + ChainedAoSaxHandler.DATE + ">");
    }

    private String encode(final Date value) {
        return encode(newDateFormat().format(value));
    }

    private String encode(final String s) {
        return XMLEscapeUtil.unicodeEncode(UtilFormatOut.encodeXmlValue(s));
    }

    private DateFormat newDateFormat() {
        final DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf;
    }

    @Override
    public void startDocument() {
    }

    @Override
    public void endDocument() {
        closeTable();
    }

    @Override
    public void endTable(final String tableName) {
        closeTable();
    }

    private void openTable(final String entityName, final List<String> columnNames) throws AbortImportException {
        try {
            File aoXmlFile = aoImportTemporaryFiles.createFileForEntity(entityName);
            aoImportTemporaryFiles.registerAoXmlFile(entityName, aoXmlFile);
            entityWriter = getWriter(aoXmlFile, encoding);
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage(), e);
            throw new AbortImportException();
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(), e);
            throw new AbortImportException();
        }

        // Add opening tags and column definitions
        if (entityWriter != null) {
            entityWriter.println("<" + ChainedAoSaxHandler.AO_BACKUP_XML + ">");
            entityWriter.println("<" + ChainedAoSaxHandler.DATA + " " + ChainedAoSaxHandler.TABLE_NAME + "=\"" + entityName + "\" >");
            for (String columnName : columnNames) {
                entityWriter.println("<" + ChainedAoSaxHandler.COLUMN + " " + ChainedAoSaxHandler.COLUMN_NAME + "=\"" + columnName + "\"/>");
            }
        }
    }

    private void closeTable() {
        // close the previous output writer if it is open
        if (entityWriter != null) {
            try {
                entityWriter.println("</" + ChainedAoSaxHandler.DATA + ">");
                entityWriter.println("</" + ChainedAoSaxHandler.AO_BACKUP_XML + ">");
            } finally {
                entityWriter.close();
                entityWriter = null;
            }
        }
    }

    PrintWriter getWriter(final File file, final String encoding) throws UnsupportedEncodingException, FileNotFoundException {
        final FileOutputStream fos = new FileOutputStream(file);
        final OutputStreamWriter out = new OutputStreamWriter(fos, encoding);
        final BufferedWriter bufferedWriter = new BufferedWriter(out, DefaultSaxEntitiesExporter.DEFAULT_BUFFER_SIZE);
        return new PrintWriter(bufferedWriter);
    }

    @Override
    public void setBackupProject(final BackupProject backupProject) {
    }

    @Override
    public void setBackupSystemInformation(final BackupSystemInformation backupSystemInformation) {
    }

    @Override
    public void setProjectImportMapper(final ProjectImportMapper projectImportMapper) {
    }

    @Override
    public void setProjectImportResults(@Nullable final ProjectImportResults projectImportResults) {
    }
}
