package com.atlassian.jira.avatar;

import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.icon.IconType;

import java.util.List;

/**
 * Persistent storage mechanism for {@link AvatarImpl}.
 *
 * @since v4.0
 */
public interface AvatarStore {
    /**
     * Retrieves the Avatar by id.
     *
     * @param avatarId the avatar's id, must not be null.
     * @return the avatar with the given id or null if it doesn't exist.
     * @throws DataAccessException if there is a back-end storage problem.
     */
    public Avatar getById(Long avatarId) throws DataAccessException;

    /**
     * Retrieves the Avatar by id,
     * ensuring the avatar file is tagged with metadata identifying the image came from JIRA.
     *
     * @param avatarId the avatar's id, must not be null.
     * @return the avatar with the given id or null if it doesn't exist.
     * @throws DataAccessException if there is a back-end storage problem.
     */
    public Avatar getByIdTagged(Long avatarId);

    /**
     * Permanently removes the avatar from the system.
     *
     * @param avatarId the avatar's id, must not be null.
     * @return the avatar with the given id or null if it doesn't exist.
     * @throws DataAccessException if there is a back-end storage problem.
     */
    public boolean delete(Long avatarId) throws DataAccessException;

    /**
     * Updates an avatar's properties to match those in the given avatar. The avatar
     * too change is identified by the id of the given avatar.
     *
     * @param avatar the avatar to update, must not be null.
     * @throws DataAccessException if there is a back-end storage problem.
     */
    public void update(Avatar avatar) throws DataAccessException;

    /**
     * Creates an avatar with the properties of the given avatar.
     *
     * @param avatar the to create, must not be null, must have a null id.
     * @return the avatar with the given id or null if it doesn't exist.
     * @throws DataAccessException if there is a back-end storage problem.
     */
    public Avatar create(Avatar avatar) throws DataAccessException;

    /**
     * Provides a list of all system avatars.
     *
     * @param iconType the types of avatar to retrieve
     * @return the system avatars, never null.
     * @throws DataAccessException if there is a back-end database problem.
     * @since v7.1
     */
    public List<Avatar> getAllSystemAvatars(final IconType iconType) throws DataAccessException;

    /**
     * Provides a list of all avatars that are of the given type which have the given owner.
     *
     * @param iconType    the desired type of the avatars to retrieve.
     * @param ownerId the id of the owner, matches the type (project id or user key).
     * @return all the avatars that have the given type and owner, never null.
     * @throws DataAccessException if there is a back-end database problem.
     * @since v7.1
     */
    public List<Avatar> getCustomAvatarsForOwner(final IconType iconType, String ownerId) throws DataAccessException;

    /**
     * Get the system icon with a particular filename.
     * @param iconType The type of icon to return.
     * @param filename The filename.
     * @return The list of icons.
     * @throws DataAccessException back end DB problem.
     */
    public List<Avatar> getSystemAvatarsForFilename(final IconType iconType, String filename) throws DataAccessException;
}
