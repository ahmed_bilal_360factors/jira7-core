package com.atlassian.jira.license;

import javax.annotation.Nonnull;

/**
 * Knows how to generate and control the license banner. A license warning is displayed on the UI if the license is
 * about to expire.
 *
 * @since v6.3
 */
public interface LicenseBannerHelper {
    /**
     * Returns HTML to render expiry banners for all licenses that are expired or are about to expire, or an empty
     * string if no banners are required (ie: no licenses have expired or are near expiry).
     *
     * @return license banner HTML, or {@code ""} if no banner should be displayed.
     * @see LicenseDetails#isExpired()
     */
    @Nonnull
    String getExpiryBanner();

    /**
     * Returns HTML to render flags for all licenses that about to expire or whose maintenance period has expired
     * or is about to expire.
     * Returns an empty string if no banners are required (ie: all licenses are within maintenance).
     *
     * @return license banner HTML, or {@code ""} if no banner should be displayed.
     * @see LicenseDetails#isEntitledToSupport()
     */
    @Nonnull
    String getLicenseFlag();

    /**
     * Hide the license flag for the calling user. The flag will re-appear later.
     */
    void remindMeLater();

    /**
     * Reset the remind me state of the current user.
     */
    void clearRemindMe();
}
