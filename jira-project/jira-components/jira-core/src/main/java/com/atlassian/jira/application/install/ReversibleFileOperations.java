package com.atlassian.jira.application.install;

import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

/**
 * This class is intended to be used with try-with-resources. You have to explicitly commit operation by calling {@code
 * commit()}. All uncomitted operations will be reverted when try block is finalized. ie.:
 * <pre>{@code  try (ReversibleFileOperations reversibleFileOperations = new ReversibleFileOperations())
 * {
 *    reversibleFileOperations.fileDelete(file1);
 *    reversibleFileOperations.fileDelete(file2);
 *    reversibleFileOperaions.commit()
 * } // exception will revert all not commited operations
 * }
 * </pre>
 *
 * @since v6.5
 */
public class ReversibleFileOperations implements AutoCloseable {
    private final Deque<ReversibleOperation> operations = new ArrayDeque<>();

    /**
     * Call this right after you created a file. By itself it doesn't perform any operations on filesystem on call and
     * on commit. It will delete created file on revert.
     */
    public void removeOnRollback(final File file) {
        operations.push(new ReversibleFileCreation(file));
    }

    /**
     * Call this to perform deletion operation on file. It will rename given file to some backup file. On commit it will
     * delete backup file and on revert it rename backup file back to its original name. If given file doesn't exist
     * operation is ignored.
     */
    public void fileDelete(final File file) throws IOException {
        if (file.exists()) {
            operations.push(ReversibleFileDeletion.delete(file));
        }
    }

    /**
     * Commits all pending operations. Not commited operation will be reverted on close.
     */
    public void commit() throws IOException {
        final Iterator<ReversibleOperation> reversibleOperationIterator = operations.iterator();
        while (reversibleOperationIterator.hasNext()) {
            reversibleOperationIterator.next().commit();
            reversibleOperationIterator.remove();
        }
    }

    @Override
    public void close() throws IOException {
        final Iterator<ReversibleOperation> reversibleOperationIterator = operations.iterator();
        while (reversibleOperationIterator.hasNext()) {
            reversibleOperationIterator.next().revert();
            reversibleOperationIterator.remove();
        }
    }

    private interface ReversibleOperation {
        void revert() throws IOException;

        void commit() throws IOException;
    }

    private static class ReversibleFileCreation implements ReversibleOperation {
        private final File createdFile;

        private ReversibleFileCreation(final File createdFile) {
            this.createdFile = createdFile;
        }

        @Override
        public void revert() throws IOException {
            if (this.createdFile.exists() && !this.createdFile.delete()) {
                throw new IOException("Cannot revert file creation: " + this.createdFile);
            }
        }

        @Override
        public void commit() throws IOException {
        }
    }

    private static class ReversibleFileDeletion implements ReversibleOperation {
        private final File deletedFile;
        private final File tempFile;

        public static ReversibleOperation delete(final File deletedFile) throws IOException {
            final ReversibleFileDeletion reversibleFileDeletion = new ReversibleFileDeletion(deletedFile);
            reversibleFileDeletion.perform();

            return reversibleFileDeletion;
        }

        private ReversibleFileDeletion(final File deletedFile) throws IOException {
            this.deletedFile = deletedFile;
            tempFile = File.createTempFile("rfo-", ".temp", deletedFile.getParentFile());
        }

        void perform() throws IOException {
            if (!tempFile.delete()) {
                throw new IOException("Cannot delete temp file: " + tempFile);
            }

            if (!this.deletedFile.renameTo(tempFile)) {
                throw new IOException("Cannot rename file: " + deletedFile + " to " + tempFile);
            }
        }

        @Override
        public void revert() throws IOException {
            if (!this.tempFile.renameTo(deletedFile)) {
                throw new IOException("Cannot rename file: " + tempFile + " to " + deletedFile);
            }
        }

        @Override
        public void commit() throws IOException {
            if (!tempFile.delete()) {
                throw new IOException("Cannot delete temp file: " + tempFile);
            }
        }
    }
}
