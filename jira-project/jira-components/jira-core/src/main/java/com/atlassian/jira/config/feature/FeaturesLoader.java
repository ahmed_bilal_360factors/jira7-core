package com.atlassian.jira.config.feature;

import com.atlassian.plugin.Plugin;

import java.util.Properties;

/**
 * Used to load JIRA features.
 *
 * @since v6.5
 */
public interface FeaturesLoader {
    Properties loadCoreProperties();

    Iterable<Properties> loadPluginsFeatureProperties();

    boolean hasFeatureResources(Plugin plugin);
}
