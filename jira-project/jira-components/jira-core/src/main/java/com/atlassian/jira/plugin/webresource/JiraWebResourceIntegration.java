package com.atlassian.jira.plugin.webresource;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.gadgets.event.ClearSpecCacheEvent;
import com.atlassian.jira.InitializingComponent;
import com.atlassian.jira.config.FeatureEvent;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.LocaleManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.i18n.CachingI18nFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.cdn.CDNStrategy;
import com.atlassian.plugin.webresource.cdn.NoOpCDNStrategy;
import com.atlassian.plugin.webresource.prebake.PrebakeConfig;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * The implementation of the {@link com.atlassian.plugin.webresource.WebResourceIntegration} for JIRA.
 */
public class JiraWebResourceIntegration implements WebResourceIntegration, InitializingComponent {
    static final String CDN_DISABLED_FEATURE_KEY = "jira.cdn.disabled";
    static final String WRM_MAPPING_EMERGENCY_ABORT_KEY = "jira.ct.cdn.emergency.abort.enabled";
    static final String PRE_BAKED_MAPPING_FILE = "jira.cdn.prebaked.mapping.file";
    static final String INCREMENTAL_CACHE_ENABLED = "jira.wrm.incremental.cache.enabled";

    /**
     * A pattern to the mapping file. e.g.: {@code /sw/web-resources/mappings-${state}/map.json}
     */
    private final String CT_CDN_MAPPING_FILE = JiraSystemProperties.getInstance().getProperty(PRE_BAKED_MAPPING_FILE);

    private final PluginAccessor pluginAccessor;
    private final ApplicationProperties applicationProperties;
    private final VelocityRequestContextFactory requestContextFactory;
    private final BuildUtilsInfo buildUtilsInfo;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final CachingI18nFactory i18nFactory;
    private final JiraHome jiraHome;
    private final EventPublisher eventPublisher;
    private final FeatureManager featureManager;
    private final PluginEventManager pluginEventManager;
    private final LocaleManager localeManager;

    public JiraWebResourceIntegration(
            final PluginAccessor pluginAccessor, final ApplicationProperties applicationProperties,
            final VelocityRequestContextFactory requestContextFactory, final BuildUtilsInfo buildUtilsInfo,
            final JiraAuthenticationContext jiraAuthenticationContext, final CachingI18nFactory i18nFactory,
            final JiraHome jiraHome, final EventPublisher eventPublisher, final FeatureManager featureManager,
            final PluginEventManager pluginEventManager, final LocaleManager localeManager) {
        this.i18nFactory = i18nFactory;
        this.pluginAccessor = notNull("pluginAccessor", pluginAccessor);
        this.applicationProperties = notNull("applicationProperties", applicationProperties);
        this.requestContextFactory = notNull("requestContextFactory", requestContextFactory);
        this.buildUtilsInfo = notNull("buildUtilsInfo", buildUtilsInfo);
        this.jiraAuthenticationContext = notNull("jiraAuthenticationContext", jiraAuthenticationContext);
        this.jiraHome = notNull("jiraHome", jiraHome);
        this.eventPublisher = eventPublisher;
        this.featureManager = featureManager;
        this.pluginEventManager = pluginEventManager;
        this.localeManager = localeManager;
    }

    @EventListener
    public void onCdnDisabled(final FeatureEvent featureEvent) {
        if (featureEvent.feature().equals(CDN_DISABLED_FEATURE_KEY)
                || featureEvent.feature().equals(JiraPrefixCDNStrategy.ENABLED_FEATURE_KEY)
                || featureEvent.feature().equals(JiraBaseUrlCDNStrategy.ENABLED_FEATURE_KEY)) {
            eventPublisher.publish(new ClearSpecCacheEvent());
        }
    }

    public PluginAccessor getPluginAccessor() {
        return pluginAccessor;
    }

    @Override
    public PluginEventManager getPluginEventManager() {
        return pluginEventManager;
    }

    public Map<String, Object> getRequestCache() {
        return JiraAuthenticationContextImpl.getRequestCache();
    }

    public String getSystemCounter() {
        return applicationProperties.getDefaultBackedString(APKeys.WEB_RESOURCE_FLUSH_COUNTER);
    }

    public String getSystemBuildNumber() {
        return buildUtilsInfo.getCurrentBuildNumber();
    }

    @Override
    public String getHostApplicationVersion() {
        return buildUtilsInfo.getVersion();
    }

    public String getBaseUrl() {
        return getBaseUrl(UrlMode.AUTO);
    }

    public String getBaseUrl(final UrlMode urlMode) {
        switch (urlMode) {
            case RELATIVE:
                // It's possible to want the base URL from outside an executing HTTP request (e.g. in a quartz scheduled
                // job). In this case we should fall through to UrlMode.AUTO
                HttpServletRequest httpServletRequest = ExecutingHttpRequest.get();
                if (httpServletRequest != null) {
                    return httpServletRequest.getContextPath();
                }
            case AUTO:
                // This may return a canonical URL, depending on the type of request being handled.
                return requestContextFactory.getJiraVelocityRequestContext().getBaseUrl();
            case ABSOLUTE:
                return requestContextFactory.getJiraVelocityRequestContext().getCanonicalBaseUrl();
            default:
                throw new AssertionError("Unsupported URLMode: " + urlMode);
        }
    }

    public String getSuperBatchVersion() {
        return applicationProperties.getDefaultBackedString(APKeys.WEB_RESOURCE_SUPER_BATCH_FLUSH_COUNTER);
    }

    @Override
    public String getStaticResourceLocale() {
        final I18nHelper i18n = jiraAuthenticationContext.getI18nHelper();
        return i18n.getLocale().toString() + i18nFactory.getStateHashCode();
    }

    @Override
    public String getI18nStateHash() {
        return i18nFactory.getStateHashCode();
    }

    @Nonnull
    @Override
    public File getTemporaryDirectory() {
        return new File(jiraHome.getLocalHome(), "tmp" + File.separator + "webresources");
    }

    @Override
    public void afterInstantiation() throws Exception {
        eventPublisher.register(this);
    }

    @Override
    public CDNStrategy getCDNStrategy() {
        final boolean disabled = featureManager.isEnabled(CDN_DISABLED_FEATURE_KEY);
        if (!disabled) {
            Optional<PrebakeConfig> prebakeConfig = getPrebakeConfig();
            if (featureManager.isEnabled(JiraBaseUrlCDNStrategy.ENABLED_FEATURE_KEY)) {
                return new JiraBaseUrlCDNStrategy(applicationProperties, prebakeConfig);
            }
            if (featureManager.isEnabled(JiraPrefixCDNStrategy.ENABLED_FEATURE_KEY)) {
                return new JiraPrefixCDNStrategy(prebakeConfig);
            }
            if (isCtCdnMappingEnabled()) {
                //in case CDN mapping is enabled return a CDNStrategy with config otherwise won't map
                return new NoOpCDNStrategy(prebakeConfig);
            }
        }
        return null;
    }

    @Override
    public boolean isCtCdnMappingEnabled() {
        return CT_CDN_MAPPING_FILE != null && !featureManager.isEnabled(CDN_DISABLED_FEATURE_KEY) && !featureManager.getDarkFeatures().isFeatureEnabled(WRM_MAPPING_EMERGENCY_ABORT_KEY);
    }

    @Override
    public Locale getLocale() {
        return jiraAuthenticationContext.getLocale();
    }

    @Override
    public Iterable<Locale> getSupportedLocales() {
        return localeManager.getInstalledLocales();
    }

    @Override
    public String getI18nRawText(final Locale locale, final String key) {
        return i18nFactory.getInstance(locale).getUnescapedText(key);
    }

    @Override
    public String getI18nText(final Locale locale, final String key) {
        return i18nFactory.getInstance(locale).getText(key);
    }

    @Override
    public Set<String> allowedCondition1Keys() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<String> allowedTransform1Keys() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean forbidCondition1AndTransformer1() {
        return false;
    }

    private Optional<PrebakeConfig> getPrebakeConfig() {
        return CT_CDN_MAPPING_FILE != null ? Optional.of(PrebakeConfig.forPattern(CT_CDN_MAPPING_FILE)) : Optional.empty();
    }

    @Override
    public boolean isIncrementalCacheEnabled() {
        return featureManager.getDarkFeatures().isFeatureEnabled(INCREMENTAL_CACHE_ENABLED);
    }
}
