package com.atlassian.jira.plugin.freeze;

import com.atlassian.plugin.loaders.classloading.Scanner;

import java.io.File;

/**
 * @since v7.3
 */
@FunctionalInterface
public interface FreezeFileScannerFactory {
    Scanner createScanner(File file);
}
