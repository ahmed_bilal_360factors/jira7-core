package com.atlassian.jira.upgrade.tasks.role;

/**
 * DAO to get and set the state of JIRA in terms of a {@link com.atlassian.jira.upgrade.tasks.role.MigrationState}.
 * It is only safe to use in migration from JIRA 6.x to JIRA 7.0.
 * <p>
 * NOTE: This class it written to access the database as it was in JIRA 6.x and should not be updated to reflect
 * changes that happen after this point.
 *
 * @since v7.0
 */
abstract class MigrationStateDao {
    /**
     * Read the current state of the JIRA database.
     *
     * @return the current state of the JIRA database.
     */
    abstract MigrationState get();

    /**
     * Synchronise the database to the passed state.
     *
     * @param migrationState the state to synchronise.
     */
    abstract void put(MigrationState migrationState);
}
