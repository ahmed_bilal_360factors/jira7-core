package com.atlassian.jira.tenancy;

import com.atlassian.jira.InitializingComponent;
import com.atlassian.jira.bc.ServiceResult;

/**
 * Manages the tenant arrival used by InstantOn. Its main responsibility is to hold a list of arbitrary tasks that
 * should only be run once a Tenant has arrived (or immediately if we're already tenanted).
 * @since 7.1
 */
public interface TenantManager extends InitializingComponent {
    /**
     * Run this function immediately if there is a tenant. Otherwise run it when a tenant arrives.
     * @param runnable the function to run
     * @param description description of the function
     */
    public void doNowOrWhenTenantArrives(Runnable runnable, String description);

    /**
     * Called when a tenant arrives via DefaultLandlordRequests
     **/
    public ServiceResult tenantArrived();

    /**
     * @return true if this instance has a tenant
     */
    public boolean isTenanted();
}
