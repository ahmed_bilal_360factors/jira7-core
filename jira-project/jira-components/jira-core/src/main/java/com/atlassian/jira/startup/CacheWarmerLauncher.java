package com.atlassian.jira.startup;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.warmer.JiraWarmer;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.config.Schedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static com.atlassian.jira.component.ComponentAccessor.getComponent;

/**
 * Warm up caches, async in the scheduler by default, but it can be done synchronously by setting a sysprop.
 */
public class CacheWarmerLauncher implements JiraLauncher {

    private static final Logger log = LoggerFactory.getLogger(CacheWarmerLauncher.class);

    private static final String CACHE_WARMER_SYNC_SYSPROP = "cache.warmer.sync";

    private static final JobRunnerKey JOB_RUNNER_KEY = JobRunnerKey.of(CacheWarmerLauncher.class.getName());

    private final JiraProperties jiraProperties;

    public CacheWarmerLauncher(JiraProperties jiraProperties) {
        this.jiraProperties = jiraProperties;
    }

    @Override
    public void start() {
        if (jiraProperties.getBoolean(CACHE_WARMER_SYNC_SYSPROP)) {
            // warm caches in current thread
            parallelWarm();
        } else {
            // let the scheduler run the warm method
            SchedulerService schedulerService = getComponent(SchedulerService.class);
            schedulerService.registerJobRunner(JOB_RUNNER_KEY,
                    jobRunnerRequest -> {
                        parallelWarm();
                        return JobRunnerResponse.success();
                    }
            );
            JobConfig jobConfig = JobConfig
                    .forJobRunnerKey(JOB_RUNNER_KEY)
                    .withRunMode(RunMode.RUN_LOCALLY)
                    .withSchedule(Schedule.runOnce(null));
            try {
                schedulerService.scheduleJobWithGeneratedId(jobConfig);
            } catch (SchedulerServiceException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void stop() {
    }

    /*
     * Run warmers in parallel.
     */
    private void parallelWarm() {
        List<JiraWarmer> warmers = ComponentManager.getComponentsOfType(JiraWarmer.class);
        log.info("Warming up {} caches.", warmers.size());
        final long startTime = System.currentTimeMillis();
        warmers.parallelStream().forEach(JiraWarmer::run);
        long totalTime = System.currentTimeMillis() - startTime;
        log.info("Warmed cache(s) in {} ms.", totalTime);
    }

}

