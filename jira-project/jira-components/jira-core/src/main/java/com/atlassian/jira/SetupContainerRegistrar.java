package com.atlassian.jira;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.extension.api.ExtensionAccessor;
import com.atlassian.extension.plugins.ExtensionAccessingPluginAccessor;
import com.atlassian.jira.ajsmeta.GoogleSiteVerification;
import com.atlassian.jira.appconsistency.db.LockedDatabaseOfBizDelegator;
import com.atlassian.jira.bc.license.BootstrapJiraServerIdProvider;
import com.atlassian.jira.bc.license.JiraServerIdProvider;
import com.atlassian.jira.concurrent.BarrierFactory;
import com.atlassian.jira.concurrent.BarrierFactoryImpl;
import com.atlassian.jira.config.DefaultLocaleManager;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.InstanceFeatureManager;
import com.atlassian.jira.config.LocaleManager;
import com.atlassian.jira.config.component.DateTimeFormatterProvider;
import com.atlassian.jira.config.feature.BootstrapDarkFeatureManager;
import com.atlassian.jira.config.feature.BootstrapFeatureManager;
import com.atlassian.jira.config.feature.BootstrapInstanceFeatureManager;
import com.atlassian.jira.config.feature.CoreFeaturesLoader;
import com.atlassian.jira.config.feature.FeaturesLoader;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.config.util.EncodingConfiguration;
import com.atlassian.jira.config.webwork.DefaultWebworkClassLoaderSupplier;
import com.atlassian.jira.config.webwork.WebworkClassLoaderSupplier;
import com.atlassian.jira.config.webwork.WebworkConfigurator;
import com.atlassian.jira.config.webwork.actions.ActionConfiguration;
import com.atlassian.jira.crowd.embedded.JiraPasswordEncoderFactory;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeFormatterFactoryImpl;
import com.atlassian.jira.help.ApplicationHelpSpaceProvider;
import com.atlassian.jira.help.BoostrapApplicationHelpSpaceProvider;
import com.atlassian.jira.help.BootstrapHelpUrlsApplicationKeyProvider;
import com.atlassian.jira.help.CachingHelpUrls;
import com.atlassian.jira.help.DefaultHelpUrlsLoader;
import com.atlassian.jira.help.DefaultLocalHelpUrls;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.help.HelpUrlsApplicationKeyProvider;
import com.atlassian.jira.help.HelpUrlsLoader;
import com.atlassian.jira.help.HelpUrlsParser;
import com.atlassian.jira.help.HelpUrlsParserBuilderFactory;
import com.atlassian.jira.help.InitialHelpUrlsParser;
import com.atlassian.jira.help.InitialHelpUrlsParserBuilderFactory;
import com.atlassian.jira.help.JiraHelpPathResolver;
import com.atlassian.jira.help.LocalHelpUrls;
import com.atlassian.jira.i18n.BackingI18nFactory;
import com.atlassian.jira.i18n.BackingI18nFactoryImpl;
import com.atlassian.jira.i18n.CachingI18nFactory;
import com.atlassian.jira.i18n.DelegateI18nFactory;
import com.atlassian.jira.i18n.JiraI18nResolver;
import com.atlassian.jira.i18n.ResourceBundleCacheCleaner;
import com.atlassian.jira.instrumentation.Instrumentation;
import com.atlassian.jira.io.ClassPathResourceLoader;
import com.atlassian.jira.io.ResourceLoader;
import com.atlassian.jira.mail.settings.MailSettings;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.plugin.BootstrapPluginDirectoryLoaderFactory;
import com.atlassian.jira.plugin.BootstrapPluginLoaderFactory;
import com.atlassian.jira.plugin.BootstrapPluginVersionStore;
import com.atlassian.jira.plugin.ComponentClassManager;
import com.atlassian.jira.plugin.DefaultComponentClassManager;
import com.atlassian.jira.plugin.DefaultPackageScannerConfiguration;
import com.atlassian.jira.plugin.ExtensionAccessorPicoAdapter;
import com.atlassian.jira.plugin.JiraApplicationDefinedPluginProvider;
import com.atlassian.jira.plugin.JiraBootstrapStateStore;
import com.atlassian.jira.plugin.JiraCacheResetter;
import com.atlassian.jira.plugin.JiraContentTypeResolver;
import com.atlassian.jira.plugin.JiraHostContainer;
import com.atlassian.jira.plugin.JiraModuleDescriptorFactory;
import com.atlassian.jira.plugin.JiraModuleFactory;
import com.atlassian.jira.plugin.JiraOsgiContainerManager;
import com.atlassian.jira.plugin.JiraOsgiContainerManagerStarter;
import com.atlassian.jira.plugin.JiraPluginManager;
import com.atlassian.jira.plugin.JiraPluginResourceDownload;
import com.atlassian.jira.plugin.JiraServletContextFactory;
import com.atlassian.jira.plugin.OsgiServiceTrackerCache;
import com.atlassian.jira.plugin.OsgiServiceTrackerCacheImpl;
import com.atlassian.jira.plugin.PluginDirectoryLoaderFactory;
import com.atlassian.jira.plugin.PluginFactoryAndLoaderRegistrar;
import com.atlassian.jira.plugin.PluginLoaderFactory;
import com.atlassian.jira.plugin.PluginPath;
import com.atlassian.jira.plugin.PluginVersionStore;
import com.atlassian.jira.plugin.bigpipe.BigPipeService;
import com.atlassian.jira.plugin.navigation.HeaderFooterRendering;
import com.atlassian.jira.plugin.util.PluginModuleTrackerFactory;
import com.atlassian.jira.plugin.webfragment.DefaultSimpleLinkFactoryModuleDescriptors;
import com.atlassian.jira.plugin.webfragment.DefaultSimpleLinkManager;
import com.atlassian.jira.plugin.webfragment.JiraWebFragmentHelper;
import com.atlassian.jira.plugin.webfragment.JiraWebInterfaceManager;
import com.atlassian.jira.plugin.webfragment.SimpleLinkFactoryModuleDescriptors;
import com.atlassian.jira.plugin.webfragment.SimpleLinkManager;
import com.atlassian.jira.plugin.webresource.JiraSetupWebResourceBatchingConfiguration;
import com.atlassian.jira.plugin.webresource.JiraWebResourceIntegration;
import com.atlassian.jira.plugin.webresource.JiraWebResourceManagerImpl;
import com.atlassian.jira.plugin.webresource.JiraWebResourceUrlProvider;
import com.atlassian.jira.plugin.webresource.SuperBatchInvalidator;
import com.atlassian.jira.plugin.webwork.AutowireCapableWebworkActionRegistry;
import com.atlassian.jira.plugin.webwork.DefaultAutowireCapableWebworkActionRegistry;
import com.atlassian.jira.sal.JiraApplicationProperties;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;
import com.atlassian.jira.security.login.BootstrapLoginManagerImpl;
import com.atlassian.jira.security.login.LoginManager;
import com.atlassian.jira.security.websudo.InternalWebSudoManager;
import com.atlassian.jira.security.websudo.InternalWebSudoManagerImpl;
import com.atlassian.jira.security.xsrf.DefaultXsrfInvocationChecker;
import com.atlassian.jira.security.xsrf.SimpleXsrfTokenGenerator;
import com.atlassian.jira.security.xsrf.XsrfDefaults;
import com.atlassian.jira.security.xsrf.XsrfDefaultsImpl;
import com.atlassian.jira.security.xsrf.XsrfInvocationChecker;
import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;
import com.atlassian.jira.servermetrics.NoopServerMetricsDetailCollector;
import com.atlassian.jira.servermetrics.ServerMetricsDetailCollector;
import com.atlassian.jira.setting.GzipCompression;
import com.atlassian.jira.setup.DefaultSetupJohnsonUtil;
import com.atlassian.jira.setup.SetupJohnsonUtil;
import com.atlassian.jira.startup.BootstrapLocaleResolver;
import com.atlassian.jira.startup.DatabaseInitialImporter;
import com.atlassian.jira.startup.DefaultInstantUpgradeManager;
import com.atlassian.jira.startup.InstantUpgradeManager;
import com.atlassian.jira.startup.JiraStartupPluginSystemListener;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.template.soy.SoyTemplateRendererProviderImpl;
import com.atlassian.jira.template.velocity.DefaultVelocityEngineFactory;
import com.atlassian.jira.template.velocity.DefaultVelocityTemplatingEngine;
import com.atlassian.jira.template.velocity.VelocityEngineFactory;
import com.atlassian.jira.tenancy.DefaultJiraTenantAccessor;
import com.atlassian.jira.tenancy.DefaultTenantManager;
import com.atlassian.jira.tenancy.JiraTenancyCondition;
import com.atlassian.jira.tenancy.JiraTenantContext;
import com.atlassian.jira.tenancy.JiraTenantContextImpl;
import com.atlassian.jira.tenancy.PluginKeyPredicateLoader;
import com.atlassian.jira.tenancy.TenancyCondition;
import com.atlassian.jira.tenancy.TenantManager;
import com.atlassian.jira.timezone.NoDatabaseTimeZoneResolver;
import com.atlassian.jira.transaction.BootstrapRequestLocalTransactionRunnableQueryFactory;
import com.atlassian.jira.transaction.RequestLocalTransactionRunnableQueueFactory;
import com.atlassian.jira.transaction.TransactionSupport;
import com.atlassian.jira.transaction.TransactionSupportImpl;
import com.atlassian.jira.user.BootstrapUserLocaleStore;
import com.atlassian.jira.user.UserLocaleStore;
import com.atlassian.jira.util.ComponentFactory;
import com.atlassian.jira.util.FileFactory;
import com.atlassian.jira.util.FileSystemFileFactory;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.JiraComponentFactory;
import com.atlassian.jira.util.JiraProductInformation;
import com.atlassian.jira.util.PropertiesFileBackedJiraProductInformation;
import com.atlassian.jira.util.UnsupportedBrowserManager;
import com.atlassian.jira.util.i18n.I18nTranslationMode;
import com.atlassian.jira.util.i18n.I18nTranslationModeImpl;
import com.atlassian.jira.util.resourcebundle.HelpResourceBundleLoader;
import com.atlassian.jira.util.resourcebundle.I18NResourceBundleLoader;
import com.atlassian.jira.util.velocity.DefaultVelocityRequestContextFactory;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.HttpServletVariablesImpl;
import com.atlassian.jira.web.ServletContextProvider;
import com.atlassian.jira.web.action.RedirectSanitiser;
import com.atlassian.jira.web.action.RedirectSanitiserImpl;
import com.atlassian.jira.web.action.setup.IndexLanguageToLocaleMapper;
import com.atlassian.jira.web.action.setup.IndexLanguageToLocaleMapperImpl;
import com.atlassian.jira.web.action.setup.MyAtlassianComRedirect;
import com.atlassian.jira.web.action.setup.SetupCompleteRedirectHelper;
import com.atlassian.jira.web.action.setup.SetupSharedVariables;
import com.atlassian.jira.web.action.util.CalendarLanguageUtil;
import com.atlassian.jira.web.action.util.CalendarLanguageUtilImpl;
import com.atlassian.jira.web.bean.i18n.DefaultTranslationStoreFactory;
import com.atlassian.jira.web.bean.i18n.TranslationStoreFactory;
import com.atlassian.jira.web.dispatcher.PluginsAwareViewMapping;
import com.atlassian.jira.web.pagebuilder.DefaultJiraPageBuilderService;
import com.atlassian.jira.web.pagebuilder.JiraPageBuilderService;
import com.atlassian.jira.web.pagebuilder.PageBuilderServiceSpi;
import com.atlassian.jira.web.session.currentusers.JiraUserSessionInvalidator;
import com.atlassian.jira.web.session.currentusers.JiraUserSessionTracker;
import com.atlassian.jira.web.util.JiraLocaleUtils;
import com.atlassian.jira.web.util.ProductVersionDataBeanProvider;
import com.atlassian.license.DefaultSIDManager;
import com.atlassian.license.SIDManager;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginRegistry;
import com.atlassian.plugin.PluginSystemLifecycle;
import com.atlassian.plugin.SplitStartupPluginSystemLifecycle;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.manager.ApplicationDefinedPluginsProvider;
import com.atlassian.plugin.manager.ClusterEnvironmentProvider;
import com.atlassian.plugin.manager.DefaultSafeModeManager;
import com.atlassian.plugin.manager.PluginPersistentStateStore;
import com.atlassian.plugin.manager.PluginRegistryImpl;
import com.atlassian.plugin.manager.ProductPluginAccessor;
import com.atlassian.plugin.manager.SafeModeManager;
import com.atlassian.plugin.metadata.DefaultPluginMetadataManager;
import com.atlassian.plugin.metadata.PluginMetadataManager;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.container.PackageScannerConfiguration;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import com.atlassian.plugin.parsers.DefaultSafeModeCommandLineArgumentsFactory;
import com.atlassian.plugin.parsers.SafeModeCommandLineArgumentsFactory;
import com.atlassian.plugin.schema.descriptor.DescribedModuleDescriptorFactory;
import com.atlassian.plugin.servlet.ContentTypeResolver;
import com.atlassian.plugin.servlet.DefaultServletModuleManager;
import com.atlassian.plugin.servlet.DownloadStrategy;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.servlet.ServletModuleManager;
import com.atlassian.plugin.web.DefaultWebInterfaceManager;
import com.atlassian.plugin.web.WebFragmentHelper;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.PluginResourceLocatorImpl;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugin.webresource.assembler.DefaultWebResourceAssemblerFactory;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerFactory;
import com.atlassian.sal.api.message.HelpPathResolver;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.message.LocaleResolver;
import com.atlassian.sal.spi.HostContextAccessor;
import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.seraph.auth.AuthenticationContextImpl;
import com.atlassian.tenancy.api.TenantAccessor;
import com.atlassian.velocity.JiraVelocityManager;
import com.atlassian.velocity.VelocityManager;
import com.atlassian.velocity.htmlsafe.event.referenceinsertion.EnableHtmlEscapingDirectiveHandler;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;
import com.atlassian.webresource.compiler.GCCResourceCompiler;
import com.atlassian.webresource.spi.ResourceCompiler;
import org.picocontainer.parameters.ConstantParameter;

import static com.atlassian.jira.ComponentContainer.Scope.INTERNAL;
import static com.atlassian.jira.ComponentContainer.Scope.PROVIDED;

/**
 * Register the components in the {@link com.atlassian.jira.ComponentContainer} that allow JIRA to be setup.
 * <p>
 * The components in here are meant to be the MINIMUM set of components that JIRA needs during setup.  Don't add
 * components here unless you are positive that setup code in JIRA needs it.
 */
@SuppressWarnings("deprecation")
class SetupContainerRegistrar {
    public void registerComponents(final ComponentContainer register) {
        // MT bootstrap stuff
        register.implementation(INTERNAL, JiraStartupPluginSystemListener.class);
        register.implementation(INTERNAL, HostContainer.class, JiraHostContainer.class);

        MultipleKeyRegistrant.registrantFor(JiraModuleDescriptorFactory.class)
                .implementing(ModuleDescriptorFactory.class)
                .implementing(ListableModuleDescriptorFactory.class)
                .implementing(DescribedModuleDescriptorFactory.class)
                .registerWith(PROVIDED, register);

        register.implementation(PROVIDED, PluginLoaderFactory.class, BootstrapPluginLoaderFactory.class);
        register.implementation(INTERNAL, PluginDirectoryLoaderFactory.class, BootstrapPluginDirectoryLoaderFactory.class);
        register.implementation(INTERNAL, PluginFactoryAndLoaderRegistrar.class);
        register.implementation(PROVIDED, ModuleFactory.class, JiraModuleFactory.class);

        register.implementation(PROVIDED, ServletModuleManager.class, DefaultServletModuleManager.class,
                new ConstantParameter(ServletContextProvider.getServletContext()),
                PluginEventManager.class);

        register.implementation(PROVIDED, PluginMetadataManager.class, DefaultPluginMetadataManager.class);

        register.implementation(INTERNAL, PluginKeyPredicateLoader.class);

        register.implementation(INTERNAL, PluginRegistry.ReadWrite.class, PluginRegistryImpl.class);

        register.implementation(PROVIDED, SafeModeManager.class, DefaultSafeModeManager.class);
        register.implementation(PROVIDED, SafeModeCommandLineArgumentsFactory.class, DefaultSafeModeCommandLineArgumentsFactory.class);
        register.implementation(PROVIDED, ApplicationDefinedPluginsProvider.class, JiraApplicationDefinedPluginProvider.class);
        // We use SINGLE_NODE here, because SetupContainerRegistrar is supposed to be invoked only when database
        // is not present, thus we can be sure, that this JIRA instance is in single node mode this time
        register.instance(PROVIDED, ClusterEnvironmentProvider.class, ClusterEnvironmentProvider.SINGLE_NODE);

        MultipleKeyRegistrant.registrantFor(JiraPluginManager.class)
                .implementing(PluginSystemLifecycle.class)
                .implementing(SplitStartupPluginSystemLifecycle.class)
                .implementing(PluginController.class)
                .registerWith(PROVIDED, register);

        register.implementation(INTERNAL, ProductPluginAccessor.class, ProductPluginAccessor.class);
        register.implementation(PROVIDED, ExtensionAccessor.class, ExtensionAccessorPicoAdapter.class);
        register.implementation(PROVIDED, PluginAccessor.class, ExtensionAccessingPluginAccessor.class,
                ExtensionAccessor.class,
                ProductPluginAccessor.class,
                ModuleDescriptorFactory.class);

        register.implementation(INTERNAL, JiraCacheResetter.class);

        register.implementation(PROVIDED, PluginPath.class, PluginPath.JiraHomeAdapter.class);
        register.implementation(INTERNAL, OsgiContainerManager.class, JiraOsgiContainerManager.class);
        register.implementation(INTERNAL, OsgiServiceTrackerCache.class, OsgiServiceTrackerCacheImpl.class);
        register.implementation(INTERNAL, JiraOsgiContainerManagerStarter.class);
        register.instance(INTERNAL, HostComponentProvider.class, register.getHostComponentProvider());
        register.implementation(PROVIDED, PackageScannerConfiguration.class, DefaultPackageScannerConfiguration.class);

        register.implementation(PROVIDED, DownloadStrategy.class, JiraPluginResourceDownload.class);
        register.implementation(PROVIDED, ContentTypeResolver.class, JiraContentTypeResolver.class);

        register.implementation(PROVIDED, PluginResourceLocator.class, PluginResourceLocatorImpl.class);
        register.implementation(PROVIDED, ServletContextFactory.class, JiraServletContextFactory.class);
        register.implementation(INTERNAL, ComponentClassManager.class, DefaultComponentClassManager.class);

        register.implementation(INTERNAL, TransactionSupport.class, TransactionSupportImpl.class);
        register.implementation(INTERNAL, RequestLocalTransactionRunnableQueueFactory.class, BootstrapRequestLocalTransactionRunnableQueryFactory.class);
        register.implementation(PROVIDED, HostContextAccessor.class, DefaultHostContextAccessor.class);

        register.implementation(INTERNAL, PluginPersistentStateStore.class, JiraBootstrapStateStore.class);
        register.implementation(INTERNAL, PluginVersionStore.class, BootstrapPluginVersionStore.class);
        register.implementation(INTERNAL, PluginModuleTrackerFactory.class);

        register.implementation(PROVIDED, WebInterfaceManager.class, DefaultWebInterfaceManager.class);
        register.implementation(PROVIDED, WebFragmentHelper.class, JiraWebFragmentHelper.class);
        register.implementation(PROVIDED, WebResourceManager.class, JiraWebResourceManagerImpl.class);
        register.implementation(PROVIDED, WebResourceIntegration.class, JiraWebResourceIntegration.class);
        register.implementation(PROVIDED, WebResourceUrlProvider.class, JiraWebResourceUrlProvider.class);
        register.implementation(INTERNAL, ResourceCompiler.class, GCCResourceCompiler.class);
        register.implementation(PROVIDED, ResourceBatchingConfiguration.class, JiraSetupWebResourceBatchingConfiguration.class);
        register.implementation(PROVIDED, SimpleLinkManager.class, DefaultSimpleLinkManager.class);
        register.implementation(PROVIDED, SimpleLinkFactoryModuleDescriptors.class, DefaultSimpleLinkFactoryModuleDescriptors.class);
        MultipleKeyRegistrant.registrantFor(DefaultWebResourceAssemblerFactory.class)
                .implementing(PrebakeWebResourceAssemblerFactory.class)
                .implementing(WebResourceAssemblerFactory.class)
                .registerWith(PROVIDED, register);
        register.implementation(INTERNAL, JiraWebInterfaceManager.class);
        register.implementation(PROVIDED, EncodingConfiguration.class, EncodingConfiguration.PropertiesAdaptor.class);

        // Velocity components
        register.implementation(INTERNAL, VelocityEngineFactory.class, DefaultVelocityEngineFactory.class);
        register.implementation(INTERNAL, EnableHtmlEscapingDirectiveHandler.class);
        register.implementation(PROVIDED, VelocityTemplatingEngine.class, DefaultVelocityTemplatingEngine.class);
        register.implementation(PROVIDED, VelocityManager.class, JiraVelocityManager.class);
        register.implementation(PROVIDED, VelocityRequestContextFactory.class, DefaultVelocityRequestContextFactory.class);

        //Soy components
        register.implementation(PROVIDED, SoyTemplateRendererProvider.class, SoyTemplateRendererProviderImpl.class);

        // this will prevent ANY database access via OfBiz.  This is belts and braces on top of the bootstrapping
        register.implementation(PROVIDED, OfBizDelegator.class, LockedDatabaseOfBizDelegator.class);

        register.implementation(INTERNAL, WebworkClassLoaderSupplier.class, DefaultWebworkClassLoaderSupplier.class);
        register.implementation(INTERNAL, WebworkConfigurator.class);
        register.implementation(INTERNAL, PluginsAwareViewMapping.Component.class);
        register.implementation(INTERNAL, AutowireCapableWebworkActionRegistry.class, DefaultAutowireCapableWebworkActionRegistry.class);
        register.implementation(INTERNAL, ActionConfiguration.class, ActionConfiguration.FromWebWorkConfiguration.class);

        register.implementation(INTERNAL, LoginManager.class, BootstrapLoginManagerImpl.class);
        register.implementation(PROVIDED, AuthenticationContext.class, AuthenticationContextImpl.class);
        register.implementation(PROVIDED, JiraAuthenticationContext.class, JiraAuthenticationContextImpl.class);
        register.implementation(INTERNAL, JiraUserSessionTracker.class);
        register.implementation(INTERNAL, JiraUserSessionInvalidator.class);

        register.implementation(PROVIDED, com.atlassian.cache.CacheManager.class, MemoryCacheManager.class);

        register.implementation(INTERNAL, UserLocaleStore.class, BootstrapUserLocaleStore.class);
        register.implementation(INTERNAL, ResourceBundleCacheCleaner.class);
        register.implementation(INTERNAL, I18NResourceBundleLoader.class);
        register.implementation(INTERNAL, HelpResourceBundleLoader.class);
        register.implementation(INTERNAL, BackingI18nFactory.class, BackingI18nFactoryImpl.class);
        register.implementation(INTERNAL, CachingI18nFactory.class);
        register.implementation(PROVIDED, I18nHelper.BeanFactory.class, DelegateI18nFactory.class);
        register.implementation(INTERNAL, JiraLocaleUtils.class);
        register.implementation(PROVIDED, I18nTranslationMode.class, I18nTranslationModeImpl.class);
        register.implementation(PROVIDED, TranslationStoreFactory.class, DefaultTranslationStoreFactory.class);
        //
        //
        // By rights, the I18nResolver and ApplicationApplication should be defined in jira-sal-plugin, but it is need by AUI during bootstrap
        // So instead of creating a separate jira-sal-plugin-micro-kernel plugin, we just whack it in the SetupContainer.
        //
        // We need to repeat this pattern whenever we have a plugin that uses SAL code but it being called inside the bootstrap
        // phase.  We can only have plugins that have light weight SAL usage.  To bring in a full SAL requires
        // much more than the bootstrap phase can provide.
        //
        //
        register.implementation(PROVIDED, I18nResolver.class, JiraI18nResolver.class);
        register.implementation(PROVIDED, com.atlassian.sal.api.features.DarkFeatureManager.class, BootstrapDarkFeatureManager.class);
        register.implementation(PROVIDED, com.atlassian.sal.api.ApplicationProperties.class, JiraApplicationProperties.class);
        register.implementation(PROVIDED, com.atlassian.sal.api.web.context.HttpContext.class, HttpServletVariablesImpl.class);

        register.implementation(PROVIDED, LocaleManager.class, DefaultLocaleManager.class);
        register.implementation(PROVIDED, IndexLanguageToLocaleMapper.class, IndexLanguageToLocaleMapperImpl.class);
        register.implementation(PROVIDED, CalendarLanguageUtil.class, CalendarLanguageUtilImpl.class);
        register.implementation(INTERNAL, UnsupportedBrowserManager.class);

        register.implementation(INTERNAL, FileFactory.class, FileSystemFileFactory.class);

        register.implementation(PROVIDED, PasswordEncoderFactory.class, JiraPasswordEncoderFactory.class);
        register.instance(INTERNAL, ComponentFactory.class, JiraComponentFactory.getInstance());

        register.implementation(INTERNAL, XsrfDefaults.class, XsrfDefaultsImpl.class);
        register.implementation(PROVIDED, XsrfTokenGenerator.class, SimpleXsrfTokenGenerator.class);
        register.implementation(PROVIDED, XsrfInvocationChecker.class, DefaultXsrfInvocationChecker.class);
        register.implementation(PROVIDED, InternalWebSudoManager.class, InternalWebSudoManagerImpl.class);
        register.implementation(PROVIDED, JiraServerIdProvider.class, BootstrapJiraServerIdProvider.class);

        register.implementation(INTERNAL, Instrumentation.class);

        register.implementation(ComponentContainer.Scope.INTERNAL, NoDatabaseTimeZoneResolver.class);
        register.implementation(PROVIDED, DateTimeFormatterFactory.class, DateTimeFormatterFactoryImpl.class);
        register.component(PROVIDED, new DateTimeFormatterProvider());

        register.implementation(PROVIDED, FeatureManager.class, BootstrapFeatureManager.class);
        register.implementation(PROVIDED, InstanceFeatureManager.class, BootstrapInstanceFeatureManager.class);
        register.implementation(INTERNAL, FeaturesLoader.class, CoreFeaturesLoader.class);
        register.implementation(INTERNAL, ResourceLoader.class, ClassPathResourceLoader.class);
        register.implementation(INTERNAL, GzipCompression.class);

        register.implementation(PROVIDED, MailSettings.class, MailSettings.DefaultMailSettings.class);
        register.implementation(INTERNAL, ProductVersionDataBeanProvider.class);
        register.implementation(INTERNAL, GoogleSiteVerification.class);
        register.implementation(INTERNAL, BigPipeService.class);
        register.implementation(INTERNAL, HeaderFooterRendering.class);

        MultipleKeyRegistrant.registrantFor(DefaultJiraPageBuilderService.class)
                .implementing(JiraPageBuilderService.class)
                .implementing(com.atlassian.webresource.api.assembler.PageBuilderService.class)
                .implementing(PageBuilderServiceSpi.class)
                .registerWith(PROVIDED, register);

        register.instance(PROVIDED, JiraProperties.class, JiraSystemProperties.getInstance());
        register.implementation(PROVIDED, TenancyCondition.class, JiraTenancyCondition.class);
        register.implementation(PROVIDED, JiraTenantContext.class, JiraTenantContextImpl.class);
        register.implementation(PROVIDED, TenantAccessor.class, DefaultJiraTenantAccessor.class);

        // Allows us to register actions to perform an various stages in the startup
        register.implementation(INTERNAL, TenantManager.class, DefaultTenantManager.class);
        register.implementation(INTERNAL, InstantUpgradeManager.class, DefaultInstantUpgradeManager.class);

        register.implementation(PROVIDED, HelpUrlsParserBuilderFactory.class, InitialHelpUrlsParserBuilderFactory.class);
        register.implementation(PROVIDED, HelpUrlsParser.class, InitialHelpUrlsParser.class);
        register.implementation(INTERNAL, LocalHelpUrls.class, DefaultLocalHelpUrls.class);
        register.implementation(INTERNAL, HelpUrlsApplicationKeyProvider.class, BootstrapHelpUrlsApplicationKeyProvider.class);
        register.implementation(INTERNAL, ApplicationHelpSpaceProvider.class, BoostrapApplicationHelpSpaceProvider.class);
        register.implementation(INTERNAL, HelpUrlsLoader.class, DefaultHelpUrlsLoader.class);
        register.implementation(PROVIDED, HelpUrls.class, CachingHelpUrls.class);
        register.implementation(PROVIDED, LocaleResolver.class, BootstrapLocaleResolver.class);
        register.implementation(PROVIDED, HelpPathResolver.class, JiraHelpPathResolver.class);

        register.implementation(INTERNAL, SIDManager.class, DefaultSIDManager.class);
        register.implementation(PROVIDED, RedirectSanitiser.class, RedirectSanitiserImpl.class);

        register.implementation(INTERNAL, SetupSharedVariables.class);
        register.implementation(INTERNAL, SetupCompleteRedirectHelper.class);
        register.implementation(INTERNAL, SetupJohnsonUtil.class, DefaultSetupJohnsonUtil.class);

        //Setup is not shielded from incomplete superbatches, therefore the superbatch needs to be invalidated
        register.implementation(INTERNAL, SuperBatchInvalidator.class);

        register.implementation(INTERNAL, JiraProductInformation.class, PropertiesFileBackedJiraProductInformation.class);
        register.implementation(INTERNAL, MyAtlassianComRedirect.class);
        register.implementation(INTERNAL, DatabaseInitialImporter.class);

        register.instance(INTERNAL, ServerMetricsDetailCollector.class, new NoopServerMetricsDetailCollector());
        register.implementation(PROVIDED, BarrierFactory.class, BarrierFactoryImpl.class);
    }

}
