package com.atlassian.jira.avatar;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Saves an image as a png with metadata signifying this image is a JIRA Avatar
 * (used by the email handler to decide whether or not to attach an image)
 *
 * @since v6.1
 */
public interface AvatarTagger {
    final String JIRA_SYSTEM_IMAGE_TYPE = "jira-system-image-type";
    final String AVATAR_SYSTEM_IMAGE_TYPE = "avatar";

    /**
     * Adds JIRA specific metadata to given image. The saved image will have PNG format.
     *
     * @param image to be tagged
     * @param name  of the image
     * @param file  in which the tagged image will be saved
     * @throws IOException
     */
    void saveTaggedAvatar(RenderedImage image, String name, File file) throws IOException;

    /**
     * Adds JIRA specific metadata to all avatar thumbnails.
     *
     * @param id       of the avatar
     * @param filename of the avatar main image
     * @return the name of tagged avatar image
     * @throws IOException
     */
    String tagAvatar(long id, String filename) throws IOException;

    /**
     * Adds JIRA specific metadata to given image.
     *
     * @param image        to be tagged
     * @param targetFormat in which image will be saved
     * @param target       output in which the tagged image will be saved
     * @throws IOException
     */
    void saveTaggedAvatar(RenderedImage image, String targetFormat, OutputStream target) throws IOException;

    /**
     * Add JIRA specific metadata to given file.
     *
     * @param source      file to be tagged
     * @param destination in which the tagged file will be saved
     * @throws IOException
     */
    void tagSingleAvatarFile(File source, File destination) throws IOException;

    /**
     * Add JIRA specific metadata to given file.
     *
     * @param inputStream  pointing to the source which will be tagged
     * @param outputStream pointing where the tagged file should be written
     * @throws IOException
     */
    void tag(InputStream inputStream, OutputStream outputStream) throws IOException;
}
