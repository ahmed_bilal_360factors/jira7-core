package com.atlassian.jira.avatar;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.dbc.Assertions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.atlassian.collectors.CollectorsUtil.toImmutableListMultiMap;
import static java.util.function.Function.identity;

/**
 * Avatar store which converts legacy avatar files to the new tagged format during retrieval
 *
 * @since v6.2
 */
@EventComponent
public class CachingTaggingAvatarStore implements AvatarStore {
    private static final Logger log = LoggerFactory.getLogger(CachingTaggingAvatarStore.class);

    public static final String AVATAR_ENTITY = "Avatar";
    public static final String ID = "id";
    public static final String FILE_NAME = "fileName";
    public static final String CONTENT_TYPE = "contentType";
    public static final String AVATAR_TYPE = "avatarType";
    public static final String OWNER = "owner";
    public static final String SYSTEM_AVATAR = "systemAvatar";
    private static final String TAGGED_AVATAR_FILE_SUFFIX = "jrvtg.png";
    private static final int IS_SYSTEM = 1;
    private static final int NOT_SYSTEM = 0;

    private final Cache<Long, Optional<Avatar>> avatars;
    private final Cache<Long, Optional<Avatar>> taggedAvatars;
    private final Cache<String, ImmutableListMultimap<String, Avatar>> systemAvatars;

    private final OfBizDelegator ofBizDelegator;
    private final AvatarTagger avatarTagger;

    // Because tagging an avatar file is too expensive when we're requesting multiple files in a short space of time
    // (e.g. user pickers) we provide a means of asking for an avatar either way, and a means of asking for an avatar
    // that must be tagged. Once an avatar is tagged, all subsequent requests for the avatar will get the tagged avatar.
    public CachingTaggingAvatarStore(final OfBizDelegator ofBizDelegator,
                                     final AvatarTagger avatarTagger, final CacheManager cacheManager) {
        this.taggedAvatars = cacheManager.getCache(getClass().getName() + ".taggedAvatars",
                this::tagAndRetrieve);
        this.avatars = cacheManager.getCache(getClass().getName() + ".avatars",
                this::checkForTaggedVersionOrFallbackToDb);
        this.systemAvatars = cacheManager.getCache(getClass().getName() + ".systemAvatars",
                (String iconTypeKey) -> getAvatars(FieldMap.build(SYSTEM_AVATAR, IS_SYSTEM, AVATAR_TYPE, iconTypeKey))
                    .stream()
                    .collect(toImmutableListMultiMap(Avatar::getFileName, identity())));

        this.ofBizDelegator = ofBizDelegator;
        this.avatarTagger = avatarTagger;
    }

    @Nonnull
    private Optional<Avatar> checkForTaggedVersionOrFallbackToDb(@Nonnull final Long id) {
        if (taggedAvatars.containsKey(id)) {
            return taggedAvatars.get(id);
        } else {
            return Optional.ofNullable(gvToAvatar(ofBizDelegator.findById(AVATAR_ENTITY, id)));
        }
    }

    // By supplying the "tag and retrieve" action to a guava cache as a function, and keying off the id,
    // we prevent concurrent attempts to tag the file without having to write our own concurrency logic
    @Nonnull
    private Optional<Avatar> tagAndRetrieve(@Nullable Long id) {
        final GenericValue gv = ofBizDelegator.findById(AVATAR_ENTITY, id);
        if (gv == null) {
            return Optional.empty();
        }
        tagLegacyAvatar(gv);
        return Optional.of(gvToAvatar(gv));
    }

    public Avatar getById(final Long avatarId) {
        return avatars.get(avatarId).orElse(null);
    }

    @Override
    public Avatar getByIdTagged(final Long avatarId) {
        try {
            return taggedAvatars.get(avatarId).orElse(null);
        } finally {
            avatars.remove(avatarId);
        }
    }

    public boolean delete(final Long avatarId) {
        Assertions.notNull("avatarId", avatarId);
        try {
            return ofBizDelegator.removeByAnd(AVATAR_ENTITY, FieldMap.build(ID, avatarId)) != 0;
        } finally {
            taggedAvatars.remove(avatarId);
            avatars.remove(avatarId);
            systemAvatars.removeAll();
        }
    }

    public void update(final Avatar avatar) {
        Assertions.notNull("avatar", avatar);
        Long avatarId = Assertions.notNull("avatar.id", avatar.getId());
        Assertions.notNull("avatar.fileName", avatar.getFileName());
        Assertions.notNull("avatar.contentType", avatar.getContentType());
        Assertions.notNull("avatar.avatarType", avatar.getIconType());
        Assertions.notNull("avatar.owner", avatar.getOwner());

        final GenericValue gv = ofBizDelegator.findById(AVATAR_ENTITY, avatarId);
        gv.setNonPKFields(getNonPkFields(avatar));
        try {
            gv.store();
        } catch (GenericEntityException e) {
            throw new DataAccessException(e);
        } finally {
            taggedAvatars.remove(avatarId);
            avatars.remove(avatarId);
            systemAvatars.removeAll();
        }
    }

    public Avatar create(final Avatar avatar) {
        Assertions.notNull("avatar", avatar);
        Assertions.stateTrue("avatar.id must be null", avatar.getId() == null);
        Assertions.notNull("avatar.fileName", avatar.getFileName());
        Assertions.notNull("avatar.contentType", avatar.getContentType());
        Assertions.notNull("avatar.avatarType", avatar.getIconType());

        Avatar createdAvatar = gvToAvatar(ofBizDelegator.createValue(AVATAR_ENTITY, getNonPkFields(avatar)));

        taggedAvatars.remove(createdAvatar.getId());
        avatars.remove(createdAvatar.getId());
        if (createdAvatar.isSystemAvatar()) {
            systemAvatars.remove(createdAvatar.getIconType().getKey());
        }

        return createdAvatar;
    }

    @Override
    public List<Avatar> getAllSystemAvatars(final IconType iconType) {
        Assertions.notNull("iconType", iconType);

        //noinspection ConstantConditions
        return ImmutableList.copyOf(systemAvatars.get(iconType.getKey()).values());
    }

    @Override
    public List<Avatar> getCustomAvatarsForOwner(final IconType iconType, final String ownerId) {
        Assertions.notNull("iconType", iconType);
        Assertions.notNull("ownerId", ownerId);

        return getAvatars(FieldMap.build(SYSTEM_AVATAR, NOT_SYSTEM, AVATAR_TYPE, iconType.getKey(), OWNER, ownerId));
    }

    @Override
    public List<Avatar> getSystemAvatarsForFilename(final IconType iconType, final String filename) throws DataAccessException {
        Assertions.notNull("iconType", iconType);
        Assertions.notNull("filename", filename);

        //noinspection ConstantConditions
        return systemAvatars.get(iconType.getKey()).get(filename);
    }

    /**
     * WARNING: BULK METHOD - IGNORES CACHE
     *
     * @param constraint search constraint
     * @return List of {@link Avatar} that match the constraint.
     */
    private List<Avatar> getAvatars(final Map<String, ?> constraint) {
        return ofBizDelegator.findByAnd(AVATAR_ENTITY, constraint).stream()
                .map(this::gvToAvatar)
                .collect(Collectors.toList());
    }

    private Map<String, Object> getNonPkFields(Avatar avatar) {
        return MapBuilder.<String, Object>newBuilder()
                .add(FILE_NAME, avatar.getFileName())
                .add(CONTENT_TYPE, avatar.getContentType())
                .add(AVATAR_TYPE, avatar.getIconType().getKey())
                .add(OWNER, avatar.getOwner())
                .add(SYSTEM_AVATAR, avatar.isSystemAvatar()? IS_SYSTEM : NOT_SYSTEM)
                .toMutableMap();
    }

    Avatar gvToAvatar(final GenericValue gv) {
        if (gv == null) {
            return null;
        }
        IconType iconType = IconType.of(gv.getString(AVATAR_TYPE));
        if (iconType == null) {
            log.error("Could not get icon type for name " + gv.getString(AVATAR_TYPE));
        }
        return new AvatarImpl(gv.getLong(ID),
                gv.getString(FILE_NAME),
                gv.getString(CONTENT_TYPE),
                iconType,
                gv.getString(OWNER),
                gv.getInteger(SYSTEM_AVATAR) != 0);
    }

    void tagLegacyAvatar(GenericValue gv) {
        final String fileName = gv.getString(FILE_NAME);
        final Long id = gv.getLong(ID);
        final Integer isSystem = gv.getInteger(SYSTEM_AVATAR);
        final String avatarType = gv.getString(AVATAR_TYPE);

        if (isSystem.equals(IS_SYSTEM) ||
                !avatarType.equals(IconType.USER_ICON_TYPE.getKey()) ||
                fileName.endsWith(TAGGED_AVATAR_FILE_SUFFIX)) {
            return;
        }

        try {
            String newFileName = avatarTagger.tagAvatar(id, fileName);
            gv.setString(FILE_NAME, newFileName);
            gv.setString(CONTENT_TYPE, "image/png");
        } catch (IOException | RuntimeException e) {
            log.warn("Could not convert avatar {} to new format with metadata." +
                    "\nThis avatar may be deleted during an upgrade to the next major version of JIRA." +
                    "\nAlso, if this avatar is embedded in reply emails picked up by the JIRA email handler, " +
                    "the handler may attach the avatar file to the associated issue", fileName);
            return;
        }
        try {
            gv.store();
        } catch (GenericEntityException e) {
            throw new DataAccessException(e);
        }
    }

    @SuppressWarnings("unused")
    @EventListener
    public void onClearCache(final ClearCacheEvent event) {
        taggedAvatars.removeAll();
        avatars.removeAll();
        systemAvatars.removeAll();
    }
}
