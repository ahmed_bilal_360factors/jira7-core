package com.atlassian.jira.cluster.zdu;

import com.atlassian.event.api.EventListener;

/**
 *
 * @since v7.3
 */
public class ClusterUpgradeLifecycleListener {

    private final Runnable onUpgradeStarted;
    private final Runnable onUpgradeFinished;
    private final Runnable onUpgradeCancelled;

    public ClusterUpgradeLifecycleListener(final Runnable onUpgradeStarted, final Runnable onUpgradeFinished, final Runnable onUpgradeCancelled) {
        this.onUpgradeStarted = onUpgradeStarted;
        this.onUpgradeFinished = onUpgradeFinished;
        this.onUpgradeCancelled = onUpgradeCancelled;
    }

    @EventListener
    public void onUpgradeStarted(final JiraUpgradeStartedEvent event) {
        onUpgradeStarted.run();
    }

    @EventListener
    public void onUpgradeCompleted(final JiraUpgradeFinishedEvent event) {
        onUpgradeFinished.run();
    }

    @EventListener
    public void onUpgradeCancelled(final JiraUpgradeCancelledEvent event) {
        onUpgradeCancelled.run();
    }
}
