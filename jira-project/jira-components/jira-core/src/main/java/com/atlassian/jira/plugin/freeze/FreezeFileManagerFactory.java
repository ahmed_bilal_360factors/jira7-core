package com.atlassian.jira.plugin.freeze;

import java.io.File;

/**
 * @since v7.3
 */
public interface FreezeFileManagerFactory {
    FreezeFileManager create(File freezeFile, File installedPluginsDirectory);
}
