package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.embedded.api.SearchRestriction;

/**
 * A search restriction that implements this interface indicates to JIRA that results <i>may</i> be unsorted.
 * Use this if unsorted results are acceptable for a performance boost when querying users or groups.
 *
 * @since 7.0
 */
public interface UnsortedRestriction extends SearchRestriction {
}