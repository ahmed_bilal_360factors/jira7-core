package com.atlassian.jira.mail.util;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarManagerImpl;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.avatar.AvatarTranscoder;
import com.atlassian.jira.cluster.ClusterSafe;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.mail.TemplateUser;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.ServletContextProvider;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toList;

public class MailAttachmentsManagerImpl implements MailAttachmentsManager {
    private static final Logger log = LoggerFactory.getLogger(MailAttachmentsManagerImpl.class);
    private static final Pattern IMG_TAG_WITH_SRC_PATTERN = Pattern.compile("(<img .*?src *= *['\"]?)([^\\s'\">]*)(['\"]?.*?>)");

    private final AvatarService avatarService;
    private final AvatarTranscoder avatarTranscoder;
    private final UserManager userManager;
    private final AvatarManager avatarManager;
    private final ApplicationProperties applicationProperties;

    @ClusterSafe("This is a local object used in the creation of a single email")
    private final Map<MailAttachment, String> mailAttachments = Collections.synchronizedMap(Maps.<MailAttachment, String>newHashMap());

    public MailAttachmentsManagerImpl(final AvatarService avatarService, final AvatarTranscoder avatarTranscoder,
                                      final UserManager userManager, final AvatarManager avatarManager, final ApplicationProperties applicationProperties) {
        this.avatarService = avatarService;
        this.avatarTranscoder = avatarTranscoder;
        this.userManager = userManager;
        this.avatarManager = avatarManager;
        this.applicationProperties = applicationProperties;
    }

    @Override
    public String getAvatarUrl(final String username) {
        final ApplicationUser user = userManager.getUserByName(username);
        return getAvatarUrl(user);
    }

    @Override
    public String getAvatarUrl(final TemplateUser templateUser) {
        return getAvatarUrl(templateUser.getName());
    }

    @Override
    public String getAvatarUrl(final ApplicationUser user) {
        if (user != null && avatarService.isUsingExternalAvatar(getLoggedInUser(), user)) {
            // If Gravatar is enabled we do not need to add Avatar as attachment
            return avatarService.getAvatarUrlNoPermCheck(user, Avatar.Size.defaultSize()).toString();
        }
        return addAttachmentAndReturnCid(createUserAvatarAttachment(user));
    }

    public String getIssueTypeIconUrl(final IssueType issueType) {
        return issueType.getAvatar() == null ?
                getImageUrl(issueType.getIconUrlHtml()) :
                addAttachmentAndReturnCid(createIssueTypeAvatarAttachment(issueType, issueType.getAvatar()));
    }

    private ApplicationUser getLoggedInUser() {
        return ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    }

    @Override
    public String addAttachmentAndReturnCid(final MailAttachment mailAttachment) {
        if (mailAttachments.containsKey(mailAttachment)) {
            return buildCidUrl(mailAttachments.get(mailAttachment));
        } else {
            String cid = generateCid(mailAttachment);
            if (mailAttachments.containsValue(cid)) {
                //shouldn't happen, but if append additional UUID to cid to ensure uniqueness
                cid = cid + UUID.randomUUID().toString();
            }
            mailAttachments.put(mailAttachment, cid);
            return buildCidUrl(cid);
        }
    }

    private String buildCidUrl(final String cid) {
        return "cid:" + cid;
    }

    private String generateCid(final MailAttachment attachment) {
        return CID_PREFIX + attachment.getUniqueName();
    }

    @Override
    public String getImageUrl(final String path) {
        final ServletContext servletContext = ServletContextProvider.getServletContext();
        try {
            //If path cannot be understood by servlet context it's probably absolutePath to external resource
            // and it cannot be attached to mail
            if (servletContext.getResource(path) == null) {
                return getAbsoluteUrl(path);
            }
        } catch (MalformedURLException | IllegalArgumentException e) {
            // servletContext.getResource() throws IllegalArgumentException for an invalid resource path since Tomcat 8
            return getAbsoluteUrl(path);
        }

        return addAttachmentAndReturnCid(MailAttachments.newImageAttachment(path, avatarTranscoder));
    }

    @Override
    public String inlineImages(final String html) {
        final Matcher matcher = IMG_TAG_WITH_SRC_PATTERN.matcher(html);
        final StringBuilder htmlWithInlineImages = new StringBuilder(html.length());

        int lastMatch = 0;
        while (matcher.find()) {
            // Append the part between the end of the last match and the '<img...'
            htmlWithInlineImages.append(html, lastMatch, matcher.start());

            // Append the part before the src: '<img...src="'
            htmlWithInlineImages.append(matcher.group(1));

            final String inlinedImage = getImageUrl(removeBaseUrl(matcher.group(2)));
            if (inlinedImage.startsWith("cid:")) {
                // Get a cid for the URL and append it: 'cid:...'
                htmlWithInlineImages.append(inlinedImage);
            } else {
                // Can't inline it, restore the original url
                htmlWithInlineImages.append(matcher.group(2));
            }

            // Append the part before the src: '"...></img>'
            htmlWithInlineImages.append(matcher.group(3));

            // Save the position of the last match
            lastMatch = matcher.end();
        }
        // Append the part between the last match and the end of the input
        htmlWithInlineImages.append(html, lastMatch, html.length());

        return htmlWithInlineImages.toString();
    }

    @Override
    public String removeBaseUrl(final String url) {
        final String baseUrl = applicationProperties.getString(APKeys.JIRA_BASEURL);
        final String basePath = getBasePath();

        final String urlWithRemovedBaseUrl = StringUtils.removeStart(url, baseUrl);
        return StringUtils.removeStart(urlWithRemovedBaseUrl, basePath);
    }

    /**
     * Gets the path (aka contextPath) from JIRA's base url.
     *
     * @return the path of the JIRA base url.
     */
    private String getBasePath() {
        final String baseUrl = applicationProperties.getString(APKeys.JIRA_BASEURL);
        final URI url;

        try {
            url = new URI(baseUrl);
            return url.getPath();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getExternalImageUrl(final String path) {
        try {
            final URI uri = new URI(path);
            if (uri.isAbsolute()) {
                return path;
            }

            return addAttachmentAndReturnCid(MailAttachments.newUrlImageAttachment(getAbsoluteUrl(path)));
        } catch (URISyntaxException e) {
            log.trace("Cannot understand URI: " + path, e);
            return path;
        }
    }

    protected String getAbsoluteUrl(final String path) {
        final String baseUrl = applicationProperties.getString(APKeys.JIRA_BASEURL);
        try {
            final URI uri = new URI(path);
            return uri.isAbsolute() ? path : StringUtils.stripEnd(baseUrl, "/") + "/" + StringUtils.stripStart(path, "/");
        } catch (URISyntaxException e) {
            log.trace("Cannot understand URI: " + path, e);
            return path;
        }
    }

    @Override
    public int getAttachmentsCount() {
        return mailAttachments.size();
    }

    @Override
    public Iterable<BodyPart> buildAttachmentsBodyParts() {
        return mailAttachments.entrySet().stream()
                .map(entry -> {
                    final BodyPart bodyPart = entry.getKey().buildBodyPart();
                    if (bodyPart != null) {
                        try {
                            final String cid = entry.getValue();
                            bodyPart.setHeader("Content-ID", String.format("<%s>", cid));

                        } catch (MessagingException e) {
                            log.warn("Cannot add 'Content-ID' header to mail part", e);
                            return null;
                        }
                    }
                    return bodyPart;
                })
                .filter(o -> o != null)
                .collect(toList());
    }

    private MailAttachment createUserAvatarAttachment(final ApplicationUser user) {
        final String userName = null == user ? "anonymous" : user.getName();
        final String avatarOwnerDescription = "user: " + userName;

        final Avatar avatar = avatarService.getAvatarTagged(user, user);
        final Optional<Avatar> jiraAvatar = getJiraAvatar(avatar);

        if (jiraAvatar.isPresent() && AvatarManagerImpl.isAvatarTranscodeable(jiraAvatar.get())) {
            return MailAttachments.newTranscodedAvatarAttachment(jiraAvatar.get(), avatarOwnerDescription, avatarManager);
        }

        return MailAttachments.newAvatarAttachment(avatar, avatarOwnerDescription, avatarManager);
    }

    private MailAttachment createIssueTypeAvatarAttachment(final IssueType issueType, @Nonnull final Avatar avatar) {
        if (AvatarManagerImpl.isAvatarTranscodeable(avatar)) {
            // Note that this will pick up the default size, which is MEDIUM (32x32). To make issue types appear
            // the same size in e-mails as on the screen, append the Avatar.Size.SMALL (16x16) parameter to this call.
            return MailAttachments.newTranscodedAvatarAttachment(avatar, "issue type: " + issueType.getName(), avatarManager);
        }

        return MailAttachments.newAvatarAttachment(avatar, "issue type: " + issueType.getName(), avatarManager);
    }

    /**
     * In some cases getting an id of avatar might return null or throw an Exception.
     * This method handles this critical path and tries to obtain JIRA specific avatar.
     */
    private Optional<Avatar> getJiraAvatar(final Avatar avatar) {
        try {
            final Long avatarId = avatar.getId();
            if (avatarId == null) {
                return Optional.empty();
            } else {
                return Optional.ofNullable(avatarManager.getById(avatarId));
            }
        } catch (Exception e) {
            log.debug("Could not obtain JIRA avatar.", e);
            return Optional.empty();
        }
    }
}
