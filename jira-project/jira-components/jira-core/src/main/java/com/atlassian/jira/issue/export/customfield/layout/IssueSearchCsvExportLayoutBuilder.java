package com.atlassian.jira.issue.export.customfield.layout;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.issue.export.FieldExportPart;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.IssueLinksSystemField;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * Used to generate the layout. This will determine the ordering of the fields as well as the number of columns
 * needed.
 *
 * @since 7.2
 */
public class IssueSearchCsvExportLayoutBuilder {

    private final Map<Field, FieldMetaData> fields = Maps.newLinkedHashMap();

    /**
     * Given the register {@link FieldExportParts} generate the layout with a determined field ordering.
     * @return CsvLayout for export.
     */
    public IssueSearchCsvExportLayout build() {
        final List<IssueSearchCsvExportLayout.FieldLayout> fieldLayouts = Lists.newArrayList();
        for (final Field field: fields.keySet()) {
            final FieldMetaData fieldMeta = fields.get(field);

            final Map<String, String> fieldHeaders = fieldMeta.getFieldItemHeader();
            final Map<String, Integer> fieldMaxCount = fieldMeta.getMaxCount();

            final List<IssueSearchCsvExportLayout.SubFieldLayout> subFieldLayouts = fieldMeta.getFieldItemOrdering().stream()
                    .map(id -> new IssueSearchCsvExportLayout.SubFieldLayout(fieldHeaders.get(id), fieldMaxCount.get(id), id))
                    .collect(CollectorsUtil.toImmutableList());
            final IssueSearchCsvExportLayout.FieldLayout fieldLayout = new IssueSearchCsvExportLayout.FieldLayout(subFieldLayouts, field);

            fieldLayouts.add(fieldLayout);
        }

        return new IssueSearchCsvExportLayout(fieldLayouts);
    }

    /**
     * Add a given field repesentation to figure out the layout. This deals with maintaining how many columns
     * are needed for certain fields, etc.
     * @param fieldRepresentation to register in the layout.
     */
    public void registerFieldRepresentation(Field field, FieldExportParts fieldRepresentation) {
        if (!fields.containsKey(field)) {
            fields.put(field, new FieldMetaData());
        }

        fields.get(field).registerFieldRepresentation(field, fieldRepresentation);
    }


    /**
     * This is a temporary class in the builder used to calculate the maximum columns needed for each Fields,
     * sub fields. For example, the comment field stores multiple comments so this would store the maximum number
     * of columns needed to display the commments for all the issues.
     */
    private class FieldMetaData {
        List<String> fieldItemOrdering = Lists.newArrayList();
        Map<String, String> fieldItemHeader = Maps.newHashMap();
        Map<String, Integer> maxCount = Maps.newHashMap();

        /**
         * This registers a new field representation updating the maximum column counts needed for each
         * FieldExportRepresentationItem as well as tracking the field item order. For example, if the items in the
         * FieldExportRepresentation in one issue have the id's a,b,c and then another issue has id's a,d,b,z.
         * The outputted final ordering would be a,d,b,z,c. This is just to maintain consistency. This is assuming
         * that the field doesn't do anything wacky and start doing random ordering like: a,b,c and then c,b,a.
         * @param fieldRepresentation to add to the registry.
         */
        public void registerFieldRepresentation(Field field, FieldExportParts fieldRepresentation) {
            Integer currentIdInsertionPoint = 0;
            for (final FieldExportPart item: fieldRepresentation.getParts()) {
                final String id = item.getId();
                final int count = (int) item.getValues().count();
                if (!fieldItemHeader.containsKey(id)) {
                    fieldItemHeader.put(id, item.getItemLabel());
                    maxCount.put(id, count);
                } else if (maxCount.get(id) < count) {
                    maxCount.put(id, count);
                }

                final Integer idPositionIndex = fieldItemOrdering.indexOf(id);
                if (idPositionIndex == -1) {
                    fieldItemOrdering.add(currentIdInsertionPoint, id);
                    currentIdInsertionPoint += 1;
                } else {
                    currentIdInsertionPoint = idPositionIndex + 1;
                }
            }

            /**
             * Issue links should sort by name of the issue link type for consistency. So for example, if there
             * are the following issue links, duplicates, blocks, relates to. It would always output in the order
             * blocks, duplicates, relates to. This is for testing making it consistent as well as for the users.
             */
            if (field instanceof IssueLinksSystemField) {
                Collections.sort(fieldItemOrdering);
            }
        }

        public List<String> getFieldItemOrdering() {
            return fieldItemOrdering;
        }

        public Map<String, String> getFieldItemHeader() {
            return fieldItemHeader;
        }

        public Map<String, Integer> getMaxCount() {
            return maxCount;
        }
    }

}
