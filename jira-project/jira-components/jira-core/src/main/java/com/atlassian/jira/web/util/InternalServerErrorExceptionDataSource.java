package com.atlassian.jira.web.util;

import com.atlassian.jira.util.ExceptionInterpreterUtil;
import com.atlassian.jira.util.system.ExtendedSystemInfoUtils;
import com.atlassian.logging.log4j.StackTraceInfo;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nullable;
import javax.servlet.ServletException;

public class InternalServerErrorExceptionDataSource {
    private final Throwable exception;
    private final ExtendedSystemInfoUtils extendedSystemInfoUtils;
    private String stacktrace = null;
    private String rootCause = null;

    public InternalServerErrorExceptionDataSource(final Throwable exception, @Nullable final ExtendedSystemInfoUtils extendedSystemInfoUtils) {
        this.exception = exception;
        this.extendedSystemInfoUtils = extendedSystemInfoUtils;
        parseException();
    }

    protected void parseException() {
        if (exception != null) {
            Throwable cause = exception;
            if (exception instanceof ServletException) {
                Throwable rootCause = ((ServletException) exception).getRootCause();
                if (rootCause != null) {
                    cause = rootCause;
                }
            }
            rootCause = cause.toString();
            stacktrace = StackTraceInfo.asString(cause);
        }
    }

    public String getInterpretedMessage() {
        if (extendedSystemInfoUtils != null) {
            String execute = ExceptionInterpreterUtil.execute(extendedSystemInfoUtils, getStacktrace());
            return StringUtils.defaultString(execute);
        }
        return "";
    }


    public String getStacktrace() {
        return StringUtils.defaultString(stacktrace);
    }

    public String getRootCause() {
        return StringUtils.defaultString(rootCause);
    }
}
