package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.plugin.webfragment.conditions.cache.ConditionCacheKeys;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nullable;

import static com.atlassian.jira.plugin.webfragment.conditions.cache.RequestCachingConditionHelper.cacheConditionResultInRequest;

/**
 * Checks that the current user is a project admin for at least one project.
 */
public class UserIsProjectAdminCondition extends AbstractWebCondition {
    private final PermissionManager permissionManager;

    public UserIsProjectAdminCondition(PermissionManager permissionManager) {
        this.permissionManager = permissionManager;
    }

    public boolean shouldDisplay(@Nullable final ApplicationUser user, @Nullable JiraHelper jiraHelper) {
        return cacheConditionResultInRequest(ConditionCacheKeys.custom("isAnyProjectAdmin", user),
                () -> permissionManager.hasProjects(ProjectPermissions.ADMINISTER_PROJECTS, user));
    }
}
