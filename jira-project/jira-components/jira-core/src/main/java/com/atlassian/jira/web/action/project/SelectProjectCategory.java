package com.atlassian.jira.web.action.project;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.entity.ProjectCategoryFactory;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.ofbiz.core.entity.GenericEntityException;

import java.util.Collection;

import static com.google.common.collect.Lists.newArrayList;

@WebSudoRequired
public class SelectProjectCategory extends JiraWebActionSupport {
    public static final long NONE_PROJECT_CATEGORY_ID = -1L;
    private Long pid;
    private Long projectCategoryId;
    private String source;

    private final ProjectManager projectManager;

    public SelectProjectCategory(final ProjectManager projectManager) {
        this.projectManager = projectManager;
    }

    @Override
    public String doDefault() throws Exception {
        final ProjectCategory projectCategory = projectManager.getProjectCategoryForProject(getProject());

        if (null != projectCategory) {
            setProjectCategoryId(projectCategory.getId());
        } else {
            setProjectCategoryId(NONE_PROJECT_CATEGORY_ID);
        }

        return super.doDefault();
    }

    @Override
    protected void doValidation() {
        if (isCurrentProjectValid()) {
            addErrorMessage(getText("admin.errors.project.specify.project"));
        }

        if (getProjectCategory().isEmpty() && isSelectedProjectCategoryNotNone()) {
            addError(ProjectService.PROJECT_CATEGORY_ID, getText("admin.errors.project.specify.project.category"));
        }
    }

    /**
     * Given a project, remove all project category links, then create one if supplied a project category.
     */
    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        projectManager.setProjectCategory(getProject(), getProjectCategory().getOrNull());

        if("viewprojects".equals(getSource())){
            return returnCompleteWithInlineRedirect("/secure/project/ViewProjects.jspa");
        }
        return returnCompleteWithInlineRedirect("/plugins/servlet/project-config/" + getProject().getKey() + "/summary");
    }

    public Collection<ProjectCategory> getProjectCategories() throws GenericEntityException {
        final Collection<ProjectCategory> projectCategoriesToDisplay = newArrayList(createEmptyProjectCategory());
        projectCategoriesToDisplay.addAll(projectManager.getAllProjectCategories());
        return projectCategoriesToDisplay;
    }

    private ProjectCategory createEmptyProjectCategory() {
        return new ProjectCategoryFactory.Builder().id(-1L).name("None").build();
    }

    public Project getProject() {
        return projectManager.getProjectObj(getPid());
    }

    private Option<ProjectCategory> getProjectCategory() {
        if (null == getProjectCategoryId() || getProjectCategoryId().equals(NONE_PROJECT_CATEGORY_ID)) {
            return Option.none();
        }

        return Option.some(projectManager.getProjectCategoryObject(getProjectCategoryId()));
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Long getProjectCategoryId() {
        return projectCategoryId;
    }

    public void setProjectCategoryId(final Long projectCategoryId) {
        this.projectCategoryId = projectCategoryId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    private boolean isSelectedProjectCategoryNotNone() {
        return getProjectCategoryId() != null && !getProjectCategoryId().equals(NONE_PROJECT_CATEGORY_ID);
    }

    private boolean isCurrentProjectValid() {
        return null == getProject();
    }
}
