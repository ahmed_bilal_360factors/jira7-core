package com.atlassian.jira.web.servlet;

/**
 * Represents a partial range of bytes to be returned in a partial HTTP request.
 *
 * @since v7.2
 */
public class RangeResponse {
    private final int startIndex;
    private final Integer endIndex;
    private final int totalFileLength;

    public RangeResponse(int startIndex, Integer endIndex, int totalFileLength) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.totalFileLength = totalFileLength;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public Integer getEndIndex() {
        return endIndex;
    }

    /**
     * Calculates the "Content-Length" response header value.
     * <p>
     * That is, the number of bytes we will return, based on the requested range and also the actual length of the file.
     * See https://tools.ietf.org/html/rfc7233#section-4.1
     * </p>
     *
     * @return the number of bytes we will return for this range.
     */
    public int calculateContentLength() {
        if (endIndex == null) {
            return totalFileLength - startIndex;
        } else {
            return endIndex - startIndex + 1;
        }
    }

    /**
     * Calculates the "Content-Range" response header value.
     * <p>
     * eg "Content-Range: bytes 21010-47021/47022"
     * See https://tools.ietf.org/html/rfc7233#section-4.1
     * </p>
     *
     * @return the number of bytes we will return for this range.
     */
    public String calculateContentRange() {
        int end = endIndex == null ? totalFileLength - 1 : endIndex;

        // eg "bytes 21010-47021/47022"
        return "bytes " + startIndex + "-" + end + "/" + totalFileLength;
    }

    @Override
    public String toString() {
        return calculateContentRange();
    }
}
