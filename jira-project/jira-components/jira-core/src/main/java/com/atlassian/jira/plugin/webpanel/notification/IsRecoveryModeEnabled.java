package com.atlassian.jira.plugin.webpanel.notification;

import com.atlassian.jira.user.util.RecoveryMode;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * A condition that returns {@code true} when the recovery mode is enabled.
 *
 * @see com.atlassian.jira.user.util.RecoveryMode
 * @since 7.0
 */
public final class IsRecoveryModeEnabled implements Condition {
    private final RecoveryMode recoveryMode;

    public IsRecoveryModeEnabled(final RecoveryMode recoveryMode) {
        this.recoveryMode = notNull("recoveryMode", recoveryMode);
    }

    @Override
    public void init(final Map<String, String> params) {
    }

    @Override
    public boolean shouldDisplay(final Map<String, Object> context) {
        return recoveryMode.isRecoveryModeOn();
    }
}
