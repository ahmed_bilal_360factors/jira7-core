package com.atlassian.jira.config.database;


import org.ofbiz.core.entity.config.ConnectionPoolInfo;
import org.ofbiz.core.entity.config.DatasourceInfo;
import org.ofbiz.core.entity.config.JdbcDatasourceInfo;

/**
 * Wrapper of DatasourceInfo that allows HSQL to start with DBCP2,
 * by providing validation query as HSQL driver does not support isValid method.
 *
 * @since v7.1
 */
public class LegacyHsqlDatasourceInfo extends DatasourceInfo {

    public LegacyHsqlDatasourceInfo(DatasourceInfo datasourceInfo) {
        super(datasourceInfo.getName(), datasourceInfo.getFieldTypeName(), datasourceInfo.getSchemaName(),
                new LegacyHsqlJdbcDatasourceInfo(datasourceInfo.getJdbcDatasource()));
    }

    static class LegacyHsqlJdbcDatasourceInfo extends JdbcDatasourceInfo {
        public LegacyHsqlJdbcDatasourceInfo(JdbcDatasourceInfo jdbcDatasource) {
            super(jdbcDatasource.getUri(),
                    jdbcDatasource.getDriverClassName(),
                    jdbcDatasource.getUsername(),
                    jdbcDatasource.getPassword(),
                    jdbcDatasource.getIsolationLevel(),
                    jdbcDatasource.getConnectionProperties(),
                    new ConnectionPoolInfo(
                            jdbcDatasource.getConnectionPoolInfo().getMaxSize(),
                            jdbcDatasource.getConnectionPoolInfo().getMinSize(),
                            jdbcDatasource.getConnectionPoolInfo().getMaxWait(),
                            jdbcDatasource.getConnectionPoolInfo().getSleepTime(),
                            jdbcDatasource.getConnectionPoolInfo().getLifeTime(),
                            jdbcDatasource.getConnectionPoolInfo().getDeadLockMaxWait(),
                            jdbcDatasource.getConnectionPoolInfo().getDeadLockRetryWait(),
                            "SELECT 1 FROM INFORMATION_SCHEMA.SYSTEM_USERS",
                            jdbcDatasource.getConnectionPoolInfo().getMinEvictableTimeMillis(),
                            jdbcDatasource.getConnectionPoolInfo().getTimeBetweenEvictionRunsMillis())
            );
        }
    }
}
