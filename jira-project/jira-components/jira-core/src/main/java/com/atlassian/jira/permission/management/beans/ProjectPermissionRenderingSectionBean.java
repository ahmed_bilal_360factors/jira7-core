package com.atlassian.jira.permission.management.beans;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.List;

@JsonAutoDetect
public class ProjectPermissionRenderingSectionBean {
    private String sectionType;
    private String heading;
    private List<String> permissions;

    public ProjectPermissionRenderingSectionBean() {
    }

    private ProjectPermissionRenderingSectionBean(String sectionType, String heading, Iterable<String> permissions) {
        this.sectionType = sectionType;
        this.heading = heading;
        this.permissions = permissions != null ? ImmutableList.copyOf(permissions) : null;
    }

    public String getSectionType() {
        return sectionType;
    }

    public void setSectionType(String sectionType) {
        this.sectionType = sectionType;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions != null ? ImmutableList.copyOf(permissions) : null;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(ProjectPermissionRenderingSectionBean data) {
        return new Builder(data);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProjectPermissionRenderingSectionBean that = (ProjectPermissionRenderingSectionBean) o;

        return Objects.equal(this.sectionType, that.sectionType) &&
                Objects.equal(this.heading, that.heading) &&
                Objects.equal(this.permissions, that.permissions);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(sectionType, heading, permissions);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("sectionType", sectionType)
                .add("heading", heading)
                .add("permissions", permissions)
                .toString();
    }

    public static final class Builder {


        private String sectionType;
        private String heading;
        private List<String> permissions = Lists.newArrayList();

        private Builder() {
        }

        private Builder(ProjectPermissionRenderingSectionBean initialData) {

            this.sectionType = initialData.sectionType;
            this.heading = initialData.heading;
            this.permissions = initialData.permissions;
        }


        public Builder setSectionType(String sectionType) {
            this.sectionType = sectionType;
            return this;
        }


        public Builder setHeading(String heading) {
            this.heading = heading;
            return this;
        }


        public Builder setPermissions(List<String> permissions) {
            this.permissions = permissions;
            return this;
        }


        public Builder addPermission(String permission) {
            this.permissions.add(permission);
            return this;
        }

        public Builder addPermissions(Iterable<String> permissions) {
            for (String permission : permissions) {
                addPermission(permission);
            }
            return this;
        }


        public ProjectPermissionRenderingSectionBean build() {
            return new ProjectPermissionRenderingSectionBean(sectionType, heading, permissions);
        }
    }
}
