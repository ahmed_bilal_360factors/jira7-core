package com.atlassian.jira.transaction;

/**
 * A queue of runnables that can be offered and then publishes them in a FIFO manner in Txn commit
 */
public interface RunnablesQueue {

    /**
     * Offer a runnable to be run on transaction commit.
     *
     * @param runnable the runnable in play
     */
    void offer(Runnable runnable);

    /**
     * Clears the current queue of runnables.
     */
    void clear();

    /**
     * Runs the runnable code currently in the queue and then clears the queue.
     */
    void runAndClear();
}
