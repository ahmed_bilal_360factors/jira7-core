package com.atlassian.jira.plugin.webwork;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.login.LoginManager;
import com.atlassian.seraph.SecurityService;
import com.atlassian.seraph.config.SecurityConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Set;

/**
 * JiraSeraphSecurityService configures Seraph based on Webwork plugin module atlassian-plugin.xml
 * <p>
 * This allows for the roles-required attribute to be used within plugins and for pluggable
 * Authorisation as well.
 *
 * @since v5.0
 */
public class JiraSeraphSecurityService implements SecurityService {
    private static final Logger LOG = LoggerFactory.getLogger(JiraSeraphSecurityService.class);

    /**
     * Seraph Initable initialisation method.
     * As we rely on plugin events to setup our required roles, we don't do anything here
     */
    public void init(Map<String, String> params, SecurityConfig config) {
    }

    /**
     * Seraph Initable cleanup method.
     */
    public void destroy() {
    }

    /**
     * This hands off to the LoginManager, who is able to live in the pico container
     */
    public Set<String> getRequiredRoles(final HttpServletRequest request) {
        return ComponentAccessor.getComponentSafely(LoginManager.class)
                .map(loginManager -> loginManager.getRequiredRoles(request))
                .orElseThrow(() ->
                {
                    LOG.debug("Request {} bypassed Johnson but still requested a security check", request.getRequestURI());
                    return new IllegalStateException("Still initializing");
                });
    }
}
