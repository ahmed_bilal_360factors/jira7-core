package com.atlassian.jira.database;

import com.atlassian.annotations.ExperimentalApi;

import javax.annotation.Nonnull;

/**
 * Factory for choosing an appropriate {@link DatabaseSystemTimeReader} for the current database type.
 *
 * @since 6.3.4
 */
@ExperimentalApi
public interface DatabaseSystemTimeReaderFactory {
    /**
     * Chooses a database system time reader for the current database.
     *
     * @return a database system time reader.
     */
    @Nonnull
    DatabaseSystemTimeReader getReader();
}
