package com.atlassian.jira.web.dispatcher;

import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.base.Throwables;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import webwork.config.util.ActionInfo;
import webwork.dispatcher.ActionResult;

import javax.annotation.Nonnull;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.function.Supplier;

import static com.atlassian.jira.component.ComponentAccessor.getComponentSafely;

/**
 * This can render an actions view via the Soy Template system and write the result directly to the {@link
 * javax.servlet.http.HttpServletResponse}
 *
 * @since v6.0
 */
class JiraSoyViewDispatcher {
    private static final Logger LOG = LoggerFactory.getLogger(JiraSoyViewDispatcher.class);
    private static final Supplier<ServletException> UNAVAILABLE = () -> new ServletException("Soy renderer unavailable");

    private final ActionViewDataSupport actionViewDataSupport = new ActionViewDataSupport();

    public void dispatch(HttpServletResponse httpServletResponse, final ActionResult ar, ActionInfo.ViewInfo viewInfo)
            throws ServletException, IOException {
        try {
            final Map<String, Object> parameters = Maps.newHashMap(actionViewDataSupport.getData(ar, viewInfo));
            render(httpServletResponse, viewInfo, parameters);
        } catch (SoyException e) {
            LOG.debug("Unable to render soy template for view '{}'", e);
            throw new ServletException(Throwables.getRootCause(e));
        }
    }

    private static void render(final HttpServletResponse response, final ActionInfo.ViewInfo viewInfo,
                               final Map<String, Object> parameters) throws ServletException, IOException {
        final SoyTemplateAddress soy = SoyTemplateAddress.address(viewInfo);
        soyRenderer().render(response.getWriter(), soy.getCompleteKey(), soy.getTemplateName(), parameters);
    }

    @Nonnull
    private static SoyTemplateRenderer soyRenderer() throws ServletException {
        return getComponentSafely(SoyTemplateRendererProvider.class)
                .map(SoyTemplateRendererProvider::getRenderer)
                .orElseThrow(UNAVAILABLE);
    }
}
