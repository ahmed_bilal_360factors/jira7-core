package com.atlassian.jira.security.auth;

import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.security.login.LoginLoggers;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.plugin.webwork.WebworkPluginSecurityServiceHelper;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.ozymandias.SafePluginPointAccess;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.collect.Sets;
import org.apache.log4j.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

import static com.atlassian.jira.security.auth.Authorisation.Decision;
import static com.atlassian.jira.security.auth.Authorisation.Decision.ABSTAIN;
import static com.atlassian.jira.security.auth.Authorisation.Decision.DENIED;
import static com.atlassian.jira.security.auth.Authorisation.Decision.GRANTED;
import static com.atlassian.jira.security.auth.Authorisation.Decision.toDecision;
import static java.lang.String.format;

/**
 */
public class AuthorisationManagerImpl implements AuthorisationManager {
    private static final Logger loggerSecurityEvents = LoginLoggers.LOGIN_SECURITY_EVENTS;

    private final PermissionManager permissionManager;
    private final PluginAccessor pluginAccessor;
    private final WebworkPluginSecurityServiceHelper webworkPluginSecurityServiceHelper;
    private final ApplicationRoleManager applicationRoleManager;
    private final GlobalPermissionManager globalPermissions;


    public AuthorisationManagerImpl(PermissionManager permissionManager, PluginAccessor pluginAccessor,
                                    WebworkPluginSecurityServiceHelper webworkPluginSecurityServiceHelper,
                                    ApplicationRoleManager applicationRoleManager, GlobalPermissionManager globalPermissions) {
        this.permissionManager = permissionManager;
        this.pluginAccessor = pluginAccessor;
        this.webworkPluginSecurityServiceHelper = webworkPluginSecurityServiceHelper;
        this.applicationRoleManager = applicationRoleManager;
        this.globalPermissions = globalPermissions;
    }

    @Override
    public boolean authoriseForLogin(@Nonnull ApplicationUser user, HttpServletRequest httpServletRequest) {
        Decision decision = authoriseForLoginViaPlugins(user, httpServletRequest);
        if (decision == ABSTAIN) {
            // we do JIRA second so that plugins can get in first.
            decision = authoriseForLoginViaJIRA(user);
        }
        boolean authorised = decision.toBoolean();
        if (!authorised) {
            loggerSecurityEvents.warn("The user '" + safeUserName(user) + "' is NOT AUTHORIZED to perform to login for this request");
        }
        return authorised;

    }

    private Decision authoriseForLoginViaPlugins(final ApplicationUser user, final HttpServletRequest httpServletRequest) {
        if (user != null) {
            for (final Authorisation authorisation : getAuthorisations()) {
                Decision decision = safeRun(authorisation, user, () -> authorisation.authoriseForLogin(user, httpServletRequest));
                if (decision == ABSTAIN) {
                    continue;
                }
                return decision;
            }
        }
        return ABSTAIN;
    }

    @Override
    public boolean hasUserAccessToJIRA(@Nonnull final ApplicationUser applicationUser) {
        return authoriseForLoginViaJIRA(applicationUser).toBoolean();
    }

    private Decision authoriseForLoginViaJIRA(ApplicationUser user) {
        return applicationRoleManager.hasAnyRole(user)
                || globalPermissions.hasPermission(GlobalPermissionKey.ADMINISTER, user)
                ? GRANTED
                : DENIED;
    }

    @Override
    public Set<String> getRequiredRoles(HttpServletRequest httpServletRequest) {
        Set<String> requiredRoles = Sets.newHashSet(webworkPluginSecurityServiceHelper.getRequiredRoles(httpServletRequest));
        for (Authorisation authorisation : getAuthorisations()) {
            try {
                Set<String> set = authorisation.getRequiredRoles(httpServletRequest);
                requiredRoles.addAll(safeSet(set));
            } catch (RuntimeException e) {
                loggerSecurityEvents.error(format("Exception thrown by '%s'. The roles will be ignored : %s", authorisation.getClass().getName(), e.getMessage()));
            }
        }
        return requiredRoles;
    }

    @Override
    public boolean authoriseForRole(@Nullable ApplicationUser user, HttpServletRequest httpServletRequest, String role) {
        Decision decision = authoriseForRoleViaPlugins(user, httpServletRequest, role);
        if (decision == ABSTAIN) {
            // we do JIRA second so that plugins can get in first.
            decision = authoriseForRoleViaJIRA(user, role);
        }
        boolean authorised = decision.toBoolean();
        if (!authorised) {
            loggerSecurityEvents.warn("The user '" + safeUserName(user) + "' is NOT AUTHORIZED to perform this request");
        }
        return authorised;
    }

    private Decision authoriseForRoleViaPlugins(final ApplicationUser user, final HttpServletRequest httpServletRequest, final String role) {
        for (final Authorisation authorisation : getAuthorisations()) {
            Decision decision = safeRun(authorisation, user, new Callable<Authorisation.Decision>() {
                @Override
                public Decision call() throws Exception {
                    return authorisation.authoriseForRole(user, httpServletRequest, role);
                }
            });
            if (decision == ABSTAIN) {
                continue;
            }
            return decision;
        }
        return ABSTAIN;
    }

    private List<Authorisation> getAuthorisations() {
        return SafePluginPointAccess
                .to(pluginAccessor)
                .forType(AuthorisationModuleDescriptor.class,
                        (moduleDescriptor, module) -> module);
    }

    private Decision authoriseForRoleViaJIRA(ApplicationUser user, String role) {
        // taken from the old JiraRoleMapper code and moved into here for consistency
        final int permissionType = Permissions.getType(role);
        if (permissionType == -1) {
            // change of behaviour here.  In the old day before pluggable roles, then set of roles
            // was a known set and hence this post condition was a valid thing to do.
            //
            // but now with pluggable roles, this is no longer the case
            //
            //throw new IllegalArgumentException("Unknown role '" + role + "'");
            //
            return DENIED;
        }
        return toDecision(permissionManager.hasPermission(permissionType, user));
    }


    private Decision safeRun(Authorisation authorisation, ApplicationUser user, Callable<Authorisation.Decision> callable) {
        try {
            Decision decision = callable.call();
            if (loggerSecurityEvents.isDebugEnabled()) {
                loggerSecurityEvents.debug(format("%s has authorised '%s' as %s", authorisation.getClass().getName(), safeUserName(user), decision));
            }
            return decision;
        } catch (Exception e) {
            //
            // if any plugin throws an exception then we define to mean ABSTAIN.  We dont want plugins bring down the request
            //
            loggerSecurityEvents.error(format("Exception thrown by '%s'. The decision will be treated as ABSTAIN : %s", authorisation.getClass().getName(), e.getMessage()));
            return ABSTAIN;
        }
    }

    private String safeUserName(ApplicationUser user) {
        return user == null ? "anonymous" : user.getUsername();
    }

    private Set<String> safeSet(Set<String> set) {
        return set == null ? Collections.<String>emptySet() : set;
    }
}
