package com.atlassian.jira.security;

import com.atlassian.jira.security.login.LoginManager;
import com.atlassian.jira.servermetrics.NoopServerMetricsDetailCollector;
import com.atlassian.jira.servermetrics.RequestCheckpoints;
import com.atlassian.jira.servermetrics.ServerMetricsDetailCollector;
import com.atlassian.jira.startup.JiraStartupChecklist;
import com.atlassian.sal.api.user.UserRole;
import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.seraph.config.SecurityConfigFactory;
import com.atlassian.seraph.filter.SecurityFilter;
import com.atlassian.seraph.util.RedirectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Set;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import static com.atlassian.jira.component.ComponentAccessor.getComponentSafely;
import static com.atlassian.jira.web.filters.johnson.ServiceUnavailableResponder.respondWithEmpty503;

/**
 * A wrapper around the Seraph SecurityFilter.
 */
public class JiraSecurityFilter extends SecurityFilter {
    private static final Logger log = LoggerFactory.getLogger(JiraSecurityFilter.class);

    public void init(FilterConfig config) {
        log.debug("Initing JIRA security filter");
        init(config, true);
        log.debug("JIRA security filter inited");
    }

    protected void init(FilterConfig config, boolean startupCheck) {
        if (!startupCheck || JiraStartupChecklist.startupOK()) {
            super.init(config);
        }
    }

    @Override
    protected String getLoginUrl(final HttpServletRequest request, final Set<String> missingRoles) {
        UserRole userRole = null;

        // Inform user that there is certain level of privileges required to access page
        // SecurityFilter is using roles-required attribute from actions.xml

        if (missingRoles.contains(Permissions.getShortName(Permissions.SYSTEM_ADMIN))) {
            userRole = UserRole.SYSADMIN;
        } else if (missingRoles.contains(Permissions.getShortName(Permissions.ADMINISTER))) {
            userRole = UserRole.ADMIN;
        }

        final SecurityConfig securityConfig = SecurityConfigFactory.getInstance();
        String loginURL = securityConfig.getLoginURL(userRole != null, false);

        if (userRole != null) {
            loginURL = loginURL.replaceAll("\\$\\{userRole\\}", userRole.toString());
        }

        return RedirectUtils.getLoginURL(loginURL, request);
    }

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
            throws IOException, ServletException {

        final ServerMetricsDetailCollector serverMetricsDetailCollector = getServerMetricsDetailCollector();

        serverMetricsDetailCollector.checkpointReachedOnce(RequestCheckpoints.beforeSecurityFilter.name());

        if (getComponentSafely(LoginManager.class).isPresent()) {
            super.doFilter(req, res, (request, response) -> {
                serverMetricsDetailCollector.checkpointReachedOnce(RequestCheckpoints.jiraSecurityFilter.name());
                chain.doFilter(request, response);
            });
        } else {
            rejectWith503((HttpServletRequest) req, res);
            serverMetricsDetailCollector.checkpointReachedOnce(RequestCheckpoints.jiraSecurityFilter.name());
        }
    }

    private ServerMetricsDetailCollector getServerMetricsDetailCollector() {
        return getComponentSafely(ServerMetricsDetailCollector.class).orElseGet(NoopServerMetricsDetailCollector::new);
    }

    /**
     * Rejects requests that reach this filter when the login manager is unavailable to provide information about
     * what security roles are required.
     * <p>
     * So here's the deal:
     * </p>
     * <ul>
     * <li>For some reason, we can't get a login manager (probably Pico isn't ready for us, yet).</li>
     * <li>Without the LoginManager, we don't know how to do security filtering correctly.</li>
     * <li>Normally, we would have expected the Johnson filter to catch this and reject the request.</li>
     * <li>It didn't for some reason -- probably because you're accessing something special like static
     * resources.</li>
     * <li>Strangely, this something special still wants security filtering even though it does not want
     * Johnson filtering.  This is dumb.</li>
     * <li>Letting the request through when the security filter can't tell us what security to enforce
     * would be dumb squared.</li>
     * </ul>
     * <p>
     * If the Johnson503Filter had been given the chance to do its job, it would have rejected this request.
     * You know what?  That sounds like a really good idea...
     * </p>
     */
    private static void rejectWith503(final HttpServletRequest req, final ServletResponse res) throws IOException {
        log.warn("Rejecting security-sensitive request that bypassed Johnson filter: {}", req.getRequestURI());
        respondWithEmpty503(res);
    }
}
