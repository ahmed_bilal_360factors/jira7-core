package com.atlassian.jira.web.action.admin.user;

import com.atlassian.core.util.StringUtils;
import com.atlassian.crowd.embedded.api.CrowdDirectoryService;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.crowd.exception.runtime.OperationFailedException;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.group.GroupService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.util.GroupToPermissionSchemeMapper;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.GlobalPermissionGroupAssociationUtil;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.SessionKeys;
import com.atlassian.jira.web.action.AbstractBrowser;
import com.atlassian.jira.web.bean.GroupBrowserFilter;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.jira.web.component.admin.group.GroupLabelView;
import com.atlassian.jira.web.component.admin.group.GroupLabelsService;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.ofbiz.core.entity.GenericEntityException;
import webwork.action.ActionContext;
import webwork.util.BeanUtil;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@WebSudoRequired
public class GroupBrowser extends AbstractBrowser {
    private List<Group> groups;
    private String addName;
    private GroupToPermissionSchemeMapper groupPermissionSchemeMapper;
    private final GlobalPermissionGroupAssociationUtil globalPermissionGroupAssociationUtil;
    private final CrowdService crowdService;
    private final GroupManager groupManager;
    private final UserManager userManager;
    private final CrowdDirectoryService crowdDirectoryService;
    private final GroupService groupService;
    private final GroupLabelsService groupLabels;

    public GroupBrowser(GroupToPermissionSchemeMapper groupToPermissionSchemeMapper, UserManager userManager,
                        GlobalPermissionGroupAssociationUtil globalPermissionGroupAssociationUtil, CrowdService crowdService,
                        CrowdDirectoryService crowdDirectoryService, GroupManager groupManager, GroupService groupService,
                        GroupLabelsService groupLabels) {
        this.globalPermissionGroupAssociationUtil = globalPermissionGroupAssociationUtil;
        this.crowdService = crowdService;
        this.crowdDirectoryService = crowdDirectoryService;
        this.groupService = groupService;
        this.groupManager = groupManager;
        this.userManager = userManager;
        this.groupLabels = groupLabels;
        if (groupToPermissionSchemeMapper != null) {
            this.groupPermissionSchemeMapper = groupToPermissionSchemeMapper;
        } else {
            addErrorMessage(getText("groupbrowser.error.retrieve.group"));
        }
    }

    public GroupBrowser(GroupManager groupManager, UserManager userManager, CrowdService crowdService,
                        CrowdDirectoryService crowdDirectoryService,
                        GlobalPermissionGroupAssociationUtil globalPermissionGroupAssociationUtil, GroupService groupService,
                        GroupLabelsService groupLabels)
            throws GenericEntityException {
        this(new GroupToPermissionSchemeMapper(ComponentAccessor.getPermissionSchemeManager(),
                        ComponentAccessor.getPermissionManager()),
                userManager,
                globalPermissionGroupAssociationUtil,
                crowdService,
                crowdDirectoryService,
                groupManager,
                groupService,
                groupLabels);
    }

    protected String doExecute() throws Exception {
        resetPager();

        BeanUtil.setProperties(params, getFilter());

        return SUCCESS;
    }

    @RequiresXsrfCheck
    public String doAdd() throws Exception {
        if (!addNewGroup()) {
            return ERROR;
        }
        return doExecute();
    }

    private boolean addNewGroup() {
        if (org.apache.commons.lang.StringUtils.isEmpty(addName)) {
            addError("addName", getText("admin.errors.cannot.add.groups.invalid.group.name"));
            return false;
        }

        //JRA-12112: If external *user* management is enabled, we do not allow the addtion of a new user.
        if (!userManager.hasGroupWritableDirectory()) {
            addErrorMessage(getText("admin.errors.cannot.add.groups.directories.read.only"));
            return false;
        }

        if (crowdService.getGroup(addName) == null) {
            try {
                crowdService.addGroup(new ImmutableGroup(addName));
            } catch (OperationNotPermittedException | InvalidGroupException | OperationFailedException e) {
                addError("addName", getText("groupbrowser.error.add", addName));
                log.error("Error occurred adding group : " + addName, e);
            }
            addName = null;
        } else {
            addError("addName", getText("groupbrowser.error.group.exists"));
        }
        return true;
    }

    public PagerFilter getPager() {
        return getFilter();
    }

    public void resetPager() {
        ActionContext.getSession().put(SessionKeys.GROUP_FILTER, null);
    }

    public GroupBrowserFilter getFilter() {
        GroupBrowserFilter filter = (GroupBrowserFilter) ActionContext.getSession().get(SessionKeys.GROUP_FILTER);

        if (filter == null) {
            filter = new GroupBrowserFilter();
            ActionContext.getSession().put(SessionKeys.GROUP_FILTER, filter);
        }

        return filter;
    }

    /**
     * Return the current 'page' of issues (given max and start) for the current filter
     */
    public List getCurrentPage() {
        return getFilter().getCurrentPage(getBrowsableItems());
    }

    public List getBrowsableItems() {
        if (groups == null) {
            try {
                groups = getFilter().getFilteredGroups();
            } catch (Exception e) {
                log.error("Exception getting groups: " + e, e);
                return Collections.emptyList();
            }
        }

        return groups;
    }

    public String getAddName() {
        return addName;
    }

    public void setAddName(String addName) {
        this.addName = addName.trim();
    }

    public String escapeAmpersand(String str) {
        return StringUtils.replaceAll(str, "&", "%26");
    }

    public int getUsersInGroupCount(Group group) {
        return groupManager.getUsersInGroupCount(group);
    }

    public Collection<Scheme> getPermissionSchemes(String groupName) {
        if (groupPermissionSchemeMapper != null) {
            return groupPermissionSchemeMapper.getMappedValues(groupName);
        } else {
            return Collections.emptyList();
        }
    }

    private Boolean hasGroupWritableDirectory = null;

    public boolean hasGroupWritableDirectory() {
        if (hasGroupWritableDirectory == null) {
            hasGroupWritableDirectory = new Boolean(userManager.hasGroupWritableDirectory());
        }
        return hasGroupWritableDirectory.booleanValue();
    }

    /**
     * Test if the current user has the necessary permissions to permit deletion of given group.
     */
    public boolean isUserAbleToDeleteGroup(String groupName) {
        return globalPermissionGroupAssociationUtil.isUserAbleToDeleteGroup(
                getLoggedInUser(), groupName);
    }

    /**
     * Returns the empty string if the given group (name) is potentially deletable, otherwise returns the
     * reason why it's not. Note that this is a different test than {@link #isUserAbleToDeleteGroup(String)}.
     *
     * @return reason why group would not be deletable, otherwise the empty string.
     * @see GroupService#validateDelete
     */
    @Nonnull
    public String isGroupDeletable(@Nonnull String groupName) {
        JiraServiceContext currentRequest = getJiraServiceContext();
        ErrorCollection potentialDeleteErrors = new SimpleErrorCollection();
        JiraServiceContext potentialDelete = new JiraServiceContextImpl(
                currentRequest.getLoggedInApplicationUser(), potentialDeleteErrors, currentRequest.getI18nBean());

        // if group delete would fail, then passed ErrorCollection will be non-empty
        groupService.validateDelete(potentialDelete, groupName, null);

        if (potentialDeleteErrors.hasAnyErrors()) {
            // then delete is expected to fail.

            // special case: if there are comments/worklogs associated to this group, then validateDelete
            // will return false. however the group is still potentially deletable on the delete confirm
            // page via assignment to a swap group.
            if (groupService.getCommentsAndWorklogsGuardedByGroupCount(groupName) > 0) {
                // delete could still succeed
                return "";
            }

            return potentialDeleteErrors.getErrorMessages().iterator().next();
        } else {
            // then delete should succeed
            return "";
        }
    }

    /**
     * Return true if any directory supports nested groups.
     *
     * @return true if any directory supports nested groups.
     */
    public boolean isNestedGroupsEnabledForAnyDirectory() {
        for (Directory directory : crowdDirectoryService.findAllDirectories()) {
            if (crowdDirectoryService.supportsNestedGroups(directory.getId())) {
                return true;
            }
        }
        return false;
    }

    public List<GroupLabelView> getGroupLabels(Group group) {
        return groupLabels.getGroupLabels(group, Optional.<Long>empty());
    }
}
