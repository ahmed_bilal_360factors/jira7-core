package com.atlassian.jira.web.servlet;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarFormatPolicy;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.io.MediaConsumer;
import com.atlassian.jira.util.Consumer;
import org.apache.commons.io.IOUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

public class AvatarToStream {
    private final AvatarManager avatarManager;

    public AvatarToStream(final AvatarManager avatarManager) {
        this.avatarManager = avatarManager;
    }

    public void sendAvatar(final Avatar avatar, final Avatar.Size size, final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        final AvatarFormatPolicy avatarFormatPolicy = toAvatarRequestedFormat(Optional.ofNullable(request.getParameter("format")));

        HttpResponseHeaders.cachePrivatelyForAboutOneYear(response);

        avatarManager.readAvatarData(
                avatar,
                size,
                avatarFormatPolicy,
                new MediaConsumer(response::setContentType, getDataConsumer(response))
        );
    }

    private Consumer<InputStream> getDataConsumer(final HttpServletResponse response) {
        return stream -> {
            try {
                final byte[] bytes = IOUtils.toByteArray(stream);
                response.setContentLength(bytes.length);
                response.getOutputStream().write(bytes);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        };
    }

    /**
     * Parses the upper cased string parameter and returns a {@link AvatarFormatPolicy}.
     * <p>
     * If the parameter is not valid, and fails to be parsed, then {@link AvatarFormatPolicy#createOriginalDataPolicy()}
     * will be returned
     *
     * @param imageFormat format value as Optional String
     * @return {@link AvatarFormatPolicy} according to the value or {@link AvatarFormatPolicy#createOriginalDataPolicy()}
     * if null or invalid
     */
    public static AvatarFormatPolicy toAvatarRequestedFormat(final Optional<String> imageFormat) {

        return imageFormat
                .filter(format -> !format.isEmpty())
                .map(String::toUpperCase)
                .map(format -> {
                    switch (format) {
                        case "PNG":
                            return AvatarFormatPolicy.createPngFormatPolicy().withFallingBackToOriginalDataStrategy();
                        default:
                            return AvatarFormatPolicy.createOriginalDataPolicy();
                    }
                })
                .orElse(AvatarFormatPolicy.createOriginalDataPolicy());
    }
}
