package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.upgrade.LegacyImmediateUpgradeTask;
import com.atlassian.jira.upgrade.tasks.util.Sequences;

import java.sql.Connection;

/**
 * Add a correctly initialised sequence for EventType
 *
 * @since 6.3
 */
public class UpgradeTask_Build6320 extends LegacyImmediateUpgradeTask {
    private final Sequences sequences;

    public UpgradeTask_Build6320(Sequences sequences) {
        super();
        this.sequences = sequences;
    }

    @Override
    public String getShortDescription() {
        return "Insert sequence for event type";
    }

    @Override
    public int getBuildNumber() {
        return 6320;
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception {
        try (Connection connection = getDatabaseConnection()) {
            sequences.update(connection, "EventType", "jiraeventtype");
        }
    }
}
