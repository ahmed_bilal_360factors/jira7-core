package com.atlassian.jira.web.action.setup;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.exception.CrowdException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.jira.application.ApplicationConfigurationHelper;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.bc.user.UserService.CreateUserRequest;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.FileFactory;
import com.atlassian.jira.util.JiraProductInformation;

import java.util.Collections;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

public class SetupAdminAccount extends AbstractSetupAction {
    final ApplicationConfigurationHelper appConfigHelper;
    private final UserService userService;
    private final GroupManager groupManager;
    private final UserUtil userUtil;
    private final JiraLicenseService jiraLicenseService;
    private final JiraWebResourceManager webResourceManager;
    private final GlobalPermissionManager globalPermissionManager;
    String username;
    String fullname;
    String firstname;
    String lastname;
    String email;
    String password;
    String confirm;
    private String EXISTING_ADMINS = "existingadmins";
    private UserService.CreateUserValidationResult result;

    public SetupAdminAccount(final UserService userService, final GroupManager groupManager, final UserUtil userUtil,
                             final FileFactory fileFactory,
                             final JiraLicenseService jiraLicenseService, final JiraWebResourceManager webResourceManager,
                             final JiraProperties jiraProperties, final GlobalPermissionManager globalPermissionManager,
                             final JiraProductInformation jiraProductInformation,
                             final ApplicationConfigurationHelper appConfigHelper) {
        super(fileFactory, jiraProperties, jiraProductInformation);
        this.userService = userService;
        this.groupManager = groupManager;
        this.userUtil = userUtil;
        this.jiraLicenseService = jiraLicenseService;
        this.webResourceManager = webResourceManager;
        this.globalPermissionManager = notNull("globalPermissionManager", globalPermissionManager);
        this.appConfigHelper = notNull("appConfigHelper", appConfigHelper);
    }

    @Override
    public String doDefault() throws Exception {
        if (setupAlready()) {
            return SETUP_ALREADY;
        }

        if (userUtil.getJiraAdministrators().size() > 0) {
            return EXISTING_ADMINS;
        }

        putSENIntoMetadata();

        return super.doDefault();
    }

    @Override
    protected void doValidation() {
        // return with no error messages, doExecute() will return the already setup view
        if (setupAlready()) {
            return;
        }
        final String adminUsername = getUsername() != null ? getUsername().trim() : getUsername();
        final CreateUserRequest createUserRequest = CreateUserRequest
                .withUserDetails(getLoggedInUser(), adminUsername, getPassword(), getEmail(), getFullname())
                .confirmPassword(getConfirm())
                .passwordRequired()
                .performPermissionCheck(false)
                .sendNotification(false);
        result = userService.validateCreateUser(createUserRequest);
        if (!result.isValid()) {
            addErrorCollection(result.getErrorCollection());
        }
    }

    @Override
    protected String doExecute() {
        if (setupAlready()) {
            return SETUP_ALREADY;
        }

        ApplicationUser administrator = null;
        try {
            if (!userUtil.getJiraAdministrators().isEmpty()) {
                return EXISTING_ADMINS;
            }

            try {
                administrator = userService.createUser(result);
            } catch (PermissionException e) {
                addErrorMessage(getText("signup.error.group.database.immutable", result.getUsername()));
            }
            final Group groupAdmins = getOrCreateGroup(DEFAULT_GROUP_ADMINS);

            if (administrator != null && groupAdmins != null) {
                try {
                    if (!groupManager.isUserInGroup(administrator, groupAdmins)) {
                        groupManager.addUserToGroup(administrator, groupAdmins);
                    }
                } catch (CrowdException e) {
                    throw new RuntimeException(e);
                }

                // Make sure to enable admin users to change licenses during install (in case the license used is too
                // old for the JIRA version)
                if (!globalPermissionManager.getGroupNames(Permissions.ADMINISTER).contains(DEFAULT_GROUP_ADMINS)) {
                    globalPermissionManager.addPermission(Permissions.ADMINISTER, DEFAULT_GROUP_ADMINS);
                }

                appConfigHelper.configureApplicationsForSetup(Collections.singleton(groupAdmins), true);
                appConfigHelper.setupAdminForDefaultApplications(administrator);
            }

        } catch (CreateException e) {
            throw new RuntimeException(e);
        }

        // Store the username in the session so that we can automatically log the user in, in SetupComplete
        request.getSession().setAttribute(SetupAdminUserSessionStorage.SESSION_KEY, new SetupAdminUserSessionStorage(result.getUsername()));

        putSENIntoMetadata();

        return getResult();
    }

    /**
     * try and get or create a group, if we get a problem let the user know
     *
     * @param groupName the name of the group to get or create
     * @return a Group if one is found or can be created, null otherwise
     */
    private Group getOrCreateGroup(String groupName) {
        Group group = groupManager.getGroup(groupName);
        if (group != null) {
            return group;
        }
        try {
            return groupManager.createGroup(groupName);
        } catch (OperationNotPermittedException | InvalidGroupException e) {
            addErrorMessage(getText("signup.error.group.database.immutable", groupName));
        }
        return null;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    // Attributes that can be set through URL parameters
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * This is used only for purpose of setup analytics
     */
    private void putSENIntoMetadata() {
        if (!jiraLicenseService.getSupportEntitlementNumbers().isEmpty()) {
            webResourceManager.putMetadata("SEN", jiraLicenseService.getSupportEntitlementNumbers().first());
        }
    }
}
