package com.atlassian.jira.license;

import com.atlassian.jira.component.ComponentAccessor;
import com.google.common.base.Joiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.filter;
import static java.util.stream.Collectors.toList;

/**
 * @since 7.0
 */
public class ClusterLicenseCheckImpl implements ClusterLicenseCheck {
    private static final Logger LOG = LoggerFactory.getLogger(ClusterLicenseCheckImpl.class);

    @Override
    public Result evaluate() {
        JiraLicenseManager licenseManager = ComponentAccessor.getComponent(JiraLicenseManager.class);

        if (!licenseManager.isLicenseSet()) {
            return FAIL_NO_LICENSES;
        }

        List<LicenseDetails> licenses = copyOf(licenseManager.getLicenses());
        List<LicenseDetails> nonDataCentreLicenses = copyOf(filter(licenses, license -> !license.isDataCenter()));

        if (!nonDataCentreLicenses.isEmpty()) {
            if (licenses.size() != nonDataCentreLicenses.size()) {
                // then heterogeneous mix of DC with non-DC licenses
                String whichLicenses = describeLicenses(nonDataCentreLicenses);
                LOG.warn("Clustering not permitted as there are non-DataCenter licenses present: " + whichLicenses);

                return new Failure(nonDataCentreLicenses,
                        "Clustering not permitted as there are non-DataCenter licenses present: " + whichLicenses);
            }

            return new Failure(nonDataCentreLicenses, "Instance is not licensed for clustering");
        }

        return PASS;
    }

    private String describeLicenses(List<LicenseDetails> nonDataCentreLicenses) {
        return Joiner.on(", ").join(
                nonDataCentreLicenses.stream()
                        .map(lic -> "\"" + lic.getDescription() + "\"")
                        .collect(toList()));
    }
}
