package com.atlassian.jira.onboarding;

import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;

/**
 * Centralises all logic related to user properties and conditions during the onboarding flow
 *
 * @since v6.5
 */
public interface UserChecker {
    /**
     * Tests whether this is the first time a user has logged in.
     *
     * @param user logging in
     * @return whether they have logged in before
     */
    boolean firstTimeLoggingIn(@Nonnull final ApplicationUser user);

    /**
     * Checks whether a given user is a sysadmin.
     *
     * @param user logged in
     * @return whether the user is a sysadmin
     */
    @Deprecated
    boolean isOnDemandSysAdmin(@Nonnull final ApplicationUser user);

    /**
     * Checks whether the current user is impersonating a second user
     *
     * @param user currently logged in (as returned by JiraAuthenticationContext)
     * @return whether or not the impersonation feature was detected as active
     */
    boolean isImpersonationActive(@Nonnull final ApplicationUser user);
}
