package com.atlassian.jira.help;

import com.atlassian.fugue.Option;
import com.google.common.annotations.VisibleForTesting;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Map;

/**
 * @since v6.2.4
 */
public class SimpleHelpUrlBuilder extends HelpUrlBuilderTemplate {
    @VisibleForTesting
    SimpleHelpUrlBuilder(String prefix, String suffix) {
        super(prefix, suffix);
    }

    @Nonnull
    @Override
    Map<String, String> getExtraParameters() {
        return Collections.emptyMap();
    }

    @Override
    HelpUrlBuilder newInstance() {
        return new SimpleHelpUrlBuilder(getPrefix(), getSuffix());
    }

    public static class Factory extends HelpUrlBuilderFactoryTemplate {

        public Factory(final String docsVersion) {
            this(docsVersion, Option.none());
        }

        public Factory(final String docsVersion, final Option<String> applicationHelpSpaceVersion) {
            super(docsVersion, applicationHelpSpaceVersion);
        }

        @Override
        HelpUrlBuilder newUrlBuilder(final String prefix, final String suffix) {
            return new SimpleHelpUrlBuilder(prefix, suffix);
        }
    }
}
